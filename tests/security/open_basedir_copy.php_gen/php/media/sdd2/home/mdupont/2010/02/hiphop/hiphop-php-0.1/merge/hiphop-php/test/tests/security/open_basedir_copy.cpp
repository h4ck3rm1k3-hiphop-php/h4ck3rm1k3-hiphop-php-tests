
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_copy.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_copy_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_copy.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_copy_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  LINE(3,f_test_open_basedir_before("copy"));
  LINE(5,x_var_dump(1, x_copy("ok.txt", "../bad")));
  LINE(6,x_var_dump(1, x_copy("ok.txt", "../bad/bad.txt")));
  LINE(7,x_var_dump(1, x_copy("ok.txt", "..")));
  LINE(8,x_var_dump(1, x_copy("ok.txt", "../")));
  LINE(9,x_var_dump(1, x_copy("ok.txt", "/")));
  LINE(10,x_var_dump(1, x_copy("ok.txt", "../bad/.")));
  LINE(11,x_var_dump(1, x_copy("ok.txt", "../bad/./bad.txt")));
  LINE(12,x_var_dump(1, x_copy("ok.txt", "./../.")));
  LINE(14,x_var_dump(1, x_copy("ok.txt", "copy.txt")));
  LINE(15,x_var_dump(1, x_unlink("copy.txt")));
  LINE(16,f_test_open_basedir_after("copy"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
