
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_readlink.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_readlink_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_readlink.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_readlink_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_initdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initdir") : g->GV(initdir);
  Variant &v_target __attribute__((__unused__)) = (variables != gVariables) ? variables->get("target") : g->GV(target);
  Variant &v_symlink __attribute__((__unused__)) = (variables != gVariables) ? variables->get("symlink") : g->GV(symlink);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  (v_initdir = LINE(3,x_getcwd()));
  LINE(4,f_test_open_basedir_before("readlink", false));
  LINE(6,x_chdir(toString(v_initdir)));
  (v_target = (concat(toString(v_initdir), "/test/bad/bad.txt")));
  (v_symlink = (concat(toString(v_initdir), "/test/ok/symlink.txt")));
  LINE(10,x_var_dump(1, x_symlink(toString(v_target), toString(v_symlink))));
  LINE(12,x_chdir(concat(toString(v_initdir), "/test/ok")));
  LINE(14,x_var_dump(1, x_readlink("symlink.txt")));
  LINE(15,x_var_dump(1, x_readlink("../ok/symlink.txt")));
  LINE(16,x_var_dump(1, x_readlink("../ok/./symlink.txt")));
  LINE(17,x_var_dump(1, x_readlink("./symlink.txt")));
  LINE(18,x_var_dump(1, x_readlink(concat(toString(v_initdir), "/test/ok/symlink.txt"))));
  (v_target = (concat(toString(v_initdir), "/test/ok/ok.txt")));
  (v_symlink = (concat(toString(v_initdir), "/test/ok/symlink.txt")));
  LINE(22,x_var_dump(1, x_symlink(toString(v_target), toString(v_symlink))));
  LINE(23,x_var_dump(1, x_readlink(toString(v_symlink))));
  LINE(24,x_var_dump(1, x_unlink(toString(v_symlink))));
  LINE(26,f_test_open_basedir_after("readlink"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
