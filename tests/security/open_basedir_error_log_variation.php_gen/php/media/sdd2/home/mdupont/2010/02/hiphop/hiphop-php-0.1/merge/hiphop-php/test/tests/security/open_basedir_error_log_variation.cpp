
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_error_log_variation.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_DESTINATION_IS_FILE = 3LL;

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_error_log_variation_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_error_log_variation.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_error_log_variation_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_initdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initdir") : g->GV(initdir);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  (v_initdir = LINE(3,x_getcwd()));
  LINE(4,f_test_open_basedir_before("error_log"));
  ;
  LINE(8,x_var_dump(1, x_error_log("Hello World!", toInt32(3LL) /* DESTINATION_IS_FILE */, concat(toString(v_initdir), "/test/bad/bad.txt"))));
  LINE(9,x_var_dump(1, x_error_log("Hello World!", toInt32(3LL) /* DESTINATION_IS_FILE */, concat(toString(v_initdir), "/test/bad.txt"))));
  LINE(10,x_var_dump(1, x_error_log("Hello World!", toInt32(3LL) /* DESTINATION_IS_FILE */, concat(toString(v_initdir), "/bad.txt"))));
  LINE(11,x_var_dump(1, x_error_log("Hello World!", toInt32(3LL) /* DESTINATION_IS_FILE */, concat(toString(v_initdir), "/test/ok/ok.txt"))));
  LINE(13,f_test_open_basedir_after("error_log"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
