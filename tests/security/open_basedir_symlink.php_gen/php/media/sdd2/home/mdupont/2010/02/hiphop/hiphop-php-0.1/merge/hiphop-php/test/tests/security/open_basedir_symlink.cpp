
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_symlink.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_symlink_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_symlink.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_symlink_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_initdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initdir") : g->GV(initdir);
  Variant &v_target __attribute__((__unused__)) = (variables != gVariables) ? variables->get("target") : g->GV(target);
  Variant &v_symlink __attribute__((__unused__)) = (variables != gVariables) ? variables->get("symlink") : g->GV(symlink);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  (v_initdir = LINE(3,x_getcwd()));
  LINE(4,f_test_open_basedir_before("symlink"));
  (v_target = (concat(toString(v_initdir), "/test/ok/ok.txt")));
  LINE(7,x_var_dump(1, x_symlink(toString(v_target), "../bad/symlink.txt")));
  LINE(8,x_var_dump(1, x_symlink(toString(v_target), "../symlink.txt")));
  LINE(9,x_var_dump(1, x_symlink(toString(v_target), "../bad/./symlink.txt")));
  LINE(10,x_var_dump(1, x_symlink(toString(v_target), "./.././symlink.txt")));
  (v_symlink = (concat(toString(v_initdir), "/test/ok/symlink.txt")));
  LINE(13,x_var_dump(1, x_symlink("../bad/bad.txt", toString(v_symlink))));
  LINE(14,x_var_dump(1, x_symlink("../bad", toString(v_symlink))));
  LINE(15,x_var_dump(1, x_symlink("../bad/./bad.txt", toString(v_symlink))));
  LINE(16,x_var_dump(1, x_symlink("../bad/bad.txt", toString(v_symlink))));
  LINE(17,x_var_dump(1, x_symlink("./.././bad", toString(v_symlink))));
  (v_target = (concat(toString(v_initdir), "/test/ok/ok.txt")));
  LINE(21,x_var_dump(1, x_symlink(toString(v_target), toString(v_symlink))));
  LINE(22,x_var_dump(1, x_unlink(toString(v_symlink))));
  LINE(24,x_var_dump(1, x_mkdir("ok2")));
  (v_symlink = (concat(toString(v_initdir), "/test/ok/ok2/ok.txt")));
  LINE(26,x_var_dump(1, x_symlink("../ok.txt", toString(v_symlink))));
  LINE(27,x_var_dump(1, x_unlink(toString(v_symlink))));
  LINE(29,f_test_open_basedir_after("symlink"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
