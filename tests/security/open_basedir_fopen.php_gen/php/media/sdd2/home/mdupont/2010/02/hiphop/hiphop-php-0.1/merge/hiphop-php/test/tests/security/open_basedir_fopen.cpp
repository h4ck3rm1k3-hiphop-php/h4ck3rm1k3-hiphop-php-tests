
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_fopen.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_fopen_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_fopen.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_fopen_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_initdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initdir") : g->GV(initdir);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  (v_initdir = LINE(3,x_getcwd()));
  LINE(4,f_test_open_basedir_before("fopen"));
  LINE(6,x_var_dump(1, x_fopen("../bad", "r")));
  LINE(7,x_var_dump(1, x_fopen("../bad/bad.txt", "r")));
  LINE(8,x_var_dump(1, x_fopen("..", "r")));
  LINE(9,x_var_dump(1, x_fopen("../", "r")));
  LINE(10,x_var_dump(1, x_fopen("/", "r")));
  LINE(11,x_var_dump(1, x_fopen("../bad/.", "r")));
  LINE(12,x_var_dump(1, x_fopen("../bad/./bad.txt", "r")));
  LINE(13,x_var_dump(1, x_fopen("./../.", "r")));
  LINE(15,x_var_dump(1, x_fopen(concat(toString(v_initdir), "/test/ok/ok.txt"), "r")));
  LINE(16,x_var_dump(1, x_fopen("./ok.txt", "r")));
  LINE(17,x_var_dump(1, x_fopen("ok.txt", "r")));
  LINE(18,x_var_dump(1, x_fopen("../ok/ok.txt", "r")));
  LINE(19,x_var_dump(1, x_fopen("../ok/./ok.txt", "r")));
  LINE(21,f_test_open_basedir_after("fopen"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
