
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_chmod.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_chmod_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_chmod.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_chmod_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_initdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initdir") : g->GV(initdir);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  (v_initdir = LINE(3,x_getcwd()));
  LINE(5,f_test_open_basedir_before("chmod"));
  LINE(7,x_var_dump(1, x_chmod("../bad", 384LL)));
  LINE(8,x_var_dump(1, x_chmod("../bad/bad.txt", 384LL)));
  LINE(9,x_var_dump(1, x_chmod("..", 384LL)));
  LINE(10,x_var_dump(1, x_chmod("../", 384LL)));
  LINE(11,x_var_dump(1, x_chmod("/", 384LL)));
  LINE(12,x_var_dump(1, x_chmod("../bad/.", 384LL)));
  LINE(13,x_var_dump(1, x_chmod("../bad/./bad.txt", 384LL)));
  LINE(14,x_var_dump(1, x_chmod("./../.", 384LL)));
  LINE(16,x_var_dump(1, x_chmod(concat(toString(v_initdir), "/test/ok/ok.txt"), 384LL)));
  LINE(17,x_var_dump(1, x_chmod("./ok.txt", 384LL)));
  LINE(18,x_var_dump(1, x_chmod("ok.txt", 384LL)));
  LINE(19,x_var_dump(1, x_chmod("../ok/ok.txt", 384LL)));
  LINE(20,x_var_dump(1, x_chmod("../ok/./ok.txt", 384LL)));
  LINE(21,x_chmod(concat(toString(v_initdir), "/test/ok/ok.txt"), 511LL));
  LINE(23,f_test_open_basedir_after("chmod"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
