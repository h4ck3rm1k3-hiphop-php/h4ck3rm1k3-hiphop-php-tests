
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_parse_ini_file.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_parse_ini_file_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_parse_ini_file.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_parse_ini_file_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directory __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directory") : g->GV(directory);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  LINE(3,f_test_open_basedir_before("parse_ini_file"));
  (v_directory = LINE(4,x_dirname(get_source_filename("/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_parse_ini_file.php"))));
  LINE(6,x_var_dump(1, x_parse_ini_file("../bad")));
  LINE(7,x_var_dump(1, x_parse_ini_file("../bad/bad.txt")));
  LINE(8,x_var_dump(1, x_parse_ini_file("..")));
  LINE(9,x_var_dump(1, x_parse_ini_file("../")));
  LINE(10,x_var_dump(1, x_parse_ini_file("../bad/.")));
  LINE(11,x_var_dump(1, x_parse_ini_file("../bad/./bad.txt")));
  LINE(12,x_var_dump(1, x_parse_ini_file("./../.")));
  LINE(14,f_test_open_basedir_after("parse_ini_file"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
