
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_security_open_basedir_inc_nophp_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_security_open_basedir_inc_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_test_open_basedir_after(CVarRef v_function);
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(bool incOnce = false, LVariableTable* variables = NULL);
bool f_recursive_delete_directory(Variant v_directory);
void f_delete_directories();
void f_test_open_basedir_array(CStrRef v_function);
void f_test_open_basedir_before(CVarRef v_function, bool v_change = true);
void f_test_open_basedir(CVarRef v_function);
void f_test_open_basedir_error(CVarRef v_function);
void f_create_directories();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_security_open_basedir_inc_nophp_h__
