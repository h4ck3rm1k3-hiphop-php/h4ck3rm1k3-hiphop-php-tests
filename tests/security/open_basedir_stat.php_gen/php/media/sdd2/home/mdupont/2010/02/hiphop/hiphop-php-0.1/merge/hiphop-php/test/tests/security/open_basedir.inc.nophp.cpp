
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 104 */
void f_test_open_basedir_after(CVarRef v_function) {
  FUNCTION_INJECTION(test_open_basedir_after);
  echo(LINE(105,concat3("*** Finished testing open_basedir configuration [", toString(v_function), "] ***\n")));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 19 */
bool f_recursive_delete_directory(Variant v_directory) {
  FUNCTION_INJECTION(recursive_delete_directory);
  Variant v_handle;
  Variant v_item;
  String v_path;

  if (equal(LINE(22,x_substr(toString(v_directory), toInt32(-1LL))), "/")) {
    (v_directory = LINE(23,x_substr(toString(v_directory), toInt32(0LL), toInt32(-1LL))));
  }
  if (equal(LINE(27,x_is_dir(toString(v_directory))), false)) {
    return false;
  }
  if (equal(LINE(32,x_is_readable(toString(v_directory))), false)) {
    return false;
  }
  (v_handle = LINE(36,x_opendir(toString(v_directory))));
  LOOP_COUNTER(1);
  {
    while (!same(false, ((v_item = LINE(39,x_readdir(toObject(v_handle))))))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (!equal(v_item, ".")) {
          if (!equal(v_item, "..")) {
            (v_path = (LINE(42,concat3(toString(v_directory), "/", toString(v_item)))));
            if (equal(LINE(43,x_is_dir(v_path)), true)) {
              LINE(44,f_recursive_delete_directory(v_path));
            }
            else {
              (silenceInc(), silenceDec(LINE(46,x_chmod(v_path, 511LL))));
              LINE(47,x_unlink(v_path));
            }
          }
        }
      }
    }
  }
  LINE(53,x_closedir(toObject(v_handle)));
  (silenceInc(), silenceDec(LINE(54,x_chmod(toString(v_directory), 511LL))));
  LINE(55,x_rmdir(toString(v_directory)));
  return true;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 71 */
void f_delete_directories() {
  FUNCTION_INJECTION(delete_directories);
  String v_directory;

  (v_directory = (concat(toString(LINE(72,x_getcwd())), "/test")));
  LINE(73,f_recursive_delete_directory(v_directory));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 109 */
void f_test_open_basedir_array(CStrRef v_function) {
  FUNCTION_INJECTION(test_open_basedir_array);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_savedDirectory __attribute__((__unused__)) = g->GV(savedDirectory);
  Variant v_directory;

  LINE(112,f_test_open_basedir_before(v_function));
  LINE(113,f_test_open_basedir_error(v_function));
  LINE(114,x_var_dump(1, x_is_array(invoke(v_function, Array(ArrayInit(1).set(0, "./../.").create()), -1))));
  LINE(115,x_var_dump(1, x_is_array(invoke(v_function, Array(ArrayInit(1).set(0, "../ok").create()), -1))));
  LINE(116,x_var_dump(1, x_is_array(invoke(v_function, Array(ArrayInit(1).set(0, "ok.txt").create()), -1))));
  LINE(117,x_var_dump(1, x_is_array(invoke(v_function, Array(ArrayInit(1).set(0, "../ok/ok.txt").create()), -1))));
  (v_directory = gv_savedDirectory);
  LINE(119,x_var_dump(1, x_is_array(invoke(v_function, Array(ArrayInit(1).set(0, concat(toString(v_directory), "/test/ok/ok.txt")).create()), -1))));
  LINE(120,x_var_dump(1, x_is_array(invoke(v_function, Array(ArrayInit(1).set(0, concat(toString(v_directory), "/test/ok/../ok/ok.txt")).create()), -1))));
  LINE(121,f_test_open_basedir_after(v_function));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 89 */
void f_test_open_basedir_before(CVarRef v_function, bool v_change //  = true
) {
  FUNCTION_INJECTION(test_open_basedir_before);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_savedDirectory __attribute__((__unused__)) = g->GV(savedDirectory);
  Variant v_directory;

  echo(LINE(91,concat3("*** Testing open_basedir configuration [", toString(v_function), "] ***\n")));
  (v_directory = LINE(92,x_getcwd()));
  (gv_savedDirectory = v_directory);
  LINE(94,x_var_dump(1, x_chdir(toString(v_directory))));
  LINE(95,f_create_directories());
  if (equal(v_change, true)) {
    LINE(99,x_var_dump(1, x_chdir(concat(toString(v_directory), "/test/ok"))));
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 124 */
void f_test_open_basedir(CVarRef v_function) {
  FUNCTION_INJECTION(test_open_basedir);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_savedDirectory __attribute__((__unused__)) = g->GV(savedDirectory);
  Variant v_directory;

  LINE(126,f_test_open_basedir_before(v_function));
  LINE(127,f_test_open_basedir_error(v_function));
  LINE(128,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "./../.").create()), -1)));
  LINE(129,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "../ok").create()), -1)));
  LINE(130,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "ok.txt").create()), -1)));
  LINE(131,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "../ok/ok.txt").create()), -1)));
  (v_directory = gv_savedDirectory);
  LINE(133,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, concat(toString(v_directory), "/test/ok/ok.txt")).create()), -1)));
  LINE(134,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, concat(toString(v_directory), "/test/ok/../ok/ok.txt")).create()), -1)));
  LINE(135,f_test_open_basedir_after(v_function));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 76 */
void f_test_open_basedir_error(CVarRef v_function) {
  FUNCTION_INJECTION(test_open_basedir_error);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_savedDirectory __attribute__((__unused__)) = g->GV(savedDirectory);
  Variant v_directory;

  LINE(78,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "../bad").create()), -1)));
  LINE(79,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "../bad/bad.txt").create()), -1)));
  LINE(80,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "..").create()), -1)));
  LINE(81,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "../").create()), -1)));
  LINE(82,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "/").create()), -1)));
  LINE(83,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, "../bad/.").create()), -1)));
  (v_directory = gv_savedDirectory);
  LINE(85,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, concat(toString(v_directory), "/test/bad/bad.txt")).create()), -1)));
  LINE(86,x_var_dump(1, invoke(toString(v_function), Array(ArrayInit(1).set(0, concat(toString(v_directory), "/test/bad/../bad/bad.txt")).create()), -1)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc line 60 */
void f_create_directories() {
  FUNCTION_INJECTION(create_directories);
  Variant v_directory;

  LINE(61,f_delete_directories());
  (v_directory = LINE(62,x_getcwd()));
  LINE(64,x_var_dump(1, x_mkdir(concat(toString(v_directory), "/test"))));
  LINE(65,x_var_dump(1, x_mkdir(concat(toString(v_directory), "/test/ok"))));
  LINE(66,x_var_dump(1, x_mkdir(concat(toString(v_directory), "/test/bad"))));
  LINE(67,x_file_put_contents(concat(toString(v_directory), "/test/ok/ok.txt"), "Hello World!"));
  LINE(68,x_file_put_contents(concat(toString(v_directory), "/test/bad/bad.txt"), "Hello World!"));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
