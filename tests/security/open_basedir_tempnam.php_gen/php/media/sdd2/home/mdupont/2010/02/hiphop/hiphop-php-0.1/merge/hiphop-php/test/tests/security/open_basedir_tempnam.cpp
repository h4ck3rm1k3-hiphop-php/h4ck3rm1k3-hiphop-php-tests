
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_tempnam.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_tempnam_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_tempnam.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_tempnam_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_initdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initdir") : g->GV(initdir);
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  (v_initdir = LINE(3,x_getcwd()));
  LINE(4,f_test_open_basedir_before("tempnam"));
  LINE(6,x_var_dump(1, x_tempnam("../bad", "test")));
  LINE(7,x_var_dump(1, x_tempnam("..", "test")));
  LINE(8,x_var_dump(1, x_tempnam("../", "test")));
  LINE(9,x_var_dump(1, x_tempnam("/", "test")));
  LINE(10,x_var_dump(1, x_tempnam("../bad/.", "test")));
  LINE(11,x_var_dump(1, x_tempnam("./../.", "test")));
  LINE(12,x_var_dump(1, x_tempnam("", "test")));
  (v_file = LINE(15,x_tempnam(concat(toString(v_initdir), "/test/ok"), "test")));
  LINE(16,x_var_dump(1, v_file));
  LINE(17,x_var_dump(1, x_unlink(toString(v_file))));
  (v_file = LINE(20,x_tempnam(".", "test")));
  LINE(21,x_var_dump(1, v_file));
  LINE(22,x_var_dump(1, x_unlink(toString(v_file))));
  (v_file = LINE(24,x_tempnam("../ok", "test")));
  LINE(25,x_var_dump(1, v_file));
  LINE(26,x_var_dump(1, x_unlink(toString(v_file))));
  LINE(28,f_test_open_basedir_after("tempnam"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
