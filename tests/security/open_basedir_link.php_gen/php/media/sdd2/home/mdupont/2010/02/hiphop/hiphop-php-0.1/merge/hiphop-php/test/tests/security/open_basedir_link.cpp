
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_link.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_link_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/security/open_basedir_link.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_link_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_initdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initdir") : g->GV(initdir);
  Variant &v_target __attribute__((__unused__)) = (variables != gVariables) ? variables->get("target") : g->GV(target);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);

  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$security$open_basedir_inc(true, variables));
  (v_initdir = LINE(3,x_getcwd()));
  LINE(4,f_test_open_basedir_before("link"));
  (v_target = (concat(toString(v_initdir), "/test/ok/ok.txt")));
  LINE(7,x_var_dump(1, x_link(toString(v_target), "../bad/link.txt")));
  LINE(8,x_var_dump(1, x_link(toString(v_target), "../link.txt")));
  LINE(9,x_var_dump(1, x_link(toString(v_target), "../bad/./link.txt")));
  LINE(10,x_var_dump(1, x_link(toString(v_target), "./.././link.txt")));
  (v_link = (concat(toString(v_initdir), "/test/ok/link.txt")));
  LINE(13,x_var_dump(1, x_link("../bad/bad.txt", toString(v_link))));
  LINE(14,x_var_dump(1, x_link("../bad", toString(v_link))));
  LINE(15,x_var_dump(1, x_link("../bad/./bad.txt", toString(v_link))));
  LINE(16,x_var_dump(1, x_link("../bad/bad.txt", toString(v_link))));
  LINE(17,x_var_dump(1, x_link("./.././bad", toString(v_link))));
  (v_target = (concat(toString(v_initdir), "/test/ok/ok.txt")));
  LINE(21,x_var_dump(1, x_link(toString(v_target), toString(v_link))));
  LINE(22,x_var_dump(1, x_unlink(toString(v_link))));
  LINE(23,f_test_open_basedir_after("link"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
