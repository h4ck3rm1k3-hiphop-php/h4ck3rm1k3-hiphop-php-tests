
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/strings/002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$strings$002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/strings/002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$strings$002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  LINE(2,x_error_reporting(toInt32(0LL)));
  (toBoolean((v_fp = LINE(4,x_fopen("php://stdout", "w"))))) || (toBoolean(f_exit("Arrggsgg!!")));
  (v_x = LINE(5,x_fprintf(3, toObject(v_fp), "fprintf test 1:%.5s", ScalarArrays::sa_[0])));
  echo("\n");
  LINE(7,x_var_dump(1, v_x));
  LINE(9,x_printf(2, "printf test 1:%s\n", ScalarArrays::sa_[1]));
  LINE(10,x_printf(2, "printf test 2:%d\n", ScalarArrays::sa_[2]));
  LINE(11,x_printf(2, "printf test 3:%f\n", ScalarArrays::sa_[3]));
  LINE(12,x_printf(2, "printf test 4:%.10f\n", ScalarArrays::sa_[3]));
  LINE(13,x_printf(2, "printf test 5:%-10.2f\n", ScalarArrays::sa_[4]));
  LINE(14,x_printf(2, "printf test 6:%-010.2f\n", ScalarArrays::sa_[4]));
  LINE(15,x_printf(2, "printf test 7:%010.2f\n", ScalarArrays::sa_[4]));
  LINE(16,x_printf(2, "printf test 8:<%20s>\n", ScalarArrays::sa_[5]));
  LINE(17,x_printf(2, "printf test 9:<%-20s>\n", ScalarArrays::sa_[6]));
  LINE(18,x_printf(1, "printf test 10: 123456789012345\n"));
  LINE(19,x_printf(2, "printf test 10:<%15s>\n", ScalarArrays::sa_[7]));
  LINE(20,x_printf(1, "printf test 11: 123456789012345678901234567890\n"));
  LINE(21,x_printf(2, "printf test 11:<%30s>\n", ScalarArrays::sa_[7]));
  LINE(22,x_printf(2, "printf test 12:%5.2f\n", ScalarArrays::sa_[8]));
  LINE(23,x_printf(2, "printf test 13:%5d\n", ScalarArrays::sa_[9]));
  LINE(24,x_printf(2, "printf test 14:%c\n", ScalarArrays::sa_[10]));
  LINE(25,x_printf(2, "printf test 15:%b\n", ScalarArrays::sa_[11]));
  LINE(26,x_printf(2, "printf test 16:%x\n", ScalarArrays::sa_[11]));
  LINE(27,x_printf(2, "printf test 17:%X\n", ScalarArrays::sa_[11]));
  LINE(28,x_printf(2, "printf test 18:%16b\n", ScalarArrays::sa_[11]));
  LINE(29,x_printf(2, "printf test 19:%16x\n", ScalarArrays::sa_[11]));
  LINE(30,x_printf(2, "printf test 20:%16X\n", ScalarArrays::sa_[11]));
  LINE(31,x_printf(2, "printf test 21:%016b\n", ScalarArrays::sa_[11]));
  LINE(32,x_printf(2, "printf test 22:%016x\n", ScalarArrays::sa_[11]));
  LINE(33,x_printf(2, "printf test 23:%016X\n", ScalarArrays::sa_[11]));
  LINE(34,x_printf(2, "printf test 24:%.5s\n", ScalarArrays::sa_[0]));
  LINE(35,x_printf(2, "printf test 25:%-2s\n", ScalarArrays::sa_[12]));
  LINE(36,x_printf(3, "printf test 26:%2$d %1$d\n", ScalarArrays::sa_[13]));
  LINE(37,x_printf(4, "printf test 27:%3$d %d %d\n", ScalarArrays::sa_[14]));
  LINE(38,x_printf(3, "printf test 28:%2$02d %1$2d\n", ScalarArrays::sa_[13]));
  LINE(39,x_printf(3, "printf test 29:%2$-2d %1$2d\n", ScalarArrays::sa_[13]));
  print("printf test 30:");
  LINE(40,x_printf(2, "%0$s", ScalarArrays::sa_[15]));
  print("x\n");
  LINE(41,x_vprintf("vprintf test 1:%2$-2d %1$2d\n", ScalarArrays::sa_[13]));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
