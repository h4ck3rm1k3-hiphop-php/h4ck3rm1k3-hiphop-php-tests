
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "str",
    "tok1",
    "tok2",
    "tok3",
    "tok4",
    "test",
    "found1",
    "found2",
    "upper",
    "lower",
    "ok",
    "tests",
    "string",
    "raw",
    "encoded",
    "correct",
    "decoded",
    "quoted",
    "uc",
    "tr",
    "as",
    "ss",
    "ui1",
    "ui2",
    "len",
  };
  if (idx >= 0 && idx < 37) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(str);
    case 13: return GV(tok1);
    case 14: return GV(tok2);
    case 15: return GV(tok3);
    case 16: return GV(tok4);
    case 17: return GV(test);
    case 18: return GV(found1);
    case 19: return GV(found2);
    case 20: return GV(upper);
    case 21: return GV(lower);
    case 22: return GV(ok);
    case 23: return GV(tests);
    case 24: return GV(string);
    case 25: return GV(raw);
    case 26: return GV(encoded);
    case 27: return GV(correct);
    case 28: return GV(decoded);
    case 29: return GV(quoted);
    case 30: return GV(uc);
    case 31: return GV(tr);
    case 32: return GV(as);
    case 33: return GV(ss);
    case 34: return GV(ui1);
    case 35: return GV(ui2);
    case 36: return GV(len);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
