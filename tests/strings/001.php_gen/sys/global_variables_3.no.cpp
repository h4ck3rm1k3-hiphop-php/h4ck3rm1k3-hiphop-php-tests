
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x2027864469AD4382LL, string, 24);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      HASH_INDEX(0x620B6102CF342703LL, tok4, 16);
      break;
    case 7:
      HASH_INDEX(0x66EFD95F1D95FC87LL, raw, 25);
      break;
    case 11:
      HASH_INDEX(0x4AE9C978520FB40BLL, ui1, 34);
      break;
    case 12:
      HASH_INDEX(0x422012670B7CFA8CLL, correct, 27);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x1E799CB8A10F0891LL, tok1, 13);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x3A1DC49AA6CFEA13LL, test, 17);
      break;
    case 26:
      HASH_INDEX(0x628D4BEA03476A1ALL, found2, 19);
      HASH_INDEX(0x730C21608765381ALL, lower, 21);
      break;
    case 28:
      HASH_INDEX(0x362158638F83BD9CLL, str, 12);
      HASH_INDEX(0x407238F7070BC71CLL, decoded, 28);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 37:
      HASH_INDEX(0x781B9D4BA403A7A5LL, tok3, 15);
      break;
    case 38:
      HASH_INDEX(0x2DD6289A4EA3BAA6LL, quoted, 29);
      break;
    case 53:
      HASH_INDEX(0x051D3DAA266C68B5LL, ss, 33);
      break;
    case 54:
      HASH_INDEX(0x216CF7DF7F77ADB6LL, tr, 31);
      break;
    case 61:
      HASH_INDEX(0x1A34E9963A99163DLL, uc, 30);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 79:
      HASH_INDEX(0x0337E0CD99E343CFLL, as, 32);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      HASH_INDEX(0x77D3DAF64A431351LL, ui2, 35);
      break;
    case 88:
      HASH_INDEX(0x29D16D2498C952D8LL, tok2, 14);
      HASH_INDEX(0x39EE6D3C694982D8LL, encoded, 26);
      break;
    case 95:
      HASH_INDEX(0x11FFDF37C0EB2C5FLL, tests, 23);
      break;
    case 96:
      HASH_INDEX(0x668CB96EEDC736E0LL, found1, 18);
      break;
    case 100:
      HASH_INDEX(0x4FA6DB6525ECE264LL, ok, 22);
      break;
    case 107:
      HASH_INDEX(0x733A634F298D78EBLL, upper, 20);
      break;
    case 109:
      HASH_INDEX(0x74D1B88DDEF21D6DLL, len, 36);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 37) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
