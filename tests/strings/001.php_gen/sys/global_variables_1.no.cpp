
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 2:
      HASH_RETURN(0x2027864469AD4382LL, g->GV(string),
                  string);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      HASH_RETURN(0x620B6102CF342703LL, g->GV(tok4),
                  tok4);
      break;
    case 7:
      HASH_RETURN(0x66EFD95F1D95FC87LL, g->GV(raw),
                  raw);
      break;
    case 11:
      HASH_RETURN(0x4AE9C978520FB40BLL, g->GV(ui1),
                  ui1);
      break;
    case 12:
      HASH_RETURN(0x422012670B7CFA8CLL, g->GV(correct),
                  correct);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x1E799CB8A10F0891LL, g->GV(tok1),
                  tok1);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x3A1DC49AA6CFEA13LL, g->GV(test),
                  test);
      break;
    case 26:
      HASH_RETURN(0x628D4BEA03476A1ALL, g->GV(found2),
                  found2);
      HASH_RETURN(0x730C21608765381ALL, g->GV(lower),
                  lower);
      break;
    case 28:
      HASH_RETURN(0x362158638F83BD9CLL, g->GV(str),
                  str);
      HASH_RETURN(0x407238F7070BC71CLL, g->GV(decoded),
                  decoded);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 37:
      HASH_RETURN(0x781B9D4BA403A7A5LL, g->GV(tok3),
                  tok3);
      break;
    case 38:
      HASH_RETURN(0x2DD6289A4EA3BAA6LL, g->GV(quoted),
                  quoted);
      break;
    case 53:
      HASH_RETURN(0x051D3DAA266C68B5LL, g->GV(ss),
                  ss);
      break;
    case 54:
      HASH_RETURN(0x216CF7DF7F77ADB6LL, g->GV(tr),
                  tr);
      break;
    case 61:
      HASH_RETURN(0x1A34E9963A99163DLL, g->GV(uc),
                  uc);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 70:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 79:
      HASH_RETURN(0x0337E0CD99E343CFLL, g->GV(as),
                  as);
      break;
    case 81:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      HASH_RETURN(0x77D3DAF64A431351LL, g->GV(ui2),
                  ui2);
      break;
    case 88:
      HASH_RETURN(0x29D16D2498C952D8LL, g->GV(tok2),
                  tok2);
      HASH_RETURN(0x39EE6D3C694982D8LL, g->GV(encoded),
                  encoded);
      break;
    case 95:
      HASH_RETURN(0x11FFDF37C0EB2C5FLL, g->GV(tests),
                  tests);
      break;
    case 96:
      HASH_RETURN(0x668CB96EEDC736E0LL, g->GV(found1),
                  found1);
      break;
    case 100:
      HASH_RETURN(0x4FA6DB6525ECE264LL, g->GV(ok),
                  ok);
      break;
    case 107:
      HASH_RETURN(0x733A634F298D78EBLL, g->GV(upper),
                  upper);
      break;
    case 109:
      HASH_RETURN(0x74D1B88DDEF21D6DLL, g->GV(len),
                  len);
      break;
    case 110:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
