
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/strings/001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$strings$001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/strings/001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$strings$001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_tok1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tok1") : g->GV(tok1);
  Variant &v_tok2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tok2") : g->GV(tok2);
  Variant &v_tok3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tok3") : g->GV(tok3);
  Variant &v_tok4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tok4") : g->GV(tok4);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_found1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("found1") : g->GV(found1);
  Variant &v_found2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("found2") : g->GV(found2);
  Variant &v_upper __attribute__((__unused__)) = (variables != gVariables) ? variables->get("upper") : g->GV(upper);
  Variant &v_lower __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lower") : g->GV(lower);
  Variant &v_ok __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ok") : g->GV(ok);
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_raw __attribute__((__unused__)) = (variables != gVariables) ? variables->get("raw") : g->GV(raw);
  Variant &v_encoded __attribute__((__unused__)) = (variables != gVariables) ? variables->get("encoded") : g->GV(encoded);
  Variant &v_correct __attribute__((__unused__)) = (variables != gVariables) ? variables->get("correct") : g->GV(correct);
  Variant &v_decoded __attribute__((__unused__)) = (variables != gVariables) ? variables->get("decoded") : g->GV(decoded);
  Variant &v_quoted __attribute__((__unused__)) = (variables != gVariables) ? variables->get("quoted") : g->GV(quoted);
  Variant &v_uc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("uc") : g->GV(uc);
  Variant &v_tr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tr") : g->GV(tr);
  Variant &v_as __attribute__((__unused__)) = (variables != gVariables) ? variables->get("as") : g->GV(as);
  Variant &v_ss __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ss") : g->GV(ss);
  Variant &v_ui1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ui1") : g->GV(ui1);
  Variant &v_ui2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ui2") : g->GV(ui2);
  Variant &v_len __attribute__((__unused__)) = (variables != gVariables) ? variables->get("len") : g->GV(len);

  LINE(3,x_error_reporting(toInt32(0LL)));
  echo("Testing strtok: ");
  (v_str = "testing 1/2\\3");
  (v_tok1 = LINE(8,x_strtok(toString(v_str), " ")));
  (v_tok2 = LINE(9,x_strtok("/")));
  (v_tok3 = LINE(10,x_strtok("\\")));
  (v_tok4 = LINE(11,x_strtok(".")));
  if (!equal(v_tok1, "testing")) {
    echo("failed 1\n");
  }
  else if (!equal(v_tok2, "1")) {
    echo("failed 2\n");
  }
  else if (!equal(v_tok3, "2")) {
    echo("failed 3\n");
  }
  else if (!equal(v_tok4, "3")) {
    echo("failed 4\n");
  }
  else {
    echo("passed\n");
  }
  echo("Testing strstr: ");
  (v_test = "This is a test");
  (v_found1 = LINE(26,x_strstr(toString(v_test), 32LL)));
  (v_found2 = LINE(27,x_strstr(toString(v_test), "a ")));
  if (!equal(v_found1, " is a test")) {
    echo("failed 1\n");
  }
  else if (!equal(v_found2, "a test")) {
    echo("failed 2\n");
  }
  else {
    echo("passed\n");
  }
  echo("Testing strrchr: ");
  (v_test = "fola fola blakken");
  (v_found1 = LINE(38,x_strrchr(toString(v_test), "b")));
  (v_found2 = LINE(39,x_strrchr(toString(v_test), 102LL)));
  if (!equal(v_found1, "blakken")) {
    echo("failed 1\n");
  }
  else if (!equal(v_found2, "fola blakken")) {
    echo("failed 2\n");
  }
  else {
    echo("passed\n");
  }
  echo("Testing strtoupper: ");
  (v_test = "abCdEfg");
  (v_upper = LINE(51,x_strtoupper(toString(v_test))));
  if (equal(v_upper, "ABCDEFG")) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing strtolower: ");
  (v_test = "ABcDeFG");
  (v_lower = LINE(60,x_strtolower(toString(v_test))));
  if (equal(v_lower, "abcdefg")) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing substr: ");
  (v_tests = (v_ok = 0LL));
  (v_string = "string12345");
  v_tests++;
  if (equal(LINE(70,x_substr(toString(v_string), toInt32(2LL), toInt32(10LL))), "ring12345")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(71,x_substr(toString(v_string), toInt32(4LL), toInt32(7LL))), "ng12345")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(72,x_substr(toString(v_string), toInt32(4LL))), "ng12345")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(73,x_substr(toString(v_string), toInt32(10LL), toInt32(2LL))), "5")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(74,x_substr(toString(v_string), toInt32(6LL), toInt32(0LL))), "")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(75,x_substr(toString(v_string), toInt32(-2LL), toInt32(2LL))), "45")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(76,x_substr(toString(v_string), toInt32(1LL), toInt32(-1LL))), "tring1234")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(77,x_substr(toString(v_string), toInt32(-1LL), toInt32(-2LL))), "")) {
    v_ok++;
  }
  v_tests++;
  if (equal(LINE(78,x_substr(toString(v_string), toInt32(-3LL), toInt32(-2LL))), "3")) {
    v_ok++;
  }
  if (equal(v_tests, v_ok)) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  (v_raw = String(" !\"#$%&'()*+,-./0123456789:;<=>\?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\0", 96, AttachLiteral));
  echo("Testing rawurlencode: ");
  (v_encoded = LINE(92,x_rawurlencode(toString(v_raw))));
  (v_correct = "%20%21%22%23%24%25%26%27%28%29%2A%2B%2C-.%2F0123456789%3A%3B%3C%3D%3E%3F%40ABCDEFGHIJKLMNOPQRSTUVWXYZ%5B%5C%5D%5E_%60abcdefghijklmnopqrstuvwxyz%7B%7C%7D~%00");
  if (equal(v_encoded, v_correct)) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing rawurldecode: ");
  (v_decoded = LINE(104,x_rawurldecode(toString(v_correct))));
  if (equal(v_decoded, v_raw)) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing urlencode: ");
  (v_encoded = LINE(112,x_urlencode(toString(v_raw))));
  (v_correct = "+%21%22%23%24%25%26%27%28%29%2A%2B%2C-.%2F0123456789%3A%3B%3C%3D%3E%3F%40ABCDEFGHIJKLMNOPQRSTUVWXYZ%5B%5C%5D%5E_%60abcdefghijklmnopqrstuvwxyz%7B%7C%7D%7E%00");
  if (equal(v_encoded, v_correct)) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing urldecode: ");
  (v_decoded = LINE(124,x_urldecode(toString(v_correct))));
  if (equal(v_decoded, v_raw)) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing quotemeta: ");
  (v_raw = concat(concat_rev(LINE(132,x_chr(93LL)), (assignCallTemp(eo_1, x_chr(91LL)),concat3("a.\\+*\?", eo_1, "^"))), "b$c"));
  (v_quoted = LINE(133,x_quotemeta(toString(v_raw))));
  if (equal(v_quoted, "a\\.\\\\\\+\\*\\\?\\[\\^\\]b\\$c")) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing ufirst: ");
  (v_str = "fahrvergnuegen");
  (v_uc = LINE(142,x_ucfirst(toString(v_str))));
  if (equal(v_uc, "Fahrvergnuegen")) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing strtr: ");
  (v_str = "test abcdefgh");
  (v_tr = LINE(151,x_strtr(toString(v_str), "def", "456")));
  if (equal(v_tr, "t5st abc456gh")) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing addslashes: ");
  (v_str = "\"\\'");
  (v_as = LINE(160,x_addslashes(toString(v_str))));
  if (equal(v_as, "\\\"\\\\\\'")) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing stripslashes: ");
  (v_str = "$\\'");
  (v_ss = LINE(169,x_stripslashes(toString(v_str))));
  if (equal(v_ss, "$'")) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  echo("Testing uniqid: ");
  (v_str = "prefix");
  (v_ui1 = LINE(179,x_uniqid(toString(v_str))));
  (v_ui2 = LINE(180,x_uniqid(toString(v_str))));
  (v_len = toBoolean(LINE(182,x_strncasecmp("Linux" /* PHP_OS */, "CYGWIN", toInt32(6LL)))) ? ((19LL)) : ((29LL)));
  if (equal_rev(LINE(184,x_strlen(toString(v_ui2))), x_strlen(toString(v_ui1))) && equal(x_strlen(toString(v_ui1)), v_len) && !equal(v_ui1, v_ui2)) {
    echo("passed\n");
  }
  else {
    echo("failed!\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
