
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/strings/bug22592.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$strings$bug22592_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/strings/bug22592.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$strings$bug22592_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_correct __attribute__((__unused__)) = (variables != gVariables) ? variables->get("correct") : g->GV(correct);
  Variant &v_wrong __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wrong") : g->GV(wrong);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_wrong = (v_correct = "abcdef"));
  (v_t = v_x.append(("x")));
  LINE(6,x_var_dump(1, v_correct));
  LINE(7,x_var_dump(1, v_wrong));
  v_correct.set(1LL, ("*"), 0x5BCA7C69B794F8CELL);
  v_correct.set(3LL, ("*"), 0x135FDDF6A6BFBBDDLL);
  v_correct.set(5LL, ("*"), 0x350AEB726A15D700LL);
  v_wrong.set(1LL, (v_wrong.set(3LL, (v_wrong.set(5LL, ("*"), 0x350AEB726A15D700LL)), 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL);
  LINE(16,x_var_dump(1, v_correct));
  LINE(17,x_var_dump(1, v_wrong));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
