
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/run-test/test007.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/run-test/test007.php line 24 */
void f_same(CStrRef v_a, CStrRef v_b) {
  FUNCTION_INJECTION(same);
  if (equal(v_a, v_b)) {
    print("OK\n");
  }
  else {
    print(LINE(28,concat5("FAIL  ", v_a, " == ", v_b, "\n")));
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/run-test/test007.php line 7 */
void f_check_dirname(CStrRef v_path) {
  FUNCTION_INJECTION(check_dirname);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_s __attribute__((__unused__)) = g->GV(s);
  Variant v_path1;
  String v_path2;
  Variant v_path3;

  (v_path1 = LINE(9,x_str_replace("%", gv_s, v_path)));
  (v_path2 = LINE(10,x_dirname(toString(v_path1))));
  (v_path3 = LINE(11,x_str_replace(gv_s, "%", v_path2)));
  print(LINE(12,concat5("dirname(", v_path, ") == ", toString(v_path3), "\n")));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$run_test$test007_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/run-test/test007.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$run_test$test007_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);

  (v_s = LINE(4,x_dirname("/foo")));
  LINE(15,f_check_dirname("%foo%"));
  LINE(16,f_check_dirname("%foo"));
  LINE(17,f_check_dirname("%foo%bar"));
  LINE(18,f_check_dirname("%"));
  LINE(19,f_check_dirname("...%foo"));
  LINE(20,f_check_dirname(".%foo"));
  LINE(21,f_check_dirname("foobar%%%"));
  LINE(22,f_check_dirname(String("%\0%\0%\0.%\0.", 10, AttachLiteral)));
  if (equal("/", v_s)) {
    LINE(33,(assignCallTemp(eo_1, x_dirname("d:\\foo\\bar.inc")),f_same(".", eo_1)));
    LINE(34,(assignCallTemp(eo_1, x_dirname("c:\\foo")),f_same(".", eo_1)));
    LINE(35,(assignCallTemp(eo_1, x_dirname("c:\\")),f_same(".", eo_1)));
    LINE(36,(assignCallTemp(eo_1, x_dirname("c:")),f_same(".", eo_1)));
  }
  else {
    LINE(38,(assignCallTemp(eo_1, x_dirname("d:\\foo\\bar.inc")),f_same("d:\\foo", eo_1)));
    LINE(39,(assignCallTemp(eo_1, x_dirname("c:\\foo")),f_same("c:\\", eo_1)));
    LINE(40,(assignCallTemp(eo_1, x_dirname("c:\\")),f_same("c:\\", eo_1)));
    LINE(41,(assignCallTemp(eo_1, x_dirname("c:")),f_same("c:", eo_1)));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
