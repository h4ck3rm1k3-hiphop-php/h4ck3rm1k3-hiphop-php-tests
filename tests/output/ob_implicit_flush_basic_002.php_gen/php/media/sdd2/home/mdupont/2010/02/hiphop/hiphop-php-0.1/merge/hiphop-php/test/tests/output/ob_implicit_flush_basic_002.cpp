
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_basic_002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_implicit_flush_basic_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_basic_002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_implicit_flush_basic_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("*** Testing ob_implicit_flush() : ensure implicit flushing does not apply to user buffers. ***\n");
  LINE(11,x_ob_start());
  LINE(13,x_ob_implicit_flush(toBoolean(1LL)));
  echo("This is being written to a user buffer.\n");
  echo("Note that even though implicit flushing is on, you should never see this,\n");
  echo("because implicit flushing affects only the top level buffer, not user buffers.\n");
  LINE(20,x_ob_end_clean());
  echo("Done");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
