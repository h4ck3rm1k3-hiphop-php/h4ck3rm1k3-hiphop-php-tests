
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 4:
      HASH_INDEX(0x1865DDFD143DA584LL, undefined_var, 16);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x612DD31212E90587LL, key, 18);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 13:
      HASH_INDEX(0x4D79F8A5C483180DLL, unset_var, 12);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 33:
      HASH_INDEX(0x5CE7485A53EA50E1LL, assoc_array, 15);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 49:
      HASH_INDEX(0x69E7413AE0C88471LL, value, 19);
      break;
    case 54:
      HASH_INDEX(0x485FC2FA62F7E4B6LL, index_array, 14);
      break;
    case 56:
      HASH_INDEX(0x7D8E46298FFE84B8LL, inputs, 17);
      break;
    case 59:
      HASH_INDEX(0x253C6CFC00352CBBLL, heredoc, 13);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 20) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
