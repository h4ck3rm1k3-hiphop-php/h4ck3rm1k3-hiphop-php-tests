
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_variation_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_variation_001.php line 33 */
Variant c_classwithouttostring::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_classwithouttostring::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_classwithouttostring::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_classwithouttostring::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_classwithouttostring::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_classwithouttostring::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_classwithouttostring::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_classwithouttostring::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(classwithouttostring)
ObjectData *c_classwithouttostring::cloneImpl() {
  c_classwithouttostring *obj = NEW(c_classwithouttostring)();
  cloneSet(obj);
  return obj;
}
void c_classwithouttostring::cloneSet(c_classwithouttostring *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_classwithouttostring::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_classwithouttostring::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_classwithouttostring::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_classwithouttostring$os_get(const char *s) {
  return c_classwithouttostring::os_get(s, -1);
}
Variant &cw_classwithouttostring$os_lval(const char *s) {
  return c_classwithouttostring::os_lval(s, -1);
}
Variant cw_classwithouttostring$os_constant(const char *s) {
  return c_classwithouttostring::os_constant(s);
}
Variant cw_classwithouttostring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_classwithouttostring::os_invoke(c, s, params, -1, fatal);
}
void c_classwithouttostring::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_variation_001.php line 26 */
Variant c_classwithtostring::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_classwithtostring::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_classwithtostring::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_classwithtostring::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_classwithtostring::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_classwithtostring::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_classwithtostring::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_classwithtostring::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(classwithtostring)
ObjectData *c_classwithtostring::cloneImpl() {
  c_classwithtostring *obj = NEW(c_classwithtostring)();
  cloneSet(obj);
  return obj;
}
void c_classwithtostring::cloneSet(c_classwithtostring *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_classwithtostring::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_classwithtostring::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_classwithtostring::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_classwithtostring$os_get(const char *s) {
  return c_classwithtostring::os_get(s, -1);
}
Variant &cw_classwithtostring$os_lval(const char *s) {
  return c_classwithtostring::os_lval(s, -1);
}
Variant cw_classwithtostring$os_constant(const char *s) {
  return c_classwithtostring::os_constant(s);
}
Variant cw_classwithtostring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_classwithtostring::os_invoke(c, s, params, -1, fatal);
}
void c_classwithtostring::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_variation_001.php line 28 */
String c_classwithtostring::t___tostring() {
  INSTANCE_METHOD_INJECTION(classWithToString, classWithToString::__toString);
  return "Class A object";
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_variation_001.php line 11 */
void f_test_error_handler(CVarRef v_err_no, CVarRef v_err_msg, CVarRef v_filename, CVarRef v_linenum, CVarRef v_vars) {
  FUNCTION_INJECTION(test_error_handler);
  if (!equal(LINE(12,x_error_reporting()), 0LL)) {
    echo(concat_rev(LINE(14,concat6(toString(v_err_msg), ", ", toString(v_filename), "(", toString(v_linenum), ")\n")), concat3("Error: ", toString(v_err_no), " - ")));
  }
} /* function */
Variant i_test_error_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6C9B684D62E12540LL, test_error_handler) {
    return (f_test_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_classwithouttostring(CArrRef params, bool init /* = true */) {
  return Object(p_classwithouttostring(NEW(c_classwithouttostring)())->dynCreate(params, init));
}
Object co_classwithtostring(CArrRef params, bool init /* = true */) {
  return Object(p_classwithtostring(NEW(c_classwithtostring)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_implicit_flush_variation_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_variation_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_implicit_flush_variation_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  Variant eo_9;
  Variant eo_10;
  Variant eo_11;
  Variant eo_12;
  Variant eo_13;
  Variant eo_14;
  Variant eo_15;
  Variant eo_16;
  Variant eo_17;
  Variant eo_18;
  Variant eo_19;
  Variant eo_20;
  Variant eo_21;
  Variant eo_22;
  Variant eo_23;
  Variant eo_24;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_unset_var __attribute__((__unused__)) = (variables != gVariables) ? variables->get("unset_var") : g->GV(unset_var);
  Variant &v_heredoc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("heredoc") : g->GV(heredoc);
  Variant &v_index_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("index_array") : g->GV(index_array);
  Variant &v_assoc_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("assoc_array") : g->GV(assoc_array);
  Variant &v_undefined_var __attribute__((__unused__)) = (variables != gVariables) ? variables->get("undefined_var") : g->GV(undefined_var);
  Variant &v_inputs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("inputs") : g->GV(inputs);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);

  echo("*** Testing ob_implicit_flush() : usage variation ***\n");
  LINE(17,x_set_error_handler("test_error_handler"));
  (v_unset_var = 10LL);
  unset(v_unset_var);
  (v_heredoc = toString("hello world"));
  (v_index_array = ScalarArrays::sa_[0]);
  (v_assoc_array = ScalarArrays::sa_[1]);
  (v_inputs = (assignCallTemp(eo_6, v_index_array),assignCallTemp(eo_7, v_assoc_array),assignCallTemp(eo_8, Array(ArrayInit(3).set(0, "foo").set(1, v_index_array).set(2, v_assoc_array).create())),assignCallTemp(eo_20, v_heredoc),assignCallTemp(eo_21, ((Object)(LINE(83,p_classwithtostring(p_classwithtostring(NEWOBJ(c_classwithtostring)())->create()))))),assignCallTemp(eo_22, ((Object)(LINE(84,p_classwithouttostring(p_classwithouttostring(NEWOBJ(c_classwithouttostring)())->create()))))),assignCallTemp(eo_23, (silenceInc(), silenceDec(v_undefined_var))),assignCallTemp(eo_24, (silenceInc(), silenceDec(v_unset_var))),Array(ArrayInit(25).set(0, "float 10.5", 10.5, 0x0026D13DDCFE239FLL).set(1, "float -10.5", -10.5, 0x661E33DA1EE3E6E2LL).set(2, "float 12.3456789000e10", 123456789000.0, 0x072C56F9B64D45D1LL).set(3, "float -12.3456789000e10", -123456789000.0, 0x7A3FEAEE75848B83LL).set(4, "float .5", 0.5, 0x300E3138BF1332BELL).set(5, "empty array", ScalarArrays::sa_[2], 0x04D3AEDAAE49A773LL).set(6, "int indexed array", eo_6, 0x67B5A1316194CB41LL).set(7, "associative array", eo_7, 0x739751C03B5618E5LL).set(8, "nested arrays", eo_8, 0x27709BF1699BBDD9LL).set(9, "uppercase NULL", null, 0x5E5B3F94E4543C4ELL).set(10, "lowercase null", null, 0x7BDC7D5EC50A9C45LL).set(11, "lowercase true", true, 0x460FD76ECE07592FLL).set(12, "lowercase false", false, 0x467245FBBA0BC62FLL).set(13, "uppercase TRUE", true, 0x7026D67894F24B43LL).set(14, "uppercase FALSE", false, 0x7DAB47FCB7233172LL).set(15, "empty string DQ", "", 0x2FA8976DD93985A1LL).set(16, "empty string SQ", "", 0x642854D1689E4293LL).set(17, "string DQ", "string", 0x6DF74D03BC4FE633LL).set(18, "string SQ", "string", 0x3192C950633E33D0LL).set(19, "mixed case string", "sTrInG", 0x155F313377B422D9LL).set(20, "heredoc", eo_20, 0x253C6CFC00352CBBLL).set(21, "instance of classWithToString", eo_21, 0x3FCBE309B7710977LL).set(22, "instance of classWithoutToString", eo_22, 0x6C544C5E515650C8LL).set(23, "undefined var", eo_23, 0x6190096580F83BDBLL).set(24, "unset var", eo_24, 0x409DA1A724702BD7LL).create())));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_inputs.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_key = iter3->first();
      {
        echo(LINE(96,concat3("\n--", toString(v_key), "--\n")));
        LINE(97,x_var_dump(1, (x_ob_implicit_flush(toBoolean(v_value)), null)));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
