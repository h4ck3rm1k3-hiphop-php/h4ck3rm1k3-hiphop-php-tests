
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_002.php line 14 */
String f_return_string(CVarRef v_string) {
  FUNCTION_INJECTION(return_string);
  return "I stole your output.";
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_002.php line 6 */
bool f_return_false(CVarRef v_string) {
  FUNCTION_INJECTION(return_false);
  return false;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_002.php line 18 */
int64 f_return_zero(CVarRef v_string) {
  FUNCTION_INJECTION(return_zero);
  return 0LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_002.php line 10 */
Variant f_return_null(CVarRef v_string) {
  FUNCTION_INJECTION(return_null);
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_002.php line 2 */
String f_return_empty_string(CVarRef v_string) {
  FUNCTION_INJECTION(return_empty_string);
  return "";
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_functions __attribute__((__unused__)) = (variables != gVariables) ? variables->get("functions") : g->GV(functions);
  Variant &v_callbacks __attribute__((__unused__)) = (variables != gVariables) ? variables->get("callbacks") : g->GV(callbacks);
  Variant &v_callback __attribute__((__unused__)) = (variables != gVariables) ? variables->get("callback") : g->GV(callback);

  (v_functions = LINE(23,x_get_defined_functions()));
  (v_callbacks = v_functions.rvalAt("user", 0x53052C743152AAE3LL));
  LINE(25,x_sort(ref(v_callbacks)));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_callbacks.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_callback = iter3->second();
      {
        echo(LINE(27,concat3("--> Use callback '", toString(v_callback), "':\n")));
        LINE(28,x_ob_start(v_callback));
        echo("My output.");
        LINE(30,x_ob_end_flush());
        echo("\n\n");
      }
    }
  }
  echo("==DONE==\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
