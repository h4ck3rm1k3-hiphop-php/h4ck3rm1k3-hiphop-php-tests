
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_error_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_error_001.php line 7 */
Variant f_justprint(CVarRef v_str) {
  FUNCTION_INJECTION(justPrint);
  return v_str;
} /* function */
Variant i_justprint(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x07037E4E2608D9E3LL, justprint) {
    return (f_justprint(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_error_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_error_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_error_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arg_1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arg_1") : g->GV(arg_1);
  Variant &v_arg_2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arg_2") : g->GV(arg_2);
  Variant &v_arg_3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arg_3") : g->GV(arg_3);
  Variant &v_extra_arg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("extra_arg") : g->GV(extra_arg);

  (v_arg_1 = "justPrint");
  (v_arg_2 = 0LL);
  (v_arg_3 = false);
  (v_extra_arg = 1LL);
  echo("\n- Too many arguments\n");
  LINE(17,x_var_dump(1, invoke_too_many_args("ob_start", (1), ((x_ob_start(v_arg_1, toInt32(v_arg_2), toBoolean(v_arg_3)))))));
  echo("\n- Arg 1 wrong type\n");
  LINE(20,x_var_dump(1, x_ob_start(1.5)));
  echo("\n- Arg 2 wrong type\n");
  LINE(23,x_var_dump(1, x_ob_start("justPrint", toInt32("this should be an int"))));
  echo("\n- Arg 3 wrong type\n");
  LINE(26,x_var_dump(1, x_ob_start("justPrint", toInt32(0LL), toBoolean("this should be a bool"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
