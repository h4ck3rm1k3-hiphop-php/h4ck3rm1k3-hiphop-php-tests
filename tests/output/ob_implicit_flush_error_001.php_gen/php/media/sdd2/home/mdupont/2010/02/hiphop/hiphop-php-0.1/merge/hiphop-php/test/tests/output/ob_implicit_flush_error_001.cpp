
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_error_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_implicit_flush_error_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_implicit_flush_error_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_implicit_flush_error_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_flag __attribute__((__unused__)) = (variables != gVariables) ? variables->get("flag") : g->GV(flag);
  Variant &v_extra_arg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("extra_arg") : g->GV(extra_arg);

  echo("*** Testing ob_implicit_flush() : error conditions ***\n");
  echo("\n-- Testing ob_implicit_flush() function with more than expected no. of arguments --\n");
  (v_flag = 10LL);
  (v_extra_arg = 10LL);
  LINE(15,x_var_dump(1, (invoke_too_many_args("ob_implicit_flush", (1), ((x_ob_implicit_flush(toBoolean(v_flag))), null)), null)));
  echo("Done");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
