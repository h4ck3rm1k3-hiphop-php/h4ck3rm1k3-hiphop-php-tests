
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_clean_basic_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_clean_basic_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_clean_basic_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_clean_basic_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_out __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out") : g->GV(out);

  echo("*** Testing ob_clean() : basic functionality ***\n");
  echo("\n-- Testing ob_clean() function with Zero arguments --\n");
  LINE(12,x_var_dump(1, (x_ob_clean(), null)));
  LINE(14,x_ob_start());
  echo("You should never see this.");
  LINE(16,x_var_dump(1, (x_ob_clean(), null)));
  echo("Ensure the buffer is still active after the clean.");
  (v_out = LINE(19,x_ob_get_clean()));
  LINE(20,x_var_dump(1, v_out));
  echo("Done");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
