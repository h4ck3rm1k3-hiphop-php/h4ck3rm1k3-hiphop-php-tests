
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_013.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_013.php line 2 */
Variant f_a(CVarRef v_s) {
  FUNCTION_INJECTION(a);
  return v_s;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_013.php line 3 */
Variant f_b(CVarRef v_s) {
  FUNCTION_INJECTION(b);
  return v_s;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_013.php line 4 */
Variant f_c(CVarRef v_s) {
  FUNCTION_INJECTION(c);
  return v_s;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_013.php line 5 */
Variant f_d(CVarRef v_s) {
  FUNCTION_INJECTION(d);
  return v_s;
} /* function */
Variant i_a(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4F38A775A0938899LL, a) {
    return (f_a(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_b(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x39E9518192E6D2AELL, b) {
    return (f_b(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_c(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5C7A29AC25A4F8E4LL, c) {
    return (f_c(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_d(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x704CEFD058905A68LL, d) {
    return (f_d(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_013_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_013.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_013_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(7,x_ob_start());
  LINE(8,x_ob_start("a"));
  LINE(9,x_ob_start("b"));
  LINE(10,x_ob_start("c"));
  LINE(11,x_ob_start("d"));
  LINE(12,x_ob_start());
  echo("foo\n");
  LINE(16,x_ob_flush());
  LINE(17,x_ob_end_clean());
  LINE(18,x_ob_flush());
  LINE(20,x_print_r(x_ob_list_handlers()));
  LINE(21,x_print_r(x_ob_get_status()));
  LINE(22,x_print_r(x_ob_get_status(true)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
