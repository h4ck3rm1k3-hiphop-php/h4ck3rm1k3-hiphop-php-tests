<?php
function flushCounter($input) {
	static $counter=0;
	return '[' . ++$counter . "] $input \n";
}

// This should set the buffer size to 4096
ob_start('flushCounter', 1);

// Get the buffer size: 
$bufferInfo = ob_get_status(true);
var_dump($bufferInfo[0]['chunk_size']);

// If the buffer size is >1, these two chars should
// come out as part of a single flush:
echo "1";
echo "2";
?>
