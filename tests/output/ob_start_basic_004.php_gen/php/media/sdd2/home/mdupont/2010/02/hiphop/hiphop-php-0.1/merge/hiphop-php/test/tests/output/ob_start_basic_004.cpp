
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_004.php line 7 */
String f_callback(CVarRef v_string) {
  FUNCTION_INJECTION(callback);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_callback_invocations __attribute__((__unused__)) = g->GV(callback_invocations);
  int v_len = 0;

  gv_callback_invocations++;
  (v_len = LINE(10,x_strlen(toString(v_string))));
  return concat("f[call:", LINE(11,concat6(toString(gv_callback_invocations), "; len:", toString(v_len), "]", toString(v_string), "\n")));
} /* function */
Variant i_callback(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2DE38EBA0ED55F48LL, callback) {
    return (f_callback(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cs") : g->GV(cs);
  Variant &v_callback_invocations __attribute__((__unused__)) = (variables != gVariables) ? variables->get("callback_invocations") : g->GV(callback_invocations);

  {
    LOOP_COUNTER(1);
    for ((v_cs = -1LL); less(v_cs, 10LL); v_cs++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(15,concat3("\n----( chunk_size: ", toString(v_cs), ", output append size: 1 )----\n")));
        (v_callback_invocations = 0LL);
        LINE(17,x_ob_start("callback", toInt32(v_cs)));
        echo("1");
        echo("2");
        echo("3");
        echo("4");
        echo("5");
        echo("6");
        echo("7");
        echo("8");
        LINE(19,x_ob_end_flush());
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_cs = -1LL); less(v_cs, 10LL); v_cs++) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(LINE(23,concat3("\n----( chunk_size: ", toString(v_cs), ", output append size: 4 )----\n")));
        (v_callback_invocations = 0LL);
        LINE(25,x_ob_start("callback", toInt32(v_cs)));
        echo("1234");
        echo("5678");
        LINE(27,x_ob_end_flush());
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
