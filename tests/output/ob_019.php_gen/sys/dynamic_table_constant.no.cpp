
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_PHP_OUTPUT_HANDLER_STDFLAGS;
extern const StaticString k_PHP_OUTPUT_HANDLER_CLEANABLE;
extern const Variant k_PHP_OUTPUT_HANDLER_REMOVABLE;
extern const StaticString k_PHP_OUTPUT_HANDLER_FLUSHABLE;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 3:
      HASH_RETURN(0x0BDE12090B1D972BLL, k_PHP_OUTPUT_HANDLER_CLEANABLE, PHP_OUTPUT_HANDLER_CLEANABLE);
      break;
    case 4:
      HASH_RETURN(0x4966DA8E1353F7ACLL, k_PHP_OUTPUT_HANDLER_FLUSHABLE, PHP_OUTPUT_HANDLER_FLUSHABLE);
      HASH_RETURN(0x6403778439AA7E84LL, k_PHP_OUTPUT_HANDLER_REMOVABLE, PHP_OUTPUT_HANDLER_REMOVABLE);
      HASH_RETURN(0x05CF47C578538D8CLL, k_PHP_OUTPUT_HANDLER_STDFLAGS, PHP_OUTPUT_HANDLER_STDFLAGS);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
