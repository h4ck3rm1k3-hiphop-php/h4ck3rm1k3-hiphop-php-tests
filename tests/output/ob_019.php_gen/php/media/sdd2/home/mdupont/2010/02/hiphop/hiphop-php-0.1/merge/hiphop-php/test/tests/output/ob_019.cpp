
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_019.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_PHP_OUTPUT_HANDLER_STDFLAGS = "PHP_OUTPUT_HANDLER_STDFLAGS";
const StaticString k_PHP_OUTPUT_HANDLER_CLEANABLE = "PHP_OUTPUT_HANDLER_CLEANABLE";
const Variant k_PHP_OUTPUT_HANDLER_REMOVABLE = "PHP_OUTPUT_HANDLER_REMOVABLE";
const StaticString k_PHP_OUTPUT_HANDLER_FLUSHABLE = "PHP_OUTPUT_HANDLER_FLUSHABLE";

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_019_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_019.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_019_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  {
  }
  echo(LINE(4,(assignCallTemp(eo_1, toString(x_ob_get_level())),concat3("\n==", eo_1, "==\n"))));
  LINE(5,x_ob_start(null, toInt32(0LL), toBoolean(k_PHP_OUTPUT_HANDLER_CLEANABLE)));
  echo("N:clean\n");
  LINE(7,x_ob_clean());
  LINE(8,x_ob_flush());
  echo(LINE(10,(assignCallTemp(eo_1, toString(x_ob_get_level())),concat3("\n==", eo_1, "==\n"))));
  LINE(11,x_ob_start(null, toInt32(0LL), toBoolean(k_PHP_OUTPUT_HANDLER_FLUSHABLE)));
  echo("Y:flush\n");
  LINE(13,x_ob_clean());
  LINE(14,x_ob_flush());
  echo(LINE(16,(assignCallTemp(eo_1, toString(x_ob_get_level())),concat3("\n==", eo_1, "==\n"))));
  LINE(17,x_ob_start(null, toInt32(0LL), toBoolean(k_PHP_OUTPUT_HANDLER_REMOVABLE)));
  echo("N:remove-clean\n");
  LINE(19,x_ob_end_clean());
  echo(LINE(21,(assignCallTemp(eo_1, toString(x_ob_get_level())),concat3("\n==", eo_1, "==\n"))));
  LINE(22,x_ob_start(null, toInt32(0LL), toBoolean(k_PHP_OUTPUT_HANDLER_REMOVABLE)));
  echo("Y:remove-flush\n");
  LINE(24,x_ob_end_flush());
  echo(LINE(26,(assignCallTemp(eo_1, toString(x_ob_get_level())),concat3("\n==", eo_1, "==\n"))));
  LINE(27,x_ob_start(null, toInt32(0LL), toBoolean(k_PHP_OUTPUT_HANDLER_STDFLAGS)));
  echo("N:standard\n");
  LINE(29,x_ob_end_clean());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
