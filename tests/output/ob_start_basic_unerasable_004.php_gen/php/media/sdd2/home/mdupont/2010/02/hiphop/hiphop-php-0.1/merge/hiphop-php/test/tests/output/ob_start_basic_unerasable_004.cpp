
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_unerasable_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_unerasable_004.php line 2 */
String f_callback(CVarRef v_string) {
  FUNCTION_INJECTION(callback);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_callback_invocations __attribute__((__unused__)) = g->sv_callback_DupIdcallback_invocations;
  bool &inited_sv_callback_invocations __attribute__((__unused__)) = g->inited_sv_callback_DupIdcallback_invocations;
  if (!inited_sv_callback_invocations) {
    (sv_callback_invocations = null);
    inited_sv_callback_invocations = true;
  }
  sv_callback_invocations++;
  return LINE(5,concat5("[callback:", toString(sv_callback_invocations), "]", toString(v_string), "\n"));
} /* function */
Variant i_callback(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2DE38EBA0ED55F48LL, callback) {
    return (f_callback(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_unerasable_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_unerasable_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_unerasable_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);

  LINE(8,x_ob_start("callback", toInt32(0LL), false));
  echo("This call will obtain the content, but will not flush the buffer.");
  (v_str = LINE(11,x_ob_get_flush()));
  LINE(12,x_var_dump(1, v_str));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
