
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php line 14 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("id", m_id.isReferenced() ? ref(m_id) : m_id));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x028B9FE0C4522BE2LL, id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x028B9FE0C4522BE2LL, m_id,
                      id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::create(String v_id) {
  init();
  t___construct(v_id);
  return this;
}
ObjectData *c_c::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_id = m_id.isReferenced() ? ref(m_id) : m_id;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x596FD6C55E8464CCLL, g) {
        return (ti_g(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 1:
      HASH_GUARD(0x7F6FEB701746C571LL, h) {
        return (t_h(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x596FD6C55E8464CCLL, g) {
        return (ti_g(o_getClassName(), a0));
      }
      break;
    case 1:
      HASH_GUARD(0x7F6FEB701746C571LL, h) {
        return (t_h(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x596FD6C55E8464CCLL, g) {
        return (ti_g(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_id = "none";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php line 17 */
void c_c::t___construct(String v_id) {
  INSTANCE_METHOD_INJECTION(C, C::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_id = v_id);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php line 21 */
String c_c::ti_g(const char* cls, CVarRef v_string) {
  STATIC_METHOD_INJECTION(C, C::g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_i __attribute__((__unused__)) = g->sv_c_DupIdg_DupIdi.lvalAt(cls);
  Variant &inited_sv_i __attribute__((__unused__)) = g->inited_sv_c_DupIdg_DupIdi.lvalAt(cls);
  int v_len = 0;

  if (!inited_sv_i) {
    (sv_i = 0LL);
    inited_sv_i = true;
  }
  sv_i++;
  (v_len = LINE(24,x_strlen(toString(v_string))));
  return concat("C::g[call:", LINE(25,concat6(toString(sv_i), "; len:", toString(v_len), "] - ", toString(v_string), "\n")));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php line 28 */
String c_c::t_h(CVarRef v_string) {
  INSTANCE_METHOD_INJECTION(C, C::h);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_i __attribute__((__unused__)) = g->sv_c_DupIdh_DupIdi.lvalAt(this->o_getClassName());
  Variant &inited_sv_i __attribute__((__unused__)) = g->inited_sv_c_DupIdh_DupIdi.lvalAt(this->o_getClassName());
  int v_len = 0;

  if (!inited_sv_i) {
    (sv_i = 0LL);
    inited_sv_i = true;
  }
  sv_i++;
  (v_len = LINE(31,x_strlen(toString(v_string))));
  return concat_rev(LINE(32,concat6(toString(v_len), "; id:", toString(m_id), "] - ", toString(v_string), "\n")), concat3("C::h[call:", toString(sv_i), "; len:"));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php line 36 */
void f_checkandclean() {
  FUNCTION_INJECTION(checkAndClean);
  LINE(37,x_print_r(x_ob_list_handlers()));
  LOOP_COUNTER(1);
  {
    while (more(LINE(38,x_ob_get_level()), 0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(39,x_ob_end_flush());
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php line 7 */
String f_f(CVarRef v_string) {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_i __attribute__((__unused__)) = g->sv_f_DupIdi;
  bool &inited_sv_i __attribute__((__unused__)) = g->inited_sv_f_DupIdi;
  int v_len = 0;

  if (!inited_sv_i) {
    (sv_i = 0LL);
    inited_sv_i = true;
  }
  sv_i++;
  (v_len = LINE(10,x_strlen(toString(v_string))));
  return concat("f[call:", LINE(11,concat6(toString(sv_i), "; len:", toString(v_len), "] - ", toString(v_string), "\n")));
} /* function */
Variant i_f(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x286CE1C477560280LL, f) {
    return (f_f(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_start_basic_006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("\n ---> Test arrays: \n");
  LINE(44,x_var_dump(1, x_ob_start(ScalarArrays::sa_[0])));
  LINE(45,f_checkandclean());
  LINE(47,x_var_dump(1, x_ob_start(ScalarArrays::sa_[1])));
  LINE(48,f_checkandclean());
  LINE(50,x_var_dump(1, x_ob_start(ScalarArrays::sa_[2])));
  LINE(51,f_checkandclean());
  LINE(53,x_var_dump(1, x_ob_start(ScalarArrays::sa_[3])));
  LINE(54,f_checkandclean());
  LINE(56,x_var_dump(1, x_ob_start(ScalarArrays::sa_[4])));
  LINE(57,f_checkandclean());
  (v_c = ((Object)(LINE(59,p_c(p_c(NEWOBJ(c_c)())->create("originalID"))))));
  LINE(60,x_var_dump(1, x_ob_start(Array(ArrayInit(2).set(0, v_c).set(1, "h").create()))));
  LINE(61,f_checkandclean());
  LINE(63,x_var_dump(1, x_ob_start(Array(ArrayInit(2).set(0, v_c).set(1, "h").create()))));
  (v_c.o_lval("id", 0x028B9FE0C4522BE2LL) = "changedID");
  LINE(65,f_checkandclean());
  (v_c.o_lval("id", 0x028B9FE0C4522BE2LL) = "changedIDagain");
  LINE(68,x_var_dump(1, x_ob_start(Array(ArrayInit(3).set(0, "f").set(1, "C::g").set(2, Array(ArrayInit(2).set(0, Array(ArrayInit(2).set(0, v_c).set(1, "g").create())).set(1, Array(ArrayInit(2).set(0, v_c).set(1, "h").create())).create())).create()))));
  LINE(69,f_checkandclean());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
