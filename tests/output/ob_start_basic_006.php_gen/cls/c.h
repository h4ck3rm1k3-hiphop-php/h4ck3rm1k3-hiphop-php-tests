
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_start_basic_006.php line 14 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: Variant m_id;
  public: void t___construct(String v_id);
  public: ObjectData *create(String v_id);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: static String ti_g(const char* cls, CVarRef v_string);
  public: String t_h(CVarRef v_string);
  public: static String t_g(CVarRef v_string) { return ti_g("c", v_string); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
