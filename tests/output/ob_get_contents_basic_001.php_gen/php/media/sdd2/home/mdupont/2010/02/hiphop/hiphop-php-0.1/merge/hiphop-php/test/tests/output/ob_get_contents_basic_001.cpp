
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_get_contents_basic_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_get_contents_basic_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/output/ob_get_contents_basic_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$output$ob_get_contents_basic_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_hello __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hello") : g->GV(hello);
  Variant &v_hello2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hello2") : g->GV(hello2);

  echo("*** Testing ob_get_contents() : basic functionality ***\n");
  echo("\n-- Testing ob_get_contents() function with Zero arguments --\n");
  LINE(14,x_var_dump(1, x_ob_get_contents()));
  LINE(16,x_ob_start());
  echo("Hello World\n");
  (v_hello = LINE(18,x_ob_get_contents()));
  LINE(19,x_var_dump(1, v_hello));
  LINE(20,x_ob_end_flush());
  echo("\ncheck that we dont have a reference\n");
  LINE(24,x_ob_start());
  echo("Hello World\n");
  (v_hello2 = LINE(26,x_ob_get_contents()));
  (v_hello2 = "bob");
  LINE(28,x_var_dump(1, x_ob_get_contents()));
  LINE(29,x_ob_end_flush());
  echo("\ncheck that contents disappear after a flush\n");
  LINE(32,x_ob_start());
  echo("Hello World\n");
  LINE(34,x_ob_flush());
  LINE(35,x_var_dump(1, x_ob_get_contents()));
  LINE(36,x_ob_end_flush());
  echo("\ncheck that no contents found after an end\n");
  LINE(39,x_ob_start());
  echo("Hello World\n");
  LINE(41,x_ob_end_flush());
  LINE(42,x_var_dump(1, x_ob_get_contents()));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
