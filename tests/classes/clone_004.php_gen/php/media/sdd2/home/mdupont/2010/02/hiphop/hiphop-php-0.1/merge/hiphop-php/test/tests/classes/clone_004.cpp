
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.php line 2 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
  m_a = "base";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.php line 6 */
Variant c_base::t___clone() {
  INSTANCE_METHOD_INJECTION(base, base::__clone);
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.php line 9 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("b", m_b.isReferenced() ? ref(m_b) : m_b));
  c_base::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    default:
      break;
  }
  return c_base::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    default:
      break;
  }
  return c_base::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    default:
      break;
  }
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    default:
      break;
  }
  return c_base::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  clone->m_b = m_b.isReferenced() ? ref(m_b) : m_b;
  c_base::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  c_base::init();
  m_b = "test";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.php line 13 */
Variant c_test::t___clone() {
  INSTANCE_METHOD_INJECTION(test, test::__clone);
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.php line 15 */
void c_test::t_show() {
  INSTANCE_METHOD_INJECTION(test, test::show);
  LINE(16,x_var_dump(1, ((Object)(this))));
} /* function */
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o1") : g->GV(o1);
  Variant &v_o2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o2") : g->GV(o2);

  echo("Original\n");
  (v_o1 = ((Object)(LINE(21,p_test(p_test(NEWOBJ(c_test)())->create())))));
  (v_o1.o_lval("a", 0x4292CEE227B9150ALL) = ScalarArrays::sa_[0]);
  (v_o1.o_lval("b", 0x08FBB133F8576BD5LL) = ScalarArrays::sa_[1]);
  LINE(24,v_o1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  echo("Clone\n");
  (v_o2 = f_clone(toObject(v_o1)));
  LINE(28,v_o2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  echo("Modify\n");
  (v_o2.o_lval("a", 0x4292CEE227B9150ALL) = 5LL);
  (v_o2.o_lval("b", 0x08FBB133F8576BD5LL) = 6LL);
  LINE(33,v_o2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
