
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_004.php line 9 */
class c_test : virtual public c_base {
  BEGIN_CLASS_MAP(test)
    PARENT_CLASS(base)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, test, base)
  void init();
  public: Variant m_b;
  public: Variant t___clone();
  public: void t_show();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
