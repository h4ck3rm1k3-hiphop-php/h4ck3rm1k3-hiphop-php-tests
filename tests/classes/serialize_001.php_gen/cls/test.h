
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/serialize_001.php line 3 */
class c_test : virtual public c_serializable {
  BEGIN_CLASS_MAP(test)
    PARENT_CLASS(serializable)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, Test, ObjectData)
  void init();
  public: Variant m_data;
  public: void t___construct(Variant v_data);
  public: ObjectData *create(Variant v_data);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_serialize();
  public: void t_unserialize(CVarRef v_serialized);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
