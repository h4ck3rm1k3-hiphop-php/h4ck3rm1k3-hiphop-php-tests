
#ifndef __GENERATED_cls_derived_h__
#define __GENERATED_cls_derived_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_final.php line 11 */
class c_derived : virtual public c_base {
  BEGIN_CLASS_MAP(derived)
    PARENT_CLASS(base)
  END_CLASS_MAP(derived)
  DECLARE_CLASS(derived, derived, base)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_h__
