
#ifndef __GENERATED_cls_fail_h__
#define __GENERATED_cls_fail_h__

#include <cls/same.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_004c.php line 21 */
class c_fail : virtual public c_same {
  BEGIN_CLASS_MAP(fail)
    PARENT_CLASS(father)
    PARENT_CLASS(same)
  END_CLASS_MAP(fail)
  DECLARE_CLASS(fail, fail, same)
  void init();
  public: void t_f4();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fail_h__
