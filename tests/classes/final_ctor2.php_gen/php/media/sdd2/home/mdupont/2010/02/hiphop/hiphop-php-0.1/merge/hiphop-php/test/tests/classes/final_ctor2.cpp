
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.php line 10 */
Variant c_works::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_works::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_works::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_works::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_works::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_works::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_works::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_works::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(works)
ObjectData *c_works::cloneImpl() {
  c_works *obj = NEW(c_works)();
  cloneSet(obj);
  return obj;
}
void c_works::cloneSet(c_works *clone) {
  c_base::cloneSet(clone);
}
Variant c_works::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_works::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_works::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_works$os_get(const char *s) {
  return c_works::os_get(s, -1);
}
Variant &cw_works$os_lval(const char *s) {
  return c_works::os_lval(s, -1);
}
Variant cw_works$os_constant(const char *s) {
  return c_works::os_constant(s);
}
Variant cw_works$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_works::os_invoke(c, s, params, -1, fatal);
}
void c_works::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.php line 3 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::create() {
  init();
  t_base();
  return this;
}
ObjectData *c_base::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.php line 5 */
void c_base::t_base() {
  INSTANCE_METHOD_INJECTION(Base, Base::Base);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.php line 14 */
Variant c_extended::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_extended::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_extended::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_extended::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_extended::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_extended::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_extended::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_extended::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(extended)
ObjectData *c_extended::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_extended::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_extended::cloneImpl() {
  c_extended *obj = NEW(c_extended)();
  cloneSet(obj);
  return obj;
}
void c_extended::cloneSet(c_extended *clone) {
  c_base::cloneSet(clone);
}
Variant c_extended::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_extended::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_extended::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_extended$os_get(const char *s) {
  return c_extended::os_get(s, -1);
}
Variant &cw_extended$os_lval(const char *s) {
  return c_extended::os_lval(s, -1);
}
Variant cw_extended$os_constant(const char *s) {
  return c_extended::os_constant(s);
}
Variant cw_extended$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_extended::os_invoke(c, s, params, -1, fatal);
}
void c_extended::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.php line 16 */
void c_extended::t___construct() {
  INSTANCE_METHOD_INJECTION(Extended, Extended::__construct);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
Object co_works(CArrRef params, bool init /* = true */) {
  return Object(p_works(NEW(c_works)())->dynCreate(params, init));
}
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Object co_extended(CArrRef params, bool init /* = true */) {
  return Object(p_extended(NEW(c_extended)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$final_ctor2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$final_ctor2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(21,throw_fatal("unknown method reflectionclass::export", ((void*)NULL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
