
#ifndef __GENERATED_cls_extended_h__
#define __GENERATED_cls_extended_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor2.php line 14 */
class c_extended : virtual public c_base {
  BEGIN_CLASS_MAP(extended)
    PARENT_CLASS(base)
  END_CLASS_MAP(extended)
  DECLARE_CLASS(extended, Extended, base)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_extended_h__
