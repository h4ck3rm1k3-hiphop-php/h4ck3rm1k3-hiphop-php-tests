
#ifndef __GENERATED_cls_ai_h__
#define __GENERATED_cls_ai_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 3 */
class c_ai : virtual public c_iterator {
  BEGIN_CLASS_MAP(ai)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iterator)
  END_CLASS_MAP(ai)
  DECLARE_CLASS(ai, ai, ObjectData)
  void init();
  public: Variant m_array;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_rewind();
  public: bool t_valid();
  public: Variant t_key();
  public: Variant t_current();
  public: void t_next();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ai_h__
