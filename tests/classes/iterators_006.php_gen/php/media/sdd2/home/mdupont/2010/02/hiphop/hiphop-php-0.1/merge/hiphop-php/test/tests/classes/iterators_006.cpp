
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 3 */
Variant c_ai::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ai::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ai::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("array", m_array.isReferenced() ? ref(m_array) : m_array));
  c_ObjectData::o_get(props);
}
bool c_ai::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x063D35168C04B960LL, array, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ai::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x063D35168C04B960LL, m_array,
                         array, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ai::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x063D35168C04B960LL, m_array,
                      array, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ai::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x063D35168C04B960LL, m_array,
                         array, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ai::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ai)
ObjectData *c_ai::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_ai::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_ai::cloneImpl() {
  c_ai *obj = NEW(c_ai)();
  cloneSet(obj);
  return obj;
}
void c_ai::cloneSet(c_ai *clone) {
  clone->m_array = m_array.isReferenced() ? ref(m_array) : m_array;
  ObjectData::cloneSet(clone);
}
Variant c_ai::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ai::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ai::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ai$os_get(const char *s) {
  return c_ai::os_get(s, -1);
}
Variant &cw_ai$os_lval(const char *s) {
  return c_ai::os_lval(s, -1);
}
Variant cw_ai$os_constant(const char *s) {
  return c_ai::os_constant(s);
}
Variant cw_ai$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ai::os_invoke(c, s, params, -1, fatal);
}
void c_ai::init() {
  m_array = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 7 */
void c_ai::t___construct() {
  INSTANCE_METHOD_INJECTION(ai, ai::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_array = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 11 */
void c_ai::t_rewind() {
  INSTANCE_METHOD_INJECTION(ai, ai::rewind);
  LINE(12,x_reset(ref(lval(m_array))));
  LINE(13,o_root_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 16 */
bool c_ai::t_valid() {
  INSTANCE_METHOD_INJECTION(ai, ai::valid);
  return !same(o_get("key", 0x612DD31212E90587LL), null);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 20 */
Variant c_ai::t_key() {
  INSTANCE_METHOD_INJECTION(ai, ai::key);
  return o_get("key", 0x612DD31212E90587LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 24 */
Variant c_ai::t_current() {
  INSTANCE_METHOD_INJECTION(ai, ai::current);
  return o_get("current", 0x235AFB77B3A5C1B3LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 28 */
void c_ai::t_next() {
  INSTANCE_METHOD_INJECTION(ai, ai::next);
  df_lambda_1(LINE(29,x_each(ref(lval(m_array)))), lval(o_lval("key", 0x612DD31212E90587LL)), lval(o_lval("current", 0x235AFB77B3A5C1B3LL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 36 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php line 38 */
p_ai c_a::t_getiterator() {
  INSTANCE_METHOD_INJECTION(a, a::getIterator);
  return ((Object)(LINE(39,p_ai(p_ai(NEWOBJ(c_ai)())->create()))));
} /* function */
Object co_ai(CArrRef params, bool init /* = true */) {
  return Object(p_ai(NEW(c_ai)())->dynCreate(params, init));
}
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$iterators_006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$iterators_006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array") : g->GV(array);
  Variant &v_property __attribute__((__unused__)) = (variables != gVariables) ? variables->get("property") : g->GV(property);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);

  (v_array = ((Object)(LINE(43,p_a(p_a(NEWOBJ(c_a)())->create())))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_array.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_property = iter3->first();
      {
        print(LINE(46,concat4(toString(v_property), ": ", toString(v_value), "\n")));
      }
    }
  }
  echo("===2nd===\n");
  (v_array = ((Object)(LINE(56,p_ai(p_ai(NEWOBJ(c_ai)())->create())))));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_array.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_value = iter6->second();
      v_property = iter6->first();
      {
        print(LINE(59,concat4(toString(v_property), ": ", toString(v_value), "\n")));
      }
    }
  }
  echo("===3rd===\n");
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_array.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_value = iter9->second();
      v_property = iter9->first();
      {
        print(LINE(65,concat4(toString(v_property), ": ", toString(v_value), "\n")));
      }
    }
  }
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
