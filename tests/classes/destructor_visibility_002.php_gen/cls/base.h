
#ifndef __GENERATED_cls_base_h__
#define __GENERATED_cls_base_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_visibility_002.php line 3 */
class c_base : virtual public ObjectData {
  BEGIN_CLASS_MAP(base)
  END_CLASS_MAP(base)
  DECLARE_CLASS(base, Base, ObjectData)
  void init();
  public: virtual void destruct();
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_base_h__
