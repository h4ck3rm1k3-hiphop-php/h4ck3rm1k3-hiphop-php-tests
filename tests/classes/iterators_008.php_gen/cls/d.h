
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__

#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_008.php line 4 */
class c_d : virtual public c_c, virtual public c_iterator {
  BEGIN_CLASS_MAP(d)
    PARENT_CLASS(c)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iterator)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, c)
  void init();
  public: int64 m_counter;
  public: int64 t_valid();
  public: void t_next();
  public: void t_rewind();
  public: void t_current();
  public: void t_key();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
