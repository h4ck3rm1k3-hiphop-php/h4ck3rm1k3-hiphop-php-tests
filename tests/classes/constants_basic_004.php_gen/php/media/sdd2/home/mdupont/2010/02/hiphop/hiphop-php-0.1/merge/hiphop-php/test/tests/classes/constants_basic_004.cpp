
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_004.php line 9 */
const StaticString q_b_KEY = "key";
const StaticString q_b_VALUE = "value";
Variant c_b::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x46AA74BAC7C97A8CLL, g->s_b_DupIdsa_b,
                  sa_b);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a_b", m_a_b));
  c_ObjectData::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x143DB983379C786ALL, a_b, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x143DB983379C786ALL, m_a_b,
                         a_b, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x143DB983379C786ALL, m_a_b,
                      a_b, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 1:
      HASH_RETURN(0x56EDB60C824E8C51LL, q_b_KEY, KEY);
      break;
    case 3:
      HASH_RETURN(0x021A52B45A788597LL, q_b_VALUE, VALUE);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  clone->m_a_b = m_a_b;
  ObjectData::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
  m_a_b = ScalarArrays::sa_[0];
}
void c_b::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_b_DupIdsa_b = ScalarArrays::sa_[0];
}
void csi_b() {
  c_b::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_004.php line 19 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 3) {
    case 3:
      HASH_RETURN(0x2D7A986D0079ECBFLL, g->s_c_DupIdsa_c_parent,
                  sa_c_parent);
      HASH_RETURN(0x030DA86515137BF3LL, g->s_c_DupIdsa_c_self,
                  sa_c_self);
      break;
    default:
      break;
  }
  return c_b::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_b::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a_c_parent", m_a_c_parent));
  props.push_back(NEW(ArrayElement)("a_c_self", m_a_c_self));
  c_b::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x1F5B14AC794F5EAALL, a_c_parent, 10);
      HASH_EXISTS_STRING(0x0A2E6B0C28E6B516LL, a_c_self, 8);
      break;
    default:
      break;
  }
  return c_b::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x1F5B14AC794F5EAALL, m_a_c_parent,
                         a_c_parent, 10);
      HASH_RETURN_STRING(0x0A2E6B0C28E6B516LL, m_a_c_self,
                         a_c_self, 8);
      break;
    default:
      break;
  }
  return c_b::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x1F5B14AC794F5EAALL, m_a_c_parent,
                      a_c_parent, 10);
      HASH_SET_STRING(0x0A2E6B0C28E6B516LL, m_a_c_self,
                      a_c_self, 8);
      break;
    default:
      break;
  }
  return c_b::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_b::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_b::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_a_c_parent = m_a_c_parent;
  clone->m_a_c_self = m_a_c_self;
  c_b::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_b::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_b::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_b::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  c_b::init();
  m_a_c_parent = ScalarArrays::sa_[0];
  m_a_c_self = Array(ArrayInit(1).set(0, "key"/* c::KEY */, "value"/* c::VALUE */).create());
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_c_DupIdsa_c_parent = ScalarArrays::sa_[0];
}
void csi_c() {
  c_c::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_004.php line 2 */
Variant c_x::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x349CD24E08545D49LL, g->s_x_DupIdsa_x,
                  sa_x);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a_x", m_a_x));
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x5F905B582185060DLL, a_x, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x5F905B582185060DLL, m_a_x,
                         a_x, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x5F905B582185060DLL, m_a_x,
                      a_x, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  clone->m_a_x = m_a_x;
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
  m_a_x = ScalarArrays::sa_[0];
}
void c_x::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_x_DupIdsa_x = ScalarArrays::sa_[0];
}
void csi_x() {
  c_x::os_static_initializer();
}
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("\nStatic properties:\n");
  LINE(33,x_var_dump(5, g->s_x_DupIdsa_x, Array(ArrayInit(4).set(0, g->s_b_DupIdsa_b).set(1, g->s_b_DupIdsa_b).set(2, g->s_c_DupIdsa_c_parent).set(3, g->s_c_DupIdsa_c_self).create())));
  echo("\nInstance properties:\n");
  (v_x = ((Object)(LINE(36,p_x(p_x(NEWOBJ(c_x)())->create())))));
  (v_b = ((Object)(LINE(37,p_b(p_b(NEWOBJ(c_b)())->create())))));
  (v_c = ((Object)(LINE(38,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(39,x_var_dump(3, v_x, Array(ArrayInit(2).set(0, v_b).set(1, v_c).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
