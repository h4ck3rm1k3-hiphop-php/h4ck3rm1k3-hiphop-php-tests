
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.php line 9 */
Variant c_test1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test1::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test1::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test1::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test1::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test1)
ObjectData *c_test1::cloneImpl() {
  c_test1 *obj = NEW(c_test1)();
  cloneSet(obj);
  return obj;
}
void c_test1::cloneSet(c_test1 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test1$os_get(const char *s) {
  return c_test1::os_get(s, -1);
}
Variant &cw_test1$os_lval(const char *s) {
  return c_test1::os_lval(s, -1);
}
Variant cw_test1$os_constant(const char *s) {
  return c_test1::os_constant(s);
}
Variant cw_test1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test1::os_invoke(c, s, params, -1, fatal);
}
void c_test1::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.php line 13 */
Variant c_test2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test2)
ObjectData *c_test2::cloneImpl() {
  c_test2 *obj = NEW(c_test2)();
  cloneSet(obj);
  return obj;
}
void c_test2::cloneSet(c_test2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test2$os_get(const char *s) {
  return c_test2::os_get(s, -1);
}
Variant &cw_test2$os_lval(const char *s) {
  return c_test2::os_lval(s, -1);
}
Variant cw_test2$os_constant(const char *s) {
  return c_test2::os_constant(s);
}
Variant cw_test2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test2::os_invoke(c, s, params, -1, fatal);
}
void c_test2::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.php line 15 */
String c_test2::t___tostring() {
  INSTANCE_METHOD_INJECTION(test2, test2::__toString);
  echo("test2::__toString()\n");
  return "Converted\n";
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.php line 22 */
Variant c_test3::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test3::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test3::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test3::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test3::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test3::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test3::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test3::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test3)
ObjectData *c_test3::cloneImpl() {
  c_test3 *obj = NEW(c_test3)();
  cloneSet(obj);
  return obj;
}
void c_test3::cloneSet(c_test3 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test3::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test3::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test3::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test3$os_get(const char *s) {
  return c_test3::os_get(s, -1);
}
Variant &cw_test3$os_lval(const char *s) {
  return c_test3::os_lval(s, -1);
}
Variant cw_test3$os_constant(const char *s) {
  return c_test3::os_constant(s);
}
Variant cw_test3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test3::os_invoke(c, s, params, -1, fatal);
}
void c_test3::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.php line 24 */
String c_test3::t___tostring() {
  INSTANCE_METHOD_INJECTION(test3, test3::__toString);
  echo("test3::__toString()\n");
  return toString(42LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.php line 3 */
void f_my_error_handler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline) {
  FUNCTION_INJECTION(my_error_handler);
  LINE(4,x_var_dump(1, v_errstr));
} /* function */
Variant i_my_error_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x606C54C87D73CF7DLL, my_error_handler) {
    return (f_my_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_test1(CArrRef params, bool init /* = true */) {
  return Object(p_test1(NEW(c_test1)())->dynCreate(params, init));
}
Object co_test2(CArrRef params, bool init /* = true */) {
  return Object(p_test2(NEW(c_test2)())->dynCreate(params, init));
}
Object co_test3(CArrRef params, bool init /* = true */) {
  return Object(p_test3(NEW(c_test3)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$tostring_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$tostring_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_ar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ar") : g->GV(ar);

  LINE(7,x_set_error_handler("my_error_handler"));
  echo("====test1====\n");
  (v_o = ((Object)(LINE(31,p_test1(p_test1(NEWOBJ(c_test1)())->create())))));
  LINE(32,x_print_r(v_o));
  LINE(33,x_var_dump(1, toString(v_o)));
  LINE(34,x_var_dump(1, v_o));
  echo("====test2====\n");
  (v_o = ((Object)(LINE(37,p_test2(p_test2(NEWOBJ(c_test2)())->create())))));
  LINE(38,x_print_r(v_o));
  print(toString(v_o));
  LINE(40,x_var_dump(1, v_o));
  echo("====test3====\n");
  echo(toString(v_o));
  echo("====test4====\n");
  echo(concat("string:", toString(v_o)));
  echo("====test5====\n");
  echo(concat(toString(1LL), toString(v_o)));
  {
    echo(toString(1LL));
    echo(toString(v_o));
  }
  echo("====test6====\n");
  echo(concat(toString(v_o), toString(v_o)));
  {
    echo(toString(v_o));
    echo(toString(v_o));
  }
  echo("====test7====\n");
  (v_ar = ScalarArrays::sa_[0]);
  v_ar.set(LINE(57,v_o.o_invoke_few_args("__toString", 0x642C2D2994B34A13LL, 0)), ("ERROR"));
  echo(toString(v_ar.rvalAt(v_o)));
  echo("====test8====\n");
  LINE(61,x_var_dump(1, x_trim(toString(v_o))));
  LINE(62,x_var_dump(1, x_trim(toString(v_o))));
  echo("====test9====\n");
  echo(LINE(65,x_sprintf(2, "%s", Array(ArrayInit(1).set(0, v_o).create()))));
  echo("====test10====\n");
  (v_o = ((Object)(LINE(68,p_test3(p_test3(NEWOBJ(c_test3)())->create())))));
  LINE(69,x_var_dump(1, v_o));
  echo(toString(v_o));
  echo("====DONE====\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
