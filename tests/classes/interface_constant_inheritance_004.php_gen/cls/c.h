
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__

#include <cls/ia.h>
#include <cls/ib.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_constant_inheritance_004.php line 9 */
class c_c : virtual public c_ia, virtual public c_ib {
  BEGIN_CLASS_MAP(c)
    PARENT_CLASS(ia)
    PARENT_CLASS(ia)
    PARENT_CLASS(ib)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
