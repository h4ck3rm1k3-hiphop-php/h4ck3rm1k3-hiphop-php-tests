
#ifndef __GENERATED_cls_testclass_h__
#define __GENERATED_cls_testclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_this.php line 3 */
class c_testclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(testclass)
  END_CLASS_MAP(testclass)
  DECLARE_CLASS(testclass, TestClass, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: static void ti_test1(const char* cls);
  public: static void ti_test2(const char* cls, p_stdclass v_this);
  public: static void t_test1() { ti_test1("testclass"); }
  public: static void t_test2(p_stdclass v_this) { ti_test2("testclass", v_this); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testclass_h__
