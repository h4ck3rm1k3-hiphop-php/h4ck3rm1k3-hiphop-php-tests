
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/new_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/new_001.php line 4 */
Variant c_inc::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x1297A95AAC6F5998LL, g->s_inc_DupIdcounter,
                  counter);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_inc::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_inc::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_inc::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_inc::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_inc::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_inc::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_inc::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(inc)
ObjectData *c_inc::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_inc::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_inc::cloneImpl() {
  c_inc *obj = NEW(c_inc)();
  cloneSet(obj);
  return obj;
}
void c_inc::cloneSet(c_inc *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_inc::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_inc::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_inc::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_inc$os_get(const char *s) {
  return c_inc::os_get(s, -1);
}
Variant &cw_inc$os_lval(const char *s) {
  return c_inc::os_lval(s, -1);
}
Variant cw_inc$os_constant(const char *s) {
  return c_inc::os_constant(s);
}
Variant cw_inc$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_inc::os_invoke(c, s, params, -1, fatal);
}
void c_inc::init() {
}
void c_inc::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_inc_DupIdcounter = 0LL;
}
void csi_inc() {
  c_inc::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/new_001.php line 7 */
void c_inc::t___construct() {
  INSTANCE_METHOD_INJECTION(Inc, Inc::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  (o_lval("id", 0x028B9FE0C4522BE2LL) = ++g->s_inc_DupIdcounter);
  gasInCtor(oldInCtor);
} /* function */
Object co_inc(CArrRef params, bool init /* = true */) {
  return Object(p_inc(NEW(c_inc)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$new_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/new_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$new_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  echo("Compile-time strict error message should precede this.\n");
  (v_f = ((Object)(LINE(13,p_inc(p_inc(NEWOBJ(c_inc)())->create())))));
  (v_k = ref(v_f));
  echo("$f initially points to the first object:\n");
  LINE(16,x_var_dump(1, v_f));
  echo("Assigning new object directly to $k affects $f:\n");
  (v_k = ((Object)(LINE(19,p_inc(p_inc(NEWOBJ(c_inc)())->create())))));
  LINE(20,x_var_dump(1, v_f));
  echo("Assigning new object by ref to $k removes it from $f's reference set, so $f is unchanged:\n");
  (v_k = LINE(23,p_inc(p_inc(NEWOBJ(c_inc)())->create())));
  LINE(24,x_var_dump(1, v_f));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
