
#ifndef __GENERATED_cls_testclass_h__
#define __GENERATED_cls_testclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug29446.php line 3 */
class c_testclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(testclass)
  END_CLASS_MAP(testclass)
  DECLARE_CLASS(testclass, testClass, ObjectData)
  void init();
  static GlobalVariables *lazy_initializer(GlobalVariables *g);
  public: void t_testclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testclass_h__
