
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug29446.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug29446.php line 3 */
Variant c_testclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_testclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_testclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_testclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_testclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_testclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_testclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_testclass::os_constant(const char *s) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x070CD14512C0C368LL, g->q_testclass_TEST_CONST, TEST_CONST);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(testclass)
ObjectData *c_testclass::create() {
  init();
  t_testclass();
  return this;
}
ObjectData *c_testclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_testclass::cloneImpl() {
  c_testclass *obj = NEW(c_testclass)();
  cloneSet(obj);
  return obj;
}
void c_testclass::cloneSet(c_testclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_testclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_testclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_testclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_testclass$os_get(const char *s) {
  return c_testclass::os_get(s, -1);
}
Variant &cw_testclass$os_lval(const char *s) {
  return c_testclass::os_lval(s, -1);
}
Variant cw_testclass$os_constant(const char *s) {
  return c_testclass::os_constant(s);
}
Variant cw_testclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_testclass::os_invoke(c, s, params, -1, fatal);
}
void c_testclass::init() {
}
GlobalVariables *c_testclass::lazy_initializer(GlobalVariables *g) {
  if (!g->csf_testclass) {
    g->csf_testclass = true;
    g->q_testclass_TEST_CONST = "test";
    g->q_testclass_TEST_CONST = "test1";
  }
  return g;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug29446.php line 7 */
void c_testclass::t_testclass() {
  INSTANCE_METHOD_INJECTION(testClass, testClass::testClass);
  bool oldInCtor = gasInCtor(true);
  echo("test" /* testclass::TEST_CONST */);
  gasInCtor(oldInCtor);
} /* function */
Object co_testclass(CArrRef params, bool init /* = true */) {
  return Object(p_testclass(NEW(c_testclass)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$bug29446_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug29446.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$bug29446_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  (v_test = ((Object)(LINE(12,p_testclass(p_testclass(NEWOBJ(c_testclass)())->create())))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
