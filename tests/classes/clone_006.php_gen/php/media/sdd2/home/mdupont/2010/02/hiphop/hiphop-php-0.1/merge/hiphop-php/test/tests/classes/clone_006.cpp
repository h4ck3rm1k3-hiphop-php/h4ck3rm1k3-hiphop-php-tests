
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_006.php line 3 */
Variant c_mycloneable::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x028B9FE0C4522BE2LL, g->s_mycloneable_DupIdid,
                  id);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_mycloneable::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_mycloneable::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_mycloneable::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_mycloneable::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_mycloneable::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_mycloneable::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_mycloneable::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(mycloneable)
ObjectData *c_mycloneable::create() {
  init();
  t_mycloneable();
  return this;
}
ObjectData *c_mycloneable::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_mycloneable::cloneImpl() {
  c_mycloneable *obj = NEW(c_mycloneable)();
  cloneSet(obj);
  return obj;
}
void c_mycloneable::cloneSet(c_mycloneable *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_mycloneable::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_mycloneable::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_mycloneable::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_mycloneable$os_get(const char *s) {
  return c_mycloneable::os_get(s, -1);
}
Variant &cw_mycloneable$os_lval(const char *s) {
  return c_mycloneable::os_lval(s, -1);
}
Variant cw_mycloneable$os_constant(const char *s) {
  return c_mycloneable::os_constant(s);
}
Variant cw_mycloneable$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_mycloneable::os_invoke(c, s, params, -1, fatal);
}
void c_mycloneable::init() {
}
void c_mycloneable::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_mycloneable_DupIdid = 0LL;
}
void csi_mycloneable() {
  c_mycloneable::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_006.php line 6 */
void c_mycloneable::t_mycloneable() {
  INSTANCE_METHOD_INJECTION(MyCloneable, MyCloneable::MyCloneable);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  (o_lval("id", 0x028B9FE0C4522BE2LL) = g->s_mycloneable_DupIdid++);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_006.php line 10 */
Variant c_mycloneable::t___clone() {
  INSTANCE_METHOD_INJECTION(MyCloneable, MyCloneable::__clone);
  DECLARE_GLOBAL_VARIABLES(g);
  (o_lval("address", 0x75B64A012E6FB179LL) = "New York");
  (o_lval("id", 0x028B9FE0C4522BE2LL) = g->s_mycloneable_DupIdid++);
  return null;
} /* function */
Object co_mycloneable(CArrRef params, bool init /* = true */) {
  return Object(p_mycloneable(NEW(c_mycloneable)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_original __attribute__((__unused__)) = (variables != gVariables) ? variables->get("original") : g->GV(original);
  Variant &v_clone __attribute__((__unused__)) = (variables != gVariables) ? variables->get("clone") : g->GV(clone);

  (v_original = ((Object)(LINE(16,p_mycloneable(p_mycloneable(NEWOBJ(c_mycloneable)())->create())))));
  (v_original.o_lval("name", 0x0BCDB293DC3CBDDCLL) = "Hello");
  (v_original.o_lval("address", 0x75B64A012E6FB179LL) = "Tel-Aviv");
  echo(concat(toString(v_original.o_get("id", 0x028B9FE0C4522BE2LL)), "\n"));
  (v_clone = f_clone(toObject(v_original)));
  echo(concat(toString(v_clone.o_get("id", 0x028B9FE0C4522BE2LL)), "\n"));
  echo(concat(toString(v_clone.o_get("name", 0x0BCDB293DC3CBDDCLL)), "\n"));
  echo(concat(toString(v_clone.o_get("address", 0x75B64A012E6FB179LL)), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
