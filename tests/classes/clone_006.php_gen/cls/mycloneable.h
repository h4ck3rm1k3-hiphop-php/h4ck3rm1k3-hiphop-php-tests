
#ifndef __GENERATED_cls_mycloneable_h__
#define __GENERATED_cls_mycloneable_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_006.php line 3 */
class c_mycloneable : virtual public ObjectData {
  BEGIN_CLASS_MAP(mycloneable)
  END_CLASS_MAP(mycloneable)
  DECLARE_CLASS(mycloneable, MyCloneable, ObjectData)
  void init();
  public: void t_mycloneable();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t___clone();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mycloneable_h__
