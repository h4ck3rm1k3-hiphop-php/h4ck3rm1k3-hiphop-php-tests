
#ifndef __GENERATED_cls_pass_h__
#define __GENERATED_cls_pass_h__

#include <cls/fail.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract.php line 7 */
class c_pass : virtual public c_fail {
  BEGIN_CLASS_MAP(pass)
    PARENT_CLASS(fail)
  END_CLASS_MAP(pass)
  DECLARE_CLASS(pass, pass, fail)
  void init();
  public: void t_show();
  public: void t_error();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pass_h__
