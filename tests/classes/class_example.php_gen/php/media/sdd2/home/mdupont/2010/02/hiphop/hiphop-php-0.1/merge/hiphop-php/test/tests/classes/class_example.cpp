
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_example.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_example.php line 6 */
Variant c_user::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_user::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_user::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("first_name", m_first_name.isReferenced() ? ref(m_first_name) : m_first_name));
  props.push_back(NEW(ArrayElement)("family_name", m_family_name.isReferenced() ? ref(m_family_name) : m_family_name));
  props.push_back(NEW(ArrayElement)("address", m_address.isReferenced() ? ref(m_address) : m_address));
  props.push_back(NEW(ArrayElement)("phone_num", m_phone_num.isReferenced() ? ref(m_phone_num) : m_phone_num));
  c_ObjectData::o_get(props);
}
bool c_user::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x75B64A012E6FB179LL, address, 7);
      break;
    case 2:
      HASH_EXISTS_STRING(0x012A5ADB562F426ALL, first_name, 10);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7D858FB5BB2AE114LL, family_name, 11);
      break;
    case 5:
      HASH_EXISTS_STRING(0x14E8372B3063AB2DLL, phone_num, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_user::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x75B64A012E6FB179LL, m_address,
                         address, 7);
      break;
    case 2:
      HASH_RETURN_STRING(0x012A5ADB562F426ALL, m_first_name,
                         first_name, 10);
      break;
    case 4:
      HASH_RETURN_STRING(0x7D858FB5BB2AE114LL, m_family_name,
                         family_name, 11);
      break;
    case 5:
      HASH_RETURN_STRING(0x14E8372B3063AB2DLL, m_phone_num,
                         phone_num, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_user::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x75B64A012E6FB179LL, m_address,
                      address, 7);
      break;
    case 2:
      HASH_SET_STRING(0x012A5ADB562F426ALL, m_first_name,
                      first_name, 10);
      break;
    case 4:
      HASH_SET_STRING(0x7D858FB5BB2AE114LL, m_family_name,
                      family_name, 11);
      break;
    case 5:
      HASH_SET_STRING(0x14E8372B3063AB2DLL, m_phone_num,
                      phone_num, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_user::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x75B64A012E6FB179LL, m_address,
                         address, 7);
      break;
    case 2:
      HASH_RETURN_STRING(0x012A5ADB562F426ALL, m_first_name,
                         first_name, 10);
      break;
    case 4:
      HASH_RETURN_STRING(0x7D858FB5BB2AE114LL, m_family_name,
                         family_name, 11);
      break;
    case 5:
      HASH_RETURN_STRING(0x14E8372B3063AB2DLL, m_phone_num,
                         phone_num, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_user::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(user)
ObjectData *c_user::cloneImpl() {
  c_user *obj = NEW(c_user)();
  cloneSet(obj);
  return obj;
}
void c_user::cloneSet(c_user *clone) {
  clone->m_first_name = m_first_name.isReferenced() ? ref(m_first_name) : m_first_name;
  clone->m_family_name = m_family_name.isReferenced() ? ref(m_family_name) : m_family_name;
  clone->m_address = m_address.isReferenced() ? ref(m_address) : m_address;
  clone->m_phone_num = m_phone_num.isReferenced() ? ref(m_phone_num) : m_phone_num;
  ObjectData::cloneSet(clone);
}
Variant c_user::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x63B3715CB4F641E4LL, initialize) {
        return (t_initialize(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x797A6FC6339ADA9ELL, display) {
        return (t_display(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_user::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x63B3715CB4F641E4LL, initialize) {
        return (t_initialize(a0, a1, a2, a3), null);
      }
      break;
    case 2:
      HASH_GUARD(0x797A6FC6339ADA9ELL, display) {
        return (t_display(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_user::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_user$os_get(const char *s) {
  return c_user::os_get(s, -1);
}
Variant &cw_user$os_lval(const char *s) {
  return c_user::os_lval(s, -1);
}
Variant cw_user$os_constant(const char *s) {
  return c_user::os_constant(s);
}
Variant cw_user$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_user::os_invoke(c, s, params, -1, fatal);
}
void c_user::init() {
  m_first_name = null;
  m_family_name = null;
  m_address = null;
  m_phone_num = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_example.php line 8 */
void c_user::t_display() {
  INSTANCE_METHOD_INJECTION(user, user::display);
  echo("User information\n");
  echo("----------------\n\n");
  echo(LINE(12,concat3("First name:\t  ", toString(m_first_name), "\n")));
  echo(LINE(13,concat3("Family name:\t  ", toString(m_family_name), "\n")));
  echo(LINE(14,concat3("Address:\t  ", toString(m_address), "\n")));
  echo(LINE(15,concat3("Phone:\t\t  ", toString(m_phone_num), "\n")));
  echo("\n\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_example.php line 18 */
void c_user::t_initialize(CVarRef v_first_name, CVarRef v_family_name, CVarRef v_address, CVarRef v_phone_num) {
  INSTANCE_METHOD_INJECTION(user, user::initialize);
  (m_first_name = v_first_name);
  (m_family_name = v_family_name);
  (m_address = v_address);
  (m_phone_num = v_phone_num);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_example.php line 28 */
Variant f_test(CVarRef v_u) {
  FUNCTION_INJECTION(test);
  Variant v_t;

  LINE(30,toObject(v_u)->o_invoke_few_args("display", 0x797A6FC6339ADA9ELL, 0));
  (v_t = v_u);
  (v_t.o_lval("address", 0x75B64A012E6FB179LL) = "New address...");
  return v_t;
} /* function */
Object co_user(CArrRef params, bool init /* = true */) {
  return Object(p_user(NEW(c_user)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$class_example_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_example.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$class_example_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_user1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("user1") : g->GV(user1);
  Variant &v_user2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("user2") : g->GV(user2);
  Variant &v_tmp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tmp") : g->GV(tmp);

  echo("\n");
  (v_user1 = ((Object)(LINE(36,p_user(p_user(NEWOBJ(c_user)())->create())))));
  (v_user2 = ((Object)(LINE(37,p_user(p_user(NEWOBJ(c_user)())->create())))));
  LINE(39,v_user1.o_invoke_few_args("initialize", 0x63B3715CB4F641E4LL, 4, "Zeev", "Suraski", "Ben Gourion 3, Kiryat Bialik, Israel", "+972-4-8713139"));
  LINE(40,v_user2.o_invoke_few_args("initialize", 0x63B3715CB4F641E4LL, 4, "Andi", "Gutmans", "Haifa, Israel", "+972-4-8231621"));
  LINE(41,v_user1.o_invoke_few_args("display", 0x797A6FC6339ADA9ELL, 0));
  LINE(42,v_user2.o_invoke_few_args("display", 0x797A6FC6339ADA9ELL, 0));
  (v_tmp = LINE(44,f_test(v_user2)));
  LINE(45,v_tmp.o_invoke_few_args("display", 0x797A6FC6339ADA9ELL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
