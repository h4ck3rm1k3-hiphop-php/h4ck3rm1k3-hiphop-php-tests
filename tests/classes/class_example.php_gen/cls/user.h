
#ifndef __GENERATED_cls_user_h__
#define __GENERATED_cls_user_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/class_example.php line 6 */
class c_user : virtual public ObjectData {
  BEGIN_CLASS_MAP(user)
  END_CLASS_MAP(user)
  DECLARE_CLASS(user, user, ObjectData)
  void init();
  public: Variant m_first_name;
  public: Variant m_family_name;
  public: Variant m_address;
  public: Variant m_phone_num;
  public: void t_display();
  public: void t_initialize(CVarRef v_first_name, CVarRef v_family_name, CVarRef v_address, CVarRef v_phone_num);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_user_h__
