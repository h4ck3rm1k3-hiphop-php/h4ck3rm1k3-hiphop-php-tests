
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 12 */
Variant c_late::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_late::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_late::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_late::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_late::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_late::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_late::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_late::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(late)
ObjectData *c_late::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_late::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_late::dynConstruct(CArrRef params) {
  (t___construct());
}
void c_late::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_late::cloneImpl() {
  c_late *obj = NEW(c_late)();
  cloneSet(obj);
  return obj;
}
void c_late::cloneSet(c_late *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_late::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_late::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_late::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_late$os_get(const char *s) {
  return c_late::os_get(s, -1);
}
Variant &cw_late$os_lval(const char *s) {
  return c_late::os_lval(s, -1);
}
Variant cw_late$os_constant(const char *s) {
  return c_late::os_constant(s);
}
Variant cw_late$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_late::os_invoke(c, s, params, -1, fatal);
}
void c_late::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 13 */
void c_late::t___construct() {
  INSTANCE_METHOD_INJECTION(late, late::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("late::__construct\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 16 */
Variant c_late::t___destruct() {
  INSTANCE_METHOD_INJECTION(late, late::__destruct);
  setInDtor();
  echo("late::__destruct\n");
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 3 */
Variant c_early::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_early::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_early::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_early::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_early::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_early::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_early::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_early::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(early)
ObjectData *c_early::create() {
  init();
  t_early();
  return this;
}
ObjectData *c_early::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_early::dynConstruct(CArrRef params) {
  (t_early());
}
void c_early::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_early::cloneImpl() {
  c_early *obj = NEW(c_early)();
  cloneSet(obj);
  return obj;
}
void c_early::cloneSet(c_early *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_early::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x22B3A189EC60BB9DLL, early) {
        return (t_early(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_early::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x22B3A189EC60BB9DLL, early) {
        return (t_early(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_early::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_early$os_get(const char *s) {
  return c_early::os_get(s, -1);
}
Variant &cw_early$os_lval(const char *s) {
  return c_early::os_lval(s, -1);
}
Variant cw_early$os_constant(const char *s) {
  return c_early::os_constant(s);
}
Variant cw_early$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_early::os_invoke(c, s, params, -1, fatal);
}
void c_early::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 4 */
void c_early::t_early() {
  INSTANCE_METHOD_INJECTION(early, early::early);
  bool oldInCtor = gasInCtor(true);
  echo("early::early\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 7 */
Variant c_early::t___destruct() {
  INSTANCE_METHOD_INJECTION(early, early::__destruct);
  setInDtor();
  echo("early::__destruct\n");
  return null;
} /* function */
Object co_late(CArrRef params, bool init /* = true */) {
  return Object(p_late(NEW(c_late)())->dynCreate(params, init));
}
Object co_early(CArrRef params, bool init /* = true */) {
  return Object(p_early(NEW(c_early)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_dtor_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_dtor_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_t = ((Object)(LINE(21,p_early(p_early(NEWOBJ(c_early)())->create())))));
  LINE(22,v_t.o_invoke_few_args("early", 0x22B3A189EC60BB9DLL, 0));
  unset(v_t);
  (v_t = ((Object)(LINE(24,p_late(p_late(NEWOBJ(c_late)())->create())))));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
