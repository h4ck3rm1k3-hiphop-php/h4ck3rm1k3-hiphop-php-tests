
#ifndef __GENERATED_cls_early_h__
#define __GENERATED_cls_early_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 3 */
class c_early : virtual public ObjectData {
  BEGIN_CLASS_MAP(early)
  END_CLASS_MAP(early)
  DECLARE_CLASS(early, early, ObjectData)
  void init();
  public: virtual void destruct();
  public: void t_early();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_early_h__
