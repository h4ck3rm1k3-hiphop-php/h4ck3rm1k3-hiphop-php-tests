
#ifndef __GENERATED_cls_late_h__
#define __GENERATED_cls_late_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor.php line 12 */
class c_late : virtual public ObjectData {
  BEGIN_CLASS_MAP(late)
  END_CLASS_MAP(late)
  DECLARE_CLASS(late, late, ObjectData)
  void init();
  public: virtual void destruct();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_late_h__
