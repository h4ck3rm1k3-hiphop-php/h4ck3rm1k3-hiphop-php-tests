
#ifndef __GENERATED_cls_pass_h__
#define __GENERATED_cls_pass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/protected_001.php line 3 */
class c_pass : virtual public ObjectData {
  BEGIN_CLASS_MAP(pass)
  END_CLASS_MAP(pass)
  DECLARE_CLASS(pass, pass, ObjectData)
  void init();
  public: static void ti_fail(const char* cls);
  public: static void ti_good(const char* cls);
  public: static void t_good() { ti_good("pass"); }
  public: static void t_fail() { ti_fail("pass"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pass_h__
