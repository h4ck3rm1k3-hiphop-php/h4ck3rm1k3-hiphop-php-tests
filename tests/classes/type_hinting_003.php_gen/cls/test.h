
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_003.php line 3 */
class c_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(test)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, Test, ObjectData)
  void init();
  public: static void ti_f1(const char* cls, CVarRef v_ar);
  public: static void ti_f2(const char* cls, CVarRef v_ar = null_variant);
  public: static void ti_f3(const char* cls, CArrRef v_ar = ScalarArrays::sa_[0]);
  public: static void ti_f4(const char* cls, CArrRef v_ar = ScalarArrays::sa_[1]);
  public: static void t_f1(CVarRef v_ar) { ti_f1("test", v_ar); }
  public: static void t_f2(CVarRef v_ar = null_variant) { ti_f2("test", v_ar); }
  public: static void t_f3(CArrRef v_ar = ScalarArrays::sa_[0]) { ti_f3("test", v_ar); }
  public: static void t_f4(CArrRef v_ar = ScalarArrays::sa_[1]) { ti_f4("test", v_ar); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
