
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_005.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_005.php line 3 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  props.push_back(NEW(ArrayElement)("b", m_b));
  props.push_back(NEW(ArrayElement)("c", m_c));
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    case 2:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    case 2:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    case 5:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  clone->m_a = m_a;
  clone->m_b = m_b;
  clone->m_c = m_c;
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
  m_a = 1LL;
  m_b = 2LL;
  m_c = 3LL;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_005.php line 9 */
void c_base::t_f() {
  INSTANCE_METHOD_INJECTION(base, base::f);
  Primitive v_k = 0;
  Variant v_v;

  {
    LOOP_COUNTER(1);
    Variant map2 = ((Object)(this));
    for (ArrayIterPtr iter3 = map2.begin("base"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        echo(LINE(12,concat4(toString(v_k), "=>", toString(v_v), "\n")));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_005.php line 17 */
Variant c_derived::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_derived::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_derived::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_derived::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_derived::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_derived::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_derived::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_derived::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(derived)
ObjectData *c_derived::cloneImpl() {
  c_derived *obj = NEW(c_derived)();
  cloneSet(obj);
  return obj;
}
void c_derived::cloneSet(c_derived *clone) {
  c_base::cloneSet(clone);
}
Variant c_derived::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_derived::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived$os_get(const char *s) {
  return c_derived::os_get(s, -1);
}
Variant &cw_derived$os_lval(const char *s) {
  return c_derived::os_lval(s, -1);
}
Variant cw_derived$os_constant(const char *s) {
  return c_derived::os_constant(s);
}
Variant cw_derived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived::os_invoke(c, s, params, -1, fatal);
}
void c_derived::init() {
  c_base::init();
}
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Object co_derived(CArrRef params, bool init /* = true */) {
  return Object(p_derived(NEW(c_derived)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$visibility_005_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_005.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$visibility_005_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  (v_o = ((Object)(LINE(21,p_base(p_base(NEWOBJ(c_base)())->create())))));
  (v_o.o_lval("d", 0x7A452383AA9BF7C4LL) = 4LL);
  echo("===base::function===\n");
  LINE(24,v_o.o_invoke_few_args("f", 0x286CE1C477560280LL, 0));
  echo("===base,foreach===\n");
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_o.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        echo(LINE(27,concat4(toString(v_k), "=>", toString(v_v), "\n")));
      }
    }
  }
  (v_o = ((Object)(LINE(30,p_derived(p_derived(NEWOBJ(c_derived)())->create())))));
  (v_o.o_lval("d", 0x7A452383AA9BF7C4LL) = 4LL);
  echo("===derived::function===\n");
  LINE(33,v_o.o_invoke_few_args("f", 0x286CE1C477560280LL, 0));
  echo("===derived,foreach===\n");
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_o.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
      v_k = iter9->first();
      {
        echo(LINE(36,concat4(toString(v_k), "=>", toString(v_v), "\n")));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
