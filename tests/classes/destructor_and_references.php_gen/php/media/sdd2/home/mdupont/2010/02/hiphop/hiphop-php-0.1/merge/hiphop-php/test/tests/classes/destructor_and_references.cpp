
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.php line 3 */
Variant c_test1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test1::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  c_ObjectData::o_get(props);
}
bool c_test1::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test1::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test1::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test1)
ObjectData *c_test1::cloneImpl() {
  c_test1 *obj = NEW(c_test1)();
  cloneSet(obj);
  return obj;
}
void c_test1::cloneSet(c_test1 *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  ObjectData::cloneSet(clone);
}
Variant c_test1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test1$os_get(const char *s) {
  return c_test1::os_get(s, -1);
}
Variant &cw_test1$os_lval(const char *s) {
  return c_test1::os_lval(s, -1);
}
Variant cw_test1$os_constant(const char *s) {
  return c_test1::os_constant(s);
}
Variant cw_test1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test1::os_invoke(c, s, params, -1, fatal);
}
void c_test1::init() {
  m_x = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.php line 4 */
Variant c_test2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test2::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  c_ObjectData::o_get(props);
}
bool c_test2::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test2::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test2::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test2)
ObjectData *c_test2::cloneImpl() {
  c_test2 *obj = NEW(c_test2)();
  cloneSet(obj);
  return obj;
}
void c_test2::cloneSet(c_test2 *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  ObjectData::cloneSet(clone);
}
Variant c_test2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test2$os_get(const char *s) {
  return c_test2::os_get(s, -1);
}
Variant &cw_test2$os_lval(const char *s) {
  return c_test2::os_lval(s, -1);
}
Variant cw_test2$os_constant(const char *s) {
  return c_test2::os_constant(s);
}
Variant cw_test2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test2::os_invoke(c, s, params, -1, fatal);
}
void c_test2::init() {
  m_x = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.php line 17 */
Variant c_once::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_once::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_once::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_once::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_once::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_once::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_once::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_once::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(once)
ObjectData *c_once::cloneImpl() {
  c_once *obj = NEW(c_once)();
  cloneSet(obj);
  return obj;
}
void c_once::cloneSet(c_once *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_once::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_once::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_once::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_once$os_get(const char *s) {
  return c_once::os_get(s, -1);
}
Variant &cw_once$os_lval(const char *s) {
  return c_once::os_lval(s, -1);
}
Variant cw_once$os_constant(const char *s) {
  return c_once::os_constant(s);
}
Variant cw_once$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_once::os_invoke(c, s, params, -1, fatal);
}
void c_once::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.php line 5 */
Variant c_test3::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test3::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test3::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  c_ObjectData::o_get(props);
}
bool c_test3::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test3::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test3::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test3::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test3::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test3)
ObjectData *c_test3::cloneImpl() {
  c_test3 *obj = NEW(c_test3)();
  cloneSet(obj);
  return obj;
}
void c_test3::cloneSet(c_test3 *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  ObjectData::cloneSet(clone);
}
Variant c_test3::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test3::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test3::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test3$os_get(const char *s) {
  return c_test3::os_get(s, -1);
}
Variant &cw_test3$os_lval(const char *s) {
  return c_test3::os_lval(s, -1);
}
Variant cw_test3$os_constant(const char *s) {
  return c_test3::os_constant(s);
}
Variant cw_test3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test3::os_invoke(c, s, params, -1, fatal);
}
void c_test3::init() {
  m_x = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.php line 6 */
Variant c_test4::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test4::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test4::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  c_ObjectData::o_get(props);
}
bool c_test4::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test4::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test4::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test4::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test4::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test4)
ObjectData *c_test4::cloneImpl() {
  c_test4 *obj = NEW(c_test4)();
  cloneSet(obj);
  return obj;
}
void c_test4::cloneSet(c_test4 *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  ObjectData::cloneSet(clone);
}
Variant c_test4::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test4::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test4::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test4$os_get(const char *s) {
  return c_test4::os_get(s, -1);
}
Variant &cw_test4$os_lval(const char *s) {
  return c_test4::os_lval(s, -1);
}
Variant cw_test4$os_constant(const char *s) {
  return c_test4::os_constant(s);
}
Variant cw_test4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test4::os_invoke(c, s, params, -1, fatal);
}
void c_test4::init() {
  m_x = null;
}
Object co_test1(CArrRef params, bool init /* = true */) {
  return Object(p_test1(NEW(c_test1)())->dynCreate(params, init));
}
Object co_test2(CArrRef params, bool init /* = true */) {
  return Object(p_test2(NEW(c_test2)())->dynCreate(params, init));
}
Object co_once(CArrRef params, bool init /* = true */) {
  return Object(p_once(NEW(c_once)())->dynCreate(params, init));
}
Object co_test3(CArrRef params, bool init /* = true */) {
  return Object(p_test3(NEW(c_test3)())->dynCreate(params, init));
}
Object co_test4(CArrRef params, bool init /* = true */) {
  return Object(p_test4(NEW(c_test4)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$destructor_and_references_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$destructor_and_references_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o1") : g->GV(o1);
  Variant &v_o2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o2") : g->GV(o2);
  Variant &v_o3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o3") : g->GV(o3);
  Variant &v_o4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o4") : g->GV(o4);
  Variant &v_r1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r1") : g->GV(r1);
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);

  (v_o1 = ((Object)(LINE(8,p_test1(p_test1(NEWOBJ(c_test1)())->create())))));
  (v_o2 = ((Object)(LINE(9,p_test2(p_test2(NEWOBJ(c_test2)())->create())))));
  (v_o3 = ((Object)(LINE(10,p_test3(p_test3(NEWOBJ(c_test3)())->create())))));
  (v_o4 = ((Object)(LINE(11,p_test4(p_test4(NEWOBJ(c_test4)())->create())))));
  (v_o3.o_lval("x", 0x04BFC205E59FA416LL) = ref(v_o4));
  (v_r1 = ref(v_o1));
  (v_o = ((Object)(LINE(19,p_once(p_once(NEWOBJ(c_once)())->create())))));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
