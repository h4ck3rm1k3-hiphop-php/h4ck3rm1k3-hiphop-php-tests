
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_destructor_and_references_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_destructor_and_references_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_references.fw.h>

// Declarations
#include <cls/test1.h>
#include <cls/test2.h>
#include <cls/once.h>
#include <cls/test3.h>
#include <cls/test4.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$destructor_and_references_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_test1(CArrRef params, bool init = true);
Object co_test2(CArrRef params, bool init = true);
Object co_once(CArrRef params, bool init = true);
Object co_test3(CArrRef params, bool init = true);
Object co_test4(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_destructor_and_references_h__
