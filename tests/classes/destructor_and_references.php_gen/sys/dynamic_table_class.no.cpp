
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_test1(CArrRef params, bool init = true);
Variant cw_test1$os_get(const char *s);
Variant &cw_test1$os_lval(const char *s);
Variant cw_test1$os_constant(const char *s);
Variant cw_test1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test2(CArrRef params, bool init = true);
Variant cw_test2$os_get(const char *s);
Variant &cw_test2$os_lval(const char *s);
Variant cw_test2$os_constant(const char *s);
Variant cw_test2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_once(CArrRef params, bool init = true);
Variant cw_once$os_get(const char *s);
Variant &cw_once$os_lval(const char *s);
Variant cw_once$os_constant(const char *s);
Variant cw_once$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test3(CArrRef params, bool init = true);
Variant cw_test3$os_get(const char *s);
Variant &cw_test3$os_lval(const char *s);
Variant cw_test3$os_constant(const char *s);
Variant cw_test3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test4(CArrRef params, bool init = true);
Variant cw_test4$os_get(const char *s);
Variant &cw_test4$os_lval(const char *s);
Variant cw_test4$os_constant(const char *s);
Variant cw_test4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_CREATE_OBJECT(0x4E9D22C1CC5ACB11LL, test3);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x01C20270EBE62C33LL, test4);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x5672EB7381D05126LL, test1);
      break;
    case 8:
      HASH_CREATE_OBJECT(0x4998AD7F5B1AFA38LL, once);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x462035572CE10F0FLL, test2);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x4E9D22C1CC5ACB11LL, test3);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x01C20270EBE62C33LL, test4);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x5672EB7381D05126LL, test1);
      break;
    case 8:
      HASH_INVOKE_STATIC_METHOD(0x4998AD7F5B1AFA38LL, once);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x462035572CE10F0FLL, test2);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x4E9D22C1CC5ACB11LL, test3);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x01C20270EBE62C33LL, test4);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x5672EB7381D05126LL, test1);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY(0x4998AD7F5B1AFA38LL, once);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x462035572CE10F0FLL, test2);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x4E9D22C1CC5ACB11LL, test3);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x01C20270EBE62C33LL, test4);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x5672EB7381D05126LL, test1);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY_LV(0x4998AD7F5B1AFA38LL, once);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x462035572CE10F0FLL, test2);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x4E9D22C1CC5ACB11LL, test3);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x01C20270EBE62C33LL, test4);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x5672EB7381D05126LL, test1);
      break;
    case 8:
      HASH_GET_CLASS_CONSTANT(0x4998AD7F5B1AFA38LL, once);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x462035572CE10F0FLL, test2);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
