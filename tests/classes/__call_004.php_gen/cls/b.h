
#ifndef __GENERATED_cls_b_h__
#define __GENERATED_cls_b_h__

#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_004.php line 9 */
class c_b : virtual public c_a {
  BEGIN_CLASS_MAP(b)
    PARENT_CLASS(a)
  END_CLASS_MAP(b)
  DECLARE_CLASS(b, B, a)
  void init();
  Variant doCall(Variant v_name, Variant v_arguments, bool fatal);
  public: Variant t___call(Variant v_strMethod, Variant v_arrArgs);
  public: void t_test();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_b_h__
