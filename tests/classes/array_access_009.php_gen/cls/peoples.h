
#ifndef __GENERATED_cls_peoples_h__
#define __GENERATED_cls_peoples_h__

#include <cls/arrayproxyaccess.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 50 */
class c_peoples : virtual public c_arrayproxyaccess {
  BEGIN_CLASS_MAP(peoples)
    PARENT_CLASS(arrayaccess)
    PARENT_CLASS(arrayproxyaccess)
  END_CLASS_MAP(peoples)
  DECLARE_CLASS(peoples, Peoples, ObjectData)
  void init();
  public: Variant m_person;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: bool t_offsetexists(CVarRef v_index);
  public: Variant t_offsetget(Variant v_index);
  public: virtual Variant &___offsetget_lval(Variant v_index);
  public: void t_offsetset(CVarRef v_index, CVarRef v_value);
  public: void t_offsetunset(CVarRef v_index);
  public: Variant t_proxyget(CVarRef v_element);
  public: void t_proxyset(CVarRef v_element, CVarRef v_index, CVarRef v_value);
  public: void t_proxyunset(CVarRef v_element, CVarRef v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_peoples_h__
