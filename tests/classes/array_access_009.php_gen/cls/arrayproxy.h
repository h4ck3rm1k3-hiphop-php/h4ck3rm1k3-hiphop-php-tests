
#ifndef __GENERATED_cls_arrayproxy_h__
#define __GENERATED_cls_arrayproxy_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 12 */
class c_arrayproxy : virtual public c_arrayaccess {
  BEGIN_CLASS_MAP(arrayproxy)
    PARENT_CLASS(arrayaccess)
  END_CLASS_MAP(arrayproxy)
  DECLARE_CLASS(arrayproxy, ArrayProxy, ObjectData)
  void init();
  public: Variant m_object;
  public: Variant m_element;
  public: void t___construct(Variant v_object, Variant v_element);
  public: ObjectData *create(Variant v_object, Variant v_element);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: bool t_offsetexists(CVarRef v_index);
  public: Variant t_offsetget(Variant v_index);
  public: virtual Variant &___offsetget_lval(Variant v_index);
  public: void t_offsetset(Variant v_index, Variant v_value);
  public: void t_offsetunset(Variant v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_arrayproxy_h__
