
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 12 */
Variant c_arrayproxy::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_arrayproxy::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_arrayproxy::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("object", m_object.isReferenced() ? ref(m_object) : m_object));
  props.push_back(NEW(ArrayElement)("element", m_element.isReferenced() ? ref(m_element) : m_element));
  c_ObjectData::o_get(props);
}
bool c_arrayproxy::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x7F30BC4E222B1861LL, object, 6);
      HASH_EXISTS_STRING(0x1AFD3E7207DEC289LL, element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_arrayproxy::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x7F30BC4E222B1861LL, m_object,
                         object, 6);
      HASH_RETURN_STRING(0x1AFD3E7207DEC289LL, m_element,
                         element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_arrayproxy::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x7F30BC4E222B1861LL, m_object,
                      object, 6);
      HASH_SET_STRING(0x1AFD3E7207DEC289LL, m_element,
                      element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_arrayproxy::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x7F30BC4E222B1861LL, m_object,
                         object, 6);
      HASH_RETURN_STRING(0x1AFD3E7207DEC289LL, m_element,
                         element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_arrayproxy::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(arrayproxy)
ObjectData *c_arrayproxy::create(Variant v_object, Variant v_element) {
  init();
  t___construct(v_object, v_element);
  return this;
}
ObjectData *c_arrayproxy::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_arrayproxy::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_arrayproxy::cloneImpl() {
  c_arrayproxy *obj = NEW(c_arrayproxy)();
  cloneSet(obj);
  return obj;
}
void c_arrayproxy::cloneSet(c_arrayproxy *clone) {
  clone->m_object = m_object.isReferenced() ? ref(m_object) : m_object;
  clone->m_element = m_element.isReferenced() ? ref(m_element) : m_element;
  ObjectData::cloneSet(clone);
}
Variant c_arrayproxy::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 10:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_arrayproxy::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      break;
    case 8:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 10:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_arrayproxy::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_arrayproxy$os_get(const char *s) {
  return c_arrayproxy::os_get(s, -1);
}
Variant &cw_arrayproxy$os_lval(const char *s) {
  return c_arrayproxy::os_lval(s, -1);
}
Variant cw_arrayproxy$os_constant(const char *s) {
  return c_arrayproxy::os_constant(s);
}
Variant cw_arrayproxy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_arrayproxy::os_invoke(c, s, params, -1, fatal);
}
void c_arrayproxy::init() {
  m_object = null;
  m_element = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 17 */
void c_arrayproxy::t___construct(Variant v_object, Variant v_element) {
  INSTANCE_METHOD_INJECTION(ArrayProxy, ArrayProxy::__construct);
  bool oldInCtor = gasInCtor(true);
  echo(concat("ArrayProxy::__construct", LINE(19,concat3("(", toString(v_element), ")\n"))));
  if (!(toBoolean(LINE(20,v_object.o_invoke_few_args("offsetExists", 0x3E6BCFB9742FC700LL, 1, v_element))))) {
    v_object.set(v_element, (ScalarArrays::sa_[0]));
  }
  (m_object = v_object);
  (m_element = v_element);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 28 */
bool c_arrayproxy::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(ArrayProxy, ArrayProxy::offsetExists);
  Variant eo_0;
  Variant eo_1;
  echo(concat("ArrayProxy::offsetExists", LINE(29,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  return LINE(30,(assignCallTemp(eo_0, v_index),assignCallTemp(eo_1, m_object.o_invoke_few_args("proxyGet", 0x44C0C00B02A678B5LL, 1, m_element)),x_array_key_exists(eo_0, eo_1)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 33 */
Variant c_arrayproxy::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayProxy, ArrayProxy::offsetGet);
  Variant v_tmp;

  echo(concat("ArrayProxy::offsetGet", LINE(34,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  (v_tmp = LINE(35,m_object.o_invoke_few_args("proxyGet", 0x44C0C00B02A678B5LL, 1, m_element)));
  return isset(v_tmp, v_index) ? ((Variant)(v_tmp.rvalAt(v_index))) : ((Variant)(null));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 33 */
Variant &c_arrayproxy::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayProxy, ArrayProxy::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 39 */
void c_arrayproxy::t_offsetset(Variant v_index, Variant v_value) {
  INSTANCE_METHOD_INJECTION(ArrayProxy, ArrayProxy::offsetSet);
  echo(concat("ArrayProxy::offsetSet", concat("(", LINE(40,concat6(toString(m_element), ", ", toString(v_index), ", ", toString(v_value), ")\n")))));
  LINE(41,m_object.o_invoke_few_args("proxySet", 0x18249ECF6CC31F2ALL, 3, m_element, v_index, v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 44 */
void c_arrayproxy::t_offsetunset(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayProxy, ArrayProxy::offsetUnset);
  echo(concat("ArrayProxy::offsetUnset", LINE(45,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  LINE(46,m_object.o_invoke_few_args("proxyUnset", 0x44516B113ADBC013LL, 2, m_element, v_index));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 50 */
Variant c_peoples::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_peoples::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_peoples::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("person", m_person.isReferenced() ? ref(m_person) : m_person));
  c_ObjectData::o_get(props);
}
bool c_peoples::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x67AE1F9174AB011DLL, person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_peoples::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_peoples::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x67AE1F9174AB011DLL, m_person,
                      person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_peoples::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_peoples::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(peoples)
ObjectData *c_peoples::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_peoples::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_peoples::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_peoples::cloneImpl() {
  c_peoples *obj = NEW(c_peoples)();
  cloneSet(obj);
  return obj;
}
void c_peoples::cloneSet(c_peoples *clone) {
  clone->m_person = m_person.isReferenced() ? ref(m_person) : m_person;
  ObjectData::cloneSet(clone);
}
Variant c_peoples::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x44516B113ADBC013LL, proxyunset) {
        return (t_proxyunset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x44C0C00B02A678B5LL, proxyget) {
        return (t_proxyget(params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 10:
      HASH_GUARD(0x18249ECF6CC31F2ALL, proxyset) {
        return (t_proxyset(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_peoples::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x44516B113ADBC013LL, proxyunset) {
        return (t_proxyunset(a0, a1), null);
      }
      break;
    case 5:
      HASH_GUARD(0x44C0C00B02A678B5LL, proxyget) {
        return (t_proxyget(a0));
      }
      break;
    case 8:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 10:
      HASH_GUARD(0x18249ECF6CC31F2ALL, proxyset) {
        return (t_proxyset(a0, a1, a2), null);
      }
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_peoples::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_peoples$os_get(const char *s) {
  return c_peoples::os_get(s, -1);
}
Variant &cw_peoples$os_lval(const char *s) {
  return c_peoples::os_lval(s, -1);
}
Variant cw_peoples$os_constant(const char *s) {
  return c_peoples::os_constant(s);
}
Variant cw_peoples$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_peoples::os_invoke(c, s, params, -1, fatal);
}
void c_peoples::init() {
  m_person = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 54 */
void c_peoples::t___construct() {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_person = ScalarArrays::sa_[1]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 59 */
bool c_peoples::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetExists);
  return LINE(61,x_array_key_exists(v_index, m_person));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 64 */
Variant c_peoples::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  return ((Object)(LINE(66,p_arrayproxy(p_arrayproxy(NEWOBJ(c_arrayproxy)())->create(((Object)(this)), v_index)))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 64 */
Variant &c_peoples::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 69 */
void c_peoples::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetSet);
  m_person.set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 74 */
void c_peoples::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  m_person.weakRemove(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 79 */
Variant c_peoples::t_proxyget(CVarRef v_element) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::proxyGet);
  return m_person.rvalAt(v_element);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 84 */
void c_peoples::t_proxyset(CVarRef v_element, CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::proxySet);
  lval(m_person.lvalAt(v_element)).set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php line 89 */
void c_peoples::t_proxyunset(CVarRef v_element, CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::proxyUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  lval(unsetLval(m_person, v_element)).weakRemove(v_index);
} /* function */
Object co_arrayproxy(CArrRef params, bool init /* = true */) {
  return Object(p_arrayproxy(NEW(c_arrayproxy)())->dynCreate(params, init));
}
Object co_peoples(CArrRef params, bool init /* = true */) {
  return Object(p_peoples(NEW(c_peoples)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_people __attribute__((__unused__)) = (variables != gVariables) ? variables->get("people") : g->GV(people);

  (v_people = ((Object)(LINE(95,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(97,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", (concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Bar")), 0x0BCDB293DC3CBDDCLL);
  LINE(99,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  concat_assign(lval(lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Baz");
  LINE(101,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("===ArrayOverloading===\n");
  (v_people = ((Object)(LINE(105,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(107,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(108,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("FooBar"), 0x0BCDB293DC3CBDDCLL);
  LINE(110,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", (concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Bar")), 0x0BCDB293DC3CBDDCLL);
  LINE(112,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  concat_assign(lval(lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Baz");
  LINE(114,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(unsetLval(v_people, 0LL, 0x77CFA1EEF01BCA90LL)).weakRemove("name", 0x0BCDB293DC3CBDDCLL);
  LINE(116,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(117,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("BlaBla"), 0x0BCDB293DC3CBDDCLL);
  LINE(119,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
