
#ifndef __GENERATED_cls_foobar_h__
#define __GENERATED_cls_foobar_h__

#include <cls/foo.h>
#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_001.php line 11 */
class c_foobar : virtual public c_foo, virtual public c_bar {
  BEGIN_CLASS_MAP(foobar)
    PARENT_CLASS(foo)
    PARENT_CLASS(bar)
  END_CLASS_MAP(foobar)
  DECLARE_CLASS(foobar, FooBar, ObjectData)
  void init();
  public: void t_a(p_foo v_foo);
  public: void t_b(p_bar v_bar);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foobar_h__
