
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_001.php line 21 */
Variant c_blort::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_blort::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_blort::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_blort::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_blort::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_blort::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_blort::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_blort::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(blort)
ObjectData *c_blort::cloneImpl() {
  c_blort *obj = NEW(c_blort)();
  cloneSet(obj);
  return obj;
}
void c_blort::cloneSet(c_blort *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_blort::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_blort::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_blort::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_blort$os_get(const char *s) {
  return c_blort::os_get(s, -1);
}
Variant &cw_blort$os_lval(const char *s) {
  return c_blort::os_lval(s, -1);
}
Variant cw_blort$os_constant(const char *s) {
  return c_blort::os_constant(s);
}
Variant cw_blort$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_blort::os_invoke(c, s, params, -1, fatal);
}
void c_blort::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_001.php line 11 */
Variant c_foobar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foobar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foobar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foobar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foobar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foobar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foobar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foobar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foobar)
ObjectData *c_foobar::cloneImpl() {
  c_foobar *obj = NEW(c_foobar)();
  cloneSet(obj);
  return obj;
}
void c_foobar::cloneSet(c_foobar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foobar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x4F38A775A0938899LL, a) {
        return (t_a(params.rvalAt(0)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x39E9518192E6D2AELL, b) {
        return (t_b(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foobar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x4F38A775A0938899LL, a) {
        return (t_a(a0), null);
      }
      break;
    case 2:
      HASH_GUARD(0x39E9518192E6D2AELL, b) {
        return (t_b(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foobar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foobar$os_get(const char *s) {
  return c_foobar::os_get(s, -1);
}
Variant &cw_foobar$os_lval(const char *s) {
  return c_foobar::os_lval(s, -1);
}
Variant cw_foobar$os_constant(const char *s) {
  return c_foobar::os_constant(s);
}
Variant cw_foobar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foobar::os_invoke(c, s, params, -1, fatal);
}
void c_foobar::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_001.php line 12 */
void c_foobar::t_a(p_foo v_foo) {
  INSTANCE_METHOD_INJECTION(FooBar, FooBar::a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_001.php line 16 */
void c_foobar::t_b(p_bar v_bar) {
  INSTANCE_METHOD_INJECTION(FooBar, FooBar::b);
} /* function */
Object co_blort(CArrRef params, bool init /* = true */) {
  return Object(p_blort(NEW(c_blort)())->dynCreate(params, init));
}
Object co_foobar(CArrRef params, bool init /* = true */) {
  return Object(p_foobar(NEW(c_foobar)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$type_hinting_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$type_hinting_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = ((Object)(LINE(24,p_foobar(p_foobar(NEWOBJ(c_foobar)())->create())))));
  (v_b = ((Object)(LINE(25,p_blort(p_blort(NEWOBJ(c_blort)())->create())))));
  LINE(27,v_a.o_invoke_few_args("a", 0x4F38A775A0938899LL, 1, v_b));
  LINE(28,v_a.o_invoke_few_args("b", 0x39E9518192E6D2AELL, 1, v_b));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
