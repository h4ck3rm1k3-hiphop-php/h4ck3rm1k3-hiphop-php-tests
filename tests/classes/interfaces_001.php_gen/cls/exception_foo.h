
#ifndef __GENERATED_cls_exception_foo_h__
#define __GENERATED_cls_exception_foo_h__

#include <cls/throwable.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_001.php line 7 */
class c_exception_foo : virtual public c_throwable {
  BEGIN_CLASS_MAP(exception_foo)
    PARENT_CLASS(throwable)
  END_CLASS_MAP(exception_foo)
  DECLARE_CLASS(exception_foo, Exception_foo, ObjectData)
  void init();
  public: String m_foo;
  public: String t_getmessage();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_exception_foo_h__
