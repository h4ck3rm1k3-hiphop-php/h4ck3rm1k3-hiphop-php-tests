
#ifndef __GENERATED_cls_derived_a_h__
#define __GENERATED_cls_derived_a_h__

#include <cls/if_a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_must_be_implemented.php line 7 */
class c_derived_a : virtual public c_if_a {
  BEGIN_CLASS_MAP(derived_a)
    PARENT_CLASS(if_a)
  END_CLASS_MAP(derived_a)
  DECLARE_CLASS(derived_a, derived_a, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_a_h__
