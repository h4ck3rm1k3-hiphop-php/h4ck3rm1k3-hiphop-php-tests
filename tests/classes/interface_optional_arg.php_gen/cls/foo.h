
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__

#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_optional_arg.php line 9 */
class c_foo : virtual public c_test {
  BEGIN_CLASS_MAP(foo)
    PARENT_CLASS(test)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, foo, ObjectData)
  void init();
  public: void t_bar(CVarRef v_foo = null_variant);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
