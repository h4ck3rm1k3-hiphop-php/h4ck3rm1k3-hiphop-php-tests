
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__

#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/method_override_optional_arg_001.php line 15 */
class c_c : virtual public c_a {
  BEGIN_CLASS_MAP(c)
    PARENT_CLASS(a)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, a)
  void init();
  public: void t_foo();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
