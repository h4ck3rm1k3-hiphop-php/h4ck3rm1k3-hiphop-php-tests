
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_004.php line 20 */
Variant c_badtostring::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_badtostring::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_badtostring::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_badtostring::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_badtostring::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_badtostring::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_badtostring::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_badtostring::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(badtostring)
ObjectData *c_badtostring::cloneImpl() {
  c_badtostring *obj = NEW(c_badtostring)();
  cloneSet(obj);
  return obj;
}
void c_badtostring::cloneSet(c_badtostring *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_badtostring::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_badtostring::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_badtostring::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_badtostring$os_get(const char *s) {
  return c_badtostring::os_get(s, -1);
}
Variant &cw_badtostring$os_lval(const char *s) {
  return c_badtostring::os_lval(s, -1);
}
Variant cw_badtostring$os_constant(const char *s) {
  return c_badtostring::os_constant(s);
}
Variant cw_badtostring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_badtostring::os_invoke(c, s, params, -1, fatal);
}
void c_badtostring::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_004.php line 21 */
String c_badtostring::t___tostring() {
  INSTANCE_METHOD_INJECTION(badToString, badToString::__toString);
  return toString(0LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_004.php line 2 */
void f_test_error_handler(CVarRef v_err_no, CVarRef v_err_msg, CVarRef v_filename, CVarRef v_linenum, CVarRef v_vars) {
  FUNCTION_INJECTION(test_error_handler);
  echo(LINE(3,concat5("Error: ", toString(v_err_no), " - ", toString(v_err_msg), "\n")));
} /* function */
Variant i_test_error_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6C9B684D62E12540LL, test_error_handler) {
    return (f_test_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_badtostring(CArrRef params, bool init /* = true */) {
  return Object(p_badtostring(NEW(c_badtostring)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$tostring_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/tostring_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$tostring_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  LINE(5,x_set_error_handler("test_error_handler"));
  LINE(6,x_error_reporting(toInt32(8191LL)));
  echo("Object with no __toString():\n");
  (v_obj = ((Object)(LINE(10,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  echo("Try 1:\n");
  LINE(12,x_printf(1, toString(v_obj)));
  LINE(13,x_printf(1, "\n"));
  echo("\nTry 2:\n");
  LINE(16,x_printf(1, concat(toString(v_obj), "\n")));
  echo("\n\nObject with bad __toString():\n");
  (v_obj = ((Object)(LINE(25,p_badtostring(p_badtostring(NEWOBJ(c_badtostring)())->create())))));
  echo("Try 1:\n");
  LINE(27,x_printf(1, toString(v_obj)));
  LINE(28,x_printf(1, "\n"));
  echo("\nTry 2:\n");
  LINE(31,x_printf(1, concat(toString(v_obj), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
