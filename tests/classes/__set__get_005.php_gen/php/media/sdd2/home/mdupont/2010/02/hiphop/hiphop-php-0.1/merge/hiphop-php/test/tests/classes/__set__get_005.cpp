
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 23 */
Variant c_autogen::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_autogen::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_autogen::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_autogen::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_autogen::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_autogen::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_autogen::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_autogen::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(autogen)
ObjectData *c_autogen::cloneImpl() {
  c_autogen *obj = NEW(c_autogen)();
  cloneSet(obj);
  return obj;
}
void c_autogen::cloneSet(c_autogen *clone) {
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_autogen::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_autogen::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_autogen::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_autogen$os_get(const char *s) {
  return c_autogen::os_get(s, -1);
}
Variant &cw_autogen$os_lval(const char *s) {
  return c_autogen::os_lval(s, -1);
}
Variant cw_autogen$os_constant(const char *s) {
  return c_autogen::os_constant(s);
}
Variant cw_autogen$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_autogen::os_invoke(c, s, params, -1, fatal);
}
void c_autogen::init() {
  m_x = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 27 */
Variant c_autogen::t___get(Variant v_name) {
  INSTANCE_METHOD_INJECTION(AutoGen, AutoGen::__get);
  echo("AutoGen::__get\n");
  if (!(isset(m_x, v_name))) {
    m_x.set(v_name, (((Object)(LINE(30,p_test(p_test(NEWOBJ(c_test)())->create()))))));
  }
  return m_x.rvalAt(v_name);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 27 */
Variant &c_autogen::___lval(Variant v_name) {
  INSTANCE_METHOD_INJECTION(AutoGen, AutoGen::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_name);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 35 */
Variant c_autogen::t___set(Variant v_name, Variant v_val) {
  INSTANCE_METHOD_INJECTION(AutoGen, AutoGen::__set);
  echo("AutoGen::__set\n");
  m_x.set(v_name, (v_val));
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 2 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  m_x = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 6 */
Variant c_test::t___get(Variant v_name) {
  INSTANCE_METHOD_INJECTION(Test, Test::__get);
  echo("Test::__get\n");
  if (isset(m_x, v_name)) {
    return m_x.rvalAt(v_name);
  }
  else {
    return null;
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 6 */
Variant &c_test::___lval(Variant v_name) {
  INSTANCE_METHOD_INJECTION(Test, Test::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_name);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php line 17 */
Variant c_test::t___set(Variant v_name, Variant v_val) {
  INSTANCE_METHOD_INJECTION(Test, Test::__set);
  echo("Test::__set\n");
  m_x.set(v_name, (v_val));
  return null;
} /* function */
Object co_autogen(CArrRef params, bool init /* = true */) {
  return Object(p_autogen(NEW(c_autogen)())->dynCreate(params, init));
}
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$__set__get_005_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_005.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$__set__get_005_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = ((Object)(LINE(41,p_autogen(p_autogen(NEWOBJ(c_autogen)())->create())))));
  (lval(v_foo.o_lval("bar", 0x4A453601FBA86E5FLL)).o_lval("baz", 0x5BE8348359F05B37LL) = "Check");
  LINE(44,x_var_dump(1, v_foo.o_get("bar", 0x4A453601FBA86E5FLL)));
  LINE(45,x_var_dump(1, v_foo.o_get("bar", 0x4A453601FBA86E5FLL).o_get("baz", 0x5BE8348359F05B37LL)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
