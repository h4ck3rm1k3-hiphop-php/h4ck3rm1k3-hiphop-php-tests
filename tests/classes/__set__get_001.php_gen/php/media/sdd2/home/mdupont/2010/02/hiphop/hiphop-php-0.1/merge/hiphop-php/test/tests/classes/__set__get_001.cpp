
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_001.php line 2 */
Variant c_setter::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_setter::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_setter::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("n", m_n.isReferenced() ? ref(m_n) : m_n));
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_setter::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x117B8667E4662809LL, n, 1);
      break;
    case 2:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_setter::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_setter::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x117B8667E4662809LL, m_n,
                      n, 1);
      break;
    case 2:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_setter::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_setter::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(setter)
ObjectData *c_setter::cloneImpl() {
  c_setter *obj = NEW(c_setter)();
  cloneSet(obj);
  return obj;
}
void c_setter::cloneSet(c_setter *clone) {
  clone->m_n = m_n.isReferenced() ? ref(m_n) : m_n;
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_setter::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_setter::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_setter::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_setter$os_get(const char *s) {
  return c_setter::os_get(s, -1);
}
Variant &cw_setter$os_lval(const char *s) {
  return c_setter::os_lval(s, -1);
}
Variant cw_setter$os_constant(const char *s) {
  return c_setter::os_constant(s);
}
Variant cw_setter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_setter::os_invoke(c, s, params, -1, fatal);
}
void c_setter::init() {
  m_n = null;
  m_x = ScalarArrays::sa_[0];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_001.php line 6 */
Variant c_setter::t___get(Variant v_nm) {
  INSTANCE_METHOD_INJECTION(setter, setter::__get);
  Variant v_r;

  echo(LINE(7,concat3("Getting [", toString(v_nm), "]\n")));
  if (isset(m_x, v_nm)) {
    (v_r = m_x.rvalAt(v_nm));
    echo(LINE(11,concat3("Returning: ", toString(v_r), "\n")));
    return v_r;
  }
  else {
    echo("Nothing!\n");
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_001.php line 6 */
Variant &c_setter::___lval(Variant v_nm) {
  INSTANCE_METHOD_INJECTION(setter, setter::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_nm);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_001.php line 19 */
Variant c_setter::t___set(Variant v_nm, Variant v_val) {
  INSTANCE_METHOD_INJECTION(setter, setter::__set);
  echo(LINE(20,concat5("Setting [", toString(v_nm), "] to ", toString(v_val), "\n")));
  if (isset(m_x, v_nm)) {
    m_x.set(v_nm, (v_val));
    echo("OK!\n");
  }
  else {
    echo("Not OK!\n");
  }
  return null;
} /* function */
Object co_setter(CArrRef params, bool init /* = true */) {
  return Object(p_setter(NEW(c_setter)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$__set__get_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$__set__get_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = ((Object)(LINE(32,p_setter(p_setter(NEWOBJ(c_setter)())->create())))));
  (v_foo.o_lval("n", 0x117B8667E4662809LL) = 1LL);
  (v_foo.o_lval("a", 0x4292CEE227B9150ALL) = 100LL);
  lval(v_foo.o_lval("a", 0x4292CEE227B9150ALL))++;
  lval(v_foo.o_lval("z", 0x62A103F6518DE2B3LL))++;
  LINE(41,x_var_dump(1, v_foo));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
