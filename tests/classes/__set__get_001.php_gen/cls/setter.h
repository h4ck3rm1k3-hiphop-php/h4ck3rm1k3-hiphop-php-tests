
#ifndef __GENERATED_cls_setter_h__
#define __GENERATED_cls_setter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_001.php line 2 */
class c_setter : virtual public ObjectData {
  BEGIN_CLASS_MAP(setter)
  END_CLASS_MAP(setter)
  DECLARE_CLASS(setter, setter, ObjectData)
  void init();
  public: Variant m_n;
  public: Array m_x;
  public: Variant t___get(Variant v_nm);
  public: Variant &___lval(Variant v_nm);
  public: Variant t___set(Variant v_nm, Variant v_val);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_setter_h__
