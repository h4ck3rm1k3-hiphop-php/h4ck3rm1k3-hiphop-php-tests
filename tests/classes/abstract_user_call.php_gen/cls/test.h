
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__

#include <cls/test_base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_user_call.php line 8 */
class c_test : virtual public c_test_base {
  BEGIN_CLASS_MAP(test)
    PARENT_CLASS(test_base)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, test, test_base)
  void init();
  public: void t_func();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
