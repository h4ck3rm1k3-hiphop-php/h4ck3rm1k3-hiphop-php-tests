
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_user_call.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_user_call.php line 3 */
Variant c_test_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test_base::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test_base::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test_base::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test_base)
ObjectData *c_test_base::cloneImpl() {
  c_test_base *obj = NEW(c_test_base)();
  cloneSet(obj);
  return obj;
}
void c_test_base::cloneSet(c_test_base *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test_base$os_get(const char *s) {
  return c_test_base::os_get(s, -1);
}
Variant &cw_test_base$os_lval(const char *s) {
  return c_test_base::os_lval(s, -1);
}
Variant cw_test_base$os_constant(const char *s) {
  return c_test_base::os_constant(s);
}
Variant cw_test_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test_base::os_invoke(c, s, params, -1, fatal);
}
void c_test_base::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_user_call.php line 5 */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_user_call.php line 8 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_test_base::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_test_base::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  c_test_base::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  return c_test_base::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  return c_test_base::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test_base::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  return c_test_base::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_test_base::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  c_test_base::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5BB82607943AE415LL, func) {
        return (t_func(), null);
      }
      break;
    default:
      break;
  }
  return c_test_base::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5BB82607943AE415LL, func) {
        return (t_func(), null);
      }
      break;
    default:
      break;
  }
  return c_test_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  c_test_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_user_call.php line 10 */
void c_test::t_func() {
  INSTANCE_METHOD_INJECTION(test, test::func);
  echo("test::func()\n");
} /* function */
Object co_test_base(CArrRef params, bool init /* = true */) {
  return Object(p_test_base(NEW(c_test_base)())->dynCreate(params, init));
}
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$abstract_user_call_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_user_call.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$abstract_user_call_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);

  (v_o = ((Object)(LINE(16,p_test(p_test(NEWOBJ(c_test)())->create())))));
  LINE(18,v_o.o_invoke_few_args("func", 0x5BB82607943AE415LL, 0));
  LINE(20,x_call_user_func(1, Array(ArrayInit(2).set(0, v_o).set(1, "test_base::func").create())));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
