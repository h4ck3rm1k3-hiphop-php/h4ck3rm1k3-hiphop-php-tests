
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_001.php line 9 */
Variant c_square::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_square::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_square::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_square::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_square::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_square::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_square::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_square::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(square)
ObjectData *c_square::cloneImpl() {
  c_square *obj = NEW(c_square)();
  cloneSet(obj);
  return obj;
}
void c_square::cloneSet(c_square *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_square::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0576A414FD788F39LL, draw) {
        return (t_draw(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_square::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0576A414FD788F39LL, draw) {
        return (t_draw(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_square::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_square$os_get(const char *s) {
  return c_square::os_get(s, -1);
}
Variant &cw_square$os_lval(const char *s) {
  return c_square::os_lval(s, -1);
}
Variant cw_square$os_constant(const char *s) {
  return c_square::os_constant(s);
}
Variant cw_square$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_square::os_invoke(c, s, params, -1, fatal);
}
void c_square::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_001.php line 10 */
void c_square::t_draw() {
  INSTANCE_METHOD_INJECTION(Square, Square::draw);
  print("Square\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_001.php line 3 */
Variant c_circle::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_circle::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_circle::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_circle::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_circle::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_circle::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_circle::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_circle::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(circle)
ObjectData *c_circle::cloneImpl() {
  c_circle *obj = NEW(c_circle)();
  cloneSet(obj);
  return obj;
}
void c_circle::cloneSet(c_circle *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_circle::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0576A414FD788F39LL, draw) {
        return (t_draw(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_circle::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0576A414FD788F39LL, draw) {
        return (t_draw(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_circle::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_circle$os_get(const char *s) {
  return c_circle::os_get(s, -1);
}
Variant &cw_circle$os_lval(const char *s) {
  return c_circle::os_lval(s, -1);
}
Variant cw_circle$os_constant(const char *s) {
  return c_circle::os_constant(s);
}
Variant cw_circle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_circle::os_invoke(c, s, params, -1, fatal);
}
void c_circle::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_001.php line 4 */
void c_circle::t_draw() {
  INSTANCE_METHOD_INJECTION(Circle, Circle::draw);
  echo("Circle\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_001.php line 15 */
Variant f_shapefactorymethod(CStrRef v_shape) {
  FUNCTION_INJECTION(ShapeFactoryMethod);
  {
    Variant tmp2 = (v_shape);
    int tmp3 = -1;
    if (equal(tmp2, ("Circle"))) {
      tmp3 = 0;
    } else if (equal(tmp2, ("Square"))) {
      tmp3 = 1;
    }
    switch (tmp3) {
    case 0:
      {
        return ((Object)(LINE(18,p_circle(p_circle(NEWOBJ(c_circle)())->create()))));
      }
    case 1:
      {
        return ((Object)(LINE(20,p_square(p_square(NEWOBJ(c_square)())->create()))));
      }
    }
  }
  return null;
} /* function */
Object co_square(CArrRef params, bool init /* = true */) {
  return Object(p_square(NEW(c_square)())->dynCreate(params, init));
}
Object co_circle(CArrRef params, bool init /* = true */) {
  return Object(p_circle(NEW(c_circle)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$factory_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$factory_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  (assignCallTemp(eo_0, toObject(LINE(24,f_shapefactorymethod("Circle")))),eo_0.o_invoke_few_args("draw", 0x0576A414FD788F39LL, 0));
  (assignCallTemp(eo_1, toObject(LINE(25,f_shapefactorymethod("Square")))),eo_1.o_invoke_few_args("draw", 0x0576A414FD788F39LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
