
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 8 */
Variant c_child_php4::os_get(const char *s, int64 hash) {
  return c_base_php4::os_get(s, hash);
}
Variant &c_child_php4::os_lval(const char *s, int64 hash) {
  return c_base_php4::os_lval(s, hash);
}
void c_child_php4::o_get(ArrayElementVec &props) const {
  c_base_php4::o_get(props);
}
bool c_child_php4::o_exists(CStrRef s, int64 hash) const {
  return c_base_php4::o_exists(s, hash);
}
Variant c_child_php4::o_get(CStrRef s, int64 hash) {
  return c_base_php4::o_get(s, hash);
}
Variant c_child_php4::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base_php4::o_set(s, hash, v, forInit);
}
Variant &c_child_php4::o_lval(CStrRef s, int64 hash) {
  return c_base_php4::o_lval(s, hash);
}
Variant c_child_php4::os_constant(const char *s) {
  return c_base_php4::os_constant(s);
}
IMPLEMENT_CLASS(child_php4)
ObjectData *c_child_php4::create() {
  init();
  t_child_php4();
  return this;
}
ObjectData *c_child_php4::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_child_php4::cloneImpl() {
  c_child_php4 *obj = NEW(c_child_php4)();
  cloneSet(obj);
  return obj;
}
void c_child_php4::cloneSet(c_child_php4 *clone) {
  c_base_php4::cloneSet(clone);
}
Variant c_child_php4::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base_php4::o_invoke(s, params, hash, fatal);
}
Variant c_child_php4::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_base_php4::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_child_php4::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base_php4::os_invoke(c, s, params, hash, fatal);
}
Variant cw_child_php4$os_get(const char *s) {
  return c_child_php4::os_get(s, -1);
}
Variant &cw_child_php4$os_lval(const char *s) {
  return c_child_php4::os_lval(s, -1);
}
Variant cw_child_php4$os_constant(const char *s) {
  return c_child_php4::os_constant(s);
}
Variant cw_child_php4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_child_php4::os_invoke(c, s, params, -1, fatal);
}
void c_child_php4::init() {
  c_base_php4::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 9 */
void c_child_php4::t_child_php4() {
  INSTANCE_METHOD_INJECTION(Child_php4, Child_php4::Child_php4);
  bool oldInCtor = gasInCtor(true);
  LINE(10,x_var_dump(1, "Child constructor"));
  LINE(11,c_base_php4::t_base_php4());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 21 */
Variant c_child_php5::os_get(const char *s, int64 hash) {
  return c_base_php5::os_get(s, hash);
}
Variant &c_child_php5::os_lval(const char *s, int64 hash) {
  return c_base_php5::os_lval(s, hash);
}
void c_child_php5::o_get(ArrayElementVec &props) const {
  c_base_php5::o_get(props);
}
bool c_child_php5::o_exists(CStrRef s, int64 hash) const {
  return c_base_php5::o_exists(s, hash);
}
Variant c_child_php5::o_get(CStrRef s, int64 hash) {
  return c_base_php5::o_get(s, hash);
}
Variant c_child_php5::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base_php5::o_set(s, hash, v, forInit);
}
Variant &c_child_php5::o_lval(CStrRef s, int64 hash) {
  return c_base_php5::o_lval(s, hash);
}
Variant c_child_php5::os_constant(const char *s) {
  return c_base_php5::os_constant(s);
}
IMPLEMENT_CLASS(child_php5)
ObjectData *c_child_php5::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_child_php5::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_child_php5::cloneImpl() {
  c_child_php5 *obj = NEW(c_child_php5)();
  cloneSet(obj);
  return obj;
}
void c_child_php5::cloneSet(c_child_php5 *clone) {
  c_base_php5::cloneSet(clone);
}
Variant c_child_php5::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_base_php5::o_invoke(s, params, hash, fatal);
}
Variant c_child_php5::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_base_php5::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_child_php5::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base_php5::os_invoke(c, s, params, hash, fatal);
}
Variant cw_child_php5$os_get(const char *s) {
  return c_child_php5::os_get(s, -1);
}
Variant &cw_child_php5$os_lval(const char *s) {
  return c_child_php5::os_lval(s, -1);
}
Variant cw_child_php5$os_constant(const char *s) {
  return c_child_php5::os_constant(s);
}
Variant cw_child_php5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_child_php5::os_invoke(c, s, params, -1, fatal);
}
void c_child_php5::init() {
  c_base_php5::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 22 */
void c_child_php5::t___construct() {
  INSTANCE_METHOD_INJECTION(Child_php5, Child_php5::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(23,x_var_dump(1, "Child constructor"));
  LINE(24,c_base_php5::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 28 */
Variant c_child_mx1::os_get(const char *s, int64 hash) {
  return c_base_php4::os_get(s, hash);
}
Variant &c_child_mx1::os_lval(const char *s, int64 hash) {
  return c_base_php4::os_lval(s, hash);
}
void c_child_mx1::o_get(ArrayElementVec &props) const {
  c_base_php4::o_get(props);
}
bool c_child_mx1::o_exists(CStrRef s, int64 hash) const {
  return c_base_php4::o_exists(s, hash);
}
Variant c_child_mx1::o_get(CStrRef s, int64 hash) {
  return c_base_php4::o_get(s, hash);
}
Variant c_child_mx1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base_php4::o_set(s, hash, v, forInit);
}
Variant &c_child_mx1::o_lval(CStrRef s, int64 hash) {
  return c_base_php4::o_lval(s, hash);
}
Variant c_child_mx1::os_constant(const char *s) {
  return c_base_php4::os_constant(s);
}
IMPLEMENT_CLASS(child_mx1)
ObjectData *c_child_mx1::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_child_mx1::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_child_mx1::cloneImpl() {
  c_child_mx1 *obj = NEW(c_child_mx1)();
  cloneSet(obj);
  return obj;
}
void c_child_mx1::cloneSet(c_child_mx1 *clone) {
  c_base_php4::cloneSet(clone);
}
Variant c_child_mx1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base_php4::o_invoke(s, params, hash, fatal);
}
Variant c_child_mx1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_base_php4::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_child_mx1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base_php4::os_invoke(c, s, params, hash, fatal);
}
Variant cw_child_mx1$os_get(const char *s) {
  return c_child_mx1::os_get(s, -1);
}
Variant &cw_child_mx1$os_lval(const char *s) {
  return c_child_mx1::os_lval(s, -1);
}
Variant cw_child_mx1$os_constant(const char *s) {
  return c_child_mx1::os_constant(s);
}
Variant cw_child_mx1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_child_mx1::os_invoke(c, s, params, -1, fatal);
}
void c_child_mx1::init() {
  c_base_php4::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 29 */
void c_child_mx1::t___construct() {
  INSTANCE_METHOD_INJECTION(Child_mx1, Child_mx1::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(30,x_var_dump(1, "Child constructor"));
  LINE(31,c_base_php4::t_base_php4());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 35 */
Variant c_child_mx2::os_get(const char *s, int64 hash) {
  return c_base_php5::os_get(s, hash);
}
Variant &c_child_mx2::os_lval(const char *s, int64 hash) {
  return c_base_php5::os_lval(s, hash);
}
void c_child_mx2::o_get(ArrayElementVec &props) const {
  c_base_php5::o_get(props);
}
bool c_child_mx2::o_exists(CStrRef s, int64 hash) const {
  return c_base_php5::o_exists(s, hash);
}
Variant c_child_mx2::o_get(CStrRef s, int64 hash) {
  return c_base_php5::o_get(s, hash);
}
Variant c_child_mx2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base_php5::o_set(s, hash, v, forInit);
}
Variant &c_child_mx2::o_lval(CStrRef s, int64 hash) {
  return c_base_php5::o_lval(s, hash);
}
Variant c_child_mx2::os_constant(const char *s) {
  return c_base_php5::os_constant(s);
}
IMPLEMENT_CLASS(child_mx2)
ObjectData *c_child_mx2::create() {
  init();
  t_child_mx2();
  return this;
}
ObjectData *c_child_mx2::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_child_mx2::cloneImpl() {
  c_child_mx2 *obj = NEW(c_child_mx2)();
  cloneSet(obj);
  return obj;
}
void c_child_mx2::cloneSet(c_child_mx2 *clone) {
  c_base_php5::cloneSet(clone);
}
Variant c_child_mx2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_base_php5::o_invoke(s, params, hash, fatal);
}
Variant c_child_mx2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_base_php5::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_child_mx2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base_php5::os_invoke(c, s, params, hash, fatal);
}
Variant cw_child_mx2$os_get(const char *s) {
  return c_child_mx2::os_get(s, -1);
}
Variant &cw_child_mx2$os_lval(const char *s) {
  return c_child_mx2::os_lval(s, -1);
}
Variant cw_child_mx2$os_constant(const char *s) {
  return c_child_mx2::os_constant(s);
}
Variant cw_child_mx2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_child_mx2::os_invoke(c, s, params, -1, fatal);
}
void c_child_mx2::init() {
  c_base_php5::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 36 */
void c_child_mx2::t_child_mx2() {
  INSTANCE_METHOD_INJECTION(Child_mx2, Child_mx2::Child_mx2);
  bool oldInCtor = gasInCtor(true);
  LINE(37,x_var_dump(1, "Child constructor"));
  LINE(38,c_base_php5::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 2 */
Variant c_base_php4::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base_php4::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base_php4::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_base_php4::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base_php4::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_base_php4::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base_php4::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base_php4::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base_php4)
ObjectData *c_base_php4::create() {
  init();
  t_base_php4();
  return this;
}
ObjectData *c_base_php4::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_base_php4::cloneImpl() {
  c_base_php4 *obj = NEW(c_base_php4)();
  cloneSet(obj);
  return obj;
}
void c_base_php4::cloneSet(c_base_php4 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_base_php4::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base_php4::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base_php4::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base_php4$os_get(const char *s) {
  return c_base_php4::os_get(s, -1);
}
Variant &cw_base_php4$os_lval(const char *s) {
  return c_base_php4::os_lval(s, -1);
}
Variant cw_base_php4$os_constant(const char *s) {
  return c_base_php4::os_constant(s);
}
Variant cw_base_php4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base_php4::os_invoke(c, s, params, -1, fatal);
}
void c_base_php4::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 3 */
void c_base_php4::t_base_php4() {
  INSTANCE_METHOD_INJECTION(Base_php4, Base_php4::Base_php4);
  bool oldInCtor = gasInCtor(true);
  LINE(4,x_var_dump(1, "Base constructor"));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 15 */
Variant c_base_php5::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base_php5::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base_php5::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_base_php5::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base_php5::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_base_php5::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base_php5::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base_php5::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base_php5)
ObjectData *c_base_php5::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_base_php5::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_base_php5::cloneImpl() {
  c_base_php5 *obj = NEW(c_base_php5)();
  cloneSet(obj);
  return obj;
}
void c_base_php5::cloneSet(c_base_php5 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_base_php5::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base_php5::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base_php5::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base_php5$os_get(const char *s) {
  return c_base_php5::os_get(s, -1);
}
Variant &cw_base_php5$os_lval(const char *s) {
  return c_base_php5::os_lval(s, -1);
}
Variant cw_base_php5$os_constant(const char *s) {
  return c_base_php5::os_constant(s);
}
Variant cw_base_php5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base_php5::os_invoke(c, s, params, -1, fatal);
}
void c_base_php5::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 16 */
void c_base_php5::t___construct() {
  INSTANCE_METHOD_INJECTION(Base_php5, Base_php5::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(17,x_var_dump(1, "Base constructor"));
  gasInCtor(oldInCtor);
} /* function */
Object co_child_php4(CArrRef params, bool init /* = true */) {
  return Object(p_child_php4(NEW(c_child_php4)())->dynCreate(params, init));
}
Object co_child_php5(CArrRef params, bool init /* = true */) {
  return Object(p_child_php5(NEW(c_child_php5)())->dynCreate(params, init));
}
Object co_child_mx1(CArrRef params, bool init /* = true */) {
  return Object(p_child_mx1(NEW(c_child_mx1)())->dynCreate(params, init));
}
Object co_child_mx2(CArrRef params, bool init /* = true */) {
  return Object(p_child_mx2(NEW(c_child_mx2)())->dynCreate(params, init));
}
Object co_base_php4(CArrRef params, bool init /* = true */) {
  return Object(p_base_php4(NEW(c_base_php4)())->dynCreate(params, init));
}
Object co_base_php5(CArrRef params, bool init /* = true */) {
  return Object(p_base_php5(NEW(c_base_php5)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$inheritance_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$inheritance_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c4") : g->GV(c4);
  Variant &v_c5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c5") : g->GV(c5);
  Variant &v_cm __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cm") : g->GV(cm);

  echo("### PHP 4 style\n");
  (v_c4 = ((Object)(LINE(43,p_child_php4(p_child_php4(NEWOBJ(c_child_php4)())->create())))));
  echo("### PHP 5 style\n");
  (v_c5 = ((Object)(LINE(46,p_child_php5(p_child_php5(NEWOBJ(c_child_php5)())->create())))));
  echo("### Mixed style 1\n");
  (v_cm = ((Object)(LINE(49,p_child_mx1(p_child_mx1(NEWOBJ(c_child_mx1)())->create())))));
  echo("### Mixed style 2\n");
  (v_cm = ((Object)(LINE(52,p_child_mx2(p_child_mx2(NEWOBJ(c_child_mx2)())->create())))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
