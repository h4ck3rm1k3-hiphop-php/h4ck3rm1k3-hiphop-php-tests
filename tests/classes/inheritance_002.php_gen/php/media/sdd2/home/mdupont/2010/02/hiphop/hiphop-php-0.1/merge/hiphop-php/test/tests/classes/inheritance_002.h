
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_inheritance_002_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_inheritance_002_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.fw.h>

// Declarations
#include <cls/child_php4.h>
#include <cls/child_php5.h>
#include <cls/child_mx1.h>
#include <cls/child_mx2.h>
#include <cls/base_php4.h>
#include <cls/base_php5.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$inheritance_002_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_child_php4(CArrRef params, bool init = true);
Object co_child_php5(CArrRef params, bool init = true);
Object co_child_mx1(CArrRef params, bool init = true);
Object co_child_mx2(CArrRef params, bool init = true);
Object co_base_php4(CArrRef params, bool init = true);
Object co_base_php5(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_inheritance_002_h__
