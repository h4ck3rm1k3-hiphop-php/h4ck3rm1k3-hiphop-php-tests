
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_child_php4(CArrRef params, bool init = true);
Variant cw_child_php4$os_get(const char *s);
Variant &cw_child_php4$os_lval(const char *s);
Variant cw_child_php4$os_constant(const char *s);
Variant cw_child_php4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_child_php5(CArrRef params, bool init = true);
Variant cw_child_php5$os_get(const char *s);
Variant &cw_child_php5$os_lval(const char *s);
Variant cw_child_php5$os_constant(const char *s);
Variant cw_child_php5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_child_mx1(CArrRef params, bool init = true);
Variant cw_child_mx1$os_get(const char *s);
Variant &cw_child_mx1$os_lval(const char *s);
Variant cw_child_mx1$os_constant(const char *s);
Variant cw_child_mx1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_child_mx2(CArrRef params, bool init = true);
Variant cw_child_mx2$os_get(const char *s);
Variant &cw_child_mx2$os_lval(const char *s);
Variant cw_child_mx2$os_constant(const char *s);
Variant cw_child_mx2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_base_php4(CArrRef params, bool init = true);
Variant cw_base_php4$os_get(const char *s);
Variant &cw_base_php4$os_lval(const char *s);
Variant cw_base_php4$os_constant(const char *s);
Variant cw_base_php4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_base_php5(CArrRef params, bool init = true);
Variant cw_base_php5$os_get(const char *s);
Variant &cw_base_php5$os_lval(const char *s);
Variant cw_base_php5$os_constant(const char *s);
Variant cw_base_php5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_CREATE_OBJECT(0x30ED05ED73783A70LL, child_mx1);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x47D9CA397DB6BB32LL, child_php5);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x406B7258A2098BC4LL, child_mx2);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x3A6F9E369AA580D5LL, base_php4);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x012617C44EF4DE3ALL, base_php5);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x1CE9F0D4AD371B0FLL, child_php4);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x30ED05ED73783A70LL, child_mx1);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x47D9CA397DB6BB32LL, child_php5);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x406B7258A2098BC4LL, child_mx2);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x3A6F9E369AA580D5LL, base_php4);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x012617C44EF4DE3ALL, base_php5);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x1CE9F0D4AD371B0FLL, child_php4);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x30ED05ED73783A70LL, child_mx1);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x47D9CA397DB6BB32LL, child_php5);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x406B7258A2098BC4LL, child_mx2);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x3A6F9E369AA580D5LL, base_php4);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x012617C44EF4DE3ALL, base_php5);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x1CE9F0D4AD371B0FLL, child_php4);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x30ED05ED73783A70LL, child_mx1);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x47D9CA397DB6BB32LL, child_php5);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x406B7258A2098BC4LL, child_mx2);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x3A6F9E369AA580D5LL, base_php4);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x012617C44EF4DE3ALL, base_php5);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x1CE9F0D4AD371B0FLL, child_php4);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x30ED05ED73783A70LL, child_mx1);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x47D9CA397DB6BB32LL, child_php5);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x406B7258A2098BC4LL, child_mx2);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x3A6F9E369AA580D5LL, base_php4);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x012617C44EF4DE3ALL, base_php5);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x1CE9F0D4AD371B0FLL, child_php4);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
