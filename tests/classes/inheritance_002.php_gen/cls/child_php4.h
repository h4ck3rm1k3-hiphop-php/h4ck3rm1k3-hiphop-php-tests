
#ifndef __GENERATED_cls_child_php4_h__
#define __GENERATED_cls_child_php4_h__

#include <cls/base_php4.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 8 */
class c_child_php4 : virtual public c_base_php4 {
  BEGIN_CLASS_MAP(child_php4)
    PARENT_CLASS(base_php4)
  END_CLASS_MAP(child_php4)
  DECLARE_CLASS(child_php4, Child_php4, base_php4)
  void init();
  public: void t_child_php4();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_child_php4_h__
