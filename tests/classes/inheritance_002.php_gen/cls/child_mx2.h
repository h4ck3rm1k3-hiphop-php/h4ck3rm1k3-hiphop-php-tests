
#ifndef __GENERATED_cls_child_mx2_h__
#define __GENERATED_cls_child_mx2_h__

#include <cls/base_php5.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 35 */
class c_child_mx2 : virtual public c_base_php5 {
  BEGIN_CLASS_MAP(child_mx2)
    PARENT_CLASS(base_php5)
  END_CLASS_MAP(child_mx2)
  DECLARE_CLASS(child_mx2, Child_mx2, base_php5)
  void init();
  public: void t_child_mx2();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_child_mx2_h__
