
#ifndef __GENERATED_cls_child_mx1_h__
#define __GENERATED_cls_child_mx1_h__

#include <cls/base_php4.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_002.php line 28 */
class c_child_mx1 : virtual public c_base_php4 {
  BEGIN_CLASS_MAP(child_mx1)
    PARENT_CLASS(base_php4)
  END_CLASS_MAP(child_mx1)
  DECLARE_CLASS(child_mx1, Child_mx1, base_php4)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_child_mx1_h__
