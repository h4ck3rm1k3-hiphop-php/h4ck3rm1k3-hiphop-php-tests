
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_UNDEFINED = "UNDEFINED";
const int64 k_DEFINED = 1234LL;

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_001.php line 7 */
const int64 q_c_c1 = 1LL;
const double q_c_c2 = 1.5;
const int64 q_c_c3 = 1LL;
const double q_c_c4 = 1.5;
const int64 q_c_c5 = -1LL;
const double q_c_c6 = -1.5;
const int q_c_c7 = 15;
const StaticString q_c_c9 = "C";
const StaticString q_c_c10 = "";
const StaticString q_c_c11 = "";
const StaticString q_c_c15 = "hello1";
const StaticString q_c_c16 = "hello2";
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_RETURN(0x7CCECB862C9769C0LL, g->q_c_c13, c13);
      break;
    case 3:
      HASH_RETURN(0x49B383BE1D5E5143LL, q_c_c6, c6);
      HASH_RETURN(0x6A4ECD5D40D441C3LL, q_c_c9, c9);
      HASH_RETURN(0x6596C768A6019983LL, q_c_c11, c11);
      break;
    case 8:
      HASH_RETURN(0x2B08C15EC00B79C8LL, q_c_c16, c16);
      break;
    case 11:
      HASH_RETURN(0x23401BF3A8F8E58BLL, q_c_c1, c1);
      break;
    case 16:
      HASH_RETURN(0x062075DC196CACD0LL, q_c_c2, c2);
      break;
    case 18:
      HASH_RETURN(0x0284BB0BD682CFD2LL, g->q_c_c0, c0);
      break;
    case 27:
      HASH_RETURN(0x7A1D64311618261BLL, q_c_c10, c10);
      break;
    case 44:
      HASH_RETURN(0x530548FA11FEE26CLL, g->q_c_c17, c17);
      break;
    case 45:
      HASH_RETURN(0x45DBC56CD443526DLL, q_c_c4, c4);
      break;
    case 46:
      HASH_RETURN(0x365DE53E91F259AELL, q_c_c3, c3);
      HASH_RETURN(0x27E3B8D774874C6ELL, q_c_c7, c7);
      break;
    case 48:
      HASH_RETURN(0x29C3FEF001269630LL, q_c_c15, c15);
      break;
    case 57:
      HASH_RETURN(0x0DA563D57511DFB9LL, g->q_c_c18, c18);
      break;
    case 58:
      HASH_RETURN(0x01E81C1EAE43633ALL, g->q_c_c14, c14);
      break;
    case 62:
      HASH_RETURN(0x4410979CDBB2B87ELL, q_c_c5, c5);
      HASH_RETURN(0x4F938B26F96E04FELL, g->q_c_c8, c8);
      HASH_RETURN(0x7EB71ADF550746BELL, g->q_c_c12, c12);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
GlobalVariables *c_c::lazy_initializer(GlobalVariables *g) {
  if (!g->csf_c) {
    g->csf_c = true;
    g->q_c_c0 = k_UNDEFINED;
    g->q_c_c8 = get_source_filename("/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_001.php");
    g->q_c_c12 = 1234LL /* DEFINED */;
    g->q_c_c13 = get_global_variables()->k_DEFINED_TO_VAR;
    g->q_c_c14 = get_global_variables()->k_DEFINED_TO_UNDEF_VAR;
    g->q_c_c17 = "hello2" /* c::c16 */;
    g->q_c_c18 = "hello2" /* c::c17 */;
  }
  return g;
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_def __attribute__((__unused__)) = (variables != gVariables) ? variables->get("def") : g->GV(def);
  Variant &v_undef __attribute__((__unused__)) = (variables != gVariables) ? variables->get("undef") : g->GV(undef);

  ;
  (v_def = 456LL);
  LINE(4,g->declareConstant("DEFINED_TO_VAR", g->k_DEFINED_TO_VAR, v_def));
  LINE(5,g->declareConstant("DEFINED_TO_UNDEF_VAR", g->k_DEFINED_TO_UNDEF_VAR, v_undef));
  echo("\nAttempt to access various kinds of class constants:\n");
  LINE(32,x_var_dump(1, k_UNDEFINED/* c::c0 */));
  LINE(33,x_var_dump(1, 1LL /* c::c1 */));
  LINE(34,x_var_dump(1, 1.5 /* c::c2 */));
  LINE(35,x_var_dump(1, 1LL /* c::c3 */));
  LINE(36,x_var_dump(1, 1.5 /* c::c4 */));
  LINE(37,x_var_dump(1, -1LL /* c::c5 */));
  LINE(38,x_var_dump(1, -1.5 /* c::c6 */));
  LINE(39,x_var_dump(1, 15 /* c::c7 */));
  LINE(40,x_var_dump(1, get_source_filename("/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_001.php")/* c::c8 */));
  LINE(41,x_var_dump(1, "C" /* c::c9 */));
  LINE(42,x_var_dump(1, "" /* c::c10 */));
  LINE(43,x_var_dump(1, "" /* c::c11 */));
  LINE(44,x_var_dump(1, 1234LL /* c::c12 */));
  LINE(45,x_var_dump(1, get_global_variables()->k_DEFINED_TO_VAR/* c::c13 */));
  LINE(46,x_var_dump(1, get_global_variables()->k_DEFINED_TO_UNDEF_VAR/* c::c14 */));
  LINE(47,x_var_dump(1, "hello1" /* c::c15 */));
  LINE(48,x_var_dump(1, "hello2" /* c::c16 */));
  LINE(49,x_var_dump(1, "hello2" /* c::c17 */));
  LINE(50,x_var_dump(1, "hello2" /* c::c18 */));
  echo("\nExpecting fatal error:\n");
  LINE(53,x_var_dump(1, throw_fatal("unknown class constant c::c19")));
  echo("\nYou should not see this.");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
