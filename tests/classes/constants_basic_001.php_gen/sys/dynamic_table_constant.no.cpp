
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_UNDEFINED;
extern const int64 k_DEFINED;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 3:
      HASH_RETURN(0x1CB0FF5DE5382423LL, k_UNDEFINED, UNDEFINED);
      break;
    case 6:
      HASH_RETURN(0x13BE8A69DF038046LL, g->k_DEFINED_TO_VAR, DEFINED_TO_VAR);
      HASH_RETURN(0x113917968B14E78ELL, g->k_DEFINED_TO_UNDEF_VAR, DEFINED_TO_UNDEF_VAR);
      break;
    case 7:
      HASH_RETURN(0x7C0C145EFE0EBF1FLL, k_DEFINED, DEFINED);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
