
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_mix_2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_mix_2.php line 3 */
Variant c_pass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_pass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_pass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_pass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_pass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_pass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_pass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_pass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(pass)
ObjectData *c_pass::cloneImpl() {
  c_pass *obj = NEW(c_pass)();
  cloneSet(obj);
  return obj;
}
void c_pass::cloneSet(c_pass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_pass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_pass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pass$os_get(const char *s) {
  return c_pass::os_get(s, -1);
}
Variant &cw_pass$os_lval(const char *s) {
  return c_pass::os_lval(s, -1);
}
Variant cw_pass$os_constant(const char *s) {
  return c_pass::os_constant(s);
}
Variant cw_pass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pass::os_invoke(c, s, params, -1, fatal);
}
void c_pass::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_mix_2.php line 4 */
void c_pass::t_show() {
  INSTANCE_METHOD_INJECTION(pass, pass::show);
  echo("Call to function pass::show()\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_mix_2.php line 9 */
Variant c_fail::os_get(const char *s, int64 hash) {
  return c_pass::os_get(s, hash);
}
Variant &c_fail::os_lval(const char *s, int64 hash) {
  return c_pass::os_lval(s, hash);
}
void c_fail::o_get(ArrayElementVec &props) const {
  c_pass::o_get(props);
}
bool c_fail::o_exists(CStrRef s, int64 hash) const {
  return c_pass::o_exists(s, hash);
}
Variant c_fail::o_get(CStrRef s, int64 hash) {
  return c_pass::o_get(s, hash);
}
Variant c_fail::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_pass::o_set(s, hash, v, forInit);
}
Variant &c_fail::o_lval(CStrRef s, int64 hash) {
  return c_pass::o_lval(s, hash);
}
Variant c_fail::os_constant(const char *s) {
  return c_pass::os_constant(s);
}
IMPLEMENT_CLASS(fail)
ObjectData *c_fail::cloneImpl() {
  c_fail *obj = NEW(c_fail)();
  cloneSet(obj);
  return obj;
}
void c_fail::cloneSet(c_fail *clone) {
  c_pass::cloneSet(clone);
}
Variant c_fail::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (ti_show(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_pass::o_invoke(s, params, hash, fatal);
}
Variant c_fail::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (ti_show(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_pass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fail::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (ti_show(c), null);
      }
      break;
    default:
      break;
  }
  return c_pass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fail$os_get(const char *s) {
  return c_fail::os_get(s, -1);
}
Variant &cw_fail$os_lval(const char *s) {
  return c_fail::os_lval(s, -1);
}
Variant cw_fail$os_constant(const char *s) {
  return c_fail::os_constant(s);
}
Variant cw_fail$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fail::os_invoke(c, s, params, -1, fatal);
}
void c_fail::init() {
  c_pass::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_mix_2.php line 10 */
void c_fail::ti_show(const char* cls) {
  STATIC_METHOD_INJECTION(fail, fail::show);
  echo("Call to function fail::show()\n");
} /* function */
Object co_pass(CArrRef params, bool init /* = true */) {
  return Object(p_pass(NEW(c_pass)())->dynCreate(params, init));
}
Object co_fail(CArrRef params, bool init /* = true */) {
  return Object(p_fail(NEW(c_fail)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$static_mix_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_mix_2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$static_mix_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_t = ((Object)(LINE(15,p_pass(p_pass(NEWOBJ(c_pass)())->create())))));
  LINE(16,v_t.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(17,c_fail::t_show());
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
