
#ifndef __GENERATED_cls_fail_h__
#define __GENERATED_cls_fail_h__

#include <cls/pass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_mix_2.php line 9 */
class c_fail : virtual public c_pass {
  BEGIN_CLASS_MAP(fail)
    PARENT_CLASS(pass)
  END_CLASS_MAP(fail)
  DECLARE_CLASS(fail, fail, pass)
  void init();
  public: static void ti_show(const char* cls);
  public: static void t_show() { ti_show("fail"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fail_h__
