
#ifndef __GENERATED_cls_second_h__
#define __GENERATED_cls_second_h__

#include <cls/first.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006.php line 14 */
class c_second : virtual public c_first {
  BEGIN_CLASS_MAP(second)
    PARENT_CLASS(first)
  END_CLASS_MAP(second)
  DECLARE_CLASS(second, second, first)
  void init();
  public: static void t_do_show() { ti_do_show("second"); }
  public: static void t_show() { ti_show("second"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_second_h__
