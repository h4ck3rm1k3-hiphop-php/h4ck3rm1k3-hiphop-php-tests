
#ifndef __GENERATED_cls_third_h__
#define __GENERATED_cls_third_h__

#include <cls/second.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006.php line 19 */
class c_third : virtual public c_second {
  BEGIN_CLASS_MAP(third)
    PARENT_CLASS(first)
    PARENT_CLASS(second)
  END_CLASS_MAP(third)
  DECLARE_CLASS(third, third, second)
  void init();
  public: static void t_do_show() { ti_do_show("third"); }
  public: static void t_show() { ti_show("third"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_third_h__
