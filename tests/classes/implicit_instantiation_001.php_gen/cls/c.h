
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/implicit_instantiation_001.php line 2 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: Variant m_boolFalse;
  public: Variant m_emptyString;
  public: Variant m_null;
  public: Variant m_boolTrue;
  public: Variant m_nonEmptyString;
  public: Variant m_intZero;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
