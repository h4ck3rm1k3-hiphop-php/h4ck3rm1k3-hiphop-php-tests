
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/implicit_instantiation_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/implicit_instantiation_001.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("boolFalse", m_boolFalse.isReferenced() ? ref(m_boolFalse) : m_boolFalse));
  props.push_back(NEW(ArrayElement)("emptyString", m_emptyString.isReferenced() ? ref(m_emptyString) : m_emptyString));
  props.push_back(NEW(ArrayElement)("null", m_null.isReferenced() ? ref(m_null) : m_null));
  props.push_back(NEW(ArrayElement)("boolTrue", m_boolTrue.isReferenced() ? ref(m_boolTrue) : m_boolTrue));
  props.push_back(NEW(ArrayElement)("nonEmptyString", m_nonEmptyString.isReferenced() ? ref(m_nonEmptyString) : m_nonEmptyString));
  props.push_back(NEW(ArrayElement)("intZero", m_intZero.isReferenced() ? ref(m_intZero) : m_intZero));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x372356B19DCB0F80LL, boolFalse, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x33482262955683A2LL, boolTrue, 8);
      break;
    case 3:
      HASH_EXISTS_STRING(0x3724F1D579A43233LL, intZero, 7);
      break;
    case 6:
      HASH_EXISTS_STRING(0x4A7AD45D44133B96LL, nonEmptyString, 14);
      break;
    case 12:
      HASH_EXISTS_STRING(0x510C79EAA6637A5CLL, emptyString, 11);
      HASH_EXISTS_STRING(0x2FF6F3DFF927FF2CLL, null, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x372356B19DCB0F80LL, m_boolFalse,
                         boolFalse, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x33482262955683A2LL, m_boolTrue,
                         boolTrue, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x3724F1D579A43233LL, m_intZero,
                         intZero, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x4A7AD45D44133B96LL, m_nonEmptyString,
                         nonEmptyString, 14);
      break;
    case 12:
      HASH_RETURN_STRING(0x510C79EAA6637A5CLL, m_emptyString,
                         emptyString, 11);
      HASH_RETURN_STRING(0x2FF6F3DFF927FF2CLL, m_null,
                         null, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x372356B19DCB0F80LL, m_boolFalse,
                      boolFalse, 9);
      break;
    case 2:
      HASH_SET_STRING(0x33482262955683A2LL, m_boolTrue,
                      boolTrue, 8);
      break;
    case 3:
      HASH_SET_STRING(0x3724F1D579A43233LL, m_intZero,
                      intZero, 7);
      break;
    case 6:
      HASH_SET_STRING(0x4A7AD45D44133B96LL, m_nonEmptyString,
                      nonEmptyString, 14);
      break;
    case 12:
      HASH_SET_STRING(0x510C79EAA6637A5CLL, m_emptyString,
                      emptyString, 11);
      HASH_SET_STRING(0x2FF6F3DFF927FF2CLL, m_null,
                      null, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x372356B19DCB0F80LL, m_boolFalse,
                         boolFalse, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x33482262955683A2LL, m_boolTrue,
                         boolTrue, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x3724F1D579A43233LL, m_intZero,
                         intZero, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x4A7AD45D44133B96LL, m_nonEmptyString,
                         nonEmptyString, 14);
      break;
    case 12:
      HASH_RETURN_STRING(0x510C79EAA6637A5CLL, m_emptyString,
                         emptyString, 11);
      HASH_RETURN_STRING(0x2FF6F3DFF927FF2CLL, m_null,
                         null, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_boolFalse = m_boolFalse.isReferenced() ? ref(m_boolFalse) : m_boolFalse;
  clone->m_emptyString = m_emptyString.isReferenced() ? ref(m_emptyString) : m_emptyString;
  clone->m_null = m_null.isReferenced() ? ref(m_null) : m_null;
  clone->m_boolTrue = m_boolTrue.isReferenced() ? ref(m_boolTrue) : m_boolTrue;
  clone->m_nonEmptyString = m_nonEmptyString.isReferenced() ? ref(m_nonEmptyString) : m_nonEmptyString;
  clone->m_intZero = m_intZero.isReferenced() ? ref(m_intZero) : m_intZero;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_boolFalse = false;
  m_emptyString = "";
  m_null = null;
  m_boolTrue = true;
  m_nonEmptyString = "hello";
  m_intZero = 0LL;
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$implicit_instantiation_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/implicit_instantiation_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$implicit_instantiation_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);

  (v_c = ((Object)(LINE(14,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_c.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_name = iter3->first();
      {
        echo(LINE(16,concat3("\n\n---( $c->", toString(v_name), " )---")));
        echo("\n  --> Attempting implicit conversion to object using increment...\n");
        lval(lval(v_c.o_lval(toString(v_name), -1LL)).o_lval("prop", 0x4EA429E0DCC990B7LL))++;
        (v_c.o_lval(toString(v_name), -1LL) = v_value);
        echo("\n  --> Attempting implicit conversion to object using assignment...\n");
        (lval(v_c.o_lval(toString(v_name), -1LL)).o_lval("prop", 0x4EA429E0DCC990B7LL) = "Implicit instantiation!");
        (v_c.o_lval(toString(v_name), -1LL) = v_value);
        echo("\n  --> Attempting implicit conversion to object using combined assignment...\n");
        concat_assign(lval(v_c.o_lval(toString(v_name), -1LL)).o_lval("prop", 0x4EA429E0DCC990B7LL), " Implicit instantiation!");
      }
    }
  }
  echo("\n\n\n --> Resulting object:");
  LINE(30,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
