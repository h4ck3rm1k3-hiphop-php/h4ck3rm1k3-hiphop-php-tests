
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_K = "nasty";

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_006.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x4292CEE227B9150ALL, g->s_c_DupIda,
                  a);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x00F31DE90234EA81LL, g->q_c_X, X);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
}
void csi_c() {
  c_c::os_static_initializer();
}
GlobalVariables *c_c::lazy_initializer(GlobalVariables *g) {
  if (!g->csf_c) {
    g->csf_c = true;
    g->q_c_X = "hello" /* e::A */;
    g->s_c_DupIda = Array(ArrayInit(2).set(0, "nasty" /* K */, throw_fatal("unknown class constant d::V"), 0x515A99F6D2901BA0LL).set(1, "hello" /* e::A */, "nasty" /* K */, 0x238948F5E6F62137LL).create());
  }
  return g;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_006.php line 10 */
const StaticString q_e_A = "hello";
Variant c_e::os_get(const char *s, int64 hash) {
  return c_d::os_get(s, hash);
}
Variant &c_e::os_lval(const char *s, int64 hash) {
  return c_d::os_lval(s, hash);
}
void c_e::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  c_d::o_get(props);
}
bool c_e::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_d::o_exists(s, hash);
}
Variant c_e::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_d::o_get(s, hash);
}
Variant c_e::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_d::o_set(s, hash, v, forInit);
}
Variant &c_e::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_d::o_lval(s, hash);
}
Variant c_e::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x4F38A775A0938899LL, q_e_A, A);
      break;
    default:
      break;
  }
  return c_d::os_constant(s);
}
IMPLEMENT_CLASS(e)
ObjectData *c_e::cloneImpl() {
  c_e *obj = NEW(c_e)();
  cloneSet(obj);
  return obj;
}
void c_e::cloneSet(c_e *clone) {
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  c_d::cloneSet(clone);
}
Variant c_e::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_d::o_invoke(s, params, hash, fatal);
}
Variant c_e::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_d::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_e::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_d::os_invoke(c, s, params, hash, fatal);
}
Variant cw_e$os_get(const char *s) {
  return c_e::os_get(s, -1);
}
Variant &cw_e$os_lval(const char *s) {
  return c_e::os_lval(s, -1);
}
Variant cw_e$os_constant(const char *s) {
  return c_e::os_constant(s);
}
Variant cw_e$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_e::os_invoke(c, s, params, -1, fatal);
}
void c_e::init() {
  c_d::init();
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_e(CArrRef params, bool init /* = true */) {
  return Object(p_e(NEW(c_e)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  f_eval("class D extends C { const V = 'test'; }");
  ;
  LINE(17,x_var_dump(6, "hello" /* c::X */, Array(ArrayInit(5).set(0, c_c::lazy_initializer(g)->s_c_DupIda).set(1, throw_fatal("unknown class constant d::X")).set(2, throw_fatal("unknown class d")).set(3, throw_fatal("unknown class constant e::X")).set(4, g->s_e_DupIda).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
