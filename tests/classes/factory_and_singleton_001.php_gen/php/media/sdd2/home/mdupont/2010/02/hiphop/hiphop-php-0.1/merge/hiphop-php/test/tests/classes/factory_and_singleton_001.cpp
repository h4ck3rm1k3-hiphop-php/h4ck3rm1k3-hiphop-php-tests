
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 2 */
Variant c_test::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 3) {
    case 1:
      HASH_RETURN(0x2072FF3918A89F3DLL, g->s_test_DupIdcnt,
                  cnt);
      break;
    case 3:
      HASH_RETURN(0x3A1DC49AA6CFEA13LL, g->s_test_DupIdtest,
                  test);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x3A1DC49AA6CFEA13LL, g->s_test_DupIdtest,
                  test);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::create(Variant v_x) {
  init();
  t___construct(v_x);
  return this;
}
ObjectData *c_test::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_test::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
void c_test::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  m_x = null;
}
void c_test::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_test_DupIdtest = null;
  g->s_test_DupIdcnt = 0LL;
}
void csi_test() {
  c_test::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 8 */
Variant c_test::ti_factory(const char* cls, int64 v_x) {
  STATIC_METHOD_INJECTION(test, test::factory);
  DECLARE_GLOBAL_VARIABLES(g);
  if (toBoolean(g->s_test_DupIdtest)) {
    return g->s_test_DupIdtest;
  }
  else {
    (g->s_test_DupIdtest = ((Object)(LINE(12,p_test(p_test(NEWOBJ(c_test)())->create(v_x))))));
    return g->s_test_DupIdtest;
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 17 */
void c_test::t___construct(Variant v_x) {
  INSTANCE_METHOD_INJECTION(test, test::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_test_DupIdcnt++;
  (m_x = v_x);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 22 */
void c_test::ti_destroy(const char* cls) {
  STATIC_METHOD_INJECTION(test, test::destroy);
  DECLARE_GLOBAL_VARIABLES(g);
  (g->s_test_DupIdtest = null);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 26 */
Variant c_test::t___destruct() {
  INSTANCE_METHOD_INJECTION(test, test::__destruct);
  setInDtor();
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_test_DupIdcnt--;
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 30 */
Variant c_test::t_get() {
  INSTANCE_METHOD_INJECTION(test, test::get);
  return m_x;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 34 */
Variant c_test::ti_getx(const char* cls) {
  STATIC_METHOD_INJECTION(test, test::getX);
  DECLARE_GLOBAL_VARIABLES(g);
  if (toBoolean(g->s_test_DupIdtest)) {
    return g->s_test_DupIdtest.o_get("x", 0x04BFC205E59FA416LL);
  }
  else {
    return null;
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 42 */
int64 c_test::ti_count(const char* cls) {
  STATIC_METHOD_INJECTION(test, test::count);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->s_test_DupIdcnt;
} /* function */
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$factory_and_singleton_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$factory_and_singleton_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  echo("Access static members\n");
  LINE(48,x_var_dump(1, c_test::t_getx()));
  LINE(49,x_var_dump(1, c_test::t_count()));
  echo("Create x and y\n");
  (v_x = LINE(52,c_test::t_factory(1LL)));
  (v_y = LINE(53,c_test::t_factory(2LL)));
  LINE(54,x_var_dump(1, c_test::t_getx()));
  LINE(55,x_var_dump(1, c_test::t_count()));
  LINE(56,x_var_dump(1, v_x.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 0)));
  LINE(57,x_var_dump(1, v_y.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 0)));
  echo("Destruct x\n");
  setNull(v_x);
  LINE(61,x_var_dump(1, c_test::t_getx()));
  LINE(62,x_var_dump(1, c_test::t_count()));
  LINE(63,x_var_dump(1, v_y.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 0)));
  echo("Destruct y\n");
  setNull(v_y);
  LINE(67,x_var_dump(1, c_test::t_getx()));
  LINE(68,x_var_dump(1, c_test::t_count()));
  echo("Destruct static\n");
  LINE(71,c_test::t_destroy());
  LINE(72,x_var_dump(1, c_test::t_getx()));
  LINE(73,x_var_dump(1, c_test::t_count()));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
