
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/factory_and_singleton_001.php line 2 */
class c_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(test)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, test, ObjectData)
  void init();
  public: Variant m_x;
  public: virtual void destruct();
  public: static Variant ti_factory(const char* cls, int64 v_x);
  public: void t___construct(Variant v_x);
  public: ObjectData *create(Variant v_x);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static void ti_destroy(const char* cls);
  public: Variant t___destruct();
  public: Variant t_get();
  public: static Variant ti_getx(const char* cls);
  public: static int64 ti_count(const char* cls);
  public: static void t_destroy() { ti_destroy("test"); }
  public: static Variant t_factory(int64 v_x) { return ti_factory("test", v_x); }
  public: static int64 t_count() { return ti_count("test"); }
  public: static Variant t_getx() { return ti_getx("test"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
