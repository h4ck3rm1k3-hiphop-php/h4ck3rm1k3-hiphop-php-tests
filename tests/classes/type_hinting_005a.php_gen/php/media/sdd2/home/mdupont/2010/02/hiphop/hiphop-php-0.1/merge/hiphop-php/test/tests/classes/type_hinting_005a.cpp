
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 2 */
void c_c::t_f(CArrRef v_a) {
  INSTANCE_METHOD_INJECTION(C, C::f);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 5 */
Variant c_d1::os_get(const char *s, int64 hash) {
  return c_c::os_get(s, hash);
}
Variant &c_d1::os_lval(const char *s, int64 hash) {
  return c_c::os_lval(s, hash);
}
void c_d1::o_get(ArrayElementVec &props) const {
  c_c::o_get(props);
}
bool c_d1::o_exists(CStrRef s, int64 hash) const {
  return c_c::o_exists(s, hash);
}
Variant c_d1::o_get(CStrRef s, int64 hash) {
  return c_c::o_get(s, hash);
}
Variant c_d1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_c::o_set(s, hash, v, forInit);
}
Variant &c_d1::o_lval(CStrRef s, int64 hash) {
  return c_c::o_lval(s, hash);
}
Variant c_d1::os_constant(const char *s) {
  return c_c::os_constant(s);
}
IMPLEMENT_CLASS(d1)
ObjectData *c_d1::cloneImpl() {
  c_d1 *obj = NEW(c_d1)();
  cloneSet(obj);
  return obj;
}
void c_d1::cloneSet(c_d1 *clone) {
  c_c::cloneSet(clone);
}
Variant c_d1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke(s, params, hash, fatal);
}
Variant c_d1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(a0), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_c::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d1$os_get(const char *s) {
  return c_d1::os_get(s, -1);
}
Variant &cw_d1$os_lval(const char *s) {
  return c_d1::os_lval(s, -1);
}
Variant cw_d1$os_constant(const char *s) {
  return c_d1::os_constant(s);
}
Variant cw_d1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d1::os_invoke(c, s, params, -1, fatal);
}
void c_d1::init() {
  c_c::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 5 */
void c_d1::t_f(CArrRef v_a) {
  INSTANCE_METHOD_INJECTION(D1, D1::f);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 8 */
Variant c_d2::os_get(const char *s, int64 hash) {
  return c_c::os_get(s, hash);
}
Variant &c_d2::os_lval(const char *s, int64 hash) {
  return c_c::os_lval(s, hash);
}
void c_d2::o_get(ArrayElementVec &props) const {
  c_c::o_get(props);
}
bool c_d2::o_exists(CStrRef s, int64 hash) const {
  return c_c::o_exists(s, hash);
}
Variant c_d2::o_get(CStrRef s, int64 hash) {
  return c_c::o_get(s, hash);
}
Variant c_d2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_c::o_set(s, hash, v, forInit);
}
Variant &c_d2::o_lval(CStrRef s, int64 hash) {
  return c_c::o_lval(s, hash);
}
Variant c_d2::os_constant(const char *s) {
  return c_c::os_constant(s);
}
IMPLEMENT_CLASS(d2)
ObjectData *c_d2::cloneImpl() {
  c_d2 *obj = NEW(c_d2)();
  cloneSet(obj);
  return obj;
}
void c_d2::cloneSet(c_d2 *clone) {
  c_c::cloneSet(clone);
}
Variant c_d2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke(s, params, hash, fatal);
}
Variant c_d2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(a0), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_c::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d2$os_get(const char *s) {
  return c_d2::os_get(s, -1);
}
Variant &cw_d2$os_lval(const char *s) {
  return c_d2::os_lval(s, -1);
}
Variant cw_d2$os_constant(const char *s) {
  return c_d2::os_constant(s);
}
Variant cw_d2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d2::os_invoke(c, s, params, -1, fatal);
}
void c_d2::init() {
  c_c::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 8 */
void c_d2::t_f(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(D2, D2::f);
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d1(CArrRef params, bool init /* = true */) {
  return Object(p_d1(NEW(c_d1)())->dynCreate(params, init));
}
Object co_d2(CArrRef params, bool init /* = true */) {
  return Object(p_d2(NEW(c_d2)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$type_hinting_005a_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$type_hinting_005a_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Compatible hint.\n");
  echo("Class hint, should be array.\n");
  echo("==DONE==\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
