
#ifndef __GENERATED_cls_d2_h__
#define __GENERATED_cls_d2_h__

#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 8 */
class c_d2 : virtual public c_c {
  BEGIN_CLASS_MAP(d2)
    PARENT_CLASS(c)
  END_CLASS_MAP(d2)
  DECLARE_CLASS(d2, D2, c)
  void init();
  public: void t_f(CVarRef v_a);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d2_h__
