
#ifndef __GENERATED_cls_d1_h__
#define __GENERATED_cls_d1_h__

#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005a.php line 5 */
class c_d1 : virtual public c_c {
  BEGIN_CLASS_MAP(d1)
    PARENT_CLASS(c)
  END_CLASS_MAP(d1)
  DECLARE_CLASS(d1, D1, c)
  void init();
  public: void t_f(CArrRef v_a);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d1_h__
