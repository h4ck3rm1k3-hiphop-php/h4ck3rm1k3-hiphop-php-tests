
#ifndef __GENERATED_cls_e_h__
#define __GENERATED_cls_e_h__

#include <cls/d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_properties_004.php line 4 */
class c_e : virtual public c_d {
  BEGIN_CLASS_MAP(e)
    PARENT_CLASS(c)
    PARENT_CLASS(d)
  END_CLASS_MAP(e)
  DECLARE_CLASS(e, E, d)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_e_h__
