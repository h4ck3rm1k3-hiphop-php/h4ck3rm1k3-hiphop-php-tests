
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_properties_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_properties_004.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x77F632A4E34F1526LL, g->s_c_DupIdp,
                  p);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x77F632A4E34F1526LL, g->s_c_DupIdp,
                  p);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_c_DupIdp = "original";
}
void csi_c() {
  c_c::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_properties_004.php line 3 */
Variant c_d::os_get(const char *s, int64 hash) {
  return c_c::os_get(s, hash);
}
Variant &c_d::os_lval(const char *s, int64 hash) {
  return c_c::os_lval(s, hash);
}
void c_d::o_get(ArrayElementVec &props) const {
  c_c::o_get(props);
}
bool c_d::o_exists(CStrRef s, int64 hash) const {
  return c_c::o_exists(s, hash);
}
Variant c_d::o_get(CStrRef s, int64 hash) {
  return c_c::o_get(s, hash);
}
Variant c_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_c::o_set(s, hash, v, forInit);
}
Variant &c_d::o_lval(CStrRef s, int64 hash) {
  return c_c::o_lval(s, hash);
}
Variant c_d::os_constant(const char *s) {
  return c_c::os_constant(s);
}
IMPLEMENT_CLASS(d)
ObjectData *c_d::cloneImpl() {
  c_d *obj = NEW(c_d)();
  cloneSet(obj);
  return obj;
}
void c_d::cloneSet(c_d *clone) {
  c_c::cloneSet(clone);
}
Variant c_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_c::o_invoke(s, params, hash, fatal);
}
Variant c_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_c::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_c::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d$os_get(const char *s) {
  return c_d::os_get(s, -1);
}
Variant &cw_d$os_lval(const char *s) {
  return c_d::os_lval(s, -1);
}
Variant cw_d$os_constant(const char *s) {
  return c_d::os_constant(s);
}
Variant cw_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d::os_invoke(c, s, params, -1, fatal);
}
void c_d::init() {
  c_c::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_properties_004.php line 4 */
Variant c_e::os_get(const char *s, int64 hash) {
  return c_d::os_get(s, hash);
}
Variant &c_e::os_lval(const char *s, int64 hash) {
  return c_d::os_lval(s, hash);
}
void c_e::o_get(ArrayElementVec &props) const {
  c_d::o_get(props);
}
bool c_e::o_exists(CStrRef s, int64 hash) const {
  return c_d::o_exists(s, hash);
}
Variant c_e::o_get(CStrRef s, int64 hash) {
  return c_d::o_get(s, hash);
}
Variant c_e::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_d::o_set(s, hash, v, forInit);
}
Variant &c_e::o_lval(CStrRef s, int64 hash) {
  return c_d::o_lval(s, hash);
}
Variant c_e::os_constant(const char *s) {
  return c_d::os_constant(s);
}
IMPLEMENT_CLASS(e)
ObjectData *c_e::cloneImpl() {
  c_e *obj = NEW(c_e)();
  cloneSet(obj);
  return obj;
}
void c_e::cloneSet(c_e *clone) {
  c_d::cloneSet(clone);
}
Variant c_e::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_d::o_invoke(s, params, hash, fatal);
}
Variant c_e::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_d::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_e::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_d::os_invoke(c, s, params, hash, fatal);
}
Variant cw_e$os_get(const char *s) {
  return c_e::os_get(s, -1);
}
Variant &cw_e$os_lval(const char *s) {
  return c_e::os_lval(s, -1);
}
Variant cw_e$os_constant(const char *s) {
  return c_e::os_constant(s);
}
Variant cw_e$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_e::os_invoke(c, s, params, -1, fatal);
}
void c_e::init() {
  c_d::init();
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d(CArrRef params, bool init /* = true */) {
  return Object(p_d(NEW(c_d)())->dynCreate(params, init));
}
Object co_e(CArrRef params, bool init /* = true */) {
  return Object(p_e(NEW(c_e)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$static_properties_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/static_properties_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$static_properties_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ref __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ref") : g->GV(ref);

  echo("\nInherited static properties refer to the same value accross classes:\n");
  LINE(7,x_var_dump(3, g->s_c_DupIdp, Array(ArrayInit(2).set(0, g->s_c_DupIdp).set(1, g->s_c_DupIdp).create())));
  echo("\nChanging one changes all the others:\n");
  (g->s_c_DupIdp = "changed.all");
  LINE(11,x_var_dump(3, g->s_c_DupIdp, Array(ArrayInit(2).set(0, g->s_c_DupIdp).set(1, g->s_c_DupIdp).create())));
  echo("\nBut because this is implemented using PHP references, the reference set can easily be split:\n");
  (v_ref = "changed.one");
  (g->s_c_DupIdp = ref(v_ref));
  LINE(16,x_var_dump(3, g->s_c_DupIdp, Array(ArrayInit(2).set(0, g->s_c_DupIdp).set(1, g->s_c_DupIdp).create())));
  echo("==Done==\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
