
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php line 10 */
Variant c_counter::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_counter::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_counter::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("id", m_id.isReferenced() ? ref(m_id) : m_id));
  c_ObjectData::o_get(props);
}
bool c_counter::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x028B9FE0C4522BE2LL, id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_counter::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_counter::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x028B9FE0C4522BE2LL, m_id,
                      id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_counter::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_counter::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(counter)
ObjectData *c_counter::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_counter::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_counter::dynConstruct(CArrRef params) {
  (t___construct());
}
void c_counter::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_counter::cloneImpl() {
  c_counter *obj = NEW(c_counter)();
  cloneSet(obj);
  return obj;
}
void c_counter::cloneSet(c_counter *clone) {
  clone->m_id = m_id.isReferenced() ? ref(m_id) : m_id;
  ObjectData::cloneSet(clone);
}
Variant c_counter::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_counter::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_counter::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_counter$os_get(const char *s) {
  return c_counter::os_get(s, -1);
}
Variant &cw_counter$os_lval(const char *s) {
  return c_counter::os_lval(s, -1);
}
Variant cw_counter$os_constant(const char *s) {
  return c_counter::os_constant(s);
}
Variant cw_counter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_counter::os_invoke(c, s, params, -1, fatal);
}
void c_counter::init() {
  m_id = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php line 13 */
void c_counter::t___construct() {
  INSTANCE_METHOD_INJECTION(counter, counter::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_test_cnt __attribute__((__unused__)) = g->GV(test_cnt);
  Variant &gv_test_num __attribute__((__unused__)) = g->GV(test_num);
  {
  }
  gv_test_cnt++;
  (m_id = gv_test_num++);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php line 19 */
void c_counter::t_show() {
  INSTANCE_METHOD_INJECTION(counter, counter::Show);
  echo(LINE(20,concat3("Id: ", toString(m_id), "\n")));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php line 24 */
Variant c_counter::t___destruct() {
  INSTANCE_METHOD_INJECTION(counter, counter::__destruct);
  setInDtor();
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_test_cnt __attribute__((__unused__)) = g->GV(test_cnt);
  gv_test_cnt--;
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php line 29 */
void c_counter::ti_destroy(const char* cls, Variant v_obj) {
  STATIC_METHOD_INJECTION(counter, counter::destroy);
  setNull(v_obj);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php line 5 */
void f_show() {
  FUNCTION_INJECTION(Show);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_test_cnt __attribute__((__unused__)) = g->GV(test_cnt);
  echo(LINE(7,concat3("Count: ", toString(gv_test_cnt), "\n")));
} /* function */
Object co_counter(CArrRef params, bool init /* = true */) {
  return Object(p_counter(NEW(c_counter)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$destructor_and_globals_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$destructor_and_globals_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test_cnt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_cnt") : g->GV(test_cnt);
  Variant &v_test_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_num") : g->GV(test_num);
  Variant &v_obj1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj1") : g->GV(obj1);
  Variant &v_obj2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj2") : g->GV(obj2);

  (v_test_cnt = 0LL);
  (v_test_num = 0LL);
  LINE(33,f_show());
  (v_obj1 = ((Object)(LINE(34,p_counter(p_counter(NEWOBJ(c_counter)())->create())))));
  LINE(35,v_obj1.o_invoke_few_args("Show", 0x35121557DC0EAE15LL, 0));
  LINE(36,f_show());
  (v_obj2 = ((Object)(LINE(37,p_counter(p_counter(NEWOBJ(c_counter)())->create())))));
  LINE(38,v_obj2.o_invoke_few_args("Show", 0x35121557DC0EAE15LL, 0));
  LINE(39,f_show());
  LINE(40,c_counter::t_destroy(ref(v_obj1)));
  LINE(41,f_show());
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
