
#ifndef __GENERATED_cls_counter_h__
#define __GENERATED_cls_counter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_globals.php line 10 */
class c_counter : virtual public ObjectData {
  BEGIN_CLASS_MAP(counter)
  END_CLASS_MAP(counter)
  DECLARE_CLASS(counter, counter, ObjectData)
  void init();
  public: Variant m_id;
  public: virtual void destruct();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_show();
  public: Variant t___destruct();
  public: static void ti_destroy(const char* cls, Variant v_obj);
  public: static void t_destroy(CVarRef v_obj) { ti_destroy("counter", v_obj); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_counter_h__
