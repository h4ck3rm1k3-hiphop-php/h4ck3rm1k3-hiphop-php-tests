
#ifndef __GENERATED_cls_singletoncounter_h__
#define __GENERATED_cls_singletoncounter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.php line 13 */
class c_singletoncounter : virtual public ObjectData {
  BEGIN_CLASS_MAP(singletoncounter)
  END_CLASS_MAP(singletoncounter)
  DECLARE_CLASS(singletoncounter, SingletonCounter, ObjectData)
  void init();
  public: static Variant ti_instance(const char* cls);
  public: static Variant t_instance() { return ti_instance("singletoncounter"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_singletoncounter_h__
