
#ifndef __GENERATED_cls_counter_h__
#define __GENERATED_cls_counter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.php line 3 */
class c_counter : virtual public ObjectData {
  BEGIN_CLASS_MAP(counter)
  END_CLASS_MAP(counter)
  DECLARE_CLASS(counter, Counter, ObjectData)
  void init();
  public: int64 m_counter;
  public: void t_increment_and_print();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_counter_h__
