
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.php line 13 */
Variant c_singletoncounter::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x07115F79B1A84A2ALL, g->s_singletoncounter_DupIdm_instance,
                  m_instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_singletoncounter::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x07115F79B1A84A2ALL, g->s_singletoncounter_DupIdm_instance,
                  m_instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_singletoncounter::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_singletoncounter::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_singletoncounter::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_singletoncounter::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_singletoncounter::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_singletoncounter::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(singletoncounter)
ObjectData *c_singletoncounter::cloneImpl() {
  c_singletoncounter *obj = NEW(c_singletoncounter)();
  cloneSet(obj);
  return obj;
}
void c_singletoncounter::cloneSet(c_singletoncounter *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_singletoncounter::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_singletoncounter::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_singletoncounter::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_singletoncounter$os_get(const char *s) {
  return c_singletoncounter::os_get(s, -1);
}
Variant &cw_singletoncounter$os_lval(const char *s) {
  return c_singletoncounter::os_lval(s, -1);
}
Variant cw_singletoncounter$os_constant(const char *s) {
  return c_singletoncounter::os_constant(s);
}
Variant cw_singletoncounter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_singletoncounter::os_invoke(c, s, params, -1, fatal);
}
void c_singletoncounter::init() {
}
void c_singletoncounter::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_singletoncounter_DupIdm_instance = null;
}
void csi_singletoncounter() {
  c_singletoncounter::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.php line 16 */
Variant c_singletoncounter::ti_instance(const char* cls) {
  STATIC_METHOD_INJECTION(SingletonCounter, SingletonCounter::Instance);
  DECLARE_GLOBAL_VARIABLES(g);
  if (equal(g->s_singletoncounter_DupIdm_instance, null)) {
    (g->s_singletoncounter_DupIdm_instance = ((Object)(LINE(18,p_counter(p_counter(NEWOBJ(c_counter)())->create())))));
  }
  return g->s_singletoncounter_DupIdm_instance;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.php line 3 */
Variant c_counter::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_counter::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_counter::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("counter", m_counter));
  c_ObjectData::o_get(props);
}
bool c_counter::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x1297A95AAC6F5998LL, counter, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_counter::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x1297A95AAC6F5998LL, m_counter,
                         counter, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_counter::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x1297A95AAC6F5998LL, m_counter,
                      counter, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_counter::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_counter::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(counter)
ObjectData *c_counter::cloneImpl() {
  c_counter *obj = NEW(c_counter)();
  cloneSet(obj);
  return obj;
}
void c_counter::cloneSet(c_counter *clone) {
  clone->m_counter = m_counter;
  ObjectData::cloneSet(clone);
}
Variant c_counter::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3276ABC24A08239BLL, increment_and_print) {
        return (t_increment_and_print(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_counter::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3276ABC24A08239BLL, increment_and_print) {
        return (t_increment_and_print(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_counter::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_counter$os_get(const char *s) {
  return c_counter::os_get(s, -1);
}
Variant &cw_counter$os_lval(const char *s) {
  return c_counter::os_lval(s, -1);
}
Variant cw_counter$os_constant(const char *s) {
  return c_counter::os_constant(s);
}
Variant cw_counter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_counter::os_invoke(c, s, params, -1, fatal);
}
void c_counter::init() {
  m_counter = 0LL;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.php line 6 */
void c_counter::t_increment_and_print() {
  INSTANCE_METHOD_INJECTION(Counter, Counter::increment_and_print);
  echo(toString(++m_counter));
  echo("\n");
} /* function */
Object co_singletoncounter(CArrRef params, bool init /* = true */) {
  return Object(p_singletoncounter(NEW(c_singletoncounter)())->dynCreate(params, init));
}
Object co_counter(CArrRef params, bool init /* = true */) {
  return Object(p_counter(NEW(c_counter)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$singleton_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/singleton_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$singleton_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  (assignCallTemp(eo_0, toObject(LINE(24,c_singletoncounter::t_instance()))),eo_0.o_invoke_few_args("increment_and_print", 0x3276ABC24A08239BLL, 0));
  (assignCallTemp(eo_1, toObject(LINE(25,c_singletoncounter::t_instance()))),eo_1.o_invoke_few_args("increment_and_print", 0x3276ABC24A08239BLL, 0));
  (assignCallTemp(eo_2, toObject(LINE(26,c_singletoncounter::t_instance()))),eo_2.o_invoke_few_args("increment_and_print", 0x3276ABC24A08239BLL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
