
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_002.php line 8 */
Variant c_exception_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_exception_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_exception_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("foo", m_foo));
  c_ObjectData::o_get(props);
}
bool c_exception_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x4154FA2EF733DA8FLL, foo, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_exception_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4154FA2EF733DA8FLL, m_foo,
                         foo, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_exception_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x4154FA2EF733DA8FLL, m_foo,
                      foo, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_exception_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_exception_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(exception_foo)
ObjectData *c_exception_foo::cloneImpl() {
  c_exception_foo *obj = NEW(c_exception_foo)();
  cloneSet(obj);
  return obj;
}
void c_exception_foo::cloneSet(c_exception_foo *clone) {
  clone->m_foo = m_foo;
  ObjectData::cloneSet(clone);
}
Variant c_exception_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x71859D7313E682D2LL, getmessage) {
        return (t_getmessage());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_exception_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x71859D7313E682D2LL, getmessage) {
        return (t_getmessage());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_exception_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_exception_foo$os_get(const char *s) {
  return c_exception_foo::os_get(s, -1);
}
Variant &cw_exception_foo$os_lval(const char *s) {
  return c_exception_foo::os_lval(s, -1);
}
Variant cw_exception_foo$os_constant(const char *s) {
  return c_exception_foo::os_constant(s);
}
Variant cw_exception_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_exception_foo::os_invoke(c, s, params, -1, fatal);
}
void c_exception_foo::init() {
  m_foo = "foo";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_002.php line 11 */
String c_exception_foo::t_getmessage() {
  INSTANCE_METHOD_INJECTION(Exception_foo, Exception_foo::getMessage);
  return m_foo;
} /* function */
Object co_exception_foo(CArrRef params, bool init /* = true */) {
  return Object(p_exception_foo(NEW(c_exception_foo)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interfaces_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interfaces_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = ((Object)(LINE(17,p_exception_foo(p_exception_foo(NEWOBJ(c_exception_foo)())->create())))));
  echo(LINE(18,(assignCallTemp(eo_1, toString(v_foo.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Message: ", eo_1, "\n"))));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
