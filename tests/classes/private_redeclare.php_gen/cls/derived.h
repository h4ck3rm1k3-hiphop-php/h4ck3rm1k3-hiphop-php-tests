
#ifndef __GENERATED_cls_derived_h__
#define __GENERATED_cls_derived_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_redeclare.php line 14 */
class c_derived : virtual public c_base {
  BEGIN_CLASS_MAP(derived)
    PARENT_CLASS(base)
  END_CLASS_MAP(derived)
  DECLARE_CLASS(derived, derived, base)
  void init();
  public: void t_show();
  public: void t_test();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_h__
