
#ifndef __GENERATED_cls_c2_h__
#define __GENERATED_cls_c2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php line 15 */
class c_c2 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c2)
  END_CLASS_MAP(c2)
  DECLARE_CLASS(c2, c2, ObjectData)
  void init();
  public: int64 m_max;
  public: int64 m_num;
  public: int64 t_current();
  public: void t_next();
  public: bool t_valid();
  public: Variant t_key();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c2_h__
