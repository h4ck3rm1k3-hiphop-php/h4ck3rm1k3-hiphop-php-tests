
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php line 5 */
Variant c_c1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c1::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c1::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c1::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c1::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c1)
ObjectData *c_c1::cloneImpl() {
  c_c1 *obj = NEW(c_c1)();
  cloneSet(obj);
  return obj;
}
void c_c1::cloneSet(c_c1 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c1$os_get(const char *s) {
  return c_c1::os_get(s, -1);
}
Variant &cw_c1$os_lval(const char *s) {
  return c_c1::os_lval(s, -1);
}
Variant cw_c1$os_constant(const char *s) {
  return c_c1::os_constant(s);
}
Variant cw_c1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c1::os_invoke(c, s, params, -1, fatal);
}
void c_c1::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php line 15 */
Variant c_c2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c2::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("max", m_max));
  props.push_back(NEW(ArrayElement)("num", m_num));
  c_ObjectData::o_get(props);
}
bool c_c2::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x022BD5C706BD9850LL, max, 3);
      HASH_EXISTS_STRING(0x336176791BC03F04LL, num, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c2::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x022BD5C706BD9850LL, m_max,
                         max, 3);
      HASH_RETURN_STRING(0x336176791BC03F04LL, m_num,
                         num, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x022BD5C706BD9850LL, m_max,
                      max, 3);
      HASH_SET_STRING(0x336176791BC03F04LL, m_num,
                      num, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c2)
ObjectData *c_c2::cloneImpl() {
  c_c2 *obj = NEW(c_c2)();
  cloneSet(obj);
  return obj;
}
void c_c2::cloneSet(c_c2 *clone) {
  clone->m_max = m_max;
  clone->m_num = m_num;
  ObjectData::cloneSet(clone);
}
Variant c_c2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c2$os_get(const char *s) {
  return c_c2::os_get(s, -1);
}
Variant &cw_c2$os_lval(const char *s) {
  return c_c2::os_lval(s, -1);
}
Variant cw_c2$os_constant(const char *s) {
  return c_c2::os_constant(s);
}
Variant cw_c2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c2::os_invoke(c, s, params, -1, fatal);
}
void c_c2::init() {
  m_max = 3LL;
  m_num = 0LL;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php line 20 */
int64 c_c2::t_current() {
  INSTANCE_METHOD_INJECTION(c2, c2::current);
  echo("c2::current\n");
  return m_num;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php line 24 */
void c_c2::t_next() {
  INSTANCE_METHOD_INJECTION(c2, c2::next);
  echo("c2::next\n");
  m_num++;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php line 28 */
bool c_c2::t_valid() {
  INSTANCE_METHOD_INJECTION(c2, c2::valid);
  echo("c2::valid\n");
  return less(m_num, m_max);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php line 32 */
Variant c_c2::t_key() {
  INSTANCE_METHOD_INJECTION(c2, c2::key);
  echo("c2::key\n");
  {
    switch (m_num) {
    case 0LL:
      {
        return "1st";
      }
    case 1LL:
      {
        return "2nd";
      }
    case 2LL:
      {
        return "3rd";
      }
    default:
      {
        return "\?\?\?";
      }
    }
  }
  return null;
} /* function */
Object co_c1(CArrRef params, bool init /* = true */) {
  return Object(p_c1(NEW(c_c1)())->dynCreate(params, init));
}
Object co_c2(CArrRef params, bool init /* = true */) {
  return Object(p_c2(NEW(c_c2)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$iterators_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$iterators_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("1st try\n");
  (v_obj = ((Object)(LINE(7,p_c1(p_c1(NEWOBJ(c_c1)())->create())))));
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_obj.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_w = iter4->second();
      {
        echo(LINE(10,concat3("object:", toString(v_w), "\n")));
      }
    }
  }
  echo("2nd try\n");
  (v_obj = ((Object)(LINE(43,p_c2(p_c2(NEWOBJ(c_c2)())->create())))));
  {
    LOOP_COUNTER(5);
    for (ArrayIterPtr iter7 = v_obj.begin(); !iter7->end(); iter7->next()) {
      LOOP_COUNTER_CHECK(5);
      v_w = iter7->second();
      v_v = iter7->first();
      {
        echo(LINE(46,concat5("object:", toString(v_v), "=>", toString(v_w), "\n")));
      }
    }
  }
  print("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
