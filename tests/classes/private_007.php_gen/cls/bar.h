
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_007.php line 3 */
class c_bar : virtual public ObjectData {
  BEGIN_CLASS_MAP(bar)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, Bar, ObjectData)
  void init();
  public: static void ti_pub(const char* cls);
  public: static void ti_priv(const char* cls);
  public: static void t_pub() { ti_pub("bar"); }
  public: static void t_priv() { ti_priv("bar"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
