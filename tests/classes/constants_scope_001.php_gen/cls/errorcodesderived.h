
#ifndef __GENERATED_cls_errorcodesderived_h__
#define __GENERATED_cls_errorcodesderived_h__

#include <cls/errorcodes.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.php line 14 */
class c_errorcodesderived : virtual public c_errorcodes {
  BEGIN_CLASS_MAP(errorcodesderived)
    PARENT_CLASS(errorcodes)
  END_CLASS_MAP(errorcodesderived)
  DECLARE_CLASS(errorcodesderived, ErrorCodesDerived, errorcodes)
  void init();
  public: static void ti_print_fatal_error_codes(const char* cls);
  public: static void t_print_fatal_error_codes() { ti_print_fatal_error_codes("errorcodesderived"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_errorcodesderived_h__
