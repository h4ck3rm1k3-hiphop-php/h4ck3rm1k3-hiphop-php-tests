
#ifndef __GENERATED_cls_errorcodes_h__
#define __GENERATED_cls_errorcodes_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.php line 3 */
class c_errorcodes : virtual public ObjectData {
  BEGIN_CLASS_MAP(errorcodes)
  END_CLASS_MAP(errorcodes)
  DECLARE_CLASS(errorcodes, ErrorCodes, ObjectData)
  void init();
  public: static void ti_print_fatal_error_codes(const char* cls);
  public: static void t_print_fatal_error_codes() { ti_print_fatal_error_codes("errorcodes"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_errorcodes_h__
