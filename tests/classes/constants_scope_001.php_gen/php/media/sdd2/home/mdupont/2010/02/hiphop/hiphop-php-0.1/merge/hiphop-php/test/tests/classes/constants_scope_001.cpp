
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_FATAL = "FATAL";

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.php line 14 */
const StaticString q_errorcodesderived_FATAL = "Worst error\n";
Variant c_errorcodesderived::os_get(const char *s, int64 hash) {
  return c_errorcodes::os_get(s, hash);
}
Variant &c_errorcodesderived::os_lval(const char *s, int64 hash) {
  return c_errorcodes::os_lval(s, hash);
}
void c_errorcodesderived::o_get(ArrayElementVec &props) const {
  c_errorcodes::o_get(props);
}
bool c_errorcodesderived::o_exists(CStrRef s, int64 hash) const {
  return c_errorcodes::o_exists(s, hash);
}
Variant c_errorcodesderived::o_get(CStrRef s, int64 hash) {
  return c_errorcodes::o_get(s, hash);
}
Variant c_errorcodesderived::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_errorcodes::o_set(s, hash, v, forInit);
}
Variant &c_errorcodesderived::o_lval(CStrRef s, int64 hash) {
  return c_errorcodes::o_lval(s, hash);
}
Variant c_errorcodesderived::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x009D9FB08AE89B71LL, q_errorcodesderived_FATAL, FATAL);
      break;
    default:
      break;
  }
  return c_errorcodes::os_constant(s);
}
IMPLEMENT_CLASS(errorcodesderived)
ObjectData *c_errorcodesderived::cloneImpl() {
  c_errorcodesderived *obj = NEW(c_errorcodesderived)();
  cloneSet(obj);
  return obj;
}
void c_errorcodesderived::cloneSet(c_errorcodesderived *clone) {
  c_errorcodes::cloneSet(clone);
}
Variant c_errorcodesderived::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5A8DFD7FB42A261FLL, print_fatal_error_codes) {
        return (ti_print_fatal_error_codes(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_errorcodes::o_invoke(s, params, hash, fatal);
}
Variant c_errorcodesderived::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5A8DFD7FB42A261FLL, print_fatal_error_codes) {
        return (ti_print_fatal_error_codes(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_errorcodes::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_errorcodesderived::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5A8DFD7FB42A261FLL, print_fatal_error_codes) {
        return (ti_print_fatal_error_codes(c), null);
      }
      break;
    default:
      break;
  }
  return c_errorcodes::os_invoke(c, s, params, hash, fatal);
}
Variant cw_errorcodesderived$os_get(const char *s) {
  return c_errorcodesderived::os_get(s, -1);
}
Variant &cw_errorcodesderived$os_lval(const char *s) {
  return c_errorcodesderived::os_lval(s, -1);
}
Variant cw_errorcodesderived$os_constant(const char *s) {
  return c_errorcodesderived::os_constant(s);
}
Variant cw_errorcodesderived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_errorcodesderived::os_invoke(c, s, params, -1, fatal);
}
void c_errorcodesderived::init() {
  c_errorcodes::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.php line 16 */
void c_errorcodesderived::ti_print_fatal_error_codes(const char* cls) {
  STATIC_METHOD_INJECTION(ErrorCodesDerived, ErrorCodesDerived::print_fatal_error_codes);
  echo("self::FATAL = Worst error\n");
  echo("parent::FATAL = Fatal error\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.php line 3 */
const StaticString q_errorcodes_FATAL = "Fatal error\n";
const StaticString q_errorcodes_WARNING = "Warning\n";
const StaticString q_errorcodes_INFO = "Informational message\n";
Variant c_errorcodes::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_errorcodes::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_errorcodes::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_errorcodes::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_errorcodes::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_errorcodes::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_errorcodes::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_errorcodes::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x009D9FB08AE89B71LL, q_errorcodes_FATAL, FATAL);
      break;
    case 6:
      HASH_RETURN(0x0FBE0FB6B386A8E6LL, q_errorcodes_WARNING, WARNING);
      break;
    case 7:
      HASH_RETURN(0x0F2EF58F157D479FLL, q_errorcodes_INFO, INFO);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(errorcodes)
ObjectData *c_errorcodes::cloneImpl() {
  c_errorcodes *obj = NEW(c_errorcodes)();
  cloneSet(obj);
  return obj;
}
void c_errorcodes::cloneSet(c_errorcodes *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_errorcodes::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5A8DFD7FB42A261FLL, print_fatal_error_codes) {
        return (ti_print_fatal_error_codes(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_errorcodes::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5A8DFD7FB42A261FLL, print_fatal_error_codes) {
        return (ti_print_fatal_error_codes(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_errorcodes::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5A8DFD7FB42A261FLL, print_fatal_error_codes) {
        return (ti_print_fatal_error_codes(c), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_errorcodes$os_get(const char *s) {
  return c_errorcodes::os_get(s, -1);
}
Variant &cw_errorcodes$os_lval(const char *s) {
  return c_errorcodes::os_lval(s, -1);
}
Variant cw_errorcodes$os_constant(const char *s) {
  return c_errorcodes::os_constant(s);
}
Variant cw_errorcodes$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_errorcodes::os_invoke(c, s, params, -1, fatal);
}
void c_errorcodes::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.php line 8 */
void c_errorcodes::ti_print_fatal_error_codes(const char* cls) {
  STATIC_METHOD_INJECTION(ErrorCodes, ErrorCodes::print_fatal_error_codes);
  echo(LINE(9,concat3("FATAL = ", k_FATAL, "\n")));
  echo("self::FATAL = Fatal error\n");
} /* function */
Object co_errorcodesderived(CArrRef params, bool init /* = true */) {
  return Object(p_errorcodesderived(NEW(c_errorcodesderived)())->dynCreate(params, init));
}
Object co_errorcodes(CArrRef params, bool init /* = true */) {
  return Object(p_errorcodes(NEW(c_errorcodes)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_scope_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_scope_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_scope_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(23,c_errorcodes::t_print_fatal_error_codes());
  LINE(24,c_errorcodesderived::t_print_fatal_error_codes());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
