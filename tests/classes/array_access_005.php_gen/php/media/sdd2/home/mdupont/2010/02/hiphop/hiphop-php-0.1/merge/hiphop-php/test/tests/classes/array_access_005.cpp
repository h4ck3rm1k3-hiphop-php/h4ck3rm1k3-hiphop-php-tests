
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php line 3 */
Variant c_peoples::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_peoples::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_peoples::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("person", m_person.isReferenced() ? ref(m_person) : m_person));
  c_ObjectData::o_get(props);
}
bool c_peoples::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x67AE1F9174AB011DLL, person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_peoples::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_peoples::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x67AE1F9174AB011DLL, m_person,
                      person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_peoples::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_peoples::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(peoples)
ObjectData *c_peoples::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_peoples::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_peoples::cloneImpl() {
  c_peoples *obj = NEW(c_peoples)();
  cloneSet(obj);
  return obj;
}
void c_peoples::cloneSet(c_peoples *clone) {
  clone->m_person = m_person.isReferenced() ? ref(m_person) : m_person;
  ObjectData::cloneSet(clone);
}
Variant c_peoples::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_peoples::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_peoples::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_peoples$os_get(const char *s) {
  return c_peoples::os_get(s, -1);
}
Variant &cw_peoples$os_lval(const char *s) {
  return c_peoples::os_lval(s, -1);
}
Variant cw_peoples$os_constant(const char *s) {
  return c_peoples::os_constant(s);
}
Variant cw_peoples$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_peoples::os_invoke(c, s, params, -1, fatal);
}
void c_peoples::init() {
  m_person = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php line 6 */
void c_peoples::t___construct() {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_person = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php line 10 */
bool c_peoples::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetExists);
  return LINE(11,x_array_key_exists(m_person, v_index));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php line 14 */
Variant c_peoples::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  return m_person.rvalAt(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php line 14 */
Variant &c_peoples::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php line 18 */
void c_peoples::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetSet);
  m_person.set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php line 22 */
void c_peoples::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  m_person.weakRemove(v_index);
} /* function */
Object co_peoples(CArrRef params, bool init /* = true */) {
  return Object(p_peoples(NEW(c_peoples)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_005_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_005.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_005_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_people __attribute__((__unused__)) = (variables != gVariables) ? variables->get("people") : g->GV(people);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_people = ((Object)(LINE(27,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(29,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", (concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Foo")), 0x0BCDB293DC3CBDDCLL);
  LINE(31,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  concat_assign(lval(lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Bar");
  LINE(33,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("---ArrayOverloading---\n");
  (v_people = ((Object)(LINE(37,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(39,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(40,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  LINE(41,x_var_dump(1, concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Foo")));
  (v_x = v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  concat_assign(lval(v_x.lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Foo");
  v_people.set(0LL, (v_x), 0x77CFA1EEF01BCA90LL);
  LINE(45,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("JoeFoo"), 0x0BCDB293DC3CBDDCLL);
  LINE(47,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("JoeFooBar"), 0x0BCDB293DC3CBDDCLL);
  LINE(49,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
