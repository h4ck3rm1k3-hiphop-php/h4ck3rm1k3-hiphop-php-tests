
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_001.php line 2 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("p1", m_p1));
  props.push_back(NEW(ArrayElement)("p2", m_p2.isReferenced() ? ref(m_p2) : m_p2));
  props.push_back(NEW(ArrayElement)("p3", m_p3.isReferenced() ? ref(m_p3) : m_p3));
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 6:
      HASH_EXISTS_STRING(0x18B6B61EF19E261ELL, p3, 2);
      break;
    case 7:
      HASH_EXISTS_STRING(0x5C657D380DA684F7LL, p1, 2);
      HASH_EXISTS_STRING(0x0FEFA20DB1B08AC7LL, p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 6:
      HASH_RETURN_STRING(0x18B6B61EF19E261ELL, m_p3,
                         p3, 2);
      break;
    case 7:
      HASH_RETURN_STRING(0x5C657D380DA684F7LL, m_p1,
                         p1, 2);
      HASH_RETURN_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                         p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 6:
      HASH_SET_STRING(0x18B6B61EF19E261ELL, m_p3,
                      p3, 2);
      break;
    case 7:
      HASH_SET_STRING(0x5C657D380DA684F7LL, m_p1,
                      p1, 2);
      HASH_SET_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                      p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x18B6B61EF19E261ELL, m_p3,
                         p3, 2);
      break;
    case 3:
      HASH_RETURN_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                         p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  clone->m_p1 = m_p1;
  clone->m_p2 = m_p2.isReferenced() ? ref(m_p2) : m_p2;
  clone->m_p3 = m_p3.isReferenced() ? ref(m_p3) : m_p3;
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  m_p1 = 1LL;
  m_p2 = 2LL;
  m_p3 = null;
}
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_copy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("copy") : g->GV(copy);

  (v_obj = ((Object)(LINE(8,p_test(p_test(NEWOBJ(c_test)())->create())))));
  (v_obj.o_lval("p2", 0x0FEFA20DB1B08AC7LL) = "A");
  (v_obj.o_lval("p3", 0x18B6B61EF19E261ELL) = "B");
  (v_copy = f_clone(toObject(v_obj)));
  (v_copy.o_lval("p3", 0x18B6B61EF19E261ELL) = "C");
  echo("Object\n");
  LINE(14,x_var_dump(1, v_obj));
  echo("Clown\n");
  LINE(16,x_var_dump(1, v_copy));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
