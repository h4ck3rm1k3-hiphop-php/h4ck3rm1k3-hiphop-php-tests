
#ifndef __GENERATED_cls_derived_h__
#define __GENERATED_cls_derived_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php line 23 */
class c_derived : virtual public c_base {
  BEGIN_CLASS_MAP(derived)
    PARENT_CLASS(base)
  END_CLASS_MAP(derived)
  DECLARE_CLASS(derived, derived, base)
  void init();
  public: String m_other;
  public: virtual void destruct();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_h__
