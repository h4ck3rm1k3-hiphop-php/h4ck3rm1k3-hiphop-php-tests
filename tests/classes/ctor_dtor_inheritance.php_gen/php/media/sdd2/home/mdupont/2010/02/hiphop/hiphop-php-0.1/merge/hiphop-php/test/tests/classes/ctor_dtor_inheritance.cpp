
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php line 8 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name));
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_base::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_base::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  clone->m_name = m_name;
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
  m_name = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php line 11 */
void c_base::t___construct() {
  INSTANCE_METHOD_INJECTION(base, base::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("base::__construct\n");
  (m_name = "base");
  LINE(14,x_print_r(((Object)(this))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php line 17 */
Variant c_base::t___destruct() {
  INSTANCE_METHOD_INJECTION(base, base::__destruct);
  setInDtor();
  echo("base::__destruct\n");
  LINE(19,x_print_r(((Object)(this))));
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php line 23 */
Variant c_derived::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_derived::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_derived::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("other", m_other));
  c_base::o_get(props);
}
bool c_derived::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x353F92C20A856FC5LL, other, 5);
      break;
    default:
      break;
  }
  return c_base::o_exists(s, hash);
}
Variant c_derived::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x353F92C20A856FC5LL, m_other,
                         other, 5);
      break;
    default:
      break;
  }
  return c_base::o_get(s, hash);
}
Variant c_derived::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x353F92C20A856FC5LL, m_other,
                      other, 5);
      break;
    default:
      break;
  }
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_derived::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_derived::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(derived)
ObjectData *c_derived::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_derived::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_derived::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_derived::cloneImpl() {
  c_derived *obj = NEW(c_derived)();
  cloneSet(obj);
  return obj;
}
void c_derived::cloneSet(c_derived *clone) {
  clone->m_other = m_other;
  c_base::cloneSet(clone);
}
Variant c_derived::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_derived::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived$os_get(const char *s) {
  return c_derived::os_get(s, -1);
}
Variant &cw_derived$os_lval(const char *s) {
  return c_derived::os_lval(s, -1);
}
Variant cw_derived$os_constant(const char *s) {
  return c_derived::os_constant(s);
}
Variant cw_derived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived::os_invoke(c, s, params, -1, fatal);
}
void c_derived::init() {
  c_base::init();
  m_other = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php line 26 */
void c_derived::t___construct() {
  INSTANCE_METHOD_INJECTION(derived, derived::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_name = "init");
  (m_other = "other");
  LINE(29,x_print_r(((Object)(this))));
  LINE(30,c_base::t___construct());
  echo("derived::__construct\n");
  (m_name = "derived");
  LINE(33,x_print_r(((Object)(this))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php line 36 */
Variant c_derived::t___destruct() {
  INSTANCE_METHOD_INJECTION(derived, derived::__destruct);
  setInDtor();
  LINE(37,c_base::t___destruct());
  echo("derived::__destruct\n");
  LINE(39,x_print_r(((Object)(this))));
  return null;
} /* function */
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Object co_derived(CArrRef params, bool init /* = true */) {
  return Object(p_derived(NEW(c_derived)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_dtor_inheritance_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_dtor_inheritance.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_dtor_inheritance_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  echo("Testing class base\n");
  (v_t = ((Object)(LINE(44,p_base(p_base(NEWOBJ(c_base)())->create())))));
  unset(v_t);
  echo("Testing class derived\n");
  (v_t = ((Object)(LINE(47,p_derived(p_derived(NEWOBJ(c_derived)())->create())))));
  unset(v_t);
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
