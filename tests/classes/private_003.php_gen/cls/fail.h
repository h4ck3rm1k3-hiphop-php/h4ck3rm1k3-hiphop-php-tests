
#ifndef __GENERATED_cls_fail_h__
#define __GENERATED_cls_fail_h__

#include <cls/pass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_003.php line 12 */
class c_fail : virtual public c_pass {
  BEGIN_CLASS_MAP(fail)
    PARENT_CLASS(pass)
  END_CLASS_MAP(fail)
  DECLARE_CLASS(fail, fail, pass)
  void init();
  public: static void ti_ok(const char* cls);
  public: static void ti_not_ok(const char* cls);
  public: static void t_not_ok() { ti_not_ok("fail"); }
  public: static void t_ok() { ti_ok("fail"); }
  public: static void t_show() { ti_show("fail"); }
  public: static void t_good() { ti_good("fail"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fail_h__
