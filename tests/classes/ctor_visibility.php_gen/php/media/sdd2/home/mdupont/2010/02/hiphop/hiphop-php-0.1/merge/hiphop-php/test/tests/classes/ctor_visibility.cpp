
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 42 */
Variant c_derivedpriv::os_get(const char *s, int64 hash) {
  return c_testpriv::os_get(s, hash);
}
Variant &c_derivedpriv::os_lval(const char *s, int64 hash) {
  return c_testpriv::os_lval(s, hash);
}
void c_derivedpriv::o_get(ArrayElementVec &props) const {
  c_testpriv::o_get(props);
}
bool c_derivedpriv::o_exists(CStrRef s, int64 hash) const {
  return c_testpriv::o_exists(s, hash);
}
Variant c_derivedpriv::o_get(CStrRef s, int64 hash) {
  return c_testpriv::o_get(s, hash);
}
Variant c_derivedpriv::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_testpriv::o_set(s, hash, v, forInit);
}
Variant &c_derivedpriv::o_lval(CStrRef s, int64 hash) {
  return c_testpriv::o_lval(s, hash);
}
Variant c_derivedpriv::os_constant(const char *s) {
  return c_testpriv::os_constant(s);
}
IMPLEMENT_CLASS(derivedpriv)
ObjectData *c_derivedpriv::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_derivedpriv::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_derivedpriv::cloneImpl() {
  c_derivedpriv *obj = NEW(c_derivedpriv)();
  cloneSet(obj);
  return obj;
}
void c_derivedpriv::cloneSet(c_derivedpriv *clone) {
  c_testpriv::cloneSet(clone);
}
Variant c_derivedpriv::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (ti_f(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_testpriv::o_invoke(s, params, hash, fatal);
}
Variant c_derivedpriv::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (ti_f(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_testpriv::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derivedpriv::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (ti_f(c), null);
      }
      break;
    default:
      break;
  }
  return c_testpriv::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derivedpriv$os_get(const char *s) {
  return c_derivedpriv::os_get(s, -1);
}
Variant &cw_derivedpriv$os_lval(const char *s) {
  return c_derivedpriv::os_lval(s, -1);
}
Variant cw_derivedpriv$os_constant(const char *s) {
  return c_derivedpriv::os_constant(s);
}
Variant cw_derivedpriv$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derivedpriv::os_invoke(c, s, params, -1, fatal);
}
void c_derivedpriv::init() {
  c_testpriv::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 44 */
void c_derivedpriv::t___construct() {
  INSTANCE_METHOD_INJECTION(DerivedPriv, DerivedPriv::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("DerivedPriv::__construct()\n");
  LINE(47,c_testpriv::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 50 */
void c_derivedpriv::ti_f(const char* cls) {
  STATIC_METHOD_INJECTION(DerivedPriv, DerivedPriv::f);
  ((Object)(LINE(52,p_derivedpriv(p_derivedpriv(NEWOBJ(c_derivedpriv)())->create()))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 27 */
Variant c_testpriv::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_testpriv::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_testpriv::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_testpriv::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_testpriv::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_testpriv::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_testpriv::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_testpriv::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(testpriv)
ObjectData *c_testpriv::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_testpriv::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_testpriv::cloneImpl() {
  c_testpriv *obj = NEW(c_testpriv)();
  cloneSet(obj);
  return obj;
}
void c_testpriv::cloneSet(c_testpriv *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_testpriv::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (ti_f(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_testpriv::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (ti_f(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_testpriv::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (ti_f(c), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_testpriv$os_get(const char *s) {
  return c_testpriv::os_get(s, -1);
}
Variant &cw_testpriv$os_lval(const char *s) {
  return c_testpriv::os_lval(s, -1);
}
Variant cw_testpriv$os_constant(const char *s) {
  return c_testpriv::os_constant(s);
}
Variant cw_testpriv$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_testpriv::os_invoke(c, s, params, -1, fatal);
}
void c_testpriv::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 29 */
void c_testpriv::t___construct() {
  INSTANCE_METHOD_INJECTION(TestPriv, TestPriv::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("TestPriv::__construct()\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 34 */
void c_testpriv::ti_f(const char* cls) {
  STATIC_METHOD_INJECTION(TestPriv, TestPriv::f);
  ((Object)(LINE(36,p_testpriv(p_testpriv(NEWOBJ(c_testpriv)())->create()))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 3 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_test::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 5 */
void c_test::t___construct() {
  INSTANCE_METHOD_INJECTION(Test, Test::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("Test::__construct()\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 11 */
Variant c_derived::os_get(const char *s, int64 hash) {
  return c_test::os_get(s, hash);
}
Variant &c_derived::os_lval(const char *s, int64 hash) {
  return c_test::os_lval(s, hash);
}
void c_derived::o_get(ArrayElementVec &props) const {
  c_test::o_get(props);
}
bool c_derived::o_exists(CStrRef s, int64 hash) const {
  return c_test::o_exists(s, hash);
}
Variant c_derived::o_get(CStrRef s, int64 hash) {
  return c_test::o_get(s, hash);
}
Variant c_derived::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test::o_set(s, hash, v, forInit);
}
Variant &c_derived::o_lval(CStrRef s, int64 hash) {
  return c_test::o_lval(s, hash);
}
Variant c_derived::os_constant(const char *s) {
  return c_test::os_constant(s);
}
IMPLEMENT_CLASS(derived)
ObjectData *c_derived::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_derived::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_derived::cloneImpl() {
  c_derived *obj = NEW(c_derived)();
  cloneSet(obj);
  return obj;
}
void c_derived::cloneSet(c_derived *clone) {
  c_test::cloneSet(clone);
}
Variant c_derived::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke(s, params, hash, fatal);
}
Variant c_derived::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived$os_get(const char *s) {
  return c_derived::os_get(s, -1);
}
Variant &cw_derived$os_lval(const char *s) {
  return c_derived::os_lval(s, -1);
}
Variant cw_derived$os_constant(const char *s) {
  return c_derived::os_constant(s);
}
Variant cw_derived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived::os_invoke(c, s, params, -1, fatal);
}
void c_derived::init() {
  c_test::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 13 */
void c_derived::t___construct() {
  INSTANCE_METHOD_INJECTION(Derived, Derived::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("Derived::__construct()\n");
  LINE(16,c_test::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 19 */
void c_derived::ti_f(const char* cls) {
  STATIC_METHOD_INJECTION(Derived, Derived::f);
  ((Object)(LINE(21,p_derived(p_derived(NEWOBJ(c_derived)())->create()))));
} /* function */
Object co_derivedpriv(CArrRef params, bool init /* = true */) {
  return Object(p_derivedpriv(NEW(c_derivedpriv)())->dynCreate(params, init));
}
Object co_testpriv(CArrRef params, bool init /* = true */) {
  return Object(p_testpriv(NEW(c_testpriv)())->dynCreate(params, init));
}
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Object co_derived(CArrRef params, bool init /* = true */) {
  return Object(p_derived(NEW(c_derived)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_visibility_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_visibility_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(25,c_derived::t_f());
  LINE(40,c_testpriv::t_f());
  LINE(56,c_derivedpriv::t_f());
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
