
#ifndef __GENERATED_cls_derivedpriv_h__
#define __GENERATED_cls_derivedpriv_h__

#include <cls/testpriv.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 42 */
class c_derivedpriv : virtual public c_testpriv {
  BEGIN_CLASS_MAP(derivedpriv)
    PARENT_CLASS(testpriv)
  END_CLASS_MAP(derivedpriv)
  DECLARE_CLASS(derivedpriv, DerivedPriv, testpriv)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: static void ti_f(const char* cls);
  public: static void t_f() { ti_f("derivedpriv"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derivedpriv_h__
