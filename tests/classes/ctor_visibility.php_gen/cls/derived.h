
#ifndef __GENERATED_cls_derived_h__
#define __GENERATED_cls_derived_h__

#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 11 */
class c_derived : virtual public c_test {
  BEGIN_CLASS_MAP(derived)
    PARENT_CLASS(test)
  END_CLASS_MAP(derived)
  DECLARE_CLASS(derived, Derived, test)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: static void ti_f(const char* cls);
  public: static void t_f() { ti_f("derived"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_h__
