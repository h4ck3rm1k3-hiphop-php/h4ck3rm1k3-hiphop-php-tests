
#ifndef __GENERATED_cls_testpriv_h__
#define __GENERATED_cls_testpriv_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_visibility.php line 27 */
class c_testpriv : virtual public ObjectData {
  BEGIN_CLASS_MAP(testpriv)
  END_CLASS_MAP(testpriv)
  DECLARE_CLASS(testpriv, TestPriv, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: static void ti_f(const char* cls);
  public: static void t_f() { ti_f("testpriv"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testpriv_h__
