
#ifndef __GENERATED_cls_arrayreferenceproxy_h__
#define __GENERATED_cls_arrayreferenceproxy_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 5 */
class c_arrayreferenceproxy : virtual public c_arrayaccess {
  BEGIN_CLASS_MAP(arrayreferenceproxy)
    PARENT_CLASS(arrayaccess)
  END_CLASS_MAP(arrayreferenceproxy)
  DECLARE_CLASS(arrayreferenceproxy, ArrayReferenceProxy, ObjectData)
  void init();
  public: Variant m_object;
  public: Variant m_element;
  public: void t___construct(p_arrayaccess v_object, Variant v_element);
  public: ObjectData *create(p_arrayaccess v_object, Variant v_element);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: bool t_offsetexists(CVarRef v_index);
  public: Variant t_offsetget(Variant v_index);
  public: virtual Variant &___offsetget_lval(Variant v_index);
  public: void t_offsetset(CVarRef v_index, CVarRef v_value);
  public: void t_offsetunset(CVarRef v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_arrayreferenceproxy_h__
