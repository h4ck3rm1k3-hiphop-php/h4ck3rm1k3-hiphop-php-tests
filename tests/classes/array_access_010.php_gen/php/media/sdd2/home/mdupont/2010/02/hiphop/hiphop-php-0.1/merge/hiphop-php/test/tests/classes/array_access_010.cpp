
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 5 */
Variant c_arrayreferenceproxy::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_arrayreferenceproxy::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_arrayreferenceproxy::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("object", m_object.isReferenced() ? ref(m_object) : m_object));
  props.push_back(NEW(ArrayElement)("element", m_element.isReferenced() ? ref(m_element) : m_element));
  c_ObjectData::o_get(props);
}
bool c_arrayreferenceproxy::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x7F30BC4E222B1861LL, object, 6);
      HASH_EXISTS_STRING(0x1AFD3E7207DEC289LL, element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_arrayreferenceproxy::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x7F30BC4E222B1861LL, m_object,
                         object, 6);
      HASH_RETURN_STRING(0x1AFD3E7207DEC289LL, m_element,
                         element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_arrayreferenceproxy::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x7F30BC4E222B1861LL, m_object,
                      object, 6);
      HASH_SET_STRING(0x1AFD3E7207DEC289LL, m_element,
                      element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_arrayreferenceproxy::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x7F30BC4E222B1861LL, m_object,
                         object, 6);
      HASH_RETURN_STRING(0x1AFD3E7207DEC289LL, m_element,
                         element, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_arrayreferenceproxy::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(arrayreferenceproxy)
ObjectData *c_arrayreferenceproxy::create(p_arrayaccess v_object, Variant v_element) {
  init();
  t___construct(v_object, ref(v_element));
  return this;
}
ObjectData *c_arrayreferenceproxy::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))));
  } else return this;
}
ObjectData *c_arrayreferenceproxy::cloneImpl() {
  c_arrayreferenceproxy *obj = NEW(c_arrayreferenceproxy)();
  cloneSet(obj);
  return obj;
}
void c_arrayreferenceproxy::cloneSet(c_arrayreferenceproxy *clone) {
  clone->m_object = m_object.isReferenced() ? ref(m_object) : m_object;
  clone->m_element = m_element.isReferenced() ? ref(m_element) : m_element;
  ObjectData::cloneSet(clone);
}
Variant c_arrayreferenceproxy::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_arrayreferenceproxy::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_arrayreferenceproxy::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_arrayreferenceproxy$os_get(const char *s) {
  return c_arrayreferenceproxy::os_get(s, -1);
}
Variant &cw_arrayreferenceproxy$os_lval(const char *s) {
  return c_arrayreferenceproxy::os_lval(s, -1);
}
Variant cw_arrayreferenceproxy$os_constant(const char *s) {
  return c_arrayreferenceproxy::os_constant(s);
}
Variant cw_arrayreferenceproxy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_arrayreferenceproxy::os_invoke(c, s, params, -1, fatal);
}
void c_arrayreferenceproxy::init() {
  m_object = null;
  m_element = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 10 */
void c_arrayreferenceproxy::t___construct(p_arrayaccess v_object, Variant v_element) {
  INSTANCE_METHOD_INJECTION(ArrayReferenceProxy, ArrayReferenceProxy::__construct);
  bool oldInCtor = gasInCtor(true);
  echo(concat("ArrayReferenceProxy::__construct", LINE(12,concat3("(", toString(v_element), ")\n"))));
  (m_object = ((Object)(v_object)));
  (m_element = ref(v_element));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 17 */
bool c_arrayreferenceproxy::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(ArrayReferenceProxy, ArrayReferenceProxy::offsetExists);
  echo(concat("ArrayReferenceProxy::offsetExists", LINE(18,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  return LINE(19,x_array_key_exists(v_index, m_element));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 22 */
Variant c_arrayreferenceproxy::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayReferenceProxy, ArrayReferenceProxy::offsetGet);
  echo(concat("ArrayReferenceProxy::offsetGet", LINE(23,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  return isset(m_element, v_index) ? ((Variant)(m_element.rvalAt(v_index))) : ((Variant)(null));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 22 */
Variant &c_arrayreferenceproxy::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayReferenceProxy, ArrayReferenceProxy::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 27 */
void c_arrayreferenceproxy::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(ArrayReferenceProxy, ArrayReferenceProxy::offsetSet);
  echo(concat("ArrayReferenceProxy::offsetSet", concat("(", LINE(28,concat6(toString(m_element), ", ", toString(v_index), ", ", toString(v_value), ")\n")))));
  m_element.set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 32 */
void c_arrayreferenceproxy::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(ArrayReferenceProxy, ArrayReferenceProxy::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  echo(concat("ArrayReferenceProxy::offsetUnset", LINE(33,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  m_element.weakRemove(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 38 */
Variant c_peoples::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_peoples::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_peoples::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("person", m_person.isReferenced() ? ref(m_person) : m_person));
  c_ObjectData::o_get(props);
}
bool c_peoples::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x67AE1F9174AB011DLL, person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_peoples::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_peoples::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x67AE1F9174AB011DLL, m_person,
                      person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_peoples::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_peoples::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(peoples)
ObjectData *c_peoples::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_peoples::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_peoples::cloneImpl() {
  c_peoples *obj = NEW(c_peoples)();
  cloneSet(obj);
  return obj;
}
void c_peoples::cloneSet(c_peoples *clone) {
  clone->m_person = m_person.isReferenced() ? ref(m_person) : m_person;
  ObjectData::cloneSet(clone);
}
Variant c_peoples::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_peoples::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_peoples::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_peoples$os_get(const char *s) {
  return c_peoples::os_get(s, -1);
}
Variant &cw_peoples$os_lval(const char *s) {
  return c_peoples::os_lval(s, -1);
}
Variant cw_peoples$os_constant(const char *s) {
  return c_peoples::os_constant(s);
}
Variant cw_peoples$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_peoples::os_invoke(c, s, params, -1, fatal);
}
void c_peoples::init() {
  m_person = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 42 */
void c_peoples::t___construct() {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_person = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 47 */
bool c_peoples::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetExists);
  return LINE(49,x_array_key_exists(v_index, m_person));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 52 */
Variant c_peoples::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  return ((Object)(LINE(54,p_arrayreferenceproxy(p_arrayreferenceproxy(NEWOBJ(c_arrayreferenceproxy)())->create(p_arrayaccess(this), ref(lval(m_person.lvalAt(v_index))))))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 52 */
Variant &c_peoples::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 57 */
void c_peoples::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetSet);
  m_person.set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php line 62 */
void c_peoples::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  m_person.weakRemove(v_index);
} /* function */
Object co_arrayreferenceproxy(CArrRef params, bool init /* = true */) {
  return Object(p_arrayreferenceproxy(NEW(c_arrayreferenceproxy)())->dynCreate(params, init));
}
Object co_peoples(CArrRef params, bool init /* = true */) {
  return Object(p_peoples(NEW(c_peoples)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_010_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_010.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_010_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_people __attribute__((__unused__)) = (variables != gVariables) ? variables->get("people") : g->GV(people);

  (v_people = ((Object)(LINE(68,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(70,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", (concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Bar")), 0x0BCDB293DC3CBDDCLL);
  LINE(72,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  concat_assign(lval(lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Baz");
  LINE(74,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("===ArrayOverloading===\n");
  (v_people = ((Object)(LINE(78,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(80,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(81,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("FooBar"), 0x0BCDB293DC3CBDDCLL);
  LINE(83,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", (concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Bar")), 0x0BCDB293DC3CBDDCLL);
  LINE(85,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  concat_assign(lval(lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Baz");
  LINE(87,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(unsetLval(v_people, 0LL, 0x77CFA1EEF01BCA90LL)).weakRemove("name", 0x0BCDB293DC3CBDDCLL);
  LINE(89,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(90,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("BlaBla"), 0x0BCDB293DC3CBDDCLL);
  LINE(92,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
