
#ifndef __GENERATED_cls_mytestclass_h__
#define __GENERATED_cls_mytestclass_h__

#include <cls/myinterface.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_003.php line 10 */
class c_mytestclass : virtual public c_myinterface {
  BEGIN_CLASS_MAP(mytestclass)
    PARENT_CLASS(myinterface)
  END_CLASS_MAP(mytestclass)
  DECLARE_CLASS(mytestclass, MyTestClass, ObjectData)
  void init();
  public: void t___construct(p_myobject v_o);
  public: ObjectData *create(p_myobject v_o);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mytestclass_h__
