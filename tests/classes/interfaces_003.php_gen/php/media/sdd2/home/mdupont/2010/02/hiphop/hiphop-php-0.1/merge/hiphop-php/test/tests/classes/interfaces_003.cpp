
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_003.php line 10 */
Variant c_mytestclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_mytestclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_mytestclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_mytestclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_mytestclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_mytestclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_mytestclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_mytestclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(mytestclass)
ObjectData *c_mytestclass::create(p_myobject v_o) {
  init();
  t___construct(v_o);
  return this;
}
ObjectData *c_mytestclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_mytestclass::cloneImpl() {
  c_mytestclass *obj = NEW(c_mytestclass)();
  cloneSet(obj);
  return obj;
}
void c_mytestclass::cloneSet(c_mytestclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_mytestclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_mytestclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_mytestclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_mytestclass$os_get(const char *s) {
  return c_mytestclass::os_get(s, -1);
}
Variant &cw_mytestclass$os_lval(const char *s) {
  return c_mytestclass::os_lval(s, -1);
}
Variant cw_mytestclass$os_constant(const char *s) {
  return c_mytestclass::os_constant(s);
}
Variant cw_mytestclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_mytestclass::os_invoke(c, s, params, -1, fatal);
}
void c_mytestclass::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_003.php line 12 */
void c_mytestclass::t___construct(p_myobject v_o) {
  INSTANCE_METHOD_INJECTION(MyTestClass, MyTestClass::__construct);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_003.php line 3 */
Variant c_myobject::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myobject::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myobject::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myobject::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myobject::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myobject::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myobject::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myobject::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myobject)
ObjectData *c_myobject::cloneImpl() {
  c_myobject *obj = NEW(c_myobject)();
  cloneSet(obj);
  return obj;
}
void c_myobject::cloneSet(c_myobject *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myobject::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myobject::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myobject::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myobject$os_get(const char *s) {
  return c_myobject::os_get(s, -1);
}
Variant &cw_myobject$os_lval(const char *s) {
  return c_myobject::os_lval(s, -1);
}
Variant cw_myobject$os_constant(const char *s) {
  return c_myobject::os_constant(s);
}
Variant cw_myobject$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myobject::os_invoke(c, s, params, -1, fatal);
}
void c_myobject::init() {
}
Object co_mytestclass(CArrRef params, bool init /* = true */) {
  return Object(p_mytestclass(NEW(c_mytestclass)())->dynCreate(params, init));
}
Object co_myobject(CArrRef params, bool init /* = true */) {
  return Object(p_myobject(NEW(c_myobject)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interfaces_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interfaces_003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interfaces_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  (v_obj = ((Object)(LINE(17,create_object("mytestclass", Array())))));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
