
#ifndef __GENERATED_cls_overloadedarray_h__
#define __GENERATED_cls_overloadedarray_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_006.php line 3 */
class c_overloadedarray : virtual public c_arrayaccess {
  BEGIN_CLASS_MAP(overloadedarray)
    PARENT_CLASS(arrayaccess)
  END_CLASS_MAP(overloadedarray)
  DECLARE_CLASS(overloadedarray, OverloadedArray, ObjectData)
  void init();
  public: Variant m_realArray;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: bool t_offsetexists(CVarRef v_index);
  public: Variant t_offsetget(Variant v_index);
  public: virtual Variant &___offsetget_lval(Variant v_index);
  public: void t_offsetset(CVarRef v_index, CVarRef v_value);
  public: void t_offsetunset(CVarRef v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_overloadedarray_h__
