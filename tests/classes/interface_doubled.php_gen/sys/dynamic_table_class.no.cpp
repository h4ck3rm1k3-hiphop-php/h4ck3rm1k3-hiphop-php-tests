
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_class_a(CArrRef params, bool init = true);
Variant cw_class_a$os_get(const char *s);
Variant &cw_class_a$os_lval(const char *s);
Variant cw_class_a$os_constant(const char *s);
Variant cw_class_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class_b(CArrRef params, bool init = true);
Variant cw_class_b$os_get(const char *s);
Variant &cw_class_b$os_lval(const char *s);
Variant cw_class_b$os_constant(const char *s);
Variant cw_class_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class_c(CArrRef params, bool init = true);
Variant cw_class_c$os_get(const char *s);
Variant &cw_class_c$os_lval(const char *s);
Variant cw_class_c$os_constant(const char *s);
Variant cw_class_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class_d(CArrRef params, bool init = true);
Variant cw_class_d$os_get(const char *s);
Variant &cw_class_d$os_lval(const char *s);
Variant cw_class_d$os_constant(const char *s);
Variant cw_class_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class_e(CArrRef params, bool init = true);
Variant cw_class_e$os_get(const char *s);
Variant &cw_class_e$os_lval(const char *s);
Variant cw_class_e$os_constant(const char *s);
Variant cw_class_e$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class_f(CArrRef params, bool init = true);
Variant cw_class_f$os_get(const char *s);
Variant &cw_class_f$os_lval(const char *s);
Variant cw_class_f$os_constant(const char *s);
Variant cw_class_f$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class_g(CArrRef params, bool init = true);
Variant cw_class_g$os_get(const char *s);
Variant &cw_class_g$os_lval(const char *s);
Variant cw_class_g$os_constant(const char *s);
Variant cw_class_g$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_base(CArrRef params, bool init = true);
Variant cw_base$os_get(const char *s);
Variant &cw_base$os_lval(const char *s);
Variant cw_base$os_constant(const char *s);
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_CREATE_OBJECT(0x333208BF21A873C1LL, class_g);
      HASH_CREATE_OBJECT(0x62D2775E17EE82D1LL, base);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x2E556997DA64C423LL, class_e);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x06A7DC3D42BB28B4LL, class_d);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x7F5A4A99F31920E5LL, class_a);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x45624A3AE7810DA9LL, class_c);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x14698A85431CD40CLL, class_b);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x71EC76BCC66E94CFLL, class_f);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x333208BF21A873C1LL, class_g);
      HASH_INVOKE_STATIC_METHOD(0x62D2775E17EE82D1LL, base);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x2E556997DA64C423LL, class_e);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x06A7DC3D42BB28B4LL, class_d);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x7F5A4A99F31920E5LL, class_a);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x45624A3AE7810DA9LL, class_c);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x14698A85431CD40CLL, class_b);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x71EC76BCC66E94CFLL, class_f);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x333208BF21A873C1LL, class_g);
      HASH_GET_STATIC_PROPERTY(0x62D2775E17EE82D1LL, base);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x2E556997DA64C423LL, class_e);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x06A7DC3D42BB28B4LL, class_d);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x7F5A4A99F31920E5LL, class_a);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x45624A3AE7810DA9LL, class_c);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x14698A85431CD40CLL, class_b);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x71EC76BCC66E94CFLL, class_f);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x333208BF21A873C1LL, class_g);
      HASH_GET_STATIC_PROPERTY_LV(0x62D2775E17EE82D1LL, base);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x2E556997DA64C423LL, class_e);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x06A7DC3D42BB28B4LL, class_d);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x7F5A4A99F31920E5LL, class_a);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x45624A3AE7810DA9LL, class_c);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x14698A85431CD40CLL, class_b);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x71EC76BCC66E94CFLL, class_f);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x333208BF21A873C1LL, class_g);
      HASH_GET_CLASS_CONSTANT(0x62D2775E17EE82D1LL, base);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x2E556997DA64C423LL, class_e);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x06A7DC3D42BB28B4LL, class_d);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x7F5A4A99F31920E5LL, class_a);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x45624A3AE7810DA9LL, class_c);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x14698A85431CD40CLL, class_b);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x71EC76BCC66E94CFLL, class_f);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
