
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_doubled_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_doubled_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.fw.h>

// Declarations
#include <cls/class_a.h>
#include <cls/class_b.h>
#include <cls/class_c.h>
#include <cls/class_d.h>
#include <cls/class_e.h>
#include <cls/class_f.h>
#include <cls/class_g.h>
#include <cls/if_a.h>
#include <cls/if_b.h>
#include <cls/if_c.h>
#include <cls/if_d.h>
#include <cls/if_e.h>
#include <cls/if_f.h>
#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_doubled_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_class_a(CArrRef params, bool init = true);
Object co_class_b(CArrRef params, bool init = true);
Object co_class_c(CArrRef params, bool init = true);
Object co_class_d(CArrRef params, bool init = true);
Object co_class_e(CArrRef params, bool init = true);
Object co_class_f(CArrRef params, bool init = true);
Object co_class_g(CArrRef params, bool init = true);
Object co_base(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_doubled_h__
