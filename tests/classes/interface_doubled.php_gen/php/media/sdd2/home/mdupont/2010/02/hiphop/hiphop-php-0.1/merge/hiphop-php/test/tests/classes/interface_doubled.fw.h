
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_doubled_fw_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_doubled_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(class_a)
FORWARD_DECLARE_CLASS(class_b)
FORWARD_DECLARE_CLASS(class_c)
FORWARD_DECLARE_CLASS(class_d)
FORWARD_DECLARE_CLASS(class_e)
FORWARD_DECLARE_CLASS(class_f)
FORWARD_DECLARE_CLASS(class_g)
FORWARD_DECLARE_CLASS(if_a);
FORWARD_DECLARE_CLASS(if_b);
FORWARD_DECLARE_CLASS(if_c);
FORWARD_DECLARE_CLASS(if_d);
FORWARD_DECLARE_CLASS(if_e);
FORWARD_DECLARE_CLASS(if_f);
FORWARD_DECLARE_CLASS(base)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_doubled_fw_h__
