
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 34 */
Variant c_class_a::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_class_a::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_class_a::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_class_a::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_class_a::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_class_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_class_a::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_class_a::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(class_a)
ObjectData *c_class_a::cloneImpl() {
  c_class_a *obj = NEW(c_class_a)();
  cloneSet(obj);
  return obj;
}
void c_class_a::cloneSet(c_class_a *clone) {
  c_base::cloneSet(clone);
}
Variant c_class_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_class_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class_a$os_get(const char *s) {
  return c_class_a::os_get(s, -1);
}
Variant &cw_class_a$os_lval(const char *s) {
  return c_class_a::os_lval(s, -1);
}
Variant cw_class_a$os_constant(const char *s) {
  return c_class_a::os_constant(s);
}
Variant cw_class_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class_a::os_invoke(c, s, params, -1, fatal);
}
void c_class_a::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 35 */
void c_class_a::t_f_a() {
  INSTANCE_METHOD_INJECTION(class_a, class_a::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 36 */
void c_class_a::t_f_b() {
  INSTANCE_METHOD_INJECTION(class_a, class_a::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 37 */
void c_class_a::t_f_c() {
  INSTANCE_METHOD_INJECTION(class_a, class_a::f_c);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 38 */
void c_class_a::t_f_d() {
  INSTANCE_METHOD_INJECTION(class_a, class_a::f_d);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 39 */
void c_class_a::t_f_e() {
  INSTANCE_METHOD_INJECTION(class_a, class_a::f_e);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 51 */
Variant c_class_b::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_class_b::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_class_b::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_class_b::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_class_b::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_class_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_class_b::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_class_b::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(class_b)
ObjectData *c_class_b::cloneImpl() {
  c_class_b *obj = NEW(c_class_b)();
  cloneSet(obj);
  return obj;
}
void c_class_b::cloneSet(c_class_b *clone) {
  c_base::cloneSet(clone);
}
Variant c_class_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_class_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class_b$os_get(const char *s) {
  return c_class_b::os_get(s, -1);
}
Variant &cw_class_b$os_lval(const char *s) {
  return c_class_b::os_lval(s, -1);
}
Variant cw_class_b$os_constant(const char *s) {
  return c_class_b::os_constant(s);
}
Variant cw_class_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class_b::os_invoke(c, s, params, -1, fatal);
}
void c_class_b::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 52 */
void c_class_b::t_f_a() {
  INSTANCE_METHOD_INJECTION(class_b, class_b::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 53 */
void c_class_b::t_f_b() {
  INSTANCE_METHOD_INJECTION(class_b, class_b::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 54 */
void c_class_b::t_f_c() {
  INSTANCE_METHOD_INJECTION(class_b, class_b::f_c);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 55 */
void c_class_b::t_f_d() {
  INSTANCE_METHOD_INJECTION(class_b, class_b::f_d);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 56 */
void c_class_b::t_f_e() {
  INSTANCE_METHOD_INJECTION(class_b, class_b::f_e);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 68 */
Variant c_class_c::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_class_c::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_class_c::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_class_c::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_class_c::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_class_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_class_c::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_class_c::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(class_c)
ObjectData *c_class_c::cloneImpl() {
  c_class_c *obj = NEW(c_class_c)();
  cloneSet(obj);
  return obj;
}
void c_class_c::cloneSet(c_class_c *clone) {
  c_base::cloneSet(clone);
}
Variant c_class_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      HASH_GUARD(0x5AD935177983491DLL, f_c) {
        return (t_f_c(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_class_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      HASH_GUARD(0x5AD935177983491DLL, f_c) {
        return (t_f_c(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class_c$os_get(const char *s) {
  return c_class_c::os_get(s, -1);
}
Variant &cw_class_c$os_lval(const char *s) {
  return c_class_c::os_lval(s, -1);
}
Variant cw_class_c$os_constant(const char *s) {
  return c_class_c::os_constant(s);
}
Variant cw_class_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class_c::os_invoke(c, s, params, -1, fatal);
}
void c_class_c::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 69 */
void c_class_c::t_f_a() {
  INSTANCE_METHOD_INJECTION(class_c, class_c::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 70 */
void c_class_c::t_f_b() {
  INSTANCE_METHOD_INJECTION(class_c, class_c::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 71 */
void c_class_c::t_f_c() {
  INSTANCE_METHOD_INJECTION(class_c, class_c::f_c);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 72 */
void c_class_c::t_f_d() {
  INSTANCE_METHOD_INJECTION(class_c, class_c::f_d);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 73 */
void c_class_c::t_f_e() {
  INSTANCE_METHOD_INJECTION(class_c, class_c::f_e);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 85 */
Variant c_class_d::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_class_d::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_class_d::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_class_d::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_class_d::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_class_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_class_d::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_class_d::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(class_d)
ObjectData *c_class_d::cloneImpl() {
  c_class_d *obj = NEW(c_class_d)();
  cloneSet(obj);
  return obj;
}
void c_class_d::cloneSet(c_class_d *clone) {
  c_base::cloneSet(clone);
}
Variant c_class_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_class_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class_d$os_get(const char *s) {
  return c_class_d::os_get(s, -1);
}
Variant &cw_class_d$os_lval(const char *s) {
  return c_class_d::os_lval(s, -1);
}
Variant cw_class_d$os_constant(const char *s) {
  return c_class_d::os_constant(s);
}
Variant cw_class_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class_d::os_invoke(c, s, params, -1, fatal);
}
void c_class_d::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 86 */
void c_class_d::t_f_a() {
  INSTANCE_METHOD_INJECTION(class_d, class_d::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 87 */
void c_class_d::t_f_b() {
  INSTANCE_METHOD_INJECTION(class_d, class_d::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 88 */
void c_class_d::t_f_c() {
  INSTANCE_METHOD_INJECTION(class_d, class_d::f_c);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 89 */
void c_class_d::t_f_d() {
  INSTANCE_METHOD_INJECTION(class_d, class_d::f_d);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 90 */
void c_class_d::t_f_e() {
  INSTANCE_METHOD_INJECTION(class_d, class_d::f_e);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 102 */
Variant c_class_e::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_class_e::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_class_e::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_class_e::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_class_e::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_class_e::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_class_e::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_class_e::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(class_e)
ObjectData *c_class_e::cloneImpl() {
  c_class_e *obj = NEW(c_class_e)();
  cloneSet(obj);
  return obj;
}
void c_class_e::cloneSet(c_class_e *clone) {
  c_base::cloneSet(clone);
}
Variant c_class_e::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    case 13:
      HASH_GUARD(0x5AD935177983491DLL, f_c) {
        return (t_f_c(), null);
      }
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_class_e::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    case 13:
      HASH_GUARD(0x5AD935177983491DLL, f_c) {
        return (t_f_c(), null);
      }
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class_e::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class_e$os_get(const char *s) {
  return c_class_e::os_get(s, -1);
}
Variant &cw_class_e$os_lval(const char *s) {
  return c_class_e::os_lval(s, -1);
}
Variant cw_class_e$os_constant(const char *s) {
  return c_class_e::os_constant(s);
}
Variant cw_class_e$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class_e::os_invoke(c, s, params, -1, fatal);
}
void c_class_e::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 103 */
void c_class_e::t_f_a() {
  INSTANCE_METHOD_INJECTION(class_e, class_e::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 104 */
void c_class_e::t_f_b() {
  INSTANCE_METHOD_INJECTION(class_e, class_e::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 105 */
void c_class_e::t_f_c() {
  INSTANCE_METHOD_INJECTION(class_e, class_e::f_c);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 106 */
void c_class_e::t_f_d() {
  INSTANCE_METHOD_INJECTION(class_e, class_e::f_d);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 107 */
void c_class_e::t_f_e() {
  INSTANCE_METHOD_INJECTION(class_e, class_e::f_e);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 119 */
Variant c_class_f::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_class_f::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_class_f::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_class_f::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_class_f::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_class_f::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_class_f::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_class_f::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(class_f)
ObjectData *c_class_f::cloneImpl() {
  c_class_f *obj = NEW(c_class_f)();
  cloneSet(obj);
  return obj;
}
void c_class_f::cloneSet(c_class_f *clone) {
  c_base::cloneSet(clone);
}
Variant c_class_f::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_class_f::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class_f::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class_f$os_get(const char *s) {
  return c_class_f::os_get(s, -1);
}
Variant &cw_class_f$os_lval(const char *s) {
  return c_class_f::os_lval(s, -1);
}
Variant cw_class_f$os_constant(const char *s) {
  return c_class_f::os_constant(s);
}
Variant cw_class_f$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class_f::os_invoke(c, s, params, -1, fatal);
}
void c_class_f::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 120 */
void c_class_f::t_f_a() {
  INSTANCE_METHOD_INJECTION(class_f, class_f::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 121 */
void c_class_f::t_f_b() {
  INSTANCE_METHOD_INJECTION(class_f, class_f::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 122 */
void c_class_f::t_f_c() {
  INSTANCE_METHOD_INJECTION(class_f, class_f::f_c);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 123 */
void c_class_f::t_f_d() {
  INSTANCE_METHOD_INJECTION(class_f, class_f::f_d);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 124 */
void c_class_f::t_f_e() {
  INSTANCE_METHOD_INJECTION(class_f, class_f::f_e);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 136 */
Variant c_class_g::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_class_g::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_class_g::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_class_g::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_class_g::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_class_g::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_class_g::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_class_g::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(class_g)
ObjectData *c_class_g::cloneImpl() {
  c_class_g *obj = NEW(c_class_g)();
  cloneSet(obj);
  return obj;
}
void c_class_g::cloneSet(c_class_g *clone) {
  c_base::cloneSet(clone);
}
Variant c_class_g::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    case 13:
      HASH_GUARD(0x5AD935177983491DLL, f_c) {
        return (t_f_c(), null);
      }
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_class_g::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    case 13:
      HASH_GUARD(0x5AD935177983491DLL, f_c) {
        return (t_f_c(), null);
      }
      HASH_GUARD(0x2E4E7981BD91290DLL, f_d) {
        return (t_f_d(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class_g::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class_g$os_get(const char *s) {
  return c_class_g::os_get(s, -1);
}
Variant &cw_class_g$os_lval(const char *s) {
  return c_class_g::os_lval(s, -1);
}
Variant cw_class_g$os_constant(const char *s) {
  return c_class_g::os_constant(s);
}
Variant cw_class_g$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class_g::os_invoke(c, s, params, -1, fatal);
}
void c_class_g::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 137 */
void c_class_g::t_f_a() {
  INSTANCE_METHOD_INJECTION(class_g, class_g::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 138 */
void c_class_g::t_f_b() {
  INSTANCE_METHOD_INJECTION(class_g, class_g::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 139 */
void c_class_g::t_f_c() {
  INSTANCE_METHOD_INJECTION(class_g, class_g::f_c);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 140 */
void c_class_g::t_f_d() {
  INSTANCE_METHOD_INJECTION(class_g, class_g::f_d);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 141 */
void c_class_g::t_f_e() {
  INSTANCE_METHOD_INJECTION(class_g, class_g::f_e);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 26 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 27 */
void c_base::t_test(CVarRef v_class) {
  INSTANCE_METHOD_INJECTION(base, base::test);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  echo(LINE(28,(assignCallTemp(eo_1, toString(x_get_class(((Object)(this))))),assignCallTemp(eo_2, concat3(", ", toString(v_class), ") ")),assignCallTemp(eo_3, (toString((instanceOf(((Object)(this)), toString(v_class))) ? (("yes\n")) : (("no\n"))))),concat4("is_a(", eo_1, eo_2, eo_3))));
} /* function */
Object co_class_a(CArrRef params, bool init /* = true */) {
  return Object(p_class_a(NEW(c_class_a)())->dynCreate(params, init));
}
Object co_class_b(CArrRef params, bool init /* = true */) {
  return Object(p_class_b(NEW(c_class_b)())->dynCreate(params, init));
}
Object co_class_c(CArrRef params, bool init /* = true */) {
  return Object(p_class_c(NEW(c_class_c)())->dynCreate(params, init));
}
Object co_class_d(CArrRef params, bool init /* = true */) {
  return Object(p_class_d(NEW(c_class_d)())->dynCreate(params, init));
}
Object co_class_e(CArrRef params, bool init /* = true */) {
  return Object(p_class_e(NEW(c_class_e)())->dynCreate(params, init));
}
Object co_class_f(CArrRef params, bool init /* = true */) {
  return Object(p_class_f(NEW(c_class_f)())->dynCreate(params, init));
}
Object co_class_g(CArrRef params, bool init /* = true */) {
  return Object(p_class_g(NEW(c_class_g)())->dynCreate(params, init));
}
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_doubled_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_doubled_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  echo("class_a\n");
  (v_t = ((Object)(LINE(42,p_class_a(p_class_a(NEWOBJ(c_class_a)())->create())))));
  echo(toString(LINE(43,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_a"))));
  echo(toString(LINE(44,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_b"))));
  echo(toString(LINE(45,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_c"))));
  echo(toString(LINE(46,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_d"))));
  echo(toString(LINE(47,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_e"))));
  echo("class_b\n");
  (v_t = ((Object)(LINE(59,p_class_b(p_class_b(NEWOBJ(c_class_b)())->create())))));
  echo(toString(LINE(60,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_a"))));
  echo(toString(LINE(61,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_b"))));
  echo(toString(LINE(62,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_c"))));
  echo(toString(LINE(63,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_d"))));
  echo(toString(LINE(64,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_e"))));
  echo("class_c\n");
  (v_t = ((Object)(LINE(76,p_class_c(p_class_c(NEWOBJ(c_class_c)())->create())))));
  echo(toString(LINE(77,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_a"))));
  echo(toString(LINE(78,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_b"))));
  echo(toString(LINE(79,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_c"))));
  echo(toString(LINE(80,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_d"))));
  echo(toString(LINE(81,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_e"))));
  echo("class_d\n");
  (v_t = ((Object)(LINE(93,p_class_d(p_class_d(NEWOBJ(c_class_d)())->create())))));
  echo(toString(LINE(94,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_a"))));
  echo(toString(LINE(95,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_b"))));
  echo(toString(LINE(96,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_c"))));
  echo(toString(LINE(97,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_d"))));
  echo(toString(LINE(98,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_e"))));
  echo("class_e\n");
  (v_t = ((Object)(LINE(110,p_class_e(p_class_e(NEWOBJ(c_class_e)())->create())))));
  echo(toString(LINE(111,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_a"))));
  echo(toString(LINE(112,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_b"))));
  echo(toString(LINE(113,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_c"))));
  echo(toString(LINE(114,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_d"))));
  echo(toString(LINE(115,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_e"))));
  echo("class_f\n");
  (v_t = ((Object)(LINE(127,p_class_f(p_class_f(NEWOBJ(c_class_f)())->create())))));
  echo(toString(LINE(128,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_a"))));
  echo(toString(LINE(129,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_b"))));
  echo(toString(LINE(130,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_c"))));
  echo(toString(LINE(131,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_d"))));
  echo(toString(LINE(132,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_e"))));
  echo("class_g\n");
  (v_t = ((Object)(LINE(144,p_class_g(p_class_g(NEWOBJ(c_class_g)())->create())))));
  echo(toString(LINE(145,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_a"))));
  echo(toString(LINE(146,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_b"))));
  echo(toString(LINE(147,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_c"))));
  echo(toString(LINE(148,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_d"))));
  echo(toString(LINE(149,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, "if_e"))));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
