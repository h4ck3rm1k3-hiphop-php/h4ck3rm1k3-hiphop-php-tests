
#ifndef __GENERATED_cls_class_d_h__
#define __GENERATED_cls_class_d_h__

#include <cls/base.h>
#include <cls/if_d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 85 */
class c_class_d : virtual public c_base, virtual public c_if_d {
  BEGIN_CLASS_MAP(class_d)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_d)
  END_CLASS_MAP(class_d)
  DECLARE_CLASS(class_d, class_d, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
  public: void t_f_c();
  public: void t_f_d();
  public: void t_f_e();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class_d_h__
