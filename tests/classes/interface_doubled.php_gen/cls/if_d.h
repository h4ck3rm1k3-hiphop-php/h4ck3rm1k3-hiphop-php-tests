
#ifndef __GENERATED_cls_if_d_h__
#define __GENERATED_cls_if_d_h__

#include <cls/if_a.h>
#include <cls/if_b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 15 */
class c_if_d : virtual public c_if_a, virtual public c_if_b {
  // public: void t_f_d() = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_if_d_h__
