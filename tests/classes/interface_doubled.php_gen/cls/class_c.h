
#ifndef __GENERATED_cls_class_c_h__
#define __GENERATED_cls_class_c_h__

#include <cls/base.h>
#include <cls/if_c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 68 */
class c_class_c : virtual public c_base, virtual public c_if_c {
  BEGIN_CLASS_MAP(class_c)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_c)
  END_CLASS_MAP(class_c)
  DECLARE_CLASS(class_c, class_c, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
  public: void t_f_c();
  public: void t_f_d();
  public: void t_f_e();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class_c_h__
