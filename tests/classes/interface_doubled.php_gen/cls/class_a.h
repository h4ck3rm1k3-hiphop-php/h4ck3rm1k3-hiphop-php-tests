
#ifndef __GENERATED_cls_class_a_h__
#define __GENERATED_cls_class_a_h__

#include <cls/base.h>
#include <cls/if_a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 34 */
class c_class_a : virtual public c_base, virtual public c_if_a {
  BEGIN_CLASS_MAP(class_a)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
  END_CLASS_MAP(class_a)
  DECLARE_CLASS(class_a, class_a, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
  public: void t_f_c();
  public: void t_f_d();
  public: void t_f_e();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class_a_h__
