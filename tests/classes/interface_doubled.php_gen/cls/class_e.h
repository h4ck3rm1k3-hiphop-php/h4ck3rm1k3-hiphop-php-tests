
#ifndef __GENERATED_cls_class_e_h__
#define __GENERATED_cls_class_e_h__

#include <cls/base.h>
#include <cls/if_a.h>
#include <cls/if_b.h>
#include <cls/if_c.h>
#include <cls/if_d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 102 */
class c_class_e : virtual public c_base, virtual public c_if_a, virtual public c_if_b, virtual public c_if_c, virtual public c_if_d {
  BEGIN_CLASS_MAP(class_e)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_c)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_d)
  END_CLASS_MAP(class_e)
  DECLARE_CLASS(class_e, class_e, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
  public: void t_f_c();
  public: void t_f_d();
  public: void t_f_e();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class_e_h__
