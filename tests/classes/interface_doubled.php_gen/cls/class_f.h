
#ifndef __GENERATED_cls_class_f_h__
#define __GENERATED_cls_class_f_h__

#include <cls/base.h>
#include <cls/if_e.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 119 */
class c_class_f : virtual public c_base, virtual public c_if_e {
  BEGIN_CLASS_MAP(class_f)
    PARENT_CLASS(base)
    PARENT_CLASS(if_e)
  END_CLASS_MAP(class_f)
  DECLARE_CLASS(class_f, class_f, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
  public: void t_f_c();
  public: void t_f_d();
  public: void t_f_e();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class_f_h__
