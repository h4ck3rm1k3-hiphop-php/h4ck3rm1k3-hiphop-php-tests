
#ifndef __GENERATED_cls_if_f_h__
#define __GENERATED_cls_if_f_h__

#include <cls/if_a.h>
#include <cls/if_b.h>
#include <cls/if_c.h>
#include <cls/if_d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 23 */
class c_if_f : virtual public c_if_a, virtual public c_if_b, virtual public c_if_c, virtual public c_if_d {
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_if_f_h__
