
#ifndef __GENERATED_cls_class_b_h__
#define __GENERATED_cls_class_b_h__

#include <cls/base.h>
#include <cls/if_a.h>
#include <cls/if_b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 51 */
class c_class_b : virtual public c_base, virtual public c_if_a, virtual public c_if_b {
  BEGIN_CLASS_MAP(class_b)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
  END_CLASS_MAP(class_b)
  DECLARE_CLASS(class_b, class_b, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
  public: void t_f_c();
  public: void t_f_d();
  public: void t_f_e();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class_b_h__
