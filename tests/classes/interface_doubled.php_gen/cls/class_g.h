
#ifndef __GENERATED_cls_class_g_h__
#define __GENERATED_cls_class_g_h__

#include <cls/base.h>
#include <cls/if_f.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_doubled.php line 136 */
class c_class_g : virtual public c_base, virtual public c_if_f {
  BEGIN_CLASS_MAP(class_g)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_c)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(if_d)
    PARENT_CLASS(if_f)
  END_CLASS_MAP(class_g)
  DECLARE_CLASS(class_g, class_g, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
  public: void t_f_c();
  public: void t_f_d();
  public: void t_f_e();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class_g_h__
