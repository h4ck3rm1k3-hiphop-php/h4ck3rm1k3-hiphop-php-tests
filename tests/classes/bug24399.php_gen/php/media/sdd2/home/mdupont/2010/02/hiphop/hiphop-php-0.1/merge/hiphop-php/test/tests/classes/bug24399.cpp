
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug24399.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug24399.php line 2 */
Variant c_dooh::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_dooh::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_dooh::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("blah", m_blah));
  c_ObjectData::o_get(props);
}
bool c_dooh::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x49B1816976A1D113LL, blah, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_dooh::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x49B1816976A1D113LL, m_blah,
                         blah, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_dooh::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x49B1816976A1D113LL, m_blah,
                      blah, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_dooh::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_dooh::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(dooh)
ObjectData *c_dooh::cloneImpl() {
  c_dooh *obj = NEW(c_dooh)();
  cloneSet(obj);
  return obj;
}
void c_dooh::cloneSet(c_dooh *clone) {
  clone->m_blah = m_blah;
  ObjectData::cloneSet(clone);
}
Variant c_dooh::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_dooh::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_dooh::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_dooh$os_get(const char *s) {
  return c_dooh::os_get(s, -1);
}
Variant &cw_dooh$os_lval(const char *s) {
  return c_dooh::os_lval(s, -1);
}
Variant cw_dooh$os_constant(const char *s) {
  return c_dooh::os_constant(s);
}
Variant cw_dooh$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_dooh::os_invoke(c, s, params, -1, fatal);
}
void c_dooh::init() {
  m_blah = null;
}
Object co_dooh(CArrRef params, bool init /* = true */) {
  return Object(p_dooh(NEW(c_dooh)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$bug24399_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug24399.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$bug24399_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  (v_d = ((Object)(LINE(5,p_dooh(p_dooh(NEWOBJ(c_dooh)())->create())))));
  LINE(6,x_var_dump(1, x_is_subclass_of(v_d, "dooh")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
