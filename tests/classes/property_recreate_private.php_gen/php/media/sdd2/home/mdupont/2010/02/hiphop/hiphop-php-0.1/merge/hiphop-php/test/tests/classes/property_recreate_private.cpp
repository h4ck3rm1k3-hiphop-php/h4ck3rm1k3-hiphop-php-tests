
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/property_recreate_private.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/property_recreate_private.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("p", m_p.isReferenced() ? ref(m_p) : m_p));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x77F632A4E34F1526LL, p, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x77F632A4E34F1526LL, m_p,
                         p, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x77F632A4E34F1526LL, m_p,
                      p, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x77F632A4E34F1526LL, m_p,
                         p, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_p = m_p.isReferenced() ? ref(m_p) : m_p;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x4D3F1AA89A342B74LL, setprivate) {
        return (t_setprivate(), null);
      }
      HASH_GUARD(0x0B761156C0E2DCC8LL, unsetprivate) {
        return (t_unsetprivate(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x4D3F1AA89A342B74LL, setprivate) {
        return (t_setprivate(), null);
      }
      HASH_GUARD(0x0B761156C0E2DCC8LL, unsetprivate) {
        return (t_unsetprivate(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_p = "test";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/property_recreate_private.php line 4 */
void c_c::t_unsetprivate() {
  INSTANCE_METHOD_INJECTION(C, C::unsetPrivate);
  DECLARE_GLOBAL_VARIABLES(g);
  t___unset("p");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/property_recreate_private.php line 7 */
void c_c::t_setprivate() {
  INSTANCE_METHOD_INJECTION(C, C::setPrivate);
  (m_p = "changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/property_recreate_private.php line 12 */
Variant c_d::os_get(const char *s, int64 hash) {
  return c_c::os_get(s, hash);
}
Variant &c_d::os_lval(const char *s, int64 hash) {
  return c_c::os_lval(s, hash);
}
void c_d::o_get(ArrayElementVec &props) const {
  c_c::o_get(props);
}
bool c_d::o_exists(CStrRef s, int64 hash) const {
  return c_c::o_exists(s, hash);
}
Variant c_d::o_get(CStrRef s, int64 hash) {
  return c_c::o_get(s, hash);
}
Variant c_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_c::o_set(s, hash, v, forInit);
}
Variant &c_d::o_lval(CStrRef s, int64 hash) {
  return c_c::o_lval(s, hash);
}
Variant c_d::os_constant(const char *s) {
  return c_c::os_constant(s);
}
IMPLEMENT_CLASS(d)
ObjectData *c_d::cloneImpl() {
  c_d *obj = NEW(c_d)();
  cloneSet(obj);
  return obj;
}
void c_d::cloneSet(c_d *clone) {
  c_c::cloneSet(clone);
}
Variant c_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x0B761156C0E2DCC8LL, unsetprivate) {
        return (t_unsetprivate(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x4D3F1AA89A342B74LL, setprivate) {
        return (t_setprivate(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x116DDB0655F7688DLL, setp) {
        return (t_setp(), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke(s, params, hash, fatal);
}
Variant c_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x0B761156C0E2DCC8LL, unsetprivate) {
        return (t_unsetprivate(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x4D3F1AA89A342B74LL, setprivate) {
        return (t_setprivate(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x116DDB0655F7688DLL, setp) {
        return (t_setp(), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_c::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d$os_get(const char *s) {
  return c_d::os_get(s, -1);
}
Variant &cw_d$os_lval(const char *s) {
  return c_d::os_lval(s, -1);
}
Variant cw_d$os_constant(const char *s) {
  return c_d::os_constant(s);
}
Variant cw_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d::os_invoke(c, s, params, -1, fatal);
}
void c_d::init() {
  c_c::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/property_recreate_private.php line 13 */
void c_d::t_setp() {
  INSTANCE_METHOD_INJECTION(D, D::setP);
  (m_p = "changed in D");
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d(CArrRef params, bool init /* = true */) {
  return Object(p_d(NEW(c_d)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$property_recreate_private_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/property_recreate_private.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$property_recreate_private_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("Unset and recreate a superclass's private property:\n");
  (v_d = ((Object)(LINE(19,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(20,v_d.o_invoke_few_args("unsetPrivate", 0x0B761156C0E2DCC8LL, 0));
  LINE(21,v_d.o_invoke_few_args("setPrivate", 0x4D3F1AA89A342B74LL, 0));
  LINE(22,x_var_dump(1, v_d));
  echo("\nUnset superclass's private property, and recreate it as public in subclass:\n");
  (v_d = ((Object)(LINE(25,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(26,v_d.o_invoke_few_args("unsetPrivate", 0x0B761156C0E2DCC8LL, 0));
  LINE(27,v_d.o_invoke_few_args("setP", 0x116DDB0655F7688DLL, 0));
  LINE(28,x_var_dump(1, v_d));
  echo("\nUnset superclass's private property, and recreate it as public at global scope:\n");
  (v_d = ((Object)(LINE(31,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(32,v_d.o_invoke_few_args("unsetPrivate", 0x0B761156C0E2DCC8LL, 0));
  (v_d.o_lval("p", 0x77F632A4E34F1526LL) = "this will create a public property");
  LINE(34,x_var_dump(1, v_d));
  echo("\n\nUnset and recreate a private property:\n");
  (v_c = ((Object)(LINE(38,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(39,v_c.o_invoke_few_args("unsetPrivate", 0x0B761156C0E2DCC8LL, 0));
  LINE(40,v_c.o_invoke_few_args("setPrivate", 0x4D3F1AA89A342B74LL, 0));
  LINE(41,x_var_dump(1, v_c));
  echo("\nUnset a private property, and attempt to recreate at global scope (expecting failure):\n");
  (v_c = ((Object)(LINE(44,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(45,v_c.o_invoke_few_args("unsetPrivate", 0x0B761156C0E2DCC8LL, 0));
  (v_c.o_lval("p", 0x77F632A4E34F1526LL) = "this will fail");
  LINE(47,x_var_dump(1, v_c));
  echo("==Done==\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
