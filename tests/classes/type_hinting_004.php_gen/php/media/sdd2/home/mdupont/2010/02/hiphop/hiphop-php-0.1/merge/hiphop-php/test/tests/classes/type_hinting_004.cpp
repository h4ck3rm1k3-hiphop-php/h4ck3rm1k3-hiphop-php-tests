
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 9 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 25 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        int count = params.size();
        if (count <= 0) return (ti_f2(o_getClassName()), null);
        return (ti_f2(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (ti_f1(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        if (count <= 0) return (ti_f2(o_getClassName()), null);
        return (ti_f2(o_getClassName(), a0), null);
      }
      break;
    case 3:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (ti_f1(o_getClassName(), a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        int count = params.size();
        if (count <= 0) return (ti_f2(c), null);
        return (ti_f2(c, params.rvalAt(0)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (ti_f1(c, params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 26 */
void c_c::ti_f1(const char* cls, p_a v_a) {
  STATIC_METHOD_INJECTION(C, C::f1);
  Variant v_this;

  if (isset(v_this)) {
    echo("in C::f1 (instance);\n");
  }
  else {
    echo("in C::f1 (static);\n");
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 33 */
void c_c::ti_f2(const char* cls, CVarRef v_a //  = null_variant
) {
  STATIC_METHOD_INJECTION(C, C::f2);
  Variant v_this;

  if (isset(v_this)) {
    echo("in C::f2 (instance);\n");
  }
  else {
    echo("in C::f2 (static);\n");
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 50 */
Variant c_d::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_d::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_d::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_d::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_d::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_d::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_d::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(d)
ObjectData *c_d::cloneImpl() {
  c_d *obj = NEW(c_d)();
  cloneSet(obj);
  return obj;
}
void c_d::cloneSet(c_d *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        int count = params.size();
        if (count <= 0) return (t_f2(), null);
        return (t_f2(params.rvalAt(0)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        if (count <= 0) return (t_f2(), null);
        return (t_f2(a0), null);
      }
      break;
    case 3:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d$os_get(const char *s) {
  return c_d::os_get(s, -1);
}
Variant &cw_d$os_lval(const char *s) {
  return c_d::os_lval(s, -1);
}
Variant cw_d$os_constant(const char *s) {
  return c_d::os_constant(s);
}
Variant cw_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d::os_invoke(c, s, params, -1, fatal);
}
void c_d::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 51 */
void c_d::t_f1(p_a v_a) {
  INSTANCE_METHOD_INJECTION(D, D::f1);
  if (isset(((Object)(this)))) {
    echo("in C::f1 (instance);\n");
  }
  else {
    echo("in C::f1 (static);\n");
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 58 */
void c_d::t_f2(CVarRef v_a //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(D, D::f2);
  if (isset(((Object)(this)))) {
    echo("in C::f2 (instance);\n");
  }
  else {
    echo("in C::f2 (static);\n");
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 10 */
void f_f1(p_a v_a) {
  FUNCTION_INJECTION(f1);
  echo("in f1;\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 3 */
bool f_myerrorhandler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline) {
  FUNCTION_INJECTION(myErrorHandler);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  echo(LINE(4,(assignCallTemp(eo_0, toString(v_errno)),assignCallTemp(eo_2, concat6(toString(v_errstr), " - ", toString(v_errfile), "(", toString(v_errline), ")\n")),concat3(eo_0, ": ", eo_2))));
  return true;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 13 */
void f_f2(CVarRef v_a //  = null_variant
) {
  FUNCTION_INJECTION(f2);
  echo("in f2;\n");
} /* function */
Variant i_f1(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
    return (f_f1(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_myerrorhandler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x32D66CB21D0498C5LL, myerrorhandler) {
    return (f_myerrorhandler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_f2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
    int count = params.size();
    if (count <= 0) return (f_f2(), null);
    return (f_f2(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d(CArrRef params, bool init /* = true */) {
  return Object(p_d(NEW(c_d)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$type_hinting_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$type_hinting_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  LINE(2,x_set_error_handler("myErrorHandler", toInt32(4096LL) /* E_RECOVERABLE_ERROR */));
  echo("---> Type hints with callback function:\n");
  LINE(16,x_call_user_func(2, "f1", ScalarArrays::sa_[0]));
  LINE(17,(assignCallTemp(eo_1, ((Object)(p_a(p_a(NEWOBJ(c_a)())->create())))),x_call_user_func(2, "f1", Array(ArrayInit(1).set(0, eo_1).create()))));
  LINE(18,x_call_user_func(2, "f2", ScalarArrays::sa_[0]));
  LINE(19,x_call_user_func(1, "f2"));
  LINE(20,(assignCallTemp(eo_1, ((Object)(p_a(p_a(NEWOBJ(c_a)())->create())))),x_call_user_func(2, "f2", Array(ArrayInit(1).set(0, eo_1).create()))));
  LINE(21,x_call_user_func(2, "f2", ScalarArrays::sa_[1]));
  echo("\n\n---> Type hints with callback static method:\n");
  LINE(41,x_call_user_func(2, ScalarArrays::sa_[2], ScalarArrays::sa_[0]));
  LINE(42,(assignCallTemp(eo_1, ((Object)(p_a(p_a(NEWOBJ(c_a)())->create())))),x_call_user_func(2, ScalarArrays::sa_[2], Array(ArrayInit(1).set(0, eo_1).create()))));
  LINE(43,x_call_user_func(2, ScalarArrays::sa_[3], ScalarArrays::sa_[0]));
  LINE(44,x_call_user_func(1, ScalarArrays::sa_[3]));
  LINE(45,(assignCallTemp(eo_1, ((Object)(p_a(p_a(NEWOBJ(c_a)())->create())))),x_call_user_func(2, ScalarArrays::sa_[3], Array(ArrayInit(1).set(0, eo_1).create()))));
  LINE(46,x_call_user_func(2, ScalarArrays::sa_[3], ScalarArrays::sa_[1]));
  echo("\n\n---> Type hints with callback instance method:\n");
  (v_d = ((Object)(LINE(66,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(67,x_call_user_func(2, Array(ArrayInit(2).set(0, v_d).set(1, "f1").create()), ScalarArrays::sa_[0]));
  LINE(68,(assignCallTemp(eo_0, Array(ArrayInit(2).set(0, v_d).set(1, "f1").create())),assignCallTemp(eo_1, ((Object)(p_a(p_a(NEWOBJ(c_a)())->create())))),x_call_user_func(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))));
  LINE(69,x_call_user_func(2, Array(ArrayInit(2).set(0, v_d).set(1, "f2").create()), ScalarArrays::sa_[0]));
  LINE(70,x_call_user_func(1, Array(ArrayInit(2).set(0, v_d).set(1, "f2").create())));
  LINE(71,(assignCallTemp(eo_0, Array(ArrayInit(2).set(0, v_d).set(1, "f2").create())),assignCallTemp(eo_1, ((Object)(p_a(p_a(NEWOBJ(c_a)())->create())))),x_call_user_func(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))));
  LINE(72,x_call_user_func(2, Array(ArrayInit(2).set(0, v_d).set(1, "f2").create()), ScalarArrays::sa_[1]));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
