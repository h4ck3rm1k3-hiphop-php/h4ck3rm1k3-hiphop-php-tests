
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_004.php line 25 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: static void ti_f1(const char* cls, p_a v_a);
  public: static void ti_f2(const char* cls, CVarRef v_a = null_variant);
  public: static void t_f1(p_a v_a) { ti_f1("c", v_a); }
  public: static void t_f2(CVarRef v_a = null_variant) { ti_f2("c", v_a); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
