
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.php line 2 */
Variant c_object::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_object::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_object::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  c_ObjectData::o_get(props);
}
bool c_object::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_object::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_object::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_object::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_object::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(object)
ObjectData *c_object::cloneImpl() {
  c_object *obj = NEW(c_object)();
  cloneSet(obj);
  return obj;
}
void c_object::cloneSet(c_object *clone) {
  clone->m_a = m_a;
  ObjectData::cloneSet(clone);
}
Variant c_object::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_object::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_object::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_object$os_get(const char *s) {
  return c_object::os_get(s, -1);
}
Variant &cw_object$os_lval(const char *s) {
  return c_object::os_lval(s, -1);
}
Variant cw_object$os_constant(const char *s) {
  return c_object::os_constant(s);
}
Variant cw_object$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_object::os_invoke(c, s, params, -1, fatal);
}
void c_object::init() {
  m_a = ScalarArrays::sa_[0];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.php line 6 */
bool c_object::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(object, object::offsetExists);
  echo(concat("object::offsetExists", LINE(7,concat3("(", toString(v_index), ")\n"))));
  return LINE(8,x_array_key_exists(v_index, m_a));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.php line 10 */
Variant c_object::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(object, object::offsetGet);
  echo(concat("object::offsetGet", LINE(11,concat3("(", toString(v_index), ")\n"))));
  return m_a.rvalAt(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.php line 10 */
Variant &c_object::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(object, object::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.php line 14 */
void c_object::t_offsetset(CVarRef v_index, CVarRef v_newval) {
  INSTANCE_METHOD_INJECTION(object, object::offsetSet);
  echo(concat("object::offsetSet", LINE(15,concat5("(", toString(v_index), ",", toString(v_newval), ")\n"))));
  return m_a.set(v_index, (v_newval));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.php line 18 */
void c_object::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(object, object::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  echo(concat("object::offsetUnset", LINE(19,concat3("(", toString(v_index), ")\n"))));
  m_a.weakRemove(v_index);
} /* function */
Object co_object(CArrRef params, bool init /* = true */) {
  return Object(p_object(NEW(c_object)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_obj = ((Object)(LINE(24,p_object(p_object(NEWOBJ(c_object)())->create())))));
  LINE(26,x_var_dump(1, v_obj.o_get("a", 0x4292CEE227B9150ALL)));
  echo("===EMPTY===\n");
  LINE(29,x_var_dump(1, empty(v_obj, 0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(30,x_var_dump(1, empty(v_obj, 1LL, 0x5BCA7C69B794F8CELL)));
  LINE(31,x_var_dump(1, empty(v_obj, 2LL, 0x486AFCC090D5F98CLL)));
  LINE(32,x_var_dump(1, empty(v_obj, "4th", 0x0F3845EA3A1DD2E8LL)));
  LINE(33,x_var_dump(1, empty(v_obj, "5th", 0x64512A94325204EELL)));
  LINE(34,x_var_dump(1, empty(v_obj, 6LL, 0x26BF47194D7E8E12LL)));
  echo("===isset===\n");
  LINE(37,x_var_dump(1, isset(v_obj, 0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(38,x_var_dump(1, isset(v_obj, 1LL, 0x5BCA7C69B794F8CELL)));
  LINE(39,x_var_dump(1, isset(v_obj, 2LL, 0x486AFCC090D5F98CLL)));
  LINE(40,x_var_dump(1, isset(v_obj, "4th", 0x0F3845EA3A1DD2E8LL)));
  LINE(41,x_var_dump(1, isset(v_obj, "5th", 0x64512A94325204EELL)));
  LINE(42,x_var_dump(1, isset(v_obj, 6LL, 0x26BF47194D7E8E12LL)));
  echo("===offsetGet===\n");
  LINE(45,x_var_dump(1, v_obj.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(46,x_var_dump(1, v_obj.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  LINE(47,x_var_dump(1, v_obj.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  LINE(48,x_var_dump(1, v_obj.rvalAt("4th", 0x0F3845EA3A1DD2E8LL)));
  LINE(49,x_var_dump(1, v_obj.rvalAt("5th", 0x64512A94325204EELL)));
  LINE(50,x_var_dump(1, v_obj.rvalAt(6LL, 0x26BF47194D7E8E12LL)));
  echo("===offsetSet===\n");
  echo("WRITE 1\n");
  v_obj.set(1LL, ("Changed 1"), 0x5BCA7C69B794F8CELL);
  LINE(55,x_var_dump(1, v_obj.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  echo("WRITE 2\n");
  v_obj.set("4th", ("Changed 4th"), 0x0F3845EA3A1DD2E8LL);
  LINE(58,x_var_dump(1, v_obj.rvalAt("4th", 0x0F3845EA3A1DD2E8LL)));
  echo("WRITE 3\n");
  v_obj.set("5th", ("Added 5th"), 0x64512A94325204EELL);
  LINE(61,x_var_dump(1, v_obj.rvalAt("5th", 0x64512A94325204EELL)));
  echo("WRITE 4\n");
  v_obj.set(6LL, ("Added 6"), 0x26BF47194D7E8E12LL);
  LINE(64,x_var_dump(1, v_obj.rvalAt(6LL, 0x26BF47194D7E8E12LL)));
  LINE(66,x_var_dump(1, v_obj.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(67,x_var_dump(1, v_obj.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  (v_x = v_obj.set(6LL, ("changed 6"), 0x26BF47194D7E8E12LL));
  LINE(70,x_var_dump(1, v_obj.rvalAt(6LL, 0x26BF47194D7E8E12LL)));
  LINE(71,x_var_dump(1, v_x));
  echo("===unset===\n");
  LINE(74,x_var_dump(1, v_obj.o_get("a", 0x4292CEE227B9150ALL)));
  v_obj.weakRemove(2LL, 0x486AFCC090D5F98CLL);
  v_obj.weakRemove("4th", 0x0F3845EA3A1DD2E8LL);
  v_obj.weakRemove(7LL, 0x7D75B33B7AEB669DLL);
  v_obj.weakRemove("8th", 0x1781CE6581A0562ELL);
  LINE(79,x_var_dump(1, v_obj.o_get("a", 0x4292CEE227B9150ALL)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
