
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__

#include <cls/b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance_006.php line 10 */
class c_c : virtual public c_b {
  BEGIN_CLASS_MAP(c)
    PARENT_CLASS(a)
    PARENT_CLASS(b)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, b)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
