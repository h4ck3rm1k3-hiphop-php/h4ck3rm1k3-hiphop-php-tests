
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 11 */
Variant c_same::os_get(const char *s, int64 hash) {
  return c_father::os_get(s, hash);
}
Variant &c_same::os_lval(const char *s, int64 hash) {
  return c_father::os_lval(s, hash);
}
void c_same::o_get(ArrayElementVec &props) const {
  c_father::o_get(props);
}
bool c_same::o_exists(CStrRef s, int64 hash) const {
  return c_father::o_exists(s, hash);
}
Variant c_same::o_get(CStrRef s, int64 hash) {
  return c_father::o_get(s, hash);
}
Variant c_same::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_father::o_set(s, hash, v, forInit);
}
Variant &c_same::o_lval(CStrRef s, int64 hash) {
  return c_father::o_lval(s, hash);
}
Variant c_same::os_constant(const char *s) {
  return c_father::os_constant(s);
}
IMPLEMENT_CLASS(same)
ObjectData *c_same::cloneImpl() {
  c_same *obj = NEW(c_same)();
  cloneSet(obj);
  return obj;
}
void c_same::cloneSet(c_same *clone) {
  c_father::cloneSet(clone);
}
Variant c_same::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x324D8162913739DBLL, f0) {
        return (t_f0(), null);
      }
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_father::o_invoke(s, params, hash, fatal);
}
Variant c_same::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x324D8162913739DBLL, f0) {
        return (t_f0(), null);
      }
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_father::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_same::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_father::os_invoke(c, s, params, hash, fatal);
}
Variant cw_same$os_get(const char *s) {
  return c_same::os_get(s, -1);
}
Variant &cw_same$os_lval(const char *s) {
  return c_same::os_lval(s, -1);
}
Variant cw_same$os_constant(const char *s) {
  return c_same::os_constant(s);
}
Variant cw_same$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_same::os_invoke(c, s, params, -1, fatal);
}
void c_same::init() {
  c_father::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 14 */
void c_same::t_f0() {
  INSTANCE_METHOD_INJECTION(same, same::f0);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 15 */
void c_same::t_f1() {
  INSTANCE_METHOD_INJECTION(same, same::f1);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 16 */
void c_same::t_f2() {
  INSTANCE_METHOD_INJECTION(same, same::f2);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 17 */
void c_same::t_f3() {
  INSTANCE_METHOD_INJECTION(same, same::f3);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 18 */
void c_same::t_f4() {
  INSTANCE_METHOD_INJECTION(same, same::f4);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 3 */
Variant c_father::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_father::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_father::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_father::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_father::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_father::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_father::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_father::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(father)
ObjectData *c_father::cloneImpl() {
  c_father *obj = NEW(c_father)();
  cloneSet(obj);
  return obj;
}
void c_father::cloneSet(c_father *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_father::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x324D8162913739DBLL, f0) {
        return (t_f0(), null);
      }
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_father::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x324D8162913739DBLL, f0) {
        return (t_f0(), null);
      }
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_father::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_father$os_get(const char *s) {
  return c_father::os_get(s, -1);
}
Variant &cw_father$os_lval(const char *s) {
  return c_father::os_lval(s, -1);
}
Variant cw_father$os_constant(const char *s) {
  return c_father::os_constant(s);
}
Variant cw_father$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_father::os_invoke(c, s, params, -1, fatal);
}
void c_father::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 4 */
void c_father::t_f0() {
  INSTANCE_METHOD_INJECTION(father, father::f0);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 5 */
void c_father::t_f1() {
  INSTANCE_METHOD_INJECTION(father, father::f1);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 6 */
void c_father::t_f2() {
  INSTANCE_METHOD_INJECTION(father, father::f2);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 7 */
void c_father::t_f3() {
  INSTANCE_METHOD_INJECTION(father, father::f3);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 8 */
void c_father::t_f4() {
  INSTANCE_METHOD_INJECTION(father, father::f4);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 21 */
Variant c_fail::os_get(const char *s, int64 hash) {
  return c_same::os_get(s, hash);
}
Variant &c_fail::os_lval(const char *s, int64 hash) {
  return c_same::os_lval(s, hash);
}
void c_fail::o_get(ArrayElementVec &props) const {
  c_same::o_get(props);
}
bool c_fail::o_exists(CStrRef s, int64 hash) const {
  return c_same::o_exists(s, hash);
}
Variant c_fail::o_get(CStrRef s, int64 hash) {
  return c_same::o_get(s, hash);
}
Variant c_fail::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_same::o_set(s, hash, v, forInit);
}
Variant &c_fail::o_lval(CStrRef s, int64 hash) {
  return c_same::o_lval(s, hash);
}
Variant c_fail::os_constant(const char *s) {
  return c_same::os_constant(s);
}
IMPLEMENT_CLASS(fail)
ObjectData *c_fail::cloneImpl() {
  c_fail *obj = NEW(c_fail)();
  cloneSet(obj);
  return obj;
}
void c_fail::cloneSet(c_fail *clone) {
  c_same::cloneSet(clone);
}
Variant c_fail::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x324D8162913739DBLL, f0) {
        return (t_f0(), null);
      }
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_same::o_invoke(s, params, hash, fatal);
}
Variant c_fail::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x324D8162913739DBLL, f0) {
        return (t_f0(), null);
      }
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_same::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fail::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_same::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fail$os_get(const char *s) {
  return c_fail::os_get(s, -1);
}
Variant &cw_fail$os_lval(const char *s) {
  return c_fail::os_lval(s, -1);
}
Variant cw_fail$os_constant(const char *s) {
  return c_fail::os_constant(s);
}
Variant cw_fail$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fail::os_invoke(c, s, params, -1, fatal);
}
void c_fail::init() {
  c_same::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 22 */
void c_fail::t_f1() {
  INSTANCE_METHOD_INJECTION(fail, fail::f1);
} /* function */
Object co_same(CArrRef params, bool init /* = true */) {
  return Object(p_same(NEW(c_same)())->dynCreate(params, init));
}
Object co_father(CArrRef params, bool init /* = true */) {
  return Object(p_father(NEW(c_father)())->dynCreate(params, init));
}
Object co_fail(CArrRef params, bool init /* = true */) {
  return Object(p_fail(NEW(c_fail)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$visibility_001a_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$visibility_001a_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
