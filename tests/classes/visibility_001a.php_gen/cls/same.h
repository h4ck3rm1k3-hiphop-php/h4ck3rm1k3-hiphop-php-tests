
#ifndef __GENERATED_cls_same_h__
#define __GENERATED_cls_same_h__

#include <cls/father.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/visibility_001a.php line 11 */
class c_same : virtual public c_father {
  BEGIN_CLASS_MAP(same)
    PARENT_CLASS(father)
  END_CLASS_MAP(same)
  DECLARE_CLASS(same, same, father)
  void init();
  public: void t_f0();
  public: void t_f1();
  public: void t_f2();
  public: void t_f3();
  public: void t_f4();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_same_h__
