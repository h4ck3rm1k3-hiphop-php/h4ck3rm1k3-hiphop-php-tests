
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/method_call_variation_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/method_call_variation_001.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/method_call_variation_001.php line 4 */
void c_c::t_foo(CVarRef v_a, CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(C, C::foo);
  echo(LINE(6,concat5("Called C::foo(", toString(v_a), ", ", toString(v_b), ")\n")));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/method_call_variation_001.php line 19 */
void f_foo(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(foo);
  echo(LINE(21,concat5("Called global foo(", toString(v_a), ", ", toString(v_b), ")\n")));
} /* function */
Variant i_foo(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6176C0B993BF5914LL, foo) {
    return (f_foo(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$method_call_variation_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/method_call_variation_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$method_call_variation_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_functions __attribute__((__unused__)) = (variables != gVariables) ? variables->get("functions") : g->GV(functions);

  (v_c = ((Object)(LINE(10,p_c(p_c(NEWOBJ(c_c)())->create())))));
  v_functions.set(0LL, ("foo"), 0x77CFA1EEF01BCA90LL);
  lval(lval(lval(v_functions.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)).lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).set(4LL, ("foo"), 0x6F2A25235E544A31LL);
  LINE(15,v_c.o_invoke_few_args(toString(v_functions.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), -1LL, 2, 1LL, 2LL));
  LINE(16,v_c.o_invoke_few_args(toString(v_functions.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL).rvalAt(4LL, 0x6F2A25235E544A31LL)), -1LL, 2, 3LL, 4LL));
  lval(v_c.o_lval("functions", 0x345241CAC8396B02LL)).set(0LL, ("foo"), 0x77CFA1EEF01BCA90LL);
  lval(lval(lval(lval(v_c.o_lval("functions", 0x345241CAC8396B02LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)).lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).set(4LL, ("foo"), 0x6F2A25235E544A31LL);
  LINE(27,invoke((toString(v_c.o_get("functions", 0x345241CAC8396B02LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), Array(ArrayInit(2).set(0, 5LL).set(1, 6LL).create()), -1));
  LINE(28,invoke((toString(v_c.o_get("functions", 0x345241CAC8396B02LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL).rvalAt(4LL, 0x6F2A25235E544A31LL))), Array(ArrayInit(2).set(0, 7LL).set(1, 8LL).create()), -1));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
