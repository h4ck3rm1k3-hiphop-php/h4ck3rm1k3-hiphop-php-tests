
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_and_extends.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_and_extends.php line 8 */
Variant c_tester::os_get(const char *s, int64 hash) {
  return c_test::os_get(s, hash);
}
Variant &c_tester::os_lval(const char *s, int64 hash) {
  return c_test::os_lval(s, hash);
}
void c_tester::o_get(ArrayElementVec &props) const {
  c_test::o_get(props);
}
bool c_tester::o_exists(CStrRef s, int64 hash) const {
  return c_test::o_exists(s, hash);
}
Variant c_tester::o_get(CStrRef s, int64 hash) {
  return c_test::o_get(s, hash);
}
Variant c_tester::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test::o_set(s, hash, v, forInit);
}
Variant &c_tester::o_lval(CStrRef s, int64 hash) {
  return c_test::o_lval(s, hash);
}
Variant c_tester::os_constant(const char *s) {
  return c_test::os_constant(s);
}
IMPLEMENT_CLASS(tester)
ObjectData *c_tester::cloneImpl() {
  c_tester *obj = NEW(c_tester)();
  cloneSet(obj);
  return obj;
}
void c_tester::cloneSet(c_tester *clone) {
  c_test::cloneSet(clone);
}
Variant c_tester::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke(s, params, hash, fatal);
}
Variant c_tester::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_tester::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test::os_invoke(c, s, params, hash, fatal);
}
Variant cw_tester$os_get(const char *s) {
  return c_tester::os_get(s, -1);
}
Variant &cw_tester$os_lval(const char *s) {
  return c_tester::os_lval(s, -1);
}
Variant cw_tester$os_constant(const char *s) {
  return c_tester::os_constant(s);
}
Variant cw_tester$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_tester::os_invoke(c, s, params, -1, fatal);
}
void c_tester::init() {
  c_test::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_and_extends.php line 10 */
void c_tester::t_show() {
  INSTANCE_METHOD_INJECTION(Tester, Tester::show);
  echo("Tester::show\n");
} /* function */
Object co_tester(CArrRef params, bool init /* = true */) {
  return Object(p_tester(NEW(c_tester)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_and_extends_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_and_extends.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_and_extends_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);

  (v_o = ((Object)(LINE(15,p_tester(p_tester(NEWOBJ(c_tester)())->create())))));
  LINE(16,v_o.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
