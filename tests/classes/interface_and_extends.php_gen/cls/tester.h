
#ifndef __GENERATED_cls_tester_h__
#define __GENERATED_cls_tester_h__

#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_and_extends.php line 8 */
class c_tester : virtual public c_test {
  BEGIN_CLASS_MAP(tester)
    PARENT_CLASS(test)
  END_CLASS_MAP(tester)
  DECLARE_CLASS(tester, Tester, test)
  void init();
  public: void t_show();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_tester_h__
