
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__set__get_004.php line 2 */
class c_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(test)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, Test, ObjectData)
  void init();
  public: String m_x;
  public: Variant t___get(Variant v_name);
  public: Variant &___lval(Variant v_name);
  public: Variant t___set(Variant v_name, Variant v_val);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
