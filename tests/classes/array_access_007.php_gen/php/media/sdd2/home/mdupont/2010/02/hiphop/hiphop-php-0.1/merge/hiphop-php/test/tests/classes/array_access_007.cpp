
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 3 */
Variant c_overloadedarray::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_overloadedarray::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_overloadedarray::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("realArray", m_realArray.isReferenced() ? ref(m_realArray) : m_realArray));
  c_ObjectData::o_get(props);
}
bool c_overloadedarray::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x090D8AED56860382LL, realArray, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_overloadedarray::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x090D8AED56860382LL, m_realArray,
                         realArray, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_overloadedarray::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x090D8AED56860382LL, m_realArray,
                      realArray, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_overloadedarray::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x090D8AED56860382LL, m_realArray,
                         realArray, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_overloadedarray::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(overloadedarray)
ObjectData *c_overloadedarray::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_overloadedarray::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_overloadedarray::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_overloadedarray::cloneImpl() {
  c_overloadedarray *obj = NEW(c_overloadedarray)();
  cloneSet(obj);
  return obj;
}
void c_overloadedarray::cloneSet(c_overloadedarray *clone) {
  clone->m_realArray = m_realArray.isReferenced() ? ref(m_realArray) : m_realArray;
  ObjectData::cloneSet(clone);
}
Variant c_overloadedarray::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 10:
      HASH_GUARD(0x7C4DCA0AB41BBDBALL, dump) {
        return (t_dump(), null);
      }
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_overloadedarray::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      break;
    case 8:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 10:
      HASH_GUARD(0x7C4DCA0AB41BBDBALL, dump) {
        return (t_dump(), null);
      }
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_overloadedarray::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_overloadedarray$os_get(const char *s) {
  return c_overloadedarray::os_get(s, -1);
}
Variant &cw_overloadedarray$os_lval(const char *s) {
  return c_overloadedarray::os_lval(s, -1);
}
Variant cw_overloadedarray$os_constant(const char *s) {
  return c_overloadedarray::os_constant(s);
}
Variant cw_overloadedarray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_overloadedarray::os_invoke(c, s, params, -1, fatal);
}
void c_overloadedarray::init() {
  m_realArray = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 6 */
void c_overloadedarray::t___construct() {
  INSTANCE_METHOD_INJECTION(OverloadedArray, OverloadedArray::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_realArray = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 10 */
bool c_overloadedarray::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(OverloadedArray, OverloadedArray::offsetExists);
  return LINE(11,x_array_key_exists(m_realArray, v_index));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 14 */
Variant c_overloadedarray::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(OverloadedArray, OverloadedArray::offsetGet);
  return m_realArray.rvalAt(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 14 */
Variant &c_overloadedarray::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(OverloadedArray, OverloadedArray::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 18 */
void c_overloadedarray::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(OverloadedArray, OverloadedArray::offsetSet);
  if (LINE(19,x_is_null(v_index))) {
    m_realArray.append((v_value));
  }
  else {
    m_realArray.set(v_index, (v_value));
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 26 */
void c_overloadedarray::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(OverloadedArray, OverloadedArray::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  m_realArray.weakRemove(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php line 30 */
void c_overloadedarray::t_dump() {
  INSTANCE_METHOD_INJECTION(OverloadedArray, OverloadedArray::dump);
  LINE(31,x_var_dump(1, m_realArray));
} /* function */
Object co_overloadedarray(CArrRef params, bool init /* = true */) {
  return Object(p_overloadedarray(NEW(c_overloadedarray)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_007_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_007.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_007_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = ((Object)(LINE(35,p_overloadedarray(p_overloadedarray(NEWOBJ(c_overloadedarray)())->create())))));
  v_a.append((1LL));
  v_a.set(1LL, (2LL), 0x5BCA7C69B794F8CELL);
  v_a.set(2LL, (3LL), 0x486AFCC090D5F98CLL);
  v_a.append((4LL));
  LINE(40,v_a.o_invoke_few_args("dump", 0x7C4DCA0AB41BBDBALL, 0));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
