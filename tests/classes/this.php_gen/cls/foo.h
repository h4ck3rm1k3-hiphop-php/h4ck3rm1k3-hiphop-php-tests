
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/this.php line 6 */
class c_foo : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, Foo, ObjectData)
  void init();
  public: void t_replace(CVarRef v_other);
  public: void t_indirect(CVarRef v_other);
  public: void t_retrieve(Variant v_other);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
