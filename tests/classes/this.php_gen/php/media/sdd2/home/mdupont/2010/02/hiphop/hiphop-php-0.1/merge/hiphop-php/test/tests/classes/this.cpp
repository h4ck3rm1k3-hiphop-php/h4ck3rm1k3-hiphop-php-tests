
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/this.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/this.php line 6 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x4BC39CEC02179BAALL, retrieve) {
        return (t_retrieve(ref(const_cast<Array&>(params).lvalAt(0))), null);
      }
      HASH_GUARD(0x08E83C64C6A3BA92LL, indirect) {
        return (t_indirect(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x4BC39CEC02179BAALL, retrieve) {
        return (t_retrieve(ref(a0)), null);
      }
      HASH_GUARD(0x08E83C64C6A3BA92LL, indirect) {
        return (t_indirect(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/this.php line 8 */
void c_foo::t_replace(CVarRef v_other) {
  INSTANCE_METHOD_INJECTION(Foo, Foo::replace);
  echo("Foo::replace\n");
  ((Object)((this = v_other)));
  print(toString(o_get("prop", 0x4EA429E0DCC990B7LL)));
  print(toString(toObject(v_other).o_get("prop", 0x4EA429E0DCC990B7LL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/this.php line 16 */
void c_foo::t_indirect(CVarRef v_other) {
  INSTANCE_METHOD_INJECTION(Foo, Foo::indirect);
  Variant v_result;

  echo("Foo::indirect\n");
  ((Object)((this = v_other)));
  (v_result = (this = v_other));
  print(toString(v_result.o_get("prop", 0x4EA429E0DCC990B7LL)));
  print(toString(o_get("prop", 0x4EA429E0DCC990B7LL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/this.php line 25 */
void c_foo::t_retrieve(Variant v_other) {
  INSTANCE_METHOD_INJECTION(Foo, Foo::retrieve);
  echo("Foo::retrieve\n");
  (v_other = ((Object)(this)));
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$this_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/this.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$this_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_object __attribute__((__unused__)) = (variables != gVariables) ? variables->get("object") : g->GV(object);
  Variant &v_other __attribute__((__unused__)) = (variables != gVariables) ? variables->get("other") : g->GV(other);

  (v_object = ((Object)(LINE(32,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  (v_object.o_lval("prop", 0x4EA429E0DCC990B7LL) = "Hello\n");
  (v_other = ((Object)(LINE(35,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  (v_other.o_lval("prop", 0x4EA429E0DCC990B7LL) = "World\n");
  LINE(38,v_object.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 1, v_other));
  LINE(39,v_object.o_invoke_few_args("indirect", 0x08E83C64C6A3BA92LL, 1, v_other));
  print(toString(v_object.o_get("prop", 0x4EA429E0DCC990B7LL)));
  LINE(43,v_object.o_invoke_few_args("retrieve", 0x4BC39CEC02179BAALL, 1, v_other));
  print(toString(v_other.o_get("prop", 0x4EA429E0DCC990B7LL)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
