
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__

#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/type_hinting_005c.php line 5 */
class c_d : virtual public c_c {
  BEGIN_CLASS_MAP(d)
    PARENT_CLASS(c)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, c)
  void init();
  public: void t_f(CArrRef v_a);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
