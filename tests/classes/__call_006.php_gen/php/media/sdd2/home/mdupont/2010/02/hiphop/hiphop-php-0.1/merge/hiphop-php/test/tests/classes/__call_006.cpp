
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_006.php line 2 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::doCall(Variant v_name, Variant v_arguments, bool fatal) {
  return t___call(v_name, v_arguments);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_006.php line 3 */
Variant c_a::t___call(Variant v_strMethod, Variant v_arrArgs) {
  INSTANCE_METHOD_INJECTION(A, A::__call);
  INCALL_HELPER(v_strMethod);
  LINE(4,x_var_dump(1, ((Object)(this))));
  throw_exception(((Object)(LINE(5,p_exception(p_exception(NEWOBJ(c_exception)())->create())))));
  echo("You should not see this");
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_006.php line 8 */
void c_a::t_test() {
  INSTANCE_METHOD_INJECTION(A, A::test);
  LINE(9,throw_fatal("unknown method a::unknowncalledwithsro", ((void*)NULL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_006.php line 13 */
Variant c_b::os_get(const char *s, int64 hash) {
  return c_a::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_a::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  c_a::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  return c_a::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  return c_a::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_a::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_a::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  return c_a::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  c_a::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_a::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
  c_a::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_006.php line 14 */
void c_b::t_test() {
  INSTANCE_METHOD_INJECTION(B, B::test);
  LINE(15,throw_fatal("unknown method b::unknowncalledwithsrofromchild", ((void*)NULL)));
} /* function */
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$__call_006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$__call_006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = ((Object)(LINE(19,p_a(p_a(NEWOBJ(c_a)())->create())))));
  echo("---> Invoke __call via simple method call.\n");
  try {
    LINE(23,v_a.o_invoke_few_args("unknown", 0x766537E197B73AF9LL, 0));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo("Exception caught OK; continuing.\n");
    } else {
      throw;
    }
  }
  echo("\n\n---> Invoke __call via scope resolution operator within instance.\n");
  try {
    LINE(30,v_a.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo("Exception caught OK; continuing.\n");
    } else {
      throw;
    }
  }
  echo("\n\n---> Invoke __call via scope resolution operator within child instance.\n");
  (v_b = ((Object)(LINE(36,p_b(p_b(NEWOBJ(c_b)())->create())))));
  try {
    LINE(38,v_b.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo("Exception caught OK; continuing.\n");
    } else {
      throw;
    }
  }
  echo("\n\n---> Invoke __call via callback.\n");
  try {
    LINE(45,x_call_user_func(4, Array(ArrayInit(2).set(0, v_b).set(1, "unknownCallback").create()), ScalarArrays::sa_[0]));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo("Exception caught OK; continuing.\n");
    } else {
      throw;
    }
  }
  echo("==DONE==\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
