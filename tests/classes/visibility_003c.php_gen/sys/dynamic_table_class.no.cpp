
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_same(CArrRef params, bool init = true);
Variant cw_same$os_get(const char *s);
Variant &cw_same$os_lval(const char *s);
Variant cw_same$os_constant(const char *s);
Variant cw_same$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_father(CArrRef params, bool init = true);
Variant cw_father$os_get(const char *s);
Variant &cw_father$os_lval(const char *s);
Variant cw_father$os_constant(const char *s);
Variant cw_father$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fail(CArrRef params, bool init = true);
Variant cw_fail$os_get(const char *s);
Variant &cw_fail$os_lval(const char *s);
Variant cw_fail$os_constant(const char *s);
Variant cw_fail$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_CREATE_OBJECT(0x4FF7127E117F4AEALL, father);
      HASH_CREATE_OBJECT(0x731DBC30C2876EAALL, fail);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x4285C5EAADA6686DLL, same);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x4FF7127E117F4AEALL, father);
      HASH_INVOKE_STATIC_METHOD(0x731DBC30C2876EAALL, fail);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x4285C5EAADA6686DLL, same);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x4FF7127E117F4AEALL, father);
      HASH_GET_STATIC_PROPERTY(0x731DBC30C2876EAALL, fail);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x4285C5EAADA6686DLL, same);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x4FF7127E117F4AEALL, father);
      HASH_GET_STATIC_PROPERTY_LV(0x731DBC30C2876EAALL, fail);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x4285C5EAADA6686DLL, same);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x4FF7127E117F4AEALL, father);
      HASH_GET_CLASS_CONSTANT(0x731DBC30C2876EAALL, fail);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x4285C5EAADA6686DLL, same);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
