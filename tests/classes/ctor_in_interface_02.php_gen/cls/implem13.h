
#ifndef __GENERATED_cls_implem13_h__
#define __GENERATED_cls_implem13_h__

#include <cls/constr1.h>
#include <cls/constr3.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.php line 23 */
class c_implem13 : virtual public c_constr1, virtual public c_constr3 {
  BEGIN_CLASS_MAP(implem13)
    PARENT_CLASS(constr1)
    PARENT_CLASS(constr3)
  END_CLASS_MAP(implem13)
  DECLARE_CLASS(implem13, implem13, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_implem13_h__
