
#ifndef __GENERATED_cls_implem12_h__
#define __GENERATED_cls_implem12_h__

#include <cls/constr2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.php line 11 */
class c_implem12 : virtual public c_constr2 {
  BEGIN_CLASS_MAP(implem12)
    PARENT_CLASS(constr1)
    PARENT_CLASS(constr2)
  END_CLASS_MAP(implem12)
  DECLARE_CLASS(implem12, implem12, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_implem12_h__
