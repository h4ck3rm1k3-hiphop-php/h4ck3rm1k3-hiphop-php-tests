
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.php line 23 */
Variant c_implem13::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_implem13::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_implem13::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_implem13::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_implem13::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_implem13::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_implem13::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_implem13::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(implem13)
ObjectData *c_implem13::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_implem13::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_implem13::cloneImpl() {
  c_implem13 *obj = NEW(c_implem13)();
  cloneSet(obj);
  return obj;
}
void c_implem13::cloneSet(c_implem13 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_implem13::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_implem13::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_implem13::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_implem13$os_get(const char *s) {
  return c_implem13::os_get(s, -1);
}
Variant &cw_implem13$os_lval(const char *s) {
  return c_implem13::os_lval(s, -1);
}
Variant cw_implem13$os_constant(const char *s) {
  return c_implem13::os_constant(s);
}
Variant cw_implem13$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_implem13::os_invoke(c, s, params, -1, fatal);
}
void c_implem13::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.php line 25 */
void c_implem13::t___construct() {
  INSTANCE_METHOD_INJECTION(implem13, implem13::__construct);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.php line 11 */
Variant c_implem12::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_implem12::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_implem12::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_implem12::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_implem12::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_implem12::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_implem12::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_implem12::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(implem12)
ObjectData *c_implem12::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_implem12::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_implem12::cloneImpl() {
  c_implem12 *obj = NEW(c_implem12)();
  cloneSet(obj);
  return obj;
}
void c_implem12::cloneSet(c_implem12 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_implem12::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_implem12::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_implem12::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_implem12$os_get(const char *s) {
  return c_implem12::os_get(s, -1);
}
Variant &cw_implem12$os_lval(const char *s) {
  return c_implem12::os_lval(s, -1);
}
Variant cw_implem12$os_constant(const char *s) {
  return c_implem12::os_constant(s);
}
Variant cw_implem12$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_implem12::os_invoke(c, s, params, -1, fatal);
}
void c_implem12::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.php line 13 */
void c_implem12::t___construct() {
  INSTANCE_METHOD_INJECTION(implem12, implem12::__construct);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
Object co_implem13(CArrRef params, bool init /* = true */) {
  return Object(p_implem13(NEW(c_implem13)())->dynCreate(params, init));
}
Object co_implem12(CArrRef params, bool init /* = true */) {
  return Object(p_implem12(NEW(c_implem12)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_in_interface_02_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_02.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_in_interface_02_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
