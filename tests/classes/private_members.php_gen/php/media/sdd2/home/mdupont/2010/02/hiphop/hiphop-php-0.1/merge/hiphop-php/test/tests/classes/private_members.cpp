
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.php line 3 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("member", m_member));
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7D0BEDECC8D925AELL, member, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7D0BEDECC8D925AELL, m_member,
                         member, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7D0BEDECC8D925AELL, m_member,
                      member, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_base::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_base::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  clone->m_member = m_member;
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
  m_member = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.php line 7 */
void c_base::t___construct() {
  INSTANCE_METHOD_INJECTION(base, base::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("base::__construct(begin)\n");
  (m_member = "base::member");
  LINE(11,o_root_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  echo("base::__construct(end)\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.php line 15 */
void c_base::t_test() {
  INSTANCE_METHOD_INJECTION(base, base::test);
  echo("base::test\n");
  LINE(18,x_print_r(((Object)(this))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.php line 22 */
Variant c_derived::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_derived::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_derived::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("member", m_member));
  c_base::o_get(props);
}
bool c_derived::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_derived::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_derived::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_derived::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_derived::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(derived)
ObjectData *c_derived::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_derived::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_derived::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_derived::cloneImpl() {
  c_derived *obj = NEW(c_derived)();
  cloneSet(obj);
  return obj;
}
void c_derived::cloneSet(c_derived *clone) {
  clone->m_member = m_member;
  c_base::cloneSet(clone);
}
Variant c_derived::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_derived::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived$os_get(const char *s) {
  return c_derived::os_get(s, -1);
}
Variant &cw_derived$os_lval(const char *s) {
  return c_derived::os_lval(s, -1);
}
Variant cw_derived$os_constant(const char *s) {
  return c_derived::os_constant(s);
}
Variant cw_derived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived::os_invoke(c, s, params, -1, fatal);
}
void c_derived::init() {
  c_base::init();
  m_member = "derived::member (default)";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.php line 26 */
void c_derived::t___construct() {
  INSTANCE_METHOD_INJECTION(derived, derived::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("derived::__construct(begin)\n");
  LINE(29,c_base::t___construct());
  LINE(30,c_base::t_test());
  LINE(31,o_root_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  (m_member = "derived::member");
  echo("derived::__construct(end)\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.php line 36 */
void c_derived::t_test() {
  INSTANCE_METHOD_INJECTION(derived, derived::test);
  LINE(38,c_base::t_test());
  echo("derived::test\n");
  LINE(40,x_print_r(((Object)(this))));
} /* function */
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Object co_derived(CArrRef params, bool init /* = true */) {
  return Object(p_derived(NEW(c_derived)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$private_members_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_members.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$private_members_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_t = ((Object)(LINE(44,p_derived(p_derived(NEWOBJ(c_derived)())->create())))));
  LINE(45,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  unset(v_t);
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
