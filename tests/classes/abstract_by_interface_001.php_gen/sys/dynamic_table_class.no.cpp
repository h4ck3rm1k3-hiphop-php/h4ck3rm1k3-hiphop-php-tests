
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_leaf(CArrRef params, bool init = true);
Variant cw_leaf$os_get(const char *s);
Variant &cw_leaf$os_lval(const char *s);
Variant cw_leaf$os_constant(const char *s);
Variant cw_leaf$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fails(CArrRef params, bool init = true);
Variant cw_fails$os_get(const char *s);
Variant &cw_fails$os_lval(const char *s);
Variant cw_fails$os_constant(const char *s);
Variant cw_fails$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_root(CArrRef params, bool init = true);
Variant cw_root$os_get(const char *s);
Variant &cw_root$os_lval(const char *s);
Variant cw_root$os_constant(const char *s);
Variant cw_root$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_derived(CArrRef params, bool init = true);
Variant cw_derived$os_get(const char *s);
Variant &cw_derived$os_lval(const char *s);
Variant cw_derived$os_constant(const char *s);
Variant cw_derived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_CREATE_OBJECT(0x120F7875BF6E3418LL, fails);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x2934AF750B718FABLL, derived);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x11F2027FAD9B4C1CLL, leaf);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x79A0919592E4AFDFLL, root);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x120F7875BF6E3418LL, fails);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x2934AF750B718FABLL, derived);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x11F2027FAD9B4C1CLL, leaf);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x79A0919592E4AFDFLL, root);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x120F7875BF6E3418LL, fails);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x2934AF750B718FABLL, derived);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x11F2027FAD9B4C1CLL, leaf);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x79A0919592E4AFDFLL, root);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x120F7875BF6E3418LL, fails);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x2934AF750B718FABLL, derived);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x11F2027FAD9B4C1CLL, leaf);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x79A0919592E4AFDFLL, root);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x120F7875BF6E3418LL, fails);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x2934AF750B718FABLL, derived);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x11F2027FAD9B4C1CLL, leaf);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x79A0919592E4AFDFLL, root);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
