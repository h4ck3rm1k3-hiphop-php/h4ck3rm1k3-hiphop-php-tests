
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php line 14 */
Variant c_leaf::os_get(const char *s, int64 hash) {
  return c_derived::os_get(s, hash);
}
Variant &c_leaf::os_lval(const char *s, int64 hash) {
  return c_derived::os_lval(s, hash);
}
void c_leaf::o_get(ArrayElementVec &props) const {
  c_derived::o_get(props);
}
bool c_leaf::o_exists(CStrRef s, int64 hash) const {
  return c_derived::o_exists(s, hash);
}
Variant c_leaf::o_get(CStrRef s, int64 hash) {
  return c_derived::o_get(s, hash);
}
Variant c_leaf::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_derived::o_set(s, hash, v, forInit);
}
Variant &c_leaf::o_lval(CStrRef s, int64 hash) {
  return c_derived::o_lval(s, hash);
}
Variant c_leaf::os_constant(const char *s) {
  return c_derived::os_constant(s);
}
IMPLEMENT_CLASS(leaf)
ObjectData *c_leaf::cloneImpl() {
  c_leaf *obj = NEW(c_leaf)();
  cloneSet(obj);
  return obj;
}
void c_leaf::cloneSet(c_leaf *clone) {
  c_derived::cloneSet(clone);
}
Variant c_leaf::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x776A626FE3514AD7LL, myinterfacefunc) {
        return (t_myinterfacefunc(), null);
      }
      break;
    default:
      break;
  }
  return c_derived::o_invoke(s, params, hash, fatal);
}
Variant c_leaf::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x776A626FE3514AD7LL, myinterfacefunc) {
        return (t_myinterfacefunc(), null);
      }
      break;
    default:
      break;
  }
  return c_derived::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_leaf::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_derived::os_invoke(c, s, params, hash, fatal);
}
Variant cw_leaf$os_get(const char *s) {
  return c_leaf::os_get(s, -1);
}
Variant &cw_leaf$os_lval(const char *s) {
  return c_leaf::os_lval(s, -1);
}
Variant cw_leaf$os_constant(const char *s) {
  return c_leaf::os_constant(s);
}
Variant cw_leaf$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_leaf::os_invoke(c, s, params, -1, fatal);
}
void c_leaf::init() {
  c_derived::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php line 16 */
void c_leaf::t_myinterfacefunc() {
  INSTANCE_METHOD_INJECTION(Leaf, Leaf::MyInterfaceFunc);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php line 21 */
Variant c_fails::os_get(const char *s, int64 hash) {
  return c_root::os_get(s, hash);
}
Variant &c_fails::os_lval(const char *s, int64 hash) {
  return c_root::os_lval(s, hash);
}
void c_fails::o_get(ArrayElementVec &props) const {
  c_root::o_get(props);
}
bool c_fails::o_exists(CStrRef s, int64 hash) const {
  return c_root::o_exists(s, hash);
}
Variant c_fails::o_get(CStrRef s, int64 hash) {
  return c_root::o_get(s, hash);
}
Variant c_fails::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_root::o_set(s, hash, v, forInit);
}
Variant &c_fails::o_lval(CStrRef s, int64 hash) {
  return c_root::o_lval(s, hash);
}
Variant c_fails::os_constant(const char *s) {
  return c_root::os_constant(s);
}
IMPLEMENT_CLASS(fails)
ObjectData *c_fails::cloneImpl() {
  c_fails *obj = NEW(c_fails)();
  cloneSet(obj);
  return obj;
}
void c_fails::cloneSet(c_fails *clone) {
  c_root::cloneSet(clone);
}
Variant c_fails::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_root::o_invoke(s, params, hash, fatal);
}
Variant c_fails::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_root::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fails::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_root::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fails$os_get(const char *s) {
  return c_fails::os_get(s, -1);
}
Variant &cw_fails$os_lval(const char *s) {
  return c_fails::os_lval(s, -1);
}
Variant cw_fails$os_constant(const char *s) {
  return c_fails::os_constant(s);
}
Variant cw_fails$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fails::os_invoke(c, s, params, -1, fatal);
}
void c_fails::init() {
  c_root::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php line 3 */
Variant c_root::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_root::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_root::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_root::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_root::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_root::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_root::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_root::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(root)
ObjectData *c_root::cloneImpl() {
  c_root *obj = NEW(c_root)();
  cloneSet(obj);
  return obj;
}
void c_root::cloneSet(c_root *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_root::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_root::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_root::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_root$os_get(const char *s) {
  return c_root::os_get(s, -1);
}
Variant &cw_root$os_lval(const char *s) {
  return c_root::os_lval(s, -1);
}
Variant cw_root$os_constant(const char *s) {
  return c_root::os_constant(s);
}
Variant cw_root$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_root::os_invoke(c, s, params, -1, fatal);
}
void c_root::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php line 11 */
Variant c_derived::os_get(const char *s, int64 hash) {
  return c_root::os_get(s, hash);
}
Variant &c_derived::os_lval(const char *s, int64 hash) {
  return c_root::os_lval(s, hash);
}
void c_derived::o_get(ArrayElementVec &props) const {
  c_root::o_get(props);
}
bool c_derived::o_exists(CStrRef s, int64 hash) const {
  return c_root::o_exists(s, hash);
}
Variant c_derived::o_get(CStrRef s, int64 hash) {
  return c_root::o_get(s, hash);
}
Variant c_derived::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_root::o_set(s, hash, v, forInit);
}
Variant &c_derived::o_lval(CStrRef s, int64 hash) {
  return c_root::o_lval(s, hash);
}
Variant c_derived::os_constant(const char *s) {
  return c_root::os_constant(s);
}
IMPLEMENT_CLASS(derived)
ObjectData *c_derived::cloneImpl() {
  c_derived *obj = NEW(c_derived)();
  cloneSet(obj);
  return obj;
}
void c_derived::cloneSet(c_derived *clone) {
  c_root::cloneSet(clone);
}
Variant c_derived::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_root::o_invoke(s, params, hash, fatal);
}
Variant c_derived::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_root::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_root::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived$os_get(const char *s) {
  return c_derived::os_get(s, -1);
}
Variant &cw_derived$os_lval(const char *s) {
  return c_derived::os_lval(s, -1);
}
Variant cw_derived$os_constant(const char *s) {
  return c_derived::os_constant(s);
}
Variant cw_derived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived::os_invoke(c, s, params, -1, fatal);
}
void c_derived::init() {
  c_root::init();
}
Object co_leaf(CArrRef params, bool init /* = true */) {
  return Object(p_leaf(NEW(c_leaf)())->dynCreate(params, init));
}
Object co_fails(CArrRef params, bool init /* = true */) {
  return Object(p_fails(NEW(c_fails)())->dynCreate(params, init));
}
Object co_root(CArrRef params, bool init /* = true */) {
  return Object(p_root(NEW(c_root)())->dynCreate(params, init));
}
Object co_derived(CArrRef params, bool init /* = true */) {
  return Object(p_derived(NEW(c_derived)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$abstract_by_interface_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$abstract_by_interface_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(19,x_var_dump(1, ((Object)(p_leaf(p_leaf(NEWOBJ(c_leaf)())->create())))));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
