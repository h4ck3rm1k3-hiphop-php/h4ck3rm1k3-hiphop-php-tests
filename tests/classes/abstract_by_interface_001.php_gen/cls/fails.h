
#ifndef __GENERATED_cls_fails_h__
#define __GENERATED_cls_fails_h__

#include <cls/root.h>
#include <cls/myinterface.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php line 21 */
class c_fails : virtual public c_root, virtual public c_myinterface {
  BEGIN_CLASS_MAP(fails)
    PARENT_CLASS(root)
    PARENT_CLASS(myinterface)
  END_CLASS_MAP(fails)
  DECLARE_CLASS(fails, Fails, root)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fails_h__
