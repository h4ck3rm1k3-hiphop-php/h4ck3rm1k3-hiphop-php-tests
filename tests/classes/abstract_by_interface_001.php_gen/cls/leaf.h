
#ifndef __GENERATED_cls_leaf_h__
#define __GENERATED_cls_leaf_h__

#include <cls/derived.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_001.php line 14 */
class c_leaf : virtual public c_derived {
  BEGIN_CLASS_MAP(leaf)
    PARENT_CLASS(root)
    PARENT_CLASS(myinterface)
    PARENT_CLASS(derived)
  END_CLASS_MAP(leaf)
  DECLARE_CLASS(leaf, Leaf, derived)
  void init();
  public: void t_myinterfacefunc();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_leaf_h__
