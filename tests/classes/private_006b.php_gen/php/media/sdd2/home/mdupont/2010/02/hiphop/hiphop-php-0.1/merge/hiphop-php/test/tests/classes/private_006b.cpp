
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php line 22 */
Variant c_third::os_get(const char *s, int64 hash) {
  return c_second::os_get(s, hash);
}
Variant &c_third::os_lval(const char *s, int64 hash) {
  return c_second::os_lval(s, hash);
}
void c_third::o_get(ArrayElementVec &props) const {
  c_second::o_get(props);
}
bool c_third::o_exists(CStrRef s, int64 hash) const {
  return c_second::o_exists(s, hash);
}
Variant c_third::o_get(CStrRef s, int64 hash) {
  return c_second::o_get(s, hash);
}
Variant c_third::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_second::o_set(s, hash, v, forInit);
}
Variant &c_third::o_lval(CStrRef s, int64 hash) {
  return c_second::o_lval(s, hash);
}
Variant c_third::os_constant(const char *s) {
  return c_second::os_constant(s);
}
IMPLEMENT_CLASS(third)
ObjectData *c_third::cloneImpl() {
  c_third *obj = NEW(c_third)();
  cloneSet(obj);
  return obj;
}
void c_third::cloneSet(c_third *clone) {
  c_second::cloneSet(clone);
}
Variant c_third::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x632CAE0088C496F0LL, do_show) {
        return (t_do_show(), null);
      }
      break;
    default:
      break;
  }
  return c_second::o_invoke(s, params, hash, fatal);
}
Variant c_third::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x632CAE0088C496F0LL, do_show) {
        return (t_do_show(), null);
      }
      break;
    default:
      break;
  }
  return c_second::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_third::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_second::os_invoke(c, s, params, hash, fatal);
}
Variant cw_third$os_get(const char *s) {
  return c_third::os_get(s, -1);
}
Variant &cw_third$os_lval(const char *s) {
  return c_third::os_lval(s, -1);
}
Variant cw_third$os_constant(const char *s) {
  return c_third::os_constant(s);
}
Variant cw_third$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_third::os_invoke(c, s, params, -1, fatal);
}
void c_third::init() {
  c_second::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php line 23 */
void c_third::t_show() {
  INSTANCE_METHOD_INJECTION(third, third::show);
  echo("Call show()\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php line 3 */
Variant c_first::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_first::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_first::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_first::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_first::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_first::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_first::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_first::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(first)
ObjectData *c_first::cloneImpl() {
  c_first *obj = NEW(c_first)();
  cloneSet(obj);
  return obj;
}
void c_first::cloneSet(c_first *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_first::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x632CAE0088C496F0LL, do_show) {
        return (t_do_show(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_first::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x632CAE0088C496F0LL, do_show) {
        return (t_do_show(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_first::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_first$os_get(const char *s) {
  return c_first::os_get(s, -1);
}
Variant &cw_first$os_lval(const char *s) {
  return c_first::os_lval(s, -1);
}
Variant cw_first$os_constant(const char *s) {
  return c_first::os_constant(s);
}
Variant cw_first$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_first::os_invoke(c, s, params, -1, fatal);
}
void c_first::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php line 4 */
void c_first::t_show() {
  INSTANCE_METHOD_INJECTION(first, first::show);
  echo("Call show()\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php line 8 */
void c_first::t_do_show() {
  INSTANCE_METHOD_INJECTION(first, first::do_show);
  LINE(9,t_show());
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php line 16 */
Variant c_second::os_get(const char *s, int64 hash) {
  return c_first::os_get(s, hash);
}
Variant &c_second::os_lval(const char *s, int64 hash) {
  return c_first::os_lval(s, hash);
}
void c_second::o_get(ArrayElementVec &props) const {
  c_first::o_get(props);
}
bool c_second::o_exists(CStrRef s, int64 hash) const {
  return c_first::o_exists(s, hash);
}
Variant c_second::o_get(CStrRef s, int64 hash) {
  return c_first::o_get(s, hash);
}
Variant c_second::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_first::o_set(s, hash, v, forInit);
}
Variant &c_second::o_lval(CStrRef s, int64 hash) {
  return c_first::o_lval(s, hash);
}
Variant c_second::os_constant(const char *s) {
  return c_first::os_constant(s);
}
IMPLEMENT_CLASS(second)
ObjectData *c_second::cloneImpl() {
  c_second *obj = NEW(c_second)();
  cloneSet(obj);
  return obj;
}
void c_second::cloneSet(c_second *clone) {
  c_first::cloneSet(clone);
}
Variant c_second::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x632CAE0088C496F0LL, do_show) {
        return (t_do_show(), null);
      }
      break;
    default:
      break;
  }
  return c_first::o_invoke(s, params, hash, fatal);
}
Variant c_second::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x632CAE0088C496F0LL, do_show) {
        return (t_do_show(), null);
      }
      break;
    default:
      break;
  }
  return c_first::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_second::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_first::os_invoke(c, s, params, hash, fatal);
}
Variant cw_second$os_get(const char *s) {
  return c_second::os_get(s, -1);
}
Variant &cw_second$os_lval(const char *s) {
  return c_second::os_lval(s, -1);
}
Variant cw_second$os_constant(const char *s) {
  return c_second::os_constant(s);
}
Variant cw_second$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_second::os_invoke(c, s, params, -1, fatal);
}
void c_second::init() {
  c_first::init();
}
Object co_third(CArrRef params, bool init /* = true */) {
  return Object(p_third(NEW(c_third)())->dynCreate(params, init));
}
Object co_first(CArrRef params, bool init /* = true */) {
  return Object(p_first(NEW(c_first)())->dynCreate(params, init));
}
Object co_second(CArrRef params, bool init /* = true */) {
  return Object(p_second(NEW(c_second)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$private_006b_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$private_006b_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t1") : g->GV(t1);
  Variant &v_t3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t3") : g->GV(t3);

  (v_t1 = ((Object)(LINE(13,p_first(p_first(NEWOBJ(c_first)())->create())))));
  LINE(14,v_t1.o_invoke_few_args("do_show", 0x632CAE0088C496F0LL, 0));
  (v_t3 = ((Object)(LINE(28,p_third(p_third(NEWOBJ(c_third)())->create())))));
  LINE(29,v_t3.o_invoke_few_args("do_show", 0x632CAE0088C496F0LL, 0));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
