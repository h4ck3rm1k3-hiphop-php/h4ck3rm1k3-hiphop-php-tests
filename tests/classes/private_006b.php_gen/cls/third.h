
#ifndef __GENERATED_cls_third_h__
#define __GENERATED_cls_third_h__

#include <cls/second.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_006b.php line 22 */
class c_third : virtual public c_second {
  BEGIN_CLASS_MAP(third)
    PARENT_CLASS(first)
    PARENT_CLASS(second)
  END_CLASS_MAP(third)
  DECLARE_CLASS(third, third, second)
  void init();
  public: void t_show();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_third_h__
