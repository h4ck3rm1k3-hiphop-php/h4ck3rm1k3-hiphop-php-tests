
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_007b.php line 3 */
class c_bar : virtual public ObjectData {
  BEGIN_CLASS_MAP(bar)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, Bar, ObjectData)
  void init();
  public: void t_pub();
  public: void t_priv();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
