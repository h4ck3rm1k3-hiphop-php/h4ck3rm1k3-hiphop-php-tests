
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__

#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_007b.php line 11 */
class c_foo : virtual public c_bar {
  BEGIN_CLASS_MAP(foo)
    PARENT_CLASS(bar)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, Foo, bar)
  void init();
  public: void t_priv();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
