
#ifndef __GENERATED_cls_pass_h__
#define __GENERATED_cls_pass_h__

#include <cls/showable.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_static.php line 8 */
class c_pass : virtual public c_showable {
  BEGIN_CLASS_MAP(pass)
    PARENT_CLASS(showable)
  END_CLASS_MAP(pass)
  DECLARE_CLASS(pass, pass, ObjectData)
  void init();
  public: static void ti_show(const char* cls);
  public: static void t_show() { ti_show("pass"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pass_h__
