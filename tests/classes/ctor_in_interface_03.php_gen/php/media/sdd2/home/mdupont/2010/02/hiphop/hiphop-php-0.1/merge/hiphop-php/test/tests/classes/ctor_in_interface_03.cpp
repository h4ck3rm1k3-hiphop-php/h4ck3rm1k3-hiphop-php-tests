
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_03.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_03.php line 11 */
Variant c_derived::os_get(const char *s, int64 hash) {
  return c_implem::os_get(s, hash);
}
Variant &c_derived::os_lval(const char *s, int64 hash) {
  return c_implem::os_lval(s, hash);
}
void c_derived::o_get(ArrayElementVec &props) const {
  c_implem::o_get(props);
}
bool c_derived::o_exists(CStrRef s, int64 hash) const {
  return c_implem::o_exists(s, hash);
}
Variant c_derived::o_get(CStrRef s, int64 hash) {
  return c_implem::o_get(s, hash);
}
Variant c_derived::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_implem::o_set(s, hash, v, forInit);
}
Variant &c_derived::o_lval(CStrRef s, int64 hash) {
  return c_implem::o_lval(s, hash);
}
Variant c_derived::os_constant(const char *s) {
  return c_implem::os_constant(s);
}
IMPLEMENT_CLASS(derived)
ObjectData *c_derived::create(Variant v_a) {
  init();
  t___construct(v_a);
  return this;
}
ObjectData *c_derived::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_derived::cloneImpl() {
  c_derived *obj = NEW(c_derived)();
  cloneSet(obj);
  return obj;
}
void c_derived::cloneSet(c_derived *clone) {
  c_implem::cloneSet(clone);
}
Variant c_derived::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_implem::o_invoke(s, params, hash, fatal);
}
Variant c_derived::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_implem::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_implem::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived$os_get(const char *s) {
  return c_derived::os_get(s, -1);
}
Variant &cw_derived$os_lval(const char *s) {
  return c_derived::os_lval(s, -1);
}
Variant cw_derived$os_constant(const char *s) {
  return c_derived::os_constant(s);
}
Variant cw_derived$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived::os_invoke(c, s, params, -1, fatal);
}
void c_derived::init() {
  c_implem::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_03.php line 13 */
void c_derived::t___construct(Variant v_a) {
  INSTANCE_METHOD_INJECTION(derived, derived::__construct);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_03.php line 7 */
Variant c_implem::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_implem::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_implem::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_implem::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_implem::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_implem::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_implem::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_implem::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(implem)
ObjectData *c_implem::cloneImpl() {
  c_implem *obj = NEW(c_implem)();
  cloneSet(obj);
  return obj;
}
void c_implem::cloneSet(c_implem *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_implem::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_implem::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_implem::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_implem$os_get(const char *s) {
  return c_implem::os_get(s, -1);
}
Variant &cw_implem$os_lval(const char *s) {
  return c_implem::os_lval(s, -1);
}
Variant cw_implem$os_constant(const char *s) {
  return c_implem::os_constant(s);
}
Variant cw_implem$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_implem::os_invoke(c, s, params, -1, fatal);
}
void c_implem::init() {
}
Object co_derived(CArrRef params, bool init /* = true */) {
  return Object(p_derived(NEW(c_derived)())->dynCreate(params, init));
}
Object co_implem(CArrRef params, bool init /* = true */) {
  return Object(p_implem(NEW(c_implem)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_in_interface_03_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_03.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$ctor_in_interface_03_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
