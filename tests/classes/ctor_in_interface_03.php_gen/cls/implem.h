
#ifndef __GENERATED_cls_implem_h__
#define __GENERATED_cls_implem_h__

#include <cls/constr.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_03.php line 7 */
class c_implem : virtual public c_constr {
  BEGIN_CLASS_MAP(implem)
    PARENT_CLASS(constr)
  END_CLASS_MAP(implem)
  DECLARE_CLASS(implem, implem, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_implem_h__
