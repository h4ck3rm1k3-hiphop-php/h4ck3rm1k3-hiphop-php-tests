
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 5 */
Variant c_arrayaccessreferenceproxy::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_arrayaccessreferenceproxy::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_arrayaccessreferenceproxy::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("object", m_object.isReferenced() ? ref(m_object) : m_object));
  props.push_back(NEW(ArrayElement)("oarray", m_oarray.isReferenced() ? ref(m_oarray) : m_oarray));
  props.push_back(NEW(ArrayElement)("element", m_element.isReferenced() ? ref(m_element) : m_element));
  c_ObjectData::o_get(props);
}
bool c_arrayaccessreferenceproxy::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x7F30BC4E222B1861LL, object, 6);
      HASH_EXISTS_STRING(0x1AFD3E7207DEC289LL, element, 7);
      break;
    case 6:
      HASH_EXISTS_STRING(0x28B47EBFB7C2E206LL, oarray, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_arrayaccessreferenceproxy::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x7F30BC4E222B1861LL, m_object,
                         object, 6);
      HASH_RETURN_STRING(0x1AFD3E7207DEC289LL, m_element,
                         element, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x28B47EBFB7C2E206LL, m_oarray,
                         oarray, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_arrayaccessreferenceproxy::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x7F30BC4E222B1861LL, m_object,
                      object, 6);
      HASH_SET_STRING(0x1AFD3E7207DEC289LL, m_element,
                      element, 7);
      break;
    case 6:
      HASH_SET_STRING(0x28B47EBFB7C2E206LL, m_oarray,
                      oarray, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_arrayaccessreferenceproxy::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x7F30BC4E222B1861LL, m_object,
                         object, 6);
      HASH_RETURN_STRING(0x1AFD3E7207DEC289LL, m_element,
                         element, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x28B47EBFB7C2E206LL, m_oarray,
                         oarray, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_arrayaccessreferenceproxy::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(arrayaccessreferenceproxy)
ObjectData *c_arrayaccessreferenceproxy::create(p_arrayaccess v_object, Variant v_array, Variant v_element) {
  init();
  t___construct(v_object, ref(v_array), v_element);
  return this;
}
ObjectData *c_arrayaccessreferenceproxy::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1)), params.rvalAt(2)));
  } else return this;
}
ObjectData *c_arrayaccessreferenceproxy::cloneImpl() {
  c_arrayaccessreferenceproxy *obj = NEW(c_arrayaccessreferenceproxy)();
  cloneSet(obj);
  return obj;
}
void c_arrayaccessreferenceproxy::cloneSet(c_arrayaccessreferenceproxy *clone) {
  clone->m_object = m_object.isReferenced() ? ref(m_object) : m_object;
  clone->m_oarray = m_oarray.isReferenced() ? ref(m_oarray) : m_oarray;
  clone->m_element = m_element.isReferenced() ? ref(m_element) : m_element;
  ObjectData::cloneSet(clone);
}
Variant c_arrayaccessreferenceproxy::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_arrayaccessreferenceproxy::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_arrayaccessreferenceproxy::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_arrayaccessreferenceproxy$os_get(const char *s) {
  return c_arrayaccessreferenceproxy::os_get(s, -1);
}
Variant &cw_arrayaccessreferenceproxy$os_lval(const char *s) {
  return c_arrayaccessreferenceproxy::os_lval(s, -1);
}
Variant cw_arrayaccessreferenceproxy$os_constant(const char *s) {
  return c_arrayaccessreferenceproxy::os_constant(s);
}
Variant cw_arrayaccessreferenceproxy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_arrayaccessreferenceproxy::os_invoke(c, s, params, -1, fatal);
}
void c_arrayaccessreferenceproxy::init() {
  m_object = null;
  m_oarray = null;
  m_element = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 11 */
void c_arrayaccessreferenceproxy::t___construct(p_arrayaccess v_object, Variant v_array, Variant v_element) {
  INSTANCE_METHOD_INJECTION(ArrayAccessReferenceProxy, ArrayAccessReferenceProxy::__construct);
  bool oldInCtor = gasInCtor(true);
  echo(concat("ArrayAccessReferenceProxy::__construct", LINE(13,concat3("(", toString(v_element), ")\n"))));
  (m_object = ((Object)(v_object)));
  (m_oarray = ref(v_array));
  (m_element = v_element);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 19 */
bool c_arrayaccessreferenceproxy::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessReferenceProxy, ArrayAccessReferenceProxy::offsetExists);
  echo(concat("ArrayAccessReferenceProxy::offsetExists", LINE(20,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  return LINE(21,x_array_key_exists(v_index, m_oarray.rvalAt(m_element)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 24 */
Variant c_arrayaccessreferenceproxy::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessReferenceProxy, ArrayAccessReferenceProxy::offsetGet);
  echo(concat("ArrayAccessReferenceProxy::offsetGet", LINE(25,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  return isset(m_oarray.rvalAt(m_element), v_index) ? ((Variant)(m_oarray.rvalAt(m_element).rvalAt(v_index))) : ((Variant)(null));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 24 */
Variant &c_arrayaccessreferenceproxy::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessReferenceProxy, ArrayAccessReferenceProxy::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 29 */
void c_arrayaccessreferenceproxy::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(ArrayAccessReferenceProxy, ArrayAccessReferenceProxy::offsetSet);
  echo(concat("ArrayAccessReferenceProxy::offsetSet", concat("(", LINE(30,concat6(toString(m_element), ", ", toString(v_index), ", ", toString(v_value), ")\n")))));
  lval(m_oarray.lvalAt(m_element)).set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 34 */
void c_arrayaccessreferenceproxy::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessReferenceProxy, ArrayAccessReferenceProxy::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  echo(concat("ArrayAccessReferenceProxy::offsetUnset", LINE(35,concat5("(", toString(m_element), ", ", toString(v_index), ")\n"))));
  lval(unsetLval(m_oarray, m_element)).weakRemove(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 40 */
Variant c_peoples::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_peoples::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_peoples::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("person", m_person.isReferenced() ? ref(m_person) : m_person));
  c_ObjectData::o_get(props);
}
bool c_peoples::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x67AE1F9174AB011DLL, person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_peoples::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_peoples::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x67AE1F9174AB011DLL, m_person,
                      person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_peoples::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x67AE1F9174AB011DLL, m_person,
                         person, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_peoples::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(peoples)
ObjectData *c_peoples::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_peoples::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_peoples::cloneImpl() {
  c_peoples *obj = NEW(c_peoples)();
  cloneSet(obj);
  return obj;
}
void c_peoples::cloneSet(c_peoples *clone) {
  clone->m_person = m_person.isReferenced() ? ref(m_person) : m_person;
  ObjectData::cloneSet(clone);
}
Variant c_peoples::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_peoples::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_peoples::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_peoples$os_get(const char *s) {
  return c_peoples::os_get(s, -1);
}
Variant &cw_peoples$os_lval(const char *s) {
  return c_peoples::os_lval(s, -1);
}
Variant cw_peoples$os_constant(const char *s) {
  return c_peoples::os_constant(s);
}
Variant cw_peoples$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_peoples::os_invoke(c, s, params, -1, fatal);
}
void c_peoples::init() {
  m_person = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 44 */
void c_peoples::t___construct() {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_person = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 49 */
bool c_peoples::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetExists);
  return LINE(51,x_array_key_exists(v_index, m_person));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 54 */
Variant c_peoples::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  if (LINE(56,x_is_array(m_person.rvalAt(v_index)))) {
    return ((Object)(LINE(58,p_arrayaccessreferenceproxy(p_arrayaccessreferenceproxy(NEWOBJ(c_arrayaccessreferenceproxy)())->create(p_arrayaccess(this), ref(lval(m_person)), v_index)))));
  }
  else {
    return m_person.rvalAt(v_index);
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 54 */
Variant &c_peoples::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 66 */
void c_peoples::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetSet);
  m_person.set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php line 71 */
void c_peoples::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(Peoples, Peoples::offsetUnset);
  DECLARE_GLOBAL_VARIABLES(g);
  m_person.weakRemove(v_index);
} /* function */
Object co_arrayaccessreferenceproxy(CArrRef params, bool init /* = true */) {
  return Object(p_arrayaccessreferenceproxy(NEW(c_arrayaccessreferenceproxy)())->dynCreate(params, init));
}
Object co_peoples(CArrRef params, bool init /* = true */) {
  return Object(p_peoples(NEW(c_peoples)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_011_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_011.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_011_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_people __attribute__((__unused__)) = (variables != gVariables) ? variables->get("people") : g->GV(people);

  (v_people = ((Object)(LINE(77,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(79,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", (concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Bar")), 0x0BCDB293DC3CBDDCLL);
  LINE(81,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  concat_assign(lval(lval(lval(v_people.o_lval("person", 0x67AE1F9174AB011DLL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Baz");
  LINE(83,x_var_dump(1, v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("===ArrayOverloading===\n");
  (v_people = ((Object)(LINE(87,p_peoples(p_peoples(NEWOBJ(c_peoples)())->create())))));
  LINE(89,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(90,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("FooBar"), 0x0BCDB293DC3CBDDCLL);
  LINE(92,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", (concat(toString(v_people.o_get("person", 0x67AE1F9174AB011DLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Bar")), 0x0BCDB293DC3CBDDCLL);
  LINE(94,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  concat_assign(lval(lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt("name", 0x0BCDB293DC3CBDDCLL)), "Baz");
  LINE(96,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(unsetLval(v_people, 0LL, 0x77CFA1EEF01BCA90LL)).weakRemove("name", 0x0BCDB293DC3CBDDCLL);
  LINE(98,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(99,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  lval(v_people.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set("name", ("BlaBla"), 0x0BCDB293DC3CBDDCLL);
  LINE(101,x_var_dump(1, v_people.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
