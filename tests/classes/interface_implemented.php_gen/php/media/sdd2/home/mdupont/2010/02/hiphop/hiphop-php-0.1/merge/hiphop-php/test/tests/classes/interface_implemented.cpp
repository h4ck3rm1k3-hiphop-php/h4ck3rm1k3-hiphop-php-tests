
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 27 */
Variant c_derived_a::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_derived_a::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_derived_a::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_derived_a::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_derived_a::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_derived_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_derived_a::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_derived_a::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(derived_a)
ObjectData *c_derived_a::cloneImpl() {
  c_derived_a *obj = NEW(c_derived_a)();
  cloneSet(obj);
  return obj;
}
void c_derived_a::cloneSet(c_derived_a *clone) {
  c_base::cloneSet(clone);
}
Variant c_derived_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_derived_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived_a$os_get(const char *s) {
  return c_derived_a::os_get(s, -1);
}
Variant &cw_derived_a$os_lval(const char *s) {
  return c_derived_a::os_lval(s, -1);
}
Variant cw_derived_a$os_constant(const char *s) {
  return c_derived_a::os_constant(s);
}
Variant cw_derived_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived_a::os_invoke(c, s, params, -1, fatal);
}
void c_derived_a::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 28 */
void c_derived_a::t_f_a() {
  INSTANCE_METHOD_INJECTION(derived_a, derived_a::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 31 */
Variant c_derived_b::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_derived_b::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_derived_b::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_derived_b::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_derived_b::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_derived_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_derived_b::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_derived_b::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(derived_b)
ObjectData *c_derived_b::cloneImpl() {
  c_derived_b *obj = NEW(c_derived_b)();
  cloneSet(obj);
  return obj;
}
void c_derived_b::cloneSet(c_derived_b *clone) {
  c_base::cloneSet(clone);
}
Variant c_derived_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_derived_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived_b$os_get(const char *s) {
  return c_derived_b::os_get(s, -1);
}
Variant &cw_derived_b$os_lval(const char *s) {
  return c_derived_b::os_lval(s, -1);
}
Variant cw_derived_b$os_constant(const char *s) {
  return c_derived_b::os_constant(s);
}
Variant cw_derived_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived_b::os_invoke(c, s, params, -1, fatal);
}
void c_derived_b::init() {
  c_base::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 32 */
void c_derived_b::t_f_a() {
  INSTANCE_METHOD_INJECTION(derived_b, derived_b::f_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 33 */
void c_derived_b::t_f_b() {
  INSTANCE_METHOD_INJECTION(derived_b, derived_b::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 36 */
Variant c_derived_c::os_get(const char *s, int64 hash) {
  return c_derived_a::os_get(s, hash);
}
Variant &c_derived_c::os_lval(const char *s, int64 hash) {
  return c_derived_a::os_lval(s, hash);
}
void c_derived_c::o_get(ArrayElementVec &props) const {
  c_derived_a::o_get(props);
}
bool c_derived_c::o_exists(CStrRef s, int64 hash) const {
  return c_derived_a::o_exists(s, hash);
}
Variant c_derived_c::o_get(CStrRef s, int64 hash) {
  return c_derived_a::o_get(s, hash);
}
Variant c_derived_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_derived_a::o_set(s, hash, v, forInit);
}
Variant &c_derived_c::o_lval(CStrRef s, int64 hash) {
  return c_derived_a::o_lval(s, hash);
}
Variant c_derived_c::os_constant(const char *s) {
  return c_derived_a::os_constant(s);
}
IMPLEMENT_CLASS(derived_c)
ObjectData *c_derived_c::cloneImpl() {
  c_derived_c *obj = NEW(c_derived_c)();
  cloneSet(obj);
  return obj;
}
void c_derived_c::cloneSet(c_derived_c *clone) {
  c_derived_a::cloneSet(clone);
}
Variant c_derived_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_derived_a::o_invoke(s, params, hash, fatal);
}
Variant c_derived_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_derived_a::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_derived_a::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived_c$os_get(const char *s) {
  return c_derived_c::os_get(s, -1);
}
Variant &cw_derived_c$os_lval(const char *s) {
  return c_derived_c::os_lval(s, -1);
}
Variant cw_derived_c$os_constant(const char *s) {
  return c_derived_c::os_constant(s);
}
Variant cw_derived_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived_c::os_invoke(c, s, params, -1, fatal);
}
void c_derived_c::init() {
  c_derived_a::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 37 */
void c_derived_c::t_f_b() {
  INSTANCE_METHOD_INJECTION(derived_c, derived_c::f_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 40 */
Variant c_derived_d::os_get(const char *s, int64 hash) {
  return c_derived_c::os_get(s, hash);
}
Variant &c_derived_d::os_lval(const char *s, int64 hash) {
  return c_derived_c::os_lval(s, hash);
}
void c_derived_d::o_get(ArrayElementVec &props) const {
  c_derived_c::o_get(props);
}
bool c_derived_d::o_exists(CStrRef s, int64 hash) const {
  return c_derived_c::o_exists(s, hash);
}
Variant c_derived_d::o_get(CStrRef s, int64 hash) {
  return c_derived_c::o_get(s, hash);
}
Variant c_derived_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_derived_c::o_set(s, hash, v, forInit);
}
Variant &c_derived_d::o_lval(CStrRef s, int64 hash) {
  return c_derived_c::o_lval(s, hash);
}
Variant c_derived_d::os_constant(const char *s) {
  return c_derived_c::os_constant(s);
}
IMPLEMENT_CLASS(derived_d)
ObjectData *c_derived_d::cloneImpl() {
  c_derived_d *obj = NEW(c_derived_d)();
  cloneSet(obj);
  return obj;
}
void c_derived_d::cloneSet(c_derived_d *clone) {
  c_derived_c::cloneSet(clone);
}
Variant c_derived_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_derived_c::o_invoke(s, params, hash, fatal);
}
Variant c_derived_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x7918298849760025LL, f_a) {
        return (t_f_a(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4AF340E66305ED5ELL, f_b) {
        return (t_f_b(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_derived_c::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_derived_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_derived_c::os_invoke(c, s, params, hash, fatal);
}
Variant cw_derived_d$os_get(const char *s) {
  return c_derived_d::os_get(s, -1);
}
Variant &cw_derived_d$os_lval(const char *s) {
  return c_derived_d::os_lval(s, -1);
}
Variant cw_derived_d$os_constant(const char *s) {
  return c_derived_d::os_constant(s);
}
Variant cw_derived_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_derived_d::os_invoke(c, s, params, -1, fatal);
}
void c_derived_d::init() {
  c_derived_c::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 11 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 12 */
void c_base::t__is_a(CStrRef v_sub) {
  INSTANCE_METHOD_INJECTION(base, base::_is_a);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  echo(concat("is_a(", LINE(13,(assignCallTemp(eo_0, toString(x_get_class(((Object)(this))))),assignCallTemp(eo_2, v_sub),assignCallTemp(eo_4, (toString((instanceOf(((Object)(this)), v_sub)) ? (("yes")) : (("no"))))),concat6(eo_0, ", ", eo_2, ") = ", eo_4, "\n")))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 15 */
void c_base::t_test() {
  INSTANCE_METHOD_INJECTION(base, base::test);
  echo((LINE(16,t__is_a("base")), null));
  echo((LINE(17,t__is_a("derived_a")), null));
  echo((LINE(18,t__is_a("derived_b")), null));
  echo((LINE(19,t__is_a("derived_c")), null));
  echo((LINE(20,t__is_a("derived_d")), null));
  echo((LINE(21,t__is_a("if_a")), null));
  echo((LINE(22,t__is_a("if_b")), null));
  echo("\n");
} /* function */
Object co_derived_a(CArrRef params, bool init /* = true */) {
  return Object(p_derived_a(NEW(c_derived_a)())->dynCreate(params, init));
}
Object co_derived_b(CArrRef params, bool init /* = true */) {
  return Object(p_derived_b(NEW(c_derived_b)())->dynCreate(params, init));
}
Object co_derived_c(CArrRef params, bool init /* = true */) {
  return Object(p_derived_c(NEW(c_derived_c)())->dynCreate(params, init));
}
Object co_derived_d(CArrRef params, bool init /* = true */) {
  return Object(p_derived_d(NEW(c_derived_d)())->dynCreate(params, init));
}
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_implemented_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_implemented_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_t = ((Object)(LINE(43,p_base(p_base(NEWOBJ(c_base)())->create())))));
  LINE(44,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  (v_t = ((Object)(LINE(46,p_derived_a(p_derived_a(NEWOBJ(c_derived_a)())->create())))));
  LINE(47,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  (v_t = ((Object)(LINE(49,p_derived_b(p_derived_b(NEWOBJ(c_derived_b)())->create())))));
  LINE(50,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  (v_t = ((Object)(LINE(52,p_derived_c(p_derived_c(NEWOBJ(c_derived_c)())->create())))));
  LINE(53,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  (v_t = ((Object)(LINE(55,p_derived_d(p_derived_d(NEWOBJ(c_derived_d)())->create())))));
  LINE(56,v_t.o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
