
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_implemented_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_implemented_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.fw.h>

// Declarations
#include <cls/if_a.h>
#include <cls/if_b.h>
#include <cls/derived_a.h>
#include <cls/derived_b.h>
#include <cls/derived_c.h>
#include <cls/derived_d.h>
#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$interface_implemented_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_derived_a(CArrRef params, bool init = true);
Object co_derived_b(CArrRef params, bool init = true);
Object co_derived_c(CArrRef params, bool init = true);
Object co_derived_d(CArrRef params, bool init = true);
Object co_base(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_classes_interface_implemented_h__
