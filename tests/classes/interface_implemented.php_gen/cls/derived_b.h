
#ifndef __GENERATED_cls_derived_b_h__
#define __GENERATED_cls_derived_b_h__

#include <cls/base.h>
#include <cls/if_a.h>
#include <cls/if_b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 31 */
class c_derived_b : virtual public c_base, virtual public c_if_a, virtual public c_if_b {
  BEGIN_CLASS_MAP(derived_b)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
  END_CLASS_MAP(derived_b)
  DECLARE_CLASS(derived_b, derived_b, base)
  void init();
  public: void t_f_a();
  public: void t_f_b();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_b_h__
