
#ifndef __GENERATED_cls_derived_c_h__
#define __GENERATED_cls_derived_c_h__

#include <cls/derived_a.h>
#include <cls/if_b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 36 */
class c_derived_c : virtual public c_derived_a, virtual public c_if_b {
  BEGIN_CLASS_MAP(derived_c)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(derived_a)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
  END_CLASS_MAP(derived_c)
  DECLARE_CLASS(derived_c, derived_c, derived_a)
  void init();
  public: void t_f_b();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_c_h__
