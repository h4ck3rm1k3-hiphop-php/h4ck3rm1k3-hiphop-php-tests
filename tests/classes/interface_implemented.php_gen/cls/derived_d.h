
#ifndef __GENERATED_cls_derived_d_h__
#define __GENERATED_cls_derived_d_h__

#include <cls/derived_c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_implemented.php line 40 */
class c_derived_d : virtual public c_derived_c {
  BEGIN_CLASS_MAP(derived_d)
    PARENT_CLASS(base)
    PARENT_CLASS(if_a)
    PARENT_CLASS(derived_a)
    PARENT_CLASS(if_a)
    PARENT_CLASS(if_b)
    PARENT_CLASS(derived_c)
  END_CLASS_MAP(derived_d)
  DECLARE_CLASS(derived_d, derived_d, derived_c)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_d_h__
