
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_derived_a(CArrRef params, bool init = true);
Variant cw_derived_a$os_get(const char *s);
Variant &cw_derived_a$os_lval(const char *s);
Variant cw_derived_a$os_constant(const char *s);
Variant cw_derived_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_derived_b(CArrRef params, bool init = true);
Variant cw_derived_b$os_get(const char *s);
Variant &cw_derived_b$os_lval(const char *s);
Variant cw_derived_b$os_constant(const char *s);
Variant cw_derived_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_derived_c(CArrRef params, bool init = true);
Variant cw_derived_c$os_get(const char *s);
Variant &cw_derived_c$os_lval(const char *s);
Variant cw_derived_c$os_constant(const char *s);
Variant cw_derived_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_derived_d(CArrRef params, bool init = true);
Variant cw_derived_d$os_get(const char *s);
Variant &cw_derived_d$os_lval(const char *s);
Variant cw_derived_d$os_constant(const char *s);
Variant cw_derived_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_base(CArrRef params, bool init = true);
Variant cw_base$os_get(const char *s);
Variant &cw_base$os_lval(const char *s);
Variant cw_base$os_constant(const char *s);
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_CREATE_OBJECT(0x62D2775E17EE82D1LL, base);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x2B05B2B46091B6A5LL, derived_a);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x529C564B1C8E8E87LL, derived_c);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x37072B442DDD92E9LL, derived_d);
      break;
    case 11:
      HASH_CREATE_OBJECT(0x2E9D023065F8889BLL, derived_b);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x62D2775E17EE82D1LL, base);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x2B05B2B46091B6A5LL, derived_a);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x529C564B1C8E8E87LL, derived_c);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x37072B442DDD92E9LL, derived_d);
      break;
    case 11:
      HASH_INVOKE_STATIC_METHOD(0x2E9D023065F8889BLL, derived_b);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x62D2775E17EE82D1LL, base);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x2B05B2B46091B6A5LL, derived_a);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x529C564B1C8E8E87LL, derived_c);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x37072B442DDD92E9LL, derived_d);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY(0x2E9D023065F8889BLL, derived_b);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x62D2775E17EE82D1LL, base);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x2B05B2B46091B6A5LL, derived_a);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x529C564B1C8E8E87LL, derived_c);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x37072B442DDD92E9LL, derived_d);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY_LV(0x2E9D023065F8889BLL, derived_b);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x62D2775E17EE82D1LL, base);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x2B05B2B46091B6A5LL, derived_a);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x529C564B1C8E8E87LL, derived_c);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x37072B442DDD92E9LL, derived_d);
      break;
    case 11:
      HASH_GET_CLASS_CONSTANT(0x2E9D023065F8889BLL, derived_b);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
