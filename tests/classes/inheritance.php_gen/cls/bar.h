
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__

#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/inheritance.php line 18 */
class c_bar : virtual public c_foo {
  BEGIN_CLASS_MAP(bar)
    PARENT_CLASS(foo)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, bar, foo)
  void init();
  public: Variant m_c;
  public: void t_display();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
