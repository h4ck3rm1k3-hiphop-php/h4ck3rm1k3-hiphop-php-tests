
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_003.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_003.php line 3 */
Variant c_b::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x32C769EE5C5509B0LL, g->s_b_DupIdc,
                  c);
      break;
    case 2:
      HASH_RETURN(0x4292CEE227B9150ALL, g->s_b_DupIda,
                  a);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x0DA7522EE0AE69FCLL, g->q_b_cc, cc);
      break;
    case 3:
      HASH_RETURN(0x5F5D9E1F8DFEE5D3LL, g->q_b_ca, ca);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
}
void c_b::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_b_DupIda = "hello from A" /* a::MY_CONST */;
  g->s_b_DupIdc = "hello from C" /* c::MY_CONST */;
}
void csi_b() {
  c_b::os_static_initializer();
}
GlobalVariables *c_b::lazy_initializer(GlobalVariables *g) {
  if (!g->csf_b) {
    g->csf_b = true;
    g->q_b_ca = "hello from A" /* a::MY_CONST */;
    g->q_b_cc = "hello from C" /* c::MY_CONST */;
  }
  return g;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_003.php line 11 */
const StaticString q_c_MY_CONST = "hello from C";
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x2A94B0C074C2CE3ALL, q_c_MY_CONST, MY_CONST);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/constants_basic_003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$constants_basic_003_inc(false, variables));
  LINE(16,x_var_dump(1, c_b::lazy_initializer(g)->s_b_DupIda));
  LINE(17,x_var_dump(1, c_b::lazy_initializer(g)->s_b_DupIdc));
  LINE(18,x_var_dump(1, "hello from A" /* b::ca */));
  LINE(19,x_var_dump(1, "hello from C" /* b::cc */));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
