
#ifndef __GENERATED_cls_fail_h__
#define __GENERATED_cls_fail_h__

#include <cls/pass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/private_004.php line 13 */
class c_fail : virtual public c_pass {
  BEGIN_CLASS_MAP(fail)
    PARENT_CLASS(pass)
  END_CLASS_MAP(fail)
  DECLARE_CLASS(fail, fail, pass)
  void init();
  public: static void ti_do_show(const char* cls);
  public: static void t_do_show() { ti_do_show("fail"); }
  public: static void t_show() { ti_show("fail"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fail_h__
