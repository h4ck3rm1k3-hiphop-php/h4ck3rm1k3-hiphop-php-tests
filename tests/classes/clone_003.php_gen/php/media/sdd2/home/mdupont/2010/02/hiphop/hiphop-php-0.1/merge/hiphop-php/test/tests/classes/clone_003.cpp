
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_003.php line 2 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("p1", m_p1));
  props.push_back(NEW(ArrayElement)("p2", m_p2));
  props.push_back(NEW(ArrayElement)("p3", m_p3));
  props.push_back(NEW(ArrayElement)("p4", m_p4.isReferenced() ? ref(m_p4) : m_p4));
  props.push_back(NEW(ArrayElement)("p5", m_p5));
  props.push_back(NEW(ArrayElement)("p6", m_p6));
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 7:
      HASH_EXISTS_STRING(0x5C657D380DA684F7LL, p1, 2);
      HASH_EXISTS_STRING(0x0FEFA20DB1B08AC7LL, p2, 2);
      break;
    case 12:
      HASH_EXISTS_STRING(0x20511842520A276CLL, p4, 2);
      break;
    case 13:
      HASH_EXISTS_STRING(0x7712320985A0560DLL, p5, 2);
      break;
    case 14:
      HASH_EXISTS_STRING(0x18B6B61EF19E261ELL, p3, 2);
      HASH_EXISTS_STRING(0x7ADD4C161B321ABELL, p6, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 7:
      HASH_RETURN_STRING(0x5C657D380DA684F7LL, m_p1,
                         p1, 2);
      HASH_RETURN_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                         p2, 2);
      break;
    case 12:
      HASH_RETURN_STRING(0x20511842520A276CLL, m_p4,
                         p4, 2);
      break;
    case 13:
      HASH_RETURN_STRING(0x7712320985A0560DLL, m_p5,
                         p5, 2);
      break;
    case 14:
      HASH_RETURN_STRING(0x18B6B61EF19E261ELL, m_p3,
                         p3, 2);
      HASH_RETURN_STRING(0x7ADD4C161B321ABELL, m_p6,
                         p6, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 7:
      HASH_SET_STRING(0x5C657D380DA684F7LL, m_p1,
                      p1, 2);
      HASH_SET_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                      p2, 2);
      break;
    case 12:
      HASH_SET_STRING(0x20511842520A276CLL, m_p4,
                      p4, 2);
      break;
    case 13:
      HASH_SET_STRING(0x7712320985A0560DLL, m_p5,
                      p5, 2);
      break;
    case 14:
      HASH_SET_STRING(0x18B6B61EF19E261ELL, m_p3,
                      p3, 2);
      HASH_SET_STRING(0x7ADD4C161B321ABELL, m_p6,
                      p6, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x20511842520A276CLL, m_p4,
                         p4, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  clone->m_p1 = m_p1;
  clone->m_p2 = m_p2;
  clone->m_p3 = m_p3;
  clone->m_p4 = m_p4.isReferenced() ? ref(m_p4) : m_p4;
  clone->m_p5 = m_p5;
  clone->m_p6 = m_p6;
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0F2CA10C0004BE9BLL, __clone) {
        return (t___clone());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0F2CA10C0004BE9BLL, __clone) {
        return (t___clone());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
  m_p1 = "base:1";
  m_p2 = "base:2";
  m_p3 = "base:3";
  m_p4 = "base:4";
  m_p5 = "base:5";
  m_p6 = "base:6";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_003.php line 9 */
Variant c_base::t___clone() {
  INSTANCE_METHOD_INJECTION(base, base::__clone);
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_003.php line 13 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("p1", m_p1));
  props.push_back(NEW(ArrayElement)("p3", m_p3));
  props.push_back(NEW(ArrayElement)("p4", m_p4.isReferenced() ? ref(m_p4) : m_p4));
  props.push_back(NEW(ArrayElement)("p5", m_p5));
  c_base::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  clone->m_p1 = m_p1;
  clone->m_p3 = m_p3;
  clone->m_p4 = m_p4.isReferenced() ? ref(m_p4) : m_p4;
  clone->m_p5 = m_p5;
  c_base::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0F2CA10C0004BE9BLL, __clone) {
        return (t___clone());
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0F2CA10C0004BE9BLL, __clone) {
        return (t___clone());
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  c_base::init();
  m_p1 = "test:1";
  m_p3 = "test:3";
  m_p4 = "test:4";
  m_p5 = "test:5";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_003.php line 18 */
Variant c_test::t___clone() {
  INSTANCE_METHOD_INJECTION(test, test::__clone);
  (m_p5 = "clone:5");
  return null;
} /* function */
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/clone_003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$clone_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_copy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("copy") : g->GV(copy);

  (v_obj = ((Object)(LINE(23,p_test(p_test(NEWOBJ(c_test)())->create())))));
  (v_obj.o_lval("p4", 0x20511842520A276CLL) = "A");
  (v_copy = f_clone(toObject(v_obj)));
  echo("Object\n");
  LINE(27,x_print_r(v_obj));
  echo("Clown\n");
  LINE(29,x_print_r(v_copy));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
