
#ifndef __GENERATED_cls_base_h__
#define __GENERATED_cls_base_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor1.php line 3 */
class c_base : virtual public ObjectData {
  BEGIN_CLASS_MAP(base)
  END_CLASS_MAP(base)
  DECLARE_CLASS(base, Base, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_base_h__
