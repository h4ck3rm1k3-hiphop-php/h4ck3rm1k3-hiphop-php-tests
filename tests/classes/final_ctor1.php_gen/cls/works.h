
#ifndef __GENERATED_cls_works_h__
#define __GENERATED_cls_works_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor1.php line 10 */
class c_works : virtual public c_base {
  BEGIN_CLASS_MAP(works)
    PARENT_CLASS(base)
  END_CLASS_MAP(works)
  DECLARE_CLASS(works, Works, base)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_works_h__
