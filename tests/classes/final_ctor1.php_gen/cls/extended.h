
#ifndef __GENERATED_cls_extended_h__
#define __GENERATED_cls_extended_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_ctor1.php line 14 */
class c_extended : virtual public c_base {
  BEGIN_CLASS_MAP(extended)
    PARENT_CLASS(base)
  END_CLASS_MAP(extended)
  DECLARE_CLASS(extended, Extended, base)
  void init();
  public: void t_extended();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_extended_h__
