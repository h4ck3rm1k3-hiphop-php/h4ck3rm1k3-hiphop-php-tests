
#ifndef __GENERATED_cls_c_iter_h__
#define __GENERATED_cls_c_iter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 2 */
class c_c_iter : virtual public c_iterator {
  BEGIN_CLASS_MAP(c_iter)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iterator)
  END_CLASS_MAP(c_iter)
  DECLARE_CLASS(c_iter, c_iter, ObjectData)
  void init();
  public: Variant m_obj;
  public: int64 m_num;
  public: virtual void destruct();
  public: void t___construct(p_c v_obj);
  public: ObjectData *create(p_c v_obj);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_rewind();
  public: bool t_valid();
  public: int64 t_current();
  public: void t_next();
  public: Variant t_key();
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_iter_h__
