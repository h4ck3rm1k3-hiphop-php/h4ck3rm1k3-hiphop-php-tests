
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 42 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("max", m_max));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x022BD5C706BD9850LL, max, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x022BD5C706BD9850LL, m_max,
                         max, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x022BD5C706BD9850LL, m_max,
                      max, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
void c_c::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_max = m_max;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_max = 3LL;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 46 */
p_c_iter c_c::t_getiterator() {
  INSTANCE_METHOD_INJECTION(c, c::getIterator);
  echo("c::getIterator\n");
  return ((Object)(LINE(48,p_c_iter(p_c_iter(NEWOBJ(c_c_iter)())->create(((Object)(this)))))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 50 */
Variant c_c::t___destruct() {
  INSTANCE_METHOD_INJECTION(c, c::__destruct);
  setInDtor();
  echo("c::__destruct\n");
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 2 */
Variant c_c_iter::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c_iter::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c_iter::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("obj", m_obj.isReferenced() ? ref(m_obj) : m_obj));
  props.push_back(NEW(ArrayElement)("num", m_num));
  c_ObjectData::o_get(props);
}
bool c_c_iter::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x336176791BC03F04LL, num, 3);
      break;
    case 3:
      HASH_EXISTS_STRING(0x7FB577570F61BD03LL, obj, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c_iter::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x336176791BC03F04LL, m_num,
                         num, 3);
      break;
    case 3:
      HASH_RETURN_STRING(0x7FB577570F61BD03LL, m_obj,
                         obj, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c_iter::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x336176791BC03F04LL, m_num,
                      num, 3);
      break;
    case 3:
      HASH_SET_STRING(0x7FB577570F61BD03LL, m_obj,
                      obj, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c_iter::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x7FB577570F61BD03LL, m_obj,
                         obj, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c_iter::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c_iter)
ObjectData *c_c_iter::create(p_c v_obj) {
  init();
  t___construct(v_obj);
  return this;
}
ObjectData *c_c_iter::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_c_iter::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_c_iter::cloneImpl() {
  c_c_iter *obj = NEW(c_c_iter)();
  cloneSet(obj);
  return obj;
}
void c_c_iter::cloneSet(c_c_iter *clone) {
  clone->m_obj = m_obj.isReferenced() ? ref(m_obj) : m_obj;
  clone->m_num = m_num;
  ObjectData::cloneSet(clone);
}
Variant c_c_iter::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c_iter::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c_iter::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c_iter$os_get(const char *s) {
  return c_c_iter::os_get(s, -1);
}
Variant &cw_c_iter$os_lval(const char *s) {
  return c_c_iter::os_lval(s, -1);
}
Variant cw_c_iter$os_constant(const char *s) {
  return c_c_iter::os_constant(s);
}
Variant cw_c_iter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c_iter::os_invoke(c, s, params, -1, fatal);
}
void c_c_iter::init() {
  m_obj = null;
  m_num = 0LL;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 7 */
void c_c_iter::t___construct(p_c v_obj) {
  INSTANCE_METHOD_INJECTION(c_iter, c_iter::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("c_iter::__construct\n");
  (m_obj = ((Object)(v_obj)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 11 */
void c_c_iter::t_rewind() {
  INSTANCE_METHOD_INJECTION(c_iter, c_iter::rewind);
  echo("c_iter::rewind\n");
  (m_num = 0LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 15 */
bool c_c_iter::t_valid() {
  INSTANCE_METHOD_INJECTION(c_iter, c_iter::valid);
  bool v_more = false;

  (v_more = less(m_num, m_obj.o_get("max", 0x022BD5C706BD9850LL)));
  echo(LINE(17,concat3("c_iter::valid = ", (toString(v_more ? (("true")) : (("false")))), "\n")));
  return v_more;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 20 */
int64 c_c_iter::t_current() {
  INSTANCE_METHOD_INJECTION(c_iter, c_iter::current);
  echo("c_iter::current\n");
  return m_num;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 24 */
void c_c_iter::t_next() {
  INSTANCE_METHOD_INJECTION(c_iter, c_iter::next);
  echo("c_iter::next\n");
  m_num++;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 28 */
Variant c_c_iter::t_key() {
  INSTANCE_METHOD_INJECTION(c_iter, c_iter::key);
  echo("c_iter::key\n");
  {
    switch (m_num) {
    case 0LL:
      {
        return "1st";
      }
    case 1LL:
      {
        return "2nd";
      }
    case 2LL:
      {
        return "3rd";
      }
    default:
      {
        return "\?\?\?";
      }
    }
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php line 37 */
Variant c_c_iter::t___destruct() {
  INSTANCE_METHOD_INJECTION(c_iter, c_iter::__destruct);
  setInDtor();
  echo("c_iter::__destruct\n");
  return null;
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_c_iter(CArrRef params, bool init /* = true */) {
  return Object(p_c_iter(NEW(c_c_iter)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$iterators_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/iterators_002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$iterators_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);

  (v_t = ((Object)(LINE(55,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_t.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_v = iter4->second();
      v_k = iter4->first();
      {
        {
          LOOP_COUNTER(5);
          for (ArrayIterPtr iter7 = v_t.begin(); !iter7->end(); iter7->next()) {
            LOOP_COUNTER_CHECK(5);
            v_w = iter7->second();
            {
              echo(LINE(59,concat5("double:", toString(v_v), ":", toString(v_w), "\n")));
              break;
            }
          }
        }
      }
    }
  }
  unset(v_t);
  print("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
