
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__

#include <cls/i1.h>
#include <cls/i2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/interface_constant_inheritance_003.php line 10 */
class c_c : virtual public c_i1, virtual public c_i2 {
  BEGIN_CLASS_MAP(c)
    PARENT_CLASS(i1)
    PARENT_CLASS(i2)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
