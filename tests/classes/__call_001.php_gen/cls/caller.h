
#ifndef __GENERATED_cls_caller_h__
#define __GENERATED_cls_caller_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/__call_001.php line 3 */
class c_caller : virtual public ObjectData {
  BEGIN_CLASS_MAP(caller)
  END_CLASS_MAP(caller)
  DECLARE_CLASS(caller, Caller, ObjectData)
  void init();
  public: Array m_x;
  Variant doCall(Variant v_name, Variant v_arguments, bool fatal);
  public: Variant t___call(Variant v_m, Variant v_a);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_caller_h__
