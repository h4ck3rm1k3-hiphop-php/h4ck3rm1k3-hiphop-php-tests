
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php line 3 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php line 5 */
bool c_test::t_offsetexists(CVarRef v_offset) {
  INSTANCE_METHOD_INJECTION(Test, Test::offsetExists);
  throw_exception(((Object)(LINE(5,p_exception(p_exception(NEWOBJ(c_exception)())->create("Test::offsetExists"))))));
  return false;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php line 6 */
Variant c_test::t_offsetget(Variant v_offset) {
  INSTANCE_METHOD_INJECTION(Test, Test::offsetGet);
  throw_exception(((Object)(LINE(6,p_exception(p_exception(NEWOBJ(c_exception)())->create("Test::offsetGet"))))));
  return v_offset;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php line 6 */
Variant &c_test::___offsetget_lval(Variant v_offset) {
  INSTANCE_METHOD_INJECTION(Test, Test::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_offset);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php line 7 */
void c_test::t_offsetset(CVarRef v_offset, CVarRef v_data) {
  INSTANCE_METHOD_INJECTION(Test, Test::offsetSet);
  throw_exception(((Object)(LINE(7,p_exception(p_exception(NEWOBJ(c_exception)())->create("Test::offsetSet"))))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php line 8 */
void c_test::t_offsetunset(CVarRef v_offset) {
  INSTANCE_METHOD_INJECTION(Test, Test::offsetUnset);
  throw_exception(((Object)(LINE(8,p_exception(p_exception(NEWOBJ(c_exception)())->create("Test::offsetUnset"))))));
} /* function */
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_013_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_013_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  (v_t = ((Object)(LINE(11,p_test(p_test(NEWOBJ(c_test)())->create())))));
  try {
    echo(toString(isset(v_t, 0LL, 0x77CFA1EEF01BCA90LL)));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(LINE(19,(assignCallTemp(eo_1, toString(v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Caught in ", eo_1, "()\n"))));
    } else {
      throw;
    }
  }
  try {
    echo(toString(v_t.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(LINE(28,(assignCallTemp(eo_1, toString(v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Caught in ", eo_1, "()\n"))));
    } else {
      throw;
    }
  }
  try {
    v_t.set(0LL, (1LL), 0x77CFA1EEF01BCA90LL);
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(LINE(37,(assignCallTemp(eo_1, toString(v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Caught in ", eo_1, "()\n"))));
    } else {
      throw;
    }
  }
  try {
    v_t.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(LINE(46,(assignCallTemp(eo_1, toString(v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Caught in ", eo_1, "()\n"))));
    } else {
      throw;
    }
  }
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
