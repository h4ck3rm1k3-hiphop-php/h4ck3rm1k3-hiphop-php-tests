
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_013.php line 3 */
class c_test : virtual public c_arrayaccess {
  BEGIN_CLASS_MAP(test)
    PARENT_CLASS(arrayaccess)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, Test, ObjectData)
  void init();
  public: bool t_offsetexists(CVarRef v_offset);
  public: Variant t_offsetget(Variant v_offset);
  public: virtual Variant &___offsetget_lval(Variant v_offset);
  public: void t_offsetset(CVarRef v_offset, CVarRef v_data);
  public: void t_offsetunset(CVarRef v_offset);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
