
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_FOO1;
extern const int64 k_FOO2;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 2:
      HASH_RETURN(0x73F4ECE5B2992FB2LL, k_FOO1, FOO1);
      break;
    case 3:
      HASH_RETURN(0x5E8A4ABBBDD06CD7LL, k_FOO2, FOO2);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
