
#ifndef __GENERATED_cls_b_h__
#define __GENERATED_cls_b_h__

#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/bug23951.php line 12 */
class c_b : virtual public c_a {
  BEGIN_CLASS_MAP(b)
    PARENT_CLASS(a)
  END_CLASS_MAP(b)
  DECLARE_CLASS(b, B, a)
  void init();
  public: String m_b_var;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_b_h__
