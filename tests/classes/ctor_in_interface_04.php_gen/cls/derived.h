
#ifndef __GENERATED_cls_derived_h__
#define __GENERATED_cls_derived_h__

#include <cls/implem.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/ctor_in_interface_04.php line 14 */
class c_derived : virtual public c_implem {
  BEGIN_CLASS_MAP(derived)
    PARENT_CLASS(constr)
    PARENT_CLASS(implem)
  END_CLASS_MAP(derived)
  DECLARE_CLASS(derived, derived, implem)
  void init();
  public: void t___construct(Variant v_a);
  public: ObjectData *create(Variant v_a);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_h__
