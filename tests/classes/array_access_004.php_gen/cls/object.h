
#ifndef __GENERATED_cls_object_h__
#define __GENERATED_cls_object_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_004.php line 2 */
class c_object : virtual public c_arrayaccess {
  BEGIN_CLASS_MAP(object)
    PARENT_CLASS(arrayaccess)
  END_CLASS_MAP(object)
  DECLARE_CLASS(object, object, ObjectData)
  void init();
  public: Array m_a;
  public: bool t_offsetexists(CVarRef v_index);
  public: Variant t_offsetget(Variant v_index);
  public: virtual Variant &___offsetget_lval(Variant v_index);
  public: void t_offsetset(CVarRef v_index, CVarRef v_newval);
  public: void t_offsetunset(CVarRef v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_object_h__
