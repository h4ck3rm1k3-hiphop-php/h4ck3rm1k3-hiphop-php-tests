
#ifndef __GENERATED_cls_fail_h__
#define __GENERATED_cls_fail_h__

#include <cls/pass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/final_redeclare.php line 11 */
class c_fail : virtual public c_pass {
  BEGIN_CLASS_MAP(fail)
    PARENT_CLASS(pass)
  END_CLASS_MAP(fail)
  DECLARE_CLASS(fail, fail, pass)
  void init();
  public: void t_show();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fail_h__
