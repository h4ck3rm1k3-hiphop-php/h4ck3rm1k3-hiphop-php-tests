
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php line 3 */
Variant c_name::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_name::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_name::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_name::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_name::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_name::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_name::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_name::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(name)
ObjectData *c_name::create(CVarRef v__name) {
  init();
  t_name(v__name);
  return this;
}
ObjectData *c_name::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_name::cloneImpl() {
  c_name *obj = NEW(c_name)();
  cloneSet(obj);
  return obj;
}
void c_name::cloneSet(c_name *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_name::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x797A6FC6339ADA9ELL, display) {
        return (t_display(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_name::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x797A6FC6339ADA9ELL, display) {
        return (t_display(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_name::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_name$os_get(const char *s) {
  return c_name::os_get(s, -1);
}
Variant &cw_name$os_lval(const char *s) {
  return c_name::os_lval(s, -1);
}
Variant cw_name$os_constant(const char *s) {
  return c_name::os_constant(s);
}
Variant cw_name$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_name::os_invoke(c, s, params, -1, fatal);
}
void c_name::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php line 4 */
void c_name::t_name(CVarRef v__name) {
  INSTANCE_METHOD_INJECTION(Name, Name::Name);
  bool oldInCtor = gasInCtor(true);
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = v__name);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php line 8 */
void c_name::t_display() {
  INSTANCE_METHOD_INJECTION(Name, Name::display);
  echo(concat(toString(o_get("name", 0x0BCDB293DC3CBDDCLL)), "\n"));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php line 13 */
Variant c_person::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_person::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_person::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  c_ObjectData::o_get(props);
}
bool c_person::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_person::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_person::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_person::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_person::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(person)
ObjectData *c_person::create(CStrRef v__name, CStrRef v__address) {
  init();
  t_person(v__name, v__address);
  return this;
}
ObjectData *c_person::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
ObjectData *c_person::cloneImpl() {
  c_person *obj = NEW(c_person)();
  cloneSet(obj);
  return obj;
}
void c_person::cloneSet(c_person *clone) {
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  ObjectData::cloneSet(clone);
}
Variant c_person::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_person::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_person::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_person$os_get(const char *s) {
  return c_person::os_get(s, -1);
}
Variant &cw_person$os_lval(const char *s) {
  return c_person::os_lval(s, -1);
}
Variant cw_person$os_constant(const char *s) {
  return c_person::os_constant(s);
}
Variant cw_person$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_person::os_invoke(c, s, params, -1, fatal);
}
void c_person::init() {
  m_name = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php line 16 */
void c_person::t_person(CStrRef v__name, CStrRef v__address) {
  INSTANCE_METHOD_INJECTION(Person, Person::person);
  bool oldInCtor = gasInCtor(true);
  (m_name = ((Object)(LINE(17,p_name(p_name(NEWOBJ(c_name)())->create(v__name))))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php line 20 */
Variant c_person::t_getname() {
  INSTANCE_METHOD_INJECTION(Person, Person::getName);
  return m_name;
} /* function */
Object co_name(CArrRef params, bool init /* = true */) {
  return Object(p_name(NEW(c_name)())->dynCreate(params, init));
}
Object co_person(CArrRef params, bool init /* = true */) {
  return Object(p_person(NEW(c_person)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$dereferencing_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$dereferencing_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_person __attribute__((__unused__)) = (variables != gVariables) ? variables->get("person") : g->GV(person);

  (v_person = ((Object)(LINE(25,p_person(p_person(NEWOBJ(c_person)())->create("John", "New York"))))));
  (assignCallTemp(eo_0, toObject(LINE(26,v_person.o_invoke_few_args("getName", 0x23F51CDECC198965LL, 0)))),eo_0.o_invoke_few_args("display", 0x797A6FC6339ADA9ELL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
