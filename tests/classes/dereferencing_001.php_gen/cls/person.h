
#ifndef __GENERATED_cls_person_h__
#define __GENERATED_cls_person_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/dereferencing_001.php line 13 */
class c_person : virtual public ObjectData {
  BEGIN_CLASS_MAP(person)
  END_CLASS_MAP(person)
  DECLARE_CLASS(person, Person, ObjectData)
  void init();
  public: Variant m_name;
  public: void t_person(CStrRef v__name, CStrRef v__address);
  public: ObjectData *create(CStrRef v__name, CStrRef v__address);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_getname();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_person_h__
