
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.php line 3 */
Variant c_failclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_failclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_failclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("fatal", m_fatal));
  c_ObjectData::o_get(props);
}
bool c_failclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x521290E6C7B656E3LL, fatal, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_failclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x521290E6C7B656E3LL, m_fatal,
                         fatal, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_failclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x521290E6C7B656E3LL, m_fatal,
                      fatal, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_failclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_failclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(failclass)
void c_failclass::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_failclass::cloneImpl() {
  c_failclass *obj = NEW(c_failclass)();
  cloneSet(obj);
  return obj;
}
void c_failclass::cloneSet(c_failclass *clone) {
  clone->m_fatal = m_fatal;
  ObjectData::cloneSet(clone);
}
Variant c_failclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_failclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_failclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_failclass$os_get(const char *s) {
  return c_failclass::os_get(s, -1);
}
Variant &cw_failclass$os_lval(const char *s) {
  return c_failclass::os_lval(s, -1);
}
Variant cw_failclass$os_constant(const char *s) {
  return c_failclass::os_constant(s);
}
Variant cw_failclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_failclass::os_invoke(c, s, params, -1, fatal);
}
void c_failclass::init() {
  m_fatal = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.php line 7 */
Variant c_failclass::t___destruct() {
  INSTANCE_METHOD_INJECTION(FailClass, FailClass::__destruct);
  setInDtor();
  echo("FailClass::__destruct\n");
  throw_exception(((Object)(LINE(10,p_exception(p_exception(NEWOBJ(c_exception)())->create("FailClass"))))));
  echo("Done: FailClass::__destruct\n");
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.php line 25 */
Variant c_fatalexception::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_fatalexception::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_fatalexception::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_fatalexception::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_fatalexception::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_fatalexception::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_fatalexception::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_fatalexception::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(fatalexception)
ObjectData *c_fatalexception::create(Variant v_what) {
  init();
  t___construct(v_what);
  return this;
}
ObjectData *c_fatalexception::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_fatalexception::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_fatalexception::cloneImpl() {
  c_fatalexception *obj = NEW(c_fatalexception)();
  cloneSet(obj);
  return obj;
}
void c_fatalexception::cloneSet(c_fatalexception *clone) {
  c_exception::cloneSet(clone);
}
Variant c_fatalexception::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x71859D7313E682D2LL, getmessage) {
        return (t_getmessage());
      }
      break;
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_fatalexception::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x71859D7313E682D2LL, getmessage) {
        return (t_getmessage());
      }
      break;
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fatalexception::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fatalexception$os_get(const char *s) {
  return c_fatalexception::os_get(s, -1);
}
Variant &cw_fatalexception$os_lval(const char *s) {
  return c_fatalexception::os_lval(s, -1);
}
Variant cw_fatalexception$os_constant(const char *s) {
  return c_fatalexception::os_constant(s);
}
Variant cw_fatalexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fatalexception::os_invoke(c, s, params, -1, fatal);
}
void c_fatalexception::init() {
  c_exception::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.php line 27 */
void c_fatalexception::t___construct(Variant v_what) {
  INSTANCE_METHOD_INJECTION(FatalException, FatalException::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_o;

  echo("FatalException::__construct\n");
  (v_o = ((Object)(LINE(30,p_failclass(p_failclass(NEWOBJ(c_failclass)())->create())))));
  unset(v_o);
  echo("Done: FatalException::__construct\n");
  gasInCtor(oldInCtor);
} /* function */
Object co_failclass(CArrRef params, bool init /* = true */) {
  return Object(p_failclass(NEW(c_failclass)())->dynCreate(params, init));
}
Object co_fatalexception(CArrRef params, bool init /* = true */) {
  return Object(p_fatalexception(NEW(c_fatalexception)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$destructor_and_exceptions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$destructor_and_exceptions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  try {
    (v_a = ((Object)(LINE(17,p_failclass(p_failclass(NEWOBJ(c_failclass)())->create())))));
    unset(v_a);
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(LINE(22,(assignCallTemp(eo_1, toString(v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Caught: ", eo_1, "\n"))));
    } else {
      throw;
    }
  }
  try {
    throw_exception(((Object)(LINE(38,p_fatalexception(p_fatalexception(NEWOBJ(c_fatalexception)())->create("Damn"))))));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(LINE(42,(assignCallTemp(eo_1, toString(v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Caught Exception: ", eo_1, "\n"))));
    } else if (e.instanceof("fatalexception")) {
      v_e = e;
      echo(LINE(46,(assignCallTemp(eo_1, toString(v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))),concat3("Caught FatalException: ", eo_1, "\n"))));
    } else {
      throw;
    }
  }
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
