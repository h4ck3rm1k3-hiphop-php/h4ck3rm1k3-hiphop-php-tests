
#ifndef __GENERATED_cls_fatalexception_h__
#define __GENERATED_cls_fatalexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.php line 25 */
class c_fatalexception : virtual public c_exception {
  BEGIN_CLASS_MAP(fatalexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(fatalexception)
  DECLARE_CLASS(fatalexception, FatalException, exception)
  void init();
  public: void t___construct(Variant v_what);
  public: ObjectData *create(Variant v_what);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fatalexception_h__
