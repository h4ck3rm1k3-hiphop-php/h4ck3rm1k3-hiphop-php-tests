
#ifndef __GENERATED_cls_failclass_h__
#define __GENERATED_cls_failclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/destructor_and_exceptions.php line 3 */
class c_failclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(failclass)
  END_CLASS_MAP(failclass)
  DECLARE_CLASS(failclass, FailClass, ObjectData)
  void init();
  public: String m_fatal;
  public: virtual void destruct();
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_failclass_h__
