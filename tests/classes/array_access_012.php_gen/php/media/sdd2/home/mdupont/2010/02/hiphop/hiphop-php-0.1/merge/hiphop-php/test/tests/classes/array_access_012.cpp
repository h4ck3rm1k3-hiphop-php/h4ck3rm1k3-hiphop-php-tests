
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php line 3 */
Variant c_arrayaccessimpl::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_arrayaccessimpl::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_arrayaccessimpl::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("data", m_data));
  c_ObjectData::o_get(props);
}
bool c_arrayaccessimpl::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x30164401A9853128LL, data, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_arrayaccessimpl::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x30164401A9853128LL, m_data,
                         data, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_arrayaccessimpl::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x30164401A9853128LL, m_data,
                      data, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_arrayaccessimpl::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_arrayaccessimpl::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(arrayaccessimpl)
ObjectData *c_arrayaccessimpl::cloneImpl() {
  c_arrayaccessimpl *obj = NEW(c_arrayaccessimpl)();
  cloneSet(obj);
  return obj;
}
void c_arrayaccessimpl::cloneSet(c_arrayaccessimpl *clone) {
  clone->m_data = m_data;
  ObjectData::cloneSet(clone);
}
Variant c_arrayaccessimpl::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(params.rvalAt(0)));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(params.rvalAt(0)));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_arrayaccessimpl::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x61D11ECEF4404498LL, offsetget) {
        return (t_offsetget(a0));
      }
      HASH_GUARD(0x3E6BCFB9742FC700LL, offsetexists) {
        return (t_offsetexists(a0));
      }
      HASH_GUARD(0x0957F693A48AF738LL, offsetset) {
        return (t_offsetset(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x08329980E6369ABALL, offsetunset) {
        return (t_offsetunset(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_arrayaccessimpl::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_arrayaccessimpl$os_get(const char *s) {
  return c_arrayaccessimpl::os_get(s, -1);
}
Variant &cw_arrayaccessimpl$os_lval(const char *s) {
  return c_arrayaccessimpl::os_lval(s, -1);
}
Variant cw_arrayaccessimpl$os_constant(const char *s) {
  return c_arrayaccessimpl::os_constant(s);
}
Variant cw_arrayaccessimpl$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_arrayaccessimpl::os_invoke(c, s, params, -1, fatal);
}
void c_arrayaccessimpl::init() {
  m_data = ScalarArrays::sa_[0];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php line 6 */
void c_arrayaccessimpl::t_offsetunset(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessImpl, ArrayAccessImpl::offsetUnset);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php line 8 */
void c_arrayaccessimpl::t_offsetset(CVarRef v_index, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(ArrayAccessImpl, ArrayAccessImpl::offsetSet);
  m_data.set(v_index, (v_value));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php line 12 */
Variant c_arrayaccessimpl::t_offsetget(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessImpl, ArrayAccessImpl::offsetGet);
  return m_data.rvalAt(v_index);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php line 12 */
Variant &c_arrayaccessimpl::___offsetget_lval(Variant v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessImpl, ArrayAccessImpl::offsetGet);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t_offsetget(v_index);
  return v;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php line 16 */
bool c_arrayaccessimpl::t_offsetexists(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(ArrayAccessImpl, ArrayAccessImpl::offsetExists);
  return isset(m_data, v_index);
} /* function */
Object co_arrayaccessimpl(CArrRef params, bool init /* = true */) {
  return Object(p_arrayaccessimpl(NEW(c_arrayaccessimpl)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_012_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$classes$array_access_012_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  (v_data = ((Object)(LINE(21,p_arrayaccessimpl(p_arrayaccessimpl(NEWOBJ(c_arrayaccessimpl)())->create())))));
  (v_test = "some data");
  v_data.set("element", (null), 0x1AFD3E7207DEC289LL);
  v_data.set("element", (ref(v_test)), 0x1AFD3E7207DEC289LL);
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
