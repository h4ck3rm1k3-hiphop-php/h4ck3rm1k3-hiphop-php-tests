
#ifndef __GENERATED_cls_arrayaccessimpl_h__
#define __GENERATED_cls_arrayaccessimpl_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/array_access_012.php line 3 */
class c_arrayaccessimpl : virtual public c_arrayaccess {
  BEGIN_CLASS_MAP(arrayaccessimpl)
    PARENT_CLASS(arrayaccess)
  END_CLASS_MAP(arrayaccessimpl)
  DECLARE_CLASS(arrayaccessimpl, ArrayAccessImpl, ObjectData)
  void init();
  public: Array m_data;
  public: void t_offsetunset(CVarRef v_index);
  public: void t_offsetset(CVarRef v_index, CVarRef v_value);
  public: Variant t_offsetget(Variant v_index);
  public: virtual Variant &___offsetget_lval(Variant v_index);
  public: bool t_offsetexists(CVarRef v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_arrayaccessimpl_h__
