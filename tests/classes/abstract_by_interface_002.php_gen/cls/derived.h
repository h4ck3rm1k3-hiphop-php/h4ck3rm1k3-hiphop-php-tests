
#ifndef __GENERATED_cls_derived_h__
#define __GENERATED_cls_derived_h__

#include <cls/root.h>
#include <cls/myinterface.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/classes/abstract_by_interface_002.php line 11 */
class c_derived : virtual public c_root, virtual public c_myinterface {
  BEGIN_CLASS_MAP(derived)
    PARENT_CLASS(root)
    PARENT_CLASS(myinterface)
  END_CLASS_MAP(derived)
  DECLARE_CLASS(derived, Derived, root)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_derived_h__
