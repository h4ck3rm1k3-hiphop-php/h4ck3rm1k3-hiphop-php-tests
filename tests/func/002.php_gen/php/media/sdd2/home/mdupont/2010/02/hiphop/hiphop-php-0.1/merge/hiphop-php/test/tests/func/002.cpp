
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/002.php line 2 */
void f_blah() {
  FUNCTION_INJECTION(blah);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_hey __attribute__((__unused__)) = g->sv_blah_DupIdhey;
  bool &inited_sv_hey __attribute__((__unused__)) = g->inited_sv_blah_DupIdhey;
  int64 &sv_yo __attribute__((__unused__)) = g->sv_blah_DupIdyo;
  bool &inited_sv_yo __attribute__((__unused__)) = g->inited_sv_blah_DupIdyo;
  {
    if (!inited_sv_hey) {
      (sv_hey = 0LL);
      inited_sv_hey = true;
    }
    if (!inited_sv_yo) {
      (sv_yo = 0LL);
      inited_sv_yo = true;
    }
  }
  {
    echo(LINE(6,(assignCallTemp(eo_1, toString(sv_hey++)),concat3("hey=", eo_1, ", "))));
    echo(concat(toString(sv_yo--), "\n"));
  }
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_hey __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hey") : g->GV(hey);
  Variant &v_yo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("yo") : g->GV(yo);

  LINE(9,f_blah());
  LINE(10,f_blah());
  LINE(11,f_blah());
  if (isset(v_hey) || isset(v_yo)) {
    echo("Local variables became global :(\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
