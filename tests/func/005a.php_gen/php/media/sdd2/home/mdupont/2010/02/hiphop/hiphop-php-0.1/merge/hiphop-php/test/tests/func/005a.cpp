
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/005a.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/005a.php line 7 */
void f_boo() {
  FUNCTION_INJECTION(boo);
  echo("Shutdown\n");
} /* function */
Variant i_boo(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x38803DA17FF5469CLL, boo) {
    return (f_boo(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$005a_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/005a.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$005a_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_ini_set("display_errors", toString(0LL)));
  echo("Start\n");
  LINE(12,x_register_shutdown_function(1, "boo"));
  LINE(15,x_set_time_limit(toInt32(1LL)));
  {
    LOOP_COUNTER(1);
    for (; ; ) {
      LOOP_COUNTER_CHECK(1);
      {
      }
    }
  }
  echo("End\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
