
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/007.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$007_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/007.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$007_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ini1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ini1") : g->GV(ini1);
  Variant &v_ini2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ini2") : g->GV(ini2);

  (v_ini1 = LINE(3,x_ini_get("include_path")));
  LINE(4,x_ini_set("include_path", "ini_set_works"));
  echo(concat(LINE(5,x_ini_get("include_path")), "\n"));
  LINE(6,x_ini_restore("include_path"));
  (v_ini2 = LINE(7,x_ini_get("include_path")));
  if (!same(v_ini1, v_ini2)) {
    echo("ini_restore() does not work.\n");
  }
  else {
    echo("ini_restore_works\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
