
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/010.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/010.php line 14 */
void f_test2(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(test2);
  if (!same(v_a, v_b)) {
    LINE(17,x_var_dump(1, concat4("something went wrong: ", toString(v_a), " !== ", toString(v_b))));
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/010.php line 8 */
void f_test(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(test);
  LINE(10,x_var_dump(1, same(v_a, v_b)));
  LINE(11,f_test2(v_a, v_b));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$010_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/010.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$010_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_boundary __attribute__((__unused__)) = (variables != gVariables) ? variables->get("boundary") : g->GV(boundary);
  Variant &v_limit __attribute__((__unused__)) = (variables != gVariables) ? variables->get("limit") : g->GV(limit);
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);

  (v_boundary = 65536LL);
  (v_limit = v_boundary + 42LL);
  (v_str = "<\?php\nfunction x(");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_limit); ++v_i) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_str, LINE(26,(assignCallTemp(eo_1, x_dechex(toInt64(v_i))),assignCallTemp(eo_2, (toString(same(v_i, (v_limit - 1LL)) ? (("")) : ((","))))),concat3("$v", eo_1, eo_2))));
      }
    }
  }
  concat_assign(v_str, concat(concat_rev(LINE(37,x_dechex(toInt64(v_boundary - 1LL))), concat(concat_rev(x_dechex(toInt64(v_boundary - 1LL)), concat(concat_rev(LINE(36,x_dechex(toInt64(v_boundary + 1LL))), concat(concat_rev(x_dechex(toInt64(v_boundary + 1LL)), concat(concat_rev(LINE(35,x_dechex(toInt64(v_boundary))), concat(concat_rev(x_dechex(toInt64(v_boundary)), concat(concat_rev(LINE(34,x_dechex(toInt64(v_limit - 1LL))), (assignCallTemp(eo_1, x_dechex(toInt64(v_limit - 1LL))),concat3(") {\n\ttest($v42, '42');\n\ttest('4000', $v4000);\n\ttest2($v300, '300');\n\ttest($v0, '0'); // first\n\ttest($v", eo_1, ", '"))), "'); // last\n\ttest($v")), ", '")), "'); //boundary\n\ttest($v")), ", '")), "'); //boundary+1\n\ttest($v")), ", '")), "'); //boundary-1\n}"));
  concat_assign(v_str, "\n\nx(");
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, v_limit); ++v_i) {
      LOOP_COUNTER_CHECK(2);
      {
        concat_assign(v_str, LINE(44,(assignCallTemp(eo_1, x_dechex(toInt64(v_i))),assignCallTemp(eo_3, (toString(same(v_i, (v_limit - 1LL)) ? (("")) : ((","))))),concat4("'", eo_1, "'", eo_3))));
      }
    }
  }
  concat_assign(v_str, ");\n");
  (v_filename = concat(LINE(49,x_dirname(get_source_filename("/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/010.php"))), "/010-file.php"));
  LINE(50,(assignCallTemp(eo_0, concat(x_dirname(get_source_filename("/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/010.php")), "/010-file.php")),assignCallTemp(eo_1, v_str),x_file_put_contents(eo_0, eo_1)));
  unset(v_str);
  LINE(53,include(toString((toString(v_filename))), false, variables, "/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/"));
  echo("Done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
