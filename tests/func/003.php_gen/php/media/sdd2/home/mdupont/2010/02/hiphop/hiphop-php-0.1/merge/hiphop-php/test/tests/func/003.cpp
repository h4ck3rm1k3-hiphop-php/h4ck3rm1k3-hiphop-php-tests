
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 87 */
void f_andi(int64 v_i, int64 v_j) {
  FUNCTION_INJECTION(andi);
  int64 v_k = 0;

  {
    LOOP_COUNTER(1);
    for ((v_k = v_i); not_more(v_k, v_j); v_k++) {
      LOOP_COUNTER_CHECK(1);
      {
        if (more(v_k, 5LL)) continue;
        echo(toString(v_k) + toString("\n"));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 33 */
Variant f_factorial(CVarRef v_n) {
  FUNCTION_INJECTION(factorial);
  if (equal(v_n, 0LL) || equal(v_n, 1LL)) {
    return 1LL;
  }
  else {
    return LINE(38,f_factorial(v_n - 1LL)) * v_n;
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 3 */
void f_a() {
  FUNCTION_INJECTION(a);
  echo("hey\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 8 */
void f_b(CStrRef v_i) {
  FUNCTION_INJECTION(b);
  echo(v_i + toString("\n"));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 14 */
void f_c(int64 v_i, int64 v_j) {
  FUNCTION_INJECTION(c);
  int64 v_k = 0;

  echo(LINE(16,concat5("Counting from ", toString(v_i), " to ", toString(v_j), "\n")));
  {
    LOOP_COUNTER(2);
    for ((v_k = v_i); not_more(v_k, v_j); v_k++) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(toString(v_k) + toString("\n"));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 43 */
Variant f_factorial2(int64 v_start, Numeric v_n) {
  FUNCTION_INJECTION(factorial2);
  if (not_more(v_n, v_start)) {
    return v_start;
  }
  else {
    return LINE(48,f_factorial2(v_start, v_n - 1LL)) * v_n;
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 73 */
int64 f_return4() {
  FUNCTION_INJECTION(return4);
  return 4LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 74 */
int64 f_return7() {
  FUNCTION_INJECTION(return7);
  return 7LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php line 63 */
void f_call_fact() {
  FUNCTION_INJECTION(call_fact);
  int64 v_i = 0;
  Variant v_n;

  echo("(it should break at 5...)\n");
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); not_more(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        if (equal(v_i, 5LL)) break;
        (v_n = LINE(68,f_factorial(v_i)));
        echo(LINE(69,concat5("factorial(", toString(v_i), ") = ", toString(v_n), "\n")));
      }
    }
  }
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);

  LINE(24,f_a());
  LINE(25,f_b("blah"));
  LINE(26,f_a());
  LINE(27,invoke_too_many_args("b", (1), ((f_b("blah")), null)));
  LINE(28,f_c(7LL, 14LL));
  LINE(30,f_a());
  {
    LOOP_COUNTER(4);
    for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
      LOOP_COUNTER_CHECK(4);
      {
        {
          LOOP_COUNTER(5);
          for ((v_i = 0LL); not_more(v_i, 10LL); v_i++) {
            LOOP_COUNTER_CHECK(5);
            {
              (v_n = LINE(55,f_factorial(v_i)));
              echo(LINE(56,concat5("factorial(", toString(v_i), ") = ", toString(v_n), "\n")));
            }
          }
        }
      }
    }
  }
  echo("and now, from a function...\n");
  {
    LOOP_COUNTER(6);
    for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
      LOOP_COUNTER_CHECK(6);
      {
        LINE(77,f_call_fact());
      }
    }
  }
  echo("------\n");
  (v_result = LINE(81,f_factorial(f_factorial(3LL))));
  echo(toString(v_result) + toString("\n"));
  (v_result = LINE(84,(assignCallTemp(eo_0, f_return4()),assignCallTemp(eo_1, f_return7()),f_factorial2(eo_0, eo_1))));
  echo(toString(v_result) + toString("\n"));
  LINE(95,f_andi(3LL, 10LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
