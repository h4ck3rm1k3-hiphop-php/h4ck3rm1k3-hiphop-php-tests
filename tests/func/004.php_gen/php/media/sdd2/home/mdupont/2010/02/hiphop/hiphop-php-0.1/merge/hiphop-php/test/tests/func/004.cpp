
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/004.php line 14 */
void f_some_other_function() {
  FUNCTION_INJECTION(some_other_function);
  echo("This is some other function, to ensure more than just one function works fine...\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/004.php line 5 */
void f_print_something_multiple_times(CStrRef v_something, int64 v_times) {
  FUNCTION_INJECTION(print_something_multiple_times);
  int64 v_i = 0;

  echo(LINE(7,concat5("----\nIn function, printing the string \"", v_something, "\" ", toString(v_times), " times\n")));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_times); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(9,concat4(toString(v_i), ") ", v_something, "\n")));
      }
    }
  }
  echo("Done with function...\n-----\n");
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/func/004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$func$004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Before function declaration...\n");
  echo("After function declaration...\n");
  echo("Calling function for the first time...\n");
  LINE(23,f_print_something_multiple_times("This works!", 10LL));
  echo("Returned from function call...\n");
  echo("Calling the function for the second time...\n");
  LINE(27,f_print_something_multiple_times("This like, really works and stuff...", 3LL));
  echo("Returned from function call...\n");
  LINE(30,f_some_other_function());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
