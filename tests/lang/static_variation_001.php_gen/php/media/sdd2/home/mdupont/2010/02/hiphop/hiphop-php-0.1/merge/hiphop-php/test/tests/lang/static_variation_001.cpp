
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_variation_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_variation_001.php line 26 */
void f_g2a() {
  FUNCTION_INJECTION(g2a);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_b;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_b;
    VariableTable(Variant &r_b) : v_b(r_b) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 1:
          HASH_RETURN(0x08FBB133F8576BD5LL, v_b,
                      b);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_b);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  f_eval(" static $b = array(4,5,6); ");
  LINE(28,x_var_dump(1, v_b));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_variation_001.php line 5 */
void f_f1() {
  FUNCTION_INJECTION(f1);
  DECLARE_GLOBAL_VARIABLES(g);
  Array &sv_a __attribute__((__unused__)) = g->sv_f1_DupIda;
  bool &inited_sv_a __attribute__((__unused__)) = g->inited_sv_f1_DupIda;
  if (!inited_sv_a) {
    (sv_a = ScalarArrays::sa_[1]);
    inited_sv_a = true;
  }
  LINE(13,x_var_dump(1, sv_a));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_variation_001.php line 23 */
void f_f2() {
  FUNCTION_INJECTION(f2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_b;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_b;
    VariableTable(Variant &r_b) : v_b(r_b) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 1:
          HASH_RETURN(0x08FBB133F8576BD5LL, v_b,
                      b);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_b);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  f_eval(" static $b = array(1,2,3); ");
  f_eval("function g2b() { static $b = array(7, 8, 9); var_dump($b); } ");
  LINE(32,x_var_dump(1, v_b));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_variation_001.php line 8 */
void f_g1() {
  FUNCTION_INJECTION(g1);
  DECLARE_GLOBAL_VARIABLES(g);
  Array &sv_a __attribute__((__unused__)) = g->sv_g1_DupIda;
  bool &inited_sv_a __attribute__((__unused__)) = g->inited_sv_g1_DupIda;
  if (!inited_sv_a) {
    (sv_a = ScalarArrays::sa_[2]);
    inited_sv_a = true;
  }
  LINE(10,x_var_dump(1, sv_a));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$static_variation_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_variation_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$static_variation_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = ScalarArrays::sa_[0]);
  LINE(17,f_f1());
  LINE(18,f_g1());
  LINE(19,x_var_dump(1, v_a));
  f_eval(" static $b = array(10,11,12); ");
  LINE(35,f_f2());
  LINE(36,f_g2a());
  LINE(37,invoke_failed("g2b", Array(), 0x00000000839F9540LL));
  LINE(38,x_var_dump(1, v_b));
  f_eval(" function f3() { static $c = array(1,2,3); var_dump($c); }");
  LINE(42,invoke_failed("f3", Array(), 0x00000000259E9021LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
