
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_arg_variation.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_arg_variation.php line 3 */
void f_foo(int num_args, int64 v_a, Array args /* = Array() */) {
  FUNCTION_INJECTION(foo);
  (v_a = 5LL);
  echo(toString(LINE(6,func_get_arg(num_args, Array(ArrayInit(1).set(0, v_a).create()),args))));
  echo(toString(LINE(7,func_get_arg(num_args, Array(ArrayInit(1).set(0, v_a).create()),args,toInt32(2LL), 2LL))));
  echo(toString(LINE(8,func_get_arg(num_args, Array(ArrayInit(1).set(0, v_a).create()),args,toInt32("hello")))));
  echo(toString(LINE(9,func_get_arg(num_args, Array(ArrayInit(1).set(0, v_a).create()),args,toInt32(-1LL)))));
  echo(toString(LINE(10,func_get_arg(num_args, Array(ArrayInit(1).set(0, v_a).create()),args,toInt32(2LL)))));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$func_get_arg_variation_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_arg_variation.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$func_get_arg_variation_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(12,f_foo(1, 2LL));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
