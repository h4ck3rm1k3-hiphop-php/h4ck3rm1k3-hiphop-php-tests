
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.009.php line 4 */
Variant f_returnvarbyref() {
  FUNCTION_INJECTION(returnVarByRef);
  Variant v_b;

  (v_b = 1LL);
  return ref(v_b);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.009.php line 17 */
Variant f_testreturnvalbyref() {
  FUNCTION_INJECTION(testReturnValByRef);
  return ref(LINE(18,f_returnval()));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.009.php line 9 */
Variant f_testreturnvarbyref() {
  FUNCTION_INJECTION(testReturnVarByRef);
  return ref(LINE(10,f_returnvarbyref()));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.009.php line 13 */
int64 f_returnval() {
  FUNCTION_INJECTION(returnVal);
  return 1LL;
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\n---> 1. Return a variable by reference -> No warning:\n");
  LINE(23,x_var_dump(1, f_testreturnvarbyref()));
  echo("\n---> 2. Return a value by reference -> Warning:\n");
  LINE(27,x_var_dump(1, f_testreturnvalbyref()));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
