
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_counter __attribute__((__unused__)) = (variables != gVariables) ? variables->get("counter") : g->GV(counter);

  echo("\nDirectly changing array values.\n");
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        v_a.set(v_k, (toString("changed.") + toString(v_k)));
        LINE(7,x_var_dump(1, v_v));
      }
    }
  }
  LINE(9,x_var_dump(1, v_a));
  echo("\nModifying the foreach $value.\n");
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
  LINE(16,x_var_dump(1, v_a));
  echo("\nModifying the foreach &$value.\n");
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(7);
    Variant map8 = ref(v_a);
    map8.escalate();
    for (MutableArrayIterPtr iter9 = map8.begin(&v_k, v_v); iter9->advance();) {
      LOOP_COUNTER_CHECK(7);
      {
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
  LINE(24,x_var_dump(1, v_a));
  echo("\nPushing elements onto an unreferenced array.\n");
  (v_a = ScalarArrays::sa_[0]);
  (v_counter = 0LL);
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_a.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_v = iter12->second();
      {
        LINE(30,x_array_push(2, ref(v_a), toString("new.") + toString(v_counter)));
        if (more(v_counter++, 10LL)) {
          echo("Loop detected\n");
          break;
        }
      }
    }
  }
  LINE(38,x_var_dump(1, v_a));
  echo("\nPushing elements onto an unreferenced array, using &$value.\n");
  (v_a = ScalarArrays::sa_[0]);
  (v_counter = 0LL);
  {
    LOOP_COUNTER(13);
    Variant map14 = ref(v_a);
    map14.escalate();
    for (MutableArrayIterPtr iter15 = map14.begin(NULL, v_v); iter15->advance();) {
      LOOP_COUNTER_CHECK(13);
      {
        LINE(44,x_array_push(2, ref(v_a), toString("new.") + toString(v_counter)));
        if (more(v_counter++, 10LL)) {
          echo("Loop detected\n");
          break;
        }
      }
    }
  }
  LINE(52,x_var_dump(1, v_a));
  echo("\nPopping elements off an unreferenced array.\n");
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(16);
    for (ArrayIterPtr iter18 = v_a.begin(); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_v = iter18->second();
      {
        LINE(57,x_array_pop(ref(v_a)));
        LINE(58,x_var_dump(1, v_v));
      }
    }
  }
  LINE(60,x_var_dump(1, v_a));
  echo("\nPopping elements off an unreferenced array, using &$value.\n");
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(19);
    Variant map20 = ref(v_a);
    map20.escalate();
    for (MutableArrayIterPtr iter21 = map20.begin(NULL, v_v); iter21->advance();) {
      LOOP_COUNTER_CHECK(19);
      {
        LINE(65,x_array_pop(ref(v_a)));
        LINE(66,x_var_dump(1, v_v));
      }
    }
  }
  LINE(68,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
