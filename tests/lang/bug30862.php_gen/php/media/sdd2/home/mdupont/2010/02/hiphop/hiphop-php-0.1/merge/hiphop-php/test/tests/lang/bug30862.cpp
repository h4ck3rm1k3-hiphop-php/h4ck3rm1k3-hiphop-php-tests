
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30862.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_X = 0LL;
const int64 k_Y = 1LL;

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30862.php line 11 */
Variant c_t2::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x4292CEE227B9150ALL, g->s_t2_DupIda,
                  a);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_t2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_t2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_t2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_t2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_t2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_t2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_t2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(t2)
ObjectData *c_t2::cloneImpl() {
  c_t2 *obj = NEW(c_t2)();
  cloneSet(obj);
  return obj;
}
void c_t2::cloneSet(c_t2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_t2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_t2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_t2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_t2$os_get(const char *s) {
  return c_t2::os_get(s, -1);
}
Variant &cw_t2$os_lval(const char *s) {
  return c_t2::os_lval(s, -1);
}
Variant cw_t2$os_constant(const char *s) {
  return c_t2::os_constant(s);
}
Variant cw_t2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_t2::os_invoke(c, s, params, -1, fatal);
}
void c_t2::init() {
}
void c_t2::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_t2_DupIda = ScalarArrays::sa_[1];
}
void csi_t2() {
  c_t2::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30862.php line 2 */
Variant c_t::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x4292CEE227B9150ALL, g->s_t_DupIda,
                  a);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_t::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_t::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_t::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_t::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_t::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_t::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_t::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(t)
ObjectData *c_t::cloneImpl() {
  c_t *obj = NEW(c_t)();
  cloneSet(obj);
  return obj;
}
void c_t::cloneSet(c_t *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_t::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_t::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_t::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_t$os_get(const char *s) {
  return c_t::os_get(s, -1);
}
Variant &cw_t$os_lval(const char *s) {
  return c_t::os_lval(s, -1);
}
Variant cw_t$os_constant(const char *s) {
  return c_t::os_constant(s);
}
Variant cw_t$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_t::os_invoke(c, s, params, -1, fatal);
}
void c_t::init() {
}
void c_t::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_t_DupIda = ScalarArrays::sa_[0];
}
void csi_t() {
  c_t::os_static_initializer();
}
Object co_t2(CArrRef params, bool init /* = true */) {
  return Object(p_t2(NEW(c_t2)())->dynCreate(params, init));
}
Object co_t(CArrRef params, bool init /* = true */) {
  return Object(p_t(NEW(c_t)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug30862_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30862.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug30862_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(5,x_print_r(g->s_t_DupIda));
  echo("----------\n");
  ;
  ;
  LINE(14,x_print_r(g->s_t2_DupIda));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
