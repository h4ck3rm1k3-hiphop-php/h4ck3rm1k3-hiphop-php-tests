
#ifndef __GENERATED_cls_factory_h__
#define __GENERATED_cls_factory_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21669.php line 8 */
class c_factory : virtual public ObjectData {
  BEGIN_CLASS_MAP(factory)
  END_CLASS_MAP(factory)
  DECLARE_CLASS(factory, Factory, ObjectData)
  void init();
  public: String m_name;
  public: Variant t_create();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_factory_h__
