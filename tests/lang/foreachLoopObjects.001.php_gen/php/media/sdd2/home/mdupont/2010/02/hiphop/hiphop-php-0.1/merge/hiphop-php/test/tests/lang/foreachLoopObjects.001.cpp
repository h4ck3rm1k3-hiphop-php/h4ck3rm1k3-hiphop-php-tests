
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.001.php line 3 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  props.push_back(NEW(ArrayElement)("b", m_b));
  props.push_back(NEW(ArrayElement)("c", m_c));
  props.push_back(NEW(ArrayElement)("d", m_d));
  props.push_back(NEW(ArrayElement)("e", m_e));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    case 10:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    case 11:
      HASH_EXISTS_STRING(0x61B161496B7EA7EBLL, e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 10:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    case 4:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    case 5:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    case 10:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    case 11:
      HASH_SET_STRING(0x61B161496B7EA7EBLL, m_e,
                      e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_a = m_a;
  clone->m_b = m_b;
  clone->m_c = m_c;
  clone->m_d = m_d;
  clone->m_e = m_e;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_a = "Original a";
  m_b = "Original b";
  m_c = "Original c";
  m_d = "Original d";
  m_e = "Original e";
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopObjects_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopObjects_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  echo("\n\nSimple loop.\n");
  (v_obj = ((Object)(LINE(13,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_obj.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        LINE(15,x_var_dump(1, v_v));
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_obj.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        LINE(18,x_var_dump(2, v_k, Array(ArrayInit(1).set(0, v_v).create())));
      }
    }
  }
  echo("\nCheck key and value after the loop.\n");
  LINE(21,x_var_dump(2, v_k, Array(ArrayInit(1).set(0, v_v).create())));
  echo("\n\nObject instantiated inside loop.\n");
  {
    LOOP_COUNTER(7);
    Variant map8 = ((Object)(LINE(25,p_c(p_c(NEWOBJ(c_c)())->create()))));
    for (ArrayIterPtr iter9 = map8.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
      {
        LINE(26,x_var_dump(1, v_v));
      }
    }
  }
  {
    LOOP_COUNTER(10);
    Variant map11 = ((Object)(LINE(28,p_c(p_c(NEWOBJ(c_c)())->create()))));
    for (ArrayIterPtr iter12 = map11.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_v = iter12->second();
      v_k = iter12->first();
      {
        LINE(29,x_var_dump(2, v_k, Array(ArrayInit(1).set(0, v_v).create())));
      }
    }
  }
  echo("\nCheck key and value after the loop.\n");
  LINE(32,x_var_dump(2, v_k, Array(ArrayInit(1).set(0, v_v).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
