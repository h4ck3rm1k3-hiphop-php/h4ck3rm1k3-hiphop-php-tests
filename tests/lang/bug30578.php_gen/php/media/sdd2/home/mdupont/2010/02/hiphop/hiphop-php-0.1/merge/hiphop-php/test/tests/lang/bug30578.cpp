
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30578.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30578.php line 5 */
Variant c_example::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_example::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_example::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_example::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_example::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_example::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_example::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_example::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(example)
ObjectData *c_example::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_example::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_example::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_example::cloneImpl() {
  c_example *obj = NEW(c_example)();
  cloneSet(obj);
  return obj;
}
void c_example::cloneSet(c_example *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_example::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_example::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_example::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_example$os_get(const char *s) {
  return c_example::os_get(s, -1);
}
Variant &cw_example$os_lval(const char *s) {
  return c_example::os_lval(s, -1);
}
Variant cw_example$os_constant(const char *s) {
  return c_example::os_constant(s);
}
Variant cw_example$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_example::os_invoke(c, s, params, -1, fatal);
}
void c_example::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30578.php line 7 */
void c_example::t___construct() {
  INSTANCE_METHOD_INJECTION(Example, Example::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(9,x_ob_start());
  echo("This should be displayed last.\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30578.php line 13 */
Variant c_example::t___destruct() {
  INSTANCE_METHOD_INJECTION(Example, Example::__destruct);
  setInDtor();
  String v_buffered_data;

  (v_buffered_data = LINE(15,x_ob_get_contents()));
  LINE(16,x_ob_end_clean());
  echo("This should be displayed first.\n");
  echo(toString("Buffered data: ") + v_buffered_data);
  return null;
} /* function */
Object co_example(CArrRef params, bool init /* = true */) {
  return Object(p_example(NEW(c_example)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug30578_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30578.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug30578_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  LINE(3,x_error_reporting(toInt32(6143LL) /* E_ALL */));
  (v_obj = ((Object)(LINE(23,p_example(p_example(NEWOBJ(c_example)())->create())))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
