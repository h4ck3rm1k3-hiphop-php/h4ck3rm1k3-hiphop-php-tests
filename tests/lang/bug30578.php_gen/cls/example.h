
#ifndef __GENERATED_cls_example_h__
#define __GENERATED_cls_example_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30578.php line 5 */
class c_example : virtual public ObjectData {
  BEGIN_CLASS_MAP(example)
  END_CLASS_MAP(example)
  DECLARE_CLASS(example, Example, ObjectData)
  void init();
  public: virtual void destruct();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_example_h__
