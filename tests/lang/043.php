<?php
class A {
    static function foo() { return 'foo'; }
}

$classname       =  'A';
$binaryClassname = b'A';
$wrongClassname  =  'B';

echo $classname::foo()."\n";
echo $binaryClassname::foo()."\n";
echo $wrongClassname::foo()."\n";
?> 
===DONE===
