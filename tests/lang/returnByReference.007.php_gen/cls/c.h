
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.php line 2 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: static int64 ti_returnconstantbyvalue(const char* cls);
  public: static Variant ti_returnconstantbyref(const char* cls);
  public: static Variant ti_returnvariablebyref(const char* cls);
  public: static Variant ti_returnfunctioncallbyref(const char* cls, CStrRef v_functionToCall);
  public: static Variant t_returnconstantbyref() { return ti_returnconstantbyref("c"); }
  public: static Variant t_returnvariablebyref() { return ti_returnvariablebyref("c"); }
  public: static int64 t_returnconstantbyvalue() { return ti_returnconstantbyvalue("c"); }
  public: static Variant t_returnfunctioncallbyref(CStrRef v_functionToCall) { return ti_returnfunctioncallbyref("c", v_functionToCall); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
