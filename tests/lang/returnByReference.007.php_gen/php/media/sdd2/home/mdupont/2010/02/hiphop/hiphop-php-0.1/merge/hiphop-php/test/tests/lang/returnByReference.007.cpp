
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x4383C2CCCF1034CDLL, returnconstantbyref) {
        return ref(ti_returnconstantbyref(o_getClassName()));
      }
      HASH_GUARD(0x20C0DB8C2C250C4DLL, returnvariablebyref) {
        return ref(ti_returnvariablebyref(o_getClassName()));
      }
      HASH_GUARD(0x401553DF8461B185LL, returnconstantbyvalue) {
        return (ti_returnconstantbyvalue(o_getClassName()));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x4383C2CCCF1034CDLL, returnconstantbyref) {
        return ref(ti_returnconstantbyref(o_getClassName()));
      }
      HASH_GUARD(0x20C0DB8C2C250C4DLL, returnvariablebyref) {
        return ref(ti_returnvariablebyref(o_getClassName()));
      }
      HASH_GUARD(0x401553DF8461B185LL, returnconstantbyvalue) {
        return (ti_returnconstantbyvalue(o_getClassName()));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x4383C2CCCF1034CDLL, returnconstantbyref) {
        return ref(ti_returnconstantbyref(c));
      }
      HASH_GUARD(0x20C0DB8C2C250C4DLL, returnvariablebyref) {
        return ref(ti_returnvariablebyref(c));
      }
      HASH_GUARD(0x401553DF8461B185LL, returnconstantbyvalue) {
        return (ti_returnconstantbyvalue(c));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.php line 3 */
int64 c_c::ti_returnconstantbyvalue(const char* cls) {
  STATIC_METHOD_INJECTION(C, C::returnConstantByValue);
  return 100LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.php line 7 */
Variant c_c::ti_returnconstantbyref(const char* cls) {
  STATIC_METHOD_INJECTION(C, C::returnConstantByRef);
  return 100LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.php line 11 */
Variant c_c::ti_returnvariablebyref(const char* cls) {
  STATIC_METHOD_INJECTION(C, C::returnVariableByRef);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(a)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.php line 15 */
Variant c_c::ti_returnfunctioncallbyref(const char* cls, CStrRef v_functionToCall) {
  STATIC_METHOD_INJECTION(C, C::returnFunctionCallByRef);
  return ref(LINE(16,c_c::os_invoke("c", v_functionToCall, Array(), -1)));
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_007_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.007.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_007_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("\n---> 1. Via a return by ref function call, assign by reference the return value of a function that returns by value:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(23,c_c::t_returnfunctioncallbyref("returnConstantByValue"))));
  v_a++;
  LINE(25,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 2. Via a return by ref function call, assign by reference the return value of a function that returns a constant by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(30,c_c::t_returnfunctioncallbyref("returnConstantByRef"))));
  v_a++;
  LINE(32,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 3. Via a return by ref function call, assign by reference the return value of a function that returns by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(37,c_c::t_returnfunctioncallbyref("returnVariableByRef"))));
  v_a++;
  LINE(39,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
