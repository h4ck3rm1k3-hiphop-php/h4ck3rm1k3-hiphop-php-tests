
#ifndef __GENERATED_cls_myexception_h__
#define __GENERATED_cls_myexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/038.php line 3 */
class c_myexception : virtual public c_exception {
  BEGIN_CLASS_MAP(myexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(myexception)
  DECLARE_CLASS(myexception, MyException, exception)
  void init();
  public: void t___construct(Variant v_errstr, Variant v_errno = 0LL, Variant v_errfile = "", Variant v_errline = "");
  public: ObjectData *create(Variant v_errstr, Variant v_errno = 0LL, Variant v_errfile = "", Variant v_errline = "");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myexception_h__
