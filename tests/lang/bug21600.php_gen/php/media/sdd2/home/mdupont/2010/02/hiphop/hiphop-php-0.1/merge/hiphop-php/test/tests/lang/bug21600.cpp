
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21600.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21600.php line 14 */
Variant f_bar(CVarRef v_text) {
  FUNCTION_INJECTION(bar);
  return v_text;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21600.php line 18 */
Variant f_fubar(Variant v_text) {
  FUNCTION_INJECTION(fubar);
  (v_text = ref(v_text));
  return v_text;
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug21600_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21600.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug21600_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tmp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tmp") : g->GV(tmp);

  (v_tmp = ScalarArrays::sa_[0]);
  v_tmp.set("foo", ("test"), 0x4154FA2EF733DA8FLL);
  v_tmp.set("foo", (ref(LINE(4,f_bar(v_tmp.rvalAt("foo", 0x4154FA2EF733DA8FLL))))), 0x4154FA2EF733DA8FLL);
  LINE(5,x_var_dump(1, v_tmp));
  unset(v_tmp);
  (v_tmp = ScalarArrays::sa_[0]);
  v_tmp.set("foo", ("test"), 0x4154FA2EF733DA8FLL);
  v_tmp.set("foo", (ref(LINE(11,f_fubar(v_tmp.rvalAt("foo", 0x4154FA2EF733DA8FLL))))), 0x4154FA2EF733DA8FLL);
  LINE(12,x_var_dump(1, v_tmp));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
