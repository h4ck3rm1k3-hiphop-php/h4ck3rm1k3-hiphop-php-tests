
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25922.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25922.php line 2 */
void f_my_error_handler(CVarRef v_error, CVarRef v_errmsg //  = ""
, CVarRef v_errfile //  = ""
, CVarRef v_errline //  = 0LL
, Variant v_errcontext //  = ""
) {
  FUNCTION_INJECTION(my_error_handler);
  echo(toString(v_errmsg) + toString("\n"));
  (v_errcontext = "");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25922.php line 10 */
void f_test() {
  FUNCTION_INJECTION(test);
  Variant v_data;

  echo(LINE(12,concat3("Undefined index here: '", toString(v_data.rvalAt("HTTP_HEADER", 0x32C017B094A6A2E6LL)), "'\n")));
} /* function */
Variant i_my_error_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x606C54C87D73CF7DLL, my_error_handler) {
    int count = params.size();
    if (count <= 1) return (f_my_error_handler(params.rvalAt(0)), null);
    if (count == 2) return (f_my_error_handler(params.rvalAt(0), params.rvalAt(1)), null);
    if (count == 3) return (f_my_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
    if (count == 4) return (f_my_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
    return (f_my_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug25922_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25922.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug25922_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(8,x_set_error_handler("my_error_handler"));
  LINE(14,f_test());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
