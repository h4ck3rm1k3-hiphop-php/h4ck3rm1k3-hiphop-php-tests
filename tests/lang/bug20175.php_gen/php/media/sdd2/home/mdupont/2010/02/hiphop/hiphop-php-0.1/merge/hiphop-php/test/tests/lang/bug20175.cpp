
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 90 */
Variant c_oop_class::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_oop_class::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_oop_class::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("oop_name", m_oop_name));
  c_ObjectData::o_get(props);
}
bool c_oop_class::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x203485B5B3CE5DEELL, oop_name, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_oop_class::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x203485B5B3CE5DEELL, m_oop_name,
                         oop_name, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_oop_class::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x203485B5B3CE5DEELL, m_oop_name,
                      oop_name, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_oop_class::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_oop_class::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(oop_class)
ObjectData *c_oop_class::create() {
  init();
  t_oop_class();
  return this;
}
ObjectData *c_oop_class::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_oop_class::cloneImpl() {
  c_oop_class *obj = NEW(c_oop_class)();
  cloneSet(obj);
  return obj;
}
void c_oop_class::cloneSet(c_oop_class *clone) {
  clone->m_oop_name = m_oop_name;
  ObjectData::cloneSet(clone);
}
Variant c_oop_class::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_oop_class::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_oop_class::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_oop_class$os_get(const char *s) {
  return c_oop_class::os_get(s, -1);
}
Variant &cw_oop_class$os_lval(const char *s) {
  return c_oop_class::os_lval(s, -1);
}
Variant cw_oop_class$os_constant(const char *s) {
  return c_oop_class::os_constant(s);
}
Variant cw_oop_class$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_oop_class::os_invoke(c, s, params, -1, fatal);
}
void c_oop_class::init() {
  m_oop_name = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 93 */
void c_oop_class::t_oop_class() {
  INSTANCE_METHOD_INJECTION(oop_class, oop_class::oop_class);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_oop_global __attribute__((__unused__)) = g->GV(oop_global);
  echo("oop_class()\n");
  (m_oop_name = concat("oop:", toString(++gv_oop_global)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 100 */
Variant c_oop_test::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x45B8E596FBDFBB55LL, g->s_oop_test_DupIdoop_value,
                  oop_value);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_oop_test::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x45B8E596FBDFBB55LL, g->s_oop_test_DupIdoop_value,
                  oop_value);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_oop_test::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_oop_test::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_oop_test::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_oop_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_oop_test::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_oop_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(oop_test)
ObjectData *c_oop_test::create() {
  init();
  t_oop_test();
  return this;
}
ObjectData *c_oop_test::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_oop_test::cloneImpl() {
  c_oop_test *obj = NEW(c_oop_test)();
  cloneSet(obj);
  return obj;
}
void c_oop_test::cloneSet(c_oop_test *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_oop_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3D24EF7B03F174A1LL, oop_static) {
        return (t_oop_static(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_oop_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3D24EF7B03F174A1LL, oop_static) {
        return (t_oop_static(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_oop_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_oop_test$os_get(const char *s) {
  return c_oop_test::os_get(s, -1);
}
Variant &cw_oop_test$os_lval(const char *s) {
  return c_oop_test::os_lval(s, -1);
}
Variant cw_oop_test$os_constant(const char *s) {
  return c_oop_test::os_constant(s);
}
Variant cw_oop_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_oop_test::os_invoke(c, s, params, -1, fatal);
}
void c_oop_test::init() {
}
void c_oop_test::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_oop_test_DupIdoop_value = null;
}
void csi_oop_test() {
  c_oop_test::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 103 */
void c_oop_test::t_oop_test() {
  INSTANCE_METHOD_INJECTION(oop_test, oop_test::oop_test);
  bool oldInCtor = gasInCtor(true);
  echo("oop_test()\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 107 */
void c_oop_test::t_oop_static() {
  INSTANCE_METHOD_INJECTION(oop_test, oop_test::oop_static);
  DECLARE_GLOBAL_VARIABLES(g);
  echo("oop_static()\n");
  if (!(isset(g->s_oop_test_DupIdoop_value))) {
    (g->s_oop_test_DupIdoop_value = LINE(110,p_oop_class(p_oop_class(NEWOBJ(c_oop_class)())->create())));
  }
  echo(toString(g->s_oop_test_DupIdoop_value.o_get("oop_name", 0x203485B5B3CE5DEELL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 37 */
String f_bar_global() {
  FUNCTION_INJECTION(bar_global);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_bar_count __attribute__((__unused__)) = g->GV(bar_count);
  echo("bar_global()\n");
  return concat("bar:", toString(++gv_bar_count));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 19 */
Variant f_foo_static() {
  FUNCTION_INJECTION(foo_static);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_foo_value __attribute__((__unused__)) = g->sv_foo_static_DupIdfoo_value;
  bool &inited_sv_foo_value __attribute__((__unused__)) = g->inited_sv_foo_static_DupIdfoo_value;
  if (!inited_sv_foo_value) {
    (sv_foo_value = null);
    inited_sv_foo_value = true;
  }
  echo("foo_static()\n");
  if (!(isset(sv_foo_value))) {
    (sv_foo_value = LINE(23,f_foo_global()));
  }
  return sv_foo_value;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 13 */
String f_foo_global() {
  FUNCTION_INJECTION(foo_global);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_foo_count __attribute__((__unused__)) = g->GV(foo_count);
  echo("foo_global()\n");
  return concat("foo:", toString(++gv_foo_count));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 43 */
Variant f_bar_static() {
  FUNCTION_INJECTION(bar_static);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_bar_value __attribute__((__unused__)) = g->sv_bar_static_DupIdbar_value;
  bool &inited_sv_bar_value __attribute__((__unused__)) = g->inited_sv_bar_static_DupIdbar_value;
  if (!inited_sv_bar_value) {
    (sv_bar_value = null);
    inited_sv_bar_value = true;
  }
  echo("bar_static()\n");
  if (!(isset(sv_bar_value))) {
    (sv_bar_value = ref(LINE(47,f_bar_global())));
  }
  return sv_bar_value;
} /* function */
Object co_oop_class(CArrRef params, bool init /* = true */) {
  return Object(p_oop_class(NEW(c_oop_class)())->dynCreate(params, init));
}
Object co_oop_test(CArrRef params, bool init /* = true */) {
  return Object(p_oop_test(NEW(c_oop_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug20175_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug20175_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo_count") : g->GV(foo_count);
  Variant &v_bar_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar_count") : g->GV(bar_count);
  Variant &v_oop_global __attribute__((__unused__)) = (variables != gVariables) ? variables->get("oop_global") : g->GV(oop_global);
  Variant &v_oop_tester __attribute__((__unused__)) = (variables != gVariables) ? variables->get("oop_tester") : g->GV(oop_tester);

  print(concat(LINE(2,x_zend_version()), "\n"));
  (v_foo_count = 0LL);
  (v_bar_count = 0LL);
  (v_oop_global = 0LL);
  print(concat(toString(LINE(116,f_foo_static())), "\n"));
  print(concat(toString(LINE(117,f_foo_static())), "\n"));
  print(concat(toString(LINE(118,f_bar_static())), "\n"));
  print(concat(toString(LINE(119,f_bar_static())), "\n"));
  echo("wow_static()\nwow_global()\nwow:1\nwow_static()\nwow:1\n");
  (v_oop_tester = ((Object)(LINE(128,p_oop_test(p_oop_test(NEWOBJ(c_oop_test)())->create())))));
  print(concat(toString(LINE(129,v_oop_tester.o_invoke_few_args("oop_static", 0x3D24EF7B03F174A1LL, 0))), "\n"));
  print(concat(toString(LINE(130,v_oop_tester.o_invoke_few_args("oop_static", 0x3D24EF7B03F174A1LL, 0))), "\n"));
  (v_oop_tester = ((Object)(LINE(131,p_oop_test(p_oop_test(NEWOBJ(c_oop_test)())->create())))));
  print(concat(toString(LINE(132,v_oop_tester.o_invoke_few_args("oop_static", 0x3D24EF7B03F174A1LL, 0))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
