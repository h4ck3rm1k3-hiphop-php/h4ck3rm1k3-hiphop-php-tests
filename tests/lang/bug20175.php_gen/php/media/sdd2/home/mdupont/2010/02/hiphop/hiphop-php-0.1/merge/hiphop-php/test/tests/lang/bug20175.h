
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_lang_bug20175_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_lang_bug20175_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.fw.h>

// Declarations
#include <cls/oop_class.h>
#include <cls/oop_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_bar_global();
Variant f_foo_static();
String f_foo_global();
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug20175_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_bar_static();
Object co_oop_class(CArrRef params, bool init = true);
Object co_oop_test(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_lang_bug20175_h__
