
#ifndef __GENERATED_cls_oop_class_h__
#define __GENERATED_cls_oop_class_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 90 */
class c_oop_class : virtual public ObjectData {
  BEGIN_CLASS_MAP(oop_class)
  END_CLASS_MAP(oop_class)
  DECLARE_CLASS(oop_class, oop_class, ObjectData)
  void init();
  public: String m_oop_name;
  public: void t_oop_class();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_oop_class_h__
