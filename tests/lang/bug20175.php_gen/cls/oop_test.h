
#ifndef __GENERATED_cls_oop_test_h__
#define __GENERATED_cls_oop_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug20175.php line 100 */
class c_oop_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(oop_test)
  END_CLASS_MAP(oop_test)
  DECLARE_CLASS(oop_test, oop_test, ObjectData)
  void init();
  public: void t_oop_test();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_oop_static();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_oop_test_h__
