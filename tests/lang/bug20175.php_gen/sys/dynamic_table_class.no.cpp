
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_oop_class(CArrRef params, bool init = true);
Variant cw_oop_class$os_get(const char *s);
Variant &cw_oop_class$os_lval(const char *s);
Variant cw_oop_class$os_constant(const char *s);
Variant cw_oop_class$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_oop_test(CArrRef params, bool init = true);
Variant cw_oop_test$os_get(const char *s);
Variant &cw_oop_test$os_lval(const char *s);
Variant cw_oop_test$os_constant(const char *s);
Variant cw_oop_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_CREATE_OBJECT(0x10F8F0864F4F31DELL, oop_class);
      HASH_CREATE_OBJECT(0x054694D4BD647392LL, oop_test);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x10F8F0864F4F31DELL, oop_class);
      HASH_INVOKE_STATIC_METHOD(0x054694D4BD647392LL, oop_test);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x10F8F0864F4F31DELL, oop_class);
      HASH_GET_STATIC_PROPERTY(0x054694D4BD647392LL, oop_test);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x10F8F0864F4F31DELL, oop_class);
      HASH_GET_STATIC_PROPERTY_LV(0x054694D4BD647392LL, oop_test);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x10F8F0864F4F31DELL, oop_class);
      HASH_GET_CLASS_CONSTANT(0x054694D4BD647392LL, oop_test);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
