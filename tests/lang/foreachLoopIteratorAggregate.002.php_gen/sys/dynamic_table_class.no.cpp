
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_bad1(CArrRef params, bool init = true);
Variant cw_bad1$os_get(const char *s);
Variant &cw_bad1$os_lval(const char *s);
Variant cw_bad1$os_constant(const char *s);
Variant cw_bad1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bad2(CArrRef params, bool init = true);
Variant cw_bad2$os_get(const char *s);
Variant &cw_bad2$os_lval(const char *s);
Variant cw_bad2$os_constant(const char *s);
Variant cw_bad2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bad3(CArrRef params, bool init = true);
Variant cw_bad3$os_get(const char *s);
Variant &cw_bad3$os_lval(const char *s);
Variant cw_bad3$os_constant(const char *s);
Variant cw_bad3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bad4(CArrRef params, bool init = true);
Variant cw_bad4$os_get(const char *s);
Variant &cw_bad4$os_lval(const char *s);
Variant cw_bad4$os_constant(const char *s);
Variant cw_bad4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_CREATE_OBJECT(0x79A97F0C48DAF5C0LL, bad4);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x131A26FD4535370BLL, bad3);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x435DA618D5D7B7B4LL, bad1);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x4900ECD1B07589F6LL, bad2);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x79A97F0C48DAF5C0LL, bad4);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x131A26FD4535370BLL, bad3);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x435DA618D5D7B7B4LL, bad1);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x4900ECD1B07589F6LL, bad2);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x79A97F0C48DAF5C0LL, bad4);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x131A26FD4535370BLL, bad3);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x435DA618D5D7B7B4LL, bad1);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x4900ECD1B07589F6LL, bad2);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x79A97F0C48DAF5C0LL, bad4);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x131A26FD4535370BLL, bad3);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x435DA618D5D7B7B4LL, bad1);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x4900ECD1B07589F6LL, bad2);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x79A97F0C48DAF5C0LL, bad4);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x131A26FD4535370BLL, bad3);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x435DA618D5D7B7B4LL, bad1);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x4900ECD1B07589F6LL, bad2);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
