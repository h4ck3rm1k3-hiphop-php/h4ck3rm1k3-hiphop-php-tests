
#ifndef __GENERATED_cls_bad1_h__
#define __GENERATED_cls_bad1_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 3 */
class c_bad1 : virtual public c_iteratoraggregate {
  BEGIN_CLASS_MAP(bad1)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iteratoraggregate)
  END_CLASS_MAP(bad1)
  DECLARE_CLASS(bad1, bad1, ObjectData)
  void init();
  public: Variant t_getiterator();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bad1_h__
