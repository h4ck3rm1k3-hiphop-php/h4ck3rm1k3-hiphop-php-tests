
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 3 */
Variant c_bad1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bad1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bad1::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bad1::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bad1::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bad1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bad1::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bad1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bad1)
ObjectData *c_bad1::cloneImpl() {
  c_bad1 *obj = NEW(c_bad1)();
  cloneSet(obj);
  return obj;
}
void c_bad1::cloneSet(c_bad1 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bad1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bad1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bad1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bad1$os_get(const char *s) {
  return c_bad1::os_get(s, -1);
}
Variant &cw_bad1$os_lval(const char *s) {
  return c_bad1::os_lval(s, -1);
}
Variant cw_bad1$os_constant(const char *s) {
  return c_bad1::os_constant(s);
}
Variant cw_bad1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bad1::os_invoke(c, s, params, -1, fatal);
}
void c_bad1::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 4 */
Variant c_bad1::t_getiterator() {
  INSTANCE_METHOD_INJECTION(bad1, bad1::getIterator);
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 9 */
Variant c_bad2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bad2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bad2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bad2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bad2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bad2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bad2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bad2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bad2)
ObjectData *c_bad2::cloneImpl() {
  c_bad2 *obj = NEW(c_bad2)();
  cloneSet(obj);
  return obj;
}
void c_bad2::cloneSet(c_bad2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bad2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bad2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bad2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bad2$os_get(const char *s) {
  return c_bad2::os_get(s, -1);
}
Variant &cw_bad2$os_lval(const char *s) {
  return c_bad2::os_lval(s, -1);
}
Variant cw_bad2$os_constant(const char *s) {
  return c_bad2::os_constant(s);
}
Variant cw_bad2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bad2::os_invoke(c, s, params, -1, fatal);
}
void c_bad2::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 10 */
p_stdclass c_bad2::t_getiterator() {
  INSTANCE_METHOD_INJECTION(bad2, bad2::getIterator);
  return ((Object)(LINE(11,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create()))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 15 */
Variant c_bad3::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bad3::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bad3::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bad3::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bad3::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bad3::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bad3::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bad3::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bad3)
ObjectData *c_bad3::cloneImpl() {
  c_bad3 *obj = NEW(c_bad3)();
  cloneSet(obj);
  return obj;
}
void c_bad3::cloneSet(c_bad3 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bad3::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bad3::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bad3::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bad3$os_get(const char *s) {
  return c_bad3::os_get(s, -1);
}
Variant &cw_bad3$os_lval(const char *s) {
  return c_bad3::os_lval(s, -1);
}
Variant cw_bad3$os_constant(const char *s) {
  return c_bad3::os_constant(s);
}
Variant cw_bad3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bad3::os_invoke(c, s, params, -1, fatal);
}
void c_bad3::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 16 */
int64 c_bad3::t_getiterator() {
  INSTANCE_METHOD_INJECTION(bad3, bad3::getIterator);
  return 1LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 21 */
Variant c_bad4::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bad4::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bad4::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bad4::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bad4::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bad4::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bad4::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bad4::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bad4)
ObjectData *c_bad4::cloneImpl() {
  c_bad4 *obj = NEW(c_bad4)();
  cloneSet(obj);
  return obj;
}
void c_bad4::cloneSet(c_bad4 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bad4::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bad4::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bad4::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bad4$os_get(const char *s) {
  return c_bad4::os_get(s, -1);
}
Variant &cw_bad4$os_lval(const char *s) {
  return c_bad4::os_lval(s, -1);
}
Variant cw_bad4$os_constant(const char *s) {
  return c_bad4::os_constant(s);
}
Variant cw_bad4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bad4::os_invoke(c, s, params, -1, fatal);
}
void c_bad4::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 22 */
Array c_bad4::t_getiterator() {
  INSTANCE_METHOD_INJECTION(bad4, bad4::getIterator);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php line 28 */
void f_f(CStrRef v_className) {
  FUNCTION_INJECTION(f);
  Primitive v_k = 0;
  Variant v_v;
  p_exception v_e;

  try {
    {
      LOOP_COUNTER(1);
      Variant map2 = LINE(30,create_object(v_className, Array()));
      for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
        LOOP_COUNTER_CHECK(1);
        v_v = iter3->second();
        v_k = iter3->first();
        {
          echo(LINE(31,concat4(toString(v_k), " => ", toString(v_v), "\n")));
        }
      }
    }
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(concat(concat_rev(toString(LINE(34,v_e->t_getmessage())), concat(toString(v_e->t_getline()), ": ")), "\n"));
    } else {
      throw;
    }
  }
} /* function */
Object co_bad1(CArrRef params, bool init /* = true */) {
  return Object(p_bad1(NEW(c_bad1)())->dynCreate(params, init));
}
Object co_bad2(CArrRef params, bool init /* = true */) {
  return Object(p_bad2(NEW(c_bad2)())->dynCreate(params, init));
}
Object co_bad3(CArrRef params, bool init /* = true */) {
  return Object(p_bad3(NEW(c_bad3)())->dynCreate(params, init));
}
Object co_bad4(CArrRef params, bool init /* = true */) {
  return Object(p_bad4(NEW(c_bad4)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIteratorAggregate_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIteratorAggregate_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(38,f_f("bad1"));
  LINE(39,f_f("bad2"));
  LINE(40,f_f("bad3"));
  LINE(41,f_f("bad4"));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
