
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_009.php line 2 */
Variant f_f(CVarRef v_x) {
  FUNCTION_INJECTION(f);
  echo(LINE(3,concat3("f(", toString(v_x), ") ")));
  return v_x;
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("Function call args:\n");
  LINE(8,x_var_dump(1, less_rev(f_f(++v_i), f_f((v_i = 0LL)))));
  LINE(9,x_var_dump(1, not_more_rev(f_f(++v_i), f_f((v_i = 0LL)))));
  LINE(10,x_var_dump(1, more_rev(f_f(++v_i), f_f((v_i = 0LL)))));
  LINE(11,x_var_dump(1, not_less_rev(f_f(++v_i), f_f((v_i = 0LL)))));
  echo("\nArray indices:\n");
  lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(2LL, (0LL), 0x486AFCC090D5F98CLL);
  lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).set(4LL, (1LL), 0x6F2A25235E544A31LL);
  (v_i = 0LL);
  LINE(17,x_var_dump(1, less_rev(v_a.rvalAt(++v_i).rvalAt(++v_i), v_a.rvalAt((v_i = 1LL)).rvalAt(++v_i))));
  LINE(18,x_var_dump(1, not_more_rev(v_a.rvalAt(++v_i).rvalAt(++v_i), v_a.rvalAt((v_i = 1LL)).rvalAt(++v_i))));
  LINE(19,x_var_dump(1, more_rev(v_a.rvalAt(++v_i).rvalAt(++v_i), v_a.rvalAt((v_i = 1LL)).rvalAt(++v_i))));
  LINE(20,x_var_dump(1, not_less_rev(v_a.rvalAt(++v_i).rvalAt(++v_i), v_a.rvalAt((v_i = 1LL)).rvalAt(++v_i))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
