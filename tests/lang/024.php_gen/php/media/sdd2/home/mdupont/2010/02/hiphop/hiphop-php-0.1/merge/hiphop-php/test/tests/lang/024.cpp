
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/023-2.inc.nophp.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/024.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$024_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/024.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$024_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_jdk __attribute__((__unused__)) = (variables != gVariables) ? variables->get("jdk") : g->GV(jdk);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_blah __attribute__((__unused__)) = (variables != gVariables) ? variables->get("blah") : g->GV(blah);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_arr_len __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr_len") : g->GV(arr_len);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_resize __attribute__((__unused__)) = (variables != gVariables) ? variables->get("resize") : g->GV(resize);
  Variant &v_q __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q") : g->GV(q);

  {
    LOOP_COUNTER(1);
    for ((v_jdk = 0LL); less(v_jdk, 50LL); v_jdk++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo("<html>\n<head>\n");
        echo("*** Testing assignments and variable aliasing: ***\n");
        (v_a = "b");
        (variables->get(toString(v_a)) = "test");
        (variables->get(toString(variables->get(toString(v_a)))) = "blah");
        variables->get(toString(variables->get(toString(variables->get(toString(v_a)))))).set("associative arrays work too", ("this is nifty"), 0x79514826196824FBLL);
        echo("This should read \"blah\": ");
        echo(toString(v_test) + toString("\n"));
        echo("This should read \"this is nifty\": ");
        echo(concat(toString(v_blah.rvalAt((v_test = "associative arrays work too"))), "\n"));
        echo("*************************************************\n\n*** Testing integer operators ***\n");
        (v_i = 5LL);
        (v_j = 3LL);
        echo("Correct result - 8:  ");
        echo(toString(v_i + v_j));
        echo("\nCorrect result - 8:  ");
        echo(toString(v_i + v_j));
        echo("\nCorrect result - 2:  ");
        echo(toString(v_i - v_j));
        echo("\nCorrect result - -2:  ");
        echo(toString(v_j - v_i));
        echo("\nCorrect result - 15:  ");
        echo(toString(v_i * v_j));
        echo("\nCorrect result - 15:  ");
        echo(toString(v_j * v_i));
        echo("\nCorrect result - 2:  ");
        echo(toString(modulo(toInt64(v_i), toInt64(v_j))));
        echo("\nCorrect result - 3:  ");
        echo(toString(modulo(toInt64(v_j), toInt64(v_i))));
        echo("\n*********************************\n\n*** Testing real operators ***\n");
        (v_i = 5.0);
        (v_j = 3.0);
        echo("Correct result - 8:  ");
        echo(toString(v_i + v_j));
        echo("\nCorrect result - 8:  ");
        echo(toString(v_i + v_j));
        echo("\nCorrect result - 2:  ");
        echo(toString(v_i - v_j));
        echo("\nCorrect result - -2:  ");
        echo(toString(v_j - v_i));
        echo("\nCorrect result - 15:  ");
        echo(toString(v_i * v_j));
        echo("\nCorrect result - 15:  ");
        echo(toString(v_j * v_i));
        echo("\nCorrect result - 2:  ");
        echo(toString(modulo(toInt64(v_i), toInt64(v_j))));
        echo("\nCorrect result - 3:  ");
        echo(toString(modulo(toInt64(v_j), toInt64(v_i))));
        echo("\n*********************************\n\n*** Testing if/elseif/else control ***\n\n");
        (v_a = 5LL);
        if (equal(v_a, "4")) {
          echo("This  does   not  work\n");
        }
        else if (equal(v_a, "5")) {
          echo("This  works\n");
          (v_a = 6LL);
          if (equal("andi", ((v_test = "andi")))) {
            echo("this_still_works\n");
          }
          else if (toBoolean(1LL)) {
            echo("should_not_print\n");
          }
          else {
            echo("should_not_print\n");
          }
          {
            echo("should_print\n");
          }
        }
        else if (equal(v_a, 6LL)) {
          echo("this broken\n");
          if (toBoolean(0LL)) {
            echo("this_should_not_print\n");
          }
          else {
            echo("TestingDanglingElse_This_Should_not_print\n");
          }
        }
        else {
          echo("This does  not work\n");
        }
        echo("\n\n*** Seriously nested if's test ***\n** spelling correction by kluzz **\n");
        (v_i = (v_j = 0LL));
        echo("Only two lines of text should follow:\n");
        if (toBoolean(0LL)) {
          echo("hmm, this shouldn't be displayed #1\n");
          v_j++;
          if (toBoolean(1LL)) {
            v_i += v_j;
            if (toBoolean(0LL)) {
              (v_j = ++v_i);
              if (toBoolean(1LL)) {
                v_j *= v_i;
                echo("damn, this shouldn't be displayed\n");
              }
              else {
                v_j /= v_i;
                ++v_j;
                echo("this shouldn't be displayed either\n");
              }
            }
            else if (toBoolean(1LL)) {
              v_i++;
              v_j++;
              echo("this isn't supposed to be displayed\n");
            }
          }
          else if (toBoolean(0LL)) {
            v_i++;
            echo("this definitely shouldn't be displayed\n");
          }
          else {
            --v_j;
            echo("and this too shouldn't be displayed\n");
            LOOP_COUNTER(2);
            {
              while (more(v_j, 0LL)) {
                LOOP_COUNTER_CHECK(2);
                {
                  v_j--;
                }
              }
            }
          }
        }
        else if (toBoolean(0LL)) {
          (v_i = ++v_j);
          echo("hmm, this shouldn't be displayed #2\n");
          if (toBoolean(1LL)) {
            (v_j = ++v_i);
            if (toBoolean(0LL)) {
              (v_j = v_i * 2LL + v_j * (v_i++));
              if (toBoolean(1LL)) {
                v_i++;
                echo("damn, this shouldn't be displayed\n");
              }
              else {
                v_j++;
                echo("this shouldn't be displayed either\n");
              }
            }
            else if (toBoolean(1LL)) {
              ++v_j;
              echo("this isn't supposed to be displayed\n");
            }
          }
          else if (toBoolean(0LL)) {
            v_j++;
            echo("this definitely shouldn't be displayed\n");
          }
          else {
            v_i++;
            echo("and this too shouldn't be displayed\n");
          }
        }
        else {
          (v_j = v_i++);
          echo(LINE(167,concat5("this should be displayed. should be:  $i=1, $j=0.  is:  $i=", toString(v_i), ", $j=", toString(v_j), "\n")));
          if (toBoolean(1LL)) {
            v_j += ++v_i;
            if (toBoolean(0LL)) {
              v_j += 40LL;
              if (toBoolean(1LL)) {
                v_i += 50LL;
                echo("damn, this shouldn't be displayed\n");
              }
              else {
                v_j += 20LL;
                echo("this shouldn't be displayed either\n");
              }
            }
            else if (toBoolean(1LL)) {
              v_j *= v_i;
              echo(LINE(181,concat5("this is supposed to be displayed. should be:  $i=2, $j=4.  is:  $i=", toString(v_i), ", $j=", toString(v_j), "\n")));
              echo("3 loop iterations should follow:\n");
              LOOP_COUNTER(3);
              {
                while (not_more(v_i, v_j)) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    echo(concat_rev(LINE(184,concat3(" ", toString(v_j), "\n")), toString(v_i++)));
                  }
                }
              }
            }
          }
          else if (toBoolean(0LL)) {
            echo("this definitely shouldn't be displayed\n");
          }
          else {
            echo("and this too shouldn't be displayed\n");
          }
          echo("**********************************\n");
        }
        echo("\n*** C-style else-if's ***\n");
        if (toBoolean((v_a = 0LL))) {
          echo("This shouldn't be displayed\n");
        }
        else if (toBoolean(v_a++)) {
          echo("This shouldn't be displayed either\n");
        }
        else if (toBoolean(--v_a)) {
          echo("No, this neither\n");
        }
        else if (toBoolean(++v_a)) {
          echo("This should be displayed\n");
        }
        else {
          echo("This shouldn't be displayed at all\n");
        }
        echo("*************************\n\n*** WHILE tests ***\n");
        (v_i = 0LL);
        (v_j = 20LL);
        LOOP_COUNTER(4);
        {
          while (less(v_i, (2LL * v_j))) {
            LOOP_COUNTER_CHECK(4);
            {
              if (more(v_i, v_j)) {
                echo(LINE(219,concat4(toString(v_i), " is greater than ", toString(v_j), "\n")));
              }
              else if (equal(v_i, v_j)) {
                echo(LINE(221,concat4(toString(v_i), " equals ", toString(v_j), "\n")));
              }
              else {
                echo(LINE(223,concat4(toString(v_i), " is smaller than ", toString(v_j), "\n")));
              }
              v_i++;
            }
          }
        }
        echo("*******************\n\n\n*** Nested WHILEs ***\n");
        (v_arr_len = 3LL);
        (v_i = 0LL);
        LOOP_COUNTER(5);
        {
          while (less(v_i, v_arr_len)) {
            LOOP_COUNTER_CHECK(5);
            {
              (v_j = 0LL);
              LOOP_COUNTER(6);
              {
                while (less(v_j, v_arr_len)) {
                  LOOP_COUNTER_CHECK(6);
                  {
                    (v_k = 0LL);
                    LOOP_COUNTER(7);
                    {
                      while (less(v_k, v_arr_len)) {
                        LOOP_COUNTER_CHECK(7);
                        {
                          variables->get(LINE(241,concat3("test", toString(v_i), toString(v_j)))).set(v_k, (v_i + v_j + v_k));
                          v_k++;
                        }
                      }
                    }
                    v_j++;
                  }
                }
              }
              v_i++;
            }
          }
        }
        echo("Each array variable should be equal to the sum of its indices:\n");
        (v_i = 0LL);
        LOOP_COUNTER(8);
        {
          while (less(v_i, v_arr_len)) {
            LOOP_COUNTER_CHECK(8);
            {
              (v_j = 0LL);
              LOOP_COUNTER(9);
              {
                while (less(v_j, v_arr_len)) {
                  LOOP_COUNTER_CHECK(9);
                  {
                    (v_k = 0LL);
                    LOOP_COUNTER(10);
                    {
                      while (less(v_k, v_arr_len)) {
                        LOOP_COUNTER_CHECK(10);
                        {
                          echo(LINE(257,(assignCallTemp(eo_1, toString(v_i)),assignCallTemp(eo_2, concat6(toString(v_j), "}[", toString(v_k), "] = ", toString(variables->get(concat3("test", toString(v_i), toString(v_j))).rvalAt(v_k)), "\n")),concat3("${test", eo_1, eo_2))));
                          v_k++;
                        }
                      }
                    }
                    v_j++;
                  }
                }
              }
              v_i++;
            }
          }
        }
        echo("*********************\n\n*** hash test... ***\n");
        echo("commented out...");
        echo("\n**************************\n\n*** Hash resizing test ***\n");
        (v_i = 10LL);
        (v_a = "b");
        LOOP_COUNTER(11);
        {
          while (more(v_i, 0LL)) {
            LOOP_COUNTER_CHECK(11);
            {
              (v_a = concat(toString(v_a), "a"));
              echo(toString(v_a) + toString("\n"));
              v_resize.set(v_a, (v_i));
              v_i--;
            }
          }
        }
        (v_i = 10LL);
        (v_a = "b");
        LOOP_COUNTER(12);
        {
          while (more(v_i, 0LL)) {
            LOOP_COUNTER_CHECK(12);
            {
              (v_a = concat(toString(v_a), "a"));
              echo(toString(v_a) + toString("\n"));
              echo(concat(toString(v_resize.rvalAt(v_a)), "\n"));
              v_i--;
            }
          }
        }
        echo("**************************\n\n\n*** break/continue test ***\n");
        (v_i = 0LL);
        echo("$i should go from 0 to 2\n");
        LOOP_COUNTER(13);
        {
          while (less(v_i, 5LL)) {
            LOOP_COUNTER_CHECK(13);
            {
              if (more(v_i, 2LL)) {
                break;
              }
              (v_j = 0LL);
              echo("$j should go from 3 to 4, and $q should go from 3 to 4\n");
              LOOP_COUNTER(14);
              {
                while (less(v_j, 5LL)) {
                  LOOP_COUNTER_CHECK(14);
                  {
                    if (not_more(v_j, 2LL)) {
                      v_j++;
                      continue;
                    }
                    echo(LINE(325,concat3("  $j=", toString(v_j), "\n")));
                    {
                      LOOP_COUNTER(15);
                      for ((v_q = 0LL); not_more(v_q, 10LL); v_q++) {
                        LOOP_COUNTER_CHECK(15);
                        {
                          if (less(v_q, 3LL)) {
                            continue;
                          }
                          if (more(v_q, 4LL)) {
                            break;
                          }
                          echo(LINE(333,concat3("    $q=", toString(v_q), "\n")));
                        }
                      }
                    }
                    v_j++;
                  }
                }
              }
              (v_j = 0LL);
              echo("$j should go from 0 to 2\n");
              LOOP_COUNTER(16);
              {
                while (less(v_j, 5LL)) {
                  LOOP_COUNTER_CHECK(16);
                  {
                    if (more(v_j, 2LL)) {
                      (v_k = 0LL);
                      echo("$k should go from 0 to 2\n");
                      LOOP_COUNTER(17);
                      {
                        while (less(v_k, 5LL)) {
                          LOOP_COUNTER_CHECK(17);
                          {
                            if (more(v_k, 2LL)) {
                              goto break16;
                            }
                            echo(LINE(347,concat3("    $k=", toString(v_k), "\n")));
                            v_k++;
                          }
                        }
                      }
                    }
                    echo(LINE(351,concat3("  $j=", toString(v_j), "\n")));
                    v_j++;
                  }
                }
                break16:;
              }
              echo(LINE(354,concat3("$i=", toString(v_i), "\n")));
              v_i++;
            }
          }
        }
        echo("***********************\n\n*** Nested file include test ***\n");
        LINE(361,pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$023_2_inc(false, variables));
        echo("********************************\n\n");
        {
          echo("Tests completed.\n");
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
