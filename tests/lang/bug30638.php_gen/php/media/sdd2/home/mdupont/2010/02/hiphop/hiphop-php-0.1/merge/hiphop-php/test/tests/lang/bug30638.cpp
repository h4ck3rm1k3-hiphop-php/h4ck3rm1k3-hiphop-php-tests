
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30638.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug30638_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug30638.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug30638_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_lc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lc") : g->GV(lc);

  LINE(3,x_setlocale(5, toInt32(1LL) /* LC_NUMERIC */, "de_DE", ScalarArrays::sa_[0]));
  (v_lc = LINE(5,x_localeconv()));
  LINE(6,x_printf(2, "decimal_point: %s\n", Array(ArrayInit(1).set(0, v_lc.rvalAt("decimal_point", 0x0076A8AA3D777F93LL)).create())));
  LINE(7,x_printf(2, "thousands_sep: %s\n", Array(ArrayInit(1).set(0, v_lc.rvalAt("thousands_sep", 0x7443E7EED9CAFCA7LL)).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
