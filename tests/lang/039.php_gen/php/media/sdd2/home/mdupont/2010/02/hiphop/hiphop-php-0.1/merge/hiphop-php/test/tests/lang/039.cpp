
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/039.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/039.php line 7 */
Variant c_myexception::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_myexception::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_myexception::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_myexception::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_myexception::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_myexception::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_myexception::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_myexception::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(myexception)
ObjectData *c_myexception::create(Variant v_errstr, Variant v_errno, Variant v_errfile, Variant v_errline) {
  init();
  t___construct(v_errstr, v_errno, v_errfile, v_errline);
  return this;
}
ObjectData *c_myexception::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
ObjectData *c_myexception::cloneImpl() {
  c_myexception *obj = NEW(c_myexception)();
  cloneSet(obj);
  return obj;
}
void c_myexception::cloneSet(c_myexception *clone) {
  c_exception::cloneSet(clone);
}
Variant c_myexception::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_myexception::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2, a3), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myexception::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myexception$os_get(const char *s) {
  return c_myexception::os_get(s, -1);
}
Variant &cw_myexception$os_lval(const char *s) {
  return c_myexception::os_lval(s, -1);
}
Variant cw_myexception$os_constant(const char *s) {
  return c_myexception::os_constant(s);
}
Variant cw_myexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myexception::os_invoke(c, s, params, -1, fatal);
}
void c_myexception::init() {
  c_exception::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/039.php line 9 */
void c_myexception::t___construct(Variant v_errstr, Variant v_errno, Variant v_errfile, Variant v_errline) {
  INSTANCE_METHOD_INJECTION(MyException, MyException::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(11,c_exception::t___construct(v_errstr, v_errno));
  (m_file = v_errfile);
  (m_line = v_errline);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/039.php line 17 */
void f_error2exception(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline) {
  FUNCTION_INJECTION(Error2Exception);
  throw_exception(((Object)(LINE(19,p_myexception(p_myexception(NEWOBJ(c_myexception)())->create(v_errstr, v_errno, v_errfile, v_errline))))));
} /* function */
Variant i_error2exception(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x01A0C66890068A06LL, error2exception) {
    return (f_error2exception(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_myexception(CArrRef params, bool init /* = true */) {
  return Object(p_myexception(NEW(c_myexception)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$039_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/039.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$039_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_err_msg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("err_msg") : g->GV(err_msg);
  Variant &v_con __attribute__((__unused__)) = (variables != gVariables) ? variables->get("con") : g->GV(con);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  (v_err_msg = "no exception");
  LINE(23,x_set_error_handler("Error2Exception"));
  try {
    (v_con = LINE(27,x_fopen("/tmp/a_file_that_does_not_exist", "r")));
  } catch (Object e) {
    if (e.instanceof("catchable")) {
      v_e = e;
      echo("Catchable\n");
    } else if (e.instanceof("exception")) {
      v_e = e;
      echo("Exception\n");
    } else {
      throw;
    }
  }
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
