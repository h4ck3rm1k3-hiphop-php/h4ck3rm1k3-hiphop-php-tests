
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_010.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_010.php line 3 */
void f_f(Variant v_a) {
  FUNCTION_INJECTION(f);
  LINE(4,x_var_dump(1, v_a));
  (v_a = "a.changed");
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_010_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_010.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_010_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("\n\n---> Pass constant assignment by reference:\n");
  LINE(9,f_f((v_a = "a.original")));
  LINE(10,x_var_dump(1, v_a));
  echo("\n\n---> Pass variable assignment by reference:\n");
  unset(v_a);
  (v_a = "a.original");
  LINE(15,f_f((v_b = v_a)));
  LINE(16,x_var_dump(1, v_a));
  echo("\n\n---> Pass reference assignment by reference:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = "a.original");
  LINE(21,f_f((v_b = ref(v_a))));
  LINE(22,x_var_dump(1, v_a));
  echo("\n\n---> Pass concat assignment by reference:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_b = "b.original");
  (v_a = "a.original");
  LINE(28,f_f(concat_assign(v_b, toString(v_a))));
  LINE(29,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
