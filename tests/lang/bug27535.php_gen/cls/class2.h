
#ifndef __GENERATED_cls_class2_h__
#define __GENERATED_cls_class2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27535.php line 8 */
class c_class2 : virtual public ObjectData {
  BEGIN_CLASS_MAP(class2)
  END_CLASS_MAP(class2)
  DECLARE_CLASS(class2, Class2, ObjectData)
  void init();
  public: Variant m_storage;
  public: void t_class2();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class2_h__
