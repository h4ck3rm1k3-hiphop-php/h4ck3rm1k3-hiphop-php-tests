
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27535.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27535.php line 3 */
Variant c_class1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_class1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_class1::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_Class2_obj", m__Class2_obj.isReferenced() ? ref(m__Class2_obj) : m__Class2_obj));
  c_ObjectData::o_get(props);
}
bool c_class1::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4E4E2AF42351CA4ELL, _Class2_obj, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_class1::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4E4E2AF42351CA4ELL, m__Class2_obj,
                         _Class2_obj, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_class1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4E4E2AF42351CA4ELL, m__Class2_obj,
                      _Class2_obj, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_class1::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4E4E2AF42351CA4ELL, m__Class2_obj,
                         _Class2_obj, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_class1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(class1)
ObjectData *c_class1::cloneImpl() {
  c_class1 *obj = NEW(c_class1)();
  cloneSet(obj);
  return obj;
}
void c_class1::cloneSet(c_class1 *clone) {
  clone->m__Class2_obj = m__Class2_obj.isReferenced() ? ref(m__Class2_obj) : m__Class2_obj;
  ObjectData::cloneSet(clone);
}
Variant c_class1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_class1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class1$os_get(const char *s) {
  return c_class1::os_get(s, -1);
}
Variant &cw_class1$os_lval(const char *s) {
  return c_class1::os_lval(s, -1);
}
Variant cw_class1$os_constant(const char *s) {
  return c_class1::os_constant(s);
}
Variant cw_class1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class1::os_invoke(c, s, params, -1, fatal);
}
void c_class1::init() {
  m__Class2_obj = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27535.php line 8 */
Variant c_class2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_class2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_class2::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("storage", m_storage.isReferenced() ? ref(m_storage) : m_storage));
  c_ObjectData::o_get(props);
}
bool c_class2::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x1EA489BB64FC2CB1LL, storage, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_class2::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x1EA489BB64FC2CB1LL, m_storage,
                         storage, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_class2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x1EA489BB64FC2CB1LL, m_storage,
                      storage, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_class2::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x1EA489BB64FC2CB1LL, m_storage,
                         storage, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_class2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(class2)
ObjectData *c_class2::create() {
  init();
  t_class2();
  return this;
}
ObjectData *c_class2::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_class2::cloneImpl() {
  c_class2 *obj = NEW(c_class2)();
  cloneSet(obj);
  return obj;
}
void c_class2::cloneSet(c_class2 *clone) {
  clone->m_storage = m_storage.isReferenced() ? ref(m_storage) : m_storage;
  ObjectData::cloneSet(clone);
}
Variant c_class2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_class2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class2$os_get(const char *s) {
  return c_class2::os_get(s, -1);
}
Variant &cw_class2$os_lval(const char *s) {
  return c_class2::os_lval(s, -1);
}
Variant cw_class2$os_constant(const char *s) {
  return c_class2::os_constant(s);
}
Variant cw_class2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class2::os_invoke(c, s, params, -1, fatal);
}
void c_class2::init() {
  m_storage = "";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27535.php line 12 */
void c_class2::t_class2() {
  INSTANCE_METHOD_INJECTION(Class2, Class2::Class2);
  bool oldInCtor = gasInCtor(true);
  (m_storage = ((Object)(LINE(14,p_class1(p_class1(NEWOBJ(c_class1)())->create())))));
  (m_storage.o_lval("_Class2_obj", 0x4E4E2AF42351CA4ELL) = ((Object)(this)));
  gasInCtor(oldInCtor);
} /* function */
Object co_class1(CArrRef params, bool init /* = true */) {
  return Object(p_class1(NEW(c_class1)())->dynCreate(params, init));
}
Object co_class2(CArrRef params, bool init /* = true */) {
  return Object(p_class2(NEW(c_class2)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug27535_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27535.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug27535_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = ((Object)(LINE(20,p_class2(p_class2(NEWOBJ(c_class2)())->create())))));
  echo("Alive!\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
