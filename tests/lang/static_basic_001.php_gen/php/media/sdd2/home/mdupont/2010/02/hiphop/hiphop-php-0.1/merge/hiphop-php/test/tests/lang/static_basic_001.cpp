
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_basic_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_basic_001.php line 17 */
void f_manyinits() {
  FUNCTION_INJECTION(manyInits);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_counter __attribute__((__unused__)) = g->sv_manyinits_DupIdcounter;
  bool &inited_sv_counter __attribute__((__unused__)) = g->inited_sv_manyinits_DupIdcounter;
  Variant &sv_a __attribute__((__unused__)) = g->sv_manyinits_DupIda;
  bool &inited_sv_a __attribute__((__unused__)) = g->inited_sv_manyinits_DupIda;
  int64 &sv_b __attribute__((__unused__)) = g->sv_manyinits_DupIdb;
  bool &inited_sv_b __attribute__((__unused__)) = g->inited_sv_manyinits_DupIdb;
  int64 &sv_c __attribute__((__unused__)) = g->sv_manyinits_DupIdc;
  bool &inited_sv_c __attribute__((__unused__)) = g->inited_sv_manyinits_DupIdc;
  Variant &sv_d __attribute__((__unused__)) = g->sv_manyinits_DupIdd;
  bool &inited_sv_d __attribute__((__unused__)) = g->inited_sv_manyinits_DupIdd;
  int64 &sv_e __attribute__((__unused__)) = g->sv_manyinits_DupIde;
  bool &inited_sv_e __attribute__((__unused__)) = g->inited_sv_manyinits_DupIde;
  if (!inited_sv_counter) {
    (sv_counter = 0LL);
    inited_sv_counter = true;
  }
  echo(LINE(19,concat3("------------- Call ", toString(sv_counter), " --------------\n")));
  {
    if (!inited_sv_a) {
      (sv_a = null);
      inited_sv_a = true;
    }
    if (!inited_sv_b) {
      (sv_b = 10LL);
      inited_sv_b = true;
    }
    if (!inited_sv_c) {
      (sv_c = 20LL);
      inited_sv_c = true;
    }
    if (!inited_sv_d) {
      (sv_d = null);
      inited_sv_d = true;
    }
    if (!inited_sv_e) {
      (sv_e = 30LL);
      inited_sv_e = true;
    }
  }
  echo(LINE(21,concat3("Unitialised      : ", toString(sv_a), "\n")));
  echo(LINE(22,concat3("Initialised to 10: ", toString(sv_b), "\n")));
  echo(LINE(23,concat3("Initialised to 20: ", toString(sv_c), "\n")));
  echo(LINE(24,concat3("Unitialised      : ", toString(sv_d), "\n")));
  echo(LINE(25,concat3("Initialised to 30: ", toString(sv_e), "\n")));
  sv_a++;
  sv_b++;
  sv_c++;
  sv_d++;
  sv_e++;
  sv_counter++;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_basic_001.php line 4 */
void f_staticnonstatic() {
  FUNCTION_INJECTION(staticNonStatic);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_a __attribute__((__unused__)) = g->sv_staticnonstatic_DupIda;
  bool &inited_sv_a __attribute__((__unused__)) = g->inited_sv_staticnonstatic_DupIda;
  Variant v_a;

  echo("---------\n");
  (v_a = 0LL);
  echo(toString(v_a) + toString("\n"));
  v_a = ref(sv_a);
  if (!inited_sv_a) {
    (v_a = 10LL);
    inited_sv_a = true;
  }
  echo(toString(v_a) + toString("\n"));
  v_a++;
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$static_basic_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/static_basic_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$static_basic_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  echo("\nSame variable used as static and non static.\n");
  LINE(12,f_staticnonstatic());
  LINE(13,f_staticnonstatic());
  LINE(14,f_staticnonstatic());
  echo("\nLots of initialisations in the same statement.\n");
  LINE(33,f_manyinits());
  LINE(34,f_manyinits());
  LINE(35,f_manyinits());
  echo("\nUsing static keyword at global scope\n");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 3LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          (v_s = null);
          (v_k = 10LL);
        }
        echo(LINE(40,concat4(toString(v_s), " ", toString(v_k), "\n")));
        v_s++;
        v_k++;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
