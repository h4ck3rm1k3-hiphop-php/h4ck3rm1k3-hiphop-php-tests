
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(i)
    GVS(s)
    GVS(k)
  END_GVS(3)

  // Dynamic Constants

  // Function/Method Static Variables
  Variant sv_staticnonstatic_DupIda;
  int64 sv_manyinits_DupIdcounter;
  Variant sv_manyinits_DupIda;
  int64 sv_manyinits_DupIdb;
  int64 sv_manyinits_DupIdc;
  Variant sv_manyinits_DupIdd;
  int64 sv_manyinits_DupIde;

  // Function/Method Static Variable Initialization Booleans
  bool inited_sv_staticnonstatic_DupIda;
  bool inited_sv_manyinits_DupIdcounter;
  bool inited_sv_manyinits_DupIda;
  bool inited_sv_manyinits_DupIdb;
  bool inited_sv_manyinits_DupIdc;
  bool inited_sv_manyinits_DupIdd;
  bool inited_sv_manyinits_DupIde;

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$static_basic_001_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 15;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
