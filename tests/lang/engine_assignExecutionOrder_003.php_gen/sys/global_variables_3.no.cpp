
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x4056C2E766E0B782LL, val, 24);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x68DF81F26D942FC7LL, a1, 17);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 13);
      break;
    case 13:
      HASH_INDEX(0x43EDA7BEE714570DLL, a3, 19);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 12);
      HASH_INDEX(0x0EC80E071B573095LL, aa, 15);
      break;
    case 22:
      HASH_INDEX(0x04BFC205E59FA416LL, x, 25);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 20);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 38:
      HASH_INDEX(0x0C058190047906E6LL, x3, 28);
      break;
    case 40:
      HASH_INDEX(0x1776D8467CB08D68LL, arr, 21);
      break;
    case 42:
      HASH_INDEX(0x24CB9043B79631EALL, crr, 23);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 48:
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 16);
      break;
    case 49:
      HASH_INDEX(0x55093A23014F1AB1LL, bb, 14);
      break;
    case 50:
      HASH_INDEX(0x04308C0F6B45D0F2LL, brr, 22);
      break;
    case 54:
      HASH_INDEX(0x6C05C2858C72A876LL, a2, 18);
      HASH_INDEX(0x5FCB7D54BC579D36LL, bx, 29);
      break;
    case 57:
      HASH_INDEX(0x5075985E0413CB39LL, x2, 27);
      break;
    case 61:
      HASH_INDEX(0x5685725E6F24EAFDLL, x1, 26);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 30) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
