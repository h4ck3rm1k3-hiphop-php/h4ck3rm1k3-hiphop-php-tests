
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "b",
    "a",
    "bb",
    "aa",
    "c",
    "a1",
    "a2",
    "a3",
    "i",
    "arr",
    "brr",
    "crr",
    "val",
    "x",
    "x1",
    "x2",
    "x3",
    "bx",
  };
  if (idx >= 0 && idx < 30) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(b);
    case 13: return GV(a);
    case 14: return GV(bb);
    case 15: return GV(aa);
    case 16: return GV(c);
    case 17: return GV(a1);
    case 18: return GV(a2);
    case 19: return GV(a3);
    case 20: return GV(i);
    case 21: return GV(arr);
    case 22: return GV(brr);
    case 23: return GV(crr);
    case 24: return GV(val);
    case 25: return GV(x);
    case 26: return GV(x1);
    case 27: return GV(x2);
    case 28: return GV(x3);
    case 29: return GV(bx);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
