
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_live(CArrRef params);
Variant i_foo(CArrRef params);
static Variant invoke_case_4(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x529F4067BC16F10CLL, live);
  HASH_INVOKE(0x6176C0B993BF5914LL, foo);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_baa(CArrRef params);
Variant i_dead(CArrRef params);
static Variant invoke_case_7(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x185570B359931707LL, baa);
  HASH_INVOKE(0x0FC8C41A036B5347LL, dead);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[8])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 8; i++) funcTable[i] = &invoke_builtin;
    funcTable[4] = &invoke_case_4;
    funcTable[7] = &invoke_case_7;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 7](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
