
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.php line 29 */
void f_live() {
  FUNCTION_INJECTION(live);
  echo("Good call\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.php line 63 */
int64 f_mod(CVarRef v_b) {
  FUNCTION_INJECTION(mod);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  (gv_x = v_b);
  return 0LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.php line 45 */
Primitive f_foo1() {
  FUNCTION_INJECTION(foo1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return ++gv_a;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.php line 5 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  echo("Bad call\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.php line 10 */
void f_baa() {
  FUNCTION_INJECTION(baa);
  echo("Good call\n");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.php line 34 */
void f_dead() {
  FUNCTION_INJECTION(dead);
  echo("Bad call\n");
} /* function */
Variant i_live(CArrRef params) {
  return (f_live(), null);
}
Variant i_foo(CArrRef params) {
  return (f_foo(), null);
}
Variant i_baa(CArrRef params) {
  return (f_baa(), null);
}
Variant i_dead(CArrRef params) {
  return (f_dead(), null);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_bb __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bb") : g->GV(bb);
  Variant &v_aa __attribute__((__unused__)) = (variables != gVariables) ? variables->get("aa") : g->GV(aa);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_a3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a3") : g->GV(a3);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_brr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("brr") : g->GV(brr);
  Variant &v_crr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("crr") : g->GV(crr);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_x1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x1") : g->GV(x1);
  Variant &v_x2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x2") : g->GV(x2);
  Variant &v_x3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x3") : g->GV(x3);
  Variant &v_bx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bx") : g->GV(bx);

  (v_b = "bb");
  (v_a = "aa");
  (v_bb = "baa");
  (v_aa = "foo");
  (v_c = variables->get(toString((v_a = v_b))));
  LINE(21,invoke(toString(v_c), Array(), -1));
  (v_a1 = ScalarArrays::sa_[0]);
  (v_a2 = ScalarArrays::sa_[1]);
  (v_a3 = ScalarArrays::sa_[0]);
  (v_a = Array(ArrayInit(3).set(0, v_a1).set(1, v_a2).set(2, v_a3).create()));
  (v_i = 0LL);
  LINE(41,invoke((toString(v_a.rvalAt((v_i = 1LL)).rvalAt(++v_i))), Array(), -1));
  (v_a = -1LL);
  (v_arr = ScalarArrays::sa_[2]);
  (v_brr = ScalarArrays::sa_[3]);
  (v_crr = ScalarArrays::sa_[4]);
  lval(v_arr.lvalAt(LINE(56,f_foo1()))).set(f_foo1(), (plus_rev(v_crr.rvalAt(LINE(57,f_foo1())).rvalAt(f_foo1()), v_brr.rvalAt(LINE(56,f_foo1())).rvalAt(f_foo1()))));
  (v_val = v_arr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  echo(LINE(60,concat3("Expect 15 and get...", toString(v_val), "\n")));
  (v_x = ScalarArrays::sa_[5]);
  (v_x1 = ScalarArrays::sa_[6]);
  (v_x2 = ScalarArrays::sa_[7]);
  (v_x3 = ScalarArrays::sa_[8]);
  (v_bx = ScalarArrays::sa_[9]);
  lval(v_x.lvalAt(LINE(75,f_mod(v_x1)))).set(f_mod(v_x2), (v_bx.rvalAt(f_mod(v_x3))));
  LINE(79,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
