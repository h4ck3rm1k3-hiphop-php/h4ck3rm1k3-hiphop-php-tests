
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.php line 3 */
Variant c_mealiterator::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_mealiterator::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_mealiterator::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("pos", m_pos));
  props.push_back(NEW(ArrayElement)("myContent", m_myContent));
  c_ObjectData::o_get(props);
}
bool c_mealiterator::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x0CF096B3F7233879LL, myContent, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x277B6D36F75741AELL, pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_mealiterator::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x0CF096B3F7233879LL, m_myContent,
                         myContent, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x277B6D36F75741AELL, m_pos,
                         pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_mealiterator::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x0CF096B3F7233879LL, m_myContent,
                      myContent, 9);
      break;
    case 2:
      HASH_SET_STRING(0x277B6D36F75741AELL, m_pos,
                      pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_mealiterator::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_mealiterator::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(mealiterator)
ObjectData *c_mealiterator::cloneImpl() {
  c_mealiterator *obj = NEW(c_mealiterator)();
  cloneSet(obj);
  return obj;
}
void c_mealiterator::cloneSet(c_mealiterator *clone) {
  clone->m_pos = m_pos;
  clone->m_myContent = m_myContent;
  ObjectData::cloneSet(clone);
}
Variant c_mealiterator::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_mealiterator::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_mealiterator::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_mealiterator$os_get(const char *s) {
  return c_mealiterator::os_get(s, -1);
}
Variant &cw_mealiterator$os_lval(const char *s) {
  return c_mealiterator::os_lval(s, -1);
}
Variant cw_mealiterator$os_constant(const char *s) {
  return c_mealiterator::os_constant(s);
}
Variant cw_mealiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_mealiterator::os_invoke(c, s, params, -1, fatal);
}
void c_mealiterator::init() {
  m_pos = 0LL;
  m_myContent = ScalarArrays::sa_[0];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.php line 7 */
bool c_mealiterator::t_valid() {
  INSTANCE_METHOD_INJECTION(MealIterator, MealIterator::valid);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(9,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "MealIterator::valid", eo_2))));
  return less(m_pos, 3LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.php line 13 */
Variant c_mealiterator::t_next() {
  INSTANCE_METHOD_INJECTION(MealIterator, MealIterator::next);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(15,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "MealIterator::next", eo_2))));
  return m_myContent.rvalAt(m_pos++);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.php line 19 */
void c_mealiterator::t_rewind() {
  INSTANCE_METHOD_INJECTION(MealIterator, MealIterator::rewind);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(21,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "MealIterator::rewind", eo_2))));
  (m_pos = 0LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.php line 25 */
Variant c_mealiterator::t_current() {
  INSTANCE_METHOD_INJECTION(MealIterator, MealIterator::current);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(27,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "MealIterator::current", eo_2))));
  return m_myContent.rvalAt(m_pos);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.php line 31 */
String c_mealiterator::t_key() {
  INSTANCE_METHOD_INJECTION(MealIterator, MealIterator::key);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(33,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "MealIterator::key", eo_2))));
  return concat("meal ", toString(m_pos));
} /* function */
Object co_mealiterator(CArrRef params, bool init /* = true */) {
  return Object(p_mealiterator(NEW(c_mealiterator)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIterator_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIterator_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_indent __attribute__((__unused__)) = (variables != gVariables) ? variables->get("indent") : g->GV(indent);
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);

  (v_f = ((Object)(LINE(39,p_mealiterator(p_mealiterator(NEWOBJ(c_mealiterator)())->create())))));
  LINE(40,x_var_dump(1, v_f));
  echo("-----( Simple iteration: )-----\n");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_f.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        echo(LINE(44,concat4(toString(v_k), " => ", toString(v_v), "\n")));
      }
    }
  }
  LINE(47,v_f.o_invoke_few_args("rewind", 0x1670096FDE27AF6ALL, 0));
  (v_indent = " ");
  echo("\n\n\n-----( Nested iteration: )-----\n");
  (v_count = 1LL);
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_f.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        echo(LINE(54,(assignCallTemp(eo_1, toString(v_count++)),concat3("\nTop level ", eo_1, ": \n"))));
        echo(LINE(55,concat4(toString(v_k), " => ", toString(v_v), "\n")));
        (v_indent = "     ");
        {
          LOOP_COUNTER(7);
          for (ArrayIterPtr iter9 = v_f.begin(); !iter9->end(); iter9->next()) {
            LOOP_COUNTER_CHECK(7);
            v_v = iter9->second();
            v_k = iter9->first();
            {
              echo(LINE(58,concat5("     ", toString(v_k), " => ", toString(v_v), "\n")));
            }
          }
        }
        (v_indent = " ");
      }
    }
  }
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
