
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22592.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22592.php line 2 */
void f_error_hdlr(CVarRef v_errno, CVarRef v_errstr) {
  FUNCTION_INJECTION(error_hdlr);
  echo(LINE(3,concat3("[", toString(v_errstr), "]\n")));
} /* function */
Variant i_error_hdlr(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x02D70B8F27073E45LL, error_hdlr) {
    return (f_error_hdlr(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug22592_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22592.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug22592_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  LINE(6,x_set_error_handler("error_hdlr"));
  (v_i = 4LL);
  (v_s = "string");
  (v_result = "* *-*");
  LINE(12,x_var_dump(1, v_result));
  v_result.set(6LL, ("*"), 0x26BF47194D7E8E12LL);
  LINE(14,x_var_dump(1, v_result));
  v_result.set(1LL, (v_i), 0x5BCA7C69B794F8CELL);
  LINE(16,x_var_dump(1, v_result));
  v_result.set(3LL, (v_s), 0x135FDDF6A6BFBBDDLL);
  LINE(18,x_var_dump(1, v_result));
  v_result.set(7LL, (0LL), 0x7D75B33B7AEB669DLL);
  LINE(20,x_var_dump(1, v_result));
  (v_a = v_result.set(1LL, (v_result.set(3LL, ("-"), 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL));
  LINE(22,x_var_dump(1, v_result));
  (v_b = v_result.set(3LL, (v_result.set(5LL, (v_s), 0x350AEB726A15D700LL)), 0x135FDDF6A6BFBBDDLL));
  LINE(24,x_var_dump(1, v_result));
  (v_c = v_result.set(0LL, (v_result.set(2LL, (v_result.set(4LL, (v_i), 0x6F2A25235E544A31LL)), 0x486AFCC090D5F98CLL)), 0x77CFA1EEF01BCA90LL));
  LINE(26,x_var_dump(1, v_result));
  (v_d = v_result.set(6LL, (v_result.set(8LL, (5LL), 0x21ABC084C3578135LL)), 0x26BF47194D7E8E12LL));
  LINE(28,x_var_dump(1, v_result));
  (v_e = v_result.set(1LL, (v_result.rvalAt(6LL, 0x26BF47194D7E8E12LL)), 0x5BCA7C69B794F8CELL));
  LINE(30,x_var_dump(1, v_result));
  LINE(31,x_var_dump(5, v_a, Array(ArrayInit(4).set(0, v_b).set(1, v_c).set(2, v_d).set(3, v_e).create())));
  v_result.set(-1LL, ("a"), 0x1F89206E3F8EC794LL);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
