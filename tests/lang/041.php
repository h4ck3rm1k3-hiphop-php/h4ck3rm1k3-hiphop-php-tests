<?php
class A {
    public    static $b = 'foo';
}

$classname       =  'A';
$binaryClassname = b'A';
$wrongClassname  =  'B';

echo $classname::$b."\n";
echo $binaryClassname::$b."\n";
echo $wrongClassname::$b."\n";

?> 
===DONE===
