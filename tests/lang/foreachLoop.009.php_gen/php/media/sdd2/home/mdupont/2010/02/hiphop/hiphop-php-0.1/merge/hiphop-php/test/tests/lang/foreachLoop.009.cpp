
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_refedArray __attribute__((__unused__)) = (variables != gVariables) ? variables->get("refedArray") : g->GV(refedArray);
  Variant &v_ref __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ref") : g->GV(ref);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v1") : g->GV(v1);
  Variant &v_v2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v2") : g->GV(v2);
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);
  Variant &v_v3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v3") : g->GV(v3);
  Variant &v_v4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v4") : g->GV(v4);

  echo("\nRemove elements from a referenced array during loop\n");
  (v_refedArray = ScalarArrays::sa_[0]);
  (v_ref = ref(v_refedArray));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_refedArray.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v1 = iter3->second();
      v_k = iter3->first();
      {
        LINE(9,x_array_pop(ref(v_refedArray)));
        echo(LINE(10,concat5("key: ", toString(v_k), "; value: ", toString(v_v1), "\n")));
      }
    }
  }
  echo("\nRemove elements from a referenced array during loop, using &$value\n");
  (v_refedArray = ScalarArrays::sa_[0]);
  (v_ref = ref(v_refedArray));
  {
    LOOP_COUNTER(4);
    Variant map5 = ref(v_refedArray);
    map5.escalate();
    for (MutableArrayIterPtr iter6 = map5.begin(&v_k, v_v2); iter6->advance();) {
      LOOP_COUNTER_CHECK(4);
      {
        LINE(17,x_array_pop(ref(v_refedArray)));
        echo(LINE(18,concat5("key: ", toString(v_k), "; value: ", toString(v_v2), "\n")));
      }
    }
  }
  echo("\nAdd elements to a referenced array during loop\n");
  (v_refedArray = ScalarArrays::sa_[0]);
  (v_ref = ref(v_refedArray));
  (v_count = 0LL);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_refedArray.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v3 = iter9->second();
      v_k = iter9->first();
      {
        LINE(26,x_array_push(2, ref(v_refedArray), toString("new.") + toString(v_k)));
        echo(LINE(27,concat5("key: ", toString(v_k), "; value: ", toString(v_v3), "\n")));
        if (more(v_count++, 5LL)) {
          echo("Loop detected, as expected.\n");
          break;
        }
      }
    }
  }
  echo("\nAdd elements to a referenced array during loop, using &$value\n");
  (v_refedArray = ScalarArrays::sa_[0]);
  (v_ref = ref(v_refedArray));
  (v_count = 0LL);
  {
    LOOP_COUNTER(10);
    Variant map11 = ref(v_refedArray);
    map11.escalate();
    for (MutableArrayIterPtr iter12 = map11.begin(&v_k, v_v4); iter12->advance();) {
      LOOP_COUNTER_CHECK(10);
      {
        LINE(40,x_array_push(2, ref(v_refedArray), toString("new.") + toString(v_k)));
        echo(LINE(41,concat5("key: ", toString(v_k), "; value: ", toString(v_v4), "\n")));
        if (more(v_count++, 5LL)) {
          echo("Loop detected, as expected.\n");
          break;
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
