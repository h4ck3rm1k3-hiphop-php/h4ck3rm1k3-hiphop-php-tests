
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_europeanmeals(CArrRef params, bool init = true);
Variant cw_europeanmeals$os_get(const char *s);
Variant &cw_europeanmeals$os_lval(const char *s);
Variant cw_europeanmeals$os_constant(const char *s);
Variant cw_europeanmeals$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_englishmealiterator(CArrRef params, bool init = true);
Variant cw_englishmealiterator$os_get(const char *s);
Variant &cw_englishmealiterator$os_lval(const char *s);
Variant cw_englishmealiterator$os_constant(const char *s);
Variant cw_englishmealiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_frenchmealiterator(CArrRef params, bool init = true);
Variant cw_frenchmealiterator$os_get(const char *s);
Variant &cw_frenchmealiterator$os_lval(const char *s);
Variant cw_frenchmealiterator$os_constant(const char *s);
Variant cw_frenchmealiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_CREATE_OBJECT(0x2C7CA5DABCA011B3LL, europeanmeals);
      HASH_CREATE_OBJECT(0x27C0829F5E50228BLL, frenchmealiterator);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x2C7CA5DABCA011B3LL, europeanmeals);
      HASH_INVOKE_STATIC_METHOD(0x27C0829F5E50228BLL, frenchmealiterator);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GET_STATIC_PROPERTY(0x2C7CA5DABCA011B3LL, europeanmeals);
      HASH_GET_STATIC_PROPERTY(0x27C0829F5E50228BLL, frenchmealiterator);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x2C7CA5DABCA011B3LL, europeanmeals);
      HASH_GET_STATIC_PROPERTY_LV(0x27C0829F5E50228BLL, frenchmealiterator);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GET_CLASS_CONSTANT(0x2C7CA5DABCA011B3LL, europeanmeals);
      HASH_GET_CLASS_CONSTANT(0x27C0829F5E50228BLL, frenchmealiterator);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
