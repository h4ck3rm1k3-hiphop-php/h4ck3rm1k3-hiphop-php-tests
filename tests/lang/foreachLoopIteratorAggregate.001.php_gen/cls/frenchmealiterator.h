
#ifndef __GENERATED_cls_frenchmealiterator_h__
#define __GENERATED_cls_frenchmealiterator_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 38 */
class c_frenchmealiterator : virtual public c_iterator {
  BEGIN_CLASS_MAP(frenchmealiterator)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iterator)
  END_CLASS_MAP(frenchmealiterator)
  DECLARE_CLASS(frenchmealiterator, FrenchMealIterator, ObjectData)
  void init();
  public: int64 m_pos;
  public: Array m_myContent;
  public: bool t_valid();
  public: void t_next();
  public: void t_rewind();
  public: Variant t_current();
  public: String t_key();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_frenchmealiterator_h__
