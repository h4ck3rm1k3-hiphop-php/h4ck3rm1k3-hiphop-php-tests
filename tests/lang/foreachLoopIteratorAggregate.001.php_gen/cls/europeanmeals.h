
#ifndef __GENERATED_cls_europeanmeals_h__
#define __GENERATED_cls_europeanmeals_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 75 */
class c_europeanmeals : virtual public c_iteratoraggregate {
  BEGIN_CLASS_MAP(europeanmeals)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iteratoraggregate)
  END_CLASS_MAP(europeanmeals)
  DECLARE_CLASS(europeanmeals, EuropeanMeals, ObjectData)
  void init();
  public: Variant m_storedEnglishMealIterator;
  public: Variant m_storedFrenchMealIterator;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_getiterator();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_europeanmeals_h__
