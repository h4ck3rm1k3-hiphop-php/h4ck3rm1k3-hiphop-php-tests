
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 75 */
Variant c_europeanmeals::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_europeanmeals::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_europeanmeals::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("storedEnglishMealIterator", m_storedEnglishMealIterator.isReferenced() ? ref(m_storedEnglishMealIterator) : m_storedEnglishMealIterator));
  props.push_back(NEW(ArrayElement)("storedFrenchMealIterator", m_storedFrenchMealIterator.isReferenced() ? ref(m_storedFrenchMealIterator) : m_storedFrenchMealIterator));
  c_ObjectData::o_get(props);
}
bool c_europeanmeals::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x52778B157A680D0ELL, storedFrenchMealIterator, 24);
      break;
    case 3:
      HASH_EXISTS_STRING(0x26E144F10F69A3D3LL, storedEnglishMealIterator, 25);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_europeanmeals::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x52778B157A680D0ELL, m_storedFrenchMealIterator,
                         storedFrenchMealIterator, 24);
      break;
    case 3:
      HASH_RETURN_STRING(0x26E144F10F69A3D3LL, m_storedEnglishMealIterator,
                         storedEnglishMealIterator, 25);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_europeanmeals::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x52778B157A680D0ELL, m_storedFrenchMealIterator,
                      storedFrenchMealIterator, 24);
      break;
    case 3:
      HASH_SET_STRING(0x26E144F10F69A3D3LL, m_storedEnglishMealIterator,
                      storedEnglishMealIterator, 25);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_europeanmeals::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x52778B157A680D0ELL, m_storedFrenchMealIterator,
                         storedFrenchMealIterator, 24);
      break;
    case 3:
      HASH_RETURN_STRING(0x26E144F10F69A3D3LL, m_storedEnglishMealIterator,
                         storedEnglishMealIterator, 25);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_europeanmeals::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(europeanmeals)
ObjectData *c_europeanmeals::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_europeanmeals::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_europeanmeals::cloneImpl() {
  c_europeanmeals *obj = NEW(c_europeanmeals)();
  cloneSet(obj);
  return obj;
}
void c_europeanmeals::cloneSet(c_europeanmeals *clone) {
  clone->m_storedEnglishMealIterator = m_storedEnglishMealIterator.isReferenced() ? ref(m_storedEnglishMealIterator) : m_storedEnglishMealIterator;
  clone->m_storedFrenchMealIterator = m_storedFrenchMealIterator.isReferenced() ? ref(m_storedFrenchMealIterator) : m_storedFrenchMealIterator;
  ObjectData::cloneSet(clone);
}
Variant c_europeanmeals::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_europeanmeals::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_europeanmeals::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_europeanmeals$os_get(const char *s) {
  return c_europeanmeals::os_get(s, -1);
}
Variant &cw_europeanmeals$os_lval(const char *s) {
  return c_europeanmeals::os_lval(s, -1);
}
Variant cw_europeanmeals$os_constant(const char *s) {
  return c_europeanmeals::os_constant(s);
}
Variant cw_europeanmeals$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_europeanmeals::os_invoke(c, s, params, -1, fatal);
}
void c_europeanmeals::init() {
  m_storedEnglishMealIterator = null;
  m_storedFrenchMealIterator = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 80 */
void c_europeanmeals::t___construct() {
  INSTANCE_METHOD_INJECTION(EuropeanMeals, EuropeanMeals::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_storedEnglishMealIterator = ((Object)(LINE(81,p_englishmealiterator(p_englishmealiterator(NEWOBJ(c_englishmealiterator)())->create())))));
  (m_storedFrenchMealIterator = ((Object)(LINE(82,p_frenchmealiterator(p_frenchmealiterator(NEWOBJ(c_frenchmealiterator)())->create())))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 85 */
Variant c_europeanmeals::t_getiterator() {
  INSTANCE_METHOD_INJECTION(EuropeanMeals, EuropeanMeals::getIterator);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  Variant &sv_i __attribute__((__unused__)) = g->sv_europeanmeals_DupIdgetiterator_DupIdi.lvalAt(this->o_getClassName());
  Variant &inited_sv_i __attribute__((__unused__)) = g->inited_sv_europeanmeals_DupIdgetiterator_DupIdi.lvalAt(this->o_getClassName());
  echo(concat(toString(gv_indent) + toString("--> "), "EuropeanMeals::getIterator\n"));
  if (!inited_sv_i) {
    (sv_i = 0LL);
    inited_sv_i = true;
  }
  if (equal(modulo(toInt64(sv_i++), 2LL), 0LL)) {
    return m_storedEnglishMealIterator;
  }
  else {
    return m_storedFrenchMealIterator;
  }
  return null;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 2 */
Variant c_englishmealiterator::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_englishmealiterator::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_englishmealiterator::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("pos", m_pos));
  props.push_back(NEW(ArrayElement)("myContent", m_myContent));
  c_ObjectData::o_get(props);
}
bool c_englishmealiterator::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x0CF096B3F7233879LL, myContent, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x277B6D36F75741AELL, pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_englishmealiterator::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x0CF096B3F7233879LL, m_myContent,
                         myContent, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x277B6D36F75741AELL, m_pos,
                         pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_englishmealiterator::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x0CF096B3F7233879LL, m_myContent,
                      myContent, 9);
      break;
    case 2:
      HASH_SET_STRING(0x277B6D36F75741AELL, m_pos,
                      pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_englishmealiterator::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_englishmealiterator::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(englishmealiterator)
ObjectData *c_englishmealiterator::cloneImpl() {
  c_englishmealiterator *obj = NEW(c_englishmealiterator)();
  cloneSet(obj);
  return obj;
}
void c_englishmealiterator::cloneSet(c_englishmealiterator *clone) {
  clone->m_pos = m_pos;
  clone->m_myContent = m_myContent;
  ObjectData::cloneSet(clone);
}
Variant c_englishmealiterator::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_englishmealiterator::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_englishmealiterator::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_englishmealiterator$os_get(const char *s) {
  return c_englishmealiterator::os_get(s, -1);
}
Variant &cw_englishmealiterator$os_lval(const char *s) {
  return c_englishmealiterator::os_lval(s, -1);
}
Variant cw_englishmealiterator$os_constant(const char *s) {
  return c_englishmealiterator::os_constant(s);
}
Variant cw_englishmealiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_englishmealiterator::os_invoke(c, s, params, -1, fatal);
}
void c_englishmealiterator::init() {
  m_pos = 0LL;
  m_myContent = ScalarArrays::sa_[0];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 6 */
bool c_englishmealiterator::t_valid() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::valid);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(8,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::valid", eo_2))));
  return less(m_pos, LINE(9,x_count(m_myContent)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 12 */
void c_englishmealiterator::t_next() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::next);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(14,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::next", eo_2))));
  m_pos++;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 18 */
void c_englishmealiterator::t_rewind() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::rewind);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(20,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::rewind", eo_2))));
  (m_pos = 0LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 24 */
Variant c_englishmealiterator::t_current() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::current);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(26,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::current", eo_2))));
  return m_myContent.rvalAt(m_pos);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 30 */
String c_englishmealiterator::t_key() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::key);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(32,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::key", eo_2))));
  return concat("meal ", toString(m_pos));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 38 */
Variant c_frenchmealiterator::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_frenchmealiterator::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_frenchmealiterator::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("pos", m_pos));
  props.push_back(NEW(ArrayElement)("myContent", m_myContent));
  c_ObjectData::o_get(props);
}
bool c_frenchmealiterator::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x0CF096B3F7233879LL, myContent, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x277B6D36F75741AELL, pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_frenchmealiterator::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x0CF096B3F7233879LL, m_myContent,
                         myContent, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x277B6D36F75741AELL, m_pos,
                         pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_frenchmealiterator::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x0CF096B3F7233879LL, m_myContent,
                      myContent, 9);
      break;
    case 2:
      HASH_SET_STRING(0x277B6D36F75741AELL, m_pos,
                      pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_frenchmealiterator::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_frenchmealiterator::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(frenchmealiterator)
ObjectData *c_frenchmealiterator::cloneImpl() {
  c_frenchmealiterator *obj = NEW(c_frenchmealiterator)();
  cloneSet(obj);
  return obj;
}
void c_frenchmealiterator::cloneSet(c_frenchmealiterator *clone) {
  clone->m_pos = m_pos;
  clone->m_myContent = m_myContent;
  ObjectData::cloneSet(clone);
}
Variant c_frenchmealiterator::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_frenchmealiterator::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_frenchmealiterator::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_frenchmealiterator$os_get(const char *s) {
  return c_frenchmealiterator::os_get(s, -1);
}
Variant &cw_frenchmealiterator$os_lval(const char *s) {
  return c_frenchmealiterator::os_lval(s, -1);
}
Variant cw_frenchmealiterator$os_constant(const char *s) {
  return c_frenchmealiterator::os_constant(s);
}
Variant cw_frenchmealiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_frenchmealiterator::os_invoke(c, s, params, -1, fatal);
}
void c_frenchmealiterator::init() {
  m_pos = 0LL;
  m_myContent = ScalarArrays::sa_[1];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 42 */
bool c_frenchmealiterator::t_valid() {
  INSTANCE_METHOD_INJECTION(FrenchMealIterator, FrenchMealIterator::valid);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(44,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "FrenchMealIterator::valid", eo_2))));
  return less(m_pos, LINE(45,x_count(m_myContent)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 48 */
void c_frenchmealiterator::t_next() {
  INSTANCE_METHOD_INJECTION(FrenchMealIterator, FrenchMealIterator::next);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(50,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "FrenchMealIterator::next", eo_2))));
  m_pos++;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 54 */
void c_frenchmealiterator::t_rewind() {
  INSTANCE_METHOD_INJECTION(FrenchMealIterator, FrenchMealIterator::rewind);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(56,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "FrenchMealIterator::rewind", eo_2))));
  (m_pos = 0LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 60 */
Variant c_frenchmealiterator::t_current() {
  INSTANCE_METHOD_INJECTION(FrenchMealIterator, FrenchMealIterator::current);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(62,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "FrenchMealIterator::current", eo_2))));
  return m_myContent.rvalAt(m_pos);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php line 66 */
String c_frenchmealiterator::t_key() {
  INSTANCE_METHOD_INJECTION(FrenchMealIterator, FrenchMealIterator::key);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(68,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "FrenchMealIterator::key", eo_2))));
  return concat("meal ", toString(m_pos));
} /* function */
Object co_europeanmeals(CArrRef params, bool init /* = true */) {
  return Object(p_europeanmeals(NEW(c_europeanmeals)())->dynCreate(params, init));
}
Object co_englishmealiterator(CArrRef params, bool init /* = true */) {
  return Object(p_englishmealiterator(NEW(c_englishmealiterator)())->dynCreate(params, init));
}
Object co_frenchmealiterator(CArrRef params, bool init /* = true */) {
  return Object(p_frenchmealiterator(NEW(c_frenchmealiterator)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIteratorAggregate_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIteratorAggregate_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_indent __attribute__((__unused__)) = (variables != gVariables) ? variables->get("indent") : g->GV(indent);
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);

  (v_f = ((Object)(LINE(100,p_europeanmeals(p_europeanmeals(NEWOBJ(c_europeanmeals)())->create())))));
  LINE(101,x_var_dump(1, v_f));
  echo("-----( Simple iteration 1: )-----\n");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_f.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        echo(LINE(105,concat4(toString(v_k), " => ", toString(v_v), "\n")));
      }
    }
  }
  echo("-----( Simple iteration 2: )-----\n");
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_f.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        echo(LINE(109,concat4(toString(v_k), " => ", toString(v_v), "\n")));
      }
    }
  }
  (v_indent = " ");
  echo("\n\n\n-----( Nested iteration: )-----\n");
  (v_count = 1LL);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_f.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
      v_k = iter9->first();
      {
        echo(LINE(117,(assignCallTemp(eo_1, toString(v_count++)),concat3("\nTop level ", eo_1, ": \n"))));
        echo(LINE(118,concat4(toString(v_k), " => ", toString(v_v), "\n")));
        (v_indent = "     ");
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = v_f.begin(); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_v = iter12->second();
            v_k = iter12->first();
            {
              echo(LINE(121,concat5("     ", toString(v_k), " => ", toString(v_v), "\n")));
            }
          }
        }
        (v_indent = " ");
      }
    }
  }
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
