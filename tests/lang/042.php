<?php
class A {
    const B = 'foo';
}

$classname       =  'A';
$binaryClassname = b'A';
$wrongClassname  =  'B';

echo $classname::B."\n";
echo $binaryClassname::B."\n";
echo $wrongClassname::B."\n";
?> 
===DONE===
