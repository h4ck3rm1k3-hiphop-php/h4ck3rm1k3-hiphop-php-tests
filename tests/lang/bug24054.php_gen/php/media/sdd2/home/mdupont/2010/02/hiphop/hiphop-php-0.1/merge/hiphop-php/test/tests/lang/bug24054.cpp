
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24054.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24054_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24054.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24054_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(3,g->declareConstant("LONG_MAX", g->k_LONG_MAX, x_is_int(5000000000LL) ? ((Variant)(9223372036854775800.0)) : ((Variant)(2147483647LL))));
  LINE(4,g->declareConstant("LONG_MIN", g->k_LONG_MIN, negate(get_global_variables()->k_LONG_MAX) - 1LL));
  LINE(6,(assignCallTemp(eo_1, LINE(5,x_is_int(get_global_variables()->k_LONG_MIN))),assignCallTemp(eo_2, x_is_int(get_global_variables()->k_LONG_MAX)),assignCallTemp(eo_3, LINE(6,x_is_int(get_global_variables()->k_LONG_MIN - 1LL))),assignCallTemp(eo_4, x_is_int(get_global_variables()->k_LONG_MAX + 1LL)),x_printf(5, "%d,%d,%d,%d\n", Array(ArrayInit(4).set(0, eo_1).set(1, eo_2).set(2, eo_3).set(3, eo_4).create()))));
  (v_i = get_global_variables()->k_LONG_MAX);
  (v_j = v_i * 1001LL);
  v_i *= 1001LL;
  (v_tests = LINE(15,concat4(toString(v_i), " === ", toString(v_j), "")));
  LINE(17,include((concat(x_dirname(get_source_filename("/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24054.php")), "/../quicktester.inc")), false, variables, "/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
