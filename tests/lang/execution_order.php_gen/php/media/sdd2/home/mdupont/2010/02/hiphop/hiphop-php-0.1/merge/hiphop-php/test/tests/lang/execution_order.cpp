
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/execution_order.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/execution_order.php line 147 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x3B32AB004231C30FLL, g->s_c_DupIdstat,
                  stat);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("val", m_val.isReferenced() ? ref(m_val) : m_val));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4056C2E766E0B782LL, val, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4056C2E766E0B782LL, m_val,
                         val, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4056C2E766E0B782LL, m_val,
                      val, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4056C2E766E0B782LL, m_val,
                         val, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_val = m_val.isReferenced() ? ref(m_val) : m_val;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_val = 10LL;
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_c_DupIdstat = 20LL;
}
void csi_c() {
  c_c::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/execution_order.php line 5 */
Variant c_strclass::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x4ECB324C599B87C5LL, g->s_strclass_DupIdstatstr,
                  statstr);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_strclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_strclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("str", m_str.isReferenced() ? ref(m_str) : m_str));
  c_ObjectData::o_get(props);
}
bool c_strclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x362158638F83BD9CLL, str, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_strclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x362158638F83BD9CLL, m_str,
                         str, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_strclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x362158638F83BD9CLL, m_str,
                      str, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_strclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x362158638F83BD9CLL, m_str,
                         str, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_strclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(strclass)
ObjectData *c_strclass::cloneImpl() {
  c_strclass *obj = NEW(c_strclass)();
  cloneSet(obj);
  return obj;
}
void c_strclass::cloneSet(c_strclass *clone) {
  clone->m_str = m_str.isReferenced() ? ref(m_str) : m_str;
  ObjectData::cloneSet(clone);
}
Variant c_strclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_strclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_strclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_strclass$os_get(const char *s) {
  return c_strclass::os_get(s, -1);
}
Variant &cw_strclass$os_lval(const char *s) {
  return c_strclass::os_lval(s, -1);
}
Variant cw_strclass$os_constant(const char *s) {
  return c_strclass::os_constant(s);
}
Variant cw_strclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_strclass::os_invoke(c, s, params, -1, fatal);
}
void c_strclass::init() {
  m_str = "bad";
}
void c_strclass::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_strclass_DupIdstatstr = "bad";
}
void csi_strclass() {
  c_strclass::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/execution_order.php line 51 */
Variant f_foo() {
  FUNCTION_INJECTION(foo);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  (gv_a = "good");
  return gv_a;
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_strclass(CArrRef params, bool init /* = true */) {
  return Object(p_strclass(NEW(c_strclass)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$execution_order_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/execution_order.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$execution_order_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_a = "bad");
  (v_b = "good");
  echo("1)");
  (v_c = concat(toString(v_a), (toString((v_a = v_b)))));
  echo(toString(v_c));
  echo("\r\n");
  (v_a = "bad");
  (v_b = "good");
  (v_c = concat((toString((v_a = v_b))), toString(v_a)));
  echo("2)");
  echo(toString(v_c));
  echo("\r\n");
  (v_str = ((Object)(LINE(25,p_strclass(p_strclass(NEWOBJ(c_strclass)())->create())))));
  (v_c = concat(toString(v_str.o_get("str", 0x362158638F83BD9CLL)), ((v_str.o_lval("str", 0x362158638F83BD9CLL) = "good"))));
  echo("3)");
  echo(toString(v_c));
  echo("\r\n");
  (v_str.o_lval("str", 0x362158638F83BD9CLL) = "bad");
  (v_c = concat(((v_str.o_lval("str", 0x362158638F83BD9CLL) = "good")), toString(v_str.o_get("str", 0x362158638F83BD9CLL))));
  echo("4)");
  echo(toString(v_c));
  echo("\r\n");
  (v_c = concat(g->s_strclass_DupIdstatstr, ((g->s_strclass_DupIdstatstr = "good"))));
  echo("5)");
  echo(toString(v_c));
  echo("\r\n");
  (g->s_strclass_DupIdstatstr = "bad");
  (v_c = concat(((g->s_strclass_DupIdstatstr = "good")), g->s_strclass_DupIdstatstr));
  echo("6)");
  echo(toString(v_c));
  echo("\r\n");
  (v_a = "bad");
  echo("7)");
  echo(concat(toString(LINE(60,f_foo())), toString(v_a)));
  echo("\r\n");
  (v_a = "bad");
  echo("8)");
  echo(concat(toString(v_a), toString(LINE(65,f_foo()))));
  echo("\r\n");
  (v_x = 1LL);
  (v_z = v_x - (v_x++));
  echo("9)");
  echo(toString(v_z));
  echo("\r\n");
  (v_x = 1LL);
  (v_z = (v_x++) - v_x);
  echo("10)");
  echo(toString(v_z));
  echo("\r\n");
  (v_x = 1LL);
  (v_z = v_x - (++v_x));
  echo("11)");
  echo(toString(v_z));
  echo("\r\n");
  (v_x = 1LL);
  (v_z = (++v_x) - v_x);
  echo("12)");
  echo(toString(v_z));
  echo("\r\n");
  (v_x = 1LL);
  (v_y = 3LL);
  (v_z = v_x - ((v_x = v_y)));
  echo("13)");
  echo(toString(v_z));
  echo("\r\n");
  (v_x = 1LL);
  (v_y = 3LL);
  (v_z = ((v_x = v_y)) - v_x);
  echo("14)");
  echo(toString(v_z));
  echo("\r\n");
  (v_a = 100LL);
  (v_b = 200LL);
  echo("15)");
  echo(toString(v_a + ((v_a = v_b))));
  echo("\r\n");
  (v_a = 100LL);
  (v_b = 200LL);
  echo("16)");
  echo(toString(((v_a = v_b)) + v_a));
  echo("\r\n");
  (v_a = ScalarArrays::sa_[0]);
  (v_i = 0LL);
  echo("17)");
  echo(toString(plus_rev(v_a.rvalAt(v_i++), v_a.rvalAt(v_i++))));
  echo("\r\n");
  (v_i = -1LL);
  echo("18)");
  echo(toString(plus_rev(v_a.rvalAt(++v_i), v_a.rvalAt(++v_i))));
  echo("\r\n");
  (v_i = 0LL);
  echo("19)");
  echo(toString(v_a.rvalAt(v_i) + (v_a.set(v_i, (400LL)))));
  echo("\r\n");
  v_a.set(0LL, (100LL), 0x77CFA1EEF01BCA90LL);
  echo("20)");
  echo(toString((v_a.set(v_i, (400LL))) + v_a.rvalAt(v_i)));
  echo("\r\n");
  echo("21)");
  echo(toString(g->s_c_DupIdstat + ((g->s_c_DupIdstat = 200LL))));
  echo("\r\n");
  echo("22)");
  echo(toString(((g->s_c_DupIdstat = 300LL)) + g->s_c_DupIdstat));
  echo("\r\n");
  (v_c = ((Object)(LINE(160,p_c(p_c(NEWOBJ(c_c)())->create())))));
  echo("23)");
  echo(toString(v_c.o_get("val", 0x4056C2E766E0B782LL) + ((v_c.o_lval("val", 0x4056C2E766E0B782LL) = 200LL))));
  echo("\r\n");
  echo("24)");
  echo(toString(((v_c.o_lval("val", 0x4056C2E766E0B782LL) = 300LL)) + v_c.o_get("val", 0x4056C2E766E0B782LL)));
  echo("\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
