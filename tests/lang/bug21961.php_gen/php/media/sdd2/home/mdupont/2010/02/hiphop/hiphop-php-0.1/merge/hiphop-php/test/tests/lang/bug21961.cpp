
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 3 */
Variant c_man::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_man::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_man::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  props.push_back(NEW(ArrayElement)("bars", m_bars.isReferenced() ? ref(m_bars) : m_bars));
  c_ObjectData::o_get(props);
}
bool c_man::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      HASH_EXISTS_STRING(0x28FE6B2BB9755E3CLL, bars, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_man::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      HASH_RETURN_STRING(0x28FE6B2BB9755E3CLL, m_bars,
                         bars, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_man::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      HASH_SET_STRING(0x28FE6B2BB9755E3CLL, m_bars,
                      bars, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_man::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      HASH_RETURN_STRING(0x28FE6B2BB9755E3CLL, m_bars,
                         bars, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_man::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(man)
ObjectData *c_man::create() {
  init();
  t_man();
  return this;
}
ObjectData *c_man::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_man::cloneImpl() {
  c_man *obj = NEW(c_man)();
  cloneSet(obj);
  return obj;
}
void c_man::cloneSet(c_man *clone) {
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  clone->m_bars = m_bars.isReferenced() ? ref(m_bars) : m_bars;
  ObjectData::cloneSet(clone);
}
Variant c_man::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 2:
      HASH_GUARD(0x04A19EB519A4C6C2LL, getdrunk) {
        return (t_getdrunk(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_man::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 2:
      HASH_GUARD(0x04A19EB519A4C6C2LL, getdrunk) {
        return (t_getdrunk(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_man::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_man$os_get(const char *s) {
  return c_man::os_get(s, -1);
}
Variant &cw_man$os_lval(const char *s) {
  return c_man::os_lval(s, -1);
}
Variant cw_man$os_constant(const char *s) {
  return c_man::os_constant(s);
}
Variant cw_man$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_man::os_invoke(c, s, params, -1, fatal);
}
void c_man::init() {
  m_name = null;
  m_bars = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 6 */
void c_man::t_man() {
  INSTANCE_METHOD_INJECTION(man, man::man);
  bool oldInCtor = gasInCtor(true);
  (m_name = "Mr. X");
  (m_bars = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 12 */
void c_man::t_getdrunk(CVarRef v_where) {
  INSTANCE_METHOD_INJECTION(man, man::getdrunk);
  m_bars.append((((Object)(LINE(14,p_bar(p_bar(NEWOBJ(c_bar)())->create(v_where)))))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 17 */
Variant c_man::t_getname() {
  INSTANCE_METHOD_INJECTION(man, man::getName);
  return m_name;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 23 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_man::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_man::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  c_man::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_man::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_man::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_man::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_man::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_man::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::create(CVarRef v_w) {
  init();
  t_bar(v_w);
  return this;
}
ObjectData *c_bar::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  c_man::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x04A19EB519A4C6C2LL, getdrunk) {
        return (t_getdrunk(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x477C0B50B84ED935LL, whosdrunk) {
        return (t_whosdrunk());
      }
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    default:
      break;
  }
  return c_man::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x04A19EB519A4C6C2LL, getdrunk) {
        return (t_getdrunk(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x477C0B50B84ED935LL, whosdrunk) {
        return (t_whosdrunk());
      }
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    default:
      break;
  }
  return c_man::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_man::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
  c_man::init();
  m_name = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 27 */
void c_bar::t_bar(CVarRef v_w) {
  INSTANCE_METHOD_INJECTION(bar, bar::bar);
  bool oldInCtor = gasInCtor(true);
  (m_name = v_w);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 32 */
Variant c_bar::t_getname() {
  INSTANCE_METHOD_INJECTION(bar, bar::getName);
  return m_name;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 37 */
String c_bar::t_whosdrunk() {
  INSTANCE_METHOD_INJECTION(bar, bar::whosdrunk);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_who;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_who;
    VariableTable(Variant &r_who) : v_who(r_who) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 0:
          HASH_RETURN(0x7BDF07E79BB2A200LL, v_who,
                      who);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_who);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_who = LINE(39,x_get_parent_class(((Object)(this)))));
  if (equal(v_who, null)) {
    return "nobody";
  }
  return f_eval(LINE(44,concat3("return ", toString(v_who), "::getName();")));
} /* function */
Object co_man(CArrRef params, bool init /* = true */) {
  return Object(p_man(NEW(c_man)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug21961_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug21961_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = ((Object)(LINE(48,p_man(p_man(NEWOBJ(c_man)())->create())))));
  LINE(49,v_x.o_invoke_few_args("getdrunk", 0x04A19EB519A4C6C2LL, 1, "The old Tavern"));
  LINE(50,x_var_dump(1, v_x.o_get("bars", 0x28FE6B2BB9755E3CLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_invoke_few_args("whosdrunk", 0x477C0B50B84ED935LL, 0)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
