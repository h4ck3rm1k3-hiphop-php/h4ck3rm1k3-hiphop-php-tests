
#ifndef __GENERATED_cls_man_h__
#define __GENERATED_cls_man_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 3 */
class c_man : virtual public ObjectData {
  BEGIN_CLASS_MAP(man)
  END_CLASS_MAP(man)
  DECLARE_CLASS(man, man, ObjectData)
  void init();
  public: Variant m_name;
  public: Variant m_bars;
  public: void t_man();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_getdrunk(CVarRef v_where);
  public: Variant t_getname();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_man_h__
