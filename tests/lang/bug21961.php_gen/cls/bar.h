
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__

#include <cls/man.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug21961.php line 23 */
class c_bar : virtual public c_man {
  BEGIN_CLASS_MAP(bar)
    PARENT_CLASS(man)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, bar, man)
  void init();
  public: void t_bar(CVarRef v_w);
  public: ObjectData *create(CVarRef v_w);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_getname();
  public: String t_whosdrunk();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
