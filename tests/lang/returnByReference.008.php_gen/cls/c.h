
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.php line 2 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: int64 t_returnconstantbyvalue();
  public: Variant t_returnconstantbyref();
  public: Variant t_returnvariablebyref();
  public: Variant t_returnfunctioncallbyref(CVarRef v_functionToCall);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
