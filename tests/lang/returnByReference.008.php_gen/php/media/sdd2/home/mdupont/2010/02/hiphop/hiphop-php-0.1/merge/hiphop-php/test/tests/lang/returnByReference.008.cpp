
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 4:
      HASH_GUARD(0x7495A26E643CE04CLL, returnfunctioncallbyref) {
        return ref(t_returnfunctioncallbyref(params.rvalAt(0)));
      }
      break;
    case 5:
      HASH_GUARD(0x4383C2CCCF1034CDLL, returnconstantbyref) {
        return ref(t_returnconstantbyref());
      }
      HASH_GUARD(0x20C0DB8C2C250C4DLL, returnvariablebyref) {
        return ref(t_returnvariablebyref());
      }
      HASH_GUARD(0x401553DF8461B185LL, returnconstantbyvalue) {
        return (t_returnconstantbyvalue());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 4:
      HASH_GUARD(0x7495A26E643CE04CLL, returnfunctioncallbyref) {
        return ref(t_returnfunctioncallbyref(a0));
      }
      break;
    case 5:
      HASH_GUARD(0x4383C2CCCF1034CDLL, returnconstantbyref) {
        return ref(t_returnconstantbyref());
      }
      HASH_GUARD(0x20C0DB8C2C250C4DLL, returnvariablebyref) {
        return ref(t_returnvariablebyref());
      }
      HASH_GUARD(0x401553DF8461B185LL, returnconstantbyvalue) {
        return (t_returnconstantbyvalue());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.php line 3 */
int64 c_c::t_returnconstantbyvalue() {
  INSTANCE_METHOD_INJECTION(C, C::returnConstantByValue);
  return 100LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.php line 7 */
Variant c_c::t_returnconstantbyref() {
  INSTANCE_METHOD_INJECTION(C, C::returnConstantByRef);
  return 100LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.php line 11 */
Variant c_c::t_returnvariablebyref() {
  INSTANCE_METHOD_INJECTION(C, C::returnVariableByRef);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(a)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.php line 15 */
Variant c_c::t_returnfunctioncallbyref(CVarRef v_functionToCall) {
  INSTANCE_METHOD_INJECTION(C, C::returnFunctionCallByRef);
  return ref(LINE(16,o_root_invoke_few_args(toString(v_functionToCall), -1LL, 0)));
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_008_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.008.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_008_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_c = ((Object)(LINE(19,p_c(p_c(NEWOBJ(c_c)())->create())))));
  echo("\n---> 1. Via a return by ref function call, assign by reference the return value of a function that returns by value:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(24,v_c.o_invoke_few_args("returnFunctionCallByRef", 0x7495A26E643CE04CLL, 1, "returnConstantByValue"))));
  v_a++;
  LINE(26,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 2. Via a return by ref function call, assign by reference the return value of a function that returns a constant by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(31,v_c.o_invoke_few_args("returnFunctionCallByRef", 0x7495A26E643CE04CLL, 1, "returnConstantByRef"))));
  v_a++;
  LINE(33,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 3. Via a return by ref function call, assign by reference the return value of a function that returns by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(38,v_c.o_invoke_few_args("returnFunctionCallByRef", 0x7495A26E643CE04CLL, 1, "returnVariableByRef"))));
  v_a++;
  LINE(40,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
