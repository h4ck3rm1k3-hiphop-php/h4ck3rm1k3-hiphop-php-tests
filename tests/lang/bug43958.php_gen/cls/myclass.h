
#ifndef __GENERATED_cls_myclass_h__
#define __GENERATED_cls_myclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug43958.php line 2 */
class c_myclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass)
  END_CLASS_MAP(myclass)
  DECLARE_CLASS(myclass, MyClass, ObjectData)
  void init();
  public: static bool ti_loadcode(const char* cls, Variant v_p);
  public: static bool t_loadcode(CVarRef v_p) { return ti_loadcode("myclass", v_p); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass_h__
