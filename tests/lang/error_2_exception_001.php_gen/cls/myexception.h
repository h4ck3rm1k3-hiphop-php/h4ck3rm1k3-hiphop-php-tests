
#ifndef __GENERATED_cls_myexception_h__
#define __GENERATED_cls_myexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/error_2_exception_001.php line 3 */
class c_myexception : virtual public c_exception {
  BEGIN_CLASS_MAP(myexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(myexception)
  DECLARE_CLASS(myexception, MyException, exception)
  void init();
  public: void t_myexception(CVarRef v__errno, CVarRef v__errmsg);
  public: ObjectData *create(CVarRef v__errno, CVarRef v__errmsg);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_geterrno();
  public: Variant t_geterrmsg();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myexception_h__
