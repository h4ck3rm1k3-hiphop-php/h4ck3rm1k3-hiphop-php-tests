
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24499.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24499.php line 2 */
Variant c_id::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_id::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_id::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("id", m_id.isReferenced() ? ref(m_id) : m_id));
  c_ObjectData::o_get(props);
}
bool c_id::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x028B9FE0C4522BE2LL, id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_id::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_id::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x028B9FE0C4522BE2LL, m_id,
                      id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_id::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_id::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(id)
ObjectData *c_id::cloneImpl() {
  c_id *obj = NEW(c_id)();
  cloneSet(obj);
  return obj;
}
void c_id::cloneSet(c_id *clone) {
  clone->m_id = m_id.isReferenced() ? ref(m_id) : m_id;
  ObjectData::cloneSet(clone);
}
Variant c_id::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x029A69A9C2136316LL, tester) {
        return (t_tester(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_id::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x029A69A9C2136316LL, tester) {
        return (t_tester(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_id::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_id$os_get(const char *s) {
  return c_id::os_get(s, -1);
}
Variant &cw_id$os_lval(const char *s) {
  return c_id::os_lval(s, -1);
}
Variant cw_id$os_constant(const char *s) {
  return c_id::os_constant(s);
}
Variant cw_id$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_id::os_invoke(c, s, params, -1, fatal);
}
void c_id::init() {
  m_id = "priv";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24499.php line 5 */
void c_id::t_tester(Variant v_obj) {
  INSTANCE_METHOD_INJECTION(Id, Id::tester);
  (v_obj.o_lval("id", 0x028B9FE0C4522BE2LL) = "bar");
} /* function */
Object co_id(CArrRef params, bool init /* = true */) {
  return Object(p_id(NEW(c_id)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24499_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24499.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24499_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_id __attribute__((__unused__)) = (variables != gVariables) ? variables->get("id") : g->GV(id);
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  (v_id = ((Object)(LINE(11,p_id(p_id(NEWOBJ(c_id)())->create())))));
  (silenceInc(), silenceDec((v_obj.o_lval("foo", 0x4154FA2EF733DA8FLL) = "bar")));
  LINE(13,v_id.o_invoke_few_args("tester", 0x029A69A9C2136316LL, 1, v_obj));
  LINE(14,x_print_r(v_obj));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
