
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug23279.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug23279.php line 7 */
void f_redirect_on_error(CVarRef v_e) {
  FUNCTION_INJECTION(redirect_on_error);
  LINE(8,x_ob_end_clean());
  echo("Goodbye Cruel World\n");
} /* function */
Variant i_redirect_on_error(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1E2F0B2116A1C814LL, redirect_on_error) {
    return (f_redirect_on_error(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug23279_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug23279.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug23279_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_ob_start());
  LINE(3,x_set_exception_handler("redirect_on_error"));
  echo("Hello World\n");
  throw_exception(((Object)(LINE(5,p_exception(p_exception(NEWOBJ(c_exception)())->create())))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
