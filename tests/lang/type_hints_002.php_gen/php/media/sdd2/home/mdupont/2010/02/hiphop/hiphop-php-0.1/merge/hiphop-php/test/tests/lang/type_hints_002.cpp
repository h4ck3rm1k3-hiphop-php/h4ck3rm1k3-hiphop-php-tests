
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/type_hints_002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/type_hints_002.php line 2 */
Variant c_p::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_p::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_p::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_p::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_p::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_p::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_p::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_p::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(p)
ObjectData *c_p::cloneImpl() {
  c_p *obj = NEW(c_p)();
  cloneSet(obj);
  return obj;
}
void c_p::cloneSet(c_p *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_p::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_p::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_p::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_p$os_get(const char *s) {
  return c_p::os_get(s, -1);
}
Variant &cw_p$os_lval(const char *s) {
  return c_p::os_lval(s, -1);
}
Variant cw_p$os_constant(const char *s) {
  return c_p::os_constant(s);
}
Variant cw_p$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_p::os_invoke(c, s, params, -1, fatal);
}
void c_p::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/type_hints_002.php line 3 */
Variant c_t::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_t::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_t::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_t::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_t::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_t::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_t::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_t::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(t)
ObjectData *c_t::cloneImpl() {
  c_t *obj = NEW(c_t)();
  cloneSet(obj);
  return obj;
}
void c_t::cloneSet(c_t *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_t::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        int count = params.size();
        if (count <= 0) return (t_f(), null);
        return (t_f(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_t::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        if (count <= 0) return (t_f(), null);
        return (t_f(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_t::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_t$os_get(const char *s) {
  return c_t::os_get(s, -1);
}
Variant &cw_t$os_lval(const char *s) {
  return c_t::os_lval(s, -1);
}
Variant cw_t$os_constant(const char *s) {
  return c_t::os_constant(s);
}
Variant cw_t$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_t::os_invoke(c, s, params, -1, fatal);
}
void c_t::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/type_hints_002.php line 4 */
void c_t::t_f(CVarRef v_p //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(T, T::f);
  LINE(5,x_var_dump(1, v_p));
  echo("-\n");
} /* function */
Object co_p(CArrRef params, bool init /* = true */) {
  return Object(p_p(NEW(c_p)())->dynCreate(params, init));
}
Object co_t(CArrRef params, bool init /* = true */) {
  return Object(p_t(NEW(c_t)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$type_hints_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/type_hints_002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$type_hints_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);

  (v_o = ((Object)(LINE(10,p_t(p_t(NEWOBJ(c_t)())->create())))));
  LINE(11,v_o.o_invoke_few_args("f", 0x286CE1C477560280LL, 1, p_p(p_p(NEWOBJ(c_p)())->create())));
  LINE(12,v_o.o_invoke_few_args("f", 0x286CE1C477560280LL, 0));
  LINE(13,v_o.o_invoke_few_args("f", 0x286CE1C477560280LL, 1, null));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
