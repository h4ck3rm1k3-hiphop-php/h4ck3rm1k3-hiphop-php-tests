
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_003.php line 2 */
void f_passbyval(CVarRef v_val) {
  FUNCTION_INJECTION(passbyVal);
  echo("\nInside passbyVal call:\n");
  LINE(4,x_var_dump(1, v_val));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_003.php line 7 */
void f_passbyref(Variant v_ref) {
  FUNCTION_INJECTION(passbyRef);
  echo("\nInside passbyRef call:\n");
  LINE(9,x_var_dump(1, v_ref));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_undef1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("undef1") : g->GV(undef1);
  Variant &v_undef2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("undef2") : g->GV(undef2);

  echo("\nPassing undefined by value\n");
  LINE(13,f_passbyval(v_undef1.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  echo("\nAfter call\n");
  LINE(15,x_var_dump(1, v_undef1));
  echo("\nPassing undefined by reference\n");
  LINE(18,f_passbyref(ref(lval(v_undef2.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  echo("\nAfter call\n");
  LINE(20,x_var_dump(1, v_undef2));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
