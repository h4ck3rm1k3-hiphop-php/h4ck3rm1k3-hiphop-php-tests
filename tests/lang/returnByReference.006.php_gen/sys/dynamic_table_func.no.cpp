
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_returnconstantbyref(CArrRef params);
Variant i_returnvariablebyref(CArrRef params);
Variant i_returnconstantbyvalue(CArrRef params);
static Variant invoke_case_5(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x4383C2CCCF1034CDLL, returnconstantbyref);
  HASH_INVOKE(0x20C0DB8C2C250C4DLL, returnvariablebyref);
  HASH_INVOKE(0x401553DF8461B185LL, returnconstantbyvalue);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[8])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 8; i++) funcTable[i] = &invoke_builtin;
    funcTable[5] = &invoke_case_5;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 7](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
