
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.006.php line 6 */
Variant f_returnconstantbyref() {
  FUNCTION_INJECTION(returnConstantByRef);
  return 100LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.006.php line 10 */
Variant f_returnvariablebyref() {
  FUNCTION_INJECTION(returnVariableByRef);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(a)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.006.php line 2 */
int64 f_returnconstantbyvalue() {
  FUNCTION_INJECTION(returnConstantByValue);
  return 100LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.006.php line 14 */
Variant f_returnfunctioncallbyref(CStrRef v_functionToCall) {
  FUNCTION_INJECTION(returnFunctionCallByRef);
  return ref(LINE(15,invoke(v_functionToCall, Array(), -1)));
} /* function */
Variant i_returnconstantbyref(CArrRef params) {
  return ref(f_returnconstantbyref());
}
Variant i_returnvariablebyref(CArrRef params) {
  return ref(f_returnvariablebyref());
}
Variant i_returnconstantbyvalue(CArrRef params) {
  return (f_returnconstantbyvalue());
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("\n---> 1. Via a return by ref function call, assign by reference the return value of a function that returns by value:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(21,f_returnfunctioncallbyref("returnConstantByValue"))));
  v_a++;
  LINE(23,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 2. Via a return by ref function call, assign by reference the return value of a function that returns a constant by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(28,f_returnfunctioncallbyref("returnConstantByRef"))));
  v_a++;
  LINE(30,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 3. Via a return by ref function call, assign by reference the return value of a function that returns by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(35,f_returnfunctioncallbyref("returnVariableByRef"))));
  v_a++;
  LINE(37,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
