
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_001.php line 40 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 7) {
    case 2:
      HASH_RETURN(0x4292CEE227B9150ALL, g->s_c_DupIda,
                  a);
      HASH_RETURN(0x2027864469AD4382LL, g->s_c_DupIdstring,
                  string);
      break;
    case 4:
      HASH_RETURN(0x0BCDB293DC3CBDDCLL, g->s_c_DupIdname,
                  name);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 7) {
    case 2:
      HASH_RETURN(0x4292CEE227B9150ALL, g->s_c_DupIda,
                  a);
      HASH_RETURN(0x2027864469AD4382LL, g->s_c_DupIdstring,
                  string);
      break;
    case 4:
      HASH_RETURN(0x0BCDB293DC3CBDDCLL, g->s_c_DupIdname,
                  name);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_c_DupIdname = "original";
  g->s_c_DupIda = ScalarArrays::sa_[0];
  g->s_c_DupIdstring = "hello";
}
void csi_c() {
  c_c::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_001.php line 55 */
int64 f_getoffset() {
  FUNCTION_INJECTION(getOffset);
  echo("in getOffset()\n");
  return 0LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_001.php line 3 */
String f_f() {
  FUNCTION_INJECTION(f);
  echo("in f()\n");
  return "name";
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_001.php line 8 */
String f_g() {
  FUNCTION_INJECTION(g);
  echo("in g()\n");
  return "assigned value";
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_001.php line 59 */
String f_newchar() {
  FUNCTION_INJECTION(newChar);
  echo("in newChar()\n");
  return "j";
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_oa __attribute__((__unused__)) = (variables != gVariables) ? variables->get("oa") : g->GV(oa);
  Variant &v_ob __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ob") : g->GV(ob);
  Variant &v_oc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("oc") : g->GV(oc);
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);

  echo("\n\nOrder with local assignment:\n");
  (variables->get(LINE(15,f_f())) = f_g());
  LINE(16,x_var_dump(1, v_name));
  echo("\n\nOrder with array assignment:\n");
  v_a.set(LINE(19,f_f()), (f_g()));
  LINE(20,x_var_dump(1, v_a));
  echo("\n\nOrder with object property assignment:\n");
  (v_oa = ((Object)(LINE(23,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (v_oa.o_lval(toString(variables->get(LINE(24,f_f()))), -1LL) = f_g());
  LINE(25,x_var_dump(1, v_oa));
  echo("\n\nOrder with nested object property assignment:\n");
  (v_ob = ((Object)(LINE(28,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (v_ob.o_lval("o1", 0x4877B8D09F78456BLL) = ((Object)(LINE(29,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (lval(v_ob.o_lval("o1", 0x4877B8D09F78456BLL)).o_lval("o2", 0x55D1F13047B68DAALL) = ((Object)(LINE(30,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (lval(lval(v_ob.o_lval("o1", 0x4877B8D09F78456BLL)).o_lval("o2", 0x55D1F13047B68DAALL)).o_lval(toString(variables->get(LINE(31,f_f()))), -1LL) = f_g());
  LINE(32,x_var_dump(1, v_ob));
  echo("\n\nOrder with dim_list property assignment:\n");
  (v_oc = ((Object)(LINE(35,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  lval(v_oc.o_lval("a", 0x4292CEE227B9150ALL)).set(variables->get(LINE(36,f_f())), (f_g()));
  LINE(37,x_var_dump(1, v_oc));
  echo("\n\nOrder with static property assignment:\n");
  (c_c::os_lval(LINE(46,f_f()), -1) = f_g());
  LINE(47,x_var_dump(1, g->s_c_DupIdname));
  echo("\n\nOrder with static array property assignment:\n");
  g->s_c_DupIda.set(LINE(50,f_f()), (f_g()));
  LINE(51,x_var_dump(1, g->s_c_DupIda));
  echo("\n\nOrder with indexed string assignment:\n");
  (v_string = "hello");
  v_string.set(LINE(63,f_getoffset()), (f_newchar()));
  LINE(64,x_var_dump(1, v_string));
  echo("\n\nOrder with static string property assignment:\n");
  g->s_c_DupIdstring.set(LINE(67,f_getoffset()), (f_newchar()));
  LINE(68,x_var_dump(1, g->s_c_DupIdstring));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
