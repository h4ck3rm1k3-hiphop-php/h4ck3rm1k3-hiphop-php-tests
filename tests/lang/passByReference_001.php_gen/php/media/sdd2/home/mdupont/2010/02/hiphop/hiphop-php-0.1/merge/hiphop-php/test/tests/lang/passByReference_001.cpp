
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_001.php line 2 */
void f_f(Variant v_arg1, Variant v_arg2) {
  FUNCTION_INJECTION(f);
  LINE(4,x_var_dump(1, v_arg1++));
  LINE(5,x_var_dump(1, v_arg2++));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_001.php line 8 */
void f_g(Variant v_arg1, Variant v_arg2) {
  FUNCTION_INJECTION(g);
  LINE(10,x_var_dump(1, v_arg1));
  LINE(11,x_var_dump(1, v_arg2));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_a = 7LL);
  (v_b = 15LL);
  LINE(16,f_f(v_a, ref(v_b)));
  LINE(18,x_var_dump(1, v_a));
  LINE(19,x_var_dump(1, v_b));
  (v_c = ScalarArrays::sa_[0]);
  LINE(22,f_g(ref(v_c), ref(lval(v_c.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
