
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24951.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24951.php line 6 */
void f_t1() {
  FUNCTION_INJECTION(t1);
  LINE(8,x_ob_start("test"));
  echo("Hello from t1 1 ");
  echo("Hello from t1 2 ");
  LINE(11,x_ob_end_flush());
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24951.php line 13 */
void f_t2() {
  FUNCTION_INJECTION(t2);
  LINE(15,x_ob_start("test"));
  echo("Hello from t2 1 ");
  LINE(17,x_ob_flush());
  echo("Hello from t2 2 ");
  LINE(19,x_ob_end_flush());
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24951.php line 21 */
void f_t3() {
  FUNCTION_INJECTION(t3);
  LINE(23,x_ob_start("test"));
  echo("Hello from t3 1 ");
  LINE(25,x_ob_clean());
  echo("Hello from t3 2 ");
  LINE(27,x_ob_end_flush());
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24951.php line 2 */
String f_test(CVarRef v_s, CVarRef v_mode) {
  FUNCTION_INJECTION(test);
  return LINE(4,concat3((toString((toBoolean(bitwise_and(v_mode, 1LL /* PHP_OUTPUT_HANDLER_START */))) ? (("[")) : (("")))), toString(v_s), (toString((toBoolean(bitwise_and(v_mode, 4LL /* PHP_OUTPUT_HANDLER_END */))) ? (("]\n")) : ((""))))));
} /* function */
Variant i_test(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x37349B25A0ED29E7LL, test) {
    return (f_test(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24951_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24951.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24951_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(30,f_t1());
  echo("\n");
  LINE(31,f_t2());
  echo("\n");
  LINE(32,f_t3());
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
