
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_typehint_ref(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_typehint(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_no_typehint_ref(CArrRef params);
Variant i_no_typehint(CArrRef params);
static Variant invoke_case_3(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x6C6CFDD8BE13418BLL, no_typehint_ref);
  HASH_INVOKE(0x2E85320BDB41A8D3LL, no_typehint);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[8])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 8; i++) funcTable[i] = &invoke_builtin;
    funcTable[0] = &i_typehint_ref;
    funcTable[2] = &i_typehint;
    funcTable[3] = &invoke_case_3;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 7](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
