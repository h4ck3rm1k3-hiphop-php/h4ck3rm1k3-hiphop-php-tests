
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24658.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24658.php line 2 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24658.php line 12 */
void f_typehint_ref(Variant v_a) {
  FUNCTION_INJECTION(typehint_ref);
  LINE(13,x_var_dump(1, v_a));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24658.php line 9 */
void f_no_typehint_ref(Variant v_a) {
  FUNCTION_INJECTION(no_typehint_ref);
  LINE(10,x_var_dump(1, v_a));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24658.php line 3 */
void f_no_typehint(CVarRef v_a) {
  FUNCTION_INJECTION(no_typehint);
  LINE(4,x_var_dump(1, v_a));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24658.php line 6 */
void f_typehint(CVarRef v_a) {
  FUNCTION_INJECTION(typehint);
  LINE(7,x_var_dump(1, v_a));
} /* function */
Variant i_typehint_ref(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2F24BECBD09B0060LL, typehint_ref) {
    return (f_typehint_ref(ref(const_cast<Array&>(params).lvalAt(0))), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_no_typehint_ref(CArrRef params) {
  return (f_no_typehint_ref(ref(const_cast<Array&>(params).lvalAt(0))), null);
}
Variant i_no_typehint(CArrRef params) {
  return (f_no_typehint(params.rvalAt(0)), null);
}
Variant i_typehint(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6844D3A6343BAC4ALL, typehint) {
    return (f_typehint(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24658_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24658.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24658_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_v = ((Object)(LINE(15,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  (v_a = (assignCallTemp(eo_0, ((Object)(LINE(16,p_foo(p_foo(NEWOBJ(c_foo)())->create()))))),Array(ArrayInit(3).set(0, eo_0).set(1, 1LL).set(2, 2LL).create())));
  LINE(17,f_no_typehint(v_v));
  LINE(18,f_typehint(v_v));
  LINE(19,f_no_typehint_ref(ref(v_v)));
  LINE(20,f_typehint_ref(ref(v_v)));
  echo("===no_typehint===\n");
  LINE(22,x_array_walk(ref(v_a), "no_typehint"));
  echo("===no_typehint_ref===\n");
  LINE(24,x_array_walk(ref(v_a), "no_typehint_ref"));
  echo("===typehint===\n");
  LINE(26,x_array_walk(ref(v_a), "typehint"));
  echo("===typehint_ref===\n");
  LINE(28,x_array_walk(ref(v_a), "typehint_ref"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
