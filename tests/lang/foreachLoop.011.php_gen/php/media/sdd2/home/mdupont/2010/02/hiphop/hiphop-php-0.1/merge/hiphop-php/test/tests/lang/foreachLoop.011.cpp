
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.011.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_011_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.011.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_011_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("\nChange from array to non iterable:\n");
  (v_a = ScalarArrays::sa_[0]);
  (v_b = ref(v_a));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        LINE(6,x_var_dump(1, v_v));
        (v_b = 1LL);
      }
    }
  }
  echo("\nChange from object to non iterable:\n");
  (v_a = ((Object)(LINE(11,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (v_a.o_lval("a", 0x4292CEE227B9150ALL) = 1LL);
  (v_a.o_lval("b", 0x08FBB133F8576BD5LL) = 2LL);
  (v_b = ref(v_a));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      {
        LINE(16,x_var_dump(1, v_v));
        (v_b = "x");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
