
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(f)
    GVS(a)
    GVS(b)
    GVS(c)
    GVS(d)
    GVS(i)
    GVS(e)
    GVS(g1)
    GVS(g2)
    GVS(g3)
    GVS(g)
    GVS(i1)
    GVS(i2)
    GVS(i3)
    GVS(i4)
    GVS(j)
    GVS(h)
    GVS(k3)
    GVS(k)
    GVS(l)
    GVS(m)
    GVS(n)
    GVS(o)
    GVS(p)
    GVS(q1)
    GVS(q2)
    GVS(q3)
    GVS(q4)
    GVS(r)
    GVS(s)
    GVS(t)
    GVS(u)
    GVS(v)
    GVS(w)
    GVS(x)
    GVS(y)
    GVS(z)
    GVS(aa)
    GVS(bb)
    GVS(cc)
    GVS(dd)
    GVS(ee)
  END_GVS(42)

  // Dynamic Constants

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_002_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 54;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[17];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
