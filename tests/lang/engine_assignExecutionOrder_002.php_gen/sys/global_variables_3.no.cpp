
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 0:
      HASH_INDEX(0x2C5D5BC33920E200LL, o, 34);
      break;
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x2343A72E98024302LL, l, 31);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 8:
      HASH_INDEX(0x2BCCE9AD79BC2688LL, g, 22);
      break;
    case 9:
      HASH_INDEX(0x117B8667E4662809LL, n, 33);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 13);
      HASH_INDEX(0x4F56B733A4DFC78ALL, y, 47);
      break;
    case 11:
      HASH_INDEX(0x6014C6271D255F8BLL, i2, 24);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x7441D7CC0C080C90LL, g2, 20);
      HASH_INDEX(0x0BC2CDE4BBFC9C10LL, s, 41);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x0EC80E071B573095LL, aa, 49);
      break;
    case 22:
      HASH_INDEX(0x04BFC205E59FA416LL, x, 46);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 17);
      break;
    case 26:
      HASH_INDEX(0x7C60E6EC5B67C29ALL, k3, 29);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      HASH_INDEX(0x609DCCC7C4FD00A3LL, i4, 26);
      break;
    case 38:
      HASH_INDEX(0x77F632A4E34F1526LL, p, 35);
      break;
    case 41:
      HASH_INDEX(0x009CBDB5D52B2AA9LL, w, 45);
      break;
    case 42:
      HASH_INDEX(0x3E2D0448B9E5042ALL, g3, 21);
      break;
    case 46:
      HASH_INDEX(0x6BB4A0689FBAD42ELL, f, 12);
      break;
    case 48:
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 15);
      break;
    case 49:
      HASH_INDEX(0x55093A23014F1AB1LL, bb, 50);
      break;
    case 50:
      HASH_INDEX(0x4D9E12E2960B9832LL, g1, 19);
      break;
    case 51:
      HASH_INDEX(0x62A103F6518DE2B3LL, z, 48);
      break;
    case 53:
      HASH_INDEX(0x45493E1A832563B5LL, i3, 25);
      break;
    case 60:
      HASH_INDEX(0x11C9D2391242AEBCLL, t, 42);
      break;
    case 61:
      HASH_INDEX(0x087A1402E96A67BDLL, q3, 38);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 67:
      HASH_INDEX(0x2858FD2124D17743LL, q4, 39);
      break;
    case 68:
      HASH_INDEX(0x7A452383AA9BF7C4LL, d, 16);
      HASH_INDEX(0x13AF82759F6F5344LL, ee, 53);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x4B27011F259D2446LL, j, 27);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x2A98F317E9E2AA49LL, u, 43);
      break;
    case 74:
      HASH_INDEX(0x66CCECE7A97587CALL, q1, 36);
      break;
    case 75:
      HASH_INDEX(0x7A0C10E3609EA04BLL, m, 32);
      break;
    case 79:
      HASH_INDEX(0x0D6857FDDD7F21CFLL, k, 30);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 85:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 14);
      break;
    case 104:
      HASH_INDEX(0x608D572C3F34DB68LL, i1, 23);
      break;
    case 107:
      HASH_INDEX(0x61B161496B7EA7EBLL, e, 18);
      HASH_INDEX(0x3C2F961831E4EF6BLL, v, 44);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 111:
      HASH_INDEX(0x3F9687191A57146FLL, dd, 52);
      break;
    case 112:
      HASH_INDEX(0x695875365480A8F0LL, h, 28);
      break;
    case 118:
      HASH_INDEX(0x6ECE84440D42ECF6LL, r, 40);
      break;
    case 121:
      HASH_INDEX(0x692ED0D4E4A733F9LL, q2, 37);
      break;
    case 124:
      HASH_INDEX(0x0DA7522EE0AE69FCLL, cc, 51);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 54) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
