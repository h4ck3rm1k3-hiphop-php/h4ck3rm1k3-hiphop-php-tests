
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 0:
      HASH_INITIALIZED(0x2C5D5BC33920E200LL, g->GV(o),
                       o);
      break;
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 2:
      HASH_INITIALIZED(0x2343A72E98024302LL, g->GV(l),
                       l);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 8:
      HASH_INITIALIZED(0x2BCCE9AD79BC2688LL, g->GV(g),
                       g);
      break;
    case 9:
      HASH_INITIALIZED(0x117B8667E4662809LL, g->GV(n),
                       n);
      break;
    case 10:
      HASH_INITIALIZED(0x4292CEE227B9150ALL, g->GV(a),
                       a);
      HASH_INITIALIZED(0x4F56B733A4DFC78ALL, g->GV(y),
                       y);
      break;
    case 11:
      HASH_INITIALIZED(0x6014C6271D255F8BLL, g->GV(i2),
                       i2);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      HASH_INITIALIZED(0x7441D7CC0C080C90LL, g->GV(g2),
                       g2);
      HASH_INITIALIZED(0x0BC2CDE4BBFC9C10LL, g->GV(s),
                       s);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 21:
      HASH_INITIALIZED(0x0EC80E071B573095LL, g->GV(aa),
                       aa);
      break;
    case 22:
      HASH_INITIALIZED(0x04BFC205E59FA416LL, g->GV(x),
                       x);
      break;
    case 24:
      HASH_INITIALIZED(0x0EB22EDA95766E98LL, g->GV(i),
                       i);
      break;
    case 26:
      HASH_INITIALIZED(0x7C60E6EC5B67C29ALL, g->GV(k3),
                       k3);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      HASH_INITIALIZED(0x609DCCC7C4FD00A3LL, g->GV(i4),
                       i4);
      break;
    case 38:
      HASH_INITIALIZED(0x77F632A4E34F1526LL, g->GV(p),
                       p);
      break;
    case 41:
      HASH_INITIALIZED(0x009CBDB5D52B2AA9LL, g->GV(w),
                       w);
      break;
    case 42:
      HASH_INITIALIZED(0x3E2D0448B9E5042ALL, g->GV(g3),
                       g3);
      break;
    case 46:
      HASH_INITIALIZED(0x6BB4A0689FBAD42ELL, g->GV(f),
                       f);
      break;
    case 48:
      HASH_INITIALIZED(0x32C769EE5C5509B0LL, g->GV(c),
                       c);
      break;
    case 49:
      HASH_INITIALIZED(0x55093A23014F1AB1LL, g->GV(bb),
                       bb);
      break;
    case 50:
      HASH_INITIALIZED(0x4D9E12E2960B9832LL, g->GV(g1),
                       g1);
      break;
    case 51:
      HASH_INITIALIZED(0x62A103F6518DE2B3LL, g->GV(z),
                       z);
      break;
    case 53:
      HASH_INITIALIZED(0x45493E1A832563B5LL, g->GV(i3),
                       i3);
      break;
    case 60:
      HASH_INITIALIZED(0x11C9D2391242AEBCLL, g->GV(t),
                       t);
      break;
    case 61:
      HASH_INITIALIZED(0x087A1402E96A67BDLL, g->GV(q3),
                       q3);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    case 67:
      HASH_INITIALIZED(0x2858FD2124D17743LL, g->GV(q4),
                       q4);
      break;
    case 68:
      HASH_INITIALIZED(0x7A452383AA9BF7C4LL, g->GV(d),
                       d);
      HASH_INITIALIZED(0x13AF82759F6F5344LL, g->GV(ee),
                       ee);
      break;
    case 70:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      HASH_INITIALIZED(0x4B27011F259D2446LL, g->GV(j),
                       j);
      break;
    case 73:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x2A98F317E9E2AA49LL, g->GV(u),
                       u);
      break;
    case 74:
      HASH_INITIALIZED(0x66CCECE7A97587CALL, g->GV(q1),
                       q1);
      break;
    case 75:
      HASH_INITIALIZED(0x7A0C10E3609EA04BLL, g->GV(m),
                       m);
      break;
    case 79:
      HASH_INITIALIZED(0x0D6857FDDD7F21CFLL, g->GV(k),
                       k);
      break;
    case 81:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 85:
      HASH_INITIALIZED(0x08FBB133F8576BD5LL, g->GV(b),
                       b);
      break;
    case 104:
      HASH_INITIALIZED(0x608D572C3F34DB68LL, g->GV(i1),
                       i1);
      break;
    case 107:
      HASH_INITIALIZED(0x61B161496B7EA7EBLL, g->GV(e),
                       e);
      HASH_INITIALIZED(0x3C2F961831E4EF6BLL, g->GV(v),
                       v);
      break;
    case 110:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 111:
      HASH_INITIALIZED(0x3F9687191A57146FLL, g->GV(dd),
                       dd);
      break;
    case 112:
      HASH_INITIALIZED(0x695875365480A8F0LL, g->GV(h),
                       h);
      break;
    case 118:
      HASH_INITIALIZED(0x6ECE84440D42ECF6LL, g->GV(r),
                       r);
      break;
    case 121:
      HASH_INITIALIZED(0x692ED0D4E4A733F9LL, g->GV(q2),
                       q2);
      break;
    case 124:
      HASH_INITIALIZED(0x0DA7522EE0AE69FCLL, g->GV(cc),
                       cc);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
