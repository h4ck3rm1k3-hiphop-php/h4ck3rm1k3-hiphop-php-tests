
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "f",
    "a",
    "b",
    "c",
    "d",
    "i",
    "e",
    "g1",
    "g2",
    "g3",
    "g",
    "i1",
    "i2",
    "i3",
    "i4",
    "j",
    "h",
    "k3",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q1",
    "q2",
    "q3",
    "q4",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "aa",
    "bb",
    "cc",
    "dd",
    "ee",
  };
  if (idx >= 0 && idx < 54) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(f);
    case 13: return GV(a);
    case 14: return GV(b);
    case 15: return GV(c);
    case 16: return GV(d);
    case 17: return GV(i);
    case 18: return GV(e);
    case 19: return GV(g1);
    case 20: return GV(g2);
    case 21: return GV(g3);
    case 22: return GV(g);
    case 23: return GV(i1);
    case 24: return GV(i2);
    case 25: return GV(i3);
    case 26: return GV(i4);
    case 27: return GV(j);
    case 28: return GV(h);
    case 29: return GV(k3);
    case 30: return GV(k);
    case 31: return GV(l);
    case 32: return GV(m);
    case 33: return GV(n);
    case 34: return GV(o);
    case 35: return GV(p);
    case 36: return GV(q1);
    case 37: return GV(q2);
    case 38: return GV(q3);
    case 39: return GV(q4);
    case 40: return GV(r);
    case 41: return GV(s);
    case 42: return GV(t);
    case 43: return GV(u);
    case 44: return GV(v);
    case 45: return GV(w);
    case 46: return GV(x);
    case 47: return GV(y);
    case 48: return GV(z);
    case 49: return GV(aa);
    case 50: return GV(bb);
    case 51: return GV(cc);
    case 52: return GV(dd);
    case 53: return GV(ee);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
