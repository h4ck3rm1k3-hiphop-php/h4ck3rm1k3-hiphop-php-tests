
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
Variant a1 = a0.rvalAt(0);
  p0 = a1.rvalAt(0);
  p1 = a1.rvalAt(1);
  p2 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
Variant a1 = a0.rvalAt(0);
  p0 = a1.rvalAt(0);
  p1 = a1.rvalAt(1);
  p2 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_6(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3, Variant &p4, Variant &p5, Variant &p6) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
Variant a1 = a0.rvalAt(0);
  Variant a2 = a1.rvalAt(0);
  p0 = a2.rvalAt(0);
  p1 = a2.rvalAt(1);
  p2 = a2.rvalAt(3);
  Variant a3 = a1.rvalAt(1);
  p3 = a3.rvalAt(0);
  p4 = a3.rvalAt(1);
  p5 = a1.rvalAt(3);
  p6 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_7(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_002.php line 79 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ee __attribute__((__unused__)) = g->GV(ee);
  (gv_ee = ScalarArrays::sa_[16]);
  return 1LL;
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_g1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g1") : g->GV(g1);
  Variant &v_g2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g2") : g->GV(g2);
  Variant &v_g3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g3") : g->GV(g3);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_i1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i1") : g->GV(i1);
  Variant &v_i2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i2") : g->GV(i2);
  Variant &v_i3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i3") : g->GV(i3);
  Variant &v_i4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i4") : g->GV(i4);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_k3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k3") : g->GV(k3);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_l __attribute__((__unused__)) = (variables != gVariables) ? variables->get("l") : g->GV(l);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_p __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p") : g->GV(p);
  Variant &v_q1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q1") : g->GV(q1);
  Variant &v_q2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q2") : g->GV(q2);
  Variant &v_q3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q3") : g->GV(q3);
  Variant &v_q4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q4") : g->GV(q4);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_u __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u") : g->GV(u);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_aa __attribute__((__unused__)) = (variables != gVariables) ? variables->get("aa") : g->GV(aa);
  Variant &v_bb __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bb") : g->GV(bb);
  Variant &v_cc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cc") : g->GV(cc);
  Variant &v_dd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dd") : g->GV(dd);
  Variant &v_ee __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ee") : g->GV(ee);

  (v_f = ScalarArrays::sa_[0]);
  df_lambda_1(v_f, v_a, v_b);
  echo(LINE(6,concat5("A=", toString(v_a), " B=", toString(v_b), "\n")));
  v_c.set((v_c = 1LL), (1LL));
  (v_d = ScalarArrays::sa_[1]);
  (v_i = 0LL);
  v_d.set(v_i++, (v_i * 10LL));
  LINE(17,x_var_dump(1, v_d));
  (v_e = ScalarArrays::sa_[2]);
  (v_f = 0LL);
  (v_g1 = ScalarArrays::sa_[3]);
  (v_g2 = ScalarArrays::sa_[4]);
  (v_g3 = ScalarArrays::sa_[5]);
  (v_g = Array(ArrayInit(3).set(0, v_g1).set(1, v_g2).set(2, v_g3).create()));
  df_lambda_2(v_g.rvalAt(v_f), lval(v_e.lvalAt(v_f++)), lval(v_e.lvalAt(v_f++)));
  LINE(28,x_var_dump(1, v_e));
  (v_i1 = ScalarArrays::sa_[6]);
  (v_i2 = ScalarArrays::sa_[7]);
  (v_i3 = ScalarArrays::sa_[8]);
  (v_i4 = ScalarArrays::sa_[9]);
  (v_i = Array(ArrayInit(4).set(0, v_i1).set(1, v_i2).set(2, v_i3).set(3, v_i4).create()));
  (v_j = ScalarArrays::sa_[10]);
  (v_h = 0LL);
  df_lambda_3(v_i.rvalAt(v_h), lval(v_j.lvalAt(v_h++)), lval(v_j.lvalAt(v_h++)), lval(v_j.lvalAt(v_h++)));
  LINE(40,x_var_dump(1, v_j));
  (v_k3 = ScalarArrays::sa_[8]);
  (v_k = Array(ArrayInit(2).set(0, v_k3).set(1, 300LL).create()));
  df_lambda_4(v_k, v_l, v_m, v_n);
  echo(concat("L=", LINE(47,concat6(toString(v_l), " M=", toString(v_m), " N=", toString(v_n), "\n"))));
  df_lambda_5(20LL, v_o, v_p);
  echo(LINE(52,concat5("O=", toString(v_o), " and P=", toString(v_p), "\n")));
  (v_q1 = ScalarArrays::sa_[11]);
  (v_q2 = ScalarArrays::sa_[12]);
  (v_q3 = Array(ArrayInit(4).set(0, v_q1).set(1, v_q2).set(2, null).set(3, 70LL).create()));
  (v_q4 = Array(ArrayInit(3).set(0, v_q3).set(1, null).set(2, 80LL).create()));
  df_lambda_6(v_q4, v_r, v_s, v_t, v_u, v_v, v_w, v_x);
  echo(concat_rev(LINE(62,concat6(toString(v_v), " ", toString(v_w), " ", toString(v_x), "\n")), (assignCallTemp(eo_0, toString(v_r)),assignCallTemp(eo_2, concat6(toString(v_s), " ", toString(v_t), " ", toString(v_u), " ")),concat3(eo_0, " ", eo_2))));
  df_lambda_7(ScalarArrays::sa_[13], v_y, v_z);
  echo(LINE(67,concat5("Y=", toString(v_y), ",Z=", toString(v_z), "\n")));
  df_lambda_8(ScalarArrays::sa_[14], v_aa, v_bb);
  echo(LINE(71,concat3("AA=", toString(v_aa), "\n")));
  df_lambda_9(ScalarArrays::sa_[11], v_cc, v_dd);
  echo(LINE(75,concat5("CC=", toString(v_cc), " DD=", toString(v_dd), "\n")));
  (v_ee = ScalarArrays::sa_[15]);
  lval(v_ee.lvalAt("array entry created after f()", 0x2A9310687D644243LL)).set(LINE(84,f_f()), ("hello"));
  LINE(85,x_print_r(v_ee));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
