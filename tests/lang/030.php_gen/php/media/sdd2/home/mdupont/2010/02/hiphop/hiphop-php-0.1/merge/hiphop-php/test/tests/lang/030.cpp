
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/030.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/030.php line 2 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::create(CStrRef v_name) {
  init();
  t_foo(v_name);
  return this;
}
ObjectData *c_foo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x2A0173EAEAE17B74LL, echoname) {
        return (t_echoname(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x2A0173EAEAE17B74LL, echoname) {
        return (t_echoname(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/030.php line 3 */
void c_foo::t_foo(CStrRef v_name) {
  INSTANCE_METHOD_INJECTION(foo, foo::foo);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  (g->GV(List) = ref(this));
  (o_lval("Name", 0x50913E962B3F92ABLL) = v_name);
  LINE(6,g->GV(List).o_invoke_few_args("echoName", 0x2A0173EAEAE17B74LL, 0));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/030.php line 9 */
void c_foo::t_echoname() {
  INSTANCE_METHOD_INJECTION(foo, foo::echoName);
  DECLARE_GLOBAL_VARIABLES(g);
  g->GV(names).append((o_get("Name", 0x50913E962B3F92ABLL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/030.php line 14 */
Variant f_foo2(Variant v_foo) {
  FUNCTION_INJECTION(foo2);
  return ref(v_foo);
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$030_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/030.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$030_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar1") : g->GV(bar1);
  Variant &v_List __attribute__((__unused__)) = (variables != gVariables) ? variables->get("List") : g->GV(List);
  Variant &v_names __attribute__((__unused__)) = (variables != gVariables) ? variables->get("names") : g->GV(names);

  (v_bar1 = ((Object)(LINE(19,p_foo(p_foo(NEWOBJ(c_foo)())->create("constructor"))))));
  (v_bar1.o_lval("Name", 0x50913E962B3F92ABLL) = "outside");
  LINE(21,v_bar1.o_invoke_few_args("echoName", 0x2A0173EAEAE17B74LL, 0));
  LINE(22,v_List.o_invoke_few_args("echoName", 0x2A0173EAEAE17B74LL, 0));
  (v_bar1 = ref(LINE(24,f_foo2(ref(p_foo(p_foo(NEWOBJ(c_foo)())->create("constructor")))))));
  (v_bar1.o_lval("Name", 0x50913E962B3F92ABLL) = "outside");
  LINE(26,v_bar1.o_invoke_few_args("echoName", 0x2A0173EAEAE17B74LL, 0));
  LINE(28,v_List.o_invoke_few_args("echoName", 0x2A0173EAEAE17B74LL, 0));
  print(toString((equal(v_names, ScalarArrays::sa_[0])) ? (("success")) : (("failure"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
