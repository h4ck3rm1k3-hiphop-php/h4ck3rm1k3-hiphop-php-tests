
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.003.php line 3 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  props.push_back(NEW(ArrayElement)("b", m_b.isReferenced() ? ref(m_b) : m_b));
  props.push_back(NEW(ArrayElement)("c", m_c.isReferenced() ? ref(m_c) : m_c));
  props.push_back(NEW(ArrayElement)("d", m_d.isReferenced() ? ref(m_d) : m_d));
  props.push_back(NEW(ArrayElement)("e", m_e.isReferenced() ? ref(m_e) : m_e));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    case 10:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    case 11:
      HASH_EXISTS_STRING(0x61B161496B7EA7EBLL, e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 10:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    case 4:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    case 5:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    case 10:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    case 11:
      HASH_SET_STRING(0x61B161496B7EA7EBLL, m_e,
                      e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 10:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  clone->m_b = m_b.isReferenced() ? ref(m_b) : m_b;
  clone->m_c = m_c.isReferenced() ? ref(m_c) : m_c;
  clone->m_d = m_d.isReferenced() ? ref(m_d) : m_d;
  clone->m_e = m_e.isReferenced() ? ref(m_e) : m_e;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_a = "Original a";
  m_b = "Original b";
  m_c = "Original c";
  m_d = "Original d";
  m_e = "Original e";
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopObjects_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopObjects_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_counter __attribute__((__unused__)) = (variables != gVariables) ? variables->get("counter") : g->GV(counter);
  Variant &v_newPropName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newPropName") : g->GV(newPropName);

  echo("\nDirectly changing object values.\n");
  (v_obj = ((Object)(LINE(12,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_obj.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        (v_obj.o_lval(toString(v_k), -1LL) = toString("changed.") + toString(v_k));
        LINE(15,x_var_dump(1, v_v));
      }
    }
  }
  LINE(17,x_var_dump(1, v_obj));
  echo("\nModifying the foreach $value.\n");
  (v_obj = ((Object)(LINE(20,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_obj.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
  LINE(24,x_var_dump(1, v_obj));
  echo("\nModifying the foreach &$value.\n");
  (v_obj = ((Object)(LINE(28,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(7);
    Variant map8 = ref(v_obj);
    map8.escalate();
    for (MutableArrayIterPtr iter9 = map8.begin(&v_k, v_v); iter9->advance();) {
      LOOP_COUNTER_CHECK(7);
      {
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
  LINE(32,x_var_dump(1, v_obj));
  echo("\nAdding properties to an an object.\n");
  (v_obj = ((Object)(LINE(35,p_c(p_c(NEWOBJ(c_c)())->create())))));
  (v_counter = 0LL);
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_obj.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_v = iter12->second();
      {
        (v_newPropName = toString("new") + toString(v_counter));
        (v_obj.o_lval(toString(v_newPropName), -1LL) = toString("Added property ") + toString(v_counter));
        if (more(v_counter++, 10LL)) {
          echo("Loop detected\n");
          break;
        }
        LINE(44,x_var_dump(1, v_v));
      }
    }
  }
  LINE(46,x_var_dump(1, v_obj));
  echo("\nAdding properties to an an object, using &$value.\n");
  (v_obj = ((Object)(LINE(49,p_c(p_c(NEWOBJ(c_c)())->create())))));
  (v_counter = 0LL);
  {
    LOOP_COUNTER(13);
    Variant map14 = ref(v_obj);
    map14.escalate();
    for (MutableArrayIterPtr iter15 = map14.begin(NULL, v_v); iter15->advance();) {
      LOOP_COUNTER_CHECK(13);
      {
        (v_newPropName = toString("new") + toString(v_counter));
        (v_obj.o_lval(toString(v_newPropName), -1LL) = toString("Added property ") + toString(v_counter));
        if (more(v_counter++, 10LL)) {
          echo("Loop detected\n");
          break;
        }
        LINE(58,x_var_dump(1, v_v));
      }
    }
  }
  LINE(60,x_var_dump(1, v_obj));
  echo("\nRemoving properties from an object.\n");
  (v_obj = ((Object)(LINE(63,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(16);
    for (ArrayIterPtr iter18 = v_obj.begin(); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_v = iter18->second();
      {
        toObject(v_obj)->t___unset("a");
        toObject(v_obj)->t___unset("b");
        toObject(v_obj)->t___unset("c");
        LINE(68,x_var_dump(1, v_v));
      }
    }
  }
  LINE(70,x_var_dump(1, v_obj));
  echo("\nRemoving properties from an object, using &$value.\n");
  (v_obj = ((Object)(LINE(73,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(19);
    Variant map20 = ref(v_obj);
    map20.escalate();
    for (MutableArrayIterPtr iter21 = map20.begin(NULL, v_v); iter21->advance();) {
      LOOP_COUNTER_CHECK(19);
      {
        toObject(v_obj)->t___unset("a");
        toObject(v_obj)->t___unset("b");
        toObject(v_obj)->t___unset("c");
        LINE(78,x_var_dump(1, v_v));
      }
    }
  }
  LINE(80,x_var_dump(1, v_obj));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
