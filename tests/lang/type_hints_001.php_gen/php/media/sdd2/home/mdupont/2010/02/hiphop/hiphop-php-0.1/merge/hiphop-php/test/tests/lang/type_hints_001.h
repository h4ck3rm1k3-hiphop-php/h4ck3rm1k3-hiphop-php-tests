
#ifndef __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_lang_type_hints_001_h__
#define __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_lang_type_hints_001_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/type_hints_001.fw.h>

// Declarations
#include <cls/foo.h>
#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$type_hints_001_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_type_hint_foo(CVarRef v_a);
Object co_foo(CArrRef params, bool init = true);
Object co_bar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php__media_sdd2_home_mdupont_2010_02_hiphop_hiphop_php_0_1_merge_hiphop_php_test_tests_lang_type_hints_001_h__
