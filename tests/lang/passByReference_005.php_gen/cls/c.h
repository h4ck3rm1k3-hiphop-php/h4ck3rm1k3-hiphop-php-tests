
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 33 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: void t___construct(Variant v_val, Variant v_ref);
  public: ObjectData *create(Variant v_val, Variant v_ref);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static void ti_v(const char* cls, Variant v_val);
  public: static void ti_r(const char* cls, Variant v_ref);
  public: static void ti_vv(const char* cls, Variant v_val1, Variant v_val2);
  public: static void ti_vr(const char* cls, Variant v_val, Variant v_ref);
  public: static void ti_rv(const char* cls, Variant v_ref, Variant v_val);
  public: static void ti_rr(const char* cls, Variant v_ref1, Variant v_ref2);
  public: static void t_rr(CVarRef v_ref1, CVarRef v_ref2) { ti_rr("c", v_ref1, v_ref2); }
  public: static void t_rv(CVarRef v_ref, CVarRef v_val) { ti_rv("c", v_ref, v_val); }
  public: static void t_r(CVarRef v_ref) { ti_r("c", v_ref); }
  public: static void t_v(CVarRef v_val) { ti_v("c", v_val); }
  public: static void t_vr(CVarRef v_val, CVarRef v_ref) { ti_vr("c", v_val, v_ref); }
  public: static void t_vv(CVarRef v_val1, CVarRef v_val2) { ti_vv("c", v_val1, v_val2); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
