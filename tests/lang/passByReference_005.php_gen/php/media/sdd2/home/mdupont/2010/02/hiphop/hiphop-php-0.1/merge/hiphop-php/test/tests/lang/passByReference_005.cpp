
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 33 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::create(Variant v_val, Variant v_ref) {
  init();
  t___construct(v_val, ref(v_ref));
  return this;
}
ObjectData *c_c::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))));
  } else return this;
}
void c_c::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))));
}
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x46FD2B5C4D59C761LL, rr) {
        return (ti_rr(o_getClassName(), ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))), null);
      }
      break;
    case 6:
      HASH_GUARD(0x6DF58A31D1B6BCC6LL, vv) {
        return (ti_vv(o_getClassName(), params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 8:
      HASH_GUARD(0x246825A55DC38218LL, rv) {
        return (ti_rv(o_getClassName(), ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x280F37D1F5DAB958LL, v) {
        return (ti_v(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    case 11:
      HASH_GUARD(0x77D76D40122506FBLL, r) {
        return (ti_r(o_getClassName(), ref(const_cast<Array&>(params).lvalAt(0))), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))), null);
      }
      HASH_GUARD(0x11B5E0441A0D026FLL, vr) {
        return (ti_vr(o_getClassName(), params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x46FD2B5C4D59C761LL, rr) {
        return (ti_rr(o_getClassName(), ref(a0), ref(a1)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x6DF58A31D1B6BCC6LL, vv) {
        return (ti_vv(o_getClassName(), a0, a1), null);
      }
      break;
    case 8:
      HASH_GUARD(0x246825A55DC38218LL, rv) {
        return (ti_rv(o_getClassName(), ref(a0), a1), null);
      }
      HASH_GUARD(0x280F37D1F5DAB958LL, v) {
        return (ti_v(o_getClassName(), a0), null);
      }
      break;
    case 11:
      HASH_GUARD(0x77D76D40122506FBLL, r) {
        return (ti_r(o_getClassName(), ref(a0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, ref(a1)), null);
      }
      HASH_GUARD(0x11B5E0441A0D026FLL, vr) {
        return (ti_vr(o_getClassName(), a0, ref(a1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x46FD2B5C4D59C761LL, rr) {
        return (ti_rr(c, ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))), null);
      }
      break;
    case 6:
      HASH_GUARD(0x6DF58A31D1B6BCC6LL, vv) {
        return (ti_vv(c, params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 8:
      HASH_GUARD(0x246825A55DC38218LL, rv) {
        return (ti_rv(c, ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x280F37D1F5DAB958LL, v) {
        return (ti_v(c, params.rvalAt(0)), null);
      }
      break;
    case 11:
      HASH_GUARD(0x77D76D40122506FBLL, r) {
        return (ti_r(c, ref(const_cast<Array&>(params).lvalAt(0))), null);
      }
      break;
    case 15:
      HASH_GUARD(0x11B5E0441A0D026FLL, vr) {
        return (ti_vr(c, params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 35 */
void c_c::t___construct(Variant v_val, Variant v_ref) {
  INSTANCE_METHOD_INJECTION(C, C::__construct);
  bool oldInCtor = gasInCtor(true);
  (v_val = "Val changed");
  (v_ref = "Ref changed");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 40 */
void c_c::ti_v(const char* cls, Variant v_val) {
  STATIC_METHOD_INJECTION(C, C::v);
  (v_val = "Val changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 44 */
void c_c::ti_r(const char* cls, Variant v_ref) {
  STATIC_METHOD_INJECTION(C, C::r);
  (v_ref = "Ref changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 48 */
void c_c::ti_vv(const char* cls, Variant v_val1, Variant v_val2) {
  STATIC_METHOD_INJECTION(C, C::vv);
  (v_val1 = "Val1 changed");
  (v_val2 = "Val2 changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 53 */
void c_c::ti_vr(const char* cls, Variant v_val, Variant v_ref) {
  STATIC_METHOD_INJECTION(C, C::vr);
  (v_val = "Val changed");
  (v_ref = "Ref changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 58 */
void c_c::ti_rv(const char* cls, Variant v_ref, Variant v_val) {
  STATIC_METHOD_INJECTION(C, C::rv);
  (v_val = "Val changed");
  (v_ref = "Ref changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 63 */
void c_c::ti_rr(const char* cls, Variant v_ref1, Variant v_ref2) {
  STATIC_METHOD_INJECTION(C, C::rr);
  (v_ref1 = "Ref1 changed");
  (v_ref2 = "Ref2 changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 27 */
void f_rr(Variant v_ref1, Variant v_ref2) {
  FUNCTION_INJECTION(rr);
  (v_ref1 = "Ref1 changed");
  (v_ref2 = "Ref2 changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 22 */
void f_rv(Variant v_ref, Variant v_val) {
  FUNCTION_INJECTION(rv);
  (v_val = "Val changed");
  (v_ref = "Ref changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 7 */
void f_r(Variant v_ref) {
  FUNCTION_INJECTION(r);
  (v_ref = "Ref changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 3 */
void f_v(Variant v_val) {
  FUNCTION_INJECTION(v);
  (v_val = "Val changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 17 */
void f_vr(Variant v_val, Variant v_ref) {
  FUNCTION_INJECTION(vr);
  (v_val = "Val changed");
  (v_ref = "Ref changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php line 12 */
void f_vv(Variant v_val1, Variant v_val2) {
  FUNCTION_INJECTION(vv);
  (v_val1 = "Val1 changed");
  (v_val2 = "Val2 changed");
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_005_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_005.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_005_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_u1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u1") : g->GV(u1);
  Variant &v_u2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u2") : g->GV(u2);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("\n ---- Pass by ref / pass by val: functions ----\n");
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(72,f_v(v_u1));
  LINE(73,f_r(ref(v_u2)));
  LINE(74,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(77,f_vv(v_u1, v_u2));
  LINE(78,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(81,f_vr(v_u1, ref(v_u2)));
  LINE(82,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(85,f_rv(ref(v_u1), v_u2));
  LINE(86,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(89,f_rr(ref(v_u1), ref(v_u2)));
  LINE(90,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  echo("\n\n ---- Pass by ref / pass by val: static method calls ----\n");
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(95,c_c::t_v(v_u1));
  LINE(96,c_c::t_r(ref(v_u2)));
  LINE(97,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(100,c_c::t_vv(v_u1, v_u2));
  LINE(101,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(104,c_c::t_vr(v_u1, ref(v_u2)));
  LINE(105,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(108,c_c::t_rv(ref(v_u1), v_u2));
  LINE(109,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(112,c_c::t_rr(ref(v_u1), ref(v_u2)));
  LINE(113,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  echo("\n\n ---- Pass by ref / pass by val: instance method calls ----\n");
  {
    unset(v_u1);
    unset(v_u2);
  }
  (v_c = ((Object)(LINE(117,p_c(p_c(NEWOBJ(c_c)())->create(v_u1, ref(v_u2)))))));
  LINE(118,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(121,v_c.o_invoke_few_args("v", 0x280F37D1F5DAB958LL, 1, v_u1));
  LINE(122,v_c.o_invoke_few_args("r", 0x77D76D40122506FBLL, 1, v_u2));
  LINE(123,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(126,v_c.o_invoke_few_args("vv", 0x6DF58A31D1B6BCC6LL, 2, v_u1, v_u2));
  LINE(127,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(130,v_c.o_invoke_few_args("vr", 0x11B5E0441A0D026FLL, 2, v_u1, v_u2));
  LINE(131,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(134,v_c.o_invoke_few_args("rv", 0x246825A55DC38218LL, 2, v_u1, v_u2));
  LINE(135,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  {
    unset(v_u1);
    unset(v_u2);
  }
  LINE(138,v_c.o_invoke_few_args("rr", 0x46FD2B5C4D59C761LL, 2, v_u1, v_u2));
  LINE(139,x_var_dump(2, v_u1, Array(ArrayInit(1).set(0, v_u2).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
