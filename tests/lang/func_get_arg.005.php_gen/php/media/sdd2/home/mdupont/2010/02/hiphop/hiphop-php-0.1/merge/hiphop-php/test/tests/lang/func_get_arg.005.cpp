
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_arg.005.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_arg.005.php line 2 */
void f_refval(int num_args, CVarRef v_x, Array args /* = Array() */) {
  FUNCTION_INJECTION(refVal);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  (gv_a = "changed.a");
  LINE(5,x_var_dump(1, v_x));
  LINE(6,x_var_dump(1, func_get_arg(num_args, Array(ArrayInit(1).set(0, v_x).create()),args,toInt32(0LL))));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$func_get_arg_005_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_arg.005.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$func_get_arg_005_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_ref __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ref") : g->GV(ref);

  (v_a = "original.a");
  (v_ref = ref(v_a));
  LINE(11,f_refval(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
