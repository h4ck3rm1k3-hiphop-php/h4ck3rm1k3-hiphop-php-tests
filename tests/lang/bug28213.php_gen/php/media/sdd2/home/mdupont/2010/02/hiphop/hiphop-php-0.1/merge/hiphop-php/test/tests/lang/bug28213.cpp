
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug28213.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug28213.php line 2 */
Variant c_foobar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foobar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foobar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foobar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foobar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foobar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foobar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foobar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foobar)
ObjectData *c_foobar::cloneImpl() {
  c_foobar *obj = NEW(c_foobar)();
  cloneSet(obj);
  return obj;
}
void c_foobar::cloneSet(c_foobar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foobar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x016509DCA13DB6DFLL, error) {
        return (ti_error(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foobar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x016509DCA13DB6DFLL, error) {
        return (ti_error(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foobar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x016509DCA13DB6DFLL, error) {
        return (ti_error(c), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foobar$os_get(const char *s) {
  return c_foobar::os_get(s, -1);
}
Variant &cw_foobar$os_lval(const char *s) {
  return c_foobar::os_lval(s, -1);
}
Variant cw_foobar$os_constant(const char *s) {
  return c_foobar::os_constant(s);
}
Variant cw_foobar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foobar::os_invoke(c, s, params, -1, fatal);
}
void c_foobar::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug28213.php line 2 */
void c_foobar::ti_error(const char* cls) {
  STATIC_METHOD_INJECTION(FooBar, FooBar::error);
  LINE(2,x_debug_print_backtrace());
} /* function */
Object co_foobar(CArrRef params, bool init /* = true */) {
  return Object(p_foobar(NEW(c_foobar)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug28213_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug28213.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug28213_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_set_error_handler(toString(ScalarArrays::sa_[0])));
  LINE(4,include("foobar.php", false, variables, "/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
