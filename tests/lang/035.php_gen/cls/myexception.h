
#ifndef __GENERATED_cls_myexception_h__
#define __GENERATED_cls_myexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/035.php line 2 */
class c_myexception : virtual public c_exception {
  BEGIN_CLASS_MAP(myexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(myexception)
  DECLARE_CLASS(myexception, MyException, exception)
  void init();
  public: void t_myexception(CStrRef v__error);
  public: ObjectData *create(CStrRef v__error);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_getexception();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myexception_h__
