
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/035.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/035.php line 2 */
Variant c_myexception::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_myexception::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_myexception::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_myexception::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_myexception::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_myexception::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_myexception::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_myexception::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(myexception)
ObjectData *c_myexception::create(CStrRef v__error) {
  init();
  t_myexception(v__error);
  return this;
}
ObjectData *c_myexception::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_myexception::cloneImpl() {
  c_myexception *obj = NEW(c_myexception)();
  cloneSet(obj);
  return obj;
}
void c_myexception::cloneSet(c_myexception *clone) {
  c_exception::cloneSet(clone);
}
Variant c_myexception::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x0840A780622FFDDALL, getexception) {
        return (t_getexception());
      }
      break;
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_myexception::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x0840A780622FFDDALL, getexception) {
        return (t_getexception());
      }
      break;
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myexception::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myexception$os_get(const char *s) {
  return c_myexception::os_get(s, -1);
}
Variant &cw_myexception$os_lval(const char *s) {
  return c_myexception::os_lval(s, -1);
}
Variant cw_myexception$os_constant(const char *s) {
  return c_myexception::os_constant(s);
}
Variant cw_myexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myexception::os_invoke(c, s, params, -1, fatal);
}
void c_myexception::init() {
  c_exception::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/035.php line 3 */
void c_myexception::t_myexception(CStrRef v__error) {
  INSTANCE_METHOD_INJECTION(MyException, MyException::MyException);
  bool oldInCtor = gasInCtor(true);
  (o_lval("error", 0x7CF1CB319E5158A7LL) = v__error);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/035.php line 7 */
Variant c_myexception::t_getexception() {
  INSTANCE_METHOD_INJECTION(MyException, MyException::getException);
  return o_get("error", 0x7CF1CB319E5158A7LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/035.php line 13 */
void f_throwexception() {
  FUNCTION_INJECTION(ThrowException);
  throw_exception(((Object)(LINE(15,p_myexception(p_myexception(NEWOBJ(c_myexception)())->create("'This is an exception!'"))))));
} /* function */
Object co_myexception(CArrRef params, bool init /* = true */) {
  return Object(p_myexception(NEW(c_myexception)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$035_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/035.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$035_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_exception __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exception") : g->GV(exception);

  try {
  } catch (Object e) {
    if (e.instanceof("myexception")) {
      v_exception = e;
      print(concat("There shouldn't be an exception: ", toString(LINE(21,v_exception.o_invoke_few_args("getException", 0x0840A780622FFDDALL, 0)))));
      print("\n");
    } else {
      throw;
    }
  }
  try {
    LINE(26,f_throwexception());
  } catch (Object e) {
    if (e.instanceof("myexception")) {
      v_exception = e;
      print(concat("There was an exception: ", toString(LINE(28,v_exception.o_invoke_few_args("getException", 0x0840A780622FFDDALL, 0)))));
      print("\n");
    } else {
      throw;
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
