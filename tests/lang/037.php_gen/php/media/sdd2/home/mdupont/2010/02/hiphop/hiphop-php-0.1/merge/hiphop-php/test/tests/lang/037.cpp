
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/037.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/037.php line 3 */
Variant c_par::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_par::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_par::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("id", m_id));
  c_ObjectData::o_get(props);
}
bool c_par::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x028B9FE0C4522BE2LL, id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_par::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_par::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x028B9FE0C4522BE2LL, m_id,
                      id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_par::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_par::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(par)
ObjectData *c_par::cloneImpl() {
  c_par *obj = NEW(c_par)();
  cloneSet(obj);
  return obj;
}
void c_par::cloneSet(c_par *clone) {
  clone->m_id = m_id;
  ObjectData::cloneSet(clone);
}
Variant c_par::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5EEA453C788DEA05LL, displayme) {
        return (t_displayme(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_par::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5EEA453C788DEA05LL, displayme) {
        return (t_displayme(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_par::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_par$os_get(const char *s) {
  return c_par::os_get(s, -1);
}
Variant &cw_par$os_lval(const char *s) {
  return c_par::os_lval(s, -1);
}
Variant cw_par$os_constant(const char *s) {
  return c_par::os_constant(s);
}
Variant cw_par$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_par::os_invoke(c, s, params, -1, fatal);
}
void c_par::init() {
  m_id = "foo";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/037.php line 6 */
void c_par::t_displayme() {
  INSTANCE_METHOD_INJECTION(par, par::displayMe);
  LINE(8,o_root_invoke_few_args("displayChild", 0x3BDDF3B21EF0DCEALL, 0));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/037.php line 12 */
Variant c_chld::os_get(const char *s, int64 hash) {
  return c_par::os_get(s, hash);
}
Variant &c_chld::os_lval(const char *s, int64 hash) {
  return c_par::os_lval(s, hash);
}
void c_chld::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("id", m_id));
  c_par::o_get(props);
}
bool c_chld::o_exists(CStrRef s, int64 hash) const {
  return c_par::o_exists(s, hash);
}
Variant c_chld::o_get(CStrRef s, int64 hash) {
  return c_par::o_get(s, hash);
}
Variant c_chld::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_par::o_set(s, hash, v, forInit);
}
Variant &c_chld::o_lval(CStrRef s, int64 hash) {
  return c_par::o_lval(s, hash);
}
Variant c_chld::os_constant(const char *s) {
  return c_par::os_constant(s);
}
IMPLEMENT_CLASS(chld)
ObjectData *c_chld::cloneImpl() {
  c_chld *obj = NEW(c_chld)();
  cloneSet(obj);
  return obj;
}
void c_chld::cloneSet(c_chld *clone) {
  clone->m_id = m_id;
  c_par::cloneSet(clone);
}
Variant c_chld::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x5EEA453C788DEA05LL, displayme) {
        return (t_displayme(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x3BDDF3B21EF0DCEALL, displaychild) {
        return (t_displaychild(), null);
      }
      break;
    default:
      break;
  }
  return c_par::o_invoke(s, params, hash, fatal);
}
Variant c_chld::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x5EEA453C788DEA05LL, displayme) {
        return (t_displayme(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x3BDDF3B21EF0DCEALL, displaychild) {
        return (t_displaychild(), null);
      }
      break;
    default:
      break;
  }
  return c_par::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_chld::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_par::os_invoke(c, s, params, hash, fatal);
}
Variant cw_chld$os_get(const char *s) {
  return c_chld::os_get(s, -1);
}
Variant &cw_chld$os_lval(const char *s) {
  return c_chld::os_lval(s, -1);
}
Variant cw_chld$os_constant(const char *s) {
  return c_chld::os_constant(s);
}
Variant cw_chld$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_chld::os_invoke(c, s, params, -1, fatal);
}
void c_chld::init() {
  c_par::init();
  m_id = "bar";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/037.php line 15 */
void c_chld::t_displaychild() {
  INSTANCE_METHOD_INJECTION(chld, chld::displayChild);
  print(m_id);
} /* function */
Object co_par(CArrRef params, bool init /* = true */) {
  return Object(p_par(NEW(c_par)())->dynCreate(params, init));
}
Object co_chld(CArrRef params, bool init /* = true */) {
  return Object(p_chld(NEW(c_chld)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$037_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/037.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$037_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  (v_obj = ((Object)(LINE(22,p_chld(p_chld(NEWOBJ(c_chld)())->create())))));
  LINE(23,v_obj.o_invoke_few_args("displayMe", 0x5EEA453C788DEA05LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
