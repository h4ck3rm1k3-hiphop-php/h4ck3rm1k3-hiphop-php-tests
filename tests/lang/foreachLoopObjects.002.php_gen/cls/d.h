
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__

#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 36 */
class c_d : virtual public c_c {
  BEGIN_CLASS_MAP(d)
    PARENT_CLASS(c)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, c)
  void init();
  public: String m_f;
  public: String m_g;
  public: static void ti_doforeach(const char* cls, Variant v_obj);
  public: void t_doforeachonthis();
  public: static void t_doforeach(CVarRef v_obj) { ti_doforeach("d", v_obj); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
