
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 3 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: String m_a;
  public: String m_b;
  public: String m_c;
  public: String m_d;
  public: String m_e;
  public: void t_doforeachc();
  public: static void ti_doforeach(const char* cls, Variant v_obj);
  public: void t_doforeachonthis();
  public: static void t_doforeach(CVarRef v_obj) { ti_doforeach("c", v_obj); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
