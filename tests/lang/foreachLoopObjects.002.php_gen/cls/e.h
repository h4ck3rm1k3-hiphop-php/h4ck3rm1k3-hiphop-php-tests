
#ifndef __GENERATED_cls_e_h__
#define __GENERATED_cls_e_h__

#include <cls/d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 58 */
class c_e : virtual public c_d {
  BEGIN_CLASS_MAP(e)
    PARENT_CLASS(c)
    PARENT_CLASS(d)
  END_CLASS_MAP(e)
  DECLARE_CLASS(e, E, d)
  void init();
  public: static void ti_doforeach(const char* cls, Variant v_obj);
  public: void t_doforeachonthis();
  public: static void t_doforeach(CVarRef v_obj) { ti_doforeach("e", v_obj); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_e_h__
