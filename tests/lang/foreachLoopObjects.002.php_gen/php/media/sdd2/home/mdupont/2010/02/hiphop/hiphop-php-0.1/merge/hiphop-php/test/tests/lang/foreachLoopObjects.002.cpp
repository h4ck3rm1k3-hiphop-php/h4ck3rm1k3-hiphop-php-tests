
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 3 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  props.push_back(NEW(ArrayElement)("b", m_b));
  props.push_back(NEW(ArrayElement)("c", m_c));
  props.push_back(NEW(ArrayElement)("d", m_d));
  props.push_back(NEW(ArrayElement)("e", m_e));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    case 10:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    case 11:
      HASH_EXISTS_STRING(0x61B161496B7EA7EBLL, e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 10:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    case 4:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    case 5:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    case 10:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    case 11:
      HASH_SET_STRING(0x61B161496B7EA7EBLL, m_e,
                      e, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_a = m_a;
  clone->m_b = m_b;
  clone->m_c = m_c;
  clone->m_d = m_d;
  clone->m_e = m_e;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x4FD510CAD0EDB080LL, doforeachonthis) {
        return (t_doforeachonthis(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x3E392113198E8026LL, doforeachc) {
        return (t_doforeachc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x4FD510CAD0EDB080LL, doforeachonthis) {
        return (t_doforeachonthis(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(o_getClassName(), a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x3E392113198E8026LL, doforeachc) {
        return (t_doforeachc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(c, params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_a = "Original a";
  m_b = "Original b";
  m_c = "Original c";
  m_d = "Original d";
  m_e = "Original e";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 10 */
void c_c::t_doforeachc() {
  INSTANCE_METHOD_INJECTION(C, C::doForEachC);
  Primitive v_k = 0;
  Variant v_v;

  echo("in C::doForEachC\n");
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(this);
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(&v_k, v_v); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(13,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 18 */
void c_c::ti_doforeach(const char* cls, Variant v_obj) {
  STATIC_METHOD_INJECTION(C, C::doForEach);
  Primitive v_k = 0;
  Variant v_v;

  echo("in C::doForEach\n");
  {
    LOOP_COUNTER(4);
    Variant map5 = ref(v_obj);
    map5.escalate();
    for (MutableArrayIterPtr iter6 = map5.begin(&v_k, v_v); iter6->advance();) {
      LOOP_COUNTER_CHECK(4);
      {
        LINE(21,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 26 */
void c_c::t_doforeachonthis() {
  INSTANCE_METHOD_INJECTION(C, C::doForEachOnThis);
  Primitive v_k = 0;
  Variant v_v;

  echo("in C::doForEachOnThis\n");
  {
    LOOP_COUNTER(7);
    Variant map8 = ref(this);
    map8.escalate();
    for (MutableArrayIterPtr iter9 = map8.begin(&v_k, v_v); iter9->advance();) {
      LOOP_COUNTER_CHECK(7);
      {
        LINE(29,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 36 */
Variant c_d::os_get(const char *s, int64 hash) {
  return c_c::os_get(s, hash);
}
Variant &c_d::os_lval(const char *s, int64 hash) {
  return c_c::os_lval(s, hash);
}
void c_d::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("f", m_f));
  props.push_back(NEW(ArrayElement)("g", m_g));
  c_c::o_get(props);
}
bool c_d::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x2BCCE9AD79BC2688LL, g, 1);
      break;
    case 2:
      HASH_EXISTS_STRING(0x6BB4A0689FBAD42ELL, f, 1);
      break;
    default:
      break;
  }
  return c_c::o_exists(s, hash);
}
Variant c_d::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2BCCE9AD79BC2688LL, m_g,
                         g, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x6BB4A0689FBAD42ELL, m_f,
                         f, 1);
      break;
    default:
      break;
  }
  return c_c::o_get(s, hash);
}
Variant c_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x2BCCE9AD79BC2688LL, m_g,
                      g, 1);
      break;
    case 2:
      HASH_SET_STRING(0x6BB4A0689FBAD42ELL, m_f,
                      f, 1);
      break;
    default:
      break;
  }
  return c_c::o_set(s, hash, v, forInit);
}
Variant &c_d::o_lval(CStrRef s, int64 hash) {
  return c_c::o_lval(s, hash);
}
Variant c_d::os_constant(const char *s) {
  return c_c::os_constant(s);
}
IMPLEMENT_CLASS(d)
ObjectData *c_d::cloneImpl() {
  c_d *obj = NEW(c_d)();
  cloneSet(obj);
  return obj;
}
void c_d::cloneSet(c_d *clone) {
  clone->m_f = m_f;
  clone->m_g = m_g;
  c_c::cloneSet(clone);
}
Variant c_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x4FD510CAD0EDB080LL, doforeachonthis) {
        return (t_doforeachonthis(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x3E392113198E8026LL, doforeachc) {
        return (t_doforeachc(), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke(s, params, hash, fatal);
}
Variant c_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x4FD510CAD0EDB080LL, doforeachonthis) {
        return (t_doforeachonthis(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(o_getClassName(), a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x3E392113198E8026LL, doforeachc) {
        return (t_doforeachc(), null);
      }
      break;
    default:
      break;
  }
  return c_c::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(c, params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_c::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d$os_get(const char *s) {
  return c_d::os_get(s, -1);
}
Variant &cw_d$os_lval(const char *s) {
  return c_d::os_lval(s, -1);
}
Variant cw_d$os_constant(const char *s) {
  return c_d::os_constant(s);
}
Variant cw_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d::os_invoke(c, s, params, -1, fatal);
}
void c_d::init() {
  c_c::init();
  m_f = "Original f";
  m_g = "Original g";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 41 */
void c_d::ti_doforeach(const char* cls, Variant v_obj) {
  STATIC_METHOD_INJECTION(D, D::doForEach);
  Primitive v_k = 0;
  Variant v_v;

  echo("in D::doForEach\n");
  {
    LOOP_COUNTER(10);
    Variant map11 = ref(v_obj);
    map11.escalate();
    for (MutableArrayIterPtr iter12 = map11.begin(&v_k, v_v); iter12->advance();) {
      LOOP_COUNTER_CHECK(10);
      {
        LINE(44,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 49 */
void c_d::t_doforeachonthis() {
  INSTANCE_METHOD_INJECTION(D, D::doForEachOnThis);
  Primitive v_k = 0;
  Variant v_v;

  echo("in D::doForEachOnThis\n");
  {
    LOOP_COUNTER(13);
    Variant map14 = ref(this);
    map14.escalate();
    for (MutableArrayIterPtr iter15 = map14.begin(&v_k, v_v); iter15->advance();) {
      LOOP_COUNTER_CHECK(13);
      {
        LINE(52,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 58 */
Variant c_e::os_get(const char *s, int64 hash) {
  return c_d::os_get(s, hash);
}
Variant &c_e::os_lval(const char *s, int64 hash) {
  return c_d::os_lval(s, hash);
}
void c_e::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  props.push_back(NEW(ArrayElement)("b", m_b));
  props.push_back(NEW(ArrayElement)("c", m_c));
  props.push_back(NEW(ArrayElement)("d", m_d));
  props.push_back(NEW(ArrayElement)("e", m_e));
  c_d::o_get(props);
}
bool c_e::o_exists(CStrRef s, int64 hash) const {
  return c_d::o_exists(s, hash);
}
Variant c_e::o_get(CStrRef s, int64 hash) {
  return c_d::o_get(s, hash);
}
Variant c_e::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_d::o_set(s, hash, v, forInit);
}
Variant &c_e::o_lval(CStrRef s, int64 hash) {
  return c_d::o_lval(s, hash);
}
Variant c_e::os_constant(const char *s) {
  return c_d::os_constant(s);
}
IMPLEMENT_CLASS(e)
ObjectData *c_e::cloneImpl() {
  c_e *obj = NEW(c_e)();
  cloneSet(obj);
  return obj;
}
void c_e::cloneSet(c_e *clone) {
  clone->m_a = m_a;
  clone->m_b = m_b;
  clone->m_c = m_c;
  clone->m_d = m_d;
  clone->m_e = m_e;
  c_d::cloneSet(clone);
}
Variant c_e::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x4FD510CAD0EDB080LL, doforeachonthis) {
        return (t_doforeachonthis(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x3E392113198E8026LL, doforeachc) {
        return (t_doforeachc(), null);
      }
      break;
    default:
      break;
  }
  return c_d::o_invoke(s, params, hash, fatal);
}
Variant c_e::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x4FD510CAD0EDB080LL, doforeachonthis) {
        return (t_doforeachonthis(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(o_getClassName(), a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x3E392113198E8026LL, doforeachc) {
        return (t_doforeachc(), null);
      }
      break;
    default:
      break;
  }
  return c_d::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_e::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x2AA146BA211A7649LL, doforeach) {
        return (ti_doforeach(c, params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_d::os_invoke(c, s, params, hash, fatal);
}
Variant cw_e$os_get(const char *s) {
  return c_e::os_get(s, -1);
}
Variant &cw_e$os_lval(const char *s) {
  return c_e::os_lval(s, -1);
}
Variant cw_e$os_constant(const char *s) {
  return c_e::os_constant(s);
}
Variant cw_e$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_e::os_invoke(c, s, params, -1, fatal);
}
void c_e::init() {
  c_d::init();
  m_a = "Overridden a";
  m_b = "Overridden b";
  m_c = "Overridden c";
  m_d = "Overridden d";
  m_e = "Overridden e";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 65 */
void c_e::ti_doforeach(const char* cls, Variant v_obj) {
  STATIC_METHOD_INJECTION(E, E::doForEach);
  Primitive v_k = 0;
  Variant v_v;

  echo("in E::doForEach\n");
  {
    LOOP_COUNTER(16);
    Variant map17 = ref(v_obj);
    map17.escalate();
    for (MutableArrayIterPtr iter18 = map17.begin(&v_k, v_v); iter18->advance();) {
      LOOP_COUNTER_CHECK(16);
      {
        LINE(68,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php line 73 */
void c_e::t_doforeachonthis() {
  INSTANCE_METHOD_INJECTION(E, E::doForEachOnThis);
  Primitive v_k = 0;
  Variant v_v;

  echo("in E::doForEachOnThis\n");
  {
    LOOP_COUNTER(19);
    Variant map20 = ref(this);
    map20.escalate();
    for (MutableArrayIterPtr iter21 = map20.begin(&v_k, v_v); iter21->advance();) {
      LOOP_COUNTER_CHECK(19);
      {
        LINE(76,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d(CArrRef params, bool init /* = true */) {
  return Object(p_d(NEW(c_d)())->dynCreate(params, init));
}
Object co_e(CArrRef params, bool init /* = true */) {
  return Object(p_e(NEW(c_e)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopObjects_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopObjects.002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopObjects_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_myC __attribute__((__unused__)) = (variables != gVariables) ? variables->get("myC") : g->GV(myC);
  Variant &v_myD __attribute__((__unused__)) = (variables != gVariables) ? variables->get("myD") : g->GV(myD);
  Variant &v_myE __attribute__((__unused__)) = (variables != gVariables) ? variables->get("myE") : g->GV(myE);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("\n\nIterate over various generations from within overridden methods:\n");
  echo("\n--> Using instance of C:\n");
  (v_myC = ((Object)(LINE(84,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(85,v_myC.o_invoke_few_args("doForEachOnThis", 0x4FD510CAD0EDB080LL, 0));
  LINE(86,x_var_dump(1, v_myC));
  echo("\n--> Using instance of D:\n");
  (v_myD = ((Object)(LINE(88,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(89,v_myD.o_invoke_few_args("doForEachOnThis", 0x4FD510CAD0EDB080LL, 0));
  LINE(90,x_var_dump(1, v_myD));
  echo("\n--> Using instance of E:\n");
  (v_myE = ((Object)(LINE(92,p_e(p_e(NEWOBJ(c_e)())->create())))));
  LINE(93,v_myE.o_invoke_few_args("doForEachOnThis", 0x4FD510CAD0EDB080LL, 0));
  LINE(94,x_var_dump(1, v_myE));
  echo("\n\nIterate over various generations from within an inherited method:\n");
  echo("\n--> Using instance of C:\n");
  (v_myC = ((Object)(LINE(98,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(99,v_myC.o_invoke_few_args("doForEachC", 0x3E392113198E8026LL, 0));
  LINE(100,x_var_dump(1, v_myC));
  echo("\n--> Using instance of D:\n");
  (v_myD = ((Object)(LINE(102,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(103,v_myD.o_invoke_few_args("doForEachC", 0x3E392113198E8026LL, 0));
  LINE(104,x_var_dump(1, v_myD));
  echo("\n--> Using instance of E:\n");
  (v_myE = ((Object)(LINE(106,p_e(p_e(NEWOBJ(c_e)())->create())))));
  LINE(107,v_myE.o_invoke_few_args("doForEachC", 0x3E392113198E8026LL, 0));
  LINE(108,x_var_dump(1, v_myE));
  echo("\n\nIterate over various generations from within an overridden static method:\n");
  echo("\n--> Using instance of C:\n");
  (v_myC = ((Object)(LINE(112,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(113,c_c::t_doforeach(v_myC));
  LINE(114,x_var_dump(1, v_myC));
  (v_myC = ((Object)(LINE(115,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(116,c_d::t_doforeach(v_myC));
  LINE(117,x_var_dump(1, v_myC));
  (v_myC = ((Object)(LINE(118,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(119,c_e::t_doforeach(v_myC));
  LINE(120,x_var_dump(1, v_myC));
  echo("\n--> Using instance of D:\n");
  (v_myD = ((Object)(LINE(122,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(123,c_c::t_doforeach(v_myD));
  LINE(124,x_var_dump(1, v_myD));
  (v_myD = ((Object)(LINE(125,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(126,c_d::t_doforeach(v_myD));
  LINE(127,x_var_dump(1, v_myD));
  (v_myD = ((Object)(LINE(128,p_d(p_d(NEWOBJ(c_d)())->create())))));
  LINE(129,c_e::t_doforeach(v_myD));
  LINE(130,x_var_dump(1, v_myD));
  echo("\n--> Using instance of E:\n");
  (v_myE = ((Object)(LINE(132,p_e(p_e(NEWOBJ(c_e)())->create())))));
  LINE(133,c_c::t_doforeach(v_myE));
  LINE(134,x_var_dump(1, v_myE));
  (v_myE = ((Object)(LINE(135,p_e(p_e(NEWOBJ(c_e)())->create())))));
  LINE(136,c_d::t_doforeach(v_myE));
  LINE(137,x_var_dump(1, v_myE));
  (v_myE = ((Object)(LINE(138,p_e(p_e(NEWOBJ(c_e)())->create())))));
  LINE(139,c_e::t_doforeach(v_myE));
  LINE(140,x_var_dump(1, v_myE));
  echo("\n\nIterate over various generations from outside the object:\n");
  echo("\n--> Using instance of C:\n");
  (v_myC = ((Object)(LINE(145,p_c(p_c(NEWOBJ(c_c)())->create())))));
  {
    LOOP_COUNTER(22);
    Variant map23 = ref(v_myC);
    map23.escalate();
    for (MutableArrayIterPtr iter24 = map23.begin(&v_k, v_v); iter24->advance();) {
      LOOP_COUNTER_CHECK(22);
      {
        LINE(147,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
  LINE(150,x_var_dump(1, v_myC));
  echo("\n--> Using instance of D:\n");
  (v_myD = ((Object)(LINE(152,p_d(p_d(NEWOBJ(c_d)())->create())))));
  {
    LOOP_COUNTER(25);
    Variant map26 = ref(v_myD);
    map26.escalate();
    for (MutableArrayIterPtr iter27 = map26.begin(&v_k, v_v); iter27->advance();) {
      LOOP_COUNTER_CHECK(25);
      {
        LINE(154,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
  LINE(157,x_var_dump(1, v_myD));
  echo("\n--> Using instance of E:\n");
  (v_myE = ((Object)(LINE(159,p_e(p_e(NEWOBJ(c_e)())->create())))));
  {
    LOOP_COUNTER(28);
    Variant map29 = ref(v_myE);
    map29.escalate();
    for (MutableArrayIterPtr iter30 = map29.begin(&v_k, v_v); iter30->advance();) {
      LOOP_COUNTER_CHECK(28);
      {
        LINE(161,x_var_dump(1, v_v));
        (v_v = toString("changed.") + toString(v_k));
      }
    }
  }
  LINE(164,x_var_dump(1, v_myE));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
