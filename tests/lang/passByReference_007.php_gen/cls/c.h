
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 2 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: static Variant ti_sreturnval(const char* cls);
  public: static Variant ti_sreturnreference(const char* cls);
  public: Variant t_returnval();
  public: Variant t_returnreference();
  public: static Variant t_sreturnval() { return ti_sreturnval("c"); }
  public: static Variant t_sreturnreference() { return ti_sreturnreference("c"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
