
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x1F17F430FBB81FD8LL, returnval) {
        return (t_returnval());
      }
      break;
    case 2:
      HASH_GUARD(0x333FA9155C73066ALL, returnreference) {
        return ref(t_returnreference());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x1F17F430FBB81FD8LL, returnval) {
        return (t_returnval());
      }
      break;
    case 2:
      HASH_GUARD(0x333FA9155C73066ALL, returnreference) {
        return ref(t_returnreference());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 3 */
Variant c_c::ti_sreturnval(const char* cls) {
  STATIC_METHOD_INJECTION(C, C::sreturnVal);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return gv_a;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 8 */
Variant c_c::ti_sreturnreference(const char* cls) {
  STATIC_METHOD_INJECTION(C, C::sreturnReference);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return ref(gv_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 13 */
Variant c_c::t_returnval() {
  INSTANCE_METHOD_INJECTION(C, C::returnVal);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return gv_a;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 18 */
Variant c_c::t_returnreference() {
  INSTANCE_METHOD_INJECTION(C, C::returnReference);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return ref(gv_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 24 */
Variant f_returnval() {
  FUNCTION_INJECTION(returnVal);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return gv_a;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 29 */
Variant f_returnreference() {
  FUNCTION_INJECTION(returnReference);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return ref(gv_a);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php line 36 */
void f_foo(Variant v_ref) {
  FUNCTION_INJECTION(foo);
  LINE(37,x_var_dump(1, v_ref));
  (v_ref = "changed");
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_007_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_007.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_007_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_myC __attribute__((__unused__)) = (variables != gVariables) ? variables->get("myC") : g->GV(myC);

  echo("Pass a function call that returns a value:\n");
  (v_a = "original");
  LINE(44,f_foo(ref(f_returnval())));
  LINE(45,x_var_dump(1, v_a));
  echo("Pass a function call that returns a reference:\n");
  (v_a = "original");
  LINE(49,f_foo(ref(f_returnreference())));
  LINE(50,x_var_dump(1, v_a));
  echo("\nPass a static method call that returns a value:\n");
  (v_a = "original");
  LINE(55,f_foo(ref(c_c::t_sreturnval())));
  LINE(56,x_var_dump(1, v_a));
  echo("Pass a static method call that returns a reference:\n");
  (v_a = "original");
  LINE(60,f_foo(ref(c_c::t_sreturnreference())));
  LINE(61,x_var_dump(1, v_a));
  (v_myC = ((Object)(LINE(64,p_c(p_c(NEWOBJ(c_c)())->create())))));
  echo("\nPass a method call that returns a value:\n");
  (v_a = "original");
  LINE(67,f_foo(ref(v_myC.o_invoke_few_args("returnVal", 0x1F17F430FBB81FD8LL, 0))));
  LINE(68,x_var_dump(1, v_a));
  echo("Pass a method call that returns a reference:\n");
  (v_a = "original");
  LINE(72,f_foo(ref(v_myC.o_invoke_few_args("returnReference", 0x333FA9155C73066ALL, 0))));
  LINE(73,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
