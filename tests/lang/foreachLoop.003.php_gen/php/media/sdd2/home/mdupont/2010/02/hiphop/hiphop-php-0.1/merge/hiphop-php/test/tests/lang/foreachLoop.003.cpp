
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("\nNot an array.\n");
  (v_a = true);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        LINE(5,x_var_dump(1, v_v));
      }
    }
  }
  setNull(v_a);
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      {
        LINE(10,x_var_dump(1, v_v));
      }
    }
  }
  (v_a = 1LL);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_a.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
      {
        LINE(15,x_var_dump(1, v_v));
      }
    }
  }
  (v_a = 1.5);
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_a.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_v = iter12->second();
      {
        LINE(20,x_var_dump(1, v_v));
      }
    }
  }
  (v_a = "hello");
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_a.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_v = iter15->second();
      {
        LINE(25,x_var_dump(1, v_v));
      }
    }
  }
  echo("done.\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
