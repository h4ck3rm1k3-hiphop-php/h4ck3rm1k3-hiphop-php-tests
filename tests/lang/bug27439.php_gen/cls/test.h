
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 9 */
class c_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(test)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, test, ObjectData)
  void init();
  public: Array m_array;
  public: Variant m_string;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Array t_getarray();
  public: Variant t_getstring();
  public: void t_case1();
  public: void t_case2();
  public: void t_case3();
  public: void t_case4();
  public: void t_case5();
  public: void t_case6();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
