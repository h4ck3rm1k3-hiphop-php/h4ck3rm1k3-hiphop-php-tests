
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 3 */
Variant c_test_props::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test_props::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test_props::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  props.push_back(NEW(ArrayElement)("b", m_b));
  props.push_back(NEW(ArrayElement)("c", m_c));
  c_ObjectData::o_get(props);
}
bool c_test_props::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    case 2:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test_props::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test_props::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    case 2:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    case 5:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test_props::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test_props::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test_props)
ObjectData *c_test_props::cloneImpl() {
  c_test_props *obj = NEW(c_test_props)();
  cloneSet(obj);
  return obj;
}
void c_test_props::cloneSet(c_test_props *clone) {
  clone->m_a = m_a;
  clone->m_b = m_b;
  clone->m_c = m_c;
  ObjectData::cloneSet(clone);
}
Variant c_test_props::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test_props::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test_props::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test_props$os_get(const char *s) {
  return c_test_props::os_get(s, -1);
}
Variant &cw_test_props$os_lval(const char *s) {
  return c_test_props::os_lval(s, -1);
}
Variant cw_test_props$os_constant(const char *s) {
  return c_test_props::os_constant(s);
}
Variant cw_test_props$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test_props::os_invoke(c, s, params, -1, fatal);
}
void c_test_props::init() {
  m_a = 1LL;
  m_b = 2LL;
  m_c = 3LL;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 9 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("array", m_array));
  props.push_back(NEW(ArrayElement)("string", m_string.isReferenced() ? ref(m_string) : m_string));
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x063D35168C04B960LL, array, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x2027864469AD4382LL, string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x063D35168C04B960LL, m_array,
                         array, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x2027864469AD4382LL, m_string,
                         string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x063D35168C04B960LL, m_array,
                      array, 5);
      break;
    case 2:
      HASH_SET_STRING(0x2027864469AD4382LL, m_string,
                      string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2027864469AD4382LL, m_string,
                         string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_test::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_test::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  clone->m_array = m_array;
  clone->m_string = m_string.isReferenced() ? ref(m_string) : m_string;
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3594B01D88AF36A0LL, case2) {
        return (t_case2(), null);
      }
      HASH_GUARD(0x64E501A2E5751960LL, case3) {
        return (t_case3(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x31A9CEB2F6C59965LL, case4) {
        return (t_case4(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4EA46759920A1E06LL, case6) {
        return (t_case6(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x1F80BC55D38C36FCLL, case1) {
        return (t_case1(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x510651ED53E5FBCELL, case5) {
        return (t_case5(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3594B01D88AF36A0LL, case2) {
        return (t_case2(), null);
      }
      HASH_GUARD(0x64E501A2E5751960LL, case3) {
        return (t_case3(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x31A9CEB2F6C59965LL, case4) {
        return (t_case4(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x4EA46759920A1E06LL, case6) {
        return (t_case6(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x1F80BC55D38C36FCLL, case1) {
        return (t_case1(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x510651ED53E5FBCELL, case5) {
        return (t_case5(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  m_array = ScalarArrays::sa_[0];
  m_string = "string";
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 13 */
void c_test::t___construct() {
  INSTANCE_METHOD_INJECTION(test, test::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("object", 0x7F30BC4E222B1861LL) = ((Object)(LINE(14,p_test_props(p_test_props(NEWOBJ(c_test_props)())->create())))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 17 */
Array c_test::t_getarray() {
  INSTANCE_METHOD_INJECTION(test, test::getArray);
  return m_array;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 21 */
Variant c_test::t_getstring() {
  INSTANCE_METHOD_INJECTION(test, test::getString);
  return m_string;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 25 */
void c_test::t_case1() {
  INSTANCE_METHOD_INJECTION(test, test::case1);
  Variant v_foo;

  {
    LOOP_COUNTER(1);
    Variant map2 = m_array;
    for (ArrayIterPtr iter3 = map2.begin("test"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_foo = iter3->second();
      {
        echo(toString(v_foo));
      }
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 31 */
void c_test::t_case2() {
  INSTANCE_METHOD_INJECTION(test, test::case2);
  Variant v_foo;

  {
    LOOP_COUNTER(4);
    Variant map5 = o_get("foobar", 0x78BF256A72B2C70DLL);
    for (ArrayIterPtr iter6 = map5.begin("test"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_foo = iter6->second();
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 35 */
void c_test::t_case3() {
  INSTANCE_METHOD_INJECTION(test, test::case3);
  Variant v_foo;

  {
    LOOP_COUNTER(7);
    Variant map8 = m_string;
    for (ArrayIterPtr iter9 = map8.begin("test"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_foo = iter9->second();
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 39 */
void c_test::t_case4() {
  INSTANCE_METHOD_INJECTION(test, test::case4);
  Variant v_foo;

  {
    LOOP_COUNTER(10);
    Variant map11 = LINE(40,t_getarray());
    for (ArrayIterPtr iter12 = map11.begin("test"); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_foo = iter12->second();
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 43 */
void c_test::t_case5() {
  INSTANCE_METHOD_INJECTION(test, test::case5);
  Variant v_foo;

  {
    LOOP_COUNTER(13);
    Variant map14 = LINE(44,t_getstring());
    for (ArrayIterPtr iter15 = map14.begin("test"); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_foo = iter15->second();
    }
  }
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php line 47 */
void c_test::t_case6() {
  INSTANCE_METHOD_INJECTION(test, test::case6);
  Variant v_foo;

  {
    LOOP_COUNTER(16);
    Variant map17 = o_get("object", 0x7F30BC4E222B1861LL);
    for (ArrayIterPtr iter18 = map17.begin("test"); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_foo = iter18->second();
      {
        echo(toString(v_foo));
      }
    }
  }
} /* function */
Object co_test_props(CArrRef params, bool init /* = true */) {
  return Object(p_test_props(NEW(c_test_props)())->dynCreate(params, init));
}
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug27439_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug27439.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug27439_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  (v_test = ((Object)(LINE(53,p_test(p_test(NEWOBJ(c_test)())->create())))));
  LINE(54,v_test.o_invoke_few_args("case1", 0x1F80BC55D38C36FCLL, 0));
  LINE(55,v_test.o_invoke_few_args("case2", 0x3594B01D88AF36A0LL, 0));
  LINE(56,v_test.o_invoke_few_args("case3", 0x64E501A2E5751960LL, 0));
  LINE(57,v_test.o_invoke_few_args("case4", 0x31A9CEB2F6C59965LL, 0));
  LINE(58,v_test.o_invoke_few_args("case5", 0x510651ED53E5FBCELL, 0));
  LINE(59,v_test.o_invoke_few_args("case6", 0x4EA46759920A1E06LL, 0));
  echo("\n");
  echo("===DONE===");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
