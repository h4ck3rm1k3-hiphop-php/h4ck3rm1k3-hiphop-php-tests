
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.002.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.002.php line 7 */
Variant f_returnval() {
  FUNCTION_INJECTION(returnVal);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return gv_a;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.002.php line 2 */
Variant f_returnref() {
  FUNCTION_INJECTION(returnRef);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  return ref(gv_a);
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_002_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.002.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_002_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = "original");
  (v_b = ref(LINE(13,f_returnval())));
  (v_b = "changed");
  LINE(15,x_var_dump(1, v_a));
  (v_a = "original");
  (v_b = ref(LINE(18,f_returnref())));
  (v_b = "changed");
  LINE(20,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
