
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 2 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("list", m_list.isReferenced() ? ref(m_list) : m_list));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x507C12C34EF2AF32LL, list, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x507C12C34EF2AF32LL, m_list,
                         list, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x507C12C34EF2AF32LL, m_list,
                      list, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x507C12C34EF2AF32LL, m_list,
                         list, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_list = m_list.isReferenced() ? ref(m_list) : m_list;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x797B42C72E405E22LL, method2) {
        return ref(t_method2());
      }
      break;
    case 4:
      HASH_GUARD(0x32A58B0CE493957CLL, finalize) {
        return (t_finalize(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x403790E2ECACC8EDLL, method1) {
        return ref(t_method1());
      }
      break;
    case 7:
      HASH_GUARD(0x7344FB3D600FB4CFLL, method3) {
        return (t_method3());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x797B42C72E405E22LL, method2) {
        return ref(t_method2());
      }
      break;
    case 4:
      HASH_GUARD(0x32A58B0CE493957CLL, finalize) {
        return (t_finalize(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x403790E2ECACC8EDLL, method1) {
        return ref(t_method1());
      }
      break;
    case 7:
      HASH_GUARD(0x7344FB3D600FB4CFLL, method3) {
        return (t_method3());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_list = ScalarArrays::sa_[0];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 6 */
void c_foo::t_finalize() {
  INSTANCE_METHOD_INJECTION(foo, foo::finalize);
  Variant v_cl;

  print("foo::finalize\n");
  (v_cl = ref(lval(m_list)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 11 */
Variant c_foo::t_method1() {
  INSTANCE_METHOD_INJECTION(foo, foo::method1);
  print("foo::method1\n");
  return (silenceInc(), silenceDec(o_get("foo", 0x4154FA2EF733DA8FLL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 16 */
Variant c_foo::t_method2() {
  INSTANCE_METHOD_INJECTION(foo, foo::method2);
  print("foo::method2\n");
  return ref(lval(o_lval("foo", 0x4154FA2EF733DA8FLL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 21 */
Variant c_foo::t_method3() {
  INSTANCE_METHOD_INJECTION(foo, foo::method3);
  print("foo::method3\n");
  return (silenceInc(), silenceDec(o_get("foo", 0x4154FA2EF733DA8FLL)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 27 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x09195D14A769E460LL, run3) {
        return (t_run3(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x146F3A2013334C49LL, run1) {
        return (t_run1(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x5497E84B0CC522C5LL, run2) {
        return (t_run2(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x09195D14A769E460LL, run3) {
        return (t_run3(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x146F3A2013334C49LL, run1) {
        return (t_run1(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x5497E84B0CC522C5LL, run2) {
        return (t_run2(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 29 */
void c_bar::t_run1() {
  INSTANCE_METHOD_INJECTION(bar, bar::run1);
  print("bar::run1\n");
  (o_lval("instance", 0x24965F78CA389518LL) = ((Object)(LINE(31,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(32,o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("method1", 0x403790E2ECACC8EDLL, 1, this));
  LINE(33,o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("method1", 0x403790E2ECACC8EDLL, 1, this));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 36 */
void c_bar::t_run2() {
  INSTANCE_METHOD_INJECTION(bar, bar::run2);
  print("bar::run2\n");
  (o_lval("instance", 0x24965F78CA389518LL) = ((Object)(LINE(38,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(39,o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("method2", 0x797B42C72E405E22LL, 1, this));
  LINE(40,o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("method2", 0x797B42C72E405E22LL, 1, this));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 43 */
void c_bar::t_run3() {
  INSTANCE_METHOD_INJECTION(bar, bar::run3);
  print("bar::run3\n");
  (o_lval("instance", 0x24965F78CA389518LL) = ((Object)(LINE(45,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(46,o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("method3", 0x7344FB3D600FB4CFLL, 1, this));
  LINE(47,o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("method3", 0x7344FB3D600FB4CFLL, 1, this));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 51 */
void f_ouch(Variant v_bar) {
  FUNCTION_INJECTION(ouch);
  Variant v_a;

  print("ouch\n");
  (silenceInc(), silenceDec((v_a = v_a)));
  LINE(54,toObject(v_bar)->o_invoke_few_args("run1", 0x146F3A2013334C49LL, 0));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 57 */
void f_ok1(Variant v_bar) {
  FUNCTION_INJECTION(ok1);
  print("ok1\n");
  LINE(59,toObject(v_bar)->o_invoke_few_args("run1", 0x146F3A2013334C49LL, 0));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 62 */
void f_ok2(Variant v_bar) {
  FUNCTION_INJECTION(ok2);
  Variant v_a;

  print("ok2\n");
  (silenceInc(), silenceDec((v_a = v_a)));
  LINE(65,toObject(v_bar)->o_invoke_few_args("run2", 0x5497E84B0CC522C5LL, 0));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php line 68 */
void f_ok3(Variant v_bar) {
  FUNCTION_INJECTION(ok3);
  Variant v_a;

  print("ok3\n");
  (silenceInc(), silenceDec((v_a = v_a)));
  LINE(71,toObject(v_bar)->o_invoke_few_args("run3", 0x09195D14A769E460LL, 0));
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug22510_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug22510.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug22510_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_bar = LINE(74,p_bar(p_bar(NEWOBJ(c_bar)())->create())));
  LINE(75,f_ok1(ref(v_bar)));
  LINE(76,v_bar.o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("finalize", 0x32A58B0CE493957CLL, 0));
  print("done!\n");
  LINE(78,f_ok2(ref(v_bar)));
  LINE(79,v_bar.o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("finalize", 0x32A58B0CE493957CLL, 0));
  print("done!\n");
  LINE(81,f_ok3(ref(v_bar)));
  LINE(82,v_bar.o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("finalize", 0x32A58B0CE493957CLL, 0));
  print("done!\n");
  LINE(84,f_ouch(ref(v_bar)));
  LINE(85,v_bar.o_get("instance", 0x24965F78CA389518LL).o_invoke_few_args("finalize", 0x32A58B0CE493957CLL, 0));
  print("I'm alive!\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
