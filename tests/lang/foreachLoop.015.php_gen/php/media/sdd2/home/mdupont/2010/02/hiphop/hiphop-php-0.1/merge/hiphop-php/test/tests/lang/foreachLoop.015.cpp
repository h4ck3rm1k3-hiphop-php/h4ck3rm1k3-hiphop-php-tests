
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.015.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_MAX_LOOPS = 5LL;

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.015.php line 5 */
void f_withrefvalue(Variant v_elements, Variant v_transform) {
  FUNCTION_INJECTION(withRefValue);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_i;
  Variant v_a;
  Variant v_counter;
  Variant v_ref;
  Variant v_k;
  Variant v_v;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_elements; Variant &v_transform; Variant &v_i; Variant &v_a; Variant &v_counter; Variant &v_ref; Variant &v_k; Variant &v_v;
    VariableTable(Variant &r_elements, Variant &r_transform, Variant &r_i, Variant &r_a, Variant &r_counter, Variant &r_ref, Variant &r_k, Variant &r_v) : v_elements(r_elements), v_transform(r_transform), v_i(r_i), v_a(r_a), v_counter(r_counter), v_ref(r_ref), v_k(r_k), v_v(r_v) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 15) {
        case 6:
          HASH_RETURN(0x1DC554137FBC6EF6LL, v_transform,
                      transform);
          break;
        case 8:
          HASH_RETURN(0x0EB22EDA95766E98LL, v_i,
                      i);
          HASH_RETURN(0x1297A95AAC6F5998LL, v_counter,
                      counter);
          break;
        case 10:
          HASH_RETURN(0x4292CEE227B9150ALL, v_a,
                      a);
          HASH_RETURN(0x0B1A6D25134FD5FALL, v_ref,
                      ref);
          break;
        case 11:
          HASH_RETURN(0x3C2F961831E4EF6BLL, v_v,
                      v);
          break;
        case 13:
          HASH_RETURN(0x0654BBB6DFA3D1ADLL, v_elements,
                      elements);
          break;
        case 15:
          HASH_RETURN(0x0D6857FDDD7F21CFLL, v_k,
                      k);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_elements, v_transform, v_i, v_a, v_counter, v_ref, v_k, v_v);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  echo(LINE(6,concat3("\n---( Array with ", toString(v_elements), " element(s): )---\n")));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_elements); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_a.append((toString("v.") + toString(v_i)));
      }
    }
  }
  (v_counter = 0LL);
  (v_ref = ref(v_a));
  echo("--> State of referenced array before loop:\n");
  LINE(16,x_var_dump(1, v_a));
  echo("--> Do loop:\n");
  {
    LOOP_COUNTER(2);
    Variant map3 = ref(v_a);
    map3.escalate();
    for (MutableArrayIterPtr iter4 = map3.begin(&v_k, v_v); iter4->advance();) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(concat("     iteration ", LINE(20,concat6(toString(v_counter), ":  $k=", toString(v_k), "; $v=", toString(v_v), "\n"))));
        f_eval(toString(v_transform));
        v_counter++;
        if (more(v_counter, 5LL /* MAX_LOOPS */)) {
          echo("  ** Stuck in a loop! **\n");
          break;
        }
      }
    }
  }
  echo("--> State of array after loop:\n");
  LINE(30,x_var_dump(1, v_a));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_015_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoop.015.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoop_015_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_transform __attribute__((__unused__)) = (variables != gVariables) ? variables->get("transform") : g->GV(transform);

  ;
  echo("\nPopping elements off end of a referenced array, using &$value");
  (v_transform = "array_pop($a);");
  LINE(36,f_withrefvalue(1LL, v_transform));
  LINE(37,f_withrefvalue(2LL, v_transform));
  LINE(38,f_withrefvalue(3LL, v_transform));
  LINE(39,f_withrefvalue(4LL, v_transform));
  echo("\n\n\nShift elements off start of a referenced array, using &$value");
  (v_transform = "array_shift($a);");
  LINE(43,f_withrefvalue(1LL, v_transform));
  LINE(44,f_withrefvalue(2LL, v_transform));
  LINE(45,f_withrefvalue(3LL, v_transform));
  LINE(46,f_withrefvalue(4LL, v_transform));
  echo("\n\n\nRemove current element of a referenced array, using &$value");
  (v_transform = "unset($a[$k]);");
  LINE(50,f_withrefvalue(1LL, v_transform));
  LINE(51,f_withrefvalue(2LL, v_transform));
  LINE(52,f_withrefvalue(3LL, v_transform));
  LINE(53,f_withrefvalue(4LL, v_transform));
  echo("\n\n\nAdding elements to the end of a referenced array, using &$value");
  (v_transform = "array_push($a, \"new.$counter\");");
  LINE(57,f_withrefvalue(1LL, v_transform));
  LINE(58,f_withrefvalue(2LL, v_transform));
  LINE(59,f_withrefvalue(3LL, v_transform));
  LINE(60,f_withrefvalue(4LL, v_transform));
  echo("\n\n\nAdding elements to the start of a referenced array, using &$value");
  (v_transform = "array_unshift($a, \"new.$counter\");");
  LINE(64,f_withrefvalue(1LL, v_transform));
  LINE(65,f_withrefvalue(2LL, v_transform));
  LINE(66,f_withrefvalue(3LL, v_transform));
  LINE(67,f_withrefvalue(4LL, v_transform));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
