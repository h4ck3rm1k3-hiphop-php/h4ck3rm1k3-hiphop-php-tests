
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24640.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24640.php line 2 */
void f_test(double v_v) {
  FUNCTION_INJECTION(test);
  echo(concat(toString(LINE(4,x_var_export(v_v, true))), "\n"));
  LINE(5,x_var_dump(1, v_v));
  echo(toString(v_v) + toString("\n"));
  LINE(7,x_print_r(v_v));
  echo("\n------\n");
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24640_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug24640.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug24640_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(11,f_test(1.7000000000000001E+300));
  LINE(12,f_test(1.7000000000000001E-300));
  LINE(13,f_test(1.7000000000000002E+79));
  LINE(14,f_test(1.6999999999999999E-79));
  LINE(15,f_test(1.7E+80));
  LINE(16,f_test(1.7E-80));
  LINE(17,f_test(1.7E+81));
  LINE(18,f_test(1.6999999999999999E-81));
  LINE(19,f_test(Limits::inf_double));
  LINE(20,f_test(1.6999810742105611E-319));
  LINE(21,f_test(Limits::inf_double));
  LINE(22,f_test(1.7000798873397294E-320));
  LINE(23,f_test(Limits::inf_double));
  LINE(24,f_test(1.6995858216938881E-321));
  LINE(25,f_test(Limits::inf_double));
  LINE(26,f_test(0.0));
  LINE(27,f_test(Limits::inf_double));
  LINE(28,f_test(0.0));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
