
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug18872.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug18872.php line 2 */
const int64 q_foobar_BIFF = 3LL;
Variant c_foobar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foobar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foobar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foobar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foobar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foobar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foobar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foobar::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x1343B70CF2B982DCLL, q_foobar_BIFF, BIFF);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foobar)
ObjectData *c_foobar::cloneImpl() {
  c_foobar *obj = NEW(c_foobar)();
  cloneSet(obj);
  return obj;
}
void c_foobar::cloneSet(c_foobar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foobar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foobar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foobar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foobar$os_get(const char *s) {
  return c_foobar::os_get(s, -1);
}
Variant &cw_foobar$os_lval(const char *s) {
  return c_foobar::os_lval(s, -1);
}
Variant cw_foobar$os_constant(const char *s) {
  return c_foobar::os_constant(s);
}
Variant cw_foobar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foobar::os_invoke(c, s, params, -1, fatal);
}
void c_foobar::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug18872.php line 6 */
void f_foo(int64 v_biff //  = 3LL /* foobar::BIFF */
) {
  FUNCTION_INJECTION(foo);
  echo(concat(toString(v_biff), "\n"));
} /* function */
Object co_foobar(CArrRef params, bool init /* = true */) {
  return Object(p_foobar(NEW(c_foobar)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug18872_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug18872.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug18872_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(10,f_foo());
  LINE(11,f_foo());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
