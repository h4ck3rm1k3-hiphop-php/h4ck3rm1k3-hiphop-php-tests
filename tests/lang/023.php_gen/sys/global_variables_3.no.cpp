
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x4B27011F259D2446LL, j, 16);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 12);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x0D6857FDDD7F21CFLL, k, 18);
      HASH_INDEX(0x3BE238B514802B8FLL, days, 23);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x3A1DC49AA6CFEA13LL, test, 13);
      HASH_INDEX(0x49B1816976A1D113LL, blah, 14);
      break;
    case 20:
      HASH_INDEX(0x431BBB0ACB16B5D4LL, arr_len, 17);
      break;
    case 21:
      HASH_INDEX(0x48AB75EE88EA8115LL, wedding_date, 26);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 15);
      break;
    case 26:
      HASH_INDEX(0x7254CFD0EBD34B1ALL, time_left, 22);
      break;
    case 33:
      HASH_INDEX(0x2B17A96EB6DAA7A1LL, minutes, 25);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      HASH_INDEX(0x05CAFC91A9B37763LL, q, 20);
      break;
    case 38:
      HASH_INDEX(0x699674A9E26E1066LL, hours, 24);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 52:
      HASH_INDEX(0x573F3D2E97212B34LL, resize, 19);
      break;
    case 56:
      HASH_INDEX(0x753E9785D72F93F8LL, wedding_timestamp, 21);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 27) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
