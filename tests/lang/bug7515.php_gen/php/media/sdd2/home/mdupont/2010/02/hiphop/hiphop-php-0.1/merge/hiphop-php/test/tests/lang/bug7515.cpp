
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug7515.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug7515.php line 2 */
Variant c_obj::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_obj::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_obj::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_obj::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_obj::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_obj::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_obj::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_obj::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(obj)
ObjectData *c_obj::cloneImpl() {
  c_obj *obj = NEW(c_obj)();
  cloneSet(obj);
  return obj;
}
void c_obj::cloneSet(c_obj *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_obj::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x4C211FA78927D132LL, method) {
        return (t_method(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_obj::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x4C211FA78927D132LL, method) {
        return (t_method(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_obj::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_obj$os_get(const char *s) {
  return c_obj::os_get(s, -1);
}
Variant &cw_obj$os_lval(const char *s) {
  return c_obj::os_lval(s, -1);
}
Variant cw_obj$os_constant(const char *s) {
  return c_obj::os_constant(s);
}
Variant cw_obj$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_obj::os_invoke(c, s, params, -1, fatal);
}
void c_obj::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug7515.php line 3 */
void c_obj::t_method() {
  INSTANCE_METHOD_INJECTION(obj, obj::method);
} /* function */
Object co_obj(CArrRef params, bool init /* = true */) {
  return Object(p_obj(NEW(c_obj)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug7515_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug7515.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug7515_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_o.o_lval("root", 0x05E3DE381E45A0DBLL) = ((Object)(LINE(6,p_obj(p_obj(NEWOBJ(c_obj)())->create())))));
  LINE(8,x_ob_start());
  LINE(9,x_var_dump(1, v_o));
  (v_x = LINE(10,x_ob_get_contents()));
  LINE(11,x_ob_end_clean());
  LINE(13,v_o.o_get("root", 0x05E3DE381E45A0DBLL).o_invoke_few_args("method", 0x4C211FA78927D132LL, 0));
  LINE(15,x_ob_start());
  LINE(16,x_var_dump(1, v_o));
  (v_y = LINE(17,x_ob_get_contents()));
  LINE(18,x_ob_end_clean());
  if (equal(v_x, v_y)) {
    print("success");
  }
  else {
    print(LINE(26,concat5("failure\nx=", toString(v_x), "\ny=", toString(v_y), "\n")));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
