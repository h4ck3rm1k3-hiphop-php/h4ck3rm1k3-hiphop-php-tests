
#ifndef __GENERATED_cls_myiterator_h__
#define __GENERATED_cls_myiterator_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIterator.002.php line 3 */
class c_myiterator : virtual public c_iterator {
  BEGIN_CLASS_MAP(myiterator)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iterator)
  END_CLASS_MAP(myiterator)
  DECLARE_CLASS(myiterator, MyIterator, ObjectData)
  void init();
  public: bool t_valid();
  public: void t_next();
  public: void t_rewind();
  public: void t_current();
  public: void t_key();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myiterator_h__
