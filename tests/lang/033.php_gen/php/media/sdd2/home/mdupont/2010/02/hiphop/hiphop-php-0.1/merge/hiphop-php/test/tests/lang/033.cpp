
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/033.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$033_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/033.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$033_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 1LL);
  echo("If: ");
  if (toBoolean(v_a)) echo(toString(1LL));
  else echo(toString(0LL));
  if (toBoolean(v_a)) {
    echo(toString(1LL));
  }
  else {
    echo(toString(0LL));
  }
  echo("\nWhile: ");
  LOOP_COUNTER(1);
  {
    while (less(v_a, 5LL)) {
      LOOP_COUNTER_CHECK(1);
      echo(toString(v_a++));
    }
  }
  LOOP_COUNTER(2);
  {
    while (less(v_a, 9LL)) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(toString(++v_a));
      }
    }
  }
  echo("\nFor: ");
  {
    LOOP_COUNTER(3);
    for ((v_a = 0LL); less(v_a, 5LL); v_a++) {
      LOOP_COUNTER_CHECK(3);
      echo(toString(v_a));
    }
  }
  {
    LOOP_COUNTER(4);
    for ((v_a = 0LL); less(v_a, 5LL); v_a++) {
      LOOP_COUNTER_CHECK(4);
      {
        echo(toString(v_a));
      }
    }
  }
  echo("\nSwitch: ");
  {
    switch (toInt64(v_a)) {
    case 0LL:
      {
        echo(toString(0LL));
        break;
      }
    case 5LL:
      {
        echo(toString(1LL));
        break;
      }
    default:
      {
        echo(toString(0LL));
        break;
      }
    }
  }
  echo("\n===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
