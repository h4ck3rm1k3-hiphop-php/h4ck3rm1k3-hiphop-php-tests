
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.php line 2 */
Variant c_first::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_first::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_first::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_first::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_first::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_first::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_first::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_first::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(first)
ObjectData *c_first::cloneImpl() {
  c_first *obj = NEW(c_first)();
  cloneSet(obj);
  return obj;
}
void c_first::cloneSet(c_first *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_first::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x0B16FF3C75FA4C02LL, me) {
        return (t_me(), null);
      }
      HASH_GUARD(0x764726F7F631ABE2LL, who) {
        return (t_who(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_first::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x0B16FF3C75FA4C02LL, me) {
        return (t_me(), null);
      }
      HASH_GUARD(0x764726F7F631ABE2LL, who) {
        return (t_who(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_first::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_first$os_get(const char *s) {
  return c_first::os_get(s, -1);
}
Variant &cw_first$os_lval(const char *s) {
  return c_first::os_lval(s, -1);
}
Variant cw_first$os_constant(const char *s) {
  return c_first::os_constant(s);
}
Variant cw_first$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_first::os_invoke(c, s, params, -1, fatal);
}
void c_first::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.php line 4 */
void c_first::t_me() {
  INSTANCE_METHOD_INJECTION(first, first::me);
  echo("first");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.php line 6 */
void c_first::t_who() {
  INSTANCE_METHOD_INJECTION(first, first::who);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  Variant &gv_b __attribute__((__unused__)) = g->GV(b);
  {
  }
  LINE(8,t_me());
  LINE(9,gv_a.o_invoke_few_args("me", 0x0B16FF3C75FA4C02LL, 0));
  LINE(10,gv_b.o_invoke_few_args("me", 0x0B16FF3C75FA4C02LL, 0));
  (gv_b = ((Object)(LINE(11,p_second(p_second(NEWOBJ(c_second)())->create())))));
  LINE(12,t_me());
  LINE(13,gv_a.o_invoke_few_args("me", 0x0B16FF3C75FA4C02LL, 0));
  LINE(14,gv_b.o_invoke_few_args("me", 0x0B16FF3C75FA4C02LL, 0));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.php line 18 */
Variant c_second::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_second::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_second::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_second::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_second::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_second::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_second::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_second::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(second)
ObjectData *c_second::cloneImpl() {
  c_second *obj = NEW(c_second)();
  cloneSet(obj);
  return obj;
}
void c_second::cloneSet(c_second *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_second::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x0B16FF3C75FA4C02LL, me) {
        return (t_me(), null);
      }
      HASH_GUARD(0x764726F7F631ABE2LL, who) {
        return (t_who(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_second::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x0B16FF3C75FA4C02LL, me) {
        return (t_me(), null);
      }
      HASH_GUARD(0x764726F7F631ABE2LL, who) {
        return (t_who(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_second::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_second$os_get(const char *s) {
  return c_second::os_get(s, -1);
}
Variant &cw_second$os_lval(const char *s) {
  return c_second::os_lval(s, -1);
}
Variant cw_second$os_constant(const char *s) {
  return c_second::os_constant(s);
}
Variant cw_second$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_second::os_invoke(c, s, params, -1, fatal);
}
void c_second::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.php line 20 */
void c_second::t_who() {
  INSTANCE_METHOD_INJECTION(second, second::who);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  Variant &gv_b __attribute__((__unused__)) = g->GV(b);
  {
  }
  LINE(22,t_me());
  LINE(23,gv_a.o_invoke_few_args("me", 0x0B16FF3C75FA4C02LL, 0));
  LINE(24,gv_b.o_invoke_few_args("me", 0x0B16FF3C75FA4C02LL, 0));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.php line 26 */
void c_second::t_me() {
  INSTANCE_METHOD_INJECTION(second, second::me);
  echo("second");
} /* function */
Object co_first(CArrRef params, bool init /* = true */) {
  return Object(p_first(NEW(c_first)())->dynCreate(params, init));
}
Object co_second(CArrRef params, bool init /* = true */) {
  return Object(p_second(NEW(c_second)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$this_assignment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/this_assignment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$this_assignment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = ((Object)(LINE(29,p_first(p_first(NEWOBJ(c_first)())->create())))));
  (v_b = ref(v_a));
  LINE(32,v_a.o_invoke_few_args("who", 0x764726F7F631ABE2LL, 0));
  LINE(33,v_b.o_invoke_few_args("who", 0x764726F7F631ABE2LL, 0));
  echo("\n");
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
