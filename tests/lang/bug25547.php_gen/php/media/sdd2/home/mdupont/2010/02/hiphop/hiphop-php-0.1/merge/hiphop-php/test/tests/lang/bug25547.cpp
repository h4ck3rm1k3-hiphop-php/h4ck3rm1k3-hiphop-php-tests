
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25547.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25547.php line 3 */
void f_handler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline, CVarRef v_context) {
  FUNCTION_INJECTION(handler);
  echo(concat("handler", LINE(5,concat3("(", toString(v_errstr), ")\n"))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25547.php line 10 */
String f_foo(CVarRef v_x) {
  FUNCTION_INJECTION(foo);
  return "foo";
} /* function */
Variant i_handler(CArrRef params) {
  return (f_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
}
Variant i_foo(CArrRef params) {
  return (f_foo(params.rvalAt(0)));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug25547_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug25547.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$bug25547_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_output __attribute__((__unused__)) = (variables != gVariables) ? variables->get("output") : g->GV(output);

  LINE(8,x_set_error_handler("handler"));
  (v_output = ScalarArrays::sa_[0]);
  ++lval(v_output.lvalAt(LINE(15,f_foo("bar"))));
  LINE(17,x_print_r(v_output));
  echo("Done");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
