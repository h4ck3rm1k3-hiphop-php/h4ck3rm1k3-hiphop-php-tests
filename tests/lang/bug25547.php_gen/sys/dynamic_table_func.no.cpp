
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_handler(CArrRef params);
Variant i_foo(CArrRef params);
static Variant invoke_case_0(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x48E3BEA2F1B19618LL, handler);
  HASH_INVOKE(0x6176C0B993BF5914LL, foo);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[4])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 4; i++) funcTable[i] = &invoke_builtin;
    funcTable[0] = &invoke_case_0;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 3](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
