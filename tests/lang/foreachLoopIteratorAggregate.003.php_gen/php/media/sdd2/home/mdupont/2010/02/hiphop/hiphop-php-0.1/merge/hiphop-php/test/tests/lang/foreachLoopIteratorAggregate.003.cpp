
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 2 */
Variant c_englishmealiterator::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_englishmealiterator::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_englishmealiterator::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("pos", m_pos));
  props.push_back(NEW(ArrayElement)("myContent", m_myContent));
  c_ObjectData::o_get(props);
}
bool c_englishmealiterator::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x0CF096B3F7233879LL, myContent, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x277B6D36F75741AELL, pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_englishmealiterator::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x0CF096B3F7233879LL, m_myContent,
                         myContent, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x277B6D36F75741AELL, m_pos,
                         pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_englishmealiterator::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x0CF096B3F7233879LL, m_myContent,
                      myContent, 9);
      break;
    case 2:
      HASH_SET_STRING(0x277B6D36F75741AELL, m_pos,
                      pos, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_englishmealiterator::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_englishmealiterator::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(englishmealiterator)
ObjectData *c_englishmealiterator::cloneImpl() {
  c_englishmealiterator *obj = NEW(c_englishmealiterator)();
  cloneSet(obj);
  return obj;
}
void c_englishmealiterator::cloneSet(c_englishmealiterator *clone) {
  clone->m_pos = m_pos;
  clone->m_myContent = m_myContent;
  ObjectData::cloneSet(clone);
}
Variant c_englishmealiterator::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_englishmealiterator::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_englishmealiterator::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_englishmealiterator$os_get(const char *s) {
  return c_englishmealiterator::os_get(s, -1);
}
Variant &cw_englishmealiterator$os_lval(const char *s) {
  return c_englishmealiterator::os_lval(s, -1);
}
Variant cw_englishmealiterator$os_constant(const char *s) {
  return c_englishmealiterator::os_constant(s);
}
Variant cw_englishmealiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_englishmealiterator::os_invoke(c, s, params, -1, fatal);
}
void c_englishmealiterator::init() {
  m_pos = 0LL;
  m_myContent = ScalarArrays::sa_[0];
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 6 */
bool c_englishmealiterator::t_valid() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::valid);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(8,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::valid", eo_2))));
  return less(m_pos, 3LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 12 */
Variant c_englishmealiterator::t_next() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::next);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(14,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::next", eo_2))));
  return m_myContent.rvalAt(m_pos++);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 18 */
void c_englishmealiterator::t_rewind() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::rewind);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(20,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::rewind", eo_2))));
  (m_pos = 0LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 24 */
Variant c_englishmealiterator::t_current() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::current);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(26,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::current", eo_2))));
  return m_myContent.rvalAt(m_pos);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 30 */
String c_englishmealiterator::t_key() {
  INSTANCE_METHOD_INJECTION(EnglishMealIterator, EnglishMealIterator::key);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_indent __attribute__((__unused__)) = g->GV(indent);
  echo(LINE(32,(assignCallTemp(eo_0, toString(gv_indent) + toString("--> ")),assignCallTemp(eo_2, concat3(" (", toString(m_pos), ")\n")),concat3(eo_0, "EnglishMealIterator::key", eo_2))));
  return concat("meal ", toString(m_pos));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 38 */
Variant c_a1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a1::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a1::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a1::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a1::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a1)
ObjectData *c_a1::cloneImpl() {
  c_a1 *obj = NEW(c_a1)();
  cloneSet(obj);
  return obj;
}
void c_a1::cloneSet(c_a1 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a1$os_get(const char *s) {
  return c_a1::os_get(s, -1);
}
Variant &cw_a1$os_lval(const char *s) {
  return c_a1::os_lval(s, -1);
}
Variant cw_a1$os_constant(const char *s) {
  return c_a1::os_constant(s);
}
Variant cw_a1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a1::os_invoke(c, s, params, -1, fatal);
}
void c_a1::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 39 */
p_englishmealiterator c_a1::t_getiterator() {
  INSTANCE_METHOD_INJECTION(A1, A1::getIterator);
  return ((Object)(LINE(40,p_englishmealiterator(p_englishmealiterator(NEWOBJ(c_englishmealiterator)())->create()))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 44 */
Variant c_a2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a2)
ObjectData *c_a2::cloneImpl() {
  c_a2 *obj = NEW(c_a2)();
  cloneSet(obj);
  return obj;
}
void c_a2::cloneSet(c_a2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a2$os_get(const char *s) {
  return c_a2::os_get(s, -1);
}
Variant &cw_a2$os_lval(const char *s) {
  return c_a2::os_lval(s, -1);
}
Variant cw_a2$os_constant(const char *s) {
  return c_a2::os_constant(s);
}
Variant cw_a2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a2::os_invoke(c, s, params, -1, fatal);
}
void c_a2::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 45 */
p_a1 c_a2::t_getiterator() {
  INSTANCE_METHOD_INJECTION(A2, A2::getIterator);
  return ((Object)(LINE(46,p_a1(p_a1(NEWOBJ(c_a1)())->create()))));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 50 */
Variant c_a3::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a3::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a3::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a3::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a3::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a3::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a3::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a3::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a3)
ObjectData *c_a3::cloneImpl() {
  c_a3 *obj = NEW(c_a3)();
  cloneSet(obj);
  return obj;
}
void c_a3::cloneSet(c_a3 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a3::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a3::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a3::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a3$os_get(const char *s) {
  return c_a3::os_get(s, -1);
}
Variant &cw_a3$os_lval(const char *s) {
  return c_a3::os_lval(s, -1);
}
Variant cw_a3$os_constant(const char *s) {
  return c_a3::os_constant(s);
}
Variant cw_a3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a3::os_invoke(c, s, params, -1, fatal);
}
void c_a3::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 51 */
p_a2 c_a3::t_getiterator() {
  INSTANCE_METHOD_INJECTION(A3, A3::getIterator);
  return ((Object)(LINE(52,p_a2(p_a2(NEWOBJ(c_a2)())->create()))));
} /* function */
Object co_englishmealiterator(CArrRef params, bool init /* = true */) {
  return Object(p_englishmealiterator(NEW(c_englishmealiterator)())->dynCreate(params, init));
}
Object co_a1(CArrRef params, bool init /* = true */) {
  return Object(p_a1(NEW(c_a1)())->dynCreate(params, init));
}
Object co_a2(CArrRef params, bool init /* = true */) {
  return Object(p_a2(NEW(c_a2)())->dynCreate(params, init));
}
Object co_a3(CArrRef params, bool init /* = true */) {
  return Object(p_a3(NEW(c_a3)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIteratorAggregate_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$foreachLoopIteratorAggregate_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("\n-----( A1: )-----\n");
  {
    LOOP_COUNTER(1);
    Variant map2 = ((Object)(LINE(57,p_a1(p_a1(NEWOBJ(c_a1)())->create()))));
    for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        echo(LINE(58,concat4(toString(v_k), " => ", toString(v_v), "\n")));
      }
    }
  }
  echo("\n-----( A2: )-----\n");
  {
    LOOP_COUNTER(4);
    Variant map5 = ((Object)(LINE(62,p_a2(p_a2(NEWOBJ(c_a2)())->create()))));
    for (ArrayIterPtr iter6 = map5.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        echo(LINE(63,concat4(toString(v_k), " => ", toString(v_v), "\n")));
      }
    }
  }
  echo("\n-----( A3: )-----\n");
  {
    LOOP_COUNTER(7);
    Variant map8 = ((Object)(LINE(67,p_a3(p_a3(NEWOBJ(c_a3)())->create()))));
    for (ArrayIterPtr iter9 = map8.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
      v_k = iter9->first();
      {
        echo(LINE(68,concat4(toString(v_k), " => ", toString(v_v), "\n")));
      }
    }
  }
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
