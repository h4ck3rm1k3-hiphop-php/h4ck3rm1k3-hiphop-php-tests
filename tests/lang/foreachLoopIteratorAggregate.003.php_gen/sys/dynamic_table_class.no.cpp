
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_englishmealiterator(CArrRef params, bool init = true);
Variant cw_englishmealiterator$os_get(const char *s);
Variant &cw_englishmealiterator$os_lval(const char *s);
Variant cw_englishmealiterator$os_constant(const char *s);
Variant cw_englishmealiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_a1(CArrRef params, bool init = true);
Variant cw_a1$os_get(const char *s);
Variant &cw_a1$os_lval(const char *s);
Variant cw_a1$os_constant(const char *s);
Variant cw_a1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_a2(CArrRef params, bool init = true);
Variant cw_a2$os_get(const char *s);
Variant &cw_a2$os_lval(const char *s);
Variant cw_a2$os_constant(const char *s);
Variant cw_a2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_a3(CArrRef params, bool init = true);
Variant cw_a3$os_get(const char *s);
Variant &cw_a3$os_lval(const char *s);
Variant cw_a3$os_constant(const char *s);
Variant cw_a3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_CREATE_OBJECT(0x3538C0170C51A929LL, a2);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x2C36A9A38111F70ALL, a1);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x44989E63B44E4D6CLL, a3);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x3538C0170C51A929LL, a2);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x2C36A9A38111F70ALL, a1);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x44989E63B44E4D6CLL, a3);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x3538C0170C51A929LL, a2);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x2C36A9A38111F70ALL, a1);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x44989E63B44E4D6CLL, a3);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x3538C0170C51A929LL, a2);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x2C36A9A38111F70ALL, a1);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x44989E63B44E4D6CLL, a3);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x3538C0170C51A929LL, a2);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x2C36A9A38111F70ALL, a1);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x44989E63B44E4D6CLL, a3);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x2A683C78A9FE361FLL, englishmealiterator);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
