
#ifndef __GENERATED_cls_a1_h__
#define __GENERATED_cls_a1_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 38 */
class c_a1 : virtual public c_iteratoraggregate {
  BEGIN_CLASS_MAP(a1)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iteratoraggregate)
  END_CLASS_MAP(a1)
  DECLARE_CLASS(a1, A1, ObjectData)
  void init();
  public: p_englishmealiterator t_getiterator();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_a1_h__
