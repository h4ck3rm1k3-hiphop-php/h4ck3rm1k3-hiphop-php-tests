
#ifndef __GENERATED_cls_englishmealiterator_h__
#define __GENERATED_cls_englishmealiterator_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.003.php line 2 */
class c_englishmealiterator : virtual public c_iterator {
  BEGIN_CLASS_MAP(englishmealiterator)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iterator)
  END_CLASS_MAP(englishmealiterator)
  DECLARE_CLASS(englishmealiterator, EnglishMealIterator, ObjectData)
  void init();
  public: int64 m_pos;
  public: Array m_myContent;
  public: bool t_valid();
  public: Variant t_next();
  public: void t_rewind();
  public: Variant t_current();
  public: String t_key();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_englishmealiterator_h__
