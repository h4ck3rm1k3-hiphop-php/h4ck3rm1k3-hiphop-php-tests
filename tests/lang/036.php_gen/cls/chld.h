
#ifndef __GENERATED_cls_chld_h__
#define __GENERATED_cls_chld_h__

#include <cls/par.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/036.php line 11 */
class c_chld : virtual public c_par {
  BEGIN_CLASS_MAP(chld)
    PARENT_CLASS(par)
  END_CLASS_MAP(chld)
  DECLARE_CLASS(chld, chld, par)
  void init();
  public: void t_displayhim();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_chld_h__
