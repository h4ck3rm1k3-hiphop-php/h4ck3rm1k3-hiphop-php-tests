
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 2 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 3 */
void c_a::t_foo() {
  INSTANCE_METHOD_INJECTION(A, A::foo);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 6 */
Variant c_b::os_get(const char *s, int64 hash) {
  return c_a::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_a::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  c_a::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  return c_a::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  return c_a::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_a::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_a::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  return c_a::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  c_a::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_a::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
  c_a::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 7 */
void c_b::t_foo() {
  INSTANCE_METHOD_INJECTION(B, B::foo);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 10 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_b::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_b::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_b::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_b::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_b::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_b::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_b::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_b::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  c_b::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_b::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_b::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_b::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  c_b::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 11 */
void c_c::t_foo() {
  INSTANCE_METHOD_INJECTION(C, C::foo);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 14 */
Variant c_d::os_get(const char *s, int64 hash) {
  return c_a::os_get(s, hash);
}
Variant &c_d::os_lval(const char *s, int64 hash) {
  return c_a::os_lval(s, hash);
}
void c_d::o_get(ArrayElementVec &props) const {
  c_a::o_get(props);
}
bool c_d::o_exists(CStrRef s, int64 hash) const {
  return c_a::o_exists(s, hash);
}
Variant c_d::o_get(CStrRef s, int64 hash) {
  return c_a::o_get(s, hash);
}
Variant c_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_a::o_set(s, hash, v, forInit);
}
Variant &c_d::o_lval(CStrRef s, int64 hash) {
  return c_a::o_lval(s, hash);
}
Variant c_d::os_constant(const char *s) {
  return c_a::os_constant(s);
}
IMPLEMENT_CLASS(d)
ObjectData *c_d::cloneImpl() {
  c_d *obj = NEW(c_d)();
  cloneSet(obj);
  return obj;
}
void c_d::cloneSet(c_d *clone) {
  c_a::cloneSet(clone);
}
Variant c_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke(s, params, hash, fatal);
}
Variant c_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_a::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d$os_get(const char *s) {
  return c_d::os_get(s, -1);
}
Variant &cw_d$os_lval(const char *s) {
  return c_d::os_lval(s, -1);
}
Variant cw_d$os_constant(const char *s) {
  return c_d::os_constant(s);
}
Variant cw_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d::os_invoke(c, s, params, -1, fatal);
}
void c_d::init() {
  c_a::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 17 */
Variant c_f::os_get(const char *s, int64 hash) {
  return c_d::os_get(s, hash);
}
Variant &c_f::os_lval(const char *s, int64 hash) {
  return c_d::os_lval(s, hash);
}
void c_f::o_get(ArrayElementVec &props) const {
  c_d::o_get(props);
}
bool c_f::o_exists(CStrRef s, int64 hash) const {
  return c_d::o_exists(s, hash);
}
Variant c_f::o_get(CStrRef s, int64 hash) {
  return c_d::o_get(s, hash);
}
Variant c_f::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_d::o_set(s, hash, v, forInit);
}
Variant &c_f::o_lval(CStrRef s, int64 hash) {
  return c_d::o_lval(s, hash);
}
Variant c_f::os_constant(const char *s) {
  return c_d::os_constant(s);
}
IMPLEMENT_CLASS(f)
ObjectData *c_f::cloneImpl() {
  c_f *obj = NEW(c_f)();
  cloneSet(obj);
  return obj;
}
void c_f::cloneSet(c_f *clone) {
  c_d::cloneSet(clone);
}
Variant c_f::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_d::o_invoke(s, params, hash, fatal);
}
Variant c_f::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_d::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_f::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_d::os_invoke(c, s, params, hash, fatal);
}
Variant cw_f$os_get(const char *s) {
  return c_f::os_get(s, -1);
}
Variant &cw_f$os_lval(const char *s) {
  return c_f::os_lval(s, -1);
}
Variant cw_f$os_constant(const char *s) {
  return c_f::os_constant(s);
}
Variant cw_f$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_f::os_invoke(c, s, params, -1, fatal);
}
void c_f::init() {
  c_d::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 18 */
void c_f::t_foo() {
  INSTANCE_METHOD_INJECTION(F, F::foo);
} /* function */
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d(CArrRef params, bool init /* = true */) {
  return Object(p_d(NEW(c_d)())->dynCreate(params, init));
}
Object co_f(CArrRef params, bool init /* = true */) {
  return Object(p_f(NEW(c_f)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$032_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$032_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("OK\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
