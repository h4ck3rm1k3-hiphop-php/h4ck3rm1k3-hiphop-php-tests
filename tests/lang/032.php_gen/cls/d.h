
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__

#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 14 */
class c_d : virtual public c_a {
  BEGIN_CLASS_MAP(d)
    PARENT_CLASS(a)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, a)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
