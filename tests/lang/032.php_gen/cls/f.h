
#ifndef __GENERATED_cls_f_h__
#define __GENERATED_cls_f_h__

#include <cls/d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/032.php line 17 */
class c_f : virtual public c_d {
  BEGIN_CLASS_MAP(f)
    PARENT_CLASS(a)
    PARENT_CLASS(d)
  END_CLASS_MAP(f)
  DECLARE_CLASS(f, F, d)
  void init();
  public: void t_foo();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_f_h__
