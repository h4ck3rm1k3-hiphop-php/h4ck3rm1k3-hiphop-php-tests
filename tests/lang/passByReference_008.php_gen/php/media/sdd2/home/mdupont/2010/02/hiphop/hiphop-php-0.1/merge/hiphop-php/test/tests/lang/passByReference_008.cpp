
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_008.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_008.php line 8 */
void f_refval(Variant v_x, Variant v_y) {
  FUNCTION_INJECTION(refVal);
  LINE(9,x_var_dump(2, v_x, Array(ArrayInit(1).set(0, v_y).create())));
  (v_x = "changed.x");
  (v_y = "changed.y");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_008.php line 2 */
void f_valref(Variant v_x, Variant v_y) {
  FUNCTION_INJECTION(valRef);
  LINE(3,x_var_dump(2, v_x, Array(ArrayInit(1).set(0, v_y).create())));
  (v_x = "changed.x");
  (v_y = "changed.y");
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_008_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_008.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_008_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("\n\n-- Val, Ref --\n");
  (v_a = "original.a");
  LINE(17,f_valref(v_a, ref(v_a)));
  LINE(18,x_var_dump(1, v_a));
  echo("\n\n-- Ref, Val --\n");
  (v_b = "original.b");
  LINE(22,f_refval(ref(v_b), v_b));
  LINE(23,x_var_dump(1, v_b));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
