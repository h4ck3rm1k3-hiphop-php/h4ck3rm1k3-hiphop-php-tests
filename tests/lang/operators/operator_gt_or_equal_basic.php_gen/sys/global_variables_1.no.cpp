
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 8:
      HASH_RETURN(0x223766275C7DFA08LL, g->GV(invalid_int1),
                  invalid_int1);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 15:
      HASH_RETURN(0x27A4D5389686050FLL, g->GV(invalid_float1),
                  invalid_float1);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 21:
      HASH_RETURN(0x0C6074876F909395LL, g->GV(float1),
                  float1);
      break;
    case 24:
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 30:
      HASH_RETURN(0x22AB70710351EF1ELL, g->GV(invalid_float2),
                  invalid_float2);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 40:
      HASH_RETURN(0x7041ED00136182A8LL, g->GV(valid_float1),
                  valid_float1);
      break;
    case 47:
      HASH_RETURN(0x5A13C12A32BA05AFLL, g->GV(valid_compares),
                  valid_compares);
      break;
    case 49:
      HASH_RETURN(0x705CF4B70927EA31LL, g->GV(toCompare),
                  toCompare);
      break;
    case 50:
      HASH_RETURN(0x1AE92C90D341EE32LL, g->GV(int2),
                  int2);
      break;
    case 53:
      HASH_RETURN(0x055499C26E552935LL, g->GV(float2),
                  float2);
      break;
    case 56:
      HASH_RETURN(0x1C7B111915EB4DB8LL, g->GV(valid_float2),
                  valid_float2);
      break;
    case 60:
      HASH_RETURN(0x5A4EE8DE56E66A3CLL, g->GV(valid_false),
                  valid_false);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 70:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 71:
      HASH_RETURN(0x65CCC12FFAFD9647LL, g->GV(valid_int1),
                  valid_int1);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 81:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 99:
      HASH_RETURN(0x2B1CCCDFA7F10AE3LL, g->GV(failed),
                  failed);
      break;
    case 102:
      HASH_RETURN(0x5D8717CC04BC3C66LL, g->GV(valid_int2),
                  valid_int2);
      HASH_RETURN(0x081D58FD5DE44266LL, g->GV(typeToTest),
                  typeToTest);
      break;
    case 107:
      HASH_RETURN(0x56A8E1F4FADD65EBLL, g->GV(compareVal),
                  compareVal);
      break;
    case 109:
      HASH_RETURN(0x447F5EFF34810BEDLL, g->GV(invalid_int2),
                  invalid_int2);
      break;
    case 110:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 111:
      HASH_RETURN(0x6CABC2B2F8135FEFLL, g->GV(int1),
                  int1);
      break;
    case 116:
      HASH_RETURN(0x606859DECAD31E74LL, g->GV(invalid_compares),
                  invalid_compares);
      break;
    case 127:
      HASH_RETURN(0x27746B4821FFDBFFLL, g->GV(valid_true),
                  valid_true);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
