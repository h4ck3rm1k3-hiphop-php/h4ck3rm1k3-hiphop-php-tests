
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 8:
      HASH_INDEX(0x223766275C7DFA08LL, invalid_int1, 18);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x27A4D5389686050FLL, invalid_float1, 24);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x0C6074876F909395LL, float1, 20);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 28);
      break;
    case 30:
      HASH_INDEX(0x22AB70710351EF1ELL, invalid_float2, 25);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 40:
      HASH_INDEX(0x7041ED00136182A8LL, valid_float1, 22);
      break;
    case 47:
      HASH_INDEX(0x5A13C12A32BA05AFLL, valid_compares, 30);
      break;
    case 49:
      HASH_INDEX(0x705CF4B70927EA31LL, toCompare, 26);
      break;
    case 50:
      HASH_INDEX(0x1AE92C90D341EE32LL, int2, 15);
      break;
    case 53:
      HASH_INDEX(0x055499C26E552935LL, float2, 21);
      break;
    case 56:
      HASH_INDEX(0x1C7B111915EB4DB8LL, valid_float2, 23);
      break;
    case 60:
      HASH_INDEX(0x5A4EE8DE56E66A3CLL, valid_false, 13);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 71:
      HASH_INDEX(0x65CCC12FFAFD9647LL, valid_int1, 16);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 99:
      HASH_INDEX(0x2B1CCCDFA7F10AE3LL, failed, 27);
      break;
    case 102:
      HASH_INDEX(0x5D8717CC04BC3C66LL, valid_int2, 17);
      HASH_INDEX(0x081D58FD5DE44266LL, typeToTest, 29);
      break;
    case 107:
      HASH_INDEX(0x56A8E1F4FADD65EBLL, compareVal, 32);
      break;
    case 109:
      HASH_INDEX(0x447F5EFF34810BEDLL, invalid_int2, 19);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 111:
      HASH_INDEX(0x6CABC2B2F8135FEFLL, int1, 14);
      break;
    case 116:
      HASH_INDEX(0x606859DECAD31E74LL, invalid_compares, 31);
      break;
    case 127:
      HASH_INDEX(0x27746B4821FFDBFFLL, valid_true, 12);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 33) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
