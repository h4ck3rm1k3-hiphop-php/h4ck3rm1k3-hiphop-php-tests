
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/negate_basiclong_64bit.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_MIN_32Bit = 0x84205d880000000LL;
const double k_MIN_64Bit = -9223372036854775800.0;
const int64 k_MAX_32Bit = 2147483647LL;
const double k_MAX_64Bit = 9223372036854775800.0;

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$negate_basiclong_64bit_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/negate_basiclong_64bit.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$negate_basiclong_64bit_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_longVals __attribute__((__unused__)) = (variables != gVariables) ? variables->get("longVals") : g->GV(longVals);
  Variant &v_longVal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("longVal") : g->GV(longVal);

  ;
  ;
  ;
  ;
  (v_longVals = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_longVals.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_longVal = iter3->second();
      {
        echo(LINE(16,concat3("--- testing: ", toString(v_longVal), " ---\n")));
        LINE(17,x_var_dump(1, negate(v_longVal)));
      }
    }
  }
  echo("===DONE===\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
