
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/divide_basiclong_64bit.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_MIN_32Bit = 0x84205d880000000LL;
const double k_MIN_64Bit = -9223372036854775800.0;
const int64 k_MAX_32Bit = 2147483647LL;
const double k_MAX_64Bit = 9223372036854775800.0;

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$divide_basiclong_64bit_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/divide_basiclong_64bit.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$divide_basiclong_64bit_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_longVals __attribute__((__unused__)) = (variables != gVariables) ? variables->get("longVals") : g->GV(longVals);
  Variant &v_otherVals __attribute__((__unused__)) = (variables != gVariables) ? variables->get("otherVals") : g->GV(otherVals);
  Variant &v_longVal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("longVal") : g->GV(longVal);
  Variant &v_otherVal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("otherVal") : g->GV(otherVal);

  ;
  ;
  ;
  ;
  (v_longVals = ScalarArrays::sa_[0]);
  (v_otherVals = ScalarArrays::sa_[1]);
  LINE(16,x_error_reporting(toInt32(1LL) /* E_ERROR */));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_longVals.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_longVal = iter3->second();
      {
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_otherVals.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_otherVal = iter6->second();
            {
              echo(LINE(20,concat5("--- testing: ", toString(v_longVal), " / ", toString(v_otherVal), " ---\n")));
              LINE(21,x_var_dump(1, divide(v_longVal, v_otherVal)));
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_otherVals.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_otherVal = iter9->second();
      {
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = v_longVals.begin(); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_longVal = iter12->second();
            {
              echo(LINE(27,concat5("--- testing: ", toString(v_otherVal), " / ", toString(v_longVal), " ---\n")));
              LINE(28,x_var_dump(1, divide(v_otherVal, v_longVal)));
            }
          }
        }
      }
    }
  }
  echo("===DONE===\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
