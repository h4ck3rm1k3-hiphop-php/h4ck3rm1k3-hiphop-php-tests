
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/operator_lt_basic.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$operator_lt_basic_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/operator_lt_basic.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$operator_lt_basic_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_valid_true __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valid_true") : g->GV(valid_true);
  Variant &v_valid_false __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valid_false") : g->GV(valid_false);
  Variant &v_int1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("int1") : g->GV(int1);
  Variant &v_int2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("int2") : g->GV(int2);
  Variant &v_valid_int1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valid_int1") : g->GV(valid_int1);
  Variant &v_valid_int2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valid_int2") : g->GV(valid_int2);
  Variant &v_invalid_int1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("invalid_int1") : g->GV(invalid_int1);
  Variant &v_invalid_int2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("invalid_int2") : g->GV(invalid_int2);
  Variant &v_float1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("float1") : g->GV(float1);
  Variant &v_float2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("float2") : g->GV(float2);
  Variant &v_valid_float1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valid_float1") : g->GV(valid_float1);
  Variant &v_valid_float2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valid_float2") : g->GV(valid_float2);
  Variant &v_invalid_float1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("invalid_float1") : g->GV(invalid_float1);
  Variant &v_invalid_float2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("invalid_float2") : g->GV(invalid_float2);
  Variant &v_toCompare __attribute__((__unused__)) = (variables != gVariables) ? variables->get("toCompare") : g->GV(toCompare);
  Variant &v_failed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("failed") : g->GV(failed);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_typeToTest __attribute__((__unused__)) = (variables != gVariables) ? variables->get("typeToTest") : g->GV(typeToTest);
  Variant &v_valid_compares __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valid_compares") : g->GV(valid_compares);
  Variant &v_invalid_compares __attribute__((__unused__)) = (variables != gVariables) ? variables->get("invalid_compares") : g->GV(invalid_compares);
  Variant &v_compareVal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("compareVal") : g->GV(compareVal);

  (v_valid_true = ScalarArrays::sa_[0]);
  (v_valid_false = ScalarArrays::sa_[1]);
  (v_int1 = 677LL);
  (v_int2 = -67837LL);
  (v_valid_int1 = ScalarArrays::sa_[2]);
  (v_valid_int2 = ScalarArrays::sa_[3]);
  (v_invalid_int1 = ScalarArrays::sa_[4]);
  (v_invalid_int2 = ScalarArrays::sa_[5]);
  (v_float1 = 57385.458350000001);
  (v_float2 = -67345.765669999993);
  (v_valid_float1 = ScalarArrays::sa_[6]);
  (v_valid_float2 = ScalarArrays::sa_[7]);
  (v_invalid_float1 = ScalarArrays::sa_[8]);
  (v_invalid_float2 = ScalarArrays::sa_[9]);
  (v_toCompare = Array(ArrayInit(15).set(0, false).set(1, v_valid_true).set(2, v_valid_false).set(3, v_int1).set(4, v_valid_int1).set(5, v_invalid_int1).set(6, v_int2).set(7, v_valid_int2).set(8, v_invalid_int2).set(9, v_float1).set(10, v_valid_float1).set(11, v_invalid_float1).set(12, v_float2).set(13, v_valid_float2).set(14, v_invalid_float2).create()));
  (v_failed = false);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(29,x_count(v_toCompare))); v_i += 3LL) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_typeToTest = v_toCompare.rvalAt(v_i));
        (v_valid_compares = v_toCompare.rvalAt(v_i + 1LL));
        (v_invalid_compares = v_toCompare.rvalAt(v_i + 2LL));
        {
          LOOP_COUNTER(2);
          for (ArrayIterPtr iter4 = v_valid_compares.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_compareVal = iter4->second();
            {
              if (less(v_typeToTest, v_compareVal)) {
              }
              else {
                echo(LINE(39,concat5("FAILED: '", toString(v_typeToTest), "' >= '", toString(v_compareVal), "'\n")));
                (v_failed = true);
              }
            }
          }
        }
        {
          LOOP_COUNTER(5);
          for (ArrayIterPtr iter7 = v_invalid_compares.begin(); !iter7->end(); iter7->next()) {
            LOOP_COUNTER_CHECK(5);
            v_compareVal = iter7->second();
            {
              if (less(v_typeToTest, v_compareVal)) {
                echo(LINE(46,concat5("FAILED: '", toString(v_typeToTest), "' < '", toString(v_compareVal), "'\n")));
                (v_failed = true);
              }
            }
          }
        }
      }
    }
  }
  if (equal(v_failed, false)) {
    echo("Test Passed\n");
  }
  echo("===DONE===\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
