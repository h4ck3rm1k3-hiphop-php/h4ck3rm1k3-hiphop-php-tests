
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "valid_true",
    "valid_false",
    "int1",
    "int2",
    "valid_int1",
    "valid_int2",
    "invalid_int1",
    "invalid_int2",
    "float1",
    "float2",
    "valid_float1",
    "valid_float2",
    "invalid_float1",
    "invalid_float2",
    "toCompare",
    "failed",
    "i",
    "typeToTest",
    "valid_compares",
    "invalid_compares",
    "compareVal",
  };
  if (idx >= 0 && idx < 33) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(valid_true);
    case 13: return GV(valid_false);
    case 14: return GV(int1);
    case 15: return GV(int2);
    case 16: return GV(valid_int1);
    case 17: return GV(valid_int2);
    case 18: return GV(invalid_int1);
    case 19: return GV(invalid_int2);
    case 20: return GV(float1);
    case 21: return GV(float2);
    case 22: return GV(valid_float1);
    case 23: return GV(valid_float2);
    case 24: return GV(invalid_float1);
    case 25: return GV(invalid_float2);
    case 26: return GV(toCompare);
    case 27: return GV(failed);
    case 28: return GV(i);
    case 29: return GV(typeToTest);
    case 30: return GV(valid_compares);
    case 31: return GV(invalid_compares);
    case 32: return GV(compareVal);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
