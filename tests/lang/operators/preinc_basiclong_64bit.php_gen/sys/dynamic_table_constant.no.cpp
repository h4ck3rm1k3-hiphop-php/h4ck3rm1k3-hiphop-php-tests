
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_MIN_32Bit;
extern const double k_MIN_64Bit;
extern const int64 k_MAX_32Bit;
extern const double k_MAX_64Bit;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x3B18B7944B4BF938LL, k_MIN_64Bit, MIN_64Bit);
      break;
    case 1:
      HASH_RETURN(0x478624D256C58EF1LL, k_MIN_32Bit, MIN_32Bit);
      break;
    case 5:
      HASH_RETURN(0x6859F2FBD6268B55LL, k_MAX_32Bit, MAX_32Bit);
      break;
    case 7:
      HASH_RETURN(0x50045AAAB196F007LL, k_MAX_64Bit, MAX_64Bit);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
