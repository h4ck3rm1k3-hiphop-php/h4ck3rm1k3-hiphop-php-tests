
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/operator_gt_variation.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_MIN_32Bit = 0x84205d880000000LL;
const double k_MIN_64Bit = -9223372036854775800.0;
const int64 k_MAX_32Bit = 2147483647LL;
const double k_MAX_64Bit = 9223372036854775800.0;

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$operator_gt_variation_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/operators/operator_gt_variation.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$operators$operator_gt_variation_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_validGreaterThan __attribute__((__unused__)) = (variables != gVariables) ? variables->get("validGreaterThan") : g->GV(validGreaterThan);
  Variant &v_invalidGreaterThan __attribute__((__unused__)) = (variables != gVariables) ? variables->get("invalidGreaterThan") : g->GV(invalidGreaterThan);
  Variant &v_failed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("failed") : g->GV(failed);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_typeToTestVal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("typeToTestVal") : g->GV(typeToTestVal);
  Variant &v_compares __attribute__((__unused__)) = (variables != gVariables) ? variables->get("compares") : g->GV(compares);
  Variant &v_compareVal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("compareVal") : g->GV(compareVal);

  ;
  ;
  ;
  ;
  (v_validGreaterThan = ScalarArrays::sa_[0]);
  (v_invalidGreaterThan = ScalarArrays::sa_[1]);
  (v_failed = false);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(22,x_count(v_validGreaterThan))); v_i += 2LL) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_typeToTestVal = v_validGreaterThan.rvalAt(v_i));
        (v_compares = v_validGreaterThan.rvalAt(v_i + 1LL));
        {
          LOOP_COUNTER(2);
          for (ArrayIterPtr iter4 = v_compares.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_compareVal = iter4->second();
            {
              if (more(v_typeToTestVal, v_compareVal)) {
              }
              else {
                echo(LINE(30,concat5("FAILED: '", toString(v_typeToTestVal), "' <= '", toString(v_compareVal), "'\n")));
                (v_failed = true);
              }
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(5);
    for ((v_i = 0LL); less(v_i, LINE(36,x_count(v_invalidGreaterThan))); v_i += 2LL) {
      LOOP_COUNTER_CHECK(5);
      {
        (v_typeToTestVal = v_invalidGreaterThan.rvalAt(v_i));
        (v_compares = v_invalidGreaterThan.rvalAt(v_i + 1LL));
        {
          LOOP_COUNTER(6);
          for (ArrayIterPtr iter8 = v_compares.begin(); !iter8->end(); iter8->next()) {
            LOOP_COUNTER_CHECK(6);
            v_compareVal = iter8->second();
            {
              if (more(v_typeToTestVal, v_compareVal)) {
                echo(LINE(41,concat5("FAILED: '", toString(v_typeToTestVal), "' > '", toString(v_compareVal), "'\n")));
                (v_failed = true);
              }
            }
          }
        }
      }
    }
  }
  if (equal(v_failed, false)) {
    echo("Test Passed\n");
  }
  echo("===DONE===\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
