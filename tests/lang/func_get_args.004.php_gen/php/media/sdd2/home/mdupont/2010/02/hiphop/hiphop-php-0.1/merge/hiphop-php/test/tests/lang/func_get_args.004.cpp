
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_args.004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_args.004.php line 10 */
void f_refval(int num_args, Variant v_x, Variant v_y, Array args /* = Array() */) {
  FUNCTION_INJECTION(refVal);
  LINE(11,x_var_dump(2, v_x, Array(ArrayInit(1).set(0, v_y).create())));
  LINE(12,x_var_dump(1, func_get_args(num_args, Array(ArrayInit(2).setRef(0, ref(v_x)).set(1, v_y).create()),args)));
  (v_x = "changed.x");
  (v_y = "changed.y");
  LINE(15,x_var_dump(1, func_get_args(num_args, Array(ArrayInit(2).setRef(0, ref(v_x)).set(1, v_y).create()),args)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_args.004.php line 2 */
void f_valref(int num_args, Variant v_x, Variant v_y, Array args /* = Array() */) {
  FUNCTION_INJECTION(valRef);
  LINE(3,x_var_dump(2, v_x, Array(ArrayInit(1).set(0, v_y).create())));
  LINE(4,x_var_dump(1, func_get_args(num_args, Array(ArrayInit(2).set(0, v_x).setRef(1, ref(v_y)).create()),args)));
  (v_x = "changed.x");
  (v_y = "changed.y");
  LINE(7,x_var_dump(1, func_get_args(num_args, Array(ArrayInit(2).set(0, v_x).setRef(1, ref(v_y)).create()),args)));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$func_get_args_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/func_get_args.004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$func_get_args_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("\n\n-- Val, Ref --\n");
  (v_a = "original.a");
  LINE(21,f_valref(2, v_a, ref(v_a)));
  LINE(22,x_var_dump(1, v_a));
  echo("\n\n-- Ref, Val --\n");
  (v_b = "original.b");
  LINE(26,f_refval(2, ref(v_b), v_b));
  LINE(27,x_var_dump(1, v_b));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
