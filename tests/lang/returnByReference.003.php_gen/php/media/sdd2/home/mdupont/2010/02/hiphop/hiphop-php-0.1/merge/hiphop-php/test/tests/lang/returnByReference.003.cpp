
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.003.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.003.php line 6 */
Variant f_returnconstantbyref() {
  FUNCTION_INJECTION(returnConstantByRef);
  return 100LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.003.php line 10 */
Variant f_returnvariablebyref() {
  FUNCTION_INJECTION(returnVariableByRef);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(a)));
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.003.php line 2 */
int64 f_returnconstantbyvalue() {
  FUNCTION_INJECTION(returnConstantByValue);
  return 100LL;
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_003_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/returnByReference.003.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$returnByReference_003_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("\n---> 1. Trying to assign by reference the return value of a function that returns by value:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(17,f_returnconstantbyvalue())));
  v_a++;
  LINE(19,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 2. Trying to assign by reference the return value of a function that returns a constant by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(24,f_returnconstantbyref())));
  v_a++;
  LINE(26,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("\n---> 3. Trying to assign by reference the return value of a function that returns by ref:\n");
  {
    unset(v_a);
    unset(v_b);
  }
  (v_a = 4LL);
  (v_b = ref(LINE(31,f_returnvariablebyref())));
  v_a++;
  LINE(33,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
