
#include <php/df_lambda.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: php$df_lambda line 2 */
int64 f_df_lambda_1() {
  FUNCTION_INJECTION(df_lambda_1);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_foo __attribute__((__unused__)) = g->sv_df_lambda_1_DupIdfoo;
  bool &inited_sv_foo __attribute__((__unused__)) = g->inited_sv_df_lambda_1_DupIdfoo;
  if (!inited_sv_foo) {
    (sv_foo = 0LL);
    inited_sv_foo = true;
  }
  return sv_foo++;
} /* function */
Variant pm_php$df_lambda(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::df_lambda);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$df_lambda;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
