
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 27 */
Variant c_dafna_class::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_dafna_class::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_dafna_class::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_dafna_class::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_dafna_class::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_dafna_class::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_dafna_class::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_dafna_class::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(dafna_class)
ObjectData *c_dafna_class::create() {
  init();
  t_dafna_class();
  return this;
}
ObjectData *c_dafna_class::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_dafna_class::cloneImpl() {
  c_dafna_class *obj = NEW(c_dafna_class)();
  cloneSet(obj);
  return obj;
}
void c_dafna_class::cloneSet(c_dafna_class *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_dafna_class::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5C4B6D10B8B22045LL, getmyname) {
        return (t_getmyname());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_dafna_class::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5C4B6D10B8B22045LL, getmyname) {
        return (t_getmyname());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_dafna_class::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_dafna_class$os_get(const char *s) {
  return c_dafna_class::os_get(s, -1);
}
Variant &cw_dafna_class$os_lval(const char *s) {
  return c_dafna_class::os_lval(s, -1);
}
Variant cw_dafna_class$os_constant(const char *s) {
  return c_dafna_class::os_constant(s);
}
Variant cw_dafna_class$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_dafna_class::os_invoke(c, s, params, -1, fatal);
}
void c_dafna_class::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 28 */
void c_dafna_class::t_dafna_class() {
  INSTANCE_METHOD_INJECTION(dafna_class, dafna_class::dafna_class);
  bool oldInCtor = gasInCtor(true);
  (o_lval("myname", 0x4A35AA1563887466LL) = "Dafna");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 31 */
Variant c_dafna_class::t_getmyname() {
  INSTANCE_METHOD_INJECTION(dafna_class, dafna_class::GetMyName);
  return o_get("myname", 0x4A35AA1563887466LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 34 */
void c_dafna_class::t_setmyname(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(dafna_class, dafna_class::SetMyName);
  (o_lval("myname", 0x4A35AA1563887466LL) = v_name);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 11 */
String f_still_working() {
  FUNCTION_INJECTION(still_working);
  return "I'm still alive";
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 16 */
String f_dafna() {
  FUNCTION_INJECTION(dafna);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_foo __attribute__((__unused__)) = g->sv_dafna_DupIdfoo;
  bool &inited_sv_foo __attribute__((__unused__)) = g->inited_sv_dafna_DupIdfoo;
  if (!inited_sv_foo) {
    (sv_foo = 0LL);
    inited_sv_foo = true;
  }
  print("Dafna!\n");
  print(concat(toString(LINE(21,x_call_user_func(1, "still_working"))), "\n"));
  sv_foo++;
  return toString(sv_foo);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 5 */
void f_print_stuff(CVarRef v_stuff) {
  FUNCTION_INJECTION(print_stuff);
  print(toString(v_stuff));
} /* function */
Variant i_still_working(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x044CD1EE57D302B5LL, still_working) {
    return (f_still_working());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_dafna(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x54A90411051082F2LL, dafna) {
    return (f_dafna());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_print_stuff(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x28913A0C7C060D5BLL, print_stuff) {
    return (f_print_stuff(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_dafna_class(CArrRef params, bool init /* = true */) {
  return Object(p_dafna_class(NEW(c_dafna_class)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$028_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$028_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_dafna __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dafna") : g->GV(dafna);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);

  LINE(3,x_error_reporting(toInt32(1023LL)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 200LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        print(toString(v_i) + toString("\n"));
        LINE(41,x_call_user_func(1, "dafna"));
        LINE(42,x_call_user_func(2, "print_stuff", ScalarArrays::sa_[0]));
        print(toString(v_i) + toString("\n"));
      }
    }
  }
  (v_dafna = ((Object)(LINE(47,p_dafna_class(p_dafna_class(NEWOBJ(c_dafna_class)())->create())))));
  print(toString((v_name = LINE(49,x_call_user_func(1, Array(ArrayInit(2).setRef(0, ref(v_dafna)).set(1, "GetMyName").create()))))));
  print("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
