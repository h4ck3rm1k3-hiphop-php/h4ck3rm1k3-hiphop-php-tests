
#ifndef __GENERATED_cls_dafna_class_h__
#define __GENERATED_cls_dafna_class_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/028.php line 27 */
class c_dafna_class : virtual public ObjectData {
  BEGIN_CLASS_MAP(dafna_class)
  END_CLASS_MAP(dafna_class)
  DECLARE_CLASS(dafna_class, dafna_class, ObjectData)
  void init();
  public: void t_dafna_class();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_getmyname();
  public: void t_setmyname(CVarRef v_name);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_dafna_class_h__
