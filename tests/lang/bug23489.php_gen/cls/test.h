
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/bug23489.php line 2 */
class c_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(test)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, Test, ObjectData)
  void init();
  public: void t_test();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: String t_transform(CVarRef v_buffer);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
