
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_006.php line 12 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: void t___construct(Variant v_ref1, Variant v_ref2, Variant v_ref3, Variant v_ref4, Variant v_ref5);
  public: ObjectData *create(Variant v_ref1, Variant v_ref2, Variant v_ref3, Variant v_ref4, Variant v_ref5);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static void ti_refs(const char* cls, Variant v_ref1, Variant v_ref2, Variant v_ref3, Variant v_ref4, Variant v_ref5);
  public: static void t_refs(CVarRef v_ref1, CVarRef v_ref2, CVarRef v_ref3, CVarRef v_ref4, CVarRef v_ref5) { ti_refs("c", v_ref1, v_ref2, v_ref3, v_ref4, v_ref5); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
