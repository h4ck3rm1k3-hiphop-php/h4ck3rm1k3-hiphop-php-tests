
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_006.php line 12 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::create(Variant v_ref1, Variant v_ref2, Variant v_ref3, Variant v_ref4, Variant v_ref5) {
  init();
  t___construct(ref(v_ref1), ref(v_ref2), ref(v_ref3), ref(v_ref4), ref(v_ref5));
  return this;
}
ObjectData *c_c::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), ref(const_cast<Array&>(params).lvalAt(2)), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))));
  } else return this;
}
void c_c::dynConstruct(CArrRef params) {
  (t___construct(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), ref(const_cast<Array&>(params).lvalAt(2)), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))));
}
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x260587883CE5A5ACLL, refs) {
        return (ti_refs(o_getClassName(), ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), ref(const_cast<Array&>(params).lvalAt(2)), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), ref(const_cast<Array&>(params).lvalAt(2)), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x260587883CE5A5ACLL, refs) {
        return (ti_refs(o_getClassName(), ref(a0), ref(a1), ref(a2), ref(a3), ref(a4)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(ref(a0), ref(a1), ref(a2), ref(a3), ref(a4)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x260587883CE5A5ACLL, refs) {
        return (ti_refs(c, ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), ref(const_cast<Array&>(params).lvalAt(2)), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_006.php line 14 */
void c_c::t___construct(Variant v_ref1, Variant v_ref2, Variant v_ref3, Variant v_ref4, Variant v_ref5) {
  INSTANCE_METHOD_INJECTION(C, C::__construct);
  bool oldInCtor = gasInCtor(true);
  (v_ref1 = "Ref1 changed");
  (v_ref2 = "Ref2 changed");
  (v_ref3 = "Ref3 changed");
  (v_ref4 = "Ref4 changed");
  (v_ref5 = "Ref5 changed");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_006.php line 22 */
void c_c::ti_refs(const char* cls, Variant v_ref1, Variant v_ref2, Variant v_ref3, Variant v_ref4, Variant v_ref5) {
  STATIC_METHOD_INJECTION(C, C::refs);
  (v_ref1 = "Ref1 changed");
  (v_ref2 = "Ref2 changed");
  (v_ref3 = "Ref3 changed");
  (v_ref4 = "Ref4 changed");
  (v_ref5 = "Ref5 changed");
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_006.php line 3 */
void f_refs(Variant v_ref1, Variant v_ref2, Variant v_ref3, Variant v_ref4, Variant v_ref5) {
  FUNCTION_INJECTION(refs);
  (v_ref1 = "Ref1 changed");
  (v_ref2 = "Ref2 changed");
  (v_ref3 = "Ref3 changed");
  (v_ref4 = "Ref4 changed");
  (v_ref5 = "Ref5 changed");
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_u1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u1") : g->GV(u1);
  Variant &v_u2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u2") : g->GV(u2);
  Variant &v_u3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u3") : g->GV(u3);
  Variant &v_u4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u4") : g->GV(u4);
  Variant &v_u5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u5") : g->GV(u5);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("\n ---- Pass uninitialised array & object by ref: function call ---\n");
  {
    unset(v_u1);
    unset(v_u2);
    unset(v_u3);
    unset(v_u4);
    unset(v_u5);
  }
  LINE(34,f_refs(ref(lval(v_u1.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))), ref(lval(lval(v_u2.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL))), ref(lval(v_u3.o_lval("a", 0x4292CEE227B9150ALL))), ref(lval(lval(v_u4.o_lval("a", 0x4292CEE227B9150ALL)).o_lval("b", 0x08FBB133F8576BD5LL))), ref(lval(lval(lval(v_u5.o_lval("a", 0x4292CEE227B9150ALL)).o_lval("b", 0x08FBB133F8576BD5LL)).o_lval("c", 0x32C769EE5C5509B0LL)))));
  LINE(35,x_var_dump(5, v_u1, Array(ArrayInit(4).set(0, v_u2).set(1, v_u3).set(2, v_u4).set(3, v_u5).create())));
  echo("\n ---- Pass uninitialised arrays & objects by ref: static method call ---\n");
  {
    unset(v_u1);
    unset(v_u2);
    unset(v_u3);
    unset(v_u4);
    unset(v_u5);
  }
  LINE(39,c_c::t_refs(ref(lval(v_u1.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))), ref(lval(lval(v_u2.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL))), ref(lval(v_u3.o_lval("a", 0x4292CEE227B9150ALL))), ref(lval(lval(v_u4.o_lval("a", 0x4292CEE227B9150ALL)).o_lval("b", 0x08FBB133F8576BD5LL))), ref(lval(lval(lval(v_u5.o_lval("a", 0x4292CEE227B9150ALL)).o_lval("b", 0x08FBB133F8576BD5LL)).o_lval("c", 0x32C769EE5C5509B0LL)))));
  LINE(40,x_var_dump(5, v_u1, Array(ArrayInit(4).set(0, v_u2).set(1, v_u3).set(2, v_u4).set(3, v_u5).create())));
  echo("\n\n---- Pass uninitialised arrays & objects by ref: constructor ---\n");
  {
    unset(v_u1);
    unset(v_u2);
    unset(v_u3);
    unset(v_u4);
    unset(v_u5);
  }
  (v_c = ((Object)(LINE(44,p_c(p_c(NEWOBJ(c_c)())->create(ref(lval(v_u1.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))), ref(lval(lval(v_u2.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL))), ref(lval(v_u3.o_lval("a", 0x4292CEE227B9150ALL))), ref(lval(lval(v_u4.o_lval("a", 0x4292CEE227B9150ALL)).o_lval("b", 0x08FBB133F8576BD5LL))), ref(lval(lval(lval(v_u5.o_lval("a", 0x4292CEE227B9150ALL)).o_lval("b", 0x08FBB133F8576BD5LL)).o_lval("c", 0x32C769EE5C5509B0LL)))))))));
  LINE(45,x_var_dump(5, v_u1, Array(ArrayInit(4).set(0, v_u2).set(1, v_u3).set(2, v_u4).set(3, v_u5).create())));
  echo("\n ---- Pass uninitialised arrays & objects by ref: instance method call ---\n");
  {
    unset(v_u1);
    unset(v_u2);
    unset(v_u3);
    unset(v_u4);
    unset(v_u5);
  }
  LINE(49,v_c.o_invoke_few_args("refs", 0x260587883CE5A5ACLL, 5, v_u1.refvalAt(0LL, 0x77CFA1EEF01BCA90LL), lval(v_u2.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).refvalAt(1LL, 0x5BCA7C69B794F8CELL), v_u3.o_lval("a", 0x4292CEE227B9150ALL), v_u4.o_get("a", 0x4292CEE227B9150ALL).o_lval("b", 0x08FBB133F8576BD5LL), v_u5.o_get("a", 0x4292CEE227B9150ALL).o_get("b", 0x08FBB133F8576BD5LL).o_lval("c", 0x32C769EE5C5509B0LL)));
  LINE(50,x_var_dump(5, v_u1, Array(ArrayInit(4).set(0, v_u2).set(1, v_u3).set(2, v_u4).set(3, v_u5).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
