
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/foreachLoopIteratorAggregate.004.php line 41 */
class c_c : virtual public c_iteratoraggregate {
  BEGIN_CLASS_MAP(c)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iteratoraggregate)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, c, ObjectData)
  void init();
  public: int64 m_max;
  public: virtual void destruct();
  public: p_c_iter t_getiterator();
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
