
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_008.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_008.php line 45 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x77F632A4E34F1526LL, g->s_c_DupIdp,
                  p);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x77F632A4E34F1526LL, g->s_c_DupIdp,
                  p);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_c_DupIdp = null;
}
void csi_c() {
  c_c::os_static_initializer();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_008.php line 4 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  return 0LL;
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_008_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_008.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_008_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  LINE(2,x_error_reporting(toInt32(6143LL)));
  lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1LL, ("good"), 0x5BCA7C69B794F8CELL);
  lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1LL, ("bad"), 0x5BCA7C69B794F8CELL);
  echo("\n$i=f(): ");
  echo(toString(v_a.rvalAt((v_i = LINE(9,f_f()))).rvalAt(++v_i)));
  unset(v_i);
  echo("\n$$x=f(): ");
  (v_x = "i");
  echo(toString(v_a.rvalAt((variables->get(toString(v_x)) = LINE(14,f_f()))).rvalAt(++variables->get(toString(v_x)))));
  {
    unset(v_i);
    unset(v_x);
  }
  echo("\n${'i'}=f(): ");
  echo(toString(v_a.rvalAt((v_i = LINE(18,f_f()))).rvalAt(++v_i)));
  unset(v_i);
  echo("\n$i[0]=f(): ");
  echo(toString(v_a.rvalAt(v_i.set(0LL, (LINE(22,f_f())), 0x77CFA1EEF01BCA90LL)).rvalAt(++lval(v_i.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  unset(v_i);
  echo("\n$i[0][0]=f(): ");
  echo(toString(v_a.rvalAt(lval(v_i.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(0LL, (LINE(26,f_f())), 0x77CFA1EEF01BCA90LL)).rvalAt(++lval(lval(v_i.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  unset(v_i);
  echo("\n$i->p=f(): ");
  echo(toString(v_a.rvalAt((v_i.o_lval("p", 0x77F632A4E34F1526LL) = LINE(30,f_f()))).rvalAt(++lval(v_i.o_lval("p", 0x77F632A4E34F1526LL)))));
  unset(v_i);
  echo("\n$i->p->q=f(): ");
  echo(toString(v_a.rvalAt((lval(v_i.o_lval("p", 0x77F632A4E34F1526LL)).o_lval("q", 0x05CAFC91A9B37763LL) = LINE(34,f_f()))).rvalAt(++lval(lval(v_i.o_lval("p", 0x77F632A4E34F1526LL)).o_lval("q", 0x05CAFC91A9B37763LL)))));
  unset(v_i);
  echo("\n$i->p[0]=f(): ");
  echo(toString(v_a.rvalAt(lval(v_i.o_lval("p", 0x77F632A4E34F1526LL)).set(0LL, (LINE(38,f_f())), 0x77CFA1EEF01BCA90LL)).rvalAt(++lval(lval(v_i.o_lval("p", 0x77F632A4E34F1526LL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  unset(v_i);
  echo("\n$i->p[0]->p=f(): ");
  echo(toString(v_a.rvalAt((lval(lval(v_i.o_lval("p", 0x77F632A4E34F1526LL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("p", 0x77F632A4E34F1526LL) = LINE(42,f_f()))).rvalAt(++lval(lval(lval(v_i.o_lval("p", 0x77F632A4E34F1526LL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("p", 0x77F632A4E34F1526LL)))));
  unset(v_i);
  echo("\nC::$p=f(): ");
  echo(toString(v_a.rvalAt((g->s_c_DupIdp = LINE(50,f_f()))).rvalAt(++g->s_c_DupIdp)));
  echo("\nC::$p[0]=f(): ");
  (g->s_c_DupIdp = ScalarArrays::sa_[0]);
  echo(toString(v_a.rvalAt(g->s_c_DupIdp.set(0LL, (LINE(54,f_f())), 0x77CFA1EEF01BCA90LL)).rvalAt(++lval(g->s_c_DupIdp.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  echo("\nC::$p->q=f(): ");
  (g->s_c_DupIdp = ((Object)(LINE(57,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  echo(toString(v_a.rvalAt((g->s_c_DupIdp.o_lval("q", 0x05CAFC91A9B37763LL) = LINE(58,f_f()))).rvalAt(++lval(g->s_c_DupIdp.o_lval("q", 0x05CAFC91A9B37763LL)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
