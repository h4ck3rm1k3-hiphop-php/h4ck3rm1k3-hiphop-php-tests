
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic2.php line 9 */
Variant c_x::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$compare_objects_basic2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$compare_objects_basic2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj1") : g->GV(obj1);
  Variant &v_obj2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj2") : g->GV(obj2);

  echo("\n");
  LINE(5,x_date_default_timezone_set("Europe/London"));
  echo("Simple test comparing two objects with different compare callback handler\n");
  (v_obj1 = ((Object)(LINE(12,p_x(p_x(NEWOBJ(c_x)())->create())))));
  (v_obj2 = ((Object)(LINE(13,p_datetime(p_datetime(NEWOBJ(c_datetime)())->create("2009-02-12 12:47:41 GMT"))))));
  LINE(15,x_var_dump(1, equal(v_obj1, v_obj2)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
