
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_004.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  Variant a1 = a0.rvalAt(2);
  p1 = a1.rvalAt(0);
  p2 = a1.rvalAt(1);
  p3 = a0.rvalAt(3);
  return aa;
  
  }
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_004.php line 3 */
int64 f_i1() {
  FUNCTION_INJECTION(i1);
  echo("i1\n");
  return 1LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_004.php line 8 */
int64 f_i2() {
  FUNCTION_INJECTION(i2);
  echo("i2\n");
  return 1LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_004.php line 13 */
int64 f_i3() {
  FUNCTION_INJECTION(i3);
  echo("i3\n");
  return 3LL;
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_004.php line 18 */
int64 f_i4() {
  FUNCTION_INJECTION(i4);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  (gv_a = ScalarArrays::sa_[0]);
  echo("i4\n");
  return 4LL;
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_004_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/engine_assignExecutionOrder_004.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$engine_assignExecutionOrder_004_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 0LL);
  df_lambda_1(ScalarArrays::sa_[1], lval(v_a.lvalAt(plus_rev(LINE(26,f_i2()), f_i1()))), lval(v_a.lvalAt(f_i3())), lval(v_a.lvalAt(f_i4())), lval(v_a.lvalAt()));
  LINE(28,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
