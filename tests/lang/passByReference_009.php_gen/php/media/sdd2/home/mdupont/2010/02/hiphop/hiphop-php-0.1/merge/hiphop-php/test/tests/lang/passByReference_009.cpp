
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_009.php line 8 */
void f_foo2(CVarRef v_x, Variant v_y, int64 v_z) {
  FUNCTION_INJECTION(foo2);
  echo(toString(v_x));
  echo(toString(v_y));
  (v_y = 2LL);
} /* function */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_009.php line 2 */
void f_foo(Variant v_x, Variant v_y) {
  FUNCTION_INJECTION(foo);
  (v_x = 1LL);
  echo(toString(v_y));
} /* function */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/passByReference_009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$passByReference_009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 0LL);
  LINE(5,f_foo(ref(v_x), ref(v_x)));
  (v_x = 0LL);
  LINE(17,(assignCallTemp(eo_0, v_x),assignCallTemp(eo_1, ref(v_x)),assignCallTemp(eo_2, (v_x = 1LL)),f_foo2(eo_0, eo_1, eo_2)));
  echo(toString(v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
