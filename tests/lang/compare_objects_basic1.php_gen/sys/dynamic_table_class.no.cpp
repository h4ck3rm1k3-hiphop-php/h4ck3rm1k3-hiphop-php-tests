
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_class1(CArrRef params, bool init = true);
Variant cw_class1$os_get(const char *s);
Variant &cw_class1$os_lval(const char *s);
Variant cw_class1$os_constant(const char *s);
Variant cw_class1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class2(CArrRef params, bool init = true);
Variant cw_class2$os_get(const char *s);
Variant &cw_class2$os_lval(const char *s);
Variant cw_class2$os_constant(const char *s);
Variant cw_class2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class3(CArrRef params, bool init = true);
Variant cw_class3$os_get(const char *s);
Variant &cw_class3$os_lval(const char *s);
Variant cw_class3$os_constant(const char *s);
Variant cw_class3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class4(CArrRef params, bool init = true);
Variant cw_class4$os_get(const char *s);
Variant &cw_class4$os_lval(const char *s);
Variant cw_class4$os_constant(const char *s);
Variant cw_class4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_class5(CArrRef params, bool init = true);
Variant cw_class5$os_get(const char *s);
Variant &cw_class5$os_lval(const char *s);
Variant cw_class5$os_constant(const char *s);
Variant cw_class5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 4:
      HASH_CREATE_OBJECT(0x0F76C0CB1F591F94LL, class4);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x60608122E96C2F27LL, class1);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x3FB9B0EB4F3723D9LL, class5);
      break;
    case 13:
      HASH_CREATE_OBJECT(0x646D1F2C0E1403CDLL, class2);
      break;
    case 14:
      HASH_CREATE_OBJECT(0x3C436B1E27865ECELL, class3);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x0F76C0CB1F591F94LL, class4);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x60608122E96C2F27LL, class1);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x3FB9B0EB4F3723D9LL, class5);
      break;
    case 13:
      HASH_INVOKE_STATIC_METHOD(0x646D1F2C0E1403CDLL, class2);
      break;
    case 14:
      HASH_INVOKE_STATIC_METHOD(0x3C436B1E27865ECELL, class3);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 4:
      HASH_GET_STATIC_PROPERTY(0x0F76C0CB1F591F94LL, class4);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x60608122E96C2F27LL, class1);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x3FB9B0EB4F3723D9LL, class5);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY(0x646D1F2C0E1403CDLL, class2);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY(0x3C436B1E27865ECELL, class3);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x0F76C0CB1F591F94LL, class4);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x60608122E96C2F27LL, class1);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x3FB9B0EB4F3723D9LL, class5);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY_LV(0x646D1F2C0E1403CDLL, class2);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY_LV(0x3C436B1E27865ECELL, class3);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 4:
      HASH_GET_CLASS_CONSTANT(0x0F76C0CB1F591F94LL, class4);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x60608122E96C2F27LL, class1);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x3FB9B0EB4F3723D9LL, class5);
      break;
    case 13:
      HASH_GET_CLASS_CONSTANT(0x646D1F2C0E1403CDLL, class2);
      break;
    case 14:
      HASH_GET_CLASS_CONSTANT(0x3C436B1E27865ECELL, class3);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
