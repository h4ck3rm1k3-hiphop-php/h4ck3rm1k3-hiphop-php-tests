
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php line 6 */
Variant c_class1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_class1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_class1::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_class1::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_class1::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_class1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_class1::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_class1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(class1)
ObjectData *c_class1::cloneImpl() {
  c_class1 *obj = NEW(c_class1)();
  cloneSet(obj);
  return obj;
}
void c_class1::cloneSet(c_class1 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_class1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_class1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class1$os_get(const char *s) {
  return c_class1::os_get(s, -1);
}
Variant &cw_class1$os_lval(const char *s) {
  return c_class1::os_lval(s, -1);
}
Variant cw_class1$os_constant(const char *s) {
  return c_class1::os_constant(s);
}
Variant cw_class1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class1::os_invoke(c, s, params, -1, fatal);
}
void c_class1::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php line 8 */
Variant c_class2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_class2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_class2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_class2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_class2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_class2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_class2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_class2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(class2)
ObjectData *c_class2::cloneImpl() {
  c_class2 *obj = NEW(c_class2)();
  cloneSet(obj);
  return obj;
}
void c_class2::cloneSet(c_class2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_class2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_class2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class2$os_get(const char *s) {
  return c_class2::os_get(s, -1);
}
Variant &cw_class2$os_lval(const char *s) {
  return c_class2::os_lval(s, -1);
}
Variant cw_class2$os_constant(const char *s) {
  return c_class2::os_constant(s);
}
Variant cw_class2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class2::os_invoke(c, s, params, -1, fatal);
}
void c_class2::init() {
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php line 10 */
Variant c_class3::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_class3::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_class3::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("aaa", m_aaa));
  props.push_back(NEW(ArrayElement)("bbb", m_bbb));
  props.push_back(NEW(ArrayElement)("ccc", m_ccc));
  c_ObjectData::o_get(props);
}
bool c_class3::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_EXISTS_STRING(0x13377D6BCAE847ABLL, ccc, 3);
      break;
    case 4:
      HASH_EXISTS_STRING(0x5E02A58855E1FC8CLL, bbb, 3);
      break;
    case 6:
      HASH_EXISTS_STRING(0x061F924BFAA46AD6LL, aaa, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_class3::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_RETURN_STRING(0x13377D6BCAE847ABLL, m_ccc,
                         ccc, 3);
      break;
    case 4:
      HASH_RETURN_STRING(0x5E02A58855E1FC8CLL, m_bbb,
                         bbb, 3);
      break;
    case 6:
      HASH_RETURN_STRING(0x061F924BFAA46AD6LL, m_aaa,
                         aaa, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_class3::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_SET_STRING(0x13377D6BCAE847ABLL, m_ccc,
                      ccc, 3);
      break;
    case 4:
      HASH_SET_STRING(0x5E02A58855E1FC8CLL, m_bbb,
                      bbb, 3);
      break;
    case 6:
      HASH_SET_STRING(0x061F924BFAA46AD6LL, m_aaa,
                      aaa, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_class3::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_class3::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(class3)
ObjectData *c_class3::cloneImpl() {
  c_class3 *obj = NEW(c_class3)();
  cloneSet(obj);
  return obj;
}
void c_class3::cloneSet(c_class3 *clone) {
  clone->m_aaa = m_aaa;
  clone->m_bbb = m_bbb;
  clone->m_ccc = m_ccc;
  ObjectData::cloneSet(clone);
}
Variant c_class3::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_class3::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class3::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class3$os_get(const char *s) {
  return c_class3::os_get(s, -1);
}
Variant &cw_class3$os_lval(const char *s) {
  return c_class3::os_lval(s, -1);
}
Variant cw_class3$os_constant(const char *s) {
  return c_class3::os_constant(s);
}
Variant cw_class3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class3::os_invoke(c, s, params, -1, fatal);
}
void c_class3::init() {
  m_aaa = null;
  m_bbb = null;
  m_ccc = null;
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php line 16 */
Variant c_class4::os_get(const char *s, int64 hash) {
  return c_class3::os_get(s, hash);
}
Variant &c_class4::os_lval(const char *s, int64 hash) {
  return c_class3::os_lval(s, hash);
}
void c_class4::o_get(ArrayElementVec &props) const {
  c_class3::o_get(props);
}
bool c_class4::o_exists(CStrRef s, int64 hash) const {
  return c_class3::o_exists(s, hash);
}
Variant c_class4::o_get(CStrRef s, int64 hash) {
  return c_class3::o_get(s, hash);
}
Variant c_class4::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_class3::o_set(s, hash, v, forInit);
}
Variant &c_class4::o_lval(CStrRef s, int64 hash) {
  return c_class3::o_lval(s, hash);
}
Variant c_class4::os_constant(const char *s) {
  return c_class3::os_constant(s);
}
IMPLEMENT_CLASS(class4)
ObjectData *c_class4::cloneImpl() {
  c_class4 *obj = NEW(c_class4)();
  cloneSet(obj);
  return obj;
}
void c_class4::cloneSet(c_class4 *clone) {
  c_class3::cloneSet(clone);
}
Variant c_class4::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_class3::o_invoke(s, params, hash, fatal);
}
Variant c_class4::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_class3::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class4::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_class3::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class4$os_get(const char *s) {
  return c_class4::os_get(s, -1);
}
Variant &cw_class4$os_lval(const char *s) {
  return c_class4::os_lval(s, -1);
}
Variant cw_class4$os_constant(const char *s) {
  return c_class4::os_constant(s);
}
Variant cw_class4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class4::os_invoke(c, s, params, -1, fatal);
}
void c_class4::init() {
  c_class3::init();
}
/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php line 19 */
Variant c_class5::os_get(const char *s, int64 hash) {
  return c_class3::os_get(s, hash);
}
Variant &c_class5::os_lval(const char *s, int64 hash) {
  return c_class3::os_lval(s, hash);
}
void c_class5::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("ddd", m_ddd));
  props.push_back(NEW(ArrayElement)("eee", m_eee));
  c_class3::o_get(props);
}
bool c_class5::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x6E39D2F8F85C8A7ELL, ddd, 3);
      HASH_EXISTS_STRING(0x2BB5B8CCC7E06662LL, eee, 3);
      break;
    default:
      break;
  }
  return c_class3::o_exists(s, hash);
}
Variant c_class5::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x6E39D2F8F85C8A7ELL, m_ddd,
                         ddd, 3);
      HASH_RETURN_STRING(0x2BB5B8CCC7E06662LL, m_eee,
                         eee, 3);
      break;
    default:
      break;
  }
  return c_class3::o_get(s, hash);
}
Variant c_class5::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x6E39D2F8F85C8A7ELL, m_ddd,
                      ddd, 3);
      HASH_SET_STRING(0x2BB5B8CCC7E06662LL, m_eee,
                      eee, 3);
      break;
    default:
      break;
  }
  return c_class3::o_set(s, hash, v, forInit);
}
Variant &c_class5::o_lval(CStrRef s, int64 hash) {
  return c_class3::o_lval(s, hash);
}
Variant c_class5::os_constant(const char *s) {
  return c_class3::os_constant(s);
}
IMPLEMENT_CLASS(class5)
ObjectData *c_class5::cloneImpl() {
  c_class5 *obj = NEW(c_class5)();
  cloneSet(obj);
  return obj;
}
void c_class5::cloneSet(c_class5 *clone) {
  clone->m_ddd = m_ddd;
  clone->m_eee = m_eee;
  c_class3::cloneSet(clone);
}
Variant c_class5::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_class3::o_invoke(s, params, hash, fatal);
}
Variant c_class5::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_class3::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class5::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_class3::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class5$os_get(const char *s) {
  return c_class5::os_get(s, -1);
}
Variant &cw_class5$os_lval(const char *s) {
  return c_class5::os_lval(s, -1);
}
Variant cw_class5$os_constant(const char *s) {
  return c_class5::os_constant(s);
}
Variant cw_class5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class5::os_invoke(c, s, params, -1, fatal);
}
void c_class5::init() {
  c_class3::init();
  m_ddd = null;
  m_eee = null;
}
Object co_class1(CArrRef params, bool init /* = true */) {
  return Object(p_class1(NEW(c_class1)())->dynCreate(params, init));
}
Object co_class2(CArrRef params, bool init /* = true */) {
  return Object(p_class2(NEW(c_class2)())->dynCreate(params, init));
}
Object co_class3(CArrRef params, bool init /* = true */) {
  return Object(p_class3(NEW(c_class3)())->dynCreate(params, init));
}
Object co_class4(CArrRef params, bool init /* = true */) {
  return Object(p_class4(NEW(c_class4)())->dynCreate(params, init));
}
Object co_class5(CArrRef params, bool init /* = true */) {
  return Object(p_class5(NEW(c_class5)())->dynCreate(params, init));
}
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$compare_objects_basic1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$lang$compare_objects_basic1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj1") : g->GV(obj1);
  Variant &v_obj2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj2") : g->GV(obj2);
  Variant &v_obj3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj3") : g->GV(obj3);
  Variant &v_obj4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj4") : g->GV(obj4);
  Variant &v_obj5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj5") : g->GV(obj5);

  echo("\n");
  echo("Simple test for standard compare object handler\n");
  (v_obj1 = ((Object)(LINE(25,p_class1(p_class1(NEWOBJ(c_class1)())->create())))));
  (v_obj2 = ((Object)(LINE(26,p_class2(p_class2(NEWOBJ(c_class2)())->create())))));
  (v_obj3 = ((Object)(LINE(27,p_class3(p_class3(NEWOBJ(c_class3)())->create())))));
  (v_obj4 = ((Object)(LINE(28,p_class4(p_class4(NEWOBJ(c_class4)())->create())))));
  (v_obj5 = ((Object)(LINE(29,p_class5(p_class5(NEWOBJ(c_class5)())->create())))));
  echo("\n-- The following compare should return TRUE --\n");
  LINE(32,x_var_dump(1, equal(v_obj1, v_obj1)));
  echo("\n-- All the following compares should return FALSE --\n");
  LINE(35,x_var_dump(1, equal(v_obj1, v_obj2)));
  LINE(36,x_var_dump(1, equal(v_obj1, v_obj3)));
  LINE(37,x_var_dump(1, equal(v_obj1, v_obj4)));
  LINE(38,x_var_dump(1, equal(v_obj1, v_obj5)));
  LINE(39,x_var_dump(1, equal(v_obj4, v_obj3)));
  LINE(40,x_var_dump(1, equal(v_obj5, v_obj3)));
  echo("===DONE===\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
