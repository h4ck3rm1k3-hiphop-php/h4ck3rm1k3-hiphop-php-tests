
#ifndef __GENERATED_cls_class5_h__
#define __GENERATED_cls_class5_h__

#include <cls/class3.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php line 19 */
class c_class5 : virtual public c_class3 {
  BEGIN_CLASS_MAP(class5)
    PARENT_CLASS(class3)
  END_CLASS_MAP(class5)
  DECLARE_CLASS(class5, class5, class3)
  void init();
  public: String m_ddd;
  public: String m_eee;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class5_h__
