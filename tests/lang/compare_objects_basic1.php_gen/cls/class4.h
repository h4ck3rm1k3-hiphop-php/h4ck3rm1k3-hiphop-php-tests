
#ifndef __GENERATED_cls_class4_h__
#define __GENERATED_cls_class4_h__

#include <cls/class3.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: /media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/lang/compare_objects_basic1.php line 16 */
class c_class4 : virtual public c_class3 {
  BEGIN_CLASS_MAP(class4)
    PARENT_CLASS(class3)
  END_CLASS_MAP(class4)
  DECLARE_CLASS(class4, class4, class3)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class4_h__
