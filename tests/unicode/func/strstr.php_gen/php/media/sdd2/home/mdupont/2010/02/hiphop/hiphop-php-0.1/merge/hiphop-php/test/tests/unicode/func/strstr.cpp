
#include <php//media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/unicode/func/strstr.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$unicode$func$strstr_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::/media/sdd2/home/mdupont/2010/02/hiphop/hiphop-php-0.1/merge/hiphop-php/test/tests/unicode/func/strstr.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$$media$sdd2$home$mdupont$2010$02$hiphop$hiphop_php_0_1$merge$hiphop_php$test$tests$unicode$func$strstr_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_a = "a\342\204\242\341\204\222\\U020021z");
  LINE(3,x_var_dump(1, x_strstr(toString(v_a), "a")));
  LINE(4,x_var_dump(1, x_strstr(toString(v_a), "U")));
  LINE(5,x_var_dump(1, x_strstr(toString(v_a), "z")));
  LINE(6,x_var_dump(1, x_strstr(toString(v_a), "\\u2122")));
  LINE(7,x_var_dump(1, x_strstr(toString(v_a), "\\udc21")));
  LINE(8,x_var_dump(1, x_strstr(toString(v_a), 4370LL)));
  LINE(9,x_var_dump(1, x_strstr(toString(v_a), 131105LL)));
  (v_b = "\\U020022z\\U020021z");
  LINE(12,x_var_dump(1, x_strstr(toString(v_b), "\\U020021")));
  LINE(13,x_var_dump(1, x_strstr(toString(v_b), "z\\U020021")));
  (v_c = "-A\\u030a-\303\205-\342\204\253");
  LINE(16,x_var_dump(1, x_strstr(toString(v_c), "A")));
  LINE(17,x_var_dump(1, x_strstr(toString(v_c), "\\u030a")));
  LINE(18,x_var_dump(1, x_strstr(toString(v_c), "\\u00c5")));
  LINE(19,x_var_dump(1, x_strstr(toString(v_c), "\\u212b")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
