<?php
$a = "a™ᄒ\U020021z";
var_dump(strstr($a, 'a'));
var_dump(strstr($a, 'U'));
var_dump(strstr($a, 'z'));
var_dump(strstr($a, "\u2122"));
var_dump(strstr($a, "\udc21"));
var_dump(strstr($a, 0x1112));
var_dump(strstr($a, 0x20021));

$b = "\U020022z\U020021z";
var_dump(strstr($b, "\U020021"));
var_dump(strstr($b, "z\U020021"));

$c = "-A\u030a-Å-Å";
var_dump(strstr($c, 'A'));
var_dump(strstr($c, "\u030a"));
var_dump(strstr($c, "\u00c5"));
var_dump(strstr($c, "\u212b"));
?>
