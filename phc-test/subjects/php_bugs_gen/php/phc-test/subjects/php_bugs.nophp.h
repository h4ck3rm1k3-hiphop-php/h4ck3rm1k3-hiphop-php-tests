
#ifndef __GENERATED_php_phc_test_subjects_php_bugs_nophp_h__
#define __GENERATED_php_phc_test_subjects_php_bugs_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/php_bugs.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$php_bugs(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_php_bugs_nophp_h__
