
#include <php/phc-test/subjects/dynamic/dynamic_function.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/dynamic/dynamic_function.php line 6 */
void f_f_DupId0(CVarRef v_a, CVarRef v_b, CVarRef v_c) {
  FUNCTION_INJECTION(f);
  echo(concat("Function 1 with ", LINE(8,concat6(toString(v_a), ", ", toString(v_b), " and ", toString(v_c), "\n"))));
} /* function */
/* SRC: phc-test/subjects/dynamic/dynamic_function.php line 13 */
void f_f_DupId1(CVarRef v_a, CVarRef v_b, CVarRef v_c) {
  FUNCTION_INJECTION(f);
  echo(concat("Function 2 with ", LINE(15,concat6(toString(v_a), ", ", toString(v_b), " and ", toString(v_c), "\n"))));
} /* function */
/* SRC: phc-test/subjects/dynamic/dynamic_function.php line 18 */
void f_g(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(g);
  echo(LINE(20,concat5("Function g with ", toString(v_a), " and ", toString(v_b), "\n")));
} /* function */
Variant i_f_DupId0(CArrRef params) {
  return (f_f_DupId0(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
}
Variant i_f(CArrRef params) {
  DECLARE_GLOBAL_VARIABLES(g);
  return g->i_f(params);
}
Variant i_f_DupId1(CArrRef params) {
  return (f_f_DupId1(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
}
Variant i_g(CArrRef params) {
  return (f_g(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant pm_php$phc_test$subjects$dynamic$dynamic_function_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/dynamic/dynamic_function.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$dynamic$dynamic_function_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 17LL);
  if (more(v_x * v_x, 345LL)) {
    g->i_f = i_f_DupId0;
    g->declareFunction("f");
  }
  else {
    g->i_f = i_f_DupId1;
    g->declareFunction("f");
    g->declareFunction("g");
  }
  LINE(24,get_global_variables()->i_f(Array(ArrayInit(3).set(0, 1LL).set(1, 2LL).set(2, 3LL).create())));
  if (LINE(25,f_function_exists("g"))) {
    LINE(27,f_g(7LL, 8LL));
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
