
#ifndef __GENERATED_php_phc_test_subjects_dynamic_dynamic_function_h__
#define __GENERATED_php_phc_test_subjects_dynamic_dynamic_function_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/dynamic/dynamic_function.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$dynamic$dynamic_function_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f_DupId0(CVarRef v_a, CVarRef v_b, CVarRef v_c);
void f_f_DupId1(CVarRef v_a, CVarRef v_b, CVarRef v_c);
void f_g(CVarRef v_a, CVarRef v_b);
Variant i_f_DupId0(CArrRef params);
Variant i_f_DupId1(CArrRef params);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_dynamic_dynamic_function_h__
