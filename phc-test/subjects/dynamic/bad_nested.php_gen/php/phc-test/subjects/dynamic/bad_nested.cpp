
#include <php/phc-test/subjects/dynamic/bad_nested.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/dynamic/bad_nested.php line 7 */
void f_x() {
  FUNCTION_INJECTION(x);
} /* function */
Variant pm_php$phc_test$subjects$dynamic$bad_nested_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/dynamic/bad_nested.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$dynamic$bad_nested_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  if (toBoolean(1LL)) {
  }
  LINE(16,x_var_dump(1, 2LL /* n2::N2 */));
  LINE(17,f_x());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
