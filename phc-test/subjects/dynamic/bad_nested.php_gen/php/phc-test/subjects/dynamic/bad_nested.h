
#ifndef __GENERATED_php_phc_test_subjects_dynamic_bad_nested_h__
#define __GENERATED_php_phc_test_subjects_dynamic_bad_nested_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/dynamic/bad_nested.fw.h>

// Declarations
#include <cls/n2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_x();
Variant pm_php$phc_test$subjects$dynamic$bad_nested_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_dynamic_bad_nested_h__
