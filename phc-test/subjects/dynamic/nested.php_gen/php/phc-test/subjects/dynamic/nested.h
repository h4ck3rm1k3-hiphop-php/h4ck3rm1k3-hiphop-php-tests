
#ifndef __GENERATED_php_phc_test_subjects_dynamic_nested_h__
#define __GENERATED_php_phc_test_subjects_dynamic_nested_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/dynamic/nested.fw.h>

// Declarations
#include <cls/n2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$dynamic$nested_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_x();
void f_z();
Object co_n2(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_dynamic_nested_h__
