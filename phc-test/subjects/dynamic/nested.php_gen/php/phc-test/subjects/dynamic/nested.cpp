
#include <php/phc-test/subjects/dynamic/nested.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_N2 = "N2";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/dynamic/nested.php line 9 */
const int64 q_n2_N2 = 2LL;
Variant c_n2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_n2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_n2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_n2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_n2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_n2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_n2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_n2::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x456FA9F39B795B6ELL, q_n2_N2, N2);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(n2)
ObjectData *c_n2::cloneImpl() {
  c_n2 *obj = NEW(c_n2)();
  cloneSet(obj);
  return obj;
}
void c_n2::cloneSet(c_n2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_n2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x7F64BC1ECEF60CCCLL, y) {
        return (t_y(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_n2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x7F64BC1ECEF60CCCLL, y) {
        return (t_y(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_n2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_n2$os_get(const char *s) {
  return c_n2::os_get(s, -1);
}
Variant &cw_n2$os_lval(const char *s) {
  return c_n2::os_lval(s, -1);
}
Variant cw_n2$os_constant(const char *s) {
  return c_n2::os_constant(s);
}
Variant cw_n2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_n2::os_invoke(c, s, params, -1, fatal);
}
void c_n2::init() {
}
/* SRC: phc-test/subjects/dynamic/nested.php line 12 */
void c_n2::t_y() {
  INSTANCE_METHOD_INJECTION(N2, N2::y);
  LINE(14,x_var_dump(1, k_N2));
} /* function */
/* SRC: phc-test/subjects/dynamic/nested.php line 7 */
void f_x() {
  FUNCTION_INJECTION(x);
} /* function */
/* SRC: phc-test/subjects/dynamic/nested.php line 15 */
void f_z() {
  FUNCTION_INJECTION(z);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  (gv_x = 5LL);
} /* function */
Object co_n2(CArrRef params, bool init /* = true */) {
  return Object(p_n2(NEW(c_n2)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$dynamic$nested_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/dynamic/nested.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$dynamic$nested_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_n2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n2") : g->GV(n2);

  (v_x = 0LL);
  if (toBoolean(1LL)) {
  }
  LINE(25,f_x());
  LINE(26,x_var_dump(1, 2LL /* n2::N2 */));
  LINE(28,x_var_dump(1, v_x));
  (v_n2 = ((Object)(LINE(29,p_n2(p_n2(NEWOBJ(c_n2)())->create())))));
  LINE(30,v_n2.o_invoke_few_args("y", 0x7F64BC1ECEF60CCCLL, 0));
  LINE(31,x_var_dump(1, v_x));
  LINE(33,f_z());
  LINE(34,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
