
#ifndef __GENERATED_php_phc_test_subjects_dynamic_use_interface_h__
#define __GENERATED_php_phc_test_subjects_dynamic_use_interface_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/dynamic/use_interface.fw.h>

// Declarations
#include <cls/x.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$dynamic$use_interface_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_dynamic_use_interface_h__
