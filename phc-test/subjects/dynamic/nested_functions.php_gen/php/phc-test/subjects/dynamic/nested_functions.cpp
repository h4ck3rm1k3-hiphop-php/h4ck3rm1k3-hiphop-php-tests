
#include <php/phc-test/subjects/dynamic/nested_functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/dynamic/nested_functions.php line 3 */
void f_x() {
  FUNCTION_INJECTION(x);
  echo("x\n");
} /* function */
/* SRC: phc-test/subjects/dynamic/nested_functions.php line 6 */
void f_y() {
  FUNCTION_INJECTION(y);
  echo("y\n");
} /* function */
/* SRC: phc-test/subjects/dynamic/nested_functions.php line 9 */
void f_z() {
  FUNCTION_INJECTION(z);
  echo("z\n");
} /* function */
Variant pm_php$phc_test$subjects$dynamic$nested_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/dynamic/nested_functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$dynamic$nested_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(16,f_x());
  LINE(17,f_y());
  LINE(18,f_z());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
