
#ifndef __GENERATED_php_phc_test_subjects_dynamic_dynamic_method_h__
#define __GENERATED_php_phc_test_subjects_dynamic_dynamic_method_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/dynamic/dynamic_method.fw.h>

// Declarations
#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_a_DupId0();
void f_a_DupId1();
Variant pm_php$phc_test$subjects$dynamic$dynamic_method_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant i_a_DupId0(CArrRef params);
Variant i_a_DupId1(CArrRef params);
Object co_a(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_dynamic_dynamic_method_h__
