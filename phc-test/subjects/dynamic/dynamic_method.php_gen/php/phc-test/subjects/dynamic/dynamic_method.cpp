
#include <php/phc-test/subjects/dynamic/dynamic_method.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/dynamic/dynamic_method.php line 3 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x5C7A29AC25A4F8E4LL, c) {
        return (t_c(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x5C7A29AC25A4F8E4LL, c) {
        return (t_c(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: phc-test/subjects/dynamic/dynamic_method.php line 5 */
void c_a::t_c() {
  INSTANCE_METHOD_INJECTION(A, A::c);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a;

  if (toBoolean(v_a)) {
    g->i_a = i_a_DupId0;
    g->declareFunction("a");
  }
  else {
    g->i_a = i_a_DupId1;
    g->declareFunction("a");
  }
} /* function */
/* SRC: phc-test/subjects/dynamic/dynamic_method.php line 9 */
void f_a_DupId0() {
  FUNCTION_INJECTION(a);
  print("First a\n");
} /* function */
/* SRC: phc-test/subjects/dynamic/dynamic_method.php line 16 */
void f_a_DupId1() {
  FUNCTION_INJECTION(a);
  print("Second a\n");
} /* function */
Variant i_a_DupId0(CArrRef params) {
  return (f_a_DupId0(), null);
}
Variant i_a(CArrRef params) {
  DECLARE_GLOBAL_VARIABLES(g);
  return g->i_a(params);
}
Variant i_a_DupId1(CArrRef params) {
  return (f_a_DupId1(), null);
}
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$dynamic$dynamic_method_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/dynamic/dynamic_method.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$dynamic$dynamic_method_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = ((Object)(LINE(24,p_a(p_a(NEWOBJ(c_a)())->create())))));
  LINE(25,v_a.o_invoke_few_args("c", 0x5C7A29AC25A4F8E4LL, 0));
  LINE(26,get_global_variables()->i_a(Array()));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
