
#ifndef __GENERATED_php_phc_test_subjects_dynamic_dynamic_class_h__
#define __GENERATED_php_phc_test_subjects_dynamic_dynamic_class_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/dynamic/dynamic_class.fw.h>

// Declarations
#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$dynamic$dynamic_class_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_h();
Object co_a(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_dynamic_dynamic_class_h__
