
#ifndef __GENERATED_php_phc_test_subjects_phc_list_passes_h__
#define __GENERATED_php_phc_test_subjects_phc_list_passes_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/phc/list_passes.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$phc$list_passes_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_phc_list_passes_h__
