
#ifndef __GENERATED_php_phc_test_subjects_phc_param_is_ref_h__
#define __GENERATED_php_phc_test_subjects_phc_param_is_ref_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/phc/param_is_ref.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$phc$param_is_ref_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_phc_param_is_ref_h__
