
#ifndef __GENERATED_php_phc_test_subjects_phc_key_value_pair_h__
#define __GENERATED_php_phc_test_subjects_phc_key_value_pair_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/phc/key_value_pair.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$phc$key_value_pair_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_phc_key_value_pair_h__
