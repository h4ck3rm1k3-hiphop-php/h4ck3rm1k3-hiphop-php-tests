
#ifndef __GENERATED_php_phc_test_subjects_phc_debug1_h__
#define __GENERATED_php_phc_test_subjects_phc_debug1_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/phc/debug1.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_y(Variant v_x);
Variant pm_php$phc_test$subjects$phc$debug1_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_phc_debug1_h__
