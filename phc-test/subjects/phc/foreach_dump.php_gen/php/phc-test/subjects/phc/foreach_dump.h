
#ifndef __GENERATED_php_phc_test_subjects_phc_foreach_dump_h__
#define __GENERATED_php_phc_test_subjects_phc_foreach_dump_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/phc/foreach_dump.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$phc$foreach_dump_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_phc_foreach_dump_h__
