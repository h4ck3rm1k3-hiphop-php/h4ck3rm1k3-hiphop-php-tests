
#include <php/phc-test/subjects/unsupported/nested_foreach_with_refs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$nested_foreach_with_refs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/nested_foreach_with_refs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$nested_foreach_with_refs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x1") : g->GV(x1);
  Variant &v_x2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x2") : g->GV(x2);

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(v_a);
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(NULL, v_x1); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(4);
          Variant map5 = ref(v_a);
          map5.escalate();
          for (MutableArrayIterPtr iter6 = map5.begin(NULL, v_x2); iter6->advance();) {
            LOOP_COUNTER_CHECK(4);
            {
              echo(LINE(8,concat4(toString(v_x1), " ", toString(v_x2), "\n")));
              v_x1++;
              v_x2++;
            }
          }
        }
      }
    }
  }
  LINE(14,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
