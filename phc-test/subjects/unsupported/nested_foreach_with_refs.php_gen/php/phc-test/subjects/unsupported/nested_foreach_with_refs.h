
#ifndef __GENERATED_php_phc_test_subjects_unsupported_nested_foreach_with_refs_h__
#define __GENERATED_php_phc_test_subjects_unsupported_nested_foreach_with_refs_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/unsupported/nested_foreach_with_refs.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$unsupported$nested_foreach_with_refs_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_unsupported_nested_foreach_with_refs_h__
