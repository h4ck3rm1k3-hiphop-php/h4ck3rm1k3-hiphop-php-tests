
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$unsupported$nested_foreach_with_refs_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_INCLUDE(0x0AA809F9379A58A2LL, "phc-test/subjects/unsupported/nested_foreach_with_refs.php", php$phc_test$subjects$unsupported$nested_foreach_with_refs_php);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$phc_test$subjects$unsupported$nested_foreach_with_refs_php(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
