
#ifndef __GENERATED_php_phc_test_subjects_unsupported_integer_method_name_h__
#define __GENERATED_php_phc_test_subjects_unsupported_integer_method_name_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/unsupported/integer_method_name.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$unsupported$integer_method_name_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_unsupported_integer_method_name_h__
