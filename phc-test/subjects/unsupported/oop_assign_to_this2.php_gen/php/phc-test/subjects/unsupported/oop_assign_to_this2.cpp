
#include <php/phc-test/subjects/unsupported/oop_assign_to_this2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/unsupported/oop_assign_to_this2.php line 4 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_x = 5LL;
}
/* SRC: phc-test/subjects/unsupported/oop_assign_to_this2.php line 8 */
void c_c::t_foo() {
  INSTANCE_METHOD_INJECTION(C, C::foo);
  DECLARE_GLOBAL_VARIABLES(g);
  String v_n;


  class VariableTable : public RVariableTable {
  public:
    String &v_n;
    VariableTable(String &r_n) : v_n(r_n) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 1) {
        case 1:
          HASH_RETURN(0x117B8667E4662809LL, v_n,
                      n);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_n);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_n = "this");
  LINE(11,x_var_dump(1, variables->get(v_n).o_get("x", 0x04BFC205E59FA416LL)));
} /* function */
/* SRC: phc-test/subjects/unsupported/oop_assign_to_this2.php line 14 */
void c_c::t_bar() {
  INSTANCE_METHOD_INJECTION(C, C::bar);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_n;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_n;
    VariableTable(Variant &r_n) : v_n(r_n) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 1:
          HASH_RETURN(0x117B8667E4662809LL, v_n,
                      n);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_n);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_n = "this");
  (variables->get(toString(v_n)) = ((Object)(LINE(17,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  LINE(18,x_var_dump(1, ((Object)(this))));
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$unsupported$oop_assign_to_this2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/oop_assign_to_this2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$oop_assign_to_this2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_c = ((Object)(LINE(22,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(23,v_c.o_invoke_few_args("foo", 0x6176C0B993BF5914LL, 0));
  LINE(24,v_c.o_invoke_few_args("bar", 0x5E008C5BA4C3B3FDLL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
