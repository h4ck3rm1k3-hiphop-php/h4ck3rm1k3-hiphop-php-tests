
#ifndef __GENERATED_php_phc_test_subjects_unsupported_foreach_as_while_h__
#define __GENERATED_php_phc_test_subjects_unsupported_foreach_as_while_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/unsupported/foreach_as_while.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$unsupported$foreach_as_while_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_unsupported_foreach_as_while_h__
