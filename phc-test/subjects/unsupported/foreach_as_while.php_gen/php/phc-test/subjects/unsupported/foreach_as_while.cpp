
#include <php/phc-test/subjects/unsupported/foreach_as_while.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$foreach_as_while_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/foreach_as_while.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$foreach_as_while_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array") : g->GV(array);
  Variant &v_temp_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("temp_array") : g->GV(temp_array);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_array = ScalarArrays::sa_[0]);
  (v_temp_array = v_array);
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(7,x_each(ref(v_temp_array))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(9,x_var_export(v_key));
        LINE(10,x_var_export(v_val));
      }
    }
  }
  LINE(12,x_var_export(v_array));
  LINE(13,x_reset(ref(v_array)));
  echo("-------------- 1 -----------------\n");
  (v_temp_array = ref(v_array));
  LOOP_COUNTER(2);
  {
    while (toBoolean(df_lambda_2(LINE(18,x_each(ref(v_temp_array))), v_key))) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_val = v_temp_array.rvalAt(v_key));
        LINE(21,x_var_export(v_key));
        LINE(22,x_var_export(v_val));
      }
    }
  }
  LINE(24,x_var_export(v_array));
  LINE(25,x_reset(ref(v_array)));
  echo("-------------- 2 -----------------\n");
  LOOP_COUNTER(3);
  {
    while (toBoolean(df_lambda_3(LINE(29,x_each(ref(v_temp_array))), v_key))) {
      LOOP_COUNTER_CHECK(3);
      {
        (v_val = ref(v_key));
        LINE(32,x_var_export(v_key));
        LINE(33,x_var_export(v_val));
      }
    }
  }
  LINE(35,x_var_export(v_array));
  LINE(36,x_reset(ref(v_array)));
  echo("-------------- 3 -----------------\n");
  LOOP_COUNTER(4);
  {
    while (toBoolean(df_lambda_4(LINE(40,x_each(ref(v_temp_array))), v_key))) {
      LOOP_COUNTER_CHECK(4);
      {
        (v_val = ref(v_key));
        LINE(43,x_var_export(v_key));
        LINE(44,x_var_export(v_val));
      }
    }
  }
  LINE(46,x_var_export(v_array));
  LINE(47,x_reset(ref(v_array)));
  echo("-------------- 4 -----------------\n");
  LOOP_COUNTER(5);
  {
    while (toBoolean((v_key = LINE(51,x_each(ref(v_temp_array)))))) {
      LOOP_COUNTER_CHECK(5);
      {
        (v_val = ref(v_key));
        LINE(54,x_var_export(v_key));
        LINE(55,x_var_export(v_val));
      }
    }
  }
  LINE(57,x_var_export(v_array));
  LINE(58,x_reset(ref(v_array)));
  echo("-------------- 5 -----------------\n");
  {
    LOOP_COUNTER(6);
    Variant map7 = ref(v_array);
    map7.escalate();
    for (MutableArrayIterPtr iter8 = map7.begin(&v_key, v_val); iter8->advance();) {
      LOOP_COUNTER_CHECK(6);
      {
        v_key *= 2LL;
        LINE(66,x_var_export(v_key));
        LINE(67,x_var_export(v_val));
      }
    }
  }
  LINE(69,x_var_export(v_array));
  LINE(70,x_reset(ref(v_array)));
  echo("-------------- 6 -----------------\n");
  (v_a = ScalarArrays::sa_[1]);
  LINE(81,x_reset(ref(v_a)));
  LOOP_COUNTER(9);
  {
    while (toBoolean(df_lambda_5(LINE(82,x_each(ref(v_a))), v_x))) {
      LOOP_COUNTER_CHECK(9);
      {
        (v_b = v_a);
        (v_c = ref(v_a));
        LINE(86,x_var_export(v_a.rvalAt(v_x)));
      }
    }
  }
  echo("\n");
  echo("-------------- 7 -----------------\n");
  {
    LOOP_COUNTER(10);
    Variant map11 = ref(v_a);
    map11.escalate();
    for (MutableArrayIterPtr iter12 = map11.begin(NULL, v_x); iter12->advance();) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_b = v_a);
        v_b.set(3LL, ("A"), 0x135FDDF6A6BFBBDDLL);
        LINE(96,x_var_export(v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
        LINE(97,x_var_export(v_a));
        LINE(98,x_var_export(v_b));
      }
    }
  }
  echo("-------------- 8 -----------------\n");
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_a.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_x = iter15->second();
      {
        (v_b = v_a);
        v_b.set(3LL, ("A"), 0x135FDDF6A6BFBBDDLL);
        LINE(106,x_var_export(v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
        LINE(107,x_var_export(v_a));
        LINE(108,x_var_export(v_b));
      }
    }
  }
  echo("-------------- 9 -----------------\n");
  {
    LOOP_COUNTER(16);
    Variant map17 = ref(v_a);
    map17.escalate();
    for (MutableArrayIterPtr iter18 = map17.begin(NULL, v_x); iter18->advance();) {
      LOOP_COUNTER_CHECK(16);
      {
        LOOP_COUNTER(19);
        {
          while (toBoolean(LINE(116,x_each(ref(v_a))))) {
            LOOP_COUNTER_CHECK(19);
            {
            }
          }
        }
        (v_b = v_a);
        v_b.set(3LL, ("A"), 0x135FDDF6A6BFBBDDLL);
        LINE(119,x_var_export(v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
        LINE(120,x_var_export(v_a));
        LINE(121,x_var_export(v_b));
      }
    }
  }
  echo("-------------- 10 ----------------\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
