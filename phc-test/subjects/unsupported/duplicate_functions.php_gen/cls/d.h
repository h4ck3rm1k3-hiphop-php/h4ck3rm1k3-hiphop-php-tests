
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/unsupported/duplicate_functions.php line 8 */
class c_d : virtual public ObjectData {
  BEGIN_CLASS_MAP(d)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, ObjectData)
  void init();
  public: void t_dc(CVarRef v_x, p_d v_y);
  public: void t_dc(CArrRef v_x, CArrRef v_y);
  public: void t_dg(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
  public: void t_dg(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
