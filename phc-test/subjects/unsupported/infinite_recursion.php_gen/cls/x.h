
#ifndef __GENERATED_cls_x_h__
#define __GENERATED_cls_x_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/unsupported/infinite_recursion.php line 3 */
class c_x : virtual public ObjectData {
  BEGIN_CLASS_MAP(x)
  END_CLASS_MAP(x)
  DECLARE_CLASS(x, X, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_x_h__
