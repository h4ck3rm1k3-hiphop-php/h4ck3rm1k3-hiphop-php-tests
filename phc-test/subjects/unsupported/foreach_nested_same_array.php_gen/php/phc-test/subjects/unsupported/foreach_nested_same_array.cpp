
#include <php/phc-test/subjects/unsupported/foreach_nested_same_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$foreach_nested_same_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/foreach_nested_same_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$foreach_nested_same_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_x = iter3->second();
      {
        LINE(6,x_var_export(v_x));
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_y = iter6->second();
            {
              LINE(9,x_var_export(v_y));
            }
          }
        }
        echo("\n");
      }
    }
  }
  echo("----------------------------------\n");
  LINE(14,x_var_export(v_x));
  LINE(15,x_var_export(v_y));
  {
    LOOP_COUNTER(7);
    Variant map8 = ref(v_a);
    map8.escalate();
    for (MutableArrayIterPtr iter9 = map8.begin(NULL, v_x); iter9->advance();) {
      LOOP_COUNTER_CHECK(7);
      {
        LINE(21,x_var_export(v_x));
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = v_a.begin(); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_y = iter12->second();
            {
              LINE(24,x_var_export(v_y));
            }
          }
        }
        echo("\n");
      }
    }
  }
  echo("----------------------------------\n");
  LINE(29,x_var_export(v_x));
  LINE(30,x_var_export(v_y));
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_a.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_x = iter15->second();
      {
        LINE(36,x_var_export(v_x));
        {
          LOOP_COUNTER(16);
          Variant map17 = ref(v_a);
          map17.escalate();
          for (MutableArrayIterPtr iter18 = map17.begin(NULL, v_y); iter18->advance();) {
            LOOP_COUNTER_CHECK(16);
            {
              LINE(39,x_var_export(v_y));
            }
          }
        }
        echo("\n");
      }
    }
  }
  echo("----------------------------------\n");
  LINE(44,x_var_export(v_x));
  LINE(45,x_var_export(v_y));
  {
    LOOP_COUNTER(19);
    Variant map20 = ref(v_a);
    map20.escalate();
    for (MutableArrayIterPtr iter21 = map20.begin(NULL, v_x); iter21->advance();) {
      LOOP_COUNTER_CHECK(19);
      {
        LINE(50,x_var_export(v_x));
        {
          LOOP_COUNTER(22);
          Variant map23 = ref(v_a);
          map23.escalate();
          for (MutableArrayIterPtr iter24 = map23.begin(NULL, v_y); iter24->advance();) {
            LOOP_COUNTER_CHECK(22);
            {
              LINE(53,x_var_export(v_y));
            }
          }
        }
        echo("\n");
      }
    }
  }
  echo("----------------------------------\n");
  LINE(58,x_var_export(v_x));
  LINE(59,x_var_export(v_y));
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(25);
    Variant map26 = ref(v_a);
    map26.escalate();
    for (MutableArrayIterPtr iter27 = map26.begin(NULL, v_x); iter27->advance();) {
      LOOP_COUNTER_CHECK(25);
      {
        LINE(65,x_var_export(v_x));
        v_x *= 2LL;
        {
          LOOP_COUNTER(28);
          Variant map29 = ref(v_a);
          map29.escalate();
          for (MutableArrayIterPtr iter30 = map29.begin(NULL, v_y); iter30->advance();) {
            LOOP_COUNTER_CHECK(28);
            {
              v_y *= 3LL;
              LINE(70,x_var_export(v_y));
            }
          }
        }
      }
    }
  }
  LINE(73,x_var_export(v_x));
  LINE(74,x_var_export(v_y));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
