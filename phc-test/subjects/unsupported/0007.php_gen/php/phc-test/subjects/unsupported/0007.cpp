
#include <php/phc-test/subjects/unsupported/0007.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/unsupported/0007.php line 2 */
Variant f_factorial(CVarRef v_n) {
  FUNCTION_INJECTION(factorial);
  Variant v_a;

  return v_a;
} /* function */
Variant pm_php$phc_test$subjects$unsupported$0007_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/0007.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$0007_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  (v_c = LINE(6,f_factorial(v_b)));
  (v_d = (LINE(7,x_debug_zval_dump(v_c)), null));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
