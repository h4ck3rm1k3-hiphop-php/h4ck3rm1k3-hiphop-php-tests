
#ifndef __GENERATED_php_phc_test_subjects_unsupported_0007_h__
#define __GENERATED_php_phc_test_subjects_unsupported_0007_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/unsupported/0007.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$unsupported$0007_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_factorial(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_unsupported_0007_h__
