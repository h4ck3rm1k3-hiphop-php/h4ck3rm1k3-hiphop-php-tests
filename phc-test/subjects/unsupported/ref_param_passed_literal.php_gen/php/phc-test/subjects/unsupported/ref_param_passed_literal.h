
#ifndef __GENERATED_php_phc_test_subjects_unsupported_ref_param_passed_literal_h__
#define __GENERATED_php_phc_test_subjects_unsupported_ref_param_passed_literal_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/unsupported/ref_param_passed_literal.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_f(Variant v_x);
Variant pm_php$phc_test$subjects$unsupported$ref_param_passed_literal_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_unsupported_ref_param_passed_literal_h__
