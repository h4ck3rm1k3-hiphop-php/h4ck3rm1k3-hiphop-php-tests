
#include <php/phc-test/subjects/unsupported/resource_as_key.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$resource_as_key_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/resource_as_key.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$resource_as_key_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_res __attribute__((__unused__)) = (variables != gVariables) ? variables->get("res") : g->GV(res);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  (v_res = LINE(2,x_opendir(".")));
  (v_arr = Array(ArrayInit(1).set(0, v_res, 7LL).create()));
  LINE(5,x_var_dump(1, v_arr));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
