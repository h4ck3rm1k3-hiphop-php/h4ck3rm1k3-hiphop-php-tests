
#include <php/phc-test/subjects/unsupported/0042.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$0042_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/0042.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$0042_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_TLE27 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE27") : g->GV(TLE27);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);
  Variant &v_TSe1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSe1") : g->GV(TSe1);

  v_x.set("ASDASD", ("ASDASD"), 0x0ACF876EB2F2C59ALL);
  v_x.set(v_TLE27, (v_TLE27));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_x.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_key = iter3->first();
      {
        (v_TSe1 = (LINE(11,x_var_dump(1, v_value)), null));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
