
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$unsupported$include_complex_unsupported_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$horrible$invocation_priorities_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_INCLUDE(0x548AA19BFEB13ED1LL, "phc-test/subjects/horrible/invocation_priorities.php", php$phc_test$subjects$horrible$invocation_priorities_php);
      break;
    case 2:
      HASH_INCLUDE(0x048EA92D502B7902LL, "phc-test/subjects/unsupported/include_complex_unsupported.php", php$phc_test$subjects$unsupported$include_complex_unsupported_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
