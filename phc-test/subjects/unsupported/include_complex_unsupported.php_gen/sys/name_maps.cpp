
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "h", "phc-test/subjects/unsupported/include_complex_unsupported.php",
  "x", "phc-test/subjects/horrible/invocation_priorities.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "fail", "phc-test/subjects/unsupported/include_complex_unsupported.php",
  "foo", "phc-test/subjects/horrible/invocation_priorities.php",
  "g", "phc-test/subjects/unsupported/include_complex_unsupported.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
