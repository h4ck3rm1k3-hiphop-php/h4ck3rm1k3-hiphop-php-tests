
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "f",
    "a",
    "x",
    "a1",
    "a2",
    "b",
    "c",
    "y",
    "xx",
    "h",
    "i1",
    "i2",
    "j",
    "k",
  };
  if (idx >= 0 && idx < 26) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(f);
    case 13: return GV(a);
    case 14: return GV(x);
    case 15: return GV(a1);
    case 16: return GV(a2);
    case 17: return GV(b);
    case 18: return GV(c);
    case 19: return GV(y);
    case 20: return GV(xx);
    case 21: return GV(h);
    case 22: return GV(i1);
    case 23: return GV(i2);
    case 24: return GV(j);
    case 25: return GV(k);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
