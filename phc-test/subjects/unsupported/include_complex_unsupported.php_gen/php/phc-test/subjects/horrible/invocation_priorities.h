
#ifndef __GENERATED_php_phc_test_subjects_horrible_invocation_priorities_h__
#define __GENERATED_php_phc_test_subjects_horrible_invocation_priorities_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/horrible/invocation_priorities.fw.h>

// Declarations
#include <cls/x.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$horrible$invocation_priorities_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_foo();
Object co_x(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_horrible_invocation_priorities_h__
