
#include <php/phc-test/subjects/horrible/invocation_priorities.h>
#include <php/phc-test/subjects/unsupported/include_complex_unsupported.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/unsupported/include_complex_unsupported.php line 179 */
Variant c_h::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_h::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_h::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_h::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_h::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_h::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_h::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_h::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(h)
ObjectData *c_h::create() {
  init();
  t_h();
  return this;
}
ObjectData *c_h::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_h::cloneImpl() {
  c_h *obj = NEW(c_h)();
  cloneSet(obj);
  return obj;
}
void c_h::cloneSet(c_h *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_h::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x596FD6C55E8464CCLL, g) {
        return (t_g(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_h::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x596FD6C55E8464CCLL, g) {
        return (t_g(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_h::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_h$os_get(const char *s) {
  return c_h::os_get(s, -1);
}
Variant &cw_h$os_lval(const char *s) {
  return c_h::os_lval(s, -1);
}
Variant cw_h$os_constant(const char *s) {
  return c_h::os_constant(s);
}
Variant cw_h$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_h::os_invoke(c, s, params, -1, fatal);
}
void c_h::init() {
}
/* SRC: phc-test/subjects/unsupported/include_complex_unsupported.php line 181 */
void c_h::t_h() {
  INSTANCE_METHOD_INJECTION(H, H::H);
  bool oldInCtor = gasInCtor(true);
  echo("H\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/unsupported/include_complex_unsupported.php line 186 */
void c_h::t_g() {
  INSTANCE_METHOD_INJECTION(H, H::g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a1;
  Variant v_a2;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_a1; Variant &v_a2;
    VariableTable(Variant &r_a1, Variant &r_a2) : v_a1(r_a1), v_a2(r_a2) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x6C05C2858C72A876LL, v_a2,
                      a2);
          break;
        case 3:
          HASH_RETURN(0x68DF81F26D942FC7LL, v_a1,
                      a1);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_a1, v_a2);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_a1 = "old string");
  (v_a2 = "old string");
  echo("about to include included_var_overwrite.php\n");
  LINE(192,include("included_var_overwrite.php", false, variables, "phc-test/subjects/unsupported/"));
  if ((equal(v_a1, "old value")) || (equal(v_a2, "my old value"))) {
    LINE(196,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 196, "var overwrite within class"));
  }
} /* function */
/* SRC: phc-test/subjects/unsupported/include_complex_unsupported.php line 159 */
void f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a1;
  Variant v_a2;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_a1; Variant &v_a2;
    VariableTable(Variant &r_a1, Variant &r_a2) : v_a1(r_a1), v_a2(r_a2) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x6C05C2858C72A876LL, v_a2,
                      a2);
          break;
        case 3:
          HASH_RETURN(0x68DF81F26D942FC7LL, v_a1,
                      a1);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_a1, v_a2);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_a1 = "old string");
  (v_a2 = "old string");
  echo("about to include included_var_overwrite.php\n");
  LINE(165,require("included_var_overwrite.php", false, variables, "phc-test/subjects/unsupported/"));
  LINE(167,x_var_dump(1, v_a1));
  LINE(168,x_var_dump(1, v_a2));
  if ((equal(v_a1, "old value")) || (equal(v_a2, "my old value"))) {
    LINE(172,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 172, "var overwrite within function"));
  }
} /* function */
/* SRC: phc-test/subjects/unsupported/include_complex_unsupported.php line 29 */
void f_fail(CStrRef v_file, int v_line, CStrRef v_reason) {
  FUNCTION_INJECTION(fail);
  print(concat("Failure: '", LINE(31,concat6(v_reason, "' on ", v_file, ":", toString(v_line), "\n"))));
} /* function */
Object co_h(CArrRef params, bool init /* = true */) {
  return Object(p_h(NEW(c_h)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$unsupported$include_complex_unsupported_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/include_complex_unsupported.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$include_complex_unsupported_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_xx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xx") : g->GV(xx);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_i1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i1") : g->GV(i1);
  Variant &v_i2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i2") : g->GV(i2);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  (v_a1 = "old value");
  (v_a2 = "my old value");
  echo("about to include included_var_overwrite.php\n");
  LINE(41,include("included_var_overwrite.php", false, variables, "phc-test/subjects/unsupported/"));
  LINE(43,x_var_dump(1, v_a1));
  LINE(44,x_var_dump(1, v_a2));
  if ((equal(v_a1, "old value")) || (equal(v_a2, "my old value"))) {
    LINE(48,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 48, "var overwrite"));
  }
  echo("about to include included_classes_and_functions.php\n");
  LINE(57,include("included_classes_and_functions.php", false, variables, "phc-test/subjects/unsupported/"));
  (v_b = LINE(59,create_object("b", Array())));
  LINE(60,x_var_dump(1, v_b));
  (v_b = LINE(61,v_b.o_invoke_few_args("b", 0x39E9518192E6D2AELL, 0)));
  LINE(62,x_var_dump(1, v_b));
  if (!(toBoolean(v_b))) {
    LINE(65,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 65, "included class"));
  }
  (v_b = LINE(67,invoke_failed("b", Array(), 0x0000000092E6D2AELL)));
  LINE(68,x_var_dump(1, v_b));
  if (!(toBoolean(v_b))) {
    LINE(71,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 71, "included function"));
  }
  echo("about to include included_classes_with_return_values.php\n");
  LINE(81,include("included_classes_with_return_values.php", false, variables, "phc-test/subjects/unsupported/"));
  (v_c = LINE(83,create_object("c", Array())));
  LINE(84,x_var_dump(1, v_c));
  (v_c = LINE(85,v_c.o_invoke_few_args("cc", 0x28E4E7211D305D44LL, 0)));
  LINE(86,x_var_dump(1, v_c));
  if (!equal(v_c, 7LL)) {
    LINE(89,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 89, "class with return value"));
  }
  echo("about to include include_dir/../../bugs/././../horrible/invocation_priorities.php\n");
  LINE(97,pm_php$phc_test$subjects$horrible$invocation_priorities_php(false, variables));
  LINE(99,x_var_dump(1, v_f));
  LINE(100,x_var_dump(1, v_a));
  LINE(101,x_var_dump(1, v_x));
  if (!(toBoolean(v_x))) {
    LINE(104,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 104, "directory levels"));
  }
  (v_x = "zzz");
  (v_y = "zzz");
  (v_xx = "zzz");
  echo("about to include ./test/subjects/horrible/obfuscated_foreach.php\n");
  LINE(114,include("./test/subjects/horrible/obfuscated_foreach.php", false, variables, "phc-test/subjects/unsupported/"));
  LINE(116,x_var_dump(1, v_x));
  LINE(117,x_var_dump(1, v_y));
  LINE(118,x_var_dump(1, v_xx));
  if (!(toBoolean(v_xx))) {
    LINE(121,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 121, "current working directory"));
  }
  echo("about to include test/subjects/horrible/obfuscated_foreach.php\n");
  LINE(129,include("test/subjects/horrible/obfuscated_foreach.php", false, variables, "phc-test/subjects/unsupported/"));
  LINE(131,x_var_dump(1, v_x));
  LINE(132,x_var_dump(1, v_y));
  LINE(133,x_var_dump(1, v_xx));
  if (!(toBoolean(v_xx))) {
    LINE(136,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 136, "current working directory"));
  }
  echo("about to include included_recursive1.php\n");
  LINE(148,require("included_recursive1.php", false, variables, "phc-test/subjects/unsupported/"));
  LINE(150,x_var_dump(1, v_f));
  if (!same(v_f, 26LL)) {
    LINE(153,f_fail(get_source_filename("phc-test/subjects/unsupported/include_complex_unsupported.php"), 153, "recursive include"));
  }
  LINE(175,f_g());
  (v_h = ((Object)(LINE(200,p_h(p_h(NEWOBJ(c_h)())->create())))));
  LINE(201,x_var_dump(1, v_h));
  (v_h = LINE(202,v_h.o_invoke_few_args("g", 0x596FD6C55E8464CCLL, 0)));
  LINE(203,x_var_dump(1, v_h));
  (v_i1 = "some value");
  (v_i2 = "another value");
  echo("about to include included_use_existing_vars.php\n");
  LINE(211,include("included_use_existing_vars.php", false, variables, "phc-test/subjects/unsupported/"));
  echo("about to include included_use_vars_in_function.php\n");
  LINE(216,include("included_use_vars_in_function.php", false, variables, "phc-test/subjects/unsupported/"));
  (v_j = LINE(218,invoke_failed("j", Array(ArrayInit(2).set(0, 1LL).set(1, 2LL).create()), 0x000000009A63801DLL)));
  LINE(219,x_var_dump(1, v_j));
  echo("about to include included_use_vars_in_classes.php\n");
  LINE(224,include("included_use_vars_in_classes.php", false, variables, "phc-test/subjects/unsupported/"));
  (v_k = LINE(226,create_object("k", Array())));
  LINE(227,x_var_dump(1, v_k));
  (v_k = LINE(228,v_k.o_invoke_few_args("kk", 0x75BE8B140F69E8ECLL, 0)));
  LINE(229,x_var_dump(1, v_k));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
