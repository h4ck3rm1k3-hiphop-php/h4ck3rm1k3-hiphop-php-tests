
#ifndef __GENERATED_php_phc_test_subjects_unsupported_include_complex_unsupported_h__
#define __GENERATED_php_phc_test_subjects_unsupported_include_complex_unsupported_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/unsupported/include_complex_unsupported.fw.h>

// Declarations
#include <cls/h.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_g();
Variant pm_php$phc_test$subjects$unsupported$include_complex_unsupported_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_fail(CStrRef v_file, int v_line, CStrRef v_reason);
Object co_h(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_unsupported_include_complex_unsupported_h__
