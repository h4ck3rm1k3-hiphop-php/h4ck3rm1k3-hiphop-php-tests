
#ifndef __GENERATED_cls_x_h__
#define __GENERATED_cls_x_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/horrible/invocation_priorities.php line 2 */
class c_x : virtual public ObjectData {
  BEGIN_CLASS_MAP(x)
  END_CLASS_MAP(x)
  DECLARE_CLASS(x, X, ObjectData)
  void init();
  public: String m_f;
  public: Variant m_a;
  public: void t_x();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_foo();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_x_h__
