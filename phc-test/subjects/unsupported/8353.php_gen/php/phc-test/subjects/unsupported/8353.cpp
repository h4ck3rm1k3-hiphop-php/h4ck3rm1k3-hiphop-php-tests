
#include <php/phc-test/subjects/unsupported/8353.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$8353_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/8353.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$8353_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_var __attribute__((__unused__)) = (variables != gVariables) ? variables->get("var") : g->GV(var);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  v_var.append(("1"));
  v_var.append(("2"));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_var.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_test = iter3->second();
      {
        echo(toString(v_test));
      }
    }
  }
  LINE(10,x_reset(ref(v_var)));
  echo(toString(LINE(11,x_current(ref(v_var)))));
  v_var.append(("1"));
  v_var.append(("2"));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_var.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_test = iter6->second();
      {
        echo(toString(v_test));
        LINE(20,x_current(ref(v_var)));
      }
    }
  }
  echo(toString(LINE(22,x_current(ref(v_var)))));
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_arr.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
    }
  }
  LINE(29,x_var_dump(1, x_current(ref(v_arr))));
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_arr.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_v = iter12->second();
    }
  }
  LINE(34,x_reset(ref(v_arr)));
  LINE(35,x_var_dump(1, x_current(ref(v_arr))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
