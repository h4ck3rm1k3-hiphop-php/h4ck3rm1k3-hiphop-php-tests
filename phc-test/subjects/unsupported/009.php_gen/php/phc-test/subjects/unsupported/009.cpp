
#include <php/phc-test/subjects/unsupported/009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 5LL);
  LINE(3,x_var_dump(1, v_x.rvalAt("a", 0x4292CEE227B9150ALL)));
  LINE(4,x_var_dump(1, v_x));
  v_x.set("a", (6LL), 0x4292CEE227B9150ALL);
  LINE(6,x_var_dump(1, v_x.rvalAt("a", 0x4292CEE227B9150ALL)));
  LINE(7,x_var_dump(1, v_x));
  unset(v_x);
  v_x.set("a", (6LL), 0x4292CEE227B9150ALL);
  LINE(10,x_var_dump(1, v_x.rvalAt("a", 0x4292CEE227B9150ALL)));
  LINE(11,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
