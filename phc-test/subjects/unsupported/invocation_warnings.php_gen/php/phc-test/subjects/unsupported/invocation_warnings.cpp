
#include <php/phc-test/subjects/unsupported/invocation_warnings.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$invocation_warnings_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/invocation_warnings.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$invocation_warnings_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  ;
  ;
  LINE(5,v_x.o_invoke_few_args("x", 0x00F31DE90234EA81LL, 0));
  LINE(6,v_x.o_invoke_few_args(toString(v_x), -1LL, 0));
  ;
  ;
  ;
  (assignCallTemp(eo_0, toObject(LINE(12,v_x.o_invoke_few_args("x", 0x00F31DE90234EA81LL, 0)))),eo_0.o_invoke_few_args("x", 0x00F31DE90234EA81LL, 0));
  (assignCallTemp(eo_1, toObject(LINE(13,v_x.o_invoke_few_args(toString(v_x), -1LL, 0)))),eo_1.o_invoke_few_args(toString(v_x), -1LL, 0));
  ;
  LINE(16,invoke((toString(v_x.o_get("x", 0x04BFC205E59FA416LL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1));
  LINE(17,invoke((toString(v_x.o_get("x", 0x04BFC205E59FA416LL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1)).o_get("y", 0x4F56B733A4DFC78ALL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL);
  LINE(18,invoke((toString(invoke((toString(v_x.o_get("x", 0x04BFC205E59FA416LL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1).o_get("y", 0x4F56B733A4DFC78ALL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1));
  ;
  LINE(21,v_x.o_invoke_few_args(toString(5LL), -1LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
