
#ifndef __GENERATED_php_phc_test_subjects_unsupported_reference_var_dump_interaction_h__
#define __GENERATED_php_phc_test_subjects_unsupported_reference_var_dump_interaction_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/unsupported/reference_var_dump_interaction.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$unsupported$reference_var_dump_interaction_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_unsupported_reference_var_dump_interaction_h__
