
#include <php/phc-test/subjects/unsupported/reference_var_dump_interaction.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$reference_var_dump_interaction_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/reference_var_dump_interaction.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$reference_var_dump_interaction_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);

  (v_a = ScalarArrays::sa_[0]);
  echo("=============================================\n");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_val = iter3->second();
      v_key = iter3->first();
      {
        LINE(7,x_var_dump(1, v_key));
        LINE(8,x_var_dump(1, v_val));
        LINE(9,x_var_dump(1, v_a));
      }
    }
  }
  echo("=============================================\n");
  {
    LOOP_COUNTER(4);
    Variant map5 = ref(v_a);
    map5.escalate();
    for (MutableArrayIterPtr iter6 = map5.begin(&v_key, v_val); iter6->advance();) {
      LOOP_COUNTER_CHECK(4);
      {
        LINE(16,x_var_dump(1, v_key));
        LINE(17,x_var_dump(1, v_val));
        LINE(18,x_var_dump(1, v_a));
      }
    }
  }
  echo("=============================================\n");
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_a.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_val = iter9->second();
      v_key = iter9->first();
      {
        LINE(25,x_var_dump(1, v_key));
        LINE(26,x_var_dump(1, v_val));
        unset(v_val);
        LINE(28,x_var_dump(1, v_a));
      }
    }
  }
  echo("=============================================\n");
  {
    LOOP_COUNTER(10);
    Variant map11 = ref(v_a);
    map11.escalate();
    for (MutableArrayIterPtr iter12 = map11.begin(&v_key, v_val); iter12->advance();) {
      LOOP_COUNTER_CHECK(10);
      {
        LINE(35,x_var_dump(1, v_key));
        LINE(36,x_var_dump(1, v_val));
        unset(v_val);
        LINE(38,x_var_dump(1, v_a));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
