
#include <php/phc-test/subjects/unsupported/break7.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$unsupported$break7_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/unsupported/break7.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$unsupported$break7_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("before\n");
  {
    LOOP_COUNTER(1);
    for (; ; ) {
      LOOP_COUNTER_CHECK(1);
      {
        LOOP_COUNTER(2);
        for (; ; ) {
          LOOP_COUNTER_CHECK(2);
          {
            LOOP_COUNTER(3);
            for (; ; ) {
              LOOP_COUNTER_CHECK(3);
              {
                LOOP_COUNTER(4);
                for (; ; ) {
                  LOOP_COUNTER_CHECK(4);
                  {
                    LOOP_COUNTER(5);
                    for (; ; ) {
                      LOOP_COUNTER_CHECK(5);
                      {
                        LOOP_COUNTER(6);
                        for (; ; ) {
                          LOOP_COUNTER_CHECK(6);
                          throw_fatal("bad break");
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  echo("after\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
