
#ifndef __GENERATED_php_phc_test_subjects_errors_push_operator_on_strings_h__
#define __GENERATED_php_phc_test_subjects_errors_push_operator_on_strings_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/push_operator_on_strings.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$errors$push_operator_on_strings_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_push_operator_on_strings_h__
