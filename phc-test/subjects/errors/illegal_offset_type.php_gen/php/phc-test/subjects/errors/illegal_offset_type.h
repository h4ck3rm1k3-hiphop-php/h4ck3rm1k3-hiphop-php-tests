
#ifndef __GENERATED_php_phc_test_subjects_errors_illegal_offset_type_h__
#define __GENERATED_php_phc_test_subjects_errors_illegal_offset_type_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/illegal_offset_type.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$errors$illegal_offset_type_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_illegal_offset_type_h__
