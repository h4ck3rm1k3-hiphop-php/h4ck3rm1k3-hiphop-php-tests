
#include <php/phc-test/subjects/errors/break_more_than_allowed2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$errors$break_more_than_allowed2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/errors/break_more_than_allowed2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$errors$break_more_than_allowed2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 7LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          Variant map3 = ScalarArrays::sa_[0];
          for (ArrayIterPtr iter4 = map3.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_x = iter4->second();
            {
              LOOP_COUNTER(5);
              {
                while (more(v_x, 0LL)) {
                  LOOP_COUNTER_CHECK(5);
                  {
                    throw_fatal("bad break");
                    v_x--;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
