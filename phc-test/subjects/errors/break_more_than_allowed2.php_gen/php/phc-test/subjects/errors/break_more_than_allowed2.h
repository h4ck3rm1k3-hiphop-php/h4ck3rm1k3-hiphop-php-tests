
#ifndef __GENERATED_php_phc_test_subjects_errors_break_more_than_allowed2_h__
#define __GENERATED_php_phc_test_subjects_errors_break_more_than_allowed2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/break_more_than_allowed2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$errors$break_more_than_allowed2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_break_more_than_allowed2_h__
