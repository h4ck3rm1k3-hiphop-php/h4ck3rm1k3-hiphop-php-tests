
#ifndef __GENERATED_php_phc_test_subjects_errors_invalid_interface_member_h__
#define __GENERATED_php_phc_test_subjects_errors_invalid_interface_member_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/invalid_interface_member.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$errors$invalid_interface_member_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_invalid_interface_member_h__
