
#ifndef __GENERATED_php_phc_test_subjects_errors_bad_hint_array_h__
#define __GENERATED_php_phc_test_subjects_errors_bad_hint_array_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/bad_hint_array.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_f(int64 v_x = 6LL);
Variant pm_php$phc_test$subjects$errors$bad_hint_array_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_bad_hint_array_h__
