
#ifndef __GENERATED_php_phc_test_subjects_errors_try_catch_break_h__
#define __GENERATED_php_phc_test_subjects_errors_try_catch_break_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/try_catch_break.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$errors$try_catch_break_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_try_catch_break_h__
