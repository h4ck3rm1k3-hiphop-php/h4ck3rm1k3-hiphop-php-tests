
#include <php/phc-test/subjects/errors/try_catch_break.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$errors$try_catch_break_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/errors/try_catch_break.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$errors$try_catch_break_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  try {
    throw_fatal("bad break");
    echo("asd\n");
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
    } else {
      throw;
    }
  }
  echo("exitted safely\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
