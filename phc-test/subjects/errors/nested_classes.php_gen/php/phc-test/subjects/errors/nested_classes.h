
#ifndef __GENERATED_php_phc_test_subjects_errors_nested_classes_h__
#define __GENERATED_php_phc_test_subjects_errors_nested_classes_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/nested_classes.fw.h>

// Declarations
#include <cls/x.h>
#include <cls/y.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$errors$nested_classes_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_x(CArrRef params, bool init = true);
Object co_y(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_nested_classes_h__
