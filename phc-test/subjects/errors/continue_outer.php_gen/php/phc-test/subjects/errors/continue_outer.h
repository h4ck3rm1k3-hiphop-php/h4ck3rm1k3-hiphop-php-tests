
#ifndef __GENERATED_php_phc_test_subjects_errors_continue_outer_h__
#define __GENERATED_php_phc_test_subjects_errors_continue_outer_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/errors/continue_outer.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$errors$continue_outer_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_errors_continue_outer_h__
