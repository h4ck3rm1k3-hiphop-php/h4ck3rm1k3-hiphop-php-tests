
#include <php/phc-test/subjects/horrible/invocation_priorities.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/horrible/invocation_priorities.php line 2 */
Variant c_x::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("f", m_f));
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x6BB4A0689FBAD42ELL, f, 1);
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x6BB4A0689FBAD42ELL, m_f,
                         f, 1);
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x6BB4A0689FBAD42ELL, m_f,
                      f, 1);
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::create() {
  init();
  t_x();
  return this;
}
ObjectData *c_x::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  clone->m_f = m_f;
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
  m_f = null;
  m_a = null;
}
/* SRC: phc-test/subjects/horrible/invocation_priorities.php line 7 */
void c_x::t_x() {
  INSTANCE_METHOD_INJECTION(X, X::X);
  bool oldInCtor = gasInCtor(true);
  (m_f = "foo");
  (m_a = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/horrible/invocation_priorities.php line 13 */
void c_x::t_foo() {
  INSTANCE_METHOD_INJECTION(X, X::foo);
  echo("X::foo\n");
} /* function */
/* SRC: phc-test/subjects/horrible/invocation_priorities.php line 19 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  echo("foo\n");
} /* function */
Variant i_foo(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6176C0B993BF5914LL, foo) {
    return (f_foo(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$horrible$invocation_priorities_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/horrible/invocation_priorities.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$horrible$invocation_priorities_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_f = "foo");
  (v_a = ScalarArrays::sa_[0]);
  (v_x = ((Object)(LINE(27,p_x(p_x(NEWOBJ(c_x)())->create())))));
  LINE(29,f_foo());
  LINE(30,v_x.o_invoke_few_args("foo", 0x6176C0B993BF5914LL, 0));
  LINE(31,v_x.o_invoke_few_args(toString(v_f), -1LL, 0));
  LINE(32,v_x.o_invoke_few_args("foo", -1LL, 0));
  LINE(33,invoke((toString(v_x.o_get("a", 0x4292CEE227B9150ALL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), Array(), -1));
  LINE(34,v_x.o_invoke_few_args(toString(v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), -1LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
