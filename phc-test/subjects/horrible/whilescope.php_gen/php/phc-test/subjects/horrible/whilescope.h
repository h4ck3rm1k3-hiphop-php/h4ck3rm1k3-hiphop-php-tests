
#ifndef __GENERATED_php_phc_test_subjects_horrible_whilescope_h__
#define __GENERATED_php_phc_test_subjects_horrible_whilescope_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/horrible/whilescope.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_whiletest();
Variant pm_php$phc_test$subjects$horrible$whilescope_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_horrible_whilescope_h__
