
#include <php/phc-test/subjects/horrible/whilescope.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/horrible/whilescope.php line 5 */
void f_whiletest() {
  FUNCTION_INJECTION(whiletest);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant v_x;
  int64 v_i = 0;

  (v_x = 200LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 5LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(13,concat3("x: ", toString(v_x), "\n")));
        v_x = ref(g->GV(x));
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$horrible$whilescope_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/horrible/whilescope.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$horrible$whilescope_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 100LL);
  LINE(20,f_whiletest());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
