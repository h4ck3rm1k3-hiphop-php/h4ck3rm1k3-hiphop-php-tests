
#ifndef __GENERATED_php_phc_test_subjects_horrible_ifscope_h__
#define __GENERATED_php_phc_test_subjects_horrible_ifscope_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/horrible/ifscope.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_testif(bool v_c);
Variant pm_php$phc_test$subjects$horrible$ifscope_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_horrible_ifscope_h__
