
#include <php/phc-test/subjects/horrible/ifscope.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/horrible/ifscope.php line 2 */
void f_testif(bool v_c) {
  FUNCTION_INJECTION(testif);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant v_x;

  (v_x = 200LL);
  echo(LINE(6,concat3("x: ", toString(v_x), "\n")));
  if (v_c) {
    v_x = ref(g->GV(x));
    (v_x = 300LL);
  }
  else {
    (v_x = 400LL);
  }
  echo(LINE(17,concat3("x: ", toString(v_x), "\n")));
  (v_x = 500LL);
} /* function */
Variant pm_php$phc_test$subjects$horrible$ifscope_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/horrible/ifscope.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$horrible$ifscope_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 100LL);
  echo(LINE(24,concat3("x: ", toString(v_x), "\n")));
  LINE(25,f_testif(true));
  echo(LINE(26,concat3("x: ", toString(v_x), "\n")));
  (v_x = 600LL);
  echo(LINE(29,concat3("x: ", toString(v_x), "\n")));
  LINE(30,f_testif(false));
  echo(LINE(31,concat3("x: ", toString(v_x), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
