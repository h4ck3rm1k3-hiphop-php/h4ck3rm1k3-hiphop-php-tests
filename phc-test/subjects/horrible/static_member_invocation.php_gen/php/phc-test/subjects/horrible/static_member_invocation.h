
#ifndef __GENERATED_php_phc_test_subjects_horrible_static_member_invocation_h__
#define __GENERATED_php_phc_test_subjects_horrible_static_member_invocation_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/horrible/static_member_invocation.fw.h>

// Declarations
#include <cls/x.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$horrible$static_member_invocation_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_x(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_horrible_static_member_invocation_h__
