
#ifndef __GENERATED_php_phc_test_subjects_horrible_method_invocation_h__
#define __GENERATED_php_phc_test_subjects_horrible_method_invocation_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/horrible/method_invocation.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$horrible$method_invocation_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bar();
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_horrible_method_invocation_h__
