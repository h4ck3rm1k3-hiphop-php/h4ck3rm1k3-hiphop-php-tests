
#include <php/phc-test/subjects/horrible/method_invocation.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/horrible/method_invocation.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::create() {
  init();
  t_foo();
  return this;
}
ObjectData *c_foo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x7F64BC1ECEF60CCCLL, y) {
        return (t_y(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x7F64BC1ECEF60CCCLL, y) {
        return (t_y(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_x = null;
}
/* SRC: phc-test/subjects/horrible/method_invocation.php line 8 */
void c_foo::t_foo() {
  INSTANCE_METHOD_INJECTION(Foo, Foo::Foo);
  bool oldInCtor = gasInCtor(true);
  (m_x = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/horrible/method_invocation.php line 13 */
void c_foo::t_bar() {
  INSTANCE_METHOD_INJECTION(Foo, Foo::bar);
  echo("foo::bar\n");
} /* function */
/* SRC: phc-test/subjects/horrible/method_invocation.php line 18 */
void c_foo::t_y() {
  INSTANCE_METHOD_INJECTION(Foo, Foo::y);
  echo("y\n");
} /* function */
/* SRC: phc-test/subjects/horrible/method_invocation.php line 25 */
void f_bar() {
  FUNCTION_INJECTION(bar);
  echo("bar\n");
} /* function */
Variant i_bar(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
    return (f_bar(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$horrible$method_invocation_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/horrible/method_invocation.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$horrible$method_invocation_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = ((Object)(LINE(30,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(32,invoke((toString(v_a.o_get("x", 0x04BFC205E59FA416LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), Array(), -1));
  LINE(33,v_a.o_invoke_few_args("y", 0x7F64BC1ECEF60CCCLL, 0));
  LINE(35,v_a.o_invoke_few_args(toString(v_a.o_get("x", 0x04BFC205E59FA416LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), -1LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
