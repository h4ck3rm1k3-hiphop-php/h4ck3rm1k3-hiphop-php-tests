
#include <php/phc-test/subjects/horrible/obfuscated_foreach.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$horrible$obfuscated_foreach_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/horrible/obfuscated_foreach.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$horrible$obfuscated_foreach_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_xx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xx") : g->GV(xx);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = ScalarArrays::sa_[0]);
  echo("** 1 **\n");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_x.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_y = iter3->second();
      {
        echo(toString(v_y) + toString("\n"));
      }
    }
  }
  echo("** 2 **\n");
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_x.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_y.lvalAt(0LL, 0x77CFA1EEF01BCA90LL) = iter6->second();
      {
        echo(toString(v_y) + toString("\n"));
      }
    }
  }
  echo("** 3 **\n");
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_x.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_y.lvalAt(0LL, 0x77CFA1EEF01BCA90LL) = iter9->second();
      {
        echo(toString(v_y) + toString("\n"));
      }
    }
  }
  echo("** 4 **\n");
  (v_a = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_x.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_a.o_lval("b", 0x08FBB133F8576BD5LL) = iter12->second();
      {
        echo(LINE(26,concat5("(", toString(v_a), ") ", toString(v_a.o_get("b", 0x08FBB133F8576BD5LL)), "\n")));
      }
    }
  }
  (v_xx = "x");
  echo("** 6 **\n");
  {
    LOOP_COUNTER(13);
    Variant map14 = variables->get(toString(v_xx));
    for (ArrayIterPtr iter15 = map14.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_y = iter15->second();
      {
        echo(toString(v_y) + toString("\n"));
      }
    }
  }
  echo("** 7 **\n");
  {
    LOOP_COUNTER(16);
    Variant map17 = ref(variables->get(toString(v_xx)));
    map17.escalate();
    for (MutableArrayIterPtr iter18 = map17.begin(NULL, v_y); iter18->advance();) {
      LOOP_COUNTER_CHECK(16);
      {
        echo(toString(v_y) + toString("\n"));
      }
    }
  }
  unset(v_y);
  echo("** 8 **\n");
  {
    LOOP_COUNTER(19);
    Variant map20 = variables->get(toString(v_xx));
    for (ArrayIterPtr iter21 = map20.begin(); !iter21->end(); iter21->next()) {
      LOOP_COUNTER_CHECK(19);
      v_z = iter21->second();
      v_y = iter21->first();
      {
        echo(LINE(58,concat4(toString(v_y), " => ", toString(v_z), "\n")));
      }
    }
  }
  echo("** 9 **\n");
  {
    LOOP_COUNTER(22);
    Variant map23 = ref(variables->get(toString(v_xx)));
    map23.escalate();
    for (MutableArrayIterPtr iter24 = map23.begin(&v_y, v_z); iter24->advance();) {
      LOOP_COUNTER_CHECK(22);
      {
        echo(LINE(64,concat4(toString(v_y), " => ", toString(v_z), "\n")));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
