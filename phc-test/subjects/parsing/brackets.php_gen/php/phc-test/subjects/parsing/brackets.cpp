
#include <php/phc-test/subjects/parsing/brackets.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/brackets.php line 4 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_y;
  Variant v_x;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_y; Variant &v_x;
    VariableTable(Variant &r_y, Variant &r_x) : v_y(r_y), v_x(r_x) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x4F56B733A4DFC78ALL, v_y,
                      y);
          HASH_RETURN(0x04BFC205E59FA416LL, v_x,
                      x);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_y, v_x);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  echo("hi");
  echo("hi");
  {
    echo("hi");
    echo("foo");
  }
  print("bar");
  print("bar");
  LINE(15,include("foo", false, variables, "phc-test/subjects/parsing/"));
  LINE(16,include("foo", true, variables, "phc-test/subjects/parsing/"));
  LINE(17,require("foo", false, variables, "phc-test/subjects/parsing/"));
  LINE(18,require("foo", true, variables, "phc-test/subjects/parsing/"));
  LINE(21,include("foo", false, variables, "phc-test/subjects/parsing/"));
  LINE(22,include("foo", true, variables, "phc-test/subjects/parsing/"));
  LINE(23,require("foo", false, variables, "phc-test/subjects/parsing/"));
  LINE(24,require("foo", true, variables, "phc-test/subjects/parsing/"));
  (v_x = f_clone(toObject(v_y)));
  (v_x = f_clone(toObject((toObject(v_y)))));
  f_exit();
  f_exit();
  f_exit(0LL);
  (v_x = LINE(34,create_object("c", Array())));
  (v_x = LINE(35,create_object("c", Array())));
  (v_x = LINE(36,create_object("c", Array(ArrayInit(1).set(0, 0LL).create()))));
} /* function */
Variant pm_php$phc_test$subjects$parsing$brackets_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/brackets.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$brackets_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
