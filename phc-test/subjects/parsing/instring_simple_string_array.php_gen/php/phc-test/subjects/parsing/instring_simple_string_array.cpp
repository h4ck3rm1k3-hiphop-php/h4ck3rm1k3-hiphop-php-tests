
#include <php/phc-test/subjects/parsing/instring_simple_string_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$instring_simple_string_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/instring_simple_string_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$instring_simple_string_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  v_b.set("foo", (1LL), 0x4154FA2EF733DA8FLL);
  echo(LINE(3,concat3("a ", toString(v_b.rvalAt("foo", 0x4154FA2EF733DA8FLL)), " c\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
