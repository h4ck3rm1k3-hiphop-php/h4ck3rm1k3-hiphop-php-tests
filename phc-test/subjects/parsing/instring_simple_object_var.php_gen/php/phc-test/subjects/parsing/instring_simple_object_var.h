
#ifndef __GENERATED_php_phc_test_subjects_parsing_instring_simple_object_var_h__
#define __GENERATED_php_phc_test_subjects_parsing_instring_simple_object_var_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/instring_simple_object_var.fw.h>

// Declarations
#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$instring_simple_object_var_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_c(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_instring_simple_object_var_h__
