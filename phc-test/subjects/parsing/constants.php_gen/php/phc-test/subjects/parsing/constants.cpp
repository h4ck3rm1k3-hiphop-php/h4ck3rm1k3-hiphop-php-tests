
#include <php/phc-test/subjects/parsing/constants.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_Foo = "Foo";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/constants.php line 4 */
const int64 q_foo_BAR = 7LL;
const int64 q_foo_Bar = 8LL;
Variant c_foo::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x7DC755B00FC9EF46LL, g->s_foo_DupIdx4,
                  x4);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x1", m_x1));
  props.push_back(NEW(ArrayElement)("x2", m_x2));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x5685725E6F24EAFDLL, x1, 2);
      HASH_EXISTS_STRING(0x5075985E0413CB39LL, x2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x5685725E6F24EAFDLL, m_x1,
                         x1, 2);
      HASH_RETURN_STRING(0x5075985E0413CB39LL, m_x2,
                         x2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x5685725E6F24EAFDLL, m_x1,
                      x1, 2);
      HASH_SET_STRING(0x5075985E0413CB39LL, m_x2,
                      x2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  DECLARE_GLOBAL_VARIABLES(g);
  lazy_initializer(g);
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x061DDFC9A9DE8840LL, g->q_foo_X3, X3);
      break;
    case 3:
      HASH_RETURN(0x5B8217644DFB13A3LL, q_foo_Bar, Bar);
      break;
    case 5:
      HASH_RETURN(0x5E008C5BA4C3B3FDLL, q_foo_BAR, BAR);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_x1 = m_x1;
  clone->m_x2 = m_x2;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_x1 = 8LL /* foo::Bar */;
  m_x2 = ScalarArrays::sa_[0];
}
void c_foo::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_foo_DupIdx4 = 8LL /* foo::Bar */;
}
void csi_foo() {
  c_foo::os_static_initializer();
}
GlobalVariables *c_foo::lazy_initializer(GlobalVariables *g) {
  if (!g->csf_foo) {
    g->csf_foo = true;
    g->q_foo_X3 = 8LL /* foo::Bar */;
  }
  return g;
}
/* SRC: phc-test/subjects/parsing/constants.php line 15 */
void f_x(CVarRef v_x //  = throw_fatal("unknown class constant foo::bar")
, CVarRef v_y //  = Array(ArrayInit(2).set(0, "s").set(1, k_Foo).create())
) {
  FUNCTION_INJECTION(x);
  LINE(17,x_var_dump(1, v_x));
  LINE(18,x_var_dump(1, v_y));
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$constants_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/constants.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$constants_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(21,f_x());
  LINE(22,f_x(((Object)(p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(23,(assignCallTemp(eo_0, ((Object)(p_foo(p_foo(NEWOBJ(c_foo)())->create())))),assignCallTemp(eo_1, ((Object)(p_foo(p_foo(NEWOBJ(c_foo)())->create())))),f_x(eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
