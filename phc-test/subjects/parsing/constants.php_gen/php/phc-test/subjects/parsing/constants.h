
#ifndef __GENERATED_php_phc_test_subjects_parsing_constants_h__
#define __GENERATED_php_phc_test_subjects_parsing_constants_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/constants.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$constants_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_x(CVarRef v_x = throw_fatal("unknown class constant foo::bar"), CVarRef v_y = Array(ArrayInit(2).set(0, "s").set(1, k_Foo).create()));
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_constants_h__
