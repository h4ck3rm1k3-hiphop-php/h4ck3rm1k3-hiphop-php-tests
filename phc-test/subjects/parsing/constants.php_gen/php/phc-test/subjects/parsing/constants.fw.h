
#ifndef __GENERATED_php_phc_test_subjects_parsing_constants_fw_h__
#define __GENERATED_php_phc_test_subjects_parsing_constants_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_Foo;


// 2. Classes
FORWARD_DECLARE_CLASS(foo)
extern const int64 q_foo_BAR;
extern const int64 q_foo_Bar;

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_constants_fw_h__
