
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_classes_and_functions_h__
#define __GENERATED_php_phc_test_subjects_parsing_included_classes_and_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_classes_and_functions.fw.h>

// Declarations
#include <cls/b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_b();
Variant pm_php$phc_test$subjects$parsing$included_classes_and_functions_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_b(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_classes_and_functions_h__
