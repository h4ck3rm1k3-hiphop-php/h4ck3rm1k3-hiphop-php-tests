
#include <php/phc-test/subjects/parsing/comments2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/comments2.php line 23 */
void f_f() {
  FUNCTION_INJECTION(f);
} /* function */
Variant pm_php$phc_test$subjects$parsing$comments2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/comments2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$comments2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cond __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cond") : g->GV(cond);

  echo("hello world");
  if (toBoolean(v_cond)) {
    echo("foo");
  }
  if (toBoolean(v_cond)) {
  }
  echo("booh");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
