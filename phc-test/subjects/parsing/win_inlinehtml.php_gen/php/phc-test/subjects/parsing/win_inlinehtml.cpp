
#include <php/phc-test/subjects/parsing/win_inlinehtml.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$win_inlinehtml_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/win_inlinehtml.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$win_inlinehtml_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("1\r\n");
  if (toBoolean(1LL)) {
    echo("Some longer string\r\n");
  }
  else {
    echo("3\r\n");
  }
  echo("4\r");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
