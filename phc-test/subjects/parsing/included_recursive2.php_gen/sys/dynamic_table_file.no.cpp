
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$parsing$included_recursive2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_recursive3_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_INCLUDE(0x00908F2B684E9F31LL, "phc-test/subjects/parsing/included_recursive2.php", php$phc_test$subjects$parsing$included_recursive2_php);
      HASH_INCLUDE(0x5ADC6EB193B2E85DLL, "phc-test/subjects/parsing/included_recursive3.php", php$phc_test$subjects$parsing$included_recursive3_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
