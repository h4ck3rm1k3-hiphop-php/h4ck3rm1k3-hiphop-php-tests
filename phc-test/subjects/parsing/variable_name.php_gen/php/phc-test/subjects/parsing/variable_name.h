
#ifndef __GENERATED_php_phc_test_subjects_parsing_variable_name_h__
#define __GENERATED_php_phc_test_subjects_parsing_variable_name_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/variable_name.fw.h>

// Declarations
#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$variable_name_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_f1();
String f_f2();
void f_f3();
Object co_c(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_variable_name_h__
