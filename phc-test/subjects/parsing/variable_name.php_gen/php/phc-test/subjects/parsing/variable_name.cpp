
#include <php/phc-test/subjects/parsing/variable_name.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/variable_name.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("d", m_d.isReferenced() ? ref(m_d) : m_d));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_d = m_d.isReferenced() ? ref(m_d) : m_d;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_d = null;
}
/* SRC: phc-test/subjects/parsing/variable_name.php line 10 */
Variant f_f1() {
  FUNCTION_INJECTION(f1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  return ref(gv_x);
} /* function */
/* SRC: phc-test/subjects/parsing/variable_name.php line 16 */
String f_f2() {
  FUNCTION_INJECTION(f2);
  return "x";
} /* function */
/* SRC: phc-test/subjects/parsing/variable_name.php line 21 */
void f_f3() {
  FUNCTION_INJECTION(f3);
  DECLARE_GLOBAL_VARIABLES(g);

  class VariableTable : public RVariableTable {
  public:
    ;
    VariableTable() {}
    virtual Variant getImpl(const char *s) {
      return rvalAt(s);
    }
  } variableTable;
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  throw_fatal("dynamic global");
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$variable_name_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/variable_name.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$variable_name_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = 0LL);
  (v_c = ((Object)(LINE(8,p_c(p_c(NEWOBJ(c_c)())->create())))));
  unset(variables->get(LINE(37,f_f2())));
  toObject(v_c)->t___unset("d");
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_arr.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      variables->get(LINE(47,f_f2())) = iter3->second();
      v_y = iter3->first();
      {
        echo(toString(v_x) + toString("\n"));
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_arr.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_c.o_lval("d", 0x7A452383AA9BF7C4LL) = iter6->second();
      v_y = iter6->first();
      {
        echo(toString(v_c.o_get("d", 0x7A452383AA9BF7C4LL)) + toString("\n"));
      }
    }
  }
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_arr.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_y = iter9->second();
      variables->get(LINE(57,f_f2())) = iter9->first();
      {
        echo(toString(v_x) + toString("\n"));
      }
    }
  }
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_arr.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_y = iter12->second();
      v_c.o_lval("d", 0x7A452383AA9BF7C4LL) = iter12->first();
      {
        echo(toString(v_c.o_get("d", 0x7A452383AA9BF7C4LL)) + toString("\n"));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
