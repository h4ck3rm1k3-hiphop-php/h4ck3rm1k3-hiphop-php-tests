
#include <php/phc-test/subjects/parsing/reflect_ints.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$reflect_ints_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/reflect_ints.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$reflect_ints_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_asd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("asd") : g->GV(asd);
  Variant &v_5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("5") : g->GV(5);
  Variant &v_5.7 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("5.7") : g->GV(5.7);

  echo("\n\nasd:\n");
  echo("\n\n5:\n");
  echo("\n\n-5:\n");
  LINE(11,x_var_dump(1, v_asd));
  LINE(12,x_var_dump(1, v_5));
  LINE(13,x_var_dump(1, variables->get(toString(-5LL))));
  LINE(14,x_var_dump(1, variables->get(toString(true))));
  LINE(15,x_var_dump(1, variables->get(toString(false))));
  LINE(16,x_var_dump(1, v_5.7));
  LINE(17,x_var_dump(1, variables->get(toString(null))));
  (v_asd = 6LL);
  (v_5 = 1LL);
  (variables->get(toString(-5LL)) = 1LL);
  (variables->get(toString(true)) = 2LL);
  (variables->get(toString(false)) = 3LL);
  (v_5.7 = 4LL);
  (variables->get(toString(null)) = 5LL);
  LINE(27,x_var_dump(1, v_asd));
  LINE(28,x_var_dump(1, v_5));
  LINE(29,x_var_dump(1, variables->get(toString(-5LL))));
  LINE(30,x_var_dump(1, variables->get(toString(true))));
  LINE(31,x_var_dump(1, variables->get(toString(false))));
  LINE(32,x_var_dump(1, v_5.7));
  LINE(33,x_var_dump(1, variables->get(toString(null))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
