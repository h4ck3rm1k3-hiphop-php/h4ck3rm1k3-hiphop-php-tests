
#ifndef __GENERATED_cls_cl1_h__
#define __GENERATED_cls_cl1_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/included_cc1.php line 3 */
class c_cl1 : virtual public ObjectData {
  BEGIN_CLASS_MAP(cl1)
  END_CLASS_MAP(cl1)
  DECLARE_CLASS(cl1, cl1, ObjectData)
  void init();
  public: void t_cl1();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cl1_h__
