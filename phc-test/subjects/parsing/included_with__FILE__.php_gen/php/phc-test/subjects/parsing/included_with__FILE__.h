
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_with__FILE___h__
#define __GENERATED_php_phc_test_subjects_parsing_included_with__FILE___h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_with__FILE__.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$included_with__FILE___php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_with__FILE___h__
