
#include <php/phc-test/subjects/parsing/do.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$do_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/do.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$do_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = 100LL);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_x = v_x - 1LL);
        v_y++;
      }
    } while (more(v_x, 0LL));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
