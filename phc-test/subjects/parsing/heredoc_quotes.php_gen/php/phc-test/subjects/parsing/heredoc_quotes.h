
#ifndef __GENERATED_php_phc_test_subjects_parsing_heredoc_quotes_h__
#define __GENERATED_php_phc_test_subjects_parsing_heredoc_quotes_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/heredoc_quotes.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$heredoc_quotes_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_heredoc_quotes_h__
