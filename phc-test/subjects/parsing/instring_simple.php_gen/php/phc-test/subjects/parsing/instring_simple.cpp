
#include <php/phc-test/subjects/parsing/instring_simple.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$instring_simple_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/instring_simple.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$instring_simple_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = (v_b = (v_c = 1LL)));
  echo(LINE(3,concat3("foo ", toString(v_a), " bar\n")));
  echo(LINE(4,concat3("foo ", toString(v_a), " bar\n")));
  echo(LINE(5,(assignCallTemp(eo_1, toString(v_a)),assignCallTemp(eo_3, concat3("3 ", toString(v_b), " 4\n")),concat4("1 ", eo_1, " 2", eo_3))));
  echo(LINE(6,(assignCallTemp(eo_1, toString(v_a)),assignCallTemp(eo_3, toString(v_b)),assignCallTemp(eo_4, concat3("3 ", toString(v_c), " 4\n")),concat5("1 ", eo_1, " 2", eo_3, eo_4))));
  echo(LINE(7,(assignCallTemp(eo_1, toString(v_a)),assignCallTemp(eo_3, toString(v_b)),assignCallTemp(eo_4, toString(v_c)),assignCallTemp(eo_5, concat3("3 ", toString(v_c), " 4\n")),concat6("1 ", eo_1, " 2", eo_3, eo_4, eo_5))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
