
#ifndef __GENERATED_php_phc_test_subjects_parsing_for_cond_with_comma_h__
#define __GENERATED_php_phc_test_subjects_parsing_for_cond_with_comma_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/for_cond_with_comma.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_f(CStrRef v_id, bool v_x, bool v_y);
Variant pm_php$phc_test$subjects$parsing$for_cond_with_comma_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_for_cond_with_comma_h__
