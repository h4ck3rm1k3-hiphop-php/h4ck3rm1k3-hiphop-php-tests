
#include <php/phc-test/subjects/parsing/for_cond_with_comma.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/for_cond_with_comma.php line 2 */
void f_f(CStrRef v_id, bool v_x, bool v_y) {
  FUNCTION_INJECTION(f);
  {
    LOOP_COUNTER(1);
    for (; v_x, v_y; ) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(v_id + toString("\n"));
        break;
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$parsing$for_cond_with_comma_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/for_cond_with_comma.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$for_cond_with_comma_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(11,f_f("a", false, false));
  LINE(12,f_f("b", false, true));
  LINE(13,f_f("c", true, false));
  LINE(14,f_f("d", true, true));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
