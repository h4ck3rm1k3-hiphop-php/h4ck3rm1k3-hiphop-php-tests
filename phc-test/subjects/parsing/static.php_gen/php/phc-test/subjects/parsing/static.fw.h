
#ifndef __GENERATED_php_phc_test_subjects_parsing_static_fw_h__
#define __GENERATED_php_phc_test_subjects_parsing_static_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_FOOBAR;
extern const Variant k_FOO;
extern const StaticString k_BAR;


// 2. Classes
FORWARD_DECLARE_CLASS(foo)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_static_fw_h__
