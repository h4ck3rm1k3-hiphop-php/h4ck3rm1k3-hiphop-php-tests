
#include <php/phc-test/subjects/parsing/static.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_FOOBAR = "FOOBAR";
const Variant k_FOO = "FOO";
const StaticString k_BAR = "BAR";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/static.php line 2 */
Variant c_foo::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 31) {
    case 6:
      HASH_RETURN(0x0C058190047906E6LL, g->s_foo_DupIdx3,
                  x3);
      HASH_RETURN(0x7DC755B00FC9EF46LL, g->s_foo_DupIdx4,
                  x4);
      break;
    case 10:
      HASH_RETURN(0x4F56B733A4DFC78ALL, g->s_foo_DupIdy,
                  y);
      break;
    case 13:
      HASH_RETURN(0x18999E7F825574CDLL, g->s_foo_DupIdx7,
                  x7);
      break;
    case 14:
      HASH_RETURN(0x74778035E7D1C40ELL, g->s_foo_DupIdx10,
                  x10);
      break;
    case 16:
      HASH_RETURN(0x09B9AFEE14F19D50LL, g->s_foo_DupIdx6,
                  x6);
      HASH_RETURN(0x2EA22F63F68FABB0LL, g->s_foo_DupIdx9,
                  x9);
      break;
    case 24:
      HASH_RETURN(0x7A702ABECDB89E18LL, g->s_foo_DupIdx5,
                  x5);
      break;
    case 25:
      HASH_RETURN(0x5075985E0413CB39LL, g->s_foo_DupIdx2,
                  x2);
      break;
    case 29:
      HASH_RETURN(0x5685725E6F24EAFDLL, g->s_foo_DupIdx1,
                  x1);
      break;
    case 30:
      HASH_RETURN(0x3BB3A9560BBDB87ELL, g->s_foo_DupIdx8,
                  x8);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x7DC755B00FC9EF46LL, g->s_foo_DupIdx4,
                  x4);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
void c_foo::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_foo_DupIdx1 = null;
  g->s_foo_DupIdx2 = 10LL;
  g->s_foo_DupIdx5 = null;
  g->s_foo_DupIdy = null;
  g->s_foo_DupIdx6 = ScalarArrays::sa_[0];
  g->s_foo_DupIdx7 = ScalarArrays::sa_[1];
  g->s_foo_DupIdx9 = 5LL;
  g->s_foo_DupIdx10 = -5LL;
}
void csi_foo() {
  c_foo::os_static_initializer();
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$static_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/static.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$static_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_y = g->s_foo_DupIdx1);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
