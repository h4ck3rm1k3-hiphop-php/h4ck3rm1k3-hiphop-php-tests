
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_FOOBAR;
extern const Variant k_FOO;
extern const StaticString k_BAR;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x2ACE2D47D81F2549LL, k_FOOBAR, FOOBAR);
      break;
    case 4:
      HASH_RETURN(0x6176C0B993BF5914LL, k_FOO, FOO);
      break;
    case 5:
      HASH_RETURN(0x5E008C5BA4C3B3FDLL, k_BAR, BAR);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
