
#include <php/phc-test/subjects/parsing/break.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$break_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/break.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$break_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  {
    LOOP_COUNTER(1);
    for (; ; ) {
      LOOP_COUNTER_CHECK(1);
      {
        break;
        (v_x = 5LL);
        if ((toInt64(v_x))<2) {
        goto break1;
        } else {
        throw_fatal("bad break");
        }
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
