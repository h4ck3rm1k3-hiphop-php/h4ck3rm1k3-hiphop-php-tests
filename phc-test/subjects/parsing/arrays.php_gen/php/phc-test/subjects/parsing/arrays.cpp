
#include <php/phc-test/subjects/parsing/arrays.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$arrays_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/arrays.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$arrays_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_a3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a3") : g->GV(a3);
  Variant &v_a4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a4") : g->GV(a4);
  Variant &v_a5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a5") : g->GV(a5);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_a6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a6") : g->GV(a6);
  Variant &v_a7 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a7") : g->GV(a7);
  Variant &v_a8 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a8") : g->GV(a8);

  ;
  ;
  ;
  (v_a1 = ScalarArrays::sa_[0]);
  (v_a2 = Array(ArrayInit(1).setRef(0, ref(v_x)).create()));
  (v_a3 = ScalarArrays::sa_[1]);
  (v_a4 = Array(ArrayInit(1).setRef(0, 1LL, ref(v_x), 0x5BCA7C69B794F8CELL).create()));
  (v_a5 = ScalarArrays::sa_[2]);
  (v_a6 = Array(ArrayInit(3).setRef(0, ref(v_x)).setRef(1, ref(v_y)).setRef(2, ref(v_y)).create()));
  (v_a7 = ScalarArrays::sa_[3]);
  (v_a8 = Array(ArrayInit(3).setRef(0, 1LL, ref(v_x), 0x5BCA7C69B794F8CELL).setRef(1, 2LL, ref(v_x), 0x486AFCC090D5F98CLL).setRef(2, 3LL, ref(v_x), 0x135FDDF6A6BFBBDDLL).create()));
  LINE(21,x_var_dump(1, v_a1));
  LINE(22,x_var_dump(1, v_a2));
  LINE(23,x_var_dump(1, v_a3));
  LINE(24,x_var_dump(1, v_a4));
  LINE(25,x_var_dump(1, v_a5));
  LINE(26,x_var_dump(1, v_a6));
  LINE(27,x_var_dump(1, v_a7));
  LINE(28,x_var_dump(1, v_a8));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
