
#include <php/phc-test/subjects/parsing/foreach_with_complex_key.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$foreach_with_complex_key_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/foreach_with_complex_key.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$foreach_with_complex_key_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);

  (v_arr = ScalarArrays::sa_[0]);
  print("1\n\n\n");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_arr.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_val = iter3->second();
      v_key.lvalAt(0LL, 0x77CFA1EEF01BCA90LL) = iter3->first();
      {
        LINE(8,x_var_dump(1, v_arr));
        LINE(9,x_var_dump(1, v_key));
        LINE(10,x_var_dump(1, v_val));
      }
    }
  }
  print("2\n\n\n");
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_arr.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_val = iter6->second();
      v_key.lvalAt(0LL, 0x77CFA1EEF01BCA90LL) = iter6->first();
      {
        LINE(16,x_var_dump(1, v_arr));
        LINE(17,x_var_dump(1, v_key));
        LINE(18,x_var_dump(1, v_val));
      }
    }
  }
  print("3\n\n\n");
  (v_val = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(7);
    Variant map8 = ref(v_arr);
    map8.escalate();
    for (MutableArrayIterPtr iter9 = map8.begin(&v_key.lvalAt(0LL, 0x77CFA1EEF01BCA90LL), lval(v_val.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))); iter9->advance();) {
      LOOP_COUNTER_CHECK(7);
      {
        LINE(26,x_var_dump(1, v_arr));
        LINE(27,x_var_dump(1, v_key));
        LINE(28,x_var_dump(1, v_val));
      }
    }
  }
  print("4\n\n\n");
  (v_val = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(10);
    Variant map11 = ref(v_arr);
    map11.escalate();
    for (MutableArrayIterPtr iter12 = map11.begin(&v_key.lvalAt(0LL, 0x77CFA1EEF01BCA90LL), lval(v_val.lvalAt(1LL, 0x5BCA7C69B794F8CELL))); iter12->advance();) {
      LOOP_COUNTER_CHECK(10);
      {
        LINE(35,x_var_dump(1, v_arr));
        LINE(36,x_var_dump(1, v_key));
        LINE(37,x_var_dump(1, v_val));
      }
    }
  }
  print("5\n\n\n");
  (v_val = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(13);
    Variant map14 = ref(v_arr);
    map14.escalate();
    for (MutableArrayIterPtr iter15 = map14.begin(&v_key.lvalAt(0LL, 0x77CFA1EEF01BCA90LL), lval(v_key.lvalAt(1LL, 0x5BCA7C69B794F8CELL))); iter15->advance();) {
      LOOP_COUNTER_CHECK(13);
      {
        LINE(45,x_var_dump(1, v_arr));
        LINE(46,x_var_dump(1, v_key));
        LINE(47,x_var_dump(1, v_val));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
