
#include <php/phc-test/subjects/parsing/listassignment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_6(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_7(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3, Variant &p4) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
Variant a1 = a0.rvalAt(0);
  p0 = a1.rvalAt(0);
  p1 = a1.rvalAt(1);
  Variant a2 = a0.rvalAt(1);
  p2 = a2.rvalAt(0);
  p3 = a2.rvalAt(1);
  p4 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3, Variant &p4) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
Variant a1 = a0.rvalAt(0);
  p0 = a1.rvalAt(0);
  p1 = a1.rvalAt(2);
  Variant a2 = a0.rvalAt(1);
  p2 = a2.rvalAt(0);
  p3 = a2.rvalAt(2);
  p4 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_10(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_11(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_12(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$listassignment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/listassignment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$listassignment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_info __attribute__((__unused__)) = (variables != gVariables) ? variables->get("info") : g->GV(info);

  df_lambda_1(ScalarArrays::sa_[0], v_a, v_b);
  LINE(7,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("-------------0----------------\n");
  df_lambda_2(ScalarArrays::sa_[1], v_a, v_b);
  LINE(12,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("-------------0B----------------\n");
  LINE(17,x_var_dump(1, df_lambda_3(ScalarArrays::sa_[2], v_a, v_b)));
  LINE(18,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("-------------1----------------\n");
  LINE(22,x_var_dump(1, df_lambda_4(Array(ArrayInit(2).set(0, v_b).set(1, v_a).create()), v_a, v_b)));
  LINE(23,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("-------------2----------------\n");
  LINE(27,(assignCallTemp(eo_0, df_lambda_5(21LL, v_a, v_b)),x_var_dump(3, eo_0, ScalarArrays::sa_[3])));
  LINE(28,x_var_dump(2, v_a, Array(ArrayInit(1).set(0, v_b).create())));
  echo("-------------3----------------\n");
  (v_arr = ScalarArrays::sa_[4]);
  df_lambda_6(v_arr, v_x, v_y, v_z);
  LINE(34,x_var_dump(3, v_x, Array(ArrayInit(2).set(0, v_y).set(1, v_z).create())));
  echo("-------------4----------------\n");
  df_lambda_7(v_arr, v_x, v_z);
  LINE(38,x_var_dump(3, v_x, Array(ArrayInit(2).set(0, v_y).set(1, v_z).create())));
  echo("-------------5----------------\n");
  df_lambda_8(v_arr, v_x, v_y, v_z, v_w, v_a);
  LINE(42,x_var_dump(5, v_a, Array(ArrayInit(4).set(0, v_w).set(1, v_x).set(2, v_y).set(3, v_z).create())));
  echo("-------------6----------------\n");
  df_lambda_9(v_arr, v_x, v_z, v_z, v_v, v_a);
  LINE(46,x_var_dump(6, v_a, Array(ArrayInit(5).set(0, v_v).set(1, v_w).set(2, v_x).set(3, v_y).set(4, v_z).create())));
  echo("-------------7----------------\n");
  (v_x = df_lambda_10(ScalarArrays::sa_[5], v_y, v_z));
  LINE(51,x_var_dump(3, v_x, Array(ArrayInit(2).set(0, v_y).set(1, v_z).create())));
  echo("-------------8----------------\n");
  (v_info = ScalarArrays::sa_[6]);
  df_lambda_11(v_info, lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)), lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)), lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)));
  LINE(57,x_var_dump(1, v_a));
  echo("-------------9----------------\n");
  df_lambda_12(v_info, lval(v_info.lvalAt(2LL, 0x486AFCC090D5F98CLL)), lval(v_info.lvalAt(1LL, 0x5BCA7C69B794F8CELL)), lval(v_info.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(65,x_var_dump(1, v_info));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
