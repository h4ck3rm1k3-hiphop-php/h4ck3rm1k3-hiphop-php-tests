
#include <php/phc-test/subjects/parsing/assignments.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$assignments_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/assignments.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$assignments_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_a = v_b);
  (v_a = ref(v_b));
  (v_x = 10LL);
  (v_y = 20LL);
  v_x += v_y;
  v_x -= v_y;
  v_x *= v_y;
  v_x /= v_y;
  v_x %= toInt64(v_y);
  concat_assign(v_x, toString(v_y));
  v_x &= v_y;
  v_x |= v_y;
  v_x ^= v_y;
  v_x <<= toInt64(v_y);
  v_x >>= toInt64(v_y);
  v_x++;
  ++v_x;
  v_x--;
  --v_x;
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
