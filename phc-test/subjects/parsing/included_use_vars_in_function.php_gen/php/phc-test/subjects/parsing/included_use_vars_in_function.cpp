
#include <php/phc-test/subjects/parsing/included_use_vars_in_function.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/included_use_vars_in_function.php line 3 */
int64 f_j(CVarRef v_j1, CVarRef v_j2) {
  FUNCTION_INJECTION(j);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_i1 __attribute__((__unused__)) = g->GV(i1);
  Variant &gv_i2 __attribute__((__unused__)) = g->GV(i2);
  echo(toString(gv_i1));
  echo(toString(gv_i2));
  echo(toString(v_j1));
  echo(toString(v_j2));
  if ((!equal(gv_i1, "some value")) && (!equal(gv_i2, "another value"))) {
    LINE(15,invoke_failed("fail", Array(ArrayInit(3).set(0, get_source_filename("phc-test/subjects/parsing/included_use_vars_in_function.php")).set(1, 15).set(2, "using variables in included function").create()), 0x00000000C2876EAALL));
  }
  return 6LL;
} /* function */
Variant pm_php$phc_test$subjects$parsing$included_use_vars_in_function_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/included_use_vars_in_function.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$included_use_vars_in_function_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
