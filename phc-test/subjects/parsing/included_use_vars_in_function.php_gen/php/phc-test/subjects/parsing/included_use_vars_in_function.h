
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_use_vars_in_function_h__
#define __GENERATED_php_phc_test_subjects_parsing_included_use_vars_in_function_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_use_vars_in_function.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$included_use_vars_in_function_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_j(CVarRef v_j1, CVarRef v_j2);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_use_vars_in_function_h__
