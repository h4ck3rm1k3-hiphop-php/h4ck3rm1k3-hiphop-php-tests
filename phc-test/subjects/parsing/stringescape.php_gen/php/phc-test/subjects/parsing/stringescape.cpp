
#include <php/phc-test/subjects/parsing/stringescape.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$stringescape_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/stringescape.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$stringescape_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("@\n");
  echo("@\n");
  echo(String("Some non-printable chars: \0 \001 \002 \177\n", 34, AttachLiteral));
  echo("a\nb");
  echo(toString("a\nb"));
  echo("a\"b");
  echo(toString("a\\\"b"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
