
#include <php/phc-test/subjects/parsing/negative_literals.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$negative_literals_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/negative_literals.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$negative_literals_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_var_dump(1, 2147483645LL));
  LINE(4,x_var_dump(1, 2147483646LL));
  LINE(5,x_var_dump(1, 2147483647LL));
  LINE(6,x_var_dump(1, 0.0));
  LINE(7,x_var_dump(1, 0.0));
  LINE(8,x_var_dump(1, 0.0));
  LINE(10,x_var_dump(1, 2147483645LL));
  LINE(11,x_var_dump(1, 2147483646LL));
  LINE(12,x_var_dump(1, 2147483647LL));
  LINE(13,x_var_dump(1, 2147483648.0));
  LINE(14,x_var_dump(1, 2147483649.0));
  LINE(15,x_var_dump(1, 2147483650.0));
  LINE(17,x_var_dump(1, 0.0));
  LINE(18,x_var_dump(1, 0.0));
  LINE(19,x_var_dump(1, 0.0));
  LINE(20,x_var_dump(1, 4294967296LL));
  LINE(21,x_var_dump(1, 4294967297LL));
  LINE(22,x_var_dump(1, 4294967298LL));
  LINE(24,x_var_dump(1, 4294967293.0));
  LINE(25,x_var_dump(1, 4294967294.0));
  LINE(26,x_var_dump(1, 4294967295.0));
  LINE(27,x_var_dump(1, 4294967296LL));
  LINE(28,x_var_dump(1, 4294967297LL));
  LINE(29,x_var_dump(1, 4294967298LL));
  LINE(31,x_var_dump(1, -2147483645LL));
  LINE(32,x_var_dump(1, -2147483646LL));
  LINE(33,x_var_dump(1, -2147483647LL));
  LINE(34,x_var_dump(1, 0.0));
  LINE(35,x_var_dump(1, 0.0));
  LINE(36,x_var_dump(1, 0.0));
  LINE(38,x_var_dump(1, -2147483645LL));
  LINE(39,x_var_dump(1, -2147483646LL));
  LINE(40,x_var_dump(1, -2147483647LL));
  LINE(41,x_var_dump(1, -2147483648.0));
  LINE(42,x_var_dump(1, -2147483649.0));
  LINE(43,x_var_dump(1, -2147483650.0));
  LINE(45,x_var_dump(1, 0.0));
  LINE(46,x_var_dump(1, 0.0));
  LINE(47,x_var_dump(1, 0.0));
  LINE(48,x_var_dump(1, -4294967296LL));
  LINE(49,x_var_dump(1, -4294967297LL));
  LINE(50,x_var_dump(1, -4294967298LL));
  LINE(52,x_var_dump(1, -4294967293.0));
  LINE(53,x_var_dump(1, -4294967294.0));
  LINE(54,x_var_dump(1, -4294967295.0));
  LINE(55,x_var_dump(1, -4294967296LL));
  LINE(56,x_var_dump(1, -4294967297LL));
  LINE(57,x_var_dump(1, -4294967298LL));
  LINE(60,x_var_dump(1, 2147483647LL));
  LINE(61,x_var_dump(1, 2.0E+10));
  LINE(62,x_var_dump(1, 20000000007.0));
  LINE(63,x_var_dump(1, -2147483647LL));
  LINE(64,x_var_dump(1, -2.0E+10));
  LINE(65,x_var_dump(1, -20000000007.0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
