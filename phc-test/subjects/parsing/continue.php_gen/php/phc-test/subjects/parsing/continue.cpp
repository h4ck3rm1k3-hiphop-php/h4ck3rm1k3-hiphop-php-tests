
#include <php/phc-test/subjects/parsing/continue.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$continue_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/continue.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$continue_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 5LL);
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_x)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_x = v_x - 1LL);
        if (equal(v_x, 3LL)) {
          continue;
        }
        LINE(12,x_printf(1, toString("x is ") + toString(v_x)));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
