
#ifndef __GENERATED_php_phc_test_subjects_parsing_include_complex_fw_h__
#define __GENERATED_php_phc_test_subjects_parsing_include_complex_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(h)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/subjects/parsing/included_classes_and_functions.fw.h>
#include <php/phc-test/subjects/parsing/included_classes_with_return_values.fw.h>
#include <php/phc-test/subjects/parsing/included_use_vars_in_classes.fw.h>

#endif // __GENERATED_php_phc_test_subjects_parsing_include_complex_fw_h__
