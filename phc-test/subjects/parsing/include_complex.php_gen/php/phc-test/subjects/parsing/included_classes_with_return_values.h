
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_classes_with_return_values_h__
#define __GENERATED_php_phc_test_subjects_parsing_included_classes_with_return_values_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_classes_with_return_values.fw.h>

// Declarations
#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$included_classes_with_return_values_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_c(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_classes_with_return_values_h__
