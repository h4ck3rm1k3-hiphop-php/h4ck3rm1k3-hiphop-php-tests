
#ifndef __GENERATED_cls_h_h__
#define __GENERATED_cls_h_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/include_complex.php line 179 */
class c_h : virtual public ObjectData {
  BEGIN_CLASS_MAP(h)
  END_CLASS_MAP(h)
  DECLARE_CLASS(h, H, ObjectData)
  void init();
  public: void t_h();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_g();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_h_h__
