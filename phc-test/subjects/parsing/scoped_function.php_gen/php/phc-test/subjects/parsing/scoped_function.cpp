
#include <php/phc-test/subjects/parsing/scoped_function.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/scoped_function.php line 7 */
void f_x() {
  FUNCTION_INJECTION(x);
  print("Called\n");
} /* function */
Variant pm_php$phc_test$subjects$parsing$scoped_function_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/scoped_function.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$scoped_function_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,f_x());
  {
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
