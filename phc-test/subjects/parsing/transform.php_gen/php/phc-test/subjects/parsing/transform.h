
#ifndef __GENERATED_php_phc_test_subjects_parsing_transform_h__
#define __GENERATED_php_phc_test_subjects_parsing_transform_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/transform.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$transform_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_foobar();
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_transform_h__
