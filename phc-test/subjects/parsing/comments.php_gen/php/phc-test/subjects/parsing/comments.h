
#ifndef __GENERATED_php_phc_test_subjects_parsing_comments_h__
#define __GENERATED_php_phc_test_subjects_parsing_comments_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/comments.fw.h>

// Declarations
#include <cls/c8.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_f();
Variant pm_php$phc_test$subjects$parsing$comments_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f7();
Object co_c8(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_comments_h__
