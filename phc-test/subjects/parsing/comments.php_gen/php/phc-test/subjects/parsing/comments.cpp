
#include <php/phc-test/subjects/parsing/comments.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/comments.php line 39 */
Variant c_c8::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c8::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c8::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x9", m_x9));
  props.push_back(NEW(ArrayElement)("y10", m_y10));
  c_ObjectData::o_get(props);
}
bool c_c8::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x2EA22F63F68FABB0LL, x9, 2);
      break;
    case 3:
      HASH_EXISTS_STRING(0x41268FED966B9047LL, y10, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c8::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2EA22F63F68FABB0LL, m_x9,
                         x9, 2);
      break;
    case 3:
      HASH_RETURN_STRING(0x41268FED966B9047LL, m_y10,
                         y10, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c8::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x2EA22F63F68FABB0LL, m_x9,
                      x9, 2);
      break;
    case 3:
      HASH_SET_STRING(0x41268FED966B9047LL, m_y10,
                      y10, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c8::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c8::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c8)
ObjectData *c_c8::cloneImpl() {
  c_c8 *obj = NEW(c_c8)();
  cloneSet(obj);
  return obj;
}
void c_c8::cloneSet(c_c8 *clone) {
  clone->m_x9 = m_x9;
  clone->m_y10 = m_y10;
  ObjectData::cloneSet(clone);
}
Variant c_c8::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c8::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c8::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c8$os_get(const char *s) {
  return c_c8::os_get(s, -1);
}
Variant &cw_c8$os_lval(const char *s) {
  return c_c8::os_lval(s, -1);
}
Variant cw_c8$os_constant(const char *s) {
  return c_c8::os_constant(s);
}
Variant cw_c8$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c8::os_invoke(c, s, params, -1, fatal);
}
void c_c8::init() {
  m_x9 = null;
  m_y10 = null;
}
/* SRC: phc-test/subjects/parsing/comments.php line 47 */
void c_c8::t_c11() {
  INSTANCE_METHOD_INJECTION(C8, C8::c11);
  echo("In c11 (without comment)");
} /* function */
/* SRC: phc-test/subjects/parsing/comments.php line 130 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  return 55LL;
} /* function */
/* SRC: phc-test/subjects/parsing/comments.php line 33 */
void f_f7() {
  FUNCTION_INJECTION(f7);
  echo("In f7 (without comment)");
} /* function */
Object co_c8(CArrRef params, bool init /* = true */) {
  return Object(p_c8(NEW(c_c8)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$comments_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/comments.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$comments_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_e1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e1") : g->GV(e1);
  Variant &v_e2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e2") : g->GV(e2);

  (v_a = 3LL);
  echo(toString(1LL));
  echo(toString(2LL));
  echo(toString(3LL));
  echo(toString(4LL));
  echo(toString(41LL));
  echo(toString(42LL));
  {
    echo(toString(43LL));
    echo(toString(44LL));
  }
  echo(toString(5LL));
  (v_a = v_b);
  echo("break 5");
  if (toBoolean(12LL)) {
    echo(toString(13LL));
  }
  else {
    echo(toString(14LL));
  }
  LOOP_COUNTER(1);
  {
    while (toBoolean(15LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(16LL));
        break;
      }
    }
  }
  {
    LOOP_COUNTER(2);
    do {
      LOOP_COUNTER_CHECK(2);
      {
        echo(toString(18LL));
        break;
      }
    } while (toBoolean(17LL));
  }
  {
    LOOP_COUNTER(3);
    for (; toBoolean(0LL); ) {
      LOOP_COUNTER_CHECK(3);
      {
        echo(toString(20LL));
      }
    }
  }
  {
    LOOP_COUNTER(4);
    Variant map5 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter6 = map5.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_x = iter6->second();
      {
        echo(toString(22LL));
      }
    }
  }
  {
    switch (23LL) {
    case 1LL:
      {
        break;
      }
    case 2LL:
      {
        break;
      }
    default:
      {
        break;
      }
    }
  }
  try {
    ;
  } catch (Object e) {
    if (true) {
      throw_fatal("unknown class firstexception");
    } else if (true) {
      throw_fatal("unknown class secondexception");
    } else {
      throw;
    }
  }
  if (toBoolean(26LL)) {
    echo("break 26");
  }
  echo("break 29");
  echo("break 30");
  echo("break 31");
  echo("break 32");
  (v_x = 67LL);
  echo("break 35");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
