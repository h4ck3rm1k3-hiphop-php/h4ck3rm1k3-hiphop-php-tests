
#include <php/phc-test/subjects/parsing/if.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$if_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/if.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$if_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  if (toBoolean(0LL)) ;
  if (toBoolean(2LL)) {
    ;
    ;
  }
  if (toBoolean(5LL)) ;
  if (toBoolean(8LL)) {
    ;
    ;
  }
  if (toBoolean(13LL)) ;
  if (toBoolean(16LL)) ;
  else if (toBoolean(18LL)) ;
  if (toBoolean(21LL)) ;
  else if (toBoolean(23LL)) ;
  else if (toBoolean(25LL)) ;
  if (toBoolean(28LL)) ;
  else if (toBoolean(30LL)) ;
  else if (toBoolean(-30LL)) {}
  else if (toBoolean(32LL)) ;
  else if (toBoolean(34LL)) ;
  if (toBoolean(37LL)) ;
  else if (toBoolean(39LL)) ;
  else if (toBoolean(41LL)) ;
  else if (toBoolean(43LL)) ;
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
