
#include <php/phc-test/subjects/parsing/foreach.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$foreach_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/foreach.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$foreach_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_x.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_y = iter3->second();
      {
        echo(toString(v_y) + toString("\n"));
        break;
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_x.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_z = iter6->second();
      v_y = iter6->first();
      {
        echo(LINE(12,concat4(toString(v_y), " => ", toString(v_z), "\n")));
      }
    }
  }
  {
    LOOP_COUNTER(7);
    Variant map8 = ref(v_x);
    map8.escalate();
    for (MutableArrayIterPtr iter9 = map8.begin(NULL, v_y); iter9->advance();) {
      LOOP_COUNTER_CHECK(7);
      {
        LINE(17,x_var_export(v_x));
        echo(toString(v_y) + toString("\n"));
        LINE(19,x_var_export(v_x));
      }
    }
  }
  {
    LOOP_COUNTER(10);
    Variant map11 = ref(v_x);
    map11.escalate();
    for (MutableArrayIterPtr iter12 = map11.begin(&v_y, v_z); iter12->advance();) {
      LOOP_COUNTER_CHECK(10);
      {
        LINE(24,x_var_export(v_x));
        echo(LINE(25,concat4(toString(v_y), " => ", toString(v_z), "\n")));
        LINE(26,x_var_export(v_x));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
