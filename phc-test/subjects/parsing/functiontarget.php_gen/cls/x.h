
#ifndef __GENERATED_cls_x_h__
#define __GENERATED_cls_x_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/functiontarget.php line 7 */
class c_x : virtual public ObjectData {
  BEGIN_CLASS_MAP(x)
  END_CLASS_MAP(x)
  DECLARE_CLASS(x, X, ObjectData)
  void init();
  public: static void ti_foo(const char* cls);
  public: void t_bar();
  public: static void t_foo() { ti_foo("x"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_x_h__
