
#ifndef __GENERATED_php_phc_test_subjects_parsing_functiontarget_h__
#define __GENERATED_php_phc_test_subjects_parsing_functiontarget_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/functiontarget.fw.h>

// Declarations
#include <cls/x.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$functiontarget_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_foobar();
Object co_x(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_functiontarget_h__
