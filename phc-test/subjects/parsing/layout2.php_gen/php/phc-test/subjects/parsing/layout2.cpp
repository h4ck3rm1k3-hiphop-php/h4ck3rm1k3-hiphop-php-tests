
#include <php/phc-test/subjects/parsing/layout2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$layout2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/layout2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$layout2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_query1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("query1") : g->GV(query1);
  Variant &v_query2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("query2") : g->GV(query2);

  (v_query1 = LINE(5,(assignCallTemp(eo_1, toString(v_a)),assignCallTemp(eo_2, concat6(" FROM ", toString(v_b), " WHERE ", toString(v_c), " GROUPBY ", toString(v_d))),concat3(" SELECT ", eo_1, eo_2))));
  (v_query2 = LINE(10,(assignCallTemp(eo_1, toString(v_a)),assignCallTemp(eo_2, concat6(" FROM ", toString(v_b), " WHERE ", toString(v_c), " GROUPBY ", toString(v_d))),concat3(" SELECT ", eo_1, eo_2))));
  echo(toString(v_query1));
  echo(toString(v_query2));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
