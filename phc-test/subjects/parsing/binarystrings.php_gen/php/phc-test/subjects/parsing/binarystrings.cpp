
#include <php/phc-test/subjects/parsing/binarystrings.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$binarystrings_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/binarystrings.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$binarystrings_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("123");
  echo("123 \001");
  echo("123 \001 \002 \003 \004 \005 \006 456");
  echo(String("123 \0 \001 \002 \003 \004 \005 \006 456", 21, AttachLiteral));
  echo("123 \001");
  echo(toString(String("123 \0 \001 \002 \003 \004 \005 \006 456", 21, AttachLiteral)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
