
#ifndef __GENERATED_cls_firstexception_h__
#define __GENERATED_cls_firstexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/exceptions.php line 2 */
class c_firstexception : virtual public c_exception {
  BEGIN_CLASS_MAP(firstexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(firstexception)
  DECLARE_CLASS(firstexception, FirstException, exception)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_firstexception_h__
