
#ifndef __GENERATED_cls_secondexception_h__
#define __GENERATED_cls_secondexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/exceptions.php line 3 */
class c_secondexception : virtual public c_exception {
  BEGIN_CLASS_MAP(secondexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(secondexception)
  DECLARE_CLASS(secondexception, SecondException, exception)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_secondexception_h__
