
#ifndef __GENERATED_cls_thirdexception_h__
#define __GENERATED_cls_thirdexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/exceptions.php line 4 */
class c_thirdexception : virtual public c_exception {
  BEGIN_CLASS_MAP(thirdexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(thirdexception)
  DECLARE_CLASS(thirdexception, ThirdException, exception)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_thirdexception_h__
