
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_firstexception(CArrRef params, bool init = true);
Variant cw_firstexception$os_get(const char *s);
Variant &cw_firstexception$os_lval(const char *s);
Variant cw_firstexception$os_constant(const char *s);
Variant cw_firstexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_thirdexception(CArrRef params, bool init = true);
Variant cw_thirdexception$os_get(const char *s);
Variant &cw_thirdexception$os_lval(const char *s);
Variant cw_thirdexception$os_constant(const char *s);
Variant cw_thirdexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_secondexception(CArrRef params, bool init = true);
Variant cw_secondexception$os_get(const char *s);
Variant &cw_secondexception$os_lval(const char *s);
Variant cw_secondexception$os_constant(const char *s);
Variant cw_secondexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_CREATE_OBJECT(0x79E8DD9D36C8A470LL, secondexception);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x3A38E372BC645DF4LL, firstexception);
      HASH_CREATE_OBJECT(0x14D488E2A7DA3764LL, thirdexception);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x79E8DD9D36C8A470LL, secondexception);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x3A38E372BC645DF4LL, firstexception);
      HASH_INVOKE_STATIC_METHOD(0x14D488E2A7DA3764LL, thirdexception);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x79E8DD9D36C8A470LL, secondexception);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x3A38E372BC645DF4LL, firstexception);
      HASH_GET_STATIC_PROPERTY(0x14D488E2A7DA3764LL, thirdexception);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x79E8DD9D36C8A470LL, secondexception);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x3A38E372BC645DF4LL, firstexception);
      HASH_GET_STATIC_PROPERTY_LV(0x14D488E2A7DA3764LL, thirdexception);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x79E8DD9D36C8A470LL, secondexception);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x3A38E372BC645DF4LL, firstexception);
      HASH_GET_CLASS_CONSTANT(0x14D488E2A7DA3764LL, thirdexception);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
