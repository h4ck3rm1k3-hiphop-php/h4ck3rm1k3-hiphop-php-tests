
#include <php/phc-test/subjects/parsing/exceptions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/exceptions.php line 2 */
Variant c_firstexception::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_firstexception::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_firstexception::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_firstexception::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_firstexception::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_firstexception::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_firstexception::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_firstexception::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(firstexception)
ObjectData *c_firstexception::cloneImpl() {
  c_firstexception *obj = NEW(c_firstexception)();
  cloneSet(obj);
  return obj;
}
void c_firstexception::cloneSet(c_firstexception *clone) {
  c_exception::cloneSet(clone);
}
Variant c_firstexception::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_firstexception::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_firstexception::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_firstexception$os_get(const char *s) {
  return c_firstexception::os_get(s, -1);
}
Variant &cw_firstexception$os_lval(const char *s) {
  return c_firstexception::os_lval(s, -1);
}
Variant cw_firstexception$os_constant(const char *s) {
  return c_firstexception::os_constant(s);
}
Variant cw_firstexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_firstexception::os_invoke(c, s, params, -1, fatal);
}
void c_firstexception::init() {
  c_exception::init();
}
/* SRC: phc-test/subjects/parsing/exceptions.php line 4 */
Variant c_thirdexception::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_thirdexception::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_thirdexception::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_thirdexception::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_thirdexception::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_thirdexception::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_thirdexception::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_thirdexception::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(thirdexception)
ObjectData *c_thirdexception::cloneImpl() {
  c_thirdexception *obj = NEW(c_thirdexception)();
  cloneSet(obj);
  return obj;
}
void c_thirdexception::cloneSet(c_thirdexception *clone) {
  c_exception::cloneSet(clone);
}
Variant c_thirdexception::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_thirdexception::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_thirdexception::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_thirdexception$os_get(const char *s) {
  return c_thirdexception::os_get(s, -1);
}
Variant &cw_thirdexception$os_lval(const char *s) {
  return c_thirdexception::os_lval(s, -1);
}
Variant cw_thirdexception$os_constant(const char *s) {
  return c_thirdexception::os_constant(s);
}
Variant cw_thirdexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_thirdexception::os_invoke(c, s, params, -1, fatal);
}
void c_thirdexception::init() {
  c_exception::init();
}
/* SRC: phc-test/subjects/parsing/exceptions.php line 3 */
Variant c_secondexception::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_secondexception::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_secondexception::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_secondexception::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_secondexception::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_secondexception::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_secondexception::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_secondexception::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(secondexception)
ObjectData *c_secondexception::cloneImpl() {
  c_secondexception *obj = NEW(c_secondexception)();
  cloneSet(obj);
  return obj;
}
void c_secondexception::cloneSet(c_secondexception *clone) {
  c_exception::cloneSet(clone);
}
Variant c_secondexception::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_secondexception::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_secondexception::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_secondexception$os_get(const char *s) {
  return c_secondexception::os_get(s, -1);
}
Variant &cw_secondexception$os_lval(const char *s) {
  return c_secondexception::os_lval(s, -1);
}
Variant cw_secondexception$os_constant(const char *s) {
  return c_secondexception::os_constant(s);
}
Variant cw_secondexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_secondexception::os_invoke(c, s, params, -1, fatal);
}
void c_secondexception::init() {
  c_exception::init();
}
/* SRC: phc-test/subjects/parsing/exceptions.php line 6 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_x __attribute__((__unused__)) = g->sv_f_DupIdx;
  bool &inited_sv_x __attribute__((__unused__)) = g->inited_sv_f_DupIdx;
  if (!inited_sv_x) {
    (sv_x = 0LL);
    inited_sv_x = true;
  }
  sv_x++;
  {
    switch (sv_x) {
    case 1LL:
      {
        throw_exception(((Object)(LINE(14,p_firstexception(p_firstexception(NEWOBJ(c_firstexception)())->create())))));
      }
    case 2LL:
      {
        throw_exception(((Object)(LINE(15,p_secondexception(p_secondexception(NEWOBJ(c_secondexception)())->create())))));
      }
    case 3LL:
      {
        throw_exception(((Object)(LINE(16,p_thirdexception(p_thirdexception(NEWOBJ(c_thirdexception)())->create())))));
      }
    }
  }
} /* function */
Object co_firstexception(CArrRef params, bool init /* = true */) {
  return Object(p_firstexception(NEW(c_firstexception)())->dynCreate(params, init));
}
Object co_thirdexception(CArrRef params, bool init /* = true */) {
  return Object(p_thirdexception(NEW(c_thirdexception)())->dynCreate(params, init));
}
Object co_secondexception(CArrRef params, bool init /* = true */) {
  return Object(p_secondexception(NEW(c_secondexception)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$exceptions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/exceptions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$exceptions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  try {
    LINE(22,f_f());
  } catch (Object e) {
    if (e.instanceof("firstexception")) {
      v_e = e;
      echo("FirstException\n");
    } else {
      throw;
    }
  }
  try {
    LINE(31,f_f());
  } catch (Object e) {
    if (e.instanceof("firstexception")) {
      v_e = e;
      echo("FirstException\n");
    } else if (e.instanceof("secondexception")) {
      v_e = e;
      echo("SecondException\n");
    } else {
      throw;
    }
  }
  try {
    LINE(44,f_f());
  } catch (Object e) {
    if (e.instanceof("firstexception")) {
      v_e = e;
      echo("FirstException\n");
    } else if (e.instanceof("secondexception")) {
      v_e = e;
      echo("SecondException\n");
    } else if (e.instanceof("thirdexception")) {
      v_e = e;
      echo("ThirdException\n");
    } else {
      throw;
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
