
#ifndef __GENERATED_php_phc_test_subjects_parsing_exceptions_h__
#define __GENERATED_php_phc_test_subjects_parsing_exceptions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/exceptions.fw.h>

// Declarations
#include <cls/firstexception.h>
#include <cls/thirdexception.h>
#include <cls/secondexception.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_f();
Variant pm_php$phc_test$subjects$parsing$exceptions_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_firstexception(CArrRef params, bool init = true);
Object co_thirdexception(CArrRef params, bool init = true);
Object co_secondexception(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_exceptions_h__
