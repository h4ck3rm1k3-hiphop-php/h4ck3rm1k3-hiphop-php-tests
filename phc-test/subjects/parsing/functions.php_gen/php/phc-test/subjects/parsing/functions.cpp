
#include <php/phc-test/subjects/parsing/functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/functions.php line 2 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: phc-test/subjects/parsing/functions.php line 16 */
Variant c_d::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_d::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_d::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_d::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_d::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_d::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_d::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(d)
ObjectData *c_d::cloneImpl() {
  c_d *obj = NEW(c_d)();
  cloneSet(obj);
  return obj;
}
void c_d::cloneSet(c_d *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d$os_get(const char *s) {
  return c_d::os_get(s, -1);
}
Variant &cw_d$os_lval(const char *s) {
  return c_d::os_lval(s, -1);
}
Variant cw_d$os_constant(const char *s) {
  return c_d::os_constant(s);
}
Variant cw_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d::os_invoke(c, s, params, -1, fatal);
}
void c_d::init() {
}
/* SRC: phc-test/subjects/parsing/functions.php line 18 */
void c_d::t_da(CVarRef v_x, CVarRef v_y) {
  INSTANCE_METHOD_INJECTION(D, D::da);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 19 */
void c_d::t_db(Variant v_x, Variant v_y) {
  INSTANCE_METHOD_INJECTION(D, D::db);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 20 */
void c_d::t_dc(p_c v_x, p_d v_y) {
  INSTANCE_METHOD_INJECTION(D, D::dc);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 21 */
void c_d::t_dc(CArrRef v_x, CArrRef v_y) {
  INSTANCE_METHOD_INJECTION(D, D::dc);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 22 */
Variant c_d::t_dd(Variant v_x, Variant v_y) {
  INSTANCE_METHOD_INJECTION(D, D::dd);
  return null;
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 24 */
void c_d::t_de(CVarRef v_x //  = null_variant
, CVarRef v_y //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(D, D::de);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 25 */
void c_d::t_df(Variant v_x //  = null
, Variant v_y //  = null
) {
  INSTANCE_METHOD_INJECTION(D, D::df);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 26 */
void c_d::t_dg(CVarRef v_x //  = null_variant
, CVarRef v_y //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(D, D::dg);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 27 */
void c_d::t_dg(CVarRef v_x //  = null_variant
, CVarRef v_y //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(D, D::dg);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 28 */
Variant c_d::t_dh(Variant v_x //  = null
, Variant v_y //  = null
) {
  INSTANCE_METHOD_INJECTION(D, D::dh);
  return null;
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 4 */
void f_a(CVarRef v_x, CVarRef v_y) {
  FUNCTION_INJECTION(a);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 5 */
void f_b(Variant v_x, Variant v_y) {
  FUNCTION_INJECTION(b);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 8 */
Variant f_d(Variant v_x, Variant v_y) {
  FUNCTION_INJECTION(d);
  return null;
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 10 */
void f_e(CVarRef v_x //  = null_variant
, CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(e);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 11 */
void f_f(Variant v_x //  = null
, Variant v_y //  = null
) {
  FUNCTION_INJECTION(f);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 12 */
void f_g_DupId0(CVarRef v_x //  = null_variant
, CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(g);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 13 */
void f_g_DupId1(CVarRef v_x //  = null_variant
, CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(g);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 14 */
Variant f_h(Variant v_x //  = null
, Variant v_y //  = null
) {
  FUNCTION_INJECTION(h);
  return null;
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 6 */
void f_c1(p_c v_x, p_d v_y) {
  FUNCTION_INJECTION(c1);
} /* function */
/* SRC: phc-test/subjects/parsing/functions.php line 7 */
void f_c2(CArrRef v_x, CArrRef v_y) {
  FUNCTION_INJECTION(c2);
} /* function */
Variant i_g_DupId0(CArrRef params) {
  int count = params.size();
  if (count <= 0) return (f_g_DupId0(), null);
  if (count == 1) return (f_g_DupId0(params.rvalAt(0)), null);
  return (f_g_DupId0(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_g(CArrRef params) {
  DECLARE_GLOBAL_VARIABLES(g);
  return g->i_g(params);
}
Variant i_g_DupId1(CArrRef params) {
  int count = params.size();
  if (count <= 0) return (f_g_DupId1(), null);
  if (count == 1) return (f_g_DupId1(params.rvalAt(0)), null);
  return (f_g_DupId1(params.rvalAt(0), params.rvalAt(1)), null);
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d(CArrRef params, bool init /* = true */) {
  return Object(p_d(NEW(c_d)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  g->i_g = i_g_DupId0;
  g->declareFunction("g");
  g->i_g = i_g_DupId1;
  g->declareFunction("g");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
