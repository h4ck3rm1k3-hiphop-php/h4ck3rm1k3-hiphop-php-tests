
#ifndef __GENERATED_php_phc_test_subjects_parsing_functions_h__
#define __GENERATED_php_phc_test_subjects_parsing_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/functions.fw.h>

// Declarations
#include <cls/c.h>
#include <cls/d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$functions_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_a(CVarRef v_x, CVarRef v_y);
void f_b(Variant v_x, Variant v_y);
Variant f_d(Variant v_x, Variant v_y);
void f_e(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
void f_f(Variant v_x = null, Variant v_y = null);
void f_g_DupId0(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
void f_g_DupId1(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
Variant f_h(Variant v_x = null, Variant v_y = null);
void f_c1(p_c v_x, p_d v_y);
void f_c2(CArrRef v_x, CArrRef v_y);
Variant i_g_DupId0(CArrRef params);
Variant i_g_DupId1(CArrRef params);
Object co_c(CArrRef params, bool init = true);
Object co_d(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_functions_h__
