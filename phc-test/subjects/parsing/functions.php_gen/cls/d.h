
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/functions.php line 16 */
class c_d : virtual public ObjectData {
  BEGIN_CLASS_MAP(d)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, ObjectData)
  void init();
  public: void t_da(CVarRef v_x, CVarRef v_y);
  public: void t_db(Variant v_x, Variant v_y);
  public: void t_dc(p_c v_x, p_d v_y);
  public: void t_dc(CArrRef v_x, CArrRef v_y);
  public: Variant t_dd(Variant v_x, Variant v_y);
  public: void t_de(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
  public: void t_df(Variant v_x = null, Variant v_y = null);
  public: void t_dg(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
  public: void t_dg(CVarRef v_x = null_variant, CVarRef v_y = null_variant);
  public: Variant t_dh(Variant v_x = null, Variant v_y = null);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
