
#ifndef __GENERATED_php_phc_test_subjects_parsing_good_hint_h__
#define __GENERATED_php_phc_test_subjects_parsing_good_hint_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/good_hint.fw.h>

// Declarations
#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$good_hint_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_x1(CVarRef v_y);
void f_x2(CVarRef v_y);
void f_x3(CArrRef v_y);
void f_x4(CVarRef v_y);
void f_x5(p_a v_y);
void f_y1(CVarRef v_y = null_variant);
void f_y2(CVarRef v_y = null_variant);
void f_y3(CVarRef v_y = null_variant);
void f_y4(CVarRef v_y = null_variant);
void f_y5(CVarRef v_y = null_variant);
void f_y3_1(CArrRef v_y = ScalarArrays::sa_[0]);
Object co_a(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_good_hint_h__
