
#include <php/phc-test/subjects/parsing/good_hint.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/good_hint.php line 3 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: phc-test/subjects/parsing/good_hint.php line 6 */
void f_x1(CVarRef v_y) {
  FUNCTION_INJECTION(x1);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 7 */
void f_x2(CVarRef v_y) {
  FUNCTION_INJECTION(x2);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 8 */
void f_x3(CArrRef v_y) {
  FUNCTION_INJECTION(x3);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 9 */
void f_x4(CVarRef v_y) {
  FUNCTION_INJECTION(x4);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 10 */
void f_x5(p_a v_y) {
  FUNCTION_INJECTION(x5);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 13 */
void f_y1(CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(y1);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 14 */
void f_y2(CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(y2);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 15 */
void f_y3(CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(y3);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 17 */
void f_y4(CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(y4);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 18 */
void f_y5(CVarRef v_y //  = null_variant
) {
  FUNCTION_INJECTION(y5);
} /* function */
/* SRC: phc-test/subjects/parsing/good_hint.php line 16 */
void f_y3_1(CArrRef v_y //  = ScalarArrays::sa_[0]
) {
  FUNCTION_INJECTION(y3_1);
} /* function */
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$good_hint_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/good_hint.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$good_hint_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
