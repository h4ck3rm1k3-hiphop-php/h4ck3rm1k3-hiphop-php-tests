
#include <php/phc-test/subjects/parsing/foreach_expansion.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$foreach_expansion_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/foreach_expansion.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$foreach_expansion_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_xx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xx") : g->GV(xx);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = ScalarArrays::sa_[0]);
  (v_xx = "x");
  unset(v_y);
  echo("** 9 **\n");
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(variables->get(toString(v_xx)));
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(&v_y, v_z); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(13,concat5("", toString(v_y), " => ", toString(v_z), "\n")));
      }
    }
  }
  LINE(16,x_var_dump(1, v_x));
  LINE(17,x_var_dump(1, v_y));
  LINE(18,x_var_dump(1, v_xx));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
