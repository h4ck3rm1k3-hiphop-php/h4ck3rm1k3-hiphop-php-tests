
#include <php/phc-test/subjects/parsing/included_multiple_class_def.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/included_multiple_class_def.php line 3 */
Variant c_f::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_f::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_f::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_f::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_f::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_f::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_f::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_f::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(f)
ObjectData *c_f::cloneImpl() {
  c_f *obj = NEW(c_f)();
  cloneSet(obj);
  return obj;
}
void c_f::cloneSet(c_f *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_f::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_f::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_f::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_f$os_get(const char *s) {
  return c_f::os_get(s, -1);
}
Variant &cw_f$os_lval(const char *s) {
  return c_f::os_lval(s, -1);
}
Variant cw_f$os_constant(const char *s) {
  return c_f::os_constant(s);
}
Variant cw_f$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_f::os_invoke(c, s, params, -1, fatal);
}
void c_f::init() {
}
Object co_f(CArrRef params, bool init /* = true */) {
  return Object(p_f(NEW(c_f)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$included_multiple_class_def_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/included_multiple_class_def.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$included_multiple_class_def_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
