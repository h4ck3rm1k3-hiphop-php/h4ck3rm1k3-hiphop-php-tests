
#include <php/phc-test/subjects/parsing/layout4.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$layout4_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/layout4.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$layout4_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  (v_arr = Array(ArrayInit(11).set(0, 125LL).set(1, 1.25).set(2, "string").set(3, false).set(4, true).set(5, null).set(6, 9).set(7, get_source_filename("phc-test/subjects/parsing/layout4.php")).set(8, "").set(9, "").set(10, "").create()));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
