
#include <php/phc-test/subjects/parsing/switch.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$switch_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/switch.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$switch_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  {
    switch (toInt64(v_x)) {
    case 1LL:
      {
        echo("one\n");
        break;
      }
    case 2LL:
      {
        echo("two\n");
        break;
      }
    default:
      {
        echo("unknown\n");
        break;
      }
    }
  }
  {
    int64 tmp3 = (toInt64(v_x));
    int tmp4 = -1;
    if (equal(tmp3, (1LL))) {
      tmp4 = 1;
    } else if (equal(tmp3, (2LL))) {
      tmp4 = 2;
    } else if (true) {
      tmp4 = 0;
    }
    switch (tmp4) {
    case 0:
      {
        echo("unknown\n");
        goto break2;
      }
    case 1:
      {
        echo("one\n");
        goto break2;
      }
    case 2:
      {
        echo("two\n");
        goto break2;
      }
    }
    break2:;
  }
  {
    switch (1LL) {
    case 2LL:
      {
      }
    }
  }
  {
    switch (3LL) {
    case 1LL:
      {
        ;
      }
    case 2LL:
      {
        ;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
