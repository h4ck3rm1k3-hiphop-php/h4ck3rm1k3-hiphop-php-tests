
#include <php/phc-test/subjects/parsing/tags.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_W = "W";
const StaticString k_X = "X";
const StaticString k_Y = "Y";
const StaticString k_Z = "Z";

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$tags_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/tags.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$tags_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(k_W);
  echo(k_X);
  echo(k_Y);
  echo(k_Z);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
