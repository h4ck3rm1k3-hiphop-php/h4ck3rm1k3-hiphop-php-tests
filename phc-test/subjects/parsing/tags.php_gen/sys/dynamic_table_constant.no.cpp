
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_W;
extern const StaticString k_X;
extern const StaticString k_Y;
extern const StaticString k_Z;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x00F31DE90234EA81LL, k_X, X);
      break;
    case 2:
      HASH_RETURN(0x799C602B8BBB7C2ALL, k_W, W);
      HASH_RETURN(0x2E68D2D2AF66E5E2LL, k_Z, Z);
      break;
    case 4:
      HASH_RETURN(0x7F64BC1ECEF60CCCLL, k_Y, Y);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
