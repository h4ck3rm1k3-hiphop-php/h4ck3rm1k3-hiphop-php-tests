
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$parsing$include_once_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$removeconcatnulls_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_INCLUDE(0x33AD735241D4B044LL, "phc-test/subjects/parsing/include_once.php", php$phc_test$subjects$parsing$include_once_php);
      break;
    case 2:
      HASH_INCLUDE(0x323A4D79B59D35AELL, "phc-test/subjects/parsing/removeconcatnulls.php", php$phc_test$subjects$parsing$removeconcatnulls_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
