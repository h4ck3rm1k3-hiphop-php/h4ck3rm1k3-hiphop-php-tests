
#include <php/phc-test/subjects/parsing/removeconcatnulls.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$removeconcatnulls_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/removeconcatnulls.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$removeconcatnulls_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_left __attribute__((__unused__)) = (variables != gVariables) ? variables->get("left") : g->GV(left);
  Variant &v_right __attribute__((__unused__)) = (variables != gVariables) ? variables->get("right") : g->GV(right);
  Variant &v_both __attribute__((__unused__)) = (variables != gVariables) ? variables->get("both") : g->GV(both);
  Variant &v_more __attribute__((__unused__)) = (variables != gVariables) ? variables->get("more") : g->GV(more);
  Variant &v_one __attribute__((__unused__)) = (variables != gVariables) ? variables->get("one") : g->GV(one);

  echo("none");
  echo(toString(v_left) + toString(" text"));
  echo(toString("text ") + toString(v_right));
  echo(toString(v_both));
  echo(LINE(7,concat3(toString(v_more), " than ", toString(v_one))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
