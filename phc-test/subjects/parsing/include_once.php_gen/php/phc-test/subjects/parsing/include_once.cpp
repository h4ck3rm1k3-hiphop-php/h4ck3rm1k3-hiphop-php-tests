
#include <php/phc-test/subjects/parsing/include_once.h>
#include <php/phc-test/subjects/parsing/removeconcatnulls.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$include_once_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/include_once.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$include_once_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,pm_php$phc_test$subjects$parsing$removeconcatnulls_php(false, variables));
  LINE(4,pm_php$phc_test$subjects$parsing$removeconcatnulls_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
