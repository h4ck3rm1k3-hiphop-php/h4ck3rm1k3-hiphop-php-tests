
#include <php/phc-test/subjects/parsing/new.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$new_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/new.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$new_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);

  (v_x = LINE(2,create_object("c", Array())));
  (v_x = LINE(3,create_object("c", Array())));
  (v_y = LINE(4,create_object("d", Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_z = LINE(5,create_object("e", Array(ArrayInit(2).set(0, v_x).set(1, v_y).create()))));
  (v_a = LINE(7,create_object(toString(v_x), Array())));
  (v_b = LINE(8,create_object(toString(v_x), Array())));
  (v_c = LINE(9,create_object(toString(v_x), Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_d = LINE(10,create_object(toString(v_x), Array(ArrayInit(2).set(0, ref(v_y)).set(1, ref(v_z)).create()))));
  (v_e = LINE(12,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array())));
  (v_f = LINE(13,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array())));
  (v_g = LINE(14,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_h = LINE(15,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array(ArrayInit(2).set(0, ref(v_y)).set(1, ref(v_z)).create()))));
  (v_e = LINE(17,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array())));
  (v_f = LINE(18,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array())));
  (v_g = LINE(19,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_h = LINE(20,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array(ArrayInit(2).set(0, ref(v_y)).set(1, ref(v_z)).create()))));
  (v_x = LINE(24,create_object("c", Array())));
  (v_x = LINE(25,create_object("c", Array())));
  (v_y = LINE(26,create_object("d", Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_z = LINE(27,create_object("e", Array(ArrayInit(2).set(0, v_x).set(1, v_y).create()))));
  (v_a = LINE(29,create_object(toString(v_x), Array())));
  (v_b = LINE(30,create_object(toString(v_x), Array())));
  (v_c = LINE(31,create_object(toString(v_x), Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_d = LINE(32,create_object(toString(v_x), Array(ArrayInit(2).set(0, ref(v_y)).set(1, ref(v_z)).create()))));
  (v_e = LINE(34,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array())));
  (v_f = LINE(35,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array())));
  (v_g = LINE(36,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_h = LINE(37,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL))), Array(ArrayInit(2).set(0, ref(v_y)).set(1, ref(v_z)).create()))));
  (v_e = LINE(39,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array())));
  (v_f = LINE(40,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array())));
  (v_g = LINE(41,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array(ArrayInit(1).set(0, 1LL).create()))));
  (v_h = LINE(42,create_object((toString(v_x.o_get("y", 0x4F56B733A4DFC78ALL).o_get("z", 0x62A103F6518DE2B3LL))), Array(ArrayInit(2).set(0, ref(v_y)).set(1, ref(v_z)).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
