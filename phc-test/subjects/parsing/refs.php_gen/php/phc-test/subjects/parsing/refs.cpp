
#include <php/phc-test/subjects/parsing/refs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/refs.php line 78 */
void f_f1(Variant v_par) {
  FUNCTION_INJECTION(f1);
  LINE(80,x_var_dump(1, v_par));
  v_par *= 2LL;
} /* function */
/* SRC: phc-test/subjects/parsing/refs.php line 87 */
void f_f2(Variant v_par) {
  FUNCTION_INJECTION(f2);
  LINE(89,x_var_dump(1, v_par));
  v_par *= 2LL;
} /* function */
/* SRC: phc-test/subjects/parsing/refs.php line 96 */
void f_f3(Variant v_par) {
  FUNCTION_INJECTION(f3);
  DECLARE_GLOBAL_VARIABLES(g);
  v_par = ref(g->GV(par));
  LINE(99,x_var_dump(1, v_par));
  v_par *= 2LL;
} /* function */
/* SRC: phc-test/subjects/parsing/refs.php line 112 */
int64 f_f4() {
  FUNCTION_INJECTION(f4);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_in_f4 __attribute__((__unused__)) = g->sv_f4_DupIdin_f4;
  bool &inited_sv_in_f4 __attribute__((__unused__)) = g->inited_sv_f4_DupIdin_f4;
  if (!inited_sv_in_f4) {
    (sv_in_f4 = 0LL);
    inited_sv_in_f4 = true;
  }
  sv_in_f4++;
  return sv_in_f4;
} /* function */
/* SRC: phc-test/subjects/parsing/refs.php line 138 */
Variant f_f5() {
  FUNCTION_INJECTION(f5);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_in_f5 __attribute__((__unused__)) = g->sv_f5_DupIdin_f5;
  bool &inited_sv_in_f5 __attribute__((__unused__)) = g->inited_sv_f5_DupIdin_f5;
  if (!inited_sv_in_f5) {
    (sv_in_f5 = 0LL);
    inited_sv_in_f5 = true;
  }
  sv_in_f5++;
  return ref(sv_in_f5);
} /* function */
/* SRC: phc-test/subjects/parsing/refs.php line 175 */
void f_h1(Variant v_in_h1) {
  FUNCTION_INJECTION(h1);
  v_in_h1++;
} /* function */
/* SRC: phc-test/subjects/parsing/refs.php line 184 */
void f_h2(Variant v_in_h2) {
  FUNCTION_INJECTION(h2);
  v_in_h2++;
} /* function */
Variant pm_php$phc_test$subjects$parsing$refs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/refs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$refs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_base __attribute__((__unused__)) = (variables != gVariables) ? variables->get("base") : g->GV(base);
  Variant &v_ref __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ref") : g->GV(ref);
  Variant &v_base_1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("base_1") : g->GV(base_1);
  Variant &v_base_2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("base_2") : g->GV(base_2);
  Variant &v_base_3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("base_3") : g->GV(base_3);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_out_f4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out_f4") : g->GV(out_f4);
  Variant &v_out_f5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out_f5") : g->GV(out_f5);
  Variant &v_out_h1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out_h1") : g->GV(out_h1);
  Variant &v_out_h2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out_h2") : g->GV(out_h2);

  (v_base = 1LL);
  (v_ref = ref(v_base));
  LINE(15,x_var_dump(1, v_base));
  LINE(16,x_var_dump(1, v_ref));
  (v_ref = 2LL);
  LINE(20,x_var_dump(1, v_base));
  LINE(21,x_var_dump(1, v_ref));
  (v_base_1 = 3LL);
  (v_base_2 = 4LL);
  (v_ref = Array(ArrayInit(2).setRef(0, 1LL, ref(v_base_1), 0x5BCA7C69B794F8CELL).setRef(1, 2LL, ref(v_base_2), 0x486AFCC090D5F98CLL).create()));
  LINE(32,x_var_dump(1, v_ref));
  (v_base_1 = 5LL);
  (v_base_2 = 6LL);
  LINE(37,x_var_dump(1, v_ref));
  v_ref.set(1LL, (7LL), 0x5BCA7C69B794F8CELL);
  v_ref.set(2LL, (8LL), 0x486AFCC090D5F98CLL);
  LINE(42,x_var_dump(1, v_ref));
  LINE(43,x_var_dump(1, v_base_1));
  LINE(44,x_var_dump(1, v_base_2));
  (v_base_3 = 9LL);
  v_ref.set(2LL, (ref(v_base_3)), 0x486AFCC090D5F98CLL);
  LINE(49,x_var_dump(1, v_ref));
  (v_arr = ScalarArrays::sa_[0]);
  LINE(59,x_var_dump(1, v_arr));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_arr.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_val = iter3->second();
      v_key = iter3->first();
      {
        v_key += 10LL;
        v_val *= 2LL;
      }
    }
  }
  LINE(62,x_var_dump(1, v_arr));
  {
    LOOP_COUNTER(4);
    Variant map5 = ref(v_arr);
    map5.escalate();
    for (MutableArrayIterPtr iter6 = map5.begin(&v_key, v_val); iter6->advance();) {
      LOOP_COUNTER_CHECK(4);
      {
        v_key += 10LL;
        v_val *= 2LL;
      }
    }
  }
  LINE(65,x_var_dump(1, v_arr));
  {
    LOOP_COUNTER(7);
    Variant map8 = ref(v_arr);
    map8.escalate();
    for (MutableArrayIterPtr iter9 = map8.begin(&v_key, v_val); iter9->advance();) {
      LOOP_COUNTER_CHECK(7);
      {
        v_key += 10LL;
        v_val *= 2LL;
      }
    }
  }
  LINE(68,x_var_dump(1, v_arr));
  (v_x = 2LL);
  LINE(84,f_f1(v_x));
  LINE(85,x_var_dump(1, v_x));
  LINE(93,f_f2(ref(v_x)));
  LINE(94,x_var_dump(1, v_x));
  LINE(103,f_f3(ref(v_x)));
  LINE(104,x_var_dump(1, v_x));
  (v_out_f4 = LINE(120,f_f4()));
  LINE(121,x_var_dump(1, v_out_f4));
  (v_out_f4 = LINE(123,f_f4()));
  LINE(124,x_var_dump(1, v_out_f4));
  (v_out_f4 = 10LL);
  LINE(127,x_var_dump(1, v_out_f4));
  (v_out_f4 = LINE(128,f_f4()));
  LINE(129,x_var_dump(1, v_out_f4));
  (v_out_f4 = ref(LINE(131,f_f4())));
  LINE(132,x_var_dump(1, v_out_f4));
  (v_out_f4 = 10LL);
  LINE(134,x_var_dump(1, v_out_f4));
  (v_out_f4 = LINE(135,f_f4()));
  LINE(136,x_var_dump(1, v_out_f4));
  (v_out_f5 = LINE(146,f_f5()));
  LINE(147,x_var_dump(1, v_out_f5));
  (v_out_f5 = LINE(149,f_f5()));
  LINE(150,x_var_dump(1, v_out_f5));
  (v_out_f5 = 10LL);
  LINE(153,x_var_dump(1, v_out_f5));
  (v_out_f5 = LINE(154,f_f5()));
  LINE(155,x_var_dump(1, v_out_f5));
  (v_out_f5 = ref(LINE(157,f_f5())));
  LINE(158,x_var_dump(1, v_out_f5));
  (v_out_f5 = 10LL);
  LINE(160,x_var_dump(1, v_out_f5));
  (v_out_f5 = LINE(161,f_f5()));
  LINE(162,x_var_dump(1, v_out_f5));
  (v_out_h1 = 5LL);
  LINE(181,f_h1(ref(v_out_h1)));
  LINE(182,x_var_dump(1, v_out_h1));
  (v_out_h2 = 5LL);
  LINE(190,f_h2(ref(v_out_h2)));
  LINE(191,x_var_dump(1, v_out_h2));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
