
#ifndef __GENERATED_php_phc_test_subjects_parsing_refs_h__
#define __GENERATED_php_phc_test_subjects_parsing_refs_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/refs.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$refs_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f1(Variant v_par);
void f_f2(Variant v_par);
void f_f3(Variant v_par);
int64 f_f4();
Variant f_f5();
void f_h1(Variant v_in_h1);
void f_h2(Variant v_in_h2);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_refs_h__
