
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "base",
    "ref",
    "base_1",
    "base_2",
    "base_3",
    "arr",
    "key",
    "val",
    "x",
    "par",
    "out_f4",
    "out_f5",
    "out_h1",
    "out_h2",
  };
  if (idx >= 0 && idx < 26) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(base);
    case 13: return GV(ref);
    case 14: return GV(base_1);
    case 15: return GV(base_2);
    case 16: return GV(base_3);
    case 17: return GV(arr);
    case 18: return GV(key);
    case 19: return GV(val);
    case 20: return GV(x);
    case 21: return GV(par);
    case 22: return GV(out_f4);
    case 23: return GV(out_f5);
    case 24: return GV(out_h1);
    case 25: return GV(out_h2);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
