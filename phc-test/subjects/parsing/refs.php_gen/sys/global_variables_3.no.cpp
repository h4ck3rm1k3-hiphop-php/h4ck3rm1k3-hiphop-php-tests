
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_INDEX(0x108A202AC1B87E40LL, out_h1, 24);
      break;
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x4056C2E766E0B782LL, val, 19);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x7344C7B77EB70747LL, base_1, 14);
      HASH_INDEX(0x612DD31212E90587LL, key, 18);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x5AA045BD179DD509LL, base_2, 15);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 22:
      HASH_INDEX(0x04BFC205E59FA416LL, x, 20);
      break;
    case 23:
      HASH_INDEX(0x15FF92E9B0153597LL, base_3, 16);
      break;
    case 24:
      HASH_INDEX(0x6A0500ABA0ADBE18LL, out_f4, 22);
      break;
    case 27:
      HASH_INDEX(0x1CF81634ADE7AD5BLL, out_f5, 23);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      HASH_INDEX(0x3735A84687EBF263LL, out_h2, 25);
      break;
    case 39:
      HASH_INDEX(0x56FD8ECCBDFDD7A7LL, base, 12);
      break;
    case 40:
      HASH_INDEX(0x1776D8467CB08D68LL, arr, 17);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 58:
      HASH_INDEX(0x0B1A6D25134FD5FALL, ref, 13);
      break;
    case 61:
      HASH_INDEX(0x2D158B3C4AF37D7DLL, par, 21);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 26) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
