
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_INITIALIZED(0x108A202AC1B87E40LL, g->GV(out_h1),
                       out_h1);
      break;
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 2:
      HASH_INITIALIZED(0x4056C2E766E0B782LL, g->GV(val),
                       val);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 7:
      HASH_INITIALIZED(0x7344C7B77EB70747LL, g->GV(base_1),
                       base_1);
      HASH_INITIALIZED(0x612DD31212E90587LL, g->GV(key),
                       key);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x5AA045BD179DD509LL, g->GV(base_2),
                       base_2);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 22:
      HASH_INITIALIZED(0x04BFC205E59FA416LL, g->GV(x),
                       x);
      break;
    case 23:
      HASH_INITIALIZED(0x15FF92E9B0153597LL, g->GV(base_3),
                       base_3);
      break;
    case 24:
      HASH_INITIALIZED(0x6A0500ABA0ADBE18LL, g->GV(out_f4),
                       out_f4);
      break;
    case 27:
      HASH_INITIALIZED(0x1CF81634ADE7AD5BLL, g->GV(out_f5),
                       out_f5);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      HASH_INITIALIZED(0x3735A84687EBF263LL, g->GV(out_h2),
                       out_h2);
      break;
    case 39:
      HASH_INITIALIZED(0x56FD8ECCBDFDD7A7LL, g->GV(base),
                       base);
      break;
    case 40:
      HASH_INITIALIZED(0x1776D8467CB08D68LL, g->GV(arr),
                       arr);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 58:
      HASH_INITIALIZED(0x0B1A6D25134FD5FALL, g->GV(ref),
                       ref);
      break;
    case 61:
      HASH_INITIALIZED(0x2D158B3C4AF37D7DLL, g->GV(par),
                       par);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
