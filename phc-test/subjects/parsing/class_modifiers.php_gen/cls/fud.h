
#ifndef __GENERATED_cls_fud_h__
#define __GENERATED_cls_fud_h__

#include <cls/foobar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/class_modifiers.php line 18 */
class c_fud : virtual public c_foobar {
  BEGIN_CLASS_MAP(fud)
    PARENT_CLASS(foobar)
  END_CLASS_MAP(fud)
  DECLARE_CLASS(fud, Fud, foobar)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fud_h__
