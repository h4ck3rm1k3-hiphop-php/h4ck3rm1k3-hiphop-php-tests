
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/class_modifiers.php line 8 */
class c_bar : virtual public ObjectData {
  BEGIN_CLASS_MAP(bar)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, Bar, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
