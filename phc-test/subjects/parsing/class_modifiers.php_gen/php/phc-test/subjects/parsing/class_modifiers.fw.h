
#ifndef __GENERATED_php_phc_test_subjects_parsing_class_modifiers_fw_h__
#define __GENERATED_php_phc_test_subjects_parsing_class_modifiers_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(x)
FORWARD_DECLARE_CLASS(z)
FORWARD_DECLARE_CLASS(foo)
FORWARD_DECLARE_CLASS(foobar)
FORWARD_DECLARE_CLASS(fud)
FORWARD_DECLARE_CLASS(bar)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_class_modifiers_fw_h__
