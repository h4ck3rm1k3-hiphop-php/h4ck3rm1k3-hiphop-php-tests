
#include <php/phc-test/subjects/parsing/class_modifiers.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/class_modifiers.php line 23 */
Variant c_x::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
}
/* SRC: phc-test/subjects/parsing/class_modifiers.php line 28 */
Variant c_z::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_z::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_z::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_z::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_z::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_z::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_z::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_z::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(z)
ObjectData *c_z::cloneImpl() {
  c_z *obj = NEW(c_z)();
  cloneSet(obj);
  return obj;
}
void c_z::cloneSet(c_z *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_z::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_z::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_z::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_z$os_get(const char *s) {
  return c_z::os_get(s, -1);
}
Variant &cw_z$os_lval(const char *s) {
  return c_z::os_lval(s, -1);
}
Variant cw_z$os_constant(const char *s) {
  return c_z::os_constant(s);
}
Variant cw_z$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_z::os_invoke(c, s, params, -1, fatal);
}
void c_z::init() {
}
/* SRC: phc-test/subjects/parsing/class_modifiers.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
/* SRC: phc-test/subjects/parsing/class_modifiers.php line 13 */
Variant c_foobar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foobar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foobar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foobar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foobar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foobar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foobar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foobar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foobar)
ObjectData *c_foobar::cloneImpl() {
  c_foobar *obj = NEW(c_foobar)();
  cloneSet(obj);
  return obj;
}
void c_foobar::cloneSet(c_foobar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foobar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foobar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foobar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foobar$os_get(const char *s) {
  return c_foobar::os_get(s, -1);
}
Variant &cw_foobar$os_lval(const char *s) {
  return c_foobar::os_lval(s, -1);
}
Variant cw_foobar$os_constant(const char *s) {
  return c_foobar::os_constant(s);
}
Variant cw_foobar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foobar::os_invoke(c, s, params, -1, fatal);
}
void c_foobar::init() {
}
/* SRC: phc-test/subjects/parsing/class_modifiers.php line 18 */
Variant c_fud::os_get(const char *s, int64 hash) {
  return c_foobar::os_get(s, hash);
}
Variant &c_fud::os_lval(const char *s, int64 hash) {
  return c_foobar::os_lval(s, hash);
}
void c_fud::o_get(ArrayElementVec &props) const {
  c_foobar::o_get(props);
}
bool c_fud::o_exists(CStrRef s, int64 hash) const {
  return c_foobar::o_exists(s, hash);
}
Variant c_fud::o_get(CStrRef s, int64 hash) {
  return c_foobar::o_get(s, hash);
}
Variant c_fud::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_foobar::o_set(s, hash, v, forInit);
}
Variant &c_fud::o_lval(CStrRef s, int64 hash) {
  return c_foobar::o_lval(s, hash);
}
Variant c_fud::os_constant(const char *s) {
  return c_foobar::os_constant(s);
}
IMPLEMENT_CLASS(fud)
ObjectData *c_fud::cloneImpl() {
  c_fud *obj = NEW(c_fud)();
  cloneSet(obj);
  return obj;
}
void c_fud::cloneSet(c_fud *clone) {
  c_foobar::cloneSet(clone);
}
Variant c_fud::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_foobar::o_invoke(s, params, hash, fatal);
}
Variant c_fud::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_foobar::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fud::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_foobar::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fud$os_get(const char *s) {
  return c_fud::os_get(s, -1);
}
Variant &cw_fud$os_lval(const char *s) {
  return c_fud::os_lval(s, -1);
}
Variant cw_fud$os_constant(const char *s) {
  return c_fud::os_constant(s);
}
Variant cw_fud$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fud::os_invoke(c, s, params, -1, fatal);
}
void c_fud::init() {
  c_foobar::init();
}
/* SRC: phc-test/subjects/parsing/class_modifiers.php line 8 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Object co_z(CArrRef params, bool init /* = true */) {
  return Object(p_z(NEW(c_z)())->dynCreate(params, init));
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_foobar(CArrRef params, bool init /* = true */) {
  return Object(p_foobar(NEW(c_foobar)())->dynCreate(params, init));
}
Object co_fud(CArrRef params, bool init /* = true */) {
  return Object(p_fud(NEW(c_fud)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$class_modifiers_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/class_modifiers.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$class_modifiers_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
