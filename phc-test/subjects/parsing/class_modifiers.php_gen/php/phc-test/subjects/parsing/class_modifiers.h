
#ifndef __GENERATED_php_phc_test_subjects_parsing_class_modifiers_h__
#define __GENERATED_php_phc_test_subjects_parsing_class_modifiers_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/class_modifiers.fw.h>

// Declarations
#include <cls/x.h>
#include <cls/z.h>
#include <cls/foo.h>
#include <cls/foobar.h>
#include <cls/fud.h>
#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$class_modifiers_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_x(CArrRef params, bool init = true);
Object co_z(CArrRef params, bool init = true);
Object co_foo(CArrRef params, bool init = true);
Object co_foobar(CArrRef params, bool init = true);
Object co_fud(CArrRef params, bool init = true);
Object co_bar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_class_modifiers_h__
