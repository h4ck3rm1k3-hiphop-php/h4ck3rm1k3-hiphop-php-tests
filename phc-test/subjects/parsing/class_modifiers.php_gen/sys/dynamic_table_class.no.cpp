
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_x(CArrRef params, bool init = true);
Variant cw_x$os_get(const char *s);
Variant &cw_x$os_lval(const char *s);
Variant cw_x$os_constant(const char *s);
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_z(CArrRef params, bool init = true);
Variant cw_z$os_get(const char *s);
Variant &cw_z$os_lval(const char *s);
Variant cw_z$os_constant(const char *s);
Variant cw_z$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_foo(CArrRef params, bool init = true);
Variant cw_foo$os_get(const char *s);
Variant &cw_foo$os_lval(const char *s);
Variant cw_foo$os_constant(const char *s);
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_foobar(CArrRef params, bool init = true);
Variant cw_foobar$os_get(const char *s);
Variant &cw_foobar$os_lval(const char *s);
Variant cw_foobar$os_constant(const char *s);
Variant cw_foobar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fud(CArrRef params, bool init = true);
Variant cw_fud$os_get(const char *s);
Variant &cw_fud$os_lval(const char *s);
Variant cw_fud$os_constant(const char *s);
Variant cw_fud$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bar(CArrRef params, bool init = true);
Variant cw_bar$os_get(const char *s);
Variant &cw_bar$os_lval(const char *s);
Variant cw_bar$os_constant(const char *s);
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_CREATE_OBJECT(0x00F31DE90234EA81LL, x);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x2E68D2D2AF66E5E2LL, z);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x6176C0B993BF5914LL, foo);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x2ACE2D47D81F2549LL, foobar);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x16200C145752E18ALL, fud);
      break;
    case 13:
      HASH_CREATE_OBJECT(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x00F31DE90234EA81LL, x);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x2E68D2D2AF66E5E2LL, z);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x6176C0B993BF5914LL, foo);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x2ACE2D47D81F2549LL, foobar);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x16200C145752E18ALL, fud);
      break;
    case 13:
      HASH_INVOKE_STATIC_METHOD(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x00F31DE90234EA81LL, x);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x2E68D2D2AF66E5E2LL, z);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x6176C0B993BF5914LL, foo);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x2ACE2D47D81F2549LL, foobar);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x16200C145752E18ALL, fud);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x00F31DE90234EA81LL, x);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x2E68D2D2AF66E5E2LL, z);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x6176C0B993BF5914LL, foo);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x2ACE2D47D81F2549LL, foobar);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x16200C145752E18ALL, fud);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY_LV(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x00F31DE90234EA81LL, x);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x2E68D2D2AF66E5E2LL, z);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x6176C0B993BF5914LL, foo);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x2ACE2D47D81F2549LL, foobar);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x16200C145752E18ALL, fud);
      break;
    case 13:
      HASH_GET_CLASS_CONSTANT(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
