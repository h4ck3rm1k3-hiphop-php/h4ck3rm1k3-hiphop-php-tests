
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "bar", "phc-test/subjects/parsing/class_modifiers.php",
  "foo", "phc-test/subjects/parsing/class_modifiers.php",
  "foobar", "phc-test/subjects/parsing/class_modifiers.php",
  "fud", "phc-test/subjects/parsing/class_modifiers.php",
  "x", "phc-test/subjects/parsing/class_modifiers.php",
  "z", "phc-test/subjects/parsing/class_modifiers.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
