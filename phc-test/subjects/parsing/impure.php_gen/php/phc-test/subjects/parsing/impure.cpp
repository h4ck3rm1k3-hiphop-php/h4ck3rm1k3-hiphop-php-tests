
#include <php/phc-test/subjects/parsing/impure.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_f = "f";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/impure.php line 5 */
Variant f_f(CVarRef v_element1, CVarRef v_element2) {
  FUNCTION_INJECTION(f);
  return v_element1;
} /* function */
Variant pm_php$phc_test$subjects$parsing$impure_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/impure.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$impure_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = ScalarArrays::sa_[0]);
  (v_y = LINE(10,x_array_reduce(v_x, k_f)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
