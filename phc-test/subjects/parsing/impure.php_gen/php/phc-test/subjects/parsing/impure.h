
#ifndef __GENERATED_php_phc_test_subjects_parsing_impure_h__
#define __GENERATED_php_phc_test_subjects_parsing_impure_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/impure.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$impure_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_f(CVarRef v_element1, CVarRef v_element2);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_impure_h__
