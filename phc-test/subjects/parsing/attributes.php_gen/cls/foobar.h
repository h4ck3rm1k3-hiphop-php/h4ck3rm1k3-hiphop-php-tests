
#ifndef __GENERATED_cls_foobar_h__
#define __GENERATED_cls_foobar_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/attributes.php line 3 */
class c_foobar : virtual public ObjectData {
  BEGIN_CLASS_MAP(foobar)
  END_CLASS_MAP(foobar)
  DECLARE_CLASS(foobar, FooBar, ObjectData)
  void init();
  public: String m_a;
  public: int64 m_b;
  public: String m_c;
  public: String m_d;
  public: int64 m_e;
  public: int64 m_f;
  public: String m_g;
  public: int64 m_h;
  public: String m_i;
  public: String m_j;
  public: String m_l;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foobar_h__
