
#include <php/phc-test/subjects/parsing/attributes.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/attributes.php line 3 */
Variant c_foobar::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x117B8667E4662809LL, g->s_foobar_DupIdn,
                  n);
      break;
    case 3:
      HASH_RETURN(0x7A0C10E3609EA04BLL, g->s_foobar_DupIdm,
                  m);
      break;
    case 7:
      HASH_RETURN(0x0D6857FDDD7F21CFLL, g->s_foobar_DupIdk,
                  k);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foobar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foobar::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  props.push_back(NEW(ArrayElement)("b", m_b));
  props.push_back(NEW(ArrayElement)("c", m_c));
  props.push_back(NEW(ArrayElement)("d", m_d));
  props.push_back(NEW(ArrayElement)("e", m_e));
  props.push_back(NEW(ArrayElement)("f", m_f));
  props.push_back(NEW(ArrayElement)("g", m_g));
  props.push_back(NEW(ArrayElement)("h", m_h));
  props.push_back(NEW(ArrayElement)("i", m_i));
  props.push_back(NEW(ArrayElement)("j", m_j));
  props.push_back(NEW(ArrayElement)("l", m_l));
  c_ObjectData::o_get(props);
}
bool c_foobar::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 2:
      HASH_EXISTS_STRING(0x2343A72E98024302LL, l, 1);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x4B27011F259D2446LL, j, 1);
      break;
    case 8:
      HASH_EXISTS_STRING(0x2BCCE9AD79BC2688LL, g, 1);
      break;
    case 10:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    case 11:
      HASH_EXISTS_STRING(0x61B161496B7EA7EBLL, e, 1);
      break;
    case 14:
      HASH_EXISTS_STRING(0x6BB4A0689FBAD42ELL, f, 1);
      break;
    case 16:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      HASH_EXISTS_STRING(0x695875365480A8F0LL, h, 1);
      break;
    case 21:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    case 24:
      HASH_EXISTS_STRING(0x0EB22EDA95766E98LL, i, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foobar::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 2:
      HASH_RETURN_STRING(0x2343A72E98024302LL, m_l,
                         l, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x4B27011F259D2446LL, m_j,
                         j, 1);
      break;
    case 8:
      HASH_RETURN_STRING(0x2BCCE9AD79BC2688LL, m_g,
                         g, 1);
      break;
    case 10:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      break;
    case 14:
      HASH_RETURN_STRING(0x6BB4A0689FBAD42ELL, m_f,
                         f, 1);
      break;
    case 16:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      HASH_RETURN_STRING(0x695875365480A8F0LL, m_h,
                         h, 1);
      break;
    case 21:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 24:
      HASH_RETURN_STRING(0x0EB22EDA95766E98LL, m_i,
                         i, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foobar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 2:
      HASH_SET_STRING(0x2343A72E98024302LL, m_l,
                      l, 1);
      break;
    case 4:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    case 6:
      HASH_SET_STRING(0x4B27011F259D2446LL, m_j,
                      j, 1);
      break;
    case 8:
      HASH_SET_STRING(0x2BCCE9AD79BC2688LL, m_g,
                      g, 1);
      break;
    case 10:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    case 11:
      HASH_SET_STRING(0x61B161496B7EA7EBLL, m_e,
                      e, 1);
      break;
    case 14:
      HASH_SET_STRING(0x6BB4A0689FBAD42ELL, m_f,
                      f, 1);
      break;
    case 16:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      HASH_SET_STRING(0x695875365480A8F0LL, m_h,
                      h, 1);
      break;
    case 21:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    case 24:
      HASH_SET_STRING(0x0EB22EDA95766E98LL, m_i,
                      i, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foobar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foobar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foobar)
ObjectData *c_foobar::cloneImpl() {
  c_foobar *obj = NEW(c_foobar)();
  cloneSet(obj);
  return obj;
}
void c_foobar::cloneSet(c_foobar *clone) {
  clone->m_a = m_a;
  clone->m_b = m_b;
  clone->m_c = m_c;
  clone->m_d = m_d;
  clone->m_e = m_e;
  clone->m_f = m_f;
  clone->m_g = m_g;
  clone->m_h = m_h;
  clone->m_i = m_i;
  clone->m_j = m_j;
  clone->m_l = m_l;
  ObjectData::cloneSet(clone);
}
Variant c_foobar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foobar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foobar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foobar$os_get(const char *s) {
  return c_foobar::os_get(s, -1);
}
Variant &cw_foobar$os_lval(const char *s) {
  return c_foobar::os_lval(s, -1);
}
Variant cw_foobar$os_constant(const char *s) {
  return c_foobar::os_constant(s);
}
Variant cw_foobar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foobar::os_invoke(c, s, params, -1, fatal);
}
void c_foobar::init() {
  m_a = null;
  m_b = 10LL;
  m_c = null;
  m_d = null;
  m_e = 10LL;
  m_f = 20LL;
  m_g = null;
  m_h = 100LL;
  m_i = null;
  m_j = null;
  m_l = null;
}
void c_foobar::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_foobar_DupIdk = null;
  g->s_foobar_DupIdn = null;
  g->s_foobar_DupIdm = null;
}
void csi_foobar() {
  c_foobar::os_static_initializer();
}
Object co_foobar(CArrRef params, bool init /* = true */) {
  return Object(p_foobar(NEW(c_foobar)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$attributes_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/attributes.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$attributes_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
