
#ifndef __GENERATED_php_phc_test_subjects_parsing_attributes_h__
#define __GENERATED_php_phc_test_subjects_parsing_attributes_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/attributes.fw.h>

// Declarations
#include <cls/foobar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$attributes_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foobar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_attributes_h__
