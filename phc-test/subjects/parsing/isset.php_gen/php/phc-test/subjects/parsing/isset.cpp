
#include <php/phc-test/subjects/parsing/isset.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$isset_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/isset.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$isset_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = 10LL);
  (v_y = 20LL);
  if (isset(v_x)) {
    echo("x is set\n");
  }
  if ((isset(v_x) && isset(v_y))) {
    echo("x, y set.\n");
  }
  if (isset(v_z)) {
    echo("z is set");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
