
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_cc2_h__
#define __GENERATED_php_phc_test_subjects_parsing_included_cc2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_cc2.fw.h>

// Declarations
#include <cls/cl2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$included_cc2_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_cl2(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_cc2_h__
