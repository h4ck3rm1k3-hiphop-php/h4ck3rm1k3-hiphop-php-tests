
#include <php/phc-test/subjects/parsing/included_cc2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/included_cc2.php line 3 */
Variant c_cl2::os_get(const char *s, int64 hash) {
  return c_cl1::os_get(s, hash);
}
Variant &c_cl2::os_lval(const char *s, int64 hash) {
  return c_cl1::os_lval(s, hash);
}
void c_cl2::o_get(ArrayElementVec &props) const {
  c_cl1::o_get(props);
}
bool c_cl2::o_exists(CStrRef s, int64 hash) const {
  return c_cl1::o_exists(s, hash);
}
Variant c_cl2::o_get(CStrRef s, int64 hash) {
  return c_cl1::o_get(s, hash);
}
Variant c_cl2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_cl1::o_set(s, hash, v, forInit);
}
Variant &c_cl2::o_lval(CStrRef s, int64 hash) {
  return c_cl1::o_lval(s, hash);
}
Variant c_cl2::os_constant(const char *s) {
  return c_cl1::os_constant(s);
}
IMPLEMENT_CLASS(cl2)
ObjectData *c_cl2::create() {
  init();
  t_cl2();
  return this;
}
ObjectData *c_cl2::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_cl2::dynConstruct(CArrRef params) {
  (t_cl2());
}
ObjectData *c_cl2::cloneImpl() {
  c_cl2 *obj = NEW(c_cl2)();
  cloneSet(obj);
  return obj;
}
void c_cl2::cloneSet(c_cl2 *clone) {
  c_cl1::cloneSet(clone);
}
Variant c_cl2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x5196F01952A476CELL, cl2) {
        return (t_cl2(), null);
      }
      break;
    default:
      break;
  }
  return c_cl1::o_invoke(s, params, hash, fatal);
}
Variant c_cl2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x5196F01952A476CELL, cl2) {
        return (t_cl2(), null);
      }
      break;
    default:
      break;
  }
  return c_cl1::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_cl2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_cl1::os_invoke(c, s, params, hash, fatal);
}
Variant cw_cl2$os_get(const char *s) {
  return c_cl2::os_get(s, -1);
}
Variant &cw_cl2$os_lval(const char *s) {
  return c_cl2::os_lval(s, -1);
}
Variant cw_cl2$os_constant(const char *s) {
  return c_cl2::os_constant(s);
}
Variant cw_cl2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_cl2::os_invoke(c, s, params, -1, fatal);
}
void c_cl2::init() {
  c_cl1::init();
}
/* SRC: phc-test/subjects/parsing/included_cc2.php line 5 */
void c_cl2::t_cl2() {
  INSTANCE_METHOD_INJECTION(cl2, cl2::cl2);
  bool oldInCtor = gasInCtor(true);
  ;
  gasInCtor(oldInCtor);
} /* function */
Object co_cl2(CArrRef params, bool init /* = true */) {
  return Object(p_cl2(NEW(c_cl2)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$included_cc2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/included_cc2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$included_cc2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
