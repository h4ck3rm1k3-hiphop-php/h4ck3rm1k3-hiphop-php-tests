
#ifndef __GENERATED_cls_cl2_h__
#define __GENERATED_cls_cl2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/included_cc2.php line 3 */
class c_cl2 : virtual public ObjectData {
  BEGIN_CLASS_MAP(cl2)
  END_CLASS_MAP(cl2)
  DECLARE_CLASS(cl2, cl2, cl1)
  void init();
  public: void t_cl2();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cl2_h__
