
#ifndef __GENERATED_cls_k_h__
#define __GENERATED_cls_k_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/included_use_vars_in_classes.php line 3 */
class c_k : virtual public ObjectData {
  BEGIN_CLASS_MAP(k)
  END_CLASS_MAP(k)
  DECLARE_CLASS(k, K, ObjectData)
  void init();
  public: String m_my_var;
  public: void t_k();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_kk();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_k_h__
