
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_use_vars_in_classes_h__
#define __GENERATED_php_phc_test_subjects_parsing_included_use_vars_in_classes_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_use_vars_in_classes.fw.h>

// Declarations
#include <cls/k.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$included_use_vars_in_classes_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_k(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_use_vars_in_classes_h__
