
#ifndef __GENERATED_cls_foobar_h__
#define __GENERATED_cls_foobar_h__

#include <cls/foo.h>
#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/interfaces.php line 13 */
class c_foobar : virtual public c_foo, virtual public c_bar {
  // public: void t_fb() = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foobar_h__
