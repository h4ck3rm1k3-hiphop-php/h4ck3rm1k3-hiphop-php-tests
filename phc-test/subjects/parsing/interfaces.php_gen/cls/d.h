
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__

#include <cls/foobar.h>
#include <cls/boo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/interfaces.php line 23 */
class c_d : virtual public c_foobar, virtual public c_boo {
  BEGIN_CLASS_MAP(d)
    PARENT_CLASS(foo)
    PARENT_CLASS(bar)
    PARENT_CLASS(foobar)
    PARENT_CLASS(boo)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
