
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__

#include <cls/d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/interfaces.php line 27 */
class c_c : virtual public c_d {
  BEGIN_CLASS_MAP(c)
    PARENT_CLASS(foo)
    PARENT_CLASS(bar)
    PARENT_CLASS(foobar)
    PARENT_CLASS(boo)
    PARENT_CLASS(d)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, d)
  void init();
  public: void t_f();
  public: void t_b();
  public: void t_fb();
  public: void t_o();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
