
#include <php/phc-test/subjects/parsing/interfaces.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/interfaces.php line 27 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_d::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_d::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_d::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_d::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_d::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_d::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_d::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_d::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  c_d::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x5D15540336D3767BLL, fb) {
        return (t_fb(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x39E9518192E6D2AELL, b) {
        return (t_b(), null);
      }
      HASH_GUARD(0x3769074A877921E6LL, o) {
        return (t_o(), null);
      }
      break;
    default:
      break;
  }
  return c_d::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x286CE1C477560280LL, f) {
        return (t_f(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x5D15540336D3767BLL, fb) {
        return (t_fb(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x39E9518192E6D2AELL, b) {
        return (t_b(), null);
      }
      HASH_GUARD(0x3769074A877921E6LL, o) {
        return (t_o(), null);
      }
      break;
    default:
      break;
  }
  return c_d::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_d::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  c_d::init();
}
/* SRC: phc-test/subjects/parsing/interfaces.php line 29 */
void c_c::t_f() {
  INSTANCE_METHOD_INJECTION(C, C::f);
  echo("f\n");
} /* function */
/* SRC: phc-test/subjects/parsing/interfaces.php line 30 */
void c_c::t_b() {
  INSTANCE_METHOD_INJECTION(C, C::b);
  echo("b\n");
} /* function */
/* SRC: phc-test/subjects/parsing/interfaces.php line 31 */
void c_c::t_fb() {
  INSTANCE_METHOD_INJECTION(C, C::fb);
  echo("fb\n");
} /* function */
/* SRC: phc-test/subjects/parsing/interfaces.php line 32 */
void c_c::t_o() {
  INSTANCE_METHOD_INJECTION(C, C::o);
  echo("o\n");
} /* function */
/* SRC: phc-test/subjects/parsing/interfaces.php line 23 */
Variant c_d::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_d::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_d::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_d::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_d::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_d::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_d::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_d::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(d)
ObjectData *c_d::cloneImpl() {
  c_d *obj = NEW(c_d)();
  cloneSet(obj);
  return obj;
}
void c_d::cloneSet(c_d *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_d::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_d::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_d::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_d$os_get(const char *s) {
  return c_d::os_get(s, -1);
}
Variant &cw_d$os_lval(const char *s) {
  return c_d::os_lval(s, -1);
}
Variant cw_d$os_constant(const char *s) {
  return c_d::os_constant(s);
}
Variant cw_d$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_d::os_invoke(c, s, params, -1, fatal);
}
void c_d::init() {
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Object co_d(CArrRef params, bool init /* = true */) {
  return Object(p_d(NEW(c_d)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$interfaces_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/interfaces.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$interfaces_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_c = ((Object)(LINE(35,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(36,v_c.o_invoke_few_args("f", 0x286CE1C477560280LL, 0));
  LINE(37,v_c.o_invoke_few_args("b", 0x39E9518192E6D2AELL, 0));
  LINE(38,v_c.o_invoke_few_args("fb", 0x5D15540336D3767BLL, 0));
  LINE(39,v_c.o_invoke_few_args("o", 0x3769074A877921E6LL, 0));
  echo("5\n");
  echo(concat(toString(throw_fatal("unknown class constant foobar::x")), "\n"));
  echo(concat(toString(throw_fatal("unknown class constant c::x")), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
