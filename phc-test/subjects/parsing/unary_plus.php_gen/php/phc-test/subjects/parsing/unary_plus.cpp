
#include <php/phc-test/subjects/parsing/unary_plus.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$unary_plus_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/unary_plus.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$unary_plus_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 1LL);
  LINE(3,x_var_dump(1, v_x));
  (v_x = 1.0);
  LINE(5,x_var_dump(1, v_x));
  (v_x = 1LL);
  LINE(7,x_var_dump(1, v_x));
  (v_x = -1LL);
  LINE(9,x_var_dump(1, v_x));
  (v_x = -1LL);
  LINE(12,x_var_dump(1, v_x));
  (v_x = 1LL);
  LINE(14,x_var_dump(1, v_x));
  (v_x = -1LL);
  LINE(16,x_var_dump(1, v_x));
  (v_x = 1LL);
  LINE(18,x_var_dump(1, v_x));
  (v_x = -1LL);
  LINE(20,x_var_dump(1, v_x));
  (v_x = -1LL);
  LINE(23,x_var_dump(1, v_x));
  (v_x = -1LL);
  LINE(25,x_var_dump(1, v_x));
  (v_x = 1LL);
  LINE(27,x_var_dump(1, v_x));
  (v_x = 1LL);
  LINE(29,x_var_dump(1, v_x));
  (v_x = -1LL);
  LINE(31,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
