
#include <php/phc-test/subjects/parsing/backtick_in_string_syntax.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$backtick_in_string_syntax_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/backtick_in_string_syntax.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$backtick_in_string_syntax_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_root __attribute__((__unused__)) = (variables != gVariables) ? variables->get("root") : g->GV(root);

  (v_root = "/");
  echo(f_shell_exec(toString("ls ") + toString(v_root)));
  echo(f_shell_exec(toString("ls /")));
  echo(f_shell_exec(toString("ls \\\\`")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
