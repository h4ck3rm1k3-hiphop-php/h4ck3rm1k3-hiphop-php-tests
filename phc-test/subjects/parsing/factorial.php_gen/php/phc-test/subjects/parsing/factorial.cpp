
#include <php/phc-test/subjects/parsing/factorial.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/factorial.php line 2 */
Variant f_factorial(Numeric v_n) {
  FUNCTION_INJECTION(factorial);
  if (equal(v_n, 0LL)) return 1LL;
  else return v_n * LINE(7,f_factorial(v_n - 1LL));
  return null;
} /* function */
Variant pm_php$phc_test$subjects$parsing$factorial_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/factorial.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$factorial_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(toString(LINE(10,f_factorial(5LL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
