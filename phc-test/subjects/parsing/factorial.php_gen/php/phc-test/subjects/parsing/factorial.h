
#ifndef __GENERATED_php_phc_test_subjects_parsing_factorial_h__
#define __GENERATED_php_phc_test_subjects_parsing_factorial_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/factorial.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_factorial(Numeric v_n);
Variant pm_php$phc_test$subjects$parsing$factorial_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_factorial_h__
