
#include <php/phc-test/subjects/parsing/for.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$for_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/for.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$for_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  {
    LOOP_COUNTER(1);
    for ((v_x = 10LL); more(v_x, 0LL); (v_x = v_x - 1LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_y = 2LL);
        (v_y = v_y * v_y);
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_x = 0LL), (v_y = 0LL); more(v_x, 10LL); (v_x = v_x + 1LL)) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_x = 3LL);
      }
    }
  }
  {
    LOOP_COUNTER(3);
    for (; ; ) {
      LOOP_COUNTER_CHECK(3);
      {
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for (; ; ) {
      LOOP_COUNTER_CHECK(4);
      (v_x = v_x + 1LL);
    }
  }
  {
    LOOP_COUNTER(5);
    for (; ; ) {
      LOOP_COUNTER_CHECK(5);
    }
  }
  {
    LOOP_COUNTER(6);
    for ((v_x = 0LL); less(v_x, 10LL); (v_x = v_x + 1LL)) {
      LOOP_COUNTER_CHECK(6);
      {
        (v_x = v_y);
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
