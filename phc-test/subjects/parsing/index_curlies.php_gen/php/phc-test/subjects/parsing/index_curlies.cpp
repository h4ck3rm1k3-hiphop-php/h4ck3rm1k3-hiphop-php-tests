
#include <php/phc-test/subjects/parsing/index_curlies.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$index_curlies_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/index_curlies.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$index_curlies_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  lval(lval(lval(v_arr.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)).set(3LL, ("foo\n"), 0x135FDDF6A6BFBBDDLL);
  echo(toString(v_arr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  echo(toString(v_arr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
