
#ifndef __GENERATED_php_phc_test_subjects_parsing_index_curlies_h__
#define __GENERATED_php_phc_test_subjects_parsing_index_curlies_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/index_curlies.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$index_curlies_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_index_curlies_h__
