
#include <php/phc-test/subjects/parsing/inline_html_indent.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$inline_html_indent_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/inline_html_indent.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$inline_html_indent_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 3LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo("foo\n");
        echo("bar\n");
        echo("baz\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
