
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_multiple_class_def_h__
#define __GENERATED_php_phc_test_subjects_parsing_included_multiple_class_def_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_multiple_class_def.fw.h>

// Declarations
#include <cls/f.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$included_multiple_class_def_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_f(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_multiple_class_def_h__
