
#include <php/phc-test/subjects/parsing/nestedfndefs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/nestedfndefs.php line 4 */
void f_f_DupId0() {
  FUNCTION_INJECTION(f);
  echo("first f\n");
} /* function */
/* SRC: phc-test/subjects/parsing/nestedfndefs.php line 11 */
void f_f_DupId1() {
  FUNCTION_INJECTION(f);
  echo("second f\n");
} /* function */
/* SRC: phc-test/subjects/parsing/nestedfndefs.php line 19 */
void f_g() {
  FUNCTION_INJECTION(g);
} /* function */
/* SRC: phc-test/subjects/parsing/nestedfndefs.php line 21 */
void f_h() {
  FUNCTION_INJECTION(h);
  echo("h\n");
} /* function */
Variant i_f_DupId0(CArrRef params) {
  return (f_f_DupId0(), null);
}
Variant i_f(CArrRef params) {
  DECLARE_GLOBAL_VARIABLES(g);
  return g->i_f(params);
}
Variant i_f_DupId1(CArrRef params) {
  return (f_f_DupId1(), null);
}
Variant pm_php$phc_test$subjects$parsing$nestedfndefs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/nestedfndefs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$nestedfndefs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_expr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("expr") : g->GV(expr);

  if (toBoolean(v_expr)) {
    g->i_f = i_f_DupId0;
    g->declareFunction("f");
  }
  else {
    g->i_f = i_f_DupId1;
    g->declareFunction("f");
  }
  LINE(17,get_global_variables()->i_f(Array()));
  LINE(27,f_g());
  LINE(28,f_h());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
