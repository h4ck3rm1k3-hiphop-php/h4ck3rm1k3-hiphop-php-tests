
#ifndef __GENERATED_php_phc_test_subjects_parsing_nestedfndefs_h__
#define __GENERATED_php_phc_test_subjects_parsing_nestedfndefs_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/nestedfndefs.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$nestedfndefs_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f_DupId0();
void f_f_DupId1();
void f_g();
void f_h();
Variant i_f_DupId0(CArrRef params);
Variant i_f_DupId1(CArrRef params);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_nestedfndefs_h__
