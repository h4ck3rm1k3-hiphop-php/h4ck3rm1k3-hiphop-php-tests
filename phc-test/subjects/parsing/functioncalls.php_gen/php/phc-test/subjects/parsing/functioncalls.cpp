
#include <php/phc-test/subjects/parsing/functioncalls.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/functioncalls.php line 2 */
void f_f() {
  FUNCTION_INJECTION(f);
} /* function */
/* SRC: phc-test/subjects/parsing/functioncalls.php line 3 */
void f_g(CVarRef v_a) {
  FUNCTION_INJECTION(g);
} /* function */
/* SRC: phc-test/subjects/parsing/functioncalls.php line 4 */
void f_h(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(h);
} /* function */
/* SRC: phc-test/subjects/parsing/functioncalls.php line 5 */
void f_i(CVarRef v_a, CVarRef v_b, CVarRef v_c) {
  FUNCTION_INJECTION(i);
} /* function */
Variant pm_php$phc_test$subjects$parsing$functioncalls_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/functioncalls.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$functioncalls_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  LINE(7,f_f());
  LINE(9,f_g(5LL));
  LINE(10,f_g(v_x));
  LINE(11,f_g(ref(v_x)));
  LINE(13,f_h(5LL, 6LL));
  LINE(14,f_h(v_x, v_y));
  LINE(15,f_h(ref(v_x), ref(v_y)));
  LINE(17,f_i(5LL, 6LL, 7LL));
  LINE(18,f_i(v_x, v_y, v_z));
  LINE(19,f_i(ref(v_x), ref(v_y), ref(v_z)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
