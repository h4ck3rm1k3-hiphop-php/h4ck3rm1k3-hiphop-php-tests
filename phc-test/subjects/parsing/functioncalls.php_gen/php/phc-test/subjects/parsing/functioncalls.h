
#ifndef __GENERATED_php_phc_test_subjects_parsing_functioncalls_h__
#define __GENERATED_php_phc_test_subjects_parsing_functioncalls_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/functioncalls.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$functioncalls_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f();
void f_g(CVarRef v_a);
void f_h(CVarRef v_a, CVarRef v_b);
void f_i(CVarRef v_a, CVarRef v_b, CVarRef v_c);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_functioncalls_h__
