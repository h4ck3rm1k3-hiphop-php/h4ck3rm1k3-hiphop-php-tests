
#include <php/phc-test/subjects/parsing/invocation.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/invocation.php line 2 */
Variant c_x::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  props.push_back(NEW(ArrayElement)("a", m_a));
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::create() {
  init();
  t_x();
  return this;
}
ObjectData *c_x::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_x::dynConstruct(CArrRef params) {
  (t_x());
}
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  clone->m_a = m_a;
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x7F64BC1ECEF60CCCLL, y) {
        return (t_y());
      }
      break;
    case 1:
      HASH_GUARD(0x00F31DE90234EA81LL, x) {
        return (t_x(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x7F64BC1ECEF60CCCLL, y) {
        return (t_y());
      }
      break;
    case 1:
      HASH_GUARD(0x00F31DE90234EA81LL, x) {
        return (t_x(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
  m_x = null;
  m_a = ScalarArrays::sa_[0];
}
/* SRC: phc-test/subjects/parsing/invocation.php line 6 */
void c_x::t_x() {
  INSTANCE_METHOD_INJECTION(X, X::x);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/parsing/invocation.php line 7 */
p_x c_x::t_y() {
  INSTANCE_METHOD_INJECTION(X, X::y);
  return ((Object)(LINE(7,p_x(p_x(NEWOBJ(c_x)())->create()))));
} /* function */
/* SRC: phc-test/subjects/parsing/invocation.php line 10 */
p_x f_b() {
  FUNCTION_INJECTION(b);
  return ((Object)(LINE(12,p_x(p_x(NEWOBJ(c_x)())->create()))));
} /* function */
Variant i_b(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x39E9518192E6D2AELL, b) {
    return (f_b());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$invocation_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/invocation.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$invocation_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_y = "x");
  (v_z = "y");
  (v_x = ((Object)(LINE(17,p_x(p_x(NEWOBJ(c_x)())->create())))));
  (v_x.o_lval("x", 0x04BFC205E59FA416LL) = ((Object)(LINE(18,p_x(p_x(NEWOBJ(c_x)())->create())))));
  ;
  ;
  LINE(22,v_x.o_invoke_few_args("x", 0x00F31DE90234EA81LL, 0));
  LINE(23,v_x.o_invoke_few_args(toString(v_y), -1LL, 0));
  ;
  ;
  ;
  (assignCallTemp(eo_0, toObject(LINE(29,v_x.o_invoke_few_args("y", 0x7F64BC1ECEF60CCCLL, 0)))),eo_0.o_invoke_few_args("x", 0x00F31DE90234EA81LL, 0));
  (assignCallTemp(eo_1, toObject(LINE(30,v_x.o_invoke_few_args(toString(v_z), -1LL, 0)))),eo_1.o_invoke_few_args(toString(v_y), -1LL, 0));
  ;
  LINE(33,invoke((toString(v_x.o_get("a", 0x4292CEE227B9150ALL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1));
  LINE(34,invoke((toString(v_x.o_get("a", 0x4292CEE227B9150ALL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1)).o_get("a", 0x4292CEE227B9150ALL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL);
  LINE(35,invoke((toString(invoke((toString(v_x.o_get("a", 0x4292CEE227B9150ALL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1).o_get("a", 0x4292CEE227B9150ALL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), Array(), -1));
  ;
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
