
#ifndef __GENERATED_php_phc_test_subjects_parsing_invocation_h__
#define __GENERATED_php_phc_test_subjects_parsing_invocation_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/invocation.fw.h>

// Declarations
#include <cls/x.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

p_x f_b();
Variant pm_php$phc_test$subjects$parsing$invocation_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_x(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_invocation_h__
