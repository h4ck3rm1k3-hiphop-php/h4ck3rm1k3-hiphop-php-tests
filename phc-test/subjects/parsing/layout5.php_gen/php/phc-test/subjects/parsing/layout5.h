
#ifndef __GENERATED_php_phc_test_subjects_parsing_layout5_h__
#define __GENERATED_php_phc_test_subjects_parsing_layout5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/layout5.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$layout5_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_layout5_h__
