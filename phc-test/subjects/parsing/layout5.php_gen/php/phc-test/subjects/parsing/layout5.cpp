
#include <php/phc-test/subjects/parsing/layout5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$layout5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/layout5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$layout5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  if (toBoolean(v_x)) echo("hi");
  else echo("bye");
  if (toBoolean(v_x)) echo("hi");
  else if (toBoolean(v_y)) echo("ho");
  else echo("bo");
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_c)) {
      LOOP_COUNTER_CHECK(1);
      echo("do something");
    }
  }
  {
    LOOP_COUNTER(2);
    do {
      LOOP_COUNTER_CHECK(2);
      echo("do something else");
    } while (toBoolean(v_c));
  }
  {
    LOOP_COUNTER(3);
    for (; toBoolean(v_b); ) {
      LOOP_COUNTER_CHECK(3);
      echo(toString(v_d));
    }
  }
  {
    LOOP_COUNTER(4);
    Variant map5 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter6 = map5.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_val = iter6->second();
      v_key = iter6->first();
      echo("booh");
    }
  }
  {
    switch (toInt64(v_a)) {
    case 1LL:
      {
        break;
      }
    case 2LL:
      {
        break;
      }
    default:
      {
        break;
      }
    }
  }
  try {
    echo("do something that could fail");
  } catch (Object e) {
    if (true) {
      throw_fatal("unknown class firstexception");
    } else if (true) {
      throw_fatal("unknown class secondexception");
    } else {
      throw;
    }
  }
  echo("final statement");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
