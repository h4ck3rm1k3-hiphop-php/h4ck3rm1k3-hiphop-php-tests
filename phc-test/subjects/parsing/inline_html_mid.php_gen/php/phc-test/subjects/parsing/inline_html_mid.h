
#ifndef __GENERATED_php_phc_test_subjects_parsing_inline_html_mid_h__
#define __GENERATED_php_phc_test_subjects_parsing_inline_html_mid_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/inline_html_mid.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$inline_html_mid_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_inline_html_mid_h__
