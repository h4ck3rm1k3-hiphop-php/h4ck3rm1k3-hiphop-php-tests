
#ifndef __GENERATED_php_phc_test_subjects_parsing_const_attribute_fw_h__
#define __GENERATED_php_phc_test_subjects_parsing_const_attribute_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(foo)
extern const int64 q_foo_x;
extern const StaticString q_foo_y;
extern const StaticString q_foo_z;

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_const_attribute_fw_h__
