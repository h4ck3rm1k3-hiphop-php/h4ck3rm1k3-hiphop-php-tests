
#ifndef __GENERATED_php_phc_test_subjects_parsing_line_and_co_h__
#define __GENERATED_php_phc_test_subjects_parsing_line_and_co_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/line_and_co.fw.h>

// Declarations
#include <cls/some_class.h>
#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$line_and_co_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_some_function();
Object co_some_class(CArrRef params, bool init = true);
Object co_a(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_line_and_co_h__
