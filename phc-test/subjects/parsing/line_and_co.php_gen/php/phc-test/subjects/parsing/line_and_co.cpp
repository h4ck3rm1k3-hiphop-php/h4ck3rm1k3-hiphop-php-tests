
#include <php/phc-test/subjects/parsing/line_and_co.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/line_and_co.php line 44 */
Variant c_some_class::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_some_class::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_some_class::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_some_class::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_some_class::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_some_class::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_some_class::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_some_class::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(some_class)
ObjectData *c_some_class::cloneImpl() {
  c_some_class *obj = NEW(c_some_class)();
  cloneSet(obj);
  return obj;
}
void c_some_class::cloneSet(c_some_class *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_some_class::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x10A139CC26AA929FLL, some_method) {
        return (t_some_method(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_some_class::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x10A139CC26AA929FLL, some_method) {
        return (t_some_method(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_some_class::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_some_class$os_get(const char *s) {
  return c_some_class::os_get(s, -1);
}
Variant &cw_some_class$os_lval(const char *s) {
  return c_some_class::os_lval(s, -1);
}
Variant cw_some_class$os_constant(const char *s) {
  return c_some_class::os_constant(s);
}
Variant cw_some_class$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_some_class::os_invoke(c, s, params, -1, fatal);
}
void c_some_class::init() {
}
/* SRC: phc-test/subjects/parsing/line_and_co.php line 46 */
void c_some_class::t_some_method() {
  INSTANCE_METHOD_INJECTION(SOME_CLASS, SOME_CLASS::some_method);
  Variant v_x;

  (v_x = 48);
  (v_x = get_source_filename("phc-test/subjects/parsing/line_and_co.php"));
  echo(toString(v_x) + toString("\n"));
  (v_x = "some_method");
  echo(toString(v_x) + toString("\n"));
  (v_x = "SOME_CLASS::some_method");
  echo(toString(v_x) + toString("\n"));
  (v_x = "SOME_CLASS");
  echo(toString(v_x) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/parsing/line_and_co.php line 26 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x10A139CC26AA929FLL, some_method) {
        return (t_some_method(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x10A139CC26AA929FLL, some_method) {
        return (t_some_method(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: phc-test/subjects/parsing/line_and_co.php line 28 */
void c_a::t_some_method() {
  INSTANCE_METHOD_INJECTION(A, A::some_method);
  Variant v_x;

  (v_x = 30);
  (v_x = get_source_filename("phc-test/subjects/parsing/line_and_co.php"));
  echo(toString(v_x) + toString("\n"));
  (v_x = "some_method");
  echo(toString(v_x) + toString("\n"));
  (v_x = "A::some_method");
  echo(toString(v_x) + toString("\n"));
  (v_x = "A");
  echo(toString(v_x) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/parsing/line_and_co.php line 13 */
void f_some_function() {
  FUNCTION_INJECTION(some_function);
  Variant v_x;

  (v_x = 15);
  (v_x = get_source_filename("phc-test/subjects/parsing/line_and_co.php"));
  echo(toString(v_x) + toString("\n"));
  (v_x = "some_function");
  echo(toString(v_x) + toString("\n"));
  (v_x = "some_function");
  echo(toString(v_x) + toString("\n"));
  (v_x = "");
  echo(toString(v_x) + toString("\n"));
} /* function */
Object co_some_class(CArrRef params, bool init /* = true */) {
  return Object(p_some_class(NEW(c_some_class)())->dynCreate(params, init));
}
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$line_and_co_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/line_and_co.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$line_and_co_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = 2);
  (v_x = get_source_filename("phc-test/subjects/parsing/line_and_co.php"));
  echo(toString(v_x) + toString("\n"));
  (v_x = "");
  echo(toString(v_x) + toString("\n"));
  (v_x = "");
  echo(toString(v_x) + toString("\n"));
  (v_x = "");
  echo(toString(v_x) + toString("\n"));
  (v_x = 61);
  (v_x = get_source_filename("phc-test/subjects/parsing/line_and_co.php"));
  echo(toString(v_x) + toString("\n"));
  (v_x = "");
  echo(toString(v_x) + toString("\n"));
  (v_x = "");
  echo(toString(v_x) + toString("\n"));
  (v_x = "");
  echo(toString(v_x) + toString("\n"));
  LINE(73,f_some_function());
  (v_y = ((Object)(LINE(74,p_some_class(p_some_class(NEWOBJ(c_some_class)())->create())))));
  LINE(75,v_y.o_invoke_few_args("some_method", 0x10A139CC26AA929FLL, 0));
  (v_z = ((Object)(LINE(76,p_a(p_a(NEWOBJ(c_a)())->create())))));
  LINE(77,v_z.o_invoke_few_args("some_method", 0x10A139CC26AA929FLL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
