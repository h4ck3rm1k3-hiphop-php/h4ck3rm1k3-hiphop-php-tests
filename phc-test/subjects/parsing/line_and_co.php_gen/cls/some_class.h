
#ifndef __GENERATED_cls_some_class_h__
#define __GENERATED_cls_some_class_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/line_and_co.php line 44 */
class c_some_class : virtual public ObjectData {
  BEGIN_CLASS_MAP(some_class)
  END_CLASS_MAP(some_class)
  DECLARE_CLASS(some_class, SOME_CLASS, ObjectData)
  void init();
  public: void t_some_method();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_some_class_h__
