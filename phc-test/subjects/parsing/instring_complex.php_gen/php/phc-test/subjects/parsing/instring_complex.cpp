
#include <php/phc-test/subjects/parsing/instring_complex.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$instring_complex_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/instring_complex.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$instring_complex_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = 1LL);
  v_y.set("foo", (2LL), 0x4154FA2EF733DA8FLL);
  v_y.set(3LL, (3LL), 0x135FDDF6A6BFBBDDLL);
  lval(v_y.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(2LL, (4LL), 0x486AFCC090D5F98CLL);
  (v_z = 3LL);
  echo(LINE(8,concat3("a ", toString(v_x), " b\n")));
  echo(LINE(9,concat3("c ", toString(v_y.rvalAt("foo", 0x4154FA2EF733DA8FLL)), " d\n")));
  echo(LINE(10,concat3("e ", toString(v_y.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), " f\n")));
  echo(LINE(11,concat3("g ", toString(v_y.rvalAt(v_z)), " h\n")));
  echo(LINE(12,concat3("i ", toString(v_y.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(2LL, 0x486AFCC090D5F98CLL)), " j\n")));
  echo(toString(v_y.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(2LL, 0x486AFCC090D5F98CLL)) + toString(" k\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
