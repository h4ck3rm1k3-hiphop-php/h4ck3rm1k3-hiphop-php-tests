
#include <php/phc-test/subjects/parsing/allowed_offset_types.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_FOObar = "FOObar";

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/parsing/allowed_offset_types.php line 4 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: phc-test/subjects/parsing/allowed_offset_types.php line 3 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  return 7LL;
} /* function */
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$allowed_offset_types_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/allowed_offset_types.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$allowed_offset_types_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = Array(ArrayInit(1).set(0, ((v_z = 12LL)), 13LL).create()));
  LINE(9,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (df_lambda_1(6LL, v_z)), 14LL).create()));
  LINE(14,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, ((v_z = 5LL)), 15LL).create()));
  LINE(18,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (toByte(!(toBoolean(v_z)))), 16LL).create()));
  LINE(22,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (v_z + v_c), 17LL).create()));
  LINE(26,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (toBoolean(v_z) ? ((Variant)(v_y)) : ((Variant)(v_z))), 18LL).create()));
  LINE(30,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (silenceInc(), silenceDec(v_z)), 19LL).create()));
  LINE(34,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, k_FOObar, 20LL).create()));
  LINE(38,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (toByte(instanceOf(v_z, "int"))), 21LL).create()));
  LINE(42,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (++v_z), 22LL).create()));
  LINE(46,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, (v_z++), 23LL).create()));
  LINE(50,x_var_dump(1, v_x));
  (v_x = Array(ArrayInit(1).set(0, LINE(57,f_f()), 24LL).create()));
  LINE(58,x_var_dump(1, v_x));
  (v_x = ScalarArrays::sa_[0]);
  LINE(70,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
