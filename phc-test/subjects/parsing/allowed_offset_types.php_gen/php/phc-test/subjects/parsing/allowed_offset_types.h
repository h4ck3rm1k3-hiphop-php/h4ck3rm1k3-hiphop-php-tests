
#ifndef __GENERATED_php_phc_test_subjects_parsing_allowed_offset_types_h__
#define __GENERATED_php_phc_test_subjects_parsing_allowed_offset_types_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/allowed_offset_types.fw.h>

// Declarations
#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_f();
Variant pm_php$phc_test$subjects$parsing$allowed_offset_types_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_a(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_allowed_offset_types_h__
