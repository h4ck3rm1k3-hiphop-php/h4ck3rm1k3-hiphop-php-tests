
#include <php/phc-test/subjects/parsing/return.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/return.php line 3 */
PlusOperand f_foo(CVarRef v_x, CVarRef v_y) {
  FUNCTION_INJECTION(foo);
  return v_x + v_y;
} /* function */
/* SRC: phc-test/subjects/parsing/return.php line 13 */
Variant f_fud() {
  FUNCTION_INJECTION(fud);
  Variant v_x;

  return v_x;
} /* function */
/* SRC: phc-test/subjects/parsing/return.php line 8 */
void f_bar() {
  FUNCTION_INJECTION(bar);
  return;
} /* function */
Variant pm_php$phc_test$subjects$parsing$return_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/return.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$return_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
