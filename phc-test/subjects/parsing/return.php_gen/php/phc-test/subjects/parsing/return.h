
#ifndef __GENERATED_php_phc_test_subjects_parsing_return_h__
#define __GENERATED_php_phc_test_subjects_parsing_return_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/return.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$return_php(bool incOnce = false, LVariableTable* variables = NULL);
PlusOperand f_foo(CVarRef v_x, CVarRef v_y);
Variant f_fud();
void f_bar();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_return_h__
