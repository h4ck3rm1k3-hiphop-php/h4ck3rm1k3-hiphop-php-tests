
#include <php/phc-test/subjects/parsing/capital_ticks.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/capital_ticks.php line 3 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_i __attribute__((__unused__)) = g->sv_f_DupIdi;
  bool &inited_sv_i __attribute__((__unused__)) = g->inited_sv_f_DupIdi;
  if (!inited_sv_i) {
    (sv_i = 0LL);
    inited_sv_i = true;
  }
  echo(LINE(6,concat3("tick: ", toString(sv_i), "\n")));
  sv_i++;
} /* function */
Variant i_f(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x286CE1C477560280LL, f) {
    return (f_f(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$parsing$capital_ticks_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/capital_ticks.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$capital_ticks_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  LINE(10,x_register_tick_function(1, "f"));
  {
    {
      {
        LOOP_COUNTER(1);
        for ((v_i = 0LL); less(v_i, 100LL); v_i++) {
          LOOP_COUNTER_CHECK(1);
          {
            echo(LINE(16,concat3("i1: ", toString(v_i), "\n")));
          }
        }
      }
    }
  }
  {
    {
      {
        LOOP_COUNTER(2);
        for ((v_i = 0LL); less(v_i, 100LL); v_i++) {
          LOOP_COUNTER_CHECK(2);
          {
            echo(LINE(25,concat3("i2: ", toString(v_i), "\n")));
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
