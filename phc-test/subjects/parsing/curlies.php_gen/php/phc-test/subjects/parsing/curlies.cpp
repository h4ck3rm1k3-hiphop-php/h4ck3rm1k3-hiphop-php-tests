
#include <php/phc-test/subjects/parsing/curlies.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$curlies_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/curlies.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$curlies_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 2LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, 2LL); v_i++) {
      LOOP_COUNTER_CHECK(2);
      echo(toString(v_i));
    }
  }
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, 2LL); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, 2LL); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        echo(toString(v_i));
      }
    }
  }
  {
    LOOP_COUNTER(5);
    for ((v_i = 0LL); less(v_i, 2LL); v_i++) {
      LOOP_COUNTER_CHECK(5);
      {
        echo(toString(v_i));
        echo("\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
