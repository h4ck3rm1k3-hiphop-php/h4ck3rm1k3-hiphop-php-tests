
#include <php/phc-test/subjects/parsing/binops2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$binops2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/binops2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$binops2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  {
    LOOP_COUNTER(1);
    Variant map2 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_a = iter3->second();
      {
        {
          LOOP_COUNTER(4);
          Variant map5 = ScalarArrays::sa_[0];
          for (ArrayIterPtr iter6 = map5.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_b = iter6->second();
            {
              {
                LOOP_COUNTER(7);
                Variant map8 = ScalarArrays::sa_[0];
                for (ArrayIterPtr iter9 = map8.begin(); !iter9->end(); iter9->next()) {
                  LOOP_COUNTER_CHECK(7);
                  v_c = iter9->second();
                  {
                    {
                      LOOP_COUNTER(10);
                      Variant map11 = ScalarArrays::sa_[0];
                      for (ArrayIterPtr iter12 = map11.begin(); !iter12->end(); iter12->next()) {
                        LOOP_COUNTER_CHECK(10);
                        v_d = iter12->second();
                        {
                          echo(concat((toString(toBoolean(v_a) || toBoolean(v_b) || toBoolean(v_c) || toBoolean(v_d))), "\n"));
                          echo(concat((toString(toBoolean(v_a) || toBoolean(v_b) || toBoolean(v_c) && toBoolean(v_d))), "\n"));
                          echo(concat((toString(toBoolean(v_a) || toBoolean(v_b) && toBoolean(v_c) || toBoolean(v_d))), "\n"));
                          echo(concat((toString(toBoolean(v_a) || toBoolean(v_b) && toBoolean(v_c) && toBoolean(v_d))), "\n"));
                          echo(concat((toString(toBoolean(v_a) && toBoolean(v_b) || toBoolean(v_c) || toBoolean(v_d))), "\n"));
                          echo(concat((toString(toBoolean(v_a) && toBoolean(v_b) || toBoolean(v_c) && toBoolean(v_d))), "\n"));
                          echo(concat((toString(toBoolean(v_a) && toBoolean(v_b) && toBoolean(v_c) || toBoolean(v_d))), "\n"));
                          echo(concat((toString(toBoolean(v_a) && toBoolean(v_b) && toBoolean(v_c) && toBoolean(v_d))), "\n"));
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
