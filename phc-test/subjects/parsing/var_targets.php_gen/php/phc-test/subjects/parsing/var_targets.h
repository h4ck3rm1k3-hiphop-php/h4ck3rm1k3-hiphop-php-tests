
#ifndef __GENERATED_php_phc_test_subjects_parsing_var_targets_h__
#define __GENERATED_php_phc_test_subjects_parsing_var_targets_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/var_targets.fw.h>

// Declarations
#include <cls/fud.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$var_targets_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_foo();
Object co_fud(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_var_targets_h__
