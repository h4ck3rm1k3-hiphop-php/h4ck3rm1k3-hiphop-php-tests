
#include <php/phc-test/subjects/parsing/var_targets.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/var_targets.php line 18 */
Variant c_fud::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_fud::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_fud::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_fud::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_fud::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_fud::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_fud::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_fud::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(fud)
ObjectData *c_fud::cloneImpl() {
  c_fud *obj = NEW(c_fud)();
  cloneSet(obj);
  return obj;
}
void c_fud::cloneSet(c_fud *clone) {
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_fud::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_fud::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fud::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fud$os_get(const char *s) {
  return c_fud::os_get(s, -1);
}
Variant &cw_fud$os_lval(const char *s) {
  return c_fud::os_lval(s, -1);
}
Variant cw_fud$os_constant(const char *s) {
  return c_fud::os_constant(s);
}
Variant cw_fud$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fud::os_invoke(c, s, params, -1, fatal);
}
void c_fud::init() {
  m_x = null;
}
/* SRC: phc-test/subjects/parsing/var_targets.php line 22 */
void c_fud::t_fudbar() {
  INSTANCE_METHOD_INJECTION(Fud, Fud::fudbar);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant v_x;

  (v_x = 800LL);
  v_x = ref(g->GV(x));
  (v_x = 900LL);
} /* function */
/* SRC: phc-test/subjects/parsing/var_targets.php line 5 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant v_x;

  (v_x = 400LL);
  ;
  v_x = ref(g->GV(x));
  (v_x = 600LL);
  ;
} /* function */
Object co_fud(CArrRef params, bool init /* = true */) {
  return Object(p_fud(NEW(c_fud)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$var_targets_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/var_targets.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$var_targets_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = 100LL);
  (v_y = 200LL);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
