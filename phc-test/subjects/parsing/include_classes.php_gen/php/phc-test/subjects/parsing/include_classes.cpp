
#include <php/phc-test/subjects/parsing/include_classes.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$include_classes_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/include_classes.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$include_classes_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  {
    echo("IS \n");
    LINE(8,include("./test/subjects/parsing/included_cc1.php", true, variables, "phc-test/subjects/parsing/"));
    LINE(9,include("./test/subjects/parsing/included_cc2.php", true, variables, "phc-test/subjects/parsing/"));
    echo("IS2 \n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
