
#include <php/phc-test/subjects/parsing/included_recursive2.h>
#include <php/phc-test/subjects/parsing/included_recursive3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$included_recursive2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/included_recursive2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$included_recursive2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);

  echo(toString(v_f));
  (v_f = 8LL);
  echo(toString(v_f));
  LINE(6,pm_php$phc_test$subjects$parsing$included_recursive3_php(false, variables));
  echo(toString(v_f));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
