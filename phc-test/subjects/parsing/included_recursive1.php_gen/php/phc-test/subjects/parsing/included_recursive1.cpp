
#include <php/phc-test/subjects/parsing/included_recursive1.h>
#include <php/phc-test/subjects/parsing/included_recursive2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$included_recursive1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/included_recursive1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$included_recursive1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);

  echo(toString(v_f));
  (v_f = 6LL);
  echo(toString(v_f));
  LINE(6,pm_php$phc_test$subjects$parsing$included_recursive2_php(false, variables));
  echo(toString(v_f));
  echo((LINE(9,x_get_include_path())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
