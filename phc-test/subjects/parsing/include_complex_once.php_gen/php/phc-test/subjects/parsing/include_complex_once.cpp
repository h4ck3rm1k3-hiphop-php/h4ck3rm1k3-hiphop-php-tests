
#include <php/phc-test/subjects/horrible/invocation_priorities.h>
#include <php/phc-test/subjects/parsing/include_complex_once.h>
#include <php/phc-test/subjects/parsing/included_classes_and_functions.h>
#include <php/phc-test/subjects/parsing/included_classes_with_return_values.h>
#include <php/phc-test/subjects/parsing/included_recursive1.h>
#include <php/phc-test/subjects/parsing/included_use_existing_vars.h>
#include <php/phc-test/subjects/parsing/included_use_vars_in_classes.h>
#include <php/phc-test/subjects/parsing/included_use_vars_in_function.h>
#include <php/phc-test/subjects/parsing/included_var_overwrite.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/include_complex_once.php line 187 */
Variant c_h::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_h::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_h::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_h::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_h::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_h::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_h::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_h::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(h)
ObjectData *c_h::create() {
  init();
  t_h();
  return this;
}
ObjectData *c_h::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_h::cloneImpl() {
  c_h *obj = NEW(c_h)();
  cloneSet(obj);
  return obj;
}
void c_h::cloneSet(c_h *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_h::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x596FD6C55E8464CCLL, g) {
        return (t_g(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_h::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x596FD6C55E8464CCLL, g) {
        return (t_g(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_h::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_h$os_get(const char *s) {
  return c_h::os_get(s, -1);
}
Variant &cw_h$os_lval(const char *s) {
  return c_h::os_lval(s, -1);
}
Variant cw_h$os_constant(const char *s) {
  return c_h::os_constant(s);
}
Variant cw_h$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_h::os_invoke(c, s, params, -1, fatal);
}
void c_h::init() {
}
/* SRC: phc-test/subjects/parsing/include_complex_once.php line 189 */
void c_h::t_h() {
  INSTANCE_METHOD_INJECTION(H, H::H);
  bool oldInCtor = gasInCtor(true);
  echo("H\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/parsing/include_complex_once.php line 194 */
void c_h::t_g() {
  INSTANCE_METHOD_INJECTION(H, H::g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a1;
  Variant v_a2;
  Variant v_result;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_a1; Variant &v_a2; Variant &v_result;
    VariableTable(Variant &r_a1, Variant &r_a2, Variant &r_result) : v_a1(r_a1), v_a2(r_a2), v_result(r_result) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 7) {
        case 0:
          HASH_RETURN(0x599E3F67E2599248LL, v_result,
                      result);
          break;
        case 6:
          HASH_RETURN(0x6C05C2858C72A876LL, v_a2,
                      a2);
          break;
        case 7:
          HASH_RETURN(0x68DF81F26D942FC7LL, v_a1,
                      a1);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_a1, v_a2, v_result);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_a1 = "old string");
  (v_a2 = "old string");
  echo("about to include included_var_overwrite.php\n");
  (v_result = LINE(200,pm_php$phc_test$subjects$parsing$included_var_overwrite_php(true, variables)));
  LINE(201,x_var_dump(1, v_result));
  if ((equal(v_a1, "old value")) || (equal(v_a2, "my old value"))) {
    LINE(205,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 205, "var overwrite within class"));
  }
} /* function */
/* SRC: phc-test/subjects/parsing/include_complex_once.php line 166 */
void f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a1;
  Variant v_a2;
  Variant v_result;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_a1; Variant &v_a2; Variant &v_result;
    VariableTable(Variant &r_a1, Variant &r_a2, Variant &r_result) : v_a1(r_a1), v_a2(r_a2), v_result(r_result) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 7) {
        case 0:
          HASH_RETURN(0x599E3F67E2599248LL, v_result,
                      result);
          break;
        case 6:
          HASH_RETURN(0x6C05C2858C72A876LL, v_a2,
                      a2);
          break;
        case 7:
          HASH_RETURN(0x68DF81F26D942FC7LL, v_a1,
                      a1);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_a1, v_a2, v_result);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_a1 = "old string");
  (v_a2 = "old string");
  echo("about to include included_var_overwrite.php\n");
  (v_result = LINE(172,pm_php$phc_test$subjects$parsing$included_var_overwrite_php(false, variables)));
  LINE(173,x_var_dump(1, v_result));
  LINE(175,x_var_dump(1, v_a1));
  LINE(176,x_var_dump(1, v_a2));
  if ((equal(v_a1, "old value")) || (equal(v_a2, "my old value"))) {
    LINE(180,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 180, "var overwrite within function"));
  }
} /* function */
/* SRC: phc-test/subjects/parsing/include_complex_once.php line 29 */
void f_fail(CStrRef v_file, int v_line, CStrRef v_reason) {
  FUNCTION_INJECTION(fail);
  print(concat("Failure: '", LINE(31,concat6(v_reason, "' on ", v_file, ":", toString(v_line), "\n"))));
} /* function */
Object co_h(CArrRef params, bool init /* = true */) {
  return Object(p_h(NEW(c_h)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$include_complex_once_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/include_complex_once.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$include_complex_once_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_xx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xx") : g->GV(xx);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_i1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i1") : g->GV(i1);
  Variant &v_i2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i2") : g->GV(i2);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  (v_a1 = "old value");
  (v_a2 = "my old value");
  echo("about to include included_var_overwrite.php\n");
  (v_result = LINE(41,pm_php$phc_test$subjects$parsing$included_var_overwrite_php(true, variables)));
  LINE(42,x_var_dump(1, v_result));
  LINE(44,x_var_dump(1, v_a1));
  LINE(45,x_var_dump(1, v_a2));
  if ((equal(v_a1, "old value")) || (equal(v_a2, "my old value"))) {
    LINE(49,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 49, "var overwrite"));
  }
  echo("about to include included_classes_and_functions.php\n");
  (v_result = LINE(58,pm_php$phc_test$subjects$parsing$included_classes_and_functions_php(true, variables)));
  LINE(59,x_var_dump(1, v_result));
  (v_b = ((Object)(LINE(61,p_b(p_b(NEWOBJ(c_b)())->create())))));
  LINE(62,x_var_dump(1, v_b));
  (v_b = LINE(63,v_b.o_invoke_few_args("b", 0x39E9518192E6D2AELL, 0)));
  LINE(64,x_var_dump(1, v_b));
  if (!(toBoolean(v_b))) {
    LINE(67,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 67, "included class"));
  }
  (v_b = LINE(69,f_b()));
  LINE(70,x_var_dump(1, v_b));
  if (!(toBoolean(v_b))) {
    LINE(73,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 73, "included function"));
  }
  echo("about to include included_classes_with_return_values.php\n");
  (v_result = LINE(83,pm_php$phc_test$subjects$parsing$included_classes_with_return_values_php(true, variables)));
  LINE(84,x_var_dump(1, v_result));
  (v_c = ((Object)(LINE(86,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(87,x_var_dump(1, v_c));
  (v_c = LINE(88,v_c.o_invoke_few_args("cc", 0x28E4E7211D305D44LL, 0)));
  LINE(89,x_var_dump(1, v_c));
  if (!equal(v_c, 7LL)) {
    LINE(92,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 92, "class with return value"));
  }
  echo("about to include include_dir/../../bugs/././../horrible/invocation_priorities.php\n");
  (v_result = LINE(100,pm_php$phc_test$subjects$horrible$invocation_priorities_php(true, variables)));
  LINE(101,x_var_dump(1, v_result));
  LINE(103,x_var_dump(1, v_f));
  LINE(104,x_var_dump(1, v_a));
  LINE(105,x_var_dump(1, v_x));
  if (!(toBoolean(v_x))) {
    LINE(108,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 108, "directory levels"));
  }
  (v_x = "zzz");
  (v_y = "zzz");
  (v_xx = "zzz");
  echo("about to include ./test/subjects/horrible/obfuscated_foreach.php\n");
  (v_result = LINE(118,include("./test/subjects/horrible/obfuscated_foreach.php", true, variables, "phc-test/subjects/parsing/")));
  LINE(119,x_var_dump(1, v_result));
  LINE(121,x_var_dump(1, v_x));
  LINE(122,x_var_dump(1, v_y));
  LINE(123,x_var_dump(1, v_xx));
  if (!(toBoolean(v_xx))) {
    LINE(126,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 126, "current working directory"));
  }
  echo("about to include test/subjects/horrible/obfuscated_foreach.php\n");
  (v_result = LINE(134,include("test/subjects/horrible/obfuscated_foreach.php", true, variables, "phc-test/subjects/parsing/")));
  LINE(135,x_var_dump(1, v_result));
  LINE(137,x_var_dump(1, v_x));
  LINE(138,x_var_dump(1, v_y));
  LINE(139,x_var_dump(1, v_xx));
  if (!(toBoolean(v_xx))) {
    LINE(142,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 142, "current working directory"));
  }
  echo("about to include included_recursive1.php\n");
  (v_result = LINE(154,pm_php$phc_test$subjects$parsing$included_recursive1_php(true, variables)));
  LINE(155,x_var_dump(1, v_result));
  LINE(157,x_var_dump(1, v_f));
  if (!same(v_f, 26LL)) {
    LINE(160,f_fail(get_source_filename("phc-test/subjects/parsing/include_complex_once.php"), 160, "recursive include"));
  }
  LINE(183,f_g());
  (v_h = ((Object)(LINE(209,p_h(p_h(NEWOBJ(c_h)())->create())))));
  LINE(210,x_var_dump(1, v_h));
  (v_h = LINE(211,v_h.o_invoke_few_args("g", 0x596FD6C55E8464CCLL, 0)));
  LINE(212,x_var_dump(1, v_h));
  (v_i1 = "some value");
  (v_i2 = "another value");
  echo("about to include included_use_existing_vars.php\n");
  (v_result = LINE(220,pm_php$phc_test$subjects$parsing$included_use_existing_vars_php(true, variables)));
  LINE(221,x_var_dump(1, v_result));
  echo("about to include included_use_vars_in_function.php\n");
  (v_result = LINE(226,pm_php$phc_test$subjects$parsing$included_use_vars_in_function_php(true, variables)));
  LINE(227,x_var_dump(1, v_result));
  (v_j = LINE(229,f_j(1LL, 2LL)));
  LINE(230,x_var_dump(1, v_j));
  echo("about to include included_use_vars_in_classes.php\n");
  (v_result = LINE(235,pm_php$phc_test$subjects$parsing$included_use_vars_in_classes_php(true, variables)));
  LINE(236,x_var_dump(1, v_result));
  (v_k = ((Object)(LINE(238,p_k(p_k(NEWOBJ(c_k)())->create())))));
  LINE(239,x_var_dump(1, v_k));
  (v_k = LINE(240,v_k.o_invoke_few_args("kk", 0x75BE8B140F69E8ECLL, 0)));
  LINE(241,x_var_dump(1, v_k));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
