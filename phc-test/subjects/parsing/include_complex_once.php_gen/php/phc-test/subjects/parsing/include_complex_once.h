
#ifndef __GENERATED_php_phc_test_subjects_parsing_include_complex_once_h__
#define __GENERATED_php_phc_test_subjects_parsing_include_complex_once_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/include_complex_once.fw.h>

// Declarations
#include <cls/h.h>
#include <php/phc-test/subjects/parsing/included_classes_and_functions.h>
#include <php/phc-test/subjects/parsing/included_classes_with_return_values.h>
#include <php/phc-test/subjects/parsing/included_use_vars_in_classes.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$include_complex_once_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_g();
void f_fail(CStrRef v_file, int v_line, CStrRef v_reason);
Object co_h(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_include_complex_once_h__
