
#include <php/phc-test/subjects/parsing/include_complex_once.h>
#include <php/phc-test/subjects/parsing/included_use_vars_in_classes.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/included_use_vars_in_classes.php line 3 */
Variant c_k::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_k::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_k::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("my_var", m_my_var));
  c_ObjectData::o_get(props);
}
bool c_k::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x5B20DC6987564DA7LL, my_var, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_k::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x5B20DC6987564DA7LL, m_my_var,
                         my_var, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_k::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x5B20DC6987564DA7LL, m_my_var,
                      my_var, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_k::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_k::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(k)
ObjectData *c_k::create() {
  init();
  t_k();
  return this;
}
ObjectData *c_k::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_k::cloneImpl() {
  c_k *obj = NEW(c_k)();
  cloneSet(obj);
  return obj;
}
void c_k::cloneSet(c_k *clone) {
  clone->m_my_var = m_my_var;
  ObjectData::cloneSet(clone);
}
Variant c_k::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x75BE8B140F69E8ECLL, kk) {
        return (t_kk());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_k::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x75BE8B140F69E8ECLL, kk) {
        return (t_kk());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_k::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_k$os_get(const char *s) {
  return c_k::os_get(s, -1);
}
Variant &cw_k$os_lval(const char *s) {
  return c_k::os_lval(s, -1);
}
Variant cw_k$os_constant(const char *s) {
  return c_k::os_constant(s);
}
Variant cw_k$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_k::os_invoke(c, s, params, -1, fatal);
}
void c_k::init() {
  m_my_var = null;
}
/* SRC: phc-test/subjects/parsing/included_use_vars_in_classes.php line 7 */
void c_k::t_k() {
  INSTANCE_METHOD_INJECTION(K, K::K);
  bool oldInCtor = gasInCtor(true);
  ;
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/parsing/included_use_vars_in_classes.php line 12 */
Variant c_k::t_kk() {
  INSTANCE_METHOD_INJECTION(K, K::kk);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_i1 __attribute__((__unused__)) = g->GV(i1);
  Variant &gv_i2 __attribute__((__unused__)) = g->GV(i2);
  Variant v_my_var;

  LINE(17,x_var_dump(1, gv_i1));
  LINE(18,x_var_dump(1, gv_i2));
  if ((!equal(gv_i1, "some value")) && (!equal(gv_i2, "another value"))) {
    LINE(21,f_fail(get_source_filename("phc-test/subjects/parsing/included_use_vars_in_classes.php"), 21, "using variables in included class"));
  }
  return v_my_var;
} /* function */
Object co_k(CArrRef params, bool init /* = true */) {
  return Object(p_k(NEW(c_k)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$included_use_vars_in_classes_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/included_use_vars_in_classes.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$included_use_vars_in_classes_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
