
#include <php/phc-test/subjects/parsing/include_complex_once.h>
#include <php/phc-test/subjects/parsing/included_use_existing_vars.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$parsing$included_use_existing_vars_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/included_use_existing_vars.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$included_use_existing_vars_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i1") : g->GV(i1);
  Variant &v_i2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i2") : g->GV(i2);

  LINE(3,x_var_dump(1, v_i1));
  LINE(4,x_var_dump(1, v_i2));
  if ((!same(v_i1, "some value")) && (!same(v_i2, "another value"))) {
    LINE(7,f_fail(get_source_filename("phc-test/subjects/parsing/included_use_existing_vars.php"), 7, "using variables inncluded file"));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
