
#ifndef __GENERATED_php_phc_test_subjects_parsing_included_use_existing_vars_h__
#define __GENERATED_php_phc_test_subjects_parsing_included_use_existing_vars_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/included_use_existing_vars.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$included_use_existing_vars_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_included_use_existing_vars_h__
