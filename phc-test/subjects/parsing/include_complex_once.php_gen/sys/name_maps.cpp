
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "b", "phc-test/subjects/parsing/included_classes_and_functions.php",
  "c", "phc-test/subjects/parsing/included_classes_with_return_values.php",
  "h", "phc-test/subjects/parsing/include_complex_once.php",
  "k", "phc-test/subjects/parsing/included_use_vars_in_classes.php",
  "x", "phc-test/subjects/horrible/invocation_priorities.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "b", "phc-test/subjects/parsing/included_classes_and_functions.php",
  "fail", "phc-test/subjects/parsing/include_complex_once.php",
  "foo", "phc-test/subjects/horrible/invocation_priorities.php",
  "g", "phc-test/subjects/parsing/include_complex_once.php",
  "j", "phc-test/subjects/parsing/included_use_vars_in_function.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
