
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x4B27011F259D2446LL, j, 25);
      break;
    case 7:
      HASH_INDEX(0x68DF81F26D942FC7LL, a1, 12);
      break;
    case 8:
      HASH_INDEX(0x599E3F67E2599248LL, result, 19);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 17);
      HASH_INDEX(0x4F56B733A4DFC78ALL, y, 22);
      break;
    case 11:
      HASH_INDEX(0x6014C6271D255F8BLL, i2, 16);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x0D6857FDDD7F21CFLL, k, 26);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 20);
      break;
    case 22:
      HASH_INDEX(0x04BFC205E59FA416LL, x, 18);
      break;
    case 31:
      HASH_INDEX(0x6DA23E9B91A14D9FLL, xx, 23);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 40:
      HASH_INDEX(0x608D572C3F34DB68LL, i1, 15);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      HASH_INDEX(0x6BB4A0689FBAD42ELL, f, 14);
      break;
    case 48:
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 21);
      HASH_INDEX(0x695875365480A8F0LL, h, 24);
      break;
    case 54:
      HASH_INDEX(0x6C05C2858C72A876LL, a2, 13);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 27) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
