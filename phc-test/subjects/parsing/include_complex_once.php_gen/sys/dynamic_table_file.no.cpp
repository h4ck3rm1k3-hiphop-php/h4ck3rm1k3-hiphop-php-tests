
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$parsing$include_complex_once_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_var_overwrite_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_classes_and_functions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_classes_with_return_values_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$horrible$invocation_priorities_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_recursive1_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_use_existing_vars_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_use_vars_in_function_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_use_vars_in_classes_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_recursive2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$included_recursive3_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 3:
      HASH_INCLUDE(0x3970FFD9F498F363LL, "phc-test/subjects/parsing/included_var_overwrite.php", php$phc_test$subjects$parsing$included_var_overwrite_php);
      break;
    case 4:
      HASH_INCLUDE(0x5793F1A8952575E4LL, "phc-test/subjects/parsing/include_complex_once.php", php$phc_test$subjects$parsing$include_complex_once_php);
      break;
    case 9:
      HASH_INCLUDE(0x47A749FBDB60A689LL, "phc-test/subjects/parsing/included_use_existing_vars.php", php$phc_test$subjects$parsing$included_use_existing_vars_php);
      break;
    case 11:
      HASH_INCLUDE(0x71802034ED58B62BLL, "phc-test/subjects/parsing/included_classes_with_return_values.php", php$phc_test$subjects$parsing$included_classes_with_return_values_php);
      break;
    case 14:
      HASH_INCLUDE(0x584155500E7EA4EELL, "phc-test/subjects/parsing/included_recursive1.php", php$phc_test$subjects$parsing$included_recursive1_php);
      break;
    case 17:
      HASH_INCLUDE(0x548AA19BFEB13ED1LL, "phc-test/subjects/horrible/invocation_priorities.php", php$phc_test$subjects$horrible$invocation_priorities_php);
      HASH_INCLUDE(0x00908F2B684E9F31LL, "phc-test/subjects/parsing/included_recursive2.php", php$phc_test$subjects$parsing$included_recursive2_php);
      break;
    case 21:
      HASH_INCLUDE(0x3E89A2245C86E415LL, "phc-test/subjects/parsing/included_classes_and_functions.php", php$phc_test$subjects$parsing$included_classes_and_functions_php);
      break;
    case 23:
      HASH_INCLUDE(0x2CDBC677626D4E57LL, "phc-test/subjects/parsing/included_use_vars_in_classes.php", php$phc_test$subjects$parsing$included_use_vars_in_classes_php);
      break;
    case 26:
      HASH_INCLUDE(0x455D0DB4018420BALL, "phc-test/subjects/parsing/included_use_vars_in_function.php", php$phc_test$subjects$parsing$included_use_vars_in_function_php);
      break;
    case 29:
      HASH_INCLUDE(0x5ADC6EB193B2E85DLL, "phc-test/subjects/parsing/included_recursive3.php", php$phc_test$subjects$parsing$included_recursive3_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
