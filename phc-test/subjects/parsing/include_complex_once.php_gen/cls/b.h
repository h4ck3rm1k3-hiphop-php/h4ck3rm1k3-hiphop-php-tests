
#ifndef __GENERATED_cls_b_h__
#define __GENERATED_cls_b_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/included_classes_and_functions.php line 3 */
class c_b : virtual public ObjectData {
  BEGIN_CLASS_MAP(b)
  END_CLASS_MAP(b)
  DECLARE_CLASS(b, B, ObjectData)
  void init();
  public: bool t_b();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_b_h__
