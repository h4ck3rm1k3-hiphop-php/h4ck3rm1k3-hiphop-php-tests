
#include <php/phc-test/subjects/parsing/global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/global.php line 6 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  DECLARE_GLOBAL_VARIABLES(g);
  String v_y;
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant v_x;


  class VariableTable : public RVariableTable {
  public:
    String &v_y; Variant &v_x;
    VariableTable(String &r_y, Variant &r_x) : v_y(r_y), v_x(r_x) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x4F56B733A4DFC78ALL, v_y,
                      y);
          HASH_RETURN(0x04BFC205E59FA416LL, v_x,
                      x);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_y, v_x);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_y = "y");
  throw_fatal("dynamic global");
} /* function */
Variant pm_php$phc_test$subjects$parsing$global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = 10LL);
  (v_y = 20LL);
  LINE(14,f_foo());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
