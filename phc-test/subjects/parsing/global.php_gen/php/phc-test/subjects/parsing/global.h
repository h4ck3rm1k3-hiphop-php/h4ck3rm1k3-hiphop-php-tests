
#ifndef __GENERATED_php_phc_test_subjects_parsing_global_h__
#define __GENERATED_php_phc_test_subjects_parsing_global_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/global.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foo();
Variant pm_php$phc_test$subjects$parsing$global_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_global_h__
