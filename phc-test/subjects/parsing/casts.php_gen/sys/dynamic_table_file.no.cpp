
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$parsing$casts_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$parsing$scalar_array_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_INCLUDE(0x226B827BFE05BC05LL, "phc-test/subjects/parsing/casts.php", php$phc_test$subjects$parsing$casts_php);
      break;
    case 2:
      HASH_INCLUDE(0x35300762C8EDD662LL, "phc-test/subjects/parsing/scalar_array.php", php$phc_test$subjects$parsing$scalar_array_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
