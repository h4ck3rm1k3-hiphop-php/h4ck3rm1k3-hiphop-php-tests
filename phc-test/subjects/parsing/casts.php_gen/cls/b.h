
#ifndef __GENERATED_cls_b_h__
#define __GENERATED_cls_b_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/parsing/casts.php line 16 */
class c_b : virtual public ObjectData {
  BEGIN_CLASS_MAP(b)
  END_CLASS_MAP(b)
  DECLARE_CLASS(b, B, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_b_h__
