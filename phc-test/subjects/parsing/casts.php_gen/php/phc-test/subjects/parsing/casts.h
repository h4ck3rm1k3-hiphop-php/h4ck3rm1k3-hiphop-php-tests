
#ifndef __GENERATED_php_phc_test_subjects_parsing_casts_h__
#define __GENERATED_php_phc_test_subjects_parsing_casts_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/parsing/casts.fw.h>

// Declarations
#include <cls/a.h>
#include <cls/b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$parsing$casts_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_a(CArrRef params, bool init = true);
Object co_b(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_parsing_casts_h__
