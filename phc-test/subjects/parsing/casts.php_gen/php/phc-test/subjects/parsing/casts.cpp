
#include <php/phc-test/subjects/parsing/casts.h>
#include <php/phc-test/subjects/parsing/scalar_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/parsing/casts.php line 5 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: phc-test/subjects/parsing/casts.php line 9 */
String c_a::t___tostring() {
  INSTANCE_METHOD_INJECTION(A, A::__toString);
  return "a";
} /* function */
/* SRC: phc-test/subjects/parsing/casts.php line 16 */
Variant c_b::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_b::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
}
/* SRC: phc-test/subjects/parsing/casts.php line 18 */
void c_b::t___construct() {
  INSTANCE_METHOD_INJECTION(B, B::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("f", 0x6BB4A0689FBAD42ELL) = ((Object)(LINE(20,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (lval(o_lval("f", 0x6BB4A0689FBAD42ELL)).o_lval("g", 0x2BCCE9AD79BC2688LL) = ((Object)(LINE(21,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (lval(lval(o_lval("f", 0x6BB4A0689FBAD42ELL)).o_lval("g", 0x2BCCE9AD79BC2688LL)).o_lval("h", 0x695875365480A8F0LL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$parsing$casts_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/parsing/casts.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$parsing$casts_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scalar_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scalar_array") : g->GV(scalar_array);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  LINE(3,pm_php$phc_test$subjects$parsing$scalar_array_php(false, variables));
  v_scalar_array.append((((Object)(LINE(26,p_a(p_a(NEWOBJ(c_a)())->create()))))));
  v_scalar_array.append((((Object)(LINE(27,p_b(p_b(NEWOBJ(c_b)())->create()))))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_scalar_array.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_y = iter3->second();
      {
        print("Converting: ");
        LINE(32,x_var_dump(1, v_y));
        print("Converting to an int: ");
        (v_x = toInt64(v_y));
        LINE(36,x_var_dump(1, v_x));
        print("Converting to an integer: ");
        (v_x = toInt64(v_y));
        LINE(40,x_var_dump(1, v_x));
        print("Converting to an float: ");
        (v_x = toDouble(v_y));
        LINE(44,x_var_dump(1, v_x));
        print("Converting to an real: ");
        (v_x = toDouble(v_y));
        LINE(48,x_var_dump(1, v_x));
        print("Converting to an double: ");
        (v_x = toDouble(v_y));
        LINE(52,x_var_dump(1, v_x));
        print("Converting to an string: ");
        (v_x = toString(v_y));
        LINE(56,x_var_dump(1, v_x));
        print("Converting to an array: ");
        (v_x = toArray(v_y));
        LINE(60,x_var_dump(1, v_x));
        print("Converting to an object: ");
        (v_x = toObject(v_y));
        LINE(64,x_var_dump(1, v_x));
        print("Converting to an bool: ");
        (v_x = toBoolean(v_y));
        LINE(68,x_var_dump(1, v_x));
        print("Converting to an boolean: ");
        (v_x = toBoolean(v_y));
        LINE(72,x_var_dump(1, v_x));
        print("Converting to an unset: ");
        (v_x = unset(v_y));
        LINE(76,x_var_dump(1, v_x));
        print("Converting to an BOOLean: ");
        (v_x = toBoolean(v_y));
        LINE(82,x_var_dump(1, v_x));
        (v_x = toString(v_y));
        LINE(85,x_var_dump(1, v_x));
        (v_x = toString(v_y));
        LINE(88,x_var_dump(1, v_x));
        print("\n\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
