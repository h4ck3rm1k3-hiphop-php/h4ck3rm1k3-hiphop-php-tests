
#ifndef __GENERATED_php_phc_test_subjects_codegen_no_ashred_rvalue_h__
#define __GENERATED_php_phc_test_subjects_codegen_no_ashred_rvalue_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/no_ashred_rvalue.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_x();
Variant pm_php$phc_test$subjects$codegen$no_ashred_rvalue_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_no_ashred_rvalue_h__
