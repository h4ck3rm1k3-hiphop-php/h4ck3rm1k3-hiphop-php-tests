
#include <php/phc-test/subjects/codegen/no_ashred_rvalue.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/no_ashred_rvalue.php line 22 */
int64 f_x() {
  FUNCTION_INJECTION(x);
  return 5LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$no_ashred_rvalue_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/no_ashred_rvalue.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$no_ashred_rvalue_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  v_x.set(1LL, (2LL), 0x5BCA7C69B794F8CELL);
  (v_y = v_x.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL));
  (variables->get(toString(v_x)) = 4LL);
  v_x.weakRemove(5LL, 0x350AEB726A15D700LL);
  ;
  (v_a = 7LL + v_b);
  (v_c = v_b + 8LL);
  LINE(18,x_var_dump(1, 9LL));
  LINE(20,x_var_dump(4, v_x, Array(ArrayInit(3).set(0, v_y).set(1, v_a).set(2, v_c).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
