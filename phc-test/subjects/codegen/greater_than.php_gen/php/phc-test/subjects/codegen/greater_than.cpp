
#include <php/phc-test/subjects/codegen/greater_than.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/greater_than.php line 5 */
void f_f() {
  FUNCTION_INJECTION(f);
  echo("F called\n");
} /* function */
/* SRC: phc-test/subjects/codegen/greater_than.php line 6 */
void f_g() {
  FUNCTION_INJECTION(g);
  echo("G called\n");
} /* function */
Variant pm_php$phc_test$subjects$codegen$greater_than_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/greater_than.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$greater_than_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  if (more_rev((LINE(8,f_g()), null), (f_f(), null))) print("F is greater\n");
  else print("G is greater\n");
  if (not_less_rev((LINE(11,f_f()), null), (f_g(), null))) print("G is greater or equal\n");
  else print("F is greater\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
