
#include <php/phc-test/subjects/codegen/bench_hash1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_hash1.php line 3 */
void f_hash1(int64 v_n) {
  FUNCTION_INJECTION(hash1);
  int64 v_i = 0;
  Sequence v_X;
  int64 v_c = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); not_more(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_X.set(LINE(5,x_dechex(v_i)), (v_i));
      }
    }
  }
  (v_c = 0LL);
  {
    LOOP_COUNTER(2);
    for ((v_i = v_n); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(2);
      {
        if (toBoolean(v_X.rvalAt(LINE(9,x_dechex(v_i))))) {
          v_c++;
        }
      }
    }
  }
  print(toString(v_c) + toString("\n"));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_hash1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_hash1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_hash1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(14,f_hash1(5LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
