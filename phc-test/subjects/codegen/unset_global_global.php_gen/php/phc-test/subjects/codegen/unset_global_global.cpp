
#include <php/phc-test/subjects/codegen/unset_global_global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/unset_global_global.php line 19 */
void f_set_global_y1() {
  FUNCTION_INJECTION(set_global_y1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_y1 __attribute__((__unused__)) = g->GV(y1);
  (gv_y1 = 30LL);
} /* function */
/* SRC: phc-test/subjects/codegen/unset_global_global.php line 25 */
void f_set_global_y2() {
  FUNCTION_INJECTION(set_global_y2);
  DECLARE_GLOBAL_VARIABLES(g);
  (g->GV(y2) = 40LL);
} /* function */
/* SRC: phc-test/subjects/codegen/unset_global_global.php line 8 */
void f_show_global_x1() {
  FUNCTION_INJECTION(show_global_x1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x1 __attribute__((__unused__)) = g->GV(x1);
  echo(LINE(11,concat3("x1: ", toString(gv_x1), "\n")));
} /* function */
/* SRC: phc-test/subjects/codegen/unset_global_global.php line 14 */
void f_show_global_x2() {
  FUNCTION_INJECTION(show_global_x2);
  DECLARE_GLOBAL_VARIABLES(g);
  echo(LINE(16,concat3("x2: ", toString(g->GV(x2)), "\n")));
} /* function */
Variant pm_php$phc_test$subjects$codegen$unset_global_global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/unset_global_global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$unset_global_global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x1") : g->GV(x1);
  Variant &v_x2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x2") : g->GV(x2);
  Variant &v_y1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y1") : g->GV(y1);
  Variant &v_y2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y2") : g->GV(y2);

  (v_x1 = 10LL);
  (v_x2 = 20LL);
  unset(v_y1);
  unset(v_y2);
  LINE(36,f_show_global_x1());
  LINE(37,f_show_global_x2());
  LINE(38,f_set_global_y1());
  LINE(39,f_set_global_y2());
  echo(LINE(40,concat3("y1: ", toString(v_y1), "\n")));
  echo(LINE(41,concat3("y2: ", toString(v_y2), "\n")));
  unset(get_global_array_wrapper());
  (v_x1 = 10LL);
  (v_x2 = 20LL);
  unset(v_y1);
  unset(v_y2);
  LINE(52,f_show_global_x1());
  LINE(53,f_show_global_x2());
  LINE(54,f_set_global_y1());
  LINE(55,f_set_global_y2());
  echo(LINE(56,concat3("y1: ", toString(v_y1), "\n")));
  echo(LINE(57,concat3("y2: ", toString(v_y2), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
