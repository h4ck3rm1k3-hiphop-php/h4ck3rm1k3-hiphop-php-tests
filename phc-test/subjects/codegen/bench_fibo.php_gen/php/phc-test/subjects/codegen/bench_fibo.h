
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_fibo_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_fibo_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_fibo.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_fibo_r(Numeric v_n);
void f_fibo(int64 v_n);
Variant pm_php$phc_test$subjects$codegen$bench_fibo_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_fibo_h__
