
#include <php/phc-test/subjects/codegen/bench_fibo.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_fibo.php line 3 */
Variant f_fibo_r(Numeric v_n) {
  FUNCTION_INJECTION(fibo_r);
  return ((less(v_n, 2LL)) ? ((Variant)(1LL)) : ((Variant)(plus_rev(LINE(4,f_fibo_r(v_n - 1LL)), f_fibo_r(v_n - 2LL)))));
} /* function */
/* SRC: phc-test/subjects/codegen/bench_fibo.php line 7 */
void f_fibo(int64 v_n) {
  FUNCTION_INJECTION(fibo);
  Variant v_r;

  (v_r = LINE(8,f_fibo_r(v_n)));
  print(toString(v_r) + toString("\n"));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_fibo_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_fibo.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_fibo_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(13,f_fibo(4LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
