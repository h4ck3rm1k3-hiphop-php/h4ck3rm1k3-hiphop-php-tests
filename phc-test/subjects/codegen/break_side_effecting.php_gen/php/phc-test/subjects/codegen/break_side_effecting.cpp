
#include <php/phc-test/subjects/codegen/break_side_effecting.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/break_side_effecting.php line 3 */
int64 f_x() {
  FUNCTION_INJECTION(x);
  echo("x\n");
  return 7LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$break_side_effecting_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/break_side_effecting.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$break_side_effecting_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 5LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        if ((LINE(11,f_x()))<2) {
        goto break1;
        } else {
        throw_fatal("bad break");
        }
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
