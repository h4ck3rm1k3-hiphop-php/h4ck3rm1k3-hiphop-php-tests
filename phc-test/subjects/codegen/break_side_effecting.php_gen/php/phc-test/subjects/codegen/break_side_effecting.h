
#ifndef __GENERATED_php_phc_test_subjects_codegen_break_side_effecting_h__
#define __GENERATED_php_phc_test_subjects_codegen_break_side_effecting_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/break_side_effecting.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$break_side_effecting_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_x();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_break_side_effecting_h__
