
#include <php/phc-test/subjects/codegen/switch_more_than_once2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/switch_more_than_once2.php line 3 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("f called\n");
  return 8LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$switch_more_than_once2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_more_than_once2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_more_than_once2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  {
    int64 tmp2 = (7LL);
    int tmp3 = -1;
    if (equal(tmp2, (8LL))) {
      tmp3 = 1;
    } else if (equal(tmp2, (8LL))) {
      tmp3 = 2;
    } else if (equal(tmp2, (LINE(17,f_f())))) {
      tmp3 = 3;
    } else if (equal(tmp2, (8LL))) {
      tmp3 = 4;
    } else if (true) {
      tmp3 = 0;
    }
    switch (tmp3) {
    case 0:
      {
        echo("def\n");
      }
    case 1:
      {
        echo("8 called1\n");
      }
    case 2:
      {
        echo("8 called2\n");
      }
    case 3:
      {
        echo("8 called3\n");
        goto break1;
      }
    case 4:
      {
        echo("8 called4\n");
        goto break1;
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
