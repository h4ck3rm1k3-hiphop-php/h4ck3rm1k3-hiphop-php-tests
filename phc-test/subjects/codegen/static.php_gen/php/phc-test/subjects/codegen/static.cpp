
#include <php/phc-test/subjects/codegen/static.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/static.php line 6 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_x __attribute__((__unused__)) = g->sv_f_DupIdx;
  bool &inited_sv_x __attribute__((__unused__)) = g->inited_sv_f_DupIdx;
  if (!inited_sv_x) {
    (sv_x = 0LL);
    inited_sv_x = true;
  }
  echo(toString(sv_x));
  sv_x++;
} /* function */
Variant pm_php$phc_test$subjects$codegen$static_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/static.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$static_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(13,f_f());
  LINE(14,f_f());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
