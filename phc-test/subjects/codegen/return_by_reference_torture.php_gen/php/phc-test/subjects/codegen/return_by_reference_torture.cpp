
#include <php/phc-test/subjects/codegen/return_by_reference_torture.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/return_by_reference_torture.php line 8 */
Variant f_h() {
  FUNCTION_INJECTION(h);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(x)));
} /* function */
Variant pm_php$phc_test$subjects$codegen$return_by_reference_torture_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/return_by_reference_torture.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$return_by_reference_torture_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_eval_code __attribute__((__unused__)) = (variables != gVariables) ? variables->get("eval_code") : g->GV(eval_code);

  (v_a = 5LL);
  (v_f = ref(LINE(15,f_h())));
  (v_f = v_f + 1LL);
  unset(v_a);
  unset(v_x);
  unset(v_f);
  LINE(20,x_var_export(v_a));
  LINE(21,x_var_export(v_f));
  (v_eval_code = "\n\t$a = 5;\n\t$g =& h();\n\t$g = $f + 1;\n\tunset ($a);\n\tunset ($x);\n\tunset ($g);\n\tvar_export($a);");
  f_eval(toString(v_eval_code));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
