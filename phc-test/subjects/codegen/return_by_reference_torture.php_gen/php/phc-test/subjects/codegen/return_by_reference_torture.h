
#ifndef __GENERATED_php_phc_test_subjects_codegen_return_by_reference_torture_h__
#define __GENERATED_php_phc_test_subjects_codegen_return_by_reference_torture_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/return_by_reference_torture.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$return_by_reference_torture_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_h();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_return_by_reference_torture_h__
