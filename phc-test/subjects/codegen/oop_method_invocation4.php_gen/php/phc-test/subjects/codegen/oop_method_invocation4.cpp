
#include <php/phc-test/subjects/codegen/oop_method_invocation4.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$oop_method_invocation4_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_method_invocation4.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_method_invocation4_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  f_eval(toString("\tclass C\n\t{\n\t\tfunction foo($x, $y)\n\t\t{\n\t\t\techo \"C::foo($x, $y)\n\";\n\t\t\t$this->x = 1;\n\t\t}\n\n\t\tfunction __call($fname, $args)\n\t\t{\n\t\t\techo \"Call to $fname with arguments:\n\";\n\t\t\tvar_dump($args);\n\t\t}\n\t}\n\n\t$c = new C(); "));
  LINE(26,v_c.o_invoke_few_args("foo", 0x6176C0B993BF5914LL, 2, 1LL, 2LL));
  LINE(27,v_c.o_invoke_few_args("bar", 0x5E008C5BA4C3B3FDLL, 2, 1LL, 2LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
