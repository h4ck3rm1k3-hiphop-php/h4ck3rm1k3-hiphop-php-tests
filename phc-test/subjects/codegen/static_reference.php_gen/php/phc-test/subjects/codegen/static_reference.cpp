
#include <php/phc-test/subjects/codegen/static_reference.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/static_reference.php line 5 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_x __attribute__((__unused__)) = g->sv_f_DupIdx;
  bool &inited_sv_x __attribute__((__unused__)) = g->inited_sv_f_DupIdx;
  if (!inited_sv_x) {
    (sv_x = 1LL);
    inited_sv_x = true;
  }
  LINE(8,x_var_dump(1, sv_x));
  (sv_x = ref(lval(g->GV(y))));
  LINE(10,x_var_dump(1, sv_x));
} /* function */
Variant pm_php$phc_test$subjects$codegen$static_reference_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/static_reference.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$static_reference_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_y = 5LL);
  LINE(14,f_f());
  LINE(15,f_f());
  (v_y = 6LL);
  LINE(17,f_f());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
