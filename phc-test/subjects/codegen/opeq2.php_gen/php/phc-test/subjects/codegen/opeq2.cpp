
#include <php/phc-test/subjects/codegen/opeq2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/opeq2.php line 4 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("f is called\n");
  return 1LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$opeq2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/opeq2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$opeq2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  v_y.set(1LL, (5LL), 0x5BCA7C69B794F8CELL);
  (v_x = (lval(v_y.lvalAt(LINE(11,f_f()))) += 1LL));
  LINE(12,x_var_dump(1, v_x));
  LINE(13,x_var_dump(1, v_y));
  echo(" \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
