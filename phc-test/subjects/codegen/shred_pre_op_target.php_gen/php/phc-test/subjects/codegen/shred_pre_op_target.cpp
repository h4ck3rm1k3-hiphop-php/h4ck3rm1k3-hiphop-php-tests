
#include <php/phc-test/subjects/codegen/shred_pre_op_target.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/shred_pre_op_target.php line 9 */
Variant c_y::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_y::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_y::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("y", m_y.isReferenced() ? ref(m_y) : m_y));
  c_ObjectData::o_get(props);
}
bool c_y::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4F56B733A4DFC78ALL, y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_y::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_y::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4F56B733A4DFC78ALL, m_y,
                      y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_y::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_y::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(y)
ObjectData *c_y::cloneImpl() {
  c_y *obj = NEW(c_y)();
  cloneSet(obj);
  return obj;
}
void c_y::cloneSet(c_y *clone) {
  clone->m_y = m_y.isReferenced() ? ref(m_y) : m_y;
  ObjectData::cloneSet(clone);
}
Variant c_y::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_y::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_y::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_y$os_get(const char *s) {
  return c_y::os_get(s, -1);
}
Variant &cw_y$os_lval(const char *s) {
  return c_y::os_lval(s, -1);
}
Variant cw_y$os_constant(const char *s) {
  return c_y::os_constant(s);
}
Variant cw_y$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_y::os_invoke(c, s, params, -1, fatal);
}
void c_y::init() {
  m_y = 12LL;
}
Object co_y(CArrRef params, bool init /* = true */) {
  return Object(p_y(NEW(c_y)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$shred_pre_op_target_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/shred_pre_op_target.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$shred_pre_op_target_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_n1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n1") : g->GV(n1);
  Variant &v_n2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n2") : g->GV(n2);

  (v_x = ScalarArrays::sa_[0]);
  lval(v_x.lvalAt("y", 0x4F56B733A4DFC78ALL))++;
  LINE(6,x_var_dump(1, v_x));
  (v_y = ((Object)(LINE(10,p_y(p_y(NEWOBJ(c_y)())->create())))));
  ++lval(v_y.o_lval("y", 0x4F56B733A4DFC78ALL));
  LINE(12,x_var_dump(1, v_y));
  (v_n1 = "y");
  (v_n2 = "y");
  ++lval(variables->get(toString(v_n1)).o_lval(toString(v_n2), -1LL));
  LINE(18,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
