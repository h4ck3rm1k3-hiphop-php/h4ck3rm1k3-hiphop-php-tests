
#ifndef __GENERATED_php_phc_test_subjects_codegen_shred_pre_op_target_h__
#define __GENERATED_php_phc_test_subjects_codegen_shred_pre_op_target_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/shred_pre_op_target.fw.h>

// Declarations
#include <cls/y.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$shred_pre_op_target_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_y(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_shred_pre_op_target_h__
