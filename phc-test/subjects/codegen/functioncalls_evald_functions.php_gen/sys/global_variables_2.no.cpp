
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_INITIALIZED(0x2C5D5BC33920E200LL, g->GV(o),
                       o);
      break;
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 2:
      HASH_INITIALIZED(0x2343A72E98024302LL, g->GV(l),
                       l);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 4:
      HASH_INITIALIZED(0x7A452383AA9BF7C4LL, g->GV(d),
                       d);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      HASH_INITIALIZED(0x4B27011F259D2446LL, g->GV(j),
                       j);
      break;
    case 8:
      HASH_INITIALIZED(0x2BCCE9AD79BC2688LL, g->GV(g),
                       g);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x117B8667E4662809LL, g->GV(n),
                       n);
      break;
    case 10:
      HASH_INITIALIZED(0x4292CEE227B9150ALL, g->GV(a),
                       a);
      break;
    case 11:
      HASH_INITIALIZED(0x7A0C10E3609EA04BLL, g->GV(m),
                       m);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 15:
      HASH_INITIALIZED(0x0D6857FDDD7F21CFLL, g->GV(k),
                       k);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      HASH_INITIALIZED(0x0BC2CDE4BBFC9C10LL, g->GV(s),
                       s);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 21:
      HASH_INITIALIZED(0x08FBB133F8576BD5LL, g->GV(b),
                       b);
      break;
    case 24:
      HASH_INITIALIZED(0x0EB22EDA95766E98LL, g->GV(i),
                       i);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      HASH_INITIALIZED(0x05CAFC91A9B37763LL, g->GV(q),
                       q);
      break;
    case 38:
      HASH_INITIALIZED(0x77F632A4E34F1526LL, g->GV(p),
                       p);
      break;
    case 43:
      HASH_INITIALIZED(0x61B161496B7EA7EBLL, g->GV(e),
                       e);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      HASH_INITIALIZED(0x6BB4A0689FBAD42ELL, g->GV(f),
                       f);
      break;
    case 48:
      HASH_INITIALIZED(0x32C769EE5C5509B0LL, g->GV(c),
                       c);
      HASH_INITIALIZED(0x695875365480A8F0LL, g->GV(h),
                       h);
      break;
    case 54:
      HASH_INITIALIZED(0x6ECE84440D42ECF6LL, g->GV(r),
                       r);
      break;
    case 60:
      HASH_INITIALIZED(0x11C9D2391242AEBCLL, g->GV(t),
                       t);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
