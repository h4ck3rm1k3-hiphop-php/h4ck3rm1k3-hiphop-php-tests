
#ifndef __GENERATED_php_phc_test_subjects_codegen_functioncalls_evald_functions_h__
#define __GENERATED_php_phc_test_subjects_codegen_functioncalls_evald_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/functioncalls_evald_functions.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$functioncalls_evald_functions_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_functioncalls_evald_functions_h__
