
#include <php/phc-test/subjects/codegen/functioncalls_evald_functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$functioncalls_evald_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/functioncalls_evald_functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$functioncalls_evald_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_l __attribute__((__unused__)) = (variables != gVariables) ? variables->get("l") : g->GV(l);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_p __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p") : g->GV(p);
  Variant &v_q __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q") : g->GV(q);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  f_eval("function fun($x) { $x = 'x'; }");
  f_eval("function fun_r(&$x) { $x = 'x'; }");
  (v_a = 10LL);
  LINE(12,invoke_failed("fun", Array(ArrayInit(1).set(0, ref(v_a)).create()), 0x0000000044CB8BC8LL));
  LINE(13,x_var_export(v_a));
  (v_b = 20LL);
  LINE(16,invoke_failed("fun", Array(ArrayInit(1).set(0, ref(v_b)).create()), 0x0000000044CB8BC8LL));
  LINE(17,x_var_export(v_b));
  (v_c = 30LL);
  LINE(20,invoke_failed("fun_r", Array(ArrayInit(1).set(0, ref(v_c)).create()), 0x00000000D0FB6BCFLL));
  LINE(21,x_var_export(v_c));
  (v_d = 40LL);
  LINE(24,invoke_failed("fun_r", Array(ArrayInit(1).set(0, ref(v_d)).create()), 0x00000000D0FB6BCFLL));
  LINE(25,x_var_export(v_d));
  echo("\n");
  (v_e = 50LL);
  (v_f = v_e);
  LINE(35,invoke_failed("fun", Array(ArrayInit(1).set(0, ref(v_f)).create()), 0x0000000044CB8BC8LL));
  LINE(36,x_var_export(v_e));
  LINE(37,x_var_export(v_f));
  (v_f = "y");
  LINE(39,x_var_export(v_e));
  LINE(40,x_var_export(v_f));
  (v_g = 60LL);
  (v_h = v_g);
  LINE(44,invoke_failed("fun", Array(ArrayInit(1).set(0, ref(v_h)).create()), 0x0000000044CB8BC8LL));
  LINE(45,x_var_export(v_g));
  LINE(46,x_var_export(v_h));
  (v_h = "y");
  LINE(48,x_var_export(v_g));
  LINE(49,x_var_export(v_h));
  (v_i = 70LL);
  (v_j = v_i);
  LINE(53,invoke_failed("fun_r", Array(ArrayInit(1).set(0, ref(v_j)).create()), 0x00000000D0FB6BCFLL));
  LINE(54,x_var_export(v_i));
  LINE(55,x_var_export(v_j));
  (v_j = "y");
  LINE(57,x_var_export(v_i));
  LINE(58,x_var_export(v_j));
  (v_k = 80LL);
  (v_l = v_k);
  LINE(62,invoke_failed("fun_r", Array(ArrayInit(1).set(0, ref(v_l)).create()), 0x00000000D0FB6BCFLL));
  LINE(63,x_var_export(v_k));
  LINE(64,x_var_export(v_l));
  (v_l = "y");
  LINE(66,x_var_export(v_k));
  LINE(67,x_var_export(v_l));
  echo("\n");
  (v_m = 90LL);
  (v_n = ref(v_m));
  LINE(78,invoke_failed("fun", Array(ArrayInit(1).set(0, ref(v_n)).create()), 0x0000000044CB8BC8LL));
  LINE(79,x_var_export(v_m));
  LINE(80,x_var_export(v_n));
  (v_m = "y");
  LINE(82,x_var_export(v_m));
  LINE(83,x_var_export(v_n));
  (v_o = 100LL);
  (v_p = ref(v_o));
  LINE(87,invoke_failed("fun", Array(ArrayInit(1).set(0, ref(v_p)).create()), 0x0000000044CB8BC8LL));
  LINE(88,x_var_export(v_o));
  LINE(89,x_var_export(v_p));
  (v_p = "y");
  LINE(91,x_var_export(v_o));
  LINE(92,x_var_export(v_p));
  (v_q = 110LL);
  (v_r = ref(v_q));
  LINE(96,invoke_failed("fun_r", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000D0FB6BCFLL));
  LINE(97,x_var_export(v_q));
  LINE(98,x_var_export(v_r));
  (v_r = "y");
  LINE(100,x_var_export(v_q));
  LINE(101,x_var_export(v_r));
  (v_s = 120LL);
  (v_t = ref(v_s));
  LINE(105,invoke_failed("fun_r", Array(ArrayInit(1).set(0, ref(v_t)).create()), 0x00000000D0FB6BCFLL));
  LINE(106,x_var_export(v_s));
  LINE(107,x_var_export(v_t));
  (v_t = "y");
  LINE(109,x_var_export(v_s));
  LINE(110,x_var_export(v_t));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
