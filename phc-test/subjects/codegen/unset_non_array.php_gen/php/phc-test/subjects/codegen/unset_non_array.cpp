
#include <php/phc-test/subjects/codegen/unset_non_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$unset_non_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/unset_non_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$unset_non_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_short_scalar_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("short_scalar_array") : g->GV(short_scalar_array);
  Variant &v_scalar_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scalar_array") : g->GV(scalar_array);
  Variant &v_scalar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scalar") : g->GV(scalar);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  LINE(3,require("parsing/scalar_array.php", false, variables, "phc-test/subjects/codegen/"));
  (v_scalar_array = v_short_scalar_array);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_scalar_array.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_scalar = iter3->second();
      {
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_scalar_array.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_i = iter6->second();
            {
              echo("Working with ");
              LINE(12,x_var_dump(1, v_scalar));
              {
                echo("\nUnset single level (");
                echo(LINE(14,x_gettype(v_i)));
                echo(concat3(": ", toString(v_i), ")\n"));
              }
              (v_x = v_scalar);
              if (LINE(18,x_is_string(v_x))) {
                echo("skip\n");
                continue;
              }
              v_x.weakRemove(v_i);
              LINE(25,x_var_dump(1, v_x));
              {
                LOOP_COUNTER(7);
                for (ArrayIterPtr iter9 = v_scalar_array.begin(); !iter9->end(); iter9->next()) {
                  LOOP_COUNTER_CHECK(7);
                  v_j = iter9->second();
                  {
                    {
                      echo("\nUnset two levels (");
                      echo(LINE(29,x_gettype(v_i)));
                      echo(concat3(": ", toString(v_i), ", "));
                      echo(x_gettype(v_j));
                      echo(concat3(": ", toString(v_j), ")\n"));
                    }
                    (v_x = v_scalar);
                    if (LINE(33,x_is_string(v_x.rvalAt(v_i)))) {
                      echo("skip\n");
                      continue;
                    }
                    lval(unsetLval(v_x, v_i)).weakRemove(v_j);
                    LINE(40,x_var_dump(1, v_x));
                    {
                      LOOP_COUNTER(10);
                      for (ArrayIterPtr iter12 = v_scalar_array.begin(); !iter12->end(); iter12->next()) {
                        LOOP_COUNTER_CHECK(10);
                        v_k = iter12->second();
                        {
                          {
                            echo("\nUnset 3 levels (");
                            echo(LINE(44,x_gettype(v_i)));
                            echo(concat3(": ", toString(v_i), ", "));
                            echo(x_gettype(v_j));
                            echo(concat3(": ", toString(v_j), ", "));
                            echo(x_gettype(v_k));
                            echo(concat3(": ", toString(v_k), "))\n"));
                          }
                          (v_x = v_scalar);
                          if (LINE(48,x_is_string(v_x.rvalAt(v_i).rvalAt(v_j)))) {
                            echo("skip\n");
                            continue;
                          }
                          lval(unsetLval(lval(unsetLval(v_x, v_i)), v_j)).weakRemove(v_k);
                          LINE(55,x_var_dump(1, v_x));
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
