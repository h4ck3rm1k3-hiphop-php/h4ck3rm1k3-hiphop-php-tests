
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_method_invocation5.php line 4 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: void t_foo(CVarRef v_x, CVarRef v_y);
  public: void t_bar(CVarRef v_x, CVarRef v_y);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
