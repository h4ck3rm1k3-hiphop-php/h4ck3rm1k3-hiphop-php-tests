
#include <php/phc-test/subjects/codegen/switch_object.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/switch_object.php line 3 */
Variant c_obj::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_obj::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_obj::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_obj::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_obj::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_obj::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_obj::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_obj::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(obj)
ObjectData *c_obj::create(CVarRef v__x) {
  init();
  t_obj(v__x);
  return this;
}
ObjectData *c_obj::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_obj::cloneImpl() {
  c_obj *obj = NEW(c_obj)();
  cloneSet(obj);
  return obj;
}
void c_obj::cloneSet(c_obj *clone) {
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_obj::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_obj::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_obj::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_obj$os_get(const char *s) {
  return c_obj::os_get(s, -1);
}
Variant &cw_obj$os_lval(const char *s) {
  return c_obj::os_lval(s, -1);
}
Variant cw_obj$os_constant(const char *s) {
  return c_obj::os_constant(s);
}
Variant cw_obj$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_obj::os_invoke(c, s, params, -1, fatal);
}
void c_obj::init() {
  m_x = null;
}
/* SRC: phc-test/subjects/codegen/switch_object.php line 6 */
void c_obj::t_obj(CVarRef v__x) {
  INSTANCE_METHOD_INJECTION(Obj, Obj::Obj);
  bool oldInCtor = gasInCtor(true);
  Variant v_x;

  (v_x = v__x);
  gasInCtor(oldInCtor);
} /* function */
Object co_obj(CArrRef params, bool init /* = true */) {
  return Object(p_obj(NEW(c_obj)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$switch_object_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_object.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_object_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_instance1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("instance1") : g->GV(instance1);
  Variant &v_instance2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("instance2") : g->GV(instance2);
  Variant &v_instance3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("instance3") : g->GV(instance3);

  (v_instance1 = ((Object)(LINE(11,p_obj(p_obj(NEWOBJ(c_obj)())->create(7LL))))));
  (v_instance2 = ((Object)(LINE(12,p_obj(p_obj(NEWOBJ(c_obj)())->create(((Object)(p_obj(p_obj(NEWOBJ(c_obj)())->create("asdasd"))))))))));
  (v_instance3 = ((Object)(LINE(13,p_obj(p_obj(NEWOBJ(c_obj)())->create(((Object)(p_obj(p_obj(NEWOBJ(c_obj)())->create("asdasd"))))))))));
  {
    Variant tmp2 = (v_instance3);
    int tmp3 = -1;
    if (equal(tmp2, (0LL))) {
      tmp3 = 0;
    } else if (equal(tmp2, (v_instance2))) {
      tmp3 = 1;
    } else if (equal(tmp2, (7LL))) {
      tmp3 = 2;
    } else if (true) {
      tmp3 = 3;
    }
    switch (tmp3) {
    case 0:
      {
        echo("zero\n");
        goto break1;
      }
    case 1:
      {
        echo("the same\n");
        goto break1;
      }
    case 2:
      {
        echo("seven\n");
        goto break1;
      }
    case 3:
      {
        echo("none of the above\n");
        goto break1;
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
