
#ifndef __GENERATED_php_phc_test_subjects_codegen_switch_object_h__
#define __GENERATED_php_phc_test_subjects_codegen_switch_object_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/switch_object.fw.h>

// Declarations
#include <cls/obj.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$switch_object_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_obj(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_switch_object_h__
