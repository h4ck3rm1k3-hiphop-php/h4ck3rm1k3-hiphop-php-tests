
#ifndef __GENERATED_cls_obj_h__
#define __GENERATED_cls_obj_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/switch_object.php line 3 */
class c_obj : virtual public ObjectData {
  BEGIN_CLASS_MAP(obj)
  END_CLASS_MAP(obj)
  DECLARE_CLASS(obj, Obj, ObjectData)
  void init();
  public: String m_x;
  public: void t_obj(CVarRef v__x);
  public: ObjectData *create(CVarRef v__x);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_obj_h__
