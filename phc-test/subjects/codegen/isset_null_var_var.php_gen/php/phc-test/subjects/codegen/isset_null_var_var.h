
#ifndef __GENERATED_php_phc_test_subjects_codegen_isset_null_var_var_h__
#define __GENERATED_php_phc_test_subjects_codegen_isset_null_var_var_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/isset_null_var_var.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$isset_null_var_var_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_isset_null_var_var_h__
