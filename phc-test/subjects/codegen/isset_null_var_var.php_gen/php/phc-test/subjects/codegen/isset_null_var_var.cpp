
#include <php/phc-test/subjects/codegen/isset_null_var_var.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$isset_null_var_var_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_null_var_var.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_null_var_var_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  if (isset(variables->get(toString(v_x)))) echo("null var var is set\n");
  else echo("null var var is not set\n");
  (get_variable_table()->get("") = 1LL);
  if (isset(variables->get(toString(v_x)))) echo("null var var is set\n");
  else echo("null var var is not set\n");
  (get_variable_table()->get("") = null);
  if (isset(variables->get(toString(v_x)))) echo("null var var is set\n");
  else echo("null var var is not set\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
