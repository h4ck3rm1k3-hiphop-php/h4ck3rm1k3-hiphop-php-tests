
#include <php/phc-test/subjects/codegen/break_negative.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$break_negative_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/break_negative.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$break_negative_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LOOP_COUNTER(1);
  {
    while (true) {
      LOOP_COUNTER_CHECK(1);
      {
        echo("before\n");
        break;
        echo("below\n");
      }
    }
  }
  echo("after\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
