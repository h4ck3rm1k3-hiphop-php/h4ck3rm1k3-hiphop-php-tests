
#include <php/phc-test/subjects/codegen/bailout_die.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bailout_die.php line 3 */
void f_my_error_handler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline, CArrRef v_errcontext) {
  FUNCTION_INJECTION(my_error_handler);
  Variant v_err_context;

  echo(concat_rev(LINE(5,concat6(toString(v_errfile), "\n", toString(v_errline), ": ", toString(v_errline), "\n")), concat5("errno: ", toString(v_errno), "\nerrstr: ", toString(v_errstr), "\nerrfile: ")));
  LINE(6,x_var_export(v_err_context));
} /* function */
Variant i_my_error_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x606C54C87D73CF7DLL, my_error_handler) {
    return (f_my_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$codegen$bailout_die_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bailout_die.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bailout_die_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(12,x_set_error_handler("my_error_handler", toInt32(8191LL)));
  f_exit("asdasd");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
