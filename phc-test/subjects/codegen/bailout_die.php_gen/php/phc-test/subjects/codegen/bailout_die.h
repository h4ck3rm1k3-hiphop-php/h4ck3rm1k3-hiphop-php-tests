
#ifndef __GENERATED_php_phc_test_subjects_codegen_bailout_die_h__
#define __GENERATED_php_phc_test_subjects_codegen_bailout_die_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bailout_die.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$bailout_die_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_my_error_handler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline, CArrRef v_errcontext);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bailout_die_h__
