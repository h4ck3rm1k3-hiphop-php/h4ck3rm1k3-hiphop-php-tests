
#include <php/phc-test/subjects/codegen/conditional.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/conditional.php line 2 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("this is f\n");
  return 1LL;
} /* function */
/* SRC: phc-test/subjects/codegen/conditional.php line 8 */
int64 f_g() {
  FUNCTION_INJECTION(g);
  echo("this is g\n");
  return 2LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$conditional_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/conditional.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$conditional_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(14,x_var_dump(1, f_f()));
  LINE(15,x_var_dump(1, f_g()));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
