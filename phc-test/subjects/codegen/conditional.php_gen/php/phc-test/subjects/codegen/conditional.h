
#ifndef __GENERATED_php_phc_test_subjects_codegen_conditional_h__
#define __GENERATED_php_phc_test_subjects_codegen_conditional_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/conditional.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_f();
int64 f_g();
Variant pm_php$phc_test$subjects$codegen$conditional_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_conditional_h__
