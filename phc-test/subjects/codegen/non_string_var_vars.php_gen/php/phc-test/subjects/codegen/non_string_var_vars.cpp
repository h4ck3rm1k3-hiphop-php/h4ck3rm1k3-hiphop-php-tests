
#include <php/phc-test/subjects/codegen/non_string_var_vars.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$non_string_var_vars_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/non_string_var_vars.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$non_string_var_vars_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_0 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("0") : g->GV(0);

  (get_variable_table()->get("0") = 2LL);
  (v_0 = 5LL);
  LINE(6,x_var_dump(1, get_variable_table()->get("0")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
