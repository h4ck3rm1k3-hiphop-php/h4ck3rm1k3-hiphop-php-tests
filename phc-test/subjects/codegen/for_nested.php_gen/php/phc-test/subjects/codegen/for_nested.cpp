
#include <php/phc-test/subjects/codegen/for_nested.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$for_nested_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/for_nested.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$for_nested_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); (v_i = v_i + 1LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(v_i) + toString("\n"));
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, 10LL); (v_j = v_j + 1LL)) {
            LOOP_COUNTER_CHECK(2);
            {
              echo(toString(v_j) + toString("\n"));
              {
                LOOP_COUNTER(3);
                for ((v_k = 20LL); more(v_k, 0LL); (v_k = v_k - 1LL)) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    echo(toString(v_k) + toString("\n"));
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  LINE(15,x_var_dump(1, v_i));
  LINE(16,x_var_dump(1, v_j));
  LINE(17,x_var_dump(1, v_k));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
