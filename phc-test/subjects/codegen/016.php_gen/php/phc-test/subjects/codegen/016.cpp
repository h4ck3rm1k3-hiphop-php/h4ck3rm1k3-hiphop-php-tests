
#include <php/phc-test/subjects/codegen/016.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/016.php line 2 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("called f\n");
  return 1LL;
} /* function */
/* SRC: phc-test/subjects/codegen/016.php line 8 */
int64 f_g() {
  FUNCTION_INJECTION(g);
  echo("called g\n");
  return 2LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$016_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/016.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$016_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  v_a.set(LINE(14,f_f()), (f_g()));
  LINE(15,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
