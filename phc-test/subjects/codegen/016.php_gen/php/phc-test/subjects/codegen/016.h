
#ifndef __GENERATED_php_phc_test_subjects_codegen_016_h__
#define __GENERATED_php_phc_test_subjects_codegen_016_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/016.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$016_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_f();
int64 f_g();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_016_h__
