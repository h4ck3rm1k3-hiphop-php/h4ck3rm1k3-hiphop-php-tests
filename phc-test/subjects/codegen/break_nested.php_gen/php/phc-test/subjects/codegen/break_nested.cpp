
#include <php/phc-test/subjects/codegen/break_nested.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$break_nested_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/break_nested.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$break_nested_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(5,x_var_dump(1, v_i));
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, 10LL); v_j++) {
            LOOP_COUNTER_CHECK(2);
            {
              LINE(8,x_var_dump(1, v_j));
              break;
              LINE(10,x_var_dump(1, "XXX"));
            }
          }
        }
        if (equal(v_i, 5LL)) break;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
