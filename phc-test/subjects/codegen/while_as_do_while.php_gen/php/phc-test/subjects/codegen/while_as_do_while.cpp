
#include <php/phc-test/subjects/codegen/while_as_do_while.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$while_as_do_while_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/while_as_do_while.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$while_as_do_while_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 10LL);
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_x)) {
      LOOP_COUNTER_CHECK(1);
      {
        v_x--;
        if (less(v_x, 3LL)) continue;
        echo("x\n");
      }
    }
  }
  (v_x = 10LL);
  {
    LOOP_COUNTER(2);
    do {
      LOOP_COUNTER_CHECK(2);
      {
        if (!(toBoolean(v_x))) break;
        v_x--;
        if (less(v_x, 3LL)) continue;
        echo("x\n");
      }
    } while (true);
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
