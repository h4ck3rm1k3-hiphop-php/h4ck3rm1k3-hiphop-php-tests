
#include <php/phc-test/subjects/codegen/bench_ary3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_ary3.php line 3 */
void f_ary3(int64 v_n) {
  FUNCTION_INJECTION(ary3);
  Numeric v_i = 0;
  Sequence v_X;
  Sequence v_Y;
  int64 v_k = 0;
  Numeric v_last = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_X.set(v_i, (v_i + 1LL));
        v_Y.set(v_i, (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
      LOOP_COUNTER_CHECK(2);
      {
        {
          LOOP_COUNTER(3);
          for ((v_i = v_n - 1LL); not_less(v_i, 0LL); v_i--) {
            LOOP_COUNTER_CHECK(3);
            {
              lval(v_Y.lvalAt(v_i)) += v_X.rvalAt(v_i);
            }
          }
        }
      }
    }
  }
  (v_last = v_n - 1LL);
  print(LINE(14,concat4(toString(v_Y.rvalAt(0, 0x77CFA1EEF01BCA90LL)), " ", toString(v_Y.rvalAt(v_last)), "\n")));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_ary3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_ary3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_ary3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(18,f_ary3(5LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
