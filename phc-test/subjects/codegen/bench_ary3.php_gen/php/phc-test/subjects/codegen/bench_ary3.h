
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_ary3_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_ary3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_ary3.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$bench_ary3_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_ary3(int64 v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_ary3_h__
