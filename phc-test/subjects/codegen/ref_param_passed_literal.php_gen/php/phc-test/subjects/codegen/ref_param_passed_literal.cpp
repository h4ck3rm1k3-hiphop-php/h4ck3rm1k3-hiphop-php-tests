
#include <php/phc-test/subjects/codegen/ref_param_passed_literal.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/ref_param_passed_literal.php line 3 */
void f_f(Variant v_x) {
  FUNCTION_INJECTION(f);
  (v_x = 7LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$ref_param_passed_literal_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/ref_param_passed_literal.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$ref_param_passed_literal_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_y = "test");
  LINE(9,f_f(ref(v_y)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
