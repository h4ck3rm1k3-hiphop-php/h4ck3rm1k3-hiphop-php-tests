
#include <php/phc-test/subjects/codegen/nested_statements.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_IA = 3877LL;
const int64 k_IC = 29573LL;
const int64 k_IM = 139968LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/nested_statements.php line 10 */
Numeric f_gen_random(CVarRef v_n) {
  FUNCTION_INJECTION(gen_random);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LAST __attribute__((__unused__)) = g->GV(LAST);
  return (divide((v_n * ((gv_LAST = modulo((toInt64(gv_LAST * 3877LL /* IA */ + 29573LL /* IC */)), 139968LL /* IM */)))), 139968LL /* IM */));
} /* function */
Variant pm_php$phc_test$subjects$codegen$nested_statements_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/nested_statements.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$nested_statements_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_LAST __attribute__((__unused__)) = (variables != gVariables) ? variables->get("LAST") : g->GV(LAST);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  ;
  ;
  ;
  (v_LAST = 42LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 20LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_x = LINE(17,f_gen_random(v_i)));
        echo(toString(v_x) + toString("\n"));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
