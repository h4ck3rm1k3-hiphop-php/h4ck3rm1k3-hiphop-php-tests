
#include <php/phc-test/subjects/codegen/switch_continue.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$switch_continue_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_continue.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_continue_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  {
    LOOP_COUNTER(1);
    for ((v_x = 0LL); less(v_x, 10LL); v_x++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          switch (toInt64(v_x)) {
          case 0LL:
            {
              echo("0\n");
              break;
              echo("x\n");
            }
          case 1LL:
            {
              echo("1\n");
              break;
              echo("x\n");
            }
          case 3LL:
            {
            }
          case 2LL:
            {
              echo("2\n");
              break;
              echo("x\n");
            }
          case 4LL:
            {
              echo("4\n");
              break;
              echo("x\n");
            }
          case 5LL:
            {
              echo("5\n");
              goto continue1;
              echo("x\n");
            }
          }
        }
        echo("done switch\n");
      }
      continue1:;
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
