
#ifndef __GENERATED_php_phc_test_subjects_codegen_unset_local_global_h__
#define __GENERATED_php_phc_test_subjects_codegen_unset_local_global_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/unset_local_global.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_set_global_y1();
void f_set_global_y2();
void f_show_global_x1();
void f_show_global_x2();
Variant pm_php$phc_test$subjects$codegen$unset_local_global_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_unset_local_global_h__
