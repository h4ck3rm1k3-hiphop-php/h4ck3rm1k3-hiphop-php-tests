
#ifndef __GENERATED_php_phc_test_subjects_codegen_isset_in_main_h__
#define __GENERATED_php_phc_test_subjects_codegen_isset_in_main_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/isset_in_main.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$isset_in_main_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_isset_in_main_h__
