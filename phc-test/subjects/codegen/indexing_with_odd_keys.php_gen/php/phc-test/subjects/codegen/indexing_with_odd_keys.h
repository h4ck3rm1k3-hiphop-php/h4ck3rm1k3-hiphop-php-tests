
#ifndef __GENERATED_php_phc_test_subjects_codegen_indexing_with_odd_keys_h__
#define __GENERATED_php_phc_test_subjects_codegen_indexing_with_odd_keys_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/indexing_with_odd_keys.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$indexing_with_odd_keys_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_indexing_with_odd_keys_h__
