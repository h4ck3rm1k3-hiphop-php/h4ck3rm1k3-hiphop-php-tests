
#include <php/phc-test/subjects/codegen/indexing_with_odd_keys.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$indexing_with_odd_keys_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/indexing_with_odd_keys.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$indexing_with_odd_keys_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  (v_arr = ScalarArrays::sa_[0]);
  LINE(10,x_var_export(v_arr));
  LINE(12,x_var_dump(1, v_arr.rvalAt(10LL, 0x6A15D6AA542BAD8BLL)));
  LINE(13,x_var_dump(1, v_arr.rvalAt(2.3450000000000002)));
  LINE(14,x_var_dump(1, v_arr.rvalAt(true)));
  LINE(15,x_var_dump(1, v_arr.rvalAt(false)));
  LINE(16,x_var_dump(1, v_arr.rvalAt(null)));
  LINE(17,x_var_dump(1, v_arr.rvalAt("f", 0x6BB4A0689FBAD42ELL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
