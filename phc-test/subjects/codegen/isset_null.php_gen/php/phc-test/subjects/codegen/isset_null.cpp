
#include <php/phc-test/subjects/codegen/isset_null.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$isset_null_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_null.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_null_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  if (isset(v_a)) echo("a is set\n");
  else echo("a is not set\n");
  setNull(v_a);
  if (isset(v_a)) echo("a is set\n");
  else echo("a is not set\n");
  (v_a = 1LL);
  if (isset(v_a)) echo("a is set\n");
  else echo("a is not set\n");
  setNull(v_a);
  if (isset(v_a)) echo("a is set\n");
  else echo("a is not set\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
