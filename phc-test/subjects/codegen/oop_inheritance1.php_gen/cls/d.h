
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__

#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_inheritance1.php line 13 */
class c_d : virtual public c_c {
  BEGIN_CLASS_MAP(d)
    PARENT_CLASS(c)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, c)
  void init();
  public: void t_bar();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
