
#include <php/phc-test/subjects/codegen/oop_method_invocation3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$oop_method_invocation3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_method_invocation3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_method_invocation3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_g1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g1") : g->GV(g1);
  Variant &v_g2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g2") : g->GV(g2);
  Variant &v_g3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g3") : g->GV(g3);

  f_eval(toString("\tclass C\n\t{\n\t\t// Simple arguments\n\t\tfunction fsimple($x, $y)\n\t\t{\n\t\t\techo \"C::foo($x, $y)\n\";\n\t\t}\n\t\t\n\t\tfunction fref1($x)\n\t\t{\n\t\t\t$x = 1;\n\t\t\t$this->a1 =& $x;\n\t\t}\n\t\t\n\t\tfunction fref2($x)\n\t\t{\n\t\t\t$x = 1;\n\t\t\t$this->a2 =& $x;\n\t\t}\n\n\t\tfunction fref3(&$x)\n\t\t{\n\t\t\t$x = 1;\n\t\t\t$this->a3 =& $x;\n\t\t}\n\t}\n\n\t$c = new C(); "));
  echo("** Simple argument passing test\n");
  LINE(39,v_c.o_invoke_few_args("fsimple", 0x4C0EC971F7B4F7BBLL, 2, 1LL, 2LL));
  echo("** Pass by value\n");
  (v_g1 = 0LL);
  LINE(43,v_c.o_invoke_few_args("fref1", 0x737D9D708354E3B4LL, 1, v_g1));
  x_var_dump(1, v_g1);
  (v_c.o_lval("a1", 0x68DF81F26D942FC7LL) = 2LL);
  LINE(44,x_var_dump(1, v_g1));
  echo("** Runtime pass-by-reference\n");
  (v_g2 = 0LL);
  LINE(48,v_c.o_invoke_few_args("fref2", 0x2818615AAEB7C46BLL, 1, ref(v_g2)));
  x_var_dump(1, v_g2);
  (v_c.o_lval("a2", 0x6C05C2858C72A876LL) = 2LL);
  LINE(49,x_var_dump(1, v_g2));
  echo("** Compile-time pass-by-reference\n");
  (v_g3 = 0LL);
  LINE(53,v_c.o_invoke_few_args("fref3", 0x07739320D979E300LL, 1, v_g3));
  x_var_dump(1, v_g3);
  (v_c.o_lval("a3", 0x43EDA7BEE714570DLL) = 2LL);
  LINE(54,x_var_dump(1, v_g3));
  echo("** Final state of the object\n");
  LINE(57,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
