
#include <php/phc-test/subjects/codegen/switch_double_default.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$switch_double_default_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_double_default.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_double_default_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  {
    int64 tmp2 = (6LL);
    int tmp3 = -1;
     else if (equal(tmp2, (7LL))) {
      tmp3 = 2;
    } else if (true) {
      tmp3 = 1;
    }
    switch (tmp3) {
    case 0:
      {
        echo("default1\n");
      }
    case 1:
      {
        echo("default2\n");
      }
    case 2:
      {
        echo("seven\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
