
#include <php/phc-test/subjects/codegen/shutdown_on_exit_string.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/shutdown_on_exit_string.php line 5 */
void f_shut() {
  FUNCTION_INJECTION(shut);
  echo("shutdown function called\n");
} /* function */
Variant i_shut(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x241EF789F0A47B7ALL, shut) {
    return (f_shut(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$codegen$shutdown_on_exit_string_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/shutdown_on_exit_string.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$shutdown_on_exit_string_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_register_shutdown_function(1, "shut"));
  f_exit("asd");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
