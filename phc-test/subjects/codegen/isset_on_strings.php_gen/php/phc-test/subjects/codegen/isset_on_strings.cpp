
#include <php/phc-test/subjects/codegen/isset_on_strings.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$isset_on_strings_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_on_strings.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_on_strings_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = "012");
  LINE(4,x_var_dump(1, isset(v_x, -1LL, 0x1F89206E3F8EC794LL)));
  LINE(5,x_var_dump(1, isset(v_x, 0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(6,x_var_dump(1, isset(v_x, 1LL, 0x5BCA7C69B794F8CELL)));
  LINE(7,x_var_dump(1, isset(v_x, 2LL, 0x486AFCC090D5F98CLL)));
  LINE(8,x_var_dump(1, isset(v_x, 3LL, 0x135FDDF6A6BFBBDDLL)));
  LINE(9,x_var_dump(1, isset(v_x, 4LL, 0x6F2A25235E544A31LL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
