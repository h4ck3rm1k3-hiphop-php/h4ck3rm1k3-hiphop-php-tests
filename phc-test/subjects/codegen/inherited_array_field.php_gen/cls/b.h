
#ifndef __GENERATED_cls_b_h__
#define __GENERATED_cls_b_h__

#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/inherited_array_field.php line 9 */
class c_b : virtual public c_a {
  BEGIN_CLASS_MAP(b)
    PARENT_CLASS(a)
  END_CLASS_MAP(b)
  DECLARE_CLASS(b, B, a)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_b_h__
