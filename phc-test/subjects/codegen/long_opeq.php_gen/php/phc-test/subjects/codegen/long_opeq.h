
#ifndef __GENERATED_php_phc_test_subjects_codegen_long_opeq_h__
#define __GENERATED_php_phc_test_subjects_codegen_long_opeq_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/long_opeq.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$long_opeq_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_f(CStrRef v_param);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_long_opeq_h__
