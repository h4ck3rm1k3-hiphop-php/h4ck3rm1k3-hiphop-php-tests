
#include <php/phc-test/subjects/codegen/long_opeq.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/long_opeq.php line 2 */
int64 f_f(CStrRef v_param) {
  FUNCTION_INJECTION(f);
  echo(LINE(4,concat3("f (", v_param, ") is called\n")));
  return 1LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$long_opeq_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/long_opeq.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$long_opeq_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  lval(lval(lval(lval(lval(lval(v_x.lvalAt(LINE(9,f_f("one")))).lvalAt(2LL, 0x486AFCC090D5F98CLL)).lvalAt(LINE(11,f_f("three")))).lvalAt(4LL, 0x6F2A25235E544A31LL)).lvalAt(5LL, 0x350AEB726A15D700LL)).lvalAt(LINE(14,f_f("six")))) += 1LL;
  LINE(16,x_var_export(v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
