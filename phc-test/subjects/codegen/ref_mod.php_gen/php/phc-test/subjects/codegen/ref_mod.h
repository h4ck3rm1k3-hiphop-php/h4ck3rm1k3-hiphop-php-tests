
#ifndef __GENERATED_php_phc_test_subjects_codegen_ref_mod_h__
#define __GENERATED_php_phc_test_subjects_codegen_ref_mod_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/ref_mod.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$ref_mod_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f(Variant v_x, Variant v_y);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_ref_mod_h__
