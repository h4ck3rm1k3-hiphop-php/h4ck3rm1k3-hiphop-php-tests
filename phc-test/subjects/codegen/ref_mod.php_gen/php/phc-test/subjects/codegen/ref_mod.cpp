
#include <php/phc-test/subjects/codegen/ref_mod.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/ref_mod.php line 13 */
void f_f(Variant v_x, Variant v_y) {
  FUNCTION_INJECTION(f);
  Variant v_tmp;

  (v_tmp = ref(v_y));
  (v_y = ref(v_x));
  (v_x = ref(v_tmp));
  (v_y = v_x);
} /* function */
Variant pm_php$phc_test$subjects$codegen$ref_mod_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/ref_mod.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$ref_mod_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = 5LL);
  (v_y = 6LL);
  LINE(24,f_f(ref(v_x), ref(v_y)));
  LINE(25,x_var_dump(2, v_x, Array(ArrayInit(1).set(0, v_y).create())));
  unset(v_x);
  unset(v_y);
  LINE(29,f_f(ref(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))), ref(lval(v_y.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  LINE(30,x_var_dump(2, v_x, Array(ArrayInit(1).set(0, v_y).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
