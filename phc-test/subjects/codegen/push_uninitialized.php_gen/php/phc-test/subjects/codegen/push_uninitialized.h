
#ifndef __GENERATED_php_phc_test_subjects_codegen_push_uninitialized_h__
#define __GENERATED_php_phc_test_subjects_codegen_push_uninitialized_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/push_uninitialized.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$push_uninitialized_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_push_uninitialized_h__
