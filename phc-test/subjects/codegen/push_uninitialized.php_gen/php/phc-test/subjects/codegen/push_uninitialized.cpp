
#include <php/phc-test/subjects/codegen/push_uninitialized.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$push_uninitialized_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/push_uninitialized.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$push_uninitialized_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_scalar_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scalar_array") : g->GV(scalar_array);
  Variant &v_scalar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scalar") : g->GV(scalar);

  LINE(3,require("parsing/scalar_array.php", false, variables, "phc-test/subjects/codegen/"));
  v_x.append((5LL));
  LINE(6,x_var_dump(1, v_x));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_scalar_array.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_scalar = iter3->second();
      {
        if (LINE(11,x_is_string(v_scalar)) && more(x_strlen(toString(v_scalar)), 0LL)) continue;
        echo("\nTrying: ");
        LINE(15,x_var_dump(1, v_scalar));
        (v_x = v_scalar);
        v_x.append((5LL));
        LINE(19,x_var_dump(1, v_x));
      }
    }
  }
  (v_x = "str");
  v_x.append((5LL));
  LINE(29,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
