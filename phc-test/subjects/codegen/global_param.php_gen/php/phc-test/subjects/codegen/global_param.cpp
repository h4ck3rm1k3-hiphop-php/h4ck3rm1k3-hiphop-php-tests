
#include <php/phc-test/subjects/codegen/global_param.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/global_param.php line 6 */
void f_a(int64 v_GLOBALS) {
  FUNCTION_INJECTION(a);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(8,x_var_dump(1, get_global_array_wrapper()));
  LINE(9,x_var_dump(1, g->GV(a)));
} /* function */
Variant pm_php$phc_test$subjects$codegen$global_param_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/global_param.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$global_param_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 6LL);
  LINE(12,f_a(5LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
