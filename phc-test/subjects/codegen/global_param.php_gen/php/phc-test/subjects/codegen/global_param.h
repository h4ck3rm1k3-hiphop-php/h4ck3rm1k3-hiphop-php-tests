
#ifndef __GENERATED_php_phc_test_subjects_codegen_global_param_h__
#define __GENERATED_php_phc_test_subjects_codegen_global_param_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/global_param.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_a(int64 v_GLOBALS);
Variant pm_php$phc_test$subjects$codegen$global_param_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_global_param_h__
