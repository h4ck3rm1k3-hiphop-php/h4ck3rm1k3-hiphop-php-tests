
#include <php/phc-test/subjects/codegen/pre_post_op.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$pre_post_op_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/pre_post_op.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$pre_post_op_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = 10LL);
  (v_y = 10LL);
  (v_z = 10LL);
  (v_x = negate(v_x) + 1LL);
  (v_x = 2LL + v_y++);
  (v_x = 3LL + --v_z);
  LINE(15,x_var_dump(1, v_x));
  LINE(16,x_var_dump(1, v_y));
  LINE(17,x_var_dump(1, v_z));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
