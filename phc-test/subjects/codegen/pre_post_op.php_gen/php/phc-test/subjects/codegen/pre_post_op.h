
#ifndef __GENERATED_php_phc_test_subjects_codegen_pre_post_op_h__
#define __GENERATED_php_phc_test_subjects_codegen_pre_post_op_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/pre_post_op.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$pre_post_op_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_pre_post_op_h__
