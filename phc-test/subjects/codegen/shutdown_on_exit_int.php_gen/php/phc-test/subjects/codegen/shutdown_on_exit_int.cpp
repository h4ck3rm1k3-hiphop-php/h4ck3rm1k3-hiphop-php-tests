
#include <php/phc-test/subjects/codegen/shutdown_on_exit_int.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/shutdown_on_exit_int.php line 6 */
void f_shut() {
  FUNCTION_INJECTION(shut);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  echo("shutdown function called\n");
  echo(LINE(10,concat3("this is a later reference to the die paramter to check it wasnt altered", toString(gv_x), "\n")));
} /* function */
Variant i_shut(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x241EF789F0A47B7ALL, shut) {
    return (f_shut(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$codegen$shutdown_on_exit_int_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/shutdown_on_exit_int.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$shutdown_on_exit_int_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  LINE(3,x_register_shutdown_function(1, "shut"));
  (v_x = 252LL);
  f_exit(v_x);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
