
#include <php/phc-test/subjects/codegen/return_by_reference_evald_invocations.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/return_by_reference_evald_invocations.php line 6 */
Variant f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->GV(a);
} /* function */
/* SRC: phc-test/subjects/codegen/return_by_reference_evald_invocations.php line 11 */
Variant f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(a)));
} /* function */
Variant pm_php$phc_test$subjects$codegen$return_by_reference_evald_invocations_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/return_by_reference_evald_invocations.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$return_by_reference_evald_invocations_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  f_eval("$a = 5;");
  f_eval("$b = f();");
  f_eval("$b = $b + 1;");
  f_eval("var_export($a);");
  f_eval("var_export($b);");
  f_eval("$a = 5;");
  f_eval("$c = g();");
  f_eval("$c = $c + 1;");
  f_eval("var_export($a);");
  f_eval("var_export($c);");
  f_eval("$a = 5;");
  f_eval("$d =& f();");
  f_eval("$d = $d + 1;");
  f_eval("var_export($a);");
  f_eval("var_export($d);");
  f_eval("$a = 5;");
  f_eval("$e =& g();");
  f_eval("$e = $e + 1;");
  f_eval("var_export($a);");
  f_eval("var_export($e);");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
