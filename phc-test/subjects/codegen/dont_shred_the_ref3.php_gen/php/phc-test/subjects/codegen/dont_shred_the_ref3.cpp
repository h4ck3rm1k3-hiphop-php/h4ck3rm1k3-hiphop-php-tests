
#include <php/phc-test/subjects/codegen/dont_shred_the_ref3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/dont_shred_the_ref3.php line 7 */
void f_foo(Variant v_y) {
  FUNCTION_INJECTION(foo);
  (v_y = 7LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$dont_shred_the_ref3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/dont_shred_the_ref3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$dont_shred_the_ref3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = ScalarArrays::sa_[0]);
  LINE(13,f_foo(ref(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  LINE(14,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
