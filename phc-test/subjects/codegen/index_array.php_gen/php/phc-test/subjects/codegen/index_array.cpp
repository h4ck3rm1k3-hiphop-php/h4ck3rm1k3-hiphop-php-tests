
#include <php/phc-test/subjects/codegen/index_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$index_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/index_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$index_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = ScalarArrays::sa_[0]);
  LINE(3,x_var_dump(1, v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
