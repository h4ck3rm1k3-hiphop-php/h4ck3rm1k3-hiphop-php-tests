
#include <php/phc-test/subjects/codegen/leak_on_exit.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/leak_on_exit.php line 5 */
void f_f() {
  FUNCTION_INJECTION(f);
  ;
  f_exit(0LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$leak_on_exit_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/leak_on_exit.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$leak_on_exit_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(10,f_f());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
