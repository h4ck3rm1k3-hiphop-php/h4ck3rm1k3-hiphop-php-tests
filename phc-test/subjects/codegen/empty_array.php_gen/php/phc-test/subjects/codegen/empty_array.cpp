
#include <php/phc-test/subjects/codegen/empty_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$empty_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/empty_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$empty_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  (v_arr = ScalarArrays::sa_[0]);
  v_arr.append((1LL));
  v_arr.append((2LL));
  v_arr.append((3LL));
  LINE(6,x_var_dump(1, v_arr));
  (v_arr = ScalarArrays::sa_[0]);
  v_arr.append(("a"));
  v_arr.append(("b"));
  v_arr.append(("c"));
  LINE(12,x_var_dump(1, v_arr));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
