
#include <php/phc-test/subjects/codegen/indirect_global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/indirect_global.php line 3 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_x;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_x;
    VariableTable(Variant &r_x) : v_x(r_x) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 0:
          HASH_RETURN(0x04BFC205E59FA416LL, v_x,
                      x);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_x);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_x = "GLOBALS");
  variables->get(toString(v_x)).set("a", (1LL), 0x4292CEE227B9150ALL);
  (g->GV(b) = 2LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$indirect_global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/indirect_global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$indirect_global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  LINE(10,f_f());
  LINE(12,x_var_dump(1, v_b));
  (v_x = "GLOBALS");
  variables->get(toString(v_x)).set("c", (3LL), 0x32C769EE5C5509B0LL);
  (g->GV(d) = 4LL);
  LINE(17,x_var_dump(1, v_c));
  LINE(18,x_var_dump(1, v_d));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
