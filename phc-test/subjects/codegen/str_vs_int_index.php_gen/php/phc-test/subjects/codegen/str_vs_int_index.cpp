
#include <php/phc-test/subjects/codegen/str_vs_int_index.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_II = "II";
const StaticString k_IV = "IV";
const StaticString k_SI = "SI";
const StaticString k_SV = "SV";

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$str_vs_int_index_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/str_vs_int_index.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$str_vs_int_index_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_HTTP_COOKIE_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_COOKIE_VARS") : g->GV(HTTP_COOKIE_VARS);
  Variant &v_HTTP_ENV_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_ENV_VARS") : g->GV(HTTP_ENV_VARS);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_HTTP_POST_FILES __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_FILES") : g->GV(HTTP_POST_FILES);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_HTTP_SERVER_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_SERVER_VARS") : g->GV(HTTP_SERVER_VARS);
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  {
    unset(v_HTTP_COOKIE_VARS);
    unset(v_HTTP_ENV_VARS);
    unset(v_HTTP_GET_VARS);
  }
  {
    unset(v_HTTP_POST_FILES);
    unset(v_HTTP_POST_VARS);
    unset(v_HTTP_SERVER_VARS);
  }
  {
    unset(g->gv__COOKIE);
    unset(g->gv__ENV);
    unset(g->gv__GET);
    unset(g->gv__SERVER);
    unset(g->gv__FILES);
    unset(g->gv__REQUEST);
    unset(g->gv__POST);
  }
  {
    unset(v_argv);
    unset(v_argc);
  }
  LINE(10,throw_fatal("bad define"));
  LINE(11,throw_fatal("bad define"));
  LINE(12,throw_fatal("bad define"));
  LINE(13,throw_fatal("bad define"));
  echo("ARRAY INDEXING - int first\n");
  v_a.set(k_II, (k_IV));
  v_a.set(k_SI, (k_SV));
  LINE(18,x_var_dump(1, v_a));
  unset(v_a);
  echo("ARRAY INDEXING - string first\n");
  v_a.set(k_SI, (k_SV));
  v_a.set(k_II, (k_IV));
  LINE(24,x_var_dump(1, v_a));
  unset(v_a);
  echo("VAR-FIELDS - string first\n");
  (v_a = ((Object)(LINE(28,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (v_a.o_lval(k_II, -1LL) = k_IV);
  (v_a.o_lval(k_SI, -1LL) = k_SV);
  LINE(31,x_var_dump(1, v_a));
  unset(v_a);
  echo("VAR-FIELDS - int first\n");
  (v_a = ((Object)(LINE(35,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (v_a.o_lval(k_II, -1LL) = k_IV);
  (v_a.o_lval(k_SI, -1LL) = k_SV);
  LINE(38,x_var_dump(1, v_a));
  unset(v_a);
  echo("VAR-VARS - int first\n");
  (variables->get(k_II) = k_IV);
  (variables->get(k_SI) = k_SV);
  LINE(44,x_var_dump(1, get_global_array_wrapper()));
  {
    unset(variables->get(k_II));
    unset(variables->get(k_SI));
  }
  echo("VAR-VARS - string first\n");
  (variables->get(k_SI) = k_SV);
  (variables->get(k_II) = k_IV);
  LINE(50,x_var_dump(1, get_global_array_wrapper()));
  {
    unset(variables->get(k_II));
    unset(variables->get(k_SI));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
