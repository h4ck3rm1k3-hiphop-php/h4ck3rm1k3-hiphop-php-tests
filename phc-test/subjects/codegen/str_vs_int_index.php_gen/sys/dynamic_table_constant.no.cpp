
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_II;
extern const StaticString k_IV;
extern const StaticString k_SI;
extern const StaticString k_SV;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x29669E4DA4022740LL, k_SI, SI);
      break;
    case 4:
      HASH_RETURN(0x56ADD9F3045116E4LL, k_IV, IV);
      break;
    case 5:
      HASH_RETURN(0x38D4F8E5F46EAC0DLL, k_SV, SV);
      break;
    case 6:
      HASH_RETURN(0x0AF116278A5E96B6LL, k_II, II);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
