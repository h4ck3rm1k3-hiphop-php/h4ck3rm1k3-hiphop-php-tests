
#ifndef __GENERATED_php_phc_test_subjects_codegen_oop_static_invocation7_h__
#define __GENERATED_php_phc_test_subjects_codegen_oop_static_invocation7_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/oop_static_invocation7.fw.h>

// Declarations
#include <cls/c.h>
#include <cls/d.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$oop_static_invocation7_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_c(CArrRef params, bool init = true);
Object co_d(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_oop_static_invocation7_h__
