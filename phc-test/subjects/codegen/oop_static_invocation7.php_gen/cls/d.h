
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__

#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_static_invocation7.php line 12 */
class c_d : virtual public c_c {
  BEGIN_CLASS_MAP(d)
    PARENT_CLASS(c)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, c)
  void init();
  public: static void ti_bar(const char* cls);
  public: static void t_bar() { ti_bar("d"); }
  public: static void t_foo() { ti_foo("d"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
