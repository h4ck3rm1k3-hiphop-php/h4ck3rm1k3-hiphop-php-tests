
#include <php/phc-test/subjects/codegen/assignment2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$assignment2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/assignment2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$assignment2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_Ba __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ba") : g->GV(Ba);
  Variant &v_Bb __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bb") : g->GV(Bb);
  Variant &v_Bc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bc") : g->GV(Bc);
  Variant &v_Bd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bd") : g->GV(Bd);
  Variant &v_Be __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Be") : g->GV(Be);
  Variant &v_Bf __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bf") : g->GV(Bf);
  Variant &v_Bg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bg") : g->GV(Bg);
  Variant &v_Bh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bh") : g->GV(Bh);
  Variant &v_Bi __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bi") : g->GV(Bi);
  Variant &v_Bj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Bj") : g->GV(Bj);

  v_Ba.set(0LL, (10LL), 0x77CFA1EEF01BCA90LL);
  v_Bb.set(0LL, (v_Ba.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x77CFA1EEF01BCA90LL);
  v_Bb.set(0LL, (v_Bb.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) + 1LL), 0x77CFA1EEF01BCA90LL);
  LINE(18,x_var_export(v_Ba));
  LINE(19,x_var_export(v_Bb));
  v_Bc.set(0LL, (20LL), 0x77CFA1EEF01BCA90LL);
  v_Bd.set(0LL, (ref(lval(v_Bc.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), 0x77CFA1EEF01BCA90LL);
  v_Bd.set(0LL, (v_Bd.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) + 1LL), 0x77CFA1EEF01BCA90LL);
  LINE(25,x_var_export(v_Bc));
  LINE(26,x_var_export(v_Bd));
  v_Be.set(0LL, (30LL), 0x77CFA1EEF01BCA90LL);
  v_Bf.set(0LL, (ref(lval(v_Be.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), 0x77CFA1EEF01BCA90LL);
  v_Bg.set(0LL, (v_Bf.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x77CFA1EEF01BCA90LL);
  v_Bg.set(0LL, (v_Bg.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) + 1LL), 0x77CFA1EEF01BCA90LL);
  LINE(33,x_var_export(v_Be));
  LINE(34,x_var_export(v_Bf));
  LINE(35,x_var_export(v_Bg));
  v_Bh.set(0LL, (40LL), 0x77CFA1EEF01BCA90LL);
  v_Bi.set(0LL, (v_Bh.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x77CFA1EEF01BCA90LL);
  v_Bj.set(0LL, (ref(lval(v_Bi.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), 0x77CFA1EEF01BCA90LL);
  v_Bj.set(0LL, (v_Bj.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) + 1LL), 0x77CFA1EEF01BCA90LL);
  LINE(42,x_var_export(v_Bh));
  LINE(43,x_var_export(v_Bi));
  LINE(44,x_var_export(v_Bj));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
