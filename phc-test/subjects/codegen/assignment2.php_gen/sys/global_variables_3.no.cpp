
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x72C563DE4F46D389LL, Bi, 20);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      HASH_INDEX(0x2415249C7D92674ELL, Bg, 18);
      break;
    case 15:
      HASH_INDEX(0x7B23A5DA61AD624FLL, Bc, 14);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 20:
      HASH_INDEX(0x1FACA080A833E094LL, Bb, 13);
      break;
    case 22:
      HASH_INDEX(0x561FCDE362517FD6LL, Bf, 17);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 37:
      HASH_INDEX(0x497948E5A1CAAC25LL, Bd, 15);
      break;
    case 45:
      HASH_INDEX(0x36C5C0BA7F8EEB2DLL, Ba, 12);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 47:
      HASH_INDEX(0x5B9E849F52B70A2FLL, Be, 16);
      break;
    case 51:
      HASH_INDEX(0x26C3DEDE04B49AB3LL, Bh, 19);
      break;
    case 60:
      HASH_INDEX(0x5E8152CB9163673CLL, Bj, 21);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 22) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
