
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x72C563DE4F46D389LL, g->GV(Bi),
                       Bi);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      HASH_INITIALIZED(0x2415249C7D92674ELL, g->GV(Bg),
                       Bg);
      break;
    case 15:
      HASH_INITIALIZED(0x7B23A5DA61AD624FLL, g->GV(Bc),
                       Bc);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 20:
      HASH_INITIALIZED(0x1FACA080A833E094LL, g->GV(Bb),
                       Bb);
      break;
    case 22:
      HASH_INITIALIZED(0x561FCDE362517FD6LL, g->GV(Bf),
                       Bf);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 37:
      HASH_INITIALIZED(0x497948E5A1CAAC25LL, g->GV(Bd),
                       Bd);
      break;
    case 45:
      HASH_INITIALIZED(0x36C5C0BA7F8EEB2DLL, g->GV(Ba),
                       Ba);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 47:
      HASH_INITIALIZED(0x5B9E849F52B70A2FLL, g->GV(Be),
                       Be);
      break;
    case 51:
      HASH_INITIALIZED(0x26C3DEDE04B49AB3LL, g->GV(Bh),
                       Bh);
      break;
    case 60:
      HASH_INITIALIZED(0x5E8152CB9163673CLL, g->GV(Bj),
                       Bj);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
