
#include <php/phc-test/subjects/codegen/switch_multi_defaults.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$switch_multi_defaults_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_multi_defaults.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_multi_defaults_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  {
    int64 tmp2 = (7LL);
    int tmp3 = -1;
    if (equal(tmp2, (7LL))) {
      tmp3 = 0;
    } else if (equal(tmp2, (8LL))) {
      tmp3 = 2;
    } else if (true) {
      tmp3 = 3;
    }
    switch (tmp3) {
    case 0:
      {
        echo("seven\n");
      }
    case 1:
      {
        echo("default1\n");
        goto break1;
      }
    case 2:
      {
        echo("eight");
      }
    case 3:
      {
        echo("default2\n");
        goto break1;
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
