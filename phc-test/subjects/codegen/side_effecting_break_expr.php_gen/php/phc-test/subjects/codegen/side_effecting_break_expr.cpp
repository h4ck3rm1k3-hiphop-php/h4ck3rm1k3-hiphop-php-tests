
#include <php/phc-test/subjects/codegen/side_effecting_break_expr.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/side_effecting_break_expr.php line 4 */
int64 f_x() {
  FUNCTION_INJECTION(x);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_x __attribute__((__unused__)) = g->sv_x_DupIdx;
  bool &inited_sv_x __attribute__((__unused__)) = g->inited_sv_x_DupIdx;
  if (!inited_sv_x) {
    (sv_x = 0LL);
    inited_sv_x = true;
  }
  echo(toString(sv_x++));
  return 0LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$side_effecting_break_expr_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/side_effecting_break_expr.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$side_effecting_break_expr_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        if ((LINE(14,f_x()))<2) {
        goto break1;
        } else {
        throw_fatal("bad break");
        }
        echo("error\n");
      }
    } while (false);
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
