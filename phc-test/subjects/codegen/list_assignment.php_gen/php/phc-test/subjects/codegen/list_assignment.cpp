
#include <php/phc-test/subjects/codegen/list_assignment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
Variant a1 = a0.rvalAt(0);
  p0 = a1.rvalAt(0);
  p1 = a1.rvalAt(1);
  p2 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$list_assignment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/list_assignment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$list_assignment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);

  df_lambda_1(ScalarArrays::sa_[0], v_a, v_b, v_c);
  df_lambda_2(ScalarArrays::sa_[1], v_d, v_e);
  df_lambda_3(ScalarArrays::sa_[2], v_f, v_g, v_h);
  LINE(6,x_var_dump(1, v_a));
  LINE(7,x_var_dump(1, v_b));
  LINE(8,x_var_dump(1, v_c));
  LINE(9,x_var_dump(1, v_d));
  LINE(10,x_var_dump(1, v_e));
  LINE(11,x_var_dump(1, v_f));
  LINE(12,x_var_dump(1, v_g));
  LINE(13,x_var_dump(1, v_h));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
