
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$codegen$include_once_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$codegen$add_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$codegen$just_int_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_INCLUDE(0x51BB9217117AB434LL, "phc-test/subjects/codegen/just_int.php", php$phc_test$subjects$codegen$just_int_php);
      break;
    case 6:
      HASH_INCLUDE(0x514A7CD92C604B2ELL, "phc-test/subjects/codegen/add.php", php$phc_test$subjects$codegen$add_php);
      break;
    case 7:
      HASH_INCLUDE(0x2DFB24F12106153FLL, "phc-test/subjects/codegen/include_once.php", php$phc_test$subjects$codegen$include_once_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
