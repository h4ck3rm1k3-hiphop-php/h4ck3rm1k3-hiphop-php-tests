
#include <php/phc-test/subjects/codegen/add.h>
#include <php/phc-test/subjects/codegen/include_once.h>
#include <php/phc-test/subjects/codegen/just_int.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$include_once_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/include_once.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$include_once_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_var_dump(1, pm_php$phc_test$subjects$codegen$add_php(true, variables)));
  LINE(4,x_var_dump(1, pm_php$phc_test$subjects$codegen$add_php(true, variables)));
  LINE(6,x_var_dump(1, pm_php$phc_test$subjects$codegen$just_int_php(true, variables)));
  LINE(7,x_var_dump(1, pm_php$phc_test$subjects$codegen$just_int_php(true, variables)));
  LINE(9,x_var_dump(1, pm_php$phc_test$subjects$codegen$just_int_php(false, variables)));
  LINE(10,x_var_dump(1, pm_php$phc_test$subjects$codegen$add_php(false, variables)));
  LINE(12,x_var_dump(1, include("not_a_real_file.php", false, variables, "phc-test/subjects/codegen/")));
  LINE(13,x_var_dump(1, include("not_a_real_file.php", true, variables, "phc-test/subjects/codegen/")));
  LINE(14,x_var_dump(1, require("not_a_real_file.php", true, variables, "phc-test/subjects/codegen/")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
