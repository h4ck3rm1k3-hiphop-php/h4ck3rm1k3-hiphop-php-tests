
#include <php/phc-test/subjects/codegen/include_fail.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$include_fail_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/include_fail.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$include_fail_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_int __attribute__((__unused__)) = (variables != gVariables) ? variables->get("int") : g->GV(int);

  LINE(3,include("no_such_file.php", false, variables, "phc-test/subjects/codegen/"));
  (v_int = 7LL);
  LINE(6,include(toString((toString(v_int))), false, variables, "phc-test/subjects/codegen/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
