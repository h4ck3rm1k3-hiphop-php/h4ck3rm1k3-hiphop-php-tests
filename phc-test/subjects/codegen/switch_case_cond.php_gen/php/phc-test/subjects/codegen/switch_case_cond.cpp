
#include <php/phc-test/subjects/codegen/switch_case_cond.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/switch_case_cond.php line 20 */
void f_f() {
  FUNCTION_INJECTION(f);
  echo("f\n");
} /* function */
/* SRC: phc-test/subjects/codegen/switch_case_cond.php line 21 */
void f_g() {
  FUNCTION_INJECTION(g);
  echo("g\n");
} /* function */
/* SRC: phc-test/subjects/codegen/switch_case_cond.php line 22 */
void f_h() {
  FUNCTION_INJECTION(h);
  echo("h\n");
} /* function */
Variant pm_php$phc_test$subjects$codegen$switch_case_cond_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_case_cond.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_case_cond_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 1LL);
  {
    Variant tmp2 = (v_x);
    int tmp3 = -1;
    if (equal(tmp2, (0LL))) {
      tmp3 = 0;
    } else if (equal(tmp2, (((LINE(11,f_f()), null) ? ((Variant)((f_g(), null))) : ((Variant)((f_h(), null))))))) {
      tmp3 = 1;
    } else if (true) {
      tmp3 = 2;
    }
    switch (tmp3) {
    case 0:
      {
        echo("not done\n");
        goto break1;
      }
    case 1:
      {
        echo("done\n");
        goto break1;
      }
    case 2:
      {
        echo("default\n");
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
