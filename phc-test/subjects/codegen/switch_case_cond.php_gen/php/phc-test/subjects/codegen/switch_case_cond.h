
#ifndef __GENERATED_php_phc_test_subjects_codegen_switch_case_cond_h__
#define __GENERATED_php_phc_test_subjects_codegen_switch_case_cond_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/switch_case_cond.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_f();
void f_g();
void f_h();
Variant pm_php$phc_test$subjects$codegen$switch_case_cond_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_switch_case_cond_h__
