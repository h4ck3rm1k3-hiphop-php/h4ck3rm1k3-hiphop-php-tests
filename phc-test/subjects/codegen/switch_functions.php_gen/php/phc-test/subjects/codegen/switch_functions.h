
#ifndef __GENERATED_php_phc_test_subjects_codegen_switch_functions_h__
#define __GENERATED_php_phc_test_subjects_codegen_switch_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/switch_functions.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_f(CVarRef v_count);
Variant pm_php$phc_test$subjects$codegen$switch_functions_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_switch_functions_h__
