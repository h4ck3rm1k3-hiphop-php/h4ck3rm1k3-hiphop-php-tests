
#include <php/phc-test/subjects/codegen/switch_functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/switch_functions.php line 3 */
Variant f_f(CVarRef v_count) {
  FUNCTION_INJECTION(f);
  echo(LINE(5,concat3("f", toString(v_count), "\n")));
  return v_count;
} /* function */
Variant i_f(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x286CE1C477560280LL, f) {
    return (f_f(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$codegen$switch_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          Variant tmp3 = (v_i);
          int tmp4 = -1;
          if (equal(tmp3, (LINE(13,f_f(1LL))))) {
            tmp4 = 0;
          } else if (equal(tmp3, (LINE(17,f_f(2LL))))) {
            tmp4 = 1;
          } else if (equal(tmp3, (LINE(21,f_f(3LL))))) {
            tmp4 = 2;
          } else if (equal(tmp3, (LINE(26,f_f(7LL))))) {
            tmp4 = 3;
          } else if (equal(tmp3, (LINE(29,f_f(4LL))))) {
            tmp4 = 4;
          } else if (equal(tmp3, (LINE(33,f_f(5LL))))) {
            tmp4 = 5;
          } else if (equal(tmp3, (LINE(37,f_f(6LL))))) {
            tmp4 = 6;
          } else if (equal(tmp3, (LINE(41,f_f(8LL))))) {
            tmp4 = 7;
          } else if (equal(tmp3, (LINE(45,f_f(9LL))))) {
            tmp4 = 8;
          } else if (equal(tmp3, (LINE(49,f_f(0LL))))) {
            tmp4 = 9;
          } else if (true) {
            tmp4 = 10;
          }
          switch (tmp4) {
          case 0:
            {
              echo("1\n");
              goto break2;
            }
          case 1:
            {
              echo("2\n");
              goto break2;
            }
          case 2:
            {
              echo("3\n");
              goto break2;
            }
          case 3:
            {
              echo("7\n");
              goto break2;
            }
          case 4:
            {
              echo("4\n");
              goto break2;
            }
          case 5:
            {
              echo("5\n");
              goto break2;
            }
          case 6:
            {
              echo("6\n");
              goto break2;
            }
          case 7:
            {
              echo("8\n");
              goto break2;
            }
          case 8:
            {
              echo("9\n");
              goto break2;
            }
          case 9:
            {
              echo("0\n");
              goto break2;
            }
          case 10:
            {
              echo("default\n");
              goto break2;
            }
          }
          break2:;
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
