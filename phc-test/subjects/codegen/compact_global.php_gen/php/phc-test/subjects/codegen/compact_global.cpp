
#include <php/phc-test/subjects/codegen/compact_global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$compact_global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/compact_global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$compact_global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  LINE(5,x_var_dump(1, compact(variables, 1, Array(ArrayInit(1).set(0, "x").create()))));
  LINE(7,x_var_dump(1, compact(variables, 1, Array(ArrayInit(1).set(0, "x").create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
