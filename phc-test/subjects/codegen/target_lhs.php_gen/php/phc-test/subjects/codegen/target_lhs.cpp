
#include <php/phc-test/subjects/codegen/target_lhs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/target_lhs.php line 3 */
Variant c_x::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("z", m_z.isReferenced() ? ref(m_z) : m_z));
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    case 3:
      HASH_EXISTS_STRING(0x62A103F6518DE2B3LL, z, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    case 3:
      HASH_SET_STRING(0x62A103F6518DE2B3LL, m_z,
                      z, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  clone->m_z = m_z.isReferenced() ? ref(m_z) : m_z;
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
  m_z = ScalarArrays::sa_[0];
  m_x = ScalarArrays::sa_[1];
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$target_lhs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/target_lhs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$target_lhs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_t = ((Object)(LINE(10,p_x(p_x(NEWOBJ(c_x)())->create())))));
  (v_y = "z");
  (v_a = 0LL);
  (v_b = 1LL);
  LINE(15,x_var_dump(1, v_t));
  lval(lval(v_t.o_lval(toString(v_y), -1LL)).lvalAt(v_a)).set(v_b, (v_t.o_get("x", 0x04BFC205E59FA416LL).rvalAt("x", 0x04BFC205E59FA416LL)));
  LINE(18,x_var_dump(1, v_t));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
