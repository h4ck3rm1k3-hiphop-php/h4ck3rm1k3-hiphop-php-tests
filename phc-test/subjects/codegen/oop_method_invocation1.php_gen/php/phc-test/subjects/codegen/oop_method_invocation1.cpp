
#include <php/phc-test/subjects/codegen/oop_method_invocation1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$oop_method_invocation1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_method_invocation1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_method_invocation1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  f_eval(toString("\tclass C\n\t{\n\t\tfunction foo()\n\t\t{\n\t\t\techo \"C::foo\n\";\n\t\t}\n\t}\n\n\tclass D\n\t{\n\t\tfunction foo()\n\t\t{\n\t\t\techo \"D::foo\n\";\n\t\t}\n\t}\n\n\t$c = new C(); \n\t$d = new D();"));
  LINE(25,v_c.o_invoke_few_args("foo", 0x6176C0B993BF5914LL, 0));
  LINE(26,v_d.o_invoke_few_args("foo", 0x6176C0B993BF5914LL, 0));
  LINE(28,x_var_dump(1, v_c));
  LINE(29,x_var_dump(1, v_d));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
