
#ifndef __GENERATED_php_phc_test_subjects_codegen_simple_null_array_h__
#define __GENERATED_php_phc_test_subjects_codegen_simple_null_array_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/simple_null_array.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$simple_null_array_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_simple_null_array_h__
