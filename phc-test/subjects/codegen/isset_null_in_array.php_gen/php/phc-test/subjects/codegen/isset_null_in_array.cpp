
#include <php/phc-test/subjects/codegen/isset_null_in_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$isset_null_in_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_null_in_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_null_in_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  if (isset(v_a, 1LL, 0x5BCA7C69B794F8CELL)) echo("$a[1] is set\n");
  else echo("$a[1] is not set\n");
  v_a.set(1LL, (2LL), 0x5BCA7C69B794F8CELL);
  if (isset(v_a, 1LL, 0x5BCA7C69B794F8CELL)) echo("$a[1] is set\n");
  else echo("$a[1] is not set\n");
  v_a.set(1LL, (null), 0x5BCA7C69B794F8CELL);
  if (isset(v_a, 1LL, 0x5BCA7C69B794F8CELL)) echo("$a[1] is set\n");
  else echo("$a[1] is not set\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
