
#include <php/phc-test/subjects/codegen/multi_array_access.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$multi_array_access_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/multi_array_access.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$multi_array_access_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_p __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p") : g->GV(p);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  setNull(v_p);
  LINE(19,x_var_dump(1, v_p));
  (v_t = ref(lval(v_p.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  LINE(21,x_var_dump(1, v_p));
  setNull(v_p);
  LINE(25,x_var_dump(1, v_p));
  (v_t = v_p.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  LINE(27,x_var_dump(1, v_p));
  (v_p = ScalarArrays::sa_[0]);
  LINE(33,x_var_dump(1, v_p));
  (v_t = ref(lval(v_p.lvalAt(2LL, 0x486AFCC090D5F98CLL))));
  LINE(35,x_var_dump(1, v_p));
  (v_p = ScalarArrays::sa_[0]);
  LINE(39,x_var_dump(1, v_p));
  (v_t = v_p.rvalAt(2LL, 0x486AFCC090D5F98CLL));
  LINE(41,x_var_dump(1, v_p));
  (v_x = ScalarArrays::sa_[1]);
  LINE(47,x_var_dump(1, v_x));
  (v_y = v_x.rvalAt(v_p.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(49,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
