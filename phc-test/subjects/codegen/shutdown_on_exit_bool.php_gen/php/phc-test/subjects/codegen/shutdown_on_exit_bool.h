
#ifndef __GENERATED_php_phc_test_subjects_codegen_shutdown_on_exit_bool_h__
#define __GENERATED_php_phc_test_subjects_codegen_shutdown_on_exit_bool_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/shutdown_on_exit_bool.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$shutdown_on_exit_bool_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_shut();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_shutdown_on_exit_bool_h__
