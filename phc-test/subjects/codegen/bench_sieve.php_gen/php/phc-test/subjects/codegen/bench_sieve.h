
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_sieve_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_sieve_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_sieve.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_sieve(int64 v_n);
Variant pm_php$phc_test$subjects$codegen$bench_sieve_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_sieve_h__
