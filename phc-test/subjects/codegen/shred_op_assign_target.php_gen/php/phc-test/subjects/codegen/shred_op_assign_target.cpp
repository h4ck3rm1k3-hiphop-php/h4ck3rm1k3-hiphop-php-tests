
#include <php/phc-test/subjects/codegen/shred_op_assign_target.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/shred_op_assign_target.php line 12 */
Variant c_x::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x2", m_x2.isReferenced() ? ref(m_x2) : m_x2));
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x5075985E0413CB39LL, x2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x5075985E0413CB39LL, m_x2,
                         x2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x5075985E0413CB39LL, m_x2,
                      x2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x5075985E0413CB39LL, m_x2,
                         x2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  clone->m_x2 = m_x2.isReferenced() ? ref(m_x2) : m_x2;
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
  m_x2 = ScalarArrays::sa_[0];
}
/* SRC: phc-test/subjects/codegen/shred_op_assign_target.php line 13 */
Variant c_y::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_y::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_y::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x5", m_x5.isReferenced() ? ref(m_x5) : m_x5));
  c_ObjectData::o_get(props);
}
bool c_y::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7A702ABECDB89E18LL, x5, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_y::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A702ABECDB89E18LL, m_x5,
                         x5, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_y::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7A702ABECDB89E18LL, m_x5,
                      x5, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_y::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A702ABECDB89E18LL, m_x5,
                         x5, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_y::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(y)
ObjectData *c_y::cloneImpl() {
  c_y *obj = NEW(c_y)();
  cloneSet(obj);
  return obj;
}
void c_y::cloneSet(c_y *clone) {
  clone->m_x5 = m_x5.isReferenced() ? ref(m_x5) : m_x5;
  ObjectData::cloneSet(clone);
}
Variant c_y::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_y::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_y::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_y$os_get(const char *s) {
  return c_y::os_get(s, -1);
}
Variant &cw_y$os_lval(const char *s) {
  return c_y::os_lval(s, -1);
}
Variant cw_y$os_constant(const char *s) {
  return c_y::os_constant(s);
}
Variant cw_y$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_y::os_invoke(c, s, params, -1, fatal);
}
void c_y::init() {
  m_x5 = ScalarArrays::sa_[1];
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Object co_y(CArrRef params, bool init /* = true */) {
  return Object(p_y(NEW(c_y)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$shred_op_assign_target_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/shred_op_assign_target.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$shred_op_assign_target_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_x1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x1") : g->GV(x1);
  Variant &v_x7 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x7") : g->GV(x7);
  Variant &v_x2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x2") : g->GV(x2);
  Variant &v_x3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x3") : g->GV(x3);
  Variant &v_x4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x4") : g->GV(x4);
  Variant &v_x5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x5") : g->GV(x5);
  Variant &v_x6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x6") : g->GV(x6);
  Variant &v_x8 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x8") : g->GV(x8);

  (v_a = "x1");
  (v_b = "x2");
  (v_c = "x3");
  (v_d = "x4");
  (v_e = "x5");
  (v_f = "x6");
  (v_g = "x7");
  (v_h = "x8");
  (v_x1 = ((Object)(LINE(14,p_x(p_x(NEWOBJ(c_x)())->create())))));
  lval(lval(v_x1.o_lval("x2", 0x5075985E0413CB39LL)).lvalAt("x3", 0x0C058190047906E6LL)).set("x4", (((Object)(LINE(15,p_y(p_y(NEWOBJ(c_y)())->create()))))), 0x7DC755B00FC9EF46LL);
  (v_x7 = ScalarArrays::sa_[2]);
  lval(lval(lval(variables->get(toString(v_a)).o_lval(toString(v_b), -1LL)).lvalAt(variables->get(toString(v_c)))).lvalAt(variables->get(toString(v_d)))).o_lval(toString(v_e.rvalAt(variables->get(toString(v_f)))), -1LL) += variables->get(toString(v_g.rvalAt(variables->get(toString(v_h)))));
  LINE(21,x_var_dump(1, v_x1));
  LINE(22,x_var_dump(1, v_x2));
  LINE(23,x_var_dump(1, v_x3));
  LINE(24,x_var_dump(1, v_x4));
  LINE(25,x_var_dump(1, v_x5));
  LINE(26,x_var_dump(1, v_x6));
  LINE(27,x_var_dump(1, v_x7));
  LINE(28,x_var_dump(1, v_x8));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
