
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 4:
      HASH_INDEX(0x7A452383AA9BF7C4LL, d, 15);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x7DC755B00FC9EF46LL, x4, 24);
      break;
    case 8:
      HASH_INDEX(0x2BCCE9AD79BC2688LL, g, 18);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 12);
      break;
    case 13:
      HASH_INDEX(0x18999E7F825574CDLL, x7, 21);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x09B9AFEE14F19D50LL, x6, 26);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 13);
      break;
    case 24:
      HASH_INDEX(0x7A702ABECDB89E18LL, x5, 25);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 38:
      HASH_INDEX(0x0C058190047906E6LL, x3, 23);
      break;
    case 43:
      HASH_INDEX(0x61B161496B7EA7EBLL, e, 16);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      HASH_INDEX(0x6BB4A0689FBAD42ELL, f, 17);
      break;
    case 48:
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 14);
      HASH_INDEX(0x695875365480A8F0LL, h, 19);
      break;
    case 57:
      HASH_INDEX(0x5075985E0413CB39LL, x2, 22);
      break;
    case 61:
      HASH_INDEX(0x5685725E6F24EAFDLL, x1, 20);
      break;
    case 62:
      HASH_INDEX(0x3BB3A9560BBDB87ELL, x8, 27);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 28) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
