
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "x1",
    "x7",
    "x2",
    "x3",
    "x4",
    "x5",
    "x6",
    "x8",
  };
  if (idx >= 0 && idx < 28) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(a);
    case 13: return GV(b);
    case 14: return GV(c);
    case 15: return GV(d);
    case 16: return GV(e);
    case 17: return GV(f);
    case 18: return GV(g);
    case 19: return GV(h);
    case 20: return GV(x1);
    case 21: return GV(x7);
    case 22: return GV(x2);
    case 23: return GV(x3);
    case 24: return GV(x4);
    case 25: return GV(x5);
    case 26: return GV(x6);
    case 27: return GV(x8);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
