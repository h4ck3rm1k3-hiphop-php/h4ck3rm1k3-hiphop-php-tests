
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 4:
      HASH_RETURN(0x7A452383AA9BF7C4LL, g->GV(d),
                  d);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x7DC755B00FC9EF46LL, g->GV(x4),
                  x4);
      break;
    case 8:
      HASH_RETURN(0x2BCCE9AD79BC2688LL, g->GV(g),
                  g);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 10:
      HASH_RETURN(0x4292CEE227B9150ALL, g->GV(a),
                  a);
      break;
    case 13:
      HASH_RETURN(0x18999E7F825574CDLL, g->GV(x7),
                  x7);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      HASH_RETURN(0x09B9AFEE14F19D50LL, g->GV(x6),
                  x6);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 21:
      HASH_RETURN(0x08FBB133F8576BD5LL, g->GV(b),
                  b);
      break;
    case 24:
      HASH_RETURN(0x7A702ABECDB89E18LL, g->GV(x5),
                  x5);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 38:
      HASH_RETURN(0x0C058190047906E6LL, g->GV(x3),
                  x3);
      break;
    case 43:
      HASH_RETURN(0x61B161496B7EA7EBLL, g->GV(e),
                  e);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      HASH_RETURN(0x6BB4A0689FBAD42ELL, g->GV(f),
                  f);
      break;
    case 48:
      HASH_RETURN(0x32C769EE5C5509B0LL, g->GV(c),
                  c);
      HASH_RETURN(0x695875365480A8F0LL, g->GV(h),
                  h);
      break;
    case 57:
      HASH_RETURN(0x5075985E0413CB39LL, g->GV(x2),
                  x2);
      break;
    case 61:
      HASH_RETURN(0x5685725E6F24EAFDLL, g->GV(x1),
                  x1);
      break;
    case 62:
      HASH_RETURN(0x3BB3A9560BBDB87ELL, g->GV(x8),
                  x8);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
