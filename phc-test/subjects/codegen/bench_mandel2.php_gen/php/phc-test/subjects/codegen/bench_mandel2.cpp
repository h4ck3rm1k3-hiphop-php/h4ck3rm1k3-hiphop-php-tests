
#include <php/phc-test/subjects/codegen/bench_mandel2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_mandel2.php line 3 */
void f_mandel2() {
  FUNCTION_INJECTION(mandel2);
  String v_b;
  int64 v_y = 0;
  double v_C = 0.0;
  int64 v_x = 0;
  double v_c = 0.0;
  double v_z = 0.0;
  double v_Z = 0.0;
  double v_r = 0.0;
  double v_i = 0.0;
  int64 v_k = 0;
  double v_t = 0.0;

  (v_b = " .:,;!/>)|&IH%*#");
  {
    LOOP_COUNTER(1);
    for ((v_y = 30LL); toBoolean(LINE(6,x_printf(1, "\n"))), toBoolean((v_C = v_y * 0.10000000000000001 - 1.5)), toBoolean(v_y--); ) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_x = 0LL); toBoolean((v_c = v_x * 0.040000000000000001 - 2LL)), toBoolean((v_z = toDouble(0LL))), toBoolean((v_Z = toDouble(0LL))), less(v_x++, 7LL); ) {
            LOOP_COUNTER_CHECK(2);
            {
              {
                LOOP_COUNTER(3);
                for ((v_r = v_c), (v_i = v_C), (v_k = 0LL); toBoolean((v_t = v_z * v_z - v_Z * v_Z + v_r)), toBoolean((v_Z = 2LL * v_z * v_Z + v_i)), toBoolean((v_z = v_t)), less(v_k, 50LL); v_k++) {
                  LOOP_COUNTER_CHECK(3);
                  if (more(v_z * v_z + v_Z * v_Z, 50LL)) break;
                }
              }
              echo(v_b.rvalAt(modulo(v_k, 16LL)));
            }
          }
        }
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_mandel2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_mandel2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_mandel2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(15,f_mandel2());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
