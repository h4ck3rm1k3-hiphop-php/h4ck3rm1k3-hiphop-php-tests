
#ifndef __GENERATED_php_phc_test_subjects_codegen_variable_global_h__
#define __GENERATED_php_phc_test_subjects_codegen_variable_global_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/variable_global.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$variable_global_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f(String v_x);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_variable_global_h__
