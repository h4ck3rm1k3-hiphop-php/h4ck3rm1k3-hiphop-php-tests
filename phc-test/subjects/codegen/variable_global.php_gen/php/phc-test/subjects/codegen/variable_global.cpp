
#include <php/phc-test/subjects/codegen/variable_global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/variable_global.php line 2 */
void f_f(String v_x) {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 v_y = 0;


  class VariableTable : public RVariableTable {
  public:
    String &v_x; int64 &v_y;
    VariableTable(String &r_x, int64 &r_y) : v_x(r_x), v_y(r_y) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x04BFC205E59FA416LL, v_x,
                      x);
          HASH_RETURN(0x4F56B733A4DFC78ALL, v_y,
                      y);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_x, v_y);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  throw_fatal("dynamic global");
  (v_y = 5LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$variable_global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/variable_global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$variable_global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  LINE(8,f_f("y"));
  LINE(9,x_var_dump(1, v_y));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
