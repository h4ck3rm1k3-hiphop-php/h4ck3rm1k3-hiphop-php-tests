
#include <php/phc-test/subjects/codegen/foreach_shocking.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$foreach_shocking_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/foreach_shocking.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$foreach_shocking_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(v_x);
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(NULL, v_y); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      {
      }
    }
  }
  {
    LOOP_COUNTER(4);
    Variant map5 = ref(v_x);
    map5.escalate();
    for (MutableArrayIterPtr iter6 = map5.begin(&v_y, v_z); iter6->advance();) {
      LOOP_COUNTER_CHECK(4);
      {
        echo(LINE(14,concat4(toString(v_y), " => ", toString(v_z), "\n")));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
