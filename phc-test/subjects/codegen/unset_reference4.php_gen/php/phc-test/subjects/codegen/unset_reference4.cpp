
#include <php/phc-test/subjects/codegen/unset_reference4.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$unset_reference4_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/unset_reference4.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$unset_reference4_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = ScalarArrays::sa_[0]);
  (v_y = ref(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  (v_z = ref(v_y));
  LINE(8,x_var_dump(1, v_y));
  unset(v_y);
  LINE(10,x_var_dump(1, v_y));
  LINE(12,x_var_dump(1, v_z));
  (v_z = 5LL);
  LINE(14,x_var_dump(1, v_x));
  unset(v_z);
  LINE(17,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
