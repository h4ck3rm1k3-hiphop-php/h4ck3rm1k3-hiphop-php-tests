
#include <php/phc-test/subjects/codegen/simple_null_ref_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$simple_null_ref_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/simple_null_ref_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$simple_null_ref_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  v_x.set(0LL, (7LL), 0x77CFA1EEF01BCA90LL);
  LINE(5,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
