
#include <php/phc-test/subjects/codegen/return_global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/return_global.php line 7 */
Variant f_return_by_ref() {
  FUNCTION_INJECTION(return_by_ref);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(b).lvalAt("a", 0x4292CEE227B9150ALL)));
} /* function */
Variant pm_php$phc_test$subjects$codegen$return_global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/return_global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$return_global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_b = 7LL);
  (v_x = LINE(13,f_return_by_ref()));
  (v_x = 8LL);
  LINE(15,x_var_dump(1, v_b));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
