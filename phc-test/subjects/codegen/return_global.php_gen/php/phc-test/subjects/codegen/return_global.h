
#ifndef __GENERATED_php_phc_test_subjects_codegen_return_global_h__
#define __GENERATED_php_phc_test_subjects_codegen_return_global_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/return_global.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$return_global_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_return_by_ref();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_return_global_h__
