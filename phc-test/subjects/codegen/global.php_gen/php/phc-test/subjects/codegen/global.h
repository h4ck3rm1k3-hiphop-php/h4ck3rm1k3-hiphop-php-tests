
#ifndef __GENERATED_php_phc_test_subjects_codegen_global_h__
#define __GENERATED_php_phc_test_subjects_codegen_global_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/global.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_f();
void f_g();
Variant pm_php$phc_test$subjects$codegen$global_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_global_h__
