
#include <php/phc-test/subjects/codegen/global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/global.php line 2 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  (gv_x = 10LL);
} /* function */
/* SRC: phc-test/subjects/codegen/global.php line 8 */
void f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  (g->GV(y) = 20LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  LINE(13,f_f());
  LINE(14,x_var_dump(1, v_x));
  LINE(16,f_g());
  LINE(17,x_var_dump(1, v_y));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
