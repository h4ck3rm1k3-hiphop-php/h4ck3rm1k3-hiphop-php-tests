
#include <php/phc-test/subjects/codegen/unregistered_constant.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_MY_UNREGISTERED_CONST = "MY_UNREGISTERED_CONST";

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$unregistered_constant_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/unregistered_constant.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$unregistered_constant_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(LINE(3,concat3("ready, OK: ", k_MY_UNREGISTERED_CONST, "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
