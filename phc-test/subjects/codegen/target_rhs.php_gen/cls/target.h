
#ifndef __GENERATED_cls_target_h__
#define __GENERATED_cls_target_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/target_rhs.php line 3 */
class c_target : virtual public ObjectData {
  BEGIN_CLASS_MAP(target)
  END_CLASS_MAP(target)
  DECLARE_CLASS(target, Target, ObjectData)
  void init();
  public: Variant m_y;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_target_h__
