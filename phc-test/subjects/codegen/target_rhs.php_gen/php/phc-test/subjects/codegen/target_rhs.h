
#ifndef __GENERATED_php_phc_test_subjects_codegen_target_rhs_h__
#define __GENERATED_php_phc_test_subjects_codegen_target_rhs_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/target_rhs.fw.h>

// Declarations
#include <cls/target.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$target_rhs_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_target(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_target_rhs_h__
