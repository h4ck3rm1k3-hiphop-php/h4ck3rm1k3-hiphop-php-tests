
#include <php/phc-test/subjects/codegen/target_rhs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/target_rhs.php line 3 */
Variant c_target::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_target::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_target::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("y", m_y.isReferenced() ? ref(m_y) : m_y));
  c_ObjectData::o_get(props);
}
bool c_target::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4F56B733A4DFC78ALL, y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_target::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_target::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4F56B733A4DFC78ALL, m_y,
                      y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_target::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_target::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(target)
ObjectData *c_target::cloneImpl() {
  c_target *obj = NEW(c_target)();
  cloneSet(obj);
  return obj;
}
void c_target::cloneSet(c_target *clone) {
  clone->m_y = m_y.isReferenced() ? ref(m_y) : m_y;
  ObjectData::cloneSet(clone);
}
Variant c_target::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_target::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_target::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_target$os_get(const char *s) {
  return c_target::os_get(s, -1);
}
Variant &cw_target$os_lval(const char *s) {
  return c_target::os_lval(s, -1);
}
Variant cw_target$os_constant(const char *s) {
  return c_target::os_constant(s);
}
Variant cw_target$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_target::os_invoke(c, s, params, -1, fatal);
}
void c_target::init() {
  m_y = 42LL;
}
Object co_target(CArrRef params, bool init /* = true */) {
  return Object(p_target(NEW(c_target)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$target_rhs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/target_rhs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$target_rhs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_T __attribute__((__unused__)) = (variables != gVariables) ? variables->get("T") : g->GV(T);

  (v_r = "y");
  (v_x = v_t.o_get(toString(v_y), -1LL));
  LINE(11,x_var_dump(1, v_x));
  (v_r = "y");
  (v_x = v_t.o_get(toString(variables->get(toString(v_r))), -1LL));
  LINE(16,x_var_dump(1, v_x));
  (v_x = v_t.o_get("y", 0x4F56B733A4DFC78ALL).rvalAt(v_i));
  LINE(22,x_var_dump(1, v_x));
  (v_T = v_t.o_get("y", 0x4F56B733A4DFC78ALL));
  (v_x = v_T.rvalAt(v_i));
  LINE(27,x_var_dump(1, v_x));
  (v_x = ref(lval(lval(v_t.o_lval("y", 0x4F56B733A4DFC78ALL)).lvalAt(v_i))));
  LINE(32,x_var_dump(1, v_x));
  (v_T = ref(lval(v_t.o_lval("y", 0x4F56B733A4DFC78ALL))));
  (v_x = ref(lval(v_T.lvalAt(v_i))));
  LINE(37,x_var_dump(1, v_x));
  (v_x = v_t.o_get(toString(v_r.rvalAt(v_i)), -1LL));
  LINE(43,x_var_dump(1, v_x));
  (v_T = v_t.o_get(toString(v_r), -1LL));
  (v_x = v_T.rvalAt(v_i));
  LINE(48,x_var_dump(1, v_x));
  (v_x = ref(lval(v_t.o_lval(toString(v_r.rvalAt(v_i)), -1LL))));
  LINE(53,x_var_dump(1, v_x));
  (v_T = ref(lval(v_t.o_lval(toString(v_r), -1LL))));
  (v_x = ref(lval(v_T.lvalAt(v_i))));
  LINE(58,x_var_dump(1, v_x));
  ++lval(v_t.o_lval(toString(v_r.rvalAt(v_i)), -1LL));
  LINE(64,x_var_dump(1, v_x));
  ++lval(v_t.o_lval("y", 0x4F56B733A4DFC78ALL));
  LINE(68,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
