
#include <php/phc-test/subjects/codegen/for_varying_cond.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/for_varying_cond.php line 9 */
bool f_f(CVarRef v_y) {
  FUNCTION_INJECTION(f);
  return (!equal(v_y, 7LL));
} /* function */
Variant pm_php$phc_test$subjects$codegen$for_varying_cond_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/for_varying_cond.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$for_varying_cond_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  {
    LOOP_COUNTER(1);
    for ((v_x = 0LL); LINE(4,f_f(v_x)); v_x++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(6,x_var_dump(1, v_x));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
