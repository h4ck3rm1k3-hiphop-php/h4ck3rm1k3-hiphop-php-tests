
#ifndef __GENERATED_php_phc_test_subjects_codegen_for_varying_cond_h__
#define __GENERATED_php_phc_test_subjects_codegen_for_varying_cond_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/for_varying_cond.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$for_varying_cond_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_f(CVarRef v_y);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_for_varying_cond_h__
