
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_class_attribute5.php line 2 */
class c_c : virtual public ObjectData {
  BEGIN_CLASS_MAP(c)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, C, ObjectData)
  void init();
  public: int64 m_x;
  public: String m_y;
  public: double m_z;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
