
#ifndef __GENERATED_php_phc_test_subjects_codegen_return_by_reference_sep_h__
#define __GENERATED_php_phc_test_subjects_codegen_return_by_reference_sep_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/return_by_reference_sep.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$return_by_reference_sep_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_f();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_return_by_reference_sep_h__
