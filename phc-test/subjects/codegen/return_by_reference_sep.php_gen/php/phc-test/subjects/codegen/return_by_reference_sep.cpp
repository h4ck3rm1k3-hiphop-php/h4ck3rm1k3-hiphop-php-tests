
#include <php/phc-test/subjects/codegen/return_by_reference_sep.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/return_by_reference_sep.php line 8 */
Variant f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(b)));
} /* function */
Variant pm_php$phc_test$subjects$codegen$return_by_reference_sep_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/return_by_reference_sep.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$return_by_reference_sep_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_a = 5LL);
  (v_b = v_a);
  (v_c = ref(LINE(13,f_f())));
  (v_c = 6LL);
  LINE(16,x_var_export(v_a));
  LINE(17,x_var_export(v_b));
  LINE(18,x_var_export(v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
