
#include <php/phc-test/subjects/codegen/if_nested.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$if_nested_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/if_nested.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$if_nested_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = 0LL);
  if (equal(v_x, 1LL)) {
    (v_y = 2LL);
    (v_z = 3LL);
    if (equal(v_y, 4LL)) {
      (v_z = 5LL);
    }
    else {
      (v_z = 6LL);
    }
  }
  else {
    (v_y = 7LL);
    if (equal(v_y, 8LL)) {
      (v_z = 9LL);
    }
    else {
      (v_z = 10LL);
    }
  }
  LINE(29,x_var_dump(1, v_x));
  LINE(30,x_var_dump(1, v_y));
  LINE(31,x_var_dump(1, v_z));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
