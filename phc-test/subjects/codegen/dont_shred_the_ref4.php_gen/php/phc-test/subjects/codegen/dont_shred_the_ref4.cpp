
#include <php/phc-test/subjects/codegen/dont_shred_the_ref4.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/dont_shred_the_ref4.php line 7 */
void f_foo(CVarRef v_y) {
  FUNCTION_INJECTION(foo);
} /* function */
Variant pm_php$phc_test$subjects$codegen$dont_shred_the_ref4_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/dont_shred_the_ref4.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$dont_shred_the_ref4_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = ScalarArrays::sa_[0]);
  LINE(12,f_foo(v_x.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(13,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
