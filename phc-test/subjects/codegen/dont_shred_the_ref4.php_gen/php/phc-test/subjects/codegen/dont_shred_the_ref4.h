
#ifndef __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref4_h__
#define __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref4_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/dont_shred_the_ref4.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foo(CVarRef v_y);
Variant pm_php$phc_test$subjects$codegen$dont_shred_the_ref4_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref4_h__
