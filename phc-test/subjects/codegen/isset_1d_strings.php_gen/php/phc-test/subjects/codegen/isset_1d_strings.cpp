
#include <php/phc-test/subjects/codegen/isset_1d_strings.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$isset_1d_strings_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_1d_strings.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_1d_strings_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = ScalarArrays::sa_[0]);
  if (isset(v_x, "a", 0x4292CEE227B9150ALL)) echo("x[a] is set (1)\n");
  v_x.set("a", (1LL), 0x4292CEE227B9150ALL);
  if (isset(v_x, "a", 0x4292CEE227B9150ALL)) echo("x[a] is set (2)\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
