
#ifndef __GENERATED_cls_myiterator_h__
#define __GENERATED_cls_myiterator_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/foreach_object.php line 3 */
class c_myiterator : virtual public ObjectData {
  BEGIN_CLASS_MAP(myiterator)
  END_CLASS_MAP(myiterator)
  DECLARE_CLASS(myiterator, MyIterator, ObjectData)
  void init();
  public: void t_my_print(CVarRef v_this);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myiterator_h__
