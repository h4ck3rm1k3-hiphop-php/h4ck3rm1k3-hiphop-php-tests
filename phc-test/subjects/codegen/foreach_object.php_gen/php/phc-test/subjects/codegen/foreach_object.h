
#ifndef __GENERATED_php_phc_test_subjects_codegen_foreach_object_h__
#define __GENERATED_php_phc_test_subjects_codegen_foreach_object_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/foreach_object.fw.h>

// Declarations
#include <cls/myiterator.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$foreach_object_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_myiterator(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_foreach_object_h__
