
#include <php/phc-test/subjects/codegen/foreach_nested.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$foreach_nested_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/foreach_nested.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$foreach_nested_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_a = ScalarArrays::sa_[0]);
  (v_b = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_x = iter3->second();
      {
        LINE(7,x_var_export(v_a));
        LINE(8,x_var_export(v_x));
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_b.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_y = iter6->second();
            {
              LINE(11,x_var_export(v_b));
              LINE(12,x_var_export(v_y));
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_a.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_x = iter9->second();
      {
        LINE(19,x_var_export(v_a));
        LINE(20,x_var_export(v_x));
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = v_b.begin(); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_y = iter12->second();
            {
              LINE(23,x_var_export(v_b));
              break;
              LINE(25,x_var_export(v_y));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
