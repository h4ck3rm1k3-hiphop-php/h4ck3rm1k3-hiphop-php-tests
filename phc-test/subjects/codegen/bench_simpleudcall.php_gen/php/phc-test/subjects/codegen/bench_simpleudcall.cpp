
#include <php/phc-test/subjects/codegen/bench_simpleudcall.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_simpleudcall.php line 8 */
void f_hallo2(CStrRef v_a) {
  FUNCTION_INJECTION(hallo2);
} /* function */
/* SRC: phc-test/subjects/codegen/bench_simpleudcall.php line 3 */
void f_simpleudcall() {
  FUNCTION_INJECTION(simpleudcall);
  int64 v_i = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      LINE(5,f_hallo2("hallo"));
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_simpleudcall_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_simpleudcall.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_simpleudcall_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(11,f_simpleudcall());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
