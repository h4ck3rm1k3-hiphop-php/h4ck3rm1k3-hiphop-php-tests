
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_simpleudcall_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_simpleudcall_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_simpleudcall.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_hallo2(CStrRef v_a);
void f_simpleudcall();
Variant pm_php$phc_test$subjects$codegen$bench_simpleudcall_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_simpleudcall_h__
