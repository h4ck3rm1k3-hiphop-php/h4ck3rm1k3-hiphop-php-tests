
#include <php/phc-test/subjects/codegen/opeq.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/opeq.php line 3 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("f is called\n");
  return 1LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$opeq_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/opeq.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$opeq_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  v_x.set(1LL, (10LL), 0x5BCA7C69B794F8CELL);
  lval(v_x.lvalAt(LINE(10,f_f()))) += 1LL;
  LINE(12,x_var_export(v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
