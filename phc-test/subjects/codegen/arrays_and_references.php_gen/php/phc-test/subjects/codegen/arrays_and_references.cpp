
#include <php/phc-test/subjects/codegen/arrays_and_references.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$arrays_and_references_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/arrays_and_references.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$arrays_and_references_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x0 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x0") : g->GV(x0);
  Variant &v_x1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x1") : g->GV(x1);
  Variant &v_t0 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t0") : g->GV(t0);
  Variant &v_x2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x2") : g->GV(x2);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_t1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t1") : g->GV(t1);
  Variant &v_t2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t2") : g->GV(t2);

  ;
  LINE(8,x_var_dump(1, v_x0));
  (v_t0 = v_x1.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  LINE(11,x_var_dump(1, v_x1));
  (v_t0 = ref(lval(lval(v_x1.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  LINE(14,x_var_dump(1, v_x1));
  v_x2.set(0LL, (7LL), 0x77CFA1EEF01BCA90LL);
  LINE(17,x_var_dump(1, v_x2));
  (v_t1 = ref(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  (v_t2 = ref(v_t1));
  (v_x0 = 7LL);
  LINE(24,x_var_dump(1, v_t2));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
