
#include <php/phc-test/subjects/codegen/return_by_reference_evald_functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$return_by_reference_evald_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/return_by_reference_evald_functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$return_by_reference_evald_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  f_eval("function f()\n\t{\n\t\treturn $GLOBALS[\"a\"];\t\n\t}");
  f_eval("function &g()\n\t{\n\t\treturn $GLOBALS[\"a\"];\n\t}");
  (v_a = 5LL);
  (v_b = LINE(17,invoke_failed("f", Array(), 0x0000000077560280LL)));
  (v_b = v_b + 1LL);
  LINE(19,x_var_export(v_a));
  LINE(20,x_var_export(v_b));
  (v_a = 5LL);
  (v_c = LINE(23,invoke_failed("g", Array(), 0x000000005E8464CCLL)));
  (v_c = v_c + 1LL);
  LINE(25,x_var_export(v_a));
  LINE(26,x_var_export(v_c));
  (v_a = 5LL);
  (v_d = ref(LINE(29,invoke_failed("f", Array(), 0x0000000077560280LL))));
  (v_d = v_d + 1LL);
  LINE(31,x_var_export(v_a));
  LINE(32,x_var_export(v_d));
  (v_a = 5LL);
  (v_e = ref(LINE(35,invoke_failed("g", Array(), 0x000000005E8464CCLL))));
  (v_e = v_e + 1LL);
  LINE(37,x_var_export(v_a));
  LINE(38,x_var_export(v_e));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
