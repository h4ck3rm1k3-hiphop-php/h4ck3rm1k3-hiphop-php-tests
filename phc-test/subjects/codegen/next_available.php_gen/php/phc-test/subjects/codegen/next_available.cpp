
#include <php/phc-test/subjects/codegen/next_available.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$next_available_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/next_available.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$next_available_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  lval(lval(lval(lval(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt()).lvalAt()).lvalAt()).set(2LL, (5LL), 0x486AFCC090D5F98CLL);
  lval(lval(lval(lval(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt()).lvalAt()).lvalAt()).set(2LL, (6LL), 0x486AFCC090D5F98CLL);
  lval(lval(lval(lval(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt()).lvalAt()).lvalAt()).set(2LL, (7LL), 0x486AFCC090D5F98CLL);
  LINE(7,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
