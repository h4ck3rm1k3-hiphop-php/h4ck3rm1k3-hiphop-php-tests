
#include <php/phc-test/subjects/codegen/return_by_reference_compiled.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/return_by_reference_compiled.php line 8 */
Variant f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->GV(a);
} /* function */
/* SRC: phc-test/subjects/codegen/return_by_reference_compiled.php line 13 */
Variant f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  return ref(lval(g->GV(a)));
} /* function */
Variant pm_php$phc_test$subjects$codegen$return_by_reference_compiled_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/return_by_reference_compiled.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$return_by_reference_compiled_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  (v_a = 5LL);
  (v_b = LINE(19,f_f()));
  (v_b = v_b + 1LL);
  LINE(21,x_var_export(v_a));
  LINE(22,x_var_export(v_b));
  (v_a = 5LL);
  (v_c = LINE(25,f_g()));
  (v_c = v_c + 1LL);
  LINE(27,x_var_export(v_a));
  LINE(28,x_var_export(v_c));
  (v_a = 5LL);
  (v_d = ref(LINE(31,f_f())));
  (v_d = v_d + 1LL);
  LINE(33,x_var_export(v_a));
  LINE(34,x_var_export(v_d));
  (v_a = 5LL);
  (v_e = ref(LINE(37,f_g())));
  (v_e = v_e + 1LL);
  LINE(39,x_var_export(v_a));
  LINE(40,x_var_export(v_e));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
