
#ifndef __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref_h__
#define __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/dont_shred_the_ref.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$dont_shred_the_ref_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_x(Variant v_y);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref_h__
