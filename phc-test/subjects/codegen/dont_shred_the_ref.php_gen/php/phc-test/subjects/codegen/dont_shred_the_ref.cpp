
#include <php/phc-test/subjects/codegen/dont_shred_the_ref.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/dont_shred_the_ref.php line 7 */
void f_x(Variant v_y) {
  FUNCTION_INJECTION(x);
  (v_y = 7LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$dont_shred_the_ref_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/dont_shred_the_ref.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$dont_shred_the_ref_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_u __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u") : g->GV(u);

  LINE(13,f_x(ref(lval(v_a.lvalAt(v_u)))));
  LINE(15,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
