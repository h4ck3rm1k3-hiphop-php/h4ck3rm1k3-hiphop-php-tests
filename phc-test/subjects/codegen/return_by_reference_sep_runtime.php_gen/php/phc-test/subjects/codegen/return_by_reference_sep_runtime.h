
#ifndef __GENERATED_php_phc_test_subjects_codegen_return_by_reference_sep_runtime_h__
#define __GENERATED_php_phc_test_subjects_codegen_return_by_reference_sep_runtime_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/return_by_reference_sep_runtime.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_f();
Variant pm_php$phc_test$subjects$codegen$return_by_reference_sep_runtime_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_return_by_reference_sep_runtime_h__
