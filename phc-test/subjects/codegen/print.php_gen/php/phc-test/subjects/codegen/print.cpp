
#include <php/phc-test/subjects/codegen/print.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/print.php line 7 */
String f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_z __attribute__((__unused__)) = g->GV(z);
  gv_z++;
  return LINE(11,concat3("--", toString(gv_z), "--\n"));
} /* function */
/* SRC: phc-test/subjects/codegen/print.php line 52 */
bool f_x() {
  FUNCTION_INJECTION(x);
  return print("a string longer than 1");
} /* function */
Variant pm_php$phc_test$subjects$codegen$print_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/print.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$print_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_z = 2LL);
  (v_x = "15\n");
  print(toString(v_x));
  echo(toString(v_x));
  print(LINE(22,f_f()));
  echo(LINE(23,f_f()));
  (v_y = print(toString(5LL)));
  {
    echo(LINE(28,f_f()));
    echo(toString(v_x));
    echo(f_f());
  }
  print(toString(print(toString(print(toString(v_x))))));
  echo(toString(print(toString(print(toString(v_x))))));
  {
    echo(LINE(36,f_f()));
    echo(toString(print(toString(print(toString(v_x))))));
    echo(toString(v_x));
  }
  print(toString(print("a string of some length")));
  print(toString(LINE(57,f_x())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
