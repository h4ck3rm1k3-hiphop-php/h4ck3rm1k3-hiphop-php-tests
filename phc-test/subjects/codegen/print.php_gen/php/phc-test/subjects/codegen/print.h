
#ifndef __GENERATED_php_phc_test_subjects_codegen_print_h__
#define __GENERATED_php_phc_test_subjects_codegen_print_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/print.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$print_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_f();
bool f_x();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_print_h__
