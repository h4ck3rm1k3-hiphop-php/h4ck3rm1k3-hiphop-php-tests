
#include <php/phc-test/subjects/codegen/break_var_expr.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$break_var_expr_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/break_var_expr.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$break_var_expr_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, 10LL); v_j++) {
            LOOP_COUNTER_CHECK(2);
            {
              {
                LOOP_COUNTER(3);
                for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    switch (toInt64("a string")) {
                    case 3: goto break1;
                    case 2: goto break2;
                    case 1: goto break3;
                    default:
                    if ((toInt64("a string"))<2) {
                    goto break3;
                    } else {
                    throw_fatal("bad break");
                    }
                    }
                    LINE(12,x_var_dump(1, toString("k") + toString(v_k)));
                  }
                }
                break3:;
              }
              LINE(14,x_var_dump(1, toString("j") + toString(v_j)));
            }
          }
          break2:;
        }
        LINE(16,x_var_dump(1, toString("i") + toString(v_i)));
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
