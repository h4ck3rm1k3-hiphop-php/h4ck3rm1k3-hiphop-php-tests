
#include <php/phc-test/subjects/codegen/empty.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$empty_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/empty.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$empty_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_stringNonEmpty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stringNonEmpty") : g->GV(stringNonEmpty);
  Variant &v_stringEmpty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stringEmpty") : g->GV(stringEmpty);
  Variant &v_stringZero __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stringZero") : g->GV(stringZero);
  Variant &v_intNonZero __attribute__((__unused__)) = (variables != gVariables) ? variables->get("intNonZero") : g->GV(intNonZero);
  Variant &v_intZero __attribute__((__unused__)) = (variables != gVariables) ? variables->get("intZero") : g->GV(intZero);
  Variant &v_null __attribute__((__unused__)) = (variables != gVariables) ? variables->get("null") : g->GV(null);
  Variant &v_boolTrue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("boolTrue") : g->GV(boolTrue);
  Variant &v_boolFalse __attribute__((__unused__)) = (variables != gVariables) ? variables->get("boolFalse") : g->GV(boolFalse);
  Variant &v_arrayNonEmpty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arrayNonEmpty") : g->GV(arrayNonEmpty);
  Variant &v_arrayEmpty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arrayEmpty") : g->GV(arrayEmpty);
  Variant &v_notSet __attribute__((__unused__)) = (variables != gVariables) ? variables->get("notSet") : g->GV(notSet);

  (v_stringNonEmpty = "foo");
  (v_stringEmpty = "");
  (v_stringZero = "0");
  (v_intNonZero = 1LL);
  (v_intZero = 0LL);
  setNull(v_null);
  (v_boolTrue = true);
  (v_boolFalse = false);
  (v_arrayNonEmpty = ScalarArrays::sa_[0]);
  (v_arrayEmpty = ScalarArrays::sa_[1]);
  LINE(17,x_var_dump(1, empty(v_notSet)));
  LINE(19,x_var_dump(1, empty(v_stringNonEmpty)));
  LINE(20,x_var_dump(1, empty(v_stringEmpty)));
  LINE(21,x_var_dump(1, empty(v_stringZero)));
  LINE(23,x_var_dump(1, empty(v_intNonZero)));
  LINE(24,x_var_dump(1, empty(v_intZero)));
  LINE(26,x_var_dump(1, empty(v_null)));
  LINE(28,x_var_dump(1, empty(v_boolTrue)));
  LINE(29,x_var_dump(1, empty(v_boolFalse)));
  LINE(31,x_var_dump(1, empty(v_arrayNonEmpty)));
  LINE(32,x_var_dump(1, empty(v_arrayEmpty)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
