
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_INDEX(0x372356B19DCB0F80LL, boolFalse, 19);
      break;
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x48BC21751E4F3B07LL, arrayEmpty, 21);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 27:
      HASH_INDEX(0x3772C26BAE1C825BLL, arrayNonEmpty, 20);
      break;
    case 34:
      HASH_INDEX(0x33482262955683A2LL, boolTrue, 18);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 37:
      HASH_INDEX(0x599439FD781D7165LL, intNonZero, 15);
      break;
    case 38:
      HASH_INDEX(0x26F80C69D91A3FE6LL, notSet, 22);
      break;
    case 44:
      HASH_INDEX(0x2FF6F3DFF927FF2CLL, null, 17);
      break;
    case 45:
      HASH_INDEX(0x0A6C8194DF32D82DLL, stringEmpty, 13);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      HASH_INDEX(0x1D27178296F3796ELL, stringZero, 14);
      break;
    case 47:
      HASH_INDEX(0x6308AED2666D49AFLL, stringNonEmpty, 12);
      break;
    case 51:
      HASH_INDEX(0x3724F1D579A43233LL, intZero, 16);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 23) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
