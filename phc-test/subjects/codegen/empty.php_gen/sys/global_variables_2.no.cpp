
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_INITIALIZED(0x372356B19DCB0F80LL, g->GV(boolFalse),
                       boolFalse);
      break;
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 7:
      HASH_INITIALIZED(0x48BC21751E4F3B07LL, g->GV(arrayEmpty),
                       arrayEmpty);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 27:
      HASH_INITIALIZED(0x3772C26BAE1C825BLL, g->GV(arrayNonEmpty),
                       arrayNonEmpty);
      break;
    case 34:
      HASH_INITIALIZED(0x33482262955683A2LL, g->GV(boolTrue),
                       boolTrue);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 37:
      HASH_INITIALIZED(0x599439FD781D7165LL, g->GV(intNonZero),
                       intNonZero);
      break;
    case 38:
      HASH_INITIALIZED(0x26F80C69D91A3FE6LL, g->GV(notSet),
                       notSet);
      break;
    case 44:
      HASH_INITIALIZED(0x2FF6F3DFF927FF2CLL, g->GV(null),
                       null);
      break;
    case 45:
      HASH_INITIALIZED(0x0A6C8194DF32D82DLL, g->GV(stringEmpty),
                       stringEmpty);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      HASH_INITIALIZED(0x1D27178296F3796ELL, g->GV(stringZero),
                       stringZero);
      break;
    case 47:
      HASH_INITIALIZED(0x6308AED2666D49AFLL, g->GV(stringNonEmpty),
                       stringNonEmpty);
      break;
    case 51:
      HASH_INITIALIZED(0x3724F1D579A43233LL, g->GV(intZero),
                       intZero);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
