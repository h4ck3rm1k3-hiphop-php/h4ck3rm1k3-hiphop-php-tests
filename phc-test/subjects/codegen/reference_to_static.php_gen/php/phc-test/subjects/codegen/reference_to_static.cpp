
#include <php/phc-test/subjects/codegen/reference_to_static.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/reference_to_static.php line 7 */
Variant f_getstatic() {
  FUNCTION_INJECTION(getStatic);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_staticVar __attribute__((__unused__)) = g->sv_getstatic_DupIdstaticVar;
  bool &inited_sv_staticVar __attribute__((__unused__)) = g->inited_sv_getstatic_DupIdstaticVar;
  if (!inited_sv_staticVar) {
    (sv_staticVar = 0LL);
    inited_sv_staticVar = true;
  }
  return ref(sv_staticVar);
} /* function */
/* SRC: phc-test/subjects/codegen/reference_to_static.php line 13 */
void f_f() {
  FUNCTION_INJECTION(f);
  Variant v_x;

  (v_x = ref(LINE(15,f_getstatic())));
  LINE(16,x_var_export(v_x));
  v_x++;
} /* function */
/* SRC: phc-test/subjects/codegen/reference_to_static.php line 20 */
void f_g() {
  FUNCTION_INJECTION(g);
  Variant v_y;

  (v_y = ref(LINE(22,f_getstatic())));
  LINE(23,x_var_export(v_y));
  v_y++;
} /* function */
Variant pm_php$phc_test$subjects$codegen$reference_to_static_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/reference_to_static.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$reference_to_static_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(27,f_f());
  LINE(28,f_g());
  LINE(29,f_f());
  LINE(30,f_g());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
