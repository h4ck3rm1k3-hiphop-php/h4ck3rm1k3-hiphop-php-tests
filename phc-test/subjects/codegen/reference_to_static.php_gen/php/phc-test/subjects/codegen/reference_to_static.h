
#ifndef __GENERATED_php_phc_test_subjects_codegen_reference_to_static_h__
#define __GENERATED_php_phc_test_subjects_codegen_reference_to_static_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/reference_to_static.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$reference_to_static_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_getstatic();
void f_f();
void f_g();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_reference_to_static_h__
