
#include <php/phc-test/subjects/codegen/return.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/return.php line 2 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  return 5LL;
} /* function */
/* SRC: phc-test/subjects/codegen/return.php line 7 */
void f_g() {
  FUNCTION_INJECTION(g);
  return;
} /* function */
Variant pm_php$phc_test$subjects$codegen$return_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/return.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$return_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(12,x_var_dump(1, f_f()));
  LINE(13,x_var_dump(1, (f_g(), null)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
