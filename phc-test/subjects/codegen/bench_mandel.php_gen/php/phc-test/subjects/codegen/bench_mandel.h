
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_mandel_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_mandel_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_mandel.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_mandel();
Variant pm_php$phc_test$subjects$codegen$bench_mandel_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_mandel_h__
