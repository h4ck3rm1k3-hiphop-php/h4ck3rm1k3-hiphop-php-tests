
#include <php/phc-test/subjects/codegen/bench_mandel.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_mandel.php line 3 */
void f_mandel() {
  FUNCTION_INJECTION(mandel);
  int64 v_w1 = 0;
  int64 v_h1 = 0;
  double v_recen = 0.0;
  double v_imcen = 0.0;
  double v_r = 0.0;
  Numeric v_s = 0;
  double v_rec = 0.0;
  double v_imc = 0.0;
  double v_re = 0.0;
  double v_im = 0.0;
  double v_re2 = 0.0;
  double v_im2 = 0.0;
  int64 v_x = 0;
  int64 v_y = 0;
  int64 v_w2 = 0;
  int64 v_h2 = 0;
  int64 v_color = 0;

  (v_w1 = 50LL);
  (v_h1 = 150LL);
  (v_recen = -0.45000000000000001);
  (v_imcen = 0.0);
  (v_r = 0.69999999999999996);
  (v_s = 0LL);
  (v_rec = toDouble(0LL));
  (v_imc = toDouble(0LL));
  (v_re = toDouble(0LL));
  (v_im = toDouble(0LL));
  (v_re2 = toDouble(0LL));
  (v_im2 = toDouble(0LL));
  (v_x = 0LL);
  (v_y = 0LL);
  (v_w2 = 0LL);
  (v_h2 = 0LL);
  (v_color = 0LL);
  (v_s = divide(2LL * v_r, v_w1));
  (v_w2 = 40LL);
  (v_h2 = 12LL);
  {
    LOOP_COUNTER(1);
    for ((v_y = 0LL); not_more(v_y, v_w1); (v_y = v_y + 1LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_imc = toDouble(v_s * (v_y - v_h2)) + v_imcen);
        {
          LOOP_COUNTER(2);
          for ((v_x = 0LL); not_more(v_x, v_h1); (v_x = v_x + 1LL)) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_rec = toDouble(v_s * (v_x - v_w2)) + v_recen);
              (v_re = v_rec);
              (v_im = v_imc);
              (v_color = 10LL);
              (v_re2 = v_re * v_re);
              (v_im2 = v_im * v_im);
              LOOP_COUNTER(3);
              {
                while (((less((v_re2 + v_im2), 10LL)) && more(v_color, 0LL))) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    (v_im = v_re * v_im * 2LL + v_imc);
                    (v_re = v_re2 - v_im2 + v_rec);
                    (v_re2 = v_re * v_re);
                    (v_im2 = v_im * v_im);
                    (v_color = v_color - 1LL);
                  }
                }
              }
              if (equal(v_color, 0LL)) {
                print("_");
              }
              else {
                print("#");
              }
            }
          }
        }
        print("<br>");
        LINE(37,x_flush());
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_mandel_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_mandel.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_mandel_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(41,f_mandel());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
