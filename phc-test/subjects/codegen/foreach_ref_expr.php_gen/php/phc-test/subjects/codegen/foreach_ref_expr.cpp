
#include <php/phc-test/subjects/codegen/foreach_ref_expr.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/foreach_ref_expr.php line 6 */
Variant f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_arr __attribute__((__unused__)) = g->GV(arr);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  gv_arr.append((gv_a++));
  return ref(gv_arr);
} /* function */
/* SRC: phc-test/subjects/codegen/foreach_ref_expr.php line 14 */
Variant f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_arr2 __attribute__((__unused__)) = g->GV(arr2);
  Variant &gv_a1 __attribute__((__unused__)) = g->GV(a1);
  gv_arr2.append((gv_a1++));
  return ref(gv_arr2);
} /* function */
Variant pm_php$phc_test$subjects$codegen$foreach_ref_expr_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/foreach_ref_expr.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$foreach_ref_expr_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 5LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          Variant map3 = LINE(24,invoke_too_many_args("f", (f_g(), 2), ((f_f()))));
          for (ArrayIterPtr iter4 = map3.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_x = iter4->second();
            {
              echo(toString(v_x));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
