
#ifndef __GENERATED_php_phc_test_subjects_codegen_foreach_ref_expr_h__
#define __GENERATED_php_phc_test_subjects_codegen_foreach_ref_expr_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/foreach_ref_expr.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$foreach_ref_expr_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_f();
Variant f_g();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_foreach_ref_expr_h__
