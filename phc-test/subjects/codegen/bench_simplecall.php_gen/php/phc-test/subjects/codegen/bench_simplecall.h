
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_simplecall_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_simplecall_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_simplecall.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$bench_simplecall_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_simplecall();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_simplecall_h__
