
#include <php/phc-test/subjects/codegen/undeclared_function.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/undeclared_function.php line 6 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  return 1LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$undeclared_function_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/undeclared_function.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$undeclared_function_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  LINE(3,f_f());
  (v_c = 1LL);
  if (toBoolean(v_c)) {
  }
  LINE(8,f_f());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
