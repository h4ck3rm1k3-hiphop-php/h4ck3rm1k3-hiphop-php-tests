
#ifndef __GENERATED_php_phc_test_subjects_codegen_undeclared_function_h__
#define __GENERATED_php_phc_test_subjects_codegen_undeclared_function_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/undeclared_function.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_f();
Variant pm_php$phc_test$subjects$codegen$undeclared_function_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_undeclared_function_h__
