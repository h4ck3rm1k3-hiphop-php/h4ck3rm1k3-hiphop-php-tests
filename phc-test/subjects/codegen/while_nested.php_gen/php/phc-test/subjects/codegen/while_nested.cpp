
#include <php/phc-test/subjects/codegen/while_nested.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$while_nested_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/while_nested.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$while_nested_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  LOOP_COUNTER(1);
  {
    while (less(v_x, 10LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(v_x) + toString("\n"));
        LOOP_COUNTER(2);
        {
          while (less(v_y, 20LL)) {
            LOOP_COUNTER_CHECK(2);
            {
              echo(toString(v_y) + toString("\n"));
              v_y++;
              v_x++;
            }
          }
        }
        v_z++;
        echo(toString(v_z) + toString("\n"));
      }
    }
  }
  LINE(15,x_var_dump(1, v_x));
  LINE(16,x_var_dump(1, v_y));
  LINE(17,x_var_dump(1, v_z));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
