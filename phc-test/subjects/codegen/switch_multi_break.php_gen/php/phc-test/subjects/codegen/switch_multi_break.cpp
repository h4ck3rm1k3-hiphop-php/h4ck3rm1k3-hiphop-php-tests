
#include <php/phc-test/subjects/codegen/switch_multi_break.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$switch_multi_break_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_multi_break.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_multi_break_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 7LL);
  {
    switch (toInt64(v_x)) {
    case 7LL:
      {
        LINE(7,invoke_failed("f", Array(), 0x0000000077560280LL));
        break;
      }
    case 8LL:
      {
        LOOP_COUNTER(2);
        {
          while (!equal(v_x, 0LL)) {
            LOOP_COUNTER_CHECK(2);
            {
              if (equal(v_x, 5LL)) goto break1;
            }
          }
        }
        break;
      }
    default:
      {
        break;
      }
    }
    break1:;
  }
  (v_x = 7LL);
  {
    LOOP_COUNTER(3);
    do {
      LOOP_COUNTER_CHECK(3);
      {
        if (equal(v_x, 7LL) || equal(v_x, 8LL)) {
          LOOP_COUNTER(4);
          {
            while (!equal(v_x, 0LL)) {
              LOOP_COUNTER_CHECK(4);
              {
                if (equal(v_x, 5LL)) {
                  goto break3;
                }
              }
            }
          }
        }
      }
    } while (toBoolean(0LL));
    break3:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
