
#include <php/phc-test/subjects/codegen/separate_return_by_ref.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/separate_return_by_ref.php line 6 */
Variant f_my_ret() {
  FUNCTION_INJECTION(my_ret);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  return gv_x;
} /* function */
Variant pm_php$phc_test$subjects$codegen$separate_return_by_ref_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/separate_return_by_ref.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$separate_return_by_ref_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_x = 0LL);
  (v_y = v_x);
  (v_z = v_y);
  (v_a = ref(LINE(16,f_my_ret())));
  (v_a = 7LL);
  LINE(19,x_var_dump(1, v_a));
  LINE(20,x_var_dump(1, v_x));
  LINE(21,x_var_dump(1, v_y));
  LINE(22,x_var_dump(1, v_z));
  echo("\t\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
