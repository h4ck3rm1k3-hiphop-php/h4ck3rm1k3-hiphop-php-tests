
#ifndef __GENERATED_php_phc_test_subjects_codegen_separate_return_by_ref_h__
#define __GENERATED_php_phc_test_subjects_codegen_separate_return_by_ref_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/separate_return_by_ref.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$separate_return_by_ref_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_my_ret();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_separate_return_by_ref_h__
