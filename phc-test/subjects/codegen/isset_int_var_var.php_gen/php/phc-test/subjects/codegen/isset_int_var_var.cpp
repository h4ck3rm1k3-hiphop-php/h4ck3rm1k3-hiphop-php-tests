
#include <php/phc-test/subjects/codegen/isset_int_var_var.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$isset_int_var_var_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_int_var_var.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_int_var_var_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("1") : g->GV(1);

  (v_x = 1LL);
  if (isset(variables->get(toString(v_x)))) echo("int var var is set\n");
  else echo("int var var is not set\n");
  (get_variable_table()->get(1LL) = "a");
  if (isset(variables->get(toString(v_x)))) echo("int var var is set\n");
  else echo("int var var is not set\n");
  (get_variable_table()->get("1") = "b");
  if (isset(variables->get(toString(v_x)))) echo("int var var is set\n");
  else echo("int var var is not set\n");
  (v_1 = "c");
  if (isset(variables->get(toString(v_x)))) echo("int var var is set\n");
  else echo("int var var is not set\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
