
#include <php/phc-test/subjects/codegen/post_increment_in_break.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$post_increment_in_break_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/post_increment_in_break.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$post_increment_in_break_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_i = 0LL);
  {
    LOOP_COUNTER(1);
    for (; ; ) {
      LOOP_COUNTER_CHECK(1);
      {
        echo("a");
        {
          LOOP_COUNTER(2);
          for (; ; ) {
            LOOP_COUNTER_CHECK(2);
            {
              echo("b");
              {
                LOOP_COUNTER(3);
                for (; ; ) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    echo("c");
                    {
                      LOOP_COUNTER(4);
                      for (; ; ) {
                        LOOP_COUNTER_CHECK(4);
                        {
                          echo("d");
                          switch (toInt64(v_i++)) {
                          case 4: goto break1;
                          case 3: goto break2;
                          case 2: goto break3;
                          case 1: goto break4;
                          default:
                          if ((toInt64(v_i++))<2) {
                          goto break4;
                          } else {
                          throw_fatal("bad break");
                          }
                          }
                          echo("e");
                        }
                      }
                      break4:;
                    }
                    echo("f");
                  }
                }
                break3:;
              }
              echo("g");
            }
          }
          break2:;
        }
        echo("h");
      }
    }
    break1:;
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
