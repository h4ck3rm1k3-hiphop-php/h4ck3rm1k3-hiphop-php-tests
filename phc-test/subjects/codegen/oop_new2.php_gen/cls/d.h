
#ifndef __GENERATED_cls_d_h__
#define __GENERATED_cls_d_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_new2.php line 17 */
class c_d : virtual public ObjectData {
  BEGIN_CLASS_MAP(d)
  END_CLASS_MAP(d)
  DECLARE_CLASS(d, D, ObjectData)
  void init();
  public: void t___construct(int64 v_x, int64 v_y);
  public: ObjectData *create(int64 v_x, int64 v_y);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_d_h__
