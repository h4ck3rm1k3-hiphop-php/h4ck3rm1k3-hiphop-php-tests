
#ifndef __GENERATED_php_phc_test_subjects_codegen_push_onto_scalar_h__
#define __GENERATED_php_phc_test_subjects_codegen_push_onto_scalar_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/push_onto_scalar.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$push_onto_scalar_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_push_onto_scalar_h__
