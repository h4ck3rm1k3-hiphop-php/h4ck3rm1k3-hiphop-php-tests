
#include <php/phc-test/subjects/codegen/push_onto_scalar.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$push_onto_scalar_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/push_onto_scalar.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$push_onto_scalar_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);

  (v_x = false);
  (v_y = true);
  (v_z = 5LL);
  (v_w = 5.7000000000000002);
  v_x.append((1LL));
  v_y.append((1LL));
  v_z.append((1LL));
  v_w.append((1LL));
  LINE(13,x_var_dump(1, v_x));
  LINE(14,x_var_dump(1, v_y));
  LINE(15,x_var_dump(1, v_z));
  LINE(16,x_var_dump(1, v_w));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
