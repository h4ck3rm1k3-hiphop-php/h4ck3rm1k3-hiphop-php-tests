
#include <php/phc-test/subjects/codegen/lazy_op.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/lazy_op.php line 2 */
bool f_f() {
  FUNCTION_INJECTION(f);
  echo("this is f\n");
  return false;
} /* function */
/* SRC: phc-test/subjects/codegen/lazy_op.php line 8 */
bool f_t() {
  FUNCTION_INJECTION(t);
  echo("this is t\n");
  return true;
} /* function */
Variant pm_php$phc_test$subjects$codegen$lazy_op_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/lazy_op.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$lazy_op_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(14,x_var_dump(1, f_f() && f_f()));
  LINE(15,x_var_dump(1, f_f() && f_t()));
  LINE(16,x_var_dump(1, f_t() && f_f()));
  LINE(17,x_var_dump(1, f_t() && f_t()));
  LINE(19,x_var_dump(1, f_f() || f_f()));
  LINE(20,x_var_dump(1, f_f() || f_t()));
  LINE(21,x_var_dump(1, f_t() || f_f()));
  LINE(22,x_var_dump(1, f_t() || f_t()));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
