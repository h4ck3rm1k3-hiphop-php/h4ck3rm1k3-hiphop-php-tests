
#ifndef __GENERATED_php_phc_test_subjects_codegen_lazy_op_h__
#define __GENERATED_php_phc_test_subjects_codegen_lazy_op_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/lazy_op.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_f();
bool f_t();
Variant pm_php$phc_test$subjects$codegen$lazy_op_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_lazy_op_h__
