
#include <php/phc-test/subjects/codegen/bench_nested_loop.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_nested_loop.php line 4 */
void f_nestedloop(int64 v_n) {
  FUNCTION_INJECTION(nestedloop);
  int64 v_x = 0;
  int64 v_a = 0;
  int64 v_b = 0;
  int64 v_c = 0;
  int64 v_d = 0;
  int64 v_e = 0;
  int64 v_f = 0;

  (v_x = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_a = 0LL); less(v_a, v_n); v_a++) {
      LOOP_COUNTER_CHECK(1);
      {
        LOOP_COUNTER(2);
        for ((v_b = 0LL); less(v_b, v_n); v_b++) {
          LOOP_COUNTER_CHECK(2);
          {
            LOOP_COUNTER(3);
            for ((v_c = 0LL); less(v_c, v_n); v_c++) {
              LOOP_COUNTER_CHECK(3);
              {
                LOOP_COUNTER(4);
                for ((v_d = 0LL); less(v_d, v_n); v_d++) {
                  LOOP_COUNTER_CHECK(4);
                  {
                    LOOP_COUNTER(5);
                    for ((v_e = 0LL); less(v_e, v_n); v_e++) {
                      LOOP_COUNTER_CHECK(5);
                      {
                        LOOP_COUNTER(6);
                        for ((v_f = 0LL); less(v_f, v_n); v_f++) {
                          LOOP_COUNTER_CHECK(6);
                          v_x++;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  print(toString(v_x) + toString("\n"));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_nested_loop_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_nested_loop.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_nested_loop_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(16,f_nestedloop(2LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
