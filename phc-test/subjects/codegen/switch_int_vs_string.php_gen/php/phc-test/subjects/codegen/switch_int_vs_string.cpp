
#include <php/phc-test/subjects/codegen/switch_int_vs_string.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$switch_int_vs_string_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_int_vs_string.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_int_vs_string_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 2LL);
  {
    Variant tmp2 = (v_x);
    int tmp3 = -1;
    if (equal(tmp2, ("0"))) {
      tmp3 = 0;
    } else if (equal(tmp2, ("1"))) {
      tmp3 = 1;
    } else if (equal(tmp2, ("2"))) {
      tmp3 = 3;
    } else if (equal(tmp2, ("3"))) {
      tmp3 = 4;
    } else if (true) {
      tmp3 = 2;
    }
    switch (tmp3) {
    case 0:
      {
        echo("0\n");
        goto break1;
      }
    case 1:
      {
        echo("1\n");
        goto break1;
      }
    case 2:
      {
        echo("default\n");
        goto break1;
      }
    case 3:
      {
        echo("2\n");
        goto break1;
      }
    case 4:
      {
        echo("3\n");
        goto break1;
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
