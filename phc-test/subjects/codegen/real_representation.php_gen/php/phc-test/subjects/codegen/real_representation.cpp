
#include <php/phc-test/subjects/codegen/real_representation.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$real_representation_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/real_representation.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$real_representation_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(5,x_var_dump(1, 0.10000000000000001));
  LINE(6,x_var_dump(1, 0.97999999999999998));
  LINE(7,x_var_dump(1, 4.4687999999999999));
  LINE(9,x_var_dump(1, 131.29999999999998));
  LINE(11,x_var_dump(4, 1.0E-8, ScalarArrays::sa_[0]));
  LINE(13,x_var_dump(1, 0.20000000000000001));
  LINE(14,x_var_dump(1, 0.33333333333333331));
  LINE(15,x_var_dump(1, 0.10000000000000001));
  LINE(17,x_var_dump(1, 4.25));
  LINE(18,x_var_dump(1, 1.125));
  LINE(19,x_var_dump(1, 4.25));
  LINE(20,x_var_dump(1, 0.984375));
  LINE(22,x_var_dump(1, 1.2349999));
  LINE(23,x_var_dump(1, 1.2350000999999999));
  LINE(24,x_var_dump(1, 1.2350000000000001));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
