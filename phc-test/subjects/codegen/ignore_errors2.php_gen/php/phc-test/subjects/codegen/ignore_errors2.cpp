
#include <php/phc-test/subjects/codegen/ignore_errors2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$ignore_errors2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/ignore_errors2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$ignore_errors2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  (v_x = LINE(2,x_file_get_contents("BestaatHeusNiet")));
  LINE(3,x_var_dump(1, v_x));
  (v_y = (silenceInc(), silenceDec(LINE(4,x_file_get_contents("BestaatHeusNiet")))));
  LINE(5,x_var_dump(1, v_y));
  (v_z = LINE(6,x_file_get_contents("BestaatHeusNiet")));
  LINE(7,x_var_dump(1, v_z));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
