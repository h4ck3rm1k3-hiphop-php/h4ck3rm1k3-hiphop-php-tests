
#include <php/phc-test/subjects/codegen/assign_to_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$assign_to_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/assign_to_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$assign_to_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  v_arr.set(0LL, ("a"), 0x77CFA1EEF01BCA90LL);
  lval(v_arr.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(2LL, ("b"), 0x486AFCC090D5F98CLL);
  lval(lval(v_arr.lvalAt(4LL, 0x6F2A25235E544A31LL)).lvalAt(5LL, 0x350AEB726A15D700LL)).set(6LL, ("c"), 0x26BF47194D7E8E12LL);
  LINE(5,x_var_export(v_arr));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
