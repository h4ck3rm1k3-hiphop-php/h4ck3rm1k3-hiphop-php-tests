
#include <php/phc-test/subjects/codegen/function_call_index.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/function_call_index.php line 3 */
void f_x() {
  FUNCTION_INJECTION(x);
  echo("y\n");
} /* function */
Variant pm_php$phc_test$subjects$codegen$function_call_index_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/function_call_index.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$function_call_index_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  v_y.rvalAt(LINE(8,f_x()));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
