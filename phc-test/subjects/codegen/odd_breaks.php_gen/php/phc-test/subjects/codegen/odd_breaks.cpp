
#include <php/phc-test/subjects/codegen/odd_breaks.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$odd_breaks_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/odd_breaks.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$odd_breaks_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, 10LL); v_j++) {
            LOOP_COUNTER_CHECK(2);
            {
              {
                LOOP_COUNTER(3);
                for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    LINE(9,x_var_dump(1, toString("i") + toString(v_i)));
                    LINE(10,x_var_dump(1, toString("j") + toString(v_j)));
                    LINE(11,x_var_dump(1, toString("k") + toString(v_k)));
                    break;
                    LINE(13,x_var_dump(1, toString("i") + toString(v_i)));
                    LINE(14,x_var_dump(1, toString("j") + toString(v_j)));
                    LINE(15,x_var_dump(1, toString("k") + toString(v_k)));
                  }
                }
              }
            }
          }
        }
        {
          LOOP_COUNTER(4);
          for ((v_j = 0LL); less(v_j, 10LL); v_j++) {
            LOOP_COUNTER_CHECK(4);
            {
              {
                LOOP_COUNTER(5);
                for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
                  LOOP_COUNTER_CHECK(5);
                  {
                    LINE(23,x_var_dump(1, toString("i") + toString(v_i)));
                    LINE(24,x_var_dump(1, toString("j") + toString(v_j)));
                    LINE(25,x_var_dump(1, toString("k") + toString(v_k)));
                    break;
                    LINE(27,x_var_dump(1, toString("i") + toString(v_i)));
                    LINE(28,x_var_dump(1, toString("j") + toString(v_j)));
                    LINE(29,x_var_dump(1, toString("k") + toString(v_k)));
                  }
                }
              }
            }
          }
        }
        {
          LOOP_COUNTER(6);
          for ((v_j = 0LL); less(v_j, 10LL); v_j++) {
            LOOP_COUNTER_CHECK(6);
            {
              {
                LOOP_COUNTER(7);
                for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
                  LOOP_COUNTER_CHECK(7);
                  {
                    LINE(37,x_var_dump(1, toString("i") + toString(v_i)));
                    LINE(38,x_var_dump(1, toString("j") + toString(v_j)));
                    LINE(39,x_var_dump(1, toString("k") + toString(v_k)));
                    break;
                    LINE(41,x_var_dump(1, toString("i") + toString(v_i)));
                    LINE(42,x_var_dump(1, toString("j") + toString(v_j)));
                    LINE(43,x_var_dump(1, toString("k") + toString(v_k)));
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
