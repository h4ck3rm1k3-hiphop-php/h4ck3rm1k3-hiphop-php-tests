
#include <php/phc-test/subjects/codegen/hashtable.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$hashtable_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/hashtable.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$hashtable_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  v_arr.set("Jan", (31LL), 0x6C2625B972DC9633LL);
  v_arr.set("Feb", (28LL), 0x25DAA299A124E92BLL);
  v_arr.set("Mar", (31LL), 0x2576A193646803E0LL);
  LINE(5,x_var_dump(1, v_arr));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
