
#include <php/phc-test/subjects/codegen/array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr1") : g->GV(arr1);
  Variant &v_arr2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr2") : g->GV(arr2);

  (v_arr1 = ScalarArrays::sa_[0]);
  LINE(3,x_var_dump(1, v_arr1));
  (v_arr2 = Array(ArrayInit(2).set(0, v_arr1).setRef(1, ref(v_arr1)).create()));
  LINE(6,x_var_dump(1, v_arr2));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
