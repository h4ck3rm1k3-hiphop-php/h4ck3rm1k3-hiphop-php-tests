
#ifndef __GENERATED_php_phc_test_subjects_codegen_post_op_with_side_effect_h__
#define __GENERATED_php_phc_test_subjects_codegen_post_op_with_side_effect_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/post_op_with_side_effect.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_f();
Variant pm_php$phc_test$subjects$codegen$post_op_with_side_effect_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_post_op_with_side_effect_h__
