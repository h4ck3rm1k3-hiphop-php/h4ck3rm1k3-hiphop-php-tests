
#include <php/phc-test/subjects/codegen/post_op_with_side_effect.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/post_op_with_side_effect.php line 2 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("f was called\n");
  return 0LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$post_op_with_side_effect_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/post_op_with_side_effect.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$post_op_with_side_effect_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  v_a.set(0LL, (1LL), 0x77CFA1EEF01BCA90LL);
  lval(v_a.lvalAt(LINE(9,f_f())))++;
  ++lval(v_a.lvalAt(LINE(10,f_f())));
  LINE(11,x_var_dump(1, v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
