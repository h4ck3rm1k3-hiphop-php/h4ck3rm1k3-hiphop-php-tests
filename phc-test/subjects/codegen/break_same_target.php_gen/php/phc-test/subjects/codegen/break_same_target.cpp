
#include <php/phc-test/subjects/codegen/break_same_target.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$break_same_target_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/break_same_target.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$break_same_target_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        if (more(v_i, 3LL)) {
          {
            LOOP_COUNTER(2);
            for ((v_j = 0LL); less(v_j, 10LL); v_j++) {
              LOOP_COUNTER_CHECK(2);
              {
                if (more(v_j, 6LL)) goto break1;
                LINE(12,x_var_dump(1, v_j));
              }
            }
          }
        }
        LINE(15,x_var_dump(1, v_i));
        if (equal(v_i, 9LL)) break;
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
