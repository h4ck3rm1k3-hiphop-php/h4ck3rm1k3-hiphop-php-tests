
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x5A7E050228598BC6LL, Ca, 12);
      break;
    case 8:
      HASH_INDEX(0x4D258AC58FC35588LL, Cg, 18);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x32A0921DBD5BF589LL, Ch, 19);
      break;
    case 13:
      HASH_INDEX(0x209420ACF6A5D30DLL, Db, 23);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x34682089E0311950LL, Dc, 24);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 18:
      HASH_INDEX(0x5FC2A175FE401512LL, Di, 30);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 20:
      HASH_INDEX(0x5CB2402CEBF58DD4LL, Dd, 25);
      break;
    case 22:
      HASH_INDEX(0x436254A482933416LL, Cj, 21);
      break;
    case 24:
      HASH_INDEX(0x11E8D2432D087998LL, Cd, 15);
      HASH_INDEX(0x320F99EF1EBD3598LL, Dj, 31);
      break;
    case 33:
      HASH_INDEX(0x4DEF1DB3B27D27E1LL, Cb, 13);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      HASH_INDEX(0x2F76B520A63D1FA3LL, Df, 27);
      break;
    case 42:
      HASH_INDEX(0x319041AF0C4578AALL, Da, 22);
      break;
    case 44:
      HASH_INDEX(0x796FE4BCDF633A6CLL, De, 26);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      HASH_INDEX(0x2611AACF478041EELL, Ci, 20);
      break;
    case 53:
      HASH_INDEX(0x4F58EAA6FDEC9FF5LL, Cc, 14);
      break;
    case 54:
      HASH_INDEX(0x067C114103BFD5B6LL, Ce, 16);
      break;
    case 55:
      HASH_INDEX(0x2B24BC213B6438F7LL, Cf, 17);
      break;
    case 56:
      HASH_INDEX(0x2348BACF443C5138LL, Dg, 28);
      break;
    case 57:
      HASH_INDEX(0x40DEF2A85D92FFF9LL, Dh, 29);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 32) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
