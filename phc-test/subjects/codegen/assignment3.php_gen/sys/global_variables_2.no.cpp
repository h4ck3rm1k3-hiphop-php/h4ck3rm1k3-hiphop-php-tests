
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      HASH_INITIALIZED(0x5A7E050228598BC6LL, g->GV(Ca),
                       Ca);
      break;
    case 8:
      HASH_INITIALIZED(0x4D258AC58FC35588LL, g->GV(Cg),
                       Cg);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x32A0921DBD5BF589LL, g->GV(Ch),
                       Ch);
      break;
    case 13:
      HASH_INITIALIZED(0x209420ACF6A5D30DLL, g->GV(Db),
                       Db);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      HASH_INITIALIZED(0x34682089E0311950LL, g->GV(Dc),
                       Dc);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 18:
      HASH_INITIALIZED(0x5FC2A175FE401512LL, g->GV(Di),
                       Di);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 20:
      HASH_INITIALIZED(0x5CB2402CEBF58DD4LL, g->GV(Dd),
                       Dd);
      break;
    case 22:
      HASH_INITIALIZED(0x436254A482933416LL, g->GV(Cj),
                       Cj);
      break;
    case 24:
      HASH_INITIALIZED(0x11E8D2432D087998LL, g->GV(Cd),
                       Cd);
      HASH_INITIALIZED(0x320F99EF1EBD3598LL, g->GV(Dj),
                       Dj);
      break;
    case 33:
      HASH_INITIALIZED(0x4DEF1DB3B27D27E1LL, g->GV(Cb),
                       Cb);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      HASH_INITIALIZED(0x2F76B520A63D1FA3LL, g->GV(Df),
                       Df);
      break;
    case 42:
      HASH_INITIALIZED(0x319041AF0C4578AALL, g->GV(Da),
                       Da);
      break;
    case 44:
      HASH_INITIALIZED(0x796FE4BCDF633A6CLL, g->GV(De),
                       De);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      HASH_INITIALIZED(0x2611AACF478041EELL, g->GV(Ci),
                       Ci);
      break;
    case 53:
      HASH_INITIALIZED(0x4F58EAA6FDEC9FF5LL, g->GV(Cc),
                       Cc);
      break;
    case 54:
      HASH_INITIALIZED(0x067C114103BFD5B6LL, g->GV(Ce),
                       Ce);
      break;
    case 55:
      HASH_INITIALIZED(0x2B24BC213B6438F7LL, g->GV(Cf),
                       Cf);
      break;
    case 56:
      HASH_INITIALIZED(0x2348BACF443C5138LL, g->GV(Dg),
                       Dg);
      break;
    case 57:
      HASH_INITIALIZED(0x40DEF2A85D92FFF9LL, g->GV(Dh),
                       Dh);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
