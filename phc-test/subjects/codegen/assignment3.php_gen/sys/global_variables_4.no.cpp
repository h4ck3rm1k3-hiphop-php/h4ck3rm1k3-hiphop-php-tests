
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "Ca",
    "Cb",
    "Cc",
    "Cd",
    "Ce",
    "Cf",
    "Cg",
    "Ch",
    "Ci",
    "Cj",
    "Da",
    "Db",
    "Dc",
    "Dd",
    "De",
    "Df",
    "Dg",
    "Dh",
    "Di",
    "Dj",
  };
  if (idx >= 0 && idx < 32) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(Ca);
    case 13: return GV(Cb);
    case 14: return GV(Cc);
    case 15: return GV(Cd);
    case 16: return GV(Ce);
    case 17: return GV(Cf);
    case 18: return GV(Cg);
    case 19: return GV(Ch);
    case 20: return GV(Ci);
    case 21: return GV(Cj);
    case 22: return GV(Da);
    case 23: return GV(Db);
    case 24: return GV(Dc);
    case 25: return GV(Dd);
    case 26: return GV(De);
    case 27: return GV(Df);
    case 28: return GV(Dg);
    case 29: return GV(Dh);
    case 30: return GV(Di);
    case 31: return GV(Dj);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
