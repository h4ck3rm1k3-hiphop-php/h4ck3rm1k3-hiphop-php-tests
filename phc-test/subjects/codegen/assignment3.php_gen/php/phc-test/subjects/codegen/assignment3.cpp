
#include <php/phc-test/subjects/codegen/assignment3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$assignment3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/assignment3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$assignment3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_Ca __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ca") : g->GV(Ca);
  Variant &v_Cb __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Cb") : g->GV(Cb);
  Variant &v_Cc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Cc") : g->GV(Cc);
  Variant &v_Cd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Cd") : g->GV(Cd);
  Variant &v_Ce __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ce") : g->GV(Ce);
  Variant &v_Cf __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Cf") : g->GV(Cf);
  Variant &v_Cg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Cg") : g->GV(Cg);
  Variant &v_Ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ch") : g->GV(Ch);
  Variant &v_Ci __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ci") : g->GV(Ci);
  Variant &v_Cj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Cj") : g->GV(Cj);
  Variant &v_Da __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Da") : g->GV(Da);
  Variant &v_Db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Db") : g->GV(Db);
  Variant &v_Dc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Dc") : g->GV(Dc);
  Variant &v_Dd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Dd") : g->GV(Dd);
  Variant &v_De __attribute__((__unused__)) = (variables != gVariables) ? variables->get("De") : g->GV(De);
  Variant &v_Df __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Df") : g->GV(Df);
  Variant &v_Dg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Dg") : g->GV(Dg);
  Variant &v_Dh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Dh") : g->GV(Dh);
  Variant &v_Di __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Di") : g->GV(Di);
  Variant &v_Dj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Dj") : g->GV(Dj);

  (v_Ca = "Da");
  (v_Cb = "Db");
  (v_Cc = "Dc");
  (v_Cd = "Dd");
  (v_Ce = "De");
  (v_Cf = "Df");
  (v_Cg = "Dg");
  (v_Ch = "Dh");
  (v_Ci = "Di");
  (v_Cj = "Dj");
  (variables->get(toString(v_Ca)) = 10LL);
  (variables->get(toString(v_Cb)) = variables->get(toString(v_Ca)));
  (variables->get(toString(v_Cb)) = variables->get(toString(v_Cb)) + 1LL);
  LINE(29,x_var_export(variables->get(toString(v_Ca))));
  LINE(30,x_var_export(variables->get(toString(v_Cb))));
  LINE(31,x_var_export(v_Da));
  LINE(32,x_var_export(v_Db));
  (variables->get(toString(v_Cc)) = 20LL);
  (variables->get(toString(v_Cd)) = ref(variables->get(toString(v_Cc))));
  (variables->get(toString(v_Cd)) = variables->get(toString(v_Cd)) + 1LL);
  LINE(38,x_var_export(variables->get(toString(v_Cc))));
  LINE(39,x_var_export(variables->get(toString(v_Cd))));
  LINE(40,x_var_export(v_Dc));
  LINE(41,x_var_export(v_Dd));
  (variables->get(toString(v_Ce)) = 30LL);
  (variables->get(toString(v_Cf)) = ref(variables->get(toString(v_Ce))));
  (variables->get(toString(v_Cg)) = variables->get(toString(v_Cf)));
  (variables->get(toString(v_Cg)) = variables->get(toString(v_Cg)) + 1LL);
  LINE(48,x_var_export(variables->get(toString(v_Ce))));
  LINE(49,x_var_export(variables->get(toString(v_Cf))));
  LINE(50,x_var_export(variables->get(toString(v_Cg))));
  LINE(51,x_var_export(v_De));
  LINE(52,x_var_export(v_Df));
  LINE(53,x_var_export(v_Dg));
  (variables->get(toString(v_Ch)) = 40LL);
  (variables->get(toString(v_Ci)) = variables->get(toString(v_Ch)));
  (variables->get(toString(v_Cj)) = ref(variables->get(toString(v_Ci))));
  (variables->get(toString(v_Cj)) = variables->get(toString(v_Cj)) + 1LL);
  LINE(60,x_var_export(variables->get(toString(v_Ch))));
  LINE(61,x_var_export(variables->get(toString(v_Ci))));
  LINE(62,x_var_export(variables->get(toString(v_Cj))));
  LINE(63,x_var_export(v_Dh));
  LINE(64,x_var_export(v_Di));
  LINE(65,x_var_export(v_Dj));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
