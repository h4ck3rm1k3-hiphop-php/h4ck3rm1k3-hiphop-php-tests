
#include <php/phc-test/subjects/codegen/copy1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$copy1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/copy1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$copy1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = 5LL);
  (v_y = v_x);
  LINE(4,x_var_dump(1, v_y));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
