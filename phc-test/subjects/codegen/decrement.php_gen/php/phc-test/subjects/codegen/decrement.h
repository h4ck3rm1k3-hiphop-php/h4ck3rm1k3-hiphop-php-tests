
#ifndef __GENERATED_php_phc_test_subjects_codegen_decrement_h__
#define __GENERATED_php_phc_test_subjects_codegen_decrement_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/decrement.fw.h>

// Declarations
#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$decrement_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_a(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_decrement_h__
