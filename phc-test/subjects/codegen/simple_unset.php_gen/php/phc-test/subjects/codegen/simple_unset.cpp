
#include <php/phc-test/subjects/codegen/simple_unset.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$simple_unset_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/simple_unset.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$simple_unset_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  v_x.set(1LL, (1LL), 0x5BCA7C69B794F8CELL);
  v_x.set(2LL, (2LL), 0x486AFCC090D5F98CLL);
  v_x.weakRemove(1LL, 0x5BCA7C69B794F8CELL);
  LINE(5,x_var_dump(1, v_x));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
