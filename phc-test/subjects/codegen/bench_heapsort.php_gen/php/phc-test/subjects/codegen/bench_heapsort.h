
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_heapsort_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_heapsort_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_heapsort.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_heapsort_r(CVarRef v_n, Variant v_ra);
Numeric f_gen_random(int64 v_n);
Variant pm_php$phc_test$subjects$codegen$bench_heapsort_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_heapsort(int64 v_N);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_heapsort_h__
