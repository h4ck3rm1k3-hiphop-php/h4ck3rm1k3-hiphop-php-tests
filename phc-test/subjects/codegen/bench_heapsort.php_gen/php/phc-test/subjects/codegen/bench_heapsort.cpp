
#include <php/phc-test/subjects/codegen/bench_heapsort.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_IA = 3877LL;
const int64 k_IC = 29573LL;
const int64 k_IM = 139968LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_heapsort.php line 8 */
void f_heapsort_r(CVarRef v_n, Variant v_ra) {
  FUNCTION_INJECTION(heapsort_r);
  int64 v_l = 0;
  Variant v_ir;
  Variant v_rra;
  Numeric v_i = 0;
  Numeric v_j = 0;

  (v_l = (toInt64(toInt64(v_n)) >> 1LL) + 1LL);
  (v_ir = v_n);
  LOOP_COUNTER(1);
  {
    while (toBoolean(1LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        if (more(v_l, 1LL)) {
          (v_rra = v_ra.rvalAt(--v_l));
        }
        else {
          (v_rra = v_ra.rvalAt(v_ir));
          v_ra.set(v_ir, (v_ra.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
          if (equal(--v_ir, 1LL)) {
            v_ra.set(1LL, (v_rra), 0x5BCA7C69B794F8CELL);
            return;
          }
        }
        (v_i = v_l);
        (v_j = toInt64(v_l) << 1LL);
        LOOP_COUNTER(2);
        {
          while (not_more(v_j, v_ir)) {
            LOOP_COUNTER_CHECK(2);
            {
              if ((less(v_j, v_ir)) && (less(v_ra.rvalAt(v_j), v_ra.rvalAt(v_j + 1LL)))) {
                v_j++;
              }
              if (less(v_rra, v_ra.rvalAt(v_j))) {
                v_ra.set(v_i, (v_ra.rvalAt(v_j)));
                v_j += ((v_i = v_j));
              }
              else {
                (v_j = v_ir + 1LL);
              }
            }
          }
        }
        v_ra.set(v_i, (v_rra));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/codegen/bench_heapsort.php line 3 */
Numeric f_gen_random(int64 v_n) {
  FUNCTION_INJECTION(gen_random);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LAST __attribute__((__unused__)) = g->GV(LAST);
  return (divide((v_n * ((gv_LAST = modulo((toInt64(gv_LAST * 3877LL /* IA */ + 29573LL /* IC */)), 139968LL /* IM */)))), 139968LL /* IM */));
} /* function */
/* SRC: phc-test/subjects/codegen/bench_heapsort.php line 40 */
void f_heapsort(int64 v_N) {
  FUNCTION_INJECTION(heapsort);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LAST __attribute__((__unused__)) = g->GV(LAST);
  int64 v_i = 0;
  Variant v_ary;

  ;
  ;
  ;
  (gv_LAST = 42LL);
  {
    LOOP_COUNTER(3);
    for ((v_i = 1LL); not_more(v_i, v_N); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        v_ary.set(v_i, (LINE(49,f_gen_random(1LL))));
      }
    }
  }
  LINE(51,f_heapsort_r(v_N, ref(v_ary)));
  LINE(52,x_printf(2, "%.10f\n", Array(ArrayInit(1).set(0, v_ary.rvalAt(v_N)).create())));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_heapsort_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_heapsort.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_heapsort_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(55,f_heapsort(7LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
