
#ifndef __GENERATED_php_phc_test_subjects_codegen_overwrite_string_h__
#define __GENERATED_php_phc_test_subjects_codegen_overwrite_string_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/overwrite_string.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$overwrite_string_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_overwrite_string_h__
