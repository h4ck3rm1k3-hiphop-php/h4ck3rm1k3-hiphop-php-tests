
#include <php/phc-test/subjects/codegen/oop_new1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$oop_new1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_new1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_new1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);

  (v_b = ref(v_a));
  (v_b = ((Object)(LINE(3,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  LINE(4,x_var_dump(1, v_a));
  LINE(5,x_var_dump(1, v_b));
  (v_d = ref(v_c));
  (v_d = LINE(8,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())));
  LINE(9,x_var_dump(1, v_c));
  LINE(10,x_var_dump(1, v_d));
  (v_x = "StdClass");
  (v_f = ref(v_e));
  (v_f = LINE(15,create_object(toString(v_x), Array())));
  LINE(16,x_var_dump(1, v_e));
  LINE(17,x_var_dump(1, v_f));
  (v_h = ref(v_g));
  (v_h = LINE(20,create_object(toString(v_x), Array())));
  LINE(21,x_var_dump(1, v_g));
  LINE(22,x_var_dump(1, v_h));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
