
#ifndef __GENERATED_php_phc_test_subjects_codegen_braces_h__
#define __GENERATED_php_phc_test_subjects_codegen_braces_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/braces.fw.h>

// Declarations
#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_a();
Variant pm_php$phc_test$subjects$codegen$braces_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_b1();
void f_b2();
String f_c1();
void f_c2();
Object co_c(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_braces_h__
