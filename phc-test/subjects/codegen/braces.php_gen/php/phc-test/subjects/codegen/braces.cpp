
#include <php/phc-test/subjects/codegen/braces.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/braces.php line 28 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_x = 7LL;
}
/* SRC: phc-test/subjects/codegen/braces.php line 8 */
void f_a() {
  FUNCTION_INJECTION(a);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 v_x = 0;
  String v_y;


  class VariableTable : public RVariableTable {
  public:
    int64 &v_x; String &v_y;
    VariableTable(int64 &r_x, String &r_y) : v_x(r_x), v_y(r_y) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x04BFC205E59FA416LL, v_x,
                      x);
          HASH_RETURN(0x4F56B733A4DFC78ALL, v_y,
                      y);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_x, v_y);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_x = 6LL);
  (v_y = "x");
  throw_fatal("dynamic global");
  echo(toString(v_x) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/codegen/braces.php line 16 */
String f_b1() {
  FUNCTION_INJECTION(b1);
  return "x";
} /* function */
/* SRC: phc-test/subjects/codegen/braces.php line 21 */
void f_b2() {
  FUNCTION_INJECTION(b2);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 v_x = 0;


  class VariableTable : public RVariableTable {
  public:
    int64 &v_x;
    VariableTable(int64 &r_x) : v_x(r_x) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 1) {
        case 0:
          HASH_RETURN(0x04BFC205E59FA416LL, v_x,
                      x);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_x);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_x = 6LL);
  throw_fatal("dynamic global");
  echo(toString(v_x) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/codegen/braces.php line 30 */
String f_c1() {
  FUNCTION_INJECTION(c1);
  return "C";
} /* function */
/* SRC: phc-test/subjects/codegen/braces.php line 35 */
void f_c2() {
  FUNCTION_INJECTION(c2);
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$braces_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/braces.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$braces_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_a3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a3") : g->GV(a3);
  Variant &v_a4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a4") : g->GV(a4);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_x = 7LL);
  LINE(4,f_a());
  LINE(5,f_b2());
  LINE(6,f_c2());
  (v_a = "a1");
  (v_a1 = "a2");
  (v_a2 = "a3");
  (v_a3 = "a4");
  (v_a4 = "a5");
  (v_b = 7LL);
  (variables->get(toString(variables->get(toString(variables->get(toString(variables->get(toString(v_a)))))))) = variables->get(toString(variables->get(toString(variables->get(toString(variables->get(toString(v_b)))))))));
  LINE(50,x_var_dump(1, v_a));
  LINE(51,x_var_dump(1, v_a1));
  LINE(52,x_var_dump(1, v_a2));
  LINE(53,x_var_dump(1, v_a3));
  LINE(54,x_var_dump(1, v_a4));
  LINE(55,x_var_dump(1, variables->get(toString(v_a))));
  LINE(56,x_var_dump(1, variables->get(toString(variables->get(toString(v_a))))));
  LINE(57,x_var_dump(1, variables->get(toString(variables->get(toString(variables->get(toString(v_a))))))));
  LINE(58,x_var_dump(1, variables->get(toString(variables->get(toString(variables->get(toString(variables->get(toString(v_a))))))))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
