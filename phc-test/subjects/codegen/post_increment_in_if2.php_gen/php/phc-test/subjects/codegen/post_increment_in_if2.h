
#ifndef __GENERATED_php_phc_test_subjects_codegen_post_increment_in_if2_h__
#define __GENERATED_php_phc_test_subjects_codegen_post_increment_in_if2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/post_increment_in_if2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$post_increment_in_if2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_post_increment_in_if2_h__
