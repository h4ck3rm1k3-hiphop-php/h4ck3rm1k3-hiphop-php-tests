
#include <php/phc-test/subjects/codegen/post_increment_in_if2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$post_increment_in_if2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/post_increment_in_if2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$post_increment_in_if2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);

  (v_i = 0LL);
  if (equal(v_i++, 0LL)) {
    (v_j = 0LL);
    if (equal(v_j++, 0LL)) echo(LINE(7,concat5("(a,a) ", toString(v_i), " ", toString(v_j), "\n")));
    else echo(LINE(9,concat5("(a,b) ", toString(v_i), " ", toString(v_j), "\n")));
  }
  else {
    echo(LINE(13,concat3("(b) ", toString(v_i), "\n")));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
