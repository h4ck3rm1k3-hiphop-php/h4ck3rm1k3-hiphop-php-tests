
#ifndef __GENERATED_php_phc_test_subjects_codegen_call_evaled_fn_h__
#define __GENERATED_php_phc_test_subjects_codegen_call_evaled_fn_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/call_evaled_fn.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$call_evaled_fn_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_call_evaled_fn_h__
