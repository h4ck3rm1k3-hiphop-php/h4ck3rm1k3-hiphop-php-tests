
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_RETURN(0x00D51ABD28E2DE80LL, g->GV(Ae),
                  Ae);
      break;
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x4953807F2A294986LL, g->GV(Ad),
                  Ad);
      break;
    case 7:
      HASH_RETURN(0x52F43FA0242FC087LL, g->GV(Af),
                  Af);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 11:
      HASH_RETURN(0x76174AF66FFBABCBLL, g->GV(Ah),
                  Ah);
      break;
    case 12:
      HASH_RETURN(0x6AB0741113AE948CLL, g->GV(Ab),
                  Ab);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 21:
      HASH_RETURN(0x3CFDC91ABD11A815LL, g->GV(Aj),
                  Aj);
      break;
    case 23:
      HASH_RETURN(0x76E5E37E39135497LL, g->GV(Ac),
                  Ac);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 39:
      HASH_RETURN(0x480E703C96AB4DE7LL, g->GV(Aa),
                  Aa);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 51:
      HASH_RETURN(0x32C9CE5456396DB3LL, g->GV(Ag),
                  Ag);
      break;
    case 53:
      HASH_RETURN(0x6A85A0360A959E35LL, g->GV(Ai),
                  Ai);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
