
#include <php/phc-test/subjects/codegen/assignment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$assignment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/assignment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$assignment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_Aa __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Aa") : g->GV(Aa);
  Variant &v_Ab __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ab") : g->GV(Ab);
  Variant &v_Ac __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ac") : g->GV(Ac);
  Variant &v_Ad __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ad") : g->GV(Ad);
  Variant &v_Ae __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ae") : g->GV(Ae);
  Variant &v_Af __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Af") : g->GV(Af);
  Variant &v_Ag __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ag") : g->GV(Ag);
  Variant &v_Ah __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ah") : g->GV(Ah);
  Variant &v_Ai __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ai") : g->GV(Ai);
  Variant &v_Aj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Aj") : g->GV(Aj);

  (v_Aa = 10LL);
  (v_Ab = v_Aa);
  (v_Ab = v_Ab + 1LL);
  LINE(17,x_var_export(v_Aa));
  LINE(18,x_var_export(v_Ab));
  (v_Ac = 20LL);
  (v_Ad = ref(v_Ac));
  (v_Ad = v_Ad + 1LL);
  LINE(24,x_var_export(v_Ac));
  LINE(25,x_var_export(v_Ad));
  (v_Ae = 30LL);
  (v_Af = ref(v_Ae));
  (v_Ag = v_Af);
  (v_Ag = v_Ag + 1LL);
  LINE(32,x_var_export(v_Ae));
  LINE(33,x_var_export(v_Af));
  LINE(34,x_var_export(v_Ag));
  (v_Ah = 40LL);
  (v_Ai = v_Ah);
  (v_Aj = ref(v_Ai));
  (v_Aj = v_Aj + 1LL);
  LINE(41,x_var_export(v_Ah));
  LINE(42,x_var_export(v_Ai));
  LINE(43,x_var_export(v_Aj));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
