
#include <php/phc-test/subjects/codegen/isset_eval_order.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/isset_eval_order.php line 6 */
Primitive f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ind __attribute__((__unused__)) = g->GV(ind);
  echo(LINE(9,concat3("f ", toString(gv_ind), "\n")));
  return gv_ind++;
} /* function */
Variant i_f(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x286CE1C477560280LL, f) {
    return (f_f());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$codegen$isset_eval_order_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_eval_order.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_eval_order_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ind __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ind") : g->GV(ind);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_ind = 1LL);
  v_x.set(1LL, ("a"), 0x5BCA7C69B794F8CELL);
  if ((isset(v_x, LINE(15,f_f())) && isset(v_x, f_f()) && isset(v_x, f_f()))) {
    echo("yes\n");
  }
  else {
    echo("no\n");
  }
  v_x.set(4LL, ("b"), 0x6F2A25235E544A31LL);
  v_x.set(5LL, ("c"), 0x350AEB726A15D700LL);
  if ((isset(v_x, LINE(27,f_f())) && isset(v_x, f_f()) && isset(v_x, f_f()))) {
    echo("yes\n");
  }
  else {
    echo("no\n");
  }
  v_x.set(7LL, ("d"), 0x7D75B33B7AEB669DLL);
  v_x.set(8LL, ("e"), 0x21ABC084C3578135LL);
  v_x.set(9LL, ("f"), 0x3A1F6363F43EC697LL);
  if ((isset(v_x, LINE(40,f_f())) && isset(v_x, f_f()) && isset(v_x, f_f()))) {
    echo("yes\n");
  }
  else {
    echo("no\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
