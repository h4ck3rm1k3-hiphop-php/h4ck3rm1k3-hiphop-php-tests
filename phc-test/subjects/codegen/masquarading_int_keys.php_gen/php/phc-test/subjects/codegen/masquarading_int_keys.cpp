
#include <php/phc-test/subjects/codegen/masquarading_int_keys.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$masquarading_int_keys_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/masquarading_int_keys.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$masquarading_int_keys_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);

  (v_x = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_x.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_key = iter3->second();
      {
        v_y.set(v_key, (v_key));
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_y.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_value = iter6->second();
      v_key = iter6->first();
      {
        LINE(47,x_var_dump(1, v_key));
        LINE(48,x_var_dump(1, v_value));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
