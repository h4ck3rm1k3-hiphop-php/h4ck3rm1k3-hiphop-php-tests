
#include <php/phc-test/subjects/codegen/same_parameters.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/same_parameters.php line 4 */
void f_x(int64 v_p, int64 v_p0, int64 v_p0) {
  FUNCTION_INJECTION(x);
  LINE(6,x_var_dump(1, v_p));
} /* function */
/* SRC: phc-test/subjects/codegen/same_parameters.php line 12 */
void f_y(Variant v_p, int64 v_p0) {
  FUNCTION_INJECTION(y);
  LINE(14,x_var_dump(1, v_p));
} /* function */
Variant pm_php$phc_test$subjects$codegen$same_parameters_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/same_parameters.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$same_parameters_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  LINE(9,f_x(1LL, 2LL, 3LL));
  (v_y = 5LL);
  LINE(18,f_y(ref(v_y), 6LL));
  LINE(20,x_var_dump(1, v_y));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
