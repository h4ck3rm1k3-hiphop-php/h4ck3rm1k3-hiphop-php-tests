
#ifndef __GENERATED_php_phc_test_subjects_codegen_same_parameters_h__
#define __GENERATED_php_phc_test_subjects_codegen_same_parameters_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/same_parameters.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$same_parameters_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_x(int64 v_p, int64 v_p0, int64 v_p0);
void f_y(Variant v_p, int64 v_p0);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_same_parameters_h__
