
#include <php/phc-test/subjects/codegen/isset_2d_int.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$isset_2d_int_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_2d_int.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_2d_int_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = ScalarArrays::sa_[0]);
  if (isset(v_x, 1LL, 0x5BCA7C69B794F8CELL)) echo("x[1] is set (1)\n");
  if (isset(v_x.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 2LL, 0x486AFCC090D5F98CLL)) echo("x[1][2] is set (1)\n");
  if (isset(v_x.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 3LL, 0x135FDDF6A6BFBBDDLL)) echo("x[1][3] is set (1)\n");
  lval(v_x.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(2LL, (1LL), 0x486AFCC090D5F98CLL);
  if (isset(v_x, 1LL, 0x5BCA7C69B794F8CELL)) echo("x[1] is set (1)\n");
  if (isset(v_x.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 2LL, 0x486AFCC090D5F98CLL)) echo("x[1][2] is set (1)\n");
  if (isset(v_x.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 3LL, 0x135FDDF6A6BFBBDDLL)) echo("x[1][3] is set (1)\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
