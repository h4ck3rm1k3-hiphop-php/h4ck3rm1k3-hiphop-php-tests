
#ifndef __GENERATED_php_phc_test_subjects_codegen_isset_2d_int_h__
#define __GENERATED_php_phc_test_subjects_codegen_isset_2d_int_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/isset_2d_int.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$isset_2d_int_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_isset_2d_int_h__
