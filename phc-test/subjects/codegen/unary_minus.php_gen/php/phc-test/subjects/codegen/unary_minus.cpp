
#include <php/phc-test/subjects/codegen/unary_minus.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$unary_minus_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/unary_minus.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$unary_minus_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  (v_x = 0LL);
  (v_y = 0LL);
  LINE(4,x_var_dump(1, v_x));
  LINE(5,x_var_dump(1, v_y));
  (v_x = -3.1400000000000001);
  (v_y = -3.1400000000000001);
  LINE(9,x_var_dump(1, v_x));
  LINE(10,x_var_dump(1, v_y));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
