
#ifndef __GENERATED_php_phc_test_subjects_codegen_unary_minus_h__
#define __GENERATED_php_phc_test_subjects_codegen_unary_minus_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/unary_minus.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$unary_minus_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_unary_minus_h__
