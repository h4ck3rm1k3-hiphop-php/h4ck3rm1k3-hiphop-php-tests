
#include <php/phc-test/subjects/codegen/wordy_operators.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$wordy_operators_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/wordy_operators.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$wordy_operators_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_T __attribute__((__unused__)) = (variables != gVariables) ? variables->get("T") : g->GV(T);
  Variant &v_F __attribute__((__unused__)) = (variables != gVariables) ? variables->get("F") : g->GV(F);

  (v_T = true);
  (v_F = false);
  LINE(6,x_var_dump(1, (toBoolean(v_T)) && (toBoolean(v_F))));
  LINE(7,x_var_dump(1, (toBoolean(v_T)) || (toBoolean(v_F))));
  LINE(8,x_var_dump(1, logical_xor(toBoolean(v_T), toBoolean(v_F))));
  LINE(10,x_var_dump(1, (toBoolean(v_T)) && (toBoolean(v_F))));
  LINE(11,x_var_dump(1, (toBoolean(v_T)) || (toBoolean(v_F))));
  LINE(12,x_var_dump(1, logical_xor(toBoolean(v_T), toBoolean(v_F))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
