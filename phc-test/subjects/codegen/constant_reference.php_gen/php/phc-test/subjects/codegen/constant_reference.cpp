
#include <php/phc-test/subjects/codegen/constant_reference.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_ACONST = "ACONST";

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$constant_reference_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/constant_reference.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$constant_reference_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_y = 5LL);
  (v_x = ref(v_y));
  LINE(8,throw_fatal("bad define"));
  LINE(10,x_var_dump(1, v_x));
  (v_y = 6LL);
  LINE(14,x_var_dump(1, v_x));
  LINE(15,x_var_dump(1, k_ACONST));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
