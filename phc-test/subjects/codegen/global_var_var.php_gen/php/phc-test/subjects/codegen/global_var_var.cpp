
#include <php/phc-test/subjects/codegen/global_var_var.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/global_var_var.php line 2 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  String v_x;
  int64 v_a = 0;


  class VariableTable : public RVariableTable {
  public:
    String &v_x; int64 &v_a;
    VariableTable(String &r_x, int64 &r_a) : v_x(r_x), v_a(r_a) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x04BFC205E59FA416LL, v_x,
                      x);
          HASH_RETURN(0x4292CEE227B9150ALL, v_a,
                      a);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_x, v_a);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_x = "a");
  throw_fatal("dynamic global");
  (v_a = 1LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$global_var_var_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/global_var_var.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$global_var_var_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  LINE(9,f_f());
  echo(toString(v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
