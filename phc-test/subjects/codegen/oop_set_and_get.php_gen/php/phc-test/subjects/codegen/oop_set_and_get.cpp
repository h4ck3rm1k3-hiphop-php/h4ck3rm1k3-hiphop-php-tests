
#include <php/phc-test/subjects/codegen/oop_set_and_get.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 2 */
Variant c_c1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c1::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a1", m_a1.isReferenced() ? ref(m_a1) : m_a1));
  c_ObjectData::o_get(props);
}
bool c_c1::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x68DF81F26D942FC7LL, a1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c1::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x68DF81F26D942FC7LL, m_a1,
                         a1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x68DF81F26D942FC7LL, m_a1,
                      a1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c1::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x68DF81F26D942FC7LL, m_a1,
                         a1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c1)
ObjectData *c_c1::cloneImpl() {
  c_c1 *obj = NEW(c_c1)();
  cloneSet(obj);
  return obj;
}
void c_c1::cloneSet(c_c1 *clone) {
  clone->m_a1 = m_a1.isReferenced() ? ref(m_a1) : m_a1;
  ObjectData::cloneSet(clone);
}
Variant c_c1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x5CFDA9683DFCD331LL, f2) {
        return (t_f2(), null);
      }
      HASH_GUARD(0x34AC2E4B259E9021LL, f3) {
        return (t_f3(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c1$os_get(const char *s) {
  return c_c1::os_get(s, -1);
}
Variant &cw_c1$os_lval(const char *s) {
  return c_c1::os_lval(s, -1);
}
Variant cw_c1$os_constant(const char *s) {
  return c_c1::os_constant(s);
}
Variant cw_c1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c1::os_invoke(c, s, params, -1, fatal);
}
void c_c1::init() {
  m_a1 = null;
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 8 */
void c_c1::t_f1() {
  INSTANCE_METHOD_INJECTION(C1, C1::f1);
  int64 v_a1 = 0;

  echo("<C1::f1>\n");
  (v_a1 = 1LL);
  (m_a1 = 2LL);
  LINE(13,x_var_dump(1, v_a1));
  LINE(14,x_var_dump(1, m_a1));
  echo("</C1::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 20 */
void c_c1::t_f2() {
  INSTANCE_METHOD_INJECTION(C1, C1::f2);
  echo("<C1::f2>\n");
  (o_lval("a2", 0x6C05C2858C72A876LL) = 3LL);
  LINE(24,x_var_dump(1, o_get("a2", 0x6C05C2858C72A876LL)));
  LINE(26,x_var_dump(1, o_get("a3", 0x43EDA7BEE714570DLL)));
  LINE(27,x_var_dump(1, o_get("a3", 0x43EDA7BEE714570DLL)));
  (o_lval("a3", 0x43EDA7BEE714570DLL) = 4LL);
  (o_lval("a3", 0x43EDA7BEE714570DLL) = 5LL);
  LINE(30,x_var_dump(1, o_get("a3", 0x43EDA7BEE714570DLL)));
  echo("</C1::f2>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 36 */
void c_c1::t_f3() {
  INSTANCE_METHOD_INJECTION(C1, C1::f3);
  String v_x;

  echo("<C1::f3>\n");
  (v_x = "a4");
  LINE(42,x_var_dump(1, o_get(v_x, -1LL)));
  (o_lval(v_x, -1LL) = 6LL);
  LINE(44,x_var_dump(1, o_get(v_x, -1LL)));
  echo("</C1::f3>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 49 */
Variant c_c1::t___get(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C1, C1::__get);
  echo(LINE(51,concat3("C1::__get(", toString(v_var), ")\n")));
  return o_get(toString(v_var), -1LL);
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 49 */
Variant &c_c1::___lval(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C1, C1::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_var);
  return v;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 55 */
Variant c_c1::t___set(Variant v_var, Variant v_value) {
  INSTANCE_METHOD_INJECTION(C1, C1::__set);
  echo(LINE(57,concat3("C1::__set(", toString(v_var), ")\n")));
  (o_lval(toString(v_var), -1LL) = v_value);
  return null;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 63 */
Variant c_c2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c2)
ObjectData *c_c2::cloneImpl() {
  c_c2 *obj = NEW(c_c2)();
  cloneSet(obj);
  return obj;
}
void c_c2::cloneSet(c_c2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c2$os_get(const char *s) {
  return c_c2::os_get(s, -1);
}
Variant &cw_c2$os_lval(const char *s) {
  return c_c2::os_lval(s, -1);
}
Variant cw_c2$os_constant(const char *s) {
  return c_c2::os_constant(s);
}
Variant cw_c2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c2::os_invoke(c, s, params, -1, fatal);
}
void c_c2::init() {
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 65 */
void c_c2::t_f1() {
  INSTANCE_METHOD_INJECTION(C2, C2::f1);
  echo("<C2::f1>\n");
  LINE(68,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  (o_lval("a1", 0x68DF81F26D942FC7LL) = 7LL);
  LINE(70,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  echo("</C2::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 74 */
Variant c_c2::t___get(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C2, C2::__get);
  echo(LINE(76,concat3("C2::__get(", toString(v_var), ")\n")));
  return o_get(toString(v_var), -1LL);
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 74 */
Variant &c_c2::___lval(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C2, C2::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_var);
  return v;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 82 */
Variant c_c3::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c3::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c3::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c3::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c3::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c3::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c3::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c3::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c3)
ObjectData *c_c3::cloneImpl() {
  c_c3 *obj = NEW(c_c3)();
  cloneSet(obj);
  return obj;
}
void c_c3::cloneSet(c_c3 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c3::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c3::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c3::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c3$os_get(const char *s) {
  return c_c3::os_get(s, -1);
}
Variant &cw_c3$os_lval(const char *s) {
  return c_c3::os_lval(s, -1);
}
Variant cw_c3$os_constant(const char *s) {
  return c_c3::os_constant(s);
}
Variant cw_c3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c3::os_invoke(c, s, params, -1, fatal);
}
void c_c3::init() {
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 84 */
void c_c3::t_f1() {
  INSTANCE_METHOD_INJECTION(C3, C3::f1);
  echo("<C3::f1>\n");
  LINE(87,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  (o_lval("a1", 0x68DF81F26D942FC7LL) = 7LL);
  LINE(89,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  echo("</C3::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 93 */
Variant c_c3::t___set(Variant v_var, Variant v_value) {
  INSTANCE_METHOD_INJECTION(C3, C3::__set);
  echo(LINE(95,concat3("C3::__set(", toString(v_var), ")\n")));
  (o_lval(toString(v_var), -1LL) = v_value);
  return null;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 101 */
Variant c_c4::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c4::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c4::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c4::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c4::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c4::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c4::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c4::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c4)
ObjectData *c_c4::cloneImpl() {
  c_c4 *obj = NEW(c_c4)();
  cloneSet(obj);
  return obj;
}
void c_c4::cloneSet(c_c4 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c4::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c4::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c4::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c4$os_get(const char *s) {
  return c_c4::os_get(s, -1);
}
Variant &cw_c4$os_lval(const char *s) {
  return c_c4::os_lval(s, -1);
}
Variant cw_c4$os_constant(const char *s) {
  return c_c4::os_constant(s);
}
Variant cw_c4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c4::os_invoke(c, s, params, -1, fatal);
}
void c_c4::init() {
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 103 */
void c_c4::t_f1() {
  INSTANCE_METHOD_INJECTION(C4, C4::f1);
  echo("<C4::f1>\n");
  LINE(106,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  (o_lval("a1", 0x68DF81F26D942FC7LL) = 7LL);
  LINE(108,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  echo("</C4::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 115 */
Variant c_c5::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c5::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c5::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c5::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c5::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c5::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c5::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c5::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c5)
ObjectData *c_c5::cloneImpl() {
  c_c5 *obj = NEW(c_c5)();
  cloneSet(obj);
  return obj;
}
void c_c5::cloneSet(c_c5 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c5::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c5::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c5::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c5$os_get(const char *s) {
  return c_c5::os_get(s, -1);
}
Variant &cw_c5$os_lval(const char *s) {
  return c_c5::os_lval(s, -1);
}
Variant cw_c5$os_constant(const char *s) {
  return c_c5::os_constant(s);
}
Variant cw_c5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c5::os_invoke(c, s, params, -1, fatal);
}
void c_c5::init() {
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 117 */
void c_c5::t_f1() {
  INSTANCE_METHOD_INJECTION(C5, C5::f1);
  echo("<C5::f1>\n");
  LINE(120,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  LINE(121,x_var_dump(1, o_get("a2", 0x6C05C2858C72A876LL)));
  echo("</C5::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 125 */
Variant c_c5::t___get(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C5, C5::__get);
  echo(LINE(127,concat3("C5::__get(", toString(v_var), ")\n")));
  return o_get("a2", 0x6C05C2858C72A876LL);
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 125 */
Variant &c_c5::___lval(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C5, C5::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_var);
  return v;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 134 */
Variant c_c6::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c6::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c6::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c6::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c6::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c6::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c6::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c6::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c6)
ObjectData *c_c6::cloneImpl() {
  c_c6 *obj = NEW(c_c6)();
  cloneSet(obj);
  return obj;
}
void c_c6::cloneSet(c_c6 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c6::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c6::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c6::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c6$os_get(const char *s) {
  return c_c6::os_get(s, -1);
}
Variant &cw_c6$os_lval(const char *s) {
  return c_c6::os_lval(s, -1);
}
Variant cw_c6$os_constant(const char *s) {
  return c_c6::os_constant(s);
}
Variant cw_c6$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c6::os_invoke(c, s, params, -1, fatal);
}
void c_c6::init() {
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 136 */
void c_c6::t_f1() {
  INSTANCE_METHOD_INJECTION(C6, C6::f1);
  DECLARE_GLOBAL_VARIABLES(g);
  echo("<C6::f1>\n");
  (o_lval("a1", 0x68DF81F26D942FC7LL) = 5LL);
  t___unset("a2");
  (o_lval("a2", 0x6C05C2858C72A876LL) = 5LL);
  echo("</C6::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 145 */
Variant c_c6::t___set(Variant v_var, Variant v_value) {
  INSTANCE_METHOD_INJECTION(C6, C6::__set);
  echo(LINE(147,concat3("C6::__set(", toString(v_var), ")\n")));
  (o_lval("a2", 0x6C05C2858C72A876LL) = v_value);
  return null;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 155 */
Variant c_c7::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c7::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c7::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c7::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c7::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c7::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c7::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c7::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c7)
ObjectData *c_c7::cloneImpl() {
  c_c7 *obj = NEW(c_c7)();
  cloneSet(obj);
  return obj;
}
void c_c7::cloneSet(c_c7 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c7::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c7::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c7::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c7$os_get(const char *s) {
  return c_c7::os_get(s, -1);
}
Variant &cw_c7$os_lval(const char *s) {
  return c_c7::os_lval(s, -1);
}
Variant cw_c7$os_constant(const char *s) {
  return c_c7::os_constant(s);
}
Variant cw_c7$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c7::os_invoke(c, s, params, -1, fatal);
}
void c_c7::init() {
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 157 */
void c_c7::t_f1() {
  INSTANCE_METHOD_INJECTION(C7, C7::f1);
  DECLARE_GLOBAL_VARIABLES(g);
  echo("<C7::f1>\n");
  LINE(160,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  t___unset("a2");
  echo("****\n");
  LINE(163,x_var_dump(1, o_get("a2", 0x6C05C2858C72A876LL)));
  echo("****\n");
  (o_lval("a3", 0x43EDA7BEE714570DLL) = 5LL);
  echo("</C7::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 169 */
Variant c_c7::t___get(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C7, C7::__get);
  echo(LINE(171,concat3("C7::__get(", toString(v_var), ")\n")));
  (o_lval("a2", 0x6C05C2858C72A876LL) = 1LL);
  return o_get(toString(v_var), -1LL);
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 169 */
Variant &c_c7::___lval(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C7, C7::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_var);
  return v;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 176 */
Variant c_c7::t___set(Variant v_var, Variant v_value) {
  INSTANCE_METHOD_INJECTION(C7, C7::__set);
  echo(LINE(178,concat3("C7::__set(", toString(v_var), ")\n")));
  LINE(179,x_var_dump(1, o_get("a3", 0x43EDA7BEE714570DLL)));
  (o_lval(toString(v_var), -1LL) = v_value);
  return null;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 186 */
Variant c_c8::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c8::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c8::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c8::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c8::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c8::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c8::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c8::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c8)
ObjectData *c_c8::cloneImpl() {
  c_c8 *obj = NEW(c_c8)();
  cloneSet(obj);
  return obj;
}
void c_c8::cloneSet(c_c8 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c8::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c8::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3B2E52612E6BC133LL, f1) {
        return (t_f1(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c8::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c8$os_get(const char *s) {
  return c_c8::os_get(s, -1);
}
Variant &cw_c8$os_lval(const char *s) {
  return c_c8::os_lval(s, -1);
}
Variant cw_c8$os_constant(const char *s) {
  return c_c8::os_constant(s);
}
Variant cw_c8$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c8::os_invoke(c, s, params, -1, fatal);
}
void c_c8::init() {
}
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 188 */
void c_c8::t_f1() {
  INSTANCE_METHOD_INJECTION(C8, C8::f1);
  echo("<C8::f1>\n");
  LINE(191,x_var_dump(1, o_get("a1", 0x68DF81F26D942FC7LL)));
  echo("</C8::f1>\n");
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 195 */
Variant c_c8::t___get(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C8, C8::__get);
  echo(LINE(197,concat3("C8::__get(", toString(v_var), ")\n")));
  return LINE(198,t_g(v_var));
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 195 */
Variant &c_c8::___lval(Variant v_var) {
  INSTANCE_METHOD_INJECTION(C8, C8::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_var);
  return v;
} /* function */
/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 201 */
Variant c_c8::t_g(CVarRef v_var) {
  INSTANCE_METHOD_INJECTION(C8, C8::g);
  echo(LINE(203,concat3("C8::g(", toString(v_var), ")\n")));
  return o_get(toString(v_var), -1LL);
} /* function */
Object co_c1(CArrRef params, bool init /* = true */) {
  return Object(p_c1(NEW(c_c1)())->dynCreate(params, init));
}
Object co_c2(CArrRef params, bool init /* = true */) {
  return Object(p_c2(NEW(c_c2)())->dynCreate(params, init));
}
Object co_c3(CArrRef params, bool init /* = true */) {
  return Object(p_c3(NEW(c_c3)())->dynCreate(params, init));
}
Object co_c4(CArrRef params, bool init /* = true */) {
  return Object(p_c4(NEW(c_c4)())->dynCreate(params, init));
}
Object co_c5(CArrRef params, bool init /* = true */) {
  return Object(p_c5(NEW(c_c5)())->dynCreate(params, init));
}
Object co_c6(CArrRef params, bool init /* = true */) {
  return Object(p_c6(NEW(c_c6)())->dynCreate(params, init));
}
Object co_c7(CArrRef params, bool init /* = true */) {
  return Object(p_c7(NEW(c_c7)())->dynCreate(params, init));
}
Object co_c8(CArrRef params, bool init /* = true */) {
  return Object(p_c8(NEW(c_c8)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$oop_set_and_get_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_set_and_get.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_set_and_get_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c1") : g->GV(c1);
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_c2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c2") : g->GV(c2);
  Variant &v_c3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c3") : g->GV(c3);
  Variant &v_c4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c4") : g->GV(c4);
  Variant &v_c5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c5") : g->GV(c5);
  Variant &v_c6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c6") : g->GV(c6);
  Variant &v_c7 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c7") : g->GV(c7);
  Variant &v_c8 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c8") : g->GV(c8);

  (v_c1 = ((Object)(LINE(208,p_c1(p_c1(NEWOBJ(c_c1)())->create())))));
  LINE(210,v_c1.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0));
  LINE(211,x_var_dump(1, v_a1));
  LINE(213,v_c1.o_invoke_few_args("f2", 0x5CFDA9683DFCD331LL, 0));
  LINE(214,v_c1.o_invoke_few_args("f3", 0x34AC2E4B259E9021LL, 0));
  (v_c2 = ((Object)(LINE(216,p_c2(p_c2(NEWOBJ(c_c2)())->create())))));
  v_c2.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0);
  (v_c3 = ((Object)(LINE(217,p_c3(p_c3(NEWOBJ(c_c3)())->create())))));
  v_c3.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0);
  (v_c4 = ((Object)(LINE(218,p_c4(p_c4(NEWOBJ(c_c4)())->create())))));
  v_c4.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0);
  (v_c5 = ((Object)(LINE(219,p_c5(p_c5(NEWOBJ(c_c5)())->create())))));
  v_c5.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0);
  (v_c6 = ((Object)(LINE(220,p_c6(p_c6(NEWOBJ(c_c6)())->create())))));
  v_c6.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0);
  (v_c7 = ((Object)(LINE(221,p_c7(p_c7(NEWOBJ(c_c7)())->create())))));
  v_c7.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0);
  (v_c8 = ((Object)(LINE(222,p_c8(p_c8(NEWOBJ(c_c8)())->create())))));
  v_c8.o_invoke_few_args("f1", 0x3B2E52612E6BC133LL, 0);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
