
#ifndef __GENERATED_php_phc_test_subjects_codegen_oop_set_and_get_fw_h__
#define __GENERATED_php_phc_test_subjects_codegen_oop_set_and_get_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(c1)
FORWARD_DECLARE_CLASS(c2)
FORWARD_DECLARE_CLASS(c3)
FORWARD_DECLARE_CLASS(c4)
FORWARD_DECLARE_CLASS(c5)
FORWARD_DECLARE_CLASS(c6)
FORWARD_DECLARE_CLASS(c7)
FORWARD_DECLARE_CLASS(c8)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_oop_set_and_get_fw_h__
