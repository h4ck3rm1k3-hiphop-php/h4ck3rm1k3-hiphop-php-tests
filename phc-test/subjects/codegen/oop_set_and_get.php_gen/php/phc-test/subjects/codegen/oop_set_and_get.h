
#ifndef __GENERATED_php_phc_test_subjects_codegen_oop_set_and_get_h__
#define __GENERATED_php_phc_test_subjects_codegen_oop_set_and_get_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/oop_set_and_get.fw.h>

// Declarations
#include <cls/c1.h>
#include <cls/c2.h>
#include <cls/c3.h>
#include <cls/c4.h>
#include <cls/c5.h>
#include <cls/c6.h>
#include <cls/c7.h>
#include <cls/c8.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$oop_set_and_get_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_c1(CArrRef params, bool init = true);
Object co_c2(CArrRef params, bool init = true);
Object co_c3(CArrRef params, bool init = true);
Object co_c4(CArrRef params, bool init = true);
Object co_c5(CArrRef params, bool init = true);
Object co_c6(CArrRef params, bool init = true);
Object co_c7(CArrRef params, bool init = true);
Object co_c8(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_oop_set_and_get_h__
