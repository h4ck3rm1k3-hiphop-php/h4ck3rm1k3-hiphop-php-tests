
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_c1(CArrRef params, bool init = true);
Variant cw_c1$os_get(const char *s);
Variant &cw_c1$os_lval(const char *s);
Variant cw_c1$os_constant(const char *s);
Variant cw_c1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_c2(CArrRef params, bool init = true);
Variant cw_c2$os_get(const char *s);
Variant &cw_c2$os_lval(const char *s);
Variant cw_c2$os_constant(const char *s);
Variant cw_c2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_c3(CArrRef params, bool init = true);
Variant cw_c3$os_get(const char *s);
Variant &cw_c3$os_lval(const char *s);
Variant cw_c3$os_constant(const char *s);
Variant cw_c3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_c4(CArrRef params, bool init = true);
Variant cw_c4$os_get(const char *s);
Variant &cw_c4$os_lval(const char *s);
Variant cw_c4$os_constant(const char *s);
Variant cw_c4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_c5(CArrRef params, bool init = true);
Variant cw_c5$os_get(const char *s);
Variant &cw_c5$os_lval(const char *s);
Variant cw_c5$os_constant(const char *s);
Variant cw_c5$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_c6(CArrRef params, bool init = true);
Variant cw_c6$os_get(const char *s);
Variant &cw_c6$os_lval(const char *s);
Variant cw_c6$os_constant(const char *s);
Variant cw_c6$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_c7(CArrRef params, bool init = true);
Variant cw_c7$os_get(const char *s);
Variant &cw_c7$os_lval(const char *s);
Variant cw_c7$os_constant(const char *s);
Variant cw_c7$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_c8(CArrRef params, bool init = true);
Variant cw_c8$os_get(const char *s);
Variant &cw_c8$os_lval(const char *s);
Variant cw_c8$os_constant(const char *s);
Variant cw_c8$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_CREATE_OBJECT(0x47253AED9801BE60LL, c6);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x13D47374897E9A03LL, c2);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x258329FB5D91B9B6LL, c3);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x6507A27C53590CA9LL, c1);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x146F67A8E8BCD6EALL, c8);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x6CA193B9F7F72E6CLL, c7);
      break;
    case 13:
      HASH_CREATE_OBJECT(0x3D43E98B682A7E3DLL, c5);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x2B8D4A4B65E5FCAFLL, c4);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x47253AED9801BE60LL, c6);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x13D47374897E9A03LL, c2);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x258329FB5D91B9B6LL, c3);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x6507A27C53590CA9LL, c1);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x146F67A8E8BCD6EALL, c8);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x6CA193B9F7F72E6CLL, c7);
      break;
    case 13:
      HASH_INVOKE_STATIC_METHOD(0x3D43E98B682A7E3DLL, c5);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x2B8D4A4B65E5FCAFLL, c4);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x47253AED9801BE60LL, c6);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x13D47374897E9A03LL, c2);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x258329FB5D91B9B6LL, c3);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x6507A27C53590CA9LL, c1);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x146F67A8E8BCD6EALL, c8);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x6CA193B9F7F72E6CLL, c7);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY(0x3D43E98B682A7E3DLL, c5);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x2B8D4A4B65E5FCAFLL, c4);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x47253AED9801BE60LL, c6);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x13D47374897E9A03LL, c2);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x258329FB5D91B9B6LL, c3);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x6507A27C53590CA9LL, c1);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x146F67A8E8BCD6EALL, c8);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x6CA193B9F7F72E6CLL, c7);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY_LV(0x3D43E98B682A7E3DLL, c5);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x2B8D4A4B65E5FCAFLL, c4);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x47253AED9801BE60LL, c6);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x13D47374897E9A03LL, c2);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x258329FB5D91B9B6LL, c3);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x6507A27C53590CA9LL, c1);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x146F67A8E8BCD6EALL, c8);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x6CA193B9F7F72E6CLL, c7);
      break;
    case 13:
      HASH_GET_CLASS_CONSTANT(0x3D43E98B682A7E3DLL, c5);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x2B8D4A4B65E5FCAFLL, c4);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
