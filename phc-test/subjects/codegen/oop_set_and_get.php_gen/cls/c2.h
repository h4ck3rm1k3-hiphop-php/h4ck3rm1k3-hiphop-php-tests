
#ifndef __GENERATED_cls_c2_h__
#define __GENERATED_cls_c2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 63 */
class c_c2 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c2)
  END_CLASS_MAP(c2)
  DECLARE_CLASS(c2, C2, ObjectData)
  void init();
  public: void t_f1();
  public: Variant t___get(Variant v_var);
  public: Variant &___lval(Variant v_var);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c2_h__
