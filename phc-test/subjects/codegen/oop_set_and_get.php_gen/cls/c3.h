
#ifndef __GENERATED_cls_c3_h__
#define __GENERATED_cls_c3_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 82 */
class c_c3 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c3)
  END_CLASS_MAP(c3)
  DECLARE_CLASS(c3, C3, ObjectData)
  void init();
  public: void t_f1();
  public: Variant t___set(Variant v_var, Variant v_value);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c3_h__
