
#ifndef __GENERATED_cls_c1_h__
#define __GENERATED_cls_c1_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 2 */
class c_c1 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c1)
  END_CLASS_MAP(c1)
  DECLARE_CLASS(c1, C1, ObjectData)
  void init();
  public: Variant m_a1;
  public: void t_f1();
  public: void t_f2();
  public: void t_f3();
  public: Variant t___get(Variant v_var);
  public: Variant &___lval(Variant v_var);
  public: Variant t___set(Variant v_var, Variant v_value);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c1_h__
