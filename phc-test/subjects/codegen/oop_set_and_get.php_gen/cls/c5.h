
#ifndef __GENERATED_cls_c5_h__
#define __GENERATED_cls_c5_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 115 */
class c_c5 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c5)
  END_CLASS_MAP(c5)
  DECLARE_CLASS(c5, C5, ObjectData)
  void init();
  public: void t_f1();
  public: Variant t___get(Variant v_var);
  public: Variant &___lval(Variant v_var);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c5_h__
