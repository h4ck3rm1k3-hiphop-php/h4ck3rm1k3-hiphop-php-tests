
#ifndef __GENERATED_cls_c8_h__
#define __GENERATED_cls_c8_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 186 */
class c_c8 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c8)
  END_CLASS_MAP(c8)
  DECLARE_CLASS(c8, C8, ObjectData)
  void init();
  public: void t_f1();
  public: Variant t___get(Variant v_var);
  public: Variant &___lval(Variant v_var);
  public: Variant t_g(CVarRef v_var);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c8_h__
