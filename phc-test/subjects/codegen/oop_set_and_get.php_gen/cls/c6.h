
#ifndef __GENERATED_cls_c6_h__
#define __GENERATED_cls_c6_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 134 */
class c_c6 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c6)
  END_CLASS_MAP(c6)
  DECLARE_CLASS(c6, C6, ObjectData)
  void init();
  public: void t_f1();
  public: Variant t___set(Variant v_var, Variant v_value);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c6_h__
