
#ifndef __GENERATED_cls_c7_h__
#define __GENERATED_cls_c7_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/oop_set_and_get.php line 155 */
class c_c7 : virtual public ObjectData {
  BEGIN_CLASS_MAP(c7)
  END_CLASS_MAP(c7)
  DECLARE_CLASS(c7, C7, ObjectData)
  void init();
  public: void t_f1();
  public: Variant t___get(Variant v_var);
  public: Variant &___lval(Variant v_var);
  public: Variant t___set(Variant v_var, Variant v_value);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c7_h__
