
#ifndef __GENERATED_php_phc_test_subjects_codegen_functioncalls_compiled_h__
#define __GENERATED_php_phc_test_subjects_codegen_functioncalls_compiled_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/functioncalls_compiled.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_fun_r(Variant v_x);
Variant pm_php$phc_test$subjects$codegen$functioncalls_compiled_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_fun(Variant v_x);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_functioncalls_compiled_h__
