
#include <php/phc-test/subjects/codegen/functioncalls_compiled.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/functioncalls_compiled.php line 12 */
void f_fun_r(Variant v_x) {
  FUNCTION_INJECTION(fun_r);
  (v_x = "x");
} /* function */
/* SRC: phc-test/subjects/codegen/functioncalls_compiled.php line 11 */
void f_fun(Variant v_x) {
  FUNCTION_INJECTION(fun);
  (v_x = "x");
} /* function */
Variant pm_php$phc_test$subjects$codegen$functioncalls_compiled_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/functioncalls_compiled.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$functioncalls_compiled_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_l __attribute__((__unused__)) = (variables != gVariables) ? variables->get("l") : g->GV(l);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_p __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p") : g->GV(p);
  Variant &v_q __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q") : g->GV(q);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_a = 10LL);
  LINE(17,f_fun(v_a));
  LINE(18,x_var_export(v_a));
  (v_b = 20LL);
  LINE(21,f_fun(ref(v_b)));
  LINE(22,x_var_export(v_b));
  (v_c = 30LL);
  LINE(25,f_fun_r(ref(v_c)));
  LINE(26,x_var_export(v_c));
  (v_d = 40LL);
  LINE(29,f_fun_r(ref(v_d)));
  LINE(30,x_var_export(v_d));
  echo("\n");
  (v_e = 50LL);
  (v_f = v_e);
  LINE(40,f_fun(v_f));
  LINE(41,x_var_export(v_e));
  LINE(42,x_var_export(v_f));
  (v_f = "y");
  LINE(44,x_var_export(v_e));
  LINE(45,x_var_export(v_f));
  (v_g = 60LL);
  (v_h = v_g);
  LINE(49,f_fun(ref(v_h)));
  LINE(50,x_var_export(v_g));
  LINE(51,x_var_export(v_h));
  (v_h = "y");
  LINE(53,x_var_export(v_g));
  LINE(54,x_var_export(v_h));
  (v_i = 70LL);
  (v_j = v_i);
  LINE(58,f_fun_r(ref(v_j)));
  LINE(59,x_var_export(v_i));
  LINE(60,x_var_export(v_j));
  (v_j = "y");
  LINE(62,x_var_export(v_i));
  LINE(63,x_var_export(v_j));
  (v_k = 80LL);
  (v_l = v_k);
  LINE(67,f_fun_r(ref(v_l)));
  LINE(68,x_var_export(v_k));
  LINE(69,x_var_export(v_l));
  (v_l = "y");
  LINE(71,x_var_export(v_k));
  LINE(72,x_var_export(v_l));
  echo("\n");
  (v_m = 90LL);
  (v_n = ref(v_m));
  LINE(83,f_fun(v_n));
  LINE(84,x_var_export(v_m));
  LINE(85,x_var_export(v_n));
  (v_m = "y");
  LINE(87,x_var_export(v_m));
  LINE(88,x_var_export(v_n));
  (v_o = 100LL);
  (v_p = ref(v_o));
  LINE(92,f_fun(ref(v_p)));
  LINE(93,x_var_export(v_o));
  LINE(94,x_var_export(v_p));
  (v_p = "y");
  LINE(96,x_var_export(v_o));
  LINE(97,x_var_export(v_p));
  (v_q = 110LL);
  (v_r = ref(v_q));
  LINE(101,f_fun_r(ref(v_r)));
  LINE(102,x_var_export(v_q));
  LINE(103,x_var_export(v_r));
  (v_r = "y");
  LINE(105,x_var_export(v_q));
  LINE(106,x_var_export(v_r));
  (v_s = 120LL);
  (v_t = ref(v_s));
  LINE(110,f_fun_r(ref(v_t)));
  LINE(111,x_var_export(v_s));
  LINE(112,x_var_export(v_t));
  (v_t = "y");
  LINE(114,x_var_export(v_s));
  LINE(115,x_var_export(v_t));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
