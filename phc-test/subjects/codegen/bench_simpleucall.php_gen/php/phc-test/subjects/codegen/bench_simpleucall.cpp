
#include <php/phc-test/subjects/codegen/bench_simpleucall.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_simpleucall.php line 6 */
void f_simpleucall() {
  FUNCTION_INJECTION(simpleucall);
  int64 v_i = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      LINE(8,f_hallo("hallo"));
    }
  }
} /* function */
/* SRC: phc-test/subjects/codegen/bench_simpleucall.php line 3 */
void f_hallo(CVarRef v_a) {
  FUNCTION_INJECTION(hallo);
} /* function */
Variant i_hallo(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3579D7AB94450CD3LL, hallo) {
    return (f_hallo(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$codegen$bench_simpleucall_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_simpleucall.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_simpleucall_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(11,f_simpleucall());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
