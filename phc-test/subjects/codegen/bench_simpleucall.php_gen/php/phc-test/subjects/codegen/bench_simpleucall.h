
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_simpleucall_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_simpleucall_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_simpleucall.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_simpleucall();
Variant pm_php$phc_test$subjects$codegen$bench_simpleucall_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_hallo(CVarRef v_a);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_simpleucall_h__
