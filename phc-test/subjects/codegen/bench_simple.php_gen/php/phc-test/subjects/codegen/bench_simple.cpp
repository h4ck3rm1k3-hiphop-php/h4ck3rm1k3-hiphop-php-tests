
#include <php/phc-test/subjects/codegen/bench_simple.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_simple.php line 3 */
void f_simple() {
  FUNCTION_INJECTION(simple);
  int64 v_a = 0;
  int64 v_i = 0;
  int64 v_thisisanotherlongname = 0;
  int64 v_thisisalongname = 0;

  (v_a = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      v_a++;
    }
  }
  (v_thisisanotherlongname = 0LL);
  {
    LOOP_COUNTER(2);
    for ((v_thisisalongname = 0LL); less(v_thisisalongname, 10LL); v_thisisalongname++) {
      LOOP_COUNTER_CHECK(2);
      v_thisisanotherlongname++;
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_simple_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_simple.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_simple_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(13,f_simple());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
