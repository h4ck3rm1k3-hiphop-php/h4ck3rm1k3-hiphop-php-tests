
#include <php/phc-test/subjects/codegen/bench_strcat.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_strcat.php line 3 */
void f_strcat(int64 v_n) {
  FUNCTION_INJECTION(strcat);
  String v_str;
  int v_len = 0;

  (v_str = "");
  LOOP_COUNTER(1);
  {
    while (more(v_n--, 0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_str, "hello\n");
      }
    }
  }
  (v_len = LINE(8,x_strlen(v_str)));
  print(toString(v_len) + toString("\n"));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_strcat_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_strcat.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_strcat_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(12,f_strcat(20LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
