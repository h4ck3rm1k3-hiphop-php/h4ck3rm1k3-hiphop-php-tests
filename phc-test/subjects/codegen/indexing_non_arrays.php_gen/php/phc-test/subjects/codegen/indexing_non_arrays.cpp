
#include <php/phc-test/subjects/codegen/indexing_non_arrays.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/indexing_non_arrays.php line 54 */
void f_push(CVarRef v_init, Variant v_insert) {
  FUNCTION_INJECTION(push);
  Variant v_x;

  if (LINE(56,x_is_string(v_init))) return;
  echo("--------------------\n");
  echo("checking push\n");
  (v_x = v_init);
  v_x.append((v_insert));
  LINE(64,x_var_dump(1, v_x.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  LINE(65,x_var_dump(1, v_x.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  LINE(66,x_var_dump(1, v_x.rvalAt(4LL, 0x6F2A25235E544A31LL)));
  LINE(67,x_var_dump(1, v_x.rvalAt(17LL, 0x18731B1430E635CCLL)));
  LINE(68,x_var_dump(1, v_x));
  echo("--------------------\n");
  echo("checking (ref) push\n");
  (v_x = v_init);
  v_x.append((ref(v_insert)));
  LINE(74,x_var_dump(1, v_x.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  LINE(75,x_var_dump(1, v_x.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  LINE(76,x_var_dump(1, v_x.rvalAt(4LL, 0x6F2A25235E544A31LL)));
  LINE(77,x_var_dump(1, v_x.rvalAt(170LL, 0x0633EBD88C67DB4DLL)));
  LINE(78,x_var_dump(1, v_x));
} /* function */
/* SRC: phc-test/subjects/codegen/indexing_non_arrays.php line 5 */
void f_write(CVarRef v_init, Variant v_insert) {
  FUNCTION_INJECTION(write);
  Variant v_x;

  echo("--------------------\n");
  echo("checking early write\n");
  (v_x = v_init);
  v_x.set(3LL, (v_insert), 0x135FDDF6A6BFBBDDLL);
  LINE(11,x_var_dump(1, v_x.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  LINE(12,x_var_dump(1, v_x.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  LINE(13,x_var_dump(1, v_x.rvalAt(4LL, 0x6F2A25235E544A31LL)));
  LINE(14,x_var_dump(1, v_x.rvalAt(17LL, 0x18731B1430E635CCLL)));
  LINE(15,x_var_dump(1, v_x));
  echo("--------------------\n");
  echo("checking late write\n");
  (v_x = v_init);
  v_x.set(17LL, (v_insert), 0x18731B1430E635CCLL);
  LINE(21,x_var_dump(1, v_x.rvalAt(17LL, 0x18731B1430E635CCLL)));
  LINE(22,x_var_dump(1, v_x));
  echo("--------------------\n");
  echo("checking very early write\n");
  (v_x = v_init);
  v_x.set(-3LL, (v_insert), 0x58EBF572315B6437LL);
  LINE(28,x_var_dump(1, v_x.rvalAt(-3LL, 0x58EBF572315B6437LL)));
  LINE(29,x_var_dump(1, v_x.rvalAt(-1LL, 0x1F89206E3F8EC794LL)));
  LINE(30,x_var_dump(1, v_x));
  if (LINE(33,x_is_string(v_init))) return;
  echo("--------------------\n");
  echo("checking early (ref) write\n");
  (v_x = v_init);
  v_x.set(3LL, (ref(v_insert)), 0x135FDDF6A6BFBBDDLL);
  LINE(40,x_var_dump(1, v_x.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  LINE(41,x_var_dump(1, v_x.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  LINE(42,x_var_dump(1, v_x.rvalAt(4LL, 0x6F2A25235E544A31LL)));
  LINE(43,x_var_dump(1, v_x.rvalAt(170LL, 0x0633EBD88C67DB4DLL)));
  LINE(44,x_var_dump(1, v_x));
  echo("--------------------\n");
  echo("checking late (ref) write\n");
  (v_x = v_init);
  v_x.set(170LL, (ref(v_insert)), 0x0633EBD88C67DB4DLL);
  LINE(50,x_var_dump(1, v_x.rvalAt(170LL, 0x0633EBD88C67DB4DLL)));
  LINE(51,x_var_dump(1, v_x));
} /* function */
Variant i_write(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x548D0802D1DB44ADLL, write) {
    return (f_write(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$codegen$indexing_non_arrays_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/indexing_non_arrays.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$indexing_non_arrays_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scalar_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scalar_array") : g->GV(scalar_array);
  Variant &v_init __attribute__((__unused__)) = (variables != gVariables) ? variables->get("init") : g->GV(init);
  Variant &v_insert __attribute__((__unused__)) = (variables != gVariables) ? variables->get("insert") : g->GV(insert);

  LINE(3,require("parsing/scalar_array.php", false, variables, "phc-test/subjects/codegen/"));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_scalar_array.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_init = iter3->second();
      {
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_scalar_array.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_insert = iter6->second();
            {
              echo("--------------------\n");
              echo("Init: ");
              LINE(87,x_var_dump(1, v_init));
              echo("Insert: ");
              LINE(89,x_var_dump(1, v_insert));
              LINE(91,f_write(v_init, v_insert));
              LINE(92,f_push(v_init, v_insert));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
