
#ifndef __GENERATED_php_phc_test_subjects_codegen_switch_side_effects_h__
#define __GENERATED_php_phc_test_subjects_codegen_switch_side_effects_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/switch_side_effects.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_f();
Variant pm_php$phc_test$subjects$codegen$switch_side_effects_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_switch_side_effects_h__
