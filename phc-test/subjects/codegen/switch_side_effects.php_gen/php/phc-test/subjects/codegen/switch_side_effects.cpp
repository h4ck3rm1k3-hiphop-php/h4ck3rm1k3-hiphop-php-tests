
#include <php/phc-test/subjects/codegen/switch_side_effects.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/switch_side_effects.php line 3 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("1\n");
  return 7LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$switch_side_effects_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_side_effects.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_side_effects_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  {
    switch (LINE(5,f_f())) {
    case 1LL:
      {
        echo("one\n");
        break;
      }
    case 2LL:
      {
        echo("two\n");
        break;
      }
    case 3LL:
      {
        echo("three\n");
        break;
      }
    case 4LL:
      {
        echo("four\n");
        break;
      }
    case 5LL:
      {
        echo("five\n");
      }
    case 6LL:
      {
        echo("six\n");
      }
    case 7LL:
      {
        echo("seven\n");
      }
    default:
      {
        echo("default\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
