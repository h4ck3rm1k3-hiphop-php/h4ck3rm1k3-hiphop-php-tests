
#include <php/phc-test/subjects/codegen/write_var_var.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$write_var_var_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/write_var_var.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$write_var_var_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_x = "y");
  (variables->get(toString(v_x)) = 5LL);
  LINE(4,x_var_dump(1, v_y));
  (v_a = ScalarArrays::sa_[0]);
  (v_b = "a");
  variables->get(toString(v_b)).set(1LL, (10LL), 0x5BCA7C69B794F8CELL);
  LINE(9,x_var_dump(1, v_a));
  (v_d = 1.5);
  (v_e = "f");
  (variables->get(toString(v_e)) = ref(v_d));
  (v_f = toDouble(v_f) + 1.0);
  LINE(15,x_var_dump(1, v_d));
  (v_g = ScalarArrays::sa_[1]);
  (v_h = "g");
  lval(lval(variables->get(toString(v_h)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)).set(3LL, (7LL), 0x135FDDF6A6BFBBDDLL);
  LINE(20,x_var_dump(1, v_g));
  (v_i = ScalarArrays::sa_[2]);
  lval(lval(variables->get(toString(v_h)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)).set(3LL, (ref(lval(v_i.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), 0x135FDDF6A6BFBBDDLL);
  LINE(24,x_var_dump(1, v_g));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
