
#include <php/phc-test/subjects/codegen/invalid_foreach_array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$invalid_foreach_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/invalid_foreach_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$invalid_foreach_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_short_scalar_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("short_scalar_array") : g->GV(short_scalar_array);
  Variant &v_scalar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scalar") : g->GV(scalar);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);

  LINE(3,include("test/subjects/parsing/scalar_array.php", false, variables, "phc-test/subjects/codegen/"));
  (v_x = ((Object)(LINE(5,p_stdclass(p_stdclass(NEWOBJ(c_stdclass)())->create())))));
  (v_x.o_lval("f", 0x6BB4A0689FBAD42ELL) = 5LL);
  v_short_scalar_array.append((v_x));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_short_scalar_array.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_scalar = iter3->second();
      {
        LINE(11,x_var_dump(1, v_scalar));
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_scalar.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_z = iter6->second();
            v_y = iter6->first();
            {
              echo(LINE(14,concat4(toString(v_y), " => ", toString(v_z), "\n")));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
