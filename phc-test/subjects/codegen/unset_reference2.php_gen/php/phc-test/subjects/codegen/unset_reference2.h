
#ifndef __GENERATED_php_phc_test_subjects_codegen_unset_reference2_h__
#define __GENERATED_php_phc_test_subjects_codegen_unset_reference2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/unset_reference2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_by_ref(Variant v_param);
Variant pm_php$phc_test$subjects$codegen$unset_reference2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_unset_reference2_h__
