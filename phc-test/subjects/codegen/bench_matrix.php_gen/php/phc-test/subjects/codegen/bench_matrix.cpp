
#include <php/phc-test/subjects/codegen/bench_matrix.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_matrix.php line 14 */
Variant f_mmult(int64 v_rows, int64 v_cols, CVarRef v_m1, CVarRef v_m2) {
  FUNCTION_INJECTION(mmult);
  Variant v_m3;
  int64 v_i = 0;
  int64 v_j = 0;
  Numeric v_x = 0;
  int64 v_k = 0;

  (v_m3 = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_x = 0LL);
              {
                LOOP_COUNTER(3);
                for ((v_k = 0LL); less(v_k, v_cols); v_k++) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    v_x += v_m1.rvalAt(v_i).rvalAt(v_k) * v_m2.rvalAt(v_k).rvalAt(v_j);
                  }
                }
              }
              lval(v_m3.lvalAt(v_i)).set(v_j, (v_x));
            }
          }
        }
      }
    }
  }
  return (v_m3);
} /* function */
/* SRC: phc-test/subjects/codegen/bench_matrix.php line 3 */
Variant f_mkmatrix(int64 v_rows, int64 v_cols) {
  FUNCTION_INJECTION(mkmatrix);
  int64 v_count = 0;
  Variant v_mx;
  int64 v_i = 0;
  int64 v_j = 0;

  (v_count = 1LL);
  (v_mx = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        {
          LOOP_COUNTER(5);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(5);
            {
              lval(v_mx.lvalAt(v_i)).set(v_j, (v_count++));
            }
          }
        }
      }
    }
  }
  return (v_mx);
} /* function */
/* SRC: phc-test/subjects/codegen/bench_matrix.php line 30 */
void f_matrix(int64 v_n) {
  FUNCTION_INJECTION(matrix);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int64 v_SIZE = 0;
  Variant v_m1;
  Variant v_m2;
  Variant v_mm;

  (v_SIZE = 10LL);
  (v_m1 = LINE(32,f_mkmatrix(v_SIZE, v_SIZE)));
  (v_m2 = LINE(33,f_mkmatrix(v_SIZE, v_SIZE)));
  LOOP_COUNTER(6);
  {
    while (toBoolean(v_n--)) {
      LOOP_COUNTER_CHECK(6);
      {
        (v_mm = LINE(35,f_mmult(v_SIZE, v_SIZE, v_m1, v_m2)));
      }
    }
  }
  print(LINE(37,(assignCallTemp(eo_0, toString(v_mm.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))),assignCallTemp(eo_2, concat6(toString(v_mm.rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), " ", toString(v_mm.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL).rvalAt(2LL, 0x486AFCC090D5F98CLL)), " ", toString(v_mm.rvalAt(4LL, 0x6F2A25235E544A31LL).rvalAt(4LL, 0x6F2A25235E544A31LL)), "\n")),concat3(eo_0, " ", eo_2))));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_matrix_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_matrix.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_matrix_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(40,f_matrix(2LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
