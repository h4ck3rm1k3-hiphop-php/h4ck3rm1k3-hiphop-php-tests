
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_matrix_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_matrix_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_matrix.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_mmult(int64 v_rows, int64 v_cols, CVarRef v_m1, CVarRef v_m2);
Variant f_mkmatrix(int64 v_rows, int64 v_cols);
void f_matrix(int64 v_n);
Variant pm_php$phc_test$subjects$codegen$bench_matrix_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_matrix_h__
