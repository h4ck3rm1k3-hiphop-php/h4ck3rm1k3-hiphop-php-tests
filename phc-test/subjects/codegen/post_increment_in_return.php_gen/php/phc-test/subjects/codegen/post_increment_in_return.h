
#ifndef __GENERATED_php_phc_test_subjects_codegen_post_increment_in_return_h__
#define __GENERATED_php_phc_test_subjects_codegen_post_increment_in_return_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/post_increment_in_return.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$post_increment_in_return_php(bool incOnce = false, LVariableTable* variables = NULL);
Numeric f_f();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_post_increment_in_return_h__
