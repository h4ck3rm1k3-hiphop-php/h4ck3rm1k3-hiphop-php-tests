
#include <php/phc-test/subjects/codegen/post_increment_in_return.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/post_increment_in_return.php line 4 */
Numeric f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_i __attribute__((__unused__)) = g->GV(i);
  return gv_i++ * 2LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$post_increment_in_return_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/post_increment_in_return.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$post_increment_in_return_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_i = 0LL);
  LINE(10,x_var_dump(1, f_f() + v_i));
  LINE(11,x_var_dump(1, f_f() + v_i));
  LINE(12,x_var_dump(1, f_f() + v_i));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
