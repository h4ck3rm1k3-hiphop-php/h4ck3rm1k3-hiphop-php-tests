
#include <php/phc-test/subjects/codegen/isset_local.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/isset_local.php line 4 */
void f_f() {
  FUNCTION_INJECTION(f);
  Variant v_x;

  if (isset(v_x)) echo("x is set (1)\n");
  (v_x = 1LL);
  if (isset(v_x)) echo("x is set (2)\n");
} /* function */
Variant pm_php$phc_test$subjects$codegen$isset_local_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_local.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_local_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(15,f_f());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
