
#include <php/phc-test/subjects/codegen/unset_array_elem.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$unset_array_elem_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/unset_array_elem.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$unset_array_elem_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr1") : g->GV(arr1);
  Variant &v_arr2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr2") : g->GV(arr2);

  (v_arr1 = ScalarArrays::sa_[0]);
  v_arr1.weakRemove(1LL, 0x5BCA7C69B794F8CELL);
  LINE(4,x_var_export(v_arr1));
  (v_arr2 = ScalarArrays::sa_[1]);
  v_arr2.weakRemove("b", 0x08FBB133F8576BD5LL);
  LINE(8,x_var_export(v_arr2));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
