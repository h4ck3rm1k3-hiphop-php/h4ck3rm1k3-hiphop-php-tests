
#ifndef __GENERATED_php_phc_test_subjects_codegen_push_reference_through_array_by_copy_h__
#define __GENERATED_php_phc_test_subjects_codegen_push_reference_through_array_by_copy_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/push_reference_through_array_by_copy.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$push_reference_through_array_by_copy_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_push_reference_through_array_by_copy_h__
