
#include <php/phc-test/subjects/codegen/push_reference_through_array_by_copy.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$push_reference_through_array_by_copy_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/push_reference_through_array_by_copy.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$push_reference_through_array_by_copy_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_y1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y1") : g->GV(y1);
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_x1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x1") : g->GV(x1);
  Variant &v_b1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b1") : g->GV(b1);

  (v_y = 7LL);
  (v_a = ref(v_y));
  v_x.append((v_a));
  (v_b = ref(lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  (v_b = 8LL);
  LINE(9,x_var_dump(4, v_y, Array(ArrayInit(3).set(0, v_a).set(1, v_b).set(2, v_x).create())));
  (v_y1 = 7LL);
  (v_a1 = ref(v_y1));
  v_x1.append((ref(v_a1)));
  (v_b1 = ref(lval(v_x1.lvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  (v_b1 = 8LL);
  LINE(18,x_var_dump(4, v_y1, Array(ArrayInit(3).set(0, v_a1).set(1, v_b1).set(2, v_x1).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
