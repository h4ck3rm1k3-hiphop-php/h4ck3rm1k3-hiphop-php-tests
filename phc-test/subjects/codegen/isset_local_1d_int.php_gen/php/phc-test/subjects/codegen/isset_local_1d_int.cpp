
#include <php/phc-test/subjects/codegen/isset_local_1d_int.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/isset_local_1d_int.php line 4 */
void f_f() {
  FUNCTION_INJECTION(f);
  Variant v_x;

  if (isset(v_x, 1LL, 0x5BCA7C69B794F8CELL)) echo("x[1] is set (1)\n");
  v_x.set(1LL, (1LL), 0x5BCA7C69B794F8CELL);
  if (isset(v_x, 1LL, 0x5BCA7C69B794F8CELL)) echo("x[1] is set (2)\n");
} /* function */
Variant pm_php$phc_test$subjects$codegen$isset_local_1d_int_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/isset_local_1d_int.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$isset_local_1d_int_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(15,f_f());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
