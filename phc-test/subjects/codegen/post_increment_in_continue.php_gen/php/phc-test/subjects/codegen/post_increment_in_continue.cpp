
#include <php/phc-test/subjects/codegen/post_increment_in_continue.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$post_increment_in_continue_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/post_increment_in_continue.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$post_increment_in_continue_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_i = 0LL);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for (; ; ) {
            LOOP_COUNTER_CHECK(2);
            {
              echo("a");
              {
                LOOP_COUNTER(3);
                for (; ; ) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    echo("b");
                    {
                      LOOP_COUNTER(4);
                      for (; ; ) {
                        LOOP_COUNTER_CHECK(4);
                        {
                          echo("c");
                          {
                            LOOP_COUNTER(5);
                            for (; ; ) {
                              LOOP_COUNTER_CHECK(5);
                              {
                                echo("d");
                                switch (toInt64(v_i++)) {
                                case 5: goto continue1;
                                case 4: goto continue2;
                                case 3: goto continue3;
                                case 2: goto continue4;
                                case 1: goto continue5;
                                default:
                                if ((toInt64(v_i++))<2) {
                                goto continue5;
                                } else {
                                throw_fatal("bad continue");
                                }
                                }
                                echo("e");
                              }
                              continue5:;
                            }
                          }
                          echo("f");
                        }
                        continue4:;
                      }
                    }
                    echo("g");
                  }
                  continue3:;
                }
              }
              echo("h");
            }
            continue2:;
          }
        }
      }
      continue1:;
    } while (false);
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
