
#ifndef __GENERATED_php_phc_test_subjects_codegen_undefined_consts_h__
#define __GENERATED_php_phc_test_subjects_codegen_undefined_consts_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/undefined_consts.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$undefined_consts_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_x(int64 v_x = 7LL /* foo::BAR */, CVarRef v_y = throw_fatal("unknown class constant undef::CONST_"));
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_undefined_consts_h__
