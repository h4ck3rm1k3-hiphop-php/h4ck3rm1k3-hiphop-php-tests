
#include <php/phc-test/subjects/codegen/add.h>
#include <php/phc-test/subjects/codegen/include_once_and_include.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$include_once_and_include_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/include_once_and_include.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$include_once_and_include_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,pm_php$phc_test$subjects$codegen$add_php(true, variables));
  LINE(4,pm_php$phc_test$subjects$codegen$add_php(false, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
