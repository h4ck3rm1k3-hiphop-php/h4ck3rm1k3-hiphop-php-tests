
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$codegen$include_once_and_include_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$codegen$add_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_INCLUDE(0x5554A19B6377009DLL, "phc-test/subjects/codegen/include_once_and_include.php", php$phc_test$subjects$codegen$include_once_and_include_php);
      break;
    case 2:
      HASH_INCLUDE(0x514A7CD92C604B2ELL, "phc-test/subjects/codegen/add.php", php$phc_test$subjects$codegen$add_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
