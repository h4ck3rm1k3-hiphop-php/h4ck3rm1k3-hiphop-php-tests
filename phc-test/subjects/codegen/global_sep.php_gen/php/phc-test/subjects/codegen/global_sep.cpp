
#include <php/phc-test/subjects/codegen/global_sep.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/global_sep.php line 7 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_b __attribute__((__unused__)) = g->GV(b);
  (gv_b = gv_b + 1LL);
} /* function */
Variant pm_php$phc_test$subjects$codegen$global_sep_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/global_sep.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$global_sep_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = 5LL);
  (v_b = v_a);
  LINE(13,f_f());
  LINE(15,x_var_export(v_a));
  LINE(16,x_var_export(v_b));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
