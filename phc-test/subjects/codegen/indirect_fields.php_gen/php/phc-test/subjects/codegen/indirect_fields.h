
#ifndef __GENERATED_php_phc_test_subjects_codegen_indirect_fields_h__
#define __GENERATED_php_phc_test_subjects_codegen_indirect_fields_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/indirect_fields.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$indirect_fields_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_indirect_fields_h__
