
#include <php/phc-test/subjects/codegen/switch_all_before_default.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/switch_all_before_default.php line 4 */
int64 f_f() {
  FUNCTION_INJECTION(f);
  echo("f called\n");
  return 8LL;
} /* function */
Variant pm_php$phc_test$subjects$codegen$switch_all_before_default_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_all_before_default.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_all_before_default_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 7LL);
  {
    Variant tmp2 = (v_x);
    int tmp3 = -1;
    if (equal(tmp2, (6LL))) {
      tmp3 = 0;
    } else if (equal(tmp2, (LINE(17,f_f())))) {
      tmp3 = 2;
    } else if (true) {
      tmp3 = 1;
    }
    switch (tmp3) {
    case 0:
      {
        echo("6\n");
      }
    case 1:
      {
        echo("default\n");
      }
    case 2:
      {
        echo("f\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
