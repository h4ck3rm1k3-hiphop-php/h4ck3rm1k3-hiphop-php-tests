
#ifndef __GENERATED_php_phc_test_subjects_codegen_switch_all_before_default_h__
#define __GENERATED_php_phc_test_subjects_codegen_switch_all_before_default_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/switch_all_before_default.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$switch_all_before_default_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_f();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_switch_all_before_default_h__
