
#include <php/phc-test/subjects/codegen/functioncalls_evald_invocations.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/functioncalls_evald_invocations.php line 7 */
void f_fun_r(Variant v_x) {
  FUNCTION_INJECTION(fun_r);
  (v_x = "x");
} /* function */
/* SRC: phc-test/subjects/codegen/functioncalls_evald_invocations.php line 8 */
Variant f_fun_rr(Variant v_x) {
  FUNCTION_INJECTION(fun_rr);
  return ref(v_x);
} /* function */
/* SRC: phc-test/subjects/codegen/functioncalls_evald_invocations.php line 6 */
void f_fun(String v_x) {
  FUNCTION_INJECTION(fun);
  (v_x = "x");
} /* function */
Variant pm_php$phc_test$subjects$codegen$functioncalls_evald_invocations_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/functioncalls_evald_invocations.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$functioncalls_evald_invocations_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  f_eval("$a = 10;");
  f_eval("fun($a);");
  f_eval("var_export($a);");
  f_eval("$b = 20;");
  f_eval("fun(&$b);");
  f_eval("var_export($b);");
  f_eval("$c = 30;");
  f_eval("fun_r($c);");
  f_eval("var_export($c);");
  f_eval("$d = 40;");
  f_eval("fun_r(&$d);");
  f_eval("var_export($d);");
  f_eval("$d1 = 41;");
  f_eval("fun_rr(&$d1);");
  f_eval("var_export($d1);");
  f_eval("$d2 = 42;");
  f_eval("fun_rr(&$d2);");
  f_eval("var_export($d2);");
  f_eval("echo \"\\n\";");
  f_eval("$e = 50;");
  f_eval("$f = $e;");
  f_eval("fun($f);");
  f_eval("var_export($e);");
  f_eval("var_export($f);");
  f_eval("$f = 'y';");
  f_eval("var_export($e);");
  f_eval("var_export($f);");
  f_eval("$g = 60;");
  f_eval("$h = $g;");
  f_eval("fun(&$h);");
  f_eval("var_export($g);");
  f_eval("var_export($h);");
  f_eval("$h = 'y';");
  f_eval("var_export($g);");
  f_eval("var_export($h);");
  f_eval("$i = 70;");
  f_eval("$j = $i;");
  f_eval("fun_r($j);");
  f_eval("var_export($i);");
  f_eval("var_export($j);");
  f_eval("$j = 'y';");
  f_eval("var_export($i);");
  f_eval("var_export($j);");
  f_eval("$k = 80;");
  f_eval("$l = $k;");
  f_eval("fun_r(&$l);");
  f_eval("var_export($k);");
  f_eval("var_export($l);");
  f_eval("$l = 'y';");
  f_eval("var_export($k);");
  f_eval("var_export($l);");
  f_eval("$i1 = 71;");
  f_eval("$j1 = $i1;");
  f_eval("fun_rr($j1);");
  f_eval("var_export($i1);");
  f_eval("var_export($j1);");
  f_eval("$j1 = 'y';");
  f_eval("var_export($i1);");
  f_eval("var_export($j1);");
  f_eval("$k1 = 81;");
  f_eval("$l1 = $k1;");
  f_eval("fun_rr(&$l1);");
  f_eval("var_export($k1);");
  f_eval("var_export($l1);");
  f_eval("$l1 = 'y';");
  f_eval("var_export($k1);");
  f_eval("var_export($l1);");
  f_eval("echo \"\\n\";");
  f_eval("$m = 90;");
  f_eval("$n =& $m;");
  f_eval("fun($n);");
  f_eval("var_export($m);");
  f_eval("var_export($n);");
  f_eval("$m = 'y';");
  f_eval("var_export($m);");
  f_eval("var_export($n);");
  f_eval("$o = 100;");
  f_eval("$p =& $o;");
  f_eval("fun(&$p);");
  f_eval("var_export($o);");
  f_eval("var_export($p);");
  f_eval("$p = 'y';");
  f_eval("var_export($o);");
  f_eval("var_export($p);");
  f_eval("$q = 110;");
  f_eval("$r =& $q;");
  f_eval("fun_r($r);");
  f_eval("var_export($q);");
  f_eval("var_export($r);");
  f_eval("$r = 'y';");
  f_eval("var_export($q);");
  f_eval("var_export($r);");
  f_eval("$s = 120;");
  f_eval("$t =& $s;");
  f_eval("fun_r(&$t);");
  f_eval("var_export($s);");
  f_eval("var_export($t);");
  f_eval("$t = 'y';");
  f_eval("var_export($s);");
  f_eval("var_export($t);");
  f_eval("$q1 = 111;");
  f_eval("$r1 =& $q1;");
  f_eval("fun_rr($r1);");
  f_eval("var_export($q1);");
  f_eval("var_export($r1);");
  f_eval("$r1 = 'y';");
  f_eval("var_export($q1);");
  f_eval("var_export($r1);");
  f_eval("$s1 = 121;");
  f_eval("$t1 =& $s1;");
  f_eval("fun_rr(&$t1);");
  f_eval("var_export($s1);");
  f_eval("var_export($t1);");
  f_eval("$t1 = 'y';");
  f_eval("var_export($s1);");
  f_eval("var_export($t1);");
  f_eval("echo \"\\n\";");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
