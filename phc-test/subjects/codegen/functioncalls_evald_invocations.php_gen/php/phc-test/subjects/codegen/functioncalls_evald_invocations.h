
#ifndef __GENERATED_php_phc_test_subjects_codegen_functioncalls_evald_invocations_h__
#define __GENERATED_php_phc_test_subjects_codegen_functioncalls_evald_invocations_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/functioncalls_evald_invocations.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$functioncalls_evald_invocations_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_fun_r(Variant v_x);
Variant f_fun_rr(Variant v_x);
void f_fun(String v_x);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_functioncalls_evald_invocations_h__
