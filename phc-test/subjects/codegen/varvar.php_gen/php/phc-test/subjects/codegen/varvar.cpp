
#include <php/phc-test/subjects/codegen/varvar.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$varvar_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/varvar.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$varvar_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  (v_a = 5LL);
  v_arr.set(1LL, ("a"), 0x5BCA7C69B794F8CELL);
  LINE(5,x_var_dump(1, variables->get(toString(v_arr.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
  (variables->get(toString(v_arr.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) = 6LL);
  LINE(8,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
