
#ifndef __GENERATED_php_phc_test_subjects_codegen_indirect_arrays_h__
#define __GENERATED_php_phc_test_subjects_codegen_indirect_arrays_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/indirect_arrays.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$indirect_arrays_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_indirect_arrays_h__
