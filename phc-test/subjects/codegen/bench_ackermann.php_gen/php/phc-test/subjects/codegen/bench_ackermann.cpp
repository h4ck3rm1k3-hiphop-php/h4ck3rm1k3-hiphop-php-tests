
#include <php/phc-test/subjects/codegen/bench_ackermann.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_ackermann.php line 9 */
void f_ackermann(int64 v_n) {
  FUNCTION_INJECTION(ackermann);
  Numeric v_r = 0;

  (v_r = LINE(10,f_ack(3LL, v_n)));
  print(LINE(11,concat5("Ack(3,", toString(v_n), "): ", toString(v_r), "\n")));
} /* function */
/* SRC: phc-test/subjects/codegen/bench_ackermann.php line 3 */
Numeric f_ack(Numeric v_m, Numeric v_n) {
  FUNCTION_INJECTION(Ack);
  Variant eo_0;
  Variant eo_1;
  if (equal(v_m, 0LL)) return v_n + 1LL;
  if (equal(v_n, 0LL)) return LINE(5,f_ack(v_m - 1LL, 1LL));
  return LINE(6,(assignCallTemp(eo_0, v_m - 1LL),assignCallTemp(eo_1, f_ack(v_m, (v_n - 1LL))),f_ack(eo_0, eo_1)));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_ackermann_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_ackermann.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_ackermann_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(14,f_ackermann(3LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
