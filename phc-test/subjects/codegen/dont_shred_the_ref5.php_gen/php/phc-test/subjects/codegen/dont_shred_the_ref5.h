
#ifndef __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref5_h__
#define __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/dont_shred_the_ref5.fw.h>

// Declarations
#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$dont_shred_the_ref5_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f(CVarRef v_x);
void f_g(Variant v_x);
Object co_c(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_dont_shred_the_ref5_h__
