
#include <php/phc-test/subjects/codegen/dont_shred_the_ref5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/dont_shred_the_ref5.php line 15 */
Variant c_c::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
}
/* SRC: phc-test/subjects/codegen/dont_shred_the_ref5.php line 7 */
void f_f(CVarRef v_x) {
  FUNCTION_INJECTION(f);
} /* function */
/* SRC: phc-test/subjects/codegen/dont_shred_the_ref5.php line 11 */
void f_g(Variant v_x) {
  FUNCTION_INJECTION(g);
} /* function */
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$dont_shred_the_ref5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/dont_shred_the_ref5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$dont_shred_the_ref5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_c = ((Object)(LINE(19,p_c(p_c(NEWOBJ(c_c)())->create())))));
  LINE(20,x_var_dump(1, v_c));
  LINE(22,f_f(v_c.o_get("x", 0x04BFC205E59FA416LL)));
  LINE(23,x_var_dump(1, v_c));
  LINE(25,f_g(ref(lval(v_c.o_lval("y", 0x4F56B733A4DFC78ALL)))));
  LINE(26,x_var_dump(1, v_c));
  LINE(28,f_f(v_c.o_get("a", 0x4292CEE227B9150ALL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(29,x_var_dump(1, v_c));
  LINE(31,f_g(ref(lval(lval(v_c.o_lval("b", 0x08FBB133F8576BD5LL)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
  LINE(32,x_var_dump(1, v_c));
  LINE(34,f_f(v_c.o_get("c", 0x32C769EE5C5509B0LL).rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  LINE(35,x_var_dump(1, v_c));
  LINE(37,f_g(ref(lval(lval(lval(v_c.o_lval("d", 0x7A452383AA9BF7C4LL)).lvalAt(4LL, 0x6F2A25235E544A31LL)).lvalAt(5LL, 0x350AEB726A15D700LL)))));
  LINE(38,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
