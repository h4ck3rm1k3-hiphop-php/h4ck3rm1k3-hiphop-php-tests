
#include <php/phc-test/subjects/codegen/bench_ary2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_ary2.php line 3 */
void f_ary2(int64 v_n) {
  FUNCTION_INJECTION(ary2);
  Numeric v_i = 0;
  Sequence v_X;
  Sequence v_Y;
  Numeric v_last = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); ) {
      LOOP_COUNTER_CHECK(1);
      {
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = v_n - 1LL); not_less(v_i, 0LL); ) {
      LOOP_COUNTER_CHECK(2);
      {
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
      }
    }
  }
  (v_last = v_n - 1LL);
  print(toString(v_Y.rvalAt(v_last)) + toString("\n"));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_ary2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_ary2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_ary2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(34,f_ary2(5LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
