
#ifndef __GENERATED_php_phc_test_subjects_codegen_unset_reference_h__
#define __GENERATED_php_phc_test_subjects_codegen_unset_reference_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/unset_reference.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_by_ref(Variant v_param);
Variant pm_php$phc_test$subjects$codegen$unset_reference_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_unset_reference_h__
