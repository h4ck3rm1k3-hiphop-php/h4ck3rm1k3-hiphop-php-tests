
#include <php/phc-test/subjects/codegen/default_values.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/default_values.php line 2 */
void f_f(int64 v_a, int64 v_b //  = 2LL
, int64 v_c //  = 3LL
) {
  FUNCTION_INJECTION(f);
  LINE(4,x_var_export(v_a));
  LINE(5,x_var_export(v_b));
  LINE(6,x_var_export(v_c));
} /* function */
/* SRC: phc-test/subjects/codegen/default_values.php line 9 */
void f_g(CVarRef v_d //  = ScalarArrays::sa_[0]
) {
  FUNCTION_INJECTION(g);
  LINE(11,x_var_export(v_d));
} /* function */
/* SRC: phc-test/subjects/codegen/default_values.php line 14 */
void f_h(CVarRef v_e //  = ScalarArrays::sa_[1]
) {
  FUNCTION_INJECTION(h);
  LINE(16,x_var_export(v_e));
} /* function */
Variant pm_php$phc_test$subjects$codegen$default_values_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/default_values.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$default_values_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(19,f_f(0LL));
  LINE(20,f_f(0LL, 0LL));
  LINE(21,f_f(0LL, 0LL, 0LL));
  LINE(23,f_g());
  LINE(24,f_g(0LL));
  LINE(26,f_h());
  LINE(27,f_h(0LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
