
#ifndef __GENERATED_php_phc_test_subjects_codegen_default_values_h__
#define __GENERATED_php_phc_test_subjects_codegen_default_values_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/default_values.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$default_values_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_f(int64 v_a, int64 v_b = 2LL, int64 v_c = 3LL);
void f_g(CVarRef v_d = ScalarArrays::sa_[0]);
void f_h(CVarRef v_e = ScalarArrays::sa_[1]);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_default_values_h__
