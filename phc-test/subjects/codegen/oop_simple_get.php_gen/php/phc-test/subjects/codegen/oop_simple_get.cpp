
#include <php/phc-test/subjects/codegen/oop_simple_get.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$oop_simple_get_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_simple_get.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_simple_get_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  f_eval(toString("\tclass C\n\t{\n\t\tfunction __get($var)\n\t\t{\n\t\t\techo(\"__get($var)\n\");\n\t\t\treturn 1; \n\t\t}\n\t}\n\n\t$c = new C();"));
  LINE(16,x_var_dump(1, v_c.o_get("a", 0x4292CEE227B9150ALL)));
  (v_c.o_lval("a", 0x4292CEE227B9150ALL) = 2LL);
  LINE(18,x_var_dump(1, v_c.o_get("a", 0x4292CEE227B9150ALL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
