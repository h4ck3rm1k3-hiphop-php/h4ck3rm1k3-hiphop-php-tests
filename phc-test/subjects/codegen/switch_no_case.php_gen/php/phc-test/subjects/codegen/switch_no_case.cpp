
#include <php/phc-test/subjects/codegen/switch_no_case.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$switch_no_case_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/switch_no_case.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$switch_no_case_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = 0LL);
  echo("before\n");
  {
    Variant tmp2 = (v_x);
  }
  echo("after\n");
  echo("before\n");
  {
    Variant tmp4 = (v_x);
    int tmp5 = -1;
    if (true) {
      tmp5 = 0;
    }
    switch (tmp5) {
    case 0:
      {
        echo("default\n");
        goto break3;
      }
    }
    break3:;
  }
  echo("after\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
