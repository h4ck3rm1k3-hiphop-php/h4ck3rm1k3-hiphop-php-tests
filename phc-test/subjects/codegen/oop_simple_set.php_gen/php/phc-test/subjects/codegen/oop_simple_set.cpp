
#include <php/phc-test/subjects/codegen/oop_simple_set.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$oop_simple_set_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_simple_set.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_simple_set_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  f_eval(toString("\tclass C\n\t{\n\t\tfunction __set($var, $value)\n\t\t{\n\t\t\techo(\"__set($var, $value)\n\");\n\t\t\t$this->$var = $value;\n\t\t}\n\t}\n\n\t$c = new C();"));
  (v_c.o_lval("a", 0x4292CEE227B9150ALL) = 1LL);
  (v_c.o_lval("a", 0x4292CEE227B9150ALL) = 2LL);
  (v_c.o_lval("b", 0x08FBB133F8576BD5LL) = 3LL);
  LINE(19,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
