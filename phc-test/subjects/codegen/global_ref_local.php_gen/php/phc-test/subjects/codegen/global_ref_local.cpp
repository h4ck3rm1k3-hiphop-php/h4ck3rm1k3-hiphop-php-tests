
#include <php/phc-test/subjects/codegen/global_ref_local.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/global_ref_local.php line 12 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a;
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);

  (v_a = 10LL);
  (gv_x = ref(v_a));
} /* function */
/* SRC: phc-test/subjects/codegen/global_ref_local.php line 19 */
void f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_b;

  (v_b = 20LL);
  (g->GV(y) = ref(v_b));
} /* function */
Variant pm_php$phc_test$subjects$codegen$global_ref_local_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/global_ref_local.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$global_ref_local_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);

  LINE(25,f_f());
  LINE(26,x_var_dump(1, v_x));
  LINE(28,f_g());
  LINE(29,x_var_dump(1, v_y));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
