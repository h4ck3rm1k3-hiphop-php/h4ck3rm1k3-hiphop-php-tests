
#include <php/phc-test/subjects/codegen/foreach_simple.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$foreach_simple_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/foreach_simple.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$foreach_simple_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_val = iter3->second();
      v_key = iter3->first();
      {
        (v_b = v_a);
        LINE(7,x_var_export(v_b));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
