
#include <php/phc-test/subjects/codegen/break_string_int.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$break_string_int_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/break_string_int.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$break_string_int_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);

  echo("before\n");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 3LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(6,x_var_dump(1, toString("i") + toString(v_i)));
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, 3LL); v_j++) {
            LOOP_COUNTER_CHECK(2);
            {
              LINE(9,x_var_dump(1, toString("j") + toString(v_j)));
              {
                LOOP_COUNTER(3);
                for ((v_k = 0LL); less(v_k, 3LL); v_k++) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    LINE(12,x_var_dump(1, toString("k") + toString(v_k)));
                    {
                      LOOP_COUNTER(4);
                      for ((v_m = 0LL); less(v_m, 3LL); v_m++) {
                        LOOP_COUNTER_CHECK(4);
                        {
                          LINE(15,x_var_dump(1, toString("m") + toString(v_m)));
                          switch (toInt64("3")) {
                          case 4: goto break1;
                          case 3: goto break2;
                          case 2: goto break3;
                          case 1: goto break4;
                          default:
                          if ((toInt64("3"))<2) {
                          goto break4;
                          } else {
                          throw_fatal("bad break");
                          }
                          }
                        }
                      }
                      break4:;
                    }
                  }
                }
                break3:;
              }
            }
          }
          break2:;
        }
      }
    }
    break1:;
  }
  echo("after\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
