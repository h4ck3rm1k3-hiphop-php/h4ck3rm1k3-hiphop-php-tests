
#ifndef __GENERATED_php_phc_test_subjects_codegen_dynamic_class_def_and_constants_h__
#define __GENERATED_php_phc_test_subjects_codegen_dynamic_class_def_and_constants_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/dynamic_class_def_and_constants.fw.h>

// Declarations
#include <cls/foo.h>
#include <cls/xoo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_x(int64 v_x = 7LL /* foo::BAR */, CVarRef v_y = throw_fatal("unknown class constant undef::CONST_"));
void f_y(int64 v_y = 8LL /* xoo::XAR */);
Variant pm_php$phc_test$subjects$codegen$dynamic_class_def_and_constants_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);
Object co_xoo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_dynamic_class_def_and_constants_h__
