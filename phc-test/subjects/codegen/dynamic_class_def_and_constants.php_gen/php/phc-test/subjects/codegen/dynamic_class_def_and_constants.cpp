
#include <php/phc-test/subjects/codegen/dynamic_class_def_and_constants.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/dynamic_class_def_and_constants.php line 12 */
const int64 q_foo_BAR = 7LL;
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x5E008C5BA4C3B3FDLL, q_foo_BAR, BAR);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
/* SRC: phc-test/subjects/codegen/dynamic_class_def_and_constants.php line 28 */
const int64 q_xoo_XAR = 8LL;
Variant c_xoo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_xoo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_xoo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_xoo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_xoo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_xoo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_xoo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_xoo::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x5893661452600374LL, q_xoo_XAR, XAR);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(xoo)
ObjectData *c_xoo::cloneImpl() {
  c_xoo *obj = NEW(c_xoo)();
  cloneSet(obj);
  return obj;
}
void c_xoo::cloneSet(c_xoo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_xoo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_xoo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_xoo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_xoo$os_get(const char *s) {
  return c_xoo::os_get(s, -1);
}
Variant &cw_xoo$os_lval(const char *s) {
  return c_xoo::os_lval(s, -1);
}
Variant cw_xoo$os_constant(const char *s) {
  return c_xoo::os_constant(s);
}
Variant cw_xoo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_xoo::os_invoke(c, s, params, -1, fatal);
}
void c_xoo::init() {
}
/* SRC: phc-test/subjects/codegen/dynamic_class_def_and_constants.php line 2 */
void f_x(int64 v_x //  = 7LL /* foo::BAR */
, CVarRef v_y //  = throw_fatal("unknown class constant undef::CONST_")
) {
  FUNCTION_INJECTION(x);
  LINE(4,x_var_dump(1, v_x));
  LINE(5,x_var_dump(1, v_y));
  echo("\n");
} /* function */
/* SRC: phc-test/subjects/codegen/dynamic_class_def_and_constants.php line 35 */
void f_y(int64 v_y //  = 8LL /* xoo::XAR */
) {
  FUNCTION_INJECTION(y);
  LINE(37,x_var_dump(1, v_y));
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_xoo(CArrRef params, bool init /* = true */) {
  return Object(p_xoo(NEW(c_xoo)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$dynamic_class_def_and_constants_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/dynamic_class_def_and_constants.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$dynamic_class_def_and_constants_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(9,f_x());
  LINE(17,f_x());
  LINE(24,f_y());
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
      }
    } while (false);
  }
  LINE(40,f_y());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
