
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/codegen/dynamic_class_def_and_constants.php line 12 */
class c_foo : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, Foo, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
