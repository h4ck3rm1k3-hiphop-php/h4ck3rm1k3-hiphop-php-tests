
#include <php/phc-test/subjects/codegen/post_increment_in_if1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$codegen$post_increment_in_if1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/post_increment_in_if1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$post_increment_in_if1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_i = 0LL);
  if (equal(v_i++, 0LL)) echo(LINE(4,concat3("(a) ", toString(v_i), "\n")));
  else echo(LINE(6,concat3("(b) ", toString(v_i), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
