
#ifndef __GENERATED_php_phc_test_subjects_codegen_run_time_array_key_h__
#define __GENERATED_php_phc_test_subjects_codegen_run_time_array_key_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/run_time_array_key.fw.h>

// Declarations
#include <cls/x.h>
#include <cls/y.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$run_time_array_key_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_f();
Object co_x(CArrRef params, bool init = true);
Object co_y(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_run_time_array_key_h__
