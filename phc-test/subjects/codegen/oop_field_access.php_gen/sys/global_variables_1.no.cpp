
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      HASH_RETURN(0x2EF5CCDEC77FE801LL, g->GV(g6),
                  g6);
      break;
    case 2:
      HASH_RETURN(0x079A01435AA36382LL, g->GV(v1),
                  v1);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x5D598FF371418186LL, g->GV(v3),
                  v3);
      break;
    case 7:
      HASH_RETURN(0x6CBB90BA37C4FB47LL, g->GV(v4),
                  v4);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      HASH_RETURN(0x7441D7CC0C080C90LL, g->GV(g2),
                  g2);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 22:
      HASH_RETURN(0x04BFC205E59FA416LL, g->GV(x),
                  x);
      break;
    case 32:
      HASH_RETURN(0x0EAFB142DAAC7060LL, g->GV(v5),
                  v5);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 39:
      HASH_RETURN(0x0510BDBD02C38BA7LL, g->GV(v2),
                  v2);
      break;
    case 42:
      HASH_RETURN(0x3E2D0448B9E5042ALL, g->GV(g3),
                  g3);
      HASH_RETURN(0x7AEFE69A4C9ADEAALL, g->GV(g4),
                  g4);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 48:
      HASH_RETURN(0x32C769EE5C5509B0LL, g->GV(c),
                  c);
      break;
    case 50:
      HASH_RETURN(0x4D9E12E2960B9832LL, g->GV(g1),
                  g1);
      break;
    case 53:
      HASH_RETURN(0x54C7D122C6D5EBB5LL, g->GV(v6),
                  v6);
      break;
    case 60:
      HASH_RETURN(0x092312F91514E83CLL, g->GV(g5),
                  g5);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
