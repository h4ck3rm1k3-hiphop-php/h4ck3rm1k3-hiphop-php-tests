
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x2EF5CCDEC77FE801LL, g6, 18);
      break;
    case 2:
      HASH_INDEX(0x079A01435AA36382LL, v1, 20);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x5D598FF371418186LL, v3, 22);
      break;
    case 7:
      HASH_INDEX(0x6CBB90BA37C4FB47LL, v4, 23);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x7441D7CC0C080C90LL, g2, 14);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 22:
      HASH_INDEX(0x04BFC205E59FA416LL, x, 19);
      break;
    case 32:
      HASH_INDEX(0x0EAFB142DAAC7060LL, v5, 24);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 39:
      HASH_INDEX(0x0510BDBD02C38BA7LL, v2, 21);
      break;
    case 42:
      HASH_INDEX(0x3E2D0448B9E5042ALL, g3, 15);
      HASH_INDEX(0x7AEFE69A4C9ADEAALL, g4, 16);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 48:
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 12);
      break;
    case 50:
      HASH_INDEX(0x4D9E12E2960B9832LL, g1, 13);
      break;
    case 53:
      HASH_INDEX(0x54C7D122C6D5EBB5LL, v6, 25);
      break;
    case 60:
      HASH_INDEX(0x092312F91514E83CLL, g5, 17);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 26) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
