
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "c",
    "g1",
    "g2",
    "g3",
    "g4",
    "g5",
    "g6",
    "x",
    "v1",
    "v2",
    "v3",
    "v4",
    "v5",
    "v6",
  };
  if (idx >= 0 && idx < 26) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(c);
    case 13: return GV(g1);
    case 14: return GV(g2);
    case 15: return GV(g3);
    case 16: return GV(g4);
    case 17: return GV(g5);
    case 18: return GV(g6);
    case 19: return GV(x);
    case 20: return GV(v1);
    case 21: return GV(v2);
    case 22: return GV(v3);
    case 23: return GV(v4);
    case 24: return GV(v5);
    case 25: return GV(v6);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
