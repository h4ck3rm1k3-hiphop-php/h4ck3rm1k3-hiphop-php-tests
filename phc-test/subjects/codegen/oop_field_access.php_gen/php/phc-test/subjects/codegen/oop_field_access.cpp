
#include <php/phc-test/subjects/codegen/oop_field_access.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/oop_field_access.php line 21 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 15) {
    case 2:
      HASH_RETURN(0x1F1A885E46B787D2LL, g->s_c_DupIds2,
                  s2);
      break;
    case 9:
      HASH_RETURN(0x0B875164E69FC819LL, g->s_c_DupIds1,
                  s1);
      break;
    case 13:
      HASH_RETURN(0x73D35C8C21BC8A5DLL, g->s_c_DupIds6,
                  s6);
      break;
    case 14:
      HASH_RETURN(0x510176132E65358ELL, g->s_c_DupIds3,
                  s3);
      HASH_RETURN(0x3DD7A20F31F4D45ELL, g->s_c_DupIds4,
                  s4);
      HASH_RETURN(0x038449507753FC9ELL, g->s_c_DupIds5,
                  s5);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 15) {
    case 2:
      HASH_RETURN(0x1F1A885E46B787D2LL, g->s_c_DupIds2,
                  s2);
      break;
    case 9:
      HASH_RETURN(0x0B875164E69FC819LL, g->s_c_DupIds1,
                  s1);
      break;
    case 13:
      HASH_RETURN(0x73D35C8C21BC8A5DLL, g->s_c_DupIds6,
                  s6);
      break;
    case 14:
      HASH_RETURN(0x510176132E65358ELL, g->s_c_DupIds3,
                  s3);
      HASH_RETURN(0x3DD7A20F31F4D45ELL, g->s_c_DupIds4,
                  s4);
      HASH_RETURN(0x038449507753FC9ELL, g->s_c_DupIds5,
                  s5);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a1", m_a1.isReferenced() ? ref(m_a1) : m_a1));
  props.push_back(NEW(ArrayElement)("a2", m_a2.isReferenced() ? ref(m_a2) : m_a2));
  props.push_back(NEW(ArrayElement)("a3", m_a3.isReferenced() ? ref(m_a3) : m_a3));
  props.push_back(NEW(ArrayElement)("a4", m_a4.isReferenced() ? ref(m_a4) : m_a4));
  props.push_back(NEW(ArrayElement)("a5", m_a5.isReferenced() ? ref(m_a5) : m_a5));
  props.push_back(NEW(ArrayElement)("a6", m_a6.isReferenced() ? ref(m_a6) : m_a6));
  c_ObjectData::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x4555E94E338480E0LL, a5, 2);
      break;
    case 6:
      HASH_EXISTS_STRING(0x6C05C2858C72A876LL, a2, 2);
      break;
    case 7:
      HASH_EXISTS_STRING(0x68DF81F26D942FC7LL, a1, 2);
      break;
    case 9:
      HASH_EXISTS_STRING(0x1DC175F7E4343259LL, a4, 2);
      HASH_EXISTS_STRING(0x598AD62830EAF1E9LL, a6, 2);
      break;
    case 13:
      HASH_EXISTS_STRING(0x43EDA7BEE714570DLL, a3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x4555E94E338480E0LL, m_a5,
                         a5, 2);
      break;
    case 6:
      HASH_RETURN_STRING(0x6C05C2858C72A876LL, m_a2,
                         a2, 2);
      break;
    case 7:
      HASH_RETURN_STRING(0x68DF81F26D942FC7LL, m_a1,
                         a1, 2);
      break;
    case 9:
      HASH_RETURN_STRING(0x1DC175F7E4343259LL, m_a4,
                         a4, 2);
      HASH_RETURN_STRING(0x598AD62830EAF1E9LL, m_a6,
                         a6, 2);
      break;
    case 13:
      HASH_RETURN_STRING(0x43EDA7BEE714570DLL, m_a3,
                         a3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x4555E94E338480E0LL, m_a5,
                      a5, 2);
      break;
    case 6:
      HASH_SET_STRING(0x6C05C2858C72A876LL, m_a2,
                      a2, 2);
      break;
    case 7:
      HASH_SET_STRING(0x68DF81F26D942FC7LL, m_a1,
                      a1, 2);
      break;
    case 9:
      HASH_SET_STRING(0x1DC175F7E4343259LL, m_a4,
                      a4, 2);
      HASH_SET_STRING(0x598AD62830EAF1E9LL, m_a6,
                      a6, 2);
      break;
    case 13:
      HASH_SET_STRING(0x43EDA7BEE714570DLL, m_a3,
                      a3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x4555E94E338480E0LL, m_a5,
                         a5, 2);
      break;
    case 6:
      HASH_RETURN_STRING(0x6C05C2858C72A876LL, m_a2,
                         a2, 2);
      break;
    case 7:
      HASH_RETURN_STRING(0x68DF81F26D942FC7LL, m_a1,
                         a1, 2);
      break;
    case 9:
      HASH_RETURN_STRING(0x1DC175F7E4343259LL, m_a4,
                         a4, 2);
      HASH_RETURN_STRING(0x598AD62830EAF1E9LL, m_a6,
                         a6, 2);
      break;
    case 13:
      HASH_RETURN_STRING(0x43EDA7BEE714570DLL, m_a3,
                         a3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_a1 = m_a1.isReferenced() ? ref(m_a1) : m_a1;
  clone->m_a2 = m_a2.isReferenced() ? ref(m_a2) : m_a2;
  clone->m_a3 = m_a3.isReferenced() ? ref(m_a3) : m_a3;
  clone->m_a4 = m_a4.isReferenced() ? ref(m_a4) : m_a4;
  clone->m_a5 = m_a5.isReferenced() ? ref(m_a5) : m_a5;
  clone->m_a6 = m_a6.isReferenced() ? ref(m_a6) : m_a6;
  ObjectData::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  m_a1 = null;
  m_a2 = null;
  m_a3 = null;
  m_a4 = null;
  m_a5 = null;
  m_a6 = null;
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_c_DupIds1 = null;
  g->s_c_DupIds2 = null;
  g->s_c_DupIds3 = null;
  g->s_c_DupIds4 = null;
  g->s_c_DupIds5 = null;
  g->s_c_DupIds6 = null;
}
void csi_c() {
  c_c::os_static_initializer();
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$codegen$oop_field_access_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/oop_field_access.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$oop_field_access_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_g1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g1") : g->GV(g1);
  Variant &v_g2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g2") : g->GV(g2);
  Variant &v_g3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g3") : g->GV(g3);
  Variant &v_g4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g4") : g->GV(g4);
  Variant &v_g5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g5") : g->GV(g5);
  Variant &v_g6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g6") : g->GV(g6);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_v1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v1") : g->GV(v1);
  Variant &v_v2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v2") : g->GV(v2);
  Variant &v_v3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v3") : g->GV(v3);
  Variant &v_v4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v4") : g->GV(v4);
  Variant &v_v5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v5") : g->GV(v5);
  Variant &v_v6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v6") : g->GV(v6);

  f_eval(toString("\t$c = new C();\n\t$c->a1 = 10;\n\t$c->a2 = 20;\n\t$c->a3 = 30;\n\tC::$s1 = 40;\n\tC::$s2 = 50;\n\tC::$s3 = 60;\n\t\n\t$c->a4 = 70;\n\t$c->a5 = 80;\n\t$c->a6 = 90;\n\tC::$s4 = 100;\n\tC::$s5 = 110;\n\tC::$s6 = 120;"));
  echo("** object attribute access, regular assignment **\n");
  (v_g1 = v_c.o_get("a1", 0x68DF81F26D942FC7LL));
  f_eval("var_dump($g1); $g1++; var_dump($c->a1); $c->a1++; var_dump($g1);");
  echo("** object attribute access, reference assignment **\n");
  (v_g2 = ref(lval(v_c.o_lval("a2", 0x6C05C2858C72A876LL))));
  f_eval("var_dump($g2); $g2++; var_dump($c->a2); $c->a2++; var_dump($g2);");
  echo("** object attribute access, separation **\n");
  f_eval("$a3_copy = $c->a3;");
  (v_g3 = ref(lval(v_c.o_lval("a3", 0x43EDA7BEE714570DLL))));
  f_eval("var_dump($g3); $g3++; var_dump($a3_copy); var_dump($c->a3);");
  echo("** class attribute access, regular assignment **\n");
  (v_g4 = g->s_c_DupIds1);
  f_eval("var_dump($g4); $g4++; var_dump(C::$s1); C::$s1++; var_dump($g4);");
  echo("** class attribute access, reference assignment **\n");
  (v_g5 = ref(g->s_c_DupIds2));
  f_eval("var_dump($g5); $g5++; var_dump(C::$s2); C::$s2++; var_dump($g5);");
  echo("** class attribute access, separation **\n");
  f_eval("$s3_copy = C::$s3;");
  (v_g6 = ref(g->s_c_DupIds3));
  f_eval("var_dump($g6); $g6++; var_dump($s3_copy); var_dump(C::$s3);");
  echo("** variable object attribute access, regular assignment **\n");
  (v_x = "a4");
  (v_v1 = v_c.o_get(toString(v_x), -1LL));
  f_eval("var_dump($v1); $v1++; var_dump($c->a4); $c->a4++; var_dump($v1);");
  echo("** variable object attribute access, reference assignment **\n");
  (v_x = "a5");
  (v_v2 = ref(lval(v_c.o_lval(toString(v_x), -1LL))));
  f_eval("var_dump($v2); $v2++; var_dump($c->a5); $c->a5++; var_dump($v2);");
  echo("** variable object attribute access, separation **\n");
  f_eval("$a6_copy = $c->a6;");
  (v_x = "a6");
  (v_v3 = ref(lval(v_c.o_lval(toString(v_x), -1LL))));
  f_eval("var_dump($v3); $v3++; var_dump($a6_copy); var_dump($c->a6);");
  echo("** variable class attribute access, regular assignment **\n");
  (v_x = "s4");
  (v_v4 = c_c::os_get(toString(v_x), -1));
  f_eval("var_dump($v4); $v4++; var_dump(C::$s4); C::$s4++; var_dump($v4);");
  echo("** variable class attribute access, reference assignment **\n");
  (v_x = "s5");
  (v_v5 = ref(c_c::os_lval(toString(v_x), -1)));
  f_eval("var_dump($v5); $v5++; var_dump(C::$s5); C::$s5++; var_dump($v5);");
  echo("** variable class attribute access, separation **\n");
  f_eval("$s6_copy = C::$s6;");
  (v_x = "s6");
  (v_v6 = ref(c_c::os_lval(toString(v_x), -1)));
  f_eval("var_dump($v6); $v6++; var_dump($s6_copy); var_dump(C::$s6);");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
