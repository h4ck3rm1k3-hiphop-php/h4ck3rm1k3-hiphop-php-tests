
#include <php/phc-test/subjects/codegen/bench_hash2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/codegen/bench_hash2.php line 4 */
void f_hash2(int64 v_n) {
  FUNCTION_INJECTION(hash2);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int64 v_i = 0;
  Variant v_hash1;
  Sequence v_hash2;
  Primitive v_key = 0;
  Variant v_value;
  String v_first;
  String v_last;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_hash1.set(toString("foo_") + toString(v_i), (v_i));
        v_hash2.set(toString("foo_") + toString(v_i), (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = v_n); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(2);
      {
        {
          LOOP_COUNTER(3);
          for (ArrayIterPtr iter5 = v_hash1.begin(); !iter5->end(); iter5->next()) {
            LOOP_COUNTER_CHECK(3);
            v_value = iter5->second();
            v_key = iter5->first();
            lval(v_hash2.lvalAt(v_key)) += v_value;
          }
        }
      }
    }
  }
  (v_first = "foo_0");
  (v_last = concat("foo_", (toString(v_n - 1LL))));
  print(LINE(14,(assignCallTemp(eo_0, toString(v_hash1.rvalAt(v_first))),assignCallTemp(eo_2, concat6(toString(v_hash1.rvalAt(v_last)), " ", toString(v_hash2.rvalAt(v_first)), " ", toString(v_hash2.rvalAt(v_last)), "\n")),concat3(eo_0, " ", eo_2))));
} /* function */
Variant pm_php$phc_test$subjects$codegen$bench_hash2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/codegen/bench_hash2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$codegen$bench_hash2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(19,f_hash2(5LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
