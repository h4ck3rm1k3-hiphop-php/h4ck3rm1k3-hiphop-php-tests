
#ifndef __GENERATED_php_phc_test_subjects_codegen_bench_hash2_h__
#define __GENERATED_php_phc_test_subjects_codegen_bench_hash2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/bench_hash2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$codegen$bench_hash2_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_hash2(int64 v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_bench_hash2_h__
