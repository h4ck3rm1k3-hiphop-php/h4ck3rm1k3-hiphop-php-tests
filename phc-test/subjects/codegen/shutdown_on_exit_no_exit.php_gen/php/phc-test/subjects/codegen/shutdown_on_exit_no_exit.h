
#ifndef __GENERATED_php_phc_test_subjects_codegen_shutdown_on_exit_no_exit_h__
#define __GENERATED_php_phc_test_subjects_codegen_shutdown_on_exit_no_exit_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/codegen/shutdown_on_exit_no_exit.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_shut();
Variant pm_php$phc_test$subjects$codegen$shutdown_on_exit_no_exit_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_codegen_shutdown_on_exit_no_exit_h__
