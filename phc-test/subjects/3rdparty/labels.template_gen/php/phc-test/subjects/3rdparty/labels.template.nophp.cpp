
#include <php/phc-test/subjects/3rdparty/labels.template.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$3rdparty$labels_template(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/3rdparty/labels.template);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$3rdparty$labels_template;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("# All files will be found by recursive descent of directories, and each found\n# file will be assigned the default labels. This is then overriden by matching\n# each file against each line in this file, in order. The pattern is specified\n# as a regex, but ! is used as a regex delimiter, so you can safely use / in\n# the patterns. \n#\n# Be aware that all files in a directory are specfied use 'dir/.*', ");
  echo("not 'dir/*'.\n#\n# Possible labels:\n#\t- non-erroneous (default) or erroneous\n#\t- non-interpretable (default) or interpretable (interpretable means executable)\n#\t- long (default) or short\n#\t- size-neutral (default) or size-dependent\n#\t- non-includable (default) or includable\n#\t- no-test_name (don't run with a particular test)\n#\n# A common pattern would be to override some tests by adding, for example");
  echo(", the\n# following to the bottom of the file:\n#\t.*\t\t\t\tnon-interpretable\n\n## Examples\n#\n# general/.*\t\t\t\t\t\t\t\tshort interpretable\n# bugs/.*\t\t\t\t\t\t\t\t\tshort interpretable\n# bugs/bug0009.php\t\t\t\t\t\tsize-dependent\n# general/foreach_error2.php\t\t\terroneous non-interpretable\n# general/binops.php\t\t\t\t\t\tno-CompiledVsInterpreted\n\n## Long by default\n.*\t\tlong\n\n## Remove this to enable a test to check if files are saf");
  echo("ely interpretable\n.* non-interpretable\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
