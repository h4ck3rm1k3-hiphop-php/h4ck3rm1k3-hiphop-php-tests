
#ifndef __GENERATED_php_phc_test_subjects_3rdparty_labels_template_nophp_h__
#define __GENERATED_php_phc_test_subjects_3rdparty_labels_template_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/3rdparty/labels.template.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$3rdparty$labels_template(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_3rdparty_labels_template_nophp_h__
