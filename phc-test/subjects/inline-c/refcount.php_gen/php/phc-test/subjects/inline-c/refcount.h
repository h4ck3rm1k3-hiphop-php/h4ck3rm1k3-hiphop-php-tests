
#ifndef __GENERATED_php_phc_test_subjects_inline_c_refcount_h__
#define __GENERATED_php_phc_test_subjects_inline_c_refcount_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/inline-c/refcount.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$inline_c$refcount_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_inline_c_refcount_h__
