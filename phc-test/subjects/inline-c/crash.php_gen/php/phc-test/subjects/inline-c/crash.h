
#ifndef __GENERATED_php_phc_test_subjects_inline_c_crash_h__
#define __GENERATED_php_phc_test_subjects_inline_c_crash_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/inline-c/crash.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$inline_c$crash_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_inline_c_crash_h__
