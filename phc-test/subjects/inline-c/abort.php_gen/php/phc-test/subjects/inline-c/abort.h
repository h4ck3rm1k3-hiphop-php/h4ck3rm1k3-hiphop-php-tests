
#ifndef __GENERATED_php_phc_test_subjects_inline_c_abort_h__
#define __GENERATED_php_phc_test_subjects_inline_c_abort_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/inline-c/abort.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$inline_c$abort_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_inline_c_abort_h__
