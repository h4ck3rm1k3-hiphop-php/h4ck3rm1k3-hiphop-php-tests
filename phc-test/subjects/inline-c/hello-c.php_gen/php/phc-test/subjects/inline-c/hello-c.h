
#ifndef __GENERATED_php_phc_test_subjects_inline_c_hello_c_h__
#define __GENERATED_php_phc_test_subjects_inline_c_hello_c_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/inline-c/hello-c.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$inline_c$hello_c_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_inline_c_hello_c_h__
