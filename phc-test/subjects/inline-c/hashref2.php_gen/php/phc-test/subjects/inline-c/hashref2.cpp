
#include <php/phc-test/subjects/inline-c/hashref2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$inline_c$hashref2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/inline-c/hashref2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$inline_c$hashref2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  f_eval("$a = 5;");
  f_eval("$b[1] =& $a;");
  f_eval("$b[1] = 6;");
  ;
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
