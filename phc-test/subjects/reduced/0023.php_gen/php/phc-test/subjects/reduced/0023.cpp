
#include <php/phc-test/subjects/reduced/0023.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0023_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0023.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0023_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_Ah __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ah") : g->GV(Ah);
  Variant &v_Ai __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ai") : g->GV(Ai);
  Variant &v_Aj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Aj") : g->GV(Aj);
  Variant &v_TLE3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE3") : g->GV(TLE3);
  Variant &v_TSe8 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSe8") : g->GV(TSe8);

  (v_Ah = 40LL);
  (v_Ai = v_Ah);
  (v_Aj = ref(v_Ai));
  (v_Aj = v_Aj + v_TLE3);
  (v_TSe8 = LINE(9,x_var_export(v_Ai)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
