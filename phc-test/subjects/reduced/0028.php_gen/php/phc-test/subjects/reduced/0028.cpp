
#include <php/phc-test/subjects/reduced/0028.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0028_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0028.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0028_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_stringNonEmpty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stringNonEmpty") : g->GV(stringNonEmpty);
  Variant &v_TLE3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE3") : g->GV(TLE3);

  (v_stringNonEmpty = "foo");
  (v_TLE3 = toBoolean(v_stringNonEmpty));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
