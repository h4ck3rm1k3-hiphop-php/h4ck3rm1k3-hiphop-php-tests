
#ifndef __GENERATED_php_phc_test_subjects_reduced_0057_h__
#define __GENERATED_php_phc_test_subjects_reduced_0057_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0057.fw.h>

// Declarations
#include <cls/a.h>
#include <cls/b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$reduced$0057_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_a(CArrRef params, bool init = true);
Object co_b(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0057_h__
