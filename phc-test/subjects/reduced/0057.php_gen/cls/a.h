
#ifndef __GENERATED_cls_a_h__
#define __GENERATED_cls_a_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/reduced/0057.php line 3 */
class c_a : virtual public ObjectData {
  BEGIN_CLASS_MAP(a)
  END_CLASS_MAP(a)
  DECLARE_CLASS(a, A, ObjectData)
  void init();
  public: Variant m_Name;
  public: void t___construct(Variant v_Name);
  public: ObjectData *create(Variant v_Name);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_a_h__
