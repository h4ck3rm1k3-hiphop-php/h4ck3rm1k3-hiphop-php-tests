
#include <php/phc-test/subjects/reduced/0024.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0024_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0024.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0024_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_TLE12 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE12") : g->GV(TLE12);
  Variant &v_TLE13 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE13") : g->GV(TLE13);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_TLE15 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE15") : g->GV(TLE15);
  Variant &v_TLE16 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE16") : g->GV(TLE16);

  (v_a = concat(toString(v_TLE12), toString(v_TLE13)));
  (v_a = concat(toString(v_TLE15), toString(v_TLE16)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
