
#include <php/phc-test/subjects/reduced/0026.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0026.php line 7 */
void f_getmicrotime() {
  FUNCTION_INJECTION(getmicrotime);
  Variant v_t;

  (v_t = LINE(7,x_gettimeofday()));
} /* function */
/* SRC: phc-test/subjects/reduced/0026.php line 5 */
void f_simple() {
  FUNCTION_INJECTION(simple);
} /* function */
/* SRC: phc-test/subjects/reduced/0026.php line 9 */
Variant f_start_test() {
  FUNCTION_INJECTION(start_test);
  return (LINE(9,f_getmicrotime()), null);
} /* function */
Variant pm_php$phc_test$subjects$reduced$0026_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0026.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0026_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  LINE(11,f_start_test());
  LINE(12,f_simple());
  (v_x = v_y + v_z);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
