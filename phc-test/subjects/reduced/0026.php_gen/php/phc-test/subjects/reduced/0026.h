
#ifndef __GENERATED_php_phc_test_subjects_reduced_0026_h__
#define __GENERATED_php_phc_test_subjects_reduced_0026_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0026.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_getmicrotime();
void f_simple();
Variant pm_php$phc_test$subjects$reduced$0026_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_start_test();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0026_h__
