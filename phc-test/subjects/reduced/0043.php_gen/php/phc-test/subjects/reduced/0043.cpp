
#include <php/phc-test/subjects/reduced/0043.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0043_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0043.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0043_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_usr_langs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("usr_langs") : g->GV(usr_langs);
  Variant &v_lang __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lang") : g->GV(lang);

  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_usr_langs.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_lang = iter3->second();
      {
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_usr_langs.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_lang = iter6->second();
      {
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
