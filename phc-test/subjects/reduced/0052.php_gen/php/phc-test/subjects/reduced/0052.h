
#ifndef __GENERATED_php_phc_test_subjects_reduced_0052_h__
#define __GENERATED_php_phc_test_subjects_reduced_0052_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0052.fw.h>

// Declarations
#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$reduced$0052_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_test(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0052_h__
