
#include <php/phc-test/subjects/reduced/0016.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0016.php line 4 */
void f_write(CVarRef v_init) {
  FUNCTION_INJECTION(write);
  Variant v_TLE23;
  Variant v_TSe20;

  (v_TSe20 = (LINE(6,x_var_dump(1, v_init.rvalAt(v_TLE23))), null));
} /* function */
Variant i_write(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x548D0802D1DB44ADLL, write) {
    return (f_write(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$reduced$0016_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0016.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0016_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_check __attribute__((__unused__)) = (variables != gVariables) ? variables->get("check") : g->GV(check);
  Variant &v_PLA0 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PLA0") : g->GV(PLA0);
  Variant &v_init __attribute__((__unused__)) = (variables != gVariables) ? variables->get("init") : g->GV(init);

  v_check.append(("a"));
  (v_PLA0 = ref(LINE(9,x_each(ref(v_check)))));
  (v_init = v_check.rvalAt(v_PLA0.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(12,f_write(v_init));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
