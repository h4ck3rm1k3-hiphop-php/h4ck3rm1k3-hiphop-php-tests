
#include <php/phc-test/subjects/reduced/0013.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0013_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0013.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0013_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  (v_a = "function f()\n\t{\n\t\treturn $GLOBALS[\"a\"];\t\n\t}");
  f_eval(toString(v_a));
  (v_d = ref(LINE(12,invoke_failed("f", Array(), 0x0000000077560280LL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
