
#ifndef __GENERATED_cls_cl2_h__
#define __GENERATED_cls_cl2_h__

#include <cls/cl1.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/reduced/0056.php line 11 */
class c_cl2 : virtual public c_cl1 {
  BEGIN_CLASS_MAP(cl2)
    PARENT_CLASS(cl1)
  END_CLASS_MAP(cl2)
  DECLARE_CLASS(cl2, cl2, cl1)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cl2_h__
