
#ifndef __GENERATED_php_phc_test_subjects_reduced_0056_h__
#define __GENERATED_php_phc_test_subjects_reduced_0056_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0056.fw.h>

// Declarations
#include <cls/cl1.h>
#include <cls/cl2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$reduced$0056_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_cl1(CArrRef params, bool init = true);
Object co_cl2(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0056_h__
