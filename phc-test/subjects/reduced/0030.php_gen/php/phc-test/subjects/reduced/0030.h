
#ifndef __GENERATED_php_phc_test_subjects_reduced_0030_h__
#define __GENERATED_php_phc_test_subjects_reduced_0030_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0030.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$reduced$0030_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0030_h__
