
#include <php/phc-test/subjects/reduced/0019.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0019.php line 4 */
Variant f_f() {
  FUNCTION_INJECTION(f);
  return null;
} /* function */
Variant pm_php$phc_test$subjects$reduced$0019_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0019.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0019_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_c = ref(LINE(8,f_f())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
