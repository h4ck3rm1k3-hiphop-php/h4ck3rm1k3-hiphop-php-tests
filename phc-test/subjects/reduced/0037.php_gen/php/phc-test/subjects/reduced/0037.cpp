
#include <php/phc-test/subjects/reduced/0037.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0037.php line 4 */
void f_f(CVarRef v_count) {
  FUNCTION_INJECTION(f);
  echo(LINE(6,concat3("f", toString(v_count), "\n")));
} /* function */
Variant i_f(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x286CE1C477560280LL, f) {
    return (f_f(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$reduced$0037_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0037.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0037_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          Variant tmp3 = (v_i);
          int tmp4 = -1;
          if (equal(tmp3, ((LINE(11,f_f(1LL)), null)))) {
            tmp4 = 0;
          }
          switch (tmp4) {
          case 0:
            {
              echo("1\n");
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
