
#include <php/phc-test/subjects/reduced/0048.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0048.php line 6 */
void f_f(CVarRef v_x) {
  FUNCTION_INJECTION(f);
} /* function */
/* SRC: phc-test/subjects/reduced/0048.php line 9 */
void f_g(Variant v_x) {
  FUNCTION_INJECTION(g);
} /* function */
Variant pm_php$phc_test$subjects$reduced$0048_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0048.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0048_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_TLE2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE2") : g->GV(TLE2);
  Variant &v_TLE3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE3") : g->GV(TLE3);
  Variant &v_TLE4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE4") : g->GV(TLE4);
  Variant &v_TLE5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE5") : g->GV(TLE5);

  LINE(12,f_f(v_c.o_get("c", 0x32C769EE5C5509B0LL).rvalAt(v_TLE2).rvalAt(v_TLE3)));
  LINE(13,f_g(ref(lval(lval(lval(v_c.o_lval("d", 0x7A452383AA9BF7C4LL)).lvalAt(v_TLE4)).lvalAt(v_TLE5)))));
  LINE(14,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
