
#include <php/phc-test/subjects/reduced/0017.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0017_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0017.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0017_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_TSa5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSa5") : g->GV(TSa5);
  Variant &v_TSa6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSa6") : g->GV(TSa6);
  Variant &v_TLE28 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE28") : g->GV(TLE28);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_TSa4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSa4") : g->GV(TSa4);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_TSi29 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSi29") : g->GV(TSi29);
  Variant &v_TLE73 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE73") : g->GV(TLE73);

  v_TSa6.append((v_TSa5));
  v_arr.append((v_TLE28));
  v_arr.append((v_TSa4));
  v_arr.append((v_TSa6));
  (v_a = v_arr.rvalAt(2LL, 0x486AFCC090D5F98CLL));
  v_a.set(v_TLE73, (v_TSi29));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
