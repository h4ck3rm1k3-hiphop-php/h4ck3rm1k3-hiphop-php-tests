
#include <php/phc-test/subjects/reduced/0006.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0006.php line 2 */
void f_fun(Variant v_x) {
  FUNCTION_INJECTION(fun);
  (v_x = "x");
} /* function */
Variant pm_php$phc_test$subjects$reduced$0006_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0006.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0006_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);

  (v_g = 60LL);
  (v_h = v_g);
  LINE(9,f_fun(ref(v_h)));
  LINE(10,x_var_export(v_h));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
