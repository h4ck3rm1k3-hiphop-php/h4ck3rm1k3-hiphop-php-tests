
#include <php/phc-test/subjects/reduced/0012.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0012.php line 4 */
Variant f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_TLE0;

  return ref(lval(get_variable_table()->get(v_TLE0)));
} /* function */
Variant pm_php$phc_test$subjects$reduced$0012_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0012.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0012_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_TDEs2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TDEs2") : g->GV(TDEs2);
  Variant &v_TDEr2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TDEr2") : g->GV(TDEr2);

  (v_TDEs2 = "\n$c =& f();\n");
  (v_TDEr2 = f_eval(toString(v_TDEs2)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
