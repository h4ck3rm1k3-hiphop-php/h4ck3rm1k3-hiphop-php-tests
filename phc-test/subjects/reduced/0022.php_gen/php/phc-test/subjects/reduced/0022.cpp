
#include <php/phc-test/subjects/reduced/0022.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0022_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0022.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0022_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_Ai __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ai") : g->GV(Ai);
  Variant &v_Aj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Aj") : g->GV(Aj);

  (v_Aj = ref(v_Ai));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
