
#include <php/phc-test/subjects/reduced/0055.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0055.php line 5 */
Variant c_class1::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_class1::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_class1::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_class1::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_class1::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_class1::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_class1::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_class1::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(class1)
ObjectData *c_class1::cloneImpl() {
  c_class1 *obj = NEW(c_class1)();
  cloneSet(obj);
  return obj;
}
void c_class1::cloneSet(c_class1 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_class1::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x29FD889F97EFDFC5LL, myfunct) {
        return (t_myfunct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_class1::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x29FD889F97EFDFC5LL, myfunct) {
        return (t_myfunct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_class1::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_class1$os_get(const char *s) {
  return c_class1::os_get(s, -1);
}
Variant &cw_class1$os_lval(const char *s) {
  return c_class1::os_lval(s, -1);
}
Variant cw_class1$os_constant(const char *s) {
  return c_class1::os_constant(s);
}
Variant cw_class1$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_class1::os_invoke(c, s, params, -1, fatal);
}
void c_class1::init() {
}
/* SRC: phc-test/subjects/reduced/0055.php line 7 */
void c_class1::t_myfunct(CVarRef v_var1) {
  INSTANCE_METHOD_INJECTION(class1, class1::myfunct);
  echo(toString(v_var1));
} /* function */
Object co_class1(CArrRef params, bool init /* = true */) {
  return Object(p_class1(NEW(c_class1)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$reduced$0055_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0055.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0055_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ptr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ptr") : g->GV(ptr);
  Variant &v_myarr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("myarr") : g->GV(myarr);
  Variant &v_key1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key1") : g->GV(key1);

  (v_ptr = ((Object)(LINE(13,p_class1(p_class1(NEWOBJ(c_class1)())->create())))));
  LINE(14,v_ptr.o_invoke_few_args("myfunct", 0x29FD889F97EFDFC5LL, 1, "test1"));
  lval(v_myarr.lvalAt(v_key1)).set("key2", ("test2"), 0x227687E59C1F7C8BLL);
  LINE(16,v_ptr.o_invoke_few_args("myfunct", 0x29FD889F97EFDFC5LL, 1, lval(v_myarr.lvalAt(v_key1)).refvalAt("key2", 0x227687E59C1F7C8BLL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
