
#ifndef __GENERATED_php_phc_test_subjects_reduced_0055_h__
#define __GENERATED_php_phc_test_subjects_reduced_0055_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0055.fw.h>

// Declarations
#include <cls/class1.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$reduced$0055_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_class1(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0055_h__
