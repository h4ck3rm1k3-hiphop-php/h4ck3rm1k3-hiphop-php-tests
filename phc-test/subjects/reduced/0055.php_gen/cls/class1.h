
#ifndef __GENERATED_cls_class1_h__
#define __GENERATED_cls_class1_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/reduced/0055.php line 5 */
class c_class1 : virtual public ObjectData {
  BEGIN_CLASS_MAP(class1)
  END_CLASS_MAP(class1)
  DECLARE_CLASS(class1, class1, ObjectData)
  void init();
  public: void t_myfunct(CVarRef v_var1);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_class1_h__
