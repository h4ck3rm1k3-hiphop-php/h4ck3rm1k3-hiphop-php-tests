
#include <php/phc-test/subjects/reduced/0047.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0047.php line 4 */
Variant f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_z __attribute__((__unused__)) = g->GV(z);
  return gv_z;
} /* function */
Variant pm_php$phc_test$subjects$reduced$0047_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0047.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0047_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_TLE4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE4") : g->GV(TLE4);
  Variant &v_TLE23 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE23") : g->GV(TLE23);

  (v_z = 2LL);
  (v_TLE4 = LINE(9,f_f()));
  (v_TLE23 = "%s");
  LINE(11,x_printf(2, toString(v_TLE23), Array(ArrayInit(1).set(0, v_TLE4).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
