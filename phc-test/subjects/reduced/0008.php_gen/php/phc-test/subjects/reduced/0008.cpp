
#include <php/phc-test/subjects/reduced/0008.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0008.php line 3 */
void f_set_global_y1() {
  FUNCTION_INJECTION(set_global_y1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_y1 __attribute__((__unused__)) = g->GV(y1);
  (gv_y1 = 30LL);
} /* function */
Variant pm_php$phc_test$subjects$reduced$0008_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0008.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0008_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_TSe10 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSe10") : g->GV(TSe10);
  Variant &v_TLE31 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE31") : g->GV(TLE31);
  Variant &v_TLE25 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE25") : g->GV(TLE25);
  Variant &v_TSp5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSp5") : g->GV(TSp5);

  unset(get_global_array_wrapper());
  (v_TSe10 = (LINE(13,f_set_global_y1()), null));
  (v_TSp5 = LINE(14,x_printf(2, toString(v_TLE31), Array(ArrayInit(1).set(0, v_TLE25).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
