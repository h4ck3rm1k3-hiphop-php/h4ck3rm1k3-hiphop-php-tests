
#include <php/phc-test/subjects/reduced/0009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0009.php line 3 */
void f_set_global_y1() {
  FUNCTION_INJECTION(set_global_y1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_y1 __attribute__((__unused__)) = g->GV(y1);
  (gv_y1 = 30LL);
} /* function */
Variant pm_php$phc_test$subjects$reduced$0009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_y1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y1") : g->GV(y1);

  LINE(8,f_set_global_y1());
  LINE(9,x_printf(2, "%s", Array(ArrayInit(1).set(0, v_y1).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
