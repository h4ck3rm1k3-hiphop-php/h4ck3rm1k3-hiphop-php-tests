
#include <php/phc-test/subjects/reduced/0035.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0035_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0035.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0035_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  v_x.set(5LL, ("a"), 0x350AEB726A15D700LL);
  (v_a = "foo\n");
  echo(toString(variables->get(toString(v_x.rvalAt(5LL, 0x350AEB726A15D700LL)))));
  echo(toString(variables->get(toString(v_x.rvalAt(5LL, 0x350AEB726A15D700LL)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
