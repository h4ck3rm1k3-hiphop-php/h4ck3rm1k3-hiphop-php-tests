
#include <php/phc-test/subjects/reduced/0015.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0015_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0015.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0015_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_TSa3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TSa3") : g->GV(TSa3);
  Variant &v_PLA2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PLA2") : g->GV(PLA2);
  Variant &v_TLE15 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TLE15") : g->GV(TLE15);
  Variant &v_PLA3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PLA3") : g->GV(PLA3);

  (v_PLA2 = v_TSa3);
  (v_PLA3 = ref(lval(v_PLA2.lvalAt(v_TLE15))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
