
#ifndef __GENERATED_php_phc_test_subjects_reduced_0049_h__
#define __GENERATED_php_phc_test_subjects_reduced_0049_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0049.fw.h>

// Declarations
#include <cls/sm_smtag_area.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$reduced$0049_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_area(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0049_h__
