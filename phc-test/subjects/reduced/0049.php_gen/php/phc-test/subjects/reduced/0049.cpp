
#include <php/phc-test/subjects/reduced/0049.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/reduced/0049.php line 3 */
Variant c_sm_smtag_area::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_smtag_area::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_smtag_area::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_sm_smtag_area::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_smtag_area::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_smtag_area::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_smtag_area::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_smtag_area::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_smtag_area)
ObjectData *c_sm_smtag_area::cloneImpl() {
  c_sm_smtag_area *obj = NEW(c_sm_smtag_area)();
  cloneSet(obj);
  return obj;
}
void c_sm_smtag_area::cloneSet(c_sm_smtag_area *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_sm_smtag_area::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_smtag_area::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_smtag_area::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_smtag_area$os_get(const char *s) {
  return c_sm_smtag_area::os_get(s, -1);
}
Variant &cw_sm_smtag_area$os_lval(const char *s) {
  return c_sm_smtag_area::os_lval(s, -1);
}
Variant cw_sm_smtag_area$os_constant(const char *s) {
  return c_sm_smtag_area::os_constant(s);
}
Variant cw_sm_smtag_area$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_smtag_area::os_invoke(c, s, params, -1, fatal);
}
void c_sm_smtag_area::init() {
}
/* SRC: phc-test/subjects/reduced/0049.php line 5 */
void c_sm_smtag_area::t_tagthink() {
  INSTANCE_METHOD_INJECTION(SM_smTag_AREA, SM_smTag_AREA::tagThink);
  LINE(6,o_root_invoke_few_args("sayJS", 0x53D21EB2ECAC1B59LL, 1, o_lval("itemList", 0x0B544F21590D2A74LL)));
} /* function */
Object co_sm_smtag_area(CArrRef params, bool init /* = true */) {
  return Object(p_sm_smtag_area(NEW(c_sm_smtag_area)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$reduced$0049_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0049.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0049_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
