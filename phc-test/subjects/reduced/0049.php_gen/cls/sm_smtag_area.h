
#ifndef __GENERATED_cls_sm_smtag_area_h__
#define __GENERATED_cls_sm_smtag_area_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/reduced/0049.php line 3 */
class c_sm_smtag_area : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_area)
  END_CLASS_MAP(sm_smtag_area)
  DECLARE_CLASS(sm_smtag_area, SM_smTag_AREA, ObjectData)
  void init();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_area_h__
