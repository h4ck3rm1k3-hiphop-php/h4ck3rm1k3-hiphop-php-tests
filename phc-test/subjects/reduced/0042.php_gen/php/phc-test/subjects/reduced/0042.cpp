
#include <php/phc-test/subjects/reduced/0042.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0042_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0042.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0042_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_x1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x1") : g->GV(x1);

  (v_a = "x1");
  (v_b = "x2");
  (v_e = "x5");
  lval(lval(lval(variables->get(toString(v_a)).o_lval(toString(v_b), -1LL)).lvalAt(variables->get(toString(v_c)))).lvalAt(variables->get(toString(v_d)))).o_lval(toString(v_e.rvalAt(variables->get(toString(v_f)))), -1LL) += variables->get(toString(v_g.rvalAt(variables->get(toString(v_h)))));
  LINE(33,x_var_dump(1, v_x1));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
