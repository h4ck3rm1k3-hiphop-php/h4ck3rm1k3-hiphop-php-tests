
#include <php/phc-test/subjects/reduced/0041.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$reduced$0041_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/reduced/0041.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$reduced$0041_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  {
    LOOP_COUNTER(1);
    for ((v_x = 0LL); less(v_x, 10LL); v_x++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          switch (toInt64(v_x)) {
          case 5LL:
            {
              goto continue1;
            }
          }
        }
        echo("done switch\n");
      }
      continue1:;
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
