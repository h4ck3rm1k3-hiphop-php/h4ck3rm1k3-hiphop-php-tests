
#ifndef __GENERATED_php_phc_test_subjects_reduced_0021_h__
#define __GENERATED_php_phc_test_subjects_reduced_0021_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/reduced/0021.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$reduced$0021_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_return_by_ref();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_reduced_0021_h__
