
#include <php/phc-test/subjects/bugs/bug0019.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0019_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0019.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0019_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(String("\0", 1, AttachLiteral));
  echo(String("\0", 1, AttachLiteral));
  echo(String("\0 \001", 3, AttachLiteral));
  echo(String("\001 \0", 3, AttachLiteral));
  echo(String("\0 \001", 3, AttachLiteral));
  echo(String("\001 \0", 3, AttachLiteral));
  echo(String("\0 \001", 3, AttachLiteral));
  echo(String("\001 \0", 3, AttachLiteral));
  echo(String("\0 \001", 3, AttachLiteral));
  echo(String("\001 \0", 3, AttachLiteral));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
