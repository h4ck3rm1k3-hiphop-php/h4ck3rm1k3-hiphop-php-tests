
#ifndef __GENERATED_cls_myclass_h__
#define __GENERATED_cls_myclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/bugs/bug0028.php line 6 */
class c_myclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass)
  END_CLASS_MAP(myclass)
  DECLARE_CLASS(myclass, MyClass, ObjectData)
  void init();
  public: void t___construct(int64 v_var);
  public: ObjectData *create(int64 v_var);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass_h__
