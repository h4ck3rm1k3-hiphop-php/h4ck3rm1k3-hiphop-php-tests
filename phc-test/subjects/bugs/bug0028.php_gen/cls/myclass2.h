
#ifndef __GENERATED_cls_myclass2_h__
#define __GENERATED_cls_myclass2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/bugs/bug0028.php line 16 */
class c_myclass2 : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass2)
  END_CLASS_MAP(myclass2)
  DECLARE_CLASS(myclass2, MyClass2, ObjectData)
  void init();
  public: void t___construct(int64 v_var = 5LL);
  public: ObjectData *create(int64 v_var = 5LL);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass2_h__
