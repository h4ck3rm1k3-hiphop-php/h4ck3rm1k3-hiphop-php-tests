
#ifndef __GENERATED_php_phc_test_subjects_bugs_bug0028_h__
#define __GENERATED_php_phc_test_subjects_bugs_bug0028_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/bugs/bug0028.fw.h>

// Declarations
#include <cls/myclass2.h>
#include <cls/myclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$bugs$bug0028_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_myclass2(CArrRef params, bool init = true);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_bugs_bug0028_h__
