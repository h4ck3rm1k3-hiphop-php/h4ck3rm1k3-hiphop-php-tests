
#include <php/phc-test/subjects/bugs/bug0028.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/bugs/bug0028.php line 16 */
Variant c_myclass2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myclass2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass2)
ObjectData *c_myclass2::create(int64 v_var //  = 5LL
) {
  init();
  t___construct(v_var);
  return this;
}
ObjectData *c_myclass2::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_myclass2::cloneImpl() {
  c_myclass2 *obj = NEW(c_myclass2)();
  cloneSet(obj);
  return obj;
}
void c_myclass2::cloneSet(c_myclass2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myclass2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass2$os_get(const char *s) {
  return c_myclass2::os_get(s, -1);
}
Variant &cw_myclass2$os_lval(const char *s) {
  return c_myclass2::os_lval(s, -1);
}
Variant cw_myclass2$os_constant(const char *s) {
  return c_myclass2::os_constant(s);
}
Variant cw_myclass2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass2::os_invoke(c, s, params, -1, fatal);
}
void c_myclass2::init() {
}
/* SRC: phc-test/subjects/bugs/bug0028.php line 18 */
void c_myclass2::t___construct(int64 v_var //  = 5LL
) {
  INSTANCE_METHOD_INJECTION(MyClass2, MyClass2::__construct);
  bool oldInCtor = gasInCtor(true);
  print("Constructor\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/bugs/bug0028.php line 6 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::create(int64 v_var) {
  init();
  t___construct(v_var);
  return this;
}
ObjectData *c_myclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
}
/* SRC: phc-test/subjects/bugs/bug0028.php line 9 */
void c_myclass::t___construct(int64 v_var) {
  INSTANCE_METHOD_INJECTION(MyClass, MyClass::__construct);
  bool oldInCtor = gasInCtor(true);
  print("Constructor\n");
  gasInCtor(oldInCtor);
} /* function */
Object co_myclass2(CArrRef params, bool init /* = true */) {
  return Object(p_myclass2(NEW(c_myclass2)())->dynCreate(params, init));
}
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$bugs$bug0028_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0028.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0028_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_x = ((Object)(LINE(25,create_object("myclass", Array())))));
  (v_x = ((Object)(LINE(26,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create(1LL))))));
  (v_x = ((Object)(LINE(28,p_myclass2(p_myclass2(NEWOBJ(c_myclass2)())->create())))));
  (v_x = ((Object)(LINE(29,p_myclass2(p_myclass2(NEWOBJ(c_myclass2)())->create(1LL))))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
