
#include <php/phc-test/subjects/bugs/bug0024.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0024_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0024.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0024_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_diary_directory __attribute__((__unused__)) = (variables != gVariables) ? variables->get("diary_directory") : g->GV(diary_directory);
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_filesplit __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filesplit") : g->GV(filesplit);
  Variant &v_check_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("check_filename") : g->GV(check_filename);

  LOOP_COUNTER(1);
  {
    while (toBoolean((v_filename = LINE(8,x_readdir(toObject(v_diary_directory)))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_filesplit = LINE(10,x_explode(".", toString(v_filename))));
        if (toBoolean(LINE(12,x_ereg("[0-9]{6}", toString(v_check_filename))))) {
          concat_assign(v_check_filename, toString(".") + toString(v_filesplit.rvalAt(1, 0x5BCA7C69B794F8CELL)));
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
