
#include <php/phc-test/subjects/bugs/bug0022.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0022_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0022.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0022_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<!--\n\tAll of this magic explained in Section 10.2 on\n\thttp://ie.php.net/manual/en/language.basic-syntax.php\n-->\n\n");
  echo("script tags ok\n");
  echo("\n");
  echo("spaced script tags ok\n");
  echo("\n");
  echo("with single quotes ok\n");
  echo("\n<!--\n\tASP style tags are \"less portable\", whatever that means. They need the\n\tphp.ini directive \"asp_tags\". Short tags (aka sgml tags) are also not\n\trecommended, and come via the \"short_tags\" php directive.\n-->\n\n");
  echo("sgml style ok\n");
  echo("\n");
  echo("special sgml style echo ok\n");
  echo("\n\n");
  echo("<%");
  echo(" echo \"ASP style ok\\n\"; %>\n");
  echo("<%=");
  echo(" \"asp style special echo ok\\n\"; %>\n\n");
  echo("test success\n");
  echo("\n");
  echo("test success\n");
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
