
#include <php/phc-test/subjects/bugs/bug0021.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/bugs/bug0021.php line 7 */
Variant c_testclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_testclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_testclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("logicalKey", m_logicalKey.isReferenced() ? ref(m_logicalKey) : m_logicalKey));
  c_ObjectData::o_get(props);
}
bool c_testclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x02DB4A3261724DB5LL, logicalKey, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_testclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x02DB4A3261724DB5LL, m_logicalKey,
                         logicalKey, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_testclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x02DB4A3261724DB5LL, m_logicalKey,
                      logicalKey, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_testclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x02DB4A3261724DB5LL, m_logicalKey,
                         logicalKey, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_testclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(testclass)
ObjectData *c_testclass::cloneImpl() {
  c_testclass *obj = NEW(c_testclass)();
  cloneSet(obj);
  return obj;
}
void c_testclass::cloneSet(c_testclass *clone) {
  clone->m_logicalKey = m_logicalKey.isReferenced() ? ref(m_logicalKey) : m_logicalKey;
  ObjectData::cloneSet(clone);
}
Variant c_testclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x1376C7908C9A397FLL, getlogicalkey) {
        return (t_getlogicalkey());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_testclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x1376C7908C9A397FLL, getlogicalkey) {
        return (t_getlogicalkey());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_testclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_testclass$os_get(const char *s) {
  return c_testclass::os_get(s, -1);
}
Variant &cw_testclass$os_lval(const char *s) {
  return c_testclass::os_lval(s, -1);
}
Variant cw_testclass$os_constant(const char *s) {
  return c_testclass::os_constant(s);
}
Variant cw_testclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_testclass::os_invoke(c, s, params, -1, fatal);
}
void c_testclass::init() {
  m_logicalKey = null;
}
/* SRC: phc-test/subjects/bugs/bug0021.php line 10 */
Variant c_testclass::t_getlogicalkey() {
  INSTANCE_METHOD_INJECTION(Testclass, Testclass::getLogicalKey);
  return t___isset(toString(m_logicalKey)) ? ((Variant)(o_get(toString(m_logicalKey), -1LL))) : ((Variant)(null));
} /* function */
Object co_testclass(CArrRef params, bool init /* = true */) {
  return Object(p_testclass(NEW(c_testclass)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$bugs$bug0021_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0021.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0021_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  (v_test = ((Object)(LINE(16,p_testclass(p_testclass(NEWOBJ(c_testclass)())->create())))));
  (v_test.o_lval("logicalKey", 0x02DB4A3261724DB5LL) = "testkeyname");
  echo(toString(LINE(19,v_test.o_invoke_few_args("getLogicalKey", 0x1376C7908C9A397FLL, 0))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
