
#ifndef __GENERATED_cls_testclass_h__
#define __GENERATED_cls_testclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/bugs/bug0021.php line 7 */
class c_testclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(testclass)
  END_CLASS_MAP(testclass)
  DECLARE_CLASS(testclass, Testclass, ObjectData)
  void init();
  public: Variant m_logicalKey;
  public: Variant t_getlogicalkey();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testclass_h__
