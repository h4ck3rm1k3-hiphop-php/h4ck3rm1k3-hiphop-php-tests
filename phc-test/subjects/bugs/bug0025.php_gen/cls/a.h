
#ifndef __GENERATED_cls_a_h__
#define __GENERATED_cls_a_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/bugs/bug0025.php line 7 */
class c_a : virtual public ObjectData {
  BEGIN_CLASS_MAP(a)
  END_CLASS_MAP(a)
  DECLARE_CLASS(a, A, ObjectData)
  void init();
  public: static void ti_bar(const char* cls);
  public: static void t_bar() { ti_bar("a"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_a_h__
