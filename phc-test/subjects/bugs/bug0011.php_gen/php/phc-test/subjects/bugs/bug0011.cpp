
#include <php/phc-test/subjects/bugs/bug0011.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0011_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0011.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0011_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\nHexdecimal ints near LONG_MAX (automatic conversion to float after LONG_MAX)\n");
  echo("0x7ffffffd\t= ");
  LINE(4,x_var_dump(1, 2147483645LL));
  echo("0x7ffffffe\t= ");
  LINE(5,x_var_dump(1, 2147483646LL));
  echo("0x7fffffff\t= ");
  LINE(6,x_var_dump(1, 2147483647LL));
  echo("0x80000000\t= ");
  LINE(7,x_var_dump(1, 0.0));
  echo("0x80000001\t= ");
  LINE(8,x_var_dump(1, 0.0));
  echo("0x80000002\t= ");
  LINE(9,x_var_dump(1, 0.0));
  echo("\nDecimal ints near LONG_MAX (automatic conversion to float after LONG_MAX)\n");
  echo("2147483645\t= ");
  LINE(12,x_var_dump(1, 2147483645LL));
  echo("2147483646\t= ");
  LINE(13,x_var_dump(1, 2147483646LL));
  echo("2147483647\t= ");
  LINE(14,x_var_dump(1, 2147483647LL));
  echo("2147483648\t= ");
  LINE(15,x_var_dump(1, 2147483648.0));
  echo("2147483649\t= ");
  LINE(16,x_var_dump(1, 2147483649.0));
  echo("2147483650\t= ");
  LINE(17,x_var_dump(1, 2147483650.0));
  echo("\nHexdecimal ints near ULONG_MAX(truncated to int(LONG_MAX) after ULONG_MAX)\n");
  echo("0xfffffffd\t= ");
  LINE(20,x_var_dump(1, 0.0));
  echo("0xfffffffe\t= ");
  LINE(21,x_var_dump(1, 0.0));
  echo("0xffffffff\t= ");
  LINE(22,x_var_dump(1, 0.0));
  echo("0x100000000\t= ");
  LINE(23,x_var_dump(1, 4294967296LL));
  echo("0x100000001\t= ");
  LINE(24,x_var_dump(1, 4294967297LL));
  echo("0x100000002\t= ");
  LINE(25,x_var_dump(1, 4294967298LL));
  echo("\nDecimal ints near ULONG_MAX(floats as far as the eye can see)\n");
  echo("4294967293\t= ");
  LINE(28,x_var_dump(1, 4294967293.0));
  echo("4294967294\t= ");
  LINE(29,x_var_dump(1, 4294967294.0));
  echo("4294967295\t= ");
  LINE(30,x_var_dump(1, 4294967295.0));
  echo("4294967296\t= ");
  LINE(31,x_var_dump(1, 4294967296LL));
  echo("4294967297\t= ");
  LINE(32,x_var_dump(1, 4294967297LL));
  echo("4294967298\t= ");
  LINE(33,x_var_dump(1, 4294967298LL));
  echo("\nHexdecimal ints near LONG_MAX (automatic conversion to float after LONG_MAX)\n");
  echo("-0x7ffffffd\t= ");
  LINE(36,x_var_dump(1, -2147483645LL));
  echo("-0x7ffffffe\t= ");
  LINE(37,x_var_dump(1, -2147483646LL));
  echo("-0x7fffffff\t= ");
  LINE(38,x_var_dump(1, -2147483647LL));
  echo("-0x80000000\t= ");
  LINE(39,x_var_dump(1, 0.0));
  echo("-0x80000001\t= ");
  LINE(40,x_var_dump(1, 0.0));
  echo("-0x80000002\t= ");
  LINE(41,x_var_dump(1, 0.0));
  echo("\nDecimal ints near LONG_MAX (automatic conversion to float after LONG_MAX)\n");
  echo("-2147483645\t= ");
  LINE(44,x_var_dump(1, -2147483645LL));
  echo("-2147483646\t= ");
  LINE(45,x_var_dump(1, -2147483646LL));
  echo("-2147483647\t= ");
  LINE(46,x_var_dump(1, -2147483647LL));
  echo("-2147483648\t= ");
  LINE(47,x_var_dump(1, -2147483648.0));
  echo("-2147483649\t= ");
  LINE(48,x_var_dump(1, -2147483649.0));
  echo("-2147483650\t= ");
  LINE(49,x_var_dump(1, -2147483650.0));
  echo("\nHexdecimal ints near ULONG_MAX(truncated to int(LONG_MAX) after ULONG_MAX)\n");
  echo("-0xfffffffd\t= ");
  LINE(52,x_var_dump(1, 0.0));
  echo("-0xfffffffe\t= ");
  LINE(53,x_var_dump(1, 0.0));
  echo("-0xffffffff\t= ");
  LINE(54,x_var_dump(1, 0.0));
  echo("-0x100000000\t= ");
  LINE(55,x_var_dump(1, -4294967296LL));
  echo("-0x100000001\t= ");
  LINE(56,x_var_dump(1, -4294967297LL));
  echo("-0x100000002\t= ");
  LINE(57,x_var_dump(1, -4294967298LL));
  echo("\nDecimal ints near ULONG_MAX(floats as far as the eye can see)\n");
  echo("-4294967293\t= ");
  LINE(60,x_var_dump(1, -4294967293.0));
  echo("-4294967294\t= ");
  LINE(61,x_var_dump(1, -4294967294.0));
  echo("-4294967295\t= ");
  LINE(62,x_var_dump(1, -4294967295.0));
  echo("-4294967296\t= ");
  LINE(63,x_var_dump(1, -4294967296LL));
  echo("-4294967297\t= ");
  LINE(64,x_var_dump(1, -4294967297LL));
  echo("-4294967298\t= ");
  LINE(65,x_var_dump(1, -4294967298LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
