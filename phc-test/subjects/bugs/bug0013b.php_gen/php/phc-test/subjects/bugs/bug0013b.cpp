
#include <php/phc-test/subjects/bugs/bug0013b.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0013b_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0013b.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0013b_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c1") : g->GV(c1);
  Variant &v_c2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c2") : g->GV(c2);

  if (toBoolean(v_c1)) {
    echo("Hi");
  }
  else if (toBoolean(v_c2)) {
    echo("World");
  }
  else {
    echo("Bye");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
