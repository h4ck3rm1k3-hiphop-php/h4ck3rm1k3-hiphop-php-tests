
#ifndef __GENERATED_php_phc_test_subjects_bugs_bug0018_h__
#define __GENERATED_php_phc_test_subjects_bugs_bug0018_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/bugs/bug0018.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$bugs$bug0018_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_bugs_bug0018_h__
