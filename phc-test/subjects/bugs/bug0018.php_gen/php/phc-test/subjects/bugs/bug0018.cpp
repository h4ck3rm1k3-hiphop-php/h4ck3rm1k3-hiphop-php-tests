
#include <php/phc-test/subjects/bugs/bug0018.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0018_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0018.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0018_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = LINE(5,concat3("a ", toString(v_b), " c")));
  (v_a = LINE(6,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(7,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(8,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(9,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(10,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(11,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(12,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(13,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(14,concat3(String("\0\001\002 ", 4, AttachLiteral), toString(v_b), " \003\004\00529")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
