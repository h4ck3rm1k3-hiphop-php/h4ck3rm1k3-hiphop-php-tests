
#include <php/phc-test/subjects/bugs/bug0009.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0009_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0009.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0009_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(toString(-2LL));
  echo("\n");
  echo(toString(2LL));
  echo("\n");
  echo(toString(2LL));
  echo("\n");
  echo(toString(-2147483648.0));
  echo("\n");
  echo(toString(2147483647LL));
  echo("\n");
  echo(toString(2147483647LL));
  echo("\n");
  echo(toString(-2147483649.0));
  echo("\n");
  echo(toString(2147483648.0));
  echo("\n");
  echo(toString(2147483648.0));
  echo("\n");
  echo(toString(444LL));
  echo("\n");
  echo(toString(292LL));
  echo("\n");
  echo(toString(1092LL));
  echo("\n");
  echo(toString(2147483647LL));
  echo("\n");
  echo(toString(0.0));
  echo("\n");
  echo(toString(4294967296LL));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
