
#ifndef __GENERATED_php_phc_test_subjects_bugs_bug0009_h__
#define __GENERATED_php_phc_test_subjects_bugs_bug0009_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/bugs/bug0009.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$bugs$bug0009_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_bugs_bug0009_h__
