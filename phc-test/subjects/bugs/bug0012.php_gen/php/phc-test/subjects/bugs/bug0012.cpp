
#include <php/phc-test/subjects/bugs/bug0012.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0012_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0012.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0012_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);
  Variant &v_opcodes __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opcodes") : g->GV(opcodes);

  concat_assign(lval(v_opcodes.lvalAt("code", 0x5B2CD7DDAB7A1DECLL)), toString(v_line));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
