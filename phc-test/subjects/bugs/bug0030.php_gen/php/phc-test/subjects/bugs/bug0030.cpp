
#include <php/phc-test/subjects/bugs/bug0030.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/bugs/bug0030.php line 8 */
void f_show_error() {
  FUNCTION_INJECTION(show_error);
  Variant v_php_errormsg;
  Variant v_error_message;

  (silenceInc(), silenceDec(LINE(10,invoke_failed("strpos", Array(), 0x000000006F045BDELL))));
  (v_error_message = v_php_errormsg);
  echo(LINE(12,concat3("inside the function : ", toString(v_error_message), " \n")));
} /* function */
Variant pm_php$phc_test$subjects$bugs$bug0030_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0030.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0030_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(6,x_ini_set("track_errors", toString(true)));
  LINE(15,f_show_error());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
