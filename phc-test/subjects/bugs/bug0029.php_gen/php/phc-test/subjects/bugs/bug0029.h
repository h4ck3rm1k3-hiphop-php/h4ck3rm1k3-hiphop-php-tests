
#ifndef __GENERATED_php_phc_test_subjects_bugs_bug0029_h__
#define __GENERATED_php_phc_test_subjects_bugs_bug0029_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/bugs/bug0029.fw.h>

// Declarations
#include <cls/stack.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$bugs$bug0029_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_stack(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_bugs_bug0029_h__
