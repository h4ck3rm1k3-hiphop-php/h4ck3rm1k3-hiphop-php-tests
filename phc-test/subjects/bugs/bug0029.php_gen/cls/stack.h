
#ifndef __GENERATED_cls_stack_h__
#define __GENERATED_cls_stack_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/bugs/bug0029.php line 6 */
class c_stack : virtual public ObjectData {
  BEGIN_CLASS_MAP(stack)
  END_CLASS_MAP(stack)
  DECLARE_CLASS(stack, Stack, ObjectData)
  void init();
  public: void t___construct(Variant v_input);
  public: ObjectData *create(Variant v_input);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_stack_h__
