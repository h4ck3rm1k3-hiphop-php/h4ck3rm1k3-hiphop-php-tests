
#include <php/phc-test/subjects/bugs/bug0027.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/bugs/bug0027.php line 6 */
Variant c_superclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_superclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_superclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_superclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_superclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_superclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_superclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_superclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(superclass)
ObjectData *c_superclass::cloneImpl() {
  c_superclass *obj = NEW(c_superclass)();
  cloneSet(obj);
  return obj;
}
void c_superclass::cloneSet(c_superclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_superclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_superclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_superclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_superclass$os_get(const char *s) {
  return c_superclass::os_get(s, -1);
}
Variant &cw_superclass$os_lval(const char *s) {
  return c_superclass::os_lval(s, -1);
}
Variant cw_superclass$os_constant(const char *s) {
  return c_superclass::os_constant(s);
}
Variant cw_superclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_superclass::os_invoke(c, s, params, -1, fatal);
}
void c_superclass::init() {
}
/* SRC: phc-test/subjects/bugs/bug0027.php line 10 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_superclass::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_superclass::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  c_superclass::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  return c_superclass::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  return c_superclass::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_superclass::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_superclass::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_superclass::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  c_superclass::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_superclass::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_superclass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_superclass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
  c_superclass::init();
}
Object co_superclass(CArrRef params, bool init /* = true */) {
  return Object(p_superclass(NEW(c_superclass)())->dynCreate(params, init));
}
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$bugs$bug0027_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0027.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0027_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
