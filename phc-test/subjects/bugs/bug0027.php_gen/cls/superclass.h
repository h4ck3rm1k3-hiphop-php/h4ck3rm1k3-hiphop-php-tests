
#ifndef __GENERATED_cls_superclass_h__
#define __GENERATED_cls_superclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/bugs/bug0027.php line 6 */
class c_superclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(superclass)
  END_CLASS_MAP(superclass)
  DECLARE_CLASS(superclass, Superclass, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_superclass_h__
