
#ifndef __GENERATED_cls_myclass_h__
#define __GENERATED_cls_myclass_h__

#include <cls/superclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/bugs/bug0027.php line 10 */
class c_myclass : virtual public c_superclass {
  BEGIN_CLASS_MAP(myclass)
    PARENT_CLASS(superclass)
  END_CLASS_MAP(myclass)
  DECLARE_CLASS(myclass, MyClass, superclass)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass_h__
