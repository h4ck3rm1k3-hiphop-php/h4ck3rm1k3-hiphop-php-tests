
#include <php/phc-test/subjects/bugs/bug0001.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$bugs$bug0001_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/bugs/bug0001.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$bugs$bug0001_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = LINE(15,concat3("[", toString(v_b), "]")));
  (v_a = "[]");
  (v_a = toString(v_b));
  (v_a = toString(" ") + toString(v_b));
  (v_a = toString("e") + toString(v_b));
  (v_a = toString("ee") + toString(v_b));
  (v_a = toString("ee ") + toString(v_b));
  (v_a = toString("ee   ") + toString(v_b));
  (v_a = LINE(28,concat3("a ", toString(v_b), " c")));
  (v_a = LINE(29,concat3("\001\002 ", toString(v_b), " \003\004")));
  (v_a = LINE(30,concat3(String("\0\001\002 ", 4, AttachLiteral), toString(v_b), " \003\004\005")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
