
#include <php/phc-test/subjects/php_bugs/41118.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$php_bugs$41118_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/php_bugs/41118.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$php_bugs$41118_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_var_dump(1, 2147483647LL));
  LINE(4,x_var_dump(1, 2.0E+10));
  LINE(5,x_var_dump(1, 20000000007.0));
  LINE(6,x_var_dump(1, -2147483647LL));
  LINE(7,x_var_dump(1, -2.0E+10));
  LINE(8,x_var_dump(1, -20000000007.0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
