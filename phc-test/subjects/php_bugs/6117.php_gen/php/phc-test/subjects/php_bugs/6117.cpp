
#include <php/phc-test/subjects/php_bugs/6117.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$php_bugs$6117_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/php_bugs/6117.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$php_bugs$6117_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);

  v_test.set(123LL, ("one hundred twenty three"), 0x1CF1FACF39E3F302LL);
  v_test.set(13LL, ("thirteen"), 0x56B680C42D6D01CFLL);
  v_test.set(0LL, ("zero"), 0x77CFA1EEF01BCA90LL);
  v_test.set(3LL, ("three"), 0x135FDDF6A6BFBBDDLL);
  v_test.set(11LL, ("eleven"), 0x0E4C22B81C9845ACLL);
  {
    LOOP_COUNTER(1);
    for (LINE(12,x_reset(ref(v_test))); toBoolean((v_key = x_key(ref(v_test)))); x_next(ref(v_test))) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(13,concat5("TEST[", toString(v_key), "]=", toString(v_test.rvalAt(v_key)), "<br>")));
      }
    }
  }
  echo("************************************<br>");
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_test.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_value = iter4->second();
      v_key = iter4->first();
      {
        echo(LINE(21,concat5("TEST[", toString(v_key), "]=", toString(v_value), "<br>")));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
