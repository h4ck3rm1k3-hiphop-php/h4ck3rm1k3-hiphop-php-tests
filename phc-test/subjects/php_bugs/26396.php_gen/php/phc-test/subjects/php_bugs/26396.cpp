
#include <php/phc-test/subjects/php_bugs/26396.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/php_bugs/26396.php line 4 */
void f_f() {
  FUNCTION_INJECTION(f);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_usr_langs __attribute__((__unused__)) = g->GV(usr_langs);
  Variant v_lang;

  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = gv_usr_langs.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_lang = iter3->second();
      {
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/php_bugs/26396.php line 12 */
void f_g() {
  FUNCTION_INJECTION(g);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_usr_langs __attribute__((__unused__)) = g->GV(usr_langs);
  Variant v_lang;

  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = gv_usr_langs.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_lang = iter6->second();
      {
        LINE(16,f_f());
        echo(toString(v_lang) + toString(" "));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/php_bugs/26396.php line 32 */
void f_f2() {
  FUNCTION_INJECTION(f2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_lang;

  {
    LOOP_COUNTER(7);
    Variant map8 = g->GV(usr_langs);
    for (ArrayIterPtr iter9 = map8.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_lang = iter9->second();
      {
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/php_bugs/26396.php line 38 */
void f_g2() {
  FUNCTION_INJECTION(g2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_lang;

  {
    LOOP_COUNTER(10);
    Variant map11 = g->GV(usr_langs);
    for (ArrayIterPtr iter12 = map11.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_lang = iter12->second();
      {
        LINE(41,f_f2());
        echo(concat(toString(v_lang), " "));
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$php_bugs$26396_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/php_bugs/26396.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$php_bugs$26396_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_usr_langs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("usr_langs") : g->GV(usr_langs);
  Variant &v_lang __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lang") : g->GV(lang);

  (v_usr_langs = ScalarArrays::sa_[0]);
  echo("Test1:<br>");
  LINE(22,f_g());
  echo("<br>----------<br>");
  echo("Test2:<br>");
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_usr_langs.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_lang = iter15->second();
      {
        LINE(27,f_f());
        echo(toString(v_lang) + toString(" "));
      }
    }
  }
  echo("Test1:<br>");
  LINE(47,f_g2());
  echo("<br>----------<br>");
  echo("Test2:<br>");
  {
    LOOP_COUNTER(16);
    for (ArrayIterPtr iter18 = v_usr_langs.begin(); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_lang = iter18->second();
      {
        LINE(52,f_f2());
        echo(concat(toString(v_lang), " "));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
