
#include <php/phc-test/subjects/labels.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$labels(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/labels);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$labels;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("# All files will be found by recursive descent of directories, and each found\n# file will be assigned the default labels. This is then overriden by matching\n# each file against each line in this file, in order. The pattern is specified\n# as a regex, but ! is used as a regex delimiter, so you can safely use / in\n# the patterns. \n#\n# Be aware that all files in a directory are specfied use 'dir/.*', ");
  echo("not 'dir/*'.\n#\n# Possible labels:\n#\t- non-interpretable or interpretable (interpretable means executable). The\n#\t    default is to run a purity test to check if they are interpretable\n#\t- long (default) or short\n#\t- size-neutral (default) or size-dependent\n#\t- non-includable (default) or includable\n#\t- no-test_name (don't run with a particular test)\n#\n# A common pattern would be to override some t");
  echo("ests by adding, for example, the\n# following to the bottom of the file:\n#\t.*\t\t\t\tnon-interpretable\n#\tcodegen/.*\tinterpretable\n\n# Counter-intuitive: Invalid tests which accidentally pass are only caught when\n# they dont run by the fact that they fail when we interpret them. However,\n# they will be skipped if BasicParseTest finds the error, so its safe to label\n# them as interpreted.\n.*\t\t\t\t\t\t\t\t\t\t\tsho");
  echo("rt interpretable\ninline-c/.*\t\t\t\t\t\t\t\tnon-interpretable\nunsupported/.*\t\t\t\t\t\t\tnon-interpretable no-Annotated_test # skip the first test, and all the others will automatically be skipped.\n3rdparty/.*\t\t\t\t\t\t\t\tlong\n\nbugs/bug0009.php\t\t\t\t\t\tsize-dependent\nbugs/bug0011.php\t\t\t\t\t\tsize-dependent\n\n# These tests all cause infinite loops\nparsing/binops\\d+.php\t\t\t\tlong no-Generate_C no-Refcounts no-CompiledVsInterpr");
  echo("eted\n\n# gcc takes 44 seconds to compile this test. The generate C test is also very long\nparsing/allchars_inline.php\t\tno-Generate_C no-Refcounts no-CompiledVsInterpreted\n\n# The refcounts test is a little buggy. If there is code both before and after a function def, it will print out different results.\ncodegen/shutdown_on_.*.php\t\t\tno-Refcounts \ncodegen/nested_statements.php\t\tno-Refcounts\ncodegen/pr");
  echo("int.php\t\t\t\t\t\tno-Refcounts\ncodegen/bench_.*.php\t\t\t\t\tno-Refcounts # these take ages\n\nparsing/for.php\t\t\t\t\t\tnon-interpretable # infinite loops\nparsing/for_cond_with_comma.php\tnon-interpretable # infinite loops\ncodegen/for_varying_cond.php\t\tnon-interpretable # infinite loops\n\nparsing/instring.php\t\t\t\t\tnon-interpretable # php's parser doesnt like this\n\n# declare doesn't make sense after the ast\nparsing/c");
  echo("omments_declare.php\t\tno-cb_AST-to-HIR\nparsing/capital_ticks.php\t\t\tno-cb_AST-to-HIR\nparsing/layout5_declare.php\t\tno-cb_AST-to-HIR\n\n# Reports the contents of GLOBALS, which only differ because of temps\ncodegen/str_vs_int_index.php\t\tno-cb_AST-to-HIR\n\n# Doesn't print the hash-bang, which fails, but its expected.\nparsing/hash_bang1.php\t\t\t\tno-InterpretIncludes\n\nbenchmarks/.*\t\t\t\t\t\t\tlong no-Refcounts\n\nben");
  echo("chmarks/shootout/.*\t\t\t\t\t\t\t\t\t\t\tinterpretable\nbenchmarks/shootout/knucleotide.php\t\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/moments.php\t\t\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/regexdna.php-2.php\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/regexmatch.php\t\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/revcomp.php\t\t\t\t\t\t\t\tnon-interpretable # uses st");
  echo("din\nbenchmarks/shootout/reversefile.php\t\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/spellcheck.php\t\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/sumcol.php\t\t\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/wc.php\t\t\t\t\t\t\t\t\t\tnon-interpretable # uses stdin\nbenchmarks/shootout/wordfreq.php\t\t\t\t\t\t\t\tnon-interpretable # uses stdin\n\n\nbenchmarks/RUBBoS/.*\t\t\t\t\t\t\t\t\t\t\t\tnon-interp");
  echo("retable # uses a DB\n\nbenchmarks/roadsend/benchmarks/tests/.*\t\t\t\t\tinterpretable\nbenchmarks/roadsend/benchmarks/tests/julia-ppm.php\t\tnon-interpretable # writes a file\nbenchmarks/roadsend/benchmarks/tests/mysql.*\t\t\t\tnon-interpretable # uses a DB\nbenchmarks/roadsend/benchmarks/regression/.*\t\t\t\tnon-interpretable # reads /etc/passwd\nbenchmarks/roadsend/benchmarks/data/include/.*\t\t\tnon-interpretable # ge");
  echo("nerated code TODO remove\n\n# These tests print out timing results, which wont be identical in interpretable tests\nbenchmarks/phpbench-0.8.1/.*\t\t\t\t\t\t\t\t\tnon-interpretable\nbenchmarks/php_bs/.*\t\t\t\t\t\t\t\t\t\t\t\tnon-interpretable\nbenchmarks/zend/.*\t\t\t\t\t\t\t\t\t\t\t\tnon-interpretable\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
