
#include <php/phc-test/subjects/README.dirs.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$README_dirs(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/README.dirs);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$README_dirs;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("A quick description of the purpose of each directory. Note that in general,\ntest cases should be short, and in the case of long tests, errors should be\nmoved to a separate short test.\n\n\t3rdparty/\n\t\tWithin the testing framework, 3rdparty/ is a special directory. It uses\n\t\tits own label file, 3rdparty/labels/, and tests in this directory are\n\t\tnormally considered 'long' (i.e. you need -l to run them");
  echo("). We generally put\n\t\tapplications in here to test them.\n\t\n\tbugs/\n\t\tTest cases from bug reports that were sent to us. Named in the order\n\t\tthey were filed.\n\n\tcodegen/\n\t\tIf there is something special to be done for a particular feature when\n\t\tgenerating code, there should be a test case in here. \n\n\tdynamic/\n\t\tTests dealing with dynamic features (typing probably not included, unless\n\t\tits interestin");
  echo("g) should go in here.\n\n\terrors/\n\t\tPHP run-time errors (and some warnings) which we have moved to\n\t\tcompile-time errors. There's a fine line between errors/ and invalid/,\n\t\tsometimes.\n\n\thorrible/\n\t\tA list of difficult parsing problems.\n\n\tinline-c/\n\t\tWe allow inlined C to test certain features in codegen. This is for\n\t\ttesting only, and should certainly not be used by anybody.\n\n\tparsing/\n\t\tTests to ");
  echo("hit every bit of the parser. General tests are also put in here.\n\n\tphc/ \n\t\tTest features of phc, such as command-line options, as opposed to\n\t\tsupported PHP features.\n\n\tphp_bugs/\n\t\tTest cases from bug reports submitted to PHP bugzilla. There help us with\n\t\tcorner cases when something is being implemented.\n\n\treduced/\n\t\tWhen we reduce test cases to isolate a problem, we copy the reduced case\n\t\there.");
  echo(" Normally, we'd add a comment indicating its origin. These are named\n\t\tin the order they are created.\n\n\tunsupported/\n\t\tSome things are a combination of silly, not widely used, unsafe, dont\n\t\tmake sense for a compiler, or most importantly, very difficult to\n\t\tsupport.\n\n\twarnings/\n\t\tCurrently, the difference between errors and warnings (and sometimes\n\t\tinvalid) is a little bit vague. Supposedly test");
  echo("s which cause warnings\n\t\tshould be in here, but occasionally, they're in errors/ instead.\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
