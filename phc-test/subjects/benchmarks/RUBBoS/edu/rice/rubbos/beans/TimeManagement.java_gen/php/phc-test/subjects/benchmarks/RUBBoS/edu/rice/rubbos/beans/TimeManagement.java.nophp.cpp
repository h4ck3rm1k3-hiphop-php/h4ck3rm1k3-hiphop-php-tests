
#include <php/phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/beans/TimeManagement.java.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$beans$TimeManagement_java(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/beans/TimeManagement.java);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$beans$TimeManagement_java;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("package edu.rice.rubbos.beans;\n\nimport java.util.GregorianCalendar;\nimport java.lang.String;\n\n/** This class provides additionnal functions that the GregorianCalendar\n * class does not provide. It is mainly to compute time differences and\n * display the date in a database understandable format.\n * @author <a href=\"mailto:cecchet@rice.edu\">Emmanuel Cecchet</a> and <a href=\"mailto:julie.marguerite@inri");
  echo("alpes.fr\">Julie Marguerite</a>\n * @version 1.0\n */\n\npublic class TimeManagement extends GregorianCalendar\n{\n\n  /**\n   * Creates a new <code>TimeManagement</code> instance.\n   *\n   */\n  public TimeManagement()\n  {\n  }\n\n\n  /** Returns a string representation of the current date (when the method is called)\n   * conforming to the following database format : 'YYYY-MM-DD hh:mm:ss'\n   *\n   * @return current");
  echo(" date in database format\n   *\n   */\n  public static String currentDateToString()\n  {\n    GregorianCalendar d = new GregorianCalendar();\n    return dateToString(d);\n  }\n\n\n  /**\n   * Returns a string representation of a date conforming to \n   * the following database format : 'YYYY-MM-DD hh:mm:ss'\n   *\n   * @param d a <code>GregorianCalendar</code> value\n   * @return current date in database format\n  ");
  echo(" *\n   */\n  public static String dateToString(GregorianCalendar d)\n  {\n    String result;\n    int    year, month, day, hour, minute, second;\n\n    year   = d.get(d.YEAR);\n    month  = d.get(d.MONTH) + 1;\n    day    = d.get(d.DATE);\n    hour   = d.get(d.HOUR_OF_DAY);\n    minute = d.get(d.MINUTE);\n    second = d.get(d.SECOND);\n    result = year+\"-\"+month+\"-\"+day+\" \"+hour+\":\"+minute+\":\"+second;\n    ret");
  echo("urn result;\n  }\n\n\n  /** Returns the time elapsed between startDate and endDate in milliseconds\n   *\n   * @param startDate beginning date\n   * @param endDate ending date\n   *\n   * @return elapsed time in milliseconds\n   *\n   */\n  public static long diffTimeInMs(GregorianCalendar startDate, GregorianCalendar endDate)\n  {\n    long  year, month, day, hour, minute, second, millis;\n\n    year   = endDate");
  echo(".get(endDate.YEAR)-startDate.get(startDate.YEAR);\n    month  = endDate.get(endDate.MONTH)-startDate.get(startDate.MONTH);\n    day    = endDate.get(endDate.DATE)-startDate.get(startDate.DATE);\n    hour   = endDate.get(endDate.HOUR_OF_DAY)-startDate.get(startDate.HOUR_OF_DAY);\n    minute = endDate.get(endDate.MINUTE)-startDate.get(startDate.MINUTE);\n    second = endDate.get(endDate.SECOND)-startDate");
  echo(".get(startDate.SECOND);\n    millis = endDate.get(endDate.MILLISECOND)-startDate.get(startDate.MILLISECOND);\n \n    if (millis < 0)\n    {\n      millis = millis + 1000;\n      second = second - 1;\n    }\n    if (second < 0)\n    {\n      second = second + 60;\n      minute = minute - 1;\n    }\n    if (minute < 0)\n    {\n      minute = minute + 60;\n      hour = hour - 1;\n    }\n    if (hour < 0)\n    {\n      hour ");
  echo("= hour + 24;\n      day = day - 1;\n    }\n    if (day < 0)\n    {\n      day = day + startDate.getActualMaximum(startDate.DAY_OF_MONTH); // is the same as startDate.DATE\n      month = month - 1;\n    }\n    if (month < 0)\n    {\n      month = month + 12;\n      year = year - 1;\n    }\n    return millis+(second*1000)+(minute*60000)+(hour*3600000)+(day*24*3600000)+(month*30*24*3600000); // We approximate 30 da");
  echo("ys per month\n  }\n\n\n  /** Returns a string representation of the time elapsed between startDate\n   * and endDate. Example of a returned string : \"1 month 3 days 6 hours 33 minutes 4 seconds 234 milliseconds\"\n   *\n   * @param startDate beginning date\n   * @param endDate ending date\n   *\n   * @return string containing the time difference up to the millisecond\n   *\n   */\n  public static String diffTim");
  echo("e(GregorianCalendar startDate, GregorianCalendar endDate)\n  {\n    int    year, month, day, hour, minute, second, millis;\n    String result = \"\";\n\n    year   = endDate.get(endDate.YEAR)-startDate.get(startDate.YEAR);\n    month  = endDate.get(endDate.MONTH)-startDate.get(startDate.MONTH);\n    day    = endDate.get(endDate.DATE)-startDate.get(startDate.DATE);\n    hour   = endDate.get(endDate.HOUR_OF_D");
  echo("AY)-startDate.get(startDate.HOUR_OF_DAY);\n    minute = endDate.get(endDate.MINUTE)-startDate.get(startDate.MINUTE);\n    second = endDate.get(endDate.SECOND)-startDate.get(startDate.SECOND);\n    millis = endDate.get(endDate.MILLISECOND)-startDate.get(startDate.MILLISECOND);\n \n    if (millis < 0)\n    {\n      millis = millis + 1000;\n      second = second - 1;\n    }\n    if (second < 0)\n    {\n      secon");
  echo("d = second + 60;\n      minute = minute - 1;\n    }\n    if (minute < 0)\n    {\n      minute = minute + 60;\n      hour = hour - 1;\n    }\n    if (hour < 0)\n    {\n      hour = hour + 24;\n      day = day - 1;\n    }\n    if (day < 0)\n    {\n      day = day + startDate.getActualMaximum(startDate.DAY_OF_MONTH); // is the same as startDate.DATE\n      month = month - 1;\n    }\n    if (month < 0)\n    {\n      month = ");
  echo("month + 12;\n      year = year - 1;\n    }\n\n    if (year > 0)\n      result = year+\" year\";\n    if (year > 1)\n      result = result+\"s \";\n    else\n      result = result+\" \";\n    if (month > 0)\n      result = result+month+\" month\";\n    if (month > 1)\n      result = result+\"s \";\n    else\n      result = result+\" \";\n    if (day > 0)\n      result = result+day+\" day\";\n    if (day > 1)\n      result = result");
  echo("+\"s \";\n    else\n      result = result+\" \";\n    if (hour > 0)\n      result = result+hour+\" hour\";\n    if (hour > 1)\n      result = result+\"s \";\n    else\n      result = result+\" \";\n    if (minute > 0)\n      result = result+minute+\" minute\";\n    if (minute > 1)\n      result = result+\"s \";\n    else\n      result = result+\" \";\n    if (second > 0)\n      result = result+second+\" second\";\n    if (second > ");
  echo("1)\n      result = result+\"s \";\n    else\n      result = result+\" \";\n    result = result+millis+\" millisecond\";\n    if (millis > 1)\n      result = result+\"s \";\n    else\n      result = result+\" \";\n    return result;\n  }\n\n\n  /** Compute a new GregorianCalendar from a beginning date and a duration in days.\n   *\n   * @param startDate beginning date\n   * @param durationInDays number of days to add to sta");
  echo("rtDate.\n   *\n   * @return date corresponding to startDate+durationInDays\n   *\n   */\n  public static GregorianCalendar addDays(GregorianCalendar startDate, int durationInDays)\n  {\n    GregorianCalendar date = (GregorianCalendar)startDate.clone();\n    date.add(date.DATE, durationInDays);\n    return date;\n  }\n}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
