
#include <php/phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/client/package.html.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$client$package_html(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/client/package.html);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$client$package_html;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n<html>\r\n  <head>\r\n    <title>RUBBoS package</title>\r\n  </head>\r\n\r\n  <body>\r\n    This package contains all the client classes to benchmark RUBBoS.\r\n    The benchmark is the same for PHP, Servlet and EJB versions.\r\n    <hr>\r\n    <address><a href=\"mailto:cecchet@cs.rice.edu\">Emmanuel Cecchet</a></address>\r\n<!-- Created: Thu Sep 13 19:11:28 CEST 2001");
  echo(" -->\r\n<!-- hhmts start -->\r\nLast modified: Wed Oct 24 18:11:13 Heure d'\351t\351 Paris Madrid 2001\r\n<!-- hhmts end -->\r\n  </body>\r\n</html>\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
