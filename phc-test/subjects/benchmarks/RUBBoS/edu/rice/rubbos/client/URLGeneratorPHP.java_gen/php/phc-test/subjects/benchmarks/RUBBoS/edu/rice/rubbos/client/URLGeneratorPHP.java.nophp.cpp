
#include <php/phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/client/URLGeneratorPHP.java.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$client$URLGeneratorPHP_java(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/client/URLGeneratorPHP.java);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$client$URLGeneratorPHP_java;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("package edu.rice.rubbos.client;\n\n/**\n * This class provides the needed URLs to access all features of RUBBoS (PHP version).\n * You must provide the name and port of the Web site running RUBBoS as well\n * as the directories where the scripts and HTML files reside. For example:\n * <pre>\n * URLGenerator rubbosWeb = new URLGeneratorEJB(\"www.testbed.cs.rice.edu\", 80, \"/PHP\", \"/PHP\");\n * </pre>\n * @author");
  echo(" <a href=\"mailto:cecchet@rice.edu\">Emmanuel Cecchet</a> and <a href=\"mailto:julie.marguerite@inrialpes.fr\">Julie Marguerite</a>\n * @version 1.0\n */\n\npublic class URLGeneratorPHP extends URLGenerator\n{\n\n  /**\n   * Set the name and port of the Web site running RUBBoS as well as the\n   * directories where the HTML and PHP scripts reside. Examples:\n   * <pre>\n   * URLGenerator rubbosWeb = new URLGenerator(");
  echo("\"www.testbed.cs.rice.edu\", 80, \"/PHP\", \"/PHP\");\n   * </pre>\n   *\n   * @param host Web site address\n   * @param port HTTP server port\n   * @param HTMLFilesPath path where HTML files reside\n   * @param ScriptFilesPath path to the script files\n   */\n  public URLGeneratorPHP(String host, int port, String HTMLFilesPath, String ScriptFilesPath)\n  {\n    super(host, port, HTMLFilesPath, ScriptFilesPath);\n ");
  echo(" }\n\n\n  /**\n   * Returns the name of the Search script.\n   *\n   * @return Search script name\n   */\n  public  String SearchScript()\n  {\n    return \"Search.php\";\n  }\n\n\n  /**\n   * Returns the name of the Browse Categories script.\n   *\n   * @return Browse Categories script name\n   */\n  public  String BrowseCategoriesScript()\n  {\n    return \"BrowseCategories.php\";\n  }\n\n  /**\n   * Returns the name of the");
  echo(" Stories of the day script.\n   *\n   * @return Stories of the day script name\n   */\n  public  String StoriesOfTheDayScript()\n  {\n    return \"StoriesOfTheDay.php\";\n  }\n\n\n  /**\n   * Returns the name of the Older stories script.\n   *\n   * @return Older stories script name\n   */\n  public  String OlderStoriesScript()\n  {\n    return \"OlderStories.php\";\n  }\n\n  /**\n   * Returns the name of the Submit story");
  echo(" script.\n   *\n   * @return Submit story script name\n   */\n  public  String SubmitStoryScript()\n  {\n    return \"SubmitStory.php\";\n  }\n  /**\n   * Returns the name of the Post Comment script.\n   *\n   * @return Post Comment script name\n   */\n  public  String PostCommentScript()\n  {\n    return \"PostComment.php\";\n  }\n\n  /**\n   * Returns the name of the Register User script.\n   *\n   * @return Register Us");
  echo("er script name\n   */\n  public  String RegisterUserScript()\n  {\n    return \"RegisterUser.php\";\n  }\n\n  /**\n   * Returns the name of the Browse stories By Category script.\n   *\n   * @return Browse stories by category By Category script name\n   */\n  public  String BrowseStoriesByCategoryScript()\n  {\n    return \"BrowseStoriesByCategory.php\";\n  }\n\n  /**\n   * Returns the name of the Store Comment script.");
  echo("\n   *\n   * @return Store Store comment script name\n   */\n  public  String StoreCommentScript()\n  {\n    return \"StoreComment.php\";\n  }\n\n  /**\n   * Returns the name of the Store Story script.\n   *\n   * @return Store Story script name\n   */\n  public  String StoreStoryScript()\n  {\n    return \"StoreStory.php\";\n  }\n\n  /**\n   * Returns the name of the View Story script.\n   *\n   * @return View Story scrip");
  echo("t name\n   */\n  public  String ViewStoryScript()\n  {\n    return \"ViewStory.php\";\n  }\n\n  /**\n   * Returns the name of the View Comment script.\n   *\n   * @return View Comment Info script name\n   */\n  public  String ViewCommentScript()\n  {\n    return \"ViewComment.php\";\n  }\n\n  /**\n   * Returns the name of the Moderate Comment script.\n   *\n   * @return Moderate Comment script name\n   */\n  public String ");
  echo("ModerateCommentScript()\n  {\n    return \"ModerateComment.php\";\n  }\n\n  /**\n   * Returns the name of the Store Moderate Log script.\n   *\n   * @return Store Moderate Log script name\n   */\n  public String StoreModerateLogScript()\n  {\n    return \"StoreModeratorLog.php\";\n  }\n\n  /**\n   * Returns the name of the Author Tasks script.\n   *\n   * @return Author Tasks script name\n   */\n  public String AuthorTas");
  echo("ksScript()\n  {\n    return \"Author.php\";\n  }\n\n  /**\n   * Returns the name of the Review Stories script.\n   *\n   * @return Review Stories script name\n   */\n  public String ReviewStoriesScript()\n  {\n    return \"ReviewStories.php\";\n  }\n\n  /**\n   * Returns the name of the Accept Story script.\n   *\n   * @return Accept Story script name\n   */\n  public String AcceptStoryScript()\n  {\n    return \"AcceptStor");
  echo("y.php\";\n  }\n\n  /**\n   * Returns the name of the Accept Story script.\n   *\n   * @return Accept Story script name\n   */\n  public String RejectStoryScript()\n  {\n    return \"RejectStory.php\";\n  }\n\n}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
