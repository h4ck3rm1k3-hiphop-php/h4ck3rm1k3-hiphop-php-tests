
#include <php/phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/client/URLGeneratorServlets.java.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$client$URLGeneratorServlets_java(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/edu/rice/rubbos/client/URLGeneratorServlets.java);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$edu$rice$rubbos$client$URLGeneratorServlets_java;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("package edu.rice.rubbos.client;\n\n/**\n * This class provides the needed URLs to access all features of RUBBoS (servlets version).\n * You must provide the name and port of the Web site running RUBBoS as well\n * as the directories where the scripts and HTML files reside. For example:\n * <pre>\n * URLGenerator rubbosWeb = new URLGeneratorEJB(\"www.testbed.cs.rice.edu\", 80, \"/PHP\", \"/PHP\");\n * </pre>\n * @a");
  echo("uthor <a href=\"mailto:cecchet@rice.edu\">Emmanuel Cecchet</a> and <a href=\"mailto:julie.marguerite@inrialpes.fr\">Julie Marguerite</a>\n * @version 1.0\n */\n\npublic class URLGeneratorServlets extends URLGenerator\n{\n\n  /**\n   * Set the name and port of the Web site running RUBBoS as well as the\n   * directories where the HTML and servlets reside. Examples:\n   * <pre>\n   * URLGenerator rubbosWeb = new URLGen");
  echo("erator(\"www.testbed.cs.rice.edu\", 80, \"/PHP\", \"/PHP\");\n   * </pre>\n   *\n   * @param host Web site address\n   * @param port HTTP server port\n   * @param HTMLFilesPath path where HTML files reside\n   * @param ScriptFilesPath path to the script files\n   */\n  public URLGeneratorServlets(String host, int port, String HTMLFilesPath, String ScriptFilesPath)\n  {\n    super(host, port, HTMLFilesPath, ScriptF");
  echo("ilesPath);\n  }\n\n\n  /**\n   * Returns the name of the Search script.\n   *\n   * @return Search script name\n   */\n  public  String SearchScript()\n  {\n    return \"edu.rice.rubbos.servlets.Search\";\n  }\n\n\n  /**\n   * Returns the name of the Browse Categories script.\n   *\n   * @return Browse Categories script name\n   */\n  public  String BrowseCategoriesScript()\n  {\n    return \"edu.rice.rubbos.servlets.Brow");
  echo("seCategories\";\n  }\n\n  /**\n   * Returns the name of the Stories of the day script.\n   *\n   * @return Stories of the day script name\n   */\n  public  String StoriesOfTheDayScript()\n  {\n    return \"edu.rice.rubbos.servlets.StoriesOfTheDay\";\n  }\n\n\n  /**\n   * Returns the name of the Older stories script.\n   *\n   * @return Older stories script name\n   */\n  public  String OlderStoriesScript()\n  {\n    retu");
  echo("rn \"edu.rice.rubbos.servlets.OlderStories\";\n  }\n\n  /**\n   * Returns the name of the Submit story script.\n   *\n   * @return Submit story script name\n   */\n  public  String SubmitStoryScript()\n  {\n    return \"edu.rice.rubbos.servlets.SubmitStory\";\n  }\n  /**\n   * Returns the name of the Post Comment script.\n   *\n   * @return Post Comment script name\n   */\n  public  String PostCommentScript()\n  {\n    ");
  echo("return \"edu.rice.rubbos.servlets.PostComment\";\n  }\n\n  /**\n   * Returns the name of the Register User script.\n   *\n   * @return Register User script name\n   */\n  public  String RegisterUserScript()\n  {\n    return \"edu.rice.rubbos.servlets.RegisterUser\";\n  }\n\n  /**\n   * Returns the name of the Browse stories By Category script.\n   *\n   * @return Browse stories by category By Category script name\n   ");
  echo("*/\n  public  String BrowseStoriesByCategoryScript()\n  {\n    return \"edu.rice.rubbos.servlets.BrowseStoriesByCategory\";\n  }\n\n  /**\n   * Returns the name of the Store Comment script.\n   *\n   * @return Store Store comment script name\n   */\n  public  String StoreCommentScript()\n  {\n    return \"edu.rice.rubbos.servlets.StoreComment\";\n  }\n\n  /**\n   * Returns the name of the Store Story script.\n   *\n   *");
  echo(" @return Store Story script name\n   */\n  public  String StoreStoryScript()\n  {\n    return \"edu.rice.rubbos.servlets.StoreStory\";\n  }\n\n  /**\n   * Returns the name of the View Story script.\n   *\n   * @return View Story script name\n   */\n  public  String ViewStoryScript()\n  {\n    return \"edu.rice.rubbos.servlets.ViewStory\";\n  }\n\n  /**\n   * Returns the name of the View Comment script.\n   *\n   * @retur");
  echo("n View Comment Info script name\n   */\n  public  String ViewCommentScript()\n  {\n    return \"edu.rice.rubbos.servlets.ViewComment\";\n  }\n\n  /**\n   * Returns the name of the Moderate Comment script.\n   *\n   * @return Moderate Comment script name\n   */\n  public String ModerateCommentScript()\n  {\n    return \"edu.rice.rubbos.servlets.ModerateComment\";\n  }\n\n  /**\n   * Returns the name of the Store Moderat");
  echo("e Log script.\n   *\n   * @return Store Moderate Log script name\n   */\n  public String StoreModerateLogScript()\n  {\n    return \"edu.rice.rubbos.servlets.StoreModeratorLog\";\n  }\n\n  /**\n   * Returns the name of the Author Tasks script.\n   *\n   * @return Author Tasks script name\n   */\n  public String AuthorTasksScript()\n  {\n    return \"edu.rice.rubbos.servlets.Author\";\n  }\n\n  /**\n   * Returns the name ");
  echo("of the Review Stories script.\n   *\n   * @return Review Stories script name\n   */\n  public String ReviewStoriesScript()\n  {\n    return \"edu.rice.rubbos.servlets.ReviewStories\";\n  }\n\n  /**\n   * Returns the name of the Accept Story script.\n   *\n   * @return Accept Story script name\n   */\n  public String AcceptStoryScript()\n  {\n    return \"edu.rice.rubbos.servlets.AcceptStory\";\n  }\n\n  /**\n   * Returns");
  echo(" the name of the Accept Story script.\n   *\n   * @return Accept Story script name\n   */\n  public String RejectStoryScript()\n  {\n    return \"edu.rice.rubbos.servlets.RejectStory\";\n  }\n\n}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
