
#include <php/phc-test/subjects/benchmarks/RUBBoS/config.mk.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$config_mk(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/config.mk);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$config_mk;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("##############################\n#    Environment variables   #\n##############################\n\nJAVA_HOME=/usr\nJAVA  = $(JAVA_HOME)/bin/java\nJAVAC = $(JAVA_HOME)/bin/javac\nJAVACC = $(JAVAC) $(JAVACOPTS)\nRMIC = $(JAVA_HOME)/bin/rmic\nRMIREGISTRY= $(JAVA_HOME)/bin/rmiregistry\nCLASSPATH = .:$(J2EE_HOME)/lib/j2ee.jar:$(JAVA_HOME)/jre/lib/rt.jar:/opt/jakarta-tomcat-3.2.3/lib/servlet.jar\nJAVADOC = $(JAVA_H");
  echo("OME)/bin/javadoc\nJAR = $(JAVA_HOME)/bin/jar\n\nGENIC = ${JONAS_ROOT}/bin/unix/GenIC\n\nMAKE = gmake\nCP = /bin/cp\nRM = /bin/rm\nMKDIR = /bin/mkdir\n\n\n# EJB server: supported values are jonas or jboss\nEJB_SERVER = jonas\n\n# DB server: supported values are MySQL or PostgreSQL\nDB_SERVER = MySQL\n\n%.class: %.java\n\t${JAVACC} -classpath ${CLASSPATH} $<\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
