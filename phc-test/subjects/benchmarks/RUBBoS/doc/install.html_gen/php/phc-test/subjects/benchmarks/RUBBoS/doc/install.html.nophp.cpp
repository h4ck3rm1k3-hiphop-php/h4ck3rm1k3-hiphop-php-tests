
#include <php/phc-test/subjects/benchmarks/RUBBoS/doc/install.html.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$doc$install_html(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/doc/install.html);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$doc$install_html;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\r\n<html>\r\n<head>\r\n   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n   <meta name=\"GENERATOR\" content=\"Mozilla/4.76 [en] (Win95; U) [Netscape]\">\r\n   <meta name=\"Author\" content=\"Julie Marguerite and Emmanuel Cecchet\">\r\n   <title>RUBBoS: Installation and configuration</title>\r\n</head>\r\n<body text=\"#000000\" bgcolor=\"#FFFF");
  echo("FF\" link=\"#0000EE\" vlink=\"#551A8B\" alink=\"#FF0000\">\r\n\r\n<center><img SRC=\"RUBBoS_logo.jpg\" height=100 width=291></center>\r\n\r\n<center>\r\n<h1>\r\n<font color=\"#FF6600\">Installing and configuring</font></h1></center>\r\n\r\n<h2>\r\n<b><font color=\"#006600\">Software requirements</font></b></h2>\r\n<font color=\"#000000\">For the bulletin bord:</font>\r\n<ul>\r\n<li>\r\n<a href=\"http://www.mysql.com/\">MySQL</a> v.3.23.43-max</li>\r\n\r\n<li>\r\n<a hre");
  echo("f=\"http://www.apache.org\">Apache</a> v.1.3.22</li>\r\n\r\n<li>\r\n<a href=\"http://www.php.net\">PHP</a> v.4.0.6 module for Apache</li>\r\n</ul>\r\n<font color=\"#000000\">For the client:</font>\r\n<ul>\r\n<li>\r\nSun <a href=\"http://java.sun.com/products/\">JDK</a> 1.3.1</li>\r\n\r\n<li>\r\n<a href=\"http://freshmeat.net/projects/sysstat\">Sysstat utility</a></li>\r\n\r\n<li>\r\nGnuplot</li>\r\n</ul>\r\n\r\n<h2>\r\n<b><font color=\"#006600\">RUBBoS files structure");
  echo("</font></b></h2>\r\nThe directory tree of RUBBoS is organized as follow:\r\n<br>&nbsp;\r\n<table BORDER COLS=2 WIDTH=\"100%\" NOSAVE >\r\n<tr NOSAVE>\r\n<td WIDTH=\"25%\" NOSAVE><tt>/RUBBoS</tt></td>\r\n\r\n<td>contains configuration files for mysql (mysql.properties) and RUBiS\r\n(rubis.properties) and shell scripts for starting or stoping Jonas and\r\nTomcat. This directoty also contains the makefile.</td>\r\n</tr>\r\n\r\n<tr>\r\n<td><tt>/R");
  echo("UBBoS/doc</tt></td>\r\n\r\n<td>contains the RUBBoS documentation.</td>\r\n</tr>\r\n\r\n<tr>\r\n<td><tt>/RUBBoS/database</tt></td>\r\n\r\n<td>contains the files with the lists of regions and categories used to\r\ngenerate the database data.</td>\r\n</tr>\r\n\r\n<tr>\r\n<td><tt>/RUBBoS/PHP</tt></td>\r\n\r\n<td>contains the html files and PHP scripts for the version of RUBiS that\r\nis designed to be used with PHP only.</td>\r\n</tr>\r\n\r\n<tr>\r\n<td><tt>/RUBBo");
  echo("S/workload</tt></td>\r\n\r\n<td>contains the files that describes the workload of the bidding system.\r\nWe designed various patterns of request distribution. The browse_only_transition\r\npattern simulate user's behavior that only generate read requests from\r\nthe database.\r\n<br>The default_transition pattern is a mix of read and write requests.\r\nIt simulate a user who looks for items and categories but also ");
  echo("who can\r\nsell or put bids on items.</td>\r\n</tr>\r\n\r\n<tr>\r\n<td><tt>/RUBBoS/bench</tt></td>\r\n\r\n<td></td>\r\n</tr>\r\n\r\n<tr>\r\n<td><tt>/RUBBoS/edu/rice/rubbos/client</tt></td>\r\n\r\n<td><tt>edu.rice.rubis.client</tt> package contains the files used in the\r\nbenchmark to simulate clients and perform measurements.</td>\r\n</tr>\r\n</table>\r\n\r\n<br>&nbsp;\r\n<h2>\r\n<b><font color=\"#006600\">Configuration</font></b></h2>\r\n\r\n<h3>\r\n<font color=\"#000000\">");
  echo("Setting up the Makefile for the client</font></h3>\r\n<font color=\"#000000\">Various environment variables are defined in the\r\nmakefile. You have to set them to correspond to your installation directories\r\n:</font>\r\n<br><font color=\"#000000\">JAVA = path to the program execution tool (java)</font>\r\n<br><font color=\"#000000\">JAVAC = path to the java compiler (javac or jikes)</font>\r\n<br><font color=\"#000000\">RMIC ");
  echo("= path to the stub generator (rmic)</font>\r\n<br><font color=\"#000000\">CLASSPATH = your classpath including path to\r\nj2ee.jar and servlet.jar.</font>\r\n<br><font color=\"#000000\"></font>&nbsp;\r\n<h3>\r\nCompiling and running</h3>\r\n<b><font color=\"#CC6600\">Client emulator</font></b>\r\n<br>&nbsp;\r\n<table BORDER COLS=2 WIDTH=\"100%\" NOSAVE >\r\n<tr NOSAVE>\r\n<td WIDTH=\"20%\" NOSAVE><tt>make client</tt></td>\r\n\r\n<td><font color=\"#00000");
  echo("0\">compile the client</font></td>\r\n</tr>\r\n\r\n<tr>\r\n<td><tt>make emulator</tt></td>\r\n\r\n<td><font color=\"#000000\">run the client</font></td>\r\n</tr>\r\n</table>\r\n\r\n<p><b><font color=\"#CC6600\">API documentation</font></b>\r\n<br>&nbsp;\r\n<table BORDER COLS=2 WIDTH=\"100%\" NOSAVE >\r\n<tr NOSAVE>\r\n<td WIDTH=\"20%\" NOSAVE><tt><font color=\"#000000\">make javadoc</font></tt></td>\r\n\r\n<td><font color=\"#000000\">&nbsp;generate the javadoc documentat");
  echo("ion</font></td>\r\n</tr>\r\n</table>\r\n\r\n<h3>\r\nSetting up rubbos.properties</h3>\r\nThis files contains information about the http server : name, port, paths\r\nfor html pages and scripts for each version of RUBBoS. The version of RUBBoS\r\nand the transition table (workload pattern) currently used and are also\r\ndefined in this file\r\n<br>&nbsp;\r\n<p>\r\n<hr WIDTH=\"100%\">\r\n<center>&nbsp;<i>RUBBoS (C) 2001 - Rice University");
  echo("/INRIA</i>\r\n<br><img SRC=\"rice.gif\" height=40 width=138><img SRC=\"logo_inria.gif\" height=55 width=185></center>\r\n\r\n</body>\r\n</html>\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
