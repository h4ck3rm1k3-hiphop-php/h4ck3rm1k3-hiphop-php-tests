
#include <php/phc-test/subjects/benchmarks/RUBBoS/Makefile.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$Makefile(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/Makefile);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$Makefile;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\n###########################\n#    RUBBoS Makefile      #\n###########################\n\n\n##############################\n#    Environment variables   #\n##############################\n\nJAVA  = /usr/bin/java\nJAR  = /usr/bin/jar\nJAVAC = /usr/bin/javac\n#JAVAC = /usr/bin/jikes\n#JAVACOPTS = +E -deprecation\nJAVACC = $(JAVAC) $(JAVACOPTS)\nRMIC = /usr/bin/rmic\nRMIREGISTRY= /usr/bin/rmiregistry\n#CLASSPATH = .:");
  echo("/usr/java/j2sdkee1.3/lib/j2ee.jar:/usr/java/jdk1.3.1/jre/lib/rt.jar:/opt/jakarta-tomcat-3.2.3/lib/servlet.jar\nCLASSPATH = .\nJAVADOC = /usr/bin/javadoc\n\n\n####################\n#    EJB version   #\n####################\n\nEntityBeans = Category Region User Item OldItem Bid Comment BuyNow\nSessionBeans = Query\nBeansFiles = $(EntityBeans) $(addsuffix Home,$(EntityBeans)) $(addsuffix PK,$(EntityBeans)) $(a");
  echo("ddsuffix Bean,$(EntityBeans)) \\\n\t     $(SessionBeans) $(addsuffix Home,$(SessionBeans)) $(addsuffix Bean,$(SessionBeans))\n\nall_beans_sources =  $(addprefix edu/rice/rubbos/beans/, $(addsuffix .java, $(BeansFiles))) edu/rice/rubbos/beans/TimeManagement.java edu/rice/rubbos/beans/ItemAndBids.java\nall_beans_obj = $(addprefix edu/rice/rubbos/beans/, $(addsuffix .class, $(BeansFiles))) edu/rice/rubbos/");
  echo("beans/TimeManagement.class edu/rice/rubbos/beans/ItemAndBids.class\n\nbeans_xml = $(addprefix META-INF/, $(addsuffix .xml, $(SessionBeans)))  $(addprefix META-INF/, $(addsuffix .xml, $(EntityBeans))) \n\nbeans:  $(all_beans_obj) $(beans_xml) \n\n%.xml: Makefile\n\tGenIC -rmiopts -d . -javac $(JAVAC) -javacopts $(JAVACOPTS) -verbose $@\n\nEJB_Servlets = ServletPrinter Config \\\n\t   BrowseCategories BrowseRegi");
  echo("ons SearchItemsByCategory SearchItemsByRegion \\\n\t   ViewItem ViewUserInfo ViewBidHistory AboutMe \\\n\t   Auth RegisterUser RegisterItem SellItemForm \\\n\t   BuyNow BuyNowAuth StoreBuyNow PutBidAuth PutBid StoreBid PutCommentAuth PutComment StoreComment\n\nall_ejb_servlets_sources =  $(addprefix edu/rice/rubbos/beans/servlets/, $(addsuffix .java, $(EJB_Servlets)))\nall_ejb_servlets_obj = $(addprefix edu/r");
  echo("ice/rubbos/beans/servlets/, $(addsuffix .class, $(EJB_Servlets)))\n\nejb_servlets: $(all_ejb_servlets_obj)\n\nclean_ejb_servlets:\n\trm -f edu/rice/rubbos/beans/servlets/*.class\n\n#########################\n#    Servlets version   #\n#########################\n\nServlets = ServletPrinter Config TimeManagement BrowseCategories Auth RegisterUser RubbosHttpServlet \\\n\tBrowseRegions SearchItemsByCategory SearchIt");
  echo("emsByRegion ViewItem ViewBidHistory \\\n\tViewUserInfo SellItemForm RegisterItem PutCommentAuth PutComment StoreComment \\\n\tBuyNowAuth BuyNow StoreBuyNow PutBidAuth PutBid StoreBid AboutMe \\\n\nall_servlets_sources =  $(addprefix edu/rice/rubbos/servlets/, $(addsuffix .java, $(Servlets)))\nall_servlets_obj = $(addprefix edu/rice/rubbos/servlets/, $(addsuffix .class, $(Servlets)))\n\nservlets: $(all_servlet");
  echo("s_obj)\n\nclean_servlets:\n\trm -f edu/rice/rubbos/servlets/*.class\n\n####################\n#       Client     #\n####################\n\n# This is a merge of the provided Client/Makefile and Makefile.\n\nClientFiles = URLGenerator URLGeneratorServlets URLGeneratorPHP RUBBoSProperties Stats \\\n\t      TransitionTable ClientEmulator UserSession  ../beans/TimeManagement\n\nall_client_sources =  $(addprefix edu/ric");
  echo("e/rubbos/client/, $(addsuffix .java, $(ClientFiles)))\nall_client_obj = $(addprefix edu/rice/rubbos/client/, $(addsuffix .class, $(ClientFiles)))\n\nclient: $(all_client_obj)\n\t${JAR} cvf rubbos_client.jar $(all_client_obj)\n\nemulator:\n\t${JAVA} -classpath . edu.rice.rubbos.client.ClientEmulator\n# from Client makefile\n#\t${JAVA} -Xmx256m -Xms128m -server -classpath . edu.rice.rubbos.client.ClientEmulator");
  echo("\n\n\n############################\n#       Global rules       #\n############################\n\n\nall: beans ejb_servlets client javadoc flush_cache\n\nworld: all servlets\n\njavadoc :\n\t${JAVADOC} -d ./doc/api -bootclasspath ${CLASSPATH} -version -author -windowtitle \"RUBBoS API\" -header \"<b>RUBBoS (C)2001 Rice University/INRIA</b><br>\" edu.rice.rubbos.beans edu.rice.rubbos.beans.servlets edu.rice.rubbos.clien");
  echo("t\n\nclean:\n\trm -f core edu/rice/rubbos/beans/*.class edu/rice/rubbos/beans/JOnAS* edu/rice/rubbos/beans/servlets/*.class edu/rice/rubbos/client/*.class edu/rice/rubbos/servlets/*.class\n\n%.class: %.java\n\t${JAVACC} -classpath ${CLASSPATH} $<\n\nflush_cache: bench/flush_cache.c\n\tgcc bench/flush_cache.c -o bench/flush_cache\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
