
#include <php/phc-test/subjects/benchmarks/RUBBoS/HOWTO.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$HOWTO(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/HOWTO);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$HOWTO;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Step described below are marked with a * if they are required for code provided\nwith phc. Other steps are documentation for changes made from the original\n1.2.2 code RUBBoS code release.\n\nVersion 1.2.2 from http://jmob.objectweb.org/rubbos.html doesn't work well for\nPHP. However, there seems to be a lot of changes since 1.0, so its best to port\nchanges forward, rather than backwards (since the dat");
  echo("a is for 1.2.2).\n\nNote that this is designed for PHP4. It seems to work out of the box with PHP5.\n\nThe following packages are used (debian/ubuntu):\n\tapache2\n\tlibapache2-mod-php5\n\tphp5-mysql\n\tmysql-server-5.0\n\tmysql-client-5.0\n\ttcsh (to run bench/rubbos.sh)\n\tjava\n\tjavac\n\tsysstat (for sar)\n\tmake \n\tgcc (for flush_cache)\n\n\nThe following webpages provide documentation:\n\n\thttp://www.cs.rice.edu/CS/Syste");
  echo("ms/DynaServer/\n\t\t- Contains release 1.0, which includes the docs.\n\n\thttp://www.cs.rice.edu/CS/Systems/DynaServer/dyna_bottleneck.pdf\n\t\t- Section 4 describes benchmarks utilization.\n\t\t- There is another paper of this ilk.\n\n\n\nHow to run the benchmark:\n\n\nStep 0 - get the benchmark and docs:\n\n\tDownload v1.2.2 from:\n\t\thttp://jmob.objectweb.org/rubbos.html\n\n\tDownload v1.0 from:\n\t\thttp://www.cs.rice.edu/");
  echo("CS/Systems/DynaServer/RUBBoS/download.html\n\n\tWe use 1.2.2, and port some changes from 1.0 where 1.2.2 breaks.\n\n\tThe docs are in the doc folder of the 1.0 download.\n\n\n\nStep 1 - get the webserver running:\n\n*\tI put a symlink to the RUBBos directory from within ~/public_html, and access it at\n\thttp://localhost/~me/RUBBoS/\n\n*\tapache needs the php module, and PHP needs the mysql module. On ubuntu:\n\t\t$ s");
  echo("udo a2enmod userdir\n\t\t$ sudo a2enmod php5\n\n\tNone of the links work in this config, so they need to be patched. In the PHP/ directory:\n\t\t$ sed -i 's!/PHP/!!g' *.php\n\t\t$ sed -i 's!/PHP/!!g' *.html\n\n\tMake minor changes to the server scripts:\n\t\t- TODO prepare a diff\n\n\n\nStep 2 - Set up the databases\n\n*\tDownload the database:\n\t\t- http://download.forge.objectweb.org/rubbos/rubbos-expanded-dataset.tar.bz2");
  echo("\n\n*\tCreate a user for the database\n\t\t- I created 'rubbos' with no password (I did this with phpmyadmin, not sure how to do\n\t\t  otherwise).\n\n*\tThe database directory has 3 files to set up the DB:\n\t\tCreate the tables and the DB.\n\t\t\t- I created a DB rubbos when I created the user, so didn't need the first line of rubbos.sql.\n\t\t\t$ mysql -u rubbos < rubbos.sql\n\n*\t\tLoad the data from the downloaded, unzi");
  echo("pped rubbos-expanded-dataset into the database.\n\t\t\t- Need top change the filenames before executing.\n\t\t\t$ mysql -u rubbos rubbos < load.sql\n\t\t\t\t(I had a problem with this, so had to use\n\t\t\t\t\t$ mysql -u root rubbos < load.sql )\n\n*\t\t- test.sql\n\t\t\t$ mysql -u rubbos rubbos < test.sql\n\n\tChange the PHP scripts for your user:\n\t\t- On line 5 of PHPprinter.php, change 'cecchet' to 'rubbos'\n\n\n\nStep 3 - Build th");
  echo("e client\n\n\tMove edu/ to make the Makefiles work. From the base directory:\n\t\t$ mv Client/edu .\n\n\tMake minor changes to client files:\n\t\t- TODO: prepare a diff\n\n\tMerge the Makefile:\n\t\t- TODO: prepare a diff with version\n\t\t- InitDB can be ignored, that comes from RUBis\n\n*\tMake the client:\n\t\t$ make client\n\n*\tDon't use 'make emulator'. That gets run by bench/rubbos.sh. However, do\n\tmake sure it runs (ig");
  echo("nore errors about missing rubbos.properties. This is\n\tcopied by the test script.\n\n\n\nStep 4 - Ready the test\n\n*\tSetup the workload files:\n\t\t- edit each of bench/rubbos_properties_* for your config.\n\n*\tAdd extra properties files:\n\t\t- the tar.gz only comes with _100 to _500.\n\t\t- TODO: copy figures from v1.0\n\n\tFix flush_cache:\n\t\t- Copy flush_cache.c from RUBiS (separate download).\n*\t\t$ make flush_cach");
  echo("e\n\n\tEdit bench/rubbos.sh\n*\t\t- Fix the rsh lines. Set up keys for ssh to web server and/or database server.\n\t\t- TODO: currently testing on local machine\n\t\t- flush_cache is in bench/. Change references to flush_cache to bench/flush_cache\n\n\n\nStep 5 - Run the test with PHP\n\n*\tClear the database of benchmark data:\n\t\t- Tidy is an added script to clear any data created after 2008.\n\t\t$ mysql -u rubbos rub");
  echo("bos < database/tidy.sql\n\n*\tRun the benchmark script from bench/\n\t\t$ ./rubbos.sh\n\n\n\nStep 6 - Run the test with phc.\n\n*\tCompile the application:\n\t\t$ TODO\n\n*\tClear the database of benchmark data:\n\t\t- See step 5\n\n*\tRun the benchmark script from bench/\n\t\t$ ./rubbos.sh\n\n\n\nTODO change relative database dict and workload user to absolute\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
