
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_workload_user_default_no_search_transitions_txt_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_workload_user_default_no_search_transitions_txt_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/workload/user_default_no_search_transitions.txt.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$workload$user_default_no_search_transitions_txt(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_workload_user_default_no_search_transitions_txt_nophp_h__
