
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$workload$user_default_no_search_transitions_txt(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_INCLUDE(0x6A6734E651C5E58DLL, "phc-test/subjects/benchmarks/RUBBoS/workload/user_default_no_search_transitions.txt", php$phc_test$subjects$benchmarks$RUBBoS$workload$user_default_no_search_transitions_txt);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$phc_test$subjects$benchmarks$RUBBoS$workload$user_default_no_search_transitions_txt(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
