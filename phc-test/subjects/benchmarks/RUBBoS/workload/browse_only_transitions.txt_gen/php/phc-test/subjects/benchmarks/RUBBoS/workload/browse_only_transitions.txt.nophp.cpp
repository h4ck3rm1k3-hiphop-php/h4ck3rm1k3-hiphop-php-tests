
#include <php/phc-test/subjects/benchmarks/RUBBoS/workload/browse_only_transitions.txt.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$workload$browse_only_transitions_txt(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/workload/browse_only_transitions.txt);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$workload$browse_only_transitions_txt;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("RUBBoS Transition Table\tBrowse only transition set\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\"To >>>\nFrom vvvv  \"\tStories of the day\tReg.\tReg. User\tBrowse\tBr. Cat.\tBr Stories Cat.\tOlder stories\tView Story\tPost com\tStore com\tView com\tMod com\tStore mod log\tSubmit story\tStore story\tSearch\tSearch stories\tSearch com\tSearch users\tAuthor login\tAuthor task\tReview stories\tAccept story\tReject stor");
  echo("y\tTransition waiting time\r\nStoriesOfTheDay\t0\t0\t0\t0.19\t0\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t0.1\t7000\r\nRegister\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nRegisterUser\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nBrowse\t0.15\t0\t0\t0\t0.005\t0.005\t0.005\t0.005\t0.005\t0.5\t0.005\t0.005\t0.5\t0.005\t0.5\t0.005\t0.005\t0.005\t0.005\t0.005\t0.005\t0.005\t0.5\t0.5\t7000");
  echo("\r\nBrowseCategories\t0\t0\t0\t0.4\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nBrowseStoriesInCategory\t0\t0\t0\t0\t0.99\t0.3\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nOlderStories\t0\t0\t0\t0.4\t0\t0\t0.35\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nViewStory\t0.6\t0\t0\t0\t0\t0.44\t0.49\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.49\t0\t0.49\t0\t0\t0\t0\t0\t7000\r\nPostComment\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nStoreComment\t0\t0\t0\t0\t0\t0\t0");
  echo("\t0\t0.99\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nViewComment\t0\t0\t0\t0\t0\t0\t0\t0.49\t0\t0\t0.3\t0\t0\t0\t0\t0\t0\t0.49\t0\t0\t0\t0\t0\t0\t7000\r\nModerateComment\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nStoreModerateLog\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.89\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nSubmitStory\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nStoreStory\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.89\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t7000\r\nSear");
  echo("ch\t0.15\t0\t0\t0.005\t0.005\t0.005\t0.005\t0.005\t0.005\t0.5\t0.005\t0.005\t0.5\t0.005\t0.5\t0\t0.005\t0.005\t0.005\t0.005\t0.005\t0.005\t0.5\t0.5\t7000\r\nSearchInStories\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.5\t0.3\t0\t0\t0\t0\t0\t0\t0\t7000\r\nSearchInComments\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.3\t0\t0.3\t0\t0\t0\t0\t0\t0\t7000\r\nSearchInUsers\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.09\t0\t0\t0.3\t0\t0\t0\t0\t0\t7000\r\nAuthor (login)\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0");
  echo("\t0\t0\t0\t0\t0\t0\t0\t7000\r\nAuthor (task)\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.99\t0\t0\t0\t0\t7000\r\nReviewStories\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.99\t0\t0\t0\t7000\r\nAcceptStory\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.5\t0\t0\t7000\r\nRejectStory\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0.49\t0\t0\t7000\r\nBack probability\t0\t0\t0\t0\t0\t0.1\t0\t0.3\t0\t0\t0.5\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\r\nEnd of Session\t0.1\t0\t0.1\t0");
  echo("\t0\t0.05\t0.05\t0.1\t0\t0.1\t0.1\t0\t0.1\t0\t0.1\t0\t0.1\t0.1\t0.1\t0\t0\t0.1\t0.1\t0.1\t0\r\nHeader probability\t0.3\t0.01\t1\t0.01\t0.01\t0.01\t0.01\t0.01\t0.01\t1\t0.01\t0.01\t1\t0.01\t1\t0.01\t0.01\t0.01\t0.01\t0.01\t0.01\t0.01\t1\t1\t\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\nProbabilities for header\t\t\t\t\t\r\nRegisterProb\t0\t\t\t0\tValue that is automatically computed\r\nBrowseProb\t0.5\t\t\t0\tValue that has to be set manually\r\nSubmitStoryProb\t0\t\t\t0\tState where w");
  echo("e can potentially go back (value must be set to 0)\r\nSearchProb\t0.5\t\t\t\t\r\nAuthorProb\t0\t\t\t\t\r\nTotal\t1\t\t\t\t\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
