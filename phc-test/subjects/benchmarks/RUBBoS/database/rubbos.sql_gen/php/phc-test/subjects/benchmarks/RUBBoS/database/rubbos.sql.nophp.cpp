
#include <php/phc-test/subjects/benchmarks/RUBBoS/database/rubbos.sql.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$database$rubbos_sql(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/database/rubbos.sql);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$database$rubbos_sql;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("CREATE DATABASE rubbos;\nconnect rubbos;\n\nCREATE TABLE categories (\n   id INTEGER UNSIGNED NOT NULL UNIQUE,\n   name VARCHAR(50),\n   PRIMARY KEY(id)\n);\n\nCREATE TABLE users (\n   id            INTEGER UNSIGNED UNIQUE AUTO_INCREMENT,\n   firstname     VARCHAR(20),\n   lastname      VARCHAR(20),\n   nickname      VARCHAR(20) NOT NULL UNIQUE,\n   password      VARCHAR(20) NOT NULL,\n   email         VARCHAR(5");
  echo("0) NOT NULL,\n   rating        INTEGER,\n   access        INTEGER,\n   creation_date DATETIME,\n   PRIMARY KEY(id),\n   INDEX auth (nickname,password),\n   INDEX search (nickname)\n);\n\nINSERT INTO users VALUES (0, \"Anonymous\", \"Coward\", \"Anonymous Coward\", \"+_)(*&^%$#@!\", \"anonymous@rubbos.com\", 0, 0, NOW());\nUPDATE users SET id=0 WHERE nickname=\"Anonymous Coward\";\n\nCREATE TABLE stories (\n   id          ");
  echo("  INTEGER UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,\n   title         VARCHAR(100),\n   body          TEXT,\n   date          DATETIME,\n   writer        INTEGER UNSIGNED NOT NULL,\n   category      INTEGER UNSIGNED NOT NULL,\n   PRIMARY KEY(id),\n   INDEX writer_id (writer),\n   INDEX category_id (category),\n   INDEX search (title(10)),\n   INDEX day (date)\n);\n\nCREATE TABLE old_stories (\n   id            I");
  echo("NTEGER UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,\n   title         VARCHAR(100),\n   body          TEXT,\n   date          DATETIME,\n   writer        INTEGER UNSIGNED NOT NULL,\n   category      INTEGER UNSIGNED NOT NULL,\n   PRIMARY KEY(id),\n   INDEX writer_id (writer),\n   INDEX category_id (category),\n   INDEX search (title(10)),\n   INDEX day (date)\n);\n\nCREATE TABLE submissions (\n   id            INTE");
  echo("GER UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,\n   title         VARCHAR(100),\n   body          TEXT,\n   date          DATETIME,\n   writer        INTEGER UNSIGNED,\n   category      INTEGER UNSIGNED NOT NULL,\n   PRIMARY KEY(id),\n   INDEX writer_id (writer),\n   INDEX category_id (category),\n   INDEX day (date)\n);\n\nCREATE TABLE comments (\n   id           INTEGER UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,\n");
  echo("   writer       INTEGER UNSIGNED,\n   story_id     INTEGER UNSIGNED NOT NULL,\n   parent       INTEGER UNSIGNED,\n   childs       INTEGER UNSIGNED,\n   rating       INTEGER,\n   date         DATETIME,\n   subject      VARCHAR(100),\n   comment      TEXT,\n   PRIMARY KEY(id),\n   INDEX story (story_id),\n   INDEX search (subject(10)),\n   INDEX parent_id (parent),\n   INDEX rating_num (rating)\n);\n\n\nCREATE TABL");
  echo("E old_comments (\n   id           INTEGER UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,\n   writer       INTEGER UNSIGNED,\n   story_id     INTEGER UNSIGNED NOT NULL,\n   parent       INTEGER UNSIGNED,\n   childs       INTEGER UNSIGNED,\n   rating       INTEGER,\n   date         DATETIME,\n   subject      VARCHAR(100),\n   comment      TEXT,\n   PRIMARY KEY(id),\n   INDEX story (story_id),\n   INDEX search (subjec");
  echo("t(10)),\n   INDEX parent_id (parent),\n   INDEX rating_num (rating)\n);\n\nCREATE TABLE moderator_log (\n   id           INTEGER UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,\n   moderator_id INTEGER UNSIGNED NOT NULL,\n   comment_id   INTEGER UNSIGNED NOT NULL,\n   rating       INTEGER,\n   date         DATETIME,\n   PRIMARY KEY(id),\n   INDEX comment (comment_id)\n);\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
