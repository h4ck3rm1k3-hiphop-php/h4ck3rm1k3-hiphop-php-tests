
#include <php/phc-test/subjects/benchmarks/RUBBoS/database/load.sql.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$database$load_sql(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/database/load.sql);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$database$load_sql;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("load data infile \"/home/cecchet/RUBBoS/database/users.data\" into table users fields terminated by \"\\t\";\nload data infile \"/home/cecchet/RUBBoS/database/stories.data\" into table stories fields terminated by \"\\t\";\nload data infile \"/home/cecchet/RUBBoS/database/comments.data\" into table comments fields terminated by \"\\t\";\nload data infile \"/home/cecchet/RUBBoS/database/old_stories.data\" into table o");
  echo("ld_stories fields terminated by \"\\t\";\nload data infile \"/home/cecchet/RUBBoS/database/old_comments.data\" into table old_comments fields terminated by \"\\t\";\nload data infile \"/home/cecchet/RUBBoS/database/submissions.data\" into table submissions fields terminated by \"\\t\";\nload data infile \"/home/cecchet/RUBBoS/database/moderator_log.data\" into table moderator_log fields terminated by \"\\t\";\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
