
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$database$test_sql(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_INCLUDE(0x37EECE180FCB5835LL, "phc-test/subjects/benchmarks/RUBBoS/database/test.sql", php$phc_test$subjects$benchmarks$RUBBoS$database$test_sql);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$phc_test$subjects$benchmarks$RUBBoS$database$test_sql(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
