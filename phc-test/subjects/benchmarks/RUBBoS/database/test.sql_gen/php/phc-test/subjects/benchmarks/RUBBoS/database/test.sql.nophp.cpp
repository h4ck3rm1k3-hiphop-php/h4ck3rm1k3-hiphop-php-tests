
#include <php/phc-test/subjects/benchmarks/RUBBoS/database/test.sql.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$database$test_sql(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/database/test.sql);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$database$test_sql;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("INSERT INTO categories VALUES (0,'AMD');\nINSERT INTO categories VALUES (1,'America Online');\nINSERT INTO categories VALUES (2,'Amiga');\nINSERT INTO categories VALUES (3,'Anime');\nINSERT INTO categories VALUES (4,'Announcements');\nINSERT INTO categories VALUES (5,'Apache');\nINSERT INTO categories VALUES (6,'Apple');\nINSERT INTO categories VALUES (7,'Be');\nINSERT INTO categories VALUES (8,'Beanies')");
  echo(";\nINSERT INTO categories VALUES (9,'BSD');\nINSERT INTO categories VALUES (10,'Bug');\nINSERT INTO categories VALUES (11,'Caldera');\nINSERT INTO categories VALUES (12,'CDA');\nINSERT INTO categories VALUES (13,'Censorship');\nINSERT INTO categories VALUES (14,'Christmas Cheer');\nINSERT INTO categories VALUES (15,'Comdex');\nINSERT INTO categories VALUES (16,'Compaq');\nINSERT INTO categories VALUES (17,");
  echo("'Corel');\nINSERT INTO categories VALUES (18,'Debian');\nINSERT INTO categories VALUES (19,'Digital');\nINSERT INTO categories VALUES (20,'Editorial');\nINSERT INTO categories VALUES (21,'Education');\nINSERT INTO categories VALUES (22,'Encryption');\nINSERT INTO categories VALUES (23,'Enlightenment');\nINSERT INTO categories VALUES (24,'ePlus');\nINSERT INTO categories VALUES (25,'Games');\nINSERT INTO ca");
  echo("tegories VALUES (26,'GNOME');\nINSERT INTO categories VALUES (27,'GNU is Not Unix');\nINSERT INTO categories VALUES (28,'GNUStep');\nINSERT INTO categories VALUES (29,'Graphics');\nINSERT INTO categories VALUES (30,'Handhelds');\nINSERT INTO categories VALUES (31,'Hardware');\nINSERT INTO categories VALUES (32,'IBM');\nINSERT INTO categories VALUES (33,'Intel');\nINSERT INTO categories VALUES (34,'Interne");
  echo("t Explorer');\nINSERT INTO categories VALUES (35,'It\\'s funny.  Laugh.');\nINSERT INTO categories VALUES (36,'Java');\nINSERT INTO categories VALUES (37,'KDE');\nINSERT INTO categories VALUES (38,'Links');\nINSERT INTO categories VALUES (39,'Linux');\nINSERT INTO categories VALUES (40,'Linux Business');\nINSERT INTO categories VALUES (41,'Linux Mandrake');\nINSERT INTO categories VALUES (42,'Linuxcare');\n");
  echo("INSERT INTO categories VALUES (43,'Microsoft');\nINSERT INTO categories VALUES (44,'Movies');\nINSERT INTO categories VALUES (45,'Mozilla');\nINSERT INTO categories VALUES (46,'Music');\nINSERT INTO categories VALUES (47,'Netscape');\nINSERT INTO categories VALUES (48,'News');\nINSERT INTO categories VALUES (49,'Patents');\nINSERT INTO categories VALUES (50,'Perl');\nINSERT INTO categories VALUES (51,'PHP");
  echo("');\nINSERT INTO categories VALUES (52,'Privacy');\nINSERT INTO categories VALUES (53,'Programming');\nINSERT INTO categories VALUES (54,'Quake');\nINSERT INTO categories VALUES (55,'Quickies');\nINSERT INTO categories VALUES (56,'Red Hat Software');\nINSERT INTO categories VALUES (57,'Science');\nINSERT INTO categories VALUES (58,'Security');\nINSERT INTO categories VALUES (59,'Silicon Graphics');\nINSERT");
  echo(" INTO categories VALUES (60,'Slashback');\nINSERT INTO categories VALUES (61,'Slashdot.org');\nINSERT INTO categories VALUES (62,'Space');\nINSERT INTO categories VALUES (63,'Spam');\nINSERT INTO categories VALUES (64,'Star Wars Prequels');\nINSERT INTO categories VALUES (65,'Sun Microsystems');\nINSERT INTO categories VALUES (66,'SuSE');\nINSERT INTO categories VALUES (67,'Technology');\nINSERT INTO cate");
  echo("gories VALUES (68,'Television');\nINSERT INTO categories VALUES (69,'The Almighty Buck');\nINSERT INTO categories VALUES (70,'The Courts');\nINSERT INTO categories VALUES (71,'The Gimp');\nINSERT INTO categories VALUES (72,'The Internet');\nINSERT INTO categories VALUES (73,'The Media');\nINSERT INTO categories VALUES (74,'Toys');\nINSERT INTO categories VALUES (75,'Transmeta');\nINSERT INTO categories VA");
  echo("LUES (76,'TurboLinux');\nINSERT INTO categories VALUES (77,'United States');\nINSERT INTO categories VALUES (78,'Unix');\nINSERT INTO categories VALUES (79,'Upgrades');\nINSERT INTO categories VALUES (80,'User Journal');\nINSERT INTO categories VALUES (81,'VA');\nINSERT INTO categories VALUES (82,'Wine');\nINSERT INTO categories VALUES (83,'X');\nINSERT INTO categories VALUES (84,'Ximian');\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
