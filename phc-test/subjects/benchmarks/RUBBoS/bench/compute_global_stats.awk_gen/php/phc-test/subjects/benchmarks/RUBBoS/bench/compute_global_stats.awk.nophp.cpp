
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/compute_global_stats.awk.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$compute_global_stats_awk(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/bench/compute_global_stats.awk);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$compute_global_stats_awk;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("#!/usr/bin/awk -f\n\n# Caller must set nbscript and path values on the command line.\n# Example: compute_global_stats.awk -v path=2001-10-10@14:55:23/ -v nbscript=2 2001-10-10@14:55:23/stat_client0.html\n\nBEGIN {\n  printf \"Computing global stats for \"nbscript\" scripts in \"path\" ...\";\n  outputFile = path\"perf.html\";\n  footer = 0;\n  headerRow = 0;\n  for (i = 1 ; i < nbscript ; i++)\n    scriptName[i] = pa");
  echo("th\"stat_client\"i\".html\";\n}\n{\n  # Read each line from all files to be synchronized between them\n  line[0] = $0;\n  for (i = 1 ; i < nbscript ; i++)\n    getline line[i] < scriptName[i];\n\n  # Compute the global results for the statistics of each ramp\n  if (headerRow == 1)\n    { \n      if ($0 ~ \"<TR><TD><div align=left><B>Total</B></div>\")\n\t{ # This is the total line, compute the percentages and print the tabl");
  echo("e\n\t  totalCount = 0;\n\t  totalErrors = 0;\n\t  totalTime = 0;\n\t  for (i = 0 ; i < nbscript ; i++)\n\t    {\n\t      split(line[i], splited, \"</div><TD>\");\n\t      gsub(/<div align=right>|<B>|<\\/B>/, \"\", splited[3]); # Get rid of the '<div align=right><B>' and </B>\n\t      totalCount += splited[3];\n\t      gsub(/<div align=right>|<B>|<\\/B>/, \"\", splited[4]); # Get rid of the '<div align=right><B>' and </B>\n\t      totalErro");
  echo("rs += splited[4];\n\t    }\n\t  if (totalCount == 0)\n\t    totalCount = 1000000000000000000000000; # make totalCount infinite so that division result will give 0.\n\t  for (i = 0 ; (i < stateNb) && (stateNb != 0); i++)\n\t    {\n\t      if (count[i] != 0)\n\t\t{\n\t\t  printf \"%s</div><TD><div align=right>%.2f %%</div><TD><div align=right>%d</div><TD><div align=right>%d</div><TD><div align=right>%d ms</div><TD><div align=right>%d");
  echo(" ms</div><TD><div align=right>%.0f ms</div>\\n\", stateName[i], 100*count[i]/totalCount, count[i], errors[i], minTime[i], maxTime[i], avgTime[i]/stateNb >> outputFile;\n\t\t  totalTime += avgTime[i];\n\t\t}\n\t    }\n\t  # Display the total\n\t  if (stateNb == 0)\n\t    stateNb = 1000000000000000000000000; # make stateNb infinite so that division result will give 0.\n\t  printf \"<TR><TD><div align=left><B>Total</div></B><TD><d");
  echo("iv align=right><B>100 %</B></div><TD><div align=right><B>%d</B></div><TD><div align=right><B>%d</B></div><TD><div align=center>-</div><TD><div align=center>-</div><TD><div align=right><B>%.0f ms</B></div>\", totalCount, totalErrors, totalTime/totalCount >> outputFile;\n\t}\n      else if ($0 ~ \"<TR><TD><div align=left><B>Average throughput</div>\")\n\t{ # Average throughput\n\t  throughput = 0;\n\t  for (i = 0 ; i < nbscript ; i++)\n\t    ");
  echo("{\n\t      split(line[i], splited, /<B>|<\\/B>/);\n\t      gsub(/ req\\/s/, \"\", splited[4]); # Get rid of the '< req/s>'\n\t      throughput += splited[4];\n\t    }\n\t  print \"<TR><TD><div align=left><B>Average throughput</div></B><TD colspan=6><div align=center><B>\"throughput\" req/s</B></div>\" >> outputFile;\n\t}\n      else if ($0 ~ \"<TR><TD><div align=left>Completed sessions</div>\")\n\t{ # Completed sessions\n\t  sessions = 0;\n\t ");
  echo(" for (i = 0 ; i < nbscript ; i++)\n\t    {\n\t      split(line[i], splited, /<div align=left>/);\n\t      gsub(/<\\/div>/, \"\", splited[3]); # Get rid of the '</div>'\n\t      sessions += splited[3];\n\t    }\n\t  print \"<TR><TD><div align=left>Completed sessions</div><TD colspan=6><div align=left>\"sessions\"</div>\" >> outputFile;\n\t}\n      else if ($0 ~ \"<TR><TD><div align=left>Total time</div>\")\n\t{ # Total time\n\t  time = 0;\n\t");
  echo("  for (i = 0 ; i < nbscript ; i++)\n\t    {\n\t      split(line[i], splited, /<div align=left>/);\n\t      gsub(/ seconds<\\/div>/, \"\", splited[3]); # Get rid of the '</div>'\n\t      time += splited[3];\n\t    }\n\t  print \"<TR><TD><div align=left>Total time</div><TD colspan=6><div align=left>\"time\" seconds</div>\" >> outputFile;\n\t}\n      else if ($0 ~ \"<TR><TD><div align=left><B>Average session time</div>\")\n\t{ # Average sess");
  echo("ion time is the last stat of the table, we close the table here.\n\t  if (time == 0)\n\t    print \"<TR><TD><div align=left><B>Average session time</div></B><TD colspan=6><div align=left><B>0 second</B></div>\" >> outputFile;\n\t  else\n\t    print \"<TR><TD><div align=left><B>Average session time</div></B><TD colspan=6><div align=left><B>\"time/sessions\" seconds</B></div>\" >> outputFile;\n\t  print \"</TABLE><p>\" >> outputFile;\n\t  hea");
  echo("derRow = 0;\n\t}\n     else if ($0 ~ \"align=right>\")\n\t{ # We have to process a stat line, let's go !\n\t  count[stateNb]   = 0;\n\t  errors[stateNb]  = 0;\n\t  minTime[stateNb] = 1000000000000000;\n\t  maxTime[stateNb] = 0;\n\t  avgTime[stateNb] = 0;\n\t  for (i = 0 ; i < nbscript ; i++)\n\t    {\n\t      split(line[i], splited, \"</div><TD><div align=right>\");\n\t      if (i == 0)\n\t\tstateName[stateNb] = splited[1];\n\t     ");
  echo(" else if (splited[1] != stateName[stateNb])\n\t\tprint \"Error line \"NR\" in \"path\"stat_client\"i\".html: Bad state '\"splited[1]\"' does not match '\"stateName[stateNb]\"'\";\n\t      # Skip percentage in splited[2], we'll have to compute that later\n\t      count[stateNb]    += splited[3];\n\t      gsub(/<B>|<\\/B>/, \"\", splited[4]); # Get rid of the '<B>' and </B>\n\t      errors[stateNb]   += splited[4];\n\t      sub(/ ");
  echo("ms/, \"\", splited[5]); # Get rid of the ' ms'\n\t      if (minTime[stateNb] > splited[5])\n\t\tminTime[stateNb] = splited[5];\n\t      sub(/ ms/, \"\", splited[6]); # Get rid of the ' ms'\n\t      if (maxTime[stateNb] < splited[6])\n\t\tmaxTime[stateNb] = splited[6];\n\t      sub(/ ms<\\/div>/, \"\", splited[7]); # Get rid of the ' ms</div>'\n\t      avgTime[stateNb]  += splited[7];\n\t    }\n\t  stateNb++;\n\t}\n    }\n\n  # Prin");
  echo("t the headers for ramps\n  if ($0 ~ /stat\\\"><\\/A>|statistics<\\/h3><p>/)\n    {\n      print $0 >> outputFile;\n      testTiming = 0;\n    }\n\n  # Print a summary of 'Test timing information' for all clients\n  if ($0 ~ \"<A NAME=\\\"time\\\"></A>\")\n    testTiming = 1;\n\n  if (testTiming == 1)\n    {\n      if ($0 ~ \"<TD>\")\n\t{\n\t  printf $0 >> outputFile;\n\t  for (i = 1 ; i < nbscript ; i++)\n\t    {\n\t      split(line[i], s");
  echo("plited, \"<TD>\");\n\t      printf \"<TD>\"splited[3] >> outputFile;\n\t    }\n\t  print \"\" >> outputFile;\n\t}\n      else\n\tprint $0 >> outputFile;\n      if ($0 ~ \"<TABLE\")\n\t{\n\t  printf \"<THEAD><TR><TH>Phase<TH>Main client\" >> outputFile;\n\t  for (i = 1 ; i < nbscript ; i++)\n\t    printf \"<TH>Remote client \"i >> outputFile;\n\t  print \"<TBODY>\" >> outputFile;\n\t}\n    }\n\n  if ($0 ~ /State name/)\n    { # Table header recogniz");
  echo("ed !\n      headerRow = 1;\n      stateNb = 0;\n      print \"<TABLE BORDER=1>\" >> outputFile;\n      print $0 >> outputFile;\n    }\n\n  # Recopy the graph part which is common to every stat page\n  if ($0 ~ /\"cpu_graph\"/)\n    footer = 1;\n\n  if (footer == 1)\n    print $0 >> outputFile;\n\n}\nEND {\n  print \" Done.\";\n}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
