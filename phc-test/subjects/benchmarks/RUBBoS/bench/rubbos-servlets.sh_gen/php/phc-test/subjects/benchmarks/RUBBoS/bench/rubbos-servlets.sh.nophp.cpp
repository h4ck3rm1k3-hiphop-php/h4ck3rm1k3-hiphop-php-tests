
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/rubbos-servlets.sh.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$rubbos_servlets_sh(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/bench/rubbos-servlets.sh);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$rubbos_servlets_sh;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("#!/bin/tcsh\n\n###############################################################################\n#\n# This script runs first the RUBBoS browsing mix, then the read/write mix \n# for each rubbos.properties_XX specified where XX is the number of emulated\n# clients. Note that the rubbos.properties_XX files must be configured\n# with the corresponding number of clients.\n# In particular set the following vari");
  echo("ables in rubis.properties_XX:\n# httpd_use_version = Servlets\n# workload_number_of_clients_per_node = XX/number of client machines\n# workload_transition_table = yourPath/RUBBoS/workload/transitions.txt \n#\n# This script should be run from the RUBBoS/bench directory on the local \n# client machine. \n# Results will be generated in the RUBBoS/bench directory.\n#\n##########################################");
  echo("######################################\n\nsetenv SERVLETDIR /users/margueri/RUBBoS/Servlets\n\n# Go back to RUBBoS root directory\ncd ..\n\n# Browse only\n\ncp ./workload/browse_only_transitions.txt ./workload/user_transitions.txt\ncp ./workload/browse_only_transitions.txt ./workload/author_transitions.txt\n\nforeach i (rubbos.properties_100 rubbos.properties_200 rubbos.properties_300 rubbos.properties_400 ru");
  echo("bbos.properties_500 rubbos.properties_600 rubbos.properties_700 rubbos.properties_800 rubbos.properties_900 rubbos.properties_1000) \n  cp bench/$i Client/rubbos.properties\n  ssh sci21 -n -l margueri ${SERVLETDIR}/tomcat_stop.sh \n  sleep 4\n  ssh sci21 -n -l margueri ${SERVLETDIR}/tomcat_start.sh &\n  sleep 4\n  bench/flush_cache 490000\n  ssh sci23 RUBBoS/bench/flush_cache 880000 \t# web server\n  ssh s");
  echo("ci22 RUBBoS/bench/flush_cache 880000\t# database\n  ssh sci21 RUBBoS/bench/flush_cache 780000 \t# servlet server\n  ssh sci6 RUBBoS/bench/flush_cache 490000\t# remote client\n  ssh sci7 RUBBoS/bench/flush_cache 490000\t# remote client\n  ssh sci8 RUBBoS/bench/flush_cache 490000\t# remote client\n  make emulator \nend\n\n\n\n# Read/write mix\n\ncp ./workload/user_default_transitions.txt ./workload/user_transitions.");
  echo("txt\ncp ./workload/author_default_transitions.txt ./workload/author_transitions.txt\n\nforeach i (rubbos.properties_100 rubbos.properties_200 rubbos.properties_300 rubbos.properties_400 rubbos.properties_500 rubbos.properties_600 rubbos.properties_700 rubbos.properties_800 rubbos.properties_900 rubbos.properties_1000) \n  cp bench/$i Client/rubbos.properties\n  ssh sci21 -n -l margueri ${SERVLETDIR}/to");
  echo("mcat_stop.sh \n  sleep 4\n  ssh sci21 -n -l margueri ${SERVLETDIR}/tomcat_start.sh &\n  sleep 4\n  bench/flush_cache 490000\n  ssh sci23 RUBBoS/bench/flush_cache 880000 \t# web server\n  ssh sci22 RUBBoS/bench/flush_cache 880000\t# database\n  ssh sci21 RUBBoS/bench/flush_cache 780000 \t# servlet server\n  ssh sci6 RUBBoS/bench/flush_cache 490000\t# remote client\n  ssh sci7 RUBBoS/bench/flush_cache 490000\t# r");
  echo("emote client\n  ssh sci8 RUBBoS/bench/flush_cache 490000\t# remote client\n  make emulator\nend\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
