
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/flush_cache.c.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$flush_cache_c(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/bench/flush_cache.c);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$flush_cache_c;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\nint main(int argc, char *argv[])\n{\n  int size;\n\n  if (argc != 2)\n  {\n    printf(\"Usage: flush_cache size_of_main_memory_in_KB\");\n    exit(0);\n  }\n\n  size = atoi(argv[1]);\n  printf(\"Flushing %u KB of memory ... \", size);\n  fflush(stdout);\n  while (size)\n  {\n    void *p = (void *)malloc(1024);\n    if (!p)\n      break;\n    else\n      size--;\n  }\n  printf(\"Done\\n\");\n}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
