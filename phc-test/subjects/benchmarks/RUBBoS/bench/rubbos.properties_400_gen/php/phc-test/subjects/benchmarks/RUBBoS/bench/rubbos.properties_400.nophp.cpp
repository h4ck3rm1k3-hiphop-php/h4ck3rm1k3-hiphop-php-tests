
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/rubbos.properties_400.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$rubbos_properties_400(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/bench/rubbos.properties_400);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$rubbos_properties_400;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("# HTTP server information\nhttpd_hostname = sci23\nhttpd_port = 80\n\n# Precise which version to use. Valid options are : PHP, Servlets, EJB\nhttpd_use_version = Servlets\n\n# EJB server information\nejb_server =\nejb_html_path =\nejb_script_path =\n\n# Servlets server information\nservlets_server = sci21 \nservlets_html_path = /Servlet_HTML\nservlets_script_path = /servlet\n\n# PHP information\nphp_html_path = /PH");
  echo("P\nphp_script_path = /PHP\n\n#Database information\ndatabase_server = sci22\n\n# Workload: precise which transition table to use\nworkload_remote_client_nodes = sci6\nworkload_remote_client_command = /usr/local/jdk1.3.1/bin/java -classpath RUBBoS edu.rice.rubbos.client.ClientEmulator\nworkload_number_of_clients_per_node = 200\n\nworkload_user_transition_table = /users/cecchet/RUBBoS/workload/user_transitions");
  echo(".txt\nworkload_author_transition_table = /users/cecchet/RUBBoS/workload/author_transitions.txt\nworkload_number_of_columns = 24\nworkload_number_of_rows = 26\nworkload_maximum_number_of_transitions = 1000\nworkload_use_tpcw_think_time = yes\nworkload_number_of_stories_per_page = 20\nworkload_up_ramp_time_in_ms = 150000\nworkload_up_ramp_slowdown_factor = 2\nworkload_session_run_time_in_ms = 900000\nworkload");
  echo("_down_ramp_time_in_ms = 150000\nworkload_down_ramp_slowdown_factor = 3\nworkload_percentage_of_author = 10\n\n# Users policy\ndatabase_number_of_authors = 50\ndatabase_number_of_users = 500000\n\n# Stories policy\ndatabase_story_dictionnary = /users/cecchet/RUBBoS/database/dictionary\ndatabase_story_maximum_length = 1024\ndatabase_oldest_story_year = 1998\ndatabase_oldest_story_month = 1\n\n# Comments policy\nda");
  echo("tabase_comment_max_length = 1024\n\n\n# Monitoring Information\nmonitoring_debug_level = 0\nmonitoring_program = /usr/bin/sar\nmonitoring_options = -n DEV -n SOCK -rubcw\nmonitoring_sampling_in_seconds = 1\nmonitoring_rsh = /usr/bin/rsh\nmonitoring_gnuplot_terminal = gif");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
