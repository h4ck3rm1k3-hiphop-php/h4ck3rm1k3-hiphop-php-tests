
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_bench_rubbos_properties_300_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_bench_rubbos_properties_300_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/rubbos.properties_300.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$rubbos_properties_300(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_bench_rubbos_properties_300_nophp_h__
