
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/rubbos.sh.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$rubbos_sh(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/bench/rubbos.sh);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$rubbos_sh;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("#!/bin/tcsh\n\n# Go back to RUBBoS root directory\ncd ..\n\n# Browse only\n\ncp ./workload/browse_only_transitions.txt ./workload/user_transitions.txt\ncp ./workload/browse_only_transitions.txt ./workload/author_transitions.txt\n\nforeach i (rubbos.properties_100 rubbos.properties_200 rubbos.properties_300 rubbos.properties_400 rubbos.properties_500 rubbos.properties_600 rubbos.properties_700 rubbos.propert");
  echo("ies_800 rubbos.properties_900 rubbos.properties_1000) \n  cp bench/$i rubbos.properties\n  flush_cache 490000\n  rsh sci23 RUBBoS/flush_cache 880000 \t# web server\n  rsh sci22 RUBBoS/flush_cache 880000\t# database\n  rsh sci6 RUBBoS/flush_cache 490000\t# remote client\n  rsh sci7 RUBBoS/flush_cache 490000\t# remote client\n  rsh sci8 RUBBoS/flush_cache 490000\t# remote client\n  make emulator \nend\n\n# Default\n");
  echo("\ncp ./workload/user_default_transitions.txt ./workload/user_transitions.txt\ncp ./workload/author_default_transitions.txt ./workload/author_transitions.txt\n\nforeach i (rubbos.properties_100 rubbos.properties_200 rubbos.properties_300 rubbos.properties_400 rubbos.properties_500 rubbos.properties_600 rubbos.properties_700 rubbos.properties_800 rubbos.properties_900 rubbos.properties_1000) \n  cp bench");
  echo("/$i rubbos.properties\n  flush_cache 490000\n  rsh sci23 RUBBoS/flush_cache 880000 \t# web server\n  rsh sci22 RUBBoS/flush_cache 880000\t# database\n  rsh sci6 RUBBoS/flush_cache 490000\t# remote client\n  rsh sci7 RUBBoS/flush_cache 490000\t# remote client\n  rsh sci8 RUBBoS/flush_cache 490000\t# remote client\n  make emulator\nend\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
