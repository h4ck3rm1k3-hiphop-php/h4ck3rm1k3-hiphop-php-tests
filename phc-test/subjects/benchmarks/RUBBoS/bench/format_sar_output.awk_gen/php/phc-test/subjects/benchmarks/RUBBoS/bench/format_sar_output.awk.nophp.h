
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_bench_format_sar_output_awk_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_bench_format_sar_output_awk_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/format_sar_output.awk.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$format_sar_output_awk(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_bench_format_sar_output_awk_nophp_h__
