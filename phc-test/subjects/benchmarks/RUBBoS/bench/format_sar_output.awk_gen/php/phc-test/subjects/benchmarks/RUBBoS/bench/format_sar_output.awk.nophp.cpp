
#include <php/phc-test/subjects/benchmarks/RUBBoS/bench/format_sar_output.awk.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$format_sar_output_awk(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/bench/format_sar_output.awk);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$bench$format_sar_output_awk;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("#!/bin/awk -f\n\nBEGIN {\n  # It is not possible to define output file names here because\n  # FILENAME is not define in the BEGIN section\n  n = \"\";\n  printf \"Generating data files ...\";\n  network_max_bandwidth_in_byte = 10000000;\n  network_max_packet_per_second = 1000000;\n  last3 = 0;\n  last4 = 0;\n  last5 = 0;\n  last6 = 0;\n}\n{\n  if ($1 ~ /Average/)\n    { # Skip the Average values\n      n = \"\";\n      ");
  echo("next;\n    }\n\n  if ($2 ~ /all/)\n    { # This is the cpu info\n      print $3 > FILENAME\".cpu.user.dat\";\n#\t  print $4 > FILENAME\".cpu.nice.dat\";\n      print $5 > FILENAME\".cpu.system.dat\";\n#     print $6 > FILENAME\".cpu.iowait.dat\";\n      print $7 > FILENAME\".cpu.idle.dat\";\n      print 100-$7 > FILENAME\".cpu.busy.dat\";\n    }\n  if ($2 ~ /eth0/)\n    { # This is the eth0 network info\n      if ($3 > netw");
  echo("ork_max_packet_per_second)\n\tprint last3 > FILENAME\".net.rxpck.dat\"; # Total number of packets received per second.\n      else\n\t{\n\t  last3 = $3;\n\t  print $3 > FILENAME\".net.rxpck.dat\"; # Total number of packets received per second.\n\t}\n      if ($4 > network_max_packet_per_second)\n\tprint last4 > FILENAME\".net.txpck.dat\"; # Total number of packets transmitted per second.\n      else\n\t{\n\t  last4 = $4;\n");
  echo("\t  print $4 > FILENAME\".net.txpck.dat\"; # Total number of packets transmitted per second.\n\t}\n      if ($5 > network_max_bandwidth_in_byte)\n\tprint last5 > FILENAME\".net.rxbyt.dat\"; # Total number of bytes received per second.\n      else\n\t{\n\t  last5 = $5;\n\t  print $5 > FILENAME\".net.rxbyt.dat\"; # Total number of bytes received per second.\n\t}\n      if ($6 > network_max_bandwidth_in_byte)\n\tprint last6");
  echo(" > FILENAME\".net.txbyt.dat\"; # Total number of bytes transmitted per second.\n      else\n\t{\n\t  last6 = $6;\n\t  print $6 > FILENAME\".net.txbyt.dat\"; # Total number of bytes transmitted per second.\n\t}\n#     print $7 > FILENAME\".net.rxcmp.dat\"; # Number of compressed packets received per second (for cslip etc.).\n#     print $8 > FILENAME\".net.txcmp.dat\"; # Number of compressed packets transmitted per s");
  echo("econd.\n#     print $9 > FILENAME\".net.rxmcst.dat\"; # Number of multicast packets received per second.\n    }\n\n  # Detect which is the next info to be parsed\n  if ($2 ~ /proc|cswch|tps|kbmemfree|totsck/)\n    {\n      n = $2;\n    }\n\n  # Only get lines with numbers (real data !)\n  if ($2 ~ /[0-9]/)\n    {\n      if (n == \"proc/s\")\n\t{ # This is the proc/s info\n\t  print $2 > FILENAME\".proc.dat\";\n#\t  n = \"\"");
  echo(";\n\t}\n      if (n == \"cswch/s\")\n\t{ # This is the context switches per second info\n\t  print $2 > FILENAME\".ctxsw.dat\";\n#\t  n = \"\";\n\t}\n      if (n == \"tps\")\n\t{ # This is the disk info\n\t  print $2 > FILENAME\".disk.tps.dat\"; # total transfers per second\n\t  print $3 > FILENAME\".disk.rtps.dat\"; # read requests per second\n\t  print $4 > FILENAME\".disk.wtps.dat\"; # write requests per second\n\t  print $5 > FI");
  echo("LENAME\".disk.brdps.dat\"; # block reads per second\n\t  print $6 > FILENAME\".disk.bwrps.dat\"; # block writes per second\n#\t  n = \"\";\n\t}\n      if (n == \"kbmemfree\")\n\t{ # This is the mem info\n\t  print $2 > FILENAME\".mem.kbmemfree.dat\"; # Amount of free memory available in kilobytes.\n\t  print $3 > FILENAME\".mem.kbmemused.dat\"; # Amount of used memory in kilobytes. This does not take into account memory u");
  echo("sed by the kernel itself.\n\t  print $4 > FILENAME\".mem.memused.dat\"; # Percentage of used memory.\n#         It appears the kbmemshrd has been removed from the sysstat output - ntolia\n#\t  print $X > FILENAME\".mem.kbmemshrd.dat\"; # Amount of memory shared by the system in kilobytes.  Always zero with 2.4 kernels.\n#\t  print $5 > FILENAME\".mem.kbbuffers.dat\"; # Amount of memory used as buffers by the k");
  echo("ernel in kilobytes.\n\t  print $6 > FILENAME\".mem.kbcached.dat\"; # Amount of memory used to cache data by the kernel in kilobytes.\n#\t  print $7 > FILENAME\".mem.kbswpfree.dat\"; # Amount of free swap space in kilobytes.\n#\t  print $8 > FILENAME\".mem.kbswpused.dat\"; # Amount of used swap space in kilobytes.\n\t  print $9 > FILENAME\".mem.swpused.dat\"; # Percentage of used swap space.\n#\t  n = \"\";\n \t}\n      ");
  echo("if (n == \"totsck\")\n\t{ # This is the socket info\n\t  print $2 > FILENAME\".sock.totsck.dat\"; # Total number of used sockets.\n\t  print $3 > FILENAME\".sock.tcpsck.dat\"; # Number of TCP sockets currently in use.\n#\t  print $4 > FILENAME\".sock.udpsck.dat\"; # Number of UDP sockets currently in use.\n#\t  print $5 > FILENAME\".sock.rawsck.dat\"; # Number of RAW sockets currently in use.\n#\t  print $6 > FILENAME\"");
  echo(".sock.ip-frag.dat\"; # Number of IP fragments currently in use.\n#\t  n = \"\";\n \t}\n    }\n}\nEND {\n  print \" '\" FILENAME \"' done.\";\n}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
