
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/BrowseStoriesByCategory.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$BrowseStoriesByCategory_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/BrowseStoriesByCategory.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$BrowseStoriesByCategory_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_categoryName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("categoryName") : g->GV(categoryName);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_categoryId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("categoryId") : g->GV(categoryId);
  Variant &v_page __attribute__((__unused__)) = (variables != gVariables) ? variables->get("page") : g->GV(page);
  Variant &v_nbOfStories __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nbOfStories") : g->GV(nbOfStories);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_username __attribute__((__unused__)) = (variables != gVariables) ? variables->get("username") : g->GV(username);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "StoriesOfTheDay.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_categoryName = v_HTTP_POST_VARS.rvalAt("categoryName", 0x2BE710E5697BEABELL));
  if (equal(v_categoryName, null)) {
    (v_categoryName = v_HTTP_GET_VARS.rvalAt("categoryName", 0x2BE710E5697BEABELL));
    if (equal(v_categoryName, null)) {
      LINE(15,f_printerror(v_scriptName, v_startTime, "Browse Stories By Category", "You must provide a category name!<br>"));
      f_exit();
    }
  }
  (v_categoryId = v_HTTP_POST_VARS.rvalAt("category", 0x684FAD5C8F6EC8DELL));
  if (equal(v_categoryId, null)) {
    (v_categoryId = v_HTTP_GET_VARS.rvalAt("category", 0x684FAD5C8F6EC8DELL));
    if (equal(v_categoryId, null)) {
      LINE(26,f_printerror(v_scriptName, v_startTime, "Browse Stories By Category", "You must provide a category identifier!<br>"));
      f_exit();
    }
  }
  (v_page = v_HTTP_POST_VARS.rvalAt("page", 0x04115BE1F73AED91LL));
  if (equal(v_page, null)) {
    (v_page = v_HTTP_GET_VARS.rvalAt("page", 0x04115BE1F73AED91LL));
    if (equal(v_page, null)) (v_page = 0LL);
  }
  (v_nbOfStories = v_HTTP_POST_VARS.rvalAt("nbOfStories", 0x5B8402A024098F72LL));
  if (equal(v_nbOfStories, null)) {
    (v_nbOfStories = v_HTTP_GET_VARS.rvalAt("nbOfStories", 0x5B8402A024098F72LL));
    if (equal(v_nbOfStories, null)) (v_nbOfStories = 25LL);
  }
  LINE(47,f_printhtmlheader("RUBBoS Browse Stories By Category"));
  print((LINE(48,concat3("<br><h2>Stories in category ", toString(v_categoryName), "</h2><br>"))));
  LINE(50,f_getdatabaselink(ref(v_link)));
  (toBoolean((v_result = LINE(51,(assignCallTemp(eo_0, concat5("SELECT * FROM stories WHERE category=", toString(v_categoryId), " ORDER BY date DESC LIMIT ", toString(v_page * v_nbOfStories), toString(",") + toString(v_nbOfStories))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
  if (equal(LINE(52,x_mysql_num_rows(v_result)), 0LL)) {
    if (equal(v_page, 0LL)) print("<h2>Sorry, but there is no story available in this category !</h2>");
    else {
      print("<h2>Sorry, but there are no more stories available at this time.</h2><br>\n");
      print((LINE(60,(assignCallTemp(eo_0, toString("<p><CENTER>\n<a href=\"BrowseStoriesByCategory.php\?category=") + toString(v_categoryId)),assignCallTemp(eo_2, x_urlencode(toString(v_categoryName))),assignCallTemp(eo_4, (toString(v_page - 1LL))),assignCallTemp(eo_5, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n</CENTER>\n")),concat6(eo_0, "&categoryName=", eo_2, "&page=", eo_4, eo_5)))));
    }
    LINE(62,x_mysql_free_result(v_result));
    LINE(63,x_mysql_close(v_link));
    LINE(64,f_printhtmlfooter(v_scriptName, v_startTime));
    f_exit();
  }
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_row = LINE(69,x_mysql_fetch_array(v_result))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_username = LINE(71,f_getusername(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link)));
        print((concat_rev(LINE(72,concat6(toString(v_row.rvalAt("title", 0x66AD900A2301E2FELL)), "</a> by ", toString(v_username), " on ", toString(v_row.rvalAt("date", 0x58C7226464D57F09LL)), "<br>\n")), concat3("<a href=\"ViewStory.php\?storyId=", toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "\">"))));
      }
    }
  }
  if (equal(v_page, 0LL)) print((LINE(78,(assignCallTemp(eo_0, toString("<p><CENTER>\n<a href=\"BrowseStoriesByCategory.php\?category=") + toString(v_categoryId)),assignCallTemp(eo_2, x_urlencode(toString(v_categoryName))),assignCallTemp(eo_4, (toString(v_page + 1LL))),assignCallTemp(eo_5, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Next page</a>\n</CENTER>\n")),concat6(eo_0, "&categoryName=", eo_2, "&page=", eo_4, eo_5)))));
  else print((concat_rev(LINE(83,concat3("&nbOfStories=", toString(v_nbOfStories), "\">Next page</a>\n\n</CENTER>\n")), concat(concat(concat_rev(x_urlencode(toString(v_categoryName)), (assignCallTemp(eo_0, toString("<p><CENTER>\n<a href=\"BrowseStoriesByCategory.php\?category=") + toString(v_categoryId)),assignCallTemp(eo_2, (assignCallTemp(eo_3, LINE(81,x_urlencode(toString(v_categoryName)))),assignCallTemp(eo_5, (toString(v_page - 1LL))),assignCallTemp(eo_6, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n&nbsp&nbsp&nbsp")),assignCallTemp(eo_7, toString("<a href=\"BrowseStoriesByCategory.php\?category=") + toString(v_categoryId)),concat6(eo_3, "&page=", eo_5, eo_6, eo_7, "&categoryName="))),concat3(eo_0, "&categoryName=", eo_2))), "&page="), (toString(v_page + 1LL))))));
  LINE(85,x_mysql_free_result(v_result));
  LINE(86,x_mysql_close(v_link));
  LINE(88,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
