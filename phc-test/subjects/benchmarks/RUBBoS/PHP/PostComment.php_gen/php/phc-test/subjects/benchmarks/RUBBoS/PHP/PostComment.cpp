
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PostComment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PostComment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/PostComment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PostComment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_storyId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("storyId") : g->GV(storyId);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_parent __attribute__((__unused__)) = (variables != gVariables) ? variables->get("parent") : g->GV(parent);
  Variant &v_comment_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_table") : g->GV(comment_table);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "Search.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_storyId = v_HTTP_POST_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
  if (equal(v_storyId, null)) {
    (v_storyId = v_HTTP_GET_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
    if (equal(v_storyId, null)) {
      LINE(15,f_printerror(v_scriptName, v_startTime, "Posting comment", "You must provide a story identifier!<br>"));
      f_exit();
    }
  }
  (v_parent = v_HTTP_POST_VARS.rvalAt("parent", 0x16E2F26FFB10FD8CLL));
  if (equal(v_parent, null)) {
    (v_parent = v_HTTP_GET_VARS.rvalAt("parent", 0x16E2F26FFB10FD8CLL));
    if (equal(v_parent, null)) {
      LINE(25,f_printerror(v_scriptName, v_startTime, "Posting comment", "You must provide a follow up identifier!<br>"));
      f_exit();
    }
  }
  (v_comment_table = v_HTTP_POST_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
  if (equal(v_comment_table, null)) {
    (v_comment_table = v_HTTP_GET_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
    if (equal(v_comment_table, null)) {
      LINE(36,f_printerror(v_scriptName, v_startTime, "Viewing comment", "You must provide a comment table!<br>"));
      f_exit();
    }
  }
  LINE(41,f_printhtmlheader("RUBBoS: Comment submission"));
  print((LINE(53,(assignCallTemp(eo_1, LINE(44,concat3("<input type=hidden name=storyId value=", toString(v_storyId), ">\n"))),assignCallTemp(eo_2, LINE(45,concat3("<input type=hidden name=parent value=", toString(v_parent), ">\n"))),assignCallTemp(eo_3, LINE(53,concat3("<input type=hidden name=comment_table value=", toString(v_comment_table), ">\n<center><table>\n<tr><td><b>Nickname</b><td><input type=text size=20 name=nickname>\n<tr><td><b>Password</b><td><input type=text size=20 name=password>\n<tr><td><b>Subject</b><td><input type=text size=100 name=subject>\n</SELECT></table><p><br>\n<TEXTAREA rows=\"20\" cols=\"80\" name=\"body\">Write your comment here</TEXTAREA><br><p>\n<input type=submit value=\"Post your comment now!\"></center><p>\n"))),concat4("<p><br><center><h2>Post a comment !</h2><br>\n<form action=\"StoreComment.php\" method=POST>\n", eo_1, eo_2, eo_3)))));
  LINE(55,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
