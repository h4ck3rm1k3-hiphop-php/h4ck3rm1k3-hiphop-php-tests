
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_PostComment_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_PostComment_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PostComment.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PostComment_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_PostComment_h__
