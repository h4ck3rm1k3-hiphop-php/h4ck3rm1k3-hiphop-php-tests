
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/author.html.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$author_html(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/author.html);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$author_html;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n<html>\n<head>\n   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n   <meta name=\"GENERATOR\" content=\"Mozilla/4.72 [en] (X11; U; Linux 2.2.14-5.0 i686) [Netscape]\">\n   <meta name=\"Author\" content=\"Emmanuel Cecchet\">\n   <title>RUBBoS: Rice University Bulletin Board System</title>\n</head>\n<body text=\"#000000\" bgcolor=\"#FFFFF");
  echo("F\" link=\"#0000EE\" vlink=\"#551A8B\" alink=\"#FF0000\">\n&nbsp;\n<center><table COLS=7 WIDTH=\"100%\" NOSAVE >\n<tr NOSAVE>\n<td NOSAVE>\n<center><IMG SRC=\"RUBBoS_logo.jpg\" height=68 width=200 align=ABSCENTER><br>PHP version</center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"StoriesOfTheDay.php\">Home</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"register.html\">Register</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"browse.html\">Bro");
  echo("wse</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"Search.php\">Search</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"SubmitStory.php\">Submit a story</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"author.html\">Author</a></h2></center>\n</td>\n</tr>\n</table></center>\n<br>\n<center>\n<h2>Are you really an author \?</h2></center>\n\n<form action=\"Author.php\" method=POST>\n\n<center><table>\n<tr><td>Your nick name:</td><td><input type=te");
  echo("xt size=20 name=nickname></td></tr>\n<tr><td>Your password:</td><td><input type=password size=20 name=password></td></tr>\n</table></center>\n\n<center>\n<p><input type=submit value=\"Log In!\"></center>\n<br>&nbsp;\n<hr WIDTH=\"100%\">\n<br><i>RUBBoS (C) 2001 - Rice University/INRIA</i>\n</body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
