
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_ViewComment_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_ViewComment_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/ViewComment.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_display_follow_up(CVarRef v_cid, Numeric v_level, CVarRef v_display, CVarRef v_filter, CVarRef v_link, CVarRef v_comment_table, Variant v_separator);
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$ViewComment_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_ViewComment_h__
