
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/ViewComment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/RUBBoS/PHP/ViewComment.php line 7 */
void f_display_follow_up(CVarRef v_cid, Numeric v_level, CVarRef v_display, CVarRef v_filter, CVarRef v_link, CVarRef v_comment_table, Variant v_separator) {
  FUNCTION_INJECTION(display_follow_up);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  Variant v_follow;
  Variant v_follow_row;
  int64 v_i = 0;
  Variant v_username;

  (toBoolean((v_follow = LINE(9,(assignCallTemp(eo_0, concat6("SELECT * FROM ", toString(v_comment_table), " WHERE parent=", toString(v_cid), " AND rating>=", toString(v_filter))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_follow_row = LINE(10,x_mysql_fetch_array(v_follow))))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (not_less(v_follow_row.rvalAt("rating", 0x1337E0729FB407EFLL), v_filter)) {
          if (!(toBoolean(v_separator))) {
            print("<br><hr><br>");
            (v_separator = true);
          }
          if (equal(v_display, 1LL)) {
            {
              LOOP_COUNTER(2);
              for ((v_i = 0LL); less(v_i, v_level); v_i++) {
                LOOP_COUNTER_CHECK(2);
                LINE(22,x_printf(1, " &nbsp &nbsp &nbsp "));
              }
            }
            print((concat_rev(LINE(23,(assignCallTemp(eo_0, toString(v_follow_row.rvalAt("subject", 0x0EDD9FCC9525C3B2LL))),assignCallTemp(eo_2, toString(f_getusername(v_follow_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link))),assignCallTemp(eo_4, toString(v_follow_row.rvalAt("date", 0x58C7226464D57F09LL))),concat6(eo_0, "</a> by ", eo_2, " on ", eo_4, "<br>\n"))), concat("<a href=\"ViewComment.php\?comment_table=", (assignCallTemp(eo_0, toString(v_comment_table)),assignCallTemp(eo_2, toString(v_follow_row.rvalAt("story_id", 0x007EFF583FAEE037LL))),assignCallTemp(eo_4, toString(v_follow_row.rvalAt("id", 0x028B9FE0C4522BE2LL))),assignCallTemp(eo_5, concat5("&filter=", toString(v_filter), "&display=", toString(v_display), "\">")),concat6(eo_0, "&storyId=", eo_2, "&commentId=", eo_4, eo_5))))));
          }
          else {
            (v_username = LINE(27,f_getusername(v_follow_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link)));
            print("<TABLE bgcolor=\"#CCCCFF\"><TR>");
            {
              LOOP_COUNTER(3);
              for ((v_i = 0LL); less(v_i, v_level); v_i++) {
                LOOP_COUNTER_CHECK(3);
                LINE(30,x_printf(1, "<TD>&nbsp&nbsp&nbsp"));
              }
            }
            print((concat_rev(LINE(31,(assignCallTemp(eo_0, toString(v_follow_row.rvalAt("id", 0x028B9FE0C4522BE2LL))),assignCallTemp(eo_1, concat5("&filter=", toString(v_filter), "&display=", toString(v_display), "\">")),assignCallTemp(eo_2, toString(v_follow_row.rvalAt("subject", 0x0EDD9FCC9525C3B2LL))),assignCallTemp(eo_4, toString(v_follow_row.rvalAt("rating", 0x1337E0729FB407EFLL))),concat6(eo_0, eo_1, eo_2, "</a></B>&nbsp</FONT> (Score:", eo_4, ")</TABLE>\n"))), concat5("<TD><FONT size=\"4\" color=\"#000000\"><B><a href=\"ViewComment.php\?comment_table=", toString(v_comment_table), "&storyId=", toString(v_follow_row.rvalAt("story_id", 0x007EFF583FAEE037LL)), "&commentId="))));
            print("<TABLE>");
            {
              LOOP_COUNTER(4);
              for ((v_i = 0LL); less(v_i, v_level); v_i++) {
                LOOP_COUNTER_CHECK(4);
                LINE(34,x_printf(1, "<TD>&nbsp&nbsp&nbsp"));
              }
            }
            print((LINE(35,concat5("<TD><B>Posted by ", toString(v_username), " on ", toString(v_follow_row.rvalAt("date", 0x58C7226464D57F09LL)), "</B><p><TR>\n"))));
            {
              LOOP_COUNTER(5);
              for ((v_i = 0LL); less(v_i, v_level); v_i++) {
                LOOP_COUNTER_CHECK(5);
                LINE(37,x_printf(1, "<TD>&nbsp&nbsp&nbsp"));
              }
            }
            print((LINE(38,concat3("<TD>", toString(v_follow_row.rvalAt("comment", 0x044C0F716BE4D0ADLL)), "<TR>"))));
            {
              LOOP_COUNTER(6);
              for ((v_i = 0LL); less(v_i, v_level); v_i++) {
                LOOP_COUNTER_CHECK(6);
                LINE(40,x_printf(1, "<TD>&nbsp&nbsp&nbsp"));
              }
            }
            print((concat_rev(LINE(44,(assignCallTemp(eo_0, toString(v_follow_row.rvalAt("story_id", 0x007EFF583FAEE037LL))),assignCallTemp(eo_2, toString(v_follow_row.rvalAt("parent", 0x16E2F26FFB10FD8CLL))),assignCallTemp(eo_3, concat("&filter=", LINE(43,concat6(toString(v_filter), "&display=", toString(v_display), "\">Parent</a>&nbsp|&nbsp<a href=\"ModerateComment.php\?comment_table=", toString(v_comment_table), "&commentId=")))),assignCallTemp(eo_4, toString(v_follow_row.rvalAt("id", 0x028B9FE0C4522BE2LL))),concat6(eo_0, "&commentId=", eo_2, eo_3, eo_4, "\">Moderate</a> ]</TABLE><br>"))), LINE(44,(assignCallTemp(eo_1, toString(v_comment_table)),assignCallTemp(eo_2, (assignCallTemp(eo_4, toString(v_follow_row.rvalAt("story_id", 0x007EFF583FAEE037LL))),assignCallTemp(eo_6, toString(v_follow_row.rvalAt("id", 0x028B9FE0C4522BE2LL))),assignCallTemp(eo_8, LINE(42,concat3("&nbsp|&nbsp<a href=\"ViewComment.php\?comment_table=", toString(v_comment_table), "&storyId="))),concat6("&storyId=", eo_4, "&parent=", eo_6, "\">Reply to this</a>", eo_8))),concat3("<TD><p>[ <a href=\"PostComment.php\?comment_table=", eo_1, eo_2))))));
          }
        }
        if (more(v_follow_row.rvalAt("childs", 0x2BBE7E7ADC5B5CA1LL), 0LL)) LINE(48,f_display_follow_up(v_follow_row.rvalAt("id", 0x028B9FE0C4522BE2LL), v_level + 1LL, v_display, v_filter, v_link, v_comment_table, v_separator));
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$ViewComment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/ViewComment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$ViewComment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_filter __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filter") : g->GV(filter);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_display __attribute__((__unused__)) = (variables != gVariables) ? variables->get("display") : g->GV(display);
  Variant &v_storyId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("storyId") : g->GV(storyId);
  Variant &v_commentId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("commentId") : g->GV(commentId);
  Variant &v_comment_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_table") : g->GV(comment_table);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_parent __attribute__((__unused__)) = (variables != gVariables) ? variables->get("parent") : g->GV(parent);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_count_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count_result") : g->GV(count_result);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_count_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count_row") : g->GV(count_row);
  Variant &v_comment __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment") : g->GV(comment);
  Variant &v_comment_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_row") : g->GV(comment_row);
  Variant &v_separator __attribute__((__unused__)) = (variables != gVariables) ? variables->get("separator") : g->GV(separator);
  Variant &v_username __attribute__((__unused__)) = (variables != gVariables) ? variables->get("username") : g->GV(username);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "ViewComment.php");
  LINE(55,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(56,f_getmicrotime()));
  (v_filter = v_HTTP_POST_VARS.rvalAt("filter", 0x32E04884DF75B1C1LL));
  if (equal(v_filter, null)) {
    (v_filter = v_HTTP_GET_VARS.rvalAt("filter", 0x32E04884DF75B1C1LL));
    if (equal(v_filter, null)) (v_filter = 0LL);
  }
  (v_display = v_HTTP_POST_VARS.rvalAt("display", 0x2C7BA2A298361DE0LL));
  if (equal(v_display, null)) {
    (v_display = v_HTTP_GET_VARS.rvalAt("display", 0x2C7BA2A298361DE0LL));
    if (equal(v_display, null)) (v_display = 0LL);
  }
  (v_storyId = v_HTTP_POST_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
  if (equal(v_storyId, null)) {
    (v_storyId = v_HTTP_GET_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
    if (equal(v_storyId, null)) {
      LINE(80,f_printerror(v_scriptName, v_startTime, "Viewing comment", "You must provide a story identifier!<br>"));
      f_exit();
    }
  }
  (v_commentId = v_HTTP_POST_VARS.rvalAt("commentId", 0x2BBDDCC3BCEE6282LL));
  if (equal(v_commentId, null)) {
    (v_commentId = v_HTTP_GET_VARS.rvalAt("commentId", 0x2BBDDCC3BCEE6282LL));
    if (equal(v_commentId, null)) {
      LINE(91,f_printerror(v_scriptName, v_startTime, "Viewing comment", "You must provide a comment identifier!<br>"));
      f_exit();
    }
  }
  (v_comment_table = v_HTTP_POST_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
  if (equal(v_comment_table, null)) {
    (v_comment_table = v_HTTP_GET_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
    if (equal(v_comment_table, null)) {
      LINE(102,f_printerror(v_scriptName, v_startTime, "Viewing comment", "You must provide a comment table!<br>"));
      f_exit();
    }
  }
  LINE(107,f_getdatabaselink(ref(v_link)));
  if (equal(v_commentId, 0LL)) (v_parent = 0LL);
  else {
    (toBoolean((v_result = LINE(113,(assignCallTemp(eo_0, concat4("SELECT parent FROM ", toString(v_comment_table), " WHERE id=", toString(v_commentId))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
    if (equal(LINE(114,x_mysql_num_rows(v_result)), 0LL)) f_exit("<h3>ERROR: Sorry, but this comment does not exist.</h3><br>\n");
    (v_row = LINE(116,x_mysql_fetch_array(v_result)));
    (v_parent = v_row.rvalAt("parent", 0x16E2F26FFB10FD8CLL));
  }
  LINE(120,f_printhtmlheader("RUBBoS: Viewing comments"));
  print((LINE(127,(assignCallTemp(eo_1, LINE(124,concat3("<input type=hidden name=commentId value=", toString(v_commentId), ">\n"))),assignCallTemp(eo_2, LINE(125,concat3("<input type=hidden name=storyId value=", toString(v_storyId), ">\n"))),assignCallTemp(eo_3, LINE(127,concat3("<input type=hidden name=comment_table value=", toString(v_comment_table), ">\n<B>Filter :</B>&nbsp&nbsp<SELECT name=filter>\n"))),concat4("<center><form action=\"ViewComment.php\" method=POST>\n", eo_1, eo_2, eo_3)))));
  (toBoolean((v_count_result = LINE(128,(assignCallTemp(eo_0, concat5("SELECT rating, COUNT(rating) AS count FROM ", toString(v_comment_table), " WHERE story_id=", toString(v_storyId), " GROUP BY rating ORDER BY rating")),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
  (v_i = -1LL);
  LOOP_COUNTER(7);
  {
    while (toBoolean((v_count_row = LINE(130,x_mysql_fetch_array(v_count_result))))) {
      LOOP_COUNTER_CHECK(7);
      {
        LOOP_COUNTER(8);
        {
          while ((less(v_i, 6LL)) && (!equal(v_count_row.rvalAt("rating", 0x1337E0729FB407EFLL), v_i))) {
            LOOP_COUNTER_CHECK(8);
            {
              if (equal(v_i, v_filter)) print((LINE(135,concat5("<OPTION selected value=\"", toString(v_i), "\">", toString(v_i), ": 0 comment</OPTION>\n"))));
              else print((LINE(137,concat5("<OPTION value=\"", toString(v_i), "\">", toString(v_i), ": 0 comment</OPTION>\n"))));
              v_i++;
            }
          }
        }
        if (equal(v_count_row.rvalAt("rating", 0x1337E0729FB407EFLL), v_i)) {
          if (equal(v_i, v_filter)) print((concat("<OPTION selected value=\"", LINE(143,concat6(toString(v_i), "\">", toString(v_i), ": ", toString(v_count_row.rvalAt("count", 0x3D66B5980D54BABBLL)), " comments</OPTION>\n")))));
          else print((concat("<OPTION value=\"", LINE(145,concat6(toString(v_i), "\">", toString(v_i), ": ", toString(v_count_row.rvalAt("count", 0x3D66B5980D54BABBLL)), " comments</OPTION>\n")))));
          v_i++;
        }
      }
    }
  }
  LOOP_COUNTER(9);
  {
    while (less(v_i, 6LL)) {
      LOOP_COUNTER_CHECK(9);
      {
        print((LINE(151,concat5("<OPTION value=\"", toString(v_i), "\">", toString(v_i), ": 0 comment</OPTION>\n"))));
        v_i++;
      }
    }
  }
  print("</SELECT>&nbsp&nbsp&nbsp&nbsp<SELECT name=display>\n<OPTION value=\"0\">Main threads</OPTION>\n");
  if (equal(v_display, 1LL)) print("<OPTION selected value=\"1\">Nested</OPTION>\n");
  else print("<OPTION value=\"1\">Nested</OPTION>\n");
  if (equal(v_display, 2LL)) print("<OPTION selected value=\"2\">All comments</OPTION>\n");
  else print("<OPTION value=\"2\">All comments</OPTION>\n");
  print("</SELECT>&nbsp&nbsp&nbsp&nbsp<input type=submit value=\"Refresh display\"></center><p>\n");
  (toBoolean((v_comment = LINE(168,(assignCallTemp(eo_0, concat6("SELECT * FROM ", toString(v_comment_table), " WHERE story_id=", toString(v_storyId), " AND parent=", toString(v_parent))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
  LOOP_COUNTER(10);
  {
    while (toBoolean((v_comment_row = LINE(169,x_mysql_fetch_array(v_comment))))) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_separator = false);
        if (not_less(v_comment_row.rvalAt("rating", 0x1337E0729FB407EFLL), v_filter)) {
          (v_separator = true);
          print("<br><hr><br>");
          (v_username = LINE(176,f_getusername(v_comment_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link)));
          print((concat_rev(LINE(177,(assignCallTemp(eo_0, toString(v_comment_row.rvalAt("id", 0x028B9FE0C4522BE2LL))),assignCallTemp(eo_1, concat5("&filter=", toString(v_filter), "&display=", toString(v_display), "\">")),assignCallTemp(eo_2, toString(v_comment_row.rvalAt("subject", 0x0EDD9FCC9525C3B2LL))),assignCallTemp(eo_4, toString(v_comment_row.rvalAt("rating", 0x1337E0729FB407EFLL))),concat6(eo_0, eo_1, eo_2, "</a></B>&nbsp</FONT> (Score:", eo_4, ")</TABLE>\n"))), concat5("<TABLE width=\"100%\" bgcolor=\"#CCCCFF\"><TR><TD><FONT size=\"4\" color=\"#000000\"><B><a href=\"ViewComment.php\?comment_table=", toString(v_comment_table), "&storyId=", toString(v_storyId), "&commentId="))));
          print((LINE(178,concat5("<TABLE><TR><TD><B>Posted by ", toString(v_username), " on ", toString(v_comment_row.rvalAt("date", 0x58C7226464D57F09LL)), "</B><p>\n"))));
          print((concat("<TR><TD>", toString(v_comment_row.rvalAt("comment", 0x044C0F716BE4D0ADLL)))));
          print((concat_rev(LINE(182,(assignCallTemp(eo_0, LINE(181,concat5("<a href=\"ViewComment.php\?comment_table=", toString(v_comment_table), "&storyId=", toString(v_storyId), "&commentId="))),assignCallTemp(eo_1, toString(v_comment_row.rvalAt("parent", 0x16E2F26FFB10FD8CLL))),assignCallTemp(eo_2, concat5("&filter=", toString(v_filter), "&display=", toString(v_display), "\">Parent</a>")),assignCallTemp(eo_3, LINE(182,concat3("&nbsp|&nbsp<a href=\"ModerateComment.php\?comment_table=", toString(v_comment_table), "&commentId="))),assignCallTemp(eo_4, toString(v_comment_row.rvalAt("id", 0x028B9FE0C4522BE2LL))),concat6(eo_0, eo_1, eo_2, eo_3, eo_4, "\">Moderate</a> ]</TABLE>\n"))), concat("<TR><TD><p>[ <a href=\"PostComment.php\?comment_table=", concat6(toString(v_comment_table), "&storyId=", toString(v_storyId), "&parent=", toString(v_comment_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "\">Reply to this</a>&nbsp|&nbsp")))));
        }
        if ((more(v_display, 0LL)) && (more(v_comment_row.rvalAt("childs", 0x2BBE7E7ADC5B5CA1LL), 0LL))) LINE(185,f_display_follow_up(v_comment_row.rvalAt("id", 0x028B9FE0C4522BE2LL), 1LL, v_display, v_filter, v_link, v_comment_table, v_separator));
      }
    }
  }
  LINE(188,x_mysql_close(v_link));
  LINE(190,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
