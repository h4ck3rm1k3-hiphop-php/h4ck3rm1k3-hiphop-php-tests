
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      HASH_INITIALIZED(0x32E04884DF75B1C1LL, g->GV(filter),
                       filter);
      break;
    case 2:
      HASH_INITIALIZED(0x2BBDDCC3BCEE6282LL, g->GV(commentId),
                       commentId);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 4:
      HASH_INITIALIZED(0x12296005AB6E5904LL, g->GV(HTTP_POST_VARS),
                       HTTP_POST_VARS);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 8:
      HASH_INITIALIZED(0x2315022AC4CB4888LL, g->GV(HTTP_GET_VARS),
                       HTTP_GET_VARS);
      HASH_INITIALIZED(0x599E3F67E2599248LL, g->GV(result),
                       result);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x07D68BF68C574E09LL, g->GV(link),
                       link);
      break;
    case 12:
      HASH_INITIALIZED(0x16E2F26FFB10FD8CLL, g->GV(parent),
                       parent);
      break;
    case 13:
      HASH_INITIALIZED(0x58F24C51C564718DLL, g->GV(storyId),
                       storyId);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 18:
      HASH_INITIALIZED(0x4307151CEB3C6312LL, g->GV(row),
                       row);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 22:
      HASH_INITIALIZED(0x76A4063337CDFFD6LL, g->GV(comment_row),
                       comment_row);
      break;
    case 24:
      HASH_INITIALIZED(0x0EB22EDA95766E98LL, g->GV(i),
                       i);
      break;
    case 32:
      HASH_INITIALIZED(0x2C7BA2A298361DE0LL, g->GV(display),
                       display);
      break;
    case 34:
      HASH_INITIALIZED(0x51818D0728B4F922LL, g->GV(count_row),
                       count_row);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 44:
      HASH_INITIALIZED(0x1463CF5654FC95ACLL, g->GV(username),
                       username);
      break;
    case 45:
      HASH_INITIALIZED(0x5C5EEC0276AA952DLL, g->GV(startTime),
                       startTime);
      HASH_INITIALIZED(0x044C0F716BE4D0ADLL, g->GV(comment),
                       comment);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 51:
      HASH_INITIALIZED(0x2D9556DD57FAE1B3LL, g->GV(comment_table),
                       comment_table);
      break;
    case 56:
      HASH_INITIALIZED(0x0039BBE309A18578LL, g->GV(separator),
                       separator);
      break;
    case 60:
      HASH_INITIALIZED(0x487AC8B0170EAB3CLL, g->GV(scriptName),
                       scriptName);
      HASH_INITIALIZED(0x37499AD63C4B58BCLL, g->GV(count_result),
                       count_result);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
