
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 2:
      HASH_INITIALIZED(0x73D8A897237A4182LL, g->GV(table),
                       table);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      HASH_INITIALIZED(0x79E99297D9CF6243LL, g->GV(search),
                       search);
      break;
    case 4:
      HASH_INITIALIZED(0x12296005AB6E5904LL, g->GV(HTTP_POST_VARS),
                       HTTP_POST_VARS);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 8:
      HASH_INITIALIZED(0x2315022AC4CB4888LL, g->GV(HTTP_GET_VARS),
                       HTTP_GET_VARS);
      HASH_INITIALIZED(0x599E3F67E2599248LL, g->GV(result),
                       result);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x07D68BF68C574E09LL, g->GV(link),
                       link);
      break;
    case 10:
      HASH_INITIALIZED(0x508FC7C8724A760ALL, g->GV(type),
                       type);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      HASH_INITIALIZED(0x04115BE1F73AED91LL, g->GV(page),
                       page);
      break;
    case 18:
      HASH_INITIALIZED(0x4307151CEB3C6312LL, g->GV(row),
                       row);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 44:
      HASH_INITIALIZED(0x1463CF5654FC95ACLL, g->GV(username),
                       username);
      break;
    case 45:
      HASH_INITIALIZED(0x5C5EEC0276AA952DLL, g->GV(startTime),
                       startTime);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 50:
      HASH_INITIALIZED(0x5B8402A024098F72LL, g->GV(nbOfStories),
                       nbOfStories);
      break;
    case 51:
      HASH_INITIALIZED(0x2D9556DD57FAE1B3LL, g->GV(comment_table),
                       comment_table);
      break;
    case 60:
      HASH_INITIALIZED(0x487AC8B0170EAB3CLL, g->GV(scriptName),
                       scriptName);
      break;
    case 62:
      HASH_INITIALIZED(0x66AD900A2301E2FELL, g->GV(title),
                       title);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
