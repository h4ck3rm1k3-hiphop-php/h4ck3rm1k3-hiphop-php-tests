
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$Search_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$header_html(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_INCLUDE(0x7F7C071E7993D432LL, "phc-test/subjects/benchmarks/RUBBoS/PHP/Search.php", php$phc_test$subjects$benchmarks$RUBBoS$PHP$Search_php);
      break;
    case 5:
      HASH_INCLUDE(0x5BBA1A2AA11F102DLL, "phc-test/subjects/benchmarks/RUBBoS/PHP/header.html", php$phc_test$subjects$benchmarks$RUBBoS$PHP$header_html);
      break;
    case 7:
      HASH_INCLUDE(0x67C75A3BA6A16F8FLL, "phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.php", php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
