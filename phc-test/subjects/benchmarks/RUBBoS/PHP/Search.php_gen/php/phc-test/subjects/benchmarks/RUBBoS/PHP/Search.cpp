
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/Search.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$Search_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/Search.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$Search_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_type __attribute__((__unused__)) = (variables != gVariables) ? variables->get("type") : g->GV(type);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_search __attribute__((__unused__)) = (variables != gVariables) ? variables->get("search") : g->GV(search);
  Variant &v_page __attribute__((__unused__)) = (variables != gVariables) ? variables->get("page") : g->GV(page);
  Variant &v_nbOfStories __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nbOfStories") : g->GV(nbOfStories);
  Variant &v_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("table") : g->GV(table);
  Variant &v_title __attribute__((__unused__)) = (variables != gVariables) ? variables->get("title") : g->GV(title);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_comment_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_table") : g->GV(comment_table);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_username __attribute__((__unused__)) = (variables != gVariables) ? variables->get("username") : g->GV(username);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "Search.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_type = v_HTTP_POST_VARS.rvalAt("type", 0x508FC7C8724A760ALL));
  if (equal(v_type, null)) {
    (v_type = v_HTTP_GET_VARS.rvalAt("type", 0x508FC7C8724A760ALL));
    if (equal(v_type, null)) (v_type = 0LL);
  }
  (v_search = v_HTTP_POST_VARS.rvalAt("search", 0x79E99297D9CF6243LL));
  if (equal(v_search, null)) {
    (v_search = v_HTTP_GET_VARS.rvalAt("search", 0x79E99297D9CF6243LL));
  }
  (v_page = v_HTTP_POST_VARS.rvalAt("page", 0x04115BE1F73AED91LL));
  if (equal(v_page, null)) {
    (v_page = v_HTTP_GET_VARS.rvalAt("page", 0x04115BE1F73AED91LL));
    if (equal(v_page, null)) (v_page = 0LL);
  }
  (v_nbOfStories = v_HTTP_POST_VARS.rvalAt("nbOfStories", 0x5B8402A024098F72LL));
  if (equal(v_nbOfStories, null)) {
    (v_nbOfStories = v_HTTP_GET_VARS.rvalAt("nbOfStories", 0x5B8402A024098F72LL));
    if (equal(v_nbOfStories, null)) (v_nbOfStories = 25LL);
  }
  LINE(39,f_printhtmlheader("RUBBoS search"));
  print((concat("<form action=\"Search.php\" method=POST>\n<center><table>\n", LINE(45,concat3("<tr><td><b>Search</b><td><input type=text size=50 name=search value=", toString(v_search), ">\n<tr><td><b>in</b><td><SELECT name=type>\n")))));
  if (equal(v_type, 0LL)) {
    print("<OPTION selected value=\"0\">Stories</OPTION>\n");
    (v_table = "stories");
    (v_title = "Stories");
  }
  else print("<OPTION value=\"0\">Stories</OPTION>\n");
  if (equal(v_type, 1LL)) {
    print("<OPTION selected value=\"1\">Comments</OPTION>\n");
    (v_table = "comments");
    (v_title = "Comments");
  }
  else print("<OPTION value=\"1\">Comments</OPTION>\n");
  if (equal(v_type, 2LL)) {
    print("<OPTION selected value=\"2\">Authors</OPTION>\n");
    (v_table = "users");
    (v_title = "Stories with author");
  }
  else print("<OPTION value=\"2\">Authors</OPTION>\n");
  print("</SELECT></table><p><br>\n<input type=submit value=\"Search now!\"></center><p>\n");
  if (equal(v_search, null)) print("<br><center><h2>Please select a text to search for</h2></center><br>");
  else {
    print((LINE(79,concat5("<br><h2>", toString(v_title), " matching <i>", toString(v_search), "</i></h2></center><br>"))));
    LINE(81,f_getdatabaselink(ref(v_link)));
    if (equal(v_type, 0LL)) {
      (toBoolean((v_result = LINE(84,(assignCallTemp(eo_0, concat5("SELECT id, title, date, writer FROM stories WHERE title LIKE '", toString(v_search), "%'  ORDER BY date DESC LIMIT ", toString(v_page * v_nbOfStories), toString(",") + toString(v_nbOfStories))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
      if (equal(LINE(85,x_mysql_num_rows(v_result)), 0LL)) (toBoolean((v_result = LINE(86,(assignCallTemp(eo_0, concat5("SELECT id, title, date, writer FROM old_stories WHERE title LIKE '", toString(v_search), "%'  ORDER BY date DESC LIMIT ", toString(v_page * v_nbOfStories), toString(",") + toString(v_nbOfStories))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
      if (equal(LINE(87,x_mysql_num_rows(v_result)), 0LL)) {
        if (equal(v_page, 0LL)) print((LINE(90,concat3("<h2>Sorry, but there is no story matching <i>", toString(v_search), "</i> !</h2>"))));
        else {
          print((LINE(93,concat3("<h2>Sorry, but there are no more stories available matching <i>", toString(v_search), "</i>.</h2><br>\n"))));
          print((LINE(94,(assignCallTemp(eo_1, x_urlencode(toString(v_search))),assignCallTemp(eo_2, concat3("&type=", toString(v_type), "&page=")),assignCallTemp(eo_3, (toString(v_page - 1LL))),assignCallTemp(eo_4, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n</CENTER>\n")),concat5("<p><CENTER>\n<a href=\"Search.php\?search=", eo_1, eo_2, eo_3, eo_4)))));
        }
        LINE(96,x_mysql_free_result(v_result));
        LINE(97,x_mysql_close(v_link));
        LINE(98,f_printhtmlfooter(v_scriptName, v_startTime));
        f_exit();
      }
    }
    if (equal(v_type, 1LL)) {
      (v_comment_table = "comments");
      (toBoolean((v_result = LINE(105,(assignCallTemp(eo_0, concat5("SELECT id,story_id,subject,writer,date FROM comments WHERE subject LIKE '", toString(v_search), "%'  GROUP BY story_id ORDER BY date DESC LIMIT ", toString(v_page * v_nbOfStories), toString(",") + toString(v_nbOfStories))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
      if (equal(LINE(106,x_mysql_num_rows(v_result)), 0LL)) {
        (toBoolean((v_result = LINE(108,(assignCallTemp(eo_0, concat5("SELECT id,story_id,subject,writer,date FROM old_comments WHERE subject LIKE '", toString(v_search), "%'  ORDER BY date DESC LIMIT ", toString(v_page * v_nbOfStories), toString(",") + toString(v_nbOfStories))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
        (v_comment_table = "old_comments");
      }
      if (equal(LINE(111,x_mysql_num_rows(v_result)), 0LL)) {
        if (equal(v_page, 0LL)) print((LINE(114,concat3("<h2>Sorry, but there is no comment matching <i>", toString(v_search), "</i> !</h2>"))));
        else {
          print((LINE(117,concat3("<h2>Sorry, but there are no more comments available matching <i>", toString(v_search), "</i>.</h2><br>\n"))));
          print((LINE(118,(assignCallTemp(eo_1, x_urlencode(toString(v_search))),assignCallTemp(eo_2, concat3("&type=", toString(v_type), "&page=")),assignCallTemp(eo_3, (toString(v_page - 1LL))),assignCallTemp(eo_4, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n</CENTER>\n")),concat5("<p><CENTER>\n<a href=\"Search.php\?search=", eo_1, eo_2, eo_3, eo_4)))));
        }
        LINE(120,x_mysql_free_result(v_result));
        LINE(121,x_mysql_close(v_link));
        LINE(122,f_printhtmlfooter(v_scriptName, v_startTime));
        f_exit();
      }
      else {
        LOOP_COUNTER(1);
        {
          while (toBoolean((v_row = LINE(128,x_mysql_fetch_array(v_result))))) {
            LOOP_COUNTER_CHECK(1);
            print((concat_rev(LINE(129,(assignCallTemp(eo_0, toString(v_row.rvalAt("subject", 0x0EDD9FCC9525C3B2LL))),assignCallTemp(eo_2, toString(f_getusername(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link))),assignCallTemp(eo_4, toString(v_row.rvalAt("date", 0x58C7226464D57F09LL))),concat6(eo_0, "</a> by ", eo_2, " on ", eo_4, "<br>\n"))), concat("<a href=\"ViewComment.php\?comment_table=", concat6(toString(v_comment_table), "&storyId=", toString(v_row.rvalAt("story_id", 0x007EFF583FAEE037LL)), "&commentId=", toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "&filter=0&display=0\">")))));
          }
        }
      }
    }
    if (equal(v_type, 2LL)) {
      (toBoolean((v_result = LINE(135,(assignCallTemp(eo_0, (assignCallTemp(eo_3, concat3(" users.nickname LIKE '", toString(v_search), "%' ORDER BY date DESC LIMIT ")),assignCallTemp(eo_4, toString(v_page * v_nbOfStories)),assignCallTemp(eo_5, toString(",") + toString(v_nbOfStories)),concat4("SELECT stories.id, stories.title, stories.date, stories.writer FROM stories,users WHERE writer=users.id AND ", eo_3, eo_4, eo_5))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: stories query failed")));
      if (equal(LINE(136,x_mysql_num_rows(v_result)), 0LL)) (toBoolean((v_result = LINE(137,(assignCallTemp(eo_0, (assignCallTemp(eo_3, concat3(" users.nickname LIKE '", toString(v_search), "%' ORDER BY date DESC LIMIT ")),assignCallTemp(eo_4, toString(v_page * v_nbOfStories)),assignCallTemp(eo_5, toString(",") + toString(v_nbOfStories)),concat4("SELECT old_stories.id, old_stories.title, old_stories.date, old_stories.writer FROM old_stories,users WHERE writer=users.id AND ", eo_3, eo_4, eo_5))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: old_stories query failed")));
      if (equal(LINE(138,x_mysql_num_rows(v_result)), 0LL)) {
        if (equal(v_page, 0LL)) print((LINE(141,concat3("<h2>Sorry, but there is no story with author matching <i>", toString(v_search), "</i> !</h2>"))));
        else {
          print((LINE(144,concat3("<h2>Sorry, but there are no more stories available with author matching <i>", toString(v_search), "</i>.</h2><br>\n"))));
          print((LINE(145,(assignCallTemp(eo_1, x_urlencode(toString(v_search))),assignCallTemp(eo_2, concat3("&type=", toString(v_type), "&page=")),assignCallTemp(eo_3, (toString(v_page - 1LL))),assignCallTemp(eo_4, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n</CENTER>\n")),concat5("<p><CENTER>\n<a href=\"Search.php\?search=", eo_1, eo_2, eo_3, eo_4)))));
        }
        LINE(147,x_mysql_free_result(v_result));
        LINE(148,x_mysql_close(v_link));
        LINE(149,f_printhtmlfooter(v_scriptName, v_startTime));
        f_exit();
      }
    }
    if (!equal(v_type, 1LL)) {
      LOOP_COUNTER(2);
      {
        while (toBoolean((v_row = LINE(157,x_mysql_fetch_array(v_result))))) {
          LOOP_COUNTER_CHECK(2);
          {
            (v_username = LINE(159,f_getusername(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link)));
            print((concat_rev(LINE(160,concat6(toString(v_row.rvalAt("title", 0x66AD900A2301E2FELL)), "</a> by ", toString(v_username), " on ", toString(v_row.rvalAt("date", 0x58C7226464D57F09LL)), "<br>\n")), concat3("<a href=\"ViewStory.php\?storyId=", toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "\">"))));
          }
        }
      }
    }
    if (equal(v_page, 0LL)) print((LINE(166,(assignCallTemp(eo_1, x_urlencode(toString(v_search))),assignCallTemp(eo_2, concat3("&type=", toString(v_type), "&page=")),assignCallTemp(eo_3, (toString(v_page + 1LL))),assignCallTemp(eo_4, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Next page</a>\n</CENTER>\n")),concat5("<p><CENTER>\n<a href=\"Search.php\?search=", eo_1, eo_2, eo_3, eo_4)))));
    else print((concat_rev(LINE(169,concat3("&nbOfStories=", toString(v_nbOfStories), "\">Next page</a>\n\n</CENTER>\n")), concat(concat_rev(concat3("&type=", toString(v_type), "&page="), concat_rev(x_urlencode(toString(v_search)), (assignCallTemp(eo_1, LINE(168,x_urlencode(toString(v_search)))),assignCallTemp(eo_2, concat3("&type=", toString(v_type), "&page=")),assignCallTemp(eo_3, (toString(v_page - 1LL))),assignCallTemp(eo_4, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n&nbsp&nbsp&nbsp")),assignCallTemp(eo_5, LINE(169,concat3("<a href=\"Search.php\?category=", toString(v_search), "="))),concat6("<p><CENTER>\n<a href=\"Search.php\?search=", eo_1, eo_2, eo_3, eo_4, eo_5)))), (toString(v_page + 1LL))))));
    LINE(171,x_mysql_free_result(v_result));
    LINE(172,x_mysql_close(v_link));
  }
  LINE(175,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
