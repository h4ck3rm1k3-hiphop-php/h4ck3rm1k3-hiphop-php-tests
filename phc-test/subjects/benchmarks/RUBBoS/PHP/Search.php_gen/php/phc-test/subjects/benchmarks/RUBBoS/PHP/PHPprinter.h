
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_PHPprinter_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_PHPprinter_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_printhtmlfooter(CVarRef v_scriptName, CVarRef v_startTime);
void f_printhtmlhighlighted(CVarRef v_msg);
void f_getdatabaselink(Variant v_link);
double f_getmicrotime();
void f_printerror(CVarRef v_scriptName, CVarRef v_startTime, CVarRef v_title, CVarRef v_error);
Variant f_getusername(CVarRef v_uid, CVarRef v_link);
Variant f_authenticate(CVarRef v_nickname, CVarRef v_password, CVarRef v_link);
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_printhtmlheader(Variant v_title);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_RUBBoS_PHP_PHPprinter_h__
