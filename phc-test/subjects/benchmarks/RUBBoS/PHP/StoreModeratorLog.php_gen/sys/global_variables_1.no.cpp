
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      HASH_RETURN(0x3B5E601F690192C1LL, g->GV(userId),
                  userId);
      break;
    case 2:
      HASH_RETURN(0x2BBDDCC3BCEE6282LL, g->GV(commentId),
                  commentId);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 4:
      HASH_RETURN(0x12296005AB6E5904LL, g->GV(HTTP_POST_VARS),
                  HTTP_POST_VARS);
      break;
    case 5:
      HASH_RETURN(0x5892D96DC0798D85LL, g->GV(user_result),
                  user_result);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x1112F727DCE2BE06LL, g->GV(now),
                  now);
      break;
    case 8:
      HASH_RETURN(0x2315022AC4CB4888LL, g->GV(HTTP_GET_VARS),
                  HTTP_GET_VARS);
      HASH_RETURN(0x599E3F67E2599248LL, g->GV(result),
                  result);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x07D68BF68C574E09LL, g->GV(link),
                  link);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 15:
      HASH_RETURN(0x6AD1FAB93F84D08FLL, g->GV(password),
                  password);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 18:
      HASH_RETURN(0x4307151CEB3C6312LL, g->GV(row),
                  row);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 22:
      HASH_RETURN(0x76A4063337CDFFD6LL, g->GV(comment_row),
                  comment_row);
      break;
    case 24:
      HASH_RETURN(0x2D36EB6F33D16B58LL, g->GV(nickname),
                  nickname);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 45:
      HASH_RETURN(0x5C5EEC0276AA952DLL, g->GV(startTime),
                  startTime);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 47:
      HASH_RETURN(0x1337E0729FB407EFLL, g->GV(rating),
                  rating);
      break;
    case 48:
      HASH_RETURN(0x1B3AE15E8AF802B0LL, g->GV(user_row),
                  user_row);
      break;
    case 51:
      HASH_RETURN(0x2D9556DD57FAE1B3LL, g->GV(comment_table),
                  comment_table);
      break;
    case 58:
      HASH_RETURN(0x51B5E8B840B641FALL, g->GV(comment_result),
                  comment_result);
      break;
    case 59:
      HASH_RETURN(0x432ABF90750CDA3BLL, g->GV(access),
                  access);
      break;
    case 60:
      HASH_RETURN(0x487AC8B0170EAB3CLL, g->GV(scriptName),
                  scriptName);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
