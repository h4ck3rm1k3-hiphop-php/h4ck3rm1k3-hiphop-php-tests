
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/StoreModeratorLog.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoreModeratorLog_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/StoreModeratorLog.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoreModeratorLog_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_nickname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nickname") : g->GV(nickname);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_password __attribute__((__unused__)) = (variables != gVariables) ? variables->get("password") : g->GV(password);
  Variant &v_comment_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_table") : g->GV(comment_table);
  Variant &v_commentId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("commentId") : g->GV(commentId);
  Variant &v_rating __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rating") : g->GV(rating);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_userId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("userId") : g->GV(userId);
  Variant &v_access __attribute__((__unused__)) = (variables != gVariables) ? variables->get("access") : g->GV(access);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_comment_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_result") : g->GV(comment_result);
  Variant &v_comment_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_row") : g->GV(comment_row);
  Variant &v_user_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("user_result") : g->GV(user_result);
  Variant &v_user_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("user_row") : g->GV(user_row);
  Variant &v_now __attribute__((__unused__)) = (variables != gVariables) ? variables->get("now") : g->GV(now);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n<html>\r\n  <body>\r\n    ");
  (v_scriptName = "StoreComment.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_nickname = v_HTTP_POST_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
  if (equal(v_nickname, null)) {
    (v_nickname = v_HTTP_GET_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
    if (equal(v_nickname, null)) {
      LINE(15,f_printerror(v_scriptName, v_startTime, "Author", "You must provide a nick name!<br>"));
      f_exit();
    }
  }
  (v_password = v_HTTP_POST_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
  if (equal(v_password, null)) {
    (v_password = v_HTTP_GET_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
    if (equal(v_password, null)) {
      LINE(26,f_printerror(v_scriptName, v_startTime, "Author", "You must provide a password!<br>"));
      f_exit();
    }
  }
  (v_comment_table = v_HTTP_POST_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
  if (equal(v_comment_table, null)) {
    (v_comment_table = v_HTTP_GET_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
    if (equal(v_comment_table, null)) {
      LINE(37,f_printerror(v_scriptName, v_startTime, "Moderating comment", "You must provide a comment table!<br>"));
      f_exit();
    }
  }
  (v_commentId = v_HTTP_POST_VARS.rvalAt("commentId", 0x2BBDDCC3BCEE6282LL));
  if (equal(v_commentId, null)) {
    (v_commentId = v_HTTP_GET_VARS.rvalAt("commentId", 0x2BBDDCC3BCEE6282LL));
    if (equal(v_commentId, null)) {
      LINE(48,f_printerror(v_scriptName, v_startTime, "Moderating comment", "You must provide a comment identifier!<br>"));
      f_exit();
    }
  }
  (v_rating = v_HTTP_POST_VARS.rvalAt("rating", 0x1337E0729FB407EFLL));
  if (equal(v_rating, null)) {
    (v_rating = v_HTTP_GET_VARS.rvalAt("rating", 0x1337E0729FB407EFLL));
    if (equal(v_rating, null)) {
      LINE(59,f_printerror(v_scriptName, v_startTime, "Moderating comment", "You must provide a rating!<br>"));
      f_exit();
    }
  }
  LINE(64,f_getdatabaselink(ref(v_link)));
  (v_userId = 0LL);
  (v_access = 0LL);
  if ((!equal(v_nickname, null)) && (!equal(v_password, null))) {
    (toBoolean((v_result = LINE(71,(assignCallTemp(eo_0, concat5("SELECT id,access FROM users WHERE nickname=\"", toString(v_nickname), "\" AND password=\"", toString(v_password), "\"")),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Authentification query failed")));
    if (!equal(LINE(72,x_mysql_num_rows(v_result)), 0LL)) {
      (v_row = LINE(74,x_mysql_fetch_array(v_result)));
      (v_userId = v_row.rvalAt("id", 0x028B9FE0C4522BE2LL));
      (v_access = v_row.rvalAt("access", 0x432ABF90750CDA3BLL));
    }
    LINE(78,x_mysql_free_result(v_result));
  }
  if ((equal(v_userId, 0LL)) || (equal(v_access, 0LL))) {
    LINE(83,f_printhtmlheader("RUBBoS: Moderation"));
    print("<p><center><h2>Sorry, but this feature is only accessible by users with an author access.</h2></center><p>\n");
  }
  else {
    LINE(88,f_printhtmlheader("RUBBoS: Comment moderation result"));
    print("<center><h2>Comment moderation result:</h2></center><p>\n");
    (toBoolean((v_result = LINE(93,(assignCallTemp(eo_0, concat4("SELECT writer,rating FROM ", toString(v_comment_table), " WHERE id=", toString(v_commentId))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
    if (equal(LINE(94,x_mysql_num_rows(v_result)), 0LL)) {
      f_exit("<h3>ERROR: Sorry, but this comment does not exist.</h3><br>\n");
    }
    (v_row = LINE(99,x_mysql_fetch_array(v_result)));
    if (((equal(v_row.rvalAt("rating", 0x1337E0729FB407EFLL), -1LL)) && (equal(v_rating, -1LL))) || ((equal(v_row.rvalAt("rating", 0x1337E0729FB407EFLL), 5LL)) && (equal(v_rating, 1LL)))) print("Comment rating is already to its maximum, updating only user's rating.");
    else {
      if (!equal(v_rating, 0LL)) {
        (toBoolean(LINE(107,x_mysql_query(concat4("UPDATE users SET rating=rating+", toString(v_rating), " WHERE id=", toString(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL))))))) || (toBoolean(f_exit("ERROR: Unable to update user's rating\n")));
        (toBoolean(LINE(108,x_mysql_query(concat6("UPDATE ", toString(v_comment_table), " SET rating=rating+", toString(v_rating), " WHERE id=", toString(v_commentId)))))) || (toBoolean(f_exit("ERROR: Unable to update comment's rating\n")));
      }
    }
    (toBoolean((v_comment_result = LINE(112,(assignCallTemp(eo_0, concat4("SELECT rating FROM ", toString(v_comment_table), " WHERE id=", toString(v_commentId))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Comment rating query failed")));
    (v_comment_row = LINE(113,x_mysql_fetch_array(v_comment_result)));
    (toBoolean((v_user_result = LINE(114,x_mysql_query(concat("SELECT rating FROM users WHERE id=", toString(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL))), v_link))))) || (toBoolean(f_exit("ERROR: Authentification query failed")));
    if (equal(LINE(115,x_mysql_num_rows(v_user_result)), 0LL)) print("<h3>ERROR: Sorry, but this user does not exist.</h3><br>\n");
    else (v_user_row = LINE(118,x_mysql_fetch_array(v_user_result)));
    (v_now = LINE(123,x_date("Y:m:d H:i:s")));
    (toBoolean((v_result = LINE(124,(assignCallTemp(eo_0, concat_rev(concat6(toString(v_commentId), ", ", toString(v_rating), ", '", toString(v_now), "')"), concat3("INSERT INTO moderator_log VALUES (NULL, ", toString(v_userId), ", "))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Failed to insert new rating in moderator_log.")));
    print((LINE(126,concat3("New comment rating is :", toString(v_comment_row.rvalAt("rating", 0x1337E0729FB407EFLL)), "<br>\n"))));
    print((LINE(127,concat3("New user rating is :", toString(v_user_row.rvalAt("rating", 0x1337E0729FB407EFLL)), "<br>\n"))));
    print("<center><h2>Your moderation has been successfully stored.</h2></center>\n");
  }
  LINE(131,x_mysql_close(v_link));
  LINE(133,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\r\n</html>\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
