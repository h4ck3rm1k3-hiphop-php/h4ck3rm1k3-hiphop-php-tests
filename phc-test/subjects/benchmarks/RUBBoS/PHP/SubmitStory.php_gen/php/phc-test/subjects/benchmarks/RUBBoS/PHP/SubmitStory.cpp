
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/SubmitStory.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$SubmitStory_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/SubmitStory.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$SubmitStory_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "SubmitStory.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  LINE(9,f_getdatabaselink(ref(v_link)));
  LINE(11,f_printhtmlheader("RUBBoS: Story submission"));
  print("<center><h2>Submit your incredible story !</h2><br>\n");
  print("<form action=\"StoreStory.php\" method=POST>\n<center><table>\n<tr><td><b>Nickname</b><td><input type=text size=20 name=nickname>\n<tr><td><b>Password</b><td><input type=text size=20 name=password>\n<tr><td><b>Story title</b><td><input type=text size=100 name=title>\n<tr><td><b>Category</b><td><SELECT name=category>\n");
  (toBoolean((v_result = LINE(20,x_mysql_query("SELECT * FROM categories", v_link))))) || (toBoolean(f_exit("ERROR: Query failed")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_row = LINE(21,x_mysql_fetch_array(v_result))))) {
      LOOP_COUNTER_CHECK(1);
      {
        print((LINE(23,concat5("<OPTION value=\"", toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "\">", toString(v_row.rvalAt("name", 0x0BCDB293DC3CBDDCLL)), "</OPTION>\n"))));
      }
    }
  }
  print("</SELECT></table><p><br>\n<TEXTAREA rows=\"20\" cols=\"80\" name=\"body\">Write your story here</TEXTAREA><br><p>\n<input type=submit value=\"Submit this story now!\"></center><p>\n");
  LINE(29,x_mysql_free_result(v_result));
  LINE(30,x_mysql_close(v_link));
  LINE(32,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
