
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/StoreComment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoreComment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/StoreComment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoreComment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_nickname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nickname") : g->GV(nickname);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_password __attribute__((__unused__)) = (variables != gVariables) ? variables->get("password") : g->GV(password);
  Variant &v_storyId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("storyId") : g->GV(storyId);
  Variant &v_parent __attribute__((__unused__)) = (variables != gVariables) ? variables->get("parent") : g->GV(parent);
  Variant &v_subject __attribute__((__unused__)) = (variables != gVariables) ? variables->get("subject") : g->GV(subject);
  Variant &v_body __attribute__((__unused__)) = (variables != gVariables) ? variables->get("body") : g->GV(body);
  Variant &v_comment_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_table") : g->GV(comment_table);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_userId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("userId") : g->GV(userId);
  Variant &v_now __attribute__((__unused__)) = (variables != gVariables) ? variables->get("now") : g->GV(now);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("table") : g->GV(table);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n<html>\r\n  <body>\r\n    ");
  (v_scriptName = "StoreComment.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_nickname = v_HTTP_POST_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
  if (equal(v_nickname, null)) {
    (v_nickname = v_HTTP_GET_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
  }
  (v_password = v_HTTP_POST_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
  if (equal(v_password, null)) {
    (v_password = v_HTTP_GET_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
  }
  (v_storyId = v_HTTP_POST_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
  if (equal(v_storyId, null)) {
    (v_storyId = v_HTTP_GET_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
    if (equal(v_storyId, null)) {
      LINE(27,f_printerror(v_scriptName, v_startTime, "StoreComment", "You must provide a story identifier!<br>"));
      f_exit();
    }
  }
  (v_parent = v_HTTP_POST_VARS.rvalAt("parent", 0x16E2F26FFB10FD8CLL));
  if (equal(v_parent, null)) {
    (v_parent = v_HTTP_GET_VARS.rvalAt("parent", 0x16E2F26FFB10FD8CLL));
    if (equal(v_parent, null)) {
      LINE(38,f_printerror(v_scriptName, v_startTime, "StoreComment", "You must provide a follow up identifier!<br>"));
      f_exit();
    }
  }
  (v_subject = v_HTTP_POST_VARS.rvalAt("subject", 0x0EDD9FCC9525C3B2LL));
  if (equal(v_subject, null)) {
    (v_subject = v_HTTP_GET_VARS.rvalAt("subject", 0x0EDD9FCC9525C3B2LL));
    if (equal(v_subject, null)) {
      LINE(49,f_printerror(v_scriptName, v_startTime, "StoreComment", "You must provide a comment subject!<br>"));
      f_exit();
    }
  }
  (v_body = v_HTTP_POST_VARS.rvalAt("body", 0x6F37689A8FD32EFALL));
  if (equal(v_body, null)) {
    (v_body = v_HTTP_GET_VARS.rvalAt("body", 0x6F37689A8FD32EFALL));
    if (equal(v_body, null)) {
      LINE(60,f_printerror(v_scriptName, v_startTime, "StoreComment", "<h3>You must provide a comment body!<br></h3>"));
      f_exit();
    }
  }
  (v_comment_table = v_HTTP_POST_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
  if (equal(v_comment_table, null)) {
    (v_comment_table = v_HTTP_GET_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
    if (equal(v_comment_table, null)) {
      LINE(71,f_printerror(v_scriptName, v_startTime, "Viewing comment", "You must provide a comment table!<br>"));
      f_exit();
    }
  }
  LINE(76,f_getdatabaselink(ref(v_link)));
  LINE(78,f_printhtmlheader("RUBBoS: Comment submission result"));
  print("<center><h2>Comment submission result:</h2></center><p>\n");
  (v_userId = LINE(83,f_authenticate(v_nickname, v_password, v_link)));
  if (equal(v_userId, 0LL)) print("Comment posted by the 'Anonymous Coward'<br>\n");
  else print((LINE(87,concat3("Comment posted by user #", toString(v_userId), "<br>\n"))));
  (v_now = LINE(90,x_date("Y:m:d H:i:s")));
  (toBoolean((v_result = LINE(91,(assignCallTemp(eo_0, concat_rev(concat6(toString(v_now), "', \"", toString(v_subject), "\", \"", toString(v_body), "\")"), concat_rev(concat6(toString(v_userId), ", ", toString(v_storyId), ", ", toString(v_parent), ", 0, 0, '"), concat3("INSERT INTO ", toString(v_comment_table), " VALUES (NULL, ")))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Failed to insert new comment in database.")));
  (toBoolean((v_result = LINE(92,(assignCallTemp(eo_0, concat4("UPDATE ", toString(v_comment_table), " SET childs=childs+1 WHERE id=", toString(v_parent))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Failed to update parent childs in database.")));
  print((LINE(94,concat3("Your comment has been successfully stored in the ", toString(v_table), " database table<br>\n"))));
  LINE(96,x_mysql_close(v_link));
  LINE(98,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\r\n</html>\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
