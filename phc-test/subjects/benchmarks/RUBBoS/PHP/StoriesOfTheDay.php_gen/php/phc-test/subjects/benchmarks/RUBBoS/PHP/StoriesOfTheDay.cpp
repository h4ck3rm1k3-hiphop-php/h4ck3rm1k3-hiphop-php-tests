
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/StoriesOfTheDay.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoriesOfTheDay_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/StoriesOfTheDay.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoriesOfTheDay_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_bodySizeLimit __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bodySizeLimit") : g->GV(bodySizeLimit);
  Variant &v_now __attribute__((__unused__)) = (variables != gVariables) ? variables->get("now") : g->GV(now);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_username __attribute__((__unused__)) = (variables != gVariables) ? variables->get("username") : g->GV(username);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "StoriesOfTheDay.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  LINE(9,f_getdatabaselink(ref(v_link)));
  LINE(11,f_printhtmlheader("RUBBoS stories of the day"));
  (v_bodySizeLimit = 512LL);
  (v_now = LINE(14,x_date("Y:m:d H:i:s")));
  (toBoolean((v_result = LINE(15,x_mysql_query("SELECT * FROM stories ORDER BY date DESC LIMIT 10", v_link))))) || (toBoolean(f_exit("ERROR: Query failed")));
  if (equal(LINE(16,x_mysql_num_rows(v_result)), 0LL)) print("<h2>Sorry, but there is no story available at this time.</h2><br>\n");
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_row = LINE(19,x_mysql_fetch_array(v_result))))) {
      LOOP_COUNTER_CHECK(1);
      {
        print("<br><hr>\n");
        LINE(22,f_printhtmlhighlighted(concat5("<a href=\"ViewStory.php\?storyId=", toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "\">", toString(v_row.rvalAt("title", 0x66AD900A2301E2FELL)), "</a>")));
        (v_username = LINE(23,f_getusername(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link)));
        print((LINE(24,concat5("<B>Posted by ", toString(v_username), " on ", toString(v_row.rvalAt("date", 0x58C7226464D57F09LL)), "</B><br>\n"))));
        if (more(LINE(25,x_strlen(toString(v_row.rvalAt("body", 0x6F37689A8FD32EFALL)))), v_bodySizeLimit)) {
          print((toString(LINE(27,x_substr(toString(v_row.rvalAt("body", 0x6F37689A8FD32EFALL)), toInt32(1LL), toInt32(v_bodySizeLimit))))));
          print("<br><B>...</B>");
        }
        else print((toString(v_row.rvalAt("body", 0x6F37689A8FD32EFALL))));
        print("<br>\n");
      }
    }
  }
  LINE(34,x_mysql_free_result(v_result));
  LINE(35,x_mysql_close(v_link));
  LINE(37,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
