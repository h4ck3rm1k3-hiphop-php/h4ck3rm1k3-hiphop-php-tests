
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 4:
      HASH_INDEX(0x12296005AB6E5904LL, HTTP_POST_VARS, 14);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 8:
      HASH_INDEX(0x2315022AC4CB4888LL, HTTP_GET_VARS, 16);
      HASH_INDEX(0x599E3F67E2599248LL, result, 25);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x07D68BF68C574E09LL, link, 22);
      break;
    case 12:
      HASH_INDEX(0x3E9D67504EE78ACCLL, month, 17);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      HASH_INDEX(0x04115BE1F73AED91LL, page, 19);
      break;
    case 18:
      HASH_INDEX(0x4307151CEB3C6312LL, row, 26);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 21);
      break;
    case 33:
      HASH_INDEX(0x000786008EFDF361LL, after, 24);
      break;
    case 34:
      HASH_INDEX(0x1963EAA154C4ADA2LL, before, 23);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 40:
      HASH_INDEX(0x0E12BE46FD64D268LL, day, 15);
      break;
    case 44:
      HASH_INDEX(0x1463CF5654FC95ACLL, username, 27);
      break;
    case 45:
      HASH_INDEX(0x5C5EEC0276AA952DLL, startTime, 13);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 50:
      HASH_INDEX(0x5B8402A024098F72LL, nbOfStories, 20);
      break;
    case 54:
      HASH_INDEX(0x28D044C5EA490CB6LL, year, 18);
      break;
    case 60:
      HASH_INDEX(0x487AC8B0170EAB3CLL, scriptName, 12);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 28) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
