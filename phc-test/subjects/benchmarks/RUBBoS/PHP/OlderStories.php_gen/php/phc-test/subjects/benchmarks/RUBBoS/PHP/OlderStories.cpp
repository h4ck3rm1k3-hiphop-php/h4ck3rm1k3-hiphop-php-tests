
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/OlderStories.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$OlderStories_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/OlderStories.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$OlderStories_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_day __attribute__((__unused__)) = (variables != gVariables) ? variables->get("day") : g->GV(day);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_month __attribute__((__unused__)) = (variables != gVariables) ? variables->get("month") : g->GV(month);
  Variant &v_year __attribute__((__unused__)) = (variables != gVariables) ? variables->get("year") : g->GV(year);
  Variant &v_page __attribute__((__unused__)) = (variables != gVariables) ? variables->get("page") : g->GV(page);
  Variant &v_nbOfStories __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nbOfStories") : g->GV(nbOfStories);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_before __attribute__((__unused__)) = (variables != gVariables) ? variables->get("before") : g->GV(before);
  Variant &v_after __attribute__((__unused__)) = (variables != gVariables) ? variables->get("after") : g->GV(after);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_username __attribute__((__unused__)) = (variables != gVariables) ? variables->get("username") : g->GV(username);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "OlderStories.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_day = v_HTTP_POST_VARS.rvalAt("day", 0x0E12BE46FD64D268LL));
  if (equal(v_day, null)) {
    (v_day = v_HTTP_GET_VARS.rvalAt("day", 0x0E12BE46FD64D268LL));
  }
  (v_month = v_HTTP_POST_VARS.rvalAt("month", 0x3E9D67504EE78ACCLL));
  if (equal(v_month, null)) {
    (v_month = v_HTTP_GET_VARS.rvalAt("month", 0x3E9D67504EE78ACCLL));
  }
  (v_year = v_HTTP_POST_VARS.rvalAt("year", 0x28D044C5EA490CB6LL));
  if (equal(v_year, null)) {
    (v_year = v_HTTP_GET_VARS.rvalAt("year", 0x28D044C5EA490CB6LL));
  }
  (v_page = v_HTTP_POST_VARS.rvalAt("page", 0x04115BE1F73AED91LL));
  if (equal(v_page, null)) {
    (v_page = v_HTTP_GET_VARS.rvalAt("page", 0x04115BE1F73AED91LL));
    if (equal(v_page, null)) (v_page = 0LL);
  }
  (v_nbOfStories = v_HTTP_POST_VARS.rvalAt("nbOfStories", 0x5B8402A024098F72LL));
  if (equal(v_nbOfStories, null)) {
    (v_nbOfStories = v_HTTP_GET_VARS.rvalAt("nbOfStories", 0x5B8402A024098F72LL));
    if (equal(v_nbOfStories, null)) (v_nbOfStories = 25LL);
  }
  LINE(43,f_printhtmlheader("RUBBoS Older Stories"));
  print("<form action=\"OlderStories.php\" method=POST>\n");
  print("<center><B>Date (day/month/year):</B><SELECT name=day>\n");
  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); less(v_i, 32LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      print((LINE(49,concat5("<OPTION value=\"", toString(v_i), "\">", toString(v_i), "</OPTION>\n"))));
    }
  }
  print("</SELECT>&nbsp/&nbsp<SELECT name=month>\n");
  {
    LOOP_COUNTER(2);
    for ((v_i = 1LL); less(v_i, 13LL); v_i++) {
      LOOP_COUNTER_CHECK(2);
      print((LINE(52,concat5("<OPTION value=\"", toString(v_i), "\">", toString(v_i), "</OPTION>\n"))));
    }
  }
  print("</SELECT>&nbsp/&nbsp<SELECT name=year>\n");
  {
    LOOP_COUNTER(3);
    for ((v_i = 2001LL); less(v_i, 2013LL); v_i++) {
      LOOP_COUNTER_CHECK(3);
      print((LINE(55,concat5("<OPTION value=\"", toString(v_i), "\">", toString(v_i), "</OPTION>\n"))));
    }
  }
  print("</SELECT><p><input type=submit value=\"Retrieve stories from this date!\"><p>\n");
  if ((equal(v_day, null)) || (equal(v_month, null)) || (equal(v_year, null))) print("<br><h2>Please select a date</h2><br>");
  else {
    print((concat("<br><h2>Stories of the ", LINE(63,concat6(toString(v_day), "/", toString(v_month), "/", toString(v_year), "</h2></center><br>")))));
    LINE(65,f_getdatabaselink(ref(v_link)));
    (v_before = LINE(66,concat6(toString(v_year), "-", toString(v_month), "-", toString(v_day), " 0:0:0")));
    (v_after = LINE(67,concat6(toString(v_year), "-", toString(v_month), "-", toString(v_day), " 23:59:59")));
    (toBoolean((v_result = LINE(68,(assignCallTemp(eo_0, concat("SELECT * FROM stories WHERE date>='", concat6(toString(v_before), "' AND date<='", toString(v_after), "' ORDER BY date DESC LIMIT ", toString(v_page * v_nbOfStories), toString(",") + toString(v_nbOfStories)))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
    if (equal(LINE(69,x_mysql_num_rows(v_result)), 0LL)) (toBoolean((v_result = LINE(70,(assignCallTemp(eo_0, concat("SELECT * FROM old_stories WHERE date>='", concat6(toString(v_before), "' AND date<='", toString(v_after), "' ORDER BY date DESC LIMIT ", toString(v_page * v_nbOfStories), toString(",") + toString(v_nbOfStories)))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
    if (equal(LINE(71,x_mysql_num_rows(v_result)), 0LL)) {
      if (equal(v_page, 0LL)) print("<h2>Sorry, but there are no story available for this date !</h2>");
      else {
        print("<h2>Sorry, but there is no more stories available for this date.</h2><br>\n");
        print((concat_rev(LINE(78,(assignCallTemp(eo_0, toString(v_month)),assignCallTemp(eo_2, toString(v_year)),assignCallTemp(eo_4, (toString(v_page - 1LL))),assignCallTemp(eo_5, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n</CENTER>\n")),concat6(eo_0, "&year=", eo_2, "&page=", eo_4, eo_5))), concat3("<p><CENTER>\n<a href=\"OlderStories.php\?day=", toString(v_day), "&month="))));
      }
      LINE(80,x_mysql_free_result(v_result));
      LINE(81,x_mysql_close(v_link));
      LINE(82,f_printhtmlfooter(v_scriptName, v_startTime));
      f_exit();
    }
    LOOP_COUNTER(4);
    {
      while (toBoolean((v_row = LINE(87,x_mysql_fetch_array(v_result))))) {
        LOOP_COUNTER_CHECK(4);
        {
          (v_username = LINE(89,f_getusername(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link)));
          print((concat_rev(LINE(90,concat6(toString(v_row.rvalAt("title", 0x66AD900A2301E2FELL)), "</a> by ", toString(v_username), " on ", toString(v_row.rvalAt("date", 0x58C7226464D57F09LL)), "<br>\n")), concat3("<a href=\"ViewStory.php\?storyId=", toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "\">"))));
        }
      }
    }
    if (equal(v_page, 0LL)) print((concat_rev(LINE(95,(assignCallTemp(eo_0, toString(v_month)),assignCallTemp(eo_2, toString(v_year)),assignCallTemp(eo_4, (toString(v_page + 1LL))),assignCallTemp(eo_5, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Next page</a>\n</CENTER>\n")),concat6(eo_0, "&year=", eo_2, "&page=", eo_4, eo_5))), concat3("<p><CENTER>\n<a href=\"OlderStories.php\?day=", toString(v_day), "&month="))));
    else print((concat_rev(LINE(98,(assignCallTemp(eo_1, (toString(v_page - 1LL))),assignCallTemp(eo_2, LINE(97,concat3("&nbOfStories=", toString(v_nbOfStories), "\">Previous page</a>\n&nbsp&nbsp&nbsp"))),assignCallTemp(eo_3, concat_rev(LINE(98,concat6(toString(v_day), "&month=", toString(v_month), "&year=", toString(v_year), "&page=")), concat3("<a href=\"OlderStories.php\?category=", toString(v_day), "="))),assignCallTemp(eo_4, (toString(v_page + 1LL))),assignCallTemp(eo_5, concat3("&nbOfStories=", toString(v_nbOfStories), "\">Next page</a>\n\n</CENTER>\n")),concat6("&page=", eo_1, eo_2, eo_3, eo_4, eo_5))), concat6("<p><CENTER>\n<a href=\"OlderStories.php\?day=", toString(v_day), "&month=", toString(v_month), "&year=", toString(v_year)))));
    LINE(100,x_mysql_free_result(v_result));
    LINE(101,x_mysql_close(v_link));
  }
  LINE(104,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
