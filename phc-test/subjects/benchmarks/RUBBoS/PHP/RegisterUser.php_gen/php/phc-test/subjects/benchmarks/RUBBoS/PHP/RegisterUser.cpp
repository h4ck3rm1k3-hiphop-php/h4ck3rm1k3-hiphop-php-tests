
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/RegisterUser.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$RegisterUser_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/RegisterUser.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$RegisterUser_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_firstname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("firstname") : g->GV(firstname);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_lastname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lastname") : g->GV(lastname);
  Variant &v_nickname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nickname") : g->GV(nickname);
  Variant &v_email __attribute__((__unused__)) = (variables != gVariables) ? variables->get("email") : g->GV(email);
  Variant &v_password __attribute__((__unused__)) = (variables != gVariables) ? variables->get("password") : g->GV(password);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_nicknameResult __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nicknameResult") : g->GV(nicknameResult);
  Variant &v_now __attribute__((__unused__)) = (variables != gVariables) ? variables->get("now") : g->GV(now);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "RegisterUser.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_firstname = v_HTTP_POST_VARS.rvalAt("firstname", 0x626B3E183DE13FF9LL));
  if (equal(v_firstname, null)) {
    (v_firstname = v_HTTP_GET_VARS.rvalAt("firstname", 0x626B3E183DE13FF9LL));
    if (equal(v_firstname, null)) {
      LINE(15,f_printerror(v_scriptName, v_startTime, "Register user", "You must provide a first name!<br>"));
      f_exit();
    }
  }
  (v_lastname = v_HTTP_POST_VARS.rvalAt("lastname", 0x6D09687EAA11A99BLL));
  if (equal(v_lastname, null)) {
    (v_lastname = v_HTTP_GET_VARS.rvalAt("lastname", 0x6D09687EAA11A99BLL));
    if (equal(v_lastname, null)) {
      LINE(26,f_printerror(v_scriptName, v_startTime, "Register user", "You must provide a last name!<br>"));
      f_exit();
    }
  }
  (v_nickname = v_HTTP_POST_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
  if (equal(v_nickname, null)) {
    (v_nickname = v_HTTP_GET_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
    if (equal(v_nickname, null)) {
      LINE(37,f_printerror(v_scriptName, v_startTime, "Register user", "You must provide a nick name!<br>"));
      f_exit();
    }
  }
  (v_email = v_HTTP_POST_VARS.rvalAt("email", 0x38A4B57362FD20A6LL));
  if (equal(v_email, null)) {
    (v_email = v_HTTP_GET_VARS.rvalAt("email", 0x38A4B57362FD20A6LL));
    if (equal(v_email, null)) {
      LINE(48,f_printerror(v_scriptName, v_startTime, "Register user", "You must provide an email address!<br>"));
      f_exit();
    }
  }
  (v_password = v_HTTP_POST_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
  if (equal(v_password, null)) {
    (v_password = v_HTTP_GET_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
    if (equal(v_password, null)) {
      LINE(59,f_printerror(v_scriptName, v_startTime, "Register user", "You must provide a password!<br>"));
      f_exit();
    }
  }
  LINE(64,f_getdatabaselink(ref(v_link)));
  (toBoolean((v_nicknameResult = LINE(67,(assignCallTemp(eo_0, concat3("SELECT * FROM users WHERE nickname=\"", toString(v_nickname), "\"")),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Nickname query failed")));
  if (more(LINE(68,x_mysql_num_rows(v_nicknameResult)), 0LL)) {
    LINE(70,f_printerror(v_scriptName, v_startTime, "Register user", "The nickname you have choosen is already taken by someone else. Please choose a new nickname.<br>\n"));
    LINE(71,x_mysql_free_result(v_nicknameResult));
    f_exit();
  }
  LINE(74,x_mysql_free_result(v_nicknameResult));
  (v_now = LINE(77,x_date("Y:m:d H:i:s")));
  (toBoolean((v_result = LINE(78,(assignCallTemp(eo_0, concat_rev(concat6(toString(v_password), "\", \"", toString(v_email), "\", 0, 0, '", toString(v_now), "')"), concat("INSERT INTO users VALUES (NULL, \"", concat6(toString(v_firstname), "\", \"", toString(v_lastname), "\", \"", toString(v_nickname), "\", \"")))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Failed to insert new user in database.")));
  (toBoolean((v_result = LINE(80,(assignCallTemp(eo_0, concat3("SELECT * FROM users WHERE nickname=\"", toString(v_nickname), "\"")),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query user failed")));
  (v_row = LINE(81,x_mysql_fetch_array(v_result)));
  LINE(83,f_printhtmlheader(toString("RUBBoS: Welcome to ") + toString(v_nickname)));
  print("<h2>Your registration has been processed successfully</h2><br>\n");
  print((LINE(85,concat3("<h3>Welcome ", toString(v_nickname), "</h3>\n"))));
  print("RUBBoS has stored the following information about you:<br>\n");
  print((LINE(87,concat3("First Name : ", toString(v_row.rvalAt("firstname", 0x626B3E183DE13FF9LL)), "<br>\n"))));
  print((LINE(88,concat3("Last Name  : ", toString(v_row.rvalAt("lastname", 0x6D09687EAA11A99BLL)), "<br>\n"))));
  print((LINE(89,concat3("Nick Name  : ", toString(v_row.rvalAt("nickname", 0x2D36EB6F33D16B58LL)), "<br>\n"))));
  print((LINE(90,concat3("Email      : ", toString(v_row.rvalAt("email", 0x38A4B57362FD20A6LL)), "<br>\n"))));
  print((LINE(91,concat3("Password   : ", toString(v_row.rvalAt("password", 0x6AD1FAB93F84D08FLL)), "<br>\n"))));
  print("<br>The following information has been automatically generated by RUBBoS:<br>\n");
  print((LINE(93,concat3("User id       :", toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "<br>\n"))));
  print((LINE(94,concat3("Creation date :", toString(v_row.rvalAt("creation_date", 0x42DD75F58EEE3297LL)), "<br>\n"))));
  print((LINE(95,concat3("Rating        :", toString(v_row.rvalAt("rating", 0x1337E0729FB407EFLL)), "<br>\n"))));
  print((LINE(96,concat3("Access        :", toString(v_row.rvalAt("access", 0x432ABF90750CDA3BLL)), "<br>\n"))));
  LINE(98,x_mysql_free_result(v_result));
  LINE(99,x_mysql_close(v_link));
  LINE(101,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
