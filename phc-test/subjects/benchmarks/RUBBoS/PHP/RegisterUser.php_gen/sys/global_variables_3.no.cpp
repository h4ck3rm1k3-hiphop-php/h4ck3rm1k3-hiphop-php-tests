
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 4:
      HASH_INDEX(0x12296005AB6E5904LL, HTTP_POST_VARS, 14);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x1112F727DCE2BE06LL, now, 23);
      break;
    case 8:
      HASH_INDEX(0x2315022AC4CB4888LL, HTTP_GET_VARS, 16);
      HASH_INDEX(0x599E3F67E2599248LL, result, 24);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x07D68BF68C574E09LL, link, 21);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x6AD1FAB93F84D08FLL, password, 20);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 18:
      HASH_INDEX(0x4307151CEB3C6312LL, row, 25);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 24:
      HASH_INDEX(0x2D36EB6F33D16B58LL, nickname, 18);
      HASH_INDEX(0x333C32D194C7D3D8LL, nicknameResult, 22);
      break;
    case 27:
      HASH_INDEX(0x6D09687EAA11A99BLL, lastname, 17);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 38:
      HASH_INDEX(0x38A4B57362FD20A6LL, email, 19);
      break;
    case 45:
      HASH_INDEX(0x5C5EEC0276AA952DLL, startTime, 13);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 57:
      HASH_INDEX(0x626B3E183DE13FF9LL, firstname, 15);
      break;
    case 60:
      HASH_INDEX(0x487AC8B0170EAB3CLL, scriptName, 12);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 26) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
