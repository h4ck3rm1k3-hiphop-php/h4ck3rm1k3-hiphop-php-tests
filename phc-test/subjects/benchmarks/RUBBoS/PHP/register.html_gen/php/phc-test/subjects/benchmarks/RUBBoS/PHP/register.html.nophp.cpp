
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/register.html.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$register_html(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/register.html);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$register_html;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n<html>\n<head>\n   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n   <meta name=\"GENERATOR\" content=\"Mozilla/4.72 [en] (X11; U; Linux 2.2.14-5.0 i686) [Netscape]\">\n   <meta name=\"Author\" content=\"Emmanuel Cecchet\">\n   <title>RUBBoS: Rice University Bulletin Board System</title>\n</head>\n<body text=\"#000000\" bgcolor=\"#FFFFF");
  echo("F\" link=\"#0000EE\" vlink=\"#551A8B\" alink=\"#FF0000\">\n&nbsp;\n<center><table COLS=7 WIDTH=\"100%\" NOSAVE >\n<tr NOSAVE>\n<td NOSAVE>\n<center><IMG SRC=\"RUBBoS_logo.jpg\" height=68 width=200 align=ABSCENTER><br>PHP version</center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"StoriesOfTheDay.php\">Home</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"register.html\">Register</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"browse.html\">Bro");
  echo("wse</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"Search.php\">Search</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"SubmitStory.php\">Submit a story</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"author.html\">Author</a></h2></center>\n</td>\n</tr>\n</table></center>\n<br>\n<h2>Welcome to RUBBoS online registration</h2></center>\n\n<p><p><br>\n<center>\n<font color=\"#FF0000\">\nIt's completely free !!!\nJust fill the following form");
  echo(" to register and get your account !\n</font>\n\n<table>\n<form action=\"RegisterUser.php\" method=POST>\n<tr><td>\nFirst Name:\n<td>\n<input type=text size=20 name=firstname>\n<tr><td>\nLast Name:\n<td>\n<input type=text size=20 name=lastname>\n<tr><td>\nNick Name:\n<td>\n<input type=text size=20 name=nickname>\n<tr><td>\nPassword:\n<td>\n<input type=password size=20 name=password>\n<tr><td>\nEmail address:\n<td>\n<input type=text size=50 name=e");
  echo("mail>\n</table></center>\n<br>\n<center><input type=submit value=\"Register now!\"></center>\n<br>\n<p>\n\n<hr WIDTH=\"100%\">\n<br><i>RUBBoS (C) 2001 - Rice University/INRIA</i>\n</body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
