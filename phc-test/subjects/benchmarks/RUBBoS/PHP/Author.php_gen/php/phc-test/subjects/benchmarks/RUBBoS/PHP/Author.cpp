
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/Author.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$Author_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/Author.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$Author_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_nickname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nickname") : g->GV(nickname);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_password __attribute__((__unused__)) = (variables != gVariables) ? variables->get("password") : g->GV(password);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_userId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("userId") : g->GV(userId);
  Variant &v_access __attribute__((__unused__)) = (variables != gVariables) ? variables->get("access") : g->GV(access);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "Author.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_nickname = v_HTTP_POST_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
  if (equal(v_nickname, null)) {
    (v_nickname = v_HTTP_GET_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
    if (equal(v_nickname, null)) {
      LINE(15,f_printerror(v_scriptName, v_startTime, "Author", "You must provide a nick name!<br>"));
      f_exit();
    }
  }
  (v_password = v_HTTP_POST_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
  if (equal(v_password, null)) {
    (v_password = v_HTTP_GET_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
    if (equal(v_password, null)) {
      LINE(26,f_printerror(v_scriptName, v_startTime, "Author", "You must provide a password!<br>"));
      f_exit();
    }
  }
  LINE(31,f_getdatabaselink(ref(v_link)));
  (v_userId = 0LL);
  (v_access = 0LL);
  if ((!equal(v_nickname, null)) && (!equal(v_password, null))) {
    (toBoolean((v_result = LINE(38,(assignCallTemp(eo_0, concat5("SELECT id,access FROM users WHERE nickname=\"", toString(v_nickname), "\" AND password=\"", toString(v_password), "\"")),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Authentification query failed")));
    if (!equal(LINE(39,x_mysql_num_rows(v_result)), 0LL)) {
      (v_row = LINE(41,x_mysql_fetch_array(v_result)));
      (v_userId = v_row.rvalAt("id", 0x028B9FE0C4522BE2LL));
      (v_access = v_row.rvalAt("access", 0x432ABF90750CDA3BLL));
    }
    LINE(45,x_mysql_free_result(v_result));
  }
  if ((equal(v_userId, 0LL)) || (equal(v_access, 0LL))) {
    LINE(50,f_printhtmlheader("RUBBoS: Author page"));
    print("<p><center><h2>Sorry, but this feature is only accessible by users with an author access.</h2></center><p>\n");
  }
  else {
    LINE(55,f_printhtmlheader("RUBBoS: Author page"));
    print((concat("<p><center><h2>Which administrative task do you want to do \?</h2></center>\n", LINE(57,concat3("<p><p><a href=\"ReviewStories.php\?authorId=", toString(v_userId), "\">Review submitted stories</a><br>\n")))));
  }
  LINE(60,x_mysql_close(v_link));
  LINE(62,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
