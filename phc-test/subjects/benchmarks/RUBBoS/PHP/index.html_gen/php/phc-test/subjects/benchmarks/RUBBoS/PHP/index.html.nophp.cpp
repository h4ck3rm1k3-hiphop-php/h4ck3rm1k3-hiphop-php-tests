
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/index.html.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$index_html(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/index.html);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$index_html;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n<html>\n<head>\n   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n   <meta name=\"GENERATOR\" content=\"Mozilla/4.72 [en] (X11; U; Linux 2.2.14-5.0 i686) [Netscape]\">\n   <meta name=\"Author\" content=\"Emmanuel Cecchet\">\n   <title>RUBBoS: Rice University Bulletin Board System</title>\n</head>\n<body text=\"#000000\" bgcolor=\"#FFFFF");
  echo("F\" link=\"#0000EE\" vlink=\"#551A8B\" alink=\"#FF0000\">\n&nbsp;\n<center><table COLS=7 WIDTH=\"100%\" NOSAVE >\n<tr NOSAVE>\n<td NOSAVE>\n<center><IMG SRC=\"RUBBoS_logo.jpg\" height=68 width=200 align=ABSCENTER><br>PHP version</center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"StoriesOfTheDay.php\">Home</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"register.html\">Register</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"browse.html\">Bro");
  echo("wse</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"Search.php\">Search</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"SubmitStory.php\">Submit a story</a></h2></center>\n</td>\n\n<td>\n<center>\n<h2>\n<a href=\"author.html\">Author</a></h2></center>\n</td>\n</tr>\n</table></center>\n<br>\n<center>\n<h2>\nWelcome to RUBBoS's home page !</h2></center>\n\n<p><br>RUBBoS is a bulletin board system prototype that is used to evaluate the\nbottleneck");
  echo("s of such application.\n<br>This version is the <b><blink>PHP</blink></b> implementation of RUBBoS.\n<br>&nbsp;\n<br>&nbsp;\n<h3>\nHow to use RUBBoS</h3>\nRUBBoS can be used from a web browser for testing purposes or with the provided\nbenchmarking tools.\n<br>Here is how to use RUBBoS from your web browser :\n<p>1. If you are lost, at any time just click on the <b><i>Home</i></b>\nlink that brings you back to this page.\n");
  echo("<br>2. You first have to register yourself as a new user by selecting\n<b><i>Register</i></b>\n<br>3. You can browse the stories and comments using <b><i>Browse</i></b> or <b><i>Search</i></b>.\n<br>4. Select <b><i>Submit a story</i></b> if you want to submit a new story.\n<br>5. The <b><i>Author</i></b> link gives you a report of your personal\ninformation and the current items you are selling or bidding on.\n<p>Good luck !\n<p>");
  echo("\n<hr WIDTH=\"100%\">\n<br><i>RUBBoS (C) 2001 - Rice University/INRIA</i>\n</body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
