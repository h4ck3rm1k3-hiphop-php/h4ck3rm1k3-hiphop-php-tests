
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "scriptName",
    "startTime",
    "HTTP_POST_VARS",
    "storyId",
    "HTTP_GET_VARS",
    "link",
    "result",
    "comment_table",
    "row",
    "username",
    "count_result",
    "i",
    "count_row",
    "filter",
    "display",
    "comment",
    "comment_row",
  };
  if (idx >= 0 && idx < 29) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(scriptName);
    case 13: return GV(startTime);
    case 14: return GV(HTTP_POST_VARS);
    case 15: return GV(storyId);
    case 16: return GV(HTTP_GET_VARS);
    case 17: return GV(link);
    case 18: return GV(result);
    case 19: return GV(comment_table);
    case 20: return GV(row);
    case 21: return GV(username);
    case 22: return GV(count_result);
    case 23: return GV(i);
    case 24: return GV(count_row);
    case 25: return GV(filter);
    case 26: return GV(display);
    case 27: return GV(comment);
    case 28: return GV(comment_row);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
