
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/RejectStory.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$RejectStory_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/RejectStory.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$RejectStory_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_storyId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("storyId") : g->GV(storyId);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "RejectStory.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_storyId = v_HTTP_POST_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
  if (equal(v_storyId, null)) {
    (v_storyId = v_HTTP_GET_VARS.rvalAt("storyId", 0x58F24C51C564718DLL));
    if (equal(v_storyId, null)) {
      LINE(15,f_printerror(v_scriptName, v_startTime, "RejectStory", "<h3>You must provide a story identifier !<br></h3>"));
      f_exit();
    }
  }
  LINE(20,f_getdatabaselink(ref(v_link)));
  LINE(22,f_printhtmlheader("RUBBoS: Story submission result"));
  print("<center><h2>Story submission result:</h2></center><p>\n");
  (toBoolean((v_result = LINE(26,x_mysql_query(toString("SELECT id FROM submissions WHERE id=") + toString(v_storyId)))))) || (toBoolean(f_exit("ERROR: Query failed")));
  if (equal(LINE(27,x_mysql_num_rows(v_result)), 0LL)) f_exit("<h3>ERROR: Sorry, but this story does not exist.</h3><br>\n");
  LINE(31,x_mysql_query(toString("DELETE FROM submissions WHERE id=") + toString(v_storyId), v_link));
  print("The story has been successfully removed from the submissions database table<br>\n");
  LINE(35,x_mysql_close(v_link));
  LINE(37,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
