
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/BrowseCategories.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$BrowseCategories_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/BrowseCategories.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$BrowseCategories_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "BrowseCategories.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  LINE(9,f_getdatabaselink(ref(v_link)));
  LINE(11,f_printhtmlheader("RUBBoS available categories"));
  (toBoolean((v_result = LINE(13,x_mysql_query("SELECT * FROM categories", v_link))))) || (toBoolean(f_exit("ERROR: Query failed")));
  if (equal(LINE(14,x_mysql_num_rows(v_result)), 0LL)) print("<h2>Sorry, but there is no category available at this time. Database table is empty</h2><br>\n");
  else print("<h2>Currently available categories</h2><br>\n");
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_row = LINE(19,x_mysql_fetch_array(v_result))))) {
      LOOP_COUNTER_CHECK(1);
      {
        print((concat("<a href=\"BrowseStoriesByCategory.php\?category=", LINE(21,(assignCallTemp(eo_0, toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL))),assignCallTemp(eo_2, x_urlencode(toString(v_row.rvalAt("name", 0x0BCDB293DC3CBDDCLL)))),assignCallTemp(eo_4, toString(v_row.rvalAt("name", 0x0BCDB293DC3CBDDCLL))),concat6(eo_0, "&categoryName=", eo_2, "\">", eo_4, "</a><br>\n"))))));
      }
    }
  }
  LINE(23,x_mysql_free_result(v_result));
  LINE(24,x_mysql_close(v_link));
  LINE(26,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
