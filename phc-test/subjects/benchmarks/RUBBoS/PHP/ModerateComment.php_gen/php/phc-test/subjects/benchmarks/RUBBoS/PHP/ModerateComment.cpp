
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/ModerateComment.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$ModerateComment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/ModerateComment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$ModerateComment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_comment_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comment_table") : g->GV(comment_table);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_commentId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("commentId") : g->GV(commentId);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_username __attribute__((__unused__)) = (variables != gVariables) ? variables->get("username") : g->GV(username);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "ModerateComment.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_comment_table = v_HTTP_POST_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
  if (equal(v_comment_table, null)) {
    (v_comment_table = v_HTTP_GET_VARS.rvalAt("comment_table", 0x2D9556DD57FAE1B3LL));
    if (equal(v_comment_table, null)) {
      LINE(15,f_printerror(v_scriptName, v_startTime, "Moderating comment", "You must provide a comment table!<br>"));
      f_exit();
    }
  }
  (v_commentId = v_HTTP_POST_VARS.rvalAt("commentId", 0x2BBDDCC3BCEE6282LL));
  if (equal(v_commentId, null)) {
    (v_commentId = v_HTTP_GET_VARS.rvalAt("commentId", 0x2BBDDCC3BCEE6282LL));
    if (equal(v_commentId, null)) {
      LINE(26,f_printerror(v_scriptName, v_startTime, "Moderating comment", "You must provide a comment identifier!<br>"));
      f_exit();
    }
  }
  LINE(31,f_getdatabaselink(ref(v_link)));
  LINE(32,f_printhtmlheader("RUBBoS: Comment moderation"));
  (toBoolean((v_result = LINE(34,(assignCallTemp(eo_0, concat4("SELECT * FROM ", toString(v_comment_table), " WHERE id=", toString(v_commentId))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Query failed")));
  if (equal(LINE(35,x_mysql_num_rows(v_result)), 0LL)) f_exit("<h3>ERROR: Sorry, but this comment does not exist.</h3><br>\n");
  (v_row = LINE(37,x_mysql_fetch_array(v_result)));
  print("<p><br><center><h2>Moderate a comment !</h2></center><br>\n<br><hr><br>");
  (v_username = LINE(40,f_getusername(v_row.rvalAt("writer", 0x26C350F13DAE7F88LL), v_link)));
  print((concat_rev(LINE(41,concat6(toString(v_row.rvalAt("id", 0x028B9FE0C4522BE2LL)), "\">", toString(v_row.rvalAt("subject", 0x0EDD9FCC9525C3B2LL)), "</a></B>&nbsp</FONT> (Score:", toString(v_row.rvalAt("rating", 0x1337E0729FB407EFLL)), ")</center></TABLE>\n")), concat5("<TABLE width=\"100%\" bgcolor=\"#CCCCFF\"><TR><TD><FONT size=\"4\" color=\"#000000\"><center><B><a href=\"ViewComment.php\?comment_table=", toString(v_comment_table), "&storyId=", toString(v_row.rvalAt("storyId", 0x58F24C51C564718DLL)), "&commentId="))));
  print((LINE(42,concat5("<TABLE><TR><TD><B>Posted by ", toString(v_username), " on ", toString(v_row.rvalAt("date", 0x58C7226464D57F09LL)), "</B><p>\n"))));
  print((LINE(55,(assignCallTemp(eo_1, toString(v_row.rvalAt("comment", 0x044C0F716BE4D0ADLL))),assignCallTemp(eo_3, LINE(45,concat3("<input type=hidden name=commentId value=", toString(v_commentId), ">\n"))),assignCallTemp(eo_4, LINE(55,concat3("<input type=hidden name=comment_table value=", toString(v_comment_table), ">\n<center><table>\n<tr><td><b>Nickname</b><td><input type=text size=20 name=nickname>\n<tr><td><b>Password</b><td><input type=text size=20 name=password>\n<tr><td><b>Rating</b><td><SELECT name=rating>\n<OPTION value=\"-1\">-1: Offtopic</OPTION>\n<OPTION selected value=\"0\">0: Not rated</OPTION>\n<OPTION value=\"1\">1: Interesting</OPTION>\n</SELECT></table><p><br>\n<input type=submit value=\"Moderate this comment now!\"></center><p>\n"))),concat5("<TR><TD>", eo_1, "</TABLE><p><hr><p>\n<form action=\"StoreModeratorLog.php\" method=POST>\n", eo_3, eo_4)))));
  LINE(57,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
