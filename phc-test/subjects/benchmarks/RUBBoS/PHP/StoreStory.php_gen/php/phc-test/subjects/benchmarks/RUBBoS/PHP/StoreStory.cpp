
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/PHPprinter.h>
#include <php/phc-test/subjects/benchmarks/RUBBoS/PHP/StoreStory.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoreStory_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/RUBBoS/PHP/StoreStory.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$StoreStory_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_scriptName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scriptName") : g->GV(scriptName);
  Variant &v_startTime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("startTime") : g->GV(startTime);
  Variant &v_HTTP_POST_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_POST_VARS") : g->GV(HTTP_POST_VARS);
  Variant &v_nickname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nickname") : g->GV(nickname);
  Variant &v_HTTP_GET_VARS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("HTTP_GET_VARS") : g->GV(HTTP_GET_VARS);
  Variant &v_password __attribute__((__unused__)) = (variables != gVariables) ? variables->get("password") : g->GV(password);
  Variant &v_title __attribute__((__unused__)) = (variables != gVariables) ? variables->get("title") : g->GV(title);
  Variant &v_body __attribute__((__unused__)) = (variables != gVariables) ? variables->get("body") : g->GV(body);
  Variant &v_category __attribute__((__unused__)) = (variables != gVariables) ? variables->get("category") : g->GV(category);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_userId __attribute__((__unused__)) = (variables != gVariables) ? variables->get("userId") : g->GV(userId);
  Variant &v_access __attribute__((__unused__)) = (variables != gVariables) ? variables->get("access") : g->GV(access);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("table") : g->GV(table);
  Variant &v_now __attribute__((__unused__)) = (variables != gVariables) ? variables->get("now") : g->GV(now);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html>\n  <body>\n    ");
  (v_scriptName = "SubmitStory.php");
  LINE(6,pm_php$phc_test$subjects$benchmarks$RUBBoS$PHP$PHPprinter_php(false, variables));
  (v_startTime = LINE(7,f_getmicrotime()));
  (v_nickname = v_HTTP_POST_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
  if (equal(v_nickname, null)) {
    (v_nickname = v_HTTP_GET_VARS.rvalAt("nickname", 0x2D36EB6F33D16B58LL));
  }
  (v_password = v_HTTP_POST_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
  if (equal(v_password, null)) {
    (v_password = v_HTTP_GET_VARS.rvalAt("password", 0x6AD1FAB93F84D08FLL));
  }
  (v_title = v_HTTP_POST_VARS.rvalAt("title", 0x66AD900A2301E2FELL));
  if (equal(v_title, null)) {
    (v_title = v_HTTP_GET_VARS.rvalAt("title", 0x66AD900A2301E2FELL));
    if (equal(v_title, null)) {
      LINE(27,f_printerror(v_scriptName, v_startTime, "SubmitStory", "You must provide a story title!<br>"));
      f_exit();
    }
  }
  (v_body = v_HTTP_POST_VARS.rvalAt("body", 0x6F37689A8FD32EFALL));
  if (equal(v_body, null)) {
    (v_body = v_HTTP_GET_VARS.rvalAt("body", 0x6F37689A8FD32EFALL));
    if (equal(v_body, null)) {
      LINE(38,f_printerror(v_scriptName, v_startTime, "SubmitStory", "<h3>You must provide a story body!<br></h3>"));
      f_exit();
    }
  }
  (v_category = v_HTTP_POST_VARS.rvalAt("category", 0x684FAD5C8F6EC8DELL));
  if (equal(v_category, null)) {
    (v_category = v_HTTP_GET_VARS.rvalAt("category", 0x684FAD5C8F6EC8DELL));
    if (equal(v_category, null)) {
      LINE(49,f_printerror(v_scriptName, v_startTime, "SubmitStory", "<h3>You must provide a category !<br></h3>"));
      f_exit();
    }
  }
  LINE(54,f_getdatabaselink(ref(v_link)));
  LINE(56,f_printhtmlheader("RUBBoS: Story submission result"));
  print("<center><h2>Story submission result:</h2></center><p>\n");
  (v_userId = 0LL);
  (v_access = 0LL);
  if ((!equal(v_nickname, null)) && (!equal(v_password, null))) {
    (toBoolean((v_result = LINE(65,(assignCallTemp(eo_0, concat5("SELECT id,access FROM users WHERE nickname=\"", toString(v_nickname), "\" AND password=\"", toString(v_password), "\"")),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Authentication query failed")));
    if (!equal(LINE(66,x_mysql_num_rows(v_result)), 0LL)) {
      (v_row = LINE(68,x_mysql_fetch_array(v_result)));
      (v_userId = v_row.rvalAt("id", 0x028B9FE0C4522BE2LL));
      (v_access = v_row.rvalAt("access", 0x432ABF90750CDA3BLL));
    }
    LINE(72,x_mysql_free_result(v_result));
  }
  (v_table = "submissions");
  if (equal(v_userId, 0LL)) print("Story stored by the 'Anonymous Coward'<br>\n");
  else {
    if (equal(v_access, 0LL)) print((LINE(81,concat3("Story submitted by regular user #", toString(v_userId), "<br>\n"))));
    else {
      print((LINE(84,concat3("Story posted by author #", toString(v_userId), "<br>\n"))));
      (v_table = "stories");
    }
  }
  (v_now = LINE(90,x_date("Y:m:d H:i:s")));
  (toBoolean((v_result = LINE(91,(assignCallTemp(eo_0, concat_rev(concat6(toString(v_now), "', ", toString(v_userId), ", ", toString(v_category), ")"), concat("INSERT INTO ", concat6(toString(v_table), " VALUES (NULL, \"", toString(v_title), "\", \"", toString(v_body), "\", '")))),assignCallTemp(eo_1, v_link),x_mysql_query(eo_0, eo_1)))))) || (toBoolean(f_exit("ERROR: Failed to insert new story in database.")));
  print((LINE(93,concat3("Your story has been successfully stored in the ", toString(v_table), " database table<br>\n"))));
  LINE(95,x_mysql_close(v_link));
  LINE(97,f_printhtmlfooter(v_scriptName, v_startTime));
  echo("  </body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
