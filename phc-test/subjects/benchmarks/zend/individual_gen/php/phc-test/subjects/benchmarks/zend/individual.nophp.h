
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/zend/individual.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$zend$individual(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_nophp_h__
