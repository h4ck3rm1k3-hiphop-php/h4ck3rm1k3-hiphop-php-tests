
#include <php/phc-test/subjects/benchmarks/zend/alloc_micro.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$zend$alloc_micro_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/zend/alloc_micro.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$zend$alloc_micro_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_start __attribute__((__unused__)) = (variables != gVariables) ? variables->get("start") : g->GV(start);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_stop __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stop") : g->GV(stop);

  (v_start = LINE(2,x_microtime(true)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 1000000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_x.set(v_i, ("xxxxx"));
      }
    }
  }
  (v_stop = LINE(9,x_microtime(true)));
  print(concat((toString(v_stop - v_start)), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
