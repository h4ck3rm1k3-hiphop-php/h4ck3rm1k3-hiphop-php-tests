
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_total(CArrRef params);
Variant i_simpleudcall(CArrRef params);
static Variant invoke_case_0(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x2AB70DFF89534220LL, total);
  HASH_INVOKE(0x1E712E44E8088C40LL, simpleudcall);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_hallo(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_mandel2(CArrRef params);
Variant i_simple(CArrRef params);
static Variant invoke_case_6(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x3B9158C750389CC6LL, mandel2);
  HASH_INVOKE(0x03BCC3139BAF14A6LL, simple);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_simpleucall(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_simplecall(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_mandel(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[16])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 16; i++) funcTable[i] = &invoke_builtin;
    funcTable[0] = &invoke_case_0;
    funcTable[3] = &i_hallo;
    funcTable[6] = &invoke_case_6;
    funcTable[7] = &i_simpleucall;
    funcTable[12] = &i_simplecall;
    funcTable[14] = &i_mandel;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 15](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
