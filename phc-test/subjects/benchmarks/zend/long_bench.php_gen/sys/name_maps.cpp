
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "ary", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "ary2", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "ary3", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "end_test", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "fibo", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "hallo", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "hallo2", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "hash1", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "hash2", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "mandel", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "matrix", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "mmult", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "sieve", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "simple", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "simplecall", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "simpleucall", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "simpleudcall", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "start_test", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "strcat", "phc-test/subjects/benchmarks/zend/long_bench.php",
  "total", "phc-test/subjects/benchmarks/zend/long_bench.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
