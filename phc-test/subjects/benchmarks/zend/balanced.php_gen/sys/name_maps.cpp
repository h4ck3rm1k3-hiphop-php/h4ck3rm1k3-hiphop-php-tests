
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/balanced.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/balanced.php",
  "ary", "phc-test/subjects/benchmarks/zend/balanced.php",
  "ary2", "phc-test/subjects/benchmarks/zend/balanced.php",
  "ary3", "phc-test/subjects/benchmarks/zend/balanced.php",
  "end_test", "phc-test/subjects/benchmarks/zend/balanced.php",
  "fibo", "phc-test/subjects/benchmarks/zend/balanced.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/balanced.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/balanced.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/balanced.php",
  "hash1", "phc-test/subjects/benchmarks/zend/balanced.php",
  "hash2", "phc-test/subjects/benchmarks/zend/balanced.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/balanced.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/balanced.php",
  "mandel", "phc-test/subjects/benchmarks/zend/balanced.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/balanced.php",
  "matrix", "phc-test/subjects/benchmarks/zend/balanced.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/balanced.php",
  "mmult", "phc-test/subjects/benchmarks/zend/balanced.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/balanced.php",
  "sieve", "phc-test/subjects/benchmarks/zend/balanced.php",
  "start_test", "phc-test/subjects/benchmarks/zend/balanced.php",
  "strcat", "phc-test/subjects/benchmarks/zend/balanced.php",
  "total", "phc-test/subjects/benchmarks/zend/balanced.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
