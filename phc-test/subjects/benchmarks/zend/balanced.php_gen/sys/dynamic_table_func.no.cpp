
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_total(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_mandel2(CArrRef params);
Variant i_mandel(CArrRef params);
static Variant invoke_case_6(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x3B9158C750389CC6LL, mandel2);
  HASH_INVOKE(0x5BBF7B8D4530483ELL, mandel);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[8])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 8; i++) funcTable[i] = &invoke_builtin;
    funcTable[0] = &i_total;
    funcTable[6] = &invoke_case_6;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 7](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
