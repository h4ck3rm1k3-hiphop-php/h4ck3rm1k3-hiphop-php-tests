
#include <php/phc-test/subjects/benchmarks/zend/template.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_IA = 3877LL;
const int64 k_IC = 29573LL;
const int64 k_IM = 139968LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 305 */
void f_sieve(Variant v_n) {
  FUNCTION_INJECTION(sieve);
  int64 v_count = 0;
  Variant v_flags;
  int64 v_i = 0;
  int64 v_k = 0;

  (v_count = 0LL);
  LOOP_COUNTER(1);
  {
    while (more(v_n--, 0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_count = 0LL);
        (v_flags = LINE(309,x_range(0LL, 8192LL)));
        {
          LOOP_COUNTER(2);
          for ((v_i = 2LL); less(v_i, 8193LL); v_i++) {
            LOOP_COUNTER_CHECK(2);
            {
              if (more(v_flags.rvalAt(v_i), 0LL)) {
                {
                  LOOP_COUNTER(3);
                  for ((v_k = v_i + v_i); not_more(v_k, 8192LL); v_k += v_i) {
                    LOOP_COUNTER_CHECK(3);
                    {
                      v_flags.set(v_k, (0LL));
                    }
                  }
                }
                v_count++;
              }
            }
          }
        }
      }
    }
  }
  print(LINE(319,concat3("Count: ", toString(v_count), "\n")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 347 */
PlusOperand f_end_test(CVarRef v_start, CVarRef v_name) {
  FUNCTION_INJECTION(end_test);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_total __attribute__((__unused__)) = g->GV(total);
  PlusOperand v_end = 0;
  String v_num;
  String v_pad;

  (v_end = LINE(350,f_getmicrotime()));
  LINE(351,x_ob_end_clean());
  gv_total += v_end - v_start;
  (v_num = LINE(353,x_number_format(toDouble(v_end - v_start), toInt32(3LL))));
  (v_pad = LINE(354,(assignCallTemp(eo_1, toInt32(minus_rev(x_strlen(v_num), 24LL - x_strlen(toString(v_name))))),x_str_repeat(" ", eo_1))));
  echo(LINE(356,concat4(toString(v_name), v_pad, v_num, "\n")));
  LINE(357,x_ob_start());
  return LINE(358,f_getmicrotime());
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 265 */
Variant f_mmult(int64 v_rows, int64 v_cols, CVarRef v_m1, CVarRef v_m2) {
  FUNCTION_INJECTION(mmult);
  Variant v_m3;
  int64 v_i = 0;
  int64 v_j = 0;
  Numeric v_x = 0;
  int64 v_k = 0;

  (v_m3 = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        {
          LOOP_COUNTER(5);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(5);
            {
              (v_x = 0LL);
              {
                LOOP_COUNTER(6);
                for ((v_k = 0LL); less(v_k, v_cols); v_k++) {
                  LOOP_COUNTER_CHECK(6);
                  {
                    v_x += v_m1.rvalAt(v_i).rvalAt(v_k) * v_m2.rvalAt(v_k).rvalAt(v_j);
                  }
                }
              }
              lval(v_m3.lvalAt(v_i)).set(v_j, (v_x));
            }
          }
        }
      }
    }
  }
  return (v_m3);
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 205 */
void f_heapsort_r(CVarRef v_n, Variant v_ra) {
  FUNCTION_INJECTION(heapsort_r);
  int64 v_l = 0;
  Variant v_ir;
  Variant v_rra;
  Numeric v_i = 0;
  Numeric v_j = 0;

  (v_l = (toInt64(toInt64(v_n)) >> 1LL) + 1LL);
  (v_ir = v_n);
  LOOP_COUNTER(7);
  {
    while (toBoolean(1LL)) {
      LOOP_COUNTER_CHECK(7);
      {
        if (more(v_l, 1LL)) {
          (v_rra = v_ra.rvalAt(--v_l));
        }
        else {
          (v_rra = v_ra.rvalAt(v_ir));
          v_ra.set(v_ir, (v_ra.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
          if (equal(--v_ir, 1LL)) {
            v_ra.set(1LL, (v_rra), 0x5BCA7C69B794F8CELL);
            return;
          }
        }
        (v_i = v_l);
        (v_j = toInt64(v_l) << 1LL);
        LOOP_COUNTER(8);
        {
          while (not_more(v_j, v_ir)) {
            LOOP_COUNTER_CHECK(8);
            {
              if ((less(v_j, v_ir)) && (less(v_ra.rvalAt(v_j), v_ra.rvalAt(v_j + 1LL)))) {
                v_j++;
              }
              if (less(v_rra, v_ra.rvalAt(v_j))) {
                v_ra.set(v_i, (v_ra.rvalAt(v_j)));
                v_j += ((v_i = v_j));
              }
              else {
                (v_j = v_ir + 1LL);
              }
            }
          }
        }
        v_ra.set(v_i, (v_rra));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 87 */
void f_ary(CVarRef v_n, CVarRef v_scale) {
  FUNCTION_INJECTION(ary);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 v_s = 0;
  Numeric v_i = 0;
  Variant v_X;
  Variant v_Y;
  Numeric v_last = 0;

  {
    LOOP_COUNTER(9);
    for ((v_s = 0LL); less(v_s, v_scale); v_s++) {
      LOOP_COUNTER_CHECK(9);
      {
        {
          LOOP_COUNTER(10);
          for ((v_i = 0LL); less(v_i, v_n); v_i++) {
            LOOP_COUNTER_CHECK(10);
            {
              v_X.set(v_i, (v_i));
            }
          }
        }
        {
          LOOP_COUNTER(11);
          for ((v_i = v_n - 1LL); not_less(v_i, 0LL); v_i--) {
            LOOP_COUNTER_CHECK(11);
            {
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
            }
          }
        }
        (v_last = v_n - 1LL);
        print(toString(v_Y.rvalAt(v_last)) + toString("\n"));
        unset(v_X);
        unset(v_Y);
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 50 */
void f_mandel2() {
  FUNCTION_INJECTION(mandel2);
  String v_b;
  int64 v_y = 0;
  double v_C = 0.0;
  int64 v_x = 0;
  double v_c = 0.0;
  double v_z = 0.0;
  double v_Z = 0.0;
  double v_r = 0.0;
  double v_i = 0.0;
  int64 v_k = 0;
  double v_t = 0.0;

  (v_b = " .:,;!/>)|&IH%*#");
  {
    LOOP_COUNTER(12);
    for ((v_y = 30LL); toBoolean(LINE(53,x_printf(1, "\n"))), toBoolean((v_C = v_y * 0.10000000000000001 - 1.5)), toBoolean(v_y--); ) {
      LOOP_COUNTER_CHECK(12);
      {
        {
          LOOP_COUNTER(13);
          for ((v_x = 0LL); toBoolean((v_c = v_x * 0.040000000000000001 - 2LL)), toBoolean((v_z = toDouble(0LL))), toBoolean((v_Z = toDouble(0LL))), less(v_x++, 100LL); ) {
            LOOP_COUNTER_CHECK(13);
            {
              {
                LOOP_COUNTER(14);
                for ((v_r = v_c), (v_i = v_C), (v_k = 0LL); toBoolean((v_t = v_z * v_z - v_Z * v_Z + v_r)), toBoolean((v_Z = 2LL * v_z * v_Z + v_i)), toBoolean((v_z = v_t)), less(v_k, 5340LL); v_k++) {
                  LOOP_COUNTER_CHECK(14);
                  if (more(v_z * v_z + v_Z * v_Z, 500000LL)) break;
                }
              }
              echo(v_b.rvalAt(modulo(v_k, 16LL)));
            }
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 254 */
Variant f_mkmatrix(int64 v_rows, int64 v_cols) {
  FUNCTION_INJECTION(mkmatrix);
  int64 v_count = 0;
  Variant v_mx;
  int64 v_i = 0;
  int64 v_j = 0;

  (v_count = 1LL);
  (v_mx = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(15);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(15);
      {
        {
          LOOP_COUNTER(16);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(16);
            {
              lval(v_mx.lvalAt(v_i)).set(v_j, (v_count++));
            }
          }
        }
      }
    }
  }
  return (v_mx);
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 279 */
void f_matrix(Variant v_n) {
  FUNCTION_INJECTION(matrix);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int64 v_SIZE = 0;
  Variant v_m1;
  Variant v_m2;
  Variant v_mm;

  (v_SIZE = 30LL);
  (v_m1 = LINE(281,f_mkmatrix(v_SIZE, v_SIZE)));
  (v_m2 = LINE(282,f_mkmatrix(v_SIZE, v_SIZE)));
  LOOP_COUNTER(17);
  {
    while (toBoolean(v_n--)) {
      LOOP_COUNTER_CHECK(17);
      {
        (v_mm = LINE(284,f_mmult(v_SIZE, v_SIZE, v_m1, v_m2)));
      }
    }
  }
  print(LINE(286,(assignCallTemp(eo_0, toString(v_mm.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))),assignCallTemp(eo_2, concat6(toString(v_mm.rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), " ", toString(v_mm.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL).rvalAt(2LL, 0x486AFCC090D5F98CLL)), " ", toString(v_mm.rvalAt(4LL, 0x6F2A25235E544A31LL).rvalAt(4LL, 0x6F2A25235E544A31LL)), "\n")),concat3(eo_0, " ", eo_2))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 10 */
void f_mandel() {
  FUNCTION_INJECTION(mandel);
  int64 v_w1 = 0;
  int64 v_h1 = 0;
  double v_recen = 0.0;
  double v_imcen = 0.0;
  double v_r = 0.0;
  Numeric v_s = 0;
  double v_rec = 0.0;
  double v_imc = 0.0;
  double v_re = 0.0;
  double v_im = 0.0;
  double v_re2 = 0.0;
  double v_im2 = 0.0;
  int64 v_x = 0;
  int64 v_y = 0;
  int64 v_w2 = 0;
  int64 v_h2 = 0;
  int64 v_color = 0;

  (v_w1 = 60LL);
  (v_h1 = 78LL);
  (v_recen = -0.45000000000000001);
  (v_imcen = 0.0);
  (v_r = 0.69999999999999996);
  (v_s = 0LL);
  (v_rec = toDouble(0LL));
  (v_imc = toDouble(0LL));
  (v_re = toDouble(0LL));
  (v_im = toDouble(0LL));
  (v_re2 = toDouble(0LL));
  (v_im2 = toDouble(0LL));
  (v_x = 0LL);
  (v_y = 0LL);
  (v_w2 = 0LL);
  (v_h2 = 0LL);
  (v_color = 0LL);
  (v_s = divide(2LL * v_r, v_w1));
  (v_w2 = 40LL);
  (v_h2 = 12LL);
  {
    LOOP_COUNTER(18);
    for ((v_y = 0LL); not_more(v_y, v_w1); (v_y = v_y + 1LL)) {
      LOOP_COUNTER_CHECK(18);
      {
        (v_imc = toDouble(v_s * (v_y - v_h2)) + v_imcen);
        {
          LOOP_COUNTER(19);
          for ((v_x = 0LL); not_more(v_x, v_h1); (v_x = v_x + 1LL)) {
            LOOP_COUNTER_CHECK(19);
            {
              (v_rec = toDouble(v_s * (v_x - v_w2)) + v_recen);
              (v_re = v_rec);
              (v_im = v_imc);
              (v_color = 1000LL);
              (v_re2 = v_re * v_re);
              (v_im2 = v_im * v_im);
              LOOP_COUNTER(20);
              {
                while (((less((v_re2 + v_im2), 1000000LL)) && more(v_color, 0LL))) {
                  LOOP_COUNTER_CHECK(20);
                  {
                    (v_im = v_re * v_im * 2LL + v_imc);
                    (v_re = v_re2 - v_im2 + v_rec);
                    (v_re2 = v_re * v_re);
                    (v_im2 = v_im * v_im);
                    (v_color = v_color - 1LL);
                  }
                }
              }
              if (equal(v_color, 0LL)) {
                print("_");
              }
              else {
                print("#");
              }
            }
          }
        }
        print("<br>");
        LINE(44,x_flush());
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 335 */
PlusOperand f_getmicrotime() {
  FUNCTION_INJECTION(getmicrotime);
  Variant v_t;

  (v_t = LINE(337,x_gettimeofday()));
  return (v_t.rvalAt("sec", 0x5051D7D254724345LL) + divide(v_t.rvalAt("usec", 0x2FF027BBECC82A1FLL), 1000000LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 172 */
void f_hash1(CVarRef v_n) {
  FUNCTION_INJECTION(hash1);
  Variant v_i;
  Sequence v_X;
  int64 v_c = 0;

  {
    LOOP_COUNTER(21);
    for ((v_i = 1LL); not_more(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(21);
      {
        v_X.set(LINE(174,x_dechex(toInt64(v_i))), (v_i));
      }
    }
  }
  (v_c = 0LL);
  {
    LOOP_COUNTER(22);
    for ((v_i = v_n); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(22);
      {
        if (toBoolean(v_X.rvalAt(LINE(178,x_dechex(toInt64(v_i)))))) {
          v_c++;
        }
      }
    }
  }
  print(toString(v_c) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 185 */
void f_hash2(CVarRef v_n) {
  FUNCTION_INJECTION(hash2);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_i;
  Variant v_hash1;
  Sequence v_hash2;
  Primitive v_key = 0;
  Variant v_value;
  String v_first;
  String v_last;

  {
    LOOP_COUNTER(23);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(23);
      {
        v_hash1.set(toString("foo_") + toString(v_i), (v_i));
        v_hash2.set(toString("foo_") + toString(v_i), (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(24);
    for ((v_i = v_n); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(24);
      {
        {
          LOOP_COUNTER(25);
          for (ArrayIterPtr iter27 = v_hash1.begin(); !iter27->end(); iter27->next()) {
            LOOP_COUNTER_CHECK(25);
            v_value = iter27->second();
            v_key = iter27->first();
            lval(v_hash2.lvalAt(v_key)) += v_value;
          }
        }
      }
    }
  }
  (v_first = "foo_0");
  (v_last = concat("foo_", (toString(v_n - 1LL))));
  print(LINE(195,(assignCallTemp(eo_0, toString(v_hash1.rvalAt(v_first))),assignCallTemp(eo_2, concat6(toString(v_hash1.rvalAt(v_last)), " ", toString(v_hash2.rvalAt(v_first)), " ", toString(v_hash2.rvalAt(v_last)), "\n")),concat3(eo_0, " ", eo_2))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 324 */
void f_strcat(Variant v_n) {
  FUNCTION_INJECTION(strcat);
  String v_str;
  int v_len = 0;

  (v_str = "");
  LOOP_COUNTER(28);
  {
    while (more(v_n--, 0LL)) {
      LOOP_COUNTER_CHECK(28);
      {
        concat_assign(v_str, "hello\n");
      }
    }
  }
  (v_len = LINE(329,x_strlen(v_str)));
  print(toString(v_len) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 157 */
Variant f_fibo_r(Numeric v_n) {
  FUNCTION_INJECTION(fibo_r);
  return ((less(v_n, 2LL)) ? ((Variant)(1LL)) : ((Variant)(plus_rev(LINE(158,f_fibo_r(v_n - 1LL)), f_fibo_r(v_n - 2LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 200 */
Numeric f_gen_random(int64 v_n) {
  FUNCTION_INJECTION(gen_random);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LAST __attribute__((__unused__)) = g->GV(LAST);
  return (divide((v_n * ((gv_LAST = modulo((toInt64(gv_LAST * 3877LL /* IA */ + 29573LL /* IC */)), 139968LL /* IM */)))), 139968LL /* IM */));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 361 */
void f_total() {
  FUNCTION_INJECTION(total);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_total __attribute__((__unused__)) = g->GV(total);
  String v_pad;
  String v_num;

  (v_pad = LINE(364,x_str_repeat("-", toInt32(24LL))));
  echo(concat(v_pad, "\n"));
  (v_num = LINE(366,x_number_format(toDouble(gv_total), toInt32(3LL))));
  (v_pad = LINE(367,(assignCallTemp(eo_1, toInt32(minus_rev(x_strlen(v_num), 24LL - x_strlen("Total")))),x_str_repeat(" ", eo_1))));
  echo(LINE(368,concat4("Total", v_pad, v_num, "\n")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 70 */
void f_ackermann() {
  FUNCTION_INJECTION(ackermann);
  Numeric v_r = 0;

  (v_r = LINE(71,f_ack(3LL, 7LL)));
  print(LINE(72,concat3("Ack(3,7): ", toString(v_r), "\n")));
  (v_r = LINE(73,f_ack(3LL, 6LL)));
  print(LINE(74,concat3("Ack(3,6): ", toString(v_r), "\n")));
  (v_r = LINE(75,f_ack(3LL, 6LL)));
  print(LINE(76,concat3("Ack(3,6): ", toString(v_r), "\n")));
  (v_r = LINE(77,f_ack(3LL, 5LL)));
  print(LINE(78,concat3("Ack(3,5): ", toString(v_r), "\n")));
  (v_r = LINE(79,f_ack(3LL, 5LL)));
  print(LINE(80,concat3("Ack(3,5): ", toString(v_r), "\n")));
  (v_r = LINE(81,f_ack(3LL, 5LL)));
  print(LINE(82,concat3("Ack(3,5): ", toString(v_r), "\n")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 64 */
Numeric f_ack(Numeric v_m, Numeric v_n) {
  FUNCTION_INJECTION(Ack);
  Variant eo_0;
  Variant eo_1;
  if (equal(v_m, 0LL)) return v_n + 1LL;
  if (equal(v_n, 0LL)) return LINE(66,f_ack(v_m - 1LL, 1LL));
  return LINE(67,(assignCallTemp(eo_0, v_m - 1LL),assignCallTemp(eo_1, f_ack(v_m, (v_n - 1LL))),f_ack(eo_0, eo_1)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 291 */
void f_nestedloop(CVarRef v_n) {
  FUNCTION_INJECTION(nestedloop);
  int64 v_x = 0;
  int64 v_a = 0;
  int64 v_b = 0;
  int64 v_c = 0;
  int64 v_d = 0;
  int64 v_e = 0;
  int64 v_f = 0;

  (v_x = 0LL);
  {
    LOOP_COUNTER(29);
    for ((v_a = 0LL); less(v_a, v_n); v_a++) {
      LOOP_COUNTER_CHECK(29);
      {
        LOOP_COUNTER(30);
        for ((v_b = 0LL); less(v_b, v_n); v_b++) {
          LOOP_COUNTER_CHECK(30);
          {
            LOOP_COUNTER(31);
            for ((v_c = 0LL); less(v_c, v_n); v_c++) {
              LOOP_COUNTER_CHECK(31);
              {
                LOOP_COUNTER(32);
                for ((v_d = 0LL); less(v_d, v_n); v_d++) {
                  LOOP_COUNTER_CHECK(32);
                  {
                    LOOP_COUNTER(33);
                    for ((v_e = 0LL); less(v_e, v_n); v_e++) {
                      LOOP_COUNTER_CHECK(33);
                      {
                        LOOP_COUNTER(34);
                        for ((v_f = 0LL); less(v_f, v_n); v_f++) {
                          LOOP_COUNTER_CHECK(34);
                          v_x++;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  print(toString(v_x) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 161 */
void f_fibo() {
  FUNCTION_INJECTION(fibo);
  Variant v_r;

  (v_r = LINE(162,f_fibo_r(28LL)));
  print(toString(v_r) + toString("\n"));
  (v_r = LINE(164,f_fibo_r(27LL)));
  print(toString(v_r) + toString("\n"));
  (v_r = LINE(166,f_fibo_r(24LL)));
  print(toString(v_r) + toString("\n"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 341 */
PlusOperand f_start_test() {
  FUNCTION_INJECTION(start_test);
  LINE(343,x_ob_start());
  return LINE(344,f_getmicrotime());
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 237 */
void f_heapsort(CVarRef v_N) {
  FUNCTION_INJECTION(heapsort);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LAST __attribute__((__unused__)) = g->GV(LAST);
  int64 v_i = 0;
  Variant v_ary;

  ;
  ;
  ;
  (gv_LAST = 42LL);
  {
    LOOP_COUNTER(35);
    for ((v_i = 1LL); not_more(v_i, v_N); v_i++) {
      LOOP_COUNTER_CHECK(35);
      {
        v_ary.set(v_i, (LINE(246,f_gen_random(1LL))));
      }
    }
  }
  LINE(248,f_heapsort_r(v_N, ref(v_ary)));
  LINE(249,x_printf(2, "%.10f\n", Array(ArrayInit(1).set(0, v_ary.rvalAt(v_N)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 105 */
void f_ary2(CVarRef v_n, CVarRef v_scale) {
  FUNCTION_INJECTION(ary2);
  int64 v_s = 0;
  Numeric v_i = 0;
  Sequence v_X;
  Sequence v_Y;
  Numeric v_last = 0;

  {
    LOOP_COUNTER(36);
    for ((v_s = 0LL); less(v_s, v_scale); v_s++) {
      LOOP_COUNTER_CHECK(36);
      {
        {
          LOOP_COUNTER(37);
          for ((v_i = 0LL); less(v_i, v_n); ) {
            LOOP_COUNTER_CHECK(37);
            {
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
              v_X.set(v_i, (v_i));
              ++v_i;
            }
          }
        }
        {
          LOOP_COUNTER(38);
          for ((v_i = v_n - 1LL); not_less(v_i, 0LL); ) {
            LOOP_COUNTER_CHECK(38);
            {
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
              v_Y.set(v_i, (v_X.rvalAt(v_i)));
              --v_i;
            }
          }
        }
        (v_last = v_n - 1LL);
        print(toString(v_Y.rvalAt(v_last)) + toString("\n"));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/zend/template.php line 141 */
void f_ary3(CVarRef v_n) {
  FUNCTION_INJECTION(ary3);
  Numeric v_i = 0;
  Sequence v_X;
  Sequence v_Y;
  int64 v_k = 0;
  Numeric v_last = 0;

  {
    LOOP_COUNTER(39);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(39);
      {
        v_X.set(v_i, (v_i + 1LL));
        v_Y.set(v_i, (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(40);
    for ((v_k = 0LL); less(v_k, 2650LL); v_k++) {
      LOOP_COUNTER_CHECK(40);
      {
        {
          LOOP_COUNTER(41);
          for ((v_i = v_n - 1LL); not_less(v_i, 0LL); v_i--) {
            LOOP_COUNTER_CHECK(41);
            {
              lval(v_Y.lvalAt(v_i)) += v_X.rvalAt(v_i);
            }
          }
        }
      }
    }
  }
  (v_last = v_n - 1LL);
  print(LINE(152,concat4(toString(v_Y.rvalAt(0, 0x77CFA1EEF01BCA90LL)), " ", toString(v_Y.rvalAt(v_last)), "\n")));
} /* function */
Variant i_total(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2AB70DFF89534220LL, total) {
    return (f_total(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$zend$template_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/zend/template.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$zend$template_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_t0 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t0") : g->GV(t0);

  LINE(6,x_date_default_timezone_set("UTC"));
  (v_t0 = (v_t = LINE(371,f_start_test())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
