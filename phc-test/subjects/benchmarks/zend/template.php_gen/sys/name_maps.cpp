
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/template.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/template.php",
  "ary", "phc-test/subjects/benchmarks/zend/template.php",
  "ary2", "phc-test/subjects/benchmarks/zend/template.php",
  "ary3", "phc-test/subjects/benchmarks/zend/template.php",
  "end_test", "phc-test/subjects/benchmarks/zend/template.php",
  "fibo", "phc-test/subjects/benchmarks/zend/template.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/template.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/template.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/template.php",
  "hash1", "phc-test/subjects/benchmarks/zend/template.php",
  "hash2", "phc-test/subjects/benchmarks/zend/template.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/template.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/template.php",
  "mandel", "phc-test/subjects/benchmarks/zend/template.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/template.php",
  "matrix", "phc-test/subjects/benchmarks/zend/template.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/template.php",
  "mmult", "phc-test/subjects/benchmarks/zend/template.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/template.php",
  "sieve", "phc-test/subjects/benchmarks/zend/template.php",
  "start_test", "phc-test/subjects/benchmarks/zend/template.php",
  "strcat", "phc-test/subjects/benchmarks/zend/template.php",
  "total", "phc-test/subjects/benchmarks/zend/template.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
