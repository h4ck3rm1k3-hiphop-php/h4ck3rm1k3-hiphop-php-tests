
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "hallo", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "hallo2", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "simple", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "simplecall", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "simpleucall", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "simpleudcall", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/simpleudcall.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
