
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/ackermann.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
