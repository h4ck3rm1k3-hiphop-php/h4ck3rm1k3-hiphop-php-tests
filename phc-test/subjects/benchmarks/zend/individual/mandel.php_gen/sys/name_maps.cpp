
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/mandel.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
