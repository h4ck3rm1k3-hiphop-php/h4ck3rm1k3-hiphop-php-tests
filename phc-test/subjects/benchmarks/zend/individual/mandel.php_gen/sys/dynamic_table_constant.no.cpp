
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_IA;
extern const int64 k_IC;
extern const int64 k_IM;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x1FAB7CA7F1ED0149LL, k_IC, IC);
      break;
    case 6:
      HASH_RETURN(0x7043E7408FB71046LL, k_IA, IA);
      break;
    case 7:
      HASH_RETURN(0x2AE4591F8E41AEEFLL, k_IM, IM);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
