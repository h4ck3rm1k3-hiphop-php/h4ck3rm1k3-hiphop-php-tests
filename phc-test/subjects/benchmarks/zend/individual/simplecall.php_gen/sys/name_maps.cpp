
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "hallo", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "hallo2", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "simple", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "simplecall", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "simpleucall", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "simpleudcall", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/simplecall.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
