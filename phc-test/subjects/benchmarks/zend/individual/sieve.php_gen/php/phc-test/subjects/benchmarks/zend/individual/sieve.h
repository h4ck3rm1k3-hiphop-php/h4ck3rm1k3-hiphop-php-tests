
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_sieve_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_sieve_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/zend/individual/sieve.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_sieve(int64 v_n);
PlusOperand f_end_test(CVarRef v_start, CStrRef v_name);
Variant f_mmult(int64 v_rows, int64 v_cols, CVarRef v_m1, CVarRef v_m2);
void f_heapsort_r(CVarRef v_n, Variant v_ra);
void f_ary(CVarRef v_n, CVarRef v_scale);
void f_mandel2();
Variant f_mkmatrix(int64 v_rows, int64 v_cols);
void f_matrix(Variant v_n);
void f_mandel();
PlusOperand f_getmicrotime();
void f_hash1(CVarRef v_n);
void f_hash2(CVarRef v_n);
void f_strcat(Variant v_n);
Variant pm_php$phc_test$subjects$benchmarks$zend$individual$sieve_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_fibo_r(Numeric v_n);
Numeric f_gen_random(int64 v_n);
void f_total();
void f_ackermann();
Numeric f_ack(Numeric v_m, Numeric v_n);
void f_nestedloop(CVarRef v_n);
void f_fibo();
PlusOperand f_start_test();
void f_heapsort(CVarRef v_N);
void f_ary2(CVarRef v_n, CVarRef v_scale);
void f_ary3(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_sieve_h__
