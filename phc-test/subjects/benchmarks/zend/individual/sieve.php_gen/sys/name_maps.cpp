
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/sieve.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
