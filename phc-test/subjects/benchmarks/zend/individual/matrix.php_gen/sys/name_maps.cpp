
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/matrix.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
