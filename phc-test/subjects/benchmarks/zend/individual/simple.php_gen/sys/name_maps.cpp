
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "hallo", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "hallo2", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "simple", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "simplecall", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "simpleucall", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "simpleudcall", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/simple.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
