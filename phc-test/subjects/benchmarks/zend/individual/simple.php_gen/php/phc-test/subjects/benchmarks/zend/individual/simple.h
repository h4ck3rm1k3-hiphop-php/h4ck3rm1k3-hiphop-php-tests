
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_simple_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_simple_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/zend/individual/simple.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_sieve(Variant v_n);
PlusOperand f_end_test(CVarRef v_start, CStrRef v_name);
Variant f_mmult(int64 v_rows, int64 v_cols, CVarRef v_m1, CVarRef v_m2);
void f_hallo2(CStrRef v_a);
void f_heapsort_r(CVarRef v_n, Variant v_ra);
void f_ary(CVarRef v_n);
void f_mandel2();
Variant f_mkmatrix(int64 v_rows, int64 v_cols);
void f_matrix(Variant v_n);
void f_simpleucall();
void f_mandel();
PlusOperand f_getmicrotime();
void f_hash1(CVarRef v_n);
void f_hash2(CVarRef v_n);
void f_strcat(Variant v_n);
Variant f_fibo_r(CVarRef v_n);
void f_simple();
Numeric f_gen_random(int64 v_n);
void f_total();
void f_ackermann(CVarRef v_n);
Numeric f_ack(Numeric v_m, CVarRef v_n);
void f_nestedloop(CVarRef v_n);
void f_simpleudcall();
void f_fibo(CVarRef v_n);
PlusOperand f_start_test();
Variant pm_php$phc_test$subjects$benchmarks$zend$individual$simple_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_simplecall();
void f_heapsort(CVarRef v_N);
void f_hallo(CVarRef v_a);
void f_ary2(CVarRef v_n);
void f_ary3(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_zend_individual_simple_h__
