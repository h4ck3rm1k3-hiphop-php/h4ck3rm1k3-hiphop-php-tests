
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/ary.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
