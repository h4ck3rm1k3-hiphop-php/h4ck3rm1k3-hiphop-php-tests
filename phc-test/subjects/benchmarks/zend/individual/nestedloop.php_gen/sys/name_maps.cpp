
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/nestedloop.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
