
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/strcat.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
