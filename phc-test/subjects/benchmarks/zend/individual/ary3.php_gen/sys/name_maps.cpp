
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/ary3.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
