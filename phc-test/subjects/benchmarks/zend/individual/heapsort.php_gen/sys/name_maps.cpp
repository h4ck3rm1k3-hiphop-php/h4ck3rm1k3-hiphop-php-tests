
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/heapsort.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
