
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/fibo.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
