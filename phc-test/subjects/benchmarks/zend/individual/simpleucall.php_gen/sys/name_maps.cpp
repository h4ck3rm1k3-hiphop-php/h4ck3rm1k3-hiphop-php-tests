
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "ary", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "ary2", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "ary3", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "end_test", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "fibo", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "hallo", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "hallo2", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "hash1", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "hash2", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "mandel", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "matrix", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "mmult", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "sieve", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "simple", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "simplecall", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "simpleucall", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "simpleudcall", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "start_test", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "strcat", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  "total", "phc-test/subjects/benchmarks/zend/individual/simpleucall.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
