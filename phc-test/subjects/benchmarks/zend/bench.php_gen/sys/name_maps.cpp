
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "ack", "phc-test/subjects/benchmarks/zend/bench.php",
  "ackermann", "phc-test/subjects/benchmarks/zend/bench.php",
  "ary", "phc-test/subjects/benchmarks/zend/bench.php",
  "ary2", "phc-test/subjects/benchmarks/zend/bench.php",
  "ary3", "phc-test/subjects/benchmarks/zend/bench.php",
  "end_test", "phc-test/subjects/benchmarks/zend/bench.php",
  "fibo", "phc-test/subjects/benchmarks/zend/bench.php",
  "fibo_r", "phc-test/subjects/benchmarks/zend/bench.php",
  "gen_random", "phc-test/subjects/benchmarks/zend/bench.php",
  "getmicrotime", "phc-test/subjects/benchmarks/zend/bench.php",
  "hallo", "phc-test/subjects/benchmarks/zend/bench.php",
  "hallo2", "phc-test/subjects/benchmarks/zend/bench.php",
  "hash1", "phc-test/subjects/benchmarks/zend/bench.php",
  "hash2", "phc-test/subjects/benchmarks/zend/bench.php",
  "heapsort", "phc-test/subjects/benchmarks/zend/bench.php",
  "heapsort_r", "phc-test/subjects/benchmarks/zend/bench.php",
  "mandel", "phc-test/subjects/benchmarks/zend/bench.php",
  "mandel2", "phc-test/subjects/benchmarks/zend/bench.php",
  "matrix", "phc-test/subjects/benchmarks/zend/bench.php",
  "mkmatrix", "phc-test/subjects/benchmarks/zend/bench.php",
  "mmult", "phc-test/subjects/benchmarks/zend/bench.php",
  "nestedloop", "phc-test/subjects/benchmarks/zend/bench.php",
  "sieve", "phc-test/subjects/benchmarks/zend/bench.php",
  "simple", "phc-test/subjects/benchmarks/zend/bench.php",
  "simplecall", "phc-test/subjects/benchmarks/zend/bench.php",
  "simpleucall", "phc-test/subjects/benchmarks/zend/bench.php",
  "simpleudcall", "phc-test/subjects/benchmarks/zend/bench.php",
  "start_test", "phc-test/subjects/benchmarks/zend/bench.php",
  "strcat", "phc-test/subjects/benchmarks/zend/bench.php",
  "total", "phc-test/subjects/benchmarks/zend/bench.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
