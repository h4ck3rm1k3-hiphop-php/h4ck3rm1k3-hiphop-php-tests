
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_zend_bench_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_zend_bench_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/zend/bench.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_sieve(int64 v_n);
PlusOperand f_end_test(CVarRef v_start, CStrRef v_name);
Variant f_mmult(int64 v_rows, int64 v_cols, CVarRef v_m1, CVarRef v_m2);
void f_hallo2(CStrRef v_a);
void f_heapsort_r(CVarRef v_n, Variant v_ra);
void f_ary(int64 v_n);
void f_mandel2();
Variant f_mkmatrix(int64 v_rows, int64 v_cols);
void f_matrix(int64 v_n);
void f_simpleucall();
Variant pm_php$phc_test$subjects$benchmarks$zend$bench_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_mandel();
PlusOperand f_getmicrotime();
void f_hash1(int64 v_n);
void f_hash2(int64 v_n);
void f_strcat(int64 v_n);
Variant f_fibo_r(Numeric v_n);
void f_simple();
Numeric f_gen_random(int64 v_n);
void f_total();
void f_ackermann(int64 v_n);
Numeric f_ack(Numeric v_m, Numeric v_n);
void f_nestedloop(int64 v_n);
void f_simpleudcall();
void f_fibo(int64 v_n);
PlusOperand f_start_test();
void f_simplecall();
void f_heapsort(int64 v_N);
void f_hallo(CVarRef v_a);
void f_ary2(int64 v_n);
void f_ary3(int64 v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_zend_bench_h__
