
#include <php/phc-test/subjects/benchmarks/README.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$README(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/README);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$README;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("PHPBench: http://www.pureftpd.org/project/phpbench\n\n\tPHPbench will need a bit of modification to do good work, which negates the point somewhat. It has a divide by zero in it, which we should remove, and also an infinite loop.\n\n\tIt also loads tests like this (AKA dynamic class loading):\n\t\t$dir = opendir ($test_dir);\n\t\twhile ($entry = readdir ($dir))\n\t\t{\n\t\t\tinclude_once ($entry);\n\t\t}\n\n\tTry optimizi");
  echo("ng that.\n\n\tWe can hack it into the correct form though. Done.\n\tWont compile though.\n\n\tIt seems that this test really only tests language features, and doesnt\n\tactually do any work. Most of it will be optmized away (in the future) so\n\tits unlikely to be useful in the long run\n\nPHP BS: http://phplens.com/benchmark_suite/\n\n\tA little bit complicated. Needs to change url loading into an include. I\n\tcan");
  echo("t quite find where it calls the include or the eval or whatever, though.\n\n\tThis does include quite a few tests, as well as input data, though not much\n\tof it, it seems.\n\nShootout: http://alioth.debian.org/scm/\?group_id=30402\n\n\tThe tests are in bench/test_name/test_name.php. I'm not sure how to call the\n\ttest suite, although each test contains a command line.\n\nZend: $PHP_SRC/Zend/bench.php\n\n\tA nice");
  echo(" simple test. You just run php bench.php and it runs, printing timing info. It wont compile either though.\n\nRoadsend: http://www.roadsend.com/home/index.php\?pageID=benchmarks\n\n\tShort, doesnt take command line parameters, is varied, but simple.\n\tRequires you to \"export PCC_HOME=`pwd`\" or similar, however.\n\n\tThis seems to be the only test with data that needs to be run-through. This\n\tmakes it a some");
  echo("what better test than the others, it seems.\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
