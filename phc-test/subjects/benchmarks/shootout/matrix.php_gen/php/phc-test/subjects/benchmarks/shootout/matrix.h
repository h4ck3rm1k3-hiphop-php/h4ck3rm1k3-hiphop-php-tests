
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_matrix_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_matrix_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/matrix.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_mmult(CVarRef v_rows, CVarRef v_cols, Variant v_m1, Variant v_m2, Variant v_mm);
Sequence f_mkmatrix(CVarRef v_rows, CVarRef v_cols);
Variant pm_php$phc_test$subjects$benchmarks$shootout$matrix_php(bool incOnce = false, LVariableTable* variables = NULL);
Sequence f_mkzeromatrix(CVarRef v_rows, CVarRef v_cols);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_matrix_h__
