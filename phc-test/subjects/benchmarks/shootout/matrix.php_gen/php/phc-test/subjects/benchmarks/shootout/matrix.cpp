
#include <php/phc-test/subjects/benchmarks/shootout/matrix.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/matrix.php line 36 */
Variant f_mmult(CVarRef v_rows, CVarRef v_cols, Variant v_m1, Variant v_m2, Variant v_mm) {
  FUNCTION_INJECTION(mmult);
  int64 v_i = 0;
  int64 v_j = 0;
  Numeric v_x = 0;
  int64 v_k = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_x = 0LL);
              {
                LOOP_COUNTER(3);
                for ((v_k = 0LL); less(v_k, v_cols); v_k++) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    v_x += v_m1.rvalAt(v_i).rvalAt(v_k) * v_m2.rvalAt(v_k).rvalAt(v_j);
                  }
                }
              }
              lval(v_mm.lvalAt(v_i)).set(v_j, (v_x));
            }
          }
        }
      }
    }
  }
  return v_mm;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/matrix.php line 15 */
Sequence f_mkmatrix(CVarRef v_rows, CVarRef v_cols) {
  FUNCTION_INJECTION(mkMatrix);
  int64 v_count = 0;
  int64 v_i = 0;
  int64 v_j = 0;
  Sequence v_m;

  (v_count = 1LL);
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        {
          LOOP_COUNTER(5);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(5);
            {
              lval(v_m.lvalAt(v_i)).set(v_j, (v_count++));
            }
          }
        }
      }
    }
  }
  return v_m;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/matrix.php line 26 */
Sequence f_mkzeromatrix(CVarRef v_rows, CVarRef v_cols) {
  FUNCTION_INJECTION(mkZeroMatrix);
  int64 v_i = 0;
  int64 v_j = 0;
  Sequence v_m;

  {
    LOOP_COUNTER(6);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(6);
      {
        {
          LOOP_COUNTER(7);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(7);
            {
              lval(v_m.lvalAt(v_i)).set(v_j, (0LL));
            }
          }
        }
      }
    }
  }
  return v_m;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$matrix_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/matrix.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$matrix_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_SIZE __attribute__((__unused__)) = (variables != gVariables) ? variables->get("SIZE") : g->GV(SIZE);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_m1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m1") : g->GV(m1);
  Variant &v_m2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m2") : g->GV(m2);
  Variant &v_mm __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mm") : g->GV(mm);

  LINE(11,x_set_time_limit(toInt32(0LL)));
  (v_SIZE = 30LL);
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_m1 = LINE(52,f_mkmatrix(v_SIZE, v_SIZE)));
  (v_m2 = LINE(53,f_mkmatrix(v_SIZE, v_SIZE)));
  (v_mm = LINE(54,f_mkzeromatrix(v_SIZE, v_SIZE)));
  LOOP_COUNTER(8);
  {
    while (toBoolean(v_n--)) {
      LOOP_COUNTER_CHECK(8);
      LINE(57,f_mmult(v_SIZE, v_SIZE, ref(v_m1), ref(v_m2), ref(v_mm)));
    }
  }
  print(LINE(60,(assignCallTemp(eo_0, toString(v_mm.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))),assignCallTemp(eo_2, concat6(toString(v_mm.rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), " ", toString(v_mm.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL).rvalAt(2LL, 0x486AFCC090D5F98CLL)), " ", toString(v_mm.rvalAt(4LL, 0x6F2A25235E544A31LL).rvalAt(4LL, 0x6F2A25235E544A31LL)), "\n")),concat3(eo_0, " ", eo_2))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
