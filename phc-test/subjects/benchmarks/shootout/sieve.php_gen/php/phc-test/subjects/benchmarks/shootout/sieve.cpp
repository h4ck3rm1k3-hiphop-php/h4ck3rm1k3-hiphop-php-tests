
#include <php/phc-test/subjects/benchmarks/shootout/sieve.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$sieve_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/sieve.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$sieve_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_stop __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stop") : g->GV(stop);
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_isPrime __attribute__((__unused__)) = (variables != gVariables) ? variables->get("isPrime") : g->GV(isPrime);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_stop = 8192LL);
  (v_count = 0LL);
  LOOP_COUNTER(1);
  {
    while (more(v_n--, 0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_count = 0LL);
        (v_i = v_stop);
        LOOP_COUNTER(2);
        {
          while (toBoolean(v_i--)) {
            LOOP_COUNTER_CHECK(2);
            v_isPrime.set(v_i, (1LL));
          }
        }
        (v_i = 2LL);
        LOOP_COUNTER(3);
        {
          while (less(v_i++, v_stop)) {
            LOOP_COUNTER_CHECK(3);
            {
              if (toBoolean(v_isPrime.rvalAt(v_i))) {
                {
                  LOOP_COUNTER(4);
                  for ((v_k = v_i + v_i); not_more(v_k, v_stop); v_k += v_i) {
                    LOOP_COUNTER_CHECK(4);
                    v_isPrime.set(v_k, (0LL));
                  }
                }
                v_count++;
              }
            }
          }
        }
      }
    }
  }
  print(LINE(27,concat3("Count: ", toString(v_count), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
