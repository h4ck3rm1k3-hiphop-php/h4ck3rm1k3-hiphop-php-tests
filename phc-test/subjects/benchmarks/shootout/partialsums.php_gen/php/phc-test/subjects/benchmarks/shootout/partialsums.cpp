
#include <php/phc-test/subjects/benchmarks/shootout/partialsums.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$partialsums_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/partialsums.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$partialsums_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_a3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a3") : g->GV(a3);
  Variant &v_a4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a4") : g->GV(a4);
  Variant &v_a5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a5") : g->GV(a5);
  Variant &v_a6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a6") : g->GV(a6);
  Variant &v_a7 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a7") : g->GV(a7);
  Variant &v_a8 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a8") : g->GV(a8);
  Variant &v_a9 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a9") : g->GV(a9);
  Variant &v_twothirds __attribute__((__unused__)) = (variables != gVariables) ? variables->get("twothirds") : g->GV(twothirds);
  Variant &v_alt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("alt") : g->GV(alt);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_k2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k2") : g->GV(k2);
  Variant &v_k3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k3") : g->GV(k3);
  Variant &v_sk __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sk") : g->GV(sk);
  Variant &v_ck __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ck") : g->GV(ck);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_a1 = 0.0);
  (v_a2 = 0.0);
  (v_a3 = 0.0);
  (v_a4 = 0.0);
  (v_a5 = 0.0);
  (v_a6 = 0.0);
  (v_a7 = 0.0);
  (v_a8 = 0.0);
  (v_a9 = 0.0);
  (v_twothirds = 0.66666666666666663);
  (v_alt = -1.0);
  {
    LOOP_COUNTER(1);
    for ((v_k = 1LL); not_more(v_k, v_n); v_k++) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_k2 = v_k * v_k);
        (v_k3 = v_k2 * v_k);
        (v_sk = LINE(17,x_sin(toDouble(v_k))));
        (v_ck = LINE(18,x_cos(toDouble(v_k))));
        (v_alt = negate(v_alt));
        v_a1 += LINE(21,x_pow(v_twothirds, v_k - 1LL));
        v_a2 += LINE(22,x_pow(v_k, -0.5));
        v_a3 += divide(1.0, (toDouble(v_k) * (toDouble(v_k) + 1.0)));
        v_a4 += divide(1.0, (v_k3 * v_sk * v_sk));
        v_a5 += divide(1.0, (v_k3 * v_ck * v_ck));
        v_a6 += divide(1.0, v_k);
        v_a7 += divide(1.0, v_k2);
        v_a8 += divide(v_alt, v_k);
        v_a9 += divide(v_alt, (2LL * v_k - 1LL));
      }
    }
  }
  LINE(32,x_printf(2, "%.9f\t(2/3)^k\n", Array(ArrayInit(1).set(0, v_a1).create())));
  LINE(33,x_printf(2, "%.9f\tk^-0.5\n", Array(ArrayInit(1).set(0, v_a2).create())));
  LINE(34,x_printf(2, "%.9f\t1/k(k+1)\n", Array(ArrayInit(1).set(0, v_a3).create())));
  LINE(35,x_printf(2, "%.9f\tFlint Hills\n", Array(ArrayInit(1).set(0, v_a4).create())));
  LINE(36,x_printf(2, "%.9f\tCookson Hills\n", Array(ArrayInit(1).set(0, v_a5).create())));
  LINE(37,x_printf(2, "%.9f\tHarmonic\n", Array(ArrayInit(1).set(0, v_a6).create())));
  LINE(38,x_printf(2, "%.9f\tRiemann Zeta\n", Array(ArrayInit(1).set(0, v_a7).create())));
  LINE(39,x_printf(2, "%.9f\tAlternating Harmonic\n", Array(ArrayInit(1).set(0, v_a8).create())));
  LINE(40,x_printf(2, "%.9f\tGregory\n", Array(ArrayInit(1).set(0, v_a9).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
