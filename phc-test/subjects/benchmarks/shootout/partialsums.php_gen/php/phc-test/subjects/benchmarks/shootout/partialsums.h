
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_partialsums_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_partialsums_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/partialsums.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$partialsums_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_partialsums_h__
