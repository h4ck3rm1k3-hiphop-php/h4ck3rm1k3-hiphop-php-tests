
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "n",
    "a1",
    "a2",
    "a3",
    "a4",
    "a5",
    "a6",
    "a7",
    "a8",
    "a9",
    "twothirds",
    "alt",
    "k",
    "k2",
    "k3",
    "sk",
    "ck",
  };
  if (idx >= 0 && idx < 29) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(n);
    case 13: return GV(a1);
    case 14: return GV(a2);
    case 15: return GV(a3);
    case 16: return GV(a4);
    case 17: return GV(a5);
    case 18: return GV(a6);
    case 19: return GV(a7);
    case 20: return GV(a8);
    case 21: return GV(a9);
    case 22: return GV(twothirds);
    case 23: return GV(alt);
    case 24: return GV(k);
    case 25: return GV(k2);
    case 26: return GV(k3);
    case 27: return GV(sk);
    case 28: return GV(ck);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
