
#include <php/phc-test/subjects/benchmarks/shootout/sumcol.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$sumcol_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/sumcol.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$sumcol_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_sum __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sum") : g->GV(sum);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);

  (v_sum = 0LL);
  (v_fp = LINE(14,x_fopen("php://stdin", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_line = LINE(15,x_fgets(toObject(v_fp), 128LL))))) {
      LOOP_COUNTER_CHECK(1);
      v_sum += x_intval(v_line);
    }
  }
  LINE(16,x_fclose(toObject(v_fp)));
  print(toString(v_sum) + toString("\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
