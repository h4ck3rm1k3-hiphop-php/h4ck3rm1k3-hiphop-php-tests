
#ifndef __GENERATED_cls_intersectionpoint_h__
#define __GENERATED_cls_intersectionpoint_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 60 */
class c_intersectionpoint : virtual public ObjectData {
  BEGIN_CLASS_MAP(intersectionpoint)
  END_CLASS_MAP(intersectionpoint)
  DECLARE_CLASS(intersectionpoint, IntersectionPoint, ObjectData)
  void init();
  public: Variant m_distance;
  public: Variant m_normal;
  public: void t_intersectionpoint(CVarRef v_distance, Variant v_normal);
  public: ObjectData *create(CVarRef v_distance, Variant v_normal);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_intersectionpoint_h__
