
#ifndef __GENERATED_cls_sphere_h__
#define __GENERATED_cls_sphere_h__

#include <cls/scene.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 124 */
class c_sphere : virtual public c_scene {
  BEGIN_CLASS_MAP(sphere)
    PARENT_CLASS(scene)
  END_CLASS_MAP(sphere)
  DECLARE_CLASS(sphere, Sphere, scene)
  void init();
  public: Variant m_center;
  public: Variant m_radius;
  public: void t_sphere(Variant v_center, Numeric v_radius);
  public: ObjectData *create(Variant v_center, Numeric v_radius);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_distance(Variant v_ray);
  public: Variant t_intersect(Variant v_ray, Variant v_p);
  public: static Variant t_spherescene(Numeric v_level, CVarRef v_center, Numeric v_radius) { return ti_spherescene("sphere", v_level, v_center, v_radius); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sphere_h__
