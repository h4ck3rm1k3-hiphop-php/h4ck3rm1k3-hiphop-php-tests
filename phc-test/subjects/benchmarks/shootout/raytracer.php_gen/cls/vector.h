
#ifndef __GENERATED_cls_vector_h__
#define __GENERATED_cls_vector_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 17 */
class c_vector : virtual public ObjectData {
  BEGIN_CLASS_MAP(vector)
  END_CLASS_MAP(vector)
  DECLARE_CLASS(vector, Vector, ObjectData)
  void init();
  public: Variant m_x;
  public: Variant m_y;
  public: Variant m_z;
  public: void t_vector(CVarRef v_x, CVarRef v_y, CVarRef v_z);
  public: ObjectData *create(CVarRef v_x, CVarRef v_y, CVarRef v_z);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: p_vector t_plus(Variant v_b);
  public: p_vector t_minus(Variant v_b);
  public: PlusOperand t_dot(Variant v_b);
  public: p_vector t_scaledby(CVarRef v_s);
  public: p_vector t_normalized();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_vector_h__
