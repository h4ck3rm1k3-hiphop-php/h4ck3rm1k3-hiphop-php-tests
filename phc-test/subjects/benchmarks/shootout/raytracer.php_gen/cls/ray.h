
#ifndef __GENERATED_cls_ray_h__
#define __GENERATED_cls_ray_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 50 */
class c_ray : virtual public ObjectData {
  BEGIN_CLASS_MAP(ray)
  END_CLASS_MAP(ray)
  DECLARE_CLASS(ray, Ray, ObjectData)
  void init();
  public: Variant m_origin;
  public: Variant m_direction;
  public: void t_ray(Variant v_origin, Variant v_direction);
  public: ObjectData *create(Variant v_origin, Variant v_direction);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ray_h__
