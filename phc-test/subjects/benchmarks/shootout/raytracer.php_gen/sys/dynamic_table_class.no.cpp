
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_intersectionpoint(CArrRef params, bool init = true);
Variant cw_intersectionpoint$os_get(const char *s);
Variant &cw_intersectionpoint$os_lval(const char *s);
Variant cw_intersectionpoint$os_constant(const char *s);
Variant cw_intersectionpoint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_vector(CArrRef params, bool init = true);
Variant cw_vector$os_get(const char *s);
Variant &cw_vector$os_lval(const char *s);
Variant cw_vector$os_constant(const char *s);
Variant cw_vector$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_group(CArrRef params, bool init = true);
Variant cw_group$os_get(const char *s);
Variant &cw_group$os_lval(const char *s);
Variant cw_group$os_constant(const char *s);
Variant cw_group$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sphere(CArrRef params, bool init = true);
Variant cw_sphere$os_get(const char *s);
Variant &cw_sphere$os_lval(const char *s);
Variant cw_sphere$os_constant(const char *s);
Variant cw_sphere$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ray(CArrRef params, bool init = true);
Variant cw_ray$os_get(const char *s);
Variant &cw_ray$os_lval(const char *s);
Variant cw_ray$os_constant(const char *s);
Variant cw_ray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_scene(CArrRef params, bool init = true);
Variant cw_scene$os_get(const char *s);
Variant &cw_scene$os_lval(const char *s);
Variant cw_scene$os_constant(const char *s);
Variant cw_scene$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_CREATE_OBJECT(0x7D15D827BC5F1842LL, vector);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x0946ACCDAE982865LL, scene);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x64F533350FDCF69ALL, ray);
      break;
    case 11:
      HASH_CREATE_OBJECT(0x08568FC1070F5C5BLL, group);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x1DBC3AEFF1AEFF3CLL, intersectionpoint);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x65EBEE26A2756C7FLL, sphere);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x7D15D827BC5F1842LL, vector);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x0946ACCDAE982865LL, scene);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x64F533350FDCF69ALL, ray);
      break;
    case 11:
      HASH_INVOKE_STATIC_METHOD(0x08568FC1070F5C5BLL, group);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x1DBC3AEFF1AEFF3CLL, intersectionpoint);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x65EBEE26A2756C7FLL, sphere);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x7D15D827BC5F1842LL, vector);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x0946ACCDAE982865LL, scene);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x64F533350FDCF69ALL, ray);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY(0x08568FC1070F5C5BLL, group);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x1DBC3AEFF1AEFF3CLL, intersectionpoint);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x65EBEE26A2756C7FLL, sphere);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x7D15D827BC5F1842LL, vector);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x0946ACCDAE982865LL, scene);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x64F533350FDCF69ALL, ray);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY_LV(0x08568FC1070F5C5BLL, group);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x1DBC3AEFF1AEFF3CLL, intersectionpoint);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x65EBEE26A2756C7FLL, sphere);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x7D15D827BC5F1842LL, vector);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x0946ACCDAE982865LL, scene);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x64F533350FDCF69ALL, ray);
      break;
    case 11:
      HASH_GET_CLASS_CONSTANT(0x08568FC1070F5C5BLL, group);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x1DBC3AEFF1AEFF3CLL, intersectionpoint);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x65EBEE26A2756C7FLL, sphere);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
