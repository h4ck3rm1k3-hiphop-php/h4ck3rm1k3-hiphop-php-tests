
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "group", "phc-test/subjects/benchmarks/shootout/raytracer.php",
  "intersectionpoint", "phc-test/subjects/benchmarks/shootout/raytracer.php",
  "ray", "phc-test/subjects/benchmarks/shootout/raytracer.php",
  "scene", "phc-test/subjects/benchmarks/shootout/raytracer.php",
  "sphere", "phc-test/subjects/benchmarks/shootout/raytracer.php",
  "vector", "phc-test/subjects/benchmarks/shootout/raytracer.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
