
#include <php/phc-test/subjects/benchmarks/shootout/raytracer.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_LEVELS = 6LL;
const int64 k_SS = 4LL;
const double k_EPSILON = 1.49012E-8;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 60 */
Variant c_intersectionpoint::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_intersectionpoint::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_intersectionpoint::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("distance", m_distance.isReferenced() ? ref(m_distance) : m_distance));
  props.push_back(NEW(ArrayElement)("normal", m_normal.isReferenced() ? ref(m_normal) : m_normal));
  c_ObjectData::o_get(props);
}
bool c_intersectionpoint::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x5847C6837EFC3014LL, normal, 6);
      break;
    case 1:
      HASH_EXISTS_STRING(0x1E9F6AF49FD61031LL, distance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_intersectionpoint::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x5847C6837EFC3014LL, m_normal,
                         normal, 6);
      break;
    case 1:
      HASH_RETURN_STRING(0x1E9F6AF49FD61031LL, m_distance,
                         distance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_intersectionpoint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x5847C6837EFC3014LL, m_normal,
                      normal, 6);
      break;
    case 1:
      HASH_SET_STRING(0x1E9F6AF49FD61031LL, m_distance,
                      distance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_intersectionpoint::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x5847C6837EFC3014LL, m_normal,
                         normal, 6);
      break;
    case 1:
      HASH_RETURN_STRING(0x1E9F6AF49FD61031LL, m_distance,
                         distance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_intersectionpoint::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(intersectionpoint)
ObjectData *c_intersectionpoint::create(CVarRef v_distance, Variant v_normal) {
  init();
  t_intersectionpoint(v_distance, ref(v_normal));
  return this;
}
ObjectData *c_intersectionpoint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))));
  } else return this;
}
ObjectData *c_intersectionpoint::cloneImpl() {
  c_intersectionpoint *obj = NEW(c_intersectionpoint)();
  cloneSet(obj);
  return obj;
}
void c_intersectionpoint::cloneSet(c_intersectionpoint *clone) {
  clone->m_distance = m_distance.isReferenced() ? ref(m_distance) : m_distance;
  clone->m_normal = m_normal.isReferenced() ? ref(m_normal) : m_normal;
  ObjectData::cloneSet(clone);
}
Variant c_intersectionpoint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_intersectionpoint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_intersectionpoint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_intersectionpoint$os_get(const char *s) {
  return c_intersectionpoint::os_get(s, -1);
}
Variant &cw_intersectionpoint$os_lval(const char *s) {
  return c_intersectionpoint::os_lval(s, -1);
}
Variant cw_intersectionpoint$os_constant(const char *s) {
  return c_intersectionpoint::os_constant(s);
}
Variant cw_intersectionpoint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_intersectionpoint::os_invoke(c, s, params, -1, fatal);
}
void c_intersectionpoint::init() {
  m_distance = null;
  m_normal = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 63 */
void c_intersectionpoint::t_intersectionpoint(CVarRef v_distance, Variant v_normal) {
  INSTANCE_METHOD_INJECTION(IntersectionPoint, IntersectionPoint::IntersectionPoint);
  bool oldInCtor = gasInCtor(true);
  (m_distance = v_distance);
  (m_normal = v_normal);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 17 */
Variant c_vector::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_vector::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_vector::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  props.push_back(NEW(ArrayElement)("y", m_y.isReferenced() ? ref(m_y) : m_y));
  props.push_back(NEW(ArrayElement)("z", m_z.isReferenced() ? ref(m_z) : m_z));
  c_ObjectData::o_get(props);
}
bool c_vector::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_EXISTS_STRING(0x4F56B733A4DFC78ALL, y, 1);
      break;
    case 3:
      HASH_EXISTS_STRING(0x62A103F6518DE2B3LL, z, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_vector::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_vector::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_SET_STRING(0x4F56B733A4DFC78ALL, m_y,
                      y, 1);
      break;
    case 3:
      HASH_SET_STRING(0x62A103F6518DE2B3LL, m_z,
                      z, 1);
      break;
    case 6:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_vector::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_vector::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(vector)
ObjectData *c_vector::create(CVarRef v_x, CVarRef v_y, CVarRef v_z) {
  init();
  t_vector(v_x, v_y, v_z);
  return this;
}
ObjectData *c_vector::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
ObjectData *c_vector::cloneImpl() {
  c_vector *obj = NEW(c_vector)();
  cloneSet(obj);
  return obj;
}
void c_vector::cloneSet(c_vector *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  clone->m_y = m_y.isReferenced() ? ref(m_y) : m_y;
  clone->m_z = m_z.isReferenced() ? ref(m_z) : m_z;
  ObjectData::cloneSet(clone);
}
Variant c_vector::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x422EE21DDEFF84D1LL, normalized) {
        return (t_normalized());
      }
      break;
    case 2:
      HASH_GUARD(0x1F0BEA0B36F33CA2LL, scaledby) {
        return (t_scaledby(params.rvalAt(0)));
      }
      break;
    case 5:
      HASH_GUARD(0x07F2FE0956EFAE75LL, minus) {
        return (t_minus(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    case 7:
      HASH_GUARD(0x5E8B71F5814EF937LL, plus) {
        return (t_plus(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      HASH_GUARD(0x3040FFCBB68C33A7LL, dot) {
        return (t_dot(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_vector::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x422EE21DDEFF84D1LL, normalized) {
        return (t_normalized());
      }
      break;
    case 2:
      HASH_GUARD(0x1F0BEA0B36F33CA2LL, scaledby) {
        return (t_scaledby(a0));
      }
      break;
    case 5:
      HASH_GUARD(0x07F2FE0956EFAE75LL, minus) {
        return (t_minus(ref(a0)));
      }
      break;
    case 7:
      HASH_GUARD(0x5E8B71F5814EF937LL, plus) {
        return (t_plus(ref(a0)));
      }
      HASH_GUARD(0x3040FFCBB68C33A7LL, dot) {
        return (t_dot(ref(a0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_vector::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_vector$os_get(const char *s) {
  return c_vector::os_get(s, -1);
}
Variant &cw_vector$os_lval(const char *s) {
  return c_vector::os_lval(s, -1);
}
Variant cw_vector$os_constant(const char *s) {
  return c_vector::os_constant(s);
}
Variant cw_vector$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_vector::os_invoke(c, s, params, -1, fatal);
}
void c_vector::init() {
  m_x = null;
  m_y = null;
  m_z = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 20 */
void c_vector::t_vector(CVarRef v_x, CVarRef v_y, CVarRef v_z) {
  INSTANCE_METHOD_INJECTION(Vector, Vector::Vector);
  bool oldInCtor = gasInCtor(true);
  (m_x = v_x);
  (m_y = v_y);
  (m_z = v_z);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 24 */
p_vector c_vector::t_plus(Variant v_b) {
  INSTANCE_METHOD_INJECTION(Vector, Vector::plus);
  return ((Object)(LINE(26,p_vector(p_vector(NEWOBJ(c_vector)())->create(m_x + toObject(v_b).o_get("x", 0x04BFC205E59FA416LL), m_y + toObject(v_b).o_get("y", 0x4F56B733A4DFC78ALL), m_z + toObject(v_b).o_get("z", 0x62A103F6518DE2B3LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 29 */
p_vector c_vector::t_minus(Variant v_b) {
  INSTANCE_METHOD_INJECTION(Vector, Vector::minus);
  return ((Object)(LINE(31,p_vector(p_vector(NEWOBJ(c_vector)())->create(m_x - toObject(v_b).o_get("x", 0x04BFC205E59FA416LL), m_y - toObject(v_b).o_get("y", 0x4F56B733A4DFC78ALL), m_z - toObject(v_b).o_get("z", 0x62A103F6518DE2B3LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 34 */
PlusOperand c_vector::t_dot(Variant v_b) {
  INSTANCE_METHOD_INJECTION(Vector, Vector::dot);
  return (m_x * toObject(v_b).o_get("x", 0x04BFC205E59FA416LL)) + (m_y * toObject(v_b).o_get("y", 0x4F56B733A4DFC78ALL)) + (m_z * toObject(v_b).o_get("z", 0x62A103F6518DE2B3LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 39 */
p_vector c_vector::t_scaledby(CVarRef v_s) {
  INSTANCE_METHOD_INJECTION(Vector, Vector::scaledBy);
  return ((Object)(LINE(40,p_vector(p_vector(NEWOBJ(c_vector)())->create(m_x * v_s, m_y * v_s, m_z * v_s)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 43 */
p_vector c_vector::t_normalized() {
  INSTANCE_METHOD_INJECTION(Vector, Vector::normalized);
  return ((Object)(LINE(44,t_scaledby(divide(1.0, x_sqrt(toDouble(t_dot(ref(this)))))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 162 */
Variant c_group::os_get(const char *s, int64 hash) {
  return c_scene::os_get(s, hash);
}
Variant &c_group::os_lval(const char *s, int64 hash) {
  return c_scene::os_lval(s, hash);
}
void c_group::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("bound", m_bound.isReferenced() ? ref(m_bound) : m_bound));
  props.push_back(NEW(ArrayElement)("scenes", m_scenes.isReferenced() ? ref(m_scenes) : m_scenes));
  c_scene::o_get(props);
}
bool c_group::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x1200603171C0F3B0LL, bound, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x5EC93B9A46EA3E2ALL, scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_exists(s, hash);
}
Variant c_group::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x1200603171C0F3B0LL, m_bound,
                         bound, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x5EC93B9A46EA3E2ALL, m_scenes,
                         scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_get(s, hash);
}
Variant c_group::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x1200603171C0F3B0LL, m_bound,
                      bound, 5);
      break;
    case 2:
      HASH_SET_STRING(0x5EC93B9A46EA3E2ALL, m_scenes,
                      scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_set(s, hash, v, forInit);
}
Variant &c_group::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x1200603171C0F3B0LL, m_bound,
                         bound, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x5EC93B9A46EA3E2ALL, m_scenes,
                         scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_lval(s, hash);
}
Variant c_group::os_constant(const char *s) {
  return c_scene::os_constant(s);
}
IMPLEMENT_CLASS(group)
ObjectData *c_group::create(Variant v_bound) {
  init();
  t_group(ref(v_bound));
  return this;
}
ObjectData *c_group::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(ref(const_cast<Array&>(params).lvalAt(0))));
  } else return this;
}
ObjectData *c_group::cloneImpl() {
  c_group *obj = NEW(c_group)();
  cloneSet(obj);
  return obj;
}
void c_group::cloneSet(c_group *clone) {
  clone->m_bound = m_bound.isReferenced() ? ref(m_bound) : m_bound;
  clone->m_scenes = m_scenes.isReferenced() ? ref(m_scenes) : m_scenes;
  c_scene::cloneSet(clone);
}
Variant c_group::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke(s, params, hash, fatal);
}
Variant c_group::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(a0), ref(a1)));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(a0), ref(a1)));
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_group::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_scene::os_invoke(c, s, params, hash, fatal);
}
Variant cw_group$os_get(const char *s) {
  return c_group::os_get(s, -1);
}
Variant &cw_group$os_lval(const char *s) {
  return c_group::os_lval(s, -1);
}
Variant cw_group$os_constant(const char *s) {
  return c_group::os_constant(s);
}
Variant cw_group$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_group::os_invoke(c, s, params, -1, fatal);
}
void c_group::init() {
  c_scene::init();
  m_bound = null;
  m_scenes = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 165 */
void c_group::t_group(Variant v_bound) {
  INSTANCE_METHOD_INJECTION(Group, Group::Group);
  bool oldInCtor = gasInCtor(true);
  (m_bound = v_bound);
  (m_scenes = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 170 */
Variant c_group::t_intersect(Variant v_ray, Variant v_p) {
  INSTANCE_METHOD_INJECTION(Group, Group::intersect);
  Variant v_each;

  if (less((LINE(171,m_bound.o_invoke_few_args("distance", 0x5225E2593FAA1157LL, 1, v_ray))), v_p.o_get("distance", 0x1E9F6AF49FD61031LL))) {
    {
      LOOP_COUNTER(1);
      Variant map2 = m_scenes;
      for (ArrayIterPtr iter3 = map2.begin("group"); !iter3->end(); iter3->next()) {
        LOOP_COUNTER_CHECK(1);
        v_each = iter3->second();
        {
          (v_p = LINE(173,v_each.o_invoke_few_args("intersect", 0x4A3F7CCEE998444ALL, 2, v_ray, v_p)));
        }
      }
    }
  }
  return v_p;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 179 */
void c_group::t_add(Variant v_s) {
  INSTANCE_METHOD_INJECTION(Group, Group::add);
  LINE(180,x_array_unshift(2, ref(lval(m_scenes)), v_s));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 124 */
Variant c_sphere::os_get(const char *s, int64 hash) {
  return c_scene::os_get(s, hash);
}
Variant &c_sphere::os_lval(const char *s, int64 hash) {
  return c_scene::os_lval(s, hash);
}
void c_sphere::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("center", m_center.isReferenced() ? ref(m_center) : m_center));
  props.push_back(NEW(ArrayElement)("radius", m_radius.isReferenced() ? ref(m_radius) : m_radius));
  c_scene::o_get(props);
}
bool c_sphere::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x13251570EF5983D9LL, center, 6);
      HASH_EXISTS_STRING(0x7FC3CA32D2D213D5LL, radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_exists(s, hash);
}
Variant c_sphere::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x13251570EF5983D9LL, m_center,
                         center, 6);
      HASH_RETURN_STRING(0x7FC3CA32D2D213D5LL, m_radius,
                         radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_get(s, hash);
}
Variant c_sphere::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x13251570EF5983D9LL, m_center,
                      center, 6);
      HASH_SET_STRING(0x7FC3CA32D2D213D5LL, m_radius,
                      radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_set(s, hash, v, forInit);
}
Variant &c_sphere::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x13251570EF5983D9LL, m_center,
                         center, 6);
      HASH_RETURN_STRING(0x7FC3CA32D2D213D5LL, m_radius,
                         radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_lval(s, hash);
}
Variant c_sphere::os_constant(const char *s) {
  return c_scene::os_constant(s);
}
IMPLEMENT_CLASS(sphere)
ObjectData *c_sphere::create(Variant v_center, Numeric v_radius) {
  init();
  t_sphere(ref(v_center), v_radius);
  return this;
}
ObjectData *c_sphere::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
  } else return this;
}
ObjectData *c_sphere::cloneImpl() {
  c_sphere *obj = NEW(c_sphere)();
  cloneSet(obj);
  return obj;
}
void c_sphere::cloneSet(c_sphere *clone) {
  clone->m_center = m_center.isReferenced() ? ref(m_center) : m_center;
  clone->m_radius = m_radius.isReferenced() ? ref(m_radius) : m_radius;
  c_scene::cloneSet(clone);
}
Variant c_sphere::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      break;
    case 7:
      HASH_GUARD(0x5225E2593FAA1157LL, distance) {
        return (t_distance(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke(s, params, hash, fatal);
}
Variant c_sphere::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(a0), ref(a1)));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(a0), ref(a1)));
      }
      break;
    case 7:
      HASH_GUARD(0x5225E2593FAA1157LL, distance) {
        return (t_distance(ref(a0)));
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sphere::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_scene::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sphere$os_get(const char *s) {
  return c_sphere::os_get(s, -1);
}
Variant &cw_sphere$os_lval(const char *s) {
  return c_sphere::os_lval(s, -1);
}
Variant cw_sphere$os_constant(const char *s) {
  return c_sphere::os_constant(s);
}
Variant cw_sphere$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sphere::os_invoke(c, s, params, -1, fatal);
}
void c_sphere::init() {
  c_scene::init();
  m_center = null;
  m_radius = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 127 */
void c_sphere::t_sphere(Variant v_center, Numeric v_radius) {
  INSTANCE_METHOD_INJECTION(Sphere, Sphere::Sphere);
  bool oldInCtor = gasInCtor(true);
  (m_center = v_center);
  (m_radius = v_radius);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 132 */
Variant c_sphere::t_distance(Variant v_ray) {
  INSTANCE_METHOD_INJECTION(Sphere, Sphere::distance);
  Variant v_v;
  Variant v_b;
  PlusOperand v_disc = 0;
  double v_d = 0.0;
  double v_t1 = 0.0;
  double v_t2 = 0.0;

  (v_v = LINE(133,m_center.o_invoke_few_args("minus", 0x07F2FE0956EFAE75LL, 1, toObject(v_ray).o_lval("origin", 0x6383DE2A8EE50332LL))));
  (v_b = LINE(134,v_v.o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, toObject(v_ray).o_lval("direction", 0x10014728FF258839LL))));
  (v_disc = v_b * v_b - LINE(135,v_v.o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, v_v)) + m_radius * m_radius);
  if (less(v_disc, 0.0)) {
    return get_global_variables()->k_INFINITY;
  }
  (v_d = LINE(138,x_sqrt(toDouble(v_disc))));
  (v_t1 = toDouble(v_b) + v_d);
  if (less(v_t1, 0.0)) {
    return get_global_variables()->k_INFINITY;
  }
  (v_t2 = toDouble(v_b) - v_d);
  if (more(v_t2, 0.0)) {
    return v_t2;
  }
  else {
    return v_t1;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 146 */
Variant c_sphere::t_intersect(Variant v_ray, Variant v_p) {
  INSTANCE_METHOD_INJECTION(Sphere, Sphere::intersect);
  Variant eo_0;
  Variant eo_1;
  Variant v_d;
  Variant v_rayOrigin;
  Variant v_rayDirection;
  Variant v_scaledDirection;
  Variant v_v;

  (v_d = LINE(147,t_distance(ref(v_ray))));
  if (less(v_d, v_p.o_get("distance", 0x1E9F6AF49FD61031LL))) {
    (v_rayOrigin = v_ray.o_get("origin", 0x6383DE2A8EE50332LL));
    (v_rayDirection = v_ray.o_get("direction", 0x10014728FF258839LL));
    (v_scaledDirection = LINE(151,v_rayDirection.o_invoke_few_args("scaledBy", 0x1F0BEA0B36F33CA2LL, 1, v_d)));
    (v_v = LINE(153,v_rayOrigin.o_invoke_few_args("plus", 0x5E8B71F5814EF937LL, 1, v_scaledDirection.o_invoke_few_args("minus", 0x07F2FE0956EFAE75LL, 1, m_center))));
    (v_p = ((Object)(LINE(154,(assignCallTemp(eo_0, v_d),assignCallTemp(eo_1, ref(v_v.o_invoke_few_args("normalized", 0x422EE21DDEFF84D1LL, 0))),p_intersectionpoint(p_intersectionpoint(NEWOBJ(c_intersectionpoint)())->create(eo_0, eo_1)))))));
  }
  return v_p;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 50 */
Variant c_ray::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ray::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ray::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("origin", m_origin.isReferenced() ? ref(m_origin) : m_origin));
  props.push_back(NEW(ArrayElement)("direction", m_direction.isReferenced() ? ref(m_direction) : m_direction));
  c_ObjectData::o_get(props);
}
bool c_ray::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x10014728FF258839LL, direction, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x6383DE2A8EE50332LL, origin, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ray::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x10014728FF258839LL, m_direction,
                         direction, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x6383DE2A8EE50332LL, m_origin,
                         origin, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ray::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x10014728FF258839LL, m_direction,
                      direction, 9);
      break;
    case 2:
      HASH_SET_STRING(0x6383DE2A8EE50332LL, m_origin,
                      origin, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ray::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x10014728FF258839LL, m_direction,
                         direction, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x6383DE2A8EE50332LL, m_origin,
                         origin, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ray::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ray)
ObjectData *c_ray::create(Variant v_origin, Variant v_direction) {
  init();
  t_ray(ref(v_origin), ref(v_direction));
  return this;
}
ObjectData *c_ray::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
  } else return this;
}
ObjectData *c_ray::cloneImpl() {
  c_ray *obj = NEW(c_ray)();
  cloneSet(obj);
  return obj;
}
void c_ray::cloneSet(c_ray *clone) {
  clone->m_origin = m_origin.isReferenced() ? ref(m_origin) : m_origin;
  clone->m_direction = m_direction.isReferenced() ? ref(m_direction) : m_direction;
  ObjectData::cloneSet(clone);
}
Variant c_ray::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ray::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ray::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ray$os_get(const char *s) {
  return c_ray::os_get(s, -1);
}
Variant &cw_ray$os_lval(const char *s) {
  return c_ray::os_lval(s, -1);
}
Variant cw_ray$os_constant(const char *s) {
  return c_ray::os_constant(s);
}
Variant cw_ray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ray::os_invoke(c, s, params, -1, fatal);
}
void c_ray::init() {
  m_origin = null;
  m_direction = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 53 */
void c_ray::t_ray(Variant v_origin, Variant v_direction) {
  INSTANCE_METHOD_INJECTION(Ray, Ray::Ray);
  bool oldInCtor = gasInCtor(true);
  (m_origin = v_origin);
  (m_direction = v_direction);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 70 */
Variant c_scene::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_scene::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_scene::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_scene::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_scene::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_scene::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_scene::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_scene::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(scene)
ObjectData *c_scene::cloneImpl() {
  c_scene *obj = NEW(c_scene)();
  cloneSet(obj);
  return obj;
}
void c_scene::cloneSet(c_scene *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_scene::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_scene::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(a0), ref(a1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_scene::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_scene$os_get(const char *s) {
  return c_scene::os_get(s, -1);
}
Variant &cw_scene$os_lval(const char *s) {
  return c_scene::os_lval(s, -1);
}
Variant cw_scene$os_constant(const char *s) {
  return c_scene::os_constant(s);
}
Variant cw_scene$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_scene::os_invoke(c, s, params, -1, fatal);
}
void c_scene::init() {
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 72 */
Variant c_scene::ti_spherescene(const char* cls, Numeric v_level, Variant v_center, Numeric v_radius) {
  STATIC_METHOD_INJECTION(Scene, Scene::sphereScene);
  Variant v_sphere;
  p_group v_scene;
  Numeric v_rn = 0;
  int64 v_dz = 0;
  int64 v_dx = 0;
  Variant v_c2;

  (v_sphere = ((Object)(LINE(73,p_sphere(p_sphere(NEWOBJ(c_sphere)())->create(ref(v_center), v_radius))))));
  if (equal(v_level, 1LL)) {
    return v_sphere;
  }
  else {
    ((Object)((v_scene = ((Object)(LINE(78,p_group(p_group(NEWOBJ(c_group)())->create(ref(p_sphere(p_sphere(NEWOBJ(c_sphere)())->create(ref(v_center), 3.0 * toDouble(v_radius))))))))))));
    LINE(79,v_scene->t_add(ref(v_sphere)));
    (v_rn = divide(3.0 * toDouble(v_radius), LINE(80,x_sqrt(12.0))));
    {
      LOOP_COUNTER(4);
      for ((v_dz = -1LL); not_more(v_dz, 1LL); v_dz += 2LL) {
        LOOP_COUNTER_CHECK(4);
        {
          {
            LOOP_COUNTER(5);
            for ((v_dx = -1LL); not_more(v_dx, 1LL); v_dx += 2LL) {
              LOOP_COUNTER_CHECK(5);
              {
                (v_c2 = ((Object)(LINE(89,p_vector(p_vector(NEWOBJ(c_vector)())->create(v_center.o_get("x", 0x04BFC205E59FA416LL) - v_dx * v_rn, v_center.o_get("y", 0x4F56B733A4DFC78ALL) + v_rn, v_center.o_get("z", 0x62A103F6518DE2B3LL) - v_dz * v_rn))))));
                LINE(91,v_scene->t_add(ref(c_scene::t_spherescene(v_level - 1LL, ref(v_c2), divide(v_radius, 2.0)))));
              }
            }
          }
        }
      }
    }
    return ((Object)(v_scene));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php line 98 */
Variant c_scene::t_traceray(Variant v_ray, Variant v_light) {
  INSTANCE_METHOD_INJECTION(Scene, Scene::traceRay);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_p;
  double v_greyscale = 0.0;
  Variant v_rayOrigin;
  Variant v_scaledDirection;
  Variant v_scaledNormal;
  Variant v_o;
  p_vector v_v;
  Variant v_shadowRay;
  Variant v_shadowp;

  (v_p = (assignCallTemp(eo_0, ref(v_ray)),assignCallTemp(eo_1, ref(LINE(100,(assignCallTemp(eo_2, get_global_variables()->k_INFINITY),assignCallTemp(eo_3, ref(p_vector(p_vector(NEWOBJ(c_vector)())->create(0.0, 0.0, 0.0)))),p_intersectionpoint(p_intersectionpoint(NEWOBJ(c_intersectionpoint)())->create(eo_2, eo_3)))))),o_root_invoke("intersect", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x4A3F7CCEE998444ALL)));
  if (LINE(101,x_is_infinite(toDouble(v_p.o_get("distance", 0x1E9F6AF49FD61031LL))))) {
    return 0.0;
  }
  (v_greyscale = -1.0 * toDouble((LINE(103,v_p.o_get("normal", 0x5847C6837EFC3014LL).o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, v_light)))));
  if (not_more(v_greyscale, 0.0)) {
    return 0.0;
  }
  (v_rayOrigin = v_ray.o_get("origin", 0x6383DE2A8EE50332LL));
  (v_scaledDirection = LINE(107,v_ray.o_get("direction", 0x10014728FF258839LL).o_invoke_few_args("scaledBy", 0x1F0BEA0B36F33CA2LL, 1, v_p.o_lval("distance", 0x1E9F6AF49FD61031LL))));
  (v_scaledNormal = LINE(108,v_p.o_get("normal", 0x5847C6837EFC3014LL).o_invoke_few_args("scaledBy", 0x1F0BEA0B36F33CA2LL, 1, 1.49012E-8 /* EPSILON */)));
  (v_o = LINE(109,v_rayOrigin.o_invoke_few_args("plus", 0x5E8B71F5814EF937LL, 1, v_scaledDirection)));
  (v_o = LINE(110,v_o.o_invoke_few_args("plus", 0x5E8B71F5814EF937LL, 1, v_scaledNormal)));
  ((Object)((v_v = ((Object)(LINE(112,p_vector(p_vector(NEWOBJ(c_vector)())->create(0.0, 0.0, 0.0))))))));
  (v_shadowRay = ((Object)(LINE(113,(assignCallTemp(eo_0, ref(v_o)),assignCallTemp(eo_1, ref(v_v->t_minus(ref(v_light)))),p_ray(p_ray(NEWOBJ(c_ray)())->create(eo_0, eo_1)))))));
  (v_shadowp = (assignCallTemp(eo_0, ref(v_shadowRay)),assignCallTemp(eo_1, ref(LINE(115,p_intersectionpoint(p_intersectionpoint(NEWOBJ(c_intersectionpoint)())->create(get_global_variables()->k_INFINITY, ref(lval(v_p.o_lval("normal", 0x5847C6837EFC3014LL)))))))),o_root_invoke("intersect", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x4A3F7CCEE998444ALL)));
  if (LINE(117,x_is_infinite(toDouble(v_shadowp.o_get("distance", 0x1E9F6AF49FD61031LL))))) {
    return v_greyscale;
  }
  else {
    return 0.0;
  }
  return null;
} /* function */
Object co_intersectionpoint(CArrRef params, bool init /* = true */) {
  return Object(p_intersectionpoint(NEW(c_intersectionpoint)())->dynCreate(params, init));
}
Object co_vector(CArrRef params, bool init /* = true */) {
  return Object(p_vector(NEW(c_vector)())->dynCreate(params, init));
}
Object co_group(CArrRef params, bool init /* = true */) {
  return Object(p_group(NEW(c_group)())->dynCreate(params, init));
}
Object co_sphere(CArrRef params, bool init /* = true */) {
  return Object(p_sphere(NEW(c_sphere)())->dynCreate(params, init));
}
Object co_ray(CArrRef params, bool init /* = true */) {
  return Object(p_ray(NEW(c_ray)())->dynCreate(params, init));
}
Object co_scene(CArrRef params, bool init /* = true */) {
  return Object(p_scene(NEW(c_scene)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$raytracer_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/raytracer.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$raytracer_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_scene __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scene") : g->GV(scene);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_greyscale __attribute__((__unused__)) = (variables != gVariables) ? variables->get("greyscale") : g->GV(greyscale);
  Variant &v_dx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dx") : g->GV(dx);
  Variant &v_dy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dy") : g->GV(dy);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_ray __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ray") : g->GV(ray);
  Variant &v_u __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u") : g->GV(u);

  LINE(11,g->declareConstant("INFINITY", g->k_INFINITY, x_log(toDouble(0LL)) * -1.0));
  ;
  ;
  ;
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_scene = LINE(188,(assignCallTemp(eo_1, ref(p_vector(p_vector(NEWOBJ(c_vector)())->create(0.0, -1.0, 0.0)))),c_scene::t_spherescene(6LL /* LEVELS */, eo_1, 1.0))));
  LINE(190,x_printf(3, "P5\n%d %d\n255\n", Array(ArrayInit(2).set(0, v_n).set(1, v_n).create())));
  {
    LOOP_COUNTER(6);
    for ((v_y = v_n - 1LL); not_less(v_y, 0LL); --v_y) {
      LOOP_COUNTER_CHECK(6);
      {
        {
          LOOP_COUNTER(7);
          for ((v_x = 0LL); less(v_x, v_n); ++v_x) {
            LOOP_COUNTER_CHECK(7);
            {
              (v_greyscale = 0.0);
              {
                LOOP_COUNTER(8);
                for ((v_dx = 0LL); less(v_dx, 4LL /* SS */); ++v_dx) {
                  LOOP_COUNTER_CHECK(8);
                  {
                    {
                      LOOP_COUNTER(9);
                      for ((v_dy = 0LL); less(v_dy, 4LL /* SS */); ++v_dy) {
                        LOOP_COUNTER_CHECK(9);
                        {
                          (v_v = ((Object)(LINE(202,p_vector(p_vector(NEWOBJ(c_vector)())->create(v_x + (divide(v_dx, 4.0)) - (divide(v_n, 2.0)), v_y + (divide(v_dy, 4.0)) - (divide(v_n, 2.0)), v_n))))));
                          (v_ray = ((Object)(LINE(204,(assignCallTemp(eo_0, ref(p_vector(p_vector(NEWOBJ(c_vector)())->create(0.0, 0.0, -4.0)))),assignCallTemp(eo_1, ref(v_v.o_invoke_few_args("normalized", 0x422EE21DDEFF84D1LL, 0))),p_ray(p_ray(NEWOBJ(c_ray)())->create(eo_0, eo_1)))))));
                          (v_u = ((Object)(LINE(206,p_vector(p_vector(NEWOBJ(c_vector)())->create(-1.0, -3.0, 2.0))))));
                          v_greyscale += (assignCallTemp(eo_0, ref(v_ray)),assignCallTemp(eo_1, ref(LINE(207,v_u.o_invoke_few_args("normalized", 0x422EE21DDEFF84D1LL, 0)))),v_scene.o_invoke("traceRay", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x20DF5A198C012382LL));
                        }
                      }
                    }
                  }
                }
              }
              echo(LINE(210,x_chr(toInt64(0.5 + toDouble(divide(255.0 * toDouble(v_greyscale), 16.0))))));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
