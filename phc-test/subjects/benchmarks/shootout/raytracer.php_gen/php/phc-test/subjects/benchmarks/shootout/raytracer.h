
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/raytracer.fw.h>

// Declarations
#include <cls/intersectionpoint.h>
#include <cls/vector.h>
#include <cls/group.h>
#include <cls/sphere.h>
#include <cls/ray.h>
#include <cls/scene.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$raytracer_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_intersectionpoint(CArrRef params, bool init = true);
Object co_vector(CArrRef params, bool init = true);
Object co_group(CArrRef params, bool init = true);
Object co_sphere(CArrRef params, bool init = true);
Object co_ray(CArrRef params, bool init = true);
Object co_scene(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_h__
