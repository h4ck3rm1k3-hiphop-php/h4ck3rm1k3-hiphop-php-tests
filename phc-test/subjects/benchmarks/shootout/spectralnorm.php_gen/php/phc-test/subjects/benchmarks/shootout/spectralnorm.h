
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_spectralnorm_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_spectralnorm_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/spectralnorm.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_av(CVarRef v_n, CVarRef v_v);
Variant f_atv(CVarRef v_n, CVarRef v_v);
Variant pm_php$phc_test$subjects$benchmarks$shootout$spectralnorm_php(bool incOnce = false, LVariableTable* variables = NULL);
Numeric f_a(int64 v_i, int64 v_j);
Variant f_atav(CVarRef v_n, CVarRef v_v);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_spectralnorm_h__
