
#include <php/phc-test/subjects/benchmarks/shootout/knucleotide.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 111 */
int64 f_freq_name_comparator(CVarRef v_val1, CVarRef v_val2) {
  FUNCTION_INJECTION(freq_name_comparator);
  Numeric v_delta = 0;
  int64 v_result = 0;

  (v_delta = v_val2.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) - v_val1.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (v_result = toBoolean((equal(v_delta, 0LL)) ? ((Variant)(LINE(114,x_strcmp(toString(v_val1.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toString(v_val2.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))))) : ((Variant)((less(v_delta, 0LL))))) ? ((-1LL)) : ((1LL)));
  return v_result;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 96 */
void f_compute_freq(Variant v_count_freq, CVarRef v_key, CVarRef v_total) {
  FUNCTION_INJECTION(compute_freq);
  (v_count_freq = divide((v_count_freq * 100LL), v_total));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 107 */
void f_append_key(Variant v_val, CVarRef v_key) {
  FUNCTION_INJECTION(append_key);
  (v_val = Array(ArrayInit(2).set(0, v_val).set(1, v_key).create()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 100 */
void f_sort_by_freq_and_name(Variant v_map) {
  FUNCTION_INJECTION(sort_by_freq_and_name);
  LINE(102,x_array_walk(ref(v_map), "append_key"));
  LINE(103,x_uasort(ref(v_map), "freq_name_comparator"));
  LINE(104,x_array_walk(ref(v_map), "remove_key"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 72 */
void f_write_count(Variant v_sequence, CStrRef v_key) {
  FUNCTION_INJECTION(write_count);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_map;

  (v_map = LINE(73,(assignCallTemp(eo_0, ref(v_sequence)),assignCallTemp(eo_1, toInt64(x_strlen(v_key))),f_generate_frequencies(eo_0, eo_1, false))));
  LINE(74,(assignCallTemp(eo_1, (x_array_key_exists(v_key, ref(v_map))) ? ((Variant)(v_map.rvalAt(v_key))) : ((Variant)(0LL))),assignCallTemp(eo_2, v_key),x_printf(3, "%d\t%s\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 30 */
String f_read_sequence(CStrRef v_id) {
  FUNCTION_INJECTION(read_sequence);
  int v_ln_id = 0;
  Variant v_line;
  String v_sequence;

  (v_ln_id = LINE(31,x_strlen(v_id)));
  LOOP_COUNTER(1);
  {
    while (!(LINE(33,x_feof(toObject(STDIN))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_line = LINE(34,x_fgets(toObject(STDIN), 255LL)));
        if (equal(v_line.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), ">") && equal(LINE(35,x_substr(toString(v_line), toInt32(1LL), v_ln_id)), v_id)) {
          break;
        }
      }
    }
  }
  if (LINE(39,x_feof(toObject(STDIN)))) {
    f_exit(-1LL);
  }
  (v_sequence = "");
  LOOP_COUNTER(2);
  {
    while (!(LINE(45,x_feof(toObject(STDIN))))) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_line = LINE(46,x_fgets(toObject(STDIN), 100LL)));
        {
          Variant tmp4 = (v_line.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          int tmp5 = -1;
          if (equal(tmp4, (";"))) {
            tmp5 = 0;
          } else if (equal(tmp4, (">"))) {
            tmp5 = 1;
          } else if (true) {
            tmp5 = 2;
          }
          switch (tmp5) {
          case 0:
            {
              goto continue3;
            }
          case 1:
            {
              goto break2;
            }
          case 2:
            {
              concat_assign(v_sequence, LINE(57,x_strtoupper(x_rtrim(toString(v_line)))));
            }
          }
          continue3:;
        }
      }
    }
    break2:;
  }
  return v_sequence;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 63 */
void f_write_freq(Variant v_sequence, int64 v_key_length) {
  FUNCTION_INJECTION(write_freq);
  Variant v_map;
  Primitive v_key = 0;
  Variant v_val;

  (v_map = LINE(64,f_generate_frequencies(ref(v_sequence), v_key_length)));
  LINE(65,f_sort_by_freq_and_name(ref(v_map)));
  {
    LOOP_COUNTER(6);
    for (ArrayIterPtr iter8 = v_map.begin(); !iter8->end(); iter8->next()) {
      LOOP_COUNTER_CHECK(6);
      v_val = iter8->second();
      v_key = iter8->first();
      {
        LINE(67,x_printf(3, "%s %.3f\n", Array(ArrayInit(2).set(0, v_key).set(1, v_val).create())));
      }
    }
  }
  echo("\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 119 */
void f_remove_key(Variant v_val, CVarRef v_key) {
  FUNCTION_INJECTION(remove_key);
  (v_val = v_val.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/knucleotide.php line 80 */
Variant f_generate_frequencies(Variant v_sequence, int64 v_key_length, bool v_compute_freq //  = true
) {
  FUNCTION_INJECTION(generate_frequencies);
  Variant v_result;
  int64 v_total = 0;
  int64 v_i = 0;
  Variant v_key;

  (v_result = ScalarArrays::sa_[0]);
  (v_total = LINE(82,x_strlen(toString(v_sequence))) - v_key_length + 1LL);
  (v_i = v_total);
  LOOP_COUNTER(9);
  {
    while (not_less(--v_i, 0LL)) {
      LOOP_COUNTER_CHECK(9);
      {
        (v_key = LINE(86,x_substr(toString(v_sequence), toInt32(v_i), toInt32(v_key_length))));
        if (!(LINE(87,x_array_key_exists(v_key, ref(v_result))))) v_result.set(v_key, (0LL));
        lval(v_result.lvalAt(v_key))++;
      }
    }
  }
  if (v_compute_freq) {
    LINE(91,x_array_walk(ref(v_result), "compute_freq", v_total));
  }
  return v_result;
} /* function */
Variant i_freq_name_comparator(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x72060474DD04591ELL, freq_name_comparator) {
    return (f_freq_name_comparator(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_compute_freq(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x28DB19DF5C95ED9ALL, compute_freq) {
    return (f_compute_freq(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_append_key(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4ED2703769D65868LL, append_key) {
    return (f_append_key(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_remove_key(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x75D5D46D024183DCLL, remove_key) {
    return (f_remove_key(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$knucleotide_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/knucleotide.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$knucleotide_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_sequence __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sequence") : g->GV(sequence);

  (v_sequence = LINE(15,f_read_sequence("THREE")));
  LINE(17,x_fclose(toObject(STDIN)));
  LINE(20,f_write_freq(ref(v_sequence), 1LL));
  LINE(21,f_write_freq(ref(v_sequence), 2LL));
  LINE(22,f_write_count(ref(v_sequence), "GGT"));
  LINE(23,f_write_count(ref(v_sequence), "GGTA"));
  LINE(24,f_write_count(ref(v_sequence), "GGTATT"));
  LINE(25,f_write_count(ref(v_sequence), "GGTATTTTAATT"));
  LINE(26,f_write_count(ref(v_sequence), "GGTATTTTAATTTATAGT"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
