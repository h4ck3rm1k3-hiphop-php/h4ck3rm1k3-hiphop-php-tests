
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_knucleotide_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_knucleotide_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/knucleotide.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_freq_name_comparator(CVarRef v_val1, CVarRef v_val2);
void f_compute_freq(Variant v_count_freq, CVarRef v_key, CVarRef v_total);
Variant pm_php$phc_test$subjects$benchmarks$shootout$knucleotide_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_append_key(Variant v_val, CVarRef v_key);
void f_sort_by_freq_and_name(Variant v_map);
void f_write_count(Variant v_sequence, CStrRef v_key);
String f_read_sequence(CStrRef v_id);
void f_write_freq(Variant v_sequence, int64 v_key_length);
void f_remove_key(Variant v_val, CVarRef v_key);
Variant f_generate_frequencies(Variant v_sequence, int64 v_key_length, bool v_compute_freq = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_knucleotide_h__
