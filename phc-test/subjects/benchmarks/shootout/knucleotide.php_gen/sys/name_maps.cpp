
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "append_key", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "compute_freq", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "freq_name_comparator", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "generate_frequencies", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "read_sequence", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "remove_key", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "sort_by_freq_and_name", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "write_count", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  "write_freq", "phc-test/subjects/benchmarks/shootout/knucleotide.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
