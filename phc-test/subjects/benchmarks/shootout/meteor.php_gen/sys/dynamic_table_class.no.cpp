
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_board(CArrRef params, bool init = true);
Variant cw_board$os_get(const char *s);
Variant &cw_board$os_lval(const char *s);
Variant cw_board$os_constant(const char *s);
Variant cw_board$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_cell(CArrRef params, bool init = true);
Variant cw_cell$os_get(const char *s);
Variant &cw_cell$os_lval(const char *s);
Variant cw_cell$os_constant(const char *s);
Variant cw_cell$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_piececell(CArrRef params, bool init = true);
Variant cw_piececell$os_get(const char *s);
Variant &cw_piececell$os_lval(const char *s);
Variant cw_piececell$os_constant(const char *s);
Variant cw_piececell$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_boardcell(CArrRef params, bool init = true);
Variant cw_boardcell$os_get(const char *s);
Variant &cw_boardcell$os_lval(const char *s);
Variant cw_boardcell$os_constant(const char *s);
Variant cw_boardcell$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_piece(CArrRef params, bool init = true);
Variant cw_piece$os_get(const char *s);
Variant &cw_piece$os_lval(const char *s);
Variant cw_piece$os_constant(const char *s);
Variant cw_piece$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_solver(CArrRef params, bool init = true);
Variant cw_solver$os_get(const char *s);
Variant &cw_solver$os_lval(const char *s);
Variant cw_solver$os_constant(const char *s);
Variant cw_solver$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_CREATE_OBJECT(0x5B5DCFC208A9CF02LL, boardcell);
      HASH_CREATE_OBJECT(0x7D3432ED2F7745F2LL, piece);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x2204C58E1E3D4E96LL, solver);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x2C40BDD832A30587LL, piececell);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x47851B1F9C2ECB49LL, cell);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x34EC338E5CA6767CLL, board);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x5B5DCFC208A9CF02LL, boardcell);
      HASH_INVOKE_STATIC_METHOD(0x7D3432ED2F7745F2LL, piece);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x2204C58E1E3D4E96LL, solver);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x2C40BDD832A30587LL, piececell);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x47851B1F9C2ECB49LL, cell);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x34EC338E5CA6767CLL, board);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x5B5DCFC208A9CF02LL, boardcell);
      HASH_GET_STATIC_PROPERTY(0x7D3432ED2F7745F2LL, piece);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x2204C58E1E3D4E96LL, solver);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x2C40BDD832A30587LL, piececell);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x47851B1F9C2ECB49LL, cell);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x34EC338E5CA6767CLL, board);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x5B5DCFC208A9CF02LL, boardcell);
      HASH_GET_STATIC_PROPERTY_LV(0x7D3432ED2F7745F2LL, piece);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x2204C58E1E3D4E96LL, solver);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x2C40BDD832A30587LL, piececell);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x47851B1F9C2ECB49LL, cell);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x34EC338E5CA6767CLL, board);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x5B5DCFC208A9CF02LL, boardcell);
      HASH_GET_CLASS_CONSTANT(0x7D3432ED2F7745F2LL, piece);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x2204C58E1E3D4E96LL, solver);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x2C40BDD832A30587LL, piececell);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x47851B1F9C2ECB49LL, cell);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x34EC338E5CA6767CLL, board);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
