
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_BOARD_ROWS;
extern const int64 k_BOARD_COLS;
extern const int64 k_PIECE_FLIPS;
extern const int64 k_SIDES;
extern const int64 k_PIECES;
extern const int64 k_E;
extern const int64 k_NE;
extern const int64 k_PIECE_ROTATIONS;
extern const int64 k_W;
extern const int64 k_NW;
extern const int64 k_SE;
extern const int64 k_PIECE_SIZE;
extern const int64 k_SW;
extern const int64 k_BOARD_SIZE;
extern const int64 k_PIECE_ORIENTATIONS;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 31) {
    case 0:
      HASH_RETURN(0x76C22A3DBEC163A0LL, k_BOARD_SIZE, BOARD_SIZE);
      HASH_RETURN(0x2A9E4406CFA86DC0LL, k_PIECE_SIZE, PIECE_SIZE);
      break;
    case 3:
      HASH_RETURN(0x446EFCE7FE886EC3LL, k_BOARD_COLS, BOARD_COLS);
      break;
    case 8:
      HASH_RETURN(0x3538EEBB0C9CCD08LL, k_SE, SE);
      break;
    case 9:
      HASH_RETURN(0x549099CCDF50B089LL, k_PIECES, PIECES);
      break;
    case 10:
      HASH_RETURN(0x799C602B8BBB7C2ALL, k_W, W);
      HASH_RETURN(0x2C2A25D274BAA0CALL, k_PIECE_ROTATIONS, PIECE_ROTATIONS);
      HASH_RETURN(0x21C214D8BBFB45EALL, k_PIECE_ORIENTATIONS, PIECE_ORIENTATIONS);
      break;
    case 11:
      HASH_RETURN(0x2F74AF893AF2E88BLL, k_NW, NW);
      HASH_RETURN(0x13C415135331018BLL, k_BOARD_ROWS, BOARD_ROWS);
      break;
    case 12:
      HASH_RETURN(0x26EE889E4A63E38CLL, k_NE, NE);
      break;
    case 19:
      HASH_RETURN(0x632D27DFA2D01613LL, k_SW, SW);
      break;
    case 24:
      HASH_RETURN(0x7ACFBE7929F79558LL, k_E, E);
      break;
    case 27:
      HASH_RETURN(0x5D72B40CB658FE7BLL, k_PIECE_FLIPS, PIECE_FLIPS);
      break;
    case 28:
      HASH_RETURN(0x7639E96739F37F9CLL, k_SIDES, SIDES);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
