
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "board", "phc-test/subjects/benchmarks/shootout/meteor.php",
  "boardcell", "phc-test/subjects/benchmarks/shootout/meteor.php",
  "cell", "phc-test/subjects/benchmarks/shootout/meteor.php",
  "piece", "phc-test/subjects/benchmarks/shootout/meteor.php",
  "piececell", "phc-test/subjects/benchmarks/shootout/meteor.php",
  "solver", "phc-test/subjects/benchmarks/shootout/meteor.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
