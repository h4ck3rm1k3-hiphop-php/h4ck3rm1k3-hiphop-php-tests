
#ifndef __GENERATED_cls_piece_h__
#define __GENERATED_cls_piece_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 209 */
class c_piece : virtual public ObjectData {
  BEGIN_CLASS_MAP(piece)
  END_CLASS_MAP(piece)
  DECLARE_CLASS(piece, Piece, ObjectData)
  void init();
  public: Variant m_number;
  public: Numeric m_orientation;
  public: String m_cache;
  public: void t_piece(int64 v_n);
  public: ObjectData *create(int64 v_n);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_unmark();
  public: p_piece t_nextorientation();
  public: Variant t_cell(CVarRef v_i);
  public: void t_make0(Variant v_a);
  public: void t_make1(Variant v_a);
  public: void t_make2(Variant v_a);
  public: void t_make3(Variant v_a);
  public: void t_make4(Variant v_a);
  public: void t_make5(Variant v_a);
  public: void t_make6(Variant v_a);
  public: void t_make7(Variant v_a);
  public: void t_make8(Variant v_a);
  public: void t_make9(Variant v_a);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_piece_h__
