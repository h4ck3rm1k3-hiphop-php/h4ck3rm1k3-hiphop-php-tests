
#ifndef __GENERATED_cls_boardcell_h__
#define __GENERATED_cls_boardcell_h__

#include <cls/cell.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 24 */
class c_boardcell : virtual public c_cell {
  BEGIN_CLASS_MAP(boardcell)
    PARENT_CLASS(cell)
  END_CLASS_MAP(boardcell)
  DECLARE_CLASS(boardcell, BoardCell, cell)
  void init();
  public: Variant m_number;
  public: Variant m_piece;
  public: void t_boardcell(int64 v_i);
  public: ObjectData *create(int64 v_i);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: bool t_isempty();
  public: void t_setempty();
  public: Variant t_contiguousemptycells();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_boardcell_h__
