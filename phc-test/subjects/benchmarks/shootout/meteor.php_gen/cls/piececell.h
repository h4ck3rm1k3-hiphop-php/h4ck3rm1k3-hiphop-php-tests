
#ifndef __GENERATED_cls_piececell_h__
#define __GENERATED_cls_piececell_h__

#include <cls/cell.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 180 */
class c_piececell : virtual public c_cell {
  BEGIN_CLASS_MAP(piececell)
    PARENT_CLASS(cell)
  END_CLASS_MAP(piececell)
  DECLARE_CLASS(piececell, PieceCell, cell)
  void init();
  public: void t_piececell();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_flip();
  public: void t_rotate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_piececell_h__
