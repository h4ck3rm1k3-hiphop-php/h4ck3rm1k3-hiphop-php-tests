
#ifndef __GENERATED_cls_cell_h__
#define __GENERATED_cls_cell_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 16 */
class c_cell : virtual public ObjectData {
  BEGIN_CLASS_MAP(cell)
  END_CLASS_MAP(cell)
  DECLARE_CLASS(cell, Cell, ObjectData)
  void init();
  public: bool m_marked;
  public: Variant m_next;
  public: void t_cell();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_mark();
  public: void t_unmark();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cell_h__
