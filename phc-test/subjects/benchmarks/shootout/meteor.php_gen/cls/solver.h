
#ifndef __GENERATED_cls_solver_h__
#define __GENERATED_cls_solver_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 392 */
class c_solver : virtual public ObjectData {
  BEGIN_CLASS_MAP(solver)
  END_CLASS_MAP(solver)
  DECLARE_CLASS(solver, Solver, ObjectData)
  void init();
  public: Variant m_countdown;
  public: Variant m_n;
  public: Variant m_board;
  public: String m_pieces;
  public: String m_unplaced;
  public: Variant m_unplacedSize;
  public: Variant m_once;
  public: Variant m_first;
  public: Variant m_last;
  public: String m_current;
  public: void t_solver(CVarRef v_arg);
  public: ObjectData *create(CVarRef v_arg);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: bool t_shouldprune();
  public: void t_puzzlesolved();
  public: void t_findsolutions();
  public: void t_printboard(Variant v_s);
  public: void t_printsolutions();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_solver_h__
