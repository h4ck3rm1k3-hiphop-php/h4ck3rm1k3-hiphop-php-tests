
#ifndef __GENERATED_cls_board_h__
#define __GENERATED_cls_board_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 63 */
class c_board : virtual public ObjectData {
  BEGIN_CLASS_MAP(board)
  END_CLASS_MAP(board)
  DECLARE_CLASS(board, Board, ObjectData)
  void init();
  public: String m_cells;
  public: String m_cellsPieceWillFill;
  public: int64 m_cellCount;
  public: String m_cache;
  public: Array m_noFit;
  public: void t_board();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_initializecache();
  public: void t_unmark();
  public: int64 t_firstemptycellindex();
  public: void t_remove(Variant v_p);
  public: void t_find(Variant v_p, Variant v_c);
  public: bool t_add(Variant v_pieceIndex, CVarRef v_boardIndex, Variant v_p);
  public: String t_asstring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_board_h__
