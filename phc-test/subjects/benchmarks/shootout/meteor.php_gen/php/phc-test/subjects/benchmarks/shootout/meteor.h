
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_meteor_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_meteor_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/meteor.fw.h>

// Declarations
#include <cls/board.h>
#include <cls/cell.h>
#include <cls/piececell.h>
#include <cls/boardcell.h>
#include <cls/piece.h>
#include <cls/solver.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$meteor_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_board(CArrRef params, bool init = true);
Object co_cell(CArrRef params, bool init = true);
Object co_piececell(CArrRef params, bool init = true);
Object co_boardcell(CArrRef params, bool init = true);
Object co_piece(CArrRef params, bool init = true);
Object co_solver(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_meteor_h__
