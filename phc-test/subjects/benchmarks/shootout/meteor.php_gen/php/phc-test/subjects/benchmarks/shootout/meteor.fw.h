
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_meteor_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_meteor_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_BOARD_ROWS;
extern const int64 k_BOARD_COLS;
extern const int64 k_PIECE_FLIPS;
extern const int64 k_SIDES;
extern const int64 k_PIECES;
extern const int64 k_E;
extern const int64 k_NE;
extern const int64 k_PIECE_ROTATIONS;
extern const int64 k_W;
extern const int64 k_NW;
extern const int64 k_SE;
extern const int64 k_PIECE_SIZE;
extern const int64 k_SW;
extern const int64 k_BOARD_SIZE;
extern const int64 k_PIECE_ORIENTATIONS;


// 2. Classes
FORWARD_DECLARE_CLASS(board)
FORWARD_DECLARE_CLASS(cell)
FORWARD_DECLARE_CLASS(piececell)
FORWARD_DECLARE_CLASS(boardcell)
FORWARD_DECLARE_CLASS(piece)
FORWARD_DECLARE_CLASS(solver)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_meteor_fw_h__
