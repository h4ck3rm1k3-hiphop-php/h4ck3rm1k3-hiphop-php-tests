
#include <php/phc-test/subjects/benchmarks/shootout/meteor.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_BOARD_ROWS = 10LL;
const int64 k_BOARD_COLS = 5LL;
const int64 k_PIECE_FLIPS = 2LL;
const int64 k_SIDES = 6LL;
const int64 k_PIECES = 10LL;
const int64 k_E = 3LL;
const int64 k_NE = 1LL;
const int64 k_PIECE_ROTATIONS = 6LL /* SIDES */;
const int64 k_W = 2LL;
const int64 k_NW = 0LL;
const int64 k_SE = 5LL;
const int64 k_PIECE_SIZE = 5LL;
const int64 k_SW = 4LL;
const int64 k_BOARD_SIZE = 50LL;
const int64 k_PIECE_ORIENTATIONS = 12LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 63 */
Variant c_board::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_board::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_board::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("cells", m_cells));
  props.push_back(NEW(ArrayElement)("cellsPieceWillFill", m_cellsPieceWillFill));
  props.push_back(NEW(ArrayElement)("cellCount", m_cellCount));
  props.push_back(NEW(ArrayElement)("cache", m_cache));
  props.push_back(NEW(ArrayElement)("noFit", m_noFit));
  c_ObjectData::o_get(props);
}
bool c_board::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x3E854EA2326AD070LL, cache, 5);
      break;
    case 1:
      HASH_EXISTS_STRING(0x3C77103CD69B4F81LL, noFit, 5);
      break;
    case 12:
      HASH_EXISTS_STRING(0x10BF483EAFD5963CLL, cellCount, 9);
      break;
    case 13:
      HASH_EXISTS_STRING(0x7E5172FABBC2C34DLL, cellsPieceWillFill, 18);
      break;
    case 15:
      HASH_EXISTS_STRING(0x366B587C08AB651FLL, cells, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_board::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x3E854EA2326AD070LL, m_cache,
                         cache, 5);
      break;
    case 1:
      HASH_RETURN_STRING(0x3C77103CD69B4F81LL, m_noFit,
                         noFit, 5);
      break;
    case 12:
      HASH_RETURN_STRING(0x10BF483EAFD5963CLL, m_cellCount,
                         cellCount, 9);
      break;
    case 13:
      HASH_RETURN_STRING(0x7E5172FABBC2C34DLL, m_cellsPieceWillFill,
                         cellsPieceWillFill, 18);
      break;
    case 15:
      HASH_RETURN_STRING(0x366B587C08AB651FLL, m_cells,
                         cells, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_board::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x3E854EA2326AD070LL, m_cache,
                      cache, 5);
      break;
    case 1:
      HASH_SET_STRING(0x3C77103CD69B4F81LL, m_noFit,
                      noFit, 5);
      break;
    case 12:
      HASH_SET_STRING(0x10BF483EAFD5963CLL, m_cellCount,
                      cellCount, 9);
      break;
    case 13:
      HASH_SET_STRING(0x7E5172FABBC2C34DLL, m_cellsPieceWillFill,
                      cellsPieceWillFill, 18);
      break;
    case 15:
      HASH_SET_STRING(0x366B587C08AB651FLL, m_cells,
                      cells, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_board::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_board::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(board)
ObjectData *c_board::create() {
  init();
  t_board();
  return this;
}
ObjectData *c_board::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_board::cloneImpl() {
  c_board *obj = NEW(c_board)();
  cloneSet(obj);
  return obj;
}
void c_board::cloneSet(c_board *clone) {
  clone->m_cells = m_cells;
  clone->m_cellsPieceWillFill = m_cellsPieceWillFill;
  clone->m_cellCount = m_cellCount;
  clone->m_cache = m_cache;
  clone->m_noFit = m_noFit;
  ObjectData::cloneSet(clone);
}
Variant c_board::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 5:
      HASH_GUARD(0x5AF4C743B0CDEA15LL, asstring) {
        return (t_asstring());
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x48D183238B33FBD7LL, firstemptycellindex) {
        return (t_firstemptycellindex());
      }
      break;
    case 11:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(params.rvalAt(0), params.rvalAt(1), ref(const_cast<Array&>(params).lvalAt(2))));
      }
      break;
    case 14:
      HASH_GUARD(0x508405C2FD14564ELL, remove) {
        return (t_remove(ref(const_cast<Array&>(params).lvalAt(0))), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_board::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 5:
      HASH_GUARD(0x5AF4C743B0CDEA15LL, asstring) {
        return (t_asstring());
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x48D183238B33FBD7LL, firstemptycellindex) {
        return (t_firstemptycellindex());
      }
      break;
    case 11:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(a0, a1, ref(a2)));
      }
      break;
    case 14:
      HASH_GUARD(0x508405C2FD14564ELL, remove) {
        return (t_remove(ref(a0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_board::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_board$os_get(const char *s) {
  return c_board::os_get(s, -1);
}
Variant &cw_board$os_lval(const char *s) {
  return c_board::os_lval(s, -1);
}
Variant cw_board$os_constant(const char *s) {
  return c_board::os_constant(s);
}
Variant cw_board$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_board::os_invoke(c, s, params, -1, fatal);
}
void c_board::init() {
  m_cells = null;
  m_cellsPieceWillFill = null;
  m_cellCount = 0LL;
  m_cache = null;
  m_noFit = ScalarArrays::sa_[0];
}
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 70 */
void c_board::t_board() {
  INSTANCE_METHOD_INJECTION(Board, Board::Board);
  bool oldInCtor = gasInCtor(true);
  int64 v_i = 0;
  double v_m = 0.0;
  double v_row = 0.0;
  bool v_isFirst = false;
  bool v_isLast = false;
  Variant v_c;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 50LL /* BOARD_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(1);
      m_cells.set(v_i, (((Object)(LINE(71,p_boardcell(p_boardcell(NEWOBJ(c_boardcell)())->create(v_i)))))));
    }
  }
  (v_m = LINE(72,x_floor(toDouble(10LL))) - 1LL);
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, 50LL /* BOARD_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_row = LINE(75,x_floor(toDouble(divide(v_i, 5LL /* BOARD_COLS */)))));
        (v_isFirst = equal(modulo(v_i, 5LL /* BOARD_COLS */), 0LL));
        (v_isLast = equal(modulo((v_i + 1LL), 5LL /* BOARD_COLS */), 0LL));
        (v_c = m_cells.rvalAt(v_i));
        if (equal(modulo(toInt64(v_row), 2LL), 1LL)) {
          if (!(v_isLast)) lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (m_cells.rvalAt(v_i - 4LL)), 0x5BCA7C69B794F8CELL);
          lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (m_cells.rvalAt(v_i - 5LL /* BOARD_COLS */)), 0x77CFA1EEF01BCA90LL);
          if (!equal(v_row, v_m)) {
            if (!(v_isLast)) lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (m_cells.rvalAt(v_i + 6LL)), 0x350AEB726A15D700LL);
            lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (m_cells.rvalAt(v_i + 5LL /* BOARD_COLS */)), 0x6F2A25235E544A31LL);
          }
        }
        else {
          if (!equal(v_row, 0LL)) {
            if (!(v_isFirst)) lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (m_cells.rvalAt(v_i - 6LL)), 0x77CFA1EEF01BCA90LL);
            lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (m_cells.rvalAt(v_i - 5LL /* BOARD_COLS */)), 0x5BCA7C69B794F8CELL);
          }
          if (!equal(v_row, v_m)) {
            if (!(v_isFirst)) lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (m_cells.rvalAt(v_i + 4LL)), 0x6F2A25235E544A31LL);
            lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (m_cells.rvalAt(v_i + 5LL /* BOARD_COLS */)), 0x350AEB726A15D700LL);
          }
        }
        if (!(v_isFirst)) lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (m_cells.rvalAt(v_i - 1LL)), 0x486AFCC090D5F98CLL);
        if (!(v_isLast)) lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (m_cells.rvalAt(v_i + 1LL)), 0x135FDDF6A6BFBBDDLL);
      }
    }
  }
  LINE(100,t_initializecache());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 103 */
void c_board::t_initializecache() {
  INSTANCE_METHOD_INJECTION(Board, Board::initializeCache);
  int64 v_i = 0;
  int64 v_j = 0;
  int64 v_k = 0;
  int64 v_m = 0;

  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, 10LL /* PIECES */); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        LOOP_COUNTER(4);
        for ((v_j = 0LL); less(v_j, 12LL /* PIECE_ORIENTATIONS */); v_j++) {
          LOOP_COUNTER_CHECK(4);
          {
            LOOP_COUNTER(5);
            for ((v_k = 0LL); less(v_k, 5LL /* PIECE_SIZE */); v_k++) {
              LOOP_COUNTER_CHECK(5);
              {
                LOOP_COUNTER(6);
                for ((v_m = 0LL); less(v_m, 50LL /* BOARD_SIZE */); v_m++) {
                  LOOP_COUNTER_CHECK(6);
                  lval(lval(lval(m_cache.lvalAt(v_i)).lvalAt(v_j)).lvalAt(v_k)).set(v_m, (null));
                }
              }
            }
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 111 */
void c_board::t_unmark() {
  INSTANCE_METHOD_INJECTION(Board, Board::unmark);
  int64 v_i = 0;

  {
    LOOP_COUNTER(7);
    for ((v_i = 0LL); less(v_i, 50LL /* BOARD_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(7);
      LINE(112,m_cells.rvalAt(v_i).o_invoke_few_args("unmark", 0x07C77A769C872B06LL, 0));
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 115 */
int64 c_board::t_firstemptycellindex() {
  INSTANCE_METHOD_INJECTION(Board, Board::firstEmptyCellIndex);
  int64 v_i = 0;

  {
    LOOP_COUNTER(8);
    for ((v_i = 0LL); less(v_i, 50LL /* BOARD_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(8);
      if (toBoolean(LINE(117,m_cells.rvalAt(v_i).o_invoke_few_args("isEmpty", 0x6359F42D5FC265E8LL, 0)))) return v_i;
    }
  }
  return -1LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 121 */
void c_board::t_remove(Variant v_p) {
  INSTANCE_METHOD_INJECTION(Board, Board::remove);
  int64 v_i = 0;

  {
    LOOP_COUNTER(9);
    for ((v_i = 0LL); less(v_i, 50LL /* BOARD_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(9);
      if (equal(m_cells.rvalAt(v_i).o_get("piece", 0x7132B63767EAFEDBLL), v_p)) (lval(m_cells.lvalAt(v_i)).o_lval("piece", 0x7132B63767EAFEDBLL) = null);
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 126 */
void c_board::t_find(Variant v_p, Variant v_c) {
  INSTANCE_METHOD_INJECTION(Board, Board::find);
  int64 v_i = 0;

  if ((!equal(v_p, null)) && ((!(toBoolean(v_p.o_get("marked", 0x4981D9427CB79E5CLL)))) && (!equal(v_c, null)))) {
    m_cellsPieceWillFill.set(m_cellCount, (v_c));
    m_cellCount++;
    LINE(130,v_p.o_invoke_few_args("mark", 0x54FABE90A93466DDLL, 0));
    {
      LOOP_COUNTER(10);
      for ((v_i = 0LL); less(v_i, 6LL /* SIDES */); v_i++) {
        LOOP_COUNTER_CHECK(10);
        {
          LINE(131,t_find(ref(lval(lval(v_p.o_lval("next", 0x2A31F6A650652D33LL)).lvalAt(v_i))), ref(lval(lval(v_c.o_lval("next", 0x2A31F6A650652D33LL)).lvalAt(v_i)))));
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 135 */
bool c_board::t_add(Variant v_pieceIndex, CVarRef v_boardIndex, Variant v_p) {
  INSTANCE_METHOD_INJECTION(Board, Board::add);
  Variant eo_0;
  Variant eo_1;
  Variant v_a;
  int64 v_i = 0;

  (v_a = m_cache.rvalAt(toObject(v_p).o_get("number", 0x2E2E8BE57817A743LL)).rvalAt(toObject(v_p).o_get("orientation", 0x3D4E9CED0EC9FFBALL)).rvalAt(v_pieceIndex).rvalAt(v_boardIndex));
  (m_cellCount = 0LL);
  LINE(138,toObject(v_p)->o_invoke_few_args("unmark", 0x07C77A769C872B06LL, 0));
  if (equal(v_a, null)) {
    (assignCallTemp(eo_0, ref(LINE(141,toObject(v_p)->o_invoke_few_args("cell", 0x47851B1F9C2ECB49LL, 1, v_pieceIndex)))),assignCallTemp(eo_1, ref(lval(m_cells.lvalAt(v_boardIndex)))),t_find(eo_0, eo_1));
    if (!equal(m_cellCount, 5LL /* PIECE_SIZE */)) {
      lval(lval(lval(m_cache.lvalAt(toObject(v_p).o_get("number", 0x2E2E8BE57817A743LL))).lvalAt(toObject(v_p).o_get("orientation", 0x3D4E9CED0EC9FFBALL))).lvalAt(v_pieceIndex)).set(v_boardIndex, (m_noFit));
      return false;
    }
    (v_a = LINE(147,x_array_slice(m_cellsPieceWillFill, toInt32(0LL))));
    lval(lval(lval(m_cache.lvalAt(toObject(v_p).o_get("number", 0x2E2E8BE57817A743LL))).lvalAt(toObject(v_p).o_get("orientation", 0x3D4E9CED0EC9FFBALL))).lvalAt(v_pieceIndex)).set(v_boardIndex, (v_a));
  }
  else {
    if (equal(v_a, m_noFit)) return false;
  }
  {
    LOOP_COUNTER(11);
    for ((v_i = 0LL); less(v_i, 5LL /* PIECE_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(11);
      {
        if (!(toBoolean(LINE(152,v_a.rvalAt(v_i).o_invoke_few_args("isEmpty", 0x6359F42D5FC265E8LL, 0))))) return false;
      }
    }
  }
  {
    LOOP_COUNTER(12);
    for ((v_i = 0LL); less(v_i, 5LL /* PIECE_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(12);
      {
        (lval(v_a.lvalAt(v_i)).o_lval("piece", 0x7132B63767EAFEDBLL) = v_p);
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 157 */
String c_board::t_asstring() {
  INSTANCE_METHOD_INJECTION(Board, Board::asString);
  Array v_a;
  int64 v_i = 0;

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(13);
    for ((v_i = 0LL); less(v_i, 50LL /* BOARD_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(13);
      v_a.append((m_cells.rvalAt(v_i).o_get("piece", 0x7132B63767EAFEDBLL).o_get("number", 0x2E2E8BE57817A743LL)));
    }
  }
  return LINE(160,x_implode("", v_a));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 16 */
Variant c_cell::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_cell::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_cell::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("marked", m_marked));
  props.push_back(NEW(ArrayElement)("next", m_next.isReferenced() ? ref(m_next) : m_next));
  c_ObjectData::o_get(props);
}
bool c_cell::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x4981D9427CB79E5CLL, marked, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x2A31F6A650652D33LL, next, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_cell::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x4981D9427CB79E5CLL, m_marked,
                         marked, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x2A31F6A650652D33LL, m_next,
                         next, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_cell::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x4981D9427CB79E5CLL, m_marked,
                      marked, 6);
      break;
    case 3:
      HASH_SET_STRING(0x2A31F6A650652D33LL, m_next,
                      next, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_cell::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x2A31F6A650652D33LL, m_next,
                         next, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_cell::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(cell)
ObjectData *c_cell::create() {
  init();
  t_cell();
  return this;
}
ObjectData *c_cell::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_cell::dynConstruct(CArrRef params) {
  (t_cell());
}
ObjectData *c_cell::cloneImpl() {
  c_cell *obj = NEW(c_cell)();
  cloneSet(obj);
  return obj;
}
void c_cell::cloneSet(c_cell *clone) {
  clone->m_marked = m_marked;
  clone->m_next = m_next.isReferenced() ? ref(m_next) : m_next;
  ObjectData::cloneSet(clone);
}
Variant c_cell::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x54FABE90A93466DDLL, mark) {
        return (t_mark(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_cell::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x54FABE90A93466DDLL, mark) {
        return (t_mark(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_cell::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_cell$os_get(const char *s) {
  return c_cell::os_get(s, -1);
}
Variant &cw_cell$os_lval(const char *s) {
  return c_cell::os_lval(s, -1);
}
Variant cw_cell$os_constant(const char *s) {
  return c_cell::os_constant(s);
}
Variant cw_cell$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_cell::os_invoke(c, s, params, -1, fatal);
}
void c_cell::init() {
  m_marked = false;
  m_next = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 19 */
void c_cell::t_cell() {
  INSTANCE_METHOD_INJECTION(Cell, Cell::Cell);
  bool oldInCtor = gasInCtor(true);
  int64 v_i = 0;

  {
    LOOP_COUNTER(14);
    for ((v_i = 0LL); less(v_i, 6LL /* SIDES */); v_i++) {
      LOOP_COUNTER_CHECK(14);
      m_next.set(v_i, (null));
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 20 */
void c_cell::t_mark() {
  INSTANCE_METHOD_INJECTION(Cell, Cell::mark);
  (m_marked = true);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 21 */
void c_cell::t_unmark() {
  INSTANCE_METHOD_INJECTION(Cell, Cell::unmark);
  (m_marked = false);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 180 */
Variant c_piececell::os_get(const char *s, int64 hash) {
  return c_cell::os_get(s, hash);
}
Variant &c_piececell::os_lval(const char *s, int64 hash) {
  return c_cell::os_lval(s, hash);
}
void c_piececell::o_get(ArrayElementVec &props) const {
  c_cell::o_get(props);
}
bool c_piececell::o_exists(CStrRef s, int64 hash) const {
  return c_cell::o_exists(s, hash);
}
Variant c_piececell::o_get(CStrRef s, int64 hash) {
  return c_cell::o_get(s, hash);
}
Variant c_piececell::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_cell::o_set(s, hash, v, forInit);
}
Variant &c_piececell::o_lval(CStrRef s, int64 hash) {
  return c_cell::o_lval(s, hash);
}
Variant c_piececell::os_constant(const char *s) {
  return c_cell::os_constant(s);
}
IMPLEMENT_CLASS(piececell)
ObjectData *c_piececell::create() {
  init();
  t_piececell();
  return this;
}
ObjectData *c_piececell::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_piececell::cloneImpl() {
  c_piececell *obj = NEW(c_piececell)();
  cloneSet(obj);
  return obj;
}
void c_piececell::cloneSet(c_piececell *clone) {
  c_cell::cloneSet(clone);
}
Variant c_piececell::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x14E227E59BDD997ALL, flip) {
        return (t_flip(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x6770944C6053865BLL, rotate) {
        return (t_rotate(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x54FABE90A93466DDLL, mark) {
        return (t_mark(), null);
      }
      break;
    default:
      break;
  }
  return c_cell::o_invoke(s, params, hash, fatal);
}
Variant c_piececell::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x14E227E59BDD997ALL, flip) {
        return (t_flip(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x6770944C6053865BLL, rotate) {
        return (t_rotate(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x54FABE90A93466DDLL, mark) {
        return (t_mark(), null);
      }
      break;
    default:
      break;
  }
  return c_cell::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_piececell::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_cell::os_invoke(c, s, params, hash, fatal);
}
Variant cw_piececell$os_get(const char *s) {
  return c_piececell::os_get(s, -1);
}
Variant &cw_piececell$os_lval(const char *s) {
  return c_piececell::os_lval(s, -1);
}
Variant cw_piececell$os_constant(const char *s) {
  return c_piececell::os_constant(s);
}
Variant cw_piececell$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_piececell::os_invoke(c, s, params, -1, fatal);
}
void c_piececell::init() {
  c_cell::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 181 */
void c_piececell::t_piececell() {
  INSTANCE_METHOD_INJECTION(PieceCell, PieceCell::PieceCell);
  bool oldInCtor = gasInCtor(true);
  LINE(181,c_cell::t_cell());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 183 */
void c_piececell::t_flip() {
  INSTANCE_METHOD_INJECTION(PieceCell, PieceCell::flip);
  Variant v_swap;

  (v_swap = m_next.rvalAt(1LL /* NE */, 0x5BCA7C69B794F8CELL));
  m_next.set(1LL /* NE */, (m_next.rvalAt(0LL /* NW */, 0x77CFA1EEF01BCA90LL)), 0x5BCA7C69B794F8CELL);
  m_next.set(0LL /* NW */, (v_swap), 0x77CFA1EEF01BCA90LL);
  (v_swap = m_next.rvalAt(3LL /* E */, 0x135FDDF6A6BFBBDDLL));
  m_next.set(3LL /* E */, (m_next.rvalAt(2LL /* W */, 0x486AFCC090D5F98CLL)), 0x135FDDF6A6BFBBDDLL);
  m_next.set(2LL /* W */, (v_swap), 0x486AFCC090D5F98CLL);
  (v_swap = m_next.rvalAt(5LL /* SE */, 0x350AEB726A15D700LL));
  m_next.set(5LL /* SE */, (m_next.rvalAt(4LL /* SW */, 0x6F2A25235E544A31LL)), 0x350AEB726A15D700LL);
  m_next.set(4LL /* SW */, (v_swap), 0x6F2A25235E544A31LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 197 */
void c_piececell::t_rotate() {
  INSTANCE_METHOD_INJECTION(PieceCell, PieceCell::rotate);
  Variant v_swap;

  (v_swap = m_next.rvalAt(3LL /* E */, 0x135FDDF6A6BFBBDDLL));
  m_next.set(3LL /* E */, (m_next.rvalAt(1LL /* NE */, 0x5BCA7C69B794F8CELL)), 0x135FDDF6A6BFBBDDLL);
  m_next.set(1LL /* NE */, (m_next.rvalAt(0LL /* NW */, 0x77CFA1EEF01BCA90LL)), 0x5BCA7C69B794F8CELL);
  m_next.set(0LL /* NW */, (m_next.rvalAt(2LL /* W */, 0x486AFCC090D5F98CLL)), 0x77CFA1EEF01BCA90LL);
  m_next.set(2LL /* W */, (m_next.rvalAt(4LL /* SW */, 0x6F2A25235E544A31LL)), 0x486AFCC090D5F98CLL);
  m_next.set(4LL /* SW */, (m_next.rvalAt(5LL /* SE */, 0x350AEB726A15D700LL)), 0x6F2A25235E544A31LL);
  m_next.set(5LL /* SE */, (v_swap), 0x350AEB726A15D700LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 24 */
Variant c_boardcell::os_get(const char *s, int64 hash) {
  return c_cell::os_get(s, hash);
}
Variant &c_boardcell::os_lval(const char *s, int64 hash) {
  return c_cell::os_lval(s, hash);
}
void c_boardcell::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("number", m_number.isReferenced() ? ref(m_number) : m_number));
  props.push_back(NEW(ArrayElement)("piece", m_piece.isReferenced() ? ref(m_piece) : m_piece));
  c_cell::o_get(props);
}
bool c_boardcell::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_EXISTS_STRING(0x2E2E8BE57817A743LL, number, 6);
      HASH_EXISTS_STRING(0x7132B63767EAFEDBLL, piece, 5);
      break;
    default:
      break;
  }
  return c_cell::o_exists(s, hash);
}
Variant c_boardcell::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_RETURN_STRING(0x2E2E8BE57817A743LL, m_number,
                         number, 6);
      HASH_RETURN_STRING(0x7132B63767EAFEDBLL, m_piece,
                         piece, 5);
      break;
    default:
      break;
  }
  return c_cell::o_get(s, hash);
}
Variant c_boardcell::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_SET_STRING(0x2E2E8BE57817A743LL, m_number,
                      number, 6);
      HASH_SET_STRING(0x7132B63767EAFEDBLL, m_piece,
                      piece, 5);
      break;
    default:
      break;
  }
  return c_cell::o_set(s, hash, v, forInit);
}
Variant &c_boardcell::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_RETURN_STRING(0x2E2E8BE57817A743LL, m_number,
                         number, 6);
      HASH_RETURN_STRING(0x7132B63767EAFEDBLL, m_piece,
                         piece, 5);
      break;
    default:
      break;
  }
  return c_cell::o_lval(s, hash);
}
Variant c_boardcell::os_constant(const char *s) {
  return c_cell::os_constant(s);
}
IMPLEMENT_CLASS(boardcell)
ObjectData *c_boardcell::create(int64 v_i) {
  init();
  t_boardcell(v_i);
  return this;
}
ObjectData *c_boardcell::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_boardcell::cloneImpl() {
  c_boardcell *obj = NEW(c_boardcell)();
  cloneSet(obj);
  return obj;
}
void c_boardcell::cloneSet(c_boardcell *clone) {
  clone->m_number = m_number.isReferenced() ? ref(m_number) : m_number;
  clone->m_piece = m_piece.isReferenced() ? ref(m_piece) : m_piece;
  c_cell::cloneSet(clone);
}
Variant c_boardcell::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 4:
      HASH_GUARD(0x676025F852E098B4LL, contiguousemptycells) {
        return (t_contiguousemptycells());
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 9:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x54FABE90A93466DDLL, mark) {
        return (t_mark(), null);
      }
      break;
    default:
      break;
  }
  return c_cell::o_invoke(s, params, hash, fatal);
}
Variant c_boardcell::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 4:
      HASH_GUARD(0x676025F852E098B4LL, contiguousemptycells) {
        return (t_contiguousemptycells());
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 9:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x54FABE90A93466DDLL, mark) {
        return (t_mark(), null);
      }
      break;
    default:
      break;
  }
  return c_cell::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_boardcell::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_cell::os_invoke(c, s, params, hash, fatal);
}
Variant cw_boardcell$os_get(const char *s) {
  return c_boardcell::os_get(s, -1);
}
Variant &cw_boardcell$os_lval(const char *s) {
  return c_boardcell::os_lval(s, -1);
}
Variant cw_boardcell$os_constant(const char *s) {
  return c_boardcell::os_constant(s);
}
Variant cw_boardcell$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_boardcell::os_invoke(c, s, params, -1, fatal);
}
void c_boardcell::init() {
  c_cell::init();
  m_number = null;
  m_piece = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 28 */
void c_boardcell::t_boardcell(int64 v_i) {
  INSTANCE_METHOD_INJECTION(BoardCell, BoardCell::BoardCell);
  bool oldInCtor = gasInCtor(true);
  LINE(29,c_cell::t_cell());
  (m_number = v_i);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 33 */
bool c_boardcell::t_isempty() {
  INSTANCE_METHOD_INJECTION(BoardCell, BoardCell::isEmpty);
  return equal(m_piece, null);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 34 */
void c_boardcell::t_setempty() {
  INSTANCE_METHOD_INJECTION(BoardCell, BoardCell::setEmpty);
  (m_piece = null);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 36 */
Variant c_boardcell::t_contiguousemptycells() {
  INSTANCE_METHOD_INJECTION(BoardCell, BoardCell::contiguousEmptyCells);
  Variant v_count;
  int64 v_i = 0;
  Variant v_neighbour;

  if ((!(m_marked)) && (LINE(37,t_isempty()))) {
    LINE(38,t_mark());
    (v_count = 1LL);
    {
      LOOP_COUNTER(15);
      for ((v_i = 0LL); less(v_i, 6LL /* SIDES */); v_i++) {
        LOOP_COUNTER_CHECK(15);
        {
          (v_neighbour = m_next.rvalAt(v_i));
          if (!equal(v_neighbour, null) && toBoolean(LINE(42,v_neighbour.o_invoke_few_args("isEmpty", 0x6359F42D5FC265E8LL, 0)))) v_count += LINE(43,v_neighbour.o_invoke_few_args("contiguousEmptyCells", 0x676025F852E098B4LL, 0));
        }
      }
    }
  }
  else {
    (v_count = 0LL);
  }
  return v_count;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 209 */
Variant c_piece::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_piece::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_piece::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("number", m_number.isReferenced() ? ref(m_number) : m_number));
  props.push_back(NEW(ArrayElement)("orientation", m_orientation));
  props.push_back(NEW(ArrayElement)("cache", m_cache));
  c_ObjectData::o_get(props);
}
bool c_piece::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x3E854EA2326AD070LL, cache, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x3D4E9CED0EC9FFBALL, orientation, 11);
      break;
    case 3:
      HASH_EXISTS_STRING(0x2E2E8BE57817A743LL, number, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_piece::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x3E854EA2326AD070LL, m_cache,
                         cache, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x3D4E9CED0EC9FFBALL, m_orientation,
                         orientation, 11);
      break;
    case 3:
      HASH_RETURN_STRING(0x2E2E8BE57817A743LL, m_number,
                         number, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_piece::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x3E854EA2326AD070LL, m_cache,
                      cache, 5);
      break;
    case 2:
      HASH_SET_STRING(0x3D4E9CED0EC9FFBALL, m_orientation,
                      orientation, 11);
      break;
    case 3:
      HASH_SET_STRING(0x2E2E8BE57817A743LL, m_number,
                      number, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_piece::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x2E2E8BE57817A743LL, m_number,
                         number, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_piece::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(piece)
ObjectData *c_piece::create(int64 v_n) {
  init();
  t_piece(v_n);
  return this;
}
ObjectData *c_piece::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_piece::cloneImpl() {
  c_piece *obj = NEW(c_piece)();
  cloneSet(obj);
  return obj;
}
void c_piece::cloneSet(c_piece *clone) {
  clone->m_number = m_number.isReferenced() ? ref(m_number) : m_number;
  clone->m_orientation = m_orientation;
  clone->m_cache = m_cache;
  ObjectData::cloneSet(clone);
}
Variant c_piece::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(params.rvalAt(0)));
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x2E6EFB9B031EA3E7LL, nextorientation) {
        return (t_nextorientation());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_piece::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x47851B1F9C2ECB49LL, cell) {
        return (t_cell(a0));
      }
      break;
    case 6:
      HASH_GUARD(0x07C77A769C872B06LL, unmark) {
        return (t_unmark(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x2E6EFB9B031EA3E7LL, nextorientation) {
        return (t_nextorientation());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_piece::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_piece$os_get(const char *s) {
  return c_piece::os_get(s, -1);
}
Variant &cw_piece$os_lval(const char *s) {
  return c_piece::os_lval(s, -1);
}
Variant cw_piece$os_constant(const char *s) {
  return c_piece::os_constant(s);
}
Variant cw_piece$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_piece::os_invoke(c, s, params, -1, fatal);
}
void c_piece::init() {
  m_number = null;
  m_orientation = 0LL;
  m_cache = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 214 */
void c_piece::t_piece(int64 v_n) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::Piece);
  bool oldInCtor = gasInCtor(true);
  int64 v_k = 0;
  int64 v_i = 0;
  int64 v_j = 0;

  (m_number = v_n);
  {
    LOOP_COUNTER(16);
    for ((v_k = 0LL); less(v_k, 12LL /* PIECE_ORIENTATIONS */); v_k++) {
      LOOP_COUNTER_CHECK(16);
      {
        {
          LOOP_COUNTER(17);
          for ((v_i = 0LL); less(v_i, 5LL /* PIECE_SIZE */); v_i++) {
            LOOP_COUNTER_CHECK(17);
            lval(m_cache.lvalAt(v_k)).set(v_i, (((Object)(LINE(219,p_piececell(p_piececell(NEWOBJ(c_piececell)())->create()))))));
          }
        }
        {
          switch (v_n) {
          case 0LL:
            {
              LINE(222,t_make0(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 1LL:
            {
              LINE(223,t_make1(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 2LL:
            {
              LINE(224,t_make2(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 3LL:
            {
              LINE(225,t_make3(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 4LL:
            {
              LINE(226,t_make4(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 5LL:
            {
              LINE(227,t_make5(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 6LL:
            {
              LINE(228,t_make6(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 7LL:
            {
              LINE(229,t_make7(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 8LL:
            {
              LINE(230,t_make8(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          case 9LL:
            {
              LINE(231,t_make9(ref(lval(m_cache.lvalAt(v_k)))));
              break;
            }
          }
        }
        {
          LOOP_COUNTER(19);
          for ((v_i = 0LL); less(v_i, v_k); v_i++) {
            LOOP_COUNTER_CHECK(19);
            if (equal(modulo(v_i, 6LL /* PIECE_ROTATIONS */), 0LL)) {
              LOOP_COUNTER(20);
              for ((v_j = 0LL); less(v_j, 5LL /* PIECE_SIZE */); v_j++) {
                LOOP_COUNTER_CHECK(20);
                LINE(236,m_cache.rvalAt(v_k).rvalAt(v_j).o_invoke_few_args("flip", 0x14E227E59BDD997ALL, 0));
              }
            }
            else {
              LOOP_COUNTER(21);
              for ((v_j = 0LL); less(v_j, 5LL /* PIECE_SIZE */); v_j++) {
                LOOP_COUNTER_CHECK(21);
                LINE(238,m_cache.rvalAt(v_k).rvalAt(v_j).o_invoke_few_args("rotate", 0x6770944C6053865BLL, 0));
              }
            }
          }
        }
      }
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 242 */
void c_piece::t_unmark() {
  INSTANCE_METHOD_INJECTION(Piece, Piece::unmark);
  int64 v_i = 0;

  {
    LOOP_COUNTER(22);
    for ((v_i = 0LL); less(v_i, 5LL /* PIECE_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(22);
      LINE(243,m_cache.rvalAt(m_orientation).rvalAt(v_i).o_invoke_few_args("unmark", 0x07C77A769C872B06LL, 0));
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 246 */
p_piece c_piece::t_nextorientation() {
  INSTANCE_METHOD_INJECTION(Piece, Piece::nextOrientation);
  (m_orientation = modulo((toInt64(m_orientation + 1LL)), 12LL /* PIECE_ORIENTATIONS */));
  return ((Object)(this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 251 */
Variant c_piece::t_cell(CVarRef v_i) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::cell);
  return m_cache.rvalAt(m_orientation).rvalAt(v_i);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 253 */
void c_piece::t_make0(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make0);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 264 */
void c_piece::t_make1(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make1);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 275 */
void c_piece::t_make2(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make2);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 286 */
void c_piece::t_make3(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make3);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 299 */
void c_piece::t_make4(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make4);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 310 */
void c_piece::t_make5(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make5);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 323 */
void c_piece::t_make6(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make6);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 334 */
void c_piece::t_make7(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make7);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x350AEB726A15D700LL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 345 */
void c_piece::t_make8(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make8);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x486AFCC090D5F98CLL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 356 */
void c_piece::t_make9(Variant v_a) {
  INSTANCE_METHOD_INJECTION(Piece, Piece::make9);
  lval(lval(v_a.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(1LL /* NE */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(4LL /* SW */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x6F2A25235E544A31LL);
  lval(lval(v_a.lvalAt(2LL, 0x486AFCC090D5F98CLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(3LL /* E */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x135FDDF6A6BFBBDDLL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(2LL /* W */, (v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), 0x486AFCC090D5F98CLL);
  lval(lval(v_a.lvalAt(4LL, 0x6F2A25235E544A31LL)).o_lval("next", 0x2A31F6A650652D33LL)).set(0LL /* NW */, (v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), 0x77CFA1EEF01BCA90LL);
  lval(lval(v_a.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).o_lval("next", 0x2A31F6A650652D33LL)).set(5LL /* SE */, (v_a.rvalAt(4LL, 0x6F2A25235E544A31LL)), 0x350AEB726A15D700LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 392 */
Variant c_solver::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_solver::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_solver::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("countdown", m_countdown.isReferenced() ? ref(m_countdown) : m_countdown));
  props.push_back(NEW(ArrayElement)("n", m_n.isReferenced() ? ref(m_n) : m_n));
  props.push_back(NEW(ArrayElement)("board", m_board.isReferenced() ? ref(m_board) : m_board));
  props.push_back(NEW(ArrayElement)("pieces", m_pieces));
  props.push_back(NEW(ArrayElement)("unplaced", m_unplaced));
  props.push_back(NEW(ArrayElement)("unplacedSize", m_unplacedSize.isReferenced() ? ref(m_unplacedSize) : m_unplacedSize));
  props.push_back(NEW(ArrayElement)("once", m_once.isReferenced() ? ref(m_once) : m_once));
  props.push_back(NEW(ArrayElement)("first", m_first.isReferenced() ? ref(m_first) : m_first));
  props.push_back(NEW(ArrayElement)("last", m_last.isReferenced() ? ref(m_last) : m_last));
  props.push_back(NEW(ArrayElement)("current", m_current));
  c_ObjectData::o_get(props);
}
bool c_solver::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 3:
      HASH_EXISTS_STRING(0x0B7C9781761669E3LL, unplaced, 8);
      break;
    case 7:
      HASH_EXISTS_STRING(0x0D4563C1C6B015C7LL, board, 5);
      HASH_EXISTS_STRING(0x1D12A485470A29A7LL, first, 5);
      break;
    case 9:
      HASH_EXISTS_STRING(0x117B8667E4662809LL, n, 1);
      break;
    case 11:
      HASH_EXISTS_STRING(0x1830D997342B416BLL, pieces, 6);
      break;
    case 13:
      HASH_EXISTS_STRING(0x3AE90F825256B40DLL, once, 4);
      break;
    case 19:
      HASH_EXISTS_STRING(0x57A2E567F9D4B4D3LL, unplacedSize, 12);
      HASH_EXISTS_STRING(0x235AFB77B3A5C1B3LL, current, 7);
      break;
    case 25:
      HASH_EXISTS_STRING(0x44C1BDE66B011FB9LL, countdown, 9);
      break;
    case 28:
      HASH_EXISTS_STRING(0x760894CA6D41E4DCLL, last, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_solver::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 3:
      HASH_RETURN_STRING(0x0B7C9781761669E3LL, m_unplaced,
                         unplaced, 8);
      break;
    case 7:
      HASH_RETURN_STRING(0x0D4563C1C6B015C7LL, m_board,
                         board, 5);
      HASH_RETURN_STRING(0x1D12A485470A29A7LL, m_first,
                         first, 5);
      break;
    case 9:
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x1830D997342B416BLL, m_pieces,
                         pieces, 6);
      break;
    case 13:
      HASH_RETURN_STRING(0x3AE90F825256B40DLL, m_once,
                         once, 4);
      break;
    case 19:
      HASH_RETURN_STRING(0x57A2E567F9D4B4D3LL, m_unplacedSize,
                         unplacedSize, 12);
      HASH_RETURN_STRING(0x235AFB77B3A5C1B3LL, m_current,
                         current, 7);
      break;
    case 25:
      HASH_RETURN_STRING(0x44C1BDE66B011FB9LL, m_countdown,
                         countdown, 9);
      break;
    case 28:
      HASH_RETURN_STRING(0x760894CA6D41E4DCLL, m_last,
                         last, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_solver::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 3:
      HASH_SET_STRING(0x0B7C9781761669E3LL, m_unplaced,
                      unplaced, 8);
      break;
    case 7:
      HASH_SET_STRING(0x0D4563C1C6B015C7LL, m_board,
                      board, 5);
      HASH_SET_STRING(0x1D12A485470A29A7LL, m_first,
                      first, 5);
      break;
    case 9:
      HASH_SET_STRING(0x117B8667E4662809LL, m_n,
                      n, 1);
      break;
    case 11:
      HASH_SET_STRING(0x1830D997342B416BLL, m_pieces,
                      pieces, 6);
      break;
    case 13:
      HASH_SET_STRING(0x3AE90F825256B40DLL, m_once,
                      once, 4);
      break;
    case 19:
      HASH_SET_STRING(0x57A2E567F9D4B4D3LL, m_unplacedSize,
                      unplacedSize, 12);
      HASH_SET_STRING(0x235AFB77B3A5C1B3LL, m_current,
                      current, 7);
      break;
    case 25:
      HASH_SET_STRING(0x44C1BDE66B011FB9LL, m_countdown,
                      countdown, 9);
      break;
    case 28:
      HASH_SET_STRING(0x760894CA6D41E4DCLL, m_last,
                      last, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_solver::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_RETURN_STRING(0x57A2E567F9D4B4D3LL, m_unplacedSize,
                         unplacedSize, 12);
      break;
    case 7:
      HASH_RETURN_STRING(0x0D4563C1C6B015C7LL, m_board,
                         board, 5);
      HASH_RETURN_STRING(0x1D12A485470A29A7LL, m_first,
                         first, 5);
      break;
    case 9:
      HASH_RETURN_STRING(0x44C1BDE66B011FB9LL, m_countdown,
                         countdown, 9);
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    case 12:
      HASH_RETURN_STRING(0x760894CA6D41E4DCLL, m_last,
                         last, 4);
      break;
    case 13:
      HASH_RETURN_STRING(0x3AE90F825256B40DLL, m_once,
                         once, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_solver::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(solver)
ObjectData *c_solver::create(CVarRef v_arg) {
  init();
  t_solver(v_arg);
  return this;
}
ObjectData *c_solver::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_solver::cloneImpl() {
  c_solver *obj = NEW(c_solver)();
  cloneSet(obj);
  return obj;
}
void c_solver::cloneSet(c_solver *clone) {
  clone->m_countdown = m_countdown.isReferenced() ? ref(m_countdown) : m_countdown;
  clone->m_n = m_n.isReferenced() ? ref(m_n) : m_n;
  clone->m_board = m_board.isReferenced() ? ref(m_board) : m_board;
  clone->m_pieces = m_pieces;
  clone->m_unplaced = m_unplaced;
  clone->m_unplacedSize = m_unplacedSize.isReferenced() ? ref(m_unplacedSize) : m_unplacedSize;
  clone->m_once = m_once.isReferenced() ? ref(m_once) : m_once;
  clone->m_first = m_first.isReferenced() ? ref(m_first) : m_first;
  clone->m_last = m_last.isReferenced() ? ref(m_last) : m_last;
  clone->m_current = m_current;
  ObjectData::cloneSet(clone);
}
Variant c_solver::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x0132CC3C61C08334LL, printsolutions) {
        return (t_printsolutions(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x0893B665953644B5LL, findsolutions) {
        return (t_findsolutions(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_solver::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x0132CC3C61C08334LL, printsolutions) {
        return (t_printsolutions(), null);
      }
      break;
    case 1:
      HASH_GUARD(0x0893B665953644B5LL, findsolutions) {
        return (t_findsolutions(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_solver::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_solver$os_get(const char *s) {
  return c_solver::os_get(s, -1);
}
Variant &cw_solver$os_lval(const char *s) {
  return c_solver::os_lval(s, -1);
}
Variant cw_solver$os_constant(const char *s) {
  return c_solver::os_constant(s);
}
Variant cw_solver$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_solver::os_invoke(c, s, params, -1, fatal);
}
void c_solver::init() {
  m_countdown = null;
  m_n = null;
  m_board = null;
  m_pieces = null;
  m_unplaced = null;
  m_unplacedSize = null;
  m_once = null;
  m_first = null;
  m_last = null;
  m_current = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 396 */
void c_solver::t_solver(CVarRef v_arg) {
  INSTANCE_METHOD_INJECTION(Solver, Solver::Solver);
  bool oldInCtor = gasInCtor(true);
  int64 v_i = 0;

  (m_n = v_arg);
  (m_countdown = m_n);
  (m_board = ((Object)(LINE(399,p_board(p_board(NEWOBJ(c_board)())->create())))));
  {
    LOOP_COUNTER(23);
    for ((v_i = 0LL); less(v_i, 10LL /* PIECES */); v_i++) {
      LOOP_COUNTER_CHECK(23);
      {
        m_pieces.set(v_i, (((Object)(LINE(402,p_piece(p_piece(NEWOBJ(c_piece)())->create(v_i)))))));
        m_unplaced.set(v_i, (true));
      }
    }
  }
  (m_unplacedSize = 10LL /* PIECES */);
  (m_once = true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 410 */
bool c_solver::t_shouldprune() {
  INSTANCE_METHOD_INJECTION(Solver, Solver::shouldPrune);
  int64 v_i = 0;
  bool v_forall = false;

  LINE(411,m_board.o_invoke_few_args("unmark", 0x07C77A769C872B06LL, 0));
  {
    LOOP_COUNTER(24);
    for ((v_i = 0LL); less(v_i, 50LL /* BOARD_SIZE */); v_i++) {
      LOOP_COUNTER_CHECK(24);
      {
        (v_forall = equal((modulo(toInt64(LINE(413,m_board.o_get("cells", 0x366B587C08AB651FLL).rvalAt(v_i).o_invoke_few_args("contiguousEmptyCells", 0x676025F852E098B4LL, 0))), 5LL /* PIECE_SIZE */)), 0LL));
        if (!(v_forall)) return !(v_forall);
      }
    }
  }
  return !(v_forall);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 419 */
void c_solver::t_puzzlesolved() {
  INSTANCE_METHOD_INJECTION(Solver, Solver::puzzleSolved);
  Variant v_a;

  (v_a = LINE(420,m_board.o_invoke_few_args("asString", 0x5AF4C743B0CDEA15LL, 0)));
  if (equal(m_first, null)) {
    (m_first = v_a);
    (m_last = v_a);
  }
  else {
    if (less(v_a, m_first)) {
      (m_first = v_a);
    }
    else {
      if (more(v_a, m_last)) {
        (m_last = v_a);
      }
    }
  }
  m_countdown--;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 434 */
void c_solver::t_findsolutions() {
  INSTANCE_METHOD_INJECTION(Solver, Solver::findSolutions);
  Variant v_emptyCellIndex;
  int64 v_k = 0;
  int64 v_i = 0;
  Variant v_piece;
  Variant v_j;

  if (more(m_countdown, 0LL)) {
    if (more(m_unplacedSize, 0LL)) {
      (v_emptyCellIndex = LINE(437,m_board.o_invoke_few_args("firstEmptyCellIndex", 0x48D183238B33FBD7LL, 0)));
      {
        LOOP_COUNTER(25);
        for ((v_k = 0LL); less(v_k, 10LL /* PIECES */); v_k++) {
          LOOP_COUNTER_CHECK(25);
          {
            if (toBoolean(m_unplaced.rvalAt(v_k))) {
              m_unplaced.set(v_k, (false));
              m_unplacedSize--;
              {
                LOOP_COUNTER(26);
                for ((v_i = 0LL); less(v_i, 12LL /* PIECE_ORIENTATIONS */); v_i++) {
                  LOOP_COUNTER_CHECK(26);
                  {
                    (v_piece = LINE(444,m_pieces.rvalAt(v_k).o_invoke_few_args("nextOrientation", 0x2E6EFB9B031EA3E7LL, 0)));
                    {
                      LOOP_COUNTER(27);
                      for ((v_j = 0LL); less(v_j, 5LL /* PIECE_SIZE */); v_j++) {
                        LOOP_COUNTER_CHECK(27);
                        {
                          if (toBoolean(LINE(446,m_board.o_invoke_few_args("add", 0x15D34462FC79458BLL, 3, v_j, v_emptyCellIndex, v_piece)))) {
                            if (!(LINE(447,t_shouldprune()))) t_findsolutions();
                            LINE(448,m_board.o_invoke_few_args("remove", 0x508405C2FD14564ELL, 1, v_piece));
                          }
                        }
                      }
                    }
                  }
                }
              }
              m_unplaced.set(v_k, (true));
              m_unplacedSize++;
            }
          }
        }
      }
    }
    else {
      LINE(456,t_puzzlesolved());
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 461 */
void c_solver::t_printboard(Variant v_s) {
  INSTANCE_METHOD_INJECTION(Solver, Solver::printBoard);
  bool v_indent = false;
  int64 v_i = 0;
  int64 v_j = 0;

  (v_indent = false);
  (v_i = 0LL);
  LOOP_COUNTER(28);
  {
    while (less(v_i, LINE(464,x_strlen(toString(v_s))))) {
      LOOP_COUNTER_CHECK(28);
      {
        if (v_indent) print(" ");
        (v_j = 0LL);
        LOOP_COUNTER(29);
        {
          while (less(v_j, 5LL /* BOARD_COLS */)) {
            LOOP_COUNTER_CHECK(29);
            {
              print((toString(v_s.rvalAt(v_i))));
              print(" ");
              v_j++;
              v_i++;
            }
          }
        }
        print("\n");
        (v_indent = !(v_indent));
      }
    }
  }
  print("\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/meteor.php line 478 */
void c_solver::t_printsolutions() {
  INSTANCE_METHOD_INJECTION(Solver, Solver::printSolutions);
  LINE(479,x_printf(2, "%d solutions found\n\n", Array(ArrayInit(1).set(0, m_n).create())));
  LINE(480,t_printboard(ref(lval(m_first))));
  LINE(481,t_printboard(ref(lval(m_last))));
} /* function */
Object co_board(CArrRef params, bool init /* = true */) {
  return Object(p_board(NEW(c_board)())->dynCreate(params, init));
}
Object co_cell(CArrRef params, bool init /* = true */) {
  return Object(p_cell(NEW(c_cell)())->dynCreate(params, init));
}
Object co_piececell(CArrRef params, bool init /* = true */) {
  return Object(p_piececell(NEW(c_piececell)())->dynCreate(params, init));
}
Object co_boardcell(CArrRef params, bool init /* = true */) {
  return Object(p_boardcell(NEW(c_boardcell)())->dynCreate(params, init));
}
Object co_piece(CArrRef params, bool init /* = true */) {
  return Object(p_piece(NEW(c_piece)())->dynCreate(params, init));
}
Object co_solver(CArrRef params, bool init /* = true */) {
  return Object(p_solver(NEW(c_solver)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$meteor_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/meteor.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$meteor_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);

  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  (v_s = ((Object)(LINE(486,p_solver(p_solver(NEWOBJ(c_solver)())->create(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))))));
  LINE(487,v_s.o_invoke_few_args("findSolutions", 0x0893B665953644B5LL, 0));
  LINE(488,v_s.o_invoke_few_args("printSolutions", 0x0132CC3C61C08334LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
