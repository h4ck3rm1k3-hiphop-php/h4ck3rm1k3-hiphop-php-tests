
#include <php/phc-test/subjects/benchmarks/shootout/harmonic.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$harmonic_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/harmonic.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$harmonic_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_partialSum __attribute__((__unused__)) = (variables != gVariables) ? variables->get("partialSum") : g->GV(partialSum);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(600000LL)));
  (v_partialSum = 0.0);
  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); not_more(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      v_partialSum += divide(1.0, v_i);
    }
  }
  LINE(16,x_printf(2, "%.9f\n", Array(ArrayInit(1).set(0, v_partialSum).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
