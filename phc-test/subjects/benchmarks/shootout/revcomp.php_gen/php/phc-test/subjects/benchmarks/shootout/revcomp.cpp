
#include <php/phc-test/subjects/benchmarks/shootout/revcomp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_LINE_LENGTH = 60LL;
const StaticString k_SRC = "CGATMKRYVBHD";
const StaticString k_DST = "GCTAKMYRBVDH";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/revcomp.php line 41 */
void f_print_seq() {
  FUNCTION_INJECTION(print_seq);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_seq __attribute__((__unused__)) = g->GV(seq);
  if (!equal(gv_seq, "")) {
    {
      echo(LINE(45,(assignCallTemp(eo_0, LINE(44,x_strrev((assignCallTemp(eo_4, x_strtoupper(toString(gv_seq))),x_strtr(eo_4, "CGATMKRYVBHD" /* SRC */, "GCTAKMYRBVDH" /* DST */))))),x_wordwrap(eo_0, toInt32(60LL) /* LINE_LENGTH */, "\n", true))));
      echo("\n");
    }
  }
  (gv_seq = "");
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$revcomp_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/revcomp.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$revcomp_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_seq __attribute__((__unused__)) = (variables != gVariables) ? variables->get("seq") : g->GV(seq);

  ;
  ;
  ;
  (v_str = "");
  (v_seq = "");
  LOOP_COUNTER(1);
  {
    while (!(LINE(25,x_feof(toObject(STDIN))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_str = LINE(26,x_trim(toString(x_fgets(toObject(STDIN))))));
        if (equal(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), ">")) {
          LINE(29,f_print_seq());
          {
            echo(toString(v_str));
            echo("\n");
          }
        }
        else {
          concat_assign(v_seq, toString(v_str));
        }
      }
    }
  }
  LINE(36,f_print_seq());
  f_exit();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
