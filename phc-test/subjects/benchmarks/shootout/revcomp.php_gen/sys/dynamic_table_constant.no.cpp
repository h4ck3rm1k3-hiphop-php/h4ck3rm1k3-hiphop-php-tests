
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_LINE_LENGTH;
extern const StaticString k_SRC;
extern const StaticString k_DST;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x07CD20D829F2A1F0LL, k_DST, DST);
      break;
    case 1:
      HASH_RETURN(0x229BA113FB1D0F11LL, k_SRC, SRC);
      break;
    case 5:
      HASH_RETURN(0x5B80CC257716F3D5LL, k_LINE_LENGTH, LINE_LENGTH);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
