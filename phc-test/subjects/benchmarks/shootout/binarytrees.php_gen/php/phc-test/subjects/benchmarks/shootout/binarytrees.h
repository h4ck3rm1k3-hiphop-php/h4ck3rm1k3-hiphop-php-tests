
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_binarytrees_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_binarytrees_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/binarytrees.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_itemcheck(Variant v_treeNode);
Array f_newtreenode(Variant v_left, Variant v_right, CVarRef v_item);
Variant pm_php$phc_test$subjects$benchmarks$shootout$binarytrees_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_bottomuptree(CVarRef v_item, CVarRef v_depth);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_binarytrees_h__
