
#include <php/phc-test/subjects/benchmarks/shootout/binarytrees.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/binarytrees.php line 16 */
Variant f_itemcheck(Variant v_treeNode) {
  FUNCTION_INJECTION(itemCheck);
  if (!(isset(v_treeNode, 0LL, 0x77CFA1EEF01BCA90LL))) {
    return v_treeNode.rvalAt(2LL, 0x486AFCC090D5F98CLL);
  }
  else {
    return minus_rev(LINE(20,f_itemcheck(ref(lval(v_treeNode.lvalAt(1LL, 0x5BCA7C69B794F8CELL))))), v_treeNode.rvalAt(2LL, 0x486AFCC090D5F98CLL) + f_itemcheck(ref(lval(v_treeNode.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/binarytrees.php line 12 */
Array f_newtreenode(Variant v_left, Variant v_right, CVarRef v_item) {
  FUNCTION_INJECTION(newTreeNode);
  return Array(ArrayInit(3).set(0, v_left).set(1, v_right).set(2, v_item).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/binarytrees.php line 24 */
Variant f_bottomuptree(CVarRef v_item, CVarRef v_depth) {
  FUNCTION_INJECTION(bottomUpTree);
  Variant v_left;
  Variant v_right;
  Variant v_treeNode;

  if (more(v_depth, 0LL)) {
    (v_left = LINE(26,f_bottomuptree(2LL * v_item - 1LL, v_depth - 1LL)));
    (v_right = LINE(27,f_bottomuptree(2LL * v_item, v_depth - 1LL)));
    return LINE(28,f_newtreenode(ref(v_left), ref(v_right), v_item));
  }
  else {
    setNull(v_treeNode);
    return LINE(32,f_newtreenode(ref(v_treeNode), ref(v_treeNode), v_item));
  }
  return null;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$binarytrees_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/binarytrees.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$binarytrees_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_minDepth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("minDepth") : g->GV(minDepth);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_maxDepth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("maxDepth") : g->GV(maxDepth);
  Variant &v_stretchDepth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stretchDepth") : g->GV(stretchDepth);
  Variant &v_stretchTree __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stretchTree") : g->GV(stretchTree);
  Variant &v_longLivedTree __attribute__((__unused__)) = (variables != gVariables) ? variables->get("longLivedTree") : g->GV(longLivedTree);
  Variant &v_depth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("depth") : g->GV(depth);
  Variant &v_iterations __attribute__((__unused__)) = (variables != gVariables) ? variables->get("iterations") : g->GV(iterations);
  Variant &v_check __attribute__((__unused__)) = (variables != gVariables) ? variables->get("check") : g->GV(check);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_minDepth = 4LL);
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_maxDepth = LINE(40,x_max(2, v_minDepth + 2LL, Array(ArrayInit(1).set(0, v_n).create()))));
  (v_stretchDepth = v_maxDepth + 1LL);
  (v_stretchTree = LINE(43,f_bottomuptree(0LL, v_stretchDepth)));
  LINE(45,(assignCallTemp(eo_1, v_stretchDepth),assignCallTemp(eo_2, f_itemcheck(ref(v_stretchTree))),x_printf(3, "stretch tree of depth %d\t check: %d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  unset(v_stretchTree);
  (v_longLivedTree = LINE(48,f_bottomuptree(0LL, v_maxDepth)));
  {
    LOOP_COUNTER(1);
    for ((v_depth = v_minDepth); not_more(v_depth, v_maxDepth); v_depth += 2LL) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_iterations = toInt64(1LL) << (toInt64(v_maxDepth - v_depth + v_minDepth)));
        (v_check = 0LL);
        {
          LOOP_COUNTER(2);
          for ((v_i = 1LL); not_more(v_i, v_iterations); v_i++) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_t = LINE(55,f_bottomuptree(v_i, v_depth)));
              v_check += LINE(56,f_itemcheck(ref(v_t)));
              setNull(v_t);
              (v_t = LINE(59,f_bottomuptree(negate(v_i), v_depth)));
              v_check += LINE(60,f_itemcheck(ref(v_t)));
              setNull(v_t);
            }
          }
        }
        LINE(65,x_printf(4, "%d\t trees of depth %d\t check: %d\n", Array(ArrayInit(3).set(0, 2LL * v_iterations).set(1, v_depth).set(2, v_check).create())));
      }
    }
  }
  LINE(69,(assignCallTemp(eo_1, v_maxDepth),assignCallTemp(eo_2, f_itemcheck(ref(v_longLivedTree))),x_printf(3, "long lived tree of depth %d\t check: %d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
