
#include <php/phc-test/subjects/benchmarks/shootout/nsieve.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/nsieve.php line 9 */
int64 f_nsieve(CVarRef v_m, Variant v_isPrime) {
  FUNCTION_INJECTION(NSieve);
  int64 v_i = 0;
  int64 v_count = 0;
  int64 v_k = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 2LL); not_more(v_i, v_m); v_i++) {
      LOOP_COUNTER_CHECK(1);
      v_isPrime.set(v_i, (true));
    }
  }
  (v_count = 0LL);
  {
    LOOP_COUNTER(2);
    for ((v_i = 2LL); not_more(v_i, v_m); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        if (toBoolean(v_isPrime.rvalAt(v_i))) {
          {
            LOOP_COUNTER(3);
            for ((v_k = v_i + v_i); not_more(v_k, v_m); v_k += v_i) {
              LOOP_COUNTER_CHECK(3);
              v_isPrime.set(v_k, (false));
            }
          }
          v_count++;
        }
      }
    }
  }
  return v_count;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$nsieve_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/nsieve.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$nsieve_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_flags __attribute__((__unused__)) = (variables != gVariables) ? variables->get("flags") : g->GV(flags);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_flags = ScalarArrays::sa_[0]);
  (v_m = (toInt64(1LL) << toInt64(v_n)) * 10000LL);
  LINE(27,(assignCallTemp(eo_1, v_m),assignCallTemp(eo_2, f_nsieve(v_m, ref(v_flags))),x_printf(3, "Primes up to %8d %8d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  (v_m = (toInt64(1LL) << toInt64(v_n - 1LL)) * 10000LL);
  LINE(30,(assignCallTemp(eo_1, v_m),assignCallTemp(eo_2, f_nsieve(v_m, ref(v_flags))),x_printf(3, "Primes up to %8d %8d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  (v_m = (toInt64(1LL) << toInt64(v_n - 2LL)) * 10000LL);
  LINE(33,(assignCallTemp(eo_1, v_m),assignCallTemp(eo_2, f_nsieve(v_m, ref(v_flags))),x_printf(3, "Primes up to %8d %8d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
