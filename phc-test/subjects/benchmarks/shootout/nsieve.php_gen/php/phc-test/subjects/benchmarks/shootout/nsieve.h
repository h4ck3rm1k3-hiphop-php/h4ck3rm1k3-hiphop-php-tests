
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsieve_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsieve_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/nsieve.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_nsieve(CVarRef v_m, Variant v_isPrime);
Variant pm_php$phc_test$subjects$benchmarks$shootout$nsieve_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsieve_h__
