
#include <php/phc-test/subjects/benchmarks/shootout/regexdna.php-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$regexdna_php_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/regexdna.php-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$regexdna_php_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_variants __attribute__((__unused__)) = (variables != gVariables) ? variables->get("variants") : g->GV(variants);
  Variant &v_IUB __attribute__((__unused__)) = (variables != gVariables) ? variables->get("IUB") : g->GV(IUB);
  Variant &v_IUBnew __attribute__((__unused__)) = (variables != gVariables) ? variables->get("IUBnew") : g->GV(IUBnew);
  Variant &v_stuffToRemove __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stuffToRemove") : g->GV(stuffToRemove);
  Variant &v_contents __attribute__((__unused__)) = (variables != gVariables) ? variables->get("contents") : g->GV(contents);
  Variant &v_initialLength __attribute__((__unused__)) = (variables != gVariables) ? variables->get("initialLength") : g->GV(initialLength);
  Variant &v_codeLength __attribute__((__unused__)) = (variables != gVariables) ? variables->get("codeLength") : g->GV(codeLength);
  Variant &v_regex __attribute__((__unused__)) = (variables != gVariables) ? variables->get("regex") : g->GV(regex);
  Variant &v_discard __attribute__((__unused__)) = (variables != gVariables) ? variables->get("discard") : g->GV(discard);

  LINE(15,x_ini_set("memory_limit", "40M"));
  (v_variants = ScalarArrays::sa_[0]);
  (v_IUB = ScalarArrays::sa_[1]);
  (v_IUBnew = ScalarArrays::sa_[1]);
  v_IUB.append(("/B/"));
  v_IUBnew.append(("(c|g|t)"));
  v_IUB.append(("/D/"));
  v_IUBnew.append(("(a|g|t)"));
  v_IUB.append(("/H/"));
  v_IUBnew.append(("(a|c|t)"));
  v_IUB.append(("/K/"));
  v_IUBnew.append(("(g|t)"));
  v_IUB.append(("/M/"));
  v_IUBnew.append(("(a|c)"));
  v_IUB.append(("/N/"));
  v_IUBnew.append(("(a|c|g|t)"));
  v_IUB.append(("/R/"));
  v_IUBnew.append(("(a|g)"));
  v_IUB.append(("/S/"));
  v_IUBnew.append(("(c|g)"));
  v_IUB.append(("/V/"));
  v_IUBnew.append(("(a|c|g)"));
  v_IUB.append(("/W/"));
  v_IUBnew.append(("(a|t)"));
  v_IUB.append(("/Y/"));
  v_IUBnew.append(("(c|t)"));
  (v_stuffToRemove = "^>.*$|\\n");
  (v_contents = LINE(48,x_file_get_contents("php://stdin")));
  (v_initialLength = LINE(49,x_strlen(toString(v_contents))));
  (v_contents = LINE(52,(assignCallTemp(eo_0, concat3("/", toString(v_stuffToRemove), "/m")),assignCallTemp(eo_2, v_contents),x_preg_replace(eo_0, "", eo_2))));
  (v_codeLength = LINE(53,x_strlen(toString(v_contents))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_variants.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_regex = iter3->second();
      {
        print(LINE(57,(assignCallTemp(eo_0, toString(v_regex)),assignCallTemp(eo_2, toString((assignCallTemp(eo_4, concat3("/", toString(v_regex), "/i")),assignCallTemp(eo_5, toString(v_contents)),assignCallTemp(eo_6, ref(v_discard)),x_preg_match_all(eo_4, eo_5, eo_6)))),concat4(eo_0, " ", eo_2, "\n"))));
      }
    }
  }
  (v_contents = LINE(61,x_preg_replace(v_IUB, v_IUBnew, v_contents)));
  print(concat("\n", LINE(66,(assignCallTemp(eo_0, toString(v_initialLength)),assignCallTemp(eo_2, toString(v_codeLength)),assignCallTemp(eo_4, toString(x_strlen(toString(v_contents)))),concat6(eo_0, "\n", eo_2, "\n", eo_4, "\n")))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
