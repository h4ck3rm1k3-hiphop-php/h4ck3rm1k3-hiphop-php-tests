
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_regexdna_php_2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_regexdna_php_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/regexdna.php-2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$regexdna_php_2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_regexdna_php_2_h__
