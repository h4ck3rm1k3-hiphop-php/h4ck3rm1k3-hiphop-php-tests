
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x2963E59544F2D0CFLL, regex, 19);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x00D99111E1C44310LL, IUB, 13);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 33:
      HASH_INDEX(0x2FD8F1D6CBAFABE1LL, codeLength, 18);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 36:
      HASH_INDEX(0x0AC273C2F804B7E4LL, variants, 12);
      break;
    case 38:
      HASH_INDEX(0x4AB17722F88653A6LL, stuffToRemove, 15);
      HASH_INDEX(0x48FE11C3FC0B52E6LL, discard, 20);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 52:
      HASH_INDEX(0x3CF1D34C29CF1074LL, initialLength, 17);
      break;
    case 59:
      HASH_INDEX(0x5F2DBE4603FD097BLL, IUBnew, 14);
      break;
    case 62:
      HASH_INDEX(0x5FAC83E143BEACFELL, contents, 16);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 21) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
