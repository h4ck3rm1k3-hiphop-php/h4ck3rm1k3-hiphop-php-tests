
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 15:
      HASH_INITIALIZED(0x2963E59544F2D0CFLL, g->GV(regex),
                       regex);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      HASH_INITIALIZED(0x00D99111E1C44310LL, g->GV(IUB),
                       IUB);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 33:
      HASH_INITIALIZED(0x2FD8F1D6CBAFABE1LL, g->GV(codeLength),
                       codeLength);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 36:
      HASH_INITIALIZED(0x0AC273C2F804B7E4LL, g->GV(variants),
                       variants);
      break;
    case 38:
      HASH_INITIALIZED(0x4AB17722F88653A6LL, g->GV(stuffToRemove),
                       stuffToRemove);
      HASH_INITIALIZED(0x48FE11C3FC0B52E6LL, g->GV(discard),
                       discard);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 52:
      HASH_INITIALIZED(0x3CF1D34C29CF1074LL, g->GV(initialLength),
                       initialLength);
      break;
    case 59:
      HASH_INITIALIZED(0x5F2DBE4603FD097BLL, g->GV(IUBnew),
                       IUBnew);
      break;
    case 62:
      HASH_INITIALIZED(0x5FAC83E143BEACFELL, g->GV(contents),
                       contents);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
