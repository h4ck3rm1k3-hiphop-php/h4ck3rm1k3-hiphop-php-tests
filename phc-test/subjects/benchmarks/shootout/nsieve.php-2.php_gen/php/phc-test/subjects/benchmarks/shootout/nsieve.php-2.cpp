
#include <php/phc-test/subjects/benchmarks/shootout/nsieve.php-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/nsieve.php-2.php line 11 */
void f_nsieve(int64 v_m) {
  FUNCTION_INJECTION(nsieve);
  Variant v_flags;
  int64 v_count = 0;
  int64 v_i = 0;
  int64 v_j = 0;

  (v_flags = " ");
  v_flags.set(v_m, (" "));
  (v_count = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 2LL); less(v_i, v_m); ++v_i) {
      LOOP_COUNTER_CHECK(1);
      if (equal(v_flags.rvalAt(v_i), " ")) {
        ++v_count;
        {
          LOOP_COUNTER(2);
          for ((v_j = toInt64(v_i) << 1LL); less(v_j, v_m); v_j += v_i) {
            LOOP_COUNTER_CHECK(2);
            v_flags.set(v_j, ("x"));
          }
        }
      }
    }
  }
  LINE(25,x_printf(3, "Primes up to %8d %8d\n", Array(ArrayInit(2).set(0, v_m).set(1, v_count).create())));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$nsieve_php_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/nsieve.php-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$nsieve_php_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_m = v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, 3LL); v_i++) {
      LOOP_COUNTER_CHECK(3);
      LINE(31,f_nsieve(toInt64(10000LL) << (toInt64(v_m - v_i))));
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
