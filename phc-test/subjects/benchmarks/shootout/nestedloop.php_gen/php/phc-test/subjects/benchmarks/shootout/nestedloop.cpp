
#include <php/phc-test/subjects/benchmarks/shootout/nestedloop.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$nestedloop_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/nestedloop.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$nestedloop_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_x = 0LL);
  (v_a = v_n);
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_a--)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_b = v_n);
        LOOP_COUNTER(2);
        {
          while (toBoolean(v_b--)) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_c = v_n);
              LOOP_COUNTER(3);
              {
                while (toBoolean(v_c--)) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    (v_d = v_n);
                    LOOP_COUNTER(4);
                    {
                      while (toBoolean(v_d--)) {
                        LOOP_COUNTER_CHECK(4);
                        {
                          (v_e = v_n);
                          LOOP_COUNTER(5);
                          {
                            while (toBoolean(v_e--)) {
                              LOOP_COUNTER_CHECK(5);
                              {
                                (v_f = v_n);
                                LOOP_COUNTER(6);
                                {
                                  while (toBoolean(v_f--)) {
                                    LOOP_COUNTER_CHECK(6);
                                    v_x++;
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  print(toString(v_x) + toString("\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
