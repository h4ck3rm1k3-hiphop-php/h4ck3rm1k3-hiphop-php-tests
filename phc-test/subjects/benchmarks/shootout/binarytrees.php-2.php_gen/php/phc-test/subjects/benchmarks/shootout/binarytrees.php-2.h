
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_binarytrees_php_2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_binarytrees_php_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/binarytrees.php-2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_itemcheck(Variant v_treeNode);
Variant pm_php$phc_test$subjects$benchmarks$shootout$binarytrees_php_2_php(bool incOnce = false, LVariableTable* variables = NULL);
Array f_bottomuptree(CVarRef v_item, Variant v_depth);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_binarytrees_php_2_h__
