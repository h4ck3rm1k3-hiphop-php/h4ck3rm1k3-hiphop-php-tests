
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_LEVELS;
extern const int64 k_SS;
extern const double k_EPSILON;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x5EC39F772CCD8F80LL, k_EPSILON, EPSILON);
      break;
    case 3:
      HASH_RETURN(0x59843B1F5260A833LL, k_LEVELS, LEVELS);
      break;
    case 4:
      HASH_RETURN(0x31B6AD1B8F8D70ACLL, g->k_INFINITY, INFINITY);
      break;
    case 5:
      HASH_RETURN(0x4F0809C052D0B725LL, k_SS, SS);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
