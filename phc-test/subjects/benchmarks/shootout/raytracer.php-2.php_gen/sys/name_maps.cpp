
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "group", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "scene", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "sphere", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "add", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "dot", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "newintersectionpoint", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "newray", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "newvector", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "normalize", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "scaleby", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  "subtract", "phc-test/subjects/benchmarks/shootout/raytracer.php-2.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
