
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 5:
      HASH_RETURN(0x0F25F65BB8BD8085LL, g->GV(greyscale),
                  greyscale);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x117B8667E4662809LL, g->GV(n),
                  n);
      HASH_RETURN(0x2A98F317E9E2AA49LL, g->GV(u),
                  u);
      break;
    case 10:
      HASH_RETURN(0x4F56B733A4DFC78ALL, g->GV(y),
                  y);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x3F2BA2EBC2E2FC93LL, g->GV(scene),
                  scene);
      break;
    case 20:
      HASH_RETURN(0x7AC602B00D6CB114LL, g->GV(dy),
                  dy);
      break;
    case 22:
      HASH_RETURN(0x04BFC205E59FA416LL, g->GV(x),
                  x);
      break;
    case 23:
      HASH_RETURN(0x56670E8861466997LL, g->GV(ray),
                  ray);
      break;
    case 34:
      HASH_RETURN(0x498FFBE55EEFA4A2LL, g->GV(dx),
                  dx);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 43:
      HASH_RETURN(0x3C2F961831E4EF6BLL, g->GV(v),
                  v);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
