
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_php_2_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_php_2_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_LEVELS;
extern const int64 k_SS;
extern const double k_EPSILON;


// 2. Classes
FORWARD_DECLARE_CLASS(group)
FORWARD_DECLARE_CLASS(sphere)
FORWARD_DECLARE_CLASS(scene)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_php_2_fw_h__
