
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_php_2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_php_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/raytracer.php-2.fw.h>

// Declarations
#include <cls/group.h>
#include <cls/sphere.h>
#include <cls/scene.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_newray(CVarRef v_origin, CVarRef v_direction);
Variant pm_php$phc_test$subjects$benchmarks$shootout$raytracer_php_2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_subtract(Variant v_a, Variant v_b);
PlusOperand f_dot(Variant v_a, Variant v_b);
Variant f_newvector(Numeric v_x, CVarRef v_y, CVarRef v_z);
Variant f_newintersectionpoint(CVarRef v_distance, CVarRef v_normal);
Variant f_normalize(Variant v_a);
Variant f_scaleby(Variant v_a, CVarRef v_s);
Variant f_add(Variant v_a, Variant v_b);
Object co_group(CArrRef params, bool init = true);
Object co_sphere(CArrRef params, bool init = true);
Object co_scene(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_raytracer_php_2_h__
