
#include <php/phc-test/subjects/benchmarks/shootout/raytracer.php-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_LEVELS = 6LL;
const int64 k_SS = 4LL;
const double k_EPSILON = 1.49012E-8;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 146 */
Variant c_group::os_get(const char *s, int64 hash) {
  return c_scene::os_get(s, hash);
}
Variant &c_group::os_lval(const char *s, int64 hash) {
  return c_scene::os_lval(s, hash);
}
void c_group::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("bound", m_bound.isReferenced() ? ref(m_bound) : m_bound));
  props.push_back(NEW(ArrayElement)("scenes", m_scenes.isReferenced() ? ref(m_scenes) : m_scenes));
  c_scene::o_get(props);
}
bool c_group::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x1200603171C0F3B0LL, bound, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x5EC93B9A46EA3E2ALL, scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_exists(s, hash);
}
Variant c_group::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x1200603171C0F3B0LL, m_bound,
                         bound, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x5EC93B9A46EA3E2ALL, m_scenes,
                         scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_get(s, hash);
}
Variant c_group::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x1200603171C0F3B0LL, m_bound,
                      bound, 5);
      break;
    case 2:
      HASH_SET_STRING(0x5EC93B9A46EA3E2ALL, m_scenes,
                      scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_set(s, hash, v, forInit);
}
Variant &c_group::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x1200603171C0F3B0LL, m_bound,
                         bound, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x5EC93B9A46EA3E2ALL, m_scenes,
                         scenes, 6);
      break;
    default:
      break;
  }
  return c_scene::o_lval(s, hash);
}
Variant c_group::os_constant(const char *s) {
  return c_scene::os_constant(s);
}
IMPLEMENT_CLASS(group)
ObjectData *c_group::create(p_sphere v_bound) {
  init();
  t_group(v_bound);
  return this;
}
ObjectData *c_group::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_group::cloneImpl() {
  c_group *obj = NEW(c_group)();
  cloneSet(obj);
  return obj;
}
void c_group::cloneSet(c_group *clone) {
  clone->m_bound = m_bound.isReferenced() ? ref(m_bound) : m_bound;
  clone->m_scenes = m_scenes.isReferenced() ? ref(m_scenes) : m_scenes;
  c_scene::cloneSet(clone);
}
Variant c_group::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      break;
    case 3:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke(s, params, hash, fatal);
}
Variant c_group::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(a0), ref(a1)));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(a0), ref(a1)));
      }
      break;
    case 3:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(a0), null);
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_group::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_scene::os_invoke(c, s, params, hash, fatal);
}
Variant cw_group$os_get(const char *s) {
  return c_group::os_get(s, -1);
}
Variant &cw_group$os_lval(const char *s) {
  return c_group::os_lval(s, -1);
}
Variant cw_group$os_constant(const char *s) {
  return c_group::os_constant(s);
}
Variant cw_group$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_group::os_invoke(c, s, params, -1, fatal);
}
void c_group::init() {
  c_scene::init();
  m_bound = null;
  m_scenes = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 149 */
void c_group::t_group(p_sphere v_bound) {
  INSTANCE_METHOD_INJECTION(Group, Group::Group);
  bool oldInCtor = gasInCtor(true);
  (m_bound = ((Object)(v_bound)));
  (m_scenes = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 154 */
Variant c_group::t_intersect(Variant v_ray, Variant v_p) {
  INSTANCE_METHOD_INJECTION(Group, Group::intersect);
  Variant v_each;

  if (less((LINE(155,m_bound.o_invoke_few_args("distance", 0x5225E2593FAA1157LL, 1, v_ray))), v_p.rvalAt("distance", 0x1E9F6AF49FD61031LL))) {
    {
      LOOP_COUNTER(1);
      Variant map2 = m_scenes;
      for (ArrayIterPtr iter3 = map2.begin("group"); !iter3->end(); iter3->next()) {
        LOOP_COUNTER_CHECK(1);
        v_each = iter3->second();
        {
          (v_p = LINE(157,v_each.o_invoke_few_args("intersect", 0x4A3F7CCEE998444ALL, 2, v_ray, v_p)));
        }
      }
    }
  }
  return v_p;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 163 */
void c_group::t_add(CVarRef v_s) {
  INSTANCE_METHOD_INJECTION(Group, Group::add);
  LINE(164,x_array_unshift(2, ref(lval(m_scenes)), v_s));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 108 */
Variant c_sphere::os_get(const char *s, int64 hash) {
  return c_scene::os_get(s, hash);
}
Variant &c_sphere::os_lval(const char *s, int64 hash) {
  return c_scene::os_lval(s, hash);
}
void c_sphere::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("center", m_center.isReferenced() ? ref(m_center) : m_center));
  props.push_back(NEW(ArrayElement)("radius", m_radius.isReferenced() ? ref(m_radius) : m_radius));
  c_scene::o_get(props);
}
bool c_sphere::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x13251570EF5983D9LL, center, 6);
      HASH_EXISTS_STRING(0x7FC3CA32D2D213D5LL, radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_exists(s, hash);
}
Variant c_sphere::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x13251570EF5983D9LL, m_center,
                         center, 6);
      HASH_RETURN_STRING(0x7FC3CA32D2D213D5LL, m_radius,
                         radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_get(s, hash);
}
Variant c_sphere::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x13251570EF5983D9LL, m_center,
                      center, 6);
      HASH_SET_STRING(0x7FC3CA32D2D213D5LL, m_radius,
                      radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_set(s, hash, v, forInit);
}
Variant &c_sphere::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x13251570EF5983D9LL, m_center,
                         center, 6);
      HASH_RETURN_STRING(0x7FC3CA32D2D213D5LL, m_radius,
                         radius, 6);
      break;
    default:
      break;
  }
  return c_scene::o_lval(s, hash);
}
Variant c_sphere::os_constant(const char *s) {
  return c_scene::os_constant(s);
}
IMPLEMENT_CLASS(sphere)
ObjectData *c_sphere::create(CVarRef v_center, Numeric v_radius) {
  init();
  t_sphere(v_center, v_radius);
  return this;
}
ObjectData *c_sphere::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
ObjectData *c_sphere::cloneImpl() {
  c_sphere *obj = NEW(c_sphere)();
  cloneSet(obj);
  return obj;
}
void c_sphere::cloneSet(c_sphere *clone) {
  clone->m_center = m_center.isReferenced() ? ref(m_center) : m_center;
  clone->m_radius = m_radius.isReferenced() ? ref(m_radius) : m_radius;
  c_scene::cloneSet(clone);
}
Variant c_sphere::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      break;
    case 7:
      HASH_GUARD(0x5225E2593FAA1157LL, distance) {
        return (t_distance(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke(s, params, hash, fatal);
}
Variant c_sphere::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(a0), ref(a1)));
      }
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(ref(a0), ref(a1)));
      }
      break;
    case 7:
      HASH_GUARD(0x5225E2593FAA1157LL, distance) {
        return (t_distance(ref(a0)));
      }
      break;
    default:
      break;
  }
  return c_scene::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sphere::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_scene::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sphere$os_get(const char *s) {
  return c_sphere::os_get(s, -1);
}
Variant &cw_sphere$os_lval(const char *s) {
  return c_sphere::os_lval(s, -1);
}
Variant cw_sphere$os_constant(const char *s) {
  return c_sphere::os_constant(s);
}
Variant cw_sphere$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sphere::os_invoke(c, s, params, -1, fatal);
}
void c_sphere::init() {
  c_scene::init();
  m_center = null;
  m_radius = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 111 */
void c_sphere::t_sphere(CVarRef v_center, Numeric v_radius) {
  INSTANCE_METHOD_INJECTION(Sphere, Sphere::Sphere);
  bool oldInCtor = gasInCtor(true);
  (m_center = v_center);
  (m_radius = v_radius);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 116 */
Variant c_sphere::t_distance(Variant v_ray) {
  INSTANCE_METHOD_INJECTION(Sphere, Sphere::distance);
  Variant v_v;
  PlusOperand v_b = 0;
  PlusOperand v_disc = 0;
  double v_d = 0.0;
  double v_t1 = 0.0;
  double v_t2 = 0.0;

  (v_v = LINE(117,f_subtract(ref(lval(m_center)), ref(lval(v_ray.lvalAt("origin", 0x6383DE2A8EE50332LL))))));
  (v_b = LINE(118,f_dot(ref(v_v), ref(lval(v_ray.lvalAt("direction", 0x10014728FF258839LL))))));
  (v_disc = v_b * v_b - LINE(119,f_dot(ref(v_v), ref(v_v))) + m_radius * m_radius);
  if (less(v_disc, 0.0)) {
    return get_global_variables()->k_INFINITY;
  }
  (v_d = LINE(122,x_sqrt(toDouble(v_disc))));
  (v_t1 = toDouble(v_b) + v_d);
  if (less(v_t1, 0.0)) {
    return get_global_variables()->k_INFINITY;
  }
  (v_t2 = toDouble(v_b) - v_d);
  if (more(v_t2, 0.0)) {
    return v_t2;
  }
  else {
    return v_t1;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 130 */
Variant c_sphere::t_intersect(Variant v_ray, Variant v_p) {
  INSTANCE_METHOD_INJECTION(Sphere, Sphere::intersect);
  Variant eo_0;
  Variant eo_1;
  Variant v_d;
  Variant v_rayOrigin;
  Variant v_rayDirection;
  Variant v_scaledDirection;
  Variant v_v;

  (v_d = LINE(131,t_distance(ref(v_ray))));
  if (less(v_d, v_p.rvalAt("distance", 0x1E9F6AF49FD61031LL))) {
    (v_rayOrigin = v_ray.rvalAt("origin", 0x6383DE2A8EE50332LL));
    (v_rayDirection = v_ray.rvalAt("direction", 0x10014728FF258839LL));
    (v_scaledDirection = LINE(135,f_scaleby(ref(v_rayDirection), v_d)));
    (v_v = LINE(137,(assignCallTemp(eo_0, ref(v_rayOrigin)),assignCallTemp(eo_1, ref(f_subtract(ref(v_scaledDirection), ref(lval(m_center))))),f_add(eo_0, eo_1))));
    (v_p = LINE(138,(assignCallTemp(eo_0, v_d),assignCallTemp(eo_1, f_normalize(ref(v_v))),f_newintersectionpoint(eo_0, eo_1))));
  }
  return v_p;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 54 */
Variant c_scene::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_scene::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_scene::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_scene::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_scene::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_scene::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_scene::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_scene::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(scene)
ObjectData *c_scene::cloneImpl() {
  c_scene *obj = NEW(c_scene)();
  cloneSet(obj);
  return obj;
}
void c_scene::cloneSet(c_scene *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_scene::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1))));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_scene::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x20DF5A198C012382LL, traceray) {
        return (t_traceray(ref(a0), ref(a1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_scene::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_scene$os_get(const char *s) {
  return c_scene::os_get(s, -1);
}
Variant &cw_scene$os_lval(const char *s) {
  return c_scene::os_lval(s, -1);
}
Variant cw_scene$os_constant(const char *s) {
  return c_scene::os_constant(s);
}
Variant cw_scene$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_scene::os_invoke(c, s, params, -1, fatal);
}
void c_scene::init() {
}
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 56 */
Variant c_scene::ti_spherescene(const char* cls, Numeric v_level, Variant v_center, Numeric v_radius) {
  STATIC_METHOD_INJECTION(Scene, Scene::sphereScene);
  Variant v_sphere;
  Variant v_scene;
  Numeric v_rn = 0;
  int64 v_dz = 0;
  int64 v_dx = 0;
  Variant v_c2;

  (v_sphere = ((Object)(LINE(57,p_sphere(p_sphere(NEWOBJ(c_sphere)())->create(v_center, v_radius))))));
  if (equal(v_level, 1LL)) {
    return ref(v_sphere);
  }
  else {
    (v_scene = ((Object)(LINE(62,p_group(p_group(NEWOBJ(c_group)())->create(((Object)(p_sphere(p_sphere(NEWOBJ(c_sphere)())->create(v_center, 3.0 * toDouble(v_radius)))))))))));
    LINE(63,v_scene.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_sphere));
    (v_rn = divide(3.0 * toDouble(v_radius), LINE(64,x_sqrt(12.0))));
    {
      LOOP_COUNTER(4);
      for ((v_dz = -1LL); not_more(v_dz, 1LL); v_dz += 2LL) {
        LOOP_COUNTER_CHECK(4);
        {
          {
            LOOP_COUNTER(5);
            for ((v_dx = -1LL); not_more(v_dx, 1LL); v_dx += 2LL) {
              LOOP_COUNTER_CHECK(5);
              {
                (v_c2 = LINE(73,f_newvector(v_center.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) - v_dx * v_rn, v_center.rvalAt(1LL, 0x5BCA7C69B794F8CELL) + v_rn, v_center.rvalAt(2LL, 0x486AFCC090D5F98CLL) - v_dz * v_rn)));
                LINE(75,v_scene.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, c_scene::t_spherescene(v_level - 1LL, ref(v_c2), divide(v_radius, 2.0))));
              }
            }
          }
        }
      }
    }
    return ref(v_scene);
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 82 */
Variant c_scene::t_traceray(Variant v_ray, Variant v_light) {
  INSTANCE_METHOD_INJECTION(Scene, Scene::traceRay);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_p;
  double v_greyscale = 0.0;
  Variant v_rayOrigin;
  Variant v_scaledDirection;
  Variant v_scaledNormal;
  Variant v_o;
  Variant v_v;
  Variant v_shadowRay;
  Variant v_shadowp;

  (v_p = (assignCallTemp(eo_0, ref(v_ray)),assignCallTemp(eo_1, ref(LINE(84,(assignCallTemp(eo_2, get_global_variables()->k_INFINITY),assignCallTemp(eo_3, f_newvector(0.0, 0.0, 0.0)),f_newintersectionpoint(eo_2, eo_3))))),o_root_invoke("intersect", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x4A3F7CCEE998444ALL)));
  if (LINE(85,x_is_infinite(toDouble(v_p.rvalAt("distance", 0x1E9F6AF49FD61031LL))))) {
    return 0.0;
  }
  (v_greyscale = -1.0 * toDouble(LINE(87,f_dot(ref(lval(v_p.lvalAt("normal", 0x5847C6837EFC3014LL))), ref(v_light)))));
  if (not_more(v_greyscale, 0.0)) {
    return 0.0;
  }
  (v_rayOrigin = v_ray.rvalAt("origin", 0x6383DE2A8EE50332LL));
  (v_scaledDirection = LINE(91,f_scaleby(ref(lval(v_ray.lvalAt("direction", 0x10014728FF258839LL))), v_p.rvalAt("distance", 0x1E9F6AF49FD61031LL))));
  (v_scaledNormal = LINE(92,f_scaleby(ref(lval(v_p.lvalAt("normal", 0x5847C6837EFC3014LL))), 1.49012E-8 /* EPSILON */)));
  (v_o = LINE(93,f_add(ref(v_rayOrigin), ref(v_scaledDirection))));
  (v_o = LINE(94,f_add(ref(v_o), ref(v_scaledNormal))));
  (v_v = LINE(96,f_newvector(0.0, 0.0, 0.0)));
  (v_shadowRay = LINE(97,(assignCallTemp(eo_0, v_o),assignCallTemp(eo_1, f_subtract(ref(v_v), ref(v_light))),f_newray(eo_0, eo_1))));
  (v_shadowp = (assignCallTemp(eo_0, ref(v_shadowRay)),assignCallTemp(eo_1, ref(LINE(99,f_newintersectionpoint(get_global_variables()->k_INFINITY, v_p.rvalAt("normal", 0x5847C6837EFC3014LL))))),o_root_invoke("intersect", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x4A3F7CCEE998444ALL)));
  if (LINE(101,x_is_infinite(toDouble(v_shadowp.rvalAt("distance", 0x1E9F6AF49FD61031LL))))) {
    return v_greyscale;
  }
  else {
    return 0.0;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 43 */
Variant f_newray(CVarRef v_origin, CVarRef v_direction) {
  FUNCTION_INJECTION(newRay);
  return Array(ArrayInit(2).set(0, "origin", v_origin, 0x6383DE2A8EE50332LL).set(1, "direction", v_direction, 0x10014728FF258839LL).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 26 */
Variant f_subtract(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(subtract);
  return Array(ArrayInit(3).set(0, v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) - v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL) - v_b.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(2, v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL) - v_b.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 30 */
PlusOperand f_dot(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(dot);
  return v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) * v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) + v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL) * v_b.rvalAt(1LL, 0x5BCA7C69B794F8CELL) + v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL) * v_b.rvalAt(2LL, 0x486AFCC090D5F98CLL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 18 */
Variant f_newvector(Numeric v_x, CVarRef v_y, CVarRef v_z) {
  FUNCTION_INJECTION(newVector);
  return Array(ArrayInit(3).set(0, v_x).set(1, v_y).set(2, v_z).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 48 */
Variant f_newintersectionpoint(CVarRef v_distance, CVarRef v_normal) {
  FUNCTION_INJECTION(newIntersectionPoint);
  return Array(ArrayInit(2).set(0, "distance", v_distance, 0x1E9F6AF49FD61031LL).set(1, "normal", v_normal, 0x5847C6837EFC3014LL).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 38 */
Variant f_normalize(Variant v_a) {
  FUNCTION_INJECTION(normalize);
  Variant eo_0;
  Variant eo_1;
  return ref(LINE(39,(assignCallTemp(eo_0, ref(v_a)),assignCallTemp(eo_1, divide(1.0, x_sqrt(toDouble(f_dot(ref(v_a), ref(v_a)))))),f_scaleby(eo_0, eo_1))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 34 */
Variant f_scaleby(Variant v_a, CVarRef v_s) {
  FUNCTION_INJECTION(scaleBy);
  return Array(ArrayInit(3).set(0, v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) * v_s).set(1, v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL) * v_s).set(2, v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL) * v_s).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 22 */
Variant f_add(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(add);
  return Array(ArrayInit(3).set(0, v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) + v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL) + v_b.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(2, v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL) + v_b.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create());
} /* function */
Object co_group(CArrRef params, bool init /* = true */) {
  return Object(p_group(NEW(c_group)())->dynCreate(params, init));
}
Object co_sphere(CArrRef params, bool init /* = true */) {
  return Object(p_sphere(NEW(c_sphere)())->dynCreate(params, init));
}
Object co_scene(CArrRef params, bool init /* = true */) {
  return Object(p_scene(NEW(c_scene)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$raytracer_php_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/raytracer.php-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$raytracer_php_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_scene __attribute__((__unused__)) = (variables != gVariables) ? variables->get("scene") : g->GV(scene);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_greyscale __attribute__((__unused__)) = (variables != gVariables) ? variables->get("greyscale") : g->GV(greyscale);
  Variant &v_dx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dx") : g->GV(dx);
  Variant &v_dy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dy") : g->GV(dy);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_ray __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ray") : g->GV(ray);
  Variant &v_u __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u") : g->GV(u);

  LINE(11,g->declareConstant("INFINITY", g->k_INFINITY, x_log(toDouble(0LL)) * -1.0));
  ;
  ;
  ;
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_scene = LINE(173,(assignCallTemp(eo_1, ref(f_newvector(0.0, -1.0, 0.0))),c_scene::t_spherescene(6LL /* LEVELS */, eo_1, 1.0))));
  LINE(175,x_printf(3, "P5\n%d %d\n255\n", Array(ArrayInit(2).set(0, v_n).set(1, v_n).create())));
  {
    LOOP_COUNTER(6);
    for ((v_y = v_n - 1LL); not_less(v_y, 0LL); --v_y) {
      LOOP_COUNTER_CHECK(6);
      {
        {
          LOOP_COUNTER(7);
          for ((v_x = 0LL); less(v_x, v_n); ++v_x) {
            LOOP_COUNTER_CHECK(7);
            {
              (v_greyscale = 0.0);
              {
                LOOP_COUNTER(8);
                for ((v_dx = 0LL); less(v_dx, 4LL /* SS */); ++v_dx) {
                  LOOP_COUNTER_CHECK(8);
                  {
                    {
                      LOOP_COUNTER(9);
                      for ((v_dy = 0LL); less(v_dy, 4LL /* SS */); ++v_dy) {
                        LOOP_COUNTER_CHECK(9);
                        {
                          (v_v = LINE(187,f_newvector(v_x + (divide(v_dx, 4.0)) - (divide(v_n, 2.0)), v_y + (divide(v_dy, 4.0)) - (divide(v_n, 2.0)), v_n)));
                          (v_ray = LINE(189,(assignCallTemp(eo_0, f_newvector(0.0, 0.0, -4.0)),assignCallTemp(eo_1, f_normalize(ref(v_v))),f_newray(eo_0, eo_1))));
                          (v_u = LINE(191,f_newvector(-1.0, -3.0, 2.0)));
                          v_greyscale += (assignCallTemp(eo_0, ref(v_ray)),assignCallTemp(eo_1, ref(LINE(192,f_normalize(ref(v_u))))),v_scene.o_invoke("traceRay", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x20DF5A198C012382LL));
                        }
                      }
                    }
                  }
                }
              }
              echo(LINE(195,x_chr(toInt64(0.5 + toDouble(divide(255.0 * toDouble(v_greyscale), 16.0))))));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
