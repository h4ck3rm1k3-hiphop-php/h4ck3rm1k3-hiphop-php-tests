
#ifndef __GENERATED_cls_scene_h__
#define __GENERATED_cls_scene_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 54 */
class c_scene : virtual public ObjectData {
  BEGIN_CLASS_MAP(scene)
  END_CLASS_MAP(scene)
  DECLARE_CLASS(scene, Scene, ObjectData)
  void init();
  public: static Variant ti_spherescene(const char* cls, Numeric v_level, Variant v_center, Numeric v_radius);
  public: Variant t_traceray(Variant v_ray, Variant v_light);
  public: static Variant t_spherescene(Numeric v_level, CVarRef v_center, Numeric v_radius) { return ti_spherescene("scene", v_level, v_center, v_radius); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_scene_h__
