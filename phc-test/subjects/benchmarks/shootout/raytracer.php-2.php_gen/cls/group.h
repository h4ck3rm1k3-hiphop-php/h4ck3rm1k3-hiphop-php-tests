
#ifndef __GENERATED_cls_group_h__
#define __GENERATED_cls_group_h__

#include <cls/scene.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/raytracer.php-2.php line 146 */
class c_group : virtual public c_scene {
  BEGIN_CLASS_MAP(group)
    PARENT_CLASS(scene)
  END_CLASS_MAP(group)
  DECLARE_CLASS(group, Group, scene)
  void init();
  public: Variant m_bound;
  public: Variant m_scenes;
  public: void t_group(p_sphere v_bound);
  public: ObjectData *create(p_sphere v_bound);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_intersect(Variant v_ray, Variant v_p);
  public: void t_add(CVarRef v_s);
  public: static Variant t_spherescene(Numeric v_level, CVarRef v_center, Numeric v_radius) { return ti_spherescene("group", v_level, v_center, v_radius); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_group_h__
