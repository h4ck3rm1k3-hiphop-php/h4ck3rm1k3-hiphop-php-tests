
#include <php/phc-test/subjects/benchmarks/shootout/spellcheck.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$spellcheck_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/spellcheck.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$spellcheck_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);
  Variant &v_dict __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dict") : g->GV(dict);

  (v_fp = LINE(12,x_fopen("Usr.Dict.Words", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_line = LINE(13,x_fgets(toObject(v_fp), 128LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        v_dict.set(x_chop(toString(v_line)), (1LL));
      }
    }
  }
  LINE(14,x_fclose(toObject(v_fp)));
  (v_fp = LINE(17,x_fopen("php://stdin", "r")));
  LOOP_COUNTER(2);
  {
    while (toBoolean((v_line = LINE(18,x_fgets(toObject(v_fp), 128LL))))) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_line = LINE(19,x_chop(toString(v_line))));
        if (!(isset(v_dict, v_line))) print(toString(v_line) + toString("\n"));
      }
    }
  }
  LINE(23,x_fclose(toObject(v_fp)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
