
#ifndef __GENERATED_cls_nthtoggle_h__
#define __GENERATED_cls_nthtoggle_h__

#include <cls/toggle.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 19 */
class c_nthtoggle : virtual public c_toggle {
  BEGIN_CLASS_MAP(nthtoggle)
    PARENT_CLASS(toggle)
  END_CLASS_MAP(nthtoggle)
  DECLARE_CLASS(nthtoggle, NthToggle, toggle)
  void init();
  public: Variant m__countMax;
  public: Variant m__count;
  public: void t_nthtoggle(bool v_startState, int64 v_max);
  public: ObjectData *create(bool v_startState, int64 v_max);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_activate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_nthtoggle_h__
