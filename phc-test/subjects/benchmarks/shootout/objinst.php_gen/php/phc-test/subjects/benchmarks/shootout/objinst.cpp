
#include <php/phc-test/subjects/benchmarks/shootout/objinst.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 11 */
Variant c_toggle::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_toggle::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_toggle::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_state", m__state.isReferenced() ? ref(m__state) : m__state));
  c_ObjectData::o_get(props);
}
bool c_toggle::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x0014A349F75421A0LL, _state, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_toggle::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0014A349F75421A0LL, m__state,
                         _state, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_toggle::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x0014A349F75421A0LL, m__state,
                      _state, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_toggle::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0014A349F75421A0LL, m__state,
                         _state, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_toggle::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(toggle)
ObjectData *c_toggle::create(bool v_startState) {
  init();
  t_toggle(v_startState);
  return this;
}
ObjectData *c_toggle::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_toggle::cloneImpl() {
  c_toggle *obj = NEW(c_toggle)();
  cloneSet(obj);
  return obj;
}
void c_toggle::cloneSet(c_toggle *clone) {
  clone->m__state = m__state.isReferenced() ? ref(m__state) : m__state;
  ObjectData::cloneSet(clone);
}
Variant c_toggle::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x78F762A45418F76CLL, activate) {
        return (t_activate(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x021A52B45A788597LL, value) {
        return (t_value());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_toggle::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x78F762A45418F76CLL, activate) {
        return (t_activate(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x021A52B45A788597LL, value) {
        return (t_value());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_toggle::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_toggle$os_get(const char *s) {
  return c_toggle::os_get(s, -1);
}
Variant &cw_toggle$os_lval(const char *s) {
  return c_toggle::os_lval(s, -1);
}
Variant cw_toggle$os_constant(const char *s) {
  return c_toggle::os_constant(s);
}
Variant cw_toggle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_toggle::os_invoke(c, s, params, -1, fatal);
}
void c_toggle::init() {
  m__state = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 13 */
void c_toggle::t_toggle(bool v_startState) {
  INSTANCE_METHOD_INJECTION(Toggle, Toggle::Toggle);
  bool oldInCtor = gasInCtor(true);
  (m__state = v_startState);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 14 */
Variant c_toggle::t_value() {
  INSTANCE_METHOD_INJECTION(Toggle, Toggle::value);
  return m__state;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 15 */
void c_toggle::t_activate() {
  INSTANCE_METHOD_INJECTION(Toggle, Toggle::activate);
  (m__state = !(toBoolean(m__state)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 19 */
Variant c_nthtoggle::os_get(const char *s, int64 hash) {
  return c_toggle::os_get(s, hash);
}
Variant &c_nthtoggle::os_lval(const char *s, int64 hash) {
  return c_toggle::os_lval(s, hash);
}
void c_nthtoggle::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_countMax", m__countMax.isReferenced() ? ref(m__countMax) : m__countMax));
  props.push_back(NEW(ArrayElement)("_count", m__count.isReferenced() ? ref(m__count) : m__count));
  c_toggle::o_get(props);
}
bool c_nthtoggle::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x06DFA3FE7DC27112LL, _countMax, 9);
      break;
    case 3:
      HASH_EXISTS_STRING(0x413DE353FFC3916BLL, _count, 6);
      break;
    default:
      break;
  }
  return c_toggle::o_exists(s, hash);
}
Variant c_nthtoggle::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x06DFA3FE7DC27112LL, m__countMax,
                         _countMax, 9);
      break;
    case 3:
      HASH_RETURN_STRING(0x413DE353FFC3916BLL, m__count,
                         _count, 6);
      break;
    default:
      break;
  }
  return c_toggle::o_get(s, hash);
}
Variant c_nthtoggle::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x06DFA3FE7DC27112LL, m__countMax,
                      _countMax, 9);
      break;
    case 3:
      HASH_SET_STRING(0x413DE353FFC3916BLL, m__count,
                      _count, 6);
      break;
    default:
      break;
  }
  return c_toggle::o_set(s, hash, v, forInit);
}
Variant &c_nthtoggle::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x06DFA3FE7DC27112LL, m__countMax,
                         _countMax, 9);
      break;
    case 3:
      HASH_RETURN_STRING(0x413DE353FFC3916BLL, m__count,
                         _count, 6);
      break;
    default:
      break;
  }
  return c_toggle::o_lval(s, hash);
}
Variant c_nthtoggle::os_constant(const char *s) {
  return c_toggle::os_constant(s);
}
IMPLEMENT_CLASS(nthtoggle)
ObjectData *c_nthtoggle::create(bool v_startState, int64 v_max) {
  init();
  t_nthtoggle(v_startState, v_max);
  return this;
}
ObjectData *c_nthtoggle::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
ObjectData *c_nthtoggle::cloneImpl() {
  c_nthtoggle *obj = NEW(c_nthtoggle)();
  cloneSet(obj);
  return obj;
}
void c_nthtoggle::cloneSet(c_nthtoggle *clone) {
  clone->m__countMax = m__countMax.isReferenced() ? ref(m__countMax) : m__countMax;
  clone->m__count = m__count.isReferenced() ? ref(m__count) : m__count;
  c_toggle::cloneSet(clone);
}
Variant c_nthtoggle::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x78F762A45418F76CLL, activate) {
        return (t_activate(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x021A52B45A788597LL, value) {
        return (t_value());
      }
      break;
    default:
      break;
  }
  return c_toggle::o_invoke(s, params, hash, fatal);
}
Variant c_nthtoggle::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x78F762A45418F76CLL, activate) {
        return (t_activate(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x021A52B45A788597LL, value) {
        return (t_value());
      }
      break;
    default:
      break;
  }
  return c_toggle::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_nthtoggle::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_toggle::os_invoke(c, s, params, hash, fatal);
}
Variant cw_nthtoggle$os_get(const char *s) {
  return c_nthtoggle::os_get(s, -1);
}
Variant &cw_nthtoggle$os_lval(const char *s) {
  return c_nthtoggle::os_lval(s, -1);
}
Variant cw_nthtoggle$os_constant(const char *s) {
  return c_nthtoggle::os_constant(s);
}
Variant cw_nthtoggle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_nthtoggle::os_invoke(c, s, params, -1, fatal);
}
void c_nthtoggle::init() {
  c_toggle::init();
  m__countMax = null;
  m__count = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 23 */
void c_nthtoggle::t_nthtoggle(bool v_startState, int64 v_max) {
  INSTANCE_METHOD_INJECTION(NthToggle, NthToggle::NthToggle);
  bool oldInCtor = gasInCtor(true);
  LINE(24,c_toggle::t_toggle(v_startState));
  (m__countMax = v_max);
  (m__count = 0LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/objinst.php line 29 */
void c_nthtoggle::t_activate() {
  INSTANCE_METHOD_INJECTION(NthToggle, NthToggle::activate);
  m__count++;
  if (not_less(m__count, m__countMax)) {
    (m__state = !(toBoolean(m__state)));
    (m__count = 0LL);
  }
} /* function */
Object co_toggle(CArrRef params, bool init /* = true */) {
  return Object(p_toggle(NEW(c_toggle)())->dynCreate(params, init));
}
Object co_nthtoggle(CArrRef params, bool init /* = true */) {
  return Object(p_nthtoggle(NEW(c_nthtoggle)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$objinst_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/objinst.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$objinst_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_toggle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("toggle") : g->GV(toggle);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_ntoggle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ntoggle") : g->GV(ntoggle);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_toggle = ((Object)(LINE(41,p_toggle(p_toggle(NEWOBJ(c_toggle)())->create(true))))));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 5LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(43,v_toggle.o_invoke_few_args("activate", 0x78F762A45418F76CLL, 0));
        toBoolean(LINE(44,v_toggle.o_invoke_few_args("value", 0x021A52B45A788597LL, 0))) ? ((print("true\n"))) : ((print("false\n")));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(2);
      ((Object)(LINE(47,p_toggle(p_toggle(NEWOBJ(c_toggle)())->create(true)))));
    }
  }
  print("\n");
  (v_ntoggle = ((Object)(LINE(50,p_nthtoggle(p_nthtoggle(NEWOBJ(c_nthtoggle)())->create(true, 3LL))))));
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, 8LL); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        LINE(52,v_ntoggle.o_invoke_few_args("activate", 0x78F762A45418F76CLL, 0));
        toBoolean(LINE(53,v_ntoggle.o_invoke_few_args("value", 0x021A52B45A788597LL, 0))) ? ((print("true\n"))) : ((print("false\n")));
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(4);
      ((Object)(LINE(56,p_nthtoggle(p_nthtoggle(NEWOBJ(c_nthtoggle)())->create(true, 3LL)))));
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
