
#ifndef __GENERATED_cls_toggle_h__
#define __GENERATED_cls_toggle_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/methcall.php line 11 */
class c_toggle : virtual public ObjectData {
  BEGIN_CLASS_MAP(toggle)
  END_CLASS_MAP(toggle)
  DECLARE_CLASS(toggle, Toggle, ObjectData)
  void init();
  public: Variant m__state;
  public: void t_toggle(bool v_startState);
  public: ObjectData *create(bool v_startState);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_value();
  public: void t_activate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_toggle_h__
