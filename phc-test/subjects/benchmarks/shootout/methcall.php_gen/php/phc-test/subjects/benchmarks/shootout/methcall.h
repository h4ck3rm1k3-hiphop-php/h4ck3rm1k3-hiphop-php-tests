
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_methcall_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_methcall_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/methcall.fw.h>

// Declarations
#include <cls/toggle.h>
#include <cls/nthtoggle.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$methcall_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_toggle(CArrRef params, bool init = true);
Object co_nthtoggle(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_methcall_h__
