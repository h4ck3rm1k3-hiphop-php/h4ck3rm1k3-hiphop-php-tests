
#include <php/phc-test/subjects/benchmarks/shootout/lists.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$lists_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/lists.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$lists_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("size") : g->GV(size);
  Variant &v_L1Size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("L1Size") : g->GV(L1Size);
  Variant &v_L1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("L1") : g->GV(L1);
  Variant &v_L2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("L2") : g->GV(L2);
  Variant &v_L3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("L3") : g->GV(L3);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_size = 10000LL);
  (v_L1Size = 0LL);
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_n--)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_L1 = LINE(16,x_range(1LL, v_size)));
        (v_L2 = v_L1);
        (v_L3 = ScalarArrays::sa_[0]);
        LOOP_COUNTER(2);
        {
          while (toBoolean(v_L2)) {
            LOOP_COUNTER_CHECK(2);
            LINE(20,(assignCallTemp(eo_0, ref(v_L3)),assignCallTemp(eo_1, x_array_shift(ref(v_L2))),x_array_push(2, eo_0, eo_1)));
          }
        }
        LOOP_COUNTER(3);
        {
          while (toBoolean(v_L3)) {
            LOOP_COUNTER_CHECK(3);
            LINE(21,(assignCallTemp(eo_0, ref(v_L2)),assignCallTemp(eo_1, x_array_pop(ref(v_L3))),x_array_push(2, eo_0, eo_1)));
          }
        }
        (v_L1 = LINE(22,x_array_reverse(v_L1)));
        if (!equal(v_L1.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_size)) {
          print("First item of L1 != SIZE\n");
          break;
        }
        {
          LOOP_COUNTER(4);
          for ((v_i = 0LL); less(v_i, v_size); v_i++) {
            LOOP_COUNTER_CHECK(4);
            if (!equal(v_L1.rvalAt(v_i), v_L2.rvalAt(v_i))) {
              print("L1 != L2\n");
              goto break1;
            }
          }
        }
        (v_L1Size = LINE(30,x_sizeof(v_L1)));
        unset(v_L1);
        unset(v_L2);
        unset(v_L3);
      }
    }
    break1:;
  }
  print(toString(v_L1Size) + toString("\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
