
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      HASH_INDEX(0x1AC4E2BC656C9803LL, nl, 14);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x3FE8023BDF261C0FLL, size, 20);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 29:
      HASH_INDEX(0x66DE2313CE94825DLL, nc, 12);
      HASH_INDEX(0x7666772F8ADB251DLL, nw, 13);
      break;
    case 31:
      HASH_INDEX(0x480DFF255F94F8DFLL, words, 19);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      HASH_INDEX(0x277B6D36F75741AELL, pos, 18);
      break;
    case 48:
      HASH_INDEX(0x68512E4C61242170LL, hasSplitWord, 15);
      HASH_INDEX(0x5B0C0C547374D530LL, fp, 16);
      break;
    case 51:
      HASH_INDEX(0x037B2AC087556EF3LL, block, 17);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 21) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
