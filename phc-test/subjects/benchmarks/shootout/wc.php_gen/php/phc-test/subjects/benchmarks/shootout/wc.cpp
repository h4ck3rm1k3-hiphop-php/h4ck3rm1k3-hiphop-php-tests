
#include <php/phc-test/subjects/benchmarks/shootout/wc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$wc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/wc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$wc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_nc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nc") : g->GV(nc);
  Variant &v_nw __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nw") : g->GV(nw);
  Variant &v_nl __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nl") : g->GV(nl);
  Variant &v_hasSplitWord __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hasSplitWord") : g->GV(hasSplitWord);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_block __attribute__((__unused__)) = (variables != gVariables) ? variables->get("block") : g->GV(block);
  Variant &v_pos __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pos") : g->GV(pos);
  Variant &v_words __attribute__((__unused__)) = (variables != gVariables) ? variables->get("words") : g->GV(words);
  Variant &v_size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("size") : g->GV(size);

  (v_nl = (v_nw = (v_nc = 0LL)));
  (v_hasSplitWord = false);
  (v_fp = LINE(13,x_fopen("php://stdin", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_block = LINE(14,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        v_nc += LINE(16,x_strlen(toString(v_block)));
        (v_pos = 0LL);
        LOOP_COUNTER(2);
        {
          while (toBoolean((v_pos = LINE(19,x_strpos(toString(v_block), "\n", toInt32(v_pos + 1LL)))))) {
            LOOP_COUNTER_CHECK(2);
            v_nl++;
          }
        }
        (v_words = LINE(22,x_preg_split("/\\s+/", v_block)));
        if (toBoolean((v_size = LINE(24,x_sizeof(v_words))))) {
          v_nw += v_size;
          if (toBoolean(LINE(27,x_strlen(toString(v_words.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))))) {
            if (toBoolean(v_hasSplitWord)) v_nw--;
          }
          else {
            v_nw--;
          }
          (v_hasSplitWord = LINE(32,x_strlen(toString(v_words.rvalAt(v_size - 1LL)))));
          if (!(toBoolean(v_hasSplitWord))) v_nw--;
        }
        unset(v_words);
      }
    }
  }
  LINE(38,x_fclose(toObject(v_fp)));
  print(LINE(40,concat6(toString(v_nl), " ", toString(v_nw), " ", toString(v_nc), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
