
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_wc_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_wc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/wc.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$wc_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_wc_h__
