
#include <php/phc-test/subjects/benchmarks/shootout/pidigits.php-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php-2.php line 16 */
Array f_transformation_compose2(Variant v_y, Variant v_a) {
  FUNCTION_INJECTION(Transformation_Compose2);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  return (assignCallTemp(eo_0, LINE(19,invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, 10LL).set(1, ref(v_a.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_1, LINE(20,(assignCallTemp(eo_4, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, 10LL).set(1, ref(v_a.refvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_5, ref((assignCallTemp(eo_6, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, -10LL).set(1, ref(v_y)).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_7, ref(v_a.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))),invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, eo_6).set(1, eo_7).create()), 0x000000003052D2A3LL)))),invoke_failed("gmp_add", Array(ArrayInit(2).set(0, eo_4).set(1, eo_5).create()), 0x00000000AECAFC1DLL)))),assignCallTemp(eo_2, v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)),assignCallTemp(eo_3, v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)),Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php-2.php line 32 */
Variant f_transformation_next(Variant v_tr) {
  FUNCTION_INJECTION(Transformation_Next);
  v_tr.set(3LL, ((toInt64(toInt64(++lval(v_tr.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))) << 1LL) + 1LL), 0x135FDDF6A6BFBBDDLL);
  v_tr.set(1LL, (toInt64(toInt64(v_tr.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))) << 1LL), 0x5BCA7C69B794F8CELL);
  v_tr.set(2LL, (0LL), 0x486AFCC090D5F98CLL);
  return v_tr;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php-2.php line 25 */
Variant f_transformation_extract(Variant v_tr, Variant v_j) {
  FUNCTION_INJECTION(Transformation_Extract);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  return LINE(30,(assignCallTemp(eo_0, ref(LINE(28,(assignCallTemp(eo_2, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).set(1, ref(v_j)).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_3, ref(v_tr.refvalAt(1LL, 0x5BCA7C69B794F8CELL))),invoke_failed("gmp_add", Array(ArrayInit(2).set(0, eo_2).set(1, eo_3).create()), 0x00000000AECAFC1DLL))))),assignCallTemp(eo_1, ref(LINE(29,(assignCallTemp(eo_2, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(2LL, 0x486AFCC090D5F98CLL))).set(1, ref(v_j)).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_3, ref(v_tr.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))),invoke_failed("gmp_add", Array(ArrayInit(2).set(0, eo_2).set(1, eo_3).create()), 0x00000000AECAFC1DLL))))),invoke_failed("gmp_div_q", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x0000000027F41C93LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php-2.php line 39 */
String f_pidigit_next(Variant v_pd, Variant v_times) {
  FUNCTION_INJECTION(Pidigit_Next);
  Variant eo_0;
  Variant eo_1;
  String v_digits;
  Variant v_z;
  Variant v_y;

  (v_digits = "");
  (v_z = v_pd.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_y = LINE(45,f_transformation_extract(v_z, 3LL)));
        {
          LOOP_COUNTER(2);
          do {
            LOOP_COUNTER_CHECK(2);
            {
              (v_z = LINE(48,(assignCallTemp(eo_0, v_z),assignCallTemp(eo_1, f_transformation_next(ref(lval(v_pd.lvalAt(1LL, 0x5BCA7C69B794F8CELL))))),f_transformation_compose(eo_0, eo_1))));
              (v_y = LINE(49,f_transformation_extract(v_z, 3LL)));
            }
          } while (!equal(0LL, LINE(51,(assignCallTemp(eo_0, ref(f_transformation_extract(v_z, 4LL))),assignCallTemp(eo_1, ref(v_y)),invoke_failed("gmp_cmp", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x0000000047DE123ALL)))));
        }
        (v_z = LINE(52,f_transformation_compose2(v_y, v_z)));
        concat_assign(v_digits, toString(LINE(53,invoke_failed("gmp_strval", Array(ArrayInit(1).set(0, ref(v_y)).create()), 0x00000000F6DCC47DLL))));
      }
    } while (toBoolean(--v_times));
  }
  v_pd.set(0LL, (v_z), 0x77CFA1EEF01BCA90LL);
  return v_digits;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php-2.php line 7 */
Array f_transformation_compose(Variant v_tr, Variant v_a) {
  FUNCTION_INJECTION(Transformation_Compose);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  return (assignCallTemp(eo_0, LINE(10,invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).set(1, ref(v_a.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_1, LINE(11,(assignCallTemp(eo_4, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).set(1, ref(v_a.refvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_5, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(1LL, 0x5BCA7C69B794F8CELL))).set(1, ref(v_a.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))).create()), 0x000000003052D2A3LL))),invoke_failed("gmp_add", Array(ArrayInit(2).set(0, eo_4).set(1, eo_5).create()), 0x00000000AECAFC1DLL)))),assignCallTemp(eo_2, LINE(12,(assignCallTemp(eo_4, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(2LL, 0x486AFCC090D5F98CLL))).set(1, ref(v_a.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_5, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))).set(1, ref(v_a.refvalAt(2LL, 0x486AFCC090D5F98CLL))).create()), 0x000000003052D2A3LL))),invoke_failed("gmp_add", Array(ArrayInit(2).set(0, eo_4).set(1, eo_5).create()), 0x00000000AECAFC1DLL)))),assignCallTemp(eo_3, LINE(13,(assignCallTemp(eo_4, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(2LL, 0x486AFCC090D5F98CLL))).set(1, ref(v_a.refvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()), 0x000000003052D2A3LL))),assignCallTemp(eo_5, ref(invoke_failed("gmp_mul", Array(ArrayInit(2).set(0, ref(v_tr.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))).set(1, ref(v_a.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))).create()), 0x000000003052D2A3LL))),invoke_failed("gmp_add", Array(ArrayInit(2).set(0, eo_4).set(1, eo_5).create()), 0x00000000AECAFC1DLL)))),Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$pidigits_php_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/pidigits.php-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$pidigits_php_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_pidigit __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pidigit") : g->GV(pidigit);

  (v_n = toInt64(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  (v_i = 0LL);
  (v_pidigit = ScalarArrays::sa_[0]);
  LOOP_COUNTER(3);
  {
    while (toBoolean(v_n)) {
      LOOP_COUNTER_CHECK(3);
      {
        if (less(v_n, 10LL)) {
          LINE(68,(assignCallTemp(eo_1, f_pidigit_next(ref(v_pidigit), v_n)),assignCallTemp(eo_2, x_str_repeat(" ", toInt32(10LL - v_n))),assignCallTemp(eo_3, v_i + v_n),x_printf(4, "%s%s\t:%d\n", Array(ArrayInit(3).set(0, eo_1).set(1, eo_2).set(2, eo_3).create()))));
          break;
        }
        else {
          LINE(73,(assignCallTemp(eo_1, f_pidigit_next(ref(v_pidigit), 10LL)),assignCallTemp(eo_2, v_i += 10LL),x_printf(3, "%s\t:%d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
        }
        v_n -= 10LL;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
