
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_php_2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_php_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/pidigits.php-2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Array f_transformation_compose2(Variant v_y, Variant v_a);
Variant pm_php$phc_test$subjects$benchmarks$shootout$pidigits_php_2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_transformation_next(Variant v_tr);
Variant f_transformation_extract(Variant v_tr, Variant v_j);
String f_pidigit_next(Variant v_pd, Variant v_times);
Array f_transformation_compose(Variant v_tr, Variant v_a);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_php_2_h__
