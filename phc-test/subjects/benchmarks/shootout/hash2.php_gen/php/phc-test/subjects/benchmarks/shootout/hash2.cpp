
#include <php/phc-test/subjects/benchmarks/shootout/hash2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$hash2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/hash2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$hash2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_hash1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hash1") : g->GV(hash1);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);
  Variant &v_hash2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hash2") : g->GV(hash2);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      v_hash1.set(concat("foo_", toString(v_i)), (v_i));
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = v_n); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(2);
      {
        LOOP_COUNTER(3);
        for (ArrayIterPtr iter5 = v_hash1.begin(); !iter5->end(); iter5->next()) {
          LOOP_COUNTER_CHECK(3);
          v_value = iter5->second();
          v_key = iter5->first();
          if (isset(v_hash2, v_key)) {
            lval(v_hash2.lvalAt(v_key)) += v_value;
          }
          else {
            v_hash2.set(v_key, (v_value));
          }
        }
      }
    }
  }
  print(LINE(23,(assignCallTemp(eo_0, toString(v_hash1.rvalAt("foo_1", 0x7E5CB87B931387C5LL))),assignCallTemp(eo_2, concat6(toString(v_hash1.rvalAt("foo_9999", 0x0103EB5ECFA1F674LL)), " ", toString(v_hash2.rvalAt("foo_1", 0x7E5CB87B931387C5LL)), " ", toString(v_hash2.rvalAt("foo_9999", 0x0103EB5ECFA1F674LL)), "\n")),concat3(eo_0, " ", eo_2))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
