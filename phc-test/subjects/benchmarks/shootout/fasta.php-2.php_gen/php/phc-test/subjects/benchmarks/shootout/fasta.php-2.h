
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_fasta_php_2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_fasta_php_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/fasta.php-2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_makecumulative(Variant v_genelist);
Variant pm_php$phc_test$subjects$benchmarks$shootout$fasta_php_2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_selectrandom(Variant v_a);
Numeric f_gen_random(int64 v_max);
void f_makerepeatfasta(CStrRef v_id, CStrRef v_desc, CVarRef v_s, Numeric v_n);
void f_makerandomfasta(CStrRef v_id, CStrRef v_desc, Variant v_genelist, Numeric v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_fasta_php_2_h__
