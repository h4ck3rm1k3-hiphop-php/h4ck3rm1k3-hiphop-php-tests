
#include <php/phc-test/subjects/benchmarks/shootout/ary.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$ary_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/ary.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$ary_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      v_x.set(v_i, (v_i + 1LL));
    }
  }
  (v_y = LINE(14,x_array_pad(ScalarArrays::sa_[0], toInt32(v_n), 0LL)));
  {
    LOOP_COUNTER(2);
    for ((v_k = 0LL); less(v_k, 1000LL); v_k++) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_j = v_n);
        LOOP_COUNTER(3);
        {
          while (toBoolean(v_j--)) {
            LOOP_COUNTER_CHECK(3);
            lval(v_y.lvalAt(v_j)) += v_x.rvalAt(v_j);
          }
        }
      }
    }
  }
  LINE(20,x_printf(3, "%d %d\n", Array(ArrayInit(2).set(0, v_y.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_y.rvalAt(v_n - 1LL)).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
