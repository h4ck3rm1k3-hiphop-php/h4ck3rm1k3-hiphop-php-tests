
#include <php/phc-test/subjects/benchmarks/shootout/reversefile.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$reversefile_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/reversefile.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$reversefile_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_blocks __attribute__((__unused__)) = (variables != gVariables) ? variables->get("blocks") : g->GV(blocks);
  Variant &v_lines __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lines") : g->GV(lines);
  Variant &v_last __attribute__((__unused__)) = (variables != gVariables) ? variables->get("last") : g->GV(last);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_fp = LINE(13,x_fopen("php://stdin", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_b = LINE(14,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(1);
      v_blocks.append((v_b));
    }
  }
  LINE(15,x_fclose(toObject(v_fp)));
  (v_lines = LINE(17,(assignCallTemp(eo_1, x_implode("", v_blocks)),x_explode("\n", eo_1))));
  (v_last = LINE(18,x_count(v_lines)) - 1LL);
  if (equal(LINE(21,x_ord(toString(v_lines.rvalAt(v_last)))), 0LL)) v_last--;
  {
    LOOP_COUNTER(2);
    for ((v_i = v_last); not_less(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(toString(v_lines.rvalAt(v_i)));
        echo("\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
