
#ifndef __GENERATED_cls_body_h__
#define __GENERATED_cls_body_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 66 */
class c_body : virtual public ObjectData {
  BEGIN_CLASS_MAP(body)
  END_CLASS_MAP(body)
  DECLARE_CLASS(body, Body, ObjectData)
  void init();
  public: Variant m_x;
  public: Variant m_y;
  public: Variant m_z;
  public: Variant m_vx;
  public: Variant m_vy;
  public: Variant m_vz;
  public: Variant m_mass;
  public: static p_body ti_newbody(const char* cls, double v_x, double v_y, double v_z, double v_vx, double v_vy, double v_vz, double v_mass);
  public: static Variant ti_jupiter(const char* cls);
  public: static p_body ti_saturn(const char* cls);
  public: static p_body ti_uranus(const char* cls);
  public: static p_body ti_neptune(const char* cls);
  public: static p_body ti_sun(const char* cls);
  public: static void ti_offsetmomentum(const char* cls, Variant v_bodies);
  public: static void t_offsetmomentum(CVarRef v_bodies) { ti_offsetmomentum("body", v_bodies); }
  public: static p_body t_neptune() { return ti_neptune("body"); }
  public: static p_body t_newbody(double v_x, double v_y, double v_z, double v_vx, double v_vy, double v_vz, double v_mass) { return ti_newbody("body", v_x, v_y, v_z, v_vx, v_vy, v_vz, v_mass); }
  public: static p_body t_uranus() { return ti_uranus("body"); }
  public: static p_body t_sun() { return ti_sun("body"); }
  public: static Variant t_jupiter() { return ti_jupiter("body"); }
  public: static p_body t_saturn() { return ti_saturn("body"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_body_h__
