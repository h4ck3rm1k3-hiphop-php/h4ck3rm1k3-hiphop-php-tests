
#include <php/phc-test/subjects/benchmarks/shootout/nbody.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const double k_DAYS_PER_YEAR = 365.24000000000001;
const double k_PI = 3.1415926535897931;
const double k_SOLAR_MASS = 39.478417604357432;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 66 */
Variant c_body::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_body::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_body::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  props.push_back(NEW(ArrayElement)("y", m_y.isReferenced() ? ref(m_y) : m_y));
  props.push_back(NEW(ArrayElement)("z", m_z.isReferenced() ? ref(m_z) : m_z));
  props.push_back(NEW(ArrayElement)("vx", m_vx.isReferenced() ? ref(m_vx) : m_vx));
  props.push_back(NEW(ArrayElement)("vy", m_vy.isReferenced() ? ref(m_vy) : m_vy));
  props.push_back(NEW(ArrayElement)("vz", m_vz.isReferenced() ? ref(m_vz) : m_vz));
  props.push_back(NEW(ArrayElement)("mass", m_mass.isReferenced() ? ref(m_mass) : m_mass));
  c_ObjectData::o_get(props);
}
bool c_body::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_EXISTS_STRING(0x62A103F6518DE2B3LL, z, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    case 7:
      HASH_EXISTS_STRING(0x389AC79A7A842A27LL, vx, 2);
      break;
    case 10:
      HASH_EXISTS_STRING(0x4F56B733A4DFC78ALL, y, 1);
      HASH_EXISTS_STRING(0x4EEA4A17B9BDE58ALL, mass, 4);
      break;
    case 11:
      HASH_EXISTS_STRING(0x51B575986A4B460BLL, vy, 2);
      break;
    case 15:
      HASH_EXISTS_STRING(0x60F18BC46849C57FLL, vz, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_body::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    case 7:
      HASH_RETURN_STRING(0x389AC79A7A842A27LL, m_vx,
                         vx, 2);
      break;
    case 10:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      HASH_RETURN_STRING(0x4EEA4A17B9BDE58ALL, m_mass,
                         mass, 4);
      break;
    case 11:
      HASH_RETURN_STRING(0x51B575986A4B460BLL, m_vy,
                         vy, 2);
      break;
    case 15:
      HASH_RETURN_STRING(0x60F18BC46849C57FLL, m_vz,
                         vz, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_body::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_SET_STRING(0x62A103F6518DE2B3LL, m_z,
                      z, 1);
      break;
    case 6:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    case 7:
      HASH_SET_STRING(0x389AC79A7A842A27LL, m_vx,
                      vx, 2);
      break;
    case 10:
      HASH_SET_STRING(0x4F56B733A4DFC78ALL, m_y,
                      y, 1);
      HASH_SET_STRING(0x4EEA4A17B9BDE58ALL, m_mass,
                      mass, 4);
      break;
    case 11:
      HASH_SET_STRING(0x51B575986A4B460BLL, m_vy,
                      vy, 2);
      break;
    case 15:
      HASH_SET_STRING(0x60F18BC46849C57FLL, m_vz,
                      vz, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_body::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    case 7:
      HASH_RETURN_STRING(0x389AC79A7A842A27LL, m_vx,
                         vx, 2);
      break;
    case 10:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      HASH_RETURN_STRING(0x4EEA4A17B9BDE58ALL, m_mass,
                         mass, 4);
      break;
    case 11:
      HASH_RETURN_STRING(0x51B575986A4B460BLL, m_vy,
                         vy, 2);
      break;
    case 15:
      HASH_RETURN_STRING(0x60F18BC46849C57FLL, m_vz,
                         vz, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_body::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(body)
ObjectData *c_body::cloneImpl() {
  c_body *obj = NEW(c_body)();
  cloneSet(obj);
  return obj;
}
void c_body::cloneSet(c_body *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  clone->m_y = m_y.isReferenced() ? ref(m_y) : m_y;
  clone->m_z = m_z.isReferenced() ? ref(m_z) : m_z;
  clone->m_vx = m_vx.isReferenced() ? ref(m_vx) : m_vx;
  clone->m_vy = m_vy.isReferenced() ? ref(m_vy) : m_vy;
  clone->m_vz = m_vz.isReferenced() ? ref(m_vz) : m_vz;
  clone->m_mass = m_mass.isReferenced() ? ref(m_mass) : m_mass;
  ObjectData::cloneSet(clone);
}
Variant c_body::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_body::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_body::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_body$os_get(const char *s) {
  return c_body::os_get(s, -1);
}
Variant &cw_body$os_lval(const char *s) {
  return c_body::os_lval(s, -1);
}
Variant cw_body$os_constant(const char *s) {
  return c_body::os_constant(s);
}
Variant cw_body$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_body::os_invoke(c, s, params, -1, fatal);
}
void c_body::init() {
  m_x = null;
  m_y = null;
  m_z = null;
  m_vx = null;
  m_vy = null;
  m_vz = null;
  m_mass = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 69 */
p_body c_body::ti_newbody(const char* cls, double v_x, double v_y, double v_z, double v_vx, double v_vy, double v_vz, double v_mass) {
  STATIC_METHOD_INJECTION(Body, Body::NewBody);
  p_body v_b;

  ((Object)((v_b = ((Object)(LINE(70,p_body(p_body(NEWOBJ(c_body)())->create())))))));
  (v_b->m_x = v_x);
  (v_b->m_y = v_y);
  (v_b->m_z = v_z);
  (v_b->m_vx = v_vx);
  (v_b->m_vy = v_vy);
  (v_b->m_vz = v_vz);
  (v_b->m_mass = v_mass);
  return ((Object)(v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 81 */
Variant c_body::ti_jupiter(const char* cls) {
  STATIC_METHOD_INJECTION(Body, Body::Jupiter);
  return ((Object)(LINE(90,c_body::t_newbody(4.8414314424647209, -1.1603200440274284, -0.10362204447112311, 0.60632639299583202, 2.8119868449162602, -0.025218361659887629, 0.037693674870389493))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 93 */
p_body c_body::ti_saturn(const char* cls) {
  STATIC_METHOD_INJECTION(Body, Body::Saturn);
  return ((Object)(LINE(102,c_body::t_newbody(8.3433667182445799, 4.1247985641243048, -0.40352341711432138, -1.0107743461787924, 1.8256623712304119, 0.0084157613765841535, 0.011286326131968767))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 105 */
p_body c_body::ti_uranus(const char* cls) {
  STATIC_METHOD_INJECTION(Body, Body::Uranus);
  return ((Object)(LINE(114,c_body::t_newbody(12.894369562139131, -15.111151401698631, -0.22330757889265573, 1.0827910064415354, 0.86871301816960822, -0.010832637401363636, 0.0017237240570597112))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 117 */
p_body c_body::ti_neptune(const char* cls) {
  STATIC_METHOD_INJECTION(Body, Body::Neptune);
  return ((Object)(LINE(126,c_body::t_newbody(15.379697114850917, -25.919314609987964, 0.17925877295037118, 0.97909073224389798, 0.59469899864767617, -0.034755955504078104, 0.0020336868699246304))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 129 */
p_body c_body::ti_sun(const char* cls) {
  STATIC_METHOD_INJECTION(Body, Body::Sun);
  return ((Object)(LINE(130,c_body::t_newbody(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 39.478417604357432 /* SOLAR_MASS */))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 133 */
void c_body::ti_offsetmomentum(const char* cls, Variant v_bodies) {
  STATIC_METHOD_INJECTION(Body, Body::OffsetMomentum);
  Numeric v_px = 0;
  Numeric v_py = 0;
  Numeric v_pz = 0;
  Variant v_each;

  (v_px = 0.0);
  (v_py = 0.0);
  (v_pz = 0.0);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_bodies.begin("body"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_each = iter3->second();
      {
        v_px += v_each.o_get("vx", 0x389AC79A7A842A27LL) * v_each.o_get("mass", 0x4EEA4A17B9BDE58ALL);
        v_py += v_each.o_get("vy", 0x51B575986A4B460BLL) * v_each.o_get("mass", 0x4EEA4A17B9BDE58ALL);
        v_pz += v_each.o_get("vz", 0x60F18BC46849C57FLL) * v_each.o_get("mass", 0x4EEA4A17B9BDE58ALL);
      }
    }
  }
  (lval(v_bodies.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("vx", 0x389AC79A7A842A27LL) = divide(negate(v_px), 39.478417604357432 /* SOLAR_MASS */));
  (lval(v_bodies.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("vy", 0x51B575986A4B460BLL) = divide(negate(v_py), 39.478417604357432 /* SOLAR_MASS */));
  (lval(v_bodies.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("vz", 0x60F18BC46849C57FLL) = divide(negate(v_pz), 39.478417604357432 /* SOLAR_MASS */));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 40 */
Numeric f_energy(Variant v_bodies) {
  FUNCTION_INJECTION(Energy);
  int v_m = 0;
  Numeric v_e = 0;
  int64 v_i = 0;
  int64 v_j = 0;
  Numeric v_dx = 0;
  Numeric v_dy = 0;
  Numeric v_dz = 0;
  double v_distance = 0.0;

  (v_m = LINE(41,x_sizeof(v_bodies)));
  (v_e = 0.0);
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_m); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        v_e += 0.5 * toDouble(v_bodies.rvalAt(v_i).o_get("mass", 0x4EEA4A17B9BDE58ALL)) * toDouble((v_bodies.rvalAt(v_i).o_get("vx", 0x389AC79A7A842A27LL) * v_bodies.rvalAt(v_i).o_get("vx", 0x389AC79A7A842A27LL) + v_bodies.rvalAt(v_i).o_get("vy", 0x51B575986A4B460BLL) * v_bodies.rvalAt(v_i).o_get("vy", 0x51B575986A4B460BLL) + v_bodies.rvalAt(v_i).o_get("vz", 0x60F18BC46849C57FLL) * v_bodies.rvalAt(v_i).o_get("vz", 0x60F18BC46849C57FLL)));
        {
          LOOP_COUNTER(5);
          for ((v_j = v_i + 1LL); less(v_j, v_m); v_j++) {
            LOOP_COUNTER_CHECK(5);
            {
              (v_dx = v_bodies.rvalAt(v_i).o_get("x", 0x04BFC205E59FA416LL) - v_bodies.rvalAt(v_j).o_get("x", 0x04BFC205E59FA416LL));
              (v_dy = v_bodies.rvalAt(v_i).o_get("y", 0x4F56B733A4DFC78ALL) - v_bodies.rvalAt(v_j).o_get("y", 0x4F56B733A4DFC78ALL));
              (v_dz = v_bodies.rvalAt(v_i).o_get("z", 0x62A103F6518DE2B3LL) - v_bodies.rvalAt(v_j).o_get("z", 0x62A103F6518DE2B3LL));
              (v_distance = LINE(54,x_sqrt(toDouble(v_dx * v_dx + v_dy * v_dy + v_dz * v_dz))));
              v_e -= divide((v_bodies.rvalAt(v_i).o_get("mass", 0x4EEA4A17B9BDE58ALL) * v_bodies.rvalAt(v_j).o_get("mass", 0x4EEA4A17B9BDE58ALL)), v_distance);
            }
          }
        }
      }
    }
  }
  return v_e;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/nbody.php line 11 */
void f_advance(Variant v_bodies, double v_dt) {
  FUNCTION_INJECTION(Advance);
  int v_m = 0;
  int64 v_i = 0;
  int64 v_j = 0;
  Numeric v_dx = 0;
  Numeric v_dy = 0;
  Numeric v_dz = 0;
  double v_distance = 0.0;
  Numeric v_mag = 0;

  (v_m = LINE(12,x_sizeof(v_bodies)));
  {
    LOOP_COUNTER(6);
    for ((v_i = 0LL); less(v_i, v_m); v_i++) {
      LOOP_COUNTER_CHECK(6);
      {
        {
          LOOP_COUNTER(7);
          for ((v_j = v_i + 1LL); less(v_j, v_m); v_j++) {
            LOOP_COUNTER_CHECK(7);
            {
              (v_dx = v_bodies.rvalAt(v_i).o_get("x", 0x04BFC205E59FA416LL) - v_bodies.rvalAt(v_j).o_get("x", 0x04BFC205E59FA416LL));
              (v_dy = v_bodies.rvalAt(v_i).o_get("y", 0x4F56B733A4DFC78ALL) - v_bodies.rvalAt(v_j).o_get("y", 0x4F56B733A4DFC78ALL));
              (v_dz = v_bodies.rvalAt(v_i).o_get("z", 0x62A103F6518DE2B3LL) - v_bodies.rvalAt(v_j).o_get("z", 0x62A103F6518DE2B3LL));
              (v_distance = LINE(19,x_sqrt(toDouble(v_dx * v_dx + v_dy * v_dy + v_dz * v_dz))));
              (v_mag = divide(v_dt, (v_distance * v_distance * v_distance)));
              lval(v_bodies.lvalAt(v_i)).o_lval("vx", 0x389AC79A7A842A27LL) -= v_dx * v_bodies.rvalAt(v_j).o_get("mass", 0x4EEA4A17B9BDE58ALL) * v_mag;
              lval(v_bodies.lvalAt(v_i)).o_lval("vy", 0x51B575986A4B460BLL) -= v_dy * v_bodies.rvalAt(v_j).o_get("mass", 0x4EEA4A17B9BDE58ALL) * v_mag;
              lval(v_bodies.lvalAt(v_i)).o_lval("vz", 0x60F18BC46849C57FLL) -= v_dz * v_bodies.rvalAt(v_j).o_get("mass", 0x4EEA4A17B9BDE58ALL) * v_mag;
              lval(v_bodies.lvalAt(v_j)).o_lval("vx", 0x389AC79A7A842A27LL) += v_dx * v_bodies.rvalAt(v_i).o_get("mass", 0x4EEA4A17B9BDE58ALL) * v_mag;
              lval(v_bodies.lvalAt(v_j)).o_lval("vy", 0x51B575986A4B460BLL) += v_dy * v_bodies.rvalAt(v_i).o_get("mass", 0x4EEA4A17B9BDE58ALL) * v_mag;
              lval(v_bodies.lvalAt(v_j)).o_lval("vz", 0x60F18BC46849C57FLL) += v_dz * v_bodies.rvalAt(v_i).o_get("mass", 0x4EEA4A17B9BDE58ALL) * v_mag;
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(8);
    for ((v_i = 0LL); less(v_i, v_m); v_i++) {
      LOOP_COUNTER_CHECK(8);
      {
        lval(v_bodies.lvalAt(v_i)).o_lval("x", 0x04BFC205E59FA416LL) += v_dt * toDouble(v_bodies.rvalAt(v_i).o_get("vx", 0x389AC79A7A842A27LL));
        lval(v_bodies.lvalAt(v_i)).o_lval("y", 0x4F56B733A4DFC78ALL) += v_dt * toDouble(v_bodies.rvalAt(v_i).o_get("vy", 0x51B575986A4B460BLL));
        lval(v_bodies.lvalAt(v_i)).o_lval("z", 0x62A103F6518DE2B3LL) += v_dt * toDouble(v_bodies.rvalAt(v_i).o_get("vz", 0x60F18BC46849C57FLL));
      }
    }
  }
} /* function */
Object co_body(CArrRef params, bool init /* = true */) {
  return Object(p_body(NEW(c_body)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$nbody_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/nbody.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$nbody_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bodies __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bodies") : g->GV(bodies);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  ;
  ;
  ;
  (v_bodies = (assignCallTemp(eo_0, ((Object)(LINE(149,c_body::t_sun())))),assignCallTemp(eo_1, c_body::t_jupiter()),assignCallTemp(eo_2, ((Object)(LINE(150,c_body::t_saturn())))),assignCallTemp(eo_3, ((Object)(c_body::t_uranus()))),assignCallTemp(eo_4, ((Object)(c_body::t_neptune()))),Array(ArrayInit(5).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, eo_4).create())));
  LINE(151,c_body::t_offsetmomentum(ref(v_bodies)));
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  LINE(154,(assignCallTemp(eo_1, f_energy(ref(v_bodies))),x_printf(2, "%0.9f\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  {
    LOOP_COUNTER(9);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(9);
      {
        LINE(155,f_advance(ref(v_bodies), 0.01));
      }
    }
  }
  LINE(156,(assignCallTemp(eo_1, f_energy(ref(v_bodies))),x_printf(2, "%0.9f\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
