
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_nbody_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_nbody_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/nbody.fw.h>

// Declarations
#include <cls/body.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Numeric f_energy(Variant v_bodies);
Variant pm_php$phc_test$subjects$benchmarks$shootout$nbody_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_advance(Variant v_bodies, double v_dt);
Object co_body(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_nbody_h__
