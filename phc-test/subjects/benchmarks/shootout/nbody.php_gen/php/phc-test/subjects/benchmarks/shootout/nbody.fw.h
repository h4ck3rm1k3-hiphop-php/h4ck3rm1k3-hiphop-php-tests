
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_nbody_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_nbody_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const double k_DAYS_PER_YEAR;
extern const double k_PI;
extern const double k_SOLAR_MASS;


// 2. Classes
FORWARD_DECLARE_CLASS(body)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_nbody_fw_h__
