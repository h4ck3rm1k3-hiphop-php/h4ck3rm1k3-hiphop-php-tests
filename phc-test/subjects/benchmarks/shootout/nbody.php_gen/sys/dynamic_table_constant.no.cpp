
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const double k_DAYS_PER_YEAR;
extern const double k_PI;
extern const double k_SOLAR_MASS;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x28CE1E905A3FC3F8LL, k_SOLAR_MASS, SOLAR_MASS);
      break;
    case 3:
      HASH_RETURN(0x04F07DA1C07C8673LL, k_DAYS_PER_YEAR, DAYS_PER_YEAR);
      break;
    case 4:
      HASH_RETURN(0x18CF3E4A60E4AAACLL, k_PI, PI);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
