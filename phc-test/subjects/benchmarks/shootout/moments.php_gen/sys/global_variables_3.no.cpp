
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 4:
      HASH_INDEX(0x44C89639E13FB684LL, dev, 20);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x7E8D9B48AC47F186LL, variance, 18);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x117B8667E4662809LL, n, 13);
      break;
    case 12:
      HASH_INDEX(0x5F45ED741BEBEC4CLL, adev, 19);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 18:
      HASH_INDEX(0x6B302D735D1D9812LL, sdev, 23);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 23:
      HASH_INDEX(0x69E7B5A62F6DBC17LL, sum, 14);
      HASH_INDEX(0x607D9FF5C7EC2297LL, mid, 24);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 15);
      break;
    case 25:
      HASH_INDEX(0x0A0059908346FD19LL, mean, 21);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 39:
      HASH_INDEX(0x629FE09B293BB567LL, numbers, 12);
      break;
    case 40:
      HASH_INDEX(0x49CDCE31CE3528A8LL, dev2, 22);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 52:
      HASH_INDEX(0x60787DC87DC6FC34LL, kurtosis, 16);
      break;
    case 53:
      HASH_INDEX(0x1D1AD95464A098F5LL, median, 25);
      break;
    case 60:
      HASH_INDEX(0x64EA0CB3B40EEF3CLL, skew, 17);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 26) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
