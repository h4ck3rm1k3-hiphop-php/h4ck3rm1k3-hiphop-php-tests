
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 4:
      HASH_INITIALIZED(0x44C89639E13FB684LL, g->GV(dev),
                       dev);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      HASH_INITIALIZED(0x7E8D9B48AC47F186LL, g->GV(variance),
                       variance);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x117B8667E4662809LL, g->GV(n),
                       n);
      break;
    case 12:
      HASH_INITIALIZED(0x5F45ED741BEBEC4CLL, g->GV(adev),
                       adev);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 18:
      HASH_INITIALIZED(0x6B302D735D1D9812LL, g->GV(sdev),
                       sdev);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 23:
      HASH_INITIALIZED(0x69E7B5A62F6DBC17LL, g->GV(sum),
                       sum);
      HASH_INITIALIZED(0x607D9FF5C7EC2297LL, g->GV(mid),
                       mid);
      break;
    case 24:
      HASH_INITIALIZED(0x0EB22EDA95766E98LL, g->GV(i),
                       i);
      break;
    case 25:
      HASH_INITIALIZED(0x0A0059908346FD19LL, g->GV(mean),
                       mean);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 39:
      HASH_INITIALIZED(0x629FE09B293BB567LL, g->GV(numbers),
                       numbers);
      break;
    case 40:
      HASH_INITIALIZED(0x49CDCE31CE3528A8LL, g->GV(dev2),
                       dev2);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 52:
      HASH_INITIALIZED(0x60787DC87DC6FC34LL, g->GV(kurtosis),
                       kurtosis);
      break;
    case 53:
      HASH_INITIALIZED(0x1D1AD95464A098F5LL, g->GV(median),
                       median);
      break;
    case 60:
      HASH_INITIALIZED(0x64EA0CB3B40EEF3CLL, g->GV(skew),
                       skew);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
