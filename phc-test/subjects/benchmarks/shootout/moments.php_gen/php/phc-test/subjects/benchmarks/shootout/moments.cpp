
#include <php/phc-test/subjects/benchmarks/shootout/moments.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$moments_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/moments.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$moments_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_numbers __attribute__((__unused__)) = (variables != gVariables) ? variables->get("numbers") : g->GV(numbers);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_sum __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sum") : g->GV(sum);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_kurtosis __attribute__((__unused__)) = (variables != gVariables) ? variables->get("kurtosis") : g->GV(kurtosis);
  Variant &v_skew __attribute__((__unused__)) = (variables != gVariables) ? variables->get("skew") : g->GV(skew);
  Variant &v_variance __attribute__((__unused__)) = (variables != gVariables) ? variables->get("variance") : g->GV(variance);
  Variant &v_adev __attribute__((__unused__)) = (variables != gVariables) ? variables->get("adev") : g->GV(adev);
  Variant &v_dev __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dev") : g->GV(dev);
  Variant &v_mean __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mean") : g->GV(mean);
  Variant &v_dev2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dev2") : g->GV(dev2);
  Variant &v_sdev __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sdev") : g->GV(sdev);
  Variant &v_mid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mid") : g->GV(mid);
  Variant &v_median __attribute__((__unused__)) = (variables != gVariables) ? variables->get("median") : g->GV(median);

  (v_numbers = LINE(12,x_file("php://stdin")));
  (v_n = LINE(14,x_sizeof(v_numbers)));
  (v_sum = 0.0);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_numbers.set(v_i, (LINE(17,x_doubleval(v_numbers.rvalAt(v_i)))));
        v_sum += v_numbers.rvalAt(v_i);
      }
    }
  }
  (v_dev = (v_adev = (v_variance = (v_skew = (v_kurtosis = 0.0)))));
  (v_mean = divide(v_sum, v_n));
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_dev = v_numbers.rvalAt(v_i) - v_mean);
        v_adev += LINE(26,x_abs(v_dev));
        v_variance += ((v_dev2 = v_dev * v_dev));
        v_skew += v_dev2 * v_dev;
        v_kurtosis += v_dev2 * v_dev2;
      }
    }
  }
  v_adev /= v_n;
  v_variance /= v_n - 1LL;
  (v_sdev = LINE(34,x_sqrt(toDouble(v_variance))));
  if (!equal(v_variance, 0.0)) {
    v_skew /= v_n * v_variance * v_sdev;
    (v_kurtosis = toDouble(divide(v_kurtosis, (v_n * v_variance * v_variance))) - 3.0);
  }
  LINE(41,x_sort(ref(v_numbers)));
  (v_mid = divide(v_n, 2LL));
  (v_median = (!equal(modulo(toInt64(v_n), 2LL), 0LL)) ? ((Variant)(v_numbers.rvalAt(v_mid))) : ((Variant)(divide((v_numbers.rvalAt(v_mid) + v_numbers.rvalAt(v_mid - 1LL)), 2.0))));
  LINE(46,x_printf(2, "n:                  %d\n", Array(ArrayInit(1).set(0, v_n).create())));
  LINE(47,x_printf(2, "median:             %0.6f\n", Array(ArrayInit(1).set(0, v_median).create())));
  LINE(48,x_printf(2, "mean:               %0.6f\n", Array(ArrayInit(1).set(0, v_mean).create())));
  LINE(49,x_printf(2, "average_deviation:  %0.6f\n", Array(ArrayInit(1).set(0, v_adev).create())));
  LINE(50,x_printf(2, "standard_deviation: %0.6f\n", Array(ArrayInit(1).set(0, v_sdev).create())));
  LINE(51,x_printf(2, "variance:           %0.6f\n", Array(ArrayInit(1).set(0, v_variance).create())));
  LINE(52,x_printf(2, "skew:               %0.6f\n", Array(ArrayInit(1).set(0, v_skew).create())));
  LINE(53,x_printf(2, "kurtosis:           %0.6f\n", Array(ArrayInit(1).set(0, v_kurtosis).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
