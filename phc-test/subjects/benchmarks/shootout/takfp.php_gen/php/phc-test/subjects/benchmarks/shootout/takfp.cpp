
#include <php/phc-test/subjects/benchmarks/shootout/takfp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/takfp.php line 11 */
Variant f_takfp(CVarRef v_x, CVarRef v_y, CVarRef v_z) {
  FUNCTION_INJECTION(Takfp);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  if (not_less(v_y, v_x)) {
    return v_z;
  }
  else {
    return LINE(16,(assignCallTemp(eo_0, f_takfp(toDouble(v_x) - 1.0, v_y, v_z)),assignCallTemp(eo_1, f_takfp(toDouble(v_y) - 1.0, v_z, v_x)),assignCallTemp(eo_2, f_takfp(toDouble(v_z) - 1.0, v_x, v_y)),f_takfp(eo_0, eo_1, eo_2)));
  }
  return null;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$takfp_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/takfp.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$takfp_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  LINE(21,(assignCallTemp(eo_1, f_takfp(toDouble(v_n) * 3.0, toDouble(v_n) * 2.0, toDouble(v_n) * 1.0)),x_printf(2, "%.1f\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
