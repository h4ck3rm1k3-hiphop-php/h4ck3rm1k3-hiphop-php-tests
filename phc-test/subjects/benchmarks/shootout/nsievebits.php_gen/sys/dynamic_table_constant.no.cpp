
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_FLAG_MAX;
extern const Variant k_BPC;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x5D3B7C4CCBA56D30LL, k_BPC, BPC);
      break;
    case 1:
      HASH_RETURN(0x004EDA0807A9A3B1LL, k_FLAG_MAX, FLAG_MAX);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
