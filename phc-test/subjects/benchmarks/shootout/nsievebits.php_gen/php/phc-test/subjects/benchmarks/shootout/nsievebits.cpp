
#include <php/phc-test/subjects/benchmarks/shootout/nsievebits.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_FLAG_MAX = "FLAG_MAX";
const Variant k_BPC = "BPC";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/nsievebits.php line 11 */
void f_primes(Numeric v_n) {
  FUNCTION_INJECTION(primes);
  Numeric v_size = 0;
  Variant v_flags;
  int64 v_count = 0;
  int64 v_i = 0;
  int64 v_prime = 0;
  Numeric v_offset = 0;
  Numeric v_mask = 0;

  (v_size = 10000LL * LINE(12,x_pow(2LL, v_n)));
  (v_flags = ScalarArrays::sa_[0]);
  (v_count = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, (divide(v_size, (Variant)(k_BPC)) + 1LL)); v_i++) {
      LOOP_COUNTER_CHECK(1);
      v_flags.set(v_i, (k_FLAG_MAX));
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_prime = 2LL); less(v_prime, v_size); v_prime++) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_offset = divide(v_prime, (Variant)(k_BPC)));
        (v_mask = LINE(20,x_pow(2LL, modulo(v_prime, toInt64(k_BPC)))));
        if (!equal((bitwise_and(v_flags.rvalAt(v_offset), v_mask)), 0LL)) {
          v_count++;
          {
            LOOP_COUNTER(3);
            for ((v_i = (v_prime * 2LL)); not_more(v_i, v_size); v_i += v_prime) {
              LOOP_COUNTER_CHECK(3);
              {
                (v_offset = divide(v_i, (Variant)(k_BPC)));
                (v_mask = LINE(25,x_pow(2LL, modulo(v_i, toInt64(k_BPC)))));
                if (!equal((bitwise_and(v_flags.rvalAt(v_offset), v_mask)), 0LL)) lval(v_flags.lvalAt(v_offset)) ^= v_mask;
              }
            }
          }
        }
      }
    }
  }
  LINE(32,x_printf(3, "Primes up to %8d %8d\n", Array(ArrayInit(2).set(0, v_size).set(1, v_count).create())));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$nsievebits_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/nsievebits.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$nsievebits_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  LINE(8,throw_fatal("bad define"));
  LINE(9,throw_fatal("bad define"));
  (v_n = (!equal(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "")) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, 3LL); v_i++) {
      LOOP_COUNTER_CHECK(4);
      LINE(36,f_primes(v_n - v_i));
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
