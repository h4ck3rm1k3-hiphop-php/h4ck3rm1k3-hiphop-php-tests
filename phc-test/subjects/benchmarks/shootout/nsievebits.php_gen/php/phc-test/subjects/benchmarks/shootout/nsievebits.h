
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsievebits_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsievebits_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/nsievebits.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$nsievebits_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_primes(Numeric v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsievebits_h__
