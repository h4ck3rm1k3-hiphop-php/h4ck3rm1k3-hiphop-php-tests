
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_ackermann_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_ackermann_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/ackermann.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$ackermann_php(bool incOnce = false, LVariableTable* variables = NULL);
Numeric f_ack(Numeric v_m, CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_ackermann_h__
