
#include <php/phc-test/subjects/benchmarks/shootout/pidigits.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 11 */
Variant c_transformation::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_transformation::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_transformation::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("q", m_q.isReferenced() ? ref(m_q) : m_q));
  props.push_back(NEW(ArrayElement)("r", m_r.isReferenced() ? ref(m_r) : m_r));
  props.push_back(NEW(ArrayElement)("s", m_s.isReferenced() ? ref(m_s) : m_s));
  props.push_back(NEW(ArrayElement)("t", m_t.isReferenced() ? ref(m_t) : m_t));
  props.push_back(NEW(ArrayElement)("k", m_k.isReferenced() ? ref(m_k) : m_k));
  c_ObjectData::o_get(props);
}
bool c_transformation::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x0BC2CDE4BBFC9C10LL, s, 1);
      break;
    case 3:
      HASH_EXISTS_STRING(0x05CAFC91A9B37763LL, q, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x6ECE84440D42ECF6LL, r, 1);
      break;
    case 12:
      HASH_EXISTS_STRING(0x11C9D2391242AEBCLL, t, 1);
      break;
    case 15:
      HASH_EXISTS_STRING(0x0D6857FDDD7F21CFLL, k, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_transformation::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                         s, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x05CAFC91A9B37763LL, m_q,
                         q, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x6ECE84440D42ECF6LL, m_r,
                         r, 1);
      break;
    case 12:
      HASH_RETURN_STRING(0x11C9D2391242AEBCLL, m_t,
                         t, 1);
      break;
    case 15:
      HASH_RETURN_STRING(0x0D6857FDDD7F21CFLL, m_k,
                         k, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_transformation::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                      s, 1);
      break;
    case 3:
      HASH_SET_STRING(0x05CAFC91A9B37763LL, m_q,
                      q, 1);
      break;
    case 6:
      HASH_SET_STRING(0x6ECE84440D42ECF6LL, m_r,
                      r, 1);
      break;
    case 12:
      HASH_SET_STRING(0x11C9D2391242AEBCLL, m_t,
                      t, 1);
      break;
    case 15:
      HASH_SET_STRING(0x0D6857FDDD7F21CFLL, m_k,
                      k, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_transformation::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                         s, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x05CAFC91A9B37763LL, m_q,
                         q, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x6ECE84440D42ECF6LL, m_r,
                         r, 1);
      break;
    case 12:
      HASH_RETURN_STRING(0x11C9D2391242AEBCLL, m_t,
                         t, 1);
      break;
    case 15:
      HASH_RETURN_STRING(0x0D6857FDDD7F21CFLL, m_k,
                         k, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_transformation::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(transformation)
ObjectData *c_transformation::create(CStrRef v_q, CStrRef v_r, CStrRef v_s, CStrRef v_t) {
  init();
  t_transformation(v_q, v_r, v_s, v_t);
  return this;
}
ObjectData *c_transformation::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
ObjectData *c_transformation::cloneImpl() {
  c_transformation *obj = NEW(c_transformation)();
  cloneSet(obj);
  return obj;
}
void c_transformation::cloneSet(c_transformation *clone) {
  clone->m_q = m_q.isReferenced() ? ref(m_q) : m_q;
  clone->m_r = m_r.isReferenced() ? ref(m_r) : m_r;
  clone->m_s = m_s.isReferenced() ? ref(m_s) : m_s;
  clone->m_t = m_t.isReferenced() ? ref(m_t) : m_t;
  clone->m_k = m_k.isReferenced() ? ref(m_k) : m_k;
  ObjectData::cloneSet(clone);
}
Variant c_transformation::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x014BD9A6823256F0LL, extract) {
        return (t_extract(params.rvalAt(0)));
      }
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 6:
      HASH_GUARD(0x391960AD87E99C66LL, compose) {
        return (t_compose(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_transformation::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x014BD9A6823256F0LL, extract) {
        return (t_extract(a0));
      }
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 6:
      HASH_GUARD(0x391960AD87E99C66LL, compose) {
        return (t_compose(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_transformation::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_transformation$os_get(const char *s) {
  return c_transformation::os_get(s, -1);
}
Variant &cw_transformation$os_lval(const char *s) {
  return c_transformation::os_lval(s, -1);
}
Variant cw_transformation$os_constant(const char *s) {
  return c_transformation::os_constant(s);
}
Variant cw_transformation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_transformation::os_invoke(c, s, params, -1, fatal);
}
void c_transformation::init() {
  m_q = null;
  m_r = null;
  m_s = null;
  m_t = null;
  m_k = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 14 */
void c_transformation::t_transformation(CStrRef v_q, CStrRef v_r, CStrRef v_s, CStrRef v_t) {
  INSTANCE_METHOD_INJECTION(Transformation, Transformation::Transformation);
  bool oldInCtor = gasInCtor(true);
  (m_q = v_q);
  (m_r = v_r);
  (m_s = v_s);
  (m_t = v_t);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 21 */
p_transformation c_transformation::ti_unity(const char* cls) {
  STATIC_METHOD_INJECTION(Transformation, Transformation::Unity);
  return ((Object)(LINE(22,p_transformation(p_transformation(NEWOBJ(c_transformation)())->create("1", "0", "0", "1")))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 25 */
p_transformation c_transformation::ti_zero(const char* cls) {
  STATIC_METHOD_INJECTION(Transformation, Transformation::Zero);
  return ((Object)(LINE(26,p_transformation(p_transformation(NEWOBJ(c_transformation)())->create("0", "0", "0", "0")))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 30 */
p_transformation c_transformation::t_compose(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(Transformation, Transformation::Compose);
  Variant eo_0;
  Variant eo_1;
  String v_qq;
  String v_qrrt;
  String v_sqts;
  String v_srtt;

  (v_qq = LINE(31,x_bcmul(toString(m_q), toString(toObject(v_a).o_get("q", 0x05CAFC91A9B37763LL)))));
  (v_qrrt = LINE(32,(assignCallTemp(eo_0, x_bcmul(toString(m_q), toString(toObject(v_a).o_get("r", 0x6ECE84440D42ECF6LL)))),assignCallTemp(eo_1, x_bcmul(toString(m_r), toString(toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL)))),x_bcadd(eo_0, eo_1))));
  (v_sqts = LINE(33,(assignCallTemp(eo_0, x_bcmul(toString(m_s), toString(toObject(v_a).o_get("q", 0x05CAFC91A9B37763LL)))),assignCallTemp(eo_1, x_bcmul(toString(m_t), toString(toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL)))),x_bcadd(eo_0, eo_1))));
  (v_srtt = LINE(34,(assignCallTemp(eo_0, x_bcmul(toString(m_s), toString(toObject(v_a).o_get("r", 0x6ECE84440D42ECF6LL)))),assignCallTemp(eo_1, x_bcmul(toString(m_t), toString(toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL)))),x_bcadd(eo_0, eo_1))));
  return ((Object)(LINE(35,p_transformation(p_transformation(NEWOBJ(c_transformation)())->create(v_qq, v_qrrt, v_sqts, v_srtt)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 38 */
double c_transformation::t_extract(CVarRef v_j) {
  INSTANCE_METHOD_INJECTION(Transformation, Transformation::Extract);
  Variant eo_0;
  Variant eo_1;
  String v_bigj;
  String v_qjr;
  String v_sjt;
  String v_d;

  (v_bigj = LINE(39,x_strval(v_j)));
  (v_qjr = LINE(40,(assignCallTemp(eo_0, x_bcmul(toString(m_q), v_bigj)),assignCallTemp(eo_1, toString(m_r)),x_bcadd(eo_0, eo_1))));
  (v_sjt = LINE(41,(assignCallTemp(eo_0, x_bcmul(toString(m_s), v_bigj)),assignCallTemp(eo_1, toString(m_t)),x_bcadd(eo_0, eo_1))));
  (v_d = LINE(42,x_bcdiv(v_qjr, v_sjt)));
  return LINE(43,x_floor(toDouble(v_d)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 46 */
p_transformation c_transformation::t_next() {
  INSTANCE_METHOD_INJECTION(Transformation, Transformation::Next);
  (m_k = m_k + 1LL);
  (m_q = LINE(48,x_strval(m_k)));
  (m_r = LINE(49,x_strval(4LL * m_k + 2LL)));
  (m_s = "0");
  (m_t = LINE(51,x_strval(2LL * m_k + 1LL)));
  return ((Object)(this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 58 */
Variant c_pidigitstream::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_pidigitstream::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_pidigitstream::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("z", m_z.isReferenced() ? ref(m_z) : m_z));
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  props.push_back(NEW(ArrayElement)("inverse", m_inverse.isReferenced() ? ref(m_inverse) : m_inverse));
  c_ObjectData::o_get(props);
}
bool c_pidigitstream::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_EXISTS_STRING(0x62A103F6518DE2B3LL, z, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x09AB21CAD293689DLL, inverse, 7);
      break;
    case 6:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_pidigitstream::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x09AB21CAD293689DLL, m_inverse,
                         inverse, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_pidigitstream::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_SET_STRING(0x62A103F6518DE2B3LL, m_z,
                      z, 1);
      break;
    case 5:
      HASH_SET_STRING(0x09AB21CAD293689DLL, m_inverse,
                      inverse, 7);
      break;
    case 6:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_pidigitstream::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x09AB21CAD293689DLL, m_inverse,
                         inverse, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_pidigitstream::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(pidigitstream)
ObjectData *c_pidigitstream::create() {
  init();
  t_pidigitstream();
  return this;
}
ObjectData *c_pidigitstream::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_pidigitstream::cloneImpl() {
  c_pidigitstream *obj = NEW(c_pidigitstream)();
  cloneSet(obj);
  return obj;
}
void c_pidigitstream::cloneSet(c_pidigitstream *clone) {
  clone->m_z = m_z.isReferenced() ? ref(m_z) : m_z;
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  clone->m_inverse = m_inverse.isReferenced() ? ref(m_inverse) : m_inverse;
  ObjectData::cloneSet(clone);
}
Variant c_pidigitstream::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_pidigitstream::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pidigitstream::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pidigitstream$os_get(const char *s) {
  return c_pidigitstream::os_get(s, -1);
}
Variant &cw_pidigitstream$os_lval(const char *s) {
  return c_pidigitstream::os_lval(s, -1);
}
Variant cw_pidigitstream$os_constant(const char *s) {
  return c_pidigitstream::os_constant(s);
}
Variant cw_pidigitstream$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pidigitstream::os_invoke(c, s, params, -1, fatal);
}
void c_pidigitstream::init() {
  m_z = null;
  m_x = null;
  m_inverse = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 61 */
void c_pidigitstream::t_pidigitstream() {
  INSTANCE_METHOD_INJECTION(PiDigitStream, PiDigitStream::PiDigitStream);
  bool oldInCtor = gasInCtor(true);
  (m_z = ((Object)(LINE(62,c_transformation::t_unity()))));
  (m_x = ((Object)(LINE(63,c_transformation::t_zero()))));
  (m_inverse = ((Object)(LINE(64,c_transformation::t_zero()))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 67 */
Variant c_pidigitstream::t_produce(CVarRef v_j) {
  INSTANCE_METHOD_INJECTION(PiDigitStream, PiDigitStream::Produce);
  Variant v_i;

  (v_i = m_inverse);
  (v_i.o_lval("q", 0x05CAFC91A9B37763LL) = "10");
  (v_i.o_lval("r", 0x6ECE84440D42ECF6LL) = LINE(70,x_strval(-10LL * v_j)));
  (v_i.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = "0");
  (v_i.o_lval("t", 0x11C9D2391242AEBCLL) = "1");
  return LINE(73,v_i.o_invoke_few_args("Compose", 0x391960AD87E99C66LL, 1, m_z));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 76 */
Variant c_pidigitstream::t_consume(Variant v_a) {
  INSTANCE_METHOD_INJECTION(PiDigitStream, PiDigitStream::Consume);
  return LINE(77,m_z.o_invoke_few_args("Compose", 0x391960AD87E99C66LL, 1, v_a));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 80 */
Variant c_pidigitstream::t_digit() {
  INSTANCE_METHOD_INJECTION(PiDigitStream, PiDigitStream::Digit);
  return LINE(81,m_z.o_invoke_few_args("Extract", 0x014BD9A6823256F0LL, 1, 3LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 84 */
bool c_pidigitstream::t_issafe(CVarRef v_j) {
  INSTANCE_METHOD_INJECTION(PiDigitStream, PiDigitStream::IsSafe);
  return equal(v_j, (LINE(85,m_z.o_invoke_few_args("Extract", 0x014BD9A6823256F0LL, 1, 4LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 88 */
Variant c_pidigitstream::t_next() {
  INSTANCE_METHOD_INJECTION(PiDigitStream, PiDigitStream::Next);
  Variant v_y;

  (v_y = LINE(89,t_digit()));
  if (LINE(90,t_issafe(v_y))) {
    (m_z = LINE(91,t_produce(v_y)));
    return v_y;
  }
  else {
    (m_z = LINE(94,t_consume(m_x.o_invoke_few_args("Next", 0x3C6D50F3BB8102B8LL, 0))));
    return LINE(95,t_next());
  }
  return null;
} /* function */
Object co_transformation(CArrRef params, bool init /* = true */) {
  return Object(p_transformation(NEW(c_transformation)())->dynCreate(params, init));
}
Object co_pidigitstream(CArrRef params, bool init /* = true */) {
  return Object(p_pidigitstream(NEW(c_pidigitstream)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$pidigits_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/pidigits.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$pidigits_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_length __attribute__((__unused__)) = (variables != gVariables) ? variables->get("length") : g->GV(length);
  Variant &v_pidigit __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pidigit") : g->GV(pidigit);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_i = 0LL);
  (v_length = 10LL);
  (v_pidigit = ((Object)(LINE(104,p_pidigitstream(p_pidigitstream(NEWOBJ(c_pidigitstream)())->create())))));
  LOOP_COUNTER(1);
  {
    while (more(v_n, 0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        if (less(v_n, v_length)) {
          {
            LOOP_COUNTER(2);
            for ((v_j = 0LL); less(v_j, v_n); v_j++) {
              LOOP_COUNTER_CHECK(2);
              LINE(108,(assignCallTemp(eo_1, v_pidigit.o_invoke_few_args("Next", 0x3C6D50F3BB8102B8LL, 0)),x_printf(2, "%d", Array(ArrayInit(1).set(0, eo_1).create()))));
            }
          }
          {
            LOOP_COUNTER(3);
            for ((v_j = v_n); less(v_j, v_length); v_j++) {
              LOOP_COUNTER_CHECK(3);
              print(" ");
            }
          }
          v_i += v_n;
        }
        else {
          {
            LOOP_COUNTER(4);
            for ((v_j = 0LL); less(v_j, v_length); v_j++) {
              LOOP_COUNTER_CHECK(4);
              LINE(112,(assignCallTemp(eo_1, v_pidigit.o_invoke_few_args("Next", 0x3C6D50F3BB8102B8LL, 0)),x_printf(2, "%d", Array(ArrayInit(1).set(0, eo_1).create()))));
            }
          }
          v_i += v_length;
        }
        print(LINE(115,concat3("\t:", toString(v_i), "\n")));
        v_n -= v_length;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
