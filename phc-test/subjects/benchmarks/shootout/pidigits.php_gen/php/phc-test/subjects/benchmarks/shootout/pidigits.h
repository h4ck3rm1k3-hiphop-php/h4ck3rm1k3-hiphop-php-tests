
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/pidigits.fw.h>

// Declarations
#include <cls/transformation.h>
#include <cls/pidigitstream.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$pidigits_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_transformation(CArrRef params, bool init = true);
Object co_pidigitstream(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_h__
