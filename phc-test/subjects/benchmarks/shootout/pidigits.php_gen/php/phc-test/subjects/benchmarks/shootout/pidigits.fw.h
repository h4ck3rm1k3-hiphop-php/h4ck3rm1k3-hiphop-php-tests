
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(transformation)
FORWARD_DECLARE_CLASS(pidigitstream)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_pidigits_fw_h__
