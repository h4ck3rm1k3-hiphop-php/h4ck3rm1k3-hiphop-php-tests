
#ifndef __GENERATED_cls_transformation_h__
#define __GENERATED_cls_transformation_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 11 */
class c_transformation : virtual public ObjectData {
  BEGIN_CLASS_MAP(transformation)
  END_CLASS_MAP(transformation)
  DECLARE_CLASS(transformation, Transformation, ObjectData)
  void init();
  public: Variant m_q;
  public: Variant m_r;
  public: Variant m_s;
  public: Variant m_t;
  public: Variant m_k;
  public: void t_transformation(CStrRef v_q, CStrRef v_r, CStrRef v_s, CStrRef v_t);
  public: ObjectData *create(CStrRef v_q, CStrRef v_r, CStrRef v_s, CStrRef v_t);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: static p_transformation ti_unity(const char* cls);
  public: static p_transformation ti_zero(const char* cls);
  public: p_transformation t_compose(CVarRef v_a);
  public: double t_extract(CVarRef v_j);
  public: p_transformation t_next();
  public: static p_transformation t_unity() { return ti_unity("transformation"); }
  public: static p_transformation t_zero() { return ti_zero("transformation"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_transformation_h__
