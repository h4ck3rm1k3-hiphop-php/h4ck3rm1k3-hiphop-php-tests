
#ifndef __GENERATED_cls_pidigitstream_h__
#define __GENERATED_cls_pidigitstream_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/pidigits.php line 58 */
class c_pidigitstream : virtual public ObjectData {
  BEGIN_CLASS_MAP(pidigitstream)
  END_CLASS_MAP(pidigitstream)
  DECLARE_CLASS(pidigitstream, PiDigitStream, ObjectData)
  void init();
  public: Variant m_z;
  public: Variant m_x;
  public: Variant m_inverse;
  public: void t_pidigitstream();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_produce(CVarRef v_j);
  public: Variant t_consume(Variant v_a);
  public: Variant t_digit();
  public: bool t_issafe(CVarRef v_j);
  public: Variant t_next();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pidigitstream_h__
