
#include <php/phc-test/subjects/benchmarks/shootout/regexmatch.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$regexmatch_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/regexmatch.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$regexmatch_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_regex __attribute__((__unused__)) = (variables != gVariables) ? variables->get("regex") : g->GV(regex);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_phoneNumbers __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phoneNumbers") : g->GV(phoneNumbers);
  Variant &v_last __attribute__((__unused__)) = (variables != gVariables) ? variables->get("last") : g->GV(last);
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_match __attribute__((__unused__)) = (variables != gVariables) ? variables->get("match") : g->GV(match);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);

  (v_regex = "/(\?:^|[^\\d\\(])(\?:\\((\\d\\d\\d)\\)|(\\d\\d\\d))[ ](\\d\\d\\d)[ -](\\d\\d\\d\\d)\\D/");
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_phoneNumbers = LINE(26,x_file("php://stdin")));
  (v_last = LINE(27,x_sizeof(v_phoneNumbers)) - 1LL);
  (v_count = 0LL);
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_n--)) {
      LOOP_COUNTER_CHECK(1);
      {
        LOOP_COUNTER(2);
        for ((v_i = 0LL); less(v_i, v_last); v_i++) {
          LOOP_COUNTER_CHECK(2);
          {
            LINE(32,x_preg_match(toString(v_regex), toString(v_phoneNumbers.rvalAt(v_i)), ref(v_match)));
            if ((equal(v_n, 0LL)) && toBoolean(v_match)) {
              (v_m = toBoolean(v_match.rvalAt(1LL, 0x5BCA7C69B794F8CELL)) ? ((Variant)(v_match.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(v_match.rvalAt(2LL, 0x486AFCC090D5F98CLL))));
              LINE(35,(assignCallTemp(eo_1, ++v_count),assignCallTemp(eo_2, v_m),assignCallTemp(eo_3, v_match.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)),assignCallTemp(eo_4, v_match.rvalAt(4LL, 0x6F2A25235E544A31LL)),x_printf(5, "%d: (%s) %s-%s\n", Array(ArrayInit(4).set(0, eo_1).set(1, eo_2).set(2, eo_3).set(3, eo_4).create()))));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
