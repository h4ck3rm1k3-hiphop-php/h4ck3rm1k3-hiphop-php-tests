
#include <php/phc-test/subjects/benchmarks/shootout/random.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_IA = 3877LL;
const int64 k_IC = 29573LL;
const int64 k_IM = 139968LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/random.php line 12 */
Numeric f_gen_random(double v_max) {
  FUNCTION_INJECTION(gen_random);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LAST __attribute__((__unused__)) = g->GV(LAST);
  return (divide((v_max * toDouble(((gv_LAST = modulo((toInt64(gv_LAST * 3877LL /* IA */ + 29573LL /* IC */)), 139968LL /* IM */))))), 139968LL /* IM */));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$random_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/random.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$random_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_LAST __attribute__((__unused__)) = (variables != gVariables) ? variables->get("LAST") : g->GV(LAST);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_N __attribute__((__unused__)) = (variables != gVariables) ? variables->get("N") : g->GV(N);

  ;
  ;
  ;
  (v_LAST = 42LL);
  (v_N = ((equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL))) - 1LL);
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_N--)) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(19,f_gen_random(100.0));
      }
    }
  }
  LINE(22,(assignCallTemp(eo_1, f_gen_random(100.0)),x_printf(2, "%.9f\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
