
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_random_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_random_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/random.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Numeric f_gen_random(double v_max);
Variant pm_php$phc_test$subjects$benchmarks$shootout$random_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_random_h__
