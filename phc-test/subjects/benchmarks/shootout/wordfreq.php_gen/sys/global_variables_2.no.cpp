
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      HASH_INITIALIZED(0x0B8A6D3145247D41LL, g->GV(wordcounts),
                       wordcounts);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 15:
      HASH_INITIALIZED(0x0D6857FDDD7F21CFLL, g->GV(k),
                       k);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      HASH_INITIALIZED(0x36C65716F7A86511LL, g->GV(splitWord),
                       splitWord);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 24:
      HASH_INITIALIZED(0x0EB22EDA95766E98LL, g->GV(i),
                       i);
      break;
    case 25:
      HASH_INITIALIZED(0x5499F8A3D317E2D9LL, g->GV(counts),
                       counts);
      break;
    case 28:
      HASH_INITIALIZED(0x760894CA6D41E4DCLL, g->GV(last),
                       last);
      break;
    case 31:
      HASH_INITIALIZED(0x480DFF255F94F8DFLL, g->GV(words),
                       words);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 39:
      HASH_INITIALIZED(0x1D12A485470A29A7LL, g->GV(first),
                       first);
      break;
    case 41:
      HASH_INITIALIZED(0x009CBDB5D52B2AA9LL, g->GV(w),
                       w);
      break;
    case 43:
      HASH_INITIALIZED(0x3C2F961831E4EF6BLL, g->GV(v),
                       v);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 48:
      HASH_INITIALIZED(0x68512E4C61242170LL, g->GV(hasSplitWord),
                       hasSplitWord);
      HASH_INITIALIZED(0x5B0C0C547374D530LL, g->GV(fp),
                       fp);
      break;
    case 51:
      HASH_INITIALIZED(0x037B2AC087556EF3LL, g->GV(block),
                       block);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
