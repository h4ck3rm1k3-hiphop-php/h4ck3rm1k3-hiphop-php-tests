
#include <php/phc-test/subjects/benchmarks/shootout/wordfreq.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/wordfreq.php line 50 */
Variant f_cmpcountandname(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(CmpCountAndName);
  if (equal(v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL), v_b.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) {
    return LINE(51,x_strcmp(toString(v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  }
  else {
    return less(v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL), v_b.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  }
  return null;
} /* function */
Variant i_cmpcountandname(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x67469B1F2CCCACB4LL, cmpcountandname) {
    return (f_cmpcountandname(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$wordfreq_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/wordfreq.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$wordfreq_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_hasSplitWord __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hasSplitWord") : g->GV(hasSplitWord);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_block __attribute__((__unused__)) = (variables != gVariables) ? variables->get("block") : g->GV(block);
  Variant &v_words __attribute__((__unused__)) = (variables != gVariables) ? variables->get("words") : g->GV(words);
  Variant &v_first __attribute__((__unused__)) = (variables != gVariables) ? variables->get("first") : g->GV(first);
  Variant &v_last __attribute__((__unused__)) = (variables != gVariables) ? variables->get("last") : g->GV(last);
  Variant &v_splitWord __attribute__((__unused__)) = (variables != gVariables) ? variables->get("splitWord") : g->GV(splitWord);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);
  Variant &v_counts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("counts") : g->GV(counts);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_wordcounts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wordcounts") : g->GV(wordcounts);

  (v_hasSplitWord = false);
  (v_fp = LINE(15,x_fopen("php://stdin", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_block = LINE(17,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_words = LINE(19,x_preg_split("/[^a-zA-Z]+/", v_block)));
        (v_first = 0LL);
        (v_last = LINE(22,x_sizeof(v_words)) - 1LL);
        if (not_less(v_last, v_first)) {
          if (toBoolean(LINE(28,x_strlen(toString(v_words.rvalAt(v_first)))))) {
            if (toBoolean(v_hasSplitWord)) {
              v_words.set(v_first, (concat(toString(v_splitWord), toString(v_words.rvalAt(v_first)))));
            }
          }
          else {
            if (toBoolean(v_hasSplitWord)) {
              v_words.set(v_first, (v_splitWord));
            }
            else {
              v_first++;
            }
          }
          if (toBoolean((v_hasSplitWord = LINE(34,x_strlen(toString(v_words.rvalAt(v_last))))))) {
            (v_splitWord = v_words.rvalAt(v_last));
          }
          {
            LOOP_COUNTER(2);
            for ((v_i = v_first); less(v_i, v_last); v_i++) {
              LOOP_COUNTER_CHECK(2);
              {
                (v_w = LINE(40,x_strtolower(toString(v_words.rvalAt(v_i)))));
                if (isset(v_counts, v_w)) {
                  lval(v_counts.lvalAt(v_w))++;
                }
                else {
                  v_counts.set(v_w, (1LL));
                }
              }
            }
          }
        }
        unset(v_words);
      }
    }
  }
  LINE(46,x_fclose(toObject(v_fp)));
  LOOP_COUNTER(3);
  {
    while (toBoolean(df_lambda_1(LINE(55,x_each(ref(v_counts))), v_k, v_v))) {
      LOOP_COUNTER_CHECK(3);
      v_wordcounts.append((Array(ArrayInit(2).set(0, v_k).set(1, v_v).create())));
    }
  }
  LINE(56,x_usort(ref(v_wordcounts), "CmpCountAndName"));
  LOOP_COUNTER(4);
  {
    while (toBoolean(df_lambda_2(LINE(57,x_each(ref(v_wordcounts))), v_k, v_v))) {
      LOOP_COUNTER_CHECK(4);
      x_printf(3, "%7d %s\n", Array(ArrayInit(2).set(0, v_v.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_v.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create()));
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
