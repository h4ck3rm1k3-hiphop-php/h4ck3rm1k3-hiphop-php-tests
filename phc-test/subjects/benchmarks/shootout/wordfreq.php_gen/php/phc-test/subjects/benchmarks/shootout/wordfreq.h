
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_wordfreq_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_wordfreq_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/wordfreq.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$wordfreq_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_cmpcountandname(CVarRef v_a, CVarRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_wordfreq_h__
