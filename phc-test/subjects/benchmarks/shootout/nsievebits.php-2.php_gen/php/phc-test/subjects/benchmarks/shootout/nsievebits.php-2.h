
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsievebits_php_2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsievebits_php_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/nsievebits.php-2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$nsievebits_php_2_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_primes(CVarRef v_size);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_nsievebits_php_2_h__
