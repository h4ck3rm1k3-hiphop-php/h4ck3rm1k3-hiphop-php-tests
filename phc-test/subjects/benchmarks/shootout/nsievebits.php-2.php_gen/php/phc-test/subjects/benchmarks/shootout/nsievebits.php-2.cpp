
#include <php/phc-test/subjects/benchmarks/shootout/nsievebits.php-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/nsievebits.php-2.php line 6 */
void f_primes(CVarRef v_size) {
  FUNCTION_INJECTION(primes);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_flags;
  int64 v_count = 0;
  int64 v_i = 0;
  int64 v_offset = 0;
  int64 v_mask = 0;
  int64 v_j = 0;

  (v_flags = LINE(8,x_array_fill(toInt32(0LL), toInt32((toInt64(toInt64(v_size)) >> 5LL) + 1LL), -1LL)));
  (v_count = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 2LL); less(v_i, v_size); ++v_i) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_offset = toInt64(v_i) >> 5LL);
        (v_mask = toInt64(1LL) << (v_i - (toInt64(v_offset) << 5LL)));
        if (toBoolean(bitwise_and(v_flags.rvalAt(v_offset), v_mask))) {
          ++v_count;
          {
            LOOP_COUNTER(2);
            for ((v_j = toInt64(v_i) << 1LL); not_more(v_j, v_size); v_j += v_i) {
              LOOP_COUNTER_CHECK(2);
              {
                (v_offset = toInt64(v_j) >> 5LL);
                (v_mask = toInt64(1LL) << (v_j - (toInt64(v_offset) << 5LL)));
                if (toBoolean(bitwise_and(v_flags.rvalAt(v_offset), v_mask))) {
                  lval(v_flags.lvalAt(v_offset)) ^= v_mask;
                }
              }
            }
          }
        }
      }
    }
  }
  unset(v_flags);
  LINE(29,x_printf(3, "Primes up to %8d %8d\n", Array(ArrayInit(2).set(0, v_size).set(1, v_count).create())));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$nsievebits_php_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/nsievebits.php-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$nsievebits_php_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("size") : g->GV(size);

  (v_n = (!equal(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "")) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(g->gv__GET.rvalAt("n", 0x117B8667E4662809LL))));
  (v_size = 10000LL * (toInt64(1LL) << toInt64(v_n)));
  LINE(35,f_primes(v_size));
  LINE(36,f_primes(toInt64(toInt64(v_size)) >> 1LL));
  LINE(37,f_primes(toInt64(toInt64(v_size)) >> 2LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
