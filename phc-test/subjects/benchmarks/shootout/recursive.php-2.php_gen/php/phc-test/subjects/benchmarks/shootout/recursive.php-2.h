
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_recursive_php_2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_recursive_php_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/recursive.php-2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_tak(CVarRef v_x, CVarRef v_y, CVarRef v_z);
Variant pm_php$phc_test$subjects$benchmarks$shootout$recursive_php_2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_fib(Numeric v_n);
Numeric f_ack(Numeric v_m, CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_recursive_php_2_h__
