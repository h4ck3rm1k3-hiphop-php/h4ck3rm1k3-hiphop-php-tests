
#include <php/phc-test/subjects/benchmarks/shootout/recursive.php-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/recursive.php-2.php line 18 */
Variant f_tak(CVarRef v_x, CVarRef v_y, CVarRef v_z) {
  FUNCTION_INJECTION(tak);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  if (less(v_y, v_x)) {
    return LINE(19,(assignCallTemp(eo_0, f_tak(v_x - 1LL, v_y, v_z)),assignCallTemp(eo_1, f_tak(v_y - 1LL, v_z, v_x)),assignCallTemp(eo_2, f_tak(v_z - 1LL, v_x, v_y)),f_tak(eo_0, eo_1, eo_2)));
  }
  else {
    return v_z;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/recursive.php-2.php line 13 */
Variant f_fib(Numeric v_n) {
  FUNCTION_INJECTION(fib);
  if (less(v_n, 2LL)) {
    return 1LL;
  }
  else {
    return plus_rev(LINE(15,f_fib(v_n - 1LL)), f_fib(v_n - 2LL));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/recursive.php-2.php line 7 */
Numeric f_ack(Numeric v_m, CVarRef v_n) {
  FUNCTION_INJECTION(ack);
  Variant eo_0;
  Variant eo_1;
  if (equal(v_m, 0LL)) return v_n + 1LL;
  if (equal(v_n, 0LL)) return LINE(9,f_ack(v_m - 1LL, 1LL));
  return LINE(10,(assignCallTemp(eo_0, v_m - 1LL),assignCallTemp(eo_1, f_ack(v_m, (v_n - 1LL))),f_ack(eo_0, eo_1)));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$shootout$recursive_php_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/recursive.php-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$recursive_php_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  {
    echo(LINE(24,concat3("Ack(3,", toString(v_n), "): ")));
    echo(toString(f_ack(3LL, v_n)));
    echo("\n");
  }
  LINE(25,(assignCallTemp(eo_1, 27.0 + toDouble(v_n)),assignCallTemp(eo_2, f_fib(27.0 + toDouble(v_n))),x_printf(3, "Fib(%.1f): %.1f\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  v_n--;
  LINE(26,(assignCallTemp(eo_1, 3LL * v_n),assignCallTemp(eo_2, 2LL * v_n),assignCallTemp(eo_3, v_n),assignCallTemp(eo_4, f_tak(3LL * v_n, 2LL * v_n, v_n)),x_printf(5, "Tak(%d,%d,%d): %d\n", Array(ArrayInit(4).set(0, eo_1).set(1, eo_2).set(2, eo_3).set(3, eo_4).create()))));
  LINE(28,(assignCallTemp(eo_1, f_fib(3LL)),x_printf(2, "Fib(3): %d\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  LINE(29,(assignCallTemp(eo_1, f_tak(3.0, 2.0, 1.0)),x_printf(2, "Tak(3.0,2.0,1.0): %.1f\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
