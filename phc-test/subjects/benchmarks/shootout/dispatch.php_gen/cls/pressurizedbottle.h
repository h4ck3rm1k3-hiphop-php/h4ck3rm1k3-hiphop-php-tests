
#ifndef __GENERATED_cls_pressurizedbottle_h__
#define __GENERATED_cls_pressurizedbottle_h__

#include <cls/bottle.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 106 */
class c_pressurizedbottle : virtual public c_bottle {
  BEGIN_CLASS_MAP(pressurizedbottle)
    PARENT_CLASS(bottle)
  END_CLASS_MAP(pressurizedbottle)
  DECLARE_CLASS(pressurizedbottle, PressurizedBottle, bottle)
  void init();
  public: void t_pressurizedbottle(int64 v_id);
  public: ObjectData *create(int64 v_id);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_initialstate();
  public: void t_cycle();
  public: void t_pressurize();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pressurizedbottle_h__
