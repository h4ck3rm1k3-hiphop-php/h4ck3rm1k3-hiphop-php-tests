
#ifndef __GENERATED_cls_unpressurizedemptystate_h__
#define __GENERATED_cls_unpressurizedemptystate_h__

#include <cls/pressurizedbottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 42 */
class c_unpressurizedemptystate : virtual public c_pressurizedbottlestate {
  BEGIN_CLASS_MAP(unpressurizedemptystate)
    PARENT_CLASS(bottlestate)
    PARENT_CLASS(pressurizedbottlestate)
  END_CLASS_MAP(unpressurizedemptystate)
  DECLARE_CLASS(unpressurizedemptystate, UnpressurizedEmptyState, pressurizedbottlestate)
  void init();
  public: void t_next(CVarRef v_b);
  public: int64 t_tag();
  public: static Variant t_initialstate() { return ti_initialstate("unpressurizedemptystate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_unpressurizedemptystate_h__
