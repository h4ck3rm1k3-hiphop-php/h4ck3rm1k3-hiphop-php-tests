
#ifndef __GENERATED_cls_fullstate_h__
#define __GENERATED_cls_fullstate_h__

#include <cls/bottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 23 */
class c_fullstate : virtual public c_bottlestate {
  BEGIN_CLASS_MAP(fullstate)
    PARENT_CLASS(bottlestate)
  END_CLASS_MAP(fullstate)
  DECLARE_CLASS(fullstate, FullState, bottlestate)
  void init();
  public: void t_next(CVarRef v_b);
  public: int64 t_tag();
  public: static Variant t_initialstate() { return ti_initialstate("fullstate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fullstate_h__
