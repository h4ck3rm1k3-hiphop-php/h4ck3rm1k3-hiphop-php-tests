
#ifndef __GENERATED_cls_pressurizedbottlestate_h__
#define __GENERATED_cls_pressurizedbottlestate_h__

#include <cls/bottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 38 */
class c_pressurizedbottlestate : virtual public c_bottlestate {
  BEGIN_CLASS_MAP(pressurizedbottlestate)
    PARENT_CLASS(bottlestate)
  END_CLASS_MAP(pressurizedbottlestate)
  DECLARE_CLASS(pressurizedbottlestate, PressurizedBottleState, bottlestate)
  void init();
  public: static Variant ti_initialstate(const char* cls);
  public: static Variant t_initialstate() { return ti_initialstate("pressurizedbottlestate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pressurizedbottlestate_h__
