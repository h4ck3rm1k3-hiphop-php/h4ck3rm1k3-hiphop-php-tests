
#ifndef __GENERATED_cls_bottle_h__
#define __GENERATED_cls_bottle_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 68 */
class c_bottle : virtual public ObjectData {
  BEGIN_CLASS_MAP(bottle)
  END_CLASS_MAP(bottle)
  DECLARE_CLASS(bottle, Bottle, ObjectData)
  void init();
  public: Variant m_bottleState;
  public: Variant m_id;
  public: void t_bottle(int64 v_id);
  public: ObjectData *create(int64 v_id);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_state(CVarRef v_s);
  public: void t_cycle();
  public: Variant t_initialstate();
  public: void t_fill();
  public: void t_seal();
  public: void t_empty_();
  public: PlusOperand t_check(CVarRef v_c);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bottle_h__
