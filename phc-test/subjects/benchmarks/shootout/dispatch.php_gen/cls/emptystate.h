
#ifndef __GENERATED_cls_emptystate_h__
#define __GENERATED_cls_emptystate_h__

#include <cls/bottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 18 */
class c_emptystate : virtual public c_bottlestate {
  BEGIN_CLASS_MAP(emptystate)
    PARENT_CLASS(bottlestate)
  END_CLASS_MAP(emptystate)
  DECLARE_CLASS(emptystate, EmptyState, bottlestate)
  void init();
  public: void t_next(CVarRef v_b);
  public: int64 t_tag();
  public: static Variant t_initialstate() { return ti_initialstate("emptystate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_emptystate_h__
