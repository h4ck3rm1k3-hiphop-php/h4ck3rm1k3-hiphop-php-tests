
#ifndef __GENERATED_cls_sealedstate_h__
#define __GENERATED_cls_sealedstate_h__

#include <cls/bottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 28 */
class c_sealedstate : virtual public c_bottlestate {
  BEGIN_CLASS_MAP(sealedstate)
    PARENT_CLASS(bottlestate)
  END_CLASS_MAP(sealedstate)
  DECLARE_CLASS(sealedstate, SealedState, bottlestate)
  void init();
  public: void t_next(CVarRef v_b);
  public: int64 t_tag();
  public: static Variant t_initialstate() { return ti_initialstate("sealedstate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sealedstate_h__
