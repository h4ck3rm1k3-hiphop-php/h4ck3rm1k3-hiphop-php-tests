
#ifndef __GENERATED_cls_unpressurizedfullstate_h__
#define __GENERATED_cls_unpressurizedfullstate_h__

#include <cls/pressurizedbottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 47 */
class c_unpressurizedfullstate : virtual public c_pressurizedbottlestate {
  BEGIN_CLASS_MAP(unpressurizedfullstate)
    PARENT_CLASS(bottlestate)
    PARENT_CLASS(pressurizedbottlestate)
  END_CLASS_MAP(unpressurizedfullstate)
  DECLARE_CLASS(unpressurizedfullstate, UnpressurizedFullState, pressurizedbottlestate)
  void init();
  public: void t_next(CVarRef v_b);
  public: int64 t_tag();
  public: static Variant t_initialstate() { return ti_initialstate("unpressurizedfullstate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_unpressurizedfullstate_h__
