
#ifndef __GENERATED_cls_bottlestate_h__
#define __GENERATED_cls_bottlestate_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 11 */
class c_bottlestate : virtual public ObjectData {
  BEGIN_CLASS_MAP(bottlestate)
  END_CLASS_MAP(bottlestate)
  DECLARE_CLASS(bottlestate, BottleState, ObjectData)
  void init();
  public: void t_next(CVarRef v_b);
  public: void t_tag();
  public: static Variant ti_initialstate(const char* cls);
  public: static Variant t_initialstate() { return ti_initialstate("bottlestate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bottlestate_h__
