
#ifndef __GENERATED_cls_pressurizedsealedstate_h__
#define __GENERATED_cls_pressurizedsealedstate_h__

#include <cls/pressurizedbottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 57 */
class c_pressurizedsealedstate : virtual public c_pressurizedbottlestate {
  BEGIN_CLASS_MAP(pressurizedsealedstate)
    PARENT_CLASS(bottlestate)
    PARENT_CLASS(pressurizedbottlestate)
  END_CLASS_MAP(pressurizedsealedstate)
  DECLARE_CLASS(pressurizedsealedstate, PressurizedSealedState, pressurizedbottlestate)
  void init();
  public: void t_next(CVarRef v_b);
  public: int64 t_tag();
  public: static Variant t_initialstate() { return ti_initialstate("pressurizedsealedstate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pressurizedsealedstate_h__
