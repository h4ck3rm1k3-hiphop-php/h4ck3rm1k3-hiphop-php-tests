
#ifndef __GENERATED_cls_pressurizedunsealedstate_h__
#define __GENERATED_cls_pressurizedunsealedstate_h__

#include <cls/pressurizedbottlestate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 52 */
class c_pressurizedunsealedstate : virtual public c_pressurizedbottlestate {
  BEGIN_CLASS_MAP(pressurizedunsealedstate)
    PARENT_CLASS(bottlestate)
    PARENT_CLASS(pressurizedbottlestate)
  END_CLASS_MAP(pressurizedunsealedstate)
  DECLARE_CLASS(pressurizedunsealedstate, PressurizedUnsealedState, pressurizedbottlestate)
  void init();
  public: void t_next(CVarRef v_b);
  public: int64 t_tag();
  public: static Variant t_initialstate() { return ti_initialstate("pressurizedunsealedstate"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pressurizedunsealedstate_h__
