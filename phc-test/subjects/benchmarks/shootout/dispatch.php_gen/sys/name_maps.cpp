
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "bottle", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "bottlestate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "emptystate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "fullstate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "pressurizedbottle", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "pressurizedbottlestate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "pressurizedsealedstate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "pressurizedunsealedstate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "sealedstate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "unpressurizedemptystate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  "unpressurizedfullstate", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "bottlecheck", "phc-test/subjects/benchmarks/shootout/dispatch.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
