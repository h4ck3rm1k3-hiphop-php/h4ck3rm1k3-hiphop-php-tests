
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(Empty)
    GVS(Full)
    GVS(Sealed)
    GVS(UnpressurizedEmpty)
    GVS(UnpressurizedFull)
    GVS(PressurizedUnsealed)
    GVS(PressurizedSealed)
    GVS(n)
    GVS(b1)
    GVS(b2)
    GVS(b3)
    GVS(b4)
    GVS(b5)
    GVS(b6)
    GVS(b7)
    GVS(b8)
    GVS(b9)
    GVS(b0)
    GVS(p1)
    GVS(p2)
    GVS(p3)
    GVS(p4)
    GVS(p5)
    GVS(p6)
    GVS(p7)
    GVS(p8)
    GVS(p9)
    GVS(p0)
    GVS(check)
    GVS(i)
  END_GVS(30)

  // Dynamic Constants

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$subjects$benchmarks$shootout$dispatch_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 42;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
