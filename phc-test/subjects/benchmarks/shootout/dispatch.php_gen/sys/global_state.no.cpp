
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

static void print(FILE *fp, String s) {
  if (fp) {
    fwrite(s.c_str(), 1, s.size(), fp);
    return;
  }
  echo(s);
}
static void output_dynamic_constants(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_constants = json_decode('");
  Array dynamic_constants;
  String s = f_json_encode(dynamic_constants);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_static_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$static_global_variables = json_decode('");
  Array static_global_variables;
  static_global_variables.set("gv_argc", g->get("argc"));
  static_global_variables.set("gv_argv", g->get("argv"));
  static_global_variables.set("gv__SERVER", g->get("_SERVER"));
  static_global_variables.set("gv__GET", g->get("_GET"));
  static_global_variables.set("gv__POST", g->get("_POST"));
  static_global_variables.set("gv__COOKIE", g->get("_COOKIE"));
  static_global_variables.set("gv__FILES", g->get("_FILES"));
  static_global_variables.set("gv__ENV", g->get("_ENV"));
  static_global_variables.set("gv__REQUEST", g->get("_REQUEST"));
  static_global_variables.set("gv__SESSION", g->get("_SESSION"));
  static_global_variables.set("gv_HTTP_RAW_POST_DATA", g->get("HTTP_RAW_POST_DATA"));
  static_global_variables.set("gv_http_response_header", g->get("http_response_header"));
  static_global_variables.set("gv_Empty", g->get("Empty"));
  static_global_variables.set("gv_Full", g->get("Full"));
  static_global_variables.set("gv_Sealed", g->get("Sealed"));
  static_global_variables.set("gv_UnpressurizedEmpty", g->get("UnpressurizedEmpty"));
  static_global_variables.set("gv_UnpressurizedFull", g->get("UnpressurizedFull"));
  static_global_variables.set("gv_PressurizedUnsealed", g->get("PressurizedUnsealed"));
  static_global_variables.set("gv_PressurizedSealed", g->get("PressurizedSealed"));
  static_global_variables.set("gv_n", g->get("n"));
  static_global_variables.set("gv_b1", g->get("b1"));
  static_global_variables.set("gv_b2", g->get("b2"));
  static_global_variables.set("gv_b3", g->get("b3"));
  static_global_variables.set("gv_b4", g->get("b4"));
  static_global_variables.set("gv_b5", g->get("b5"));
  static_global_variables.set("gv_b6", g->get("b6"));
  static_global_variables.set("gv_b7", g->get("b7"));
  static_global_variables.set("gv_b8", g->get("b8"));
  static_global_variables.set("gv_b9", g->get("b9"));
  static_global_variables.set("gv_b0", g->get("b0"));
  static_global_variables.set("gv_p1", g->get("p1"));
  static_global_variables.set("gv_p2", g->get("p2"));
  static_global_variables.set("gv_p3", g->get("p3"));
  static_global_variables.set("gv_p4", g->get("p4"));
  static_global_variables.set("gv_p5", g->get("p5"));
  static_global_variables.set("gv_p6", g->get("p6"));
  static_global_variables.set("gv_p7", g->get("p7"));
  static_global_variables.set("gv_p8", g->get("p8"));
  static_global_variables.set("gv_p9", g->get("p9"));
  static_global_variables.set("gv_p0", g->get("p0"));
  static_global_variables.set("gv_check", g->get("check"));
  static_global_variables.set("gv_i", g->get("i"));
  String s = f_json_encode(static_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_dynamic_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_global_variables = json_decode('");
  Array dynamic_global_variables;
  dynamic_global_variables = *get_variable_table();
  String s = f_json_encode(dynamic_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_variables = json_decode('");
  Array method_static_variables;
  String s = f_json_encode(method_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_inited(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_inited = json_decode('");
  Array method_static_inited;
  String s = f_json_encode(method_static_inited);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_class_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$class_static_variables = json_decode('");
  Array class_static_variables;
  String s = f_json_encode(class_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_pseudomain_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$pseudomain_variables = json_decode('");
  Array pseudomain_variables;
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$shootout$dispatch_php", g->run_pm_php$phc_test$subjects$benchmarks$shootout$dispatch_php);
  String s = f_json_encode(pseudomain_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_functions(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_functions = json_decode('");
  Array redeclared_functions;
  String s = f_json_encode(redeclared_functions);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_classes(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_classes = json_decode('");
  Array redeclared_classes;
  String s = f_json_encode(redeclared_classes);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

void output_global_state(FILE *fp) {
  output_static_global_variables(fp);
  output_dynamic_global_variables(fp);
  output_dynamic_constants(fp);
  output_method_static_variables(fp);
  output_method_static_inited(fp);
  output_class_static_variables(fp);
  output_pseudomain_variables(fp);
  output_redeclared_functions(fp);
  output_redeclared_classes(fp);
}


///////////////////////////////////////////////////////////////////////////////
}
