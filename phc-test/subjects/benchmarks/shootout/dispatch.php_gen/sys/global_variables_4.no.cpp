
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "Empty",
    "Full",
    "Sealed",
    "UnpressurizedEmpty",
    "UnpressurizedFull",
    "PressurizedUnsealed",
    "PressurizedSealed",
    "n",
    "b1",
    "b2",
    "b3",
    "b4",
    "b5",
    "b6",
    "b7",
    "b8",
    "b9",
    "b0",
    "p1",
    "p2",
    "p3",
    "p4",
    "p5",
    "p6",
    "p7",
    "p8",
    "p9",
    "p0",
    "check",
    "i",
  };
  if (idx >= 0 && idx < 42) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(Empty);
    case 13: return GV(Full);
    case 14: return GV(Sealed);
    case 15: return GV(UnpressurizedEmpty);
    case 16: return GV(UnpressurizedFull);
    case 17: return GV(PressurizedUnsealed);
    case 18: return GV(PressurizedSealed);
    case 19: return GV(n);
    case 20: return GV(b1);
    case 21: return GV(b2);
    case 22: return GV(b3);
    case 23: return GV(b4);
    case 24: return GV(b5);
    case 25: return GV(b6);
    case 26: return GV(b7);
    case 27: return GV(b8);
    case 28: return GV(b9);
    case 29: return GV(b0);
    case 30: return GV(p1);
    case 31: return GV(p2);
    case 32: return GV(p3);
    case 33: return GV(p4);
    case 34: return GV(p5);
    case 35: return GV(p6);
    case 36: return GV(p7);
    case 37: return GV(p8);
    case 38: return GV(p9);
    case 39: return GV(p0);
    case 40: return GV(check);
    case 41: return GV(i);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
