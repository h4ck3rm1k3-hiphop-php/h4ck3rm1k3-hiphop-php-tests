
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x4024203A247E4F82LL, Empty, 12);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 7:
      HASH_INDEX(0x5AEC3B20C03ED687LL, p7, 36);
      break;
    case 9:
      HASH_INDEX(0x117B8667E4662809LL, n, 19);
      HASH_INDEX(0x345DF79AB6FB6B09LL, b8, 27);
      break;
    case 13:
      HASH_INDEX(0x443E7D9C98EDF18DLL, b0, 29);
      HASH_INDEX(0x7712320985A0560DLL, p5, 34);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x4212288C24593E93LL, b7, 26);
      break;
    case 24:
      HASH_INDEX(0x2D5185583EF85E98LL, b2, 21);
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 41);
      break;
    case 25:
      HASH_INDEX(0x6BB2D14D1B3F4A99LL, PressurizedUnsealed, 17);
      break;
    case 26:
      HASH_INDEX(0x0389CB8009D23A1ALL, b9, 28);
      break;
    case 30:
      HASH_INDEX(0x18B6B61EF19E261ELL, p3, 32);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 37:
      HASH_INDEX(0x0BF8E1FFDA30F125LL, UnpressurizedFull, 16);
      break;
    case 46:
      HASH_INDEX(0x226516ED20BAAFAELL, UnpressurizedEmpty, 15);
      break;
    case 47:
      HASH_INDEX(0x1A669C137C2C602FLL, check, 40);
      break;
    case 60:
      HASH_INDEX(0x4F0B2D89550857BCLL, p9, 38);
      break;
    case 62:
      HASH_INDEX(0x7ADD4C161B321ABELL, p6, 35);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 64:
      HASH_INDEX(0x6446E25DA07A12C0LL, p0, 39);
      break;
    case 66:
      HASH_INDEX(0x08424A303B976142LL, b4, 23);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 71:
      HASH_INDEX(0x0FEFA20DB1B08AC7LL, p2, 31);
      HASH_INDEX(0x64845A35C3C3C247LL, p8, 37);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x2A6886999C5D01C9LL, Full, 13);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 84:
      HASH_INDEX(0x2D22A77B66D66AD4LL, Sealed, 14);
      break;
    case 94:
      HASH_INDEX(0x5F4AE7AA1641D45ELL, b3, 22);
      break;
    case 107:
      HASH_INDEX(0x29C390281340176BLL, b1, 20);
      break;
    case 108:
      HASH_INDEX(0x561CEFF2C0D9ADECLL, PressurizedSealed, 18);
      HASH_INDEX(0x20511842520A276CLL, p4, 33);
      break;
    case 109:
      HASH_INDEX(0x7E82E1E7C641086DLL, b6, 25);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 119:
      HASH_INDEX(0x5C657D380DA684F7LL, p1, 30);
      break;
    case 121:
      HASH_INDEX(0x0B0A60EB541C6C79LL, b5, 24);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 42) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
