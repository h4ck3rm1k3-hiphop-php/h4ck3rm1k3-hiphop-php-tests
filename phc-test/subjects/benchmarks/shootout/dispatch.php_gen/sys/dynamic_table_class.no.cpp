
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_pressurizedunsealedstate(CArrRef params, bool init = true);
Variant cw_pressurizedunsealedstate$os_get(const char *s);
Variant &cw_pressurizedunsealedstate$os_lval(const char *s);
Variant cw_pressurizedunsealedstate$os_constant(const char *s);
Variant cw_pressurizedunsealedstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sealedstate(CArrRef params, bool init = true);
Variant cw_sealedstate$os_get(const char *s);
Variant &cw_sealedstate$os_lval(const char *s);
Variant cw_sealedstate$os_constant(const char *s);
Variant cw_sealedstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bottlestate(CArrRef params, bool init = true);
Variant cw_bottlestate$os_get(const char *s);
Variant &cw_bottlestate$os_lval(const char *s);
Variant cw_bottlestate$os_constant(const char *s);
Variant cw_bottlestate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_unpressurizedfullstate(CArrRef params, bool init = true);
Variant cw_unpressurizedfullstate$os_get(const char *s);
Variant &cw_unpressurizedfullstate$os_lval(const char *s);
Variant cw_unpressurizedfullstate$os_constant(const char *s);
Variant cw_unpressurizedfullstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_pressurizedsealedstate(CArrRef params, bool init = true);
Variant cw_pressurizedsealedstate$os_get(const char *s);
Variant &cw_pressurizedsealedstate$os_lval(const char *s);
Variant cw_pressurizedsealedstate$os_constant(const char *s);
Variant cw_pressurizedsealedstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_pressurizedbottle(CArrRef params, bool init = true);
Variant cw_pressurizedbottle$os_get(const char *s);
Variant &cw_pressurizedbottle$os_lval(const char *s);
Variant cw_pressurizedbottle$os_constant(const char *s);
Variant cw_pressurizedbottle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_unpressurizedemptystate(CArrRef params, bool init = true);
Variant cw_unpressurizedemptystate$os_get(const char *s);
Variant &cw_unpressurizedemptystate$os_lval(const char *s);
Variant cw_unpressurizedemptystate$os_constant(const char *s);
Variant cw_unpressurizedemptystate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_pressurizedbottlestate(CArrRef params, bool init = true);
Variant cw_pressurizedbottlestate$os_get(const char *s);
Variant &cw_pressurizedbottlestate$os_lval(const char *s);
Variant cw_pressurizedbottlestate$os_constant(const char *s);
Variant cw_pressurizedbottlestate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_emptystate(CArrRef params, bool init = true);
Variant cw_emptystate$os_get(const char *s);
Variant &cw_emptystate$os_lval(const char *s);
Variant cw_emptystate$os_constant(const char *s);
Variant cw_emptystate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bottle(CArrRef params, bool init = true);
Variant cw_bottle$os_get(const char *s);
Variant &cw_bottle$os_lval(const char *s);
Variant cw_bottle$os_constant(const char *s);
Variant cw_bottle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fullstate(CArrRef params, bool init = true);
Variant cw_fullstate$os_get(const char *s);
Variant &cw_fullstate$os_lval(const char *s);
Variant cw_fullstate$os_constant(const char *s);
Variant cw_fullstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_CREATE_OBJECT(0x0809D5798D9C5E85LL, unpressurizedemptystate);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x217481FA00625FA9LL, bottlestate);
      break;
    case 11:
      HASH_CREATE_OBJECT(0x6E20EB05593849ABLL, sealedstate);
      break;
    case 14:
      HASH_CREATE_OBJECT(0x167813DEEE2C994ELL, pressurizedsealedstate);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x72571C43F7CB256FLL, pressurizedbottle);
      break;
    case 17:
      HASH_CREATE_OBJECT(0x4BC2E2A0ED3A2BF1LL, emptystate);
      break;
    case 18:
      HASH_CREATE_OBJECT(0x5019D82296535452LL, unpressurizedfullstate);
      break;
    case 19:
      HASH_CREATE_OBJECT(0x54F1124FE4916573LL, pressurizedbottlestate);
      HASH_CREATE_OBJECT(0x5BCFA27E9B0220F3LL, bottle);
      break;
    case 25:
      HASH_CREATE_OBJECT(0x3D1E59FDF30743F9LL, pressurizedunsealedstate);
      break;
    case 28:
      HASH_CREATE_OBJECT(0x35A97CB4B58D729CLL, fullstate);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x0809D5798D9C5E85LL, unpressurizedemptystate);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x217481FA00625FA9LL, bottlestate);
      break;
    case 11:
      HASH_INVOKE_STATIC_METHOD(0x6E20EB05593849ABLL, sealedstate);
      break;
    case 14:
      HASH_INVOKE_STATIC_METHOD(0x167813DEEE2C994ELL, pressurizedsealedstate);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x72571C43F7CB256FLL, pressurizedbottle);
      break;
    case 17:
      HASH_INVOKE_STATIC_METHOD(0x4BC2E2A0ED3A2BF1LL, emptystate);
      break;
    case 18:
      HASH_INVOKE_STATIC_METHOD(0x5019D82296535452LL, unpressurizedfullstate);
      break;
    case 19:
      HASH_INVOKE_STATIC_METHOD(0x54F1124FE4916573LL, pressurizedbottlestate);
      HASH_INVOKE_STATIC_METHOD(0x5BCFA27E9B0220F3LL, bottle);
      break;
    case 25:
      HASH_INVOKE_STATIC_METHOD(0x3D1E59FDF30743F9LL, pressurizedunsealedstate);
      break;
    case 28:
      HASH_INVOKE_STATIC_METHOD(0x35A97CB4B58D729CLL, fullstate);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GET_STATIC_PROPERTY(0x0809D5798D9C5E85LL, unpressurizedemptystate);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x217481FA00625FA9LL, bottlestate);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY(0x6E20EB05593849ABLL, sealedstate);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY(0x167813DEEE2C994ELL, pressurizedsealedstate);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x72571C43F7CB256FLL, pressurizedbottle);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY(0x4BC2E2A0ED3A2BF1LL, emptystate);
      break;
    case 18:
      HASH_GET_STATIC_PROPERTY(0x5019D82296535452LL, unpressurizedfullstate);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY(0x54F1124FE4916573LL, pressurizedbottlestate);
      HASH_GET_STATIC_PROPERTY(0x5BCFA27E9B0220F3LL, bottle);
      break;
    case 25:
      HASH_GET_STATIC_PROPERTY(0x3D1E59FDF30743F9LL, pressurizedunsealedstate);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY(0x35A97CB4B58D729CLL, fullstate);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x0809D5798D9C5E85LL, unpressurizedemptystate);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x217481FA00625FA9LL, bottlestate);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY_LV(0x6E20EB05593849ABLL, sealedstate);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY_LV(0x167813DEEE2C994ELL, pressurizedsealedstate);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x72571C43F7CB256FLL, pressurizedbottle);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY_LV(0x4BC2E2A0ED3A2BF1LL, emptystate);
      break;
    case 18:
      HASH_GET_STATIC_PROPERTY_LV(0x5019D82296535452LL, unpressurizedfullstate);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY_LV(0x54F1124FE4916573LL, pressurizedbottlestate);
      HASH_GET_STATIC_PROPERTY_LV(0x5BCFA27E9B0220F3LL, bottle);
      break;
    case 25:
      HASH_GET_STATIC_PROPERTY_LV(0x3D1E59FDF30743F9LL, pressurizedunsealedstate);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY_LV(0x35A97CB4B58D729CLL, fullstate);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GET_CLASS_CONSTANT(0x0809D5798D9C5E85LL, unpressurizedemptystate);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x217481FA00625FA9LL, bottlestate);
      break;
    case 11:
      HASH_GET_CLASS_CONSTANT(0x6E20EB05593849ABLL, sealedstate);
      break;
    case 14:
      HASH_GET_CLASS_CONSTANT(0x167813DEEE2C994ELL, pressurizedsealedstate);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x72571C43F7CB256FLL, pressurizedbottle);
      break;
    case 17:
      HASH_GET_CLASS_CONSTANT(0x4BC2E2A0ED3A2BF1LL, emptystate);
      break;
    case 18:
      HASH_GET_CLASS_CONSTANT(0x5019D82296535452LL, unpressurizedfullstate);
      break;
    case 19:
      HASH_GET_CLASS_CONSTANT(0x54F1124FE4916573LL, pressurizedbottlestate);
      HASH_GET_CLASS_CONSTANT(0x5BCFA27E9B0220F3LL, bottle);
      break;
    case 25:
      HASH_GET_CLASS_CONSTANT(0x3D1E59FDF30743F9LL, pressurizedunsealedstate);
      break;
    case 28:
      HASH_GET_CLASS_CONSTANT(0x35A97CB4B58D729CLL, fullstate);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
