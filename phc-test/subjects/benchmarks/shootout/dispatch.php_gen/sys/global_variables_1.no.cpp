
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 2:
      HASH_RETURN(0x4024203A247E4F82LL, g->GV(Empty),
                  Empty);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 7:
      HASH_RETURN(0x5AEC3B20C03ED687LL, g->GV(p7),
                  p7);
      break;
    case 9:
      HASH_RETURN(0x117B8667E4662809LL, g->GV(n),
                  n);
      HASH_RETURN(0x345DF79AB6FB6B09LL, g->GV(b8),
                  b8);
      break;
    case 13:
      HASH_RETURN(0x443E7D9C98EDF18DLL, g->GV(b0),
                  b0);
      HASH_RETURN(0x7712320985A0560DLL, g->GV(p5),
                  p5);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x4212288C24593E93LL, g->GV(b7),
                  b7);
      break;
    case 24:
      HASH_RETURN(0x2D5185583EF85E98LL, g->GV(b2),
                  b2);
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 25:
      HASH_RETURN(0x6BB2D14D1B3F4A99LL, g->GV(PressurizedUnsealed),
                  PressurizedUnsealed);
      break;
    case 26:
      HASH_RETURN(0x0389CB8009D23A1ALL, g->GV(b9),
                  b9);
      break;
    case 30:
      HASH_RETURN(0x18B6B61EF19E261ELL, g->GV(p3),
                  p3);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 37:
      HASH_RETURN(0x0BF8E1FFDA30F125LL, g->GV(UnpressurizedFull),
                  UnpressurizedFull);
      break;
    case 46:
      HASH_RETURN(0x226516ED20BAAFAELL, g->GV(UnpressurizedEmpty),
                  UnpressurizedEmpty);
      break;
    case 47:
      HASH_RETURN(0x1A669C137C2C602FLL, g->GV(check),
                  check);
      break;
    case 60:
      HASH_RETURN(0x4F0B2D89550857BCLL, g->GV(p9),
                  p9);
      break;
    case 62:
      HASH_RETURN(0x7ADD4C161B321ABELL, g->GV(p6),
                  p6);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 64:
      HASH_RETURN(0x6446E25DA07A12C0LL, g->GV(p0),
                  p0);
      break;
    case 66:
      HASH_RETURN(0x08424A303B976142LL, g->GV(b4),
                  b4);
      break;
    case 70:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 71:
      HASH_RETURN(0x0FEFA20DB1B08AC7LL, g->GV(p2),
                  p2);
      HASH_RETURN(0x64845A35C3C3C247LL, g->GV(p8),
                  p8);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x2A6886999C5D01C9LL, g->GV(Full),
                  Full);
      break;
    case 81:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 84:
      HASH_RETURN(0x2D22A77B66D66AD4LL, g->GV(Sealed),
                  Sealed);
      break;
    case 94:
      HASH_RETURN(0x5F4AE7AA1641D45ELL, g->GV(b3),
                  b3);
      break;
    case 107:
      HASH_RETURN(0x29C390281340176BLL, g->GV(b1),
                  b1);
      break;
    case 108:
      HASH_RETURN(0x561CEFF2C0D9ADECLL, g->GV(PressurizedSealed),
                  PressurizedSealed);
      HASH_RETURN(0x20511842520A276CLL, g->GV(p4),
                  p4);
      break;
    case 109:
      HASH_RETURN(0x7E82E1E7C641086DLL, g->GV(b6),
                  b6);
      break;
    case 110:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 119:
      HASH_RETURN(0x5C657D380DA684F7LL, g->GV(p1),
                  p1);
      break;
    case 121:
      HASH_RETURN(0x0B0A60EB541C6C79LL, g->GV(b5),
                  b5);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
