
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_dispatch_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_dispatch_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/shootout/dispatch.fw.h>

// Declarations
#include <cls/pressurizedunsealedstate.h>
#include <cls/sealedstate.h>
#include <cls/bottlestate.h>
#include <cls/unpressurizedfullstate.h>
#include <cls/pressurizedsealedstate.h>
#include <cls/pressurizedbottle.h>
#include <cls/unpressurizedemptystate.h>
#include <cls/pressurizedbottlestate.h>
#include <cls/emptystate.h>
#include <cls/bottle.h>
#include <cls/fullstate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$shootout$dispatch_php(bool incOnce = false, LVariableTable* variables = NULL);
PlusOperand f_bottlecheck(CVarRef v_a1, CVarRef v_a2, CVarRef v_a3, CVarRef v_a4, CVarRef v_a5, CVarRef v_i);
Object co_pressurizedunsealedstate(CArrRef params, bool init = true);
Object co_sealedstate(CArrRef params, bool init = true);
Object co_bottlestate(CArrRef params, bool init = true);
Object co_unpressurizedfullstate(CArrRef params, bool init = true);
Object co_pressurizedsealedstate(CArrRef params, bool init = true);
Object co_pressurizedbottle(CArrRef params, bool init = true);
Object co_unpressurizedemptystate(CArrRef params, bool init = true);
Object co_pressurizedbottlestate(CArrRef params, bool init = true);
Object co_emptystate(CArrRef params, bool init = true);
Object co_bottle(CArrRef params, bool init = true);
Object co_fullstate(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_dispatch_h__
