
#include <php/phc-test/subjects/benchmarks/shootout/dispatch.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 52 */
Variant c_pressurizedunsealedstate::os_get(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_get(s, hash);
}
Variant &c_pressurizedunsealedstate::os_lval(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_lval(s, hash);
}
void c_pressurizedunsealedstate::o_get(ArrayElementVec &props) const {
  c_pressurizedbottlestate::o_get(props);
}
bool c_pressurizedunsealedstate::o_exists(CStrRef s, int64 hash) const {
  return c_pressurizedbottlestate::o_exists(s, hash);
}
Variant c_pressurizedunsealedstate::o_get(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_get(s, hash);
}
Variant c_pressurizedunsealedstate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_pressurizedbottlestate::o_set(s, hash, v, forInit);
}
Variant &c_pressurizedunsealedstate::o_lval(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_lval(s, hash);
}
Variant c_pressurizedunsealedstate::os_constant(const char *s) {
  return c_pressurizedbottlestate::os_constant(s);
}
IMPLEMENT_CLASS(pressurizedunsealedstate)
ObjectData *c_pressurizedunsealedstate::cloneImpl() {
  c_pressurizedunsealedstate *obj = NEW(c_pressurizedunsealedstate)();
  cloneSet(obj);
  return obj;
}
void c_pressurizedunsealedstate::cloneSet(c_pressurizedunsealedstate *clone) {
  c_pressurizedbottlestate::cloneSet(clone);
}
Variant c_pressurizedunsealedstate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_pressurizedunsealedstate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pressurizedunsealedstate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pressurizedunsealedstate$os_get(const char *s) {
  return c_pressurizedunsealedstate::os_get(s, -1);
}
Variant &cw_pressurizedunsealedstate$os_lval(const char *s) {
  return c_pressurizedunsealedstate::os_lval(s, -1);
}
Variant cw_pressurizedunsealedstate$os_constant(const char *s) {
  return c_pressurizedunsealedstate::os_constant(s);
}
Variant cw_pressurizedunsealedstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pressurizedunsealedstate::os_invoke(c, s, params, -1, fatal);
}
void c_pressurizedunsealedstate::init() {
  c_pressurizedbottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 53 */
void c_pressurizedunsealedstate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(PressurizedUnsealedState, PressurizedUnsealedState::Next);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_PressurizedSealed __attribute__((__unused__)) = g->GV(PressurizedSealed);
  LINE(53,toObject(v_b)->o_invoke_few_args("State", 0x703DF16E6CB95FBFLL, 1, gv_PressurizedSealed));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 54 */
int64 c_pressurizedunsealedstate::t_tag() {
  INSTANCE_METHOD_INJECTION(PressurizedUnsealedState, PressurizedUnsealedState::Tag);
  return 6LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 28 */
Variant c_sealedstate::os_get(const char *s, int64 hash) {
  return c_bottlestate::os_get(s, hash);
}
Variant &c_sealedstate::os_lval(const char *s, int64 hash) {
  return c_bottlestate::os_lval(s, hash);
}
void c_sealedstate::o_get(ArrayElementVec &props) const {
  c_bottlestate::o_get(props);
}
bool c_sealedstate::o_exists(CStrRef s, int64 hash) const {
  return c_bottlestate::o_exists(s, hash);
}
Variant c_sealedstate::o_get(CStrRef s, int64 hash) {
  return c_bottlestate::o_get(s, hash);
}
Variant c_sealedstate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_bottlestate::o_set(s, hash, v, forInit);
}
Variant &c_sealedstate::o_lval(CStrRef s, int64 hash) {
  return c_bottlestate::o_lval(s, hash);
}
Variant c_sealedstate::os_constant(const char *s) {
  return c_bottlestate::os_constant(s);
}
IMPLEMENT_CLASS(sealedstate)
ObjectData *c_sealedstate::cloneImpl() {
  c_sealedstate *obj = NEW(c_sealedstate)();
  cloneSet(obj);
  return obj;
}
void c_sealedstate::cloneSet(c_sealedstate *clone) {
  c_bottlestate::cloneSet(clone);
}
Variant c_sealedstate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_sealedstate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sealedstate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_bottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sealedstate$os_get(const char *s) {
  return c_sealedstate::os_get(s, -1);
}
Variant &cw_sealedstate$os_lval(const char *s) {
  return c_sealedstate::os_lval(s, -1);
}
Variant cw_sealedstate$os_constant(const char *s) {
  return c_sealedstate::os_constant(s);
}
Variant cw_sealedstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sealedstate::os_invoke(c, s, params, -1, fatal);
}
void c_sealedstate::init() {
  c_bottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 29 */
void c_sealedstate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(SealedState, SealedState::Next);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_Empty __attribute__((__unused__)) = g->GV(Empty);
  LINE(29,toObject(v_b)->o_invoke_few_args("State", 0x703DF16E6CB95FBFLL, 1, gv_Empty));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 30 */
int64 c_sealedstate::t_tag() {
  INSTANCE_METHOD_INJECTION(SealedState, SealedState::Tag);
  return 3LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 11 */
Variant c_bottlestate::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bottlestate::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bottlestate::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bottlestate::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bottlestate::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bottlestate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bottlestate::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bottlestate::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bottlestate)
ObjectData *c_bottlestate::cloneImpl() {
  c_bottlestate *obj = NEW(c_bottlestate)();
  cloneSet(obj);
  return obj;
}
void c_bottlestate::cloneSet(c_bottlestate *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bottlestate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bottlestate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bottlestate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bottlestate$os_get(const char *s) {
  return c_bottlestate::os_get(s, -1);
}
Variant &cw_bottlestate$os_lval(const char *s) {
  return c_bottlestate::os_lval(s, -1);
}
Variant cw_bottlestate$os_constant(const char *s) {
  return c_bottlestate::os_constant(s);
}
Variant cw_bottlestate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bottlestate::os_invoke(c, s, params, -1, fatal);
}
void c_bottlestate::init() {
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 12 */
void c_bottlestate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(BottleState, BottleState::Next);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 13 */
void c_bottlestate::t_tag() {
  INSTANCE_METHOD_INJECTION(BottleState, BottleState::Tag);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 15 */
Variant c_bottlestate::ti_initialstate(const char* cls) {
  STATIC_METHOD_INJECTION(BottleState, BottleState::InitialState);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_Empty __attribute__((__unused__)) = g->GV(Empty);
  return gv_Empty;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 47 */
Variant c_unpressurizedfullstate::os_get(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_get(s, hash);
}
Variant &c_unpressurizedfullstate::os_lval(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_lval(s, hash);
}
void c_unpressurizedfullstate::o_get(ArrayElementVec &props) const {
  c_pressurizedbottlestate::o_get(props);
}
bool c_unpressurizedfullstate::o_exists(CStrRef s, int64 hash) const {
  return c_pressurizedbottlestate::o_exists(s, hash);
}
Variant c_unpressurizedfullstate::o_get(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_get(s, hash);
}
Variant c_unpressurizedfullstate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_pressurizedbottlestate::o_set(s, hash, v, forInit);
}
Variant &c_unpressurizedfullstate::o_lval(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_lval(s, hash);
}
Variant c_unpressurizedfullstate::os_constant(const char *s) {
  return c_pressurizedbottlestate::os_constant(s);
}
IMPLEMENT_CLASS(unpressurizedfullstate)
ObjectData *c_unpressurizedfullstate::cloneImpl() {
  c_unpressurizedfullstate *obj = NEW(c_unpressurizedfullstate)();
  cloneSet(obj);
  return obj;
}
void c_unpressurizedfullstate::cloneSet(c_unpressurizedfullstate *clone) {
  c_pressurizedbottlestate::cloneSet(clone);
}
Variant c_unpressurizedfullstate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_unpressurizedfullstate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_unpressurizedfullstate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_unpressurizedfullstate$os_get(const char *s) {
  return c_unpressurizedfullstate::os_get(s, -1);
}
Variant &cw_unpressurizedfullstate$os_lval(const char *s) {
  return c_unpressurizedfullstate::os_lval(s, -1);
}
Variant cw_unpressurizedfullstate$os_constant(const char *s) {
  return c_unpressurizedfullstate::os_constant(s);
}
Variant cw_unpressurizedfullstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_unpressurizedfullstate::os_invoke(c, s, params, -1, fatal);
}
void c_unpressurizedfullstate::init() {
  c_pressurizedbottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 48 */
void c_unpressurizedfullstate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(UnpressurizedFullState, UnpressurizedFullState::Next);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_PressurizedUnsealed __attribute__((__unused__)) = g->GV(PressurizedUnsealed);
  LINE(48,toObject(v_b)->o_invoke_few_args("State", 0x703DF16E6CB95FBFLL, 1, gv_PressurizedUnsealed));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 49 */
int64 c_unpressurizedfullstate::t_tag() {
  INSTANCE_METHOD_INJECTION(UnpressurizedFullState, UnpressurizedFullState::Tag);
  return 5LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 57 */
Variant c_pressurizedsealedstate::os_get(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_get(s, hash);
}
Variant &c_pressurizedsealedstate::os_lval(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_lval(s, hash);
}
void c_pressurizedsealedstate::o_get(ArrayElementVec &props) const {
  c_pressurizedbottlestate::o_get(props);
}
bool c_pressurizedsealedstate::o_exists(CStrRef s, int64 hash) const {
  return c_pressurizedbottlestate::o_exists(s, hash);
}
Variant c_pressurizedsealedstate::o_get(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_get(s, hash);
}
Variant c_pressurizedsealedstate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_pressurizedbottlestate::o_set(s, hash, v, forInit);
}
Variant &c_pressurizedsealedstate::o_lval(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_lval(s, hash);
}
Variant c_pressurizedsealedstate::os_constant(const char *s) {
  return c_pressurizedbottlestate::os_constant(s);
}
IMPLEMENT_CLASS(pressurizedsealedstate)
ObjectData *c_pressurizedsealedstate::cloneImpl() {
  c_pressurizedsealedstate *obj = NEW(c_pressurizedsealedstate)();
  cloneSet(obj);
  return obj;
}
void c_pressurizedsealedstate::cloneSet(c_pressurizedsealedstate *clone) {
  c_pressurizedbottlestate::cloneSet(clone);
}
Variant c_pressurizedsealedstate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_pressurizedsealedstate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pressurizedsealedstate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pressurizedsealedstate$os_get(const char *s) {
  return c_pressurizedsealedstate::os_get(s, -1);
}
Variant &cw_pressurizedsealedstate$os_lval(const char *s) {
  return c_pressurizedsealedstate::os_lval(s, -1);
}
Variant cw_pressurizedsealedstate$os_constant(const char *s) {
  return c_pressurizedsealedstate::os_constant(s);
}
Variant cw_pressurizedsealedstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pressurizedsealedstate::os_invoke(c, s, params, -1, fatal);
}
void c_pressurizedsealedstate::init() {
  c_pressurizedbottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 58 */
void c_pressurizedsealedstate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(PressurizedSealedState, PressurizedSealedState::Next);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_UnpressurizedEmpty __attribute__((__unused__)) = g->GV(UnpressurizedEmpty);
  LINE(58,toObject(v_b)->o_invoke_few_args("State", 0x703DF16E6CB95FBFLL, 1, gv_UnpressurizedEmpty));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 59 */
int64 c_pressurizedsealedstate::t_tag() {
  INSTANCE_METHOD_INJECTION(PressurizedSealedState, PressurizedSealedState::Tag);
  return 7LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 106 */
Variant c_pressurizedbottle::os_get(const char *s, int64 hash) {
  return c_bottle::os_get(s, hash);
}
Variant &c_pressurizedbottle::os_lval(const char *s, int64 hash) {
  return c_bottle::os_lval(s, hash);
}
void c_pressurizedbottle::o_get(ArrayElementVec &props) const {
  c_bottle::o_get(props);
}
bool c_pressurizedbottle::o_exists(CStrRef s, int64 hash) const {
  return c_bottle::o_exists(s, hash);
}
Variant c_pressurizedbottle::o_get(CStrRef s, int64 hash) {
  return c_bottle::o_get(s, hash);
}
Variant c_pressurizedbottle::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_bottle::o_set(s, hash, v, forInit);
}
Variant &c_pressurizedbottle::o_lval(CStrRef s, int64 hash) {
  return c_bottle::o_lval(s, hash);
}
Variant c_pressurizedbottle::os_constant(const char *s) {
  return c_bottle::os_constant(s);
}
IMPLEMENT_CLASS(pressurizedbottle)
ObjectData *c_pressurizedbottle::create(int64 v_id) {
  init();
  t_pressurizedbottle(v_id);
  return this;
}
ObjectData *c_pressurizedbottle::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_pressurizedbottle::cloneImpl() {
  c_pressurizedbottle *obj = NEW(c_pressurizedbottle)();
  cloneSet(obj);
  return obj;
}
void c_pressurizedbottle::cloneSet(c_pressurizedbottle *clone) {
  c_bottle::cloneSet(clone);
}
Variant c_pressurizedbottle::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x66157E9B44087AEBLL, cycle) {
        return (t_cycle(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (t_initialstate());
      }
      break;
    case 5:
      HASH_GUARD(0x37DF5A8F4136C705LL, check) {
        return (t_check(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x703DF16E6CB95FBFLL, state) {
        return (t_state(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_bottle::o_invoke(s, params, hash, fatal);
}
Variant c_pressurizedbottle::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x66157E9B44087AEBLL, cycle) {
        return (t_cycle(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (t_initialstate());
      }
      break;
    case 5:
      HASH_GUARD(0x37DF5A8F4136C705LL, check) {
        return (t_check(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x703DF16E6CB95FBFLL, state) {
        return (t_state(a0), null);
      }
      break;
    default:
      break;
  }
  return c_bottle::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pressurizedbottle::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_bottle::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pressurizedbottle$os_get(const char *s) {
  return c_pressurizedbottle::os_get(s, -1);
}
Variant &cw_pressurizedbottle$os_lval(const char *s) {
  return c_pressurizedbottle::os_lval(s, -1);
}
Variant cw_pressurizedbottle$os_constant(const char *s) {
  return c_pressurizedbottle::os_constant(s);
}
Variant cw_pressurizedbottle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pressurizedbottle::os_invoke(c, s, params, -1, fatal);
}
void c_pressurizedbottle::init() {
  c_bottle::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 108 */
void c_pressurizedbottle::t_pressurizedbottle(int64 v_id) {
  INSTANCE_METHOD_INJECTION(PressurizedBottle, PressurizedBottle::PressurizedBottle);
  bool oldInCtor = gasInCtor(true);
  LINE(109,c_bottle::t_bottle(v_id));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 112 */
Variant c_pressurizedbottle::t_initialstate() {
  INSTANCE_METHOD_INJECTION(PressurizedBottle, PressurizedBottle::InitialState);
  return LINE(113,c_pressurizedbottlestate::t_initialstate());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 116 */
void c_pressurizedbottle::t_cycle() {
  INSTANCE_METHOD_INJECTION(PressurizedBottle, PressurizedBottle::Cycle);
  LINE(117,t_fill());
  t_pressurize();
  t_seal();
  t_empty_();
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 120 */
void c_pressurizedbottle::t_pressurize() {
  INSTANCE_METHOD_INJECTION(PressurizedBottle, PressurizedBottle::Pressurize);
  LINE(121,m_bottleState.o_invoke_few_args("Next", 0x3C6D50F3BB8102B8LL, 1, this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 42 */
Variant c_unpressurizedemptystate::os_get(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_get(s, hash);
}
Variant &c_unpressurizedemptystate::os_lval(const char *s, int64 hash) {
  return c_pressurizedbottlestate::os_lval(s, hash);
}
void c_unpressurizedemptystate::o_get(ArrayElementVec &props) const {
  c_pressurizedbottlestate::o_get(props);
}
bool c_unpressurizedemptystate::o_exists(CStrRef s, int64 hash) const {
  return c_pressurizedbottlestate::o_exists(s, hash);
}
Variant c_unpressurizedemptystate::o_get(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_get(s, hash);
}
Variant c_unpressurizedemptystate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_pressurizedbottlestate::o_set(s, hash, v, forInit);
}
Variant &c_unpressurizedemptystate::o_lval(CStrRef s, int64 hash) {
  return c_pressurizedbottlestate::o_lval(s, hash);
}
Variant c_unpressurizedemptystate::os_constant(const char *s) {
  return c_pressurizedbottlestate::os_constant(s);
}
IMPLEMENT_CLASS(unpressurizedemptystate)
ObjectData *c_unpressurizedemptystate::cloneImpl() {
  c_unpressurizedemptystate *obj = NEW(c_unpressurizedemptystate)();
  cloneSet(obj);
  return obj;
}
void c_unpressurizedemptystate::cloneSet(c_unpressurizedemptystate *clone) {
  c_pressurizedbottlestate::cloneSet(clone);
}
Variant c_unpressurizedemptystate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_unpressurizedemptystate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_unpressurizedemptystate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_pressurizedbottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_unpressurizedemptystate$os_get(const char *s) {
  return c_unpressurizedemptystate::os_get(s, -1);
}
Variant &cw_unpressurizedemptystate$os_lval(const char *s) {
  return c_unpressurizedemptystate::os_lval(s, -1);
}
Variant cw_unpressurizedemptystate$os_constant(const char *s) {
  return c_unpressurizedemptystate::os_constant(s);
}
Variant cw_unpressurizedemptystate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_unpressurizedemptystate::os_invoke(c, s, params, -1, fatal);
}
void c_unpressurizedemptystate::init() {
  c_pressurizedbottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 43 */
void c_unpressurizedemptystate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(UnpressurizedEmptyState, UnpressurizedEmptyState::Next);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_UnpressurizedFull __attribute__((__unused__)) = g->GV(UnpressurizedFull);
  LINE(43,toObject(v_b)->o_invoke_few_args("State", 0x703DF16E6CB95FBFLL, 1, gv_UnpressurizedFull));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 44 */
int64 c_unpressurizedemptystate::t_tag() {
  INSTANCE_METHOD_INJECTION(UnpressurizedEmptyState, UnpressurizedEmptyState::Tag);
  return 4LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 38 */
Variant c_pressurizedbottlestate::os_get(const char *s, int64 hash) {
  return c_bottlestate::os_get(s, hash);
}
Variant &c_pressurizedbottlestate::os_lval(const char *s, int64 hash) {
  return c_bottlestate::os_lval(s, hash);
}
void c_pressurizedbottlestate::o_get(ArrayElementVec &props) const {
  c_bottlestate::o_get(props);
}
bool c_pressurizedbottlestate::o_exists(CStrRef s, int64 hash) const {
  return c_bottlestate::o_exists(s, hash);
}
Variant c_pressurizedbottlestate::o_get(CStrRef s, int64 hash) {
  return c_bottlestate::o_get(s, hash);
}
Variant c_pressurizedbottlestate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_bottlestate::o_set(s, hash, v, forInit);
}
Variant &c_pressurizedbottlestate::o_lval(CStrRef s, int64 hash) {
  return c_bottlestate::o_lval(s, hash);
}
Variant c_pressurizedbottlestate::os_constant(const char *s) {
  return c_bottlestate::os_constant(s);
}
IMPLEMENT_CLASS(pressurizedbottlestate)
ObjectData *c_pressurizedbottlestate::cloneImpl() {
  c_pressurizedbottlestate *obj = NEW(c_pressurizedbottlestate)();
  cloneSet(obj);
  return obj;
}
void c_pressurizedbottlestate::cloneSet(c_pressurizedbottlestate *clone) {
  c_bottlestate::cloneSet(clone);
}
Variant c_pressurizedbottlestate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag(), null);
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_pressurizedbottlestate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag(), null);
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pressurizedbottlestate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_bottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pressurizedbottlestate$os_get(const char *s) {
  return c_pressurizedbottlestate::os_get(s, -1);
}
Variant &cw_pressurizedbottlestate$os_lval(const char *s) {
  return c_pressurizedbottlestate::os_lval(s, -1);
}
Variant cw_pressurizedbottlestate$os_constant(const char *s) {
  return c_pressurizedbottlestate::os_constant(s);
}
Variant cw_pressurizedbottlestate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pressurizedbottlestate::os_invoke(c, s, params, -1, fatal);
}
void c_pressurizedbottlestate::init() {
  c_bottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 39 */
Variant c_pressurizedbottlestate::ti_initialstate(const char* cls) {
  STATIC_METHOD_INJECTION(PressurizedBottleState, PressurizedBottleState::InitialState);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_UnpressurizedEmpty __attribute__((__unused__)) = g->GV(UnpressurizedEmpty);
  return gv_UnpressurizedEmpty;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 18 */
Variant c_emptystate::os_get(const char *s, int64 hash) {
  return c_bottlestate::os_get(s, hash);
}
Variant &c_emptystate::os_lval(const char *s, int64 hash) {
  return c_bottlestate::os_lval(s, hash);
}
void c_emptystate::o_get(ArrayElementVec &props) const {
  c_bottlestate::o_get(props);
}
bool c_emptystate::o_exists(CStrRef s, int64 hash) const {
  return c_bottlestate::o_exists(s, hash);
}
Variant c_emptystate::o_get(CStrRef s, int64 hash) {
  return c_bottlestate::o_get(s, hash);
}
Variant c_emptystate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_bottlestate::o_set(s, hash, v, forInit);
}
Variant &c_emptystate::o_lval(CStrRef s, int64 hash) {
  return c_bottlestate::o_lval(s, hash);
}
Variant c_emptystate::os_constant(const char *s) {
  return c_bottlestate::os_constant(s);
}
IMPLEMENT_CLASS(emptystate)
ObjectData *c_emptystate::cloneImpl() {
  c_emptystate *obj = NEW(c_emptystate)();
  cloneSet(obj);
  return obj;
}
void c_emptystate::cloneSet(c_emptystate *clone) {
  c_bottlestate::cloneSet(clone);
}
Variant c_emptystate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_emptystate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_emptystate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_bottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_emptystate$os_get(const char *s) {
  return c_emptystate::os_get(s, -1);
}
Variant &cw_emptystate$os_lval(const char *s) {
  return c_emptystate::os_lval(s, -1);
}
Variant cw_emptystate$os_constant(const char *s) {
  return c_emptystate::os_constant(s);
}
Variant cw_emptystate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_emptystate::os_invoke(c, s, params, -1, fatal);
}
void c_emptystate::init() {
  c_bottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 19 */
void c_emptystate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(EmptyState, EmptyState::Next);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_Full __attribute__((__unused__)) = g->GV(Full);
  LINE(19,toObject(v_b)->o_invoke_few_args("State", 0x703DF16E6CB95FBFLL, 1, gv_Full));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 20 */
int64 c_emptystate::t_tag() {
  INSTANCE_METHOD_INJECTION(EmptyState, EmptyState::Tag);
  return 1LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 68 */
Variant c_bottle::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bottle::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bottle::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("bottleState", m_bottleState.isReferenced() ? ref(m_bottleState) : m_bottleState));
  props.push_back(NEW(ArrayElement)("id", m_id.isReferenced() ? ref(m_id) : m_id));
  c_ObjectData::o_get(props);
}
bool c_bottle::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x54690C7F5D024F3CLL, bottleState, 11);
      break;
    case 2:
      HASH_EXISTS_STRING(0x028B9FE0C4522BE2LL, id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bottle::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x54690C7F5D024F3CLL, m_bottleState,
                         bottleState, 11);
      break;
    case 2:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_bottle::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x54690C7F5D024F3CLL, m_bottleState,
                      bottleState, 11);
      break;
    case 2:
      HASH_SET_STRING(0x028B9FE0C4522BE2LL, m_id,
                      id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bottle::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x54690C7F5D024F3CLL, m_bottleState,
                         bottleState, 11);
      break;
    case 2:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bottle::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bottle)
ObjectData *c_bottle::create(int64 v_id) {
  init();
  t_bottle(v_id);
  return this;
}
ObjectData *c_bottle::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_bottle::cloneImpl() {
  c_bottle *obj = NEW(c_bottle)();
  cloneSet(obj);
  return obj;
}
void c_bottle::cloneSet(c_bottle *clone) {
  clone->m_bottleState = m_bottleState.isReferenced() ? ref(m_bottleState) : m_bottleState;
  clone->m_id = m_id.isReferenced() ? ref(m_id) : m_id;
  ObjectData::cloneSet(clone);
}
Variant c_bottle::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x66157E9B44087AEBLL, cycle) {
        return (t_cycle(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (t_initialstate());
      }
      break;
    case 5:
      HASH_GUARD(0x37DF5A8F4136C705LL, check) {
        return (t_check(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x703DF16E6CB95FBFLL, state) {
        return (t_state(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bottle::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x66157E9B44087AEBLL, cycle) {
        return (t_cycle(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (t_initialstate());
      }
      break;
    case 5:
      HASH_GUARD(0x37DF5A8F4136C705LL, check) {
        return (t_check(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x703DF16E6CB95FBFLL, state) {
        return (t_state(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bottle::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bottle$os_get(const char *s) {
  return c_bottle::os_get(s, -1);
}
Variant &cw_bottle$os_lval(const char *s) {
  return c_bottle::os_lval(s, -1);
}
Variant cw_bottle$os_constant(const char *s) {
  return c_bottle::os_constant(s);
}
Variant cw_bottle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bottle::os_invoke(c, s, params, -1, fatal);
}
void c_bottle::init() {
  m_bottleState = null;
  m_id = null;
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 71 */
void c_bottle::t_bottle(int64 v_id) {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::Bottle);
  bool oldInCtor = gasInCtor(true);
  (m_id = v_id);
  (m_bottleState = LINE(73,o_root_invoke_few_args("InitialState", 0x29C6A9CB7AB8F19CLL, 0)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 76 */
void c_bottle::t_state(CVarRef v_s) {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::State);
  (m_bottleState = v_s);
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 80 */
void c_bottle::t_cycle() {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::Cycle);
  LINE(81,t_fill());
  t_seal();
  t_empty_();
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 84 */
Variant c_bottle::t_initialstate() {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::InitialState);
  return LINE(85,c_bottlestate::t_initialstate());
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 88 */
void c_bottle::t_fill() {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::Fill);
  LINE(89,m_bottleState.o_invoke_few_args("Next", 0x3C6D50F3BB8102B8LL, 1, this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 92 */
void c_bottle::t_seal() {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::Seal);
  LINE(93,m_bottleState.o_invoke_few_args("Next", 0x3C6D50F3BB8102B8LL, 1, this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 96 */
void c_bottle::t_empty_() {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::Empty_);
  LINE(97,m_bottleState.o_invoke_few_args("Next", 0x3C6D50F3BB8102B8LL, 1, this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 100 */
PlusOperand c_bottle::t_check(CVarRef v_c) {
  INSTANCE_METHOD_INJECTION(Bottle, Bottle::Check);
  return LINE(101,m_bottleState.o_invoke_few_args("Tag", 0x6D9B45F7265AC725LL, 0)) + m_id + v_c;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 23 */
Variant c_fullstate::os_get(const char *s, int64 hash) {
  return c_bottlestate::os_get(s, hash);
}
Variant &c_fullstate::os_lval(const char *s, int64 hash) {
  return c_bottlestate::os_lval(s, hash);
}
void c_fullstate::o_get(ArrayElementVec &props) const {
  c_bottlestate::o_get(props);
}
bool c_fullstate::o_exists(CStrRef s, int64 hash) const {
  return c_bottlestate::o_exists(s, hash);
}
Variant c_fullstate::o_get(CStrRef s, int64 hash) {
  return c_bottlestate::o_get(s, hash);
}
Variant c_fullstate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_bottlestate::o_set(s, hash, v, forInit);
}
Variant &c_fullstate::o_lval(CStrRef s, int64 hash) {
  return c_bottlestate::o_lval(s, hash);
}
Variant c_fullstate::os_constant(const char *s) {
  return c_bottlestate::os_constant(s);
}
IMPLEMENT_CLASS(fullstate)
ObjectData *c_fullstate::cloneImpl() {
  c_fullstate *obj = NEW(c_fullstate)();
  cloneSet(obj);
  return obj;
}
void c_fullstate::cloneSet(c_fullstate *clone) {
  c_bottlestate::cloneSet(clone);
}
Variant c_fullstate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(params.rvalAt(0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke(s, params, hash, fatal);
}
Variant c_fullstate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next(a0), null);
      }
      break;
    case 4:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(o_getClassName()));
      }
      break;
    case 5:
      HASH_GUARD(0x6D9B45F7265AC725LL, tag) {
        return (t_tag());
      }
      break;
    default:
      break;
  }
  return c_bottlestate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fullstate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x29C6A9CB7AB8F19CLL, initialstate) {
        return (ti_initialstate(c));
      }
      break;
    default:
      break;
  }
  return c_bottlestate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fullstate$os_get(const char *s) {
  return c_fullstate::os_get(s, -1);
}
Variant &cw_fullstate$os_lval(const char *s) {
  return c_fullstate::os_lval(s, -1);
}
Variant cw_fullstate$os_constant(const char *s) {
  return c_fullstate::os_constant(s);
}
Variant cw_fullstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fullstate::os_invoke(c, s, params, -1, fatal);
}
void c_fullstate::init() {
  c_bottlestate::init();
}
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 24 */
void c_fullstate::t_next(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(FullState, FullState::Next);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_Sealed __attribute__((__unused__)) = g->GV(Sealed);
  LINE(24,toObject(v_b)->o_invoke_few_args("State", 0x703DF16E6CB95FBFLL, 1, gv_Sealed));
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 25 */
int64 c_fullstate::t_tag() {
  INSTANCE_METHOD_INJECTION(FullState, FullState::Tag);
  return 2LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/shootout/dispatch.php line 126 */
PlusOperand f_bottlecheck(CVarRef v_a1, CVarRef v_a2, CVarRef v_a3, CVarRef v_a4, CVarRef v_a5, CVarRef v_i) {
  FUNCTION_INJECTION(BottleCheck);
  Variant v_c;

  LINE(127,toObject(v_a1)->o_invoke_few_args("Cycle", 0x66157E9B44087AEBLL, 0));
  toObject(v_a2)->o_invoke_few_args("Cycle", 0x66157E9B44087AEBLL, 0);
  toObject(v_a3)->o_invoke_few_args("Cycle", 0x66157E9B44087AEBLL, 0);
  toObject(v_a4)->o_invoke_few_args("Cycle", 0x66157E9B44087AEBLL, 0);
  toObject(v_a5)->o_invoke_few_args("Cycle", 0x66157E9B44087AEBLL, 0);
  (v_c = modulo(toInt64(v_i), 2LL));
  return plus_rev(LINE(130,toObject(v_a5)->o_invoke_few_args("Check", 0x37DF5A8F4136C705LL, 1, v_c)), plus_rev(toObject(v_a4)->o_invoke_few_args("Check", 0x37DF5A8F4136C705LL, 1, v_c), plus_rev(LINE(129,toObject(v_a3)->o_invoke_few_args("Check", 0x37DF5A8F4136C705LL, 1, v_c)), plus_rev(toObject(v_a2)->o_invoke_few_args("Check", 0x37DF5A8F4136C705LL, 1, v_c), toObject(v_a1)->o_invoke_few_args("Check", 0x37DF5A8F4136C705LL, 1, v_c)))));
} /* function */
Object co_pressurizedunsealedstate(CArrRef params, bool init /* = true */) {
  return Object(p_pressurizedunsealedstate(NEW(c_pressurizedunsealedstate)())->dynCreate(params, init));
}
Object co_sealedstate(CArrRef params, bool init /* = true */) {
  return Object(p_sealedstate(NEW(c_sealedstate)())->dynCreate(params, init));
}
Object co_bottlestate(CArrRef params, bool init /* = true */) {
  return Object(p_bottlestate(NEW(c_bottlestate)())->dynCreate(params, init));
}
Object co_unpressurizedfullstate(CArrRef params, bool init /* = true */) {
  return Object(p_unpressurizedfullstate(NEW(c_unpressurizedfullstate)())->dynCreate(params, init));
}
Object co_pressurizedsealedstate(CArrRef params, bool init /* = true */) {
  return Object(p_pressurizedsealedstate(NEW(c_pressurizedsealedstate)())->dynCreate(params, init));
}
Object co_pressurizedbottle(CArrRef params, bool init /* = true */) {
  return Object(p_pressurizedbottle(NEW(c_pressurizedbottle)())->dynCreate(params, init));
}
Object co_unpressurizedemptystate(CArrRef params, bool init /* = true */) {
  return Object(p_unpressurizedemptystate(NEW(c_unpressurizedemptystate)())->dynCreate(params, init));
}
Object co_pressurizedbottlestate(CArrRef params, bool init /* = true */) {
  return Object(p_pressurizedbottlestate(NEW(c_pressurizedbottlestate)())->dynCreate(params, init));
}
Object co_emptystate(CArrRef params, bool init /* = true */) {
  return Object(p_emptystate(NEW(c_emptystate)())->dynCreate(params, init));
}
Object co_bottle(CArrRef params, bool init /* = true */) {
  return Object(p_bottle(NEW(c_bottle)())->dynCreate(params, init));
}
Object co_fullstate(CArrRef params, bool init /* = true */) {
  return Object(p_fullstate(NEW(c_fullstate)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$shootout$dispatch_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/dispatch.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$dispatch_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_Empty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Empty") : g->GV(Empty);
  Variant &v_Full __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Full") : g->GV(Full);
  Variant &v_Sealed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Sealed") : g->GV(Sealed);
  Variant &v_UnpressurizedEmpty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("UnpressurizedEmpty") : g->GV(UnpressurizedEmpty);
  Variant &v_UnpressurizedFull __attribute__((__unused__)) = (variables != gVariables) ? variables->get("UnpressurizedFull") : g->GV(UnpressurizedFull);
  Variant &v_PressurizedUnsealed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PressurizedUnsealed") : g->GV(PressurizedUnsealed);
  Variant &v_PressurizedSealed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PressurizedSealed") : g->GV(PressurizedSealed);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_b1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b1") : g->GV(b1);
  Variant &v_b2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b2") : g->GV(b2);
  Variant &v_b3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b3") : g->GV(b3);
  Variant &v_b4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b4") : g->GV(b4);
  Variant &v_b5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b5") : g->GV(b5);
  Variant &v_b6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b6") : g->GV(b6);
  Variant &v_b7 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b7") : g->GV(b7);
  Variant &v_b8 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b8") : g->GV(b8);
  Variant &v_b9 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b9") : g->GV(b9);
  Variant &v_b0 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b0") : g->GV(b0);
  Variant &v_p1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p1") : g->GV(p1);
  Variant &v_p2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p2") : g->GV(p2);
  Variant &v_p3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p3") : g->GV(p3);
  Variant &v_p4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p4") : g->GV(p4);
  Variant &v_p5 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p5") : g->GV(p5);
  Variant &v_p6 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p6") : g->GV(p6);
  Variant &v_p7 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p7") : g->GV(p7);
  Variant &v_p8 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p8") : g->GV(p8);
  Variant &v_p9 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p9") : g->GV(p9);
  Variant &v_p0 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p0") : g->GV(p0);
  Variant &v_check __attribute__((__unused__)) = (variables != gVariables) ? variables->get("check") : g->GV(check);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_Empty = ((Object)(LINE(33,p_emptystate(p_emptystate(NEWOBJ(c_emptystate)())->create())))));
  (v_Full = ((Object)(LINE(34,p_fullstate(p_fullstate(NEWOBJ(c_fullstate)())->create())))));
  (v_Sealed = ((Object)(LINE(35,p_sealedstate(p_sealedstate(NEWOBJ(c_sealedstate)())->create())))));
  (v_UnpressurizedEmpty = ((Object)(LINE(62,p_unpressurizedemptystate(p_unpressurizedemptystate(NEWOBJ(c_unpressurizedemptystate)())->create())))));
  (v_UnpressurizedFull = ((Object)(LINE(63,p_unpressurizedfullstate(p_unpressurizedfullstate(NEWOBJ(c_unpressurizedfullstate)())->create())))));
  (v_PressurizedUnsealed = ((Object)(LINE(64,p_pressurizedunsealedstate(p_pressurizedunsealedstate(NEWOBJ(c_pressurizedunsealedstate)())->create())))));
  (v_PressurizedSealed = ((Object)(LINE(65,p_pressurizedsealedstate(p_pressurizedsealedstate(NEWOBJ(c_pressurizedsealedstate)())->create())))));
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1LL)));
  (v_b1 = ((Object)(LINE(136,p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(1LL))))));
  (v_b2 = ((Object)(p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(2LL)))));
  (v_b3 = ((Object)(LINE(137,p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(3LL))))));
  (v_b4 = ((Object)(p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(4LL)))));
  (v_b5 = ((Object)(LINE(138,p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(5LL))))));
  (v_b6 = ((Object)(p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(6LL)))));
  (v_b7 = ((Object)(LINE(139,p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(7LL))))));
  (v_b8 = ((Object)(p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(8LL)))));
  (v_b9 = ((Object)(LINE(140,p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(9LL))))));
  (v_b0 = ((Object)(p_bottle(p_bottle(NEWOBJ(c_bottle)())->create(0LL)))));
  (v_p1 = ((Object)(LINE(142,p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(1LL))))));
  (v_p2 = ((Object)(p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(2LL)))));
  (v_p3 = ((Object)(LINE(143,p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(3LL))))));
  (v_p4 = ((Object)(p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(4LL)))));
  (v_p5 = ((Object)(LINE(144,p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(5LL))))));
  (v_p6 = ((Object)(p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(6LL)))));
  (v_p7 = ((Object)(LINE(145,p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(7LL))))));
  (v_p8 = ((Object)(p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(8LL)))));
  (v_p9 = ((Object)(LINE(146,p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(9LL))))));
  (v_p0 = ((Object)(p_pressurizedbottle(p_pressurizedbottle(NEWOBJ(c_pressurizedbottle)())->create(0LL)))));
  (v_check = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); not_more(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_check += LINE(150,f_bottlecheck(v_b1, v_b2, v_b3, v_b4, v_b5, v_i));
        v_check += LINE(151,f_bottlecheck(v_b6, v_b7, v_b8, v_b9, v_b0, v_i));
        v_check += LINE(153,f_bottlecheck(v_p1, v_p2, v_p3, v_p4, v_p5, v_i));
        v_check -= LINE(154,f_bottlecheck(v_p6, v_p7, v_p8, v_p9, v_p0, v_i));
      }
    }
  }
  print(toString(v_check));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
