
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_shootout_dispatch_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_shootout_dispatch_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(pressurizedunsealedstate)
FORWARD_DECLARE_CLASS(sealedstate)
FORWARD_DECLARE_CLASS(bottlestate)
FORWARD_DECLARE_CLASS(unpressurizedfullstate)
FORWARD_DECLARE_CLASS(pressurizedsealedstate)
FORWARD_DECLARE_CLASS(pressurizedbottle)
FORWARD_DECLARE_CLASS(unpressurizedemptystate)
FORWARD_DECLARE_CLASS(pressurizedbottlestate)
FORWARD_DECLARE_CLASS(emptystate)
FORWARD_DECLARE_CLASS(bottle)
FORWARD_DECLARE_CLASS(fullstate)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_shootout_dispatch_fw_h__
