
#include <php/phc-test/subjects/benchmarks/shootout/mandelbrot.php-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$shootout$mandelbrot_php_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/shootout/mandelbrot.php-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$shootout$mandelbrot_php_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);
  Variant &v_h __attribute__((__unused__)) = (variables != gVariables) ? variables->get("h") : g->GV(h);
  Variant &v_bit_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bit_num") : g->GV(bit_num);
  Variant &v_byte_acc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("byte_acc") : g->GV(byte_acc);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_iter __attribute__((__unused__)) = (variables != gVariables) ? variables->get("iter") : g->GV(iter);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_limit2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("limit2") : g->GV(limit2);
  Variant &v_Zr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Zr") : g->GV(Zr);
  Variant &v_Zi __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Zi") : g->GV(Zi);
  Variant &v_Cr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Cr") : g->GV(Cr);
  Variant &v_Ci __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ci") : g->GV(Ci);
  Variant &v_Tr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Tr") : g->GV(Tr);
  Variant &v_Ti __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Ti") : g->GV(Ti);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;

  (v_w = 0LL);
  (v_h = 0LL);
  (v_bit_num = 0LL);
  (v_byte_acc = 0LL);
  (v_i = 0LL);
  (v_iter = 50LL);
  (v_x = 0LL);
  (v_y = 0LL);
  (v_limit2 = 4LL);
  (v_Zr = 0LL);
  (v_Zi = 0LL);
  (v_Cr = 0LL);
  (v_Ci = 0LL);
  (v_Tr = 0LL);
  (v_Ti = 0LL);
  (silenceInc(), silenceDec((v_h = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(600LL)))));
  (v_w = v_h);
  LINE(16,x_printf(3, "P4\n%d %d\n", Array(ArrayInit(2).set(0, v_w).set(1, v_h).create())));
  {
    LOOP_COUNTER(1);
    for ((v_y = 0LL); less(v_y, v_h); ++v_y) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_x = 0LL); less(v_x, v_w); ++v_x) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_Zr = 0LL);
              (v_Zi = 0LL);
              (v_Tr = 0LL);
              (v_Ti = 0.0);
              (v_Cr = (toDouble(divide(2.0 * toDouble(v_x), v_w)) - 1.5));
              (v_Ci = (toDouble(divide(2.0 * toDouble(v_y), v_h)) - 1.0));
              {
                LOOP_COUNTER(3);
                for ((v_i = 0LL); (less(v_i, v_iter)) && ((not_more(v_Tr + v_Ti, v_limit2))); ++v_i) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    (v_Zi = 2.0 * toDouble(v_Zr) * toDouble(v_Zi) + toDouble(v_Ci));
                    (v_Zr = v_Tr - v_Ti + v_Cr);
                    (v_Tr = v_Zr * v_Zr);
                    (v_Ti = v_Zi * v_Zi);
                  }
                }
              }
              (v_byte_acc = toInt64(toInt64(v_byte_acc)) << 1LL);
              if (not_more(v_Tr + v_Ti, v_limit2)) (v_byte_acc = bitwise_or(v_byte_acc, 1LL));
              ++v_bit_num;
              if (equal(v_bit_num, 8LL)) {
                echo(LINE(41,x_chr(toInt64(v_byte_acc))));
                (v_byte_acc = 0LL);
                (v_bit_num = 0LL);
              }
              else if (equal(v_x, v_w - 1LL)) {
                (v_byte_acc = toInt64(toInt64(v_byte_acc)) << (toInt64(8LL - modulo(toInt64(v_w), 8LL))));
                echo(LINE(48,x_chr(toInt64(v_byte_acc))));
                (v_byte_acc = 0LL);
                (v_bit_num = 0LL);
              }
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
