
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 10:
      HASH_INITIALIZED(0x4F56B733A4DFC78ALL, g->GV(y),
                       y);
      break;
    case 13:
      HASH_INITIALIZED(0x7C1544592B0D2D4DLL, g->GV(bit_num),
                       bit_num);
      HASH_INITIALIZED(0x07BED9C40C45D94DLL, g->GV(limit2),
                       limit2);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 15:
      HASH_INITIALIZED(0x605788335183734FLL, g->GV(Ti),
                       Ti);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 22:
      HASH_INITIALIZED(0x04BFC205E59FA416LL, g->GV(x),
                       x);
      break;
    case 24:
      HASH_INITIALIZED(0x0EB22EDA95766E98LL, g->GV(i),
                       i);
      break;
    case 31:
      HASH_INITIALIZED(0x4AD5071F49906E5FLL, g->GV(iter),
                       iter);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      HASH_INITIALIZED(0x06464637CE6867E3LL, g->GV(byte_acc),
                       byte_acc);
      break;
    case 39:
      HASH_INITIALIZED(0x0BE8944CFAF8D567LL, g->GV(Zi),
                       Zi);
      break;
    case 40:
      HASH_INITIALIZED(0x70ED89AAADED3628LL, g->GV(Zr),
                       Zr);
      break;
    case 41:
      HASH_INITIALIZED(0x009CBDB5D52B2AA9LL, g->GV(w),
                       w);
      break;
    case 42:
      HASH_INITIALIZED(0x21BBB1884986DD2ALL, g->GV(Tr),
                       Tr);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      HASH_INITIALIZED(0x2611AACF478041EELL, g->GV(Ci),
                       Ci);
      break;
    case 48:
      HASH_INITIALIZED(0x695875365480A8F0LL, g->GV(h),
                       h);
      break;
    case 61:
      HASH_INITIALIZED(0x3B2C4C88ACF3773DLL, g->GV(Cr),
                       Cr);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
