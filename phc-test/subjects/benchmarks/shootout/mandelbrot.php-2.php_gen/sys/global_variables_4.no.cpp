
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "w",
    "h",
    "bit_num",
    "byte_acc",
    "i",
    "iter",
    "x",
    "y",
    "limit2",
    "Zr",
    "Zi",
    "Cr",
    "Ci",
    "Tr",
    "Ti",
  };
  if (idx >= 0 && idx < 27) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(w);
    case 13: return GV(h);
    case 14: return GV(bit_num);
    case 15: return GV(byte_acc);
    case 16: return GV(i);
    case 17: return GV(iter);
    case 18: return GV(x);
    case 19: return GV(y);
    case 20: return GV(limit2);
    case 21: return GV(Zr);
    case 22: return GV(Zi);
    case 23: return GV(Cr);
    case 24: return GV(Ci);
    case 25: return GV(Tr);
    case 26: return GV(Ti);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
