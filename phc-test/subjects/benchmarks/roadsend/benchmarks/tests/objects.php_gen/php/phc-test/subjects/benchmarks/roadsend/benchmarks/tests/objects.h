
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_objects_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_objects_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.fw.h>

// Declarations
#include <cls/smallobject.h>
#include <cls/largeobject.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$objects_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_smallobject(CArrRef params, bool init = true);
Object co_largeobject(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_objects_h__
