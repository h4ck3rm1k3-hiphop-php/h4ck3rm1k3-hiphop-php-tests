
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 4 */
Variant c_smallobject::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_smallobject::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_smallobject::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("avar", m_avar));
  c_ObjectData::o_get(props);
}
bool c_smallobject::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x2A6558C007B3B00ALL, avar, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_smallobject::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2A6558C007B3B00ALL, m_avar,
                         avar, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_smallobject::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x2A6558C007B3B00ALL, m_avar,
                      avar, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_smallobject::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_smallobject::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(smallobject)
ObjectData *c_smallobject::create() {
  init();
  t_smallobject();
  return this;
}
ObjectData *c_smallobject::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_smallobject::cloneImpl() {
  c_smallobject *obj = NEW(c_smallobject)();
  cloneSet(obj);
  return obj;
}
void c_smallobject::cloneSet(c_smallobject *clone) {
  clone->m_avar = m_avar;
  ObjectData::cloneSet(clone);
}
Variant c_smallobject::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_smallobject::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_smallobject::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_smallobject$os_get(const char *s) {
  return c_smallobject::os_get(s, -1);
}
Variant &cw_smallobject$os_lval(const char *s) {
  return c_smallobject::os_lval(s, -1);
}
Variant cw_smallobject$os_constant(const char *s) {
  return c_smallobject::os_constant(s);
}
Variant cw_smallobject$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_smallobject::os_invoke(c, s, params, -1, fatal);
}
void c_smallobject::init() {
  m_avar = "hello";
}
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 8 */
void c_smallobject::t_smallobject() {
  INSTANCE_METHOD_INJECTION(smallobject, smallobject::smallobject);
  bool oldInCtor = gasInCtor(true);
  echo("some hairy constructor\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 12 */
void c_smallobject::t_afunc() {
  INSTANCE_METHOD_INJECTION(smallobject, smallobject::afunc);
  Variant v_avar;

  echo(toString(v_avar));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 18 */
Variant c_largeobject::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_largeobject::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_largeobject::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("var1", m_var1));
  props.push_back(NEW(ArrayElement)("var2", m_var2));
  props.push_back(NEW(ArrayElement)("var3", m_var3));
  props.push_back(NEW(ArrayElement)("var4", m_var4));
  props.push_back(NEW(ArrayElement)("var5", m_var5));
  props.push_back(NEW(ArrayElement)("var6", m_var6));
  props.push_back(NEW(ArrayElement)("var7", m_var7));
  props.push_back(NEW(ArrayElement)("var8", m_var8));
  props.push_back(NEW(ArrayElement)("var9", m_var9));
  props.push_back(NEW(ArrayElement)("var10", m_var10));
  props.push_back(NEW(ArrayElement)("var11", m_var11));
  props.push_back(NEW(ArrayElement)("var12", m_var12));
  props.push_back(NEW(ArrayElement)("var13", m_var13));
  props.push_back(NEW(ArrayElement)("var14", m_var14));
  props.push_back(NEW(ArrayElement)("var15", m_var15));
  props.push_back(NEW(ArrayElement)("var16", m_var16));
  props.push_back(NEW(ArrayElement)("var17", m_var17));
  props.push_back(NEW(ArrayElement)("var18", m_var18));
  props.push_back(NEW(ArrayElement)("var19", m_var19));
  props.push_back(NEW(ArrayElement)("var20", m_var20));
  props.push_back(NEW(ArrayElement)("var21", m_var21));
  props.push_back(NEW(ArrayElement)("var22", m_var22));
  props.push_back(NEW(ArrayElement)("var23", m_var23));
  props.push_back(NEW(ArrayElement)("var24", m_var24));
  props.push_back(NEW(ArrayElement)("var25", m_var25));
  props.push_back(NEW(ArrayElement)("var26", m_var26));
  props.push_back(NEW(ArrayElement)("var27", m_var27));
  props.push_back(NEW(ArrayElement)("var28", m_var28));
  props.push_back(NEW(ArrayElement)("var29", m_var29));
  props.push_back(NEW(ArrayElement)("var30", m_var30));
  props.push_back(NEW(ArrayElement)("var31", m_var31));
  props.push_back(NEW(ArrayElement)("var32", m_var32));
  props.push_back(NEW(ArrayElement)("var33", m_var33));
  props.push_back(NEW(ArrayElement)("var34", m_var34));
  props.push_back(NEW(ArrayElement)("var35", m_var35));
  props.push_back(NEW(ArrayElement)("var36", m_var36));
  props.push_back(NEW(ArrayElement)("var37", m_var37));
  props.push_back(NEW(ArrayElement)("var38", m_var38));
  props.push_back(NEW(ArrayElement)("var39", m_var39));
  props.push_back(NEW(ArrayElement)("var40", m_var40));
  c_ObjectData::o_get(props);
}
bool c_largeobject::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 2:
      HASH_EXISTS_STRING(0x3E0F3B3C4A791802LL, var18, 5);
      break;
    case 5:
      HASH_EXISTS_STRING(0x605AE4E02A228705LL, var17, 5);
      break;
    case 10:
      HASH_EXISTS_STRING(0x6C9124A64865648ALL, var19, 5);
      break;
    case 14:
      HASH_EXISTS_STRING(0x1941474221A41F8ELL, var26, 5);
      break;
    case 15:
      HASH_EXISTS_STRING(0x5523C47F305AFF0FLL, var35, 5);
      break;
    case 16:
      HASH_EXISTS_STRING(0x6D3ADC51BF9C1610LL, var21, 5);
      break;
    case 18:
      HASH_EXISTS_STRING(0x78A0CEB2DB6ACE12LL, var9, 4);
      HASH_EXISTS_STRING(0x3EA2C4B4682E1792LL, var39, 5);
      break;
    case 20:
      HASH_EXISTS_STRING(0x0E6046ED1028A014LL, var24, 5);
      break;
    case 21:
      HASH_EXISTS_STRING(0x006455BE231D5A15LL, var11, 5);
      break;
    case 22:
      HASH_EXISTS_STRING(0x4FB9137BB01B6816LL, var7, 4);
      HASH_EXISTS_STRING(0x5FBF87844DDE0B16LL, var12, 5);
      break;
    case 24:
      HASH_EXISTS_STRING(0x3B106988E96A5A18LL, var31, 5);
      break;
    case 34:
      HASH_EXISTS_STRING(0x6994D4AFFCE0FBA2LL, var8, 4);
      HASH_EXISTS_STRING(0x6274A4F853361F22LL, var38, 5);
      break;
    case 36:
      HASH_EXISTS_STRING(0x5F5DC922DB0FF124LL, var37, 5);
      break;
    case 38:
      HASH_EXISTS_STRING(0x197B9CFE2C408F26LL, var22, 5);
      break;
    case 43:
      HASH_EXISTS_STRING(0x13BBD544406FA2ABLL, var6, 4);
      break;
    case 47:
      HASH_EXISTS_STRING(0x39C73F7D3285572FLL, var14, 5);
      break;
    case 48:
      HASH_EXISTS_STRING(0x46CEA79574513E30LL, var15, 5);
      break;
    case 56:
      HASH_EXISTS_STRING(0x2926776E35362E38LL, var5, 4);
      break;
    case 61:
      HASH_EXISTS_STRING(0x243636020E2583BDLL, var10, 5);
      HASH_EXISTS_STRING(0x7FA96BA1E151493DLL, var29, 5);
      break;
    case 66:
      HASH_EXISTS_STRING(0x655123107CA46A42LL, var25, 5);
      break;
    case 70:
      HASH_EXISTS_STRING(0x756F66FE369C56C6LL, var27, 5);
      HASH_EXISTS_STRING(0x23F1506834885246LL, var28, 5);
      break;
    case 76:
      HASH_EXISTS_STRING(0x004E9D9EC5A1334CLL, var2, 4);
      break;
    case 85:
      HASH_EXISTS_STRING(0x59C0D6F4B3E1DD55LL, var36, 5);
      break;
    case 86:
      HASH_EXISTS_STRING(0x2F4B8F4EFECC7AD6LL, var4, 4);
      break;
    case 87:
      HASH_EXISTS_STRING(0x78F5A4C31B6237D7LL, var34, 5);
      break;
    case 95:
      HASH_EXISTS_STRING(0x60953A9C34BE195FLL, var13, 5);
      break;
    case 98:
      HASH_EXISTS_STRING(0x689DC9DC3C1763E2LL, var20, 5);
      break;
    case 99:
      HASH_EXISTS_STRING(0x72A4916AD3C888E3LL, var1, 4);
      break;
    case 103:
      HASH_EXISTS_STRING(0x1BDB9BBA81E8E367LL, var33, 5);
      break;
    case 104:
      HASH_EXISTS_STRING(0x4D0E0FD17DBB8E68LL, var40, 5);
      break;
    case 114:
      HASH_EXISTS_STRING(0x2259DF561A79F8F2LL, var30, 5);
      break;
    case 115:
      HASH_EXISTS_STRING(0x2EABE96C166D7EF3LL, var16, 5);
      break;
    case 116:
      HASH_EXISTS_STRING(0x5FDE5D83123DDCF4LL, var23, 5);
      break;
    case 119:
      HASH_EXISTS_STRING(0x6D59B25083FDA1F7LL, var32, 5);
      break;
    case 123:
      HASH_EXISTS_STRING(0x1AD15A84792DEF7BLL, var3, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_largeobject::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 2:
      HASH_RETURN_STRING(0x3E0F3B3C4A791802LL, m_var18,
                         var18, 5);
      break;
    case 5:
      HASH_RETURN_STRING(0x605AE4E02A228705LL, m_var17,
                         var17, 5);
      break;
    case 10:
      HASH_RETURN_STRING(0x6C9124A64865648ALL, m_var19,
                         var19, 5);
      break;
    case 14:
      HASH_RETURN_STRING(0x1941474221A41F8ELL, m_var26,
                         var26, 5);
      break;
    case 15:
      HASH_RETURN_STRING(0x5523C47F305AFF0FLL, m_var35,
                         var35, 5);
      break;
    case 16:
      HASH_RETURN_STRING(0x6D3ADC51BF9C1610LL, m_var21,
                         var21, 5);
      break;
    case 18:
      HASH_RETURN_STRING(0x78A0CEB2DB6ACE12LL, m_var9,
                         var9, 4);
      HASH_RETURN_STRING(0x3EA2C4B4682E1792LL, m_var39,
                         var39, 5);
      break;
    case 20:
      HASH_RETURN_STRING(0x0E6046ED1028A014LL, m_var24,
                         var24, 5);
      break;
    case 21:
      HASH_RETURN_STRING(0x006455BE231D5A15LL, m_var11,
                         var11, 5);
      break;
    case 22:
      HASH_RETURN_STRING(0x4FB9137BB01B6816LL, m_var7,
                         var7, 4);
      HASH_RETURN_STRING(0x5FBF87844DDE0B16LL, m_var12,
                         var12, 5);
      break;
    case 24:
      HASH_RETURN_STRING(0x3B106988E96A5A18LL, m_var31,
                         var31, 5);
      break;
    case 34:
      HASH_RETURN_STRING(0x6994D4AFFCE0FBA2LL, m_var8,
                         var8, 4);
      HASH_RETURN_STRING(0x6274A4F853361F22LL, m_var38,
                         var38, 5);
      break;
    case 36:
      HASH_RETURN_STRING(0x5F5DC922DB0FF124LL, m_var37,
                         var37, 5);
      break;
    case 38:
      HASH_RETURN_STRING(0x197B9CFE2C408F26LL, m_var22,
                         var22, 5);
      break;
    case 43:
      HASH_RETURN_STRING(0x13BBD544406FA2ABLL, m_var6,
                         var6, 4);
      break;
    case 47:
      HASH_RETURN_STRING(0x39C73F7D3285572FLL, m_var14,
                         var14, 5);
      break;
    case 48:
      HASH_RETURN_STRING(0x46CEA79574513E30LL, m_var15,
                         var15, 5);
      break;
    case 56:
      HASH_RETURN_STRING(0x2926776E35362E38LL, m_var5,
                         var5, 4);
      break;
    case 61:
      HASH_RETURN_STRING(0x243636020E2583BDLL, m_var10,
                         var10, 5);
      HASH_RETURN_STRING(0x7FA96BA1E151493DLL, m_var29,
                         var29, 5);
      break;
    case 66:
      HASH_RETURN_STRING(0x655123107CA46A42LL, m_var25,
                         var25, 5);
      break;
    case 70:
      HASH_RETURN_STRING(0x756F66FE369C56C6LL, m_var27,
                         var27, 5);
      HASH_RETURN_STRING(0x23F1506834885246LL, m_var28,
                         var28, 5);
      break;
    case 76:
      HASH_RETURN_STRING(0x004E9D9EC5A1334CLL, m_var2,
                         var2, 4);
      break;
    case 85:
      HASH_RETURN_STRING(0x59C0D6F4B3E1DD55LL, m_var36,
                         var36, 5);
      break;
    case 86:
      HASH_RETURN_STRING(0x2F4B8F4EFECC7AD6LL, m_var4,
                         var4, 4);
      break;
    case 87:
      HASH_RETURN_STRING(0x78F5A4C31B6237D7LL, m_var34,
                         var34, 5);
      break;
    case 95:
      HASH_RETURN_STRING(0x60953A9C34BE195FLL, m_var13,
                         var13, 5);
      break;
    case 98:
      HASH_RETURN_STRING(0x689DC9DC3C1763E2LL, m_var20,
                         var20, 5);
      break;
    case 99:
      HASH_RETURN_STRING(0x72A4916AD3C888E3LL, m_var1,
                         var1, 4);
      break;
    case 103:
      HASH_RETURN_STRING(0x1BDB9BBA81E8E367LL, m_var33,
                         var33, 5);
      break;
    case 104:
      HASH_RETURN_STRING(0x4D0E0FD17DBB8E68LL, m_var40,
                         var40, 5);
      break;
    case 114:
      HASH_RETURN_STRING(0x2259DF561A79F8F2LL, m_var30,
                         var30, 5);
      break;
    case 115:
      HASH_RETURN_STRING(0x2EABE96C166D7EF3LL, m_var16,
                         var16, 5);
      break;
    case 116:
      HASH_RETURN_STRING(0x5FDE5D83123DDCF4LL, m_var23,
                         var23, 5);
      break;
    case 119:
      HASH_RETURN_STRING(0x6D59B25083FDA1F7LL, m_var32,
                         var32, 5);
      break;
    case 123:
      HASH_RETURN_STRING(0x1AD15A84792DEF7BLL, m_var3,
                         var3, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_largeobject::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 2:
      HASH_SET_STRING(0x3E0F3B3C4A791802LL, m_var18,
                      var18, 5);
      break;
    case 5:
      HASH_SET_STRING(0x605AE4E02A228705LL, m_var17,
                      var17, 5);
      break;
    case 10:
      HASH_SET_STRING(0x6C9124A64865648ALL, m_var19,
                      var19, 5);
      break;
    case 14:
      HASH_SET_STRING(0x1941474221A41F8ELL, m_var26,
                      var26, 5);
      break;
    case 15:
      HASH_SET_STRING(0x5523C47F305AFF0FLL, m_var35,
                      var35, 5);
      break;
    case 16:
      HASH_SET_STRING(0x6D3ADC51BF9C1610LL, m_var21,
                      var21, 5);
      break;
    case 18:
      HASH_SET_STRING(0x78A0CEB2DB6ACE12LL, m_var9,
                      var9, 4);
      HASH_SET_STRING(0x3EA2C4B4682E1792LL, m_var39,
                      var39, 5);
      break;
    case 20:
      HASH_SET_STRING(0x0E6046ED1028A014LL, m_var24,
                      var24, 5);
      break;
    case 21:
      HASH_SET_STRING(0x006455BE231D5A15LL, m_var11,
                      var11, 5);
      break;
    case 22:
      HASH_SET_STRING(0x4FB9137BB01B6816LL, m_var7,
                      var7, 4);
      HASH_SET_STRING(0x5FBF87844DDE0B16LL, m_var12,
                      var12, 5);
      break;
    case 24:
      HASH_SET_STRING(0x3B106988E96A5A18LL, m_var31,
                      var31, 5);
      break;
    case 34:
      HASH_SET_STRING(0x6994D4AFFCE0FBA2LL, m_var8,
                      var8, 4);
      HASH_SET_STRING(0x6274A4F853361F22LL, m_var38,
                      var38, 5);
      break;
    case 36:
      HASH_SET_STRING(0x5F5DC922DB0FF124LL, m_var37,
                      var37, 5);
      break;
    case 38:
      HASH_SET_STRING(0x197B9CFE2C408F26LL, m_var22,
                      var22, 5);
      break;
    case 43:
      HASH_SET_STRING(0x13BBD544406FA2ABLL, m_var6,
                      var6, 4);
      break;
    case 47:
      HASH_SET_STRING(0x39C73F7D3285572FLL, m_var14,
                      var14, 5);
      break;
    case 48:
      HASH_SET_STRING(0x46CEA79574513E30LL, m_var15,
                      var15, 5);
      break;
    case 56:
      HASH_SET_STRING(0x2926776E35362E38LL, m_var5,
                      var5, 4);
      break;
    case 61:
      HASH_SET_STRING(0x243636020E2583BDLL, m_var10,
                      var10, 5);
      HASH_SET_STRING(0x7FA96BA1E151493DLL, m_var29,
                      var29, 5);
      break;
    case 66:
      HASH_SET_STRING(0x655123107CA46A42LL, m_var25,
                      var25, 5);
      break;
    case 70:
      HASH_SET_STRING(0x756F66FE369C56C6LL, m_var27,
                      var27, 5);
      HASH_SET_STRING(0x23F1506834885246LL, m_var28,
                      var28, 5);
      break;
    case 76:
      HASH_SET_STRING(0x004E9D9EC5A1334CLL, m_var2,
                      var2, 4);
      break;
    case 85:
      HASH_SET_STRING(0x59C0D6F4B3E1DD55LL, m_var36,
                      var36, 5);
      break;
    case 86:
      HASH_SET_STRING(0x2F4B8F4EFECC7AD6LL, m_var4,
                      var4, 4);
      break;
    case 87:
      HASH_SET_STRING(0x78F5A4C31B6237D7LL, m_var34,
                      var34, 5);
      break;
    case 95:
      HASH_SET_STRING(0x60953A9C34BE195FLL, m_var13,
                      var13, 5);
      break;
    case 98:
      HASH_SET_STRING(0x689DC9DC3C1763E2LL, m_var20,
                      var20, 5);
      break;
    case 99:
      HASH_SET_STRING(0x72A4916AD3C888E3LL, m_var1,
                      var1, 4);
      break;
    case 103:
      HASH_SET_STRING(0x1BDB9BBA81E8E367LL, m_var33,
                      var33, 5);
      break;
    case 104:
      HASH_SET_STRING(0x4D0E0FD17DBB8E68LL, m_var40,
                      var40, 5);
      break;
    case 114:
      HASH_SET_STRING(0x2259DF561A79F8F2LL, m_var30,
                      var30, 5);
      break;
    case 115:
      HASH_SET_STRING(0x2EABE96C166D7EF3LL, m_var16,
                      var16, 5);
      break;
    case 116:
      HASH_SET_STRING(0x5FDE5D83123DDCF4LL, m_var23,
                      var23, 5);
      break;
    case 119:
      HASH_SET_STRING(0x6D59B25083FDA1F7LL, m_var32,
                      var32, 5);
      break;
    case 123:
      HASH_SET_STRING(0x1AD15A84792DEF7BLL, m_var3,
                      var3, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_largeobject::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_largeobject::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(largeobject)
ObjectData *c_largeobject::create() {
  init();
  t_largeobject();
  return this;
}
ObjectData *c_largeobject::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_largeobject::cloneImpl() {
  c_largeobject *obj = NEW(c_largeobject)();
  cloneSet(obj);
  return obj;
}
void c_largeobject::cloneSet(c_largeobject *clone) {
  clone->m_var1 = m_var1;
  clone->m_var2 = m_var2;
  clone->m_var3 = m_var3;
  clone->m_var4 = m_var4;
  clone->m_var5 = m_var5;
  clone->m_var6 = m_var6;
  clone->m_var7 = m_var7;
  clone->m_var8 = m_var8;
  clone->m_var9 = m_var9;
  clone->m_var10 = m_var10;
  clone->m_var11 = m_var11;
  clone->m_var12 = m_var12;
  clone->m_var13 = m_var13;
  clone->m_var14 = m_var14;
  clone->m_var15 = m_var15;
  clone->m_var16 = m_var16;
  clone->m_var17 = m_var17;
  clone->m_var18 = m_var18;
  clone->m_var19 = m_var19;
  clone->m_var20 = m_var20;
  clone->m_var21 = m_var21;
  clone->m_var22 = m_var22;
  clone->m_var23 = m_var23;
  clone->m_var24 = m_var24;
  clone->m_var25 = m_var25;
  clone->m_var26 = m_var26;
  clone->m_var27 = m_var27;
  clone->m_var28 = m_var28;
  clone->m_var29 = m_var29;
  clone->m_var30 = m_var30;
  clone->m_var31 = m_var31;
  clone->m_var32 = m_var32;
  clone->m_var33 = m_var33;
  clone->m_var34 = m_var34;
  clone->m_var35 = m_var35;
  clone->m_var36 = m_var36;
  clone->m_var37 = m_var37;
  clone->m_var38 = m_var38;
  clone->m_var39 = m_var39;
  clone->m_var40 = m_var40;
  ObjectData::cloneSet(clone);
}
Variant c_largeobject::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x5CA54230E4F336E1LL, afunc12) {
        return (t_afunc12(params.rvalAt(0)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x0017424BA5EB1B42LL, afunc20) {
        return (t_afunc20(params.rvalAt(0)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x3C544770D7D47307LL, afunc18) {
        return (t_afunc18(params.rvalAt(0)), null);
      }
      break;
    case 10:
      HASH_GUARD(0x03CB87AA1C2B858ALL, afunc17) {
        return (t_afunc17(params.rvalAt(0)), null);
      }
      break;
    case 13:
      HASH_GUARD(0x4B9A460649DF810DLL, afunc11) {
        return (t_afunc11(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x6A39825CDDCC43CDLL, afunc21) {
        return (t_afunc21(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x58BF10768C7BE42DLL, afunc16) {
        return (t_afunc16(params.rvalAt(0)), null);
      }
      break;
    case 21:
      HASH_GUARD(0x113903680C7A28B5LL, afunc14) {
        return (t_afunc14(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x5AF16C5EE2195CB7LL, afunc13) {
        return (t_afunc13(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x6E9CD06554980438LL, afunc15) {
        return (t_afunc15(params.rvalAt(0)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x24EAEAD49219B77CLL, afunc19) {
        return (t_afunc19(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_largeobject::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x5CA54230E4F336E1LL, afunc12) {
        return (t_afunc12(a0), null);
      }
      break;
    case 2:
      HASH_GUARD(0x0017424BA5EB1B42LL, afunc20) {
        return (t_afunc20(a0), null);
      }
      break;
    case 7:
      HASH_GUARD(0x3C544770D7D47307LL, afunc18) {
        return (t_afunc18(a0), null);
      }
      break;
    case 10:
      HASH_GUARD(0x03CB87AA1C2B858ALL, afunc17) {
        return (t_afunc17(a0), null);
      }
      break;
    case 13:
      HASH_GUARD(0x4B9A460649DF810DLL, afunc11) {
        return (t_afunc11(a0), null);
      }
      HASH_GUARD(0x6A39825CDDCC43CDLL, afunc21) {
        return (t_afunc21(a0), null);
      }
      HASH_GUARD(0x58BF10768C7BE42DLL, afunc16) {
        return (t_afunc16(a0), null);
      }
      break;
    case 21:
      HASH_GUARD(0x113903680C7A28B5LL, afunc14) {
        return (t_afunc14(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x5AF16C5EE2195CB7LL, afunc13) {
        return (t_afunc13(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x6E9CD06554980438LL, afunc15) {
        return (t_afunc15(a0), null);
      }
      break;
    case 28:
      HASH_GUARD(0x24EAEAD49219B77CLL, afunc19) {
        return (t_afunc19(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_largeobject::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_largeobject$os_get(const char *s) {
  return c_largeobject::os_get(s, -1);
}
Variant &cw_largeobject$os_lval(const char *s) {
  return c_largeobject::os_lval(s, -1);
}
Variant cw_largeobject$os_constant(const char *s) {
  return c_largeobject::os_constant(s);
}
Variant cw_largeobject$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_largeobject::os_invoke(c, s, params, -1, fatal);
}
void c_largeobject::init() {
  m_var1 = "some string";
  m_var2 = "some string";
  m_var3 = "some string";
  m_var4 = "some string";
  m_var5 = "some string";
  m_var6 = "some string";
  m_var7 = "some string";
  m_var8 = "some string";
  m_var9 = "some string";
  m_var10 = "some string";
  m_var11 = "some string";
  m_var12 = "some string";
  m_var13 = "some string";
  m_var14 = "some string";
  m_var15 = "some string";
  m_var16 = "some string";
  m_var17 = "some string";
  m_var18 = "some string";
  m_var19 = "some string";
  m_var20 = "some string";
  m_var21 = "some string";
  m_var22 = "some string";
  m_var23 = "some string";
  m_var24 = "some string";
  m_var25 = "some string";
  m_var26 = "some string";
  m_var27 = "some string";
  m_var28 = "some string";
  m_var29 = "some string";
  m_var30 = "some string";
  m_var31 = "some string";
  m_var32 = "some string";
  m_var33 = "some string";
  m_var34 = "some string";
  m_var35 = "some string";
  m_var36 = "some string";
  m_var37 = "some string";
  m_var38 = "some string";
  m_var39 = "some string";
  m_var40 = "some string";
}
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 61 */
void c_largeobject::t_largeobject() {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::largeobject);
  bool oldInCtor = gasInCtor(true);
  echo("some hairy constructor\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 65 */
void c_largeobject::t_afunc11(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc11);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 69 */
void c_largeobject::t_afunc12(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc12);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 73 */
void c_largeobject::t_afunc13(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc13);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 77 */
void c_largeobject::t_afunc14(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc14);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 81 */
void c_largeobject::t_afunc15(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc15);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 85 */
void c_largeobject::t_afunc16(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc16);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 89 */
void c_largeobject::t_afunc17(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc17);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 93 */
void c_largeobject::t_afunc18(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc18);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 97 */
void c_largeobject::t_afunc19(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc19);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 101 */
void c_largeobject::t_afunc20(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc20);
  echo("a function\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 105 */
void c_largeobject::t_afunc21(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(largeobject, largeobject::afunc21);
  echo("a function\n");
} /* function */
Object co_smallobject(CArrRef params, bool init /* = true */) {
  return Object(p_smallobject(NEW(c_smallobject)())->dynCreate(params, init));
}
Object co_largeobject(CArrRef params, bool init /* = true */) {
  return Object(p_largeobject(NEW(c_largeobject)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$objects_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$objects_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_numObjs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("numObjs") : g->GV(numObjs);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_large __attribute__((__unused__)) = (variables != gVariables) ? variables->get("large") : g->GV(large);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_somevar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("somevar") : g->GV(somevar);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_numObjs = 50000LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_numObjs); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_large.set(v_i, (LINE(115,p_largeobject(p_largeobject(NEWOBJ(c_largeobject)())->create()))));
        lval(lval(v_large.lvalAt(v_i)).o_lval("small", 0x19E2370F85F3D538LL)).set(v_i, (LINE(116,p_smallobject(p_smallobject(NEWOBJ(c_smallobject)())->create()))));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, v_numObjs); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        LINE(122,v_large.rvalAt(v_i).o_invoke_few_args("afunc11", 0x4B9A460649DF810DLL, 1, "hi"));
        LINE(123,v_large.rvalAt(v_i).o_invoke_few_args("afunc12", 0x5CA54230E4F336E1LL, 1, "hi"));
        LINE(124,v_large.rvalAt(v_i).o_invoke_few_args("afunc13", 0x5AF16C5EE2195CB7LL, 1, "hi"));
        LINE(125,v_large.rvalAt(v_i).o_invoke_few_args("afunc14", 0x113903680C7A28B5LL, 1, "hi"));
        LINE(126,v_large.rvalAt(v_i).o_invoke_few_args("afunc15", 0x6E9CD06554980438LL, 1, "hi"));
        LINE(127,v_large.rvalAt(v_i).o_invoke_few_args("afunc16", 0x58BF10768C7BE42DLL, 1, "hi"));
        LINE(128,v_large.rvalAt(v_i).o_invoke_few_args("afunc17", 0x03CB87AA1C2B858ALL, 1, "hi"));
        LINE(129,v_large.rvalAt(v_i).o_invoke_few_args("afunc18", 0x3C544770D7D47307LL, 1, "hi"));
        LINE(130,v_large.rvalAt(v_i).o_invoke_few_args("afunc19", 0x24EAEAD49219B77CLL, 1, "hi"));
        LINE(131,v_large.rvalAt(v_i).o_invoke_few_args("afunc20", 0x0017424BA5EB1B42LL, 1, "hi"));
        LINE(132,v_large.rvalAt(v_i).o_invoke_few_args("afunc21", 0x6A39825CDDCC43CDLL, 1, "hi"));
      }
    }
  }
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, v_numObjs); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        {
          LOOP_COUNTER(4);
          for ((v_t = 0LL); not_more(v_t, 40LL); v_t++) {
            LOOP_COUNTER_CHECK(4);
            {
              (v_somevar = concat("var", toString(v_t)));
              (v_a = v_large.rvalAt(v_i).o_get(toString(v_somevar), -1LL));
              (v_b = v_large.rvalAt(v_i).o_get("small", 0x19E2370F85F3D538LL).rvalAt(v_i).o_get("avar", 0x2A6558C007B3B00ALL));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
