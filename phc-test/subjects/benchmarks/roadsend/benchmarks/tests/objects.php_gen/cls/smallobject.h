
#ifndef __GENERATED_cls_smallobject_h__
#define __GENERATED_cls_smallobject_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/objects.php line 4 */
class c_smallobject : virtual public ObjectData {
  BEGIN_CLASS_MAP(smallobject)
  END_CLASS_MAP(smallobject)
  DECLARE_CLASS(smallobject, smallobject, ObjectData)
  void init();
  public: String m_avar;
  public: void t_smallobject();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_afunc();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_smallobject_h__
