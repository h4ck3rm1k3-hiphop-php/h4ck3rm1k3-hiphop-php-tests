
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/fibo.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/fibo.php line 6 */
Variant f_fibo(CVarRef v_n) {
  FUNCTION_INJECTION(fibo);
  return ((less(v_n, 2LL)) ? ((Variant)(1LL)) : ((Variant)(plus_rev(LINE(7,f_fibo(v_n - 1LL)), f_fibo(v_n - 2LL)))));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$fibo_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/fibo.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$fibo_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(30LL)));
  (v_r = LINE(10,f_fibo(v_n)));
  print(toString(v_r) + toString("\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
