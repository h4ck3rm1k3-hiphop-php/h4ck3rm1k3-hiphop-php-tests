
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_include_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_include_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(sm_codeplate)
FORWARD_DECLARE_CLASS(sm_module)
FORWARD_DECLARE_CLASS(sm_smtag)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_include_fw_h__
