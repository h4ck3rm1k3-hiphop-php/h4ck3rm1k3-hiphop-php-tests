
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_include_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_include_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/include.fw.h>

// Declarations
#include <cls/sm_codeplate.h>
#include <cls/sm_module.h>
#include <cls/sm_smtag.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$include_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_codeplate(CArrRef params, bool init = true);
Object co_sm_module(CArrRef params, bool init = true);
Object co_sm_smtag(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_include_h__
