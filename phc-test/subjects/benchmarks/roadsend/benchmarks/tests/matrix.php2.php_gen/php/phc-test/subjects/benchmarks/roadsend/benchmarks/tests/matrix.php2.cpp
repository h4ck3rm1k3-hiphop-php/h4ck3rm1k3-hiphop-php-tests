
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/matrix.php2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/matrix.php2.php line 24 */
Variant f_mmult(CVarRef v_rows, CVarRef v_cols, CVarRef v_m1, CVarRef v_m2) {
  FUNCTION_INJECTION(mmult);
  Variant v_m3;
  int64 v_i = 0;
  Variant v_row;
  int64 v_j = 0;
  Numeric v_x = 0;
  int64 v_k = 0;

  (v_m3 = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_row = ScalarArrays::sa_[0]);
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_x = 0LL);
              {
                LOOP_COUNTER(3);
                for ((v_k = 0LL); less(v_k, v_cols); v_k++) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    v_x += v_m1.rvalAt(v_i).rvalAt(v_k) * v_m2.rvalAt(v_k).rvalAt(v_j);
                  }
                }
              }
              v_row.set(v_j, (v_x));
            }
          }
        }
        v_m3.set(v_i, (v_row));
      }
    }
  }
  return (v_m3);
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/matrix.php2.php line 11 */
Variant f_mkmatrix(CVarRef v_rows, CVarRef v_cols) {
  FUNCTION_INJECTION(mkmatrix);
  int64 v_count = 0;
  Variant v_mx;
  int64 v_i = 0;
  Variant v_row;
  int64 v_j = 0;

  (v_count = 1LL);
  (v_mx = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        (v_row = ScalarArrays::sa_[0]);
        {
          LOOP_COUNTER(5);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(5);
            {
              v_row.set(v_j, (v_count++));
            }
          }
        }
        v_mx.set(v_i, (v_row));
      }
    }
  }
  return (v_mx);
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$matrix_php2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/matrix.php2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$matrix_php2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_SIZE __attribute__((__unused__)) = (variables != gVariables) ? variables->get("SIZE") : g->GV(SIZE);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_m1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m1") : g->GV(m1);
  Variant &v_m2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m2") : g->GV(m2);
  Variant &v_mm __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mm") : g->GV(mm);

  (v_SIZE = 20LL);
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(100LL)));
  (v_m1 = LINE(41,f_mkmatrix(v_SIZE, v_SIZE)));
  (v_m2 = LINE(42,f_mkmatrix(v_SIZE, v_SIZE)));
  LOOP_COUNTER(6);
  {
    while (toBoolean(v_n--)) {
      LOOP_COUNTER_CHECK(6);
      {
        (v_mm = LINE(44,f_mmult(v_SIZE, v_SIZE, v_m1, v_m2)));
      }
    }
  }
  print(LINE(46,(assignCallTemp(eo_0, toString(v_mm.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))),assignCallTemp(eo_2, concat6(toString(v_mm.rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), " ", toString(v_mm.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL).rvalAt(2LL, 0x486AFCC090D5F98CLL)), " ", toString(v_mm.rvalAt(4LL, 0x6F2A25235E544A31LL).rvalAt(4LL, 0x6F2A25235E544A31LL)), "\n")),concat3(eo_0, " ", eo_2))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
