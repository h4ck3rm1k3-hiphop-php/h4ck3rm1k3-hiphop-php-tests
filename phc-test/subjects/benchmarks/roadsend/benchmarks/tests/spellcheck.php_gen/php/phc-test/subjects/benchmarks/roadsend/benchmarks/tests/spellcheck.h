
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_spellcheck_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_spellcheck_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/spellcheck.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_spellcheck();
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$spellcheck_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_spellcheck_h__
