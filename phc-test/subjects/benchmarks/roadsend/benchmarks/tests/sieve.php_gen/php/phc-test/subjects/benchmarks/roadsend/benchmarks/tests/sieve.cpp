
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/sieve.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/sieve.php line 10 */
void f_sieve(Variant v_n) {
  FUNCTION_INJECTION(sieve);
  int64 v_count = 0;
  Variant v_flags;
  int64 v_i = 0;
  int64 v_k = 0;

  (v_count = 0LL);
  LOOP_COUNTER(1);
  {
    while (more(v_n--, 0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_count = 0LL);
        (v_flags = LINE(14,x_range(0LL, 8192LL)));
        {
          LOOP_COUNTER(2);
          for ((v_i = 2LL); less(v_i, 8193LL); v_i++) {
            LOOP_COUNTER_CHECK(2);
            {
              if (more(v_flags.rvalAt(v_i), 0LL)) {
                {
                  LOOP_COUNTER(3);
                  for ((v_k = v_i + v_i); not_more(v_k, 8192LL); v_k += v_i) {
                    LOOP_COUNTER_CHECK(3);
                    {
                      v_flags.set(v_k, (0LL));
                    }
                  }
                }
                v_count++;
              }
            }
          }
        }
      }
    }
  }
  print(LINE(24,concat3("Count: ", toString(v_count), "\n")));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$sieve_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/sieve.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$sieve_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = 18LL);
  LINE(8,f_sieve(v_n));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
