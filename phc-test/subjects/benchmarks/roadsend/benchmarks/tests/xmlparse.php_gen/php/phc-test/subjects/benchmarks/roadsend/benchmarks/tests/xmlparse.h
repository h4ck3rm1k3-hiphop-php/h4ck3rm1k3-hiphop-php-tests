
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_xmlparse_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_xmlparse_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/xmlparse.fw.h>

// Declarations
#include <cls/xmlparser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$xmlparse_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_xmlfile(CVarRef v_num);
Object co_xmlparser(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_xmlparse_h__
