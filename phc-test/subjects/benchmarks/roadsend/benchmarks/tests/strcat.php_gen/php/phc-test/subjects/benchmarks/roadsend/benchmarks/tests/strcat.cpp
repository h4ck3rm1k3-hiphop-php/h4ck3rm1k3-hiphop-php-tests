
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/strcat.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/strcat.php line 35 */
void f_afunc() {
  FUNCTION_INJECTION(afunc);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  Variant v_b;
  int64 v_i = 0;
  Variant v_val;

  setNull(v_b);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 1000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for (ArrayIterPtr iter4 = gv_a.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_val = iter4->second();
            {
              concat_assign(v_b, toString(v_val));
            }
          }
        }
      }
    }
  }
  echo(toString(v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/strcat.php line 10 */
void f_mystrcat(Variant v_n) {
  FUNCTION_INJECTION(mystrcat);
  String v_str;
  int v_len = 0;

  (v_str = "");
  LOOP_COUNTER(5);
  {
    while (more(v_n--, 0LL)) {
      LOOP_COUNTER_CHECK(5);
      {
        concat_assign(v_str, "hello\n");
        (v_str = concat(v_str, "goodbyehello\n"));
      }
    }
  }
  (v_len = LINE(16,x_strlen(v_str)));
  print(toString(v_len) + toString("\n"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$strcat_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/strcat.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$strcat_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(10000LL)));
  LINE(8,f_mystrcat(v_n));
  echo("\n\n");
  v_a.append(("zot"));
  v_a.append(("zotzot"));
  v_a.append(("zotzotzot"));
  v_a.append(("zotzotzotzot"));
  v_a.append(("zotzotzotzotzot"));
  v_a.append(("zotzotzotzotzotzot"));
  v_a.append(("zotzotzotzotzotzotzot"));
  v_a.append(("zotzotzotzotzotzotzotzot"));
  v_a.append(("zotzotzotzotzotzotzotzotzot"));
  LINE(47,f_afunc());
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
