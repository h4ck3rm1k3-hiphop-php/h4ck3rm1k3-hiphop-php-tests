
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/pcre.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_SM_TAG_PREGEXP = "/<\\s*SM\\s(.+)\\s*>/Ui";
const StaticString k_SM_TAG_ATTR_PREGEXP = "/(\\w+)\\s*=\\s*[\\\"'](.+)[\\\"']/SUi";
const StaticString k_SM_TAG_IDENTIFIER = "SM";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/pcre.php line 12 */
void f_test() {
  FUNCTION_INJECTION(test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_htmlTemplate;
  Variant v_line;
  Variant v_smTagMatch;
  int64 v_i = 0;
  Variant v_smParms;
  Variant v_attrs;
  Variant v_matchList;
  int64 v_x = 0;
  String v_key;

  (v_htmlTemplate = LINE(13,x_file(concat(toString(g->gv__ENV.rvalAt("PCC_HOME", 0x4F7B58EF1F3DF563LL)), "/benchmarks/data/pcre-data.xml"))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_htmlTemplate.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_line = iter3->second();
      {
        ;
        ;
        if (toBoolean(LINE(23,x_preg_match_all("/<\\s*SM\\s(.+)\\s*>/Ui" /* SM_TAG_PREGEXP */, toString(v_line), ref(v_smTagMatch))))) {
          {
            LOOP_COUNTER(4);
            for ((v_i = 0LL); less(v_i, LINE(26,x_sizeof(v_smTagMatch.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))); v_i++) {
              LOOP_COUNTER_CHECK(4);
              {
                (v_smParms = v_smTagMatch.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(v_i));
                unset(v_attrs);
                LINE(36,x_preg_match_all("/(\\w+)\\s*=\\s*[\\\"'](.+)[\\\"']/SUi" /* SM_TAG_ATTR_PREGEXP */, toString(v_smParms), ref(v_matchList)));
                {
                  LOOP_COUNTER(5);
                  for ((v_x = 0LL); less(v_x, LINE(37,x_count(v_matchList.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))); v_x++) {
                    LOOP_COUNTER_CHECK(5);
                    {
                      (v_key = LINE(38,x_strtoupper(toString(v_matchList.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(v_x)))));
                      v_attrs.set(v_key, (v_matchList.rvalAt(2LL, 0x486AFCC090D5F98CLL).rvalAt(v_x)));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$pcre_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/pcre.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$pcre_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  ;
  ;
  ;
  {
    LOOP_COUNTER(6);
    for ((v_i = 0LL); less(v_i, 100LL); v_i++) {
      LOOP_COUNTER_CHECK(6);
      {
        LINE(49,f_test());
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
