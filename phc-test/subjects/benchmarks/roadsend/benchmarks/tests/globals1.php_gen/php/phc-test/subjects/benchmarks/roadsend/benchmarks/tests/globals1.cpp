
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/globals1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$globals1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/globals1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$globals1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_aglobal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("aglobal") : g->GV(aglobal);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_another __attribute__((__unused__)) = (variables != gVariables) ? variables->get("another") : g->GV(another);
  Variant &v_another1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("another1") : g->GV(another1);
  Variant &v_another2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("another2") : g->GV(another2);
  Variant &v_another3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("another3") : g->GV(another3);

  echo("an effort to make a more clear globals benchmark\n");
  LOOP_COUNTER(1);
  {
    while (less(v_aglobal, 1000000LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        v_aglobal++;
        (v_j = v_aglobal);
        (v_another = v_j);
        (v_another1 = v_j);
        (v_another2 = v_j);
        (v_another3 = v_j);
      }
    }
  }
  print(concat("aglobal ", LINE(13,concat6(toString(v_aglobal), ", j ", toString(v_j), ", another ", toString(v_another), "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
