
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 38 */
void f_julia_ppm(CStrRef v_filename, int64 v_size, int64 v_depth, double v_zreal, double v_zimag) {
  FUNCTION_INJECTION(julia_ppm);
  Variant eo_0;
  Variant eo_1;
  Variant v_ppm;
  Array v_z;
  double v_real_min = 0.0;
  double v_real_max = 0.0;
  double v_imag_min = 0.0;
  Numeric v_delta = 0;
  int64 v_i = 0;
  double v_xreal = 0.0;
  int64 v_j = 0;
  double v_ximag = 0.0;
  int64 v_count = 0;
  Array v_x;
  Numeric v_intensity = 0;

  if (LINE(40,x_file_exists(v_filename))) LINE(41,x_unlink(v_filename));
  (v_ppm = LINE(42,x_fopen(v_filename, "w")));
  LINE(45,f_ppm_write(v_ppm, "P3\n"));
  LINE(48,(assignCallTemp(eo_0, v_ppm),assignCallTemp(eo_1, concat4(toString(v_size), " ", toString(v_size), "\n")),f_ppm_write(eo_0, eo_1)));
  LINE(51,f_ppm_write(v_ppm, concat(toString(v_depth), "\n")));
  (v_z = LINE(54,f_complex(v_zreal, v_zimag)));
  (v_real_min = -1.2);
  (v_real_max = 1.2);
  (v_imag_min = -1.2);
  (v_delta = divide((v_real_max - v_real_min), v_size));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL), (v_xreal = v_real_min); less(v_i, v_size); v_i++, (v_xreal = (v_xreal + toDouble(v_delta)))) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL), (v_ximag = v_imag_min); less(v_j, v_size); v_j++, (v_ximag = (v_ximag + toDouble(v_delta)))) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_count = 0LL);
              (v_x = LINE(67,f_complex(v_xreal, v_ximag)));
              LOOP_COUNTER(3);
              {
                while (less(v_count, v_depth) && less(LINE(68,f_complex_abs(v_x)), 2.0)) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    v_count++;
                    (v_x = LINE(70,(assignCallTemp(eo_0, f_complex_multiply(v_x, v_x)),assignCallTemp(eo_1, v_z),f_complex_add(eo_0, eo_1))));
                  }
                }
              }
              if (not_more(LINE(72,f_complex_abs(v_x)), 2.0)) LINE(73,f_ppm_write(v_ppm, "0 0 0\n"));
              else {
                (v_intensity = divide(v_count, v_depth));
                if (more(v_intensity, 0.001)) LINE(79,(assignCallTemp(eo_0, v_ppm),assignCallTemp(eo_1, concat(concat(concat(concat_rev(toString(LINE(78,x_round(v_depth - (v_depth * v_intensity)))), concat(toString(LINE(77,x_round(v_depth - (v_depth * v_intensity)))), " ")), " "), toString(v_depth)), "\n")),f_ppm_write(eo_0, eo_1)));
              }
            }
          }
        }
      }
    }
  }
  LINE(83,x_fclose(toObject(v_ppm)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 20 */
Array f_complex_multiply(CArrRef v_complex1, CArrRef v_complex2) {
  FUNCTION_INJECTION(complex_multiply);
  Variant eo_0;
  Variant eo_1;
  return LINE(22,(assignCallTemp(eo_0, minus_rev((multiply_rev(LINE(21,f_imag_part(v_complex2)), f_imag_part(v_complex1))), (multiply_rev(f_real_part(v_complex2), f_real_part(v_complex1))))),assignCallTemp(eo_1, plus_rev((multiply_rev(LINE(22,f_real_part(v_complex2)), f_imag_part(v_complex1))), (multiply_rev(f_imag_part(v_complex2), f_real_part(v_complex1))))),f_complex(eo_0, eo_1)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 15 */
Array f_complex_add(CArrRef v_complex1, CArrRef v_complex2) {
  FUNCTION_INJECTION(complex_add);
  Variant eo_0;
  Variant eo_1;
  return LINE(17,(assignCallTemp(eo_0, plus_rev(LINE(16,f_real_part(v_complex2)), f_real_part(v_complex1))),assignCallTemp(eo_1, plus_rev(LINE(17,f_imag_part(v_complex2)), f_imag_part(v_complex1))),f_complex(eo_0, eo_1)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 11 */
Variant f_imag_part(CVarRef v_complex_number) {
  FUNCTION_INJECTION(imag_part);
  return v_complex_number.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 25 */
double f_complex_abs(CArrRef v_complex) {
  FUNCTION_INJECTION(complex_abs);
  Variant v_r;
  Variant v_i;

  (v_r = LINE(26,f_real_part(v_complex)));
  (v_i = LINE(27,f_imag_part(v_complex)));
  return LINE(28,x_sqrt(toDouble((v_r * v_r) + (v_i * v_i))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 31 */
void f_ppm_write(CVarRef v_filehandle, CStrRef v_data) {
  FUNCTION_INJECTION(ppm_write);
  LINE(33,x_fwrite(toObject(v_filehandle), v_data));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 7 */
Variant f_real_part(CVarRef v_complex_number) {
  FUNCTION_INJECTION(real_part);
  return v_complex_number.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php line 3 */
Array f_complex(CVarRef v_real_part, CVarRef v_imag_part) {
  FUNCTION_INJECTION(complex);
  return Array(ArrayInit(2).set(0, v_real_part).set(1, v_imag_part).create());
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$julia_ppm_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$julia_ppm_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);

  (v_filename = "/tmp/julia.ppm");
  LINE(89,f_julia_ppm("/tmp/julia.ppm", 100LL, 25LL, -0.74543000000000004, 0.11302));
  print((LINE(91,(assignCallTemp(eo_1, toString(x_filesize(toString(v_filename)))),concat3("image file size: ", eo_1, "\n")))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
