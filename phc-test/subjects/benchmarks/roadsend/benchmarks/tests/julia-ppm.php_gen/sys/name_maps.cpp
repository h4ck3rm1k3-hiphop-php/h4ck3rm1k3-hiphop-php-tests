
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "complex", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  "complex_abs", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  "complex_add", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  "complex_multiply", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  "imag_part", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  "julia_ppm", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  "ppm_write", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  "real_part", "phc-test/subjects/benchmarks/roadsend/benchmarks/tests/julia-ppm.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
