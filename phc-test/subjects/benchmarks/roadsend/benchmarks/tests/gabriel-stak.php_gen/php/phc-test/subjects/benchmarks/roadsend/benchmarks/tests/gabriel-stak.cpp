
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/gabriel-stak.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/gabriel-stak.php line 19 */
Variant f_stak_aux() {
  FUNCTION_INJECTION(stak_aux);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant &gv_y __attribute__((__unused__)) = g->GV(y);
  Variant &gv_z __attribute__((__unused__)) = g->GV(z);
  Variant v_savedx;
  Variant v_savedy;
  Variant v_savedz;
  Variant v_newx;
  Variant v_newy;
  Variant v_newz;

  {
  }
  if (!((less(gv_y, gv_x)))) {
    return gv_z;
  }
  else {
    (v_savedx = gv_x);
    (v_savedy = gv_y);
    (v_savedz = gv_z);
    (gv_x = v_savedx - 1LL);
    (v_newx = LINE(30,f_stak_aux()));
    (gv_x = v_savedy - 1LL);
    (gv_y = v_savedz);
    (gv_z = v_savedx);
    (v_newy = LINE(35,f_stak_aux()));
    (gv_x = v_savedz - 1LL);
    (gv_y = v_savedx);
    (gv_z = v_savedy);
    (v_newz = LINE(40,f_stak_aux()));
    (gv_x = v_newx);
    (gv_y = v_newy);
    (gv_z = v_newz);
    return LINE(45,f_stak_aux());
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/gabriel-stak.php line 9 */
Variant f_stak(int64 v_x1, int64 v_y1, int64 v_z1) {
  FUNCTION_INJECTION(stak);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_x __attribute__((__unused__)) = g->GV(x);
  Variant &gv_y __attribute__((__unused__)) = g->GV(y);
  Variant &gv_z __attribute__((__unused__)) = g->GV(z);
  {
  }
  (gv_x = v_x1);
  (gv_y = v_y1);
  (gv_z = v_z1);
  return LINE(15,f_stak_aux());
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$gabriel_stak_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/gabriel-stak.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$gabriel_stak_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("From \"Performance and Evaluation of Lisp Systems\" by Richard Gabriel \npg. 102\n\n\"STAK is a variant of TAK; it uses special binding to pass arguments\nrather than the normal argument-passing mechanism.\"\n\n");
  print(LINE(49,(assignCallTemp(eo_1, toString(f_stak(18LL, 12LL, 6LL))),concat3("result: ", eo_1, "\n"))));
  echo("    \n    \n\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
