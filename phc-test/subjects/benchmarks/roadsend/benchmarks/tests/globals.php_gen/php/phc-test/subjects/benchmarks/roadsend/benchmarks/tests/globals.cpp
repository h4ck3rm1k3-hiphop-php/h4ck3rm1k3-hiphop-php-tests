
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/globals.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/globals.php line 45 */
void f_increment_k2(Variant v_var) {
  FUNCTION_INJECTION(increment_k2);
  echo(LINE(47,concat3("$k = \"", toString(v_var), "\"\n")));
  (v_var = 0LL);
  LOOP_COUNTER(1);
  {
    while (less(v_var, 1000000LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        ++v_var;
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/tests/globals.php line 38 */
void f_increment_global_k() {
  FUNCTION_INJECTION(increment_global_k);
  DECLARE_GLOBAL_VARIABLES(g);
  LOOP_COUNTER(2);
  {
    while (less(g->GV(k), 1000000LL)) {
      LOOP_COUNTER_CHECK(2);
      {
        ++lval(g->GV(k));
      }
    }
  }
  (g->GV(k) = "");
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$globals_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/globals.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$globals_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_time_start __attribute__((__unused__)) = (variables != gVariables) ? variables->get("time_start") : g->GV(time_start);
  Variant &v_numbered __attribute__((__unused__)) = (variables != gVariables) ? variables->get("numbered") : g->GV(numbered);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  echo("This test bangs on the globals.  Courtesy of BDKR.\n");
  LINE(4,x_set_time_limit(toInt32(0LL)));
  (v_time_start = 0LL);
  (get_variable_table()->get(0LL) = 0LL);
  (g->GV(Y) = 0LL);
  v_numbered.set(0LL, (0LL), 0x77CFA1EEF01BCA90LL);
  (v_x = 0LL);
  (v_k = 0LL);
  echo("Now starting the benchmark.\n");
  LOOP_COUNTER(3);
  {
    while (less(get_variable_table()->get(0LL), 1000000LL)) {
      LOOP_COUNTER_CHECK(3);
      {
        ++lval(get_variable_table()->get(0LL));
      }
    }
  }
  unset(get_variable_table()->get(0LL));
  LOOP_COUNTER(4);
  {
    while (less(g->GV(Y), 1000000LL)) {
      LOOP_COUNTER_CHECK(4);
      {
        ++lval(g->GV(Y));
      }
    }
  }
  LOOP_COUNTER(5);
  {
    while (less(v_numbered.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 1000000LL)) {
      LOOP_COUNTER_CHECK(5);
      {
        ++lval(v_numbered.lvalAt(0LL, 0x77CFA1EEF01BCA90LL));
      }
    }
  }
  LOOP_COUNTER(6);
  {
    while (less(v_x, 1000000LL)) {
      LOOP_COUNTER_CHECK(6);
      {
        ++v_x;
      }
    }
  }
  LINE(32,f_increment_global_k());
  LINE(33,f_increment_k2(ref(v_k)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
