
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_globals_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_globals_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/globals.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_increment_k2(Variant v_var);
void f_increment_global_k();
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$globals_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_tests_globals_h__
