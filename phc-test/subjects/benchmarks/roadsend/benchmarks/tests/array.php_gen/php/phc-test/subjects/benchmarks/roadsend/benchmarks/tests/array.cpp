
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/tests/array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/tests/array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$tests$array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_NUMRECS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("NUMRECS") : g->GV(NUMRECS);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_max __attribute__((__unused__)) = (variables != gVariables) ? variables->get("max") : g->GV(max);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_arr2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr2") : g->GV(arr2);

  (v_NUMRECS = 100000LL);
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_NUMRECS); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_arr.set(toString("a name ") + toString(v_i), (Array(ArrayInit(2).set(0, v_i).set(1, v_i).create())));
      }
    }
  }
  LINE(11,x_mt_srand(10LL));
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL), (v_max = divide(v_NUMRECS, 2LL)); less(v_i, v_max); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_n = concat("a name ", toString(LINE(14,x_mt_rand(0LL, toInt64(v_NUMRECS - 1LL))))));
        (v_arr2 = v_arr.rvalAt(v_n));
        if (!(toBoolean(v_arr2))) {
          print(LINE(17,concat3("Error searching for ", toString(v_n), " n")));
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
