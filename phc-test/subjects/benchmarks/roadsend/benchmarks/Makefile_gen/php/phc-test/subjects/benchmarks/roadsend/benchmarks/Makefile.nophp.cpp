
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/Makefile.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$Makefile(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/Makefile);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$Makefile;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("TOPLEVEL      = ../\nMY_TESTDIR    = benchmarks/tests/\nMY_REGRESSIONDIR    = benchmarks/regression/\nMY_TESTOUTDIR = benchmarks/testoutput/\nDOTEST        = ./dotest\n\ncheck : \n\t-rm -rf $(TOPLEVEL)$(MY_TESTOUTDIR)\n\t-mkdir $(TOPLEVEL)$(MY_TESTOUTDIR)\n\t@(cd $(TOPLEVEL) && $(DOTEST) $(MY_TESTDIR) $(MY_TESTOUTDIR))\n\nregression : \n\t-rm -rf $(TOPLEVEL)$(MY_TESTOUTDIR)\n\t-mkdir $(TOPLEVEL)$(MY_TESTOUTDIR)\n\t@(");
  echo("cd $(TOPLEVEL) && $(DOTEST) $(MY_REGRESSIONDIR) $(MY_TESTOUTDIR))\n\n\nclean :\n\t-rm -rf testoutput\n\n.PHONY: regression");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
