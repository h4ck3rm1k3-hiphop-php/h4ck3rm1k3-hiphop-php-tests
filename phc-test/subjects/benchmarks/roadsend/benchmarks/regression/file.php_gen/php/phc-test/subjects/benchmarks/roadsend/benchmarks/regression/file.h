
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_regression_file_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_regression_file_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/regression/file.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$regression$file_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_regression_file_h__
