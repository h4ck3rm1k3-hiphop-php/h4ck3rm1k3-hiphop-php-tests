
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/xml-data-0.xml.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$xml_data_0_xml(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/data/xml-data-0.xml);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$xml_data_0_xml;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<S");
  echo("M_XML>\n");
  echo("<S");
  echo("MCONFIG VERSION=\"1.2\">\n\n    ");
  echo("<S");
  echo("ITE>                                       \n        ");
  echo("<S");
  echo("ECTION name=\"dirs\">\n\n\t  <!-- check lib first -->\n            <VAR name=\"packages\"    value = \"packages/\"/>\n            <VAR name=\"modules\"     value = \"baseModules/\"/>\n            <VAR name=\"templates\"   value = \"baseSmartForms/\"/>\n            <VAR name=\"smartForms\"  value = \"baseSmartForms/\"/>\n            <VAR name=\"codePlates\"  value = \"baseCodePlates/\"/>\n            <VAR name=\"sfEntities\"  value = \"ba");
  echo("seInputEntities/\"/>\n            <VAR name=\"smTags\"      value = \"baseTags/\"/>\n            <VAR name=\"memberSystems\" value = \"lib/memberSystem/\" />\n\n            <VAR name=\"packages\"    value = \"{$SP_portalBase}packages/\"/>\n            <VAR name=\"modules\"     value = \"{$SP_portalBase}baseModules/\"/>\n            <VAR name=\"templates\"   value = \"{$SP_portalBase}baseSmartForms/\"/>\n            <VAR name=\"smar");
  echo("tForms\"  value = \"{$SP_portalBase}baseSmartForms/\"/>\n            <VAR name=\"codePlates\"  value = \"{$SP_portalBase}baseCodePlates/\"/>\n            <VAR name=\"sfEntities\"  value = \"{$SP_portalBase}baseInputEntities/\"/>\n            <VAR name=\"smTags\"      value = \"{$SP_portalBase}baseTags/\"/>\n            <VAR name=\"memberSystems\" value = \"{$SP_portalBase}lib/memberSystem/\" />\n        </SECTION>        \n   ");
  echo("     ");
  echo("<S");
  echo("ECTION name=\"compatibility\">\n            <VAR name=\"2.2.x\"               value = \"false\"  desc = \"set to true if you want more compatibility with SiteManager 2.2.x\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"debug\">\n            <VAR name=\"errorHandler\"        value=\"spErrorHandler\" desc=\"portal error handler\" />        \n        </SECTION>        \n        ");
  echo("<S");
  echo("ECTION name=\"defaults\">\n            <VAR name=\"autoRealmAuthSelect\" value=\"false\" desc=\"when true, if a member attempts to access a realm they are not authenticated in, but they are authenticated in a different realm, select that other realm as current\" />\n            <VAR name=\"fuzzyLogins\"         value=\"true\" desc=\"when true, if member attempts authentication in one realm but fails, check parent ");
  echo("realms for same user/pass and auth in those realms instead\" />\n            <VAR name=\"realmHistoryCount\"   value=\"5\" desc=\"number of realms to keep in users realm history list\" />\n            <VAR name=\"useMemberRealmConfig\" value=\"false\" desc=\"when true, memberRealmConfig is used when determining realm/member page, etc to use on page load\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"sessions\">                \n            <VAR name=\"containerType\"    value = \"database\"          desc=\"session container\" />\n            <VAR name=\"useMemberSystem\"     value = \"true\"              desc=\"main on/off switch for member system. if on, make sure 'members' section is setup\" />\n            <VAR name=\"sessionCookie\"       value = \"SMPSESSION\"        desc=\"when non blank, create a");
  echo(" cookie on the remote users machine that stores their session id\" />            \n            <VAR name=\"autoLogin\"           value = \"true\"              desc=\"when true, if a session cookie was found, try to restore their session\" />\n            <VAR name=\"badSessionPage\"      value = \"home/home.php\"     desc=\"page to send to when a session is invalid\" />\n            <VAR name=\"sessionIDName\"       v");
  echo("alue = \"sID\"               desc=\"variable name to use to store sessions ID\" /> \n            <VAR name=\"defaultScript\"       value = \"index.php\"         desc=\"the default script that loads when no script is specified\" />\n            <VAR name=\"forceSessionID\"      value = \"false\" />\n        </SECTION>        \n        ");
  echo("<S");
  echo("ECTION name=\"sessionContainer\" id=\"database\">\n            <VAR name=\"serialize\"           value=\"true\"            desc=\"whether to serialize all persistent variables or not. must be true to keep arrays and objects perisistent\" />            \n            <VAR name=\"dataBaseID\"          value=\"default\"         desc=\"which database connection (from db section) should SiteManager use for sessions\" />\n  ");
  echo("          <VAR name=\"sessionTable\"        value=\"sessions\"        desc=\"sessions table name\" />            \n        </SECTION>        \n        ");
  echo("<S");
  echo("ECTION name=\"members\">\n            <VAR name=\"memberSystemType\"     value=\"portal\"        desc=\"which member system handler to use\" />\n            <VAR name=\"usePersistentMembers\" value=\"true\"           desc=\"when true, member data will be keep as a persistent variable instead of loaded in from the container each time\" />\n            <VAR name=\"maintainTables\"       value=\"false\"          desc=\"if tr");
  echo("ue, SiteManager will delete old sessions. performance hit, so defaults to false. better to run a cron'd script.\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"memberSystem\" id=\"portal\">\n            <VAR name=\"dataBaseID\"          value=\"default\"          desc=\"which database connection (from db section) should SiteManager use for sessions\" />        \n            <VAR name=\"memberTable\"         value=\"members\"          desc=\"member table name\" />            \n            <VAR name=\"memberInfoTable\"     value=\"memberInfo\"       desc=\"member table");
  echo(" name\" />            \n            <VAR name=\"memberInfoForm\"      value=\"memberInfoForm\"   desc=\"member table form name\" />            \n            <VAR name=\"memberSessionsTable\" value=\"memberSessions\"   desc=\"logged in member session table\" />\n            <VAR name=\"userNameField\"       value=\"userName\"         desc=\"the username field\" />            \n            <VAR name=\"passWordField\"       valu");
  echo("e=\"passWord\"         desc=\"the password field\" />\n            <VAR name=\"uIDField\"            value=\"uID\"              desc=\"the unique user id field\" />            \n            <VAR name=\"sIDField\"            value=\"sessionID\"        desc=\"the unique session id field\" />\n            <VAR name=\"dcField\"             value=\"dateCreated\"      desc=\"DATETIME field of session creation\" />            \n    ");
  echo("    </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"pVars\">\n            <VAR name=\"SP_realmTrack\" desc=\"realm a guest member is in\" />\n            <VAR name=\"SP_guestPage\" desc=\"page guest member is looking at\" />\n            <VAR name=\"SP_realmHistory\" desc=\"list of realms the user has been in, with timestamp\" />\n        </SECTION>\n    </SITE>\n        \n</SMCONFIG>\n</SM_XML>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
