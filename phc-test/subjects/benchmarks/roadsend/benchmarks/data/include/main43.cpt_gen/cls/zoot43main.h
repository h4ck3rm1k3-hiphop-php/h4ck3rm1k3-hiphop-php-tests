
#ifndef __GENERATED_cls_zoot43main_h__
#define __GENERATED_cls_zoot43main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main43.cpt line 23 */
class c_zoot43main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot43main)
  END_CLASS_MAP(zoot43main)
  DECLARE_CLASS(zoot43main, zoot43main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot43main_h__
