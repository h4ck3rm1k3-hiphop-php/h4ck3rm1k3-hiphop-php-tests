
#ifndef __GENERATED_cls_zoot89main_h__
#define __GENERATED_cls_zoot89main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main89.cpt line 23 */
class c_zoot89main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot89main)
  END_CLASS_MAP(zoot89main)
  DECLARE_CLASS(zoot89main, zoot89main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot89main_h__
