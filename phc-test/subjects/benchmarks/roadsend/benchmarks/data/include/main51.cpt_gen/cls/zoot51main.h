
#ifndef __GENERATED_cls_zoot51main_h__
#define __GENERATED_cls_zoot51main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main51.cpt line 23 */
class c_zoot51main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot51main)
  END_CLASS_MAP(zoot51main)
  DECLARE_CLASS(zoot51main, zoot51main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot51main_h__
