
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_inc16_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_inc16_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc16.fw.h>

// Declarations
#include <cls/cfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$inc16_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_cfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_inc16_h__
