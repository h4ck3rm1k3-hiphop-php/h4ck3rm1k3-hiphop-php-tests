
#ifndef __GENERATED_cls_zoot66main_h__
#define __GENERATED_cls_zoot66main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main66.cpt line 23 */
class c_zoot66main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot66main)
  END_CLASS_MAP(zoot66main)
  DECLARE_CLASS(zoot66main, zoot66main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot66main_h__
