
#ifndef __GENERATED_cls_zoot63main_h__
#define __GENERATED_cls_zoot63main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main63.cpt line 23 */
class c_zoot63main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot63main)
  END_CLASS_MAP(zoot63main)
  DECLARE_CLASS(zoot63main, zoot63main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot63main_h__
