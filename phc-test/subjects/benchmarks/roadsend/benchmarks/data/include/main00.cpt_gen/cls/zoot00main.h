
#ifndef __GENERATED_cls_zoot00main_h__
#define __GENERATED_cls_zoot00main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main00.cpt line 23 */
class c_zoot00main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot00main)
  END_CLASS_MAP(zoot00main)
  DECLARE_CLASS(zoot00main, zoot00main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot00main_h__
