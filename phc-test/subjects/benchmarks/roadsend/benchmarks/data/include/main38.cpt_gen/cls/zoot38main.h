
#ifndef __GENERATED_cls_zoot38main_h__
#define __GENERATED_cls_zoot38main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main38.cpt line 23 */
class c_zoot38main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot38main)
  END_CLASS_MAP(zoot38main)
  DECLARE_CLASS(zoot38main, zoot38main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot38main_h__
