
#ifndef __GENERATED_cls_zoot25main_h__
#define __GENERATED_cls_zoot25main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main25.cpt line 23 */
class c_zoot25main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot25main)
  END_CLASS_MAP(zoot25main)
  DECLARE_CLASS(zoot25main, zoot25main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot25main_h__
