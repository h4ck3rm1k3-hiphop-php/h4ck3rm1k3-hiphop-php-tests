
#ifndef __GENERATED_cls_nfunctionmenu_h__
#define __GENERATED_cls_nfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc.php line 22 */
class c_nfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(nfunctionmenu)
  END_CLASS_MAP(nfunctionmenu)
  DECLARE_CLASS(nfunctionmenu, nFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_nfunctionmenu_h__
