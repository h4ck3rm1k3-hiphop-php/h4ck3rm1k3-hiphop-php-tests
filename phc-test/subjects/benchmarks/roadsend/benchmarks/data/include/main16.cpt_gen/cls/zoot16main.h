
#ifndef __GENERATED_cls_zoot16main_h__
#define __GENERATED_cls_zoot16main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main16.cpt line 23 */
class c_zoot16main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot16main)
  END_CLASS_MAP(zoot16main)
  DECLARE_CLASS(zoot16main, zoot16main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot16main_h__
