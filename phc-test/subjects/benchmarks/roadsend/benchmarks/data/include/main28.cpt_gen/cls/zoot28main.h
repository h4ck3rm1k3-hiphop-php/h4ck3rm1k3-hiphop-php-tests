
#ifndef __GENERATED_cls_zoot28main_h__
#define __GENERATED_cls_zoot28main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main28.cpt line 23 */
class c_zoot28main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot28main)
  END_CLASS_MAP(zoot28main)
  DECLARE_CLASS(zoot28main, zoot28main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot28main_h__
