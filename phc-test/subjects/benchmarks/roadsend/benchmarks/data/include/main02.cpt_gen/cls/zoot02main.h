
#ifndef __GENERATED_cls_zoot02main_h__
#define __GENERATED_cls_zoot02main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main02.cpt line 23 */
class c_zoot02main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot02main)
  END_CLASS_MAP(zoot02main)
  DECLARE_CLASS(zoot02main, zoot02main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot02main_h__
