
#ifndef __GENERATED_cls_zoot12main_h__
#define __GENERATED_cls_zoot12main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main12.cpt line 23 */
class c_zoot12main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot12main)
  END_CLASS_MAP(zoot12main)
  DECLARE_CLASS(zoot12main, zoot12main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot12main_h__
