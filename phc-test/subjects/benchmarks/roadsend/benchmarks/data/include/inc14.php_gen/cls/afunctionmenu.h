
#ifndef __GENERATED_cls_afunctionmenu_h__
#define __GENERATED_cls_afunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc14.php line 22 */
class c_afunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(afunctionmenu)
  END_CLASS_MAP(afunctionmenu)
  DECLARE_CLASS(afunctionmenu, aFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_afunctionmenu_h__
