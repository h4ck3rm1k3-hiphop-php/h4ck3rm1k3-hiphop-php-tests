
#ifndef __GENERATED_cls_zoot10main_h__
#define __GENERATED_cls_zoot10main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main10.cpt line 23 */
class c_zoot10main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot10main)
  END_CLASS_MAP(zoot10main)
  DECLARE_CLASS(zoot10main, zoot10main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot10main_h__
