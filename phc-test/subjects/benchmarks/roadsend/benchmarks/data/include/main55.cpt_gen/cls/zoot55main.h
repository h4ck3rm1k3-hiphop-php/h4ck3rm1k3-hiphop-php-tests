
#ifndef __GENERATED_cls_zoot55main_h__
#define __GENERATED_cls_zoot55main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main55.cpt line 23 */
class c_zoot55main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot55main)
  END_CLASS_MAP(zoot55main)
  DECLARE_CLASS(zoot55main, zoot55main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot55main_h__
