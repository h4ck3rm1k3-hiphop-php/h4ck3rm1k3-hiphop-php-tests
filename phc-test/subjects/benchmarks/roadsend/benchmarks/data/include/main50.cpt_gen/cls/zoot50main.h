
#ifndef __GENERATED_cls_zoot50main_h__
#define __GENERATED_cls_zoot50main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main50.cpt line 23 */
class c_zoot50main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot50main)
  END_CLASS_MAP(zoot50main)
  DECLARE_CLASS(zoot50main, zoot50main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot50main_h__
