
#ifndef __GENERATED_cls_zoot49main_h__
#define __GENERATED_cls_zoot49main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main49.cpt line 23 */
class c_zoot49main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot49main)
  END_CLASS_MAP(zoot49main)
  DECLARE_CLASS(zoot49main, zoot49main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot49main_h__
