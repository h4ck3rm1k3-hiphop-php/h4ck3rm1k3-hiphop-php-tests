
#ifndef __GENERATED_cls_zoot48main_h__
#define __GENERATED_cls_zoot48main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main48.cpt line 23 */
class c_zoot48main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot48main)
  END_CLASS_MAP(zoot48main)
  DECLARE_CLASS(zoot48main, zoot48main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot48main_h__
