
#ifndef __GENERATED_cls_zoot81main_h__
#define __GENERATED_cls_zoot81main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main81.cpt line 23 */
class c_zoot81main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot81main)
  END_CLASS_MAP(zoot81main)
  DECLARE_CLASS(zoot81main, zoot81main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot81main_h__
