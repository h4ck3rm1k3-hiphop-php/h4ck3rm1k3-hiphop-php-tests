
#ifndef __GENERATED_cls_zoot57main_h__
#define __GENERATED_cls_zoot57main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main57.cpt line 23 */
class c_zoot57main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot57main)
  END_CLASS_MAP(zoot57main)
  DECLARE_CLASS(zoot57main, zoot57main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot57main_h__
