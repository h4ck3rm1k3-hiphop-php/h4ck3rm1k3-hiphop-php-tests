
#ifndef __GENERATED_cls_zoot82main_h__
#define __GENERATED_cls_zoot82main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main82.cpt line 23 */
class c_zoot82main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot82main)
  END_CLASS_MAP(zoot82main)
  DECLARE_CLASS(zoot82main, zoot82main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot82main_h__
