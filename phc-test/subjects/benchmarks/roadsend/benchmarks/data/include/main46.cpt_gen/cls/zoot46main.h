
#ifndef __GENERATED_cls_zoot46main_h__
#define __GENERATED_cls_zoot46main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main46.cpt line 23 */
class c_zoot46main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot46main)
  END_CLASS_MAP(zoot46main)
  DECLARE_CLASS(zoot46main, zoot46main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot46main_h__
