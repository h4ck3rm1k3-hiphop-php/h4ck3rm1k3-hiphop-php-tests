
#ifndef __GENERATED_cls_zoot70main_h__
#define __GENERATED_cls_zoot70main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main70.cpt line 23 */
class c_zoot70main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot70main)
  END_CLASS_MAP(zoot70main)
  DECLARE_CLASS(zoot70main, zoot70main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot70main_h__
