
#ifndef __GENERATED_cls_zoot42main_h__
#define __GENERATED_cls_zoot42main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main42.cpt line 23 */
class c_zoot42main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot42main)
  END_CLASS_MAP(zoot42main)
  DECLARE_CLASS(zoot42main, zoot42main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot42main_h__
