
#ifndef __GENERATED_cls_zoot96main_h__
#define __GENERATED_cls_zoot96main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main96.cpt line 23 */
class c_zoot96main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot96main)
  END_CLASS_MAP(zoot96main)
  DECLARE_CLASS(zoot96main, zoot96main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot96main_h__
