
#ifndef __GENERATED_cls_zoot95main_h__
#define __GENERATED_cls_zoot95main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main95.cpt line 23 */
class c_zoot95main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot95main)
  END_CLASS_MAP(zoot95main)
  DECLARE_CLASS(zoot95main, zoot95main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot95main_h__
