
#ifndef __GENERATED_cls_sm_smtag_aread_h__
#define __GENERATED_cls_sm_smtag_aread_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc4.php line 23 */
class c_sm_smtag_aread : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_aread)
  END_CLASS_MAP(sm_smtag_aread)
  DECLARE_CLASS(sm_smtag_aread, SM_smTag_AREAd, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_aread_h__
