
#ifndef __GENERATED_cls_zootmain_h__
#define __GENERATED_cls_zootmain_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main.cpt line 23 */
class c_zootmain : virtual public ObjectData {
  BEGIN_CLASS_MAP(zootmain)
  END_CLASS_MAP(zootmain)
  DECLARE_CLASS(zootmain, zootmain, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zootmain_h__
