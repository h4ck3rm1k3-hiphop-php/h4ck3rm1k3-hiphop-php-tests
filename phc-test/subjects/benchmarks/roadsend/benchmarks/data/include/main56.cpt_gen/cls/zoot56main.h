
#ifndef __GENERATED_cls_zoot56main_h__
#define __GENERATED_cls_zoot56main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main56.cpt line 23 */
class c_zoot56main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot56main)
  END_CLASS_MAP(zoot56main)
  DECLARE_CLASS(zoot56main, zoot56main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot56main_h__
