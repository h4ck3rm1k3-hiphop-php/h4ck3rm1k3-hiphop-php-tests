
#ifndef __GENERATED_cls_zoot88main_h__
#define __GENERATED_cls_zoot88main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main88.cpt line 23 */
class c_zoot88main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot88main)
  END_CLASS_MAP(zoot88main)
  DECLARE_CLASS(zoot88main, zoot88main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot88main_h__
