
#ifndef __GENERATED_cls_zoot78main_h__
#define __GENERATED_cls_zoot78main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main78.cpt line 23 */
class c_zoot78main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot78main)
  END_CLASS_MAP(zoot78main)
  DECLARE_CLASS(zoot78main, zoot78main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot78main_h__
