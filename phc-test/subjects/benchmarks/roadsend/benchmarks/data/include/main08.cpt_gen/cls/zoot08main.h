
#ifndef __GENERATED_cls_zoot08main_h__
#define __GENERATED_cls_zoot08main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main08.cpt line 23 */
class c_zoot08main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot08main)
  END_CLASS_MAP(zoot08main)
  DECLARE_CLASS(zoot08main, zoot08main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot08main_h__
