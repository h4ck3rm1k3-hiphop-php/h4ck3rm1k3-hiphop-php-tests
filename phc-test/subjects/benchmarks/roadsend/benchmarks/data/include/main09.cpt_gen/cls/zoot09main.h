
#ifndef __GENERATED_cls_zoot09main_h__
#define __GENERATED_cls_zoot09main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main09.cpt line 23 */
class c_zoot09main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot09main)
  END_CLASS_MAP(zoot09main)
  DECLARE_CLASS(zoot09main, zoot09main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot09main_h__
