
#ifndef __GENERATED_cls_zoot39main_h__
#define __GENERATED_cls_zoot39main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main39.cpt line 23 */
class c_zoot39main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot39main)
  END_CLASS_MAP(zoot39main)
  DECLARE_CLASS(zoot39main, zoot39main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot39main_h__
