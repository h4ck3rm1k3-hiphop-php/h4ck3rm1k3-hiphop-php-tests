
#ifndef __GENERATED_cls_ffunctionmenu_h__
#define __GENERATED_cls_ffunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc19.php line 22 */
class c_ffunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(ffunctionmenu)
  END_CLASS_MAP(ffunctionmenu)
  DECLARE_CLASS(ffunctionmenu, fFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ffunctionmenu_h__
