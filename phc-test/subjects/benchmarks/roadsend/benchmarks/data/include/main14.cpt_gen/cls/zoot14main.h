
#ifndef __GENERATED_cls_zoot14main_h__
#define __GENERATED_cls_zoot14main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main14.cpt line 23 */
class c_zoot14main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot14main)
  END_CLASS_MAP(zoot14main)
  DECLARE_CLASS(zoot14main, zoot14main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot14main_h__
