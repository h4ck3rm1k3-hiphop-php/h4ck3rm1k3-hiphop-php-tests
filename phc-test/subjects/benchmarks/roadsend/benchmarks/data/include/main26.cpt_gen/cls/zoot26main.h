
#ifndef __GENERATED_cls_zoot26main_h__
#define __GENERATED_cls_zoot26main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main26.cpt line 23 */
class c_zoot26main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot26main)
  END_CLASS_MAP(zoot26main)
  DECLARE_CLASS(zoot26main, zoot26main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot26main_h__
