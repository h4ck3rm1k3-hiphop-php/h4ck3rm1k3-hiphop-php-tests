
#ifndef __GENERATED_cls_zoot60main_h__
#define __GENERATED_cls_zoot60main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main60.cpt line 23 */
class c_zoot60main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot60main)
  END_CLASS_MAP(zoot60main)
  DECLARE_CLASS(zoot60main, zoot60main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot60main_h__
