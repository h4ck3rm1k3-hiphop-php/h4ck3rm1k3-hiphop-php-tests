
#ifndef __GENERATED_cls_zoot94main_h__
#define __GENERATED_cls_zoot94main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main94.cpt line 23 */
class c_zoot94main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot94main)
  END_CLASS_MAP(zoot94main)
  DECLARE_CLASS(zoot94main, zoot94main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot94main_h__
