
#ifndef __GENERATED_cls_sm_smtag_arear_h__
#define __GENERATED_cls_sm_smtag_arear_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc18.php line 23 */
class c_sm_smtag_arear : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_arear)
  END_CLASS_MAP(sm_smtag_arear)
  DECLARE_CLASS(sm_smtag_arear, SM_smTag_AREAr, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_arear_h__
