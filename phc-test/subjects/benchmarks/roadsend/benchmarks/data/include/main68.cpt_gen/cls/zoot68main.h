
#ifndef __GENERATED_cls_zoot68main_h__
#define __GENERATED_cls_zoot68main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main68.cpt line 23 */
class c_zoot68main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot68main)
  END_CLASS_MAP(zoot68main)
  DECLARE_CLASS(zoot68main, zoot68main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot68main_h__
