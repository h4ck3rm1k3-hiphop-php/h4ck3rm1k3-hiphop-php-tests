
#ifndef __GENERATED_cls_zoot23main_h__
#define __GENERATED_cls_zoot23main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main23.cpt line 23 */
class c_zoot23main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot23main)
  END_CLASS_MAP(zoot23main)
  DECLARE_CLASS(zoot23main, zoot23main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot23main_h__
