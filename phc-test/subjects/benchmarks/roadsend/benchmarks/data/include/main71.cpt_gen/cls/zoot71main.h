
#ifndef __GENERATED_cls_zoot71main_h__
#define __GENERATED_cls_zoot71main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main71.cpt line 23 */
class c_zoot71main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot71main)
  END_CLASS_MAP(zoot71main)
  DECLARE_CLASS(zoot71main, zoot71main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot71main_h__
