
#ifndef __GENERATED_cls_zoot67main_h__
#define __GENERATED_cls_zoot67main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main67.cpt line 23 */
class c_zoot67main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot67main)
  END_CLASS_MAP(zoot67main)
  DECLARE_CLASS(zoot67main, zoot67main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot67main_h__
