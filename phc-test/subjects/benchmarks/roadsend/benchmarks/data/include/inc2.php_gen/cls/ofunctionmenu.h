
#ifndef __GENERATED_cls_ofunctionmenu_h__
#define __GENERATED_cls_ofunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc2.php line 22 */
class c_ofunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(ofunctionmenu)
  END_CLASS_MAP(ofunctionmenu)
  DECLARE_CLASS(ofunctionmenu, oFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ofunctionmenu_h__
