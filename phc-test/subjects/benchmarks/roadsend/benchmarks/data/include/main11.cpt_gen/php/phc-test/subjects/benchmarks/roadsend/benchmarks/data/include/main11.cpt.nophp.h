
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_main11_cpt_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_main11_cpt_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main11.cpt.nophp.fw.h>

// Declarations
#include <cls/zoot11main.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$main11_cpt(bool incOnce = false, LVariableTable* variables = NULL);
Object co_zoot11main(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_main11_cpt_nophp_h__
