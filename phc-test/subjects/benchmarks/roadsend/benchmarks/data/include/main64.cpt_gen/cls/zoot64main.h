
#ifndef __GENERATED_cls_zoot64main_h__
#define __GENERATED_cls_zoot64main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main64.cpt line 23 */
class c_zoot64main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot64main)
  END_CLASS_MAP(zoot64main)
  DECLARE_CLASS(zoot64main, zoot64main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot64main_h__
