
#ifndef __GENERATED_cls_zoot05main_h__
#define __GENERATED_cls_zoot05main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main05.cpt line 23 */
class c_zoot05main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot05main)
  END_CLASS_MAP(zoot05main)
  DECLARE_CLASS(zoot05main, zoot05main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot05main_h__
