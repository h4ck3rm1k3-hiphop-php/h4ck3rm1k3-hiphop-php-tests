
#ifndef __GENERATED_cls_zoot01main_h__
#define __GENERATED_cls_zoot01main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main01.cpt line 23 */
class c_zoot01main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot01main)
  END_CLASS_MAP(zoot01main)
  DECLARE_CLASS(zoot01main, zoot01main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot01main_h__
