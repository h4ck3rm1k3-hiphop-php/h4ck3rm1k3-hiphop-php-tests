
#ifndef __GENERATED_cls_zoot73main_h__
#define __GENERATED_cls_zoot73main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main73.cpt line 23 */
class c_zoot73main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot73main)
  END_CLASS_MAP(zoot73main)
  DECLARE_CLASS(zoot73main, zoot73main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot73main_h__
