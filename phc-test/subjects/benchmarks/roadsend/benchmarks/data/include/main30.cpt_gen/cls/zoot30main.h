
#ifndef __GENERATED_cls_zoot30main_h__
#define __GENERATED_cls_zoot30main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main30.cpt line 23 */
class c_zoot30main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot30main)
  END_CLASS_MAP(zoot30main)
  DECLARE_CLASS(zoot30main, zoot30main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot30main_h__
