
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main69.cpt.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main69.cpt line 23 */
Variant c_zoot69main::os_get(const char *s, int64 hash) {
  return c_sm_codeplate::os_get(s, hash);
}
Variant &c_zoot69main::os_lval(const char *s, int64 hash) {
  return c_sm_codeplate::os_lval(s, hash);
}
void c_zoot69main::o_get(ArrayElementVec &props) const {
  c_sm_codeplate::o_get(props);
}
bool c_zoot69main::o_exists(CStrRef s, int64 hash) const {
  return c_sm_codeplate::o_exists(s, hash);
}
Variant c_zoot69main::o_get(CStrRef s, int64 hash) {
  return c_sm_codeplate::o_get(s, hash);
}
Variant c_zoot69main::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_sm_codeplate::o_set(s, hash, v, forInit);
}
Variant &c_zoot69main::o_lval(CStrRef s, int64 hash) {
  return c_sm_codeplate::o_lval(s, hash);
}
Variant c_zoot69main::os_constant(const char *s) {
  return c_sm_codeplate::os_constant(s);
}
IMPLEMENT_CLASS(zoot69main)
ObjectData *c_zoot69main::cloneImpl() {
  c_zoot69main *obj = NEW(c_zoot69main)();
  cloneSet(obj);
  return obj;
}
void c_zoot69main::cloneSet(c_zoot69main *clone) {
  c_sm_codeplate::cloneSet(clone);
}
Variant c_zoot69main::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_sm_codeplate::o_invoke(s, params, hash, fatal);
}
Variant c_zoot69main::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_sm_codeplate::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_zoot69main::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_sm_codeplate::os_invoke(c, s, params, hash, fatal);
}
Variant cw_zoot69main$os_get(const char *s) {
  return c_zoot69main::os_get(s, -1);
}
Variant &cw_zoot69main$os_lval(const char *s) {
  return c_zoot69main::os_lval(s, -1);
}
Variant cw_zoot69main$os_constant(const char *s) {
  return c_zoot69main::os_constant(s);
}
Variant cw_zoot69main$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_zoot69main::os_invoke(c, s, params, -1, fatal);
}
void c_zoot69main::init() {
  c_sm_codeplate::init();
}
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main69.cpt line 30 */
void c_zoot69main::t_codeplateconfig() {
  INSTANCE_METHOD_INJECTION(zoot69main, zoot69main::codePlateConfig);
  LINE(32,o_root_invoke_few_args("setMyTemplate", 0x0943195295DC8BE5LL, 1, "main"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main69.cpt line 42 */
void c_zoot69main::t_codeplatethink() {
  INSTANCE_METHOD_INJECTION(zoot69main, zoot69main::codePlateThink);
  LINE(46,o_root_invoke_few_args("loadModule", 0x701E36C56F3C23FALL, 2, "nFunctionMenu", "leftBar"));
  LINE(49,o_root_invoke_few_args("setAreaPriority", 0x2E5EADFE8B2CF8F8LL, 2, "main", 20LL));
} /* function */
Object co_zoot69main(CArrRef params, bool init /* = true */) {
  return Object(p_zoot69main(NEW(c_zoot69main)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$main69_cpt(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main69.cpt);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$main69_cpt;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("zoot69main\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
