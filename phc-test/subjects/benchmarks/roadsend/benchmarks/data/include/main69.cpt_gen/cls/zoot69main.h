
#ifndef __GENERATED_cls_zoot69main_h__
#define __GENERATED_cls_zoot69main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main69.cpt line 23 */
class c_zoot69main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot69main)
  END_CLASS_MAP(zoot69main)
  DECLARE_CLASS(zoot69main, zoot69main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot69main_h__
