
#ifndef __GENERATED_cls_zoot84main_h__
#define __GENERATED_cls_zoot84main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main84.cpt line 23 */
class c_zoot84main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot84main)
  END_CLASS_MAP(zoot84main)
  DECLARE_CLASS(zoot84main, zoot84main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot84main_h__
