
#ifndef __GENERATED_cls_zoot31main_h__
#define __GENERATED_cls_zoot31main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main31.cpt line 23 */
class c_zoot31main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot31main)
  END_CLASS_MAP(zoot31main)
  DECLARE_CLASS(zoot31main, zoot31main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot31main_h__
