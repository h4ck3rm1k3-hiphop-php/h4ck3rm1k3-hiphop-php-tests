
#ifndef __GENERATED_cls_zoot20main_h__
#define __GENERATED_cls_zoot20main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main20.cpt line 23 */
class c_zoot20main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot20main)
  END_CLASS_MAP(zoot20main)
  DECLARE_CLASS(zoot20main, zoot20main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot20main_h__
