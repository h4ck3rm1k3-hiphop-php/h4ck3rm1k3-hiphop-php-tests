
#ifndef __GENERATED_cls_zoot92main_h__
#define __GENERATED_cls_zoot92main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main92.cpt line 23 */
class c_zoot92main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot92main)
  END_CLASS_MAP(zoot92main)
  DECLARE_CLASS(zoot92main, zoot92main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot92main_h__
