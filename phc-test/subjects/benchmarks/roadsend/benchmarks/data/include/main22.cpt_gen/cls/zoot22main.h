
#ifndef __GENERATED_cls_zoot22main_h__
#define __GENERATED_cls_zoot22main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main22.cpt line 23 */
class c_zoot22main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot22main)
  END_CLASS_MAP(zoot22main)
  DECLARE_CLASS(zoot22main, zoot22main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot22main_h__
