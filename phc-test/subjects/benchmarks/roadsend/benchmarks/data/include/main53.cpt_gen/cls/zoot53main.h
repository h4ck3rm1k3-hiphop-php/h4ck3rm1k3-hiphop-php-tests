
#ifndef __GENERATED_cls_zoot53main_h__
#define __GENERATED_cls_zoot53main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main53.cpt line 23 */
class c_zoot53main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot53main)
  END_CLASS_MAP(zoot53main)
  DECLARE_CLASS(zoot53main, zoot53main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot53main_h__
