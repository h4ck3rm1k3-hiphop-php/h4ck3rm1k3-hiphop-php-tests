
#ifndef __GENERATED_cls_zoot58main_h__
#define __GENERATED_cls_zoot58main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main58.cpt line 23 */
class c_zoot58main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot58main)
  END_CLASS_MAP(zoot58main)
  DECLARE_CLASS(zoot58main, zoot58main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot58main_h__
