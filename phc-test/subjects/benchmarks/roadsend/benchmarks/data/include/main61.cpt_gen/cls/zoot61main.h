
#ifndef __GENERATED_cls_zoot61main_h__
#define __GENERATED_cls_zoot61main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main61.cpt line 23 */
class c_zoot61main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot61main)
  END_CLASS_MAP(zoot61main)
  DECLARE_CLASS(zoot61main, zoot61main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot61main_h__
