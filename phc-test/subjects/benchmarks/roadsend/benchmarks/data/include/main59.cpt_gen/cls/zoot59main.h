
#ifndef __GENERATED_cls_zoot59main_h__
#define __GENERATED_cls_zoot59main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main59.cpt line 23 */
class c_zoot59main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot59main)
  END_CLASS_MAP(zoot59main)
  DECLARE_CLASS(zoot59main, zoot59main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot59main_h__
