
#ifndef __GENERATED_cls_wfunctionmenu_h__
#define __GENERATED_cls_wfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc10.php line 22 */
class c_wfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(wfunctionmenu)
  END_CLASS_MAP(wfunctionmenu)
  DECLARE_CLASS(wfunctionmenu, wFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_wfunctionmenu_h__
