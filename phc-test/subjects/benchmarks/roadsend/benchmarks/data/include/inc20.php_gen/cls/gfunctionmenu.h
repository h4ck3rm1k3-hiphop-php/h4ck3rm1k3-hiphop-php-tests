
#ifndef __GENERATED_cls_gfunctionmenu_h__
#define __GENERATED_cls_gfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc20.php line 22 */
class c_gfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(gfunctionmenu)
  END_CLASS_MAP(gfunctionmenu)
  DECLARE_CLASS(gfunctionmenu, gFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_gfunctionmenu_h__
