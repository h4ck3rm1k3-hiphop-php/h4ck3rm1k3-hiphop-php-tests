
#ifndef __GENERATED_cls_zoot34main_h__
#define __GENERATED_cls_zoot34main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main34.cpt line 23 */
class c_zoot34main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot34main)
  END_CLASS_MAP(zoot34main)
  DECLARE_CLASS(zoot34main, zoot34main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot34main_h__
