
#ifndef __GENERATED_cls_dfunctionmenu_h__
#define __GENERATED_cls_dfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc17.php line 22 */
class c_dfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(dfunctionmenu)
  END_CLASS_MAP(dfunctionmenu)
  DECLARE_CLASS(dfunctionmenu, dFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_dfunctionmenu_h__
