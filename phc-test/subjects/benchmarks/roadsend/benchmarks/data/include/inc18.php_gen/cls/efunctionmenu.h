
#ifndef __GENERATED_cls_efunctionmenu_h__
#define __GENERATED_cls_efunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc18.php line 22 */
class c_efunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(efunctionmenu)
  END_CLASS_MAP(efunctionmenu)
  DECLARE_CLASS(efunctionmenu, eFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_efunctionmenu_h__
