
#ifndef __GENERATED_cls_zoot21main_h__
#define __GENERATED_cls_zoot21main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main21.cpt line 23 */
class c_zoot21main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot21main)
  END_CLASS_MAP(zoot21main)
  DECLARE_CLASS(zoot21main, zoot21main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot21main_h__
