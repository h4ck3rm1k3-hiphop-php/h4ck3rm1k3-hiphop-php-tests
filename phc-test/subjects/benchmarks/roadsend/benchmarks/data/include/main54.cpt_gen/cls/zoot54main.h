
#ifndef __GENERATED_cls_zoot54main_h__
#define __GENERATED_cls_zoot54main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main54.cpt line 23 */
class c_zoot54main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot54main)
  END_CLASS_MAP(zoot54main)
  DECLARE_CLASS(zoot54main, zoot54main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot54main_h__
