
#ifndef __GENERATED_cls_zoot04main_h__
#define __GENERATED_cls_zoot04main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main04.cpt line 23 */
class c_zoot04main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot04main)
  END_CLASS_MAP(zoot04main)
  DECLARE_CLASS(zoot04main, zoot04main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot04main_h__
