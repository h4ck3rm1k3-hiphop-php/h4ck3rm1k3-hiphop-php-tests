
#ifndef __GENERATED_cls_zoot33main_h__
#define __GENERATED_cls_zoot33main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main33.cpt line 23 */
class c_zoot33main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot33main)
  END_CLASS_MAP(zoot33main)
  DECLARE_CLASS(zoot33main, zoot33main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot33main_h__
