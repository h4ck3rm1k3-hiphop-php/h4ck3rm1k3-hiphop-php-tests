
#ifndef __GENERATED_cls_zoot80main_h__
#define __GENERATED_cls_zoot80main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main80.cpt line 23 */
class c_zoot80main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot80main)
  END_CLASS_MAP(zoot80main)
  DECLARE_CLASS(zoot80main, zoot80main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot80main_h__
