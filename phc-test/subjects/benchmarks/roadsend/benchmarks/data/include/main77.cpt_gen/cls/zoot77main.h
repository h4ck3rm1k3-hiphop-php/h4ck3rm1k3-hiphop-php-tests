
#ifndef __GENERATED_cls_zoot77main_h__
#define __GENERATED_cls_zoot77main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main77.cpt line 23 */
class c_zoot77main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot77main)
  END_CLASS_MAP(zoot77main)
  DECLARE_CLASS(zoot77main, zoot77main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot77main_h__
