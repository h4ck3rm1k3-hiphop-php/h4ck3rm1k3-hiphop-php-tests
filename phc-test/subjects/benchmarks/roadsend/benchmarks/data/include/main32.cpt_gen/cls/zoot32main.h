
#ifndef __GENERATED_cls_zoot32main_h__
#define __GENERATED_cls_zoot32main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main32.cpt line 23 */
class c_zoot32main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot32main)
  END_CLASS_MAP(zoot32main)
  DECLARE_CLASS(zoot32main, zoot32main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot32main_h__
