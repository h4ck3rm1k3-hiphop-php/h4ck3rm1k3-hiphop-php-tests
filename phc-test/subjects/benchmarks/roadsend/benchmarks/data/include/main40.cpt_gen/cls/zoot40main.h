
#ifndef __GENERATED_cls_zoot40main_h__
#define __GENERATED_cls_zoot40main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main40.cpt line 23 */
class c_zoot40main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot40main)
  END_CLASS_MAP(zoot40main)
  DECLARE_CLASS(zoot40main, zoot40main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot40main_h__
