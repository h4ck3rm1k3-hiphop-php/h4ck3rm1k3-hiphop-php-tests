
#ifndef __GENERATED_cls_zoot41main_h__
#define __GENERATED_cls_zoot41main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main41.cpt line 23 */
class c_zoot41main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot41main)
  END_CLASS_MAP(zoot41main)
  DECLARE_CLASS(zoot41main, zoot41main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot41main_h__
