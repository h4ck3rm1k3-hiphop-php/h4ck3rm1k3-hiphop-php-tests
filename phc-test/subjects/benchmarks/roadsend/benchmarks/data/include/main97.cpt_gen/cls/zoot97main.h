
#ifndef __GENERATED_cls_zoot97main_h__
#define __GENERATED_cls_zoot97main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main97.cpt line 23 */
class c_zoot97main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot97main)
  END_CLASS_MAP(zoot97main)
  DECLARE_CLASS(zoot97main, zoot97main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot97main_h__
