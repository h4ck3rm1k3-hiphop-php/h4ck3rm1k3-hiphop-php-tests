
#ifndef __GENERATED_cls_sm_smtag_areat_h__
#define __GENERATED_cls_sm_smtag_areat_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc20.php line 23 */
class c_sm_smtag_areat : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_areat)
  END_CLASS_MAP(sm_smtag_areat)
  DECLARE_CLASS(sm_smtag_areat, SM_smTag_AREAt, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_areat_h__
