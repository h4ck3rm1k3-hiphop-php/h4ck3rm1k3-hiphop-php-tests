
#ifndef __GENERATED_cls_zoot19main_h__
#define __GENERATED_cls_zoot19main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main19.cpt line 23 */
class c_zoot19main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot19main)
  END_CLASS_MAP(zoot19main)
  DECLARE_CLASS(zoot19main, zoot19main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot19main_h__
