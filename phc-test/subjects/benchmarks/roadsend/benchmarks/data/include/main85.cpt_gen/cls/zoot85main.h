
#ifndef __GENERATED_cls_zoot85main_h__
#define __GENERATED_cls_zoot85main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main85.cpt line 23 */
class c_zoot85main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot85main)
  END_CLASS_MAP(zoot85main)
  DECLARE_CLASS(zoot85main, zoot85main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot85main_h__
