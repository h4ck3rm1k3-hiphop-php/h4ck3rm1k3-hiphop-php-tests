
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc7.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc7.php line 22 */
Variant c_tfunctionmenu::os_get(const char *s, int64 hash) {
  return c_sm_module::os_get(s, hash);
}
Variant &c_tfunctionmenu::os_lval(const char *s, int64 hash) {
  return c_sm_module::os_lval(s, hash);
}
void c_tfunctionmenu::o_get(ArrayElementVec &props) const {
  c_sm_module::o_get(props);
}
bool c_tfunctionmenu::o_exists(CStrRef s, int64 hash) const {
  return c_sm_module::o_exists(s, hash);
}
Variant c_tfunctionmenu::o_get(CStrRef s, int64 hash) {
  return c_sm_module::o_get(s, hash);
}
Variant c_tfunctionmenu::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_sm_module::o_set(s, hash, v, forInit);
}
Variant &c_tfunctionmenu::o_lval(CStrRef s, int64 hash) {
  return c_sm_module::o_lval(s, hash);
}
Variant c_tfunctionmenu::os_constant(const char *s) {
  return c_sm_module::os_constant(s);
}
IMPLEMENT_CLASS(tfunctionmenu)
ObjectData *c_tfunctionmenu::cloneImpl() {
  c_tfunctionmenu *obj = NEW(c_tfunctionmenu)();
  cloneSet(obj);
  return obj;
}
void c_tfunctionmenu::cloneSet(c_tfunctionmenu *clone) {
  c_sm_module::cloneSet(clone);
}
Variant c_tfunctionmenu::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_sm_module::o_invoke(s, params, hash, fatal);
}
Variant c_tfunctionmenu::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_sm_module::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_tfunctionmenu::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_sm_module::os_invoke(c, s, params, hash, fatal);
}
Variant cw_tfunctionmenu$os_get(const char *s) {
  return c_tfunctionmenu::os_get(s, -1);
}
Variant &cw_tfunctionmenu$os_lval(const char *s) {
  return c_tfunctionmenu::os_lval(s, -1);
}
Variant cw_tfunctionmenu$os_constant(const char *s) {
  return c_tfunctionmenu::os_constant(s);
}
Variant cw_tfunctionmenu$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_tfunctionmenu::os_invoke(c, s, params, -1, fatal);
}
void c_tfunctionmenu::init() {
  c_sm_module::init();
}
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc7.php line 27 */
void c_tfunctionmenu::t_moduleconfig() {
  INSTANCE_METHOD_INJECTION(tFunctionMenu, tFunctionMenu::moduleConfig);
  LINE(31,o_root_invoke_few_args("useModule", 0x6300E73655A9ADDBLL, 2, "mainMenu", "ngMenu"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc7.php line 38 */
void c_tfunctionmenu::t_modulethink() {
  INSTANCE_METHOD_INJECTION(tFunctionMenu, tFunctionMenu::moduleThink);
  Variant v_menu;
  Variant v_item;

  (v_menu = ref(LINE(42,o_root_invoke_few_args("getResource", 0x35DEED3493BD9098LL, 1, "mainMenu"))));
  (v_item = ref(LINE(47,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Global Config System (GCS)"))));
  LINE(48,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "gcs.php"));
  (v_item = ref(LINE(51,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "SM Objects"))));
  LINE(52,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "smObjects.php"));
  (v_item = ref(LINE(55,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Components"))));
  LINE(56,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "components.php"));
  (v_item = ref(LINE(59,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "InVars"))));
  LINE(60,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "inVars.php\?t1=test&t2=201"));
  (v_item = ref(LINE(63,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "SM Root"))));
  LINE(64,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "smRoot.php"));
  (v_item = ref(LINE(67,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Sessions"))));
  LINE(68,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "sessions.php"));
  (v_item = ref(LINE(71,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Members"))));
  LINE(72,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "members.php"));
  (v_item = ref(LINE(75,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Templates/CodePlates"))));
  LINE(76,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "templates.php"));
  (v_item = ref(LINE(79,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Modules"))));
  LINE(80,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "modules.php\?modVar2=5&modVar1=pokey"));
  (v_item = ref(LINE(83,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "SmartForms"))));
  LINE(84,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "smartForms.php"));
  (v_item = ref(LINE(87,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Contrib Modules"))));
  LINE(88,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "contrib.php"));
  (v_item = ref(LINE(91,v_menu.o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, "Debug"))));
  LINE(92,v_item.o_invoke_few_args("addLinkItem", 0x5E1F24E9B4536B42LL, 2, "Test Suite", "debug.php"));
  LINE(95,o_root_invoke_few_args("say", 0x52F616C0D2FEC9A0LL, 1, v_menu.o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 0)));
} /* function */
Object co_tfunctionmenu(CArrRef params, bool init /* = true */) {
  return Object(p_tfunctionmenu(NEW(c_tfunctionmenu)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$inc7_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc7.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$inc7_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("inc7.php");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
