
#ifndef __GENERATED_cls_zoot74main_h__
#define __GENERATED_cls_zoot74main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main74.cpt line 23 */
class c_zoot74main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot74main)
  END_CLASS_MAP(zoot74main)
  DECLARE_CLASS(zoot74main, zoot74main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot74main_h__
