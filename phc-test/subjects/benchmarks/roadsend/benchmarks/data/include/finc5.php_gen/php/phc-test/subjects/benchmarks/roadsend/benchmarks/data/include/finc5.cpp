
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc5.php line 23 */
Variant c_sm_smtag_areae::os_get(const char *s, int64 hash) {
  return c_sm_smtag::os_get(s, hash);
}
Variant &c_sm_smtag_areae::os_lval(const char *s, int64 hash) {
  return c_sm_smtag::os_lval(s, hash);
}
void c_sm_smtag_areae::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("areaName", m_areaName.isReferenced() ? ref(m_areaName) : m_areaName));
  props.push_back(NEW(ArrayElement)("itemList", m_itemList.isReferenced() ? ref(m_itemList) : m_itemList));
  c_sm_smtag::o_get(props);
}
bool c_sm_smtag_areae::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x7DDB95372031CCF8LL, areaName, 8);
      HASH_EXISTS_STRING(0x0B544F21590D2A74LL, itemList, 8);
      break;
    default:
      break;
  }
  return c_sm_smtag::o_exists(s, hash);
}
Variant c_sm_smtag_areae::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x7DDB95372031CCF8LL, m_areaName,
                         areaName, 8);
      HASH_RETURN_STRING(0x0B544F21590D2A74LL, m_itemList,
                         itemList, 8);
      break;
    default:
      break;
  }
  return c_sm_smtag::o_get(s, hash);
}
Variant c_sm_smtag_areae::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x7DDB95372031CCF8LL, m_areaName,
                      areaName, 8);
      HASH_SET_STRING(0x0B544F21590D2A74LL, m_itemList,
                      itemList, 8);
      break;
    default:
      break;
  }
  return c_sm_smtag::o_set(s, hash, v, forInit);
}
Variant &c_sm_smtag_areae::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x7DDB95372031CCF8LL, m_areaName,
                         areaName, 8);
      HASH_RETURN_STRING(0x0B544F21590D2A74LL, m_itemList,
                         itemList, 8);
      break;
    default:
      break;
  }
  return c_sm_smtag::o_lval(s, hash);
}
Variant c_sm_smtag_areae::os_constant(const char *s) {
  return c_sm_smtag::os_constant(s);
}
IMPLEMENT_CLASS(sm_smtag_areae)
ObjectData *c_sm_smtag_areae::cloneImpl() {
  c_sm_smtag_areae *obj = NEW(c_sm_smtag_areae)();
  cloneSet(obj);
  return obj;
}
void c_sm_smtag_areae::cloneSet(c_sm_smtag_areae *clone) {
  clone->m_areaName = m_areaName.isReferenced() ? ref(m_areaName) : m_areaName;
  clone->m_itemList = m_itemList.isReferenced() ? ref(m_itemList) : m_itemList;
  c_sm_smtag::cloneSet(clone);
}
Variant c_sm_smtag_areae::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_sm_smtag::o_invoke(s, params, hash, fatal);
}
Variant c_sm_smtag_areae::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_sm_smtag::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_smtag_areae::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_sm_smtag::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_smtag_areae$os_get(const char *s) {
  return c_sm_smtag_areae::os_get(s, -1);
}
Variant &cw_sm_smtag_areae$os_lval(const char *s) {
  return c_sm_smtag_areae::os_lval(s, -1);
}
Variant cw_sm_smtag_areae$os_constant(const char *s) {
  return c_sm_smtag_areae::os_constant(s);
}
Variant cw_sm_smtag_areae$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_smtag_areae::os_invoke(c, s, params, -1, fatal);
}
void c_sm_smtag_areae::init() {
  c_sm_smtag::init();
  m_areaName = "";
  m_itemList = ScalarArrays::sa_[0];
}
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc5.php line 41 */
void c_sm_smtag_areae::t_additem(Variant v_data) {
  INSTANCE_METHOD_INJECTION(SM_smTag_AREAe, SM_smTag_AREAe::addItem);
  m_itemList.append((ref(v_data)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc5.php line 49 */
void c_sm_smtag_areae::t_tagreset() {
  INSTANCE_METHOD_INJECTION(SM_smTag_AREAe, SM_smTag_AREAe::tagReset);
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_key = 0;
  Variant v_areaData;

  {
    LOOP_COUNTER(1);
    Variant map2 = m_itemList;
    for (ArrayIterPtr iter3 = map2.begin("sm_smtag_areae"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_areaData = iter3->second();
      v_key = iter3->first();
      {
        if ((equal(LINE(55,x_get_class(m_itemList.rvalAt(v_key))), "sm_layouttemplate")) || (LINE(56,x_is_subclass_of(m_itemList.rvalAt(v_key), "sm_layouttemplate")))) {
          LINE(59,m_itemList.rvalAt(v_key).o_invoke_few_args("_resetTemplate", 0x4339A21204CDFC31LL, 0));
        }
        else if (LINE(62,x_is_subclass_of(m_itemList.rvalAt(v_key), "sm_module"))) {
          LINE(65,m_itemList.rvalAt(v_key).o_invoke_few_args("_resetModule", 0x5242FCB8D8CAEC20LL, 0));
        }
        m_itemList.set(v_key, (null));
        m_itemList.weakRemove(v_key);
      }
    }
  }
  t___unset("itemList");
  (m_itemList = ScalarArrays::sa_[0]);
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc5.php line 84 */
void c_sm_smtag_areae::t_tagconfig() {
  INSTANCE_METHOD_INJECTION(SM_smTag_AREAe, SM_smTag_AREAe::tagConfig);
  (m_areaName = o_get("attributes", 0x3E5975BA0FC37E03LL).rvalAt("NAME", 0x5655B4FF77E35232LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc5.php line 96 */
void c_sm_smtag_areae::t_tagthink() {
  INSTANCE_METHOD_INJECTION(SM_smTag_AREAe, SM_smTag_AREAe::tagThink);
  Variant v_areaOutput;
  Primitive v_key = 0;
  Variant v_areaData;

  (v_areaOutput = "");
  if (equal(LINE(101,x_sizeof(m_itemList)), 0LL)) {
    LINE(102,o_root_invoke_few_args("debugLog", 0x603BE0E0675207ABLL, 1, concat3("tagThink: no items in area [", toString(m_areaName), "], leaving blank")));
    return;
  }
  {
    LOOP_COUNTER(4);
    Variant map5 = m_itemList;
    for (ArrayIterPtr iter6 = map5.begin("sm_smtag_areae"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_areaData = iter6->second();
      v_key = iter6->first();
      {
        if ((equal(LINE(110,x_get_class(m_itemList.rvalAt(v_key))), "sm_layouttemplate")) || (LINE(111,x_is_subclass_of(m_itemList.rvalAt(v_key), "sm_layouttemplate")))) {
          concat_assign(v_areaOutput, toString(LINE(116,m_itemList.rvalAt(v_key).o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 0))));
          LINE(119,o_root_invoke_few_args("sayJS", 0x53D21EB2ECAC1B59LL, 1, m_itemList.rvalAt(v_key).o_lval("jsOutput", 0x7A9066AD10A05267LL)));
        }
        else if (LINE(122,x_is_subclass_of(m_itemList.rvalAt(v_key), "sm_module"))) {
          concat_assign(v_areaOutput, toString(LINE(127,m_itemList.rvalAt(v_key).o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 0))));
          LINE(130,o_root_invoke_few_args("sayJS", 0x53D21EB2ECAC1B59LL, 1, m_itemList.rvalAt(v_key).o_invoke_few_args("getJS", 0x51662C79E29D4426LL, 0)));
        }
        else if (LINE(133,x_is_string(m_itemList.rvalAt(v_key)))) {
          concat_assign(v_areaOutput, toString(m_itemList.rvalAt(v_key)));
        }
        else {
          LINE(140,o_root_invoke_few_args("debugLog", 0x603BE0E0675207ABLL, 1, concat3("unknown object added to template area [", toString(m_areaName), "] (was not decendent of SM_module or SM_layoutTemplate): ignoring")));
          LINE(141,o_root_invoke_few_args("debugLog", 0x603BE0E0675207ABLL, 1, concat("object appeared to be: ", toString(x_get_class(m_itemList.rvalAt(v_key))))));
        }
      }
    }
  }
  LINE(147,o_root_invoke_few_args("say", 0x52F616C0D2FEC9A0LL, 1, v_areaOutput));
} /* function */
Object co_sm_smtag_areae(CArrRef params, bool init /* = true */) {
  return Object(p_sm_smtag_areae(NEW(c_sm_smtag_areae)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$finc5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$finc5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("finc5.php");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
