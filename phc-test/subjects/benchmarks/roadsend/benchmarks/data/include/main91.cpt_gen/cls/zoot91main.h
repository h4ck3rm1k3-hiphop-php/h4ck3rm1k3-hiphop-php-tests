
#ifndef __GENERATED_cls_zoot91main_h__
#define __GENERATED_cls_zoot91main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main91.cpt line 23 */
class c_zoot91main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot91main)
  END_CLASS_MAP(zoot91main)
  DECLARE_CLASS(zoot91main, zoot91main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot91main_h__
