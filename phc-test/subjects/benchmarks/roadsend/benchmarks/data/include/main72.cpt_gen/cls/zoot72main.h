
#ifndef __GENERATED_cls_zoot72main_h__
#define __GENERATED_cls_zoot72main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main72.cpt line 23 */
class c_zoot72main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot72main)
  END_CLASS_MAP(zoot72main)
  DECLARE_CLASS(zoot72main, zoot72main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot72main_h__
