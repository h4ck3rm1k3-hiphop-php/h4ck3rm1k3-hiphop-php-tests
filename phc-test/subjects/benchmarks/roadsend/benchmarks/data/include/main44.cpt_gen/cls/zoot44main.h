
#ifndef __GENERATED_cls_zoot44main_h__
#define __GENERATED_cls_zoot44main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main44.cpt line 23 */
class c_zoot44main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot44main)
  END_CLASS_MAP(zoot44main)
  DECLARE_CLASS(zoot44main, zoot44main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot44main_h__
