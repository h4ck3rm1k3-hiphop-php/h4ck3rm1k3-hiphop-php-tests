
#ifndef __GENERATED_cls_zoot62main_h__
#define __GENERATED_cls_zoot62main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main62.cpt line 23 */
class c_zoot62main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot62main)
  END_CLASS_MAP(zoot62main)
  DECLARE_CLASS(zoot62main, zoot62main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot62main_h__
