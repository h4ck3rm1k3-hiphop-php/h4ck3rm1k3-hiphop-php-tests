
#ifndef __GENERATED_cls_sm_smtag_areaq_h__
#define __GENERATED_cls_sm_smtag_areaq_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc17.php line 23 */
class c_sm_smtag_areaq : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_areaq)
  END_CLASS_MAP(sm_smtag_areaq)
  DECLARE_CLASS(sm_smtag_areaq, SM_smTag_AREAq, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_areaq_h__
