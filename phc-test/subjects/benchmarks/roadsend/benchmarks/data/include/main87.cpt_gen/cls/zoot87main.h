
#ifndef __GENERATED_cls_zoot87main_h__
#define __GENERATED_cls_zoot87main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main87.cpt line 23 */
class c_zoot87main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot87main)
  END_CLASS_MAP(zoot87main)
  DECLARE_CLASS(zoot87main, zoot87main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot87main_h__
