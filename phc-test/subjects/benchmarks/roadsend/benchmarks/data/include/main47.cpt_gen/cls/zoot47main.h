
#ifndef __GENERATED_cls_zoot47main_h__
#define __GENERATED_cls_zoot47main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main47.cpt line 23 */
class c_zoot47main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot47main)
  END_CLASS_MAP(zoot47main)
  DECLARE_CLASS(zoot47main, zoot47main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot47main_h__
