
#ifndef __GENERATED_cls_zoot27main_h__
#define __GENERATED_cls_zoot27main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main27.cpt line 23 */
class c_zoot27main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot27main)
  END_CLASS_MAP(zoot27main)
  DECLARE_CLASS(zoot27main, zoot27main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot27main_h__
