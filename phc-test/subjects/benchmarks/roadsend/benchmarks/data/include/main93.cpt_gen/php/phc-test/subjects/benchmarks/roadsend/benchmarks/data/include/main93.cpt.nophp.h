
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_main93_cpt_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_main93_cpt_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main93.cpt.nophp.fw.h>

// Declarations
#include <cls/zoot93main.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$include$main93_cpt(bool incOnce = false, LVariableTable* variables = NULL);
Object co_zoot93main(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_roadsend_benchmarks_data_include_main93_cpt_nophp_h__
