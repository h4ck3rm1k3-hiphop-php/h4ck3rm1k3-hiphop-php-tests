
#ifndef __GENERATED_cls_zoot93main_h__
#define __GENERATED_cls_zoot93main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main93.cpt line 23 */
class c_zoot93main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot93main)
  END_CLASS_MAP(zoot93main)
  DECLARE_CLASS(zoot93main, zoot93main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot93main_h__
