
#ifndef __GENERATED_cls_zoot37main_h__
#define __GENERATED_cls_zoot37main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main37.cpt line 23 */
class c_zoot37main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot37main)
  END_CLASS_MAP(zoot37main)
  DECLARE_CLASS(zoot37main, zoot37main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot37main_h__
