
#ifndef __GENERATED_cls_zoot79main_h__
#define __GENERATED_cls_zoot79main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main79.cpt line 23 */
class c_zoot79main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot79main)
  END_CLASS_MAP(zoot79main)
  DECLARE_CLASS(zoot79main, zoot79main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot79main_h__
