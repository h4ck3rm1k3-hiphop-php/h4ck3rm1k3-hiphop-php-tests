
#ifndef __GENERATED_cls_zoot52main_h__
#define __GENERATED_cls_zoot52main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main52.cpt line 23 */
class c_zoot52main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot52main)
  END_CLASS_MAP(zoot52main)
  DECLARE_CLASS(zoot52main, zoot52main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot52main_h__
