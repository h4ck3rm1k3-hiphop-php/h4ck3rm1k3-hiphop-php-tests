
#ifndef __GENERATED_cls_zoot90main_h__
#define __GENERATED_cls_zoot90main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main90.cpt line 23 */
class c_zoot90main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot90main)
  END_CLASS_MAP(zoot90main)
  DECLARE_CLASS(zoot90main, zoot90main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot90main_h__
