
#ifndef __GENERATED_cls_zoot75main_h__
#define __GENERATED_cls_zoot75main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main75.cpt line 23 */
class c_zoot75main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot75main)
  END_CLASS_MAP(zoot75main)
  DECLARE_CLASS(zoot75main, zoot75main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot75main_h__
