
#ifndef __GENERATED_cls_zoot86main_h__
#define __GENERATED_cls_zoot86main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main86.cpt line 23 */
class c_zoot86main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot86main)
  END_CLASS_MAP(zoot86main)
  DECLARE_CLASS(zoot86main, zoot86main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot86main_h__
