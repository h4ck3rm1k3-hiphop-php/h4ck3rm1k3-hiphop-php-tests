
#ifndef __GENERATED_cls_zoot98main_h__
#define __GENERATED_cls_zoot98main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main98.cpt line 23 */
class c_zoot98main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot98main)
  END_CLASS_MAP(zoot98main)
  DECLARE_CLASS(zoot98main, zoot98main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot98main_h__
