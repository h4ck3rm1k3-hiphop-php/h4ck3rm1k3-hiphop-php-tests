
#ifndef __GENERATED_cls_zoot83main_h__
#define __GENERATED_cls_zoot83main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main83.cpt line 23 */
class c_zoot83main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot83main)
  END_CLASS_MAP(zoot83main)
  DECLARE_CLASS(zoot83main, zoot83main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot83main_h__
