
#ifndef __GENERATED_cls_zoot17main_h__
#define __GENERATED_cls_zoot17main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main17.cpt line 23 */
class c_zoot17main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot17main)
  END_CLASS_MAP(zoot17main)
  DECLARE_CLASS(zoot17main, zoot17main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot17main_h__
