
#ifndef __GENERATED_cls_zoot99main_h__
#define __GENERATED_cls_zoot99main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main99.cpt line 23 */
class c_zoot99main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot99main)
  END_CLASS_MAP(zoot99main)
  DECLARE_CLASS(zoot99main, zoot99main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot99main_h__
