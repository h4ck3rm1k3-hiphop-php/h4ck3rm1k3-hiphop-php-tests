
#ifndef __GENERATED_cls_zoot07main_h__
#define __GENERATED_cls_zoot07main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main07.cpt line 23 */
class c_zoot07main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot07main)
  END_CLASS_MAP(zoot07main)
  DECLARE_CLASS(zoot07main, zoot07main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot07main_h__
