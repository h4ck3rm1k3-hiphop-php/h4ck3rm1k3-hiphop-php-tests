
#ifndef __GENERATED_cls_zfunctionmenu_h__
#define __GENERATED_cls_zfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/inc13.php line 22 */
class c_zfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(zfunctionmenu)
  END_CLASS_MAP(zfunctionmenu)
  DECLARE_CLASS(zfunctionmenu, zFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zfunctionmenu_h__
