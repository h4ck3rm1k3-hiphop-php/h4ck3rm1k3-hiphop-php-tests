
#ifndef __GENERATED_cls_sm_smtag_areai_h__
#define __GENERATED_cls_sm_smtag_areai_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/finc9.php line 23 */
class c_sm_smtag_areai : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_areai)
  END_CLASS_MAP(sm_smtag_areai)
  DECLARE_CLASS(sm_smtag_areai, SM_smTag_AREAi, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_areai_h__
