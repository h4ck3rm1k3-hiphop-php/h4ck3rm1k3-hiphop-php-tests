
#ifndef __GENERATED_cls_zoot13main_h__
#define __GENERATED_cls_zoot13main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main13.cpt line 23 */
class c_zoot13main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot13main)
  END_CLASS_MAP(zoot13main)
  DECLARE_CLASS(zoot13main, zoot13main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot13main_h__
