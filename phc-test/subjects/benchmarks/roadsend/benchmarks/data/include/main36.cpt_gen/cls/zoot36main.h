
#ifndef __GENERATED_cls_zoot36main_h__
#define __GENERATED_cls_zoot36main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main36.cpt line 23 */
class c_zoot36main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot36main)
  END_CLASS_MAP(zoot36main)
  DECLARE_CLASS(zoot36main, zoot36main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot36main_h__
