
#ifndef __GENERATED_cls_zoot06main_h__
#define __GENERATED_cls_zoot06main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main06.cpt line 23 */
class c_zoot06main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot06main)
  END_CLASS_MAP(zoot06main)
  DECLARE_CLASS(zoot06main, zoot06main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot06main_h__
