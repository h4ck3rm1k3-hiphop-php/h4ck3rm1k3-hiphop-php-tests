
#ifndef __GENERATED_cls_zoot76main_h__
#define __GENERATED_cls_zoot76main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main76.cpt line 23 */
class c_zoot76main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot76main)
  END_CLASS_MAP(zoot76main)
  DECLARE_CLASS(zoot76main, zoot76main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot76main_h__
