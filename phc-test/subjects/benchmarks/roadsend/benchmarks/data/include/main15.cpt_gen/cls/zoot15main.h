
#ifndef __GENERATED_cls_zoot15main_h__
#define __GENERATED_cls_zoot15main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main15.cpt line 23 */
class c_zoot15main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot15main)
  END_CLASS_MAP(zoot15main)
  DECLARE_CLASS(zoot15main, zoot15main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot15main_h__
