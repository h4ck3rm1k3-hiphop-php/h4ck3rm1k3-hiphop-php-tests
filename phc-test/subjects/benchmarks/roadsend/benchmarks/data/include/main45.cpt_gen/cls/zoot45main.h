
#ifndef __GENERATED_cls_zoot45main_h__
#define __GENERATED_cls_zoot45main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main45.cpt line 23 */
class c_zoot45main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot45main)
  END_CLASS_MAP(zoot45main)
  DECLARE_CLASS(zoot45main, zoot45main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot45main_h__
