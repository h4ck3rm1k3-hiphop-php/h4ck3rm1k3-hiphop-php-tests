
#ifndef __GENERATED_cls_zoot65main_h__
#define __GENERATED_cls_zoot65main_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/roadsend/benchmarks/data/include/main65.cpt line 23 */
class c_zoot65main : virtual public ObjectData {
  BEGIN_CLASS_MAP(zoot65main)
  END_CLASS_MAP(zoot65main)
  DECLARE_CLASS(zoot65main, zoot65main, sm_codeplate)
  void init();
  public: void t_codeplateconfig();
  public: void t_codeplatethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zoot65main_h__
