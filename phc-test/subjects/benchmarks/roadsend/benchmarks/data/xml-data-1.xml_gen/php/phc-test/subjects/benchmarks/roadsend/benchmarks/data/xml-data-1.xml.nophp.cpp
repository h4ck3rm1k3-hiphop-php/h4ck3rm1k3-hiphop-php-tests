
#include <php/phc-test/subjects/benchmarks/roadsend/benchmarks/data/xml-data-1.xml.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$xml_data_1_xml(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/roadsend/benchmarks/data/xml-data-1.xml);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$roadsend$benchmarks$data$xml_data_1_xml;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<S");
  echo("M_XML>\n");
  echo("<S");
  echo("MCONFIG VERSION=\"1.2\">\n\n    ");
  echo("<S");
  echo("ITE siteid=\"GLOBAL\">                                       \n        ");
  echo("<S");
  echo("ECTION name=\"dirs\">\n            <VAR name=\"smRoot\"      value = \"/usr/local/lib/php/siteManager/\" />\n            <VAR name=\"base\"        value = \"{$smRoot}\"/>            \n            <VAR name=\"redist\"      value = \"{$base}redist/\" />\n            <VAR name=\"pear\"        value = \"{$redist}pear/\" />\n            <VAR name=\"config\"      value = \"{$base}config/\"/>\n            <VAR name=\"libs\"        value = ");
  echo("\"{$base}lib/\"/>\n            <VAR name=\"libs\"        value = \"{$base}contrib/lib/\" />\n            <VAR name=\"modules\"     value = \"{$base}contrib/modules/\"/>            \n            <VAR name=\"templates\"   value = \"{$base}contrib/templates/\"/>            \n            <VAR name=\"smartForms\"  value = \"{$base}contrib/smartForms/\"/>\n            <VAR name=\"sfEntities\"  value = \"{$base}lib/sfInputEntities/\"/>");
  echo("\n            <VAR name=\"sfFilters\"   value = \"{$base}lib/sfFilters/\"/>            \n            <VAR name=\"smTags\"      value = \"{$base}lib/smTags/\" />\n            <VAR name=\"sContainers\" value = \"{$base}lib/sessionContainers/\" />\n            <VAR name=\"cache\"       value = \"{$base}cache/\"/>\n            <VAR name=\"memberSystems\" value = \"{$base}lib/memberSystems/\" />            \n            <VAR name=\"co");
  echo("nfigReaders\" value = \"{$base}lib/configReaders/\" />                        \n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"localization\">\n            <VAR name=\"defaultLocale\"       value = \"en_US\" desc = \"default locale for site\" />\n            <VAR name=\"useClientLocale\"     value = \"true\"  desc = \"when true, SiteManager will default to the locale requested by the clients browser, if available\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"db\" id=\"default\">\n            <VAR name=\"active\"              value = \"false\" desc = \"whether this connection is active or not\" />\n            <VAR name=\"defaultConnection\"   value = \"true\"  desc = \"set to true to have this connection be the default connection. will be $dbH\" />\n            <VAR name=\"dbType\"              value = \"mysql\" desc = \"the database type. must be valid PEAR datab");
  echo("ase type.\" />\n            <VAR name=\"persistent\"          value = \"true\"  desc = \"bool to determine whether this is a persistent connection or not\" />\n            <VAR name=\"required\"            value = \"true\"  desc = \"if this is true, a fatal error will be thrown if the connection fails. if false, it will ignore a bad connection.\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"compatibility\">\n            <VAR name=\"2.2.x\"               value = \"true\"  desc = \"set to true if you want more compatibility with SiteManager 2.2.x\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"templates\">\n            <VAR name=\"allowQuickTags\"      value = \"true\"  desc = \"if true, the template engine will use the Quick Tag system\" />\n            <VAR name=\"quickTagIdentifier\"  value = \"@@\"    desc = \"Quick Tags will be wrapped in this identifier\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"flags\">\n            <VAR name=\"showLoadTime\"            value=\"true\"    desc=\"when true, all pages will have an HTML comment tacked onto them with the page's load time\" />\n            <VAR name=\"propagateInVarDefault\"   value=\"false\"   desc=\"if this is true, inVars will be propagated by default. when false, you must specify which to propagate\" />\n            <VAR name=\"sendNoCacheHeaders");
  echo("\"      value=\"false\"   desc=\"if this is true, a header will be sent to stop browsers from caching pages\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"cache\">\n            <VAR name=\"useCache\"            value=\"false\"   desc=\"master switch for using the caching system\" />            \n            <VAR name=\"cacheSMConfig\"       value=\"true\"    desc=\"(deprecate) when true, SiteManager will attempt to cache SMCONFIG files to the cache directory\" />            \n            <VAR name=\"cacheSMLanguage\"     value=\"true\"    desc=\"(deprecate) wh");
  echo("en true, SiteManager will attempt to cache SMLANGUAGE files to the cache directory\" />            \n            <VAR name=\"cacheSMObject\"       value=\"true\"    desc=\"(deprecate) when true, SiteManager will attempt to cache SMOBJECT files to the cache directory\" />\n            <VAR name=\"cacheSmartForm\"      value=\"true\"    desc=\"(deprecate) when true, SiteManager will attempt to cache SMARTFORM files");
  echo(" to the cache directory\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"cacheConfig\" id=\"smconfig\">\n            <VAR name=\"useCache\"            value=\"false\"  desc=\"cache this file type\" />\n            <VAR name=\"autoExpire\"          value=\"true\"  desc=\"if true, the file will auto-recache if the file change time is later than the cache file change time\" />\n            <VAR name=\"cacheTTL\"            value=\"86400\" desc=\"maximum length of time (in seconds) bef");
  echo("ore a file should be recached\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"debug\">   \n            <VAR name=\"debugLevel\"          value=\"0\"       desc=\"debugLog messages of this verbosity and lower will be shown in the debugLog - all else ommitted.\" />\n            <VAR name=\"errorHandler\"        value=\"SM_errorHandler\" desc=\"default SiteManager error handler (class name). To customize for a site, extend SM_errorHandler and place an entry in your localConfig.xs");
  echo("m\" />\n            <VAR name=\"showNotices\"         value=\"true\"    desc=\"when true, SiteManager will display all PHP notices and warnings in the debug output\" />\n            <VAR name=\"develState\"          value=\"false\"   desc=\"whether sitemanager is in development mode or not\"/>\n            <VAR name=\"debugOnVar\"          value=\"false\"   desc=\"if true, the debugLog will be shown above the output of a");
  echo(" page if variable SM_debug is passed as 1 to the script\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"sessions\">                \n            <VAR name=\"containerType\"       value=\"none\"            desc=\"which session container to use. currently 'database' or 'file'\" />\n            <VAR name=\"sessionCookie\"       value=\"\"                desc=\"when non blank, create a cookie on the remote users machine that stores their session id\" />            \n            <VAR name=\"useMemberSystem\"    ");
  echo(" value=\"false\"           desc=\"main on/off switch for member system. if on, make sure 'members' section is setup\" />\n            <VAR name=\"forceSessionID\"      value=\"true\"            desc=\"when set to true, the session ID variable will always be appended to links, even if a cookie is in use. true is safest bet, as users can turn cookies off.\" />\n            <VAR name=\"autoLogin\"           value=\"t");
  echo("rue\"            desc=\"when true, if a session cookie was found, try to restore their session\" />            \n            <VAR name=\"badSessionPage\"      value=\"home/home.php\"   desc=\"page to send to when a session is invalid\" />\n            <VAR name=\"scriptWithVars\"      value=\"\\.php\\\?\"         desc=\"regex to determine if linking to a script. if you use a different extension other than .php, change");
  echo(" this\" />\n            <VAR name=\"sessionIDName\"       value=\"sID\"             desc=\"variable name to use to store sessions ID\" /> \n            <VAR name=\"cookieTTL\"           value=\"2678400\"         desc=\"session cookie expire time. 2678400=1month. set to 0 to expire after browser closes.\" />\n            <VAR name=\"defaultScript\"       value=\"index.php\"       desc=\"the default script that loads when ");
  echo("no script is specified\" />            \n            <VAR name=\"cookiePath\"          value=\"/\"               desc=\"the path setcookie() will use for the session cookie\" />\n            <VAR name=\"forceAntiCacheVar\"   value=\"\"                desc=\"when set to a value, SiteManager sessions will add this variable with a random value to the URL in an attempt to thwarte caching systems\" />\n            <VAR n");
  echo("ame=\"absoluteLinkBase\"    value=\"\"                desc=\"when set, it will be prepended to all session generated links. use for making links absolute.\" />\n            <VAR name=\"trackLastPageAccess\"    value=\"false\"     desc=\"When set, we track the last page access time was for each user. if inactiveTTL is greater then 0, it will log the person (see 'inactiveTTL' in the members section) \" />\n       ");
  echo(" </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"sessionContainer\" id=\"database\">\n            <VAR name=\"serialize\"           value=\"true\"            desc=\"whether to serialize all persistent variables or not. must be true to keep arrays and objects perisistent\" />            \n            <VAR name=\"dataBaseID\"          value=\"default\"         desc=\"which database connection (from db section) should SiteManager use for sessions\" />\n  ");
  echo("          <VAR name=\"sessionTable\"        value=\"sessions\"        desc=\"sessions table name\" />            \n            <VAR name=\"hashFunc\"            value=\"crc32\"           desc=\"crc32 or md5 for computing hash of session key. crc32 is faster, md5 will produce a more unique key\" />\n        </SECTION>        \n        ");
  echo("<S");
  echo("ECTION name=\"sessionContainer\" id=\"file\">\n            <VAR name=\"savePath\"           value=\"/tmp/\"            desc=\"path to save session files. must be writable by web server!\" />\n        </SECTION>                \n        ");
  echo("<S");
  echo("ECTION name=\"members\">\n            <VAR name=\"memberSystemType\"     value=\"default\"        desc=\"which member system handler to use\" />\n            <VAR name=\"usePersistentMembers\" value=\"true\"           desc=\"when true, member data will be keep as a persistent variable instead of loaded in from the container each time\" />\n            <VAR name=\"loginTTL\"             value=\"2678400\"        desc=\"how ");
  echo("long is a login session valid for (2678400)\" />\n            <VAR name=\"inactiveTTL\"        value=\"1800\"                desc=\"Number of seconds of inactivity before session times out. 1800 default (30 mins) trackLastPageAccess in sessions must bet 'true' for this to work\" />\n            <VAR name=\"maintainTables\"       value=\"false\"          desc=\"if true, SiteManager will delete old sessions. perfor");
  echo("mance hit, so defaults to false. better to run a cron'd script.\" />\n        </SECTION>\n        ");
  echo("<S");
  echo("ECTION name=\"memberSystem\" id=\"default\">\n            <VAR name=\"dataBaseID\"          value=\"default\"          desc=\"which database connection (from db section) should SiteManager use for sessions\" />        \n            <VAR name=\"memberTable\"         value=\"members\"          desc=\"member table name\" />            \n            <VAR name=\"memberSessionsTable\" value=\"memberSessions\"   desc=\"logged in m");
  echo("ember session table\" />\n            <VAR name=\"userNameField\"       value=\"userName\"         desc=\"the username field\" />            \n            <VAR name=\"passWordField\"       value=\"passWord\"         desc=\"the password field\" />\n            <VAR name=\"uIDField\"            value=\"uID\"              desc=\"the unique user id field\" />            \n            <VAR name=\"sIDField\"            value=\"sID\" ");
  echo("             desc=\"the unique session id field\" />\n            <VAR name=\"dcField\"             value=\"dateCreated\"      desc=\"DATETIME field of session creation\" />            \n        </SECTION>\n    </SITE>\n        \n</SMCONFIG>\n</SM_XML>\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
