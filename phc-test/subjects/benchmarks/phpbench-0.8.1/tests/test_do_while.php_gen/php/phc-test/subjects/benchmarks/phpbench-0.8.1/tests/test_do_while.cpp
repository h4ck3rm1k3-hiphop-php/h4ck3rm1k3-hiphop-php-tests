
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while.php line 16 */
bool f_test_do_while_enabled() {
  FUNCTION_INJECTION(test_do_while_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while.php line 3 */
Variant f_test_do_while(CVarRef v_base) {
  FUNCTION_INJECTION(test_do_while);
  int64 v_t = 0;
  int64 v_d = 0;

  (v_t = toInt64(LINE(4,x_round(divide(v_base, 10LL)))));
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_do_while").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_d = 1000LL);
        {
          LOOP_COUNTER(2);
          do {
            LOOP_COUNTER_CHECK(2);
            {
              v_d--;
            }
          } while (!same(v_d, 0LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(13,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_do_while").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
