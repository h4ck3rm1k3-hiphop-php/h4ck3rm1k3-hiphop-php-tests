
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_local_boolean_assign_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_local_boolean_assign_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_boolean_assign.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_test_local_boolean_assign(CVarRef v_base);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_test_local_boolean_assign_enabled();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_local_boolean_assign_h__
