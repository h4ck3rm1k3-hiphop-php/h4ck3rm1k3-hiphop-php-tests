
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_boolean_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_boolean_assign.php line 3 */
Variant f_test_local_boolean_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_local_boolean_assign);
  Variant v_t;
  bool v_a = false;
  bool v_b = false;
  bool v_c = false;
  bool v_d = false;
  bool v_e = false;
  bool v_f = false;
  bool v_g = false;
  bool v_h = false;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_local_boolean_assign").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = true);
        (v_b = false);
        (v_c = true);
        (v_d = false);
        (v_e = true);
        (v_f = false);
        (v_g = true);
        (v_h = false);
        ;
        ;
        ;
        ;
        ;
        ;
        ;
        ;
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(24,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_local_boolean_assign").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_boolean_assign.php line 27 */
bool f_test_local_boolean_assign_enabled() {
  FUNCTION_INJECTION(test_local_boolean_assign_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_boolean_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
