
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_unordered_functions_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_unordered_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_tuf_1(CVarRef v_a, CVarRef v_b);
Variant f_tuf_2(CVarRef v_a, CVarRef v_b);
Variant f_tuf_3(CVarRef v_a, CVarRef v_b);
Variant f_tuf_4(CVarRef v_a, CVarRef v_b);
Variant f_tuf_5(CVarRef v_a, CVarRef v_b);
Variant f_test_unordered_functions(CVarRef v_base);
bool f_test_unordered_functions_enabled();
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_unordered_functions_h__
