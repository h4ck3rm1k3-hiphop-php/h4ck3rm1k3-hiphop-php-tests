
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_bitwise.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_bitwise.php line 23 */
bool f_test_bitwise_enabled() {
  FUNCTION_INJECTION(test_bitwise_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_bitwise.php line 3 */
Variant f_test_bitwise(CVarRef v_base) {
  FUNCTION_INJECTION(test_bitwise);
  Variant v_t;
  Variant v_a;
  int64 v_b = 0;
  int64 v_c = 0;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_bitwise").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = 2147483647LL);
        (v_a = bitwise_and(v_a, 0.0));
        (v_a = bitwise_or(v_a, 117901063LL));
        (v_a = bitwise_xor(v_a, 0.0));
        (v_a = ~v_a);
        (v_b = toInt64(toInt64(v_a)) << 11LL);
        (v_c = toInt64(toInt64(v_a)) >> 15LL);
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(16,concat3(toString(v_a), toString(v_b), toString(v_c))), "1588444911184493465648475") && !same(LINE(17,concat3(toString(v_a), toString(v_b), toString(v_c))), "-2706522385-5542957844480-82597")) {
    LINE(18,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_bitwise").create()), 0x00000000395D85E8LL));
  }
  return LINE(20,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_bitwise").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_bitwise.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
