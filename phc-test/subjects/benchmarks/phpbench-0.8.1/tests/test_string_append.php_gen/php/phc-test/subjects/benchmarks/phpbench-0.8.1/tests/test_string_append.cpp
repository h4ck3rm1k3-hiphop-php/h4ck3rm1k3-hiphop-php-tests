
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_string_append.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_string_append.php line 3 */
Variant f_test_string_append(CVarRef v_base) {
  FUNCTION_INJECTION(test_string_append);
  Variant v_t;
  String v_a;
  String v_b;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_string_append").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = "abc");
        (v_b = "def");
        concat_assign(v_a, v_b);
        concat_assign(v_b, LINE(10,concat4(v_a, v_b, v_a, v_b)));
        (v_a = concat(v_a, LINE(11,concat6(" ", v_b, " ", v_b, " ", v_a))));
        concat_assign(v_a, "xyz");
        concat_assign(v_b, concat(v_a + v_b, "hij"));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(16,x_md5(v_a + v_b)), "b7988e4fb634f7e856fb19d7005d9dfa")) {
    LINE(17,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_string_append").create()), 0x00000000395D85E8LL));
  }
  return LINE(19,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_string_append").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_string_append.php line 22 */
bool f_test_string_append_enabled() {
  FUNCTION_INJECTION(test_string_append_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_string_append.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
