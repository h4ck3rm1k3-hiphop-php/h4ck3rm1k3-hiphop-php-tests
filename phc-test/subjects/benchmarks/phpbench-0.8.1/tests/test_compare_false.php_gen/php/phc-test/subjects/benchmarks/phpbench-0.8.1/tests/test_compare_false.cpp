
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_false.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_false.php line 3 */
Variant f_test_compare_false(CVarRef v_base) {
  FUNCTION_INJECTION(test_compare_false);
  Array v_a;
  Variant v_t;

  (v_a = ScalarArrays::sa_[0]);
  (v_t = v_base);
  LINE(7,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_compare_false").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        if (same(v_a, false)) {
          LINE(10,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_compare_false").create()), 0x00000000395D85E8LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(14,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_compare_false").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_false.php line 17 */
bool f_test_compare_false_enabled() {
  FUNCTION_INJECTION(test_compare_false_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_false.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
