
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_string_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_string_assign.php line 34 */
bool f_test_local_string_assign_enabled() {
  FUNCTION_INJECTION(test_local_string_assign_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_string_assign.php line 3 */
Variant f_test_local_string_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_local_string_assign);
  Variant v_t;
  String v_a;
  String v_b;
  String v_c;
  String v_d;
  String v_e;
  String v_f;
  String v_g;
  String v_h;
  String v_i;
  String v_j;
  String v_k;
  String v_l;
  String v_m;
  String v_n;
  String v_o;
  String v_p;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_local_string_assign").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = "abcdefghijklmnop");
        (v_b = v_a);
        (v_c = concat(v_a, v_b));
        (v_d = concat(v_c, v_a));
        (v_e = LINE(12,concat4(v_a, v_c, v_d, v_b)));
        (v_f = concat(v_a, v_c));
        (v_g = concat(v_b, v_d));
        (v_h = concat(v_a, LINE(15,concat6(v_b, v_c, v_d, v_e, v_f, v_g))));
        (v_i = v_a);
        (v_j = v_b);
        (v_k = v_c);
        (v_l = v_d);
        (v_m = v_e);
        (v_n = v_f);
        (v_o = v_g);
        (v_p = v_h);
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(27,x_md5(concat_rev(concat6(v_k, v_l, v_m, v_n, v_o, v_p), concat_rev(concat6(v_e, v_f, v_g, v_h, v_i, v_j), concat4(v_a, v_b, v_c, v_d))))), "12d5479a566db15ec0028b9cf242d150")) {
    LINE(29,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_local_string_assign").create()), 0x00000000395D85E8LL));
  }
  return LINE(31,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_local_string_assign").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_string_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
