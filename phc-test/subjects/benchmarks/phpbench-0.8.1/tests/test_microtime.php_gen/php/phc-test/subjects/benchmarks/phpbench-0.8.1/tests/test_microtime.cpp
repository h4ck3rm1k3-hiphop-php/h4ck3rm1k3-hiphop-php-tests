
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_microtime.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_microtime.php line 3 */
Variant f_test_microtime(CVarRef v_base) {
  FUNCTION_INJECTION(test_microtime);
  Variant v_t;
  Variant v_a;
  Variant v_b;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_microtime").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = LINE(7,x_microtime()));
        (v_b = LINE(8,x_microtime()));
        if (less(v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) {
          LINE(10,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_microtime").create()), 0x00000000395D85E8LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(14,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_microtime").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_microtime.php line 17 */
bool f_test_microtime_enabled() {
  FUNCTION_INJECTION(test_microtime_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_microtime.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
