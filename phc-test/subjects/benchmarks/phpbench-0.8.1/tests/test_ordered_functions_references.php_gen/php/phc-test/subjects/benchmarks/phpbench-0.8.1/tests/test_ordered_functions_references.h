
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_ordered_functions_references_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_ordered_functions_references_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_test_ordered_functions_references(CVarRef v_base);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php(bool incOnce = false, LVariableTable* variables = NULL);
PlusOperand f_tofr_1(Variant v_a, Variant v_b);
PlusOperand f_tofr_2(Variant v_a, Variant v_b);
PlusOperand f_tofr_3(Variant v_a, Variant v_b);
PlusOperand f_tofr_4(Variant v_a, Variant v_b);
PlusOperand f_tofr_5(Variant v_a, Variant v_b);
bool f_test_ordered_functions_references_enabled();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_ordered_functions_references_h__
