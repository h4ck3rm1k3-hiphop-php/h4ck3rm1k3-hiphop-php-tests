
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare.php line 26 */
bool f_test_compare_enabled() {
  FUNCTION_INJECTION(test_compare_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare.php line 3 */
Variant f_test_compare(CVarRef v_base) {
  FUNCTION_INJECTION(test_compare);
  int64 v_a = 0;
  String v_b;
  Variant v_t;
  bool v_c = false;
  bool v_d = false;
  bool v_e = false;
  bool v_f = false;
  bool v_g = false;
  bool v_h = false;
  bool v_i = false;
  bool v_j = false;

  (v_a = 42LL);
  (v_b = "69");
  (v_t = v_base);
  LINE(8,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_compare").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_c = (equal(v_a, v_b)));
        (v_d = (same(v_a, v_b)));
        (v_e = (!equal(v_a, v_b)));
        (v_f = (!same(v_a, v_b)));
        (v_g = (less(v_a, v_b)));
        (v_h = (more(v_a, v_b)));
        (v_i = (not_more(v_a, v_b)));
        (v_j = (not_less(v_a, v_b)));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(concat_rev(LINE(20,concat6(" ", toString(v_h), " ", toString(v_i), " ", toString(v_j))), concat_rev(concat6(" ", toString(v_e), " ", toString(v_f), " ", toString(v_g)), concat3(toString(v_c), " ", toString(v_d)))), "  1 1 1  1 ")) {
    LINE(21,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_compare").create()), 0x00000000395D85E8LL));
  }
  return LINE(23,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_compare").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
