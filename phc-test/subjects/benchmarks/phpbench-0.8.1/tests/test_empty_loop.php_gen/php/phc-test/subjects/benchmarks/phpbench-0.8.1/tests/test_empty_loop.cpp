
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty_loop.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty_loop.php line 3 */
Variant f_test_empty_loop(CVarRef v_base) {
  FUNCTION_INJECTION(test_empty_loop);
  Numeric v_t = 0;

  (v_t = v_base * 30LL);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_empty_loop").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(10,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_empty_loop").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty_loop.php line 13 */
bool f_test_empty_loop_enabled() {
  FUNCTION_INJECTION(test_empty_loop_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty_loop.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
