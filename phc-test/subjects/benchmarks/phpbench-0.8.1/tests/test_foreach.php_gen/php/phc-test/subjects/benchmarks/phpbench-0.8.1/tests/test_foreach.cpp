
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_foreach.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_foreach.php line 38 */
bool f_test_foreach_enabled() {
  FUNCTION_INJECTION(test_foreach_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_foreach.php line 3 */
Variant f_test_foreach(CVarRef v_base) {
  FUNCTION_INJECTION(test_foreach);
  Variant v_a;
  Variant v_b;
  Array v_c;
  int64 v_t = 0;
  int64 v_d = 0;
  Primitive v_k = 0;
  Variant v_v;

  (v_a = ScalarArrays::sa_[0]);
  (v_b = ScalarArrays::sa_[0]);
  (v_c = ScalarArrays::sa_[0]);
  (v_t = 100LL);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        v_a.set(concat("x", toString(v_t)), (concat(toString(v_t), "x")));
        v_t--;
      }
    } while (!same(v_t, 0LL));
  }
  (v_t = 10LL);
  {
    LOOP_COUNTER(2);
    do {
      LOOP_COUNTER_CHECK(2);
      {
        v_b.set(concat(toString(v_t), "x"), (concat("x", toString(v_t))));
        v_t--;
      }
    } while (!same(v_t, 0LL));
  }
  (v_t = toInt64(LINE(18,x_round(divide(v_base, 15LL)))));
  LINE(19,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_foreach").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(3);
    do {
      LOOP_COUNTER_CHECK(3);
      {
        (v_d = 0LL);
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_v = iter6->second();
            v_k = iter6->first();
            {
              {
                LOOP_COUNTER(7);
                for (ArrayIterPtr iter9 = v_b.begin(); !iter9->end(); iter9->next()) {
                  LOOP_COUNTER_CHECK(7);
                  v_v = iter9->second();
                  v_k = iter9->first();
                  {
                    v_d++;
                  }
                }
              }
            }
          }
        }
        {
          LOOP_COUNTER(10);
          for (ArrayIter iter12 = v_c.begin(); !iter12.end(); ++iter12) {
            LOOP_COUNTER_CHECK(10);
            v_v = iter12.second();
            {
              v_d++;
            }
          }
        }
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(v_d, 1000LL)) {
    LINE(33,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_foreach").create()), 0x00000000395D85E8LL));
  }
  return LINE(35,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_foreach").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_foreach.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
