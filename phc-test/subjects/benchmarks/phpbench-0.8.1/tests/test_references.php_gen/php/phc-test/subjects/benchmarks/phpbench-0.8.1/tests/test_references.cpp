
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_references.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_references.php line 21 */
bool f_test_references_enabled() {
  FUNCTION_INJECTION(test_references_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_references.php line 3 */
Variant f_test_references(CVarRef v_base) {
  FUNCTION_INJECTION(test_references);
  Variant v_t;
  Variant v_a;
  Variant v_b;
  Variant v_c;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_references").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = 42LL);
        (v_b = ref(v_a));
        (v_b = 69LL);
        (v_c = ref(v_b));
        (v_a = 17LL);
        (v_c = 21LL);
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(15,concat3(toString(v_a), toString(v_b), toString(v_c))), "212121")) {
    LINE(16,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_references").create()), 0x00000000395D85E8LL));
  }
  return LINE(18,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_references").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_references.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
