
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_type.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_type.php line 23 */
bool f_test_is_type_enabled() {
  FUNCTION_INJECTION(test_is_type_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_type.php line 3 */
Variant f_test_is_type(CVarRef v_base) {
  FUNCTION_INJECTION(test_is_type);
  int64 v_a = 0;
  String v_b;
  bool v_c = false;
  double v_d = 0.0;
  Variant v_t;

  (v_a = 1LL);
  (v_b = "Yeepee !");
  (v_c = true);
  (v_d = 3.141592653);
  (v_t = v_base);
  LINE(10,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_is_type").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        if (LINE(12,x_is_string(v_a)) || x_is_bool(v_b) || x_is_float(v_c) || x_is_int(v_d)) {
          f_exit();
        }
      }
    } while (!same(--v_t, 0LL));
  }
  if (!((LINE(17,x_is_int(v_a)) && x_is_string(v_b) && x_is_bool(v_c) && x_is_float(v_d)))) {
    LINE(18,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_is_type").create()), 0x00000000395D85E8LL));
  }
  return LINE(20,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_is_type").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_type.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
