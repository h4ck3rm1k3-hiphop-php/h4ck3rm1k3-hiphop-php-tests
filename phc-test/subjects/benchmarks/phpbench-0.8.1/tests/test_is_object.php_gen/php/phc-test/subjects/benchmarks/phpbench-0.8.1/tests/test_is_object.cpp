
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_object.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_object.php line 3 */
Variant f_test_is_object(CVarRef v_base) {
  FUNCTION_INJECTION(test_is_object);
  String v_a;
  Variant v_t;

  (v_a = LINE(4,x_str_repeat("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", toInt32(1000LL))));
  (v_t = v_base);
  LINE(7,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_is_object").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        if (LINE(9,x_is_object(v_a))) {
          LINE(10,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_is_object").create()), 0x00000000395D85E8LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(14,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_is_object").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_object.php line 17 */
bool f_test_is_object_enabled() {
  FUNCTION_INJECTION(test_is_object_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_object.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
