
#ifndef __GENERATED_cls_testlocalobjectassign_h__
#define __GENERATED_cls_testlocalobjectassign_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php line 3 */
class c_testlocalobjectassign : virtual public ObjectData {
  BEGIN_CLASS_MAP(testlocalobjectassign)
  END_CLASS_MAP(testlocalobjectassign)
  DECLARE_CLASS(testlocalobjectassign, TestLocalObjectAssign, ObjectData)
  void init();
  public: void t_testlocalobjectassign();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: bool t_ok();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testlocalobjectassign_h__
