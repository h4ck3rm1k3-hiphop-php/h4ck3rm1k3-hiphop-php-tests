
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_local_object_assign_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_local_object_assign_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.fw.h>

// Declarations
#include <cls/testlocalobjectassign.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_test_local_object_assign_enabled();
Variant f_test_local_object_assign(CVarRef v_base);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_testlocalobjectassign(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_local_object_assign_h__
