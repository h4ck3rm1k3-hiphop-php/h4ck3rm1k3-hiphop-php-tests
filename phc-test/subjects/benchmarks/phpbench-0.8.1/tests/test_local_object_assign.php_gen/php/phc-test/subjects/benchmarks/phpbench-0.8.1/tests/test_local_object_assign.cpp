
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php line 3 */
Variant c_testlocalobjectassign::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_testlocalobjectassign::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_testlocalobjectassign::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_testlocalobjectassign::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_testlocalobjectassign::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_testlocalobjectassign::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_testlocalobjectassign::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_testlocalobjectassign::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(testlocalobjectassign)
ObjectData *c_testlocalobjectassign::create() {
  init();
  t_testlocalobjectassign();
  return this;
}
ObjectData *c_testlocalobjectassign::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_testlocalobjectassign::cloneImpl() {
  c_testlocalobjectassign *obj = NEW(c_testlocalobjectassign)();
  cloneSet(obj);
  return obj;
}
void c_testlocalobjectassign::cloneSet(c_testlocalobjectassign *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_testlocalobjectassign::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_testlocalobjectassign::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_testlocalobjectassign::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_testlocalobjectassign$os_get(const char *s) {
  return c_testlocalobjectassign::os_get(s, -1);
}
Variant &cw_testlocalobjectassign$os_lval(const char *s) {
  return c_testlocalobjectassign::os_lval(s, -1);
}
Variant cw_testlocalobjectassign$os_constant(const char *s) {
  return c_testlocalobjectassign::os_constant(s);
}
Variant cw_testlocalobjectassign$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_testlocalobjectassign::os_invoke(c, s, params, -1, fatal);
}
void c_testlocalobjectassign::init() {
}
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php line 4 */
void c_testlocalobjectassign::t_testlocalobjectassign() {
  INSTANCE_METHOD_INJECTION(TestLocalObjectAssign, TestLocalObjectAssign::TestLocalObjectAssign);
  bool oldInCtor = gasInCtor(true);
  {
    gasInCtor(oldInCtor);
    return;
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php line 7 */
bool c_testlocalobjectassign::t_ok() {
  INSTANCE_METHOD_INJECTION(TestLocalObjectAssign, TestLocalObjectAssign::ok);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php line 25 */
bool f_test_local_object_assign_enabled() {
  FUNCTION_INJECTION(test_local_object_assign_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php line 12 */
Variant f_test_local_object_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_local_object_assign);
  Variant v_t;
  p_testlocalobjectassign v_a;

  (v_t = v_base);
  LINE(14,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_local_object_assign").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        ((Object)((v_a = ((Object)(LINE(16,p_testlocalobjectassign(p_testlocalobjectassign(NEWOBJ(c_testlocalobjectassign)())->create())))))));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!(LINE(19,x_is_object(((Object)(v_a))))) || !same(v_a->t_ok(), true)) {
    LINE(20,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_local_object_assign").create()), 0x00000000395D85E8LL));
  }
  return LINE(22,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_local_object_assign").create()), 0x000000008B4F9CE7LL));
} /* function */
Object co_testlocalobjectassign(CArrRef params, bool init /* = true */) {
  return Object(p_testlocalobjectassign(NEW(c_testlocalobjectassign)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
