
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_increment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_increment.php line 3 */
Variant f_test_increment(CVarRef v_base) {
  FUNCTION_INJECTION(test_increment);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_t;
  int64 v_a = 0;
  int64 v_b = 0;
  int64 v_c = 0;
  int64 v_d = 0;
  int64 v_e = 0;
  double v_f = 0.0;
  double v_g = 0.0;
  PlusOperand v_h = 0;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_increment").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_b = (v_a = 5LL));
        (v_c = v_a++);
        (v_e = (v_d = ++v_b));
        (v_f = LINE(10,x_ceil(toDouble(v_d++))));
        (v_g = LINE(11,x_floor(toDouble(++v_e))));
        (v_h = v_g += toDouble(2LL));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(15,(assignCallTemp(eo_0, toString(v_a)),assignCallTemp(eo_1, toString(v_b)),assignCallTemp(eo_2, concat6(toString(v_c), toString(v_d), toString(v_e), toString(v_f), toString(v_g), toString(v_h))),concat3(eo_0, eo_1, eo_2))), "66577699")) {
    LINE(16,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_increment").create()), 0x00000000395D85E8LL));
  }
  return LINE(18,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_increment").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_increment.php line 21 */
bool f_test_increment_enabled() {
  FUNCTION_INJECTION(test_increment_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_increment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
