
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_preg_match.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_preg_match.php line 27 */
bool f_test_preg_match_enabled() {
  FUNCTION_INJECTION(test_preg_match_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_preg_match.php line 3 */
Variant f_test_preg_match(CVarRef v_base) {
  FUNCTION_INJECTION(test_preg_match);
  Array v_strings;
  int64 v_t = 0;
  Variant v_matches;
  Variant v_string;

  (v_strings = ScalarArrays::sa_[0]);
  (v_t = toInt64(LINE(9,x_round(divide(v_base, 5LL)))));
  LINE(10,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_preg_match").create()), 0x00000000611583A4LL));
  (v_matches = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for (ArrayIter iter4 = v_strings.begin(); !iter4.end(); ++iter4) {
            LOOP_COUNTER_CHECK(2);
            v_string = iter4.second();
            {
              if (not_more(LINE(17,x_preg_match("/(y(eah|o)|th(e|is)|wesh(hh)\?|benchmarks\?|a|php|is)/i", toString(v_string), ref(v_matches))), 0LL) || empty(v_matches, 1LL, 0x5BCA7C69B794F8CELL)) {
                LINE(19,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_preg_match").create()), 0x00000000395D85E8LL));
              }
            }
          }
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(24,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_preg_match").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_preg_match.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
