
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_hash_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_hash_assign.php line 22 */
bool f_test_local_hash_assign_enabled() {
  FUNCTION_INJECTION(test_local_hash_assign_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_hash_assign.php line 3 */
Variant f_test_local_hash_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_local_hash_assign);
  Variant v_a;
  Variant v_t;

  (v_a = ScalarArrays::sa_[0]);
  (v_t = v_base);
  LINE(7,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_local_hash_assign").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        v_a.set("xxx", ("xxx"), 0x104E03EB5ADE89B5LL);
        v_a.set("1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25", (v_a.rvalAt("xxx", 0x104E03EB5ADE89B5LL)), 0x40D25E2910036B3DLL);
        v_a.set("a, b", (v_a.rvalAt("1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25", 0x40D25E2910036B3DLL)), 0x1FD8B0C8AE073001LL);
        v_a.set("yyy", (v_a.rvalAt("a, b", 0x1FD8B0C8AE073001LL)), 0x1A5E5D9F11BD4039LL);
        v_a.set(4242LL, (v_a.rvalAt("yyy", 0x1A5E5D9F11BD4039LL)), 0x4A26BEC7944DD79ELL);
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(16,x_count(v_a)), 5LL)) {
    LINE(17,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_local_hash_assign").create()), 0x00000000395D85E8LL));
  }
  return LINE(19,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_local_hash_assign").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_hash_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
