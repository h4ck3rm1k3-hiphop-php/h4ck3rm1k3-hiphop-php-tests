
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_integer_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_integer_assign.php line 30 */
bool f_test_local_integer_assign_enabled() {
  FUNCTION_INJECTION(test_local_integer_assign_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_integer_assign.php line 3 */
Variant f_test_local_integer_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_local_integer_assign);
  Variant v_t;
  int64 v_a = 0;
  int64 v_b = 0;
  int64 v_c = 0;
  int64 v_d = 0;
  int64 v_e = 0;
  int64 v_f = 0;
  int64 v_g = 0;
  int64 v_h = 0;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_local_integer_assign").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = 6LL);
        (v_b = 9LL);
        (v_c = 42LL);
        (v_d = 69LL);
        (v_e = 4242LL);
        (v_f = 17LL);
        (v_g = 3200000LL);
        (v_h = -3200000LL);
        ;
        ;
        ;
        ;
        ;
        ;
        ;
        ;
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(v_g + v_h, 0LL)) {
    LINE(25,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_local_integer_assign").create()), 0x00000000395D85E8LL));
  }
  return LINE(27,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_local_integer_assign").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_integer_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
