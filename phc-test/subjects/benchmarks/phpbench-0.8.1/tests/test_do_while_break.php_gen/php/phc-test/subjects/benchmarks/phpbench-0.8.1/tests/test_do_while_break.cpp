
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while_break.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while_break.php line 3 */
Variant f_test_do_while_break(CVarRef v_base) {
  FUNCTION_INJECTION(test_do_while_break);
  Numeric v_t = 0;
  int64 v_d = 0;

  (v_t = v_base * 10LL);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_do_while_break").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_d = 1000LL);
        {
          LOOP_COUNTER(2);
          do {
            LOOP_COUNTER_CHECK(2);
            {
              break;
            }
          } while (!same(v_d, 0LL));
        }
        {
          LOOP_COUNTER(3);
          for (; ; ) {
            LOOP_COUNTER_CHECK(3);
            {
              break;
            }
          }
        }
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(v_d, 1000LL)) {
    LINE(17,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_do_while_break").create()), 0x00000000395D85E8LL));
  }
  return LINE(19,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_do_while_break").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while_break.php line 22 */
bool f_test_do_while_break_enabled() {
  FUNCTION_INJECTION(test_do_while_break_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while_break.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
