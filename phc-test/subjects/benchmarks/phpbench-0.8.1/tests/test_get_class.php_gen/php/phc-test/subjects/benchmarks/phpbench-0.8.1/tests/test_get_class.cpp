
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php line 3 */
Variant c_testgetclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_testgetclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_testgetclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_testgetclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_testgetclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_testgetclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_testgetclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_testgetclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(testgetclass)
ObjectData *c_testgetclass::create() {
  init();
  t_testgetclass();
  return this;
}
ObjectData *c_testgetclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_testgetclass::dynConstruct(CArrRef params) {
  (t_testgetclass());
}
ObjectData *c_testgetclass::cloneImpl() {
  c_testgetclass *obj = NEW(c_testgetclass)();
  cloneSet(obj);
  return obj;
}
void c_testgetclass::cloneSet(c_testgetclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_testgetclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3E209BF39A8AE9F9LL, testgetclass) {
        return (t_testgetclass(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_testgetclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x3E209BF39A8AE9F9LL, testgetclass) {
        return (t_testgetclass(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_testgetclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_testgetclass$os_get(const char *s) {
  return c_testgetclass::os_get(s, -1);
}
Variant &cw_testgetclass$os_lval(const char *s) {
  return c_testgetclass::os_lval(s, -1);
}
Variant cw_testgetclass$os_constant(const char *s) {
  return c_testgetclass::os_constant(s);
}
Variant cw_testgetclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_testgetclass::os_invoke(c, s, params, -1, fatal);
}
void c_testgetclass::init() {
}
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php line 4 */
void c_testgetclass::t_testgetclass() {
  INSTANCE_METHOD_INJECTION(TestGetClass, TestGetClass::TestGetClass);
  bool oldInCtor = gasInCtor(true);
  {
    gasInCtor(oldInCtor);
    return;
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php line 7 */
bool c_testgetclass::t_ok() {
  INSTANCE_METHOD_INJECTION(TestGetClass, TestGetClass::ok);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php line 12 */
Variant f_test_get_class(CVarRef v_base) {
  FUNCTION_INJECTION(test_get_class);
  p_testgetclass v_a;
  Variant v_t;

  ((Object)((v_a = ((Object)(LINE(13,p_testgetclass(p_testgetclass(NEWOBJ(c_testgetclass)())->create())))))));
  (v_t = v_base);
  LINE(16,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_get_class").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        if (!same(LINE(18,x_strtolower(toString(x_get_class(((Object)(v_a)))))), "testgetclass")) {
          LINE(19,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_get_class").create()), 0x00000000395D85E8LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  if (!(LINE(23,x_is_object(((Object)(v_a))))) || !same(v_a->t_ok(), true)) {
    LINE(24,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_get_class").create()), 0x00000000395D85E8LL));
  }
  return LINE(26,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_get_class").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php line 29 */
bool f_test_get_class_enabled() {
  FUNCTION_INJECTION(test_get_class_enabled);
  return true;
} /* function */
Object co_testgetclass(CArrRef params, bool init /* = true */) {
  return Object(p_testgetclass(NEW(c_testgetclass)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
