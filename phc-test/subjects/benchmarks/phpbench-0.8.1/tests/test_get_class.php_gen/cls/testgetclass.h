
#ifndef __GENERATED_cls_testgetclass_h__
#define __GENERATED_cls_testgetclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php line 3 */
class c_testgetclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(testgetclass)
  END_CLASS_MAP(testgetclass)
  DECLARE_CLASS(testgetclass, TestGetClass, ObjectData)
  void init();
  public: void t_testgetclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: bool t_ok();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testgetclass_h__
