
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_casting.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_casting.php line 3 */
Variant f_test_casting(CVarRef v_base) {
  FUNCTION_INJECTION(test_casting);
  Variant v_t;
  Variant v_a;
  int64 v_b = 0;
  Numeric v_c = 0;
  Variant v_d;
  String v_e;
  double v_f = 0.0;
  String v_g;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_casting").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = 0.58999999999999997);
        (v_b = toInt64(v_a));
        (v_c = v_a + v_b);
        (v_d = (Variant)("xxxxxxxxxxxxx") + v_c);
        (v_e = LINE(11,concat3(toString(v_b), toString(v_d), toString(v_a))));
        (v_f = toDouble(v_e));
        (v_g = LINE(13,concat6(toString(v_a), toString(v_b), toString(v_c), toString(v_d), v_e, toString(v_f))));
        LINE(14,x_settype(ref(v_a), "string"));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(v_g, "0.5900.590.5900.590.590.59")) {
    LINE(18,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_casting").create()), 0x00000000395D85E8LL));
  }
  return LINE(20,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_casting").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_casting.php line 23 */
bool f_test_casting_enabled() {
  FUNCTION_INJECTION(test_casting_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_casting.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
