
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_TEST_CONSTANTS_NAME;
extern const StaticString k_TEST_CONSTANTS_XXX;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x69804BE04BC7464CLL, k_TEST_CONSTANTS_NAME, TEST_CONSTANTS_NAME);
      break;
    case 1:
      HASH_RETURN(0x388B0A22EF38FC95LL, k_TEST_CONSTANTS_XXX, TEST_CONSTANTS_XXX);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
