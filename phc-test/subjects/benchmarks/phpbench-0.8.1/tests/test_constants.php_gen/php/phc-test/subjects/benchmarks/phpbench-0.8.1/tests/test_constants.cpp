
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_constants.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_TEST_CONSTANTS_NAME = "TEST_CONSTANTS_XXX";
const StaticString k_TEST_CONSTANTS_XXX = "xxx";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_constants.php line 3 */
Variant f_test_constants(CVarRef v_base) {
  FUNCTION_INJECTION(test_constants);
  Variant v_t;
  String v_a;
  bool v_b = false;
  Variant v_c;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_constants").create()), 0x00000000611583A4LL));
  ;
  ;
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = "xxx" /* TEST_CONSTANTS_XXX */);
        (v_b = true);
        (v_c = LINE(11,x_constant("TEST_CONSTANTS_XXX" /* TEST_CONSTANTS_NAME */)));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(14,concat3(v_a, toString(v_b), toString(v_c))), "xxx1xxx")) {
    LINE(15,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_constants").create()), 0x00000000395D85E8LL));
  }
  return LINE(17,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_constants").create()), 0x000000008B4F9CE7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_constants.php line 20 */
bool f_test_constants_enabled() {
  FUNCTION_INJECTION(test_constants_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_constants.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
