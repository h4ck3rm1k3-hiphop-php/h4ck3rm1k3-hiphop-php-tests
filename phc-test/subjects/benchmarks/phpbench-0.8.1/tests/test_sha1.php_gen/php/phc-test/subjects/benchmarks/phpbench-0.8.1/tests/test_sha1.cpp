
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_sha1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_sha1.php line 15 */
bool f_test_sha1_enabled() {
  FUNCTION_INJECTION(test_sha1_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_sha1.php line 3 */
Variant f_test_sha1(CVarRef v_base) {
  FUNCTION_INJECTION(test_sha1);
  Variant v_t;

  (v_t = v_base);
  LINE(5,invoke_failed("test_start", Array(ArrayInit(1).set(0, "test_sha1").create()), 0x00000000611583A4LL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        if (equal(LINE(7,x_sha1(toString(v_t))), "")) {
          LINE(8,invoke_failed("test_regression", Array(ArrayInit(1).set(0, "test_sha1").create()), 0x00000000395D85E8LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(12,invoke_failed("test_end", Array(ArrayInit(1).set(0, "test_sha1").create()), 0x000000008B4F9CE7LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_sha1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
