
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/README.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$README(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/README);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$README;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\n\t\t  -=- Documentation for PHPBench 0.8.1 -=-\n\t\t\thttp://phpbench.pureftpd.org\n\n\n  PHPBench is a benchmark suite for PHP.\n  \n  It performs a large number of simple tests in order to bench various\naspects of the PHP interpreter.\n\n  PHPBench can be used to compare hardware, operating systems, PHP versions,\nPHP accelerators and caches, compiler options, etc.\n\n  Custom tests can be easily added to the ");
  echo("suite.\n\n\n\t       ----------------[ Basic usage ]----------------\n\n  The 'tests' directory contains all tests that PHPBench will try to perform.\n  This directory can be copied in:\n  \n - /usr/local/lib/phpbench/tests\n - /usr/local/share/phpbench/tests\n - /usr/lib/phpbench/tests\n - /usr/share/phpbench/tests\n - /opt/phpbench/tests\n  \n  Alternatively, if you run this application in its own directory, y");
  echo("ou don't\nneed to copy the 'test' directory.\n\n  In order to start a basic benchmark, just type:\n  \n  ./phpbench.php\n  \n  or if something goes wrong:\n  \n  php phpbench.php\n  \n  or if you want to save the output to a file:\n  \n  ./phpbench.php | tee output.txt\n  \n\n\t    ----------------[ Iterations number ]----------------\n\n  In order to get accurate results, every test is evaluated more than once.\n  \n");
  echo("  By defaut, tests are evaluated 100000 times, or multiples of that value for\nsome tests that are known to be CPU-expensive or other tests that are known to\nbe very fast.\n\n  If you have slow hardware or very fast hardware, you can change the number\nof iterations, with the -i switch:\n\n  ./phpbench.php -i 5000\n  \n  The 'score' you get when the benchmark completes is a ratio between the\nnumber of ite");
  echo("rations and the total time that was needed to perform all\ntests. Thus, the score should be independant of the number of iterations,\nbut the higher number you use, the more accuracy you will get.\n\n\n\t       ----------------[ CSV output ]----------------\n\n  You can save a CSV summary of the benchmark. This summary contains the\ntime needed to perform every test, and a percentile value. A value of 12%\n");
  echo("would mean that 12% of the total time was needed for that particular test.\n\n  The summary can be loaded in almost any spreadsheet, including, but not\nlimited to: Kspread (Koffice), OOcalc (Openoffice), Gnumeric and Excel.\n\n  In order to produce that CSV file, just use the -f option, followed by the\nname of the file you are willing to create:\n\n  ./phpbench.php -f summary.csv\n\n\n\n                    ");
  echo("                 Frank Denis <j@pureftpd.org>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
