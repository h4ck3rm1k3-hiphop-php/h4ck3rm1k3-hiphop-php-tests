
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/ChangeLog.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$ChangeLog(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/ChangeLog);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$ChangeLog;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("* Version 0.8\n  Initial release.\n  \n* Version 0.8.1\n  The regression test of test_bitwise has been fixed for 64-bits CPUs.\n  \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
