
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_CSV_NL;
extern const StaticString k_CSV_SEP;
extern const int64 k_MIN_BASE;
extern const int64 k_DEFAULT_BASE;
extern const StaticString k_PHPBENCH_VERSION;

extern const StaticString k_TEST_CONSTANTS_NAME;
extern const StaticString k_TEST_CONSTANTS_XXX;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 15) {
    case 0:
      HASH_RETURN(0x1A88D64959968EE0LL, k_PHPBENCH_VERSION, PHPBENCH_VERSION);
      break;
    case 1:
      HASH_RETURN(0x1592C1C29D032B11LL, k_CSV_NL, CSV_NL);
      break;
    case 5:
      HASH_RETURN(0x388B0A22EF38FC95LL, k_TEST_CONSTANTS_XXX, TEST_CONSTANTS_XXX);
      break;
    case 7:
      HASH_RETURN(0x55F25A94E1B990C7LL, k_CSV_SEP, CSV_SEP);
      break;
    case 8:
      HASH_RETURN(0x173C7275D2FC6E48LL, k_MIN_BASE, MIN_BASE);
      break;
    case 11:
      HASH_RETURN(0x0C7350955C368FFBLL, k_DEFAULT_BASE, DEFAULT_BASE);
      break;
    case 12:
      HASH_RETURN(0x69804BE04BC7464CLL, k_TEST_CONSTANTS_NAME, TEST_CONSTANTS_NAME);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
