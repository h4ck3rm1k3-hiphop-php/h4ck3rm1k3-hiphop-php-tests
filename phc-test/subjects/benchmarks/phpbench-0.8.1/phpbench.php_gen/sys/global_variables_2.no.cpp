
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 0:
      HASH_INITIALIZED(0x2C5D5BC33920E200LL, g->GV(o),
                       o);
      break;
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 2:
      HASH_INITIALIZED(0x2343A72E98024302LL, g->GV(l),
                       l);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 8:
      HASH_INITIALIZED(0x2BCCE9AD79BC2688LL, g->GV(g),
                       g);
      break;
    case 9:
      HASH_INITIALIZED(0x117B8667E4662809LL, g->GV(n),
                       n);
      break;
    case 10:
      HASH_INITIALIZED(0x4292CEE227B9150ALL, g->GV(a),
                       a);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 24:
      HASH_INITIALIZED(0x0EB22EDA95766E98LL, g->GV(i),
                       i);
      break;
    case 31:
      HASH_INITIALIZED(0x7E4ECF842187199FLL, g->GV(tests_list),
                       tests_list);
      break;
    case 34:
      HASH_INITIALIZED(0x61E00480A583B822LL, g->GV(csv_file),
                       csv_file);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 36:
      HASH_INITIALIZED(0x626277CA747EAF24LL, g->GV(TESTS_DIRS),
                       TESTS_DIRS);
      break;
    case 38:
      HASH_INITIALIZED(0x77F632A4E34F1526LL, g->GV(p),
                       p);
      break;
    case 39:
      HASH_INITIALIZED(0x56FD8ECCBDFDD7A7LL, g->GV(base),
                       base);
      break;
    case 46:
      HASH_INITIALIZED(0x6BB4A0689FBAD42ELL, g->GV(f),
                       f);
      break;
    case 48:
      HASH_INITIALIZED(0x32C769EE5C5509B0LL, g->GV(c),
                       c);
      break;
    case 56:
      HASH_INITIALIZED(0x66E32C011EFD3838LL, g->GV(results),
                       results);
      break;
    case 60:
      HASH_INITIALIZED(0x61F3F2F68516FEBCLL, g->GV(GLOBAL_TEST_FUNC),
                       GLOBAL_TEST_FUNC);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    case 68:
      HASH_INITIALIZED(0x7A452383AA9BF7C4LL, g->GV(d),
                       d);
      break;
    case 70:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      HASH_INITIALIZED(0x4B27011F259D2446LL, g->GV(j),
                       j);
      break;
    case 73:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 75:
      HASH_INITIALIZED(0x7A0C10E3609EA04BLL, g->GV(m),
                       m);
      break;
    case 79:
      HASH_INITIALIZED(0x0D6857FDDD7F21CFLL, g->GV(k),
                       k);
      break;
    case 80:
      HASH_INITIALIZED(0x7467450ADEDFCFD0LL, g->GV(GLOBAL_TEST_START_TIME),
                       GLOBAL_TEST_START_TIME);
      break;
    case 81:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 85:
      HASH_INITIALIZED(0x08FBB133F8576BD5LL, g->GV(b),
                       b);
      break;
    case 106:
      HASH_INITIALIZED(0x7C17922060DCA1EALL, g->GV(options),
                       options);
      break;
    case 107:
      HASH_INITIALIZED(0x61B161496B7EA7EBLL, g->GV(e),
                       e);
      break;
    case 110:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 112:
      HASH_INITIALIZED(0x695875365480A8F0LL, g->GV(h),
                       h);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
