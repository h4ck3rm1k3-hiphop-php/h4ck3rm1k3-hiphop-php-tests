
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_fixed_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_hardcoded_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_comment_loop_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_strict_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_crc32_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_string_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_if_constant_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_array_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_isset_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_float_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_mt_rand_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_rand_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_strlen_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_time_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_while_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 0:
      HASH_INCLUDE(0x458D0F52647F6B80LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_bitwise.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php);
      break;
    case 2:
      HASH_INCLUDE(0x3CD9993A454BF482LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_type.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php);
      break;
    case 3:
      HASH_INCLUDE(0x1460686184164303LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty_loop.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php);
      break;
    case 4:
      HASH_INCLUDE(0x23101B0B6DCD2704LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php);
      break;
    case 6:
      HASH_INCLUDE(0x79F32792334AB986LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_strict.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_strict_php);
      break;
    case 7:
      HASH_INCLUDE(0x52B3E0B0E8F4AE07LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_increment.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php);
      break;
    case 8:
      HASH_INCLUDE(0x437874DDA4464B88LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_rand.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_rand_php);
      break;
    case 9:
      HASH_INCLUDE(0x1E9F6B04AEC88009LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_constants.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php);
      break;
    case 12:
      HASH_INCLUDE(0x00E8A5CF3792150CLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_string_append.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php);
      break;
    case 13:
      HASH_INCLUDE(0x71BEED273A99B98DLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php);
      break;
    case 15:
      HASH_INCLUDE(0x6CE867B5066EDF0FLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_invert.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php);
      break;
    case 19:
      HASH_INCLUDE(0x2DD8BF0DD87E0413LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php);
      HASH_INCLUDE(0x3587D2065F07BD93LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_hash_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php);
      break;
    case 20:
      HASH_INCLUDE(0x18EBED959286E514LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_float_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_float_assign_php);
      break;
    case 22:
      HASH_INCLUDE(0x5797631517260F16LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_arithmetic.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php);
      break;
    case 23:
      HASH_INCLUDE(0x1E1D7AA78D58C017LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_md5.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php);
      break;
    case 29:
      HASH_INCLUDE(0x2C4A9ADF4984991DLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_boolean_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php);
      break;
    case 32:
      HASH_INCLUDE(0x35D8702C0DE7B520LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php);
      break;
    case 33:
      HASH_INCLUDE(0x381C50E03F2D3E21LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_references.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php);
      break;
    case 42:
      HASH_INCLUDE(0x186C61317D66BAAALL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_foreach.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php);
      break;
    case 44:
      HASH_INCLUDE(0x4EAD74BC360317ACLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_var.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php);
      HASH_INCLUDE(0x73B3D0FA0F2A16ACLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_scalar_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php);
      break;
    case 46:
      HASH_INCLUDE(0x3F6AFA4D0EEF22AELL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while_break.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php);
      break;
    case 47:
      HASH_INCLUDE(0x698610C8481DA9AFLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_while.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_while_php);
      break;
    case 55:
      HASH_INCLUDE(0x35EA547F2B4CCAB7LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_if_constant.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_if_constant_php);
      break;
    case 60:
      HASH_INCLUDE(0x15A9F1E32AF27EBCLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_line.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php);
      break;
    case 63:
      HASH_INCLUDE(0x74CB5646EC6907BFLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_casting.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php);
      break;
    case 64:
      HASH_INCLUDE(0x3645C9BF7EA20840LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_sha1.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php);
      break;
    case 67:
      HASH_INCLUDE(0x3728D73E0E978443LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_array_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php);
      break;
    case 71:
      HASH_INCLUDE(0x60CB0C1F549E3F47LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ereg.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php);
      break;
    case 77:
      HASH_INCLUDE(0x3AEE583D9C0F51CDLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_string_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_string_assign_php);
      HASH_INCLUDE(0x5415E027EA58B44DLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_mt_rand.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_mt_rand_php);
      break;
    case 83:
      HASH_INCLUDE(0x67E55AFFE88A51D3LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_switch.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php);
      break;
    case 84:
      HASH_INCLUDE(0x33261EF4B63B7654LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_integer_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php);
      HASH_INCLUDE(0x204945BDE30036D4LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_strlen.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_strlen_php);
      break;
    case 85:
      HASH_INCLUDE(0x5F01A22768C255D5LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_string_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php);
      break;
    case 86:
      HASH_INCLUDE(0x723D8180BBE3FB56LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_isset.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_isset_php);
      HASH_INCLUDE(0x258001EDB248D3D6LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ord.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php);
      break;
    case 87:
      HASH_INCLUDE(0x1F2C98DC3AC54AD7LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_unstrict.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php);
      HASH_INCLUDE(0x791E40106EA2FDD7LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_time.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_time_php);
      break;
    case 91:
      HASH_INCLUDE(0x5A67118C7F99865BLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_scalar_assign.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php);
      break;
    case 94:
      HASH_INCLUDE(0x3655484EC5155C5ELL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_crc32.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_crc32_php);
      break;
    case 97:
      HASH_INCLUDE(0x7C21A8D73F480DE1LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_hardcoded.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_hardcoded_php);
      break;
    case 98:
      HASH_INCLUDE(0x7B760907C01AC2E2LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php);
      break;
    case 99:
      HASH_INCLUDE(0x0A5D144D8644B163LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_variable_variables.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php);
      break;
    case 103:
      HASH_INCLUDE(0x508C0630DA3FAD67LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_fixed.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_fixed_php);
      break;
    case 104:
      HASH_INCLUDE(0x18C53D3883A16668LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_preg_match.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php);
      break;
    case 106:
      HASH_INCLUDE(0x4E83F06A2173686ALL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_array.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_array_php);
      break;
    case 107:
      HASH_INCLUDE(0x68DF53D96F73AA6BLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php);
      break;
    case 114:
      HASH_INCLUDE(0x21CBEC7547155DF2LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_comment_loop.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_comment_loop_php);
      HASH_INCLUDE(0x753691A15039FCF2LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php);
      break;
    case 120:
      HASH_INCLUDE(0x7EA795C2DC1D4FF8LL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_false.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php);
      break;
    case 122:
      HASH_INCLUDE(0x1B1124C3BC9611FALL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_array_operators.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php);
      HASH_INCLUDE(0x299702ADD618157ALL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_php);
      HASH_INCLUDE(0x1104A7ADEAE160FALL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_object.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php);
      break;
    case 123:
      HASH_INCLUDE(0x2546E1769AC52CFBLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php);
      HASH_INCLUDE(0x63783164C76C4AFBLL, "phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_microtime.php", php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
