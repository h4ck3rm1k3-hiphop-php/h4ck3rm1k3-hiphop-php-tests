
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

static void print(FILE *fp, String s) {
  if (fp) {
    fwrite(s.c_str(), 1, s.size(), fp);
    return;
  }
  echo(s);
}
static void output_dynamic_constants(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_constants = json_decode('");
  Array dynamic_constants;
  String s = f_json_encode(dynamic_constants);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_static_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$static_global_variables = json_decode('");
  Array static_global_variables;
  static_global_variables.set("gv_argc", g->get("argc"));
  static_global_variables.set("gv_argv", g->get("argv"));
  static_global_variables.set("gv__SERVER", g->get("_SERVER"));
  static_global_variables.set("gv__GET", g->get("_GET"));
  static_global_variables.set("gv__POST", g->get("_POST"));
  static_global_variables.set("gv__COOKIE", g->get("_COOKIE"));
  static_global_variables.set("gv__FILES", g->get("_FILES"));
  static_global_variables.set("gv__ENV", g->get("_ENV"));
  static_global_variables.set("gv__REQUEST", g->get("_REQUEST"));
  static_global_variables.set("gv__SESSION", g->get("_SESSION"));
  static_global_variables.set("gv_HTTP_RAW_POST_DATA", g->get("HTTP_RAW_POST_DATA"));
  static_global_variables.set("gv_http_response_header", g->get("http_response_header"));
  static_global_variables.set("gv_a", g->get("a"));
  static_global_variables.set("gv_b", g->get("b"));
  static_global_variables.set("gv_c", g->get("c"));
  static_global_variables.set("gv_d", g->get("d"));
  static_global_variables.set("gv_e", g->get("e"));
  static_global_variables.set("gv_f", g->get("f"));
  static_global_variables.set("gv_g", g->get("g"));
  static_global_variables.set("gv_h", g->get("h"));
  static_global_variables.set("gv_i", g->get("i"));
  static_global_variables.set("gv_j", g->get("j"));
  static_global_variables.set("gv_k", g->get("k"));
  static_global_variables.set("gv_l", g->get("l"));
  static_global_variables.set("gv_m", g->get("m"));
  static_global_variables.set("gv_n", g->get("n"));
  static_global_variables.set("gv_o", g->get("o"));
  static_global_variables.set("gv_p", g->get("p"));
  static_global_variables.set("gv_TESTS_DIRS", g->get("TESTS_DIRS"));
  static_global_variables.set("gv_GLOBAL_TEST_FUNC", g->get("GLOBAL_TEST_FUNC"));
  static_global_variables.set("gv_GLOBAL_TEST_START_TIME", g->get("GLOBAL_TEST_START_TIME"));
  static_global_variables.set("gv_base", g->get("base"));
  static_global_variables.set("gv_csv_file", g->get("csv_file"));
  static_global_variables.set("gv_options", g->get("options"));
  static_global_variables.set("gv_tests_list", g->get("tests_list"));
  static_global_variables.set("gv_results", g->get("results"));
  String s = f_json_encode(static_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_dynamic_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_global_variables = json_decode('");
  Array dynamic_global_variables;
  dynamic_global_variables = *get_variable_table();
  String s = f_json_encode(dynamic_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_variables = json_decode('");
  Array method_static_variables;
  String s = f_json_encode(method_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_inited(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_inited = json_decode('");
  Array method_static_inited;
  String s = f_json_encode(method_static_inited);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_class_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$class_static_variables = json_decode('");
  Array class_static_variables;
  String s = f_json_encode(class_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_pseudomain_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$pseudomain_variables = json_decode('");
  Array pseudomain_variables;
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_fixed_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_fixed_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_hardcoded_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_hardcoded_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_comment_loop_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_comment_loop_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_strict_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_strict_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_crc32_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_crc32_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_string_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_string_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_if_constant_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_if_constant_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_array_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_array_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_isset_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_isset_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_float_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_float_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_mt_rand_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_mt_rand_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_rand_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_rand_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_strlen_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_strlen_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_time_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_time_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php);
  pseudomain_variables.set("run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_while_php", g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_while_php);
  String s = f_json_encode(pseudomain_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_functions(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_functions = json_decode('");
  Array redeclared_functions;
  String s = f_json_encode(redeclared_functions);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_classes(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_classes = json_decode('");
  Array redeclared_classes;
  String s = f_json_encode(redeclared_classes);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

void output_global_state(FILE *fp) {
  output_static_global_variables(fp);
  output_dynamic_global_variables(fp);
  output_dynamic_constants(fp);
  output_method_static_variables(fp);
  output_method_static_inited(fp);
  output_class_static_variables(fp);
  output_pseudomain_variables(fp);
  output_redeclared_functions(fp);
  output_redeclared_classes(fp);
}


///////////////////////////////////////////////////////////////////////////////
}
