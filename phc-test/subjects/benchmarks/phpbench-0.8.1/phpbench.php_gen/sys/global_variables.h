
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(a)
    GVS(b)
    GVS(c)
    GVS(d)
    GVS(e)
    GVS(f)
    GVS(g)
    GVS(h)
    GVS(i)
    GVS(j)
    GVS(k)
    GVS(l)
    GVS(m)
    GVS(n)
    GVS(o)
    GVS(p)
    GVS(TESTS_DIRS)
    GVS(GLOBAL_TEST_FUNC)
    GVS(GLOBAL_TEST_START_TIME)
    GVS(base)
    GVS(csv_file)
    GVS(options)
    GVS(tests_list)
    GVS(results)
  END_GVS(24)

  // Dynamic Constants

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_fixed_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_hardcoded_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_comment_loop_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_strict_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_crc32_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_string_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_if_constant_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_array_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_isset_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_float_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_mt_rand_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_rand_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_strlen_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_time_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php;
  bool run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_while_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 36;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[9];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
