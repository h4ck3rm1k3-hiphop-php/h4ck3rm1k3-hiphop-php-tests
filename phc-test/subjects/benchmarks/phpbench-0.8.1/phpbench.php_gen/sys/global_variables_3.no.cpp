
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 0:
      HASH_INDEX(0x2C5D5BC33920E200LL, o, 26);
      break;
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x2343A72E98024302LL, l, 23);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 8:
      HASH_INDEX(0x2BCCE9AD79BC2688LL, g, 18);
      break;
    case 9:
      HASH_INDEX(0x117B8667E4662809LL, n, 25);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 12);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 20);
      break;
    case 31:
      HASH_INDEX(0x7E4ECF842187199FLL, tests_list, 34);
      break;
    case 34:
      HASH_INDEX(0x61E00480A583B822LL, csv_file, 32);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 36:
      HASH_INDEX(0x626277CA747EAF24LL, TESTS_DIRS, 28);
      break;
    case 38:
      HASH_INDEX(0x77F632A4E34F1526LL, p, 27);
      break;
    case 39:
      HASH_INDEX(0x56FD8ECCBDFDD7A7LL, base, 31);
      break;
    case 46:
      HASH_INDEX(0x6BB4A0689FBAD42ELL, f, 17);
      break;
    case 48:
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 14);
      break;
    case 56:
      HASH_INDEX(0x66E32C011EFD3838LL, results, 35);
      break;
    case 60:
      HASH_INDEX(0x61F3F2F68516FEBCLL, GLOBAL_TEST_FUNC, 29);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 68:
      HASH_INDEX(0x7A452383AA9BF7C4LL, d, 15);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x4B27011F259D2446LL, j, 21);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 75:
      HASH_INDEX(0x7A0C10E3609EA04BLL, m, 24);
      break;
    case 79:
      HASH_INDEX(0x0D6857FDDD7F21CFLL, k, 22);
      break;
    case 80:
      HASH_INDEX(0x7467450ADEDFCFD0LL, GLOBAL_TEST_START_TIME, 30);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 85:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 13);
      break;
    case 106:
      HASH_INDEX(0x7C17922060DCA1EALL, options, 33);
      break;
    case 107:
      HASH_INDEX(0x61B161496B7EA7EBLL, e, 16);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 112:
      HASH_INDEX(0x695875365480A8F0LL, h, 19);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 36) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
