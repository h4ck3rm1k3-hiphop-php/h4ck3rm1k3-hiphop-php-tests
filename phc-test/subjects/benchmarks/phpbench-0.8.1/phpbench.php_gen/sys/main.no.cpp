
#include <cpp/base/hphp.h>
#include <cpp/base/array/zend_array.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


IMPLEMENT_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
GlobalVariables::GlobalVariables() : dummy(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_fixed_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_hardcoded_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_comment_loop_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_strict_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_crc32_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_string_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_if_constant_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_array_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_isset_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_float_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_mt_rand_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_rand_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_strlen_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_time_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php(false),
  run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_while_php(false) {

  // Dynamic Constants

  // Primitive Function/Method Static Variables

  // Primitive Class Static Variables

  // Redeclared Functions

  // Redeclared Classes
}

void GlobalVariables::initialize() {
  SystemGlobals::initialize();
}

void init_static_variables() { ScalarArrays::initialize();}
static ThreadLocalSingleton<GlobalVariables> g_variables;
static IMPLEMENT_THREAD_LOCAL(GlobalArrayWrapper, g_array_wrapper);
GlobalVariables *get_global_variables() {
  return g_variables.get();
}
void init_global_variables() { GlobalVariables::initialize();}
void free_global_variables() {
  g_variables.reset();
  g_array_wrapper.reset();
}
LVariableTable *get_variable_table() { return (LVariableTable*)get_global_variables();}
Globals *get_globals() { return (Globals*)get_global_variables();}
SystemGlobals *get_system_globals() { return (SystemGlobals*)get_global_variables();}
Array get_global_array_wrapper(){ return g_array_wrapper.get();}

///////////////////////////////////////////////////////////////////////////////
}

#ifndef HPHP_BUILD_LIBRARY
int main(int argc, char** argv) {
  return HPHP::execute_program(argc, argv);
}
#endif
