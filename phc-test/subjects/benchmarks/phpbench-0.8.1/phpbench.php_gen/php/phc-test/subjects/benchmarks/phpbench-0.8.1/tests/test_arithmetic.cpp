
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_arithmetic.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_arithmetic.php line 3 */
Variant f_test_arithmetic(CVarRef v_base) {
  FUNCTION_INJECTION(test_arithmetic);
  Variant v_t;
  Variant v_a;
  Numeric v_b = 0;
  Variant v_c;

  (v_t = v_base);
  LINE(5,f_test_start("test_arithmetic"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = 5LL);
        (v_b = negate(v_a));
        (v_a = v_a + v_b);
        (v_b = v_a - v_b);
        (v_a = v_a * v_b);
        (v_c = v_a);
        (v_a = 1LL);
        (silenceInc(), silenceDec((v_b = divide(v_b, v_a))));
        (v_a = modulo(toInt64(v_a), toInt64(v_b)));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!((empty(v_a) && empty(v_b))) || !same(v_c, 0LL)) {
  }
  return LINE(21,f_test_end("test_arithmetic"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_arithmetic.php line 24 */
bool f_test_arithmetic_enabled() {
  FUNCTION_INJECTION(test_arithmetic_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_arithmetic.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
