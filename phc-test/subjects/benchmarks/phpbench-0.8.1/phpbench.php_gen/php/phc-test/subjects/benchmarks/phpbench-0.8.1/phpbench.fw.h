
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_phpbench_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_phpbench_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_CSV_NL;
extern const StaticString k_CSV_SEP;
extern const int64 k_MIN_BASE;
extern const int64 k_DEFAULT_BASE;
extern const StaticString k_PHPBENCH_VERSION;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_phpbench_fw_h__
