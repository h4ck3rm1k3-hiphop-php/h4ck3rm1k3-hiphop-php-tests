
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php line 20 */
Variant f_tuf_1(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tuf_1);
  return LINE(21,f_tuf_2(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php line 24 */
Variant f_tuf_2(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tuf_2);
  return LINE(25,f_tuf_3(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php line 28 */
Variant f_tuf_3(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tuf_3);
  return LINE(29,f_tuf_4(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php line 32 */
Variant f_tuf_4(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tuf_4);
  return LINE(33,f_tuf_5(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php line 36 */
Variant f_tuf_5(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tuf_5);
  return v_a + v_b;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php line 3 */
Variant f_test_unordered_functions(CVarRef v_base) {
  FUNCTION_INJECTION(test_unordered_functions);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_t;
  Variant v_a;
  Variant v_b;
  Variant v_c;
  Variant v_d;
  Variant v_e;

  (v_t = v_base);
  LINE(5,f_test_start("test_unordered_functions"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = LINE(7,f_tuf_1(42LL, -1LL)));
        (v_b = LINE(8,f_tuf_1(v_a, 42.670000000000002)));
        (v_c = LINE(9,f_tuf_1(v_b, "xxx")));
        (v_d = LINE(10,f_tuf_1(v_c, null)));
        (v_e = LINE(11,(assignCallTemp(eo_0, v_a),assignCallTemp(eo_1, (assignCallTemp(eo_2, v_b),assignCallTemp(eo_3, (assignCallTemp(eo_4, v_c),assignCallTemp(eo_5, f_tuf_1(v_d, v_a)),f_tuf_1(eo_4, eo_5))),f_tuf_1(eo_2, eo_3))),f_tuf_1(eo_0, eo_1))));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(14,concat5(toString(v_a), toString(v_b), toString(v_c), toString(v_d), toString(v_e))), "4183.6783.6783.67333.01")) {
    LINE(15,f_test_regression("test_unordered_functions"));
  }
  return LINE(17,f_test_end("test_unordered_functions"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php line 40 */
bool f_test_unordered_functions_enabled() {
  FUNCTION_INJECTION(test_unordered_functions_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
