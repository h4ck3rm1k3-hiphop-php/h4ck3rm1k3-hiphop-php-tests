
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ereg.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ereg.php line 3 */
Variant f_test_ereg(CVarRef v_base) {
  FUNCTION_INJECTION(test_ereg);
  Array v_strings;
  int64 v_t = 0;
  Variant v_matches;
  Variant v_string;

  (v_strings = ScalarArrays::sa_[3]);
  (v_t = toInt64(LINE(9,x_round(divide(v_base, 5LL)))));
  LINE(10,f_test_start("test_ereg"));
  (v_matches = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for (ArrayIter iter4 = v_strings.begin(); !iter4.end(); ++iter4) {
            LOOP_COUNTER_CHECK(2);
            v_string = iter4.second();
            {
              if (not_more(LINE(17,x_eregi("^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\\.-][a-z0-9]{1,})+)*$", toString(v_string), ref(v_matches))), 0LL) || empty(v_matches, 2LL, 0x486AFCC090D5F98CLL)) {
                LINE(19,f_test_regression("test_ereg"));
              }
            }
          }
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(24,f_test_end("test_ereg"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ereg.php line 27 */
bool f_test_ereg_enabled() {
  FUNCTION_INJECTION(test_ereg_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ereg.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
