
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php line 23 */
Variant f_test_ordered_functions_references(CVarRef v_base) {
  FUNCTION_INJECTION(test_ordered_functions_references);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_t;
  Variant v_z;
  Variant v_y;
  Variant v_x;
  Variant v_w;
  Variant v_v;
  Variant v_a;
  Variant v_b;
  Variant v_c;
  Variant v_d;
  PlusOperand v_e = 0;

  (v_t = v_base);
  LINE(25,f_test_start("test_ordered_functions_references"));
  (v_z = 42LL);
  (v_y = -1LL);
  (v_x = 42.670000000000002);
  (v_w = "xxx");
  setNull(v_v);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = LINE(32,f_tofr_1(ref(v_z), ref(v_y))));
        (v_b = LINE(33,f_tofr_1(ref(v_a), ref(v_x))));
        (v_c = LINE(34,f_tofr_1(ref(v_b), ref(v_w))));
        (v_d = LINE(35,f_tofr_1(ref(v_c), ref(v_v))));
        (v_e = LINE(36,(assignCallTemp(eo_0, ref(v_a)),assignCallTemp(eo_1, ref((assignCallTemp(eo_2, ref(v_b)),assignCallTemp(eo_3, ref((assignCallTemp(eo_4, ref(v_c)),assignCallTemp(eo_5, ref(f_tofr_1(ref(v_d), ref(v_a)))),f_tofr_1(eo_4, eo_5)))),f_tofr_1(eo_2, eo_3)))),f_tofr_1(eo_0, eo_1))));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(39,concat5(toString(v_a), toString(v_b), toString(v_c), toString(v_d), toString(v_e))), "4183.6783.6783.67333.01")) {
    LINE(40,f_test_regression("test_ordered_functions_references"));
  }
  return LINE(42,f_test_end("test_ordered_functions_references"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php line 19 */
PlusOperand f_tofr_1(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(tofr_1);
  return LINE(20,f_tofr_2(ref(v_a), ref(v_b)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php line 15 */
PlusOperand f_tofr_2(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(tofr_2);
  return LINE(16,f_tofr_3(ref(v_a), ref(v_b)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php line 11 */
PlusOperand f_tofr_3(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(tofr_3);
  return LINE(12,f_tofr_4(ref(v_a), ref(v_b)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php line 7 */
PlusOperand f_tofr_4(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(tofr_4);
  return LINE(8,f_tofr_5(ref(v_a), ref(v_b)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php line 3 */
PlusOperand f_tofr_5(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(tofr_5);
  return v_a + v_b;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php line 45 */
bool f_test_ordered_functions_references_enabled() {
  FUNCTION_INJECTION(test_ordered_functions_references_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
