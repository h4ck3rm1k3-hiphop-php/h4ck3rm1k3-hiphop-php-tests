
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php line 40 */
bool f_test_ordered_functions_enabled() {
  FUNCTION_INJECTION(test_ordered_functions_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php line 23 */
Variant f_test_ordered_functions(CVarRef v_base) {
  FUNCTION_INJECTION(test_ordered_functions);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_t;
  PlusOperand v_a = 0;
  PlusOperand v_b = 0;
  PlusOperand v_c = 0;
  PlusOperand v_d = 0;
  PlusOperand v_e = 0;

  (v_t = v_base);
  LINE(25,f_test_start("test_ordered_functions"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = LINE(27,f_tof_1(42LL, -1LL)));
        (v_b = LINE(28,f_tof_1(v_a, 42.670000000000002)));
        (v_c = LINE(29,f_tof_1(v_b, "xxx")));
        (v_d = LINE(30,f_tof_1(v_c, null)));
        (v_e = LINE(31,(assignCallTemp(eo_0, v_a),assignCallTemp(eo_1, (assignCallTemp(eo_2, v_b),assignCallTemp(eo_3, (assignCallTemp(eo_4, v_c),assignCallTemp(eo_5, f_tof_1(v_d, v_a)),f_tof_1(eo_4, eo_5))),f_tof_1(eo_2, eo_3))),f_tof_1(eo_0, eo_1))));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(34,concat5(toString(v_a), toString(v_b), toString(v_c), toString(v_d), toString(v_e))), "4183.6783.6783.67333.01")) {
    LINE(35,f_test_regression("test_ordered_functions"));
  }
  return LINE(37,f_test_end("test_ordered_functions"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php line 19 */
PlusOperand f_tof_1(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tof_1);
  return LINE(20,f_tof_2(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php line 15 */
PlusOperand f_tof_2(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tof_2);
  return LINE(16,f_tof_3(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php line 11 */
PlusOperand f_tof_3(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tof_3);
  return LINE(12,f_tof_4(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php line 7 */
PlusOperand f_tof_4(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tof_4);
  return LINE(8,f_tof_5(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php line 3 */
PlusOperand f_tof_5(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(tof_5);
  return v_a + v_b;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
