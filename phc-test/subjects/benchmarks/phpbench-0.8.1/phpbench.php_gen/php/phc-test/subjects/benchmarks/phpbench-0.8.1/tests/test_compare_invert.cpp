
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_invert.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_invert.php line 17 */
bool f_test_compare_invert_enabled() {
  FUNCTION_INJECTION(test_compare_invert_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_invert.php line 3 */
Variant f_test_compare_invert(CVarRef v_base) {
  FUNCTION_INJECTION(test_compare_invert);
  Array v_a;
  Variant v_t;

  (v_a = ScalarArrays::sa_[2]);
  (v_t = v_base);
  LINE(7,f_test_start("test_compare_invert"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        if (!(toBoolean(v_a))) {
          LINE(10,f_test_regression("test_compare_invert"));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(14,f_test_end("test_compare_invert"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_invert.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
