
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_scalar_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_scalar_assign.php line 3 */
Variant f_test_local_scalar_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_local_scalar_assign);
  Variant v_t;
  int64 v_a = 0;
  int64 v_b = 0;
  int64 v_c = 0;
  int64 v_d = 0;
  int64 v_e = 0;
  Primitive v_f = 0;
  Primitive v_g = 0;
  Primitive v_h = 0;
  int64 v_i = 0;
  int64 v_j = 0;
  int64 v_k = 0;
  int64 v_l = 0;
  int64 v_m = 0;
  Primitive v_n = 0;
  Primitive v_o = 0;
  Primitive v_p = 0;

  (v_t = v_base);
  LINE(5,f_test_start("test_local_scalar_assign"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = 1LL);
        (v_b = v_a);
        (v_c = v_a + v_b);
        (v_d = v_c - v_a);
        (v_e = v_a + v_c - v_d - v_b);
        (v_f = bitwise_or(v_a, v_c));
        (v_g = bitwise_and(v_b, v_d));
        (v_h = bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(v_a, v_b), v_c), v_d), v_e), v_f), v_g));
        (v_i = v_a);
        (v_j = v_b);
        (v_k = v_c);
        (v_l = v_d);
        (v_m = v_e);
        (v_n = v_f);
        (v_o = v_g);
        (v_p = v_h);
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(25,x_md5(concat_rev(concat6(toString(v_k), toString(v_l), toString(v_m), toString(v_n), toString(v_o), toString(v_p)), concat_rev(concat6(toString(v_e), toString(v_f), toString(v_g), toString(v_h), toString(v_i), toString(v_j)), concat4(toString(v_a), toString(v_b), toString(v_c), toString(v_d)))))), "efe1603e8bbb8403b6bfe7a43a39ade1")) {
    LINE(27,f_test_regression("test_local_scalar_assign"));
  }
  return LINE(29,f_test_end("test_local_scalar_assign"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_scalar_assign.php line 32 */
bool f_test_local_scalar_assign_enabled() {
  FUNCTION_INJECTION(test_local_scalar_assign_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_scalar_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
