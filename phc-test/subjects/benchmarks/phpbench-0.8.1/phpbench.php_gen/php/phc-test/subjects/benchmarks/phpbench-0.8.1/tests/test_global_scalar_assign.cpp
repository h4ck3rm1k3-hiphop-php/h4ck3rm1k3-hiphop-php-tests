
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_scalar_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_scalar_assign.php line 31 */
bool f_test_global_scalar_assign_enabled() {
  FUNCTION_INJECTION(test_global_scalar_assign_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_scalar_assign.php line 3 */
Variant f_test_global_scalar_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_global_scalar_assign);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  Variant &gv_b __attribute__((__unused__)) = g->GV(b);
  Variant &gv_c __attribute__((__unused__)) = g->GV(c);
  Variant &gv_d __attribute__((__unused__)) = g->GV(d);
  Variant &gv_e __attribute__((__unused__)) = g->GV(e);
  Variant &gv_f __attribute__((__unused__)) = g->GV(f);
  Variant &gv_g __attribute__((__unused__)) = g->GV(g);
  Variant &gv_h __attribute__((__unused__)) = g->GV(h);
  Variant &gv_i __attribute__((__unused__)) = g->GV(i);
  Variant &gv_j __attribute__((__unused__)) = g->GV(j);
  Variant &gv_k __attribute__((__unused__)) = g->GV(k);
  Variant &gv_l __attribute__((__unused__)) = g->GV(l);
  Variant &gv_m __attribute__((__unused__)) = g->GV(m);
  Variant &gv_n __attribute__((__unused__)) = g->GV(n);
  Variant &gv_o __attribute__((__unused__)) = g->GV(o);
  Variant &gv_p __attribute__((__unused__)) = g->GV(p);

  (v_t = v_base);
  LINE(5,f_test_start("test_global_scalar_assign"));
  {
  }
  {
  }
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (gv_a = 1LL);
        (gv_b = gv_a);
        (gv_c = gv_a + gv_b);
        (gv_d = gv_c - gv_a);
        (gv_e = gv_a + gv_c - gv_d - gv_b);
        (gv_f = bitwise_or(gv_a, gv_c));
        (gv_g = bitwise_and(gv_b, gv_d));
        (gv_h = bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(gv_a, gv_b), gv_c), gv_d), gv_e), gv_f), gv_g));
        (gv_i = gv_a);
        (gv_j = gv_b);
        (gv_k = gv_c);
        (gv_l = gv_d);
        (gv_m = gv_e);
        (gv_n = gv_f);
        (gv_o = gv_g);
        (gv_p = gv_h);
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(28,f_test_end("test_global_scalar_assign"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_scalar_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
