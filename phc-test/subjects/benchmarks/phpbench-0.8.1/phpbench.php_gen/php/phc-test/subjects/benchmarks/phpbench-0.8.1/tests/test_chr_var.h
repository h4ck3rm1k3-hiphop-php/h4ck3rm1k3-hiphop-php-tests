
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_chr_var_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_chr_var_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_var.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_test_chr_var(CVarRef v_base);
bool f_test_chr_var_enabled();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_chr_var_h__
