
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_arithmetic.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_array_operators.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_bitwise.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_casting.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_fixed.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_hardcoded.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_var.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_comment_loop.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_false.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_invert.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_strict.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_unstrict.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_constants.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_crc32.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while_break.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_empty_loop.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ereg.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_foreach.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_get_class.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_scalar_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_global_string_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_if_constant.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_increment.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_array.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_object.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_is_type.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_isset.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_line.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_array_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_boolean_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_float_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_hash_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_integer_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_object_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_scalar_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_string_assign.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_md5.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_microtime.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_mt_rand.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ord.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions_references.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_preg_match.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_rand.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_references.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_sha1.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_string_append.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_strlen.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_switch.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_time.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_unordered_functions.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_variable_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_while.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_CSV_NL = "\n";
const StaticString k_CSV_SEP = ",";
const int64 k_MIN_BASE = 50LL;
const int64 k_DEFAULT_BASE = 100000LL;
const StaticString k_PHPBENCH_VERSION = "0.8.1";

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 213 */
void f_show_summary(CVarRef v_base, Variant v_results, CVarRef v_csv_file) {
  FUNCTION_INJECTION(show_summary);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_total_time;
  Primitive v_test = 0;
  Variant v_time;
  Variant v_percentile_times;
  Numeric v_score = 0;

  (v_total_time = 0.0);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_results.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_time = iter3->second();
      v_test = iter3->first();
      {
        v_total_time += v_time;
      }
    }
  }
  if (not_more(v_total_time, 0.0)) {
    f_exit("Not enough iterations, please try with more.\n");
  }
  (v_percentile_times = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_results.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_time = iter6->second();
      v_test = iter6->first();
      {
        v_percentile_times.set(v_test, (divide(toDouble(v_time) * 100.0, v_total_time)));
      }
    }
  }
  (v_score = divide(toDouble(v_base) * 10.0, v_total_time));
  {
    echo(LINE(227,(assignCallTemp(eo_1, x_php_uname()),concat3("System     : ", eo_1, "\n"))));
  }
  {
    echo(LINE(230,(assignCallTemp(eo_1, x_phpversion()),concat3("PHP version: ", eo_1, "\n"))));
  }
  echo(concat(concat_rev(toString(LINE(238,x_round(v_score))), concat(concat_rev(toString(LINE(237,x_round(v_total_time))), concat(concat(concat(concat_rev(toString(LINE(235,x_count(v_results))), (assignCallTemp(eo_1, LINE(234,x_date("F j, Y, g:i a"))),concat3("PHPBench   : 0.8.1\nDate       : ", eo_1, "\nTests      : "))), "\nIterations : "), toString(v_base)), "\nTotal time : ")), " seconds\nScore      : ")), " (higher is better)\n"));
  if (!same(v_csv_file, false)) {
    LINE(241,f_export_csv(v_csv_file, ref(v_results), ref(v_percentile_times)));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 176 */
Variant f_csv_escape(CVarRef v_str) {
  FUNCTION_INJECTION(csv_escape);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  if (!same(LINE(177,x_strchr(toString(v_str), "," /* CSV_SEP */)), false)) {
    return LINE(178,(assignCallTemp(eo_1, toString(x_str_replace("\"", "'", v_str))),concat3("\"", eo_1, "\"")));
  }
  return v_str;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 62 */
void f_do_tests(CVarRef v_base, Variant v_tests_list, Variant v_results) {
  FUNCTION_INJECTION(do_tests);
  Variant v_test;

  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_tests_list.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_test = iter9->second();
      {
        v_results.set(v_test, (LINE(64,x_call_user_func(3, v_test, Array(ArrayInit(2).set(0, v_base).set(1, v_results).create())))));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 35 */
Variant f_test_end(CStrRef v_func) {
  FUNCTION_INJECTION(test_end);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_GLOBAL_TEST_FUNC __attribute__((__unused__)) = g->GV(GLOBAL_TEST_FUNC);
  Variant &gv_GLOBAL_TEST_START_TIME __attribute__((__unused__)) = g->GV(GLOBAL_TEST_START_TIME);
  Variant v_usec;
  Variant v_sec;
  PlusOperand v_now = 0;
  Numeric v_duration = 0;

  df_lambda_2(LINE(39,(assignCallTemp(eo_1, toString(x_microtime())),x_explode(" ", eo_1))), v_usec, v_sec);
  (v_now = v_usec + v_sec);
  if (!same(v_func, gv_GLOBAL_TEST_FUNC)) {
    LINE(43,x_trigger_error(concat4("Wrong func: [", v_func, "] vs ", toString(gv_GLOBAL_TEST_FUNC))));
    return false;
  }
  if (less(v_now, gv_GLOBAL_TEST_START_TIME)) {
    LINE(48,x_trigger_error(concat4("Wrong func: [", v_func, "] vs ", toString(gv_GLOBAL_TEST_FUNC))));
    return false;
  }
  (v_duration = v_now - gv_GLOBAL_TEST_START_TIME);
  echo(concat(LINE(52,x_sprintf(2, "%9.04f", Array(ArrayInit(1).set(0, v_duration).create()))), " seconds.\n"));
  return v_duration;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 24 */
void f_test_start(CStrRef v_func) {
  FUNCTION_INJECTION(test_start);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_GLOBAL_TEST_FUNC __attribute__((__unused__)) = g->GV(GLOBAL_TEST_FUNC);
  Variant &gv_GLOBAL_TEST_START_TIME __attribute__((__unused__)) = g->GV(GLOBAL_TEST_START_TIME);
  Variant v_usec;
  Variant v_sec;

  (gv_GLOBAL_TEST_FUNC = v_func);
  echo(concat(LINE(29,x_sprintf(2, "%34s", Array(ArrayInit(1).set(0, v_func).create()))), "\t"));
  LINE(30,x_flush());
  df_lambda_1(LINE(31,(assignCallTemp(eo_1, toString(x_microtime())),x_explode(" ", eo_1))), v_usec, v_sec);
  (gv_GLOBAL_TEST_START_TIME = v_usec + v_sec);
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 57 */
void f_test_regression(CStrRef v_func) {
  FUNCTION_INJECTION(test_regression);
  LINE(58,x_trigger_error(concat3("* REGRESSION * [", v_func, "]\n")));
  f_exit();
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 126 */
bool f_load_test(CVarRef v_tests_dir, Variant v_tests_list) {
  FUNCTION_INJECTION(load_test);
  Variant v_dir;
  Variant v_matches;
  Variant v_entry;
  Variant v_test_name;

  if (same(((v_dir = (silenceInc(), silenceDec(LINE(127,x_opendir(toString(v_tests_dir))))))), false)) {
    return false;
  }
  (v_matches = ScalarArrays::sa_[1]);
  LOOP_COUNTER(10);
  {
    while (!same(((v_entry = LINE(131,x_readdir(toObject(v_dir))))), false)) {
      LOOP_COUNTER_CHECK(10);
      {
        if (not_more(LINE(132,x_preg_match("/^(test_.+)[.]php$/i", toString(v_entry), ref(v_matches))), 0LL)) {
          continue;
        }
        (v_test_name = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
        echo(LINE(138,concat3("Test [", toString(v_test_name), "] ")));
        LINE(139,x_flush());
        if (!(LINE(140,x_function_exists(concat(toString(v_test_name), "_enabled"))))) {
          echo("INVALID !\n");
          continue;
        }
        if (!same(LINE(144,x_call_user_func(1, concat(toString(v_test_name), "_enabled"))), true)) {
          echo("disabled.\n");
          continue;
        }
        if (!(LINE(148,x_function_exists(toString(v_test_name))))) {
          echo("BROKEN !\n");
          continue;
        }
        LINE(152,x_array_push(2, ref(v_tests_list), v_test_name));
        echo("enabled.\n");
      }
    }
  }
  LINE(155,x_closedir(toObject(v_dir)));
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 245 */
void f_help() {
  FUNCTION_INJECTION(help);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_TESTS_DIRS __attribute__((__unused__)) = g->GV(TESTS_DIRS);
  Variant v_tests_dir;

  echo("\nPHPBench version 0.8.1\n\n-f <file name> : Output a summary as a CSV file.\n-h             : Help.\n-i <number>    : Number of iterations (default=100000).\n\nScripts are loaded from the following directories: \n");
  {
    LOOP_COUNTER(11);
    for (ArrayIterPtr iter13 = gv_TESTS_DIRS.begin(); !iter13->end(); iter13->next()) {
      LOOP_COUNTER_CHECK(11);
      v_tests_dir = iter13->second();
      {
        echo(LINE(256,concat3("  - ", toString(v_tests_dir), "\n")));
      }
    }
  }
  echo("\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 160 */
bool f_load_tests(Variant v_tests_dirs, Variant v_tests_list) {
  FUNCTION_INJECTION(load_tests);
  bool v_ret = false;
  Variant v_tests_dir;

  (v_ret = false);
  {
    LOOP_COUNTER(14);
    for (ArrayIterPtr iter16 = v_tests_dirs.begin(); !iter16->end(); iter16->next()) {
      LOOP_COUNTER_CHECK(14);
      v_tests_dir = iter16->second();
      {
        if (same(LINE(164,f_load_test(v_tests_dir, ref(v_tests_list))), true)) {
          (v_ret = true);
        }
      }
    }
  }
  if (not_more(LINE(168,x_count(v_tests_list)), 0LL)) {
    return false;
  }
  LINE(171,x_asort(ref(v_tests_list)));
  return v_ret;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php line 183 */
bool f_export_csv(CVarRef v_csv_file, Variant v_results, Variant v_percentile_times) {
  FUNCTION_INJECTION(export_csv);
  Variant eo_0;
  Variant eo_1;
  Variant v_fp;
  Primitive v_test = 0;
  Variant v_time;

  if (empty(v_csv_file)) {
    return true;
  }
  if (same(((v_fp = LINE(187,x_fopen(toString(v_csv_file), "w")))), false)) {
    return false;
  }
  if (same(LINE(191,(assignCallTemp(eo_0, toObject(v_fp)),assignCallTemp(eo_1, concat(concat_rev(toString(f_csv_escape("Percentile")), concat(concat_rev(toString(LINE(190,f_csv_escape("Time"))), concat(toString(f_csv_escape("Test")), "," /* CSV_SEP */)), "," /* CSV_SEP */)), "\n" /* CSV_NL */)),x_fputs(eo_0, eo_1))), false)) {
    (silenceInc(), silenceDec(LINE(193,x_fclose(toObject(v_fp)))));
    LINE(194,x_unlink(toString(v_csv_file)));
    return false;
  }
  {
    LOOP_COUNTER(17);
    for (ArrayIterPtr iter19 = v_results.begin(); !iter19->end(); iter19->next()) {
      LOOP_COUNTER_CHECK(17);
      v_time = iter19->second();
      v_test = iter19->first();
      {
        if (same(LINE(201,(assignCallTemp(eo_0, toObject(v_fp)),assignCallTemp(eo_1, concat(concat_rev(toString(LINE(200,f_csv_escape(x_sprintf(2, "%.03f", Array(ArrayInit(1).set(0, v_percentile_times.rvalAt(v_test)).create()))))), concat(concat_rev(toString(LINE(199,f_csv_escape(x_sprintf(2, "%.04f", Array(ArrayInit(1).set(0, v_time).create()))))), concat(toString(LINE(198,f_csv_escape(v_test))), "," /* CSV_SEP */)), "," /* CSV_SEP */)), "\n" /* CSV_NL */)),x_fputs(eo_0, eo_1))), false)) {
          (silenceInc(), silenceDec(LINE(202,x_fclose(toObject(v_fp)))));
          LINE(203,x_unlink(toString(v_csv_file)));
          return false;
        }
      }
    }
  }
  if (same(LINE(207,x_fclose(toObject(v_fp))), false)) {
    return false;
  }
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_TESTS_DIRS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TESTS_DIRS") : g->GV(TESTS_DIRS);
  Variant &v_base __attribute__((__unused__)) = (variables != gVariables) ? variables->get("base") : g->GV(base);
  Variant &v_csv_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("csv_file") : g->GV(csv_file);
  Variant &v_options __attribute__((__unused__)) = (variables != gVariables) ? variables->get("options") : g->GV(options);
  Variant &v_tests_list __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests_list") : g->GV(tests_list);
  Variant &v_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("results") : g->GV(results);

  LINE(4,x_ignore_user_abort(true));
  LINE(5,x_error_reporting(toInt32(6143LL) /* E_ALL */));
  LINE(6,x_set_time_limit(toInt32(0LL)));
  LINE(7,x_ob_implicit_flush(toBoolean(1LL)));
  ;
  ;
  ;
  ;
  ;
  (v_TESTS_DIRS = ScalarArrays::sa_[4]);
  LINE(69,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_arithmetic_php(true, variables));
  LINE(70,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php(true, variables));
  LINE(71,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_bitwise_php(true, variables));
  LINE(72,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_casting_php(true, variables));
  LINE(73,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_fixed_php(true, variables));
  LINE(74,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_hardcoded_php(true, variables));
  LINE(75,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php(true, variables));
  LINE(76,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_comment_loop_php(true, variables));
  LINE(77,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_php(true, variables));
  LINE(78,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_false_php(true, variables));
  LINE(79,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_invert_php(true, variables));
  LINE(80,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_strict_php(true, variables));
  LINE(81,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php(true, variables));
  LINE(82,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_constants_php(true, variables));
  LINE(83,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_crc32_php(true, variables));
  LINE(84,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_php(true, variables));
  LINE(85,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php(true, variables));
  LINE(86,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_php(true, variables));
  LINE(87,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_empty_loop_php(true, variables));
  LINE(88,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ereg_php(true, variables));
  LINE(89,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_foreach_php(true, variables));
  LINE(90,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_get_class_php(true, variables));
  LINE(91,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_scalar_assign_php(true, variables));
  LINE(92,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_global_string_assign_php(true, variables));
  LINE(93,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_if_constant_php(true, variables));
  LINE(94,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_increment_php(true, variables));
  LINE(95,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_array_php(true, variables));
  LINE(96,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_object_php(true, variables));
  LINE(97,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_is_type_php(true, variables));
  LINE(98,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_isset_php(true, variables));
  LINE(99,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php(true, variables));
  LINE(100,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php(true, variables));
  LINE(101,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_boolean_assign_php(true, variables));
  LINE(102,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_float_assign_php(true, variables));
  LINE(103,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_hash_assign_php(true, variables));
  LINE(104,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_integer_assign_php(true, variables));
  LINE(105,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_object_assign_php(true, variables));
  LINE(106,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_scalar_assign_php(true, variables));
  LINE(107,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_string_assign_php(true, variables));
  LINE(108,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php(true, variables));
  LINE(109,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_microtime_php(true, variables));
  LINE(110,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_mt_rand_php(true, variables));
  LINE(111,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php(true, variables));
  LINE(112,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php(true, variables));
  LINE(113,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_references_php(true, variables));
  LINE(114,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_preg_match_php(true, variables));
  LINE(115,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_rand_php(true, variables));
  LINE(116,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_references_php(true, variables));
  LINE(117,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_sha1_php(true, variables));
  LINE(118,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_string_append_php(true, variables));
  LINE(119,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_strlen_php(true, variables));
  LINE(120,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php(true, variables));
  LINE(121,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_time_php(true, variables));
  LINE(122,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_unordered_functions_php(true, variables));
  LINE(123,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php(true, variables));
  LINE(124,pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_while_php(true, variables));
  (v_base = 100000LL /* DEFAULT_BASE */);
  (v_csv_file = false);
  (v_options = LINE(264,x_getopt("f:hi:")));
  if (isset(v_options, "h", 0x695875365480A8F0LL)) {
    LINE(266,f_help());
    f_exit();
  }
  if (!(empty(v_options, "f", 0x6BB4A0689FBAD42ELL))) {
    (v_csv_file = v_options.rvalAt("f", 0x6BB4A0689FBAD42ELL));
    if (not_more(LINE(271,x_preg_match("/[.]csv$/i", toString(v_csv_file))), 0LL)) {
      concat_assign(v_csv_file, ".csv");
    }
  }
  if (!(empty(v_options, "i", 0x0EB22EDA95766E98LL)) && LINE(275,x_is_numeric(v_options.rvalAt("i", 0x0EB22EDA95766E98LL)))) {
    (v_base = v_options.rvalAt("i", 0x0EB22EDA95766E98LL));
  }
  if (less(v_base, 50LL /* MIN_BASE */)) {
    f_exit("Min iterations = 50\n");
  }
  if (empty(v_options)) {
    LINE(282,f_help());
  }
  echo(LINE(284,concat3("Starting the benchmark with ", toString(v_base), " iterations.\n\n")));
  (v_tests_list = ScalarArrays::sa_[1]);
  (v_results = ScalarArrays::sa_[1]);
  if (same(LINE(287,f_load_tests(ref(v_TESTS_DIRS), ref(v_tests_list))), false)) {
    f_exit("Unable to load tests");
  }
  echo("\n");
  LINE(291,f_do_tests(v_base, ref(v_tests_list), ref(v_results)));
  echo("\n");
  LINE(293,f_show_summary(v_base, ref(v_results), v_csv_file));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
