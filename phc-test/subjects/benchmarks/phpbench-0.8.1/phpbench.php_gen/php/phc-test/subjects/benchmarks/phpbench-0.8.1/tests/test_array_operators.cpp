
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_array_operators.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_array_operators.php line 3 */
Variant f_test_array_operators(CVarRef v_base) {
  FUNCTION_INJECTION(test_array_operators);
  Array v_a;
  Array v_b;
  Array v_c;
  Variant v_t;
  PlusOperand v_d = 0;
  bool v_e = false;
  bool v_f = false;
  bool v_g = false;
  bool v_h = false;

  (v_a = ScalarArrays::sa_[6]);
  (v_b = ScalarArrays::sa_[7]);
  (v_c = ScalarArrays::sa_[8]);
  (v_t = v_base);
  LINE(9,f_test_start("test_array_operators"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_d = (Variant)(v_a) + (Variant)(v_b) + (Variant)(v_c));
        (v_e = (equal(v_a, v_b)));
        (v_f = (same(v_a, v_b)));
        (v_g = (!equal(v_a, v_b)));
        (v_h = (!same(v_a, v_b)));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(concat(concat(concat(concat(concat_rev(toString(LINE(18,x_count(v_d))), concat_rev(toString(x_count(v_c)), concat_rev(toString(x_count(v_b)), toString(x_count(v_a))))), toString(v_e)), toString(v_f)), toString(v_g)), toString(v_h)), "333311")) {
    LINE(20,f_test_regression("test_array_operators"));
  }
  return LINE(22,f_test_end("test_array_operators"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_array_operators.php line 25 */
bool f_test_array_operators_enabled() {
  FUNCTION_INJECTION(test_array_operators_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_array_operators.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_array_operators_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
