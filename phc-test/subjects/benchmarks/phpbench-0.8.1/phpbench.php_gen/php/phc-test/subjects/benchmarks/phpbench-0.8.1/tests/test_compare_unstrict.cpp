
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_unstrict.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_unstrict.php line 3 */
Variant f_test_compare_unstrict(CVarRef v_base) {
  FUNCTION_INJECTION(test_compare_unstrict);
  int64 v_a = 0;
  String v_b;
  Variant v_t;
  bool v_c = false;
  bool v_d = false;

  (v_a = 42LL);
  (v_b = "69");
  (v_t = v_base);
  LINE(8,f_test_start("test_compare_unstrict"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_c = (equal(v_a, v_b)));
        (v_d = (!equal(v_a, v_b)));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(14,concat3(toString(v_c), " ", toString(v_d))), " 1")) {
    LINE(15,f_test_regression("test_compare_unstrict"));
  }
  return LINE(17,f_test_end("test_compare_unstrict"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_unstrict.php line 20 */
bool f_test_compare_unstrict_enabled() {
  FUNCTION_INJECTION(test_compare_unstrict_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_compare_unstrict.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_compare_unstrict_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
