
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_line.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_line.php line 19 */
bool f_test_line_enabled() {
  FUNCTION_INJECTION(test_line_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_line.php line 3 */
Variant f_test_line(CVarRef v_base) {
  FUNCTION_INJECTION(test_line);
  Variant v_t;
  int64 v_a = 0;

  (v_t = v_base);
  LINE(5,f_test_start("test_line"));
  (v_a = 0LL);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        v_a += toInt64(8);
        v_a -= toInt64(9);
        v_a++;
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(v_a, 0LL)) {
    LINE(14,f_test_regression("test_line"));
  }
  return LINE(16,f_test_end("test_line"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_line.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_line_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
