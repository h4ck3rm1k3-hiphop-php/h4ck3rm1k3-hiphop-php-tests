
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_variable_variables.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_variable_variables.php line 21 */
bool f_test_variable_variables_enabled() {
  FUNCTION_INJECTION(test_variable_variables_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_variable_variables.php line 3 */
Variant f_test_variable_variables(Variant v_base) {
  FUNCTION_INJECTION(test_variable_variables);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;
  Variant v_a;
  Variant v_b;
  Variant v_c;
  Variant v_d;
  Variant v_e;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_base; Variant &v_t; Variant &v_a; Variant &v_b; Variant &v_c; Variant &v_d; Variant &v_e;
    VariableTable(Variant &r_base, Variant &r_t, Variant &r_a, Variant &r_b, Variant &r_c, Variant &r_d, Variant &r_e) : v_base(r_base), v_t(r_t), v_a(r_a), v_b(r_b), v_c(r_c), v_d(r_d), v_e(r_e) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 15) {
        case 0:
          HASH_RETURN(0x32C769EE5C5509B0LL, v_c,
                      c);
          break;
        case 4:
          HASH_RETURN(0x7A452383AA9BF7C4LL, v_d,
                      d);
          break;
        case 5:
          HASH_RETURN(0x08FBB133F8576BD5LL, v_b,
                      b);
          break;
        case 7:
          HASH_RETURN(0x56FD8ECCBDFDD7A7LL, v_base,
                      base);
          break;
        case 10:
          HASH_RETURN(0x4292CEE227B9150ALL, v_a,
                      a);
          break;
        case 11:
          HASH_RETURN(0x61B161496B7EA7EBLL, v_e,
                      e);
          break;
        case 12:
          HASH_RETURN(0x11C9D2391242AEBCLL, v_t,
                      t);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_base, v_t, v_a, v_b, v_c, v_d, v_e);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_t = v_base);
  LINE(5,f_test_start("test_variable_variables"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = "b");
        (variables->get(toString(v_a)) = "c");
        (variables->get(toString(v_b)) = "d");
        (variables->get(toString(v_c)) = "e");
        (variables->get(toString(v_d)) = variables->get(toString(v_c)));
        (variables->get(toString(variables->get(toString(v_d)))) = toString("$") + toString(v_b));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(15,concat5(toString(v_a), toString(v_b), toString(v_c), toString(v_d), toString(v_e))), "bcde$c")) {
    LINE(16,f_test_regression("test_variable_variables"));
  }
  return LINE(18,f_test_end("test_variable_variables"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_variable_variables.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_variable_variables_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
