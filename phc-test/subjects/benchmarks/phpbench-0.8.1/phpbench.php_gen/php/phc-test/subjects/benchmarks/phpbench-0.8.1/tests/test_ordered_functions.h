
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_ordered_functions_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_ordered_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ordered_functions.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_test_ordered_functions_enabled();
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ordered_functions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_test_ordered_functions(CVarRef v_base);
PlusOperand f_tof_1(CVarRef v_a, CVarRef v_b);
PlusOperand f_tof_2(CVarRef v_a, CVarRef v_b);
PlusOperand f_tof_3(CVarRef v_a, CVarRef v_b);
PlusOperand f_tof_4(CVarRef v_a, CVarRef v_b);
PlusOperand f_tof_5(CVarRef v_a, CVarRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_ordered_functions_h__
