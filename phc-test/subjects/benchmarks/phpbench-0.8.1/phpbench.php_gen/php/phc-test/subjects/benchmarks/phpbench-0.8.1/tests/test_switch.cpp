
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_switch.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_switch.php line 46 */
bool f_test_switch_enabled() {
  FUNCTION_INJECTION(test_switch_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_switch.php line 3 */
Variant f_test_switch(CVarRef v_base) {
  FUNCTION_INJECTION(test_switch);
  Variant v_t;
  Variant v_a;

  (v_t = v_base);
  LINE(5,f_test_start("test_switch"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = 1LL);
        {
          LOOP_COUNTER(2);
          do {
            LOOP_COUNTER_CHECK(2);
            {
              {
                Variant tmp4 = (v_a);
                int tmp5 = -1;
                if (equal(tmp4, (1LL))) {
                  tmp5 = 0;
                } else if (equal(tmp4, (2LL))) {
                  tmp5 = 1;
                } else if (equal(tmp4, (3LL))) {
                  tmp5 = 2;
                } else if (equal(tmp4, (4LL))) {
                  tmp5 = 3;
                } else if (equal(tmp4, ("five"))) {
                  tmp5 = 4;
                } else if (equal(tmp4, ("six"))) {
                  tmp5 = 5;
                } else if (equal(tmp4, ("seven"))) {
                  tmp5 = 6;
                } else if (equal(tmp4, ("height"))) {
                  tmp5 = 7;
                } else if (equal(tmp4, ("not_reached"))) {
                  tmp5 = 8;
                } else if (equal(tmp4, (42LL))) {
                  tmp5 = 9;
                } else if (true) {
                  tmp5 = 10;
                }
                switch (tmp5) {
                case 0:
                  {
                    (v_a = 2LL);
                    goto break3;
                  }
                case 1:
                  {
                    (v_a = 3LL);
                    goto break3;
                  }
                case 2:
                  {
                    (v_a = 4LL);
                    goto break3;
                  }
                case 3:
                  {
                    (v_a = "five");
                    goto break3;
                  }
                case 4:
                  {
                    (v_a = "six");
                    goto break3;
                  }
                case 5:
                  {
                    (v_a = "seven");
                    goto break3;
                  }
                case 6:
                  {
                    (v_a = "height");
                    goto break3;
                  }
                case 7:
                  {
                    (v_a = 9LL);
                    goto break3;
                  }
                case 8:
                  {
                  }
                case 9:
                  {
                    goto continue3;
                  }
                case 10:
                  {
                    (v_a = "end");
                  }
                }
                continue3:;
                break3:;
              }
            }
          } while (!same(v_a, "end"));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  return LINE(43,f_test_end("test_switch"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_switch.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_switch_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
