
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ord.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ord.php line 3 */
Variant f_test_ord(CVarRef v_base) {
  FUNCTION_INJECTION(test_ord);
  String v_a;
  int v_a_len = 0;
  int64 v_t = 0;
  int64 v_b = 0;
  int v_c = 0;

  (v_a = LINE(4,x_str_repeat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP", toInt32(10LL))));
  (v_a_len = LINE(5,x_strlen(v_a)));
  (v_t = toInt64(LINE(7,x_round(divide(v_base, 10LL)))));
  LINE(8,f_test_start("test_ord"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_b = 0LL);
        (v_c = v_a_len);
        {
          LOOP_COUNTER(2);
          do {
            LOOP_COUNTER_CHECK(2);
            {
              v_c--;
              v_b += LINE(14,x_ord(v_a.rvalAt(v_c)));
              v_b += LINE(15,x_ord("A")) - 65LL;
              v_b += LINE(16,x_ord("0")) - 48LL;
            }
          } while (!same(v_c, 0LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(v_b, 40070LL)) {
    LINE(21,f_test_regression("test_ord"));
  }
  return LINE(23,f_test_end("test_ord"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ord.php line 26 */
bool f_test_ord_enabled() {
  FUNCTION_INJECTION(test_ord_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_ord.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_ord_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
