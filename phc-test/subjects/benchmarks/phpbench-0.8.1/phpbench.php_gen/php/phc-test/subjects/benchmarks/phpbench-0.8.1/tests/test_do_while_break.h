
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_do_while_break_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_do_while_break_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_do_while_break.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_test_do_while_break(CVarRef v_base);
bool f_test_do_while_break_enabled();
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_do_while_break_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_do_while_break_h__
