
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_var.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_var.php line 3 */
Variant f_test_chr_var(CVarRef v_base) {
  FUNCTION_INJECTION(test_chr_var);
  int64 v_t = 0;
  String v_b;
  int64 v_c = 0;

  (v_t = toInt64(LINE(4,x_round(divide(v_base, 10LL)))));
  LINE(5,f_test_start("test_chr_var"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_b = "");
        (v_c = 256LL);
        {
          LOOP_COUNTER(2);
          do {
            LOOP_COUNTER_CHECK(2);
            {
              v_c--;
              concat_assign(v_b, LINE(11,x_chr(v_c)));
            }
          } while (!same(v_c, 0LL));
        }
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(15,x_md5(v_b)), "ec6df70f2569891eae50321a9179eb82")) {
    LINE(16,f_test_regression("test_chr_var"));
  }
  return LINE(18,f_test_end("test_chr_var"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_var.php line 21 */
bool f_test_chr_var_enabled() {
  FUNCTION_INJECTION(test_chr_var_enabled);
  return true;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_chr_var.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_chr_var_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
