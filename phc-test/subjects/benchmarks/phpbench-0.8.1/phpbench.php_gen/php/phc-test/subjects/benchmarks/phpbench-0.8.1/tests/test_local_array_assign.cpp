
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_array_assign.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_array_assign.php line 23 */
bool f_test_local_array_assign_enabled() {
  FUNCTION_INJECTION(test_local_array_assign_enabled);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_array_assign.php line 3 */
Variant f_test_local_array_assign(CVarRef v_base) {
  FUNCTION_INJECTION(test_local_array_assign);
  Variant v_t;
  Array v_a;
  Array v_b;
  Array v_c;
  Array v_d;
  Array v_e;
  Array v_f;
  Array v_g;
  Array v_h;

  (v_t = v_base);
  LINE(5,f_test_start("test_local_array_assign"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_a = ScalarArrays::sa_[1]);
        (v_b = ScalarArrays::sa_[5]);
        (v_c = Array(ArrayInit(2).set(0, v_a).set(1, v_b).create()));
        (v_d = Array(ArrayInit(2).set(0, v_b).set(1, v_a).create()));
        (v_e = Array(ArrayInit(2).set(0, v_c).set(1, v_d).create()));
        (v_f = Array(ArrayInit(5).set(0, v_a).set(1, v_b).set(2, v_c).set(3, v_d).set(4, v_e).create()));
        (v_g = Array(ArrayInit(12).set(0, 1LL).set(1, v_a).set(2, 3LL).set(3, v_b).set(4, 5LL).set(5, v_c).set(6, 7LL).set(7, v_d).set(8, 9LL).set(9, v_e).set(10, 11LL).set(11, v_f).create()));
        (v_h = Array(ArrayInit(7).set(0, v_a).set(1, v_b).set(2, v_c).set(3, v_d).set(4, v_e).set(5, v_f).set(6, v_g).create()));
      }
    } while (!same(--v_t, 0LL));
  }
  if (!same(LINE(17,x_count(v_h)), 7LL)) {
    LINE(18,f_test_regression("test_local_array_assign"));
  }
  return LINE(20,f_test_end("test_local_array_assign"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_local_array_assign.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_local_array_assign_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
