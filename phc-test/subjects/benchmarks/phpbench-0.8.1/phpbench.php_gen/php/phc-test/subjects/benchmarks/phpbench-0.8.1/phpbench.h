
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_phpbench_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_phpbench_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/phpbench.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_show_summary(CVarRef v_base, Variant v_results, CVarRef v_csv_file);
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$phpbench_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_csv_escape(CVarRef v_str);
void f_do_tests(CVarRef v_base, Variant v_tests_list, Variant v_results);
Variant f_test_end(CStrRef v_func);
void f_test_start(CStrRef v_func);
void f_test_regression(CStrRef v_func);
bool f_load_test(CVarRef v_tests_dir, Variant v_tests_list);
void f_help();
bool f_load_tests(Variant v_tests_dirs, Variant v_tests_list);
bool f_export_csv(CVarRef v_csv_file, Variant v_results, Variant v_percentile_times);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_phpbench_h__
