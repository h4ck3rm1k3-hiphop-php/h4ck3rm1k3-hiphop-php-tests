
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_md5_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_md5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/phpbench-0.8.1/tests/test_md5.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_test_md5_enabled();
Variant pm_php$phc_test$subjects$benchmarks$phpbench_0_8_1$tests$test_md5_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_test_md5(CVarRef v_base);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_phpbench_0_8_1_tests_test_md5_h__
