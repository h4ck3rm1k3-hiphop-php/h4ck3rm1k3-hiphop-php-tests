
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_nophp_h__
