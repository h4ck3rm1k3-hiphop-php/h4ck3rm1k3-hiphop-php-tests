
#ifndef __GENERATED_cls_workertask_h__
#define __GENERATED_cls_workertask_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 506 */
class c_workertask : virtual public ObjectData {
  BEGIN_CLASS_MAP(workertask)
  END_CLASS_MAP(workertask)
  DECLARE_CLASS(workertask, WorkerTask, ObjectData)
  void init();
  public: void t___construct(Variant v_scheduler, Variant v_v1, Variant v_v2);
  public: ObjectData *create(Variant v_scheduler, Variant v_v1, Variant v_v2);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_run(Variant v_packet);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_workertask_h__
