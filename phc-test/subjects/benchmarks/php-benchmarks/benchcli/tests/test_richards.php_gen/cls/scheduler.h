
#ifndef __GENERATED_cls_scheduler_h__
#define __GENERATED_cls_scheduler_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 133 */
class c_scheduler : virtual public ObjectData {
  BEGIN_CLASS_MAP(scheduler)
  END_CLASS_MAP(scheduler)
  DECLARE_CLASS(scheduler, Scheduler, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addidletask(int64 v_id, int64 v_priority, CVarRef v_queue, int64 v_count);
  public: void t_addworkertask(int64 v_id, int64 v_priority, p_packet v_queue);
  public: void t_addhandlertask(int64 v_id, int64 v_priority, p_packet v_queue);
  public: void t_adddevicetask(int64 v_id, int64 v_priority, CVarRef v_queue);
  public: void t_addrunningtask(int64 v_id, int64 v_priority, CVarRef v_queue, p_idletask v_task);
  public: void t_addtask(int64 v_id, int64 v_priority, CVarRef v_queue, CVarRef v_task);
  public: void t_schedule();
  public: Variant t_release(CVarRef v_id);
  public: Variant t_holdcurrent();
  public: Variant t_suspendcurrent();
  public: Variant t_queue(Variant v_packet);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_scheduler_h__
