
#ifndef __GENERATED_cls_idletask_h__
#define __GENERATED_cls_idletask_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 430 */
class c_idletask : virtual public ObjectData {
  BEGIN_CLASS_MAP(idletask)
  END_CLASS_MAP(idletask)
  DECLARE_CLASS(idletask, IdleTask, ObjectData)
  void init();
  public: void t___construct(Variant v_scheduler, Variant v_v1, Variant v_count);
  public: ObjectData *create(Variant v_scheduler, Variant v_v1, Variant v_count);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_run(CVarRef v_packet);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_idletask_h__
