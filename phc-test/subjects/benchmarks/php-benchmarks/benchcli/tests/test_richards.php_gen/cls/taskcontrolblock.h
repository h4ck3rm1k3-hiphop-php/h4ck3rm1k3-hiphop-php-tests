
#ifndef __GENERATED_cls_taskcontrolblock_h__
#define __GENERATED_cls_taskcontrolblock_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 320 */
class c_taskcontrolblock : virtual public ObjectData {
  BEGIN_CLASS_MAP(taskcontrolblock)
  END_CLASS_MAP(taskcontrolblock)
  DECLARE_CLASS(taskcontrolblock, TaskControlBlock, ObjectData)
  void init();
  public: void t___construct(Variant v_link, Variant v_id, Variant v_priority, Variant v_queue, Variant v_task);
  public: ObjectData *create(Variant v_link, Variant v_id, Variant v_priority, Variant v_queue, Variant v_task);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_setrunning();
  public: void t_markasnotheld();
  public: void t_markasheld();
  public: bool t_isheldorsuspended();
  public: void t_markassuspended();
  public: void t_markasrunnable();
  public: Variant t_run();
  public: Variant t_checkpriorityadd(CVarRef v_task, CVarRef v_packet);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_taskcontrolblock_h__
