
#ifndef __GENERATED_cls_packet_h__
#define __GENERATED_cls_packet_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 620 */
class c_packet : virtual public ObjectData {
  BEGIN_CLASS_MAP(packet)
  END_CLASS_MAP(packet)
  DECLARE_CLASS(packet, Packet, ObjectData)
  void init();
  public: void t___construct(Variant v_link, Variant v_id, Variant v_kind);
  public: ObjectData *create(Variant v_link, Variant v_id, Variant v_kind);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_addto(CVarRef v_queue);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_packet_h__
