
#ifndef __GENERATED_cls_devicetask_h__
#define __GENERATED_cls_devicetask_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 469 */
class c_devicetask : virtual public ObjectData {
  BEGIN_CLASS_MAP(devicetask)
  END_CLASS_MAP(devicetask)
  DECLARE_CLASS(devicetask, DeviceTask, ObjectData)
  void init();
  public: void t___construct(Variant v_scheduler);
  public: ObjectData *create(Variant v_scheduler);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_run(CVarRef v_packet);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_devicetask_h__
