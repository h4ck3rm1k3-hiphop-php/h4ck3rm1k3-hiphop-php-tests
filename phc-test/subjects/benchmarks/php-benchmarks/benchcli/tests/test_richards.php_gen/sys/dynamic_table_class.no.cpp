
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_workertask(CArrRef params, bool init = true);
Variant cw_workertask$os_get(const char *s);
Variant &cw_workertask$os_lval(const char *s);
Variant cw_workertask$os_constant(const char *s);
Variant cw_workertask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_idletask(CArrRef params, bool init = true);
Variant cw_idletask$os_get(const char *s);
Variant &cw_idletask$os_lval(const char *s);
Variant cw_idletask$os_constant(const char *s);
Variant cw_idletask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_scheduler(CArrRef params, bool init = true);
Variant cw_scheduler$os_get(const char *s);
Variant &cw_scheduler$os_lval(const char *s);
Variant cw_scheduler$os_constant(const char *s);
Variant cw_scheduler$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_taskcontrolblock(CArrRef params, bool init = true);
Variant cw_taskcontrolblock$os_get(const char *s);
Variant &cw_taskcontrolblock$os_lval(const char *s);
Variant cw_taskcontrolblock$os_constant(const char *s);
Variant cw_taskcontrolblock$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_devicetask(CArrRef params, bool init = true);
Variant cw_devicetask$os_get(const char *s);
Variant &cw_devicetask$os_lval(const char *s);
Variant cw_devicetask$os_constant(const char *s);
Variant cw_devicetask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_handlertask(CArrRef params, bool init = true);
Variant cw_handlertask$os_get(const char *s);
Variant &cw_handlertask$os_lval(const char *s);
Variant cw_handlertask$os_constant(const char *s);
Variant cw_handlertask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_packet(CArrRef params, bool init = true);
Variant cw_packet$os_get(const char *s);
Variant &cw_packet$os_lval(const char *s);
Variant cw_packet$os_constant(const char *s);
Variant cw_packet$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_CREATE_OBJECT(0x412A3ED2C4B80190LL, devicetask);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x45375AE6D6CD48D2LL, workertask);
      HASH_CREATE_OBJECT(0x162B11212028C102LL, taskcontrolblock);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x63D7FF2C7A961EE5LL, idletask);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x18A0AAE9BC2A9E67LL, handlertask);
      break;
    case 8:
      HASH_CREATE_OBJECT(0x44C973F13F110748LL, packet);
      break;
    case 13:
      HASH_CREATE_OBJECT(0x1E5F08CCBA9097DDLL, scheduler);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x412A3ED2C4B80190LL, devicetask);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x45375AE6D6CD48D2LL, workertask);
      HASH_INVOKE_STATIC_METHOD(0x162B11212028C102LL, taskcontrolblock);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x63D7FF2C7A961EE5LL, idletask);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x18A0AAE9BC2A9E67LL, handlertask);
      break;
    case 8:
      HASH_INVOKE_STATIC_METHOD(0x44C973F13F110748LL, packet);
      break;
    case 13:
      HASH_INVOKE_STATIC_METHOD(0x1E5F08CCBA9097DDLL, scheduler);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x412A3ED2C4B80190LL, devicetask);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x45375AE6D6CD48D2LL, workertask);
      HASH_GET_STATIC_PROPERTY(0x162B11212028C102LL, taskcontrolblock);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x63D7FF2C7A961EE5LL, idletask);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x18A0AAE9BC2A9E67LL, handlertask);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY(0x44C973F13F110748LL, packet);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY(0x1E5F08CCBA9097DDLL, scheduler);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x412A3ED2C4B80190LL, devicetask);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x45375AE6D6CD48D2LL, workertask);
      HASH_GET_STATIC_PROPERTY_LV(0x162B11212028C102LL, taskcontrolblock);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x63D7FF2C7A961EE5LL, idletask);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x18A0AAE9BC2A9E67LL, handlertask);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY_LV(0x44C973F13F110748LL, packet);
      break;
    case 13:
      HASH_GET_STATIC_PROPERTY_LV(0x1E5F08CCBA9097DDLL, scheduler);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x412A3ED2C4B80190LL, devicetask);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x45375AE6D6CD48D2LL, workertask);
      HASH_GET_CLASS_CONSTANT(0x162B11212028C102LL, taskcontrolblock);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x63D7FF2C7A961EE5LL, idletask);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x18A0AAE9BC2A9E67LL, handlertask);
      break;
    case 8:
      HASH_GET_CLASS_CONSTANT(0x44C973F13F110748LL, packet);
      break;
    case 13:
      HASH_GET_CLASS_CONSTANT(0x1E5F08CCBA9097DDLL, scheduler);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
