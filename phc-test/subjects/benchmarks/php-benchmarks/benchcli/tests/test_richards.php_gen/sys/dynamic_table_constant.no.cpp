
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_ID_IDLE;
extern const int64 k_ID_DEVICE_A;
extern const int64 k_ID_DEVICE_B;
extern const int64 k_STATE_RUNNING;
extern const int64 k_STATE_RUNNABLE;
extern const int64 k_NUMBER_OF_IDS;
extern const int64 k_EXPECTED_QUEUE_COUNT;
extern const int64 k_ID_HANDLER_A;
extern const int64 k_ID_HANDLER_B;
extern const int64 k_EXPECTED_HOLD_COUNT;
extern const int64 k_DATA_SIZE;
extern const int64 k_COUNT;
extern const int64 k_STATE_NOT_HELD;
extern const int64 k_KIND_DEVICE;
extern const int64 k_KIND_WORK;
extern const int64 k_STATE_HELD;
extern const int64 k_STATE_SUSPENDED;
extern const int64 k_ID_WORKER;
extern const int64 k_STATE_SUSPENDED_RUNNABLE;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 3:
      HASH_RETURN(0x0BC74DB558B71FC3LL, k_STATE_RUNNABLE, STATE_RUNNABLE);
      break;
    case 7:
      HASH_RETURN(0x15B4377038745607LL, k_ID_DEVICE_A, ID_DEVICE_A);
      break;
    case 11:
      HASH_RETURN(0x65CF7C76625F85CBLL, k_ID_HANDLER_A, ID_HANDLER_A);
      break;
    case 15:
      HASH_RETURN(0x7BF117C69218084FLL, k_STATE_RUNNING, STATE_RUNNING);
      break;
    case 16:
      HASH_RETURN(0x5CEFA5A265104D10LL, k_COUNT, COUNT);
      HASH_RETURN(0x5E6F25D98724BC10LL, k_DATA_SIZE, DATA_SIZE);
      break;
    case 19:
      HASH_RETURN(0x3F197A4F6681FB53LL, k_ID_WORKER, ID_WORKER);
      break;
    case 22:
      HASH_RETURN(0x00CD1045284B7D56LL, k_NUMBER_OF_IDS, NUMBER_OF_IDS);
      break;
    case 26:
      HASH_RETURN(0x64F171C00B40F71ALL, k_ID_HANDLER_B, ID_HANDLER_B);
      break;
    case 28:
      HASH_RETURN(0x2C97B21493FA5DDCLL, k_ID_DEVICE_B, ID_DEVICE_B);
      break;
    case 29:
      HASH_RETURN(0x6F9C62146D8B411DLL, k_KIND_WORK, KIND_WORK);
      HASH_RETURN(0x507E55B95897741DLL, k_STATE_NOT_HELD, STATE_NOT_HELD);
      break;
    case 33:
      HASH_RETURN(0x1AB59F2ED7E3C7A1LL, k_EXPECTED_QUEUE_COUNT, EXPECTED_QUEUE_COUNT);
      break;
    case 36:
      HASH_RETURN(0x448CE0D4CCF74C24LL, k_EXPECTED_HOLD_COUNT, EXPECTED_HOLD_COUNT);
      HASH_RETURN(0x567A98C1310B20E4LL, k_STATE_HELD, STATE_HELD);
      break;
    case 37:
      HASH_RETURN(0x2EF27CD4425AEF65LL, k_STATE_SUSPENDED_RUNNABLE, STATE_SUSPENDED_RUNNABLE);
      break;
    case 45:
      HASH_RETURN(0x7CCC9C269FB6FDADLL, k_STATE_SUSPENDED, STATE_SUSPENDED);
      break;
    case 48:
      HASH_RETURN(0x1FE33489FE7DF9F0LL, k_KIND_DEVICE, KIND_DEVICE);
      break;
    case 62:
      HASH_RETURN(0x56F7CA9D36002A3ELL, k_ID_IDLE, ID_IDLE);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
