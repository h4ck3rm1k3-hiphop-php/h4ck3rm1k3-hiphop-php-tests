
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_richards_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_richards_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_ID_IDLE;
extern const int64 k_ID_DEVICE_A;
extern const int64 k_ID_DEVICE_B;
extern const int64 k_STATE_RUNNING;
extern const int64 k_STATE_RUNNABLE;
extern const int64 k_NUMBER_OF_IDS;
extern const int64 k_EXPECTED_QUEUE_COUNT;
extern const int64 k_ID_HANDLER_A;
extern const int64 k_ID_HANDLER_B;
extern const int64 k_EXPECTED_HOLD_COUNT;
extern const int64 k_DATA_SIZE;
extern const int64 k_COUNT;
extern const int64 k_STATE_NOT_HELD;
extern const int64 k_KIND_DEVICE;
extern const int64 k_KIND_WORK;
extern const int64 k_STATE_HELD;
extern const int64 k_STATE_SUSPENDED;
extern const int64 k_ID_WORKER;
extern const int64 k_STATE_SUSPENDED_RUNNABLE;


// 2. Classes
FORWARD_DECLARE_CLASS(workertask)
FORWARD_DECLARE_CLASS(idletask)
FORWARD_DECLARE_CLASS(scheduler)
FORWARD_DECLARE_CLASS(taskcontrolblock)
FORWARD_DECLARE_CLASS(devicetask)
FORWARD_DECLARE_CLASS(handlertask)
FORWARD_DECLARE_CLASS(packet)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_richards_fw_h__
