
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_ID_IDLE = 0LL;
const int64 k_ID_DEVICE_A = 4LL;
const int64 k_ID_DEVICE_B = 5LL;
const int64 k_STATE_RUNNING = 0LL;
const int64 k_STATE_RUNNABLE = 1LL;
const int64 k_NUMBER_OF_IDS = 6LL;
const int64 k_EXPECTED_QUEUE_COUNT = 2322LL;
const int64 k_ID_HANDLER_A = 2LL;
const int64 k_ID_HANDLER_B = 3LL;
const int64 k_EXPECTED_HOLD_COUNT = 928LL;
const int64 k_DATA_SIZE = 4LL;
const int64 k_COUNT = 1000LL;
const int64 k_STATE_NOT_HELD = -5LL;
const int64 k_KIND_DEVICE = 0LL;
const int64 k_KIND_WORK = 1LL;
const int64 k_STATE_HELD = 4LL;
const int64 k_STATE_SUSPENDED = 2LL;
const int64 k_ID_WORKER = 1LL;
const int64 k_STATE_SUSPENDED_RUNNABLE = 3LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 506 */
Variant c_workertask::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_workertask::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_workertask::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_workertask::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_workertask::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_workertask::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_workertask::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_workertask::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(workertask)
ObjectData *c_workertask::create(Variant v_scheduler, Variant v_v1, Variant v_v2) {
  init();
  t___construct(v_scheduler, v_v1, v_v2);
  return this;
}
ObjectData *c_workertask::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_workertask::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_workertask::cloneImpl() {
  c_workertask *obj = NEW(c_workertask)();
  cloneSet(obj);
  return obj;
}
void c_workertask::cloneSet(c_workertask *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_workertask::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_workertask::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_workertask::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_workertask$os_get(const char *s) {
  return c_workertask::os_get(s, -1);
}
Variant &cw_workertask$os_lval(const char *s) {
  return c_workertask::os_lval(s, -1);
}
Variant cw_workertask$os_constant(const char *s) {
  return c_workertask::os_constant(s);
}
Variant cw_workertask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_workertask::os_invoke(c, s, params, -1, fatal);
}
void c_workertask::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 508 */
void c_workertask::t___construct(Variant v_scheduler, Variant v_v1, Variant v_v2) {
  INSTANCE_METHOD_INJECTION(WorkerTask, WorkerTask::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("scheduler", 0x2C41610FA84ADCC7LL) = v_scheduler);
  (o_lval("v1", 0x079A01435AA36382LL) = v_v1);
  (o_lval("v2", 0x0510BDBD02C38BA7LL) = v_v2);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 515 */
Variant c_workertask::t_run(Variant v_packet) {
  INSTANCE_METHOD_INJECTION(WorkerTask, WorkerTask::run);
  int64 v_i = 0;

  if (same(v_packet, null)) {
    return LINE(519,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("suspendCurrent", 0x3662DB6895EFFC84LL, 0));
  }
  else {
    if (equal(o_get("v1", 0x079A01435AA36382LL), 2LL /* ID_HANDLER_A */)) {
      (o_lval("v1", 0x079A01435AA36382LL) = 3LL /* ID_HANDLER_B */);
    }
    else {
      (o_lval("v1", 0x079A01435AA36382LL) = 2LL /* ID_HANDLER_A */);
    }
    (v_packet.o_lval("id", 0x028B9FE0C4522BE2LL) = o_get("v1", 0x079A01435AA36382LL));
    (v_packet.o_lval("a1", 0x68DF81F26D942FC7LL) = 0LL);
    {
      LOOP_COUNTER(1);
      for ((v_i = 0LL); less(v_i, 4LL /* DATA_SIZE */); v_i++) {
        LOOP_COUNTER_CHECK(1);
        {
          lval(o_lval("v2", 0x0510BDBD02C38BA7LL))++;
          if (more(o_get("v2", 0x0510BDBD02C38BA7LL), 26LL)) {
            (o_lval("v2", 0x0510BDBD02C38BA7LL) = 1LL);
            lval(v_packet.o_lval("a2", 0x6C05C2858C72A876LL)).set(v_i, (o_get("v2", 0x0510BDBD02C38BA7LL)));
          }
        }
      }
    }
    return LINE(545,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("queue", 0x7FCE70A0683D3BC7LL, 1, v_packet));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 430 */
Variant c_idletask::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_idletask::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_idletask::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_idletask::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_idletask::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_idletask::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_idletask::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_idletask::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(idletask)
ObjectData *c_idletask::create(Variant v_scheduler, Variant v_v1, Variant v_count) {
  init();
  t___construct(v_scheduler, v_v1, v_count);
  return this;
}
ObjectData *c_idletask::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_idletask::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_idletask::cloneImpl() {
  c_idletask *obj = NEW(c_idletask)();
  cloneSet(obj);
  return obj;
}
void c_idletask::cloneSet(c_idletask *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_idletask::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_idletask::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_idletask::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_idletask$os_get(const char *s) {
  return c_idletask::os_get(s, -1);
}
Variant &cw_idletask$os_lval(const char *s) {
  return c_idletask::os_lval(s, -1);
}
Variant cw_idletask$os_constant(const char *s) {
  return c_idletask::os_constant(s);
}
Variant cw_idletask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_idletask::os_invoke(c, s, params, -1, fatal);
}
void c_idletask::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 432 */
void c_idletask::t___construct(Variant v_scheduler, Variant v_v1, Variant v_count) {
  INSTANCE_METHOD_INJECTION(IdleTask, IdleTask::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("scheduler", 0x2C41610FA84ADCC7LL) = v_scheduler);
  (o_lval("v1", 0x079A01435AA36382LL) = v_v1);
  (o_lval("count", 0x3D66B5980D54BABBLL) = v_count);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 439 */
Variant c_idletask::t_run(CVarRef v_packet) {
  INSTANCE_METHOD_INJECTION(IdleTask, IdleTask::run);
  lval(o_lval("count", 0x3D66B5980D54BABBLL))--;
  if (equal(o_get("count", 0x3D66B5980D54BABBLL), 0LL)) {
    return LINE(445,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("holdCurrent", 0x737B4C5E72CCC534LL, 0));
  }
  if (equal((bitwise_and(o_get("v1", 0x079A01435AA36382LL), 1LL)), 0LL)) {
    (o_lval("v1", 0x079A01435AA36382LL) = toInt64(toInt64(o_get("v1", 0x079A01435AA36382LL))) >> 1LL);
    return LINE(452,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("release", 0x470D7F3ADB8AE6A3LL, 1, 4LL /* ID_DEVICE_A */));
  }
  else {
    (o_lval("v1", 0x079A01435AA36382LL) = bitwise_xor((toInt64(toInt64(o_get("v1", 0x079A01435AA36382LL))) >> 1LL), 53256LL));
    return LINE(458,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("release", 0x470D7F3ADB8AE6A3LL, 1, 5LL /* ID_DEVICE_B */));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 133 */
Variant c_scheduler::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_scheduler::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_scheduler::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_scheduler::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_scheduler::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_scheduler::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_scheduler::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_scheduler::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(scheduler)
ObjectData *c_scheduler::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_scheduler::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_scheduler::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_scheduler::cloneImpl() {
  c_scheduler *obj = NEW(c_scheduler)();
  cloneSet(obj);
  return obj;
}
void c_scheduler::cloneSet(c_scheduler *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_scheduler::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x470D7F3ADB8AE6A3LL, release) {
        return (t_release(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x737B4C5E72CCC534LL, holdcurrent) {
        return (t_holdcurrent());
      }
      HASH_GUARD(0x3662DB6895EFFC84LL, suspendcurrent) {
        return (t_suspendcurrent());
      }
      break;
    case 7:
      HASH_GUARD(0x7FCE70A0683D3BC7LL, queue) {
        return (t_queue(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_scheduler::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x470D7F3ADB8AE6A3LL, release) {
        return (t_release(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x737B4C5E72CCC534LL, holdcurrent) {
        return (t_holdcurrent());
      }
      HASH_GUARD(0x3662DB6895EFFC84LL, suspendcurrent) {
        return (t_suspendcurrent());
      }
      break;
    case 7:
      HASH_GUARD(0x7FCE70A0683D3BC7LL, queue) {
        return (t_queue(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_scheduler::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_scheduler$os_get(const char *s) {
  return c_scheduler::os_get(s, -1);
}
Variant &cw_scheduler$os_lval(const char *s) {
  return c_scheduler::os_lval(s, -1);
}
Variant cw_scheduler$os_constant(const char *s) {
  return c_scheduler::os_constant(s);
}
Variant cw_scheduler$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_scheduler::os_invoke(c, s, params, -1, fatal);
}
void c_scheduler::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 135 */
void c_scheduler::t___construct() {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("queueCount", 0x0E97864EB13F46F1LL) = 0LL);
  (o_lval("holdCount", 0x6C30C4E23B547CA5LL) = 0LL);
  (o_lval("blocks", 0x2120ACC1FD9FE220LL) = ScalarArrays::sa_[0]);
  (o_lval("list", 0x507C12C34EF2AF32LL) = null);
  (o_lval("currentTcb", 0x5657C0BBDD2074E2LL) = null);
  (o_lval("currentId", 0x7C96BE1C3E04B2D0LL) = null);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 152 */
void c_scheduler::t_addidletask(int64 v_id, int64 v_priority, CVarRef v_queue, int64 v_count) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::addIdleTask);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  (assignCallTemp(eo_0, v_id),assignCallTemp(eo_1, v_priority),assignCallTemp(eo_2, v_queue),assignCallTemp(eo_3, ((Object)(LINE(154,p_idletask(p_idletask(NEWOBJ(c_idletask)())->create(((Object)(this)), 1LL, v_count)))))),t_addrunningtask(eo_0, eo_1, eo_2, eo_3));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 163 */
void c_scheduler::t_addworkertask(int64 v_id, int64 v_priority, p_packet v_queue) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::addWorkerTask);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  (assignCallTemp(eo_0, v_id),assignCallTemp(eo_1, v_priority),assignCallTemp(eo_2, ((Object)(v_queue))),assignCallTemp(eo_3, ((Object)(LINE(165,p_workertask(p_workertask(NEWOBJ(c_workertask)())->create(((Object)(this)), 2LL /* ID_HANDLER_A */, 0LL)))))),t_addtask(eo_0, eo_1, eo_2, eo_3));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 174 */
void c_scheduler::t_addhandlertask(int64 v_id, int64 v_priority, p_packet v_queue) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::addHandlerTask);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  (assignCallTemp(eo_0, v_id),assignCallTemp(eo_1, v_priority),assignCallTemp(eo_2, ((Object)(v_queue))),assignCallTemp(eo_3, ((Object)(LINE(176,p_handlertask(p_handlertask(NEWOBJ(c_handlertask)())->create(((Object)(this)))))))),t_addtask(eo_0, eo_1, eo_2, eo_3));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 185 */
void c_scheduler::t_adddevicetask(int64 v_id, int64 v_priority, CVarRef v_queue) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::addDeviceTask);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  (assignCallTemp(eo_0, v_id),assignCallTemp(eo_1, v_priority),assignCallTemp(eo_2, v_queue),assignCallTemp(eo_3, ((Object)(LINE(187,p_devicetask(p_devicetask(NEWOBJ(c_devicetask)())->create(((Object)(this)))))))),t_addtask(eo_0, eo_1, eo_2, eo_3));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 197 */
void c_scheduler::t_addrunningtask(int64 v_id, int64 v_priority, CVarRef v_queue, p_idletask v_task) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::addRunningTask);
  LINE(199,t_addtask(v_id, v_priority, v_queue, ((Object)(v_task))));
  LINE(200,o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_invoke_few_args("setRunning", 0x349AF04EEB838A8CLL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 210 */
void c_scheduler::t_addtask(int64 v_id, int64 v_priority, CVarRef v_queue, CVarRef v_task) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::addTask);
  (o_lval("currentTcb", 0x5657C0BBDD2074E2LL) = ((Object)(LINE(212,p_taskcontrolblock(p_taskcontrolblock(NEWOBJ(c_taskcontrolblock)())->create(o_get("list", 0x507C12C34EF2AF32LL), v_id, v_priority, v_queue, v_task))))));
  (o_lval("list", 0x507C12C34EF2AF32LL) = o_get("currentTcb", 0x5657C0BBDD2074E2LL));
  lval(o_lval("blocks", 0x2120ACC1FD9FE220LL)).set(v_id, (o_get("currentTcb", 0x5657C0BBDD2074E2LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 220 */
void c_scheduler::t_schedule() {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::schedule);
  (o_lval("currentTcb", 0x5657C0BBDD2074E2LL) = o_get("list", 0x507C12C34EF2AF32LL));
  LOOP_COUNTER(2);
  {
    while (!same(o_get("currentTcb", 0x5657C0BBDD2074E2LL), null)) {
      LOOP_COUNTER_CHECK(2);
      {
        if (toBoolean(LINE(226,o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_invoke_few_args("isHeldOrSuspended", 0x0E5BD8DC682035ACLL, 0)))) {
          (o_lval("currentTcb", 0x5657C0BBDD2074E2LL) = o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_get("link", 0x07D68BF68C574E09LL));
        }
        else {
          (o_lval("currentId", 0x7C96BE1C3E04B2D0LL) = o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_get("id", 0x028B9FE0C4522BE2LL));
          (o_lval("currentTcb", 0x5657C0BBDD2074E2LL) = LINE(233,o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 0)));
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 242 */
Variant c_scheduler::t_release(CVarRef v_id) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::release);
  Variant v_tcb;

  (v_tcb = o_get("blocks", 0x2120ACC1FD9FE220LL).rvalAt(v_id));
  if (same(v_tcb, null)) {
    return v_tcb;
  }
  LINE(251,v_tcb.o_invoke_few_args("markAsNotHeld", 0x277D299E6F724D89LL, 0));
  if (more(v_tcb.o_get("priority", 0x7D0A97A7000B30B0LL), o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_get("priority", 0x7D0A97A7000B30B0LL))) {
    return v_tcb;
  }
  else {
    return o_get("currentTcb", 0x5657C0BBDD2074E2LL);
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 268 */
Variant c_scheduler::t_holdcurrent() {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::holdCurrent);
  lval(o_lval("holdCount", 0x6C30C4E23B547CA5LL))++;
  LINE(271,o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_invoke_few_args("markAsHeld", 0x4B5C323F7C3530B8LL, 0));
  return o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_get("link", 0x07D68BF68C574E09LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 280 */
Variant c_scheduler::t_suspendcurrent() {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::suspendCurrent);
  LINE(282,o_get("currentTcb", 0x5657C0BBDD2074E2LL).o_invoke_few_args("markAsSuspended", 0x3B3818EC5F001C32LL, 0));
  return o_get("currentTcb", 0x5657C0BBDD2074E2LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 293 */
Variant c_scheduler::t_queue(Variant v_packet) {
  INSTANCE_METHOD_INJECTION(Scheduler, Scheduler::queue);
  Variant v_t;

  (v_t = o_get("blocks", 0x2120ACC1FD9FE220LL).rvalAt(v_packet.o_get("id", 0x028B9FE0C4522BE2LL)));
  if (same(v_t, null)) {
    return v_t;
  }
  lval(o_lval("queueCount", 0x0E97864EB13F46F1LL))++;
  (v_packet.o_lval("link", 0x07D68BF68C574E09LL) = null);
  (v_packet.o_lval("id", 0x028B9FE0C4522BE2LL) = o_get("currentId", 0x7C96BE1C3E04B2D0LL));
  return LINE(306,v_t.o_invoke_few_args("checkPriorityAdd", 0x27667235DCFBDB5ELL, 2, o_lval("currentTcb", 0x5657C0BBDD2074E2LL), v_packet));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 320 */
Variant c_taskcontrolblock::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_taskcontrolblock::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_taskcontrolblock::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_taskcontrolblock::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_taskcontrolblock::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_taskcontrolblock::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_taskcontrolblock::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_taskcontrolblock::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(taskcontrolblock)
ObjectData *c_taskcontrolblock::create(Variant v_link, Variant v_id, Variant v_priority, Variant v_queue, Variant v_task) {
  init();
  t___construct(v_link, v_id, v_priority, v_queue, v_task);
  return this;
}
ObjectData *c_taskcontrolblock::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
  } else return this;
}
void c_taskcontrolblock::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
}
ObjectData *c_taskcontrolblock::cloneImpl() {
  c_taskcontrolblock *obj = NEW(c_taskcontrolblock)();
  cloneSet(obj);
  return obj;
}
void c_taskcontrolblock::cloneSet(c_taskcontrolblock *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_taskcontrolblock::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GUARD(0x3B3818EC5F001C32LL, markassuspended) {
        return (t_markassuspended(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x4B5C323F7C3530B8LL, markasheld) {
        return (t_markasheld(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      HASH_GUARD(0x277D299E6F724D89LL, markasnotheld) {
        return (t_markasnotheld(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x0E5BD8DC682035ACLL, isheldorsuspended) {
        return (t_isheldorsuspended());
      }
      HASH_GUARD(0x349AF04EEB838A8CLL, setrunning) {
        return (t_setrunning(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x27667235DCFBDB5ELL, checkpriorityadd) {
        return (t_checkpriorityadd(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_taskcontrolblock::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GUARD(0x3B3818EC5F001C32LL, markassuspended) {
        return (t_markassuspended(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x4B5C323F7C3530B8LL, markasheld) {
        return (t_markasheld(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      HASH_GUARD(0x277D299E6F724D89LL, markasnotheld) {
        return (t_markasnotheld(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x0E5BD8DC682035ACLL, isheldorsuspended) {
        return (t_isheldorsuspended());
      }
      HASH_GUARD(0x349AF04EEB838A8CLL, setrunning) {
        return (t_setrunning(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x27667235DCFBDB5ELL, checkpriorityadd) {
        return (t_checkpriorityadd(a0, a1));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2, a3, a4), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_taskcontrolblock::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_taskcontrolblock$os_get(const char *s) {
  return c_taskcontrolblock::os_get(s, -1);
}
Variant &cw_taskcontrolblock$os_lval(const char *s) {
  return c_taskcontrolblock::os_lval(s, -1);
}
Variant cw_taskcontrolblock$os_constant(const char *s) {
  return c_taskcontrolblock::os_constant(s);
}
Variant cw_taskcontrolblock$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_taskcontrolblock::os_invoke(c, s, params, -1, fatal);
}
void c_taskcontrolblock::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 322 */
void c_taskcontrolblock::t___construct(Variant v_link, Variant v_id, Variant v_priority, Variant v_queue, Variant v_task) {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("link", 0x07D68BF68C574E09LL) = v_link);
  (o_lval("id", 0x028B9FE0C4522BE2LL) = v_id);
  (o_lval("priority", 0x7D0A97A7000B30B0LL) = v_priority);
  (o_lval("queue", 0x46569A2485D3980FLL) = v_queue);
  (o_lval("task", 0x1F6B156AE2411F1DLL) = v_task);
  if (same(v_queue, null)) {
    (o_lval("state", 0x5C84C80672BA5E22LL) = 2LL /* STATE_SUSPENDED */);
  }
  else {
    (o_lval("state", 0x5C84C80672BA5E22LL) = 3LL /* STATE_SUSPENDED_RUNNABLE */);
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 340 */
void c_taskcontrolblock::t_setrunning() {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::setRunning);
  (o_lval("state", 0x5C84C80672BA5E22LL) = 0LL /* STATE_RUNNING */);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 345 */
void c_taskcontrolblock::t_markasnotheld() {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::markAsNotHeld);
  (o_lval("state", 0x5C84C80672BA5E22LL) = bitwise_and(o_get("state", 0x5C84C80672BA5E22LL), -5LL /* STATE_NOT_HELD */));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 350 */
void c_taskcontrolblock::t_markasheld() {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::markAsHeld);
  (o_lval("state", 0x5C84C80672BA5E22LL) = bitwise_or(o_get("state", 0x5C84C80672BA5E22LL), 4LL /* STATE_HELD */));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 355 */
bool c_taskcontrolblock::t_isheldorsuspended() {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::isHeldOrSuspended);
  return !equal((bitwise_and(o_get("state", 0x5C84C80672BA5E22LL), 4LL /* STATE_HELD */)), 0LL) || (equal(o_get("state", 0x5C84C80672BA5E22LL), 2LL /* STATE_SUSPENDED */));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 360 */
void c_taskcontrolblock::t_markassuspended() {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::markAsSuspended);
  (o_lval("state", 0x5C84C80672BA5E22LL) = bitwise_or(o_get("state", 0x5C84C80672BA5E22LL), 2LL /* STATE_SUSPENDED */));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 365 */
void c_taskcontrolblock::t_markasrunnable() {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::markAsRunnable);
  (o_lval("state", 0x5C84C80672BA5E22LL) = bitwise_or(o_get("state", 0x5C84C80672BA5E22LL), 1LL /* STATE_RUNNABLE */));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 373 */
Variant c_taskcontrolblock::t_run() {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::run);
  Variant v_packet;

  if (equal(o_get("state", 0x5C84C80672BA5E22LL), 3LL /* STATE_SUSPENDED_RUNNABLE */)) {
    (v_packet = o_get("queue", 0x46569A2485D3980FLL));
    (o_lval("queue", 0x46569A2485D3980FLL) = v_packet.o_get("link", 0x07D68BF68C574E09LL));
    if (same(o_get("queue", 0x46569A2485D3980FLL), null)) {
      (o_lval("state", 0x5C84C80672BA5E22LL) = 0LL /* STATE_RUNNING */);
    }
    else {
      (o_lval("state", 0x5C84C80672BA5E22LL) = 1LL /* STATE_RUNNABLE */);
    }
  }
  else {
    setNull(v_packet);
  }
  return LINE(393,o_get("task", 0x1F6B156AE2411F1DLL).o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 1, v_packet));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 401 */
Variant c_taskcontrolblock::t_checkpriorityadd(CVarRef v_task, CVarRef v_packet) {
  INSTANCE_METHOD_INJECTION(TaskControlBlock, TaskControlBlock::checkPriorityAdd);
  if (same(o_get("queue", 0x46569A2485D3980FLL), null)) {
    (o_lval("queue", 0x46569A2485D3980FLL) = v_packet);
    LINE(406,t_markasrunnable());
    if (more(o_get("priority", 0x7D0A97A7000B30B0LL), toObject(v_task).o_get("priority", 0x7D0A97A7000B30B0LL))) {
      return ((Object)(this));
    }
  }
  else {
    (o_lval("queue", 0x46569A2485D3980FLL) = LINE(415,toObject(v_packet)->o_invoke_few_args("addTo", 0x079469FB19D05535LL, 1, o_lval("queue", 0x46569A2485D3980FLL))));
  }
  return v_task;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 469 */
Variant c_devicetask::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_devicetask::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_devicetask::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_devicetask::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_devicetask::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_devicetask::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_devicetask::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_devicetask::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(devicetask)
ObjectData *c_devicetask::create(Variant v_scheduler) {
  init();
  t___construct(v_scheduler);
  return this;
}
ObjectData *c_devicetask::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_devicetask::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_devicetask::cloneImpl() {
  c_devicetask *obj = NEW(c_devicetask)();
  cloneSet(obj);
  return obj;
}
void c_devicetask::cloneSet(c_devicetask *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_devicetask::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_devicetask::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_devicetask::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_devicetask$os_get(const char *s) {
  return c_devicetask::os_get(s, -1);
}
Variant &cw_devicetask$os_lval(const char *s) {
  return c_devicetask::os_lval(s, -1);
}
Variant cw_devicetask$os_constant(const char *s) {
  return c_devicetask::os_constant(s);
}
Variant cw_devicetask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_devicetask::os_invoke(c, s, params, -1, fatal);
}
void c_devicetask::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 471 */
void c_devicetask::t___construct(Variant v_scheduler) {
  INSTANCE_METHOD_INJECTION(DeviceTask, DeviceTask::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("scheduler", 0x2C41610FA84ADCC7LL) = v_scheduler);
  (o_lval("v1", 0x079A01435AA36382LL) = null);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 477 */
Variant c_devicetask::t_run(CVarRef v_packet) {
  INSTANCE_METHOD_INJECTION(DeviceTask, DeviceTask::run);
  Variant v_v;

  if (same(v_packet, null)) {
    if (same(o_get("v1", 0x079A01435AA36382LL), null)) {
      return LINE(483,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("suspendCurrent", 0x3662DB6895EFFC84LL, 0));
    }
    (v_v = o_get("v1", 0x079A01435AA36382LL));
    (o_lval("v1", 0x079A01435AA36382LL) = null);
    return LINE(489,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("queue", 0x7FCE70A0683D3BC7LL, 1, v_v));
  }
  else {
    (o_lval("v1", 0x079A01435AA36382LL) = v_packet);
    return LINE(494,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("holdCurrent", 0x737B4C5E72CCC534LL, 0));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 555 */
Variant c_handlertask::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_handlertask::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_handlertask::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_handlertask::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_handlertask::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_handlertask::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_handlertask::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_handlertask::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(handlertask)
ObjectData *c_handlertask::create(Variant v_scheduler) {
  init();
  t___construct(v_scheduler);
  return this;
}
ObjectData *c_handlertask::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_handlertask::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_handlertask::cloneImpl() {
  c_handlertask *obj = NEW(c_handlertask)();
  cloneSet(obj);
  return obj;
}
void c_handlertask::cloneSet(c_handlertask *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_handlertask::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_handlertask::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_handlertask::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_handlertask$os_get(const char *s) {
  return c_handlertask::os_get(s, -1);
}
Variant &cw_handlertask$os_lval(const char *s) {
  return c_handlertask::os_lval(s, -1);
}
Variant cw_handlertask$os_constant(const char *s) {
  return c_handlertask::os_constant(s);
}
Variant cw_handlertask$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_handlertask::os_invoke(c, s, params, -1, fatal);
}
void c_handlertask::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 557 */
void c_handlertask::t___construct(Variant v_scheduler) {
  INSTANCE_METHOD_INJECTION(HandlerTask, HandlerTask::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("scheduler", 0x2C41610FA84ADCC7LL) = v_scheduler);
  (o_lval("v1", 0x079A01435AA36382LL) = null);
  (o_lval("v2", 0x0510BDBD02C38BA7LL) = null);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 564 */
Variant c_handlertask::t_run(CVarRef v_packet) {
  INSTANCE_METHOD_INJECTION(HandlerTask, HandlerTask::run);
  Variant v_count;
  Variant v_v;

  if (!same(v_packet, null)) {
    if (equal(toObject(v_packet).o_get("kind", 0x03590F481F50FBA4LL), 1LL /* KIND_WORK */)) {
      (o_lval("v1", 0x079A01435AA36382LL) = LINE(570,toObject(v_packet)->o_invoke_few_args("addTo", 0x079469FB19D05535LL, 1, o_lval("v1", 0x079A01435AA36382LL))));
    }
    else {
      (o_lval("v2", 0x0510BDBD02C38BA7LL) = LINE(574,toObject(v_packet)->o_invoke_few_args("addTo", 0x079469FB19D05535LL, 1, o_lval("v2", 0x0510BDBD02C38BA7LL))));
    }
  }
  if (!same(o_get("v1", 0x079A01435AA36382LL), null)) {
    (v_count = o_get("v1", 0x079A01435AA36382LL).o_get("a1", 0x68DF81F26D942FC7LL));
    if (less(v_count, 4LL /* DATA_SIZE */)) {
      if (!same(o_get("v2", 0x0510BDBD02C38BA7LL), null)) {
        (v_v = o_get("v2", 0x0510BDBD02C38BA7LL));
        (o_lval("v2", 0x0510BDBD02C38BA7LL) = o_get("v2", 0x0510BDBD02C38BA7LL).o_get("link", 0x07D68BF68C574E09LL));
        (v_v.o_lval("a1", 0x68DF81F26D942FC7LL) = o_get("v1", 0x079A01435AA36382LL).o_get("a2", 0x6C05C2858C72A876LL).rvalAt(v_count));
        (lval(o_lval("v1", 0x079A01435AA36382LL)).o_lval("a1", 0x68DF81F26D942FC7LL) = v_count + 1LL);
        return LINE(592,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("queue", 0x7FCE70A0683D3BC7LL, 1, v_v));
      }
    }
    else {
      (v_v = o_get("v1", 0x079A01435AA36382LL));
      (o_lval("v1", 0x079A01435AA36382LL) = o_get("v1", 0x079A01435AA36382LL).o_get("link", 0x07D68BF68C574E09LL));
      return LINE(600,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("queue", 0x7FCE70A0683D3BC7LL, 1, v_v));
    }
  }
  return LINE(604,o_get("scheduler", 0x2C41610FA84ADCC7LL).o_invoke_few_args("suspendCurrent", 0x3662DB6895EFFC84LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 620 */
Variant c_packet::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_packet::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_packet::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_packet::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_packet::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_packet::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_packet::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_packet::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(packet)
ObjectData *c_packet::create(Variant v_link, Variant v_id, Variant v_kind) {
  init();
  t___construct(v_link, v_id, v_kind);
  return this;
}
ObjectData *c_packet::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_packet::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_packet::cloneImpl() {
  c_packet *obj = NEW(c_packet)();
  cloneSet(obj);
  return obj;
}
void c_packet::cloneSet(c_packet *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_packet::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x079469FB19D05535LL, addto) {
        return (t_addto(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_packet::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x079469FB19D05535LL, addto) {
        return (t_addto(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_packet::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_packet$os_get(const char *s) {
  return c_packet::os_get(s, -1);
}
Variant &cw_packet$os_lval(const char *s) {
  return c_packet::os_lval(s, -1);
}
Variant cw_packet$os_constant(const char *s) {
  return c_packet::os_constant(s);
}
Variant cw_packet$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_packet::os_invoke(c, s, params, -1, fatal);
}
void c_packet::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 622 */
void c_packet::t___construct(Variant v_link, Variant v_id, Variant v_kind) {
  INSTANCE_METHOD_INJECTION(Packet, Packet::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("link", 0x07D68BF68C574E09LL) = v_link);
  (o_lval("id", 0x028B9FE0C4522BE2LL) = v_id);
  (o_lval("kind", 0x03590F481F50FBA4LL) = v_kind);
  (o_lval("a1", 0x68DF81F26D942FC7LL) = 0LL);
  (o_lval("a2", 0x6C05C2858C72A876LL) = ScalarArrays::sa_[1]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 635 */
Variant c_packet::t_addto(CVarRef v_queue) {
  INSTANCE_METHOD_INJECTION(Packet, Packet::addTo);
  Variant v_peek;
  Variant v_next;

  (o_lval("link", 0x07D68BF68C574E09LL) = null);
  if (same(v_queue, null)) {
    return ((Object)(this));
  }
  (v_peek = v_queue);
  (v_next = v_queue);
  LOOP_COUNTER(3);
  {
    while (!same(((v_peek = v_next.o_get("link", 0x07D68BF68C574E09LL))), null)) {
      LOOP_COUNTER_CHECK(3);
      {
        (v_next = v_peek);
      }
    }
  }
  (v_next.o_lval("link", 0x07D68BF68C574E09LL) = ((Object)(this)));
  return v_queue;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php line 95 */
void f_runrichards() {
  FUNCTION_INJECTION(runRichards);
  p_scheduler v_scheduler;
  p_packet v_queue;
  String v_error;

  ((Object)((v_scheduler = ((Object)(LINE(97,p_scheduler(p_scheduler(NEWOBJ(c_scheduler)())->create())))))));
  LINE(98,v_scheduler->t_addidletask(0LL /* ID_IDLE */, 0LL, null, 1000LL /* COUNT */));
  ((Object)((v_queue = ((Object)(LINE(100,p_packet(p_packet(NEWOBJ(c_packet)())->create(null, 1LL /* ID_WORKER */, 1LL /* KIND_WORK */))))))));
  ((Object)((v_queue = ((Object)(LINE(101,p_packet(p_packet(NEWOBJ(c_packet)())->create(((Object)(v_queue)), 1LL /* ID_WORKER */, 1LL /* KIND_WORK */))))))));
  LINE(102,v_scheduler->t_addworkertask(1LL /* ID_WORKER */, 1000LL, ((Object)(v_queue))));
  ((Object)((v_queue = ((Object)(LINE(104,p_packet(p_packet(NEWOBJ(c_packet)())->create(null, 4LL /* ID_DEVICE_A */, 0LL /* KIND_DEVICE */))))))));
  ((Object)((v_queue = ((Object)(LINE(105,p_packet(p_packet(NEWOBJ(c_packet)())->create(((Object)(v_queue)), 4LL /* ID_DEVICE_A */, 0LL /* KIND_DEVICE */))))))));
  ((Object)((v_queue = ((Object)(LINE(106,p_packet(p_packet(NEWOBJ(c_packet)())->create(((Object)(v_queue)), 4LL /* ID_DEVICE_A */, 0LL /* KIND_DEVICE */))))))));
  LINE(107,v_scheduler->t_addhandlertask(2LL /* ID_HANDLER_A */, 2000LL, ((Object)(v_queue))));
  ((Object)((v_queue = ((Object)(LINE(109,p_packet(p_packet(NEWOBJ(c_packet)())->create(null, 5LL /* ID_DEVICE_B */, 0LL /* KIND_DEVICE */))))))));
  ((Object)((v_queue = ((Object)(LINE(110,p_packet(p_packet(NEWOBJ(c_packet)())->create(((Object)(v_queue)), 5LL /* ID_DEVICE_B */, 0LL /* KIND_DEVICE */))))))));
  ((Object)((v_queue = ((Object)(LINE(111,p_packet(p_packet(NEWOBJ(c_packet)())->create(((Object)(v_queue)), 5LL /* ID_DEVICE_B */, 0LL /* KIND_DEVICE */))))))));
  LINE(112,v_scheduler->t_addhandlertask(3LL /* ID_HANDLER_B */, 3000LL, ((Object)(v_queue))));
  LINE(114,v_scheduler->t_adddevicetask(4LL /* ID_DEVICE_A */, 4000LL, null));
  LINE(116,v_scheduler->t_adddevicetask(5LL /* ID_DEVICE_B */, 5000LL, null));
  LINE(118,v_scheduler->t_schedule());
  if (!equal(v_scheduler.o_get("queueCount", 0x0E97864EB13F46F1LL), 2322LL /* EXPECTED_QUEUE_COUNT */) || !equal(v_scheduler.o_get("holdCount", 0x6C30C4E23B547CA5LL), 928LL /* EXPECTED_HOLD_COUNT */)) {
    (v_error = LINE(122,concat5("Error during execution: queueCount = ", toString(v_scheduler.o_get("queueCount", 0x0E97864EB13F46F1LL)), ", holdCount = ", toString(v_scheduler.o_get("holdCount", 0x6C30C4E23B547CA5LL)), ".")));
    throw_exception(((Object)(LINE(123,p_exception(p_exception(NEWOBJ(c_exception)())->create(v_error))))));
  }
} /* function */
Object co_workertask(CArrRef params, bool init /* = true */) {
  return Object(p_workertask(NEW(c_workertask)())->dynCreate(params, init));
}
Object co_idletask(CArrRef params, bool init /* = true */) {
  return Object(p_idletask(NEW(c_idletask)())->dynCreate(params, init));
}
Object co_scheduler(CArrRef params, bool init /* = true */) {
  return Object(p_scheduler(NEW(c_scheduler)())->dynCreate(params, init));
}
Object co_taskcontrolblock(CArrRef params, bool init /* = true */) {
  return Object(p_taskcontrolblock(NEW(c_taskcontrolblock)())->dynCreate(params, init));
}
Object co_devicetask(CArrRef params, bool init /* = true */) {
  return Object(p_devicetask(NEW(c_devicetask)())->dynCreate(params, init));
}
Object co_handlertask(CArrRef params, bool init /* = true */) {
  return Object(p_handlertask(NEW(c_handlertask)())->dynCreate(params, init));
}
Object co_packet(CArrRef params, bool init /* = true */) {
  return Object(p_packet(NEW(c_packet)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_richards_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_richards_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  LINE(37,x_error_reporting(toInt32(8191LL)));
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  ;
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        LINE(659,f_runrichards());
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
