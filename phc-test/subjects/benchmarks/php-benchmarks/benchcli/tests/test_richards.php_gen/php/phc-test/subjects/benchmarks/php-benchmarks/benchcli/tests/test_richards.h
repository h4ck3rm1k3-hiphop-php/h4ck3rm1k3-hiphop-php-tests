
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_richards_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_richards_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards.fw.h>

// Declarations
#include <cls/workertask.h>
#include <cls/idletask.h>
#include <cls/scheduler.h>
#include <cls/taskcontrolblock.h>
#include <cls/devicetask.h>
#include <cls/handlertask.h>
#include <cls/packet.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_richards_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_runrichards();
Object co_workertask(CArrRef params, bool init = true);
Object co_idletask(CArrRef params, bool init = true);
Object co_scheduler(CArrRef params, bool init = true);
Object co_taskcontrolblock(CArrRef params, bool init = true);
Object co_devicetask(CArrRef params, bool init = true);
Object co_handlertask(CArrRef params, bool init = true);
Object co_packet(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_richards_h__
