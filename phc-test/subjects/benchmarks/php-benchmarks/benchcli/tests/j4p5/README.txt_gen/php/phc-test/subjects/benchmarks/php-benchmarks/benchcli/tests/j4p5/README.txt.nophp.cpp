
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/README.txt.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$README_txt(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/README.txt);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$README_txt;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("J4P5 v1.1  http://j4p5.sf.net/\r\nREADME\r\n\r\nThis package contains everything necessary to run javascript code as well as further enhance the javascript parser/runtime.\r\nIf you only intend to run javascript code, the following files are not needed and can safely be removed:\r\n- js.l\r\n- js.y\r\n- parse/metascanner.y\r\n- parser/generator.so.php\r\n\r\nThis package contains a few examples. You can delete those ");
  echo("if you choose as well:\r\n- example1.php\r\n- example2.php\r\n\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
