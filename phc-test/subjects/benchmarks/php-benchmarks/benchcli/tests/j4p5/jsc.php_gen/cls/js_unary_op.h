
#ifndef __GENERATED_cls_js_unary_op_h__
#define __GENERATED_cls_js_unary_op_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 206 */
class c_js_unary_op : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_unary_op)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_unary_op)
  DECLARE_CLASS(js_unary_op, js_unary_op, js_construct)
  void init();
  public: void t___construct(Variant v_a, Variant v_w = 0LL);
  public: ObjectData *create(Variant v_a, Variant v_w = 0LL);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_unary_op_h__
