
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_js_continue(CArrRef params, bool init = true);
Variant cw_js_continue$os_get(const char *s);
Variant &cw_js_continue$os_lval(const char *s);
Variant cw_js_continue$os_constant(const char *s);
Variant cw_js_continue$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_catch(CArrRef params, bool init = true);
Variant cw_js_catch$os_get(const char *s);
Variant &cw_js_catch$os_lval(const char *s);
Variant cw_js_catch$os_constant(const char *s);
Variant cw_js_catch$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_modulo(CArrRef params, bool init = true);
Variant cw_js_modulo$os_get(const char *s);
Variant &cw_js_modulo$os_lval(const char *s);
Variant cw_js_modulo$os_constant(const char *s);
Variant cw_js_modulo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_ternary(CArrRef params, bool init = true);
Variant cw_js_ternary$os_get(const char *s);
Variant &cw_js_ternary$os_lval(const char *s);
Variant cw_js_ternary$os_constant(const char *s);
Variant cw_js_ternary$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_print(CArrRef params, bool init = true);
Variant cw_js_print$os_get(const char *s);
Variant &cw_js_print$os_lval(const char *s);
Variant cw_js_print$os_constant(const char *s);
Variant cw_js_print$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_construct(CArrRef params, bool init = true);
Variant cw_js_construct$os_get(const char *s);
Variant &cw_js_construct$os_lval(const char *s);
Variant cw_js_construct$os_constant(const char *s);
Variant cw_js_construct$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_bit_and(CArrRef params, bool init = true);
Variant cw_js_bit_and$os_get(const char *s);
Variant &cw_js_bit_and$os_lval(const char *s);
Variant cw_js_bit_and$os_constant(const char *s);
Variant cw_js_bit_and$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_bit_or(CArrRef params, bool init = true);
Variant cw_js_bit_or$os_get(const char *s);
Variant &cw_js_bit_or$os_lval(const char *s);
Variant cw_js_bit_or$os_constant(const char *s);
Variant cw_js_bit_or$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_return(CArrRef params, bool init = true);
Variant cw_js_return$os_get(const char *s);
Variant &cw_js_return$os_lval(const char *s);
Variant cw_js_return$os_constant(const char *s);
Variant cw_js_return$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_while(CArrRef params, bool init = true);
Variant cw_js_while$os_get(const char *s);
Variant &cw_js_while$os_lval(const char *s);
Variant cw_js_while$os_constant(const char *s);
Variant cw_js_while$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_gte(CArrRef params, bool init = true);
Variant cw_js_gte$os_get(const char *s);
Variant &cw_js_gte$os_lval(const char *s);
Variant cw_js_gte$os_constant(const char *s);
Variant cw_js_gte$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_literal_array(CArrRef params, bool init = true);
Variant cw_js_literal_array$os_get(const char *s);
Variant &cw_js_literal_array$os_lval(const char *s);
Variant cw_js_literal_array$os_constant(const char *s);
Variant cw_js_literal_array$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_pre_mm(CArrRef params, bool init = true);
Variant cw_js_pre_mm$os_get(const char *s);
Variant &cw_js_pre_mm$os_lval(const char *s);
Variant cw_js_pre_mm$os_constant(const char *s);
Variant cw_js_pre_mm$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_u_minus(CArrRef params, bool init = true);
Variant cw_js_u_minus$os_get(const char *s);
Variant &cw_js_u_minus$os_lval(const char *s);
Variant cw_js_u_minus$os_constant(const char *s);
Variant cw_js_u_minus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_program(CArrRef params, bool init = true);
Variant cw_js_program$os_get(const char *s);
Variant &cw_js_program$os_lval(const char *s);
Variant cw_js_program$os_constant(const char *s);
Variant cw_js_program$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_bit_xor(CArrRef params, bool init = true);
Variant cw_js_bit_xor$os_get(const char *s);
Variant &cw_js_bit_xor$os_lval(const char *s);
Variant cw_js_bit_xor$os_constant(const char *s);
Variant cw_js_bit_xor$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_compound_assign(CArrRef params, bool init = true);
Variant cw_js_compound_assign$os_get(const char *s);
Variant &cw_js_compound_assign$os_lval(const char *s);
Variant cw_js_compound_assign$os_constant(const char *s);
Variant cw_js_compound_assign$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_delete(CArrRef params, bool init = true);
Variant cw_js_delete$os_get(const char *s);
Variant &cw_js_delete$os_lval(const char *s);
Variant cw_js_delete$os_constant(const char *s);
Variant cw_js_delete$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_and(CArrRef params, bool init = true);
Variant cw_js_and$os_get(const char *s);
Variant &cw_js_and$os_lval(const char *s);
Variant cw_js_and$os_constant(const char *s);
Variant cw_js_and$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_this(CArrRef params, bool init = true);
Variant cw_js_this$os_get(const char *s);
Variant &cw_js_this$os_lval(const char *s);
Variant cw_js_this$os_constant(const char *s);
Variant cw_js_this$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_do(CArrRef params, bool init = true);
Variant cw_js_do$os_get(const char *s);
Variant &cw_js_do$os_lval(const char *s);
Variant cw_js_do$os_constant(const char *s);
Variant cw_js_do$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_jsc(CArrRef params, bool init = true);
Variant cw_jsc$os_get(const char *s);
Variant &cw_jsc$os_lval(const char *s);
Variant cw_jsc$os_constant(const char *s);
Variant cw_jsc$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_pre_pp(CArrRef params, bool init = true);
Variant cw_js_pre_pp$os_get(const char *s);
Variant &cw_js_pre_pp$os_lval(const char *s);
Variant cw_js_pre_pp$os_constant(const char *s);
Variant cw_js_pre_pp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_not_equal(CArrRef params, bool init = true);
Variant cw_js_not_equal$os_get(const char *s);
Variant &cw_js_not_equal$os_lval(const char *s);
Variant cw_js_not_equal$os_constant(const char *s);
Variant cw_js_not_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_accessor(CArrRef params, bool init = true);
Variant cw_js_accessor$os_get(const char *s);
Variant &cw_js_accessor$os_lval(const char *s);
Variant cw_js_accessor$os_constant(const char *s);
Variant cw_js_accessor$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_block(CArrRef params, bool init = true);
Variant cw_js_block$os_get(const char *s);
Variant &cw_js_block$os_lval(const char *s);
Variant cw_js_block$os_constant(const char *s);
Variant cw_js_block$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_literal_number(CArrRef params, bool init = true);
Variant cw_js_literal_number$os_get(const char *s);
Variant &cw_js_literal_number$os_lval(const char *s);
Variant cw_js_literal_number$os_constant(const char *s);
Variant cw_js_literal_number$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_literal_boolean(CArrRef params, bool init = true);
Variant cw_js_literal_boolean$os_get(const char *s);
Variant &cw_js_literal_boolean$os_lval(const char *s);
Variant cw_js_literal_boolean$os_constant(const char *s);
Variant cw_js_literal_boolean$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_if(CArrRef params, bool init = true);
Variant cw_js_if$os_get(const char *s);
Variant &cw_js_if$os_lval(const char *s);
Variant cw_js_if$os_constant(const char *s);
Variant cw_js_if$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_gt(CArrRef params, bool init = true);
Variant cw_js_gt$os_get(const char *s);
Variant &cw_js_gt$os_lval(const char *s);
Variant cw_js_gt$os_constant(const char *s);
Variant cw_js_gt$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_literal_string(CArrRef params, bool init = true);
Variant cw_js_literal_string$os_get(const char *s);
Variant &cw_js_literal_string$os_lval(const char *s);
Variant cw_js_literal_string$os_constant(const char *s);
Variant cw_js_literal_string$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_call(CArrRef params, bool init = true);
Variant cw_js_call$os_get(const char *s);
Variant &cw_js_call$os_lval(const char *s);
Variant cw_js_call$os_constant(const char *s);
Variant cw_js_call$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_minus(CArrRef params, bool init = true);
Variant cw_js_minus$os_get(const char *s);
Variant &cw_js_minus$os_lval(const char *s);
Variant cw_js_minus$os_constant(const char *s);
Variant cw_js_minus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_void(CArrRef params, bool init = true);
Variant cw_js_void$os_get(const char *s);
Variant &cw_js_void$os_lval(const char *s);
Variant cw_js_void$os_constant(const char *s);
Variant cw_js_void$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_in(CArrRef params, bool init = true);
Variant cw_js_in$os_get(const char *s);
Variant &cw_js_in$os_lval(const char *s);
Variant cw_js_in$os_constant(const char *s);
Variant cw_js_in$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_literal_object(CArrRef params, bool init = true);
Variant cw_js_literal_object$os_get(const char *s);
Variant &cw_js_literal_object$os_lval(const char *s);
Variant cw_js_literal_object$os_constant(const char *s);
Variant cw_js_literal_object$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_ursh(CArrRef params, bool init = true);
Variant cw_js_ursh$os_get(const char *s);
Variant &cw_js_ursh$os_lval(const char *s);
Variant cw_js_ursh$os_constant(const char *s);
Variant cw_js_ursh$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_binary_op(CArrRef params, bool init = true);
Variant cw_js_binary_op$os_get(const char *s);
Variant &cw_js_binary_op$os_lval(const char *s);
Variant cw_js_binary_op$os_constant(const char *s);
Variant cw_js_binary_op$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_comma(CArrRef params, bool init = true);
Variant cw_js_comma$os_get(const char *s);
Variant &cw_js_comma$os_lval(const char *s);
Variant cw_js_comma$os_constant(const char *s);
Variant cw_js_comma$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_multiply(CArrRef params, bool init = true);
Variant cw_js_multiply$os_get(const char *s);
Variant &cw_js_multiply$os_lval(const char *s);
Variant cw_js_multiply$os_constant(const char *s);
Variant cw_js_multiply$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_strict_not_equal(CArrRef params, bool init = true);
Variant cw_js_strict_not_equal$os_get(const char *s);
Variant &cw_js_strict_not_equal$os_lval(const char *s);
Variant cw_js_strict_not_equal$os_constant(const char *s);
Variant cw_js_strict_not_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_lt(CArrRef params, bool init = true);
Variant cw_js_lt$os_get(const char *s);
Variant &cw_js_lt$os_lval(const char *s);
Variant cw_js_lt$os_constant(const char *s);
Variant cw_js_lt$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_plus(CArrRef params, bool init = true);
Variant cw_js_plus$os_get(const char *s);
Variant &cw_js_plus$os_lval(const char *s);
Variant cw_js_plus$os_constant(const char *s);
Variant cw_js_plus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_instanceof(CArrRef params, bool init = true);
Variant cw_js_instanceof$os_get(const char *s);
Variant &cw_js_instanceof$os_lval(const char *s);
Variant cw_js_instanceof$os_constant(const char *s);
Variant cw_js_instanceof$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_case(CArrRef params, bool init = true);
Variant cw_js_case$os_get(const char *s);
Variant &cw_js_case$os_lval(const char *s);
Variant cw_js_case$os_constant(const char *s);
Variant cw_js_case$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_typeof(CArrRef params, bool init = true);
Variant cw_js_typeof$os_get(const char *s);
Variant &cw_js_typeof$os_lval(const char *s);
Variant cw_js_typeof$os_constant(const char *s);
Variant cw_js_typeof$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_with(CArrRef params, bool init = true);
Variant cw_js_with$os_get(const char *s);
Variant &cw_js_with$os_lval(const char *s);
Variant cw_js_with$os_constant(const char *s);
Variant cw_js_with$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_or(CArrRef params, bool init = true);
Variant cw_js_or$os_get(const char *s);
Variant &cw_js_or$os_lval(const char *s);
Variant cw_js_or$os_constant(const char *s);
Variant cw_js_or$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_source(CArrRef params, bool init = true);
Variant cw_js_source$os_get(const char *s);
Variant &cw_js_source$os_lval(const char *s);
Variant cw_js_source$os_constant(const char *s);
Variant cw_js_source$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_literal_null(CArrRef params, bool init = true);
Variant cw_js_literal_null$os_get(const char *s);
Variant &cw_js_literal_null$os_lval(const char *s);
Variant cw_js_literal_null$os_constant(const char *s);
Variant cw_js_literal_null$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_rsh(CArrRef params, bool init = true);
Variant cw_js_rsh$os_get(const char *s);
Variant &cw_js_rsh$os_lval(const char *s);
Variant cw_js_rsh$os_constant(const char *s);
Variant cw_js_rsh$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_statement(CArrRef params, bool init = true);
Variant cw_js_statement$os_get(const char *s);
Variant &cw_js_statement$os_lval(const char *s);
Variant cw_js_statement$os_constant(const char *s);
Variant cw_js_statement$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_u_plus(CArrRef params, bool init = true);
Variant cw_js_u_plus$os_get(const char *s);
Variant &cw_js_u_plus$os_lval(const char *s);
Variant cw_js_u_plus$os_constant(const char *s);
Variant cw_js_u_plus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_var(CArrRef params, bool init = true);
Variant cw_js_var$os_get(const char *s);
Variant &cw_js_var$os_lval(const char *s);
Variant cw_js_var$os_constant(const char *s);
Variant cw_js_var$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_unary_op(CArrRef params, bool init = true);
Variant cw_js_unary_op$os_get(const char *s);
Variant &cw_js_unary_op$os_lval(const char *s);
Variant cw_js_unary_op$os_constant(const char *s);
Variant cw_js_unary_op$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_identifier(CArrRef params, bool init = true);
Variant cw_js_identifier$os_get(const char *s);
Variant &cw_js_identifier$os_lval(const char *s);
Variant cw_js_identifier$os_constant(const char *s);
Variant cw_js_identifier$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_assign(CArrRef params, bool init = true);
Variant cw_js_assign$os_get(const char *s);
Variant &cw_js_assign$os_lval(const char *s);
Variant cw_js_assign$os_constant(const char *s);
Variant cw_js_assign$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_divide(CArrRef params, bool init = true);
Variant cw_js_divide$os_get(const char *s);
Variant &cw_js_divide$os_lval(const char *s);
Variant cw_js_divide$os_constant(const char *s);
Variant cw_js_divide$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_new(CArrRef params, bool init = true);
Variant cw_js_new$os_get(const char *s);
Variant &cw_js_new$os_lval(const char *s);
Variant cw_js_new$os_constant(const char *s);
Variant cw_js_new$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_throw(CArrRef params, bool init = true);
Variant cw_js_throw$os_get(const char *s);
Variant &cw_js_throw$os_lval(const char *s);
Variant cw_js_throw$os_constant(const char *s);
Variant cw_js_throw$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_lsh(CArrRef params, bool init = true);
Variant cw_js_lsh$os_get(const char *s);
Variant &cw_js_lsh$os_lval(const char *s);
Variant cw_js_lsh$os_constant(const char *s);
Variant cw_js_lsh$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_lte(CArrRef params, bool init = true);
Variant cw_js_lte$os_get(const char *s);
Variant &cw_js_lte$os_lval(const char *s);
Variant cw_js_lte$os_constant(const char *s);
Variant cw_js_lte$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_function_definition(CArrRef params, bool init = true);
Variant cw_js_function_definition$os_get(const char *s);
Variant &cw_js_function_definition$os_lval(const char *s);
Variant cw_js_function_definition$os_constant(const char *s);
Variant cw_js_function_definition$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_post_mm(CArrRef params, bool init = true);
Variant cw_js_post_mm$os_get(const char *s);
Variant &cw_js_post_mm$os_lval(const char *s);
Variant cw_js_post_mm$os_constant(const char *s);
Variant cw_js_post_mm$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_break(CArrRef params, bool init = true);
Variant cw_js_break$os_get(const char *s);
Variant &cw_js_break$os_lval(const char *s);
Variant cw_js_break$os_constant(const char *s);
Variant cw_js_break$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_for_in(CArrRef params, bool init = true);
Variant cw_js_for_in$os_get(const char *s);
Variant &cw_js_for_in$os_lval(const char *s);
Variant cw_js_for_in$os_constant(const char *s);
Variant cw_js_for_in$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_try(CArrRef params, bool init = true);
Variant cw_js_try$os_get(const char *s);
Variant &cw_js_try$os_lval(const char *s);
Variant cw_js_try$os_constant(const char *s);
Variant cw_js_try$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_post_pp(CArrRef params, bool init = true);
Variant cw_js_post_pp$os_get(const char *s);
Variant &cw_js_post_pp$os_lval(const char *s);
Variant cw_js_post_pp$os_constant(const char *s);
Variant cw_js_post_pp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_bit_not(CArrRef params, bool init = true);
Variant cw_js_bit_not$os_get(const char *s);
Variant &cw_js_bit_not$os_lval(const char *s);
Variant cw_js_bit_not$os_constant(const char *s);
Variant cw_js_bit_not$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_label(CArrRef params, bool init = true);
Variant cw_js_label$os_get(const char *s);
Variant &cw_js_label$os_lval(const char *s);
Variant cw_js_label$os_constant(const char *s);
Variant cw_js_label$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_switch(CArrRef params, bool init = true);
Variant cw_js_switch$os_get(const char *s);
Variant &cw_js_switch$os_lval(const char *s);
Variant cw_js_switch$os_constant(const char *s);
Variant cw_js_switch$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_for(CArrRef params, bool init = true);
Variant cw_js_for$os_get(const char *s);
Variant &cw_js_for$os_lval(const char *s);
Variant cw_js_for$os_constant(const char *s);
Variant cw_js_for$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_strict_equal(CArrRef params, bool init = true);
Variant cw_js_strict_equal$os_get(const char *s);
Variant &cw_js_strict_equal$os_lval(const char *s);
Variant cw_js_strict_equal$os_constant(const char *s);
Variant cw_js_strict_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_nop(CArrRef params, bool init = true);
Variant cw_js_nop$os_get(const char *s);
Variant &cw_js_nop$os_lval(const char *s);
Variant cw_js_nop$os_constant(const char *s);
Variant cw_js_nop$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_equal(CArrRef params, bool init = true);
Variant cw_js_equal$os_get(const char *s);
Variant &cw_js_equal$os_lval(const char *s);
Variant cw_js_equal$os_constant(const char *s);
Variant cw_js_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_js_not(CArrRef params, bool init = true);
Variant cw_js_not$os_get(const char *s);
Variant &cw_js_not$os_lval(const char *s);
Variant cw_js_not$os_constant(const char *s);
Variant cw_js_not$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 12:
      HASH_CREATE_OBJECT(0x1F37B03DBBB61B0CLL, js_continue);
      break;
    case 20:
      HASH_CREATE_OBJECT(0x34DD7DB214F4BE14LL, js_pre_mm);
      break;
    case 21:
      HASH_CREATE_OBJECT(0x462FD698D6324115LL, js_unary_op);
      break;
    case 23:
      HASH_CREATE_OBJECT(0x1294FFD1B3C77017LL, js_u_minus);
      break;
    case 30:
      HASH_CREATE_OBJECT(0x0A0BAF111F16A91ELL, js_nop);
      break;
    case 35:
      HASH_CREATE_OBJECT(0x66F0DE47F4D5EC23LL, js_u_plus);
      break;
    case 37:
      HASH_CREATE_OBJECT(0x1C5C2681475FD125LL, js_block);
      HASH_CREATE_OBJECT(0x4A0CEBAC54619925LL, js_throw);
      break;
    case 43:
      HASH_CREATE_OBJECT(0x23459A2ACEDA652BLL, js_label);
      break;
    case 45:
      HASH_CREATE_OBJECT(0x758F2FEB40FD9D2DLL, js_assign);
      break;
    case 47:
      HASH_CREATE_OBJECT(0x3934A967284AF72FLL, js_post_mm);
      break;
    case 50:
      HASH_CREATE_OBJECT(0x323927650B12D532LL, js_program);
      HASH_CREATE_OBJECT(0x5A27B499881CE732LL, js_identifier);
      break;
    case 53:
      HASH_CREATE_OBJECT(0x1CE374263866DA35LL, js_minus);
      break;
    case 55:
      HASH_CREATE_OBJECT(0x03DED5240126D737LL, js_while);
      break;
    case 58:
      HASH_CREATE_OBJECT(0x19C476B264C6083ALL, js_post_pp);
      break;
    case 61:
      HASH_CREATE_OBJECT(0x71DFFC872B3C1C3DLL, js_break);
      break;
    case 70:
      HASH_CREATE_OBJECT(0x3129075E05214246LL, js_void);
      break;
    case 77:
      HASH_CREATE_OBJECT(0x1A41289F4B88A24DLL, js_literal_null);
      break;
    case 81:
      HASH_CREATE_OBJECT(0x0F5F7D14F7C44051LL, js_bit_or);
      break;
    case 85:
      HASH_CREATE_OBJECT(0x0D44FA6169703F55LL, js_lt);
      break;
    case 88:
      HASH_CREATE_OBJECT(0x0EBE2B9CAC76EF58LL, js_compound_assign);
      HASH_CREATE_OBJECT(0x4D459A27F26B3E58LL, js_comma);
      HASH_CREATE_OBJECT(0x6F151D52C816E758LL, js_divide);
      break;
    case 93:
      HASH_CREATE_OBJECT(0x3B60E6F2559AD25DLL, js_lsh);
      HASH_CREATE_OBJECT(0x67D4AF583D62ED5DLL, js_equal);
      break;
    case 94:
      HASH_CREATE_OBJECT(0x21F00B0712F1495ELL, js_return);
      break;
    case 100:
      HASH_CREATE_OBJECT(0x4633212C6D67D864LL, js_lte);
      break;
    case 103:
      HASH_CREATE_OBJECT(0x4E55AB0B1EEE3167LL, js_do);
      break;
    case 104:
      HASH_CREATE_OBJECT(0x53ECC1396E2EDE68LL, js_accessor);
      HASH_CREATE_OBJECT(0x251446B9FD0EBA68LL, js_strict_not_equal);
      break;
    case 105:
      HASH_CREATE_OBJECT(0x23181199F44A5B69LL, js_typeof);
      break;
    case 106:
      HASH_CREATE_OBJECT(0x1C8EF65231584D6ALL, js_construct);
      break;
    case 123:
      HASH_CREATE_OBJECT(0x190A70CC44EECC7BLL, js_for_in);
      break;
    case 124:
      HASH_CREATE_OBJECT(0x0BA614ECF2D0C37CLL, js_this);
      break;
    case 125:
      HASH_CREATE_OBJECT(0x002FA79AB0C0947DLL, js_pre_pp);
      break;
    case 126:
      HASH_CREATE_OBJECT(0x5C5CE28A0848337ELL, js_plus);
      break;
    case 129:
      HASH_CREATE_OBJECT(0x6B30E9D184542781LL, js_with);
      break;
    case 133:
      HASH_CREATE_OBJECT(0x32F29A8BDEB2C685LL, js_ursh);
      break;
    case 134:
      HASH_CREATE_OBJECT(0x00E4932E050CF986LL, js_literal_string);
      HASH_CREATE_OBJECT(0x7EBEDE0E9C31A386LL, js_try);
      break;
    case 138:
      HASH_CREATE_OBJECT(0x41C4ACBE6B352D8ALL, js_var);
      break;
    case 139:
      HASH_CREATE_OBJECT(0x4CEF0AA1FE05928BLL, js_bit_and);
      HASH_CREATE_OBJECT(0x33811325A3294F8BLL, js_literal_object);
      break;
    case 140:
      HASH_CREATE_OBJECT(0x2991083A2C6F4A8CLL, js_print);
      break;
    case 148:
      HASH_CREATE_OBJECT(0x05527116CEB53394LL, js_if);
      break;
    case 151:
      HASH_CREATE_OBJECT(0x6088C52325C8E797LL, jsc);
      HASH_CREATE_OBJECT(0x5B6890530D787B97LL, js_instanceof);
      break;
    case 154:
      HASH_CREATE_OBJECT(0x1BB0BB4C8FA18F9ALL, js_or);
      break;
    case 162:
      HASH_CREATE_OBJECT(0x0B1C400185F865A2LL, js_for);
      break;
    case 163:
      HASH_CREATE_OBJECT(0x4525558EE049C8A3LL, js_new);
      break;
    case 166:
      HASH_CREATE_OBJECT(0x25B213AF2D515EA6LL, js_bit_not);
      break;
    case 172:
      HASH_CREATE_OBJECT(0x72967E71951BD9ACLL, js_not_equal);
      break;
    case 174:
      HASH_CREATE_OBJECT(0x550E6917ABEBD5AELL, js_delete);
      break;
    case 175:
      HASH_CREATE_OBJECT(0x2D084C5F6C0E39AFLL, js_statement);
      break;
    case 181:
      HASH_CREATE_OBJECT(0x223AE0F7CD958CB5LL, js_gte);
      HASH_CREATE_OBJECT(0x479BDB9E9CA7C3B5LL, js_gt);
      break;
    case 187:
      HASH_CREATE_OBJECT(0x3C268770EC6548BBLL, js_rsh);
      break;
    case 188:
      HASH_CREATE_OBJECT(0x6705E4C85F7D85BCLL, js_call);
      break;
    case 190:
      HASH_CREATE_OBJECT(0x71029CD6BE6938BELL, js_bit_xor);
      break;
    case 191:
      HASH_CREATE_OBJECT(0x6F38CBBE949BACBFLL, js_not);
      break;
    case 195:
      HASH_CREATE_OBJECT(0x12982001540647C3LL, js_modulo);
      break;
    case 206:
      HASH_CREATE_OBJECT(0x7157E65B8F2713CELL, js_literal_boolean);
      break;
    case 208:
      HASH_CREATE_OBJECT(0x04F4ABF0C960D1D0LL, js_binary_op);
      break;
    case 210:
      HASH_CREATE_OBJECT(0x7FAD07A4758466D2LL, js_function_definition);
      break;
    case 212:
      HASH_CREATE_OBJECT(0x10C2AA4454AA1ED4LL, js_catch);
      break;
    case 213:
      HASH_CREATE_OBJECT(0x3D0113522A73FED5LL, js_case);
      break;
    case 219:
      HASH_CREATE_OBJECT(0x7DD82DA8756A4ADBLL, js_strict_equal);
      break;
    case 222:
      HASH_CREATE_OBJECT(0x11AB1A1F6ACBF6DELL, js_literal_number);
      break;
    case 225:
      HASH_CREATE_OBJECT(0x3D4119BD1AC728E1LL, js_ternary);
      break;
    case 226:
      HASH_CREATE_OBJECT(0x738196F7F21E95E2LL, js_multiply);
      break;
    case 232:
      HASH_CREATE_OBJECT(0x3C89388CC1601EE8LL, js_in);
      break;
    case 233:
      HASH_CREATE_OBJECT(0x16AF9BBBB6F5A2E9LL, js_literal_array);
      break;
    case 252:
      HASH_CREATE_OBJECT(0x4ACA7383E44E9EFCLL, js_and);
      break;
    case 255:
      HASH_CREATE_OBJECT(0x0F2CBEEDCF3F00FFLL, js_source);
      HASH_CREATE_OBJECT(0x64DA141EDE0DC4FFLL, js_switch);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x1F37B03DBBB61B0CLL, js_continue);
      break;
    case 20:
      HASH_INVOKE_STATIC_METHOD(0x34DD7DB214F4BE14LL, js_pre_mm);
      break;
    case 21:
      HASH_INVOKE_STATIC_METHOD(0x462FD698D6324115LL, js_unary_op);
      break;
    case 23:
      HASH_INVOKE_STATIC_METHOD(0x1294FFD1B3C77017LL, js_u_minus);
      break;
    case 30:
      HASH_INVOKE_STATIC_METHOD(0x0A0BAF111F16A91ELL, js_nop);
      break;
    case 35:
      HASH_INVOKE_STATIC_METHOD(0x66F0DE47F4D5EC23LL, js_u_plus);
      break;
    case 37:
      HASH_INVOKE_STATIC_METHOD(0x1C5C2681475FD125LL, js_block);
      HASH_INVOKE_STATIC_METHOD(0x4A0CEBAC54619925LL, js_throw);
      break;
    case 43:
      HASH_INVOKE_STATIC_METHOD(0x23459A2ACEDA652BLL, js_label);
      break;
    case 45:
      HASH_INVOKE_STATIC_METHOD(0x758F2FEB40FD9D2DLL, js_assign);
      break;
    case 47:
      HASH_INVOKE_STATIC_METHOD(0x3934A967284AF72FLL, js_post_mm);
      break;
    case 50:
      HASH_INVOKE_STATIC_METHOD(0x323927650B12D532LL, js_program);
      HASH_INVOKE_STATIC_METHOD(0x5A27B499881CE732LL, js_identifier);
      break;
    case 53:
      HASH_INVOKE_STATIC_METHOD(0x1CE374263866DA35LL, js_minus);
      break;
    case 55:
      HASH_INVOKE_STATIC_METHOD(0x03DED5240126D737LL, js_while);
      break;
    case 58:
      HASH_INVOKE_STATIC_METHOD(0x19C476B264C6083ALL, js_post_pp);
      break;
    case 61:
      HASH_INVOKE_STATIC_METHOD(0x71DFFC872B3C1C3DLL, js_break);
      break;
    case 70:
      HASH_INVOKE_STATIC_METHOD(0x3129075E05214246LL, js_void);
      break;
    case 77:
      HASH_INVOKE_STATIC_METHOD(0x1A41289F4B88A24DLL, js_literal_null);
      break;
    case 81:
      HASH_INVOKE_STATIC_METHOD(0x0F5F7D14F7C44051LL, js_bit_or);
      break;
    case 85:
      HASH_INVOKE_STATIC_METHOD(0x0D44FA6169703F55LL, js_lt);
      break;
    case 88:
      HASH_INVOKE_STATIC_METHOD(0x0EBE2B9CAC76EF58LL, js_compound_assign);
      HASH_INVOKE_STATIC_METHOD(0x4D459A27F26B3E58LL, js_comma);
      HASH_INVOKE_STATIC_METHOD(0x6F151D52C816E758LL, js_divide);
      break;
    case 93:
      HASH_INVOKE_STATIC_METHOD(0x3B60E6F2559AD25DLL, js_lsh);
      HASH_INVOKE_STATIC_METHOD(0x67D4AF583D62ED5DLL, js_equal);
      break;
    case 94:
      HASH_INVOKE_STATIC_METHOD(0x21F00B0712F1495ELL, js_return);
      break;
    case 100:
      HASH_INVOKE_STATIC_METHOD(0x4633212C6D67D864LL, js_lte);
      break;
    case 103:
      HASH_INVOKE_STATIC_METHOD(0x4E55AB0B1EEE3167LL, js_do);
      break;
    case 104:
      HASH_INVOKE_STATIC_METHOD(0x53ECC1396E2EDE68LL, js_accessor);
      HASH_INVOKE_STATIC_METHOD(0x251446B9FD0EBA68LL, js_strict_not_equal);
      break;
    case 105:
      HASH_INVOKE_STATIC_METHOD(0x23181199F44A5B69LL, js_typeof);
      break;
    case 106:
      HASH_INVOKE_STATIC_METHOD(0x1C8EF65231584D6ALL, js_construct);
      break;
    case 123:
      HASH_INVOKE_STATIC_METHOD(0x190A70CC44EECC7BLL, js_for_in);
      break;
    case 124:
      HASH_INVOKE_STATIC_METHOD(0x0BA614ECF2D0C37CLL, js_this);
      break;
    case 125:
      HASH_INVOKE_STATIC_METHOD(0x002FA79AB0C0947DLL, js_pre_pp);
      break;
    case 126:
      HASH_INVOKE_STATIC_METHOD(0x5C5CE28A0848337ELL, js_plus);
      break;
    case 129:
      HASH_INVOKE_STATIC_METHOD(0x6B30E9D184542781LL, js_with);
      break;
    case 133:
      HASH_INVOKE_STATIC_METHOD(0x32F29A8BDEB2C685LL, js_ursh);
      break;
    case 134:
      HASH_INVOKE_STATIC_METHOD(0x00E4932E050CF986LL, js_literal_string);
      HASH_INVOKE_STATIC_METHOD(0x7EBEDE0E9C31A386LL, js_try);
      break;
    case 138:
      HASH_INVOKE_STATIC_METHOD(0x41C4ACBE6B352D8ALL, js_var);
      break;
    case 139:
      HASH_INVOKE_STATIC_METHOD(0x4CEF0AA1FE05928BLL, js_bit_and);
      HASH_INVOKE_STATIC_METHOD(0x33811325A3294F8BLL, js_literal_object);
      break;
    case 140:
      HASH_INVOKE_STATIC_METHOD(0x2991083A2C6F4A8CLL, js_print);
      break;
    case 148:
      HASH_INVOKE_STATIC_METHOD(0x05527116CEB53394LL, js_if);
      break;
    case 151:
      HASH_INVOKE_STATIC_METHOD(0x6088C52325C8E797LL, jsc);
      HASH_INVOKE_STATIC_METHOD(0x5B6890530D787B97LL, js_instanceof);
      break;
    case 154:
      HASH_INVOKE_STATIC_METHOD(0x1BB0BB4C8FA18F9ALL, js_or);
      break;
    case 162:
      HASH_INVOKE_STATIC_METHOD(0x0B1C400185F865A2LL, js_for);
      break;
    case 163:
      HASH_INVOKE_STATIC_METHOD(0x4525558EE049C8A3LL, js_new);
      break;
    case 166:
      HASH_INVOKE_STATIC_METHOD(0x25B213AF2D515EA6LL, js_bit_not);
      break;
    case 172:
      HASH_INVOKE_STATIC_METHOD(0x72967E71951BD9ACLL, js_not_equal);
      break;
    case 174:
      HASH_INVOKE_STATIC_METHOD(0x550E6917ABEBD5AELL, js_delete);
      break;
    case 175:
      HASH_INVOKE_STATIC_METHOD(0x2D084C5F6C0E39AFLL, js_statement);
      break;
    case 181:
      HASH_INVOKE_STATIC_METHOD(0x223AE0F7CD958CB5LL, js_gte);
      HASH_INVOKE_STATIC_METHOD(0x479BDB9E9CA7C3B5LL, js_gt);
      break;
    case 187:
      HASH_INVOKE_STATIC_METHOD(0x3C268770EC6548BBLL, js_rsh);
      break;
    case 188:
      HASH_INVOKE_STATIC_METHOD(0x6705E4C85F7D85BCLL, js_call);
      break;
    case 190:
      HASH_INVOKE_STATIC_METHOD(0x71029CD6BE6938BELL, js_bit_xor);
      break;
    case 191:
      HASH_INVOKE_STATIC_METHOD(0x6F38CBBE949BACBFLL, js_not);
      break;
    case 195:
      HASH_INVOKE_STATIC_METHOD(0x12982001540647C3LL, js_modulo);
      break;
    case 206:
      HASH_INVOKE_STATIC_METHOD(0x7157E65B8F2713CELL, js_literal_boolean);
      break;
    case 208:
      HASH_INVOKE_STATIC_METHOD(0x04F4ABF0C960D1D0LL, js_binary_op);
      break;
    case 210:
      HASH_INVOKE_STATIC_METHOD(0x7FAD07A4758466D2LL, js_function_definition);
      break;
    case 212:
      HASH_INVOKE_STATIC_METHOD(0x10C2AA4454AA1ED4LL, js_catch);
      break;
    case 213:
      HASH_INVOKE_STATIC_METHOD(0x3D0113522A73FED5LL, js_case);
      break;
    case 219:
      HASH_INVOKE_STATIC_METHOD(0x7DD82DA8756A4ADBLL, js_strict_equal);
      break;
    case 222:
      HASH_INVOKE_STATIC_METHOD(0x11AB1A1F6ACBF6DELL, js_literal_number);
      break;
    case 225:
      HASH_INVOKE_STATIC_METHOD(0x3D4119BD1AC728E1LL, js_ternary);
      break;
    case 226:
      HASH_INVOKE_STATIC_METHOD(0x738196F7F21E95E2LL, js_multiply);
      break;
    case 232:
      HASH_INVOKE_STATIC_METHOD(0x3C89388CC1601EE8LL, js_in);
      break;
    case 233:
      HASH_INVOKE_STATIC_METHOD(0x16AF9BBBB6F5A2E9LL, js_literal_array);
      break;
    case 252:
      HASH_INVOKE_STATIC_METHOD(0x4ACA7383E44E9EFCLL, js_and);
      break;
    case 255:
      HASH_INVOKE_STATIC_METHOD(0x0F2CBEEDCF3F00FFLL, js_source);
      HASH_INVOKE_STATIC_METHOD(0x64DA141EDE0DC4FFLL, js_switch);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 12:
      HASH_GET_STATIC_PROPERTY(0x1F37B03DBBB61B0CLL, js_continue);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY(0x34DD7DB214F4BE14LL, js_pre_mm);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY(0x462FD698D6324115LL, js_unary_op);
      break;
    case 23:
      HASH_GET_STATIC_PROPERTY(0x1294FFD1B3C77017LL, js_u_minus);
      break;
    case 30:
      HASH_GET_STATIC_PROPERTY(0x0A0BAF111F16A91ELL, js_nop);
      break;
    case 35:
      HASH_GET_STATIC_PROPERTY(0x66F0DE47F4D5EC23LL, js_u_plus);
      break;
    case 37:
      HASH_GET_STATIC_PROPERTY(0x1C5C2681475FD125LL, js_block);
      HASH_GET_STATIC_PROPERTY(0x4A0CEBAC54619925LL, js_throw);
      break;
    case 43:
      HASH_GET_STATIC_PROPERTY(0x23459A2ACEDA652BLL, js_label);
      break;
    case 45:
      HASH_GET_STATIC_PROPERTY(0x758F2FEB40FD9D2DLL, js_assign);
      break;
    case 47:
      HASH_GET_STATIC_PROPERTY(0x3934A967284AF72FLL, js_post_mm);
      break;
    case 50:
      HASH_GET_STATIC_PROPERTY(0x323927650B12D532LL, js_program);
      HASH_GET_STATIC_PROPERTY(0x5A27B499881CE732LL, js_identifier);
      break;
    case 53:
      HASH_GET_STATIC_PROPERTY(0x1CE374263866DA35LL, js_minus);
      break;
    case 55:
      HASH_GET_STATIC_PROPERTY(0x03DED5240126D737LL, js_while);
      break;
    case 58:
      HASH_GET_STATIC_PROPERTY(0x19C476B264C6083ALL, js_post_pp);
      break;
    case 61:
      HASH_GET_STATIC_PROPERTY(0x71DFFC872B3C1C3DLL, js_break);
      break;
    case 70:
      HASH_GET_STATIC_PROPERTY(0x3129075E05214246LL, js_void);
      break;
    case 77:
      HASH_GET_STATIC_PROPERTY(0x1A41289F4B88A24DLL, js_literal_null);
      break;
    case 81:
      HASH_GET_STATIC_PROPERTY(0x0F5F7D14F7C44051LL, js_bit_or);
      break;
    case 85:
      HASH_GET_STATIC_PROPERTY(0x0D44FA6169703F55LL, js_lt);
      break;
    case 88:
      HASH_GET_STATIC_PROPERTY(0x0EBE2B9CAC76EF58LL, js_compound_assign);
      HASH_GET_STATIC_PROPERTY(0x4D459A27F26B3E58LL, js_comma);
      HASH_GET_STATIC_PROPERTY(0x6F151D52C816E758LL, js_divide);
      break;
    case 93:
      HASH_GET_STATIC_PROPERTY(0x3B60E6F2559AD25DLL, js_lsh);
      HASH_GET_STATIC_PROPERTY(0x67D4AF583D62ED5DLL, js_equal);
      break;
    case 94:
      HASH_GET_STATIC_PROPERTY(0x21F00B0712F1495ELL, js_return);
      break;
    case 100:
      HASH_GET_STATIC_PROPERTY(0x4633212C6D67D864LL, js_lte);
      break;
    case 103:
      HASH_GET_STATIC_PROPERTY(0x4E55AB0B1EEE3167LL, js_do);
      break;
    case 104:
      HASH_GET_STATIC_PROPERTY(0x53ECC1396E2EDE68LL, js_accessor);
      HASH_GET_STATIC_PROPERTY(0x251446B9FD0EBA68LL, js_strict_not_equal);
      break;
    case 105:
      HASH_GET_STATIC_PROPERTY(0x23181199F44A5B69LL, js_typeof);
      break;
    case 106:
      HASH_GET_STATIC_PROPERTY(0x1C8EF65231584D6ALL, js_construct);
      break;
    case 123:
      HASH_GET_STATIC_PROPERTY(0x190A70CC44EECC7BLL, js_for_in);
      break;
    case 124:
      HASH_GET_STATIC_PROPERTY(0x0BA614ECF2D0C37CLL, js_this);
      break;
    case 125:
      HASH_GET_STATIC_PROPERTY(0x002FA79AB0C0947DLL, js_pre_pp);
      break;
    case 126:
      HASH_GET_STATIC_PROPERTY(0x5C5CE28A0848337ELL, js_plus);
      break;
    case 129:
      HASH_GET_STATIC_PROPERTY(0x6B30E9D184542781LL, js_with);
      break;
    case 133:
      HASH_GET_STATIC_PROPERTY(0x32F29A8BDEB2C685LL, js_ursh);
      break;
    case 134:
      HASH_GET_STATIC_PROPERTY(0x00E4932E050CF986LL, js_literal_string);
      HASH_GET_STATIC_PROPERTY(0x7EBEDE0E9C31A386LL, js_try);
      break;
    case 138:
      HASH_GET_STATIC_PROPERTY(0x41C4ACBE6B352D8ALL, js_var);
      break;
    case 139:
      HASH_GET_STATIC_PROPERTY(0x4CEF0AA1FE05928BLL, js_bit_and);
      HASH_GET_STATIC_PROPERTY(0x33811325A3294F8BLL, js_literal_object);
      break;
    case 140:
      HASH_GET_STATIC_PROPERTY(0x2991083A2C6F4A8CLL, js_print);
      break;
    case 148:
      HASH_GET_STATIC_PROPERTY(0x05527116CEB53394LL, js_if);
      break;
    case 151:
      HASH_GET_STATIC_PROPERTY(0x6088C52325C8E797LL, jsc);
      HASH_GET_STATIC_PROPERTY(0x5B6890530D787B97LL, js_instanceof);
      break;
    case 154:
      HASH_GET_STATIC_PROPERTY(0x1BB0BB4C8FA18F9ALL, js_or);
      break;
    case 162:
      HASH_GET_STATIC_PROPERTY(0x0B1C400185F865A2LL, js_for);
      break;
    case 163:
      HASH_GET_STATIC_PROPERTY(0x4525558EE049C8A3LL, js_new);
      break;
    case 166:
      HASH_GET_STATIC_PROPERTY(0x25B213AF2D515EA6LL, js_bit_not);
      break;
    case 172:
      HASH_GET_STATIC_PROPERTY(0x72967E71951BD9ACLL, js_not_equal);
      break;
    case 174:
      HASH_GET_STATIC_PROPERTY(0x550E6917ABEBD5AELL, js_delete);
      break;
    case 175:
      HASH_GET_STATIC_PROPERTY(0x2D084C5F6C0E39AFLL, js_statement);
      break;
    case 181:
      HASH_GET_STATIC_PROPERTY(0x223AE0F7CD958CB5LL, js_gte);
      HASH_GET_STATIC_PROPERTY(0x479BDB9E9CA7C3B5LL, js_gt);
      break;
    case 187:
      HASH_GET_STATIC_PROPERTY(0x3C268770EC6548BBLL, js_rsh);
      break;
    case 188:
      HASH_GET_STATIC_PROPERTY(0x6705E4C85F7D85BCLL, js_call);
      break;
    case 190:
      HASH_GET_STATIC_PROPERTY(0x71029CD6BE6938BELL, js_bit_xor);
      break;
    case 191:
      HASH_GET_STATIC_PROPERTY(0x6F38CBBE949BACBFLL, js_not);
      break;
    case 195:
      HASH_GET_STATIC_PROPERTY(0x12982001540647C3LL, js_modulo);
      break;
    case 206:
      HASH_GET_STATIC_PROPERTY(0x7157E65B8F2713CELL, js_literal_boolean);
      break;
    case 208:
      HASH_GET_STATIC_PROPERTY(0x04F4ABF0C960D1D0LL, js_binary_op);
      break;
    case 210:
      HASH_GET_STATIC_PROPERTY(0x7FAD07A4758466D2LL, js_function_definition);
      break;
    case 212:
      HASH_GET_STATIC_PROPERTY(0x10C2AA4454AA1ED4LL, js_catch);
      break;
    case 213:
      HASH_GET_STATIC_PROPERTY(0x3D0113522A73FED5LL, js_case);
      break;
    case 219:
      HASH_GET_STATIC_PROPERTY(0x7DD82DA8756A4ADBLL, js_strict_equal);
      break;
    case 222:
      HASH_GET_STATIC_PROPERTY(0x11AB1A1F6ACBF6DELL, js_literal_number);
      break;
    case 225:
      HASH_GET_STATIC_PROPERTY(0x3D4119BD1AC728E1LL, js_ternary);
      break;
    case 226:
      HASH_GET_STATIC_PROPERTY(0x738196F7F21E95E2LL, js_multiply);
      break;
    case 232:
      HASH_GET_STATIC_PROPERTY(0x3C89388CC1601EE8LL, js_in);
      break;
    case 233:
      HASH_GET_STATIC_PROPERTY(0x16AF9BBBB6F5A2E9LL, js_literal_array);
      break;
    case 252:
      HASH_GET_STATIC_PROPERTY(0x4ACA7383E44E9EFCLL, js_and);
      break;
    case 255:
      HASH_GET_STATIC_PROPERTY(0x0F2CBEEDCF3F00FFLL, js_source);
      HASH_GET_STATIC_PROPERTY(0x64DA141EDE0DC4FFLL, js_switch);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x1F37B03DBBB61B0CLL, js_continue);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY_LV(0x34DD7DB214F4BE14LL, js_pre_mm);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY_LV(0x462FD698D6324115LL, js_unary_op);
      break;
    case 23:
      HASH_GET_STATIC_PROPERTY_LV(0x1294FFD1B3C77017LL, js_u_minus);
      break;
    case 30:
      HASH_GET_STATIC_PROPERTY_LV(0x0A0BAF111F16A91ELL, js_nop);
      break;
    case 35:
      HASH_GET_STATIC_PROPERTY_LV(0x66F0DE47F4D5EC23LL, js_u_plus);
      break;
    case 37:
      HASH_GET_STATIC_PROPERTY_LV(0x1C5C2681475FD125LL, js_block);
      HASH_GET_STATIC_PROPERTY_LV(0x4A0CEBAC54619925LL, js_throw);
      break;
    case 43:
      HASH_GET_STATIC_PROPERTY_LV(0x23459A2ACEDA652BLL, js_label);
      break;
    case 45:
      HASH_GET_STATIC_PROPERTY_LV(0x758F2FEB40FD9D2DLL, js_assign);
      break;
    case 47:
      HASH_GET_STATIC_PROPERTY_LV(0x3934A967284AF72FLL, js_post_mm);
      break;
    case 50:
      HASH_GET_STATIC_PROPERTY_LV(0x323927650B12D532LL, js_program);
      HASH_GET_STATIC_PROPERTY_LV(0x5A27B499881CE732LL, js_identifier);
      break;
    case 53:
      HASH_GET_STATIC_PROPERTY_LV(0x1CE374263866DA35LL, js_minus);
      break;
    case 55:
      HASH_GET_STATIC_PROPERTY_LV(0x03DED5240126D737LL, js_while);
      break;
    case 58:
      HASH_GET_STATIC_PROPERTY_LV(0x19C476B264C6083ALL, js_post_pp);
      break;
    case 61:
      HASH_GET_STATIC_PROPERTY_LV(0x71DFFC872B3C1C3DLL, js_break);
      break;
    case 70:
      HASH_GET_STATIC_PROPERTY_LV(0x3129075E05214246LL, js_void);
      break;
    case 77:
      HASH_GET_STATIC_PROPERTY_LV(0x1A41289F4B88A24DLL, js_literal_null);
      break;
    case 81:
      HASH_GET_STATIC_PROPERTY_LV(0x0F5F7D14F7C44051LL, js_bit_or);
      break;
    case 85:
      HASH_GET_STATIC_PROPERTY_LV(0x0D44FA6169703F55LL, js_lt);
      break;
    case 88:
      HASH_GET_STATIC_PROPERTY_LV(0x0EBE2B9CAC76EF58LL, js_compound_assign);
      HASH_GET_STATIC_PROPERTY_LV(0x4D459A27F26B3E58LL, js_comma);
      HASH_GET_STATIC_PROPERTY_LV(0x6F151D52C816E758LL, js_divide);
      break;
    case 93:
      HASH_GET_STATIC_PROPERTY_LV(0x3B60E6F2559AD25DLL, js_lsh);
      HASH_GET_STATIC_PROPERTY_LV(0x67D4AF583D62ED5DLL, js_equal);
      break;
    case 94:
      HASH_GET_STATIC_PROPERTY_LV(0x21F00B0712F1495ELL, js_return);
      break;
    case 100:
      HASH_GET_STATIC_PROPERTY_LV(0x4633212C6D67D864LL, js_lte);
      break;
    case 103:
      HASH_GET_STATIC_PROPERTY_LV(0x4E55AB0B1EEE3167LL, js_do);
      break;
    case 104:
      HASH_GET_STATIC_PROPERTY_LV(0x53ECC1396E2EDE68LL, js_accessor);
      HASH_GET_STATIC_PROPERTY_LV(0x251446B9FD0EBA68LL, js_strict_not_equal);
      break;
    case 105:
      HASH_GET_STATIC_PROPERTY_LV(0x23181199F44A5B69LL, js_typeof);
      break;
    case 106:
      HASH_GET_STATIC_PROPERTY_LV(0x1C8EF65231584D6ALL, js_construct);
      break;
    case 123:
      HASH_GET_STATIC_PROPERTY_LV(0x190A70CC44EECC7BLL, js_for_in);
      break;
    case 124:
      HASH_GET_STATIC_PROPERTY_LV(0x0BA614ECF2D0C37CLL, js_this);
      break;
    case 125:
      HASH_GET_STATIC_PROPERTY_LV(0x002FA79AB0C0947DLL, js_pre_pp);
      break;
    case 126:
      HASH_GET_STATIC_PROPERTY_LV(0x5C5CE28A0848337ELL, js_plus);
      break;
    case 129:
      HASH_GET_STATIC_PROPERTY_LV(0x6B30E9D184542781LL, js_with);
      break;
    case 133:
      HASH_GET_STATIC_PROPERTY_LV(0x32F29A8BDEB2C685LL, js_ursh);
      break;
    case 134:
      HASH_GET_STATIC_PROPERTY_LV(0x00E4932E050CF986LL, js_literal_string);
      HASH_GET_STATIC_PROPERTY_LV(0x7EBEDE0E9C31A386LL, js_try);
      break;
    case 138:
      HASH_GET_STATIC_PROPERTY_LV(0x41C4ACBE6B352D8ALL, js_var);
      break;
    case 139:
      HASH_GET_STATIC_PROPERTY_LV(0x4CEF0AA1FE05928BLL, js_bit_and);
      HASH_GET_STATIC_PROPERTY_LV(0x33811325A3294F8BLL, js_literal_object);
      break;
    case 140:
      HASH_GET_STATIC_PROPERTY_LV(0x2991083A2C6F4A8CLL, js_print);
      break;
    case 148:
      HASH_GET_STATIC_PROPERTY_LV(0x05527116CEB53394LL, js_if);
      break;
    case 151:
      HASH_GET_STATIC_PROPERTY_LV(0x6088C52325C8E797LL, jsc);
      HASH_GET_STATIC_PROPERTY_LV(0x5B6890530D787B97LL, js_instanceof);
      break;
    case 154:
      HASH_GET_STATIC_PROPERTY_LV(0x1BB0BB4C8FA18F9ALL, js_or);
      break;
    case 162:
      HASH_GET_STATIC_PROPERTY_LV(0x0B1C400185F865A2LL, js_for);
      break;
    case 163:
      HASH_GET_STATIC_PROPERTY_LV(0x4525558EE049C8A3LL, js_new);
      break;
    case 166:
      HASH_GET_STATIC_PROPERTY_LV(0x25B213AF2D515EA6LL, js_bit_not);
      break;
    case 172:
      HASH_GET_STATIC_PROPERTY_LV(0x72967E71951BD9ACLL, js_not_equal);
      break;
    case 174:
      HASH_GET_STATIC_PROPERTY_LV(0x550E6917ABEBD5AELL, js_delete);
      break;
    case 175:
      HASH_GET_STATIC_PROPERTY_LV(0x2D084C5F6C0E39AFLL, js_statement);
      break;
    case 181:
      HASH_GET_STATIC_PROPERTY_LV(0x223AE0F7CD958CB5LL, js_gte);
      HASH_GET_STATIC_PROPERTY_LV(0x479BDB9E9CA7C3B5LL, js_gt);
      break;
    case 187:
      HASH_GET_STATIC_PROPERTY_LV(0x3C268770EC6548BBLL, js_rsh);
      break;
    case 188:
      HASH_GET_STATIC_PROPERTY_LV(0x6705E4C85F7D85BCLL, js_call);
      break;
    case 190:
      HASH_GET_STATIC_PROPERTY_LV(0x71029CD6BE6938BELL, js_bit_xor);
      break;
    case 191:
      HASH_GET_STATIC_PROPERTY_LV(0x6F38CBBE949BACBFLL, js_not);
      break;
    case 195:
      HASH_GET_STATIC_PROPERTY_LV(0x12982001540647C3LL, js_modulo);
      break;
    case 206:
      HASH_GET_STATIC_PROPERTY_LV(0x7157E65B8F2713CELL, js_literal_boolean);
      break;
    case 208:
      HASH_GET_STATIC_PROPERTY_LV(0x04F4ABF0C960D1D0LL, js_binary_op);
      break;
    case 210:
      HASH_GET_STATIC_PROPERTY_LV(0x7FAD07A4758466D2LL, js_function_definition);
      break;
    case 212:
      HASH_GET_STATIC_PROPERTY_LV(0x10C2AA4454AA1ED4LL, js_catch);
      break;
    case 213:
      HASH_GET_STATIC_PROPERTY_LV(0x3D0113522A73FED5LL, js_case);
      break;
    case 219:
      HASH_GET_STATIC_PROPERTY_LV(0x7DD82DA8756A4ADBLL, js_strict_equal);
      break;
    case 222:
      HASH_GET_STATIC_PROPERTY_LV(0x11AB1A1F6ACBF6DELL, js_literal_number);
      break;
    case 225:
      HASH_GET_STATIC_PROPERTY_LV(0x3D4119BD1AC728E1LL, js_ternary);
      break;
    case 226:
      HASH_GET_STATIC_PROPERTY_LV(0x738196F7F21E95E2LL, js_multiply);
      break;
    case 232:
      HASH_GET_STATIC_PROPERTY_LV(0x3C89388CC1601EE8LL, js_in);
      break;
    case 233:
      HASH_GET_STATIC_PROPERTY_LV(0x16AF9BBBB6F5A2E9LL, js_literal_array);
      break;
    case 252:
      HASH_GET_STATIC_PROPERTY_LV(0x4ACA7383E44E9EFCLL, js_and);
      break;
    case 255:
      HASH_GET_STATIC_PROPERTY_LV(0x0F2CBEEDCF3F00FFLL, js_source);
      HASH_GET_STATIC_PROPERTY_LV(0x64DA141EDE0DC4FFLL, js_switch);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 12:
      HASH_GET_CLASS_CONSTANT(0x1F37B03DBBB61B0CLL, js_continue);
      break;
    case 20:
      HASH_GET_CLASS_CONSTANT(0x34DD7DB214F4BE14LL, js_pre_mm);
      break;
    case 21:
      HASH_GET_CLASS_CONSTANT(0x462FD698D6324115LL, js_unary_op);
      break;
    case 23:
      HASH_GET_CLASS_CONSTANT(0x1294FFD1B3C77017LL, js_u_minus);
      break;
    case 30:
      HASH_GET_CLASS_CONSTANT(0x0A0BAF111F16A91ELL, js_nop);
      break;
    case 35:
      HASH_GET_CLASS_CONSTANT(0x66F0DE47F4D5EC23LL, js_u_plus);
      break;
    case 37:
      HASH_GET_CLASS_CONSTANT(0x1C5C2681475FD125LL, js_block);
      HASH_GET_CLASS_CONSTANT(0x4A0CEBAC54619925LL, js_throw);
      break;
    case 43:
      HASH_GET_CLASS_CONSTANT(0x23459A2ACEDA652BLL, js_label);
      break;
    case 45:
      HASH_GET_CLASS_CONSTANT(0x758F2FEB40FD9D2DLL, js_assign);
      break;
    case 47:
      HASH_GET_CLASS_CONSTANT(0x3934A967284AF72FLL, js_post_mm);
      break;
    case 50:
      HASH_GET_CLASS_CONSTANT(0x323927650B12D532LL, js_program);
      HASH_GET_CLASS_CONSTANT(0x5A27B499881CE732LL, js_identifier);
      break;
    case 53:
      HASH_GET_CLASS_CONSTANT(0x1CE374263866DA35LL, js_minus);
      break;
    case 55:
      HASH_GET_CLASS_CONSTANT(0x03DED5240126D737LL, js_while);
      break;
    case 58:
      HASH_GET_CLASS_CONSTANT(0x19C476B264C6083ALL, js_post_pp);
      break;
    case 61:
      HASH_GET_CLASS_CONSTANT(0x71DFFC872B3C1C3DLL, js_break);
      break;
    case 70:
      HASH_GET_CLASS_CONSTANT(0x3129075E05214246LL, js_void);
      break;
    case 77:
      HASH_GET_CLASS_CONSTANT(0x1A41289F4B88A24DLL, js_literal_null);
      break;
    case 81:
      HASH_GET_CLASS_CONSTANT(0x0F5F7D14F7C44051LL, js_bit_or);
      break;
    case 85:
      HASH_GET_CLASS_CONSTANT(0x0D44FA6169703F55LL, js_lt);
      break;
    case 88:
      HASH_GET_CLASS_CONSTANT(0x0EBE2B9CAC76EF58LL, js_compound_assign);
      HASH_GET_CLASS_CONSTANT(0x4D459A27F26B3E58LL, js_comma);
      HASH_GET_CLASS_CONSTANT(0x6F151D52C816E758LL, js_divide);
      break;
    case 93:
      HASH_GET_CLASS_CONSTANT(0x3B60E6F2559AD25DLL, js_lsh);
      HASH_GET_CLASS_CONSTANT(0x67D4AF583D62ED5DLL, js_equal);
      break;
    case 94:
      HASH_GET_CLASS_CONSTANT(0x21F00B0712F1495ELL, js_return);
      break;
    case 100:
      HASH_GET_CLASS_CONSTANT(0x4633212C6D67D864LL, js_lte);
      break;
    case 103:
      HASH_GET_CLASS_CONSTANT(0x4E55AB0B1EEE3167LL, js_do);
      break;
    case 104:
      HASH_GET_CLASS_CONSTANT(0x53ECC1396E2EDE68LL, js_accessor);
      HASH_GET_CLASS_CONSTANT(0x251446B9FD0EBA68LL, js_strict_not_equal);
      break;
    case 105:
      HASH_GET_CLASS_CONSTANT(0x23181199F44A5B69LL, js_typeof);
      break;
    case 106:
      HASH_GET_CLASS_CONSTANT(0x1C8EF65231584D6ALL, js_construct);
      break;
    case 123:
      HASH_GET_CLASS_CONSTANT(0x190A70CC44EECC7BLL, js_for_in);
      break;
    case 124:
      HASH_GET_CLASS_CONSTANT(0x0BA614ECF2D0C37CLL, js_this);
      break;
    case 125:
      HASH_GET_CLASS_CONSTANT(0x002FA79AB0C0947DLL, js_pre_pp);
      break;
    case 126:
      HASH_GET_CLASS_CONSTANT(0x5C5CE28A0848337ELL, js_plus);
      break;
    case 129:
      HASH_GET_CLASS_CONSTANT(0x6B30E9D184542781LL, js_with);
      break;
    case 133:
      HASH_GET_CLASS_CONSTANT(0x32F29A8BDEB2C685LL, js_ursh);
      break;
    case 134:
      HASH_GET_CLASS_CONSTANT(0x00E4932E050CF986LL, js_literal_string);
      HASH_GET_CLASS_CONSTANT(0x7EBEDE0E9C31A386LL, js_try);
      break;
    case 138:
      HASH_GET_CLASS_CONSTANT(0x41C4ACBE6B352D8ALL, js_var);
      break;
    case 139:
      HASH_GET_CLASS_CONSTANT(0x4CEF0AA1FE05928BLL, js_bit_and);
      HASH_GET_CLASS_CONSTANT(0x33811325A3294F8BLL, js_literal_object);
      break;
    case 140:
      HASH_GET_CLASS_CONSTANT(0x2991083A2C6F4A8CLL, js_print);
      break;
    case 148:
      HASH_GET_CLASS_CONSTANT(0x05527116CEB53394LL, js_if);
      break;
    case 151:
      HASH_GET_CLASS_CONSTANT(0x6088C52325C8E797LL, jsc);
      HASH_GET_CLASS_CONSTANT(0x5B6890530D787B97LL, js_instanceof);
      break;
    case 154:
      HASH_GET_CLASS_CONSTANT(0x1BB0BB4C8FA18F9ALL, js_or);
      break;
    case 162:
      HASH_GET_CLASS_CONSTANT(0x0B1C400185F865A2LL, js_for);
      break;
    case 163:
      HASH_GET_CLASS_CONSTANT(0x4525558EE049C8A3LL, js_new);
      break;
    case 166:
      HASH_GET_CLASS_CONSTANT(0x25B213AF2D515EA6LL, js_bit_not);
      break;
    case 172:
      HASH_GET_CLASS_CONSTANT(0x72967E71951BD9ACLL, js_not_equal);
      break;
    case 174:
      HASH_GET_CLASS_CONSTANT(0x550E6917ABEBD5AELL, js_delete);
      break;
    case 175:
      HASH_GET_CLASS_CONSTANT(0x2D084C5F6C0E39AFLL, js_statement);
      break;
    case 181:
      HASH_GET_CLASS_CONSTANT(0x223AE0F7CD958CB5LL, js_gte);
      HASH_GET_CLASS_CONSTANT(0x479BDB9E9CA7C3B5LL, js_gt);
      break;
    case 187:
      HASH_GET_CLASS_CONSTANT(0x3C268770EC6548BBLL, js_rsh);
      break;
    case 188:
      HASH_GET_CLASS_CONSTANT(0x6705E4C85F7D85BCLL, js_call);
      break;
    case 190:
      HASH_GET_CLASS_CONSTANT(0x71029CD6BE6938BELL, js_bit_xor);
      break;
    case 191:
      HASH_GET_CLASS_CONSTANT(0x6F38CBBE949BACBFLL, js_not);
      break;
    case 195:
      HASH_GET_CLASS_CONSTANT(0x12982001540647C3LL, js_modulo);
      break;
    case 206:
      HASH_GET_CLASS_CONSTANT(0x7157E65B8F2713CELL, js_literal_boolean);
      break;
    case 208:
      HASH_GET_CLASS_CONSTANT(0x04F4ABF0C960D1D0LL, js_binary_op);
      break;
    case 210:
      HASH_GET_CLASS_CONSTANT(0x7FAD07A4758466D2LL, js_function_definition);
      break;
    case 212:
      HASH_GET_CLASS_CONSTANT(0x10C2AA4454AA1ED4LL, js_catch);
      break;
    case 213:
      HASH_GET_CLASS_CONSTANT(0x3D0113522A73FED5LL, js_case);
      break;
    case 219:
      HASH_GET_CLASS_CONSTANT(0x7DD82DA8756A4ADBLL, js_strict_equal);
      break;
    case 222:
      HASH_GET_CLASS_CONSTANT(0x11AB1A1F6ACBF6DELL, js_literal_number);
      break;
    case 225:
      HASH_GET_CLASS_CONSTANT(0x3D4119BD1AC728E1LL, js_ternary);
      break;
    case 226:
      HASH_GET_CLASS_CONSTANT(0x738196F7F21E95E2LL, js_multiply);
      break;
    case 232:
      HASH_GET_CLASS_CONSTANT(0x3C89388CC1601EE8LL, js_in);
      break;
    case 233:
      HASH_GET_CLASS_CONSTANT(0x16AF9BBBB6F5A2E9LL, js_literal_array);
      break;
    case 252:
      HASH_GET_CLASS_CONSTANT(0x4ACA7383E44E9EFCLL, js_and);
      break;
    case 255:
      HASH_GET_CLASS_CONSTANT(0x0F2CBEEDCF3F00FFLL, js_source);
      HASH_GET_CLASS_CONSTANT(0x64DA141EDE0DC4FFLL, js_switch);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
