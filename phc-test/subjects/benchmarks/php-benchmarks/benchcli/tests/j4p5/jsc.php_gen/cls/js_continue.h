
#ifndef __GENERATED_cls_js_continue_h__
#define __GENERATED_cls_js_continue_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 521 */
class c_js_continue : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_continue)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_continue)
  DECLARE_CLASS(js_continue, js_continue, js_construct)
  void init();
  public: void t___construct(Variant v_label);
  public: ObjectData *create(Variant v_label);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_continue_h__
