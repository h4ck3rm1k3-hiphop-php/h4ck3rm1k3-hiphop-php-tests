
#ifndef __GENERATED_cls_js_for_in_h__
#define __GENERATED_cls_js_for_in_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 487 */
class c_js_for_in : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_for_in)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_for_in)
  DECLARE_CLASS(js_for_in, js_for_in, js_construct)
  void init();
  public: void t___construct(int num_args, Variant v_one, Variant v_list, Variant v_statement, Array args = Array());
  public: ObjectData *create(int num_args, Variant v_one, Variant v_list, Variant v_statement, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_for_in_h__
