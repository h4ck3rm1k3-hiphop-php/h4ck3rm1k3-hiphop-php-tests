
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_jsc_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_jsc_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_JS_CACHE_DIR;


// 2. Classes
FORWARD_DECLARE_CLASS(js_continue)
FORWARD_DECLARE_CLASS(js_catch)
FORWARD_DECLARE_CLASS(js_modulo)
FORWARD_DECLARE_CLASS(js_ternary)
FORWARD_DECLARE_CLASS(js_print)
FORWARD_DECLARE_CLASS(js_construct)
FORWARD_DECLARE_CLASS(js_bit_and)
FORWARD_DECLARE_CLASS(js_bit_or)
FORWARD_DECLARE_CLASS(js_return)
FORWARD_DECLARE_CLASS(js_while)
FORWARD_DECLARE_CLASS(js_gte)
FORWARD_DECLARE_CLASS(js_literal_array)
FORWARD_DECLARE_CLASS(js_pre_mm)
FORWARD_DECLARE_CLASS(js_u_minus)
FORWARD_DECLARE_CLASS(js_program)
FORWARD_DECLARE_CLASS(js_bit_xor)
FORWARD_DECLARE_CLASS(js_compound_assign)
FORWARD_DECLARE_CLASS(js_delete)
FORWARD_DECLARE_CLASS(js_and)
FORWARD_DECLARE_CLASS(js_this)
FORWARD_DECLARE_CLASS(js_do)
FORWARD_DECLARE_CLASS(jsc)
FORWARD_DECLARE_CLASS(js_pre_pp)
FORWARD_DECLARE_CLASS(js_not_equal)
FORWARD_DECLARE_CLASS(js_accessor)
FORWARD_DECLARE_CLASS(js_block)
FORWARD_DECLARE_CLASS(js_literal_number)
FORWARD_DECLARE_CLASS(js_literal_boolean)
FORWARD_DECLARE_CLASS(js_if)
FORWARD_DECLARE_CLASS(js_gt)
FORWARD_DECLARE_CLASS(js_literal_string)
FORWARD_DECLARE_CLASS(js_call)
FORWARD_DECLARE_CLASS(js_minus)
FORWARD_DECLARE_CLASS(js_void)
FORWARD_DECLARE_CLASS(js_in)
FORWARD_DECLARE_CLASS(js_literal_object)
FORWARD_DECLARE_CLASS(js_ursh)
FORWARD_DECLARE_CLASS(js_binary_op)
FORWARD_DECLARE_CLASS(js_comma)
FORWARD_DECLARE_CLASS(js_multiply)
FORWARD_DECLARE_CLASS(js_strict_not_equal)
FORWARD_DECLARE_CLASS(js_lt)
FORWARD_DECLARE_CLASS(js_plus)
FORWARD_DECLARE_CLASS(js_instanceof)
FORWARD_DECLARE_CLASS(js_case)
FORWARD_DECLARE_CLASS(js_typeof)
FORWARD_DECLARE_CLASS(js_with)
FORWARD_DECLARE_CLASS(js_or)
FORWARD_DECLARE_CLASS(js_source)
FORWARD_DECLARE_CLASS(js_literal_null)
FORWARD_DECLARE_CLASS(js_rsh)
FORWARD_DECLARE_CLASS(js_statement)
FORWARD_DECLARE_CLASS(js_u_plus)
FORWARD_DECLARE_CLASS(js_var)
FORWARD_DECLARE_CLASS(js_unary_op)
FORWARD_DECLARE_CLASS(js_identifier)
FORWARD_DECLARE_CLASS(js_assign)
FORWARD_DECLARE_CLASS(js_divide)
FORWARD_DECLARE_CLASS(js_new)
FORWARD_DECLARE_CLASS(js_throw)
FORWARD_DECLARE_CLASS(js_lsh)
FORWARD_DECLARE_CLASS(js_lte)
FORWARD_DECLARE_CLASS(js_function_definition)
FORWARD_DECLARE_CLASS(js_post_mm)
FORWARD_DECLARE_CLASS(js_break)
FORWARD_DECLARE_CLASS(js_for_in)
FORWARD_DECLARE_CLASS(js_try)
FORWARD_DECLARE_CLASS(js_post_pp)
FORWARD_DECLARE_CLASS(js_bit_not)
FORWARD_DECLARE_CLASS(js_label)
FORWARD_DECLARE_CLASS(js_switch)
FORWARD_DECLARE_CLASS(js_for)
FORWARD_DECLARE_CLASS(js_strict_equal)
FORWARD_DECLARE_CLASS(js_nop)
FORWARD_DECLARE_CLASS(js_equal)
FORWARD_DECLARE_CLASS(js_not)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_jsc_fw_h__
