
#ifndef __GENERATED_cls_js_pre_pp_h__
#define __GENERATED_cls_js_pre_pp_h__

#include <cls/js_unary_op.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 253 */
class c_js_pre_pp : virtual public c_js_unary_op {
  BEGIN_CLASS_MAP(js_pre_pp)
    PARENT_CLASS(js_construct)
    PARENT_CLASS(js_unary_op)
  END_CLASS_MAP(js_pre_pp)
  DECLARE_CLASS(js_pre_pp, js_pre_pp, js_unary_op)
  void init();
  public: void t___construct(int num_args, Array args = Array());
  public: ObjectData *create(int num_args, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_pre_pp_h__
