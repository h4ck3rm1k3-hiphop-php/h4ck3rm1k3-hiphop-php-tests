
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(def_fun_track)
    GVS(func_def)
  END_GVS(2)

  // Dynamic Constants

  // Function/Method Static Variables
  Variant sv_jsc_DupIdgensym_DupIduniq;
  Variant sv_jsc_DupIdcompile_DupIdparser;
  Variant sv_jsc_DupIdcompile_DupIdlex;

  // Function/Method Static Variable Initialization Booleans
  Variant inited_sv_jsc_DupIdgensym_DupIduniq;
  Variant inited_sv_jsc_DupIdcompile_DupIdparser;
  Variant inited_sv_jsc_DupIdcompile_DupIdlex;

  // Class Static Variables
  int64 s_js_function_definition_DupIdin_function;
  Variant s_js_program_DupIdsource;
  Variant s_js_source_DupIdthat;
  Variant s_js_source_DupIdlabels;
  Variant s_js_source_DupIdnest;

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$jsc_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 14;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[1];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
