
#ifndef __GENERATED_cls_js_function_definition_h__
#define __GENERATED_cls_js_function_definition_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 172 */
class c_js_function_definition : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_function_definition)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_function_definition)
  DECLARE_CLASS(js_function_definition, js_function_definition, js_construct)
  void init();
  public: void t___construct(Variant v_a);
  public: ObjectData *create(Variant v_a);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_toplevel_emit();
  public: String t_function_emit();
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_function_definition_h__
