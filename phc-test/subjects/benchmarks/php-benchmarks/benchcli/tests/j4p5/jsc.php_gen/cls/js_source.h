
#ifndef __GENERATED_cls_js_source_h__
#define __GENERATED_cls_js_source_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 99 */
class c_js_source : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_source)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_source)
  DECLARE_CLASS(js_source, js_source, js_construct)
  void init();
  public: void t___construct(Variant v_statements = ScalarArrays::sa_[0], Variant v_functions = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_statements = ScalarArrays::sa_[0], Variant v_functions = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addstatement(CVarRef v_statement);
  public: void t_addfunction(CVarRef v_function);
  public: void t_addvariable(CVarRef v_var);
  public: static void ti_addfunctionexpression(const char* cls, CVarRef v_function);
  public: static void ti_addfunctiondefinition(const char* cls, CVarRef v_function);
  public: String t_emit(CVarRef v_w = 0LL);
  public: static void t_addfunctionexpression(CVarRef v_function) { ti_addfunctionexpression("js_source", v_function); }
  public: static void t_addfunctiondefinition(CVarRef v_function) { ti_addfunctiondefinition("js_source", v_function); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_source_h__
