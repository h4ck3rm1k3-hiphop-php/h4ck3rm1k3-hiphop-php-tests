
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "js_accessor", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_and", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_assign", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_binary_op", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_bit_and", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_bit_not", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_bit_or", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_bit_xor", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_block", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_break", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_call", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_case", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_catch", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_comma", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_compound_assign", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_construct", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_continue", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_delete", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_divide", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_do", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_equal", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_for", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_for_in", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_function_definition", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_gt", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_gte", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_identifier", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_if", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_in", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_instanceof", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_label", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_literal_array", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_literal_boolean", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_literal_null", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_literal_number", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_literal_object", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_literal_string", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_lsh", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_lt", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_lte", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_minus", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_modulo", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_multiply", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_new", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_nop", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_not", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_not_equal", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_or", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_plus", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_post_mm", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_post_pp", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_pre_mm", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_pre_pp", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_print", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_program", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_return", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_rsh", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_source", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_statement", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_strict_equal", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_strict_not_equal", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_switch", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_ternary", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_this", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_throw", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_try", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_typeof", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_u_minus", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_u_plus", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_unary_op", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_ursh", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_var", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_void", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_while", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "js_with", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  "jsc", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
