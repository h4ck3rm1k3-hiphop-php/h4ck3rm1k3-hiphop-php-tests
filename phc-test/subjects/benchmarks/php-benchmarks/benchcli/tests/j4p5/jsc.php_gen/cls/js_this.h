
#ifndef __GENERATED_cls_js_this_h__
#define __GENERATED_cls_js_this_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 689 */
class c_js_this : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_this)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_this)
  DECLARE_CLASS(js_this, js_this, js_construct)
  void init();
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_this_h__
