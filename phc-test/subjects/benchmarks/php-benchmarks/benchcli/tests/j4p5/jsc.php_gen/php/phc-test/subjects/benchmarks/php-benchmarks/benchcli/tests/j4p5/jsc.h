
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_jsc_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_jsc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.fw.h>

// Declarations
#include <cls/js_continue.h>
#include <cls/js_catch.h>
#include <cls/js_modulo.h>
#include <cls/js_ternary.h>
#include <cls/js_print.h>
#include <cls/js_construct.h>
#include <cls/js_bit_and.h>
#include <cls/js_bit_or.h>
#include <cls/js_return.h>
#include <cls/js_while.h>
#include <cls/js_gte.h>
#include <cls/js_literal_array.h>
#include <cls/js_pre_mm.h>
#include <cls/js_u_minus.h>
#include <cls/js_program.h>
#include <cls/js_bit_xor.h>
#include <cls/js_compound_assign.h>
#include <cls/js_delete.h>
#include <cls/js_and.h>
#include <cls/js_this.h>
#include <cls/js_do.h>
#include <cls/jsc.h>
#include <cls/js_pre_pp.h>
#include <cls/js_not_equal.h>
#include <cls/js_accessor.h>
#include <cls/js_block.h>
#include <cls/js_literal_number.h>
#include <cls/js_literal_boolean.h>
#include <cls/js_if.h>
#include <cls/js_gt.h>
#include <cls/js_literal_string.h>
#include <cls/js_call.h>
#include <cls/js_minus.h>
#include <cls/js_void.h>
#include <cls/js_in.h>
#include <cls/js_literal_object.h>
#include <cls/js_ursh.h>
#include <cls/js_binary_op.h>
#include <cls/js_comma.h>
#include <cls/js_multiply.h>
#include <cls/js_strict_not_equal.h>
#include <cls/js_lt.h>
#include <cls/js_plus.h>
#include <cls/js_instanceof.h>
#include <cls/js_case.h>
#include <cls/js_typeof.h>
#include <cls/js_with.h>
#include <cls/js_or.h>
#include <cls/js_source.h>
#include <cls/js_literal_null.h>
#include <cls/js_rsh.h>
#include <cls/js_statement.h>
#include <cls/js_u_plus.h>
#include <cls/js_var.h>
#include <cls/js_unary_op.h>
#include <cls/js_identifier.h>
#include <cls/js_assign.h>
#include <cls/js_divide.h>
#include <cls/js_new.h>
#include <cls/js_throw.h>
#include <cls/js_lsh.h>
#include <cls/js_lte.h>
#include <cls/js_function_definition.h>
#include <cls/js_post_mm.h>
#include <cls/js_break.h>
#include <cls/js_for_in.h>
#include <cls/js_try.h>
#include <cls/js_post_pp.h>
#include <cls/js_bit_not.h>
#include <cls/js_label.h>
#include <cls/js_switch.h>
#include <cls/js_for.h>
#include <cls/js_strict_equal.h>
#include <cls/js_nop.h>
#include <cls/js_equal.h>
#include <cls/js_not.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$jsc_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_js_continue(CArrRef params, bool init = true);
Object co_js_catch(CArrRef params, bool init = true);
Object co_js_modulo(CArrRef params, bool init = true);
Object co_js_ternary(CArrRef params, bool init = true);
Object co_js_print(CArrRef params, bool init = true);
Object co_js_construct(CArrRef params, bool init = true);
Object co_js_bit_and(CArrRef params, bool init = true);
Object co_js_bit_or(CArrRef params, bool init = true);
Object co_js_return(CArrRef params, bool init = true);
Object co_js_while(CArrRef params, bool init = true);
Object co_js_gte(CArrRef params, bool init = true);
Object co_js_literal_array(CArrRef params, bool init = true);
Object co_js_pre_mm(CArrRef params, bool init = true);
Object co_js_u_minus(CArrRef params, bool init = true);
Object co_js_program(CArrRef params, bool init = true);
Object co_js_bit_xor(CArrRef params, bool init = true);
Object co_js_compound_assign(CArrRef params, bool init = true);
Object co_js_delete(CArrRef params, bool init = true);
Object co_js_and(CArrRef params, bool init = true);
Object co_js_this(CArrRef params, bool init = true);
Object co_js_do(CArrRef params, bool init = true);
Object co_jsc(CArrRef params, bool init = true);
Object co_js_pre_pp(CArrRef params, bool init = true);
Object co_js_not_equal(CArrRef params, bool init = true);
Object co_js_accessor(CArrRef params, bool init = true);
Object co_js_block(CArrRef params, bool init = true);
Object co_js_literal_number(CArrRef params, bool init = true);
Object co_js_literal_boolean(CArrRef params, bool init = true);
Object co_js_if(CArrRef params, bool init = true);
Object co_js_gt(CArrRef params, bool init = true);
Object co_js_literal_string(CArrRef params, bool init = true);
Object co_js_call(CArrRef params, bool init = true);
Object co_js_minus(CArrRef params, bool init = true);
Object co_js_void(CArrRef params, bool init = true);
Object co_js_in(CArrRef params, bool init = true);
Object co_js_literal_object(CArrRef params, bool init = true);
Object co_js_ursh(CArrRef params, bool init = true);
Object co_js_binary_op(CArrRef params, bool init = true);
Object co_js_comma(CArrRef params, bool init = true);
Object co_js_multiply(CArrRef params, bool init = true);
Object co_js_strict_not_equal(CArrRef params, bool init = true);
Object co_js_lt(CArrRef params, bool init = true);
Object co_js_plus(CArrRef params, bool init = true);
Object co_js_instanceof(CArrRef params, bool init = true);
Object co_js_case(CArrRef params, bool init = true);
Object co_js_typeof(CArrRef params, bool init = true);
Object co_js_with(CArrRef params, bool init = true);
Object co_js_or(CArrRef params, bool init = true);
Object co_js_source(CArrRef params, bool init = true);
Object co_js_literal_null(CArrRef params, bool init = true);
Object co_js_rsh(CArrRef params, bool init = true);
Object co_js_statement(CArrRef params, bool init = true);
Object co_js_u_plus(CArrRef params, bool init = true);
Object co_js_var(CArrRef params, bool init = true);
Object co_js_unary_op(CArrRef params, bool init = true);
Object co_js_identifier(CArrRef params, bool init = true);
Object co_js_assign(CArrRef params, bool init = true);
Object co_js_divide(CArrRef params, bool init = true);
Object co_js_new(CArrRef params, bool init = true);
Object co_js_throw(CArrRef params, bool init = true);
Object co_js_lsh(CArrRef params, bool init = true);
Object co_js_lte(CArrRef params, bool init = true);
Object co_js_function_definition(CArrRef params, bool init = true);
Object co_js_post_mm(CArrRef params, bool init = true);
Object co_js_break(CArrRef params, bool init = true);
Object co_js_for_in(CArrRef params, bool init = true);
Object co_js_try(CArrRef params, bool init = true);
Object co_js_post_pp(CArrRef params, bool init = true);
Object co_js_bit_not(CArrRef params, bool init = true);
Object co_js_label(CArrRef params, bool init = true);
Object co_js_switch(CArrRef params, bool init = true);
Object co_js_for(CArrRef params, bool init = true);
Object co_js_strict_equal(CArrRef params, bool init = true);
Object co_js_nop(CArrRef params, bool init = true);
Object co_js_equal(CArrRef params, bool init = true);
Object co_js_not(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_jsc_h__
