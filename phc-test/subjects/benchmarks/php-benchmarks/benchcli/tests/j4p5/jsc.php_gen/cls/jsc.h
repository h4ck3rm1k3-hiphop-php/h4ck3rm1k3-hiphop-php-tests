
#ifndef __GENERATED_cls_jsc_h__
#define __GENERATED_cls_jsc_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 19 */
class c_jsc : virtual public ObjectData {
  BEGIN_CLASS_MAP(jsc)
  END_CLASS_MAP(jsc)
  DECLARE_CLASS(jsc, jsc, ObjectData)
  void init();
  public: static String ti_gensym(const char* cls, CStrRef v_prefix = "");
  public: static Variant ti_compile(const char* cls, Variant v_codestr);
  public: static Variant t_compile(CVarRef v_codestr) { return ti_compile("jsc", v_codestr); }
  public: static String t_gensym(CStrRef v_prefix = "") { return ti_gensym("jsc", v_prefix); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_jsc_h__
