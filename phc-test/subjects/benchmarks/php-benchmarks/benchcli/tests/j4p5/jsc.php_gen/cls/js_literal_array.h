
#ifndef __GENERATED_cls_js_literal_array_h__
#define __GENERATED_cls_js_literal_array_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 703 */
class c_js_literal_array : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_literal_array)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_literal_array)
  DECLARE_CLASS(js_literal_array, js_literal_array, js_construct)
  void init();
  public: void t___construct(Variant v_arr);
  public: ObjectData *create(Variant v_arr);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_literal_array_h__
