
#ifndef __GENERATED_cls_js_with_h__
#define __GENERATED_cls_js_with_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 570 */
class c_js_with : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_with)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_with)
  DECLARE_CLASS(js_with, js_with, js_construct)
  void init();
  public: void t___construct(int num_args, Variant v_expr, Variant v_statement, Array args = Array());
  public: ObjectData *create(int num_args, Variant v_expr, Variant v_statement, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_with_h__
