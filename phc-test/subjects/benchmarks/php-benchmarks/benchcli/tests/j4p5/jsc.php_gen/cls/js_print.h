
#ifndef __GENERATED_cls_js_print_h__
#define __GENERATED_cls_js_print_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 157 */
class c_js_print : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_print)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_print)
  DECLARE_CLASS(js_print, js_print, js_construct)
  void init();
  public: void t___construct(int num_args, Array args = Array());
  public: ObjectData *create(int num_args, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_print_h__
