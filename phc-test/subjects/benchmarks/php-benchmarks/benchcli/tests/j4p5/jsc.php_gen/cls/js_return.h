
#ifndef __GENERATED_cls_js_return_h__
#define __GENERATED_cls_js_return_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 555 */
class c_js_return : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_return)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_return)
  DECLARE_CLASS(js_return, js_return, js_construct)
  void init();
  public: void t___construct(Variant v_expr);
  public: ObjectData *create(Variant v_expr);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_return_h__
