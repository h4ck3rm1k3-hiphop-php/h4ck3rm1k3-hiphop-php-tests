
#ifndef __GENERATED_cls_js_bit_and_h__
#define __GENERATED_cls_js_bit_and_h__

#include <cls/js_binary_op.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 329 */
class c_js_bit_and : virtual public c_js_binary_op {
  BEGIN_CLASS_MAP(js_bit_and)
    PARENT_CLASS(js_construct)
    PARENT_CLASS(js_binary_op)
  END_CLASS_MAP(js_bit_and)
  DECLARE_CLASS(js_bit_and, js_bit_and, js_binary_op)
  void init();
  public: void t___construct(int num_args, Array args = Array());
  public: ObjectData *create(int num_args, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_bit_and_h__
