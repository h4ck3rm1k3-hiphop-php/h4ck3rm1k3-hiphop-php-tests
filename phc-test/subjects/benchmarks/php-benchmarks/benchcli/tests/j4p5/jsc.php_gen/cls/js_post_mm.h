
#ifndef __GENERATED_cls_js_post_mm_h__
#define __GENERATED_cls_js_post_mm_h__

#include <cls/js_unary_op.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 241 */
class c_js_post_mm : virtual public c_js_unary_op {
  BEGIN_CLASS_MAP(js_post_mm)
    PARENT_CLASS(js_construct)
    PARENT_CLASS(js_unary_op)
  END_CLASS_MAP(js_post_mm)
  DECLARE_CLASS(js_post_mm, js_post_mm, js_unary_op)
  void init();
  public: void t___construct(int num_args, Array args = Array());
  public: ObjectData *create(int num_args, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_post_mm_h__
