
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_JS_CACHE_DIR = "JS_CACHE_DIR";

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_6(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_7(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_10(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_11(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_12(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_13(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
inline Variant df_lambda_14(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 521 */
Variant c_js_continue::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_continue::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_continue::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_continue::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_continue::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_continue::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_continue::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_continue::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_continue)
ObjectData *c_js_continue::create(Variant v_label) {
  init();
  t___construct(v_label);
  return this;
}
ObjectData *c_js_continue::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_continue::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_continue::cloneImpl() {
  c_js_continue *obj = NEW(c_js_continue)();
  cloneSet(obj);
  return obj;
}
void c_js_continue::cloneSet(c_js_continue *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_continue::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_continue::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_continue::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_continue$os_get(const char *s) {
  return c_js_continue::os_get(s, -1);
}
Variant &cw_js_continue$os_lval(const char *s) {
  return c_js_continue::os_lval(s, -1);
}
Variant cw_js_continue$os_constant(const char *s) {
  return c_js_continue::os_constant(s);
}
Variant cw_js_continue$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_continue::os_invoke(c, s, params, -1, fatal);
}
void c_js_continue::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 522 */
void c_js_continue::t___construct(Variant v_label) {
  INSTANCE_METHOD_INJECTION(js_continue, js_continue::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("label", 0x10AB80C11491BAADLL) = v_label);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 525 */
String c_js_continue::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_continue, js_continue::emit);
  DECLARE_GLOBAL_VARIABLES(g);
  Numeric v_depth = 0;
  String v_o;

  if (equal(g->s_js_source_DupIdnest, 0LL)) {
    return "ERROR: continue outside of a loop\n*************************\n\n";
  }
  if (!same(o_get("label", 0x10AB80C11491BAADLL), ";")) {
    (v_depth = g->s_js_source_DupIdnest - g->s_js_source_DupIdlabels.rvalAt(o_get("label", 0x10AB80C11491BAADLL)));
    (v_o = LINE(531,concat3("continue ", toString(v_depth), ";\n")));
  }
  else {
    (v_o = "continue;\n");
  }
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 680 */
Variant c_js_catch::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_catch::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_catch::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_catch::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_catch::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_catch::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_catch::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_catch::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_catch)
ObjectData *c_js_catch::create(int num_args, Variant v_id, Variant v_code, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_id, v_code,args);
  return this;
}
ObjectData *c_js_catch::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_js_catch::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_js_catch::cloneImpl() {
  c_js_catch *obj = NEW(c_js_catch)();
  cloneSet(obj);
  return obj;
}
void c_js_catch::cloneSet(c_js_catch *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_catch::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_catch::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_catch::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_catch$os_get(const char *s) {
  return c_js_catch::os_get(s, -1);
}
Variant &cw_js_catch$os_lval(const char *s) {
  return c_js_catch::os_lval(s, -1);
}
Variant cw_js_catch$os_constant(const char *s) {
  return c_js_catch::os_constant(s);
}
Variant cw_js_catch$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_catch::os_invoke(c, s, params, -1, fatal);
}
void c_js_catch::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 681 */
void c_js_catch::t___construct(int num_args, Variant v_id, Variant v_code, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_catch, js_catch::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_11(LINE(682,func_get_args(num_args, Array(ArrayInit(2).set(0, v_id).set(1, v_code).create()),args)), lval(o_lval("id", 0x028B9FE0C4522BE2LL)), lval(o_lval("code", 0x5B2CD7DDAB7A1DECLL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 684 */
Variant c_js_catch::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_catch, js_catch::emit);
  return LINE(686,o_get("code", 0x5B2CD7DDAB7A1DECLL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 277 */
Variant c_js_modulo::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_modulo::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_modulo::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_modulo::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_modulo::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_modulo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_modulo::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_modulo::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_modulo)
ObjectData *c_js_modulo::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_modulo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_modulo::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_modulo::cloneImpl() {
  c_js_modulo *obj = NEW(c_js_modulo)();
  cloneSet(obj);
  return obj;
}
void c_js_modulo::cloneSet(c_js_modulo *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_modulo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_modulo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_modulo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_modulo$os_get(const char *s) {
  return c_js_modulo::os_get(s, -1);
}
Variant &cw_js_modulo$os_lval(const char *s) {
  return c_js_modulo::os_lval(s, -1);
}
Variant cw_js_modulo$os_constant(const char *s) {
  return c_js_modulo::os_constant(s);
}
Variant cw_js_modulo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_modulo::os_invoke(c, s, params, -1, fatal);
}
void c_js_modulo::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 278 */
void c_js_modulo::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_modulo, js_modulo::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(278,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 228 */
Variant c_js_ternary::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_ternary::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_ternary::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_ternary::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_ternary::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_ternary::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_ternary::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_ternary::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_ternary)
ObjectData *c_js_ternary::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_ternary::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_ternary::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_ternary::cloneImpl() {
  c_js_ternary *obj = NEW(c_js_ternary)();
  cloneSet(obj);
  return obj;
}
void c_js_ternary::cloneSet(c_js_ternary *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_ternary::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_ternary::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_ternary::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_ternary$os_get(const char *s) {
  return c_js_ternary::os_get(s, -1);
}
Variant &cw_js_ternary$os_lval(const char *s) {
  return c_js_ternary::os_lval(s, -1);
}
Variant cw_js_ternary$os_constant(const char *s) {
  return c_js_ternary::os_constant(s);
}
Variant cw_js_ternary$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_ternary::os_invoke(c, s, params, -1, fatal);
}
void c_js_ternary::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 229 */
void c_js_ternary::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_ternary, js_ternary::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  (o_lval("args", 0x4AF7CD17F976719ELL) = LINE(230,func_get_args(num_args, Array(),args)));
  (o_lval("jsrt_op", 0x72BCAA5292848C70LL) = LINE(231,(assignCallTemp(eo_0, toString(x_get_class(((Object)(this))))),x_substr(eo_0, toInt32(3LL)))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 233 */
String c_js_ternary::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_ternary, js_ternary::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return concat(concat_rev(toString(LINE(235,o_get("args", 0x4AF7CD17F976719ELL).rvalAt(2LL, 0x486AFCC090D5F98CLL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), concat(concat_rev(toString(o_get("args", 0x4AF7CD17F976719ELL).rvalAt(1LL, 0x5BCA7C69B794F8CELL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)), (assignCallTemp(eo_1, toString(o_get("args", 0x4AF7CD17F976719ELL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("(js_bool(", eo_1, ")\?("))), "):(")), "))");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 157 */
Variant c_js_print::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_print::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_print::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_print::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_print::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_print::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_print::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_print::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_print)
ObjectData *c_js_print::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_print::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_print::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_print::cloneImpl() {
  c_js_print *obj = NEW(c_js_print)();
  cloneSet(obj);
  return obj;
}
void c_js_print::cloneSet(c_js_print *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_print::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_print::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_print::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_print$os_get(const char *s) {
  return c_js_print::os_get(s, -1);
}
Variant &cw_js_print$os_lval(const char *s) {
  return c_js_print::os_lval(s, -1);
}
Variant cw_js_print$os_constant(const char *s) {
  return c_js_print::os_constant(s);
}
Variant cw_js_print$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_print::os_invoke(c, s, params, -1, fatal);
}
void c_js_print::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 158 */
void c_js_print::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_print, js_print::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("args", 0x4AF7CD17F976719ELL) = LINE(159,func_get_args(num_args, Array(),args)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 161 */
String c_js_print::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_print, js_print::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_o;
  Variant v_first;
  Variant v_arg;

  (v_o = "jsrt::write( ");
  (v_first = true);
  {
    LOOP_COUNTER(1);
    Variant map2 = o_get("args", 0x4AF7CD17F976719ELL);
    for (ArrayIterPtr iter3 = map2.begin("js_print"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_arg = iter3->second();
      {
        if (toBoolean(v_first)) {
          v_first ^= toByte(true);
        }
        else {
          concat_assign(v_o, ",");
        }
        concat_assign(v_o, LINE(166,(assignCallTemp(eo_1, (toString(toBoolean(x_get_class(v_arg)) ? ((Variant)(v_arg.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))) : ((Variant)(v_arg))))),concat3("(", eo_1, ")"))));
      }
    }
  }
  concat_assign(v_o, ");\n");
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 79 */
Variant c_js_construct::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_js_construct::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_js_construct::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_js_construct::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_js_construct::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_js_construct::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_js_construct::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_js_construct::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(js_construct)
ObjectData *c_js_construct::cloneImpl() {
  c_js_construct *obj = NEW(c_js_construct)();
  cloneSet(obj);
  return obj;
}
void c_js_construct::cloneSet(c_js_construct *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_js_construct::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_js_construct::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_construct::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_construct$os_get(const char *s) {
  return c_js_construct::os_get(s, -1);
}
Variant &cw_js_construct$os_lval(const char *s) {
  return c_js_construct::os_lval(s, -1);
}
Variant cw_js_construct$os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
Variant cw_js_construct$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_construct::os_invoke(c, s, params, -1, fatal);
}
void c_js_construct::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 80 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 329 */
Variant c_js_bit_and::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_bit_and::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_bit_and::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_bit_and::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_bit_and::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_bit_and::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_bit_and::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_bit_and::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_bit_and)
ObjectData *c_js_bit_and::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_bit_and::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_bit_and::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_bit_and::cloneImpl() {
  c_js_bit_and *obj = NEW(c_js_bit_and)();
  cloneSet(obj);
  return obj;
}
void c_js_bit_and::cloneSet(c_js_bit_and *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_bit_and::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_bit_and::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_bit_and::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_bit_and$os_get(const char *s) {
  return c_js_bit_and::os_get(s, -1);
}
Variant &cw_js_bit_and$os_lval(const char *s) {
  return c_js_bit_and::os_lval(s, -1);
}
Variant cw_js_bit_and$os_constant(const char *s) {
  return c_js_bit_and::os_constant(s);
}
Variant cw_js_bit_and$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_bit_and::os_invoke(c, s, params, -1, fatal);
}
void c_js_bit_and::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 330 */
void c_js_bit_and::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_bit_and, js_bit_and::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(330,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 335 */
Variant c_js_bit_or::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_bit_or::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_bit_or::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_bit_or::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_bit_or::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_bit_or::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_bit_or::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_bit_or::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_bit_or)
ObjectData *c_js_bit_or::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_bit_or::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_bit_or::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_bit_or::cloneImpl() {
  c_js_bit_or *obj = NEW(c_js_bit_or)();
  cloneSet(obj);
  return obj;
}
void c_js_bit_or::cloneSet(c_js_bit_or *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_bit_or::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_bit_or::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_bit_or::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_bit_or$os_get(const char *s) {
  return c_js_bit_or::os_get(s, -1);
}
Variant &cw_js_bit_or$os_lval(const char *s) {
  return c_js_bit_or::os_lval(s, -1);
}
Variant cw_js_bit_or$os_constant(const char *s) {
  return c_js_bit_or::os_constant(s);
}
Variant cw_js_bit_or$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_bit_or::os_invoke(c, s, params, -1, fatal);
}
void c_js_bit_or::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 336 */
void c_js_bit_or::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_bit_or, js_bit_or::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(336,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 555 */
Variant c_js_return::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_return::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_return::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_return::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_return::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_return::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_return::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_return::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_return)
ObjectData *c_js_return::create(Variant v_expr) {
  init();
  t___construct(v_expr);
  return this;
}
ObjectData *c_js_return::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_return::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_return::cloneImpl() {
  c_js_return *obj = NEW(c_js_return)();
  cloneSet(obj);
  return obj;
}
void c_js_return::cloneSet(c_js_return *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_return::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_return::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_return::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_return$os_get(const char *s) {
  return c_js_return::os_get(s, -1);
}
Variant &cw_js_return$os_lval(const char *s) {
  return c_js_return::os_lval(s, -1);
}
Variant cw_js_return$os_constant(const char *s) {
  return c_js_return::os_constant(s);
}
Variant cw_js_return$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_return::os_invoke(c, s, params, -1, fatal);
}
void c_js_return::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 556 */
void c_js_return::t___construct(Variant v_expr) {
  INSTANCE_METHOD_INJECTION(js_return, js_return::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("expr", 0x1928DF37EFEC67D5LL) = v_expr);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 559 */
Variant c_js_return::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_return, js_return::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  if (equal(g->s_js_function_definition_DupIdin_function, 0LL)) {
    throw_exception(LINE(561,create_object("js_exception", Array(ArrayInit(1).set(0, create_object("js_syntaxerror", Array(ArrayInit(1).set(0, "invalid return").create()))).create()))));
  }
  if (equal(o_get("expr", 0x1928DF37EFEC67D5LL), ";")) {
    return "return jsrt::$undefined;\\n";
  }
  else {
    return LINE(566,(assignCallTemp(eo_1, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("return ", eo_1, ";\n")));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 460 */
Variant c_js_while::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_while::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_while::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_while::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_while::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_while::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_while::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_while::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_while)
ObjectData *c_js_while::create(Variant v_expr, Variant v_statement) {
  init();
  t___construct(v_expr, v_statement);
  return this;
}
ObjectData *c_js_while::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_while::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_while::cloneImpl() {
  c_js_while *obj = NEW(c_js_while)();
  cloneSet(obj);
  return obj;
}
void c_js_while::cloneSet(c_js_while *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_while::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_while::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_while::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_while$os_get(const char *s) {
  return c_js_while::os_get(s, -1);
}
Variant &cw_js_while$os_lval(const char *s) {
  return c_js_while::os_lval(s, -1);
}
Variant cw_js_while$os_constant(const char *s) {
  return c_js_while::os_constant(s);
}
Variant cw_js_while$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_while::os_invoke(c, s, params, -1, fatal);
}
void c_js_while::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 461 */
void c_js_while::t___construct(Variant v_expr, Variant v_statement) {
  INSTANCE_METHOD_INJECTION(js_while, js_while::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("expr", 0x1928DF37EFEC67D5LL) = v_expr);
  (o_lval("statement", 0x50D02CF824CFB5E6LL) = v_statement);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 465 */
String c_js_while::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_while, js_while::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_o;

  g->s_js_source_DupIdnest++;
  (v_o = concat(concat_rev(toString(LINE(467,o_get("statement", 0x50D02CF824CFB5E6LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), (assignCallTemp(eo_1, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("while (js_bool(", eo_1, ")) "))), "\n"));
  g->s_js_source_DupIdnest--;
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 308 */
Variant c_js_gte::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_gte::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_gte::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_gte::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_gte::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_gte::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_gte::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_gte::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_gte)
ObjectData *c_js_gte::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_gte::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_gte::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_gte::cloneImpl() {
  c_js_gte *obj = NEW(c_js_gte)();
  cloneSet(obj);
  return obj;
}
void c_js_gte::cloneSet(c_js_gte *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_gte::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_gte::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_gte::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_gte$os_get(const char *s) {
  return c_js_gte::os_get(s, -1);
}
Variant &cw_js_gte$os_lval(const char *s) {
  return c_js_gte::os_lval(s, -1);
}
Variant cw_js_gte$os_constant(const char *s) {
  return c_js_gte::os_constant(s);
}
Variant cw_js_gte$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_gte::os_invoke(c, s, params, -1, fatal);
}
void c_js_gte::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 309 */
void c_js_gte::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_gte, js_gte::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(309,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 703 */
Variant c_js_literal_array::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_literal_array::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_literal_array::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_literal_array::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_literal_array::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_literal_array::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_literal_array::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_literal_array::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_literal_array)
ObjectData *c_js_literal_array::create(Variant v_arr) {
  init();
  t___construct(v_arr);
  return this;
}
ObjectData *c_js_literal_array::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_literal_array::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_literal_array::cloneImpl() {
  c_js_literal_array *obj = NEW(c_js_literal_array)();
  cloneSet(obj);
  return obj;
}
void c_js_literal_array::cloneSet(c_js_literal_array *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_literal_array::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_literal_array::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_literal_array::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_literal_array$os_get(const char *s) {
  return c_js_literal_array::os_get(s, -1);
}
Variant &cw_js_literal_array$os_lval(const char *s) {
  return c_js_literal_array::os_lval(s, -1);
}
Variant cw_js_literal_array$os_constant(const char *s) {
  return c_js_literal_array::os_constant(s);
}
Variant cw_js_literal_array$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_literal_array::os_invoke(c, s, params, -1, fatal);
}
void c_js_literal_array::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 704 */
void c_js_literal_array::t___construct(Variant v_arr) {
  INSTANCE_METHOD_INJECTION(js_literal_array, js_literal_array::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("arr", 0x1776D8467CB08D68LL) = v_arr);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 707 */
String c_js_literal_array::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_literal_array, js_literal_array::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_a;
  int64 v_i = 0;

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, LINE(709,x_count(o_get("arr", 0x1776D8467CB08D68LL)))); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        if (!equal(o_get("arr", 0x1776D8467CB08D68LL).rvalAt(v_i), null)) {
          v_a.set(v_i, (LINE(711,o_get("arr", 0x1776D8467CB08D68LL).rvalAt(v_i).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
        }
      }
    }
  }
  if ((equal(LINE(714,x_count(o_get("arr", 0x1776D8467CB08D68LL))), 1LL)) && (equal(x_get_class(o_get("arr", 0x1776D8467CB08D68LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "js_literal_null"))) {
    (v_a = ScalarArrays::sa_[0]);
  }
  return LINE(718,(assignCallTemp(eo_1, x_implode(",", v_a)),concat3("jsrt::literal_array(", eo_1, ")")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 256 */
Variant c_js_pre_mm::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_pre_mm::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_pre_mm::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_pre_mm::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_pre_mm::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_pre_mm::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_pre_mm::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_pre_mm::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_pre_mm)
ObjectData *c_js_pre_mm::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_pre_mm::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_pre_mm::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_pre_mm::cloneImpl() {
  c_js_pre_mm *obj = NEW(c_js_pre_mm)();
  cloneSet(obj);
  return obj;
}
void c_js_pre_mm::cloneSet(c_js_pre_mm *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_pre_mm::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_pre_mm::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_pre_mm::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_pre_mm$os_get(const char *s) {
  return c_js_pre_mm::os_get(s, -1);
}
Variant &cw_js_pre_mm$os_lval(const char *s) {
  return c_js_pre_mm::os_lval(s, -1);
}
Variant cw_js_pre_mm$os_constant(const char *s) {
  return c_js_pre_mm::os_constant(s);
}
Variant cw_js_pre_mm$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_pre_mm::os_invoke(c, s, params, -1, fatal);
}
void c_js_pre_mm::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 257 */
void c_js_pre_mm::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_pre_mm, js_pre_mm::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(257,c_js_unary_op::t___construct(func_get_args(num_args, Array(),args)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 262 */
Variant c_js_u_minus::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_u_minus::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_u_minus::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_u_minus::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_u_minus::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_u_minus::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_u_minus::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_u_minus::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_u_minus)
ObjectData *c_js_u_minus::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_u_minus::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_u_minus::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_u_minus::cloneImpl() {
  c_js_u_minus *obj = NEW(c_js_u_minus)();
  cloneSet(obj);
  return obj;
}
void c_js_u_minus::cloneSet(c_js_u_minus *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_u_minus::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_u_minus::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_u_minus::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_u_minus$os_get(const char *s) {
  return c_js_u_minus::os_get(s, -1);
}
Variant &cw_js_u_minus$os_lval(const char *s) {
  return c_js_u_minus::os_lval(s, -1);
}
Variant cw_js_u_minus$os_constant(const char *s) {
  return c_js_u_minus::os_constant(s);
}
Variant cw_js_u_minus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_u_minus::os_invoke(c, s, params, -1, fatal);
}
void c_js_u_minus::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 263 */
void c_js_u_minus::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_u_minus, js_u_minus::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  LINE(263,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_unary_op::t___construct(eo_0, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 82 */
Variant c_js_program::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x12E01B4D9DD0BABCLL, g->s_js_program_DupIdsource,
                  source);
      break;
    default:
      break;
  }
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_program::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x12E01B4D9DD0BABCLL, g->s_js_program_DupIdsource,
                  source);
      break;
    default:
      break;
  }
  return c_js_construct::os_lval(s, hash);
}
void c_js_program::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_program::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_program::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_program::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_program::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_program::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_program)
ObjectData *c_js_program::create(Variant v_obj) {
  init();
  t___construct(v_obj);
  return this;
}
ObjectData *c_js_program::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_program::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_program::cloneImpl() {
  c_js_program *obj = NEW(c_js_program)();
  cloneSet(obj);
  return obj;
}
void c_js_program::cloneSet(c_js_program *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_program::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_program::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_program::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_program$os_get(const char *s) {
  return c_js_program::os_get(s, -1);
}
Variant &cw_js_program$os_lval(const char *s) {
  return c_js_program::os_lval(s, -1);
}
Variant cw_js_program$os_constant(const char *s) {
  return c_js_program::os_constant(s);
}
Variant cw_js_program$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_program::os_invoke(c, s, params, -1, fatal);
}
void c_js_program::init() {
  c_js_construct::init();
}
void c_js_program::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_js_program_DupIdsource = null;
}
void csi_js_program() {
  c_js_program::os_static_initializer();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 84 */
void c_js_program::t___construct(Variant v_obj) {
  INSTANCE_METHOD_INJECTION(js_program, js_program::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  (o_lval("src", 0x3F2BD3A200AD45AALL) = v_obj);
  (g->s_js_program_DupIdsource = v_obj);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 88 */
String c_js_program::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_program, js_program::emit);
  Variant v_source;
  String v_o;

  (v_source = LINE(89,o_get("src", 0x3F2BD3A200AD45AALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)));
  (v_o = concat("/* Code generated by J4P5, the javascript interpreter for PHP5\n * http://j4p5.sf.net/ copyright(c) 2005 Henri T. <metal_hurlant@users.sourceforge.net>\n */\n\njs::init();\n", toString(v_source)));
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 332 */
Variant c_js_bit_xor::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_bit_xor::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_bit_xor::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_bit_xor::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_bit_xor::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_bit_xor::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_bit_xor::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_bit_xor::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_bit_xor)
ObjectData *c_js_bit_xor::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_bit_xor::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_bit_xor::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_bit_xor::cloneImpl() {
  c_js_bit_xor *obj = NEW(c_js_bit_xor)();
  cloneSet(obj);
  return obj;
}
void c_js_bit_xor::cloneSet(c_js_bit_xor *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_bit_xor::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_bit_xor::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_bit_xor::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_bit_xor$os_get(const char *s) {
  return c_js_bit_xor::os_get(s, -1);
}
Variant &cw_js_bit_xor$os_lval(const char *s) {
  return c_js_bit_xor::os_lval(s, -1);
}
Variant cw_js_bit_xor$os_constant(const char *s) {
  return c_js_bit_xor::os_constant(s);
}
Variant cw_js_bit_xor$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_bit_xor::os_invoke(c, s, params, -1, fatal);
}
void c_js_bit_xor::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 333 */
void c_js_bit_xor::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_bit_xor, js_bit_xor::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(333,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 357 */
Variant c_js_compound_assign::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_compound_assign::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_compound_assign::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_compound_assign::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_compound_assign::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_compound_assign::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_compound_assign::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_compound_assign::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_compound_assign)
ObjectData *c_js_compound_assign::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_compound_assign::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_compound_assign::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_compound_assign::cloneImpl() {
  c_js_compound_assign *obj = NEW(c_js_compound_assign)();
  cloneSet(obj);
  return obj;
}
void c_js_compound_assign::cloneSet(c_js_compound_assign *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_compound_assign::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_compound_assign::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_compound_assign::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_compound_assign$os_get(const char *s) {
  return c_js_compound_assign::os_get(s, -1);
}
Variant &cw_js_compound_assign$os_lval(const char *s) {
  return c_js_compound_assign::os_lval(s, -1);
}
Variant cw_js_compound_assign$os_constant(const char *s) {
  return c_js_compound_assign::os_constant(s);
}
Variant cw_js_compound_assign$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_compound_assign::os_invoke(c, s, params, -1, fatal);
}
void c_js_compound_assign::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 358 */
void c_js_compound_assign::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_compound_assign, js_compound_assign::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_2(LINE(359,func_get_args(num_args, Array(),args)), lval(o_lval("a", 0x4292CEE227B9150ALL)), lval(o_lval("b", 0x08FBB133F8576BD5LL)), lval(o_lval("op", 0x02E342274B7AAA83LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 361 */
String c_js_compound_assign::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_compound_assign, js_compound_assign::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_s;

  {
    Variant tmp6 = (o_get("op", 0x02E342274B7AAA83LL));
    int tmp7 = -1;
    if (equal(tmp6, ("*="))) {
      tmp7 = 0;
    } else if (equal(tmp6, ("/="))) {
      tmp7 = 1;
    } else if (equal(tmp6, ("%="))) {
      tmp7 = 2;
    } else if (equal(tmp6, ("+="))) {
      tmp7 = 3;
    } else if (equal(tmp6, ("-="))) {
      tmp7 = 4;
    } else if (equal(tmp6, ("<<="))) {
      tmp7 = 5;
    } else if (equal(tmp6, (">>="))) {
      tmp7 = 6;
    } else if (equal(tmp6, (">>>="))) {
      tmp7 = 7;
    } else if (equal(tmp6, ("&="))) {
      tmp7 = 8;
    } else if (equal(tmp6, ("^="))) {
      tmp7 = 9;
    } else if (equal(tmp6, ("|="))) {
      tmp7 = 10;
    }
    switch (tmp7) {
    case 0:
      {
        (v_s = "expr_multiply");
        goto break5;
      }
    case 1:
      {
        (v_s = "expr_divide");
        goto break5;
      }
    case 2:
      {
        (v_s = "expr_modulo");
        goto break5;
      }
    case 3:
      {
        (v_s = "expr_plus");
        goto break5;
      }
    case 4:
      {
        (v_s = "expr_minus");
        goto break5;
      }
    case 5:
      {
        (v_s = "expr_lsh");
        goto break5;
      }
    case 6:
      {
        (v_s = "expr_rsh");
        goto break5;
      }
    case 7:
      {
        (v_s = "expr_ursh");
        goto break5;
      }
    case 8:
      {
        (v_s = "expr_bit_and");
        goto break5;
      }
    case 9:
      {
        (v_s = "expr_bit_xor");
        goto break5;
      }
    case 10:
      {
        (v_s = "expr_bit_or");
        goto break5;
      }
    }
    break5:;
  }
  return concat(concat(concat(concat_rev(toString(LINE(375,o_get("b", 0x08FBB133F8576BD5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), (assignCallTemp(eo_1, toString(o_get("a", 0x4292CEE227B9150ALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 0))),concat3("jsrt::expr_assign(", eo_1, ","))), ",'"), v_s), "')");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 244 */
Variant c_js_delete::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_delete::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_delete::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_delete::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_delete::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_delete::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_delete::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_delete::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_delete)
ObjectData *c_js_delete::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_delete::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_delete::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_delete::cloneImpl() {
  c_js_delete *obj = NEW(c_js_delete)();
  cloneSet(obj);
  return obj;
}
void c_js_delete::cloneSet(c_js_delete *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_delete::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_delete::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_delete::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_delete$os_get(const char *s) {
  return c_js_delete::os_get(s, -1);
}
Variant &cw_js_delete$os_lval(const char *s) {
  return c_js_delete::os_lval(s, -1);
}
Variant cw_js_delete$os_constant(const char *s) {
  return c_js_delete::os_constant(s);
}
Variant cw_js_delete$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_delete::os_invoke(c, s, params, -1, fatal);
}
void c_js_delete::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 245 */
void c_js_delete::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_delete, js_delete::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(245,c_js_unary_op::t___construct(func_get_args(num_args, Array(),args)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 338 */
Variant c_js_and::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_and::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_and::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_and::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_and::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_and::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_and::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_and::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_and)
ObjectData *c_js_and::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_and::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_and::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_and::cloneImpl() {
  c_js_and *obj = NEW(c_js_and)();
  cloneSet(obj);
  return obj;
}
void c_js_and::cloneSet(c_js_and *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_and::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_and::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_and::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_and$os_get(const char *s) {
  return c_js_and::os_get(s, -1);
}
Variant &cw_js_and$os_lval(const char *s) {
  return c_js_and::os_lval(s, -1);
}
Variant cw_js_and$os_constant(const char *s) {
  return c_js_and::os_constant(s);
}
Variant cw_js_and$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_and::os_invoke(c, s, params, -1, fatal);
}
void c_js_and::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 339 */
void c_js_and::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_and, js_and::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(339,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 341 */
String c_js_and::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_and, js_and::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  String v_tmp;

  (v_tmp = LINE(342,c_jsc::t_gensym("sc")));
  return concat(concat_rev(toString(LINE(343,o_get("arg2", 0x30BB46664DFFCCE7LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), (assignCallTemp(eo_1, v_tmp),assignCallTemp(eo_3, toString(o_get("arg1", 0x5779C2A9622744AALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),assignCallTemp(eo_4, concat3(")\?$", v_tmp, ":")),concat5("(!js_bool($", eo_1, "=", eo_3, eo_4))), ")");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 689 */
Variant c_js_this::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_this::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_this::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_this::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_this::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_this::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_this::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_this::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_this)
ObjectData *c_js_this::cloneImpl() {
  c_js_this *obj = NEW(c_js_this)();
  cloneSet(obj);
  return obj;
}
void c_js_this::cloneSet(c_js_this *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_this::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_this::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_this::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_this$os_get(const char *s) {
  return c_js_this::os_get(s, -1);
}
Variant &cw_js_this$os_lval(const char *s) {
  return c_js_this::os_lval(s, -1);
}
Variant cw_js_this$os_constant(const char *s) {
  return c_js_this::os_constant(s);
}
Variant cw_js_this$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_this::os_invoke(c, s, params, -1, fatal);
}
void c_js_this::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 690 */
String c_js_this::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_this, js_this::emit);
  return "jsrt::this()";
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 448 */
Variant c_js_do::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_do::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_do::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_do::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_do::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_do::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_do::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_do::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_do)
ObjectData *c_js_do::create(Variant v_expr, Variant v_statement) {
  init();
  t___construct(v_expr, v_statement);
  return this;
}
ObjectData *c_js_do::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_do::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_do::cloneImpl() {
  c_js_do *obj = NEW(c_js_do)();
  cloneSet(obj);
  return obj;
}
void c_js_do::cloneSet(c_js_do *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_do::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_do::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_do::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_do$os_get(const char *s) {
  return c_js_do::os_get(s, -1);
}
Variant &cw_js_do$os_lval(const char *s) {
  return c_js_do::os_lval(s, -1);
}
Variant cw_js_do$os_constant(const char *s) {
  return c_js_do::os_constant(s);
}
Variant cw_js_do$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_do::os_invoke(c, s, params, -1, fatal);
}
void c_js_do::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 449 */
void c_js_do::t___construct(Variant v_expr, Variant v_statement) {
  INSTANCE_METHOD_INJECTION(js_do, js_do::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("expr", 0x1928DF37EFEC67D5LL) = v_expr);
  (o_lval("statement", 0x50D02CF824CFB5E6LL) = v_statement);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 453 */
String c_js_do::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_do, js_do::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_o;

  g->s_js_source_DupIdnest++;
  (v_o = concat(concat_rev(toString(LINE(455,o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), (assignCallTemp(eo_1, x_rtrim(toString(o_get("statement", 0x50D02CF824CFB5E6LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)))),concat3("do ", eo_1, " while (js_bool("))), "));\n"));
  g->s_js_source_DupIdnest--;
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 19 */
Variant c_jsc::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_jsc::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_jsc::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_jsc::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_jsc::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_jsc::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_jsc::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_jsc::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(jsc)
ObjectData *c_jsc::cloneImpl() {
  c_jsc *obj = NEW(c_jsc)();
  cloneSet(obj);
  return obj;
}
void c_jsc::cloneSet(c_jsc *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_jsc::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_jsc::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_jsc::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_jsc$os_get(const char *s) {
  return c_jsc::os_get(s, -1);
}
Variant &cw_jsc$os_lval(const char *s) {
  return c_jsc::os_lval(s, -1);
}
Variant cw_jsc$os_constant(const char *s) {
  return c_jsc::os_constant(s);
}
Variant cw_jsc$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_jsc::os_invoke(c, s, params, -1, fatal);
}
void c_jsc::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 21 */
String c_jsc::ti_gensym(const char* cls, CStrRef v_prefix //  = ""
) {
  STATIC_METHOD_INJECTION(jsc, jsc::gensym);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_uniq __attribute__((__unused__)) = g->sv_jsc_DupIdgensym_DupIduniq.lvalAt(cls);
  Variant &inited_sv_uniq __attribute__((__unused__)) = g->inited_sv_jsc_DupIdgensym_DupIduniq.lvalAt(cls);
  if (!inited_sv_uniq) {
    (sv_uniq = 0LL);
    inited_sv_uniq = true;
  }
  return concat(v_prefix, toString(++sv_uniq));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 26 */
Variant c_jsc::ti_compile(const char* cls, Variant v_codestr) {
  STATIC_METHOD_INJECTION(jsc, jsc::compile);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_def_fun_track __attribute__((__unused__)) = g->GV(def_fun_track);
  Variant v_def_fun_track;
  Variant &sv_lex __attribute__((__unused__)) = g->sv_jsc_DupIdcompile_DupIdlex.lvalAt(cls);
  Variant &inited_sv_lex __attribute__((__unused__)) = g->inited_sv_jsc_DupIdcompile_DupIdlex.lvalAt(cls);
  Variant v_lex;
  Variant &sv_parser __attribute__((__unused__)) = g->sv_jsc_DupIdcompile_DupIdparser.lvalAt(cls);
  Variant &inited_sv_parser __attribute__((__unused__)) = g->inited_sv_jsc_DupIdcompile_DupIdparser.lvalAt(cls);
  Variant v_parser;
  Variant v_t0;
  Variant v_path;
  Variant v_lexp;
  Variant v_dpa;
  Variant v_t1;
  Variant v_t2;
  Variant v_t3;
  Variant v_program;
  Variant v_t4;
  Variant v_php;
  Variant v_e;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_codestr; Variant &v_def_fun_track; Variant &v_lex; Variant &v_parser; Variant &v_t0; Variant &v_path; Variant &v_lexp; Variant &v_dpa; Variant &v_t1; Variant &v_t2; Variant &v_t3; Variant &v_program; Variant &v_t4; Variant &v_php; Variant &v_e;
    VariableTable(Variant &r_codestr, Variant &r_def_fun_track, Variant &r_lex, Variant &r_parser, Variant &r_t0, Variant &r_path, Variant &r_lexp, Variant &r_dpa, Variant &r_t1, Variant &r_t2, Variant &r_t3, Variant &r_program, Variant &r_t4, Variant &r_php, Variant &r_e) : v_codestr(r_codestr), v_def_fun_track(r_def_fun_track), v_lex(r_lex), v_parser(r_parser), v_t0(r_t0), v_path(r_path), v_lexp(r_lexp), v_dpa(r_dpa), v_t1(r_t1), v_t2(r_t2), v_t3(r_t3), v_program(r_program), v_t4(r_t4), v_php(r_php), v_e(r_e) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      DECLARE_GLOBAL_VARIABLES(g);
      if (hash < 0) hash = hash_string(s);
      switch (hash & 31) {
        case 0:
          HASH_RETURN(0x5253CD732DBF3640LL, v_t0,
                      t0);
          break;
        case 1:
          HASH_RETURN(0x52B182B0A94E9C41LL, v_php,
                      php);
          break;
        case 2:
          HASH_RETURN(0x18A8B9D71D4F2D02LL, v_parser,
                      parser);
          break;
        case 4:
          HASH_RETURN(0x42DD5992F362B3C4LL, v_path,
                      path);
          break;
        case 9:
          HASH_RETURN(0x3BAD1EDAFA606429LL, v_program,
                      program);
          break;
        case 10:
          HASH_RETURN(0x5CAE865FAA95212ALL, v_dpa,
                      dpa);
          break;
        case 11:
          HASH_RETURN(0x61B161496B7EA7EBLL, v_e,
                      e);
          break;
        case 13:
          HASH_RETURN(0x4714454E685BA94DLL, v_lex,
                      lex);
          break;
        case 14:
          HASH_RETURN(0x39D9D6EECF6AE86ELL, v_t2,
                      t2);
          break;
        case 15:
          HASH_RETURN(0x3CEA7575275AC0AFLL, v_t4,
                      t4);
          break;
        case 16:
          HASH_RETURN(0x1BF38F307DF368F0LL, v_def_fun_track,
                      def_fun_track);
          break;
        case 24:
          HASH_RETURN(0x317AE9694A743398LL, v_lexp,
                      lexp);
          break;
        case 26:
          HASH_RETURN(0x45CC4E6F9FE2867ALL, v_t1,
                      t1);
          break;
        case 28:
          HASH_RETURN(0x1FA14A6E547723DCLL, v_codestr,
                      codestr);
          break;
        case 30:
          HASH_RETURN(0x54E310D4C668A31ELL, v_t3,
                      t3);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_codestr, v_def_fun_track, v_lex, v_parser, v_t0, v_path, v_lexp, v_dpa, v_t1, v_t2, v_t3, v_program, v_t4, v_php, v_e);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  v_def_fun_track = ref(g->GV(def_fun_track));
  v_lex = ref(sv_lex);
  if (!inited_sv_lex) {
    setNull(v_lex);
    inited_sv_lex = true;
  }
  v_parser = ref(sv_parser);
  if (!inited_sv_parser) {
    setNull(v_parser);
    inited_sv_parser = true;
  }
  if (equal(v_lex, null)) {
    (v_t0 = LINE(33,x_microtime(toBoolean(1LL))));
    (v_path = concat(LINE(34,x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php"))), "/js.ly.php"));
    if (LINE(35,x_file_exists(toString(v_path)))) {
      LINE(36,include(toString(v_path), false, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/"));
    }
    else {
      (v_path = concat(k_JS_CACHE_DIR, "/js.ly.php"));
      if (LINE(39,x_file_exists(toString(v_path)))) {
        LINE(40,include(toString(v_path), false, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/"));
      }
      else {
        LINE(42,require(concat(x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php")), "/parse/generator.so.php"), true, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/"));
        (v_lexp = LINE(43,(assignCallTemp(eo_0, concat(x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php")), "/js.l")),invoke_failed("generate_scanner_from_file", Array(ArrayInit(2).set(0, eo_0).set(1, 0LL).create()), 0x00000000326215A6LL))).o_get("pattern", 0x029C0A5F2FB04920LL));
        (v_dpa = LINE(44,invoke_failed("generate_parser_from_file", Array(ArrayInit(1).set(0, concat(x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php")), "/js.y")).create()), 0x000000005A4524E6LL)));
        LINE(48,(assignCallTemp(eo_0, toString(v_path)),assignCallTemp(eo_1, concat(concat_rev(toString(x_var_export(v_dpa, toBoolean(1LL))), (assignCallTemp(eo_3, toString(g->GV(func_def))),assignCallTemp(eo_5, toString(x_var_export(v_lexp, toBoolean(1LL)))),concat5("<\?php\n// This file is dynamically generated from js.l and js.y by metaphp's CFG parser\n// Do not waste your time editing it or reading it. Move along. Thank you.\n\n", eo_3, "$lexp = ", eo_5, ";\n$dpa = "))), ";\n\?>")),x_file_put_contents(eo_0, eo_1)));
      }
    }
    (v_t1 = LINE(51,x_microtime(toBoolean(1LL))));
    (v_lex = LINE(52,create_object("preg_scanner", Array(ArrayInit(2).set(0, 0LL).set(1, v_lexp).create()))));
    (v_parser = LINE(53,create_object("easy_parser", Array(ArrayInit(1).set(0, v_dpa).create()))));
    (v_t2 = LINE(54,x_microtime(toBoolean(1LL))));
  }
  (v_t3 = LINE(58,x_microtime(toBoolean(1LL))));
  LINE(59,v_lex.o_invoke_few_args("start", 0x3970634433FD3E52LL, 1, v_codestr));
  (v_program = LINE(60,v_parser.o_invoke_few_args("parse", 0x46463F1C3624CEDELL, 2, "Program", v_lex)));
  (v_t4 = LINE(61,x_microtime(toBoolean(1LL))));
  try {
    (v_php = LINE(66,v_program.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 0)));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      echo(LINE(69,concat3("Compilation Error: ", toString(v_e.o_get("value", 0x69E7413AE0C88471LL).o_get("msg", 0x05910C53309AEFFFLL)), "<hr>")));
    } else {
      throw;
    }
  }
  return v_php;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 253 */
Variant c_js_pre_pp::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_pre_pp::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_pre_pp::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_pre_pp::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_pre_pp::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_pre_pp::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_pre_pp::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_pre_pp::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_pre_pp)
ObjectData *c_js_pre_pp::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_pre_pp::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_pre_pp::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_pre_pp::cloneImpl() {
  c_js_pre_pp *obj = NEW(c_js_pre_pp)();
  cloneSet(obj);
  return obj;
}
void c_js_pre_pp::cloneSet(c_js_pre_pp *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_pre_pp::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_pre_pp::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_pre_pp::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_pre_pp$os_get(const char *s) {
  return c_js_pre_pp::os_get(s, -1);
}
Variant &cw_js_pre_pp$os_lval(const char *s) {
  return c_js_pre_pp::os_lval(s, -1);
}
Variant cw_js_pre_pp$os_constant(const char *s) {
  return c_js_pre_pp::os_constant(s);
}
Variant cw_js_pre_pp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_pre_pp::os_invoke(c, s, params, -1, fatal);
}
void c_js_pre_pp::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 254 */
void c_js_pre_pp::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_pre_pp, js_pre_pp::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(254,c_js_unary_op::t___construct(func_get_args(num_args, Array(),args)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 320 */
Variant c_js_not_equal::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_not_equal::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_not_equal::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_not_equal::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_not_equal::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_not_equal::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_not_equal::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_not_equal::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_not_equal)
ObjectData *c_js_not_equal::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_not_equal::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_not_equal::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_not_equal::cloneImpl() {
  c_js_not_equal *obj = NEW(c_js_not_equal)();
  cloneSet(obj);
  return obj;
}
void c_js_not_equal::cloneSet(c_js_not_equal *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_not_equal::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_not_equal::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_not_equal::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_not_equal$os_get(const char *s) {
  return c_js_not_equal::os_get(s, -1);
}
Variant &cw_js_not_equal$os_lval(const char *s) {
  return c_js_not_equal::os_lval(s, -1);
}
Variant cw_js_not_equal$os_constant(const char *s) {
  return c_js_not_equal::os_constant(s);
}
Variant cw_js_not_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_not_equal::os_invoke(c, s, params, -1, fatal);
}
void c_js_not_equal::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 321 */
void c_js_not_equal::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_not_equal, js_not_equal::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(321,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 858 */
Variant c_js_accessor::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_accessor::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_accessor::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_accessor::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_accessor::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_accessor::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_accessor::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_accessor::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_accessor)
ObjectData *c_js_accessor::create(int num_args, Variant v_obj, Variant v_member, Variant v_resolve, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_obj, v_member, v_resolve,args);
  return this;
}
ObjectData *c_js_accessor::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 3) return (create(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
  } else return this;
}
void c_js_accessor::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 3) (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
}
ObjectData *c_js_accessor::cloneImpl() {
  c_js_accessor *obj = NEW(c_js_accessor)();
  cloneSet(obj);
  return obj;
}
void c_js_accessor::cloneSet(c_js_accessor *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_accessor::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 3) return (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_accessor::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 3) return (t___construct(count, a0, a1, a2), null);
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, a2, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_accessor::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_accessor$os_get(const char *s) {
  return c_js_accessor::os_get(s, -1);
}
Variant &cw_js_accessor$os_lval(const char *s) {
  return c_js_accessor::os_lval(s, -1);
}
Variant cw_js_accessor$os_constant(const char *s) {
  return c_js_accessor::os_constant(s);
}
Variant cw_js_accessor$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_accessor::os_invoke(c, s, params, -1, fatal);
}
void c_js_accessor::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 859 */
void c_js_accessor::t___construct(int num_args, Variant v_obj, Variant v_member, Variant v_resolve, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_accessor, js_accessor::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_12(LINE(860,func_get_args(num_args, Array(ArrayInit(3).set(0, v_obj).set(1, v_member).set(2, v_resolve).create()),args)), lval(o_lval("obj", 0x7FB577570F61BD03LL)), lval(o_lval("member", 0x7D0BEDECC8D925AELL)), lval(o_lval("resolve", 0x0614E2BCCABDE0B3LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 862 */
String c_js_accessor::t_emit(CVarRef v_wantvalue //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_accessor, js_accessor::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant v_v;

  (v_v = toBoolean(v_wantvalue) ? (("v")) : (("")));
  return concat(concat_rev(toString(LINE(864,o_get("member", 0x7D0BEDECC8D925AELL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, o_lval("resolve", 0x0614E2BCCABDE0B3LL)))), (assignCallTemp(eo_1, toString(v_v)),assignCallTemp(eo_3, toString(o_get("obj", 0x7FB577570F61BD03LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat5("jsrt::dot", eo_1, "(", eo_3, ","))), ")");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 421 */
Variant c_js_block::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_block::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_block::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_block::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_block::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_block::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_block::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_block::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_block)
ObjectData *c_js_block::create(Variant v_a) {
  init();
  t___construct(v_a);
  return this;
}
ObjectData *c_js_block::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_block::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_block::cloneImpl() {
  c_js_block *obj = NEW(c_js_block)();
  cloneSet(obj);
  return obj;
}
void c_js_block::cloneSet(c_js_block *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_block::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_block::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_block::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_block$os_get(const char *s) {
  return c_js_block::os_get(s, -1);
}
Variant &cw_js_block$os_lval(const char *s) {
  return c_js_block::os_lval(s, -1);
}
Variant cw_js_block$os_constant(const char *s) {
  return c_js_block::os_constant(s);
}
Variant cw_js_block$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_block::os_invoke(c, s, params, -1, fatal);
}
void c_js_block::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 422 */
void c_js_block::t___construct(Variant v_a) {
  INSTANCE_METHOD_INJECTION(js_block, js_block::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("statements", 0x6DC77E521E1231FBLL) = v_a);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 425 */
String c_js_block::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_block, js_block::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  String v_o;
  Variant v_statement;

  (v_o = "{\n");
  {
    LOOP_COUNTER(8);
    Variant map9 = o_get("statements", 0x6DC77E521E1231FBLL);
    for (ArrayIterPtr iter10 = map9.begin("js_block"); !iter10->end(); iter10->next()) {
      LOOP_COUNTER_CHECK(8);
      v_statement = iter10->second();
      {
        concat_assign(v_o, LINE(428,(assignCallTemp(eo_1, x_trim(toString((assignCallTemp(eo_5, v_statement.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)),x_str_replace("\n", "\n  ", eo_5))))),concat3("  ", eo_1, "\n"))));
      }
    }
  }
  concat_assign(v_o, "}\n");
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 746 */
Variant c_js_literal_number::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_literal_number::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_literal_number::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_literal_number::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_literal_number::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_literal_number::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_literal_number::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_literal_number::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_literal_number)
ObjectData *c_js_literal_number::create(Variant v_v) {
  init();
  t___construct(v_v);
  return this;
}
ObjectData *c_js_literal_number::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_literal_number::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_literal_number::cloneImpl() {
  c_js_literal_number *obj = NEW(c_js_literal_number)();
  cloneSet(obj);
  return obj;
}
void c_js_literal_number::cloneSet(c_js_literal_number *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_literal_number::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_literal_number::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_literal_number::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_literal_number$os_get(const char *s) {
  return c_js_literal_number::os_get(s, -1);
}
Variant &cw_js_literal_number$os_lval(const char *s) {
  return c_js_literal_number::os_lval(s, -1);
}
Variant cw_js_literal_number$os_constant(const char *s) {
  return c_js_literal_number::os_constant(s);
}
Variant cw_js_literal_number$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_literal_number::os_invoke(c, s, params, -1, fatal);
}
void c_js_literal_number::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 747 */
void c_js_literal_number::t___construct(Variant v_v) {
  INSTANCE_METHOD_INJECTION(js_literal_number, js_literal_number::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("v", 0x3C2F961831E4EF6BLL) = v_v);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 750 */
String c_js_literal_number::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_literal_number, js_literal_number::emit);
  return LINE(751,concat3("js_int(", toString(o_get("v", 0x3C2F961831E4EF6BLL)), ")"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 738 */
Variant c_js_literal_boolean::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_literal_boolean::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_literal_boolean::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_literal_boolean::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_literal_boolean::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_literal_boolean::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_literal_boolean::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_literal_boolean::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_literal_boolean)
ObjectData *c_js_literal_boolean::create(Variant v_v) {
  init();
  t___construct(v_v);
  return this;
}
ObjectData *c_js_literal_boolean::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_literal_boolean::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_literal_boolean::cloneImpl() {
  c_js_literal_boolean *obj = NEW(c_js_literal_boolean)();
  cloneSet(obj);
  return obj;
}
void c_js_literal_boolean::cloneSet(c_js_literal_boolean *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_literal_boolean::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_literal_boolean::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_literal_boolean::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_literal_boolean$os_get(const char *s) {
  return c_js_literal_boolean::os_get(s, -1);
}
Variant &cw_js_literal_boolean$os_lval(const char *s) {
  return c_js_literal_boolean::os_lval(s, -1);
}
Variant cw_js_literal_boolean$os_constant(const char *s) {
  return c_js_literal_boolean::os_constant(s);
}
Variant cw_js_literal_boolean$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_literal_boolean::os_invoke(c, s, params, -1, fatal);
}
void c_js_literal_boolean::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 739 */
void c_js_literal_boolean::t___construct(Variant v_v) {
  INSTANCE_METHOD_INJECTION(js_literal_boolean, js_literal_boolean::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("v", 0x3C2F961831E4EF6BLL) = v_v);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 742 */
Variant c_js_literal_boolean::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_literal_boolean, js_literal_boolean::emit);
  return toBoolean(o_get("v", 0x3C2F961831E4EF6BLL)) ? (("jsrt::$true")) : (("jsrt::$false"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 434 */
Variant c_js_if::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_if::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_if::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_if::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_if::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_if::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_if::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_if::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_if)
ObjectData *c_js_if::create(Variant v_cond, Variant v_ifblock, Variant v_elseblock //  = null
) {
  init();
  t___construct(v_cond, v_ifblock, v_elseblock);
  return this;
}
ObjectData *c_js_if::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_js_if::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_js_if::cloneImpl() {
  c_js_if *obj = NEW(c_js_if)();
  cloneSet(obj);
  return obj;
}
void c_js_if::cloneSet(c_js_if *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_if::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_if::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_if::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_if$os_get(const char *s) {
  return c_js_if::os_get(s, -1);
}
Variant &cw_js_if$os_lval(const char *s) {
  return c_js_if::os_lval(s, -1);
}
Variant cw_js_if$os_constant(const char *s) {
  return c_js_if::os_constant(s);
}
Variant cw_js_if$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_if::os_invoke(c, s, params, -1, fatal);
}
void c_js_if::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 435 */
void c_js_if::t___construct(Variant v_cond, Variant v_ifblock, Variant v_elseblock //  = null
) {
  INSTANCE_METHOD_INJECTION(js_if, js_if::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("cond", 0x682F5D755B5A03E7LL) = v_cond);
  (o_lval("ifblock", 0x55C94636AF7D62C5LL) = v_ifblock);
  (o_lval("elseblock", 0x04464A9027B54E90LL) = v_elseblock);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 440 */
String c_js_if::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_if, js_if::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_o;

  (v_o = concat_rev(toString(LINE(441,o_get("ifblock", 0x55C94636AF7D62C5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), (assignCallTemp(eo_1, toString(o_get("cond", 0x682F5D755B5A03E7LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("if (js_bool(", eo_1, ")) "))));
  if (toBoolean(o_get("elseblock", 0x04464A9027B54E90LL))) {
    (v_o = concat(concat_rev(toString(LINE(443,o_get("elseblock", 0x04464A9027B54E90LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), concat(x_rtrim(v_o), " else ")), "\n"));
  }
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 302 */
Variant c_js_gt::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_gt::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_gt::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_gt::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_gt::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_gt::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_gt::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_gt::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_gt)
ObjectData *c_js_gt::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_gt::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_gt::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_gt::cloneImpl() {
  c_js_gt *obj = NEW(c_js_gt)();
  cloneSet(obj);
  return obj;
}
void c_js_gt::cloneSet(c_js_gt *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_gt::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_gt::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_gt::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_gt$os_get(const char *s) {
  return c_js_gt::os_get(s, -1);
}
Variant &cw_js_gt$os_lval(const char *s) {
  return c_js_gt::os_lval(s, -1);
}
Variant cw_js_gt$os_constant(const char *s) {
  return c_js_gt::os_constant(s);
}
Variant cw_js_gt$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_gt::os_invoke(c, s, params, -1, fatal);
}
void c_js_gt::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 303 */
void c_js_gt::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_gt, js_gt::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(303,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 754 */
Variant c_js_literal_string::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_literal_string::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_literal_string::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_literal_string::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_literal_string::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_literal_string::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_literal_string::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_literal_string::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_literal_string)
ObjectData *c_js_literal_string::create(Variant v_a, Variant v_stripquotes //  = 1LL
) {
  init();
  t___construct(v_a, v_stripquotes);
  return this;
}
ObjectData *c_js_literal_string::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_literal_string::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_literal_string::cloneImpl() {
  c_js_literal_string *obj = NEW(c_js_literal_string)();
  cloneSet(obj);
  return obj;
}
void c_js_literal_string::cloneSet(c_js_literal_string *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_literal_string::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_literal_string::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_literal_string::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_literal_string$os_get(const char *s) {
  return c_js_literal_string::os_get(s, -1);
}
Variant &cw_js_literal_string$os_lval(const char *s) {
  return c_js_literal_string::os_lval(s, -1);
}
Variant cw_js_literal_string$os_constant(const char *s) {
  return c_js_literal_string::os_constant(s);
}
Variant cw_js_literal_string$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_literal_string::os_invoke(c, s, params, -1, fatal);
}
void c_js_literal_string::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 756 */
void c_js_literal_string::t___construct(Variant v_a, Variant v_stripquotes //  = 1LL
) {
  INSTANCE_METHOD_INJECTION(js_literal_string, js_literal_string::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  if (toBoolean(v_stripquotes)) {
    (v_a = LINE(758,(assignCallTemp(eo_0, toString(v_a)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_a)) - 2LL)),x_substr(eo_0, toInt32(1LL), eo_2))));
  }
  (o_lval("str", 0x362158638F83BD9CLL) = LINE(760,t_parse_string(v_a)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 763 */
Variant c_js_literal_string::t_parse_string(CVarRef v_str) {
  INSTANCE_METHOD_INJECTION(js_literal_string, js_literal_string::parse_string);
  Variant v_out;
  int64 v_mode = 0;
  Variant v_c;
  Variant v_b;

  (v_out = "");
  (v_mode = 0LL);
  {
    LOOP_COUNTER(11);
    Variant map12 = LINE(766,x_str_split(toString(v_str)));
    for (ArrayIterPtr iter13 = map12.begin("js_literal_string"); !iter13->end(); iter13->next()) {
      LOOP_COUNTER_CHECK(11);
      v_c = iter13->second();
      {
        {
          switch (v_mode) {
          case 0LL:
            {
              if (equal(v_c, "\\")) {
                (v_mode = 1LL);
              }
              else {
                concat_assign(v_out, toString(v_c));
              }
              break;
            }
          case 1LL:
            {
              (v_mode = 0LL);
              {
                Variant tmp16 = (v_c);
                int tmp17 = -1;
                if (equal(tmp16, ("'"))) {
                  tmp17 = 0;
                } else if (equal(tmp16, ("\""))) {
                  tmp17 = 1;
                } else if (equal(tmp16, ("\\"))) {
                  tmp17 = 2;
                } else if (equal(tmp16, ("b"))) {
                  tmp17 = 3;
                } else if (equal(tmp16, ("f"))) {
                  tmp17 = 4;
                } else if (equal(tmp16, ("n"))) {
                  tmp17 = 5;
                } else if (equal(tmp16, ("r"))) {
                  tmp17 = 6;
                } else if (equal(tmp16, ("t"))) {
                  tmp17 = 7;
                } else if (equal(tmp16, ("v"))) {
                  tmp17 = 8;
                } else if (equal(tmp16, ("0"))) {
                  tmp17 = 9;
                } else if (equal(tmp16, ("x"))) {
                  tmp17 = 10;
                } else if (equal(tmp16, ("u"))) {
                  tmp17 = 11;
                } else if (true) {
                  tmp17 = 12;
                }
                switch (tmp17) {
                case 0:
                  {
                    concat_assign(v_out, "'");
                    goto break15;
                  }
                case 1:
                  {
                    concat_assign(v_out, "\"");
                    goto break15;
                  }
                case 2:
                  {
                    concat_assign(v_out, "\\");
                    goto break15;
                  }
                case 3:
                  {
                    concat_assign(v_out, LINE(781,x_chr(8LL)));
                    goto break15;
                  }
                case 4:
                  {
                    concat_assign(v_out, LINE(782,x_chr(12LL)));
                    goto break15;
                  }
                case 5:
                  {
                    concat_assign(v_out, LINE(783,x_chr(10LL)));
                    goto break15;
                  }
                case 6:
                  {
                    concat_assign(v_out, LINE(784,x_chr(13LL)));
                    goto break15;
                  }
                case 7:
                  {
                    concat_assign(v_out, LINE(785,x_chr(9LL)));
                    goto break15;
                  }
                case 8:
                  {
                    concat_assign(v_out, LINE(786,x_chr(11LL)));
                    goto break15;
                  }
                case 9:
                  {
                    concat_assign(v_out, LINE(787,x_chr(0LL)));
                    goto break15;
                  }
                case 10:
                  {
                    (v_mode = 2LL);
                    (v_b = "");
                    goto break15;
                  }
                case 11:
                  {
                    (v_mode = 4LL);
                    (v_b = "");
                    goto break15;
                  }
                case 12:
                  {
                    concat_assign(v_out, toString(v_c));
                    goto break15;
                  }
                }
                break15:;
              }
              break;
            }
          case 2LL:
            {
              concat_assign(v_b, toString(v_c));
              if (!same(LINE(796,x_stripos("0123456789abcdef", v_c)), false)) {
                (v_mode = 3LL);
              }
              else {
                concat_assign(v_out, concat("x", toString(v_b)));
                (v_mode = 0LL);
              }
              break;
            }
          case 3LL:
            {
              concat_assign(v_b, toString(v_c));
              if (!same(LINE(805,x_stripos("0123456789abcdef", v_c)), false)) {
                concat_assign(v_out, LINE(806,x_chr(toInt64(x_hexdec(toString(v_b))))));
                (v_mode = 0LL);
              }
              else {
                concat_assign(v_out, concat("x", toString(v_b)));
                (v_mode = 0LL);
              }
              break;
            }
          case 4LL:
            {
              concat_assign(v_b, toString(v_c));
              if (!same(LINE(815,x_stripos("0123456789abcdef", v_c)), false)) {
                (v_mode = 5LL);
              }
              else {
                concat_assign(v_out, concat("u", toString(v_b)));
                (v_mode = 0LL);
              }
              break;
            }
          case 5LL:
            {
              concat_assign(v_b, toString(v_c));
              if (!same(LINE(824,x_stripos("0123456789abcdef", v_c)), false)) {
                (v_mode = 6LL);
              }
              else {
                concat_assign(v_out, concat("u", toString(v_b)));
                (v_mode = 0LL);
              }
              break;
            }
          case 6LL:
            {
              concat_assign(v_b, toString(v_c));
              if (!same(LINE(833,x_stripos("0123456789abcdef", v_c)), false)) {
                (v_mode = 7LL);
              }
              else {
                concat_assign(v_out, concat("u", toString(v_b)));
                (v_mode = 0LL);
              }
              break;
            }
          case 7LL:
            {
              concat_assign(v_b, toString(v_c));
              if (!same(LINE(842,x_stripos("0123456789abcdef", v_c)), false)) {
                concat_assign(v_out, LINE(843,x_chr(toInt64(x_hexdec(toString(v_b))))));
                (v_mode = 0LL);
              }
              else {
                concat_assign(v_out, concat("u", toString(v_b)));
                (v_mode = 0LL);
              }
              break;
            }
          }
        }
      }
    }
  }
  return v_out;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 854 */
String c_js_literal_string::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_literal_string, js_literal_string::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return LINE(855,(assignCallTemp(eo_1, toString(x_var_export(o_get("str", 0x362158638F83BD9CLL), toBoolean(1LL)))),concat3("js_str(", eo_1, ")")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 886 */
Variant c_js_call::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_call::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_call::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_call::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_call::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_call::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_call::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_call::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_call)
ObjectData *c_js_call::create(int num_args, Variant v_expr, Variant v_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_expr, v_args,args);
  return this;
}
ObjectData *c_js_call::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_js_call::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_js_call::cloneImpl() {
  c_js_call *obj = NEW(c_js_call)();
  cloneSet(obj);
  return obj;
}
void c_js_call::cloneSet(c_js_call *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_call::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_call::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_call::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_call$os_get(const char *s) {
  return c_js_call::os_get(s, -1);
}
Variant &cw_js_call$os_lval(const char *s) {
  return c_js_call::os_lval(s, -1);
}
Variant cw_js_call$os_constant(const char *s) {
  return c_js_call::os_constant(s);
}
Variant cw_js_call$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_call::os_invoke(c, s, params, -1, fatal);
}
void c_js_call::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 887 */
void c_js_call::t___construct(int num_args, Variant v_expr, Variant v_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_call, js_call::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_14(LINE(888,func_get_args(num_args, Array(ArrayInit(2).set(0, v_expr).set(1, v_args).create()),args)), lval(o_lval("expr", 0x1928DF37EFEC67D5LL)), lval(o_lval("args", 0x4AF7CD17F976719ELL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 890 */
String c_js_call::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_call, js_call::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Array v_args;
  Variant v_arg;

  (v_args = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(18);
    Variant map19 = o_get("args", 0x4AF7CD17F976719ELL);
    for (ArrayIterPtr iter20 = map19.begin("js_call"); !iter20->end(); iter20->next()) {
      LOOP_COUNTER_CHECK(18);
      v_arg = iter20->second();
      {
        v_args.append((LINE(893,v_arg.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
      }
    }
  }
  return concat(concat_rev(LINE(895,x_implode(",", v_args)), (assignCallTemp(eo_1, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 0))),concat3("jsrt::call(", eo_1, ", array("))), "))");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 283 */
Variant c_js_minus::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_minus::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_minus::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_minus::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_minus::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_minus::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_minus::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_minus::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_minus)
ObjectData *c_js_minus::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_minus::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_minus::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_minus::cloneImpl() {
  c_js_minus *obj = NEW(c_js_minus)();
  cloneSet(obj);
  return obj;
}
void c_js_minus::cloneSet(c_js_minus *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_minus::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_minus::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_minus::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_minus$os_get(const char *s) {
  return c_js_minus::os_get(s, -1);
}
Variant &cw_js_minus$os_lval(const char *s) {
  return c_js_minus::os_lval(s, -1);
}
Variant cw_js_minus$os_constant(const char *s) {
  return c_js_minus::os_constant(s);
}
Variant cw_js_minus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_minus::os_invoke(c, s, params, -1, fatal);
}
void c_js_minus::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 284 */
void c_js_minus::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_minus, js_minus::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(284,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 247 */
Variant c_js_void::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_void::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_void::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_void::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_void::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_void::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_void::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_void::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_void)
ObjectData *c_js_void::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_void::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_void::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_void::cloneImpl() {
  c_js_void *obj = NEW(c_js_void)();
  cloneSet(obj);
  return obj;
}
void c_js_void::cloneSet(c_js_void *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_void::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_void::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_void::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_void$os_get(const char *s) {
  return c_js_void::os_get(s, -1);
}
Variant &cw_js_void$os_lval(const char *s) {
  return c_js_void::os_lval(s, -1);
}
Variant cw_js_void$os_constant(const char *s) {
  return c_js_void::os_constant(s);
}
Variant cw_js_void$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_void::os_invoke(c, s, params, -1, fatal);
}
void c_js_void::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 248 */
void c_js_void::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_void, js_void::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  LINE(248,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_unary_op::t___construct(eo_0, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 314 */
Variant c_js_in::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_in::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_in::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_in::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_in::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_in::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_in::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_in::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_in)
ObjectData *c_js_in::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_in::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_in::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_in::cloneImpl() {
  c_js_in *obj = NEW(c_js_in)();
  cloneSet(obj);
  return obj;
}
void c_js_in::cloneSet(c_js_in *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_in::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_in::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_in::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_in$os_get(const char *s) {
  return c_js_in::os_get(s, -1);
}
Variant &cw_js_in$os_lval(const char *s) {
  return c_js_in::os_lval(s, -1);
}
Variant cw_js_in$os_constant(const char *s) {
  return c_js_in::os_constant(s);
}
Variant cw_js_in$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_in::os_invoke(c, s, params, -1, fatal);
}
void c_js_in::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 315 */
void c_js_in::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_in, js_in::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(315,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 721 */
Variant c_js_literal_object::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_literal_object::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_literal_object::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_literal_object::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_literal_object::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_literal_object::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_literal_object::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_literal_object::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_literal_object)
ObjectData *c_js_literal_object::create(Variant v_o //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_o);
  return this;
}
ObjectData *c_js_literal_object::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_literal_object::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_literal_object::cloneImpl() {
  c_js_literal_object *obj = NEW(c_js_literal_object)();
  cloneSet(obj);
  return obj;
}
void c_js_literal_object::cloneSet(c_js_literal_object *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_literal_object::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_literal_object::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_literal_object::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_literal_object$os_get(const char *s) {
  return c_js_literal_object::os_get(s, -1);
}
Variant &cw_js_literal_object$os_lval(const char *s) {
  return c_js_literal_object::os_lval(s, -1);
}
Variant cw_js_literal_object$os_constant(const char *s) {
  return c_js_literal_object::os_constant(s);
}
Variant cw_js_literal_object$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_literal_object::os_invoke(c, s, params, -1, fatal);
}
void c_js_literal_object::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 722 */
void c_js_literal_object::t___construct(Variant v_o //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(js_literal_object, js_literal_object::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("obj", 0x7FB577570F61BD03LL) = v_o);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 725 */
String c_js_literal_object::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_literal_object, js_literal_object::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Array v_a;
  int64 v_i = 0;

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(21);
    for ((v_i = 0LL); less(v_i, LINE(727,x_count(o_get("obj", 0x7FB577570F61BD03LL)))); v_i++) {
      LOOP_COUNTER_CHECK(21);
      {
        v_a.append((LINE(728,o_get("obj", 0x7FB577570F61BD03LL).rvalAt(v_i).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 0))));
      }
    }
  }
  return LINE(730,(assignCallTemp(eo_1, x_implode(",", v_a)),concat3("jsrt::literal_object(", eo_1, ")")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 292 */
Variant c_js_ursh::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_ursh::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_ursh::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_ursh::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_ursh::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_ursh::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_ursh::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_ursh::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_ursh)
ObjectData *c_js_ursh::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_ursh::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_ursh::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_ursh::cloneImpl() {
  c_js_ursh *obj = NEW(c_js_ursh)();
  cloneSet(obj);
  return obj;
}
void c_js_ursh::cloneSet(c_js_ursh *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_ursh::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_ursh::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_ursh::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_ursh$os_get(const char *s) {
  return c_js_ursh::os_get(s, -1);
}
Variant &cw_js_ursh$os_lval(const char *s) {
  return c_js_ursh::os_lval(s, -1);
}
Variant cw_js_ursh$os_constant(const char *s) {
  return c_js_ursh::os_constant(s);
}
Variant cw_js_ursh$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_ursh::os_invoke(c, s, params, -1, fatal);
}
void c_js_ursh::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 293 */
void c_js_ursh::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_ursh, js_ursh::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(293,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 216 */
Variant c_js_binary_op::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_binary_op::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_binary_op::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_binary_op::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_binary_op::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_binary_op::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_binary_op::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_binary_op::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_binary_op)
ObjectData *c_js_binary_op::create(Variant v_a, Variant v_w1 //  = 0LL
, Variant v_w2 //  = 0LL
) {
  init();
  t___construct(v_a, v_w1, v_w2);
  return this;
}
ObjectData *c_js_binary_op::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_js_binary_op::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_js_binary_op::cloneImpl() {
  c_js_binary_op *obj = NEW(c_js_binary_op)();
  cloneSet(obj);
  return obj;
}
void c_js_binary_op::cloneSet(c_js_binary_op *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_binary_op::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_binary_op::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_binary_op::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_binary_op$os_get(const char *s) {
  return c_js_binary_op::os_get(s, -1);
}
Variant &cw_js_binary_op$os_lval(const char *s) {
  return c_js_binary_op::os_lval(s, -1);
}
Variant cw_js_binary_op$os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
Variant cw_js_binary_op$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_binary_op::os_invoke(c, s, params, -1, fatal);
}
void c_js_binary_op::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 217 */
void c_js_binary_op::t___construct(Variant v_a, Variant v_w1 //  = 0LL
, Variant v_w2 //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_binary_op, js_binary_op::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  (o_lval("arg1", 0x5779C2A9622744AALL) = v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (o_lval("arg2", 0x30BB46664DFFCCE7LL) = v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  (o_lval("wantValue1", 0x791EB9E3E71447AFLL) = v_w1);
  (o_lval("wantValue2", 0x52EA9250AC26E002LL) = v_w2);
  (o_lval("jsrt_op", 0x72BCAA5292848C70LL) = LINE(222,(assignCallTemp(eo_0, toString(x_get_class(((Object)(this))))),x_substr(eo_0, toInt32(3LL)))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 224 */
String c_js_binary_op::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_binary_op, js_binary_op::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  return concat(concat_rev(toString(LINE(225,o_get("arg2", 0x30BB46664DFFCCE7LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, o_lval("wantValue2", 0x52EA9250AC26E002LL)))), (assignCallTemp(eo_1, toString(o_get("jsrt_op", 0x72BCAA5292848C70LL))),assignCallTemp(eo_3, toString(o_get("arg1", 0x5779C2A9622744AALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, o_lval("wantValue1", 0x791EB9E3E71447AFLL)))),concat5("jsrt::expr_", eo_1, "(", eo_3, ","))), ")");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 378 */
Variant c_js_comma::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_comma::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_comma::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_comma::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_comma::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_comma::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_comma::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_comma::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_comma)
ObjectData *c_js_comma::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_comma::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_comma::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_comma::cloneImpl() {
  c_js_comma *obj = NEW(c_js_comma)();
  cloneSet(obj);
  return obj;
}
void c_js_comma::cloneSet(c_js_comma *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_comma::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_comma::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_comma::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_comma$os_get(const char *s) {
  return c_js_comma::os_get(s, -1);
}
Variant &cw_js_comma$os_lval(const char *s) {
  return c_js_comma::os_lval(s, -1);
}
Variant cw_js_comma$os_constant(const char *s) {
  return c_js_comma::os_constant(s);
}
Variant cw_js_comma$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_comma::os_invoke(c, s, params, -1, fatal);
}
void c_js_comma::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 379 */
void c_js_comma::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_comma, js_comma::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(379,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 271 */
Variant c_js_multiply::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_multiply::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_multiply::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_multiply::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_multiply::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_multiply::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_multiply::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_multiply::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_multiply)
ObjectData *c_js_multiply::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_multiply::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_multiply::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_multiply::cloneImpl() {
  c_js_multiply *obj = NEW(c_js_multiply)();
  cloneSet(obj);
  return obj;
}
void c_js_multiply::cloneSet(c_js_multiply *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_multiply::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_multiply::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_multiply::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_multiply$os_get(const char *s) {
  return c_js_multiply::os_get(s, -1);
}
Variant &cw_js_multiply$os_lval(const char *s) {
  return c_js_multiply::os_lval(s, -1);
}
Variant cw_js_multiply$os_constant(const char *s) {
  return c_js_multiply::os_constant(s);
}
Variant cw_js_multiply$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_multiply::os_invoke(c, s, params, -1, fatal);
}
void c_js_multiply::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 272 */
void c_js_multiply::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_multiply, js_multiply::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(272,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 326 */
Variant c_js_strict_not_equal::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_strict_not_equal::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_strict_not_equal::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_strict_not_equal::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_strict_not_equal::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_strict_not_equal::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_strict_not_equal::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_strict_not_equal::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_strict_not_equal)
ObjectData *c_js_strict_not_equal::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_strict_not_equal::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_strict_not_equal::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_strict_not_equal::cloneImpl() {
  c_js_strict_not_equal *obj = NEW(c_js_strict_not_equal)();
  cloneSet(obj);
  return obj;
}
void c_js_strict_not_equal::cloneSet(c_js_strict_not_equal *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_strict_not_equal::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_strict_not_equal::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_strict_not_equal::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_strict_not_equal$os_get(const char *s) {
  return c_js_strict_not_equal::os_get(s, -1);
}
Variant &cw_js_strict_not_equal$os_lval(const char *s) {
  return c_js_strict_not_equal::os_lval(s, -1);
}
Variant cw_js_strict_not_equal$os_constant(const char *s) {
  return c_js_strict_not_equal::os_constant(s);
}
Variant cw_js_strict_not_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_strict_not_equal::os_invoke(c, s, params, -1, fatal);
}
void c_js_strict_not_equal::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 327 */
void c_js_strict_not_equal::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_strict_not_equal, js_strict_not_equal::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(327,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 295 */
Variant c_js_lt::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_lt::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_lt::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_lt::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_lt::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_lt::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_lt::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_lt::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_lt)
ObjectData *c_js_lt::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_lt::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_lt::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_lt::cloneImpl() {
  c_js_lt *obj = NEW(c_js_lt)();
  cloneSet(obj);
  return obj;
}
void c_js_lt::cloneSet(c_js_lt *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_lt::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_lt::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_lt::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_lt$os_get(const char *s) {
  return c_js_lt::os_get(s, -1);
}
Variant &cw_js_lt$os_lval(const char *s) {
  return c_js_lt::os_lval(s, -1);
}
Variant cw_js_lt$os_constant(const char *s) {
  return c_js_lt::os_constant(s);
}
Variant cw_js_lt$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_lt::os_invoke(c, s, params, -1, fatal);
}
void c_js_lt::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 296 */
void c_js_lt::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_lt, js_lt::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(296,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 297 */
String c_js_lt::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_lt, js_lt::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return concat(concat_rev(toString(LINE(299,o_get("arg2", 0x30BB46664DFFCCE7LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), (assignCallTemp(eo_1, toString(o_get("arg1", 0x5779C2A9622744AALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("jsrt::cmp(", eo_1, ","))), ", 1)");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 280 */
Variant c_js_plus::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_plus::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_plus::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_plus::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_plus::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_plus::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_plus::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_plus::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_plus)
ObjectData *c_js_plus::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_plus::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_plus::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_plus::cloneImpl() {
  c_js_plus *obj = NEW(c_js_plus)();
  cloneSet(obj);
  return obj;
}
void c_js_plus::cloneSet(c_js_plus *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_plus::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_plus::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_plus::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_plus$os_get(const char *s) {
  return c_js_plus::os_get(s, -1);
}
Variant &cw_js_plus$os_lval(const char *s) {
  return c_js_plus::os_lval(s, -1);
}
Variant cw_js_plus$os_constant(const char *s) {
  return c_js_plus::os_constant(s);
}
Variant cw_js_plus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_plus::os_invoke(c, s, params, -1, fatal);
}
void c_js_plus::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 281 */
void c_js_plus::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_plus, js_plus::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(281,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 311 */
Variant c_js_instanceof::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_instanceof::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_instanceof::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_instanceof::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_instanceof::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_instanceof::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_instanceof::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_instanceof::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_instanceof)
ObjectData *c_js_instanceof::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_instanceof::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_instanceof::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_instanceof::cloneImpl() {
  c_js_instanceof *obj = NEW(c_js_instanceof)();
  cloneSet(obj);
  return obj;
}
void c_js_instanceof::cloneSet(c_js_instanceof *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_instanceof::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_instanceof::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_instanceof::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_instanceof$os_get(const char *s) {
  return c_js_instanceof::os_get(s, -1);
}
Variant &cw_js_instanceof$os_lval(const char *s) {
  return c_js_instanceof::os_lval(s, -1);
}
Variant cw_js_instanceof$os_constant(const char *s) {
  return c_js_instanceof::os_constant(s);
}
Variant cw_js_instanceof$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_instanceof::os_invoke(c, s, params, -1, fatal);
}
void c_js_instanceof::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 312 */
void c_js_instanceof::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_instanceof, js_instanceof::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(312,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 599 */
Variant c_js_case::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_case::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_case::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_case::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_case::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_case::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_case::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_case::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_case)
ObjectData *c_js_case::create(int num_args, Variant v_expr, Variant v_code, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_expr, v_code,args);
  return this;
}
ObjectData *c_js_case::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_js_case::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_js_case::cloneImpl() {
  c_js_case *obj = NEW(c_js_case)();
  cloneSet(obj);
  return obj;
}
void c_js_case::cloneSet(c_js_case *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_case::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_case::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_case::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_case$os_get(const char *s) {
  return c_js_case::os_get(s, -1);
}
Variant &cw_js_case$os_lval(const char *s) {
  return c_js_case::os_lval(s, -1);
}
Variant cw_js_case$os_constant(const char *s) {
  return c_js_case::os_constant(s);
}
Variant cw_js_case$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_case::os_invoke(c, s, params, -1, fatal);
}
void c_js_case::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 600 */
void c_js_case::t___construct(int num_args, Variant v_expr, Variant v_code, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_case, js_case::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_9(LINE(601,func_get_args(num_args, Array(ArrayInit(2).set(0, v_expr).set(1, v_code).create()),args)), lval(o_lval("expr", 0x1928DF37EFEC67D5LL)), lval(o_lval("code", 0x5B2CD7DDAB7A1DECLL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 603 */
String c_js_case::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_case, js_case::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  String v_o;
  Variant v_code;

  if (equal(o_get("expr", 0x1928DF37EFEC67D5LL), 0LL)) {
    (v_o = "  default:\n");
  }
  else {
    (v_o = LINE(607,(assignCallTemp(eo_1, toString(o_get("e", 0x61B161496B7EA7EBLL))),assignCallTemp(eo_3, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat5("  case (js_bool(jsrt::expr_strict_equal($", eo_1, ",", eo_3, "))):\n"))));
  }
  {
    LOOP_COUNTER(22);
    Variant map23 = o_get("code", 0x5B2CD7DDAB7A1DECLL);
    for (ArrayIterPtr iter24 = map23.begin("js_case"); !iter24->end(); iter24->next()) {
      LOOP_COUNTER_CHECK(22);
      v_code = iter24->second();
      {
        concat_assign(v_o, LINE(610,(assignCallTemp(eo_1, x_trim(toString((assignCallTemp(eo_5, v_code.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)),x_str_replace("\n", "\n    ", eo_5))))),concat3("    ", eo_1, "\n"))));
      }
    }
  }
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 250 */
Variant c_js_typeof::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_typeof::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_typeof::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_typeof::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_typeof::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_typeof::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_typeof::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_typeof::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_typeof)
ObjectData *c_js_typeof::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_typeof::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_typeof::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_typeof::cloneImpl() {
  c_js_typeof *obj = NEW(c_js_typeof)();
  cloneSet(obj);
  return obj;
}
void c_js_typeof::cloneSet(c_js_typeof *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_typeof::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_typeof::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_typeof::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_typeof$os_get(const char *s) {
  return c_js_typeof::os_get(s, -1);
}
Variant &cw_js_typeof$os_lval(const char *s) {
  return c_js_typeof::os_lval(s, -1);
}
Variant cw_js_typeof$os_constant(const char *s) {
  return c_js_typeof::os_constant(s);
}
Variant cw_js_typeof$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_typeof::os_invoke(c, s, params, -1, fatal);
}
void c_js_typeof::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 251 */
void c_js_typeof::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_typeof, js_typeof::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  LINE(251,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_unary_op::t___construct(eo_0, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 570 */
Variant c_js_with::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_with::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_with::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_with::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_with::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_with::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_with::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_with::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_with)
ObjectData *c_js_with::create(int num_args, Variant v_expr, Variant v_statement, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_expr, v_statement,args);
  return this;
}
ObjectData *c_js_with::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_js_with::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_js_with::cloneImpl() {
  c_js_with *obj = NEW(c_js_with)();
  cloneSet(obj);
  return obj;
}
void c_js_with::cloneSet(c_js_with *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_with::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_with::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_with::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_with$os_get(const char *s) {
  return c_js_with::os_get(s, -1);
}
Variant &cw_js_with$os_lval(const char *s) {
  return c_js_with::os_lval(s, -1);
}
Variant cw_js_with$os_constant(const char *s) {
  return c_js_with::os_constant(s);
}
Variant cw_js_with$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_with::os_invoke(c, s, params, -1, fatal);
}
void c_js_with::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 571 */
void c_js_with::t___construct(int num_args, Variant v_expr, Variant v_statement, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_with, js_with::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_7(LINE(572,func_get_args(num_args, Array(ArrayInit(2).set(0, v_expr).set(1, v_statement).create()),args)), lval(o_lval("expr", 0x1928DF37EFEC67D5LL)), lval(o_lval("statement", 0x50D02CF824CFB5E6LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 574 */
String c_js_with::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_with, js_with::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_o;

  (v_o = LINE(575,(assignCallTemp(eo_1, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("jsrt::push_scope(js_obj(", eo_1, "));\n"))));
  concat_assign(v_o, toString(LINE(576,o_get("statement", 0x50D02CF824CFB5E6LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
  concat_assign(v_o, "jsrt::pop_scope();\n");
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 346 */
Variant c_js_or::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_or::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_or::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_or::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_or::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_or::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_or::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_or::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_or)
ObjectData *c_js_or::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_or::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_or::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_or::cloneImpl() {
  c_js_or *obj = NEW(c_js_or)();
  cloneSet(obj);
  return obj;
}
void c_js_or::cloneSet(c_js_or *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_or::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_or::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_or::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_or$os_get(const char *s) {
  return c_js_or::os_get(s, -1);
}
Variant &cw_js_or$os_lval(const char *s) {
  return c_js_or::os_lval(s, -1);
}
Variant cw_js_or$os_constant(const char *s) {
  return c_js_or::os_constant(s);
}
Variant cw_js_or$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_or::os_invoke(c, s, params, -1, fatal);
}
void c_js_or::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 347 */
void c_js_or::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_or, js_or::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(347,c_js_binary_op::t___construct(func_get_args(num_args, Array(),args)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 349 */
String c_js_or::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_or, js_or::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  String v_tmp;

  (v_tmp = LINE(350,c_jsc::t_gensym("sc")));
  return concat(concat_rev(toString(LINE(351,o_get("arg2", 0x30BB46664DFFCCE7LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), (assignCallTemp(eo_1, v_tmp),assignCallTemp(eo_3, toString(o_get("arg1", 0x5779C2A9622744AALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),assignCallTemp(eo_4, concat3(")\?$", v_tmp, ":")),concat5("(js_bool($", eo_1, "=", eo_3, eo_4))), ")");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 99 */
Variant c_js_source::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x4A7B35D5CDE213B9LL, g->s_js_source_DupIdnest,
                  nest);
      break;
    case 6:
      HASH_RETURN(0x151BECE81100D69ELL, g->s_js_source_DupIdthat,
                  that);
      HASH_RETURN(0x7DDBEB38F40F832ELL, g->s_js_source_DupIdlabels,
                  labels);
      break;
    default:
      break;
  }
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_source::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x4A7B35D5CDE213B9LL, g->s_js_source_DupIdnest,
                  nest);
      break;
    case 6:
      HASH_RETURN(0x151BECE81100D69ELL, g->s_js_source_DupIdthat,
                  that);
      HASH_RETURN(0x7DDBEB38F40F832ELL, g->s_js_source_DupIdlabels,
                  labels);
      break;
    default:
      break;
  }
  return c_js_construct::os_lval(s, hash);
}
void c_js_source::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_source::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_source::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_source::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_source::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_source::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_source)
ObjectData *c_js_source::create(Variant v_statements //  = ScalarArrays::sa_[0]
, Variant v_functions //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_statements, v_functions);
  return this;
}
ObjectData *c_js_source::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_source::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_source::cloneImpl() {
  c_js_source *obj = NEW(c_js_source)();
  cloneSet(obj);
  return obj;
}
void c_js_source::cloneSet(c_js_source *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_source::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x0106C3E362278E61LL, addvariable) {
        return (t_addvariable(params.rvalAt(0)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_source::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x0106C3E362278E61LL, addvariable) {
        return (t_addvariable(a0), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_source::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_source$os_get(const char *s) {
  return c_js_source::os_get(s, -1);
}
Variant &cw_js_source$os_lval(const char *s) {
  return c_js_source::os_lval(s, -1);
}
Variant cw_js_source$os_constant(const char *s) {
  return c_js_source::os_constant(s);
}
Variant cw_js_source$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_source::os_invoke(c, s, params, -1, fatal);
}
void c_js_source::init() {
  c_js_construct::init();
}
void c_js_source::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_js_source_DupIdthat = null;
  g->s_js_source_DupIdnest = null;
  g->s_js_source_DupIdlabels = null;
}
void csi_js_source() {
  c_js_source::os_static_initializer();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 100 */
void c_js_source::t___construct(Variant v_statements //  = ScalarArrays::sa_[0]
, Variant v_functions //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(js_source, js_source::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("code", 0x5B2CD7DDAB7A1DECLL) = v_statements);
  (o_lval("functions", 0x345241CAC8396B02LL) = v_functions);
  (o_lval("vars", 0x45BC50DE0F30800ALL) = ScalarArrays::sa_[0]);
  (o_lval("funcdef", 0x40A623E52D29619DLL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 106 */
void c_js_source::t_addstatement(CVarRef v_statement) {
  INSTANCE_METHOD_INJECTION(js_source, js_source::addStatement);
  lval(o_lval("code", 0x5B2CD7DDAB7A1DECLL)).append((v_statement));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 109 */
void c_js_source::t_addfunction(CVarRef v_function) {
  INSTANCE_METHOD_INJECTION(js_source, js_source::addFunction);
  lval(o_lval("functions", 0x345241CAC8396B02LL)).append((v_function));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 115 */
void c_js_source::t_addvariable(CVarRef v_var) {
  INSTANCE_METHOD_INJECTION(js_source, js_source::addVariable);
  DECLARE_GLOBAL_VARIABLES(g);
  lval(g->s_js_source_DupIdthat.o_lval("vars", 0x45BC50DE0F30800ALL)).append((v_var));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 118 */
void c_js_source::ti_addfunctionexpression(const char* cls, CVarRef v_function) {
  STATIC_METHOD_INJECTION(js_source, js_source::addFunctionExpression);
  DECLARE_GLOBAL_VARIABLES(g);
  lval(g->s_js_source_DupIdthat.o_lval("functions", 0x345241CAC8396B02LL)).append((v_function));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 121 */
void c_js_source::ti_addfunctiondefinition(const char* cls, CVarRef v_function) {
  STATIC_METHOD_INJECTION(js_source, js_source::addFunctionDefinition);
  DECLARE_GLOBAL_VARIABLES(g);
  lval(g->s_js_program_DupIdsource.o_lval("funcdef", 0x40A623E52D29619DLL)).append((v_function));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 124 */
String c_js_source::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_source, js_source::emit);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_saved_that;
  String v_s;
  Variant v_statement;
  Variant v_v;
  String v_f;
  Variant v_function;
  String v_fd;

  (g->s_js_source_DupIdnest = 0LL);
  (g->s_js_source_DupIdlabels = ScalarArrays::sa_[0]);
  ;
  (v_saved_that = g->s_js_source_DupIdthat);
  (g->s_js_source_DupIdthat = ((Object)(this)));
  (v_s = "");
  {
    LOOP_COUNTER(25);
    Variant map26 = o_get("code", 0x5B2CD7DDAB7A1DECLL);
    for (ArrayIterPtr iter27 = map26.begin("js_source"); !iter27->end(); iter27->next()) {
      LOOP_COUNTER_CHECK(25);
      v_statement = iter27->second();
      {
        concat_assign(v_s, toString(LINE(133,v_statement.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
      }
    }
  }
  (g->s_js_source_DupIdthat = v_saved_that);
  (v_v = LINE(137,c_js_var::t_really_emit(o_get("vars", 0x45BC50DE0F30800ALL))));
  (v_f = "");
  {
    LOOP_COUNTER(28);
    Variant map29 = o_get("functions", 0x345241CAC8396B02LL);
    for (ArrayIterPtr iter30 = map29.begin("js_source"); !iter30->end(); iter30->next()) {
      LOOP_COUNTER_CHECK(28);
      v_function = iter30->second();
      {
        concat_assign(v_f, toString(LINE(141,v_function.o_invoke_few_args("function_emit", 0x330AAAC697BBFB7ALL, 0))));
      }
    }
  }
  if (!equal(v_f, "")) (v_f = concat("/* function mapping */\n", v_f));
  (v_fd = "");
  if (same(((Object)(this)), g->s_js_program_DupIdsource)) {
    (v_fd = "");
    {
      LOOP_COUNTER(31);
      Variant map32 = o_get("funcdef", 0x40A623E52D29619DLL);
      for (ArrayIterPtr iter33 = map32.begin("js_source"); !iter33->end(); iter33->next()) {
        LOOP_COUNTER_CHECK(31);
        v_function = iter33->second();
        {
          concat_assign(v_fd, toString(LINE(149,v_function.o_invoke_few_args("toplevel_emit", 0x69354A6AAE7724CFLL, 0))));
        }
      }
    }
    if (!equal(v_fd, "")) (v_fd = concat("/* function declarations */\n", v_fd));
  }
  return LINE(154,concat4(v_fd, v_f, toString(v_v), v_s));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 733 */
Variant c_js_literal_null::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_literal_null::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_literal_null::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_literal_null::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_literal_null::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_literal_null::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_literal_null::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_literal_null::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_literal_null)
ObjectData *c_js_literal_null::cloneImpl() {
  c_js_literal_null *obj = NEW(c_js_literal_null)();
  cloneSet(obj);
  return obj;
}
void c_js_literal_null::cloneSet(c_js_literal_null *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_literal_null::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_literal_null::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_literal_null::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_literal_null$os_get(const char *s) {
  return c_js_literal_null::os_get(s, -1);
}
Variant &cw_js_literal_null$os_lval(const char *s) {
  return c_js_literal_null::os_lval(s, -1);
}
Variant cw_js_literal_null$os_constant(const char *s) {
  return c_js_literal_null::os_constant(s);
}
Variant cw_js_literal_null$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_literal_null::os_invoke(c, s, params, -1, fatal);
}
void c_js_literal_null::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 734 */
String c_js_literal_null::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_literal_null, js_literal_null::emit);
  return "jsrt::$null";
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 289 */
Variant c_js_rsh::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_rsh::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_rsh::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_rsh::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_rsh::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_rsh::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_rsh::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_rsh::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_rsh)
ObjectData *c_js_rsh::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_rsh::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_rsh::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_rsh::cloneImpl() {
  c_js_rsh *obj = NEW(c_js_rsh)();
  cloneSet(obj);
  return obj;
}
void c_js_rsh::cloneSet(c_js_rsh *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_rsh::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_rsh::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_rsh::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_rsh$os_get(const char *s) {
  return c_js_rsh::os_get(s, -1);
}
Variant &cw_js_rsh$os_lval(const char *s) {
  return c_js_rsh::os_lval(s, -1);
}
Variant cw_js_rsh$os_constant(const char *s) {
  return c_js_rsh::os_constant(s);
}
Variant cw_js_rsh$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_rsh::os_invoke(c, s, params, -1, fatal);
}
void c_js_rsh::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 290 */
void c_js_rsh::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_rsh, js_rsh::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(290,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 413 */
Variant c_js_statement::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_statement::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_statement::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_statement::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_statement::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_statement::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_statement::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_statement::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_statement)
ObjectData *c_js_statement::create(Variant v_child) {
  init();
  t___construct(v_child);
  return this;
}
ObjectData *c_js_statement::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_statement::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_statement::cloneImpl() {
  c_js_statement *obj = NEW(c_js_statement)();
  cloneSet(obj);
  return obj;
}
void c_js_statement::cloneSet(c_js_statement *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_statement::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_statement::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_statement::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_statement$os_get(const char *s) {
  return c_js_statement::os_get(s, -1);
}
Variant &cw_js_statement$os_lval(const char *s) {
  return c_js_statement::os_lval(s, -1);
}
Variant cw_js_statement$os_constant(const char *s) {
  return c_js_statement::os_constant(s);
}
Variant cw_js_statement$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_statement::os_invoke(c, s, params, -1, fatal);
}
void c_js_statement::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 414 */
void c_js_statement::t___construct(Variant v_child) {
  INSTANCE_METHOD_INJECTION(js_statement, js_statement::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("child", 0x24FB556E313D19DFLL) = v_child);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 417 */
String c_js_statement::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_statement, js_statement::emit);
  return concat(toString(LINE(418,o_get("child", 0x24FB556E313D19DFLL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))), ";\n");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 259 */
Variant c_js_u_plus::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_u_plus::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_u_plus::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_u_plus::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_u_plus::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_u_plus::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_u_plus::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_u_plus::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_u_plus)
ObjectData *c_js_u_plus::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_u_plus::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_u_plus::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_u_plus::cloneImpl() {
  c_js_u_plus *obj = NEW(c_js_u_plus)();
  cloneSet(obj);
  return obj;
}
void c_js_u_plus::cloneSet(c_js_u_plus *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_u_plus::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_u_plus::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_u_plus::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_u_plus$os_get(const char *s) {
  return c_js_u_plus::os_get(s, -1);
}
Variant &cw_js_u_plus$os_lval(const char *s) {
  return c_js_u_plus::os_lval(s, -1);
}
Variant cw_js_u_plus$os_constant(const char *s) {
  return c_js_u_plus::os_constant(s);
}
Variant cw_js_u_plus$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_u_plus::os_invoke(c, s, params, -1, fatal);
}
void c_js_u_plus::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 260 */
void c_js_u_plus::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_u_plus, js_u_plus::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  LINE(260,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_unary_op::t___construct(eo_0, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 381 */
Variant c_js_var::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_var::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_var::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_var::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_var::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_var::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_var::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_var::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_var)
ObjectData *c_js_var::create(Variant v_args) {
  init();
  t___construct(v_args);
  return this;
}
ObjectData *c_js_var::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_var::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_var::cloneImpl() {
  c_js_var *obj = NEW(c_js_var)();
  cloneSet(obj);
  return obj;
}
void c_js_var::cloneSet(c_js_var *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_var::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 4:
      HASH_GUARD(0x2D9D8A64F609B13CLL, emit_for) {
        return (t_emit_for());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_var::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 4:
      HASH_GUARD(0x2D9D8A64F609B13CLL, emit_for) {
        return (t_emit_for());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_var::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_var$os_get(const char *s) {
  return c_js_var::os_get(s, -1);
}
Variant &cw_js_var$os_lval(const char *s) {
  return c_js_var::os_lval(s, -1);
}
Variant cw_js_var$os_constant(const char *s) {
  return c_js_var::os_constant(s);
}
Variant cw_js_var$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_var::os_invoke(c, s, params, -1, fatal);
}
void c_js_var::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 382 */
void c_js_var::t___construct(Variant v_args) {
  INSTANCE_METHOD_INJECTION(js_var, js_var::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("vars", 0x45BC50DE0F30800ALL) = v_args);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 385 */
Variant c_js_var::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_var, js_var::emit);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_o;
  Variant v_var;
  Variant v_id;
  Variant v_init;
  p_js_assign v_obj;

  (v_o = "");
  {
    LOOP_COUNTER(34);
    Variant map35 = o_get("vars", 0x45BC50DE0F30800ALL);
    for (ArrayIterPtr iter36 = map35.begin("js_var"); !iter36->end(); iter36->next()) {
      LOOP_COUNTER_CHECK(34);
      v_var = iter36->second();
      {
        df_lambda_3(v_var, v_id, v_init);
        LINE(389,g->s_js_source_DupIdthat.o_invoke_few_args("addVariable", 0x0106C3E362278E61LL, 1, v_id));
        if (toBoolean(LINE(390,x_get_class(v_init)))) {
          ((Object)((v_obj = ((Object)(LINE(391,(assignCallTemp(eo_0, ((Object)(p_js_identifier(p_js_identifier(NEWOBJ(c_js_identifier)())->create(v_id))))),assignCallTemp(eo_1, v_init),p_js_assign(p_js_assign(NEWOBJ(c_js_assign)())->create(2, Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()))))))))));
          concat_assign(v_o, toString(LINE(392,v_obj->o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
          concat_assign(v_o, ";\n");
        }
      }
    }
  }
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 398 */
String c_js_var::ti_really_emit(const char* cls, CVarRef v_arr) {
  STATIC_METHOD_INJECTION(js_var, js_var::really_emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  String v_l;

  if (equal(LINE(399,x_count(v_arr)), 0LL)) return "";
  (v_l = LINE(400,(assignCallTemp(eo_1, (assignCallTemp(eo_4, x_array_unique(v_arr)),x_implode("','", eo_4))),concat3("'", eo_1, "'"))));
  return LINE(401,concat3("jsrt::define_variables(", v_l, ");\n"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 403 */
String c_js_var::t_emit_for() {
  INSTANCE_METHOD_INJECTION(js_var, js_var::emit_for);
  LINE(404,o_root_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL));
  return LINE(405,concat3("jsrt::id('", toString(o_get("vars", 0x45BC50DE0F30800ALL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "')"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 206 */
Variant c_js_unary_op::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_unary_op::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_unary_op::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_unary_op::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_unary_op::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_unary_op::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_unary_op::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_unary_op::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_unary_op)
ObjectData *c_js_unary_op::create(Variant v_a, Variant v_w //  = 0LL
) {
  init();
  t___construct(v_a, v_w);
  return this;
}
ObjectData *c_js_unary_op::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_unary_op::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_unary_op::cloneImpl() {
  c_js_unary_op *obj = NEW(c_js_unary_op)();
  cloneSet(obj);
  return obj;
}
void c_js_unary_op::cloneSet(c_js_unary_op *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_unary_op::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_unary_op::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_unary_op::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_unary_op$os_get(const char *s) {
  return c_js_unary_op::os_get(s, -1);
}
Variant &cw_js_unary_op$os_lval(const char *s) {
  return c_js_unary_op::os_lval(s, -1);
}
Variant cw_js_unary_op$os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
Variant cw_js_unary_op$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_unary_op::os_invoke(c, s, params, -1, fatal);
}
void c_js_unary_op::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 207 */
void c_js_unary_op::t___construct(Variant v_a, Variant v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_unary_op, js_unary_op::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  (o_lval("arg", 0x72E1EE0D651E56D8LL) = v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (o_lval("wantValue", 0x485938D5DD430D41LL) = v_w);
  (o_lval("jsrt_op", 0x72BCAA5292848C70LL) = LINE(210,(assignCallTemp(eo_0, toString(x_get_class(((Object)(this))))),x_substr(eo_0, toInt32(3LL)))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 212 */
String c_js_unary_op::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_unary_op, js_unary_op::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  return LINE(213,(assignCallTemp(eo_1, toString(o_get("jsrt_op", 0x72BCAA5292848C70LL))),assignCallTemp(eo_3, toString(o_get("arg", 0x72E1EE0D651E56D8LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, o_lval("wantValue", 0x485938D5DD430D41LL)))),concat5("jsrt::expr_", eo_1, "(", eo_3, ")")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 694 */
Variant c_js_identifier::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_identifier::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_identifier::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_identifier::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_identifier::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_identifier::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_identifier::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_identifier::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_identifier)
ObjectData *c_js_identifier::create(Variant v_id) {
  init();
  t___construct(v_id);
  return this;
}
ObjectData *c_js_identifier::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_identifier::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_identifier::cloneImpl() {
  c_js_identifier *obj = NEW(c_js_identifier)();
  cloneSet(obj);
  return obj;
}
void c_js_identifier::cloneSet(c_js_identifier *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_identifier::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_identifier::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_identifier::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_identifier$os_get(const char *s) {
  return c_js_identifier::os_get(s, -1);
}
Variant &cw_js_identifier$os_lval(const char *s) {
  return c_js_identifier::os_lval(s, -1);
}
Variant cw_js_identifier$os_constant(const char *s) {
  return c_js_identifier::os_constant(s);
}
Variant cw_js_identifier$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_identifier::os_invoke(c, s, params, -1, fatal);
}
void c_js_identifier::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 695 */
void c_js_identifier::t___construct(Variant v_id) {
  INSTANCE_METHOD_INJECTION(js_identifier, js_identifier::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("id", 0x028B9FE0C4522BE2LL) = v_id);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 698 */
String c_js_identifier::t_emit(CVarRef v_wantvalue //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_identifier, js_identifier::emit);
  Variant v_v;

  (v_v = toBoolean(v_wantvalue) ? (("v")) : (("")));
  return LINE(700,concat5("jsrt::id", toString(v_v), "('", toString(o_get("id", 0x028B9FE0C4522BE2LL)), "')"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 354 */
Variant c_js_assign::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_assign::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_assign::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_assign::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_assign::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_assign::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_assign::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_assign::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_assign)
ObjectData *c_js_assign::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_assign::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_assign::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_assign::cloneImpl() {
  c_js_assign *obj = NEW(c_js_assign)();
  cloneSet(obj);
  return obj;
}
void c_js_assign::cloneSet(c_js_assign *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_assign::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_assign::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_assign::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_assign$os_get(const char *s) {
  return c_js_assign::os_get(s, -1);
}
Variant &cw_js_assign$os_lval(const char *s) {
  return c_js_assign::os_lval(s, -1);
}
Variant cw_js_assign$os_constant(const char *s) {
  return c_js_assign::os_constant(s);
}
Variant cw_js_assign$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_assign::os_invoke(c, s, params, -1, fatal);
}
void c_js_assign::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 355 */
void c_js_assign::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_assign, js_assign::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(355,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 0LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 274 */
Variant c_js_divide::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_divide::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_divide::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_divide::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_divide::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_divide::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_divide::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_divide::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_divide)
ObjectData *c_js_divide::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_divide::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_divide::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_divide::cloneImpl() {
  c_js_divide *obj = NEW(c_js_divide)();
  cloneSet(obj);
  return obj;
}
void c_js_divide::cloneSet(c_js_divide *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_divide::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_divide::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_divide::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_divide$os_get(const char *s) {
  return c_js_divide::os_get(s, -1);
}
Variant &cw_js_divide$os_lval(const char *s) {
  return c_js_divide::os_lval(s, -1);
}
Variant cw_js_divide$os_constant(const char *s) {
  return c_js_divide::os_constant(s);
}
Variant cw_js_divide$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_divide::os_invoke(c, s, params, -1, fatal);
}
void c_js_divide::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 275 */
void c_js_divide::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_divide, js_divide::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(275,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 867 */
Variant c_js_new::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_new::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_new::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_new::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_new::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_new::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_new::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_new::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_new)
ObjectData *c_js_new::create(int num_args, Variant v_expr, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_expr,args);
  return this;
}
ObjectData *c_js_new::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(count, params.rvalAt(0)));
    return (create(count,params.rvalAt(0), params.slice(1, count - 1, false)));
  } else return this;
}
void c_js_new::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(count, params.rvalAt(0)));
  (t___construct(count,params.rvalAt(0), params.slice(1, count - 1, false)));
}
ObjectData *c_js_new::cloneImpl() {
  c_js_new *obj = NEW(c_js_new)();
  cloneSet(obj);
  return obj;
}
void c_js_new::cloneSet(c_js_new *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_new::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(count, params.rvalAt(0)), null);
        return (t___construct(count,params.rvalAt(0), params.slice(1, count - 1, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_new::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(count, a0), null);
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_new::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_new$os_get(const char *s) {
  return c_js_new::os_get(s, -1);
}
Variant &cw_js_new$os_lval(const char *s) {
  return c_js_new::os_lval(s, -1);
}
Variant cw_js_new$os_constant(const char *s) {
  return c_js_new::os_constant(s);
}
Variant cw_js_new$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_new::os_invoke(c, s, params, -1, fatal);
}
void c_js_new::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 868 */
void c_js_new::t___construct(int num_args, Variant v_expr, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_new, js_new::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_13(LINE(869,func_get_args(num_args, Array(ArrayInit(1).set(0, v_expr).create()),args)), lval(o_lval("expr", 0x1928DF37EFEC67D5LL)));
  if (equal(LINE(871,x_get_class(o_get("expr", 0x1928DF37EFEC67D5LL))), "js_call")) {
    (o_lval("args", 0x4AF7CD17F976719ELL) = o_get("expr", 0x1928DF37EFEC67D5LL).o_get("args", 0x4AF7CD17F976719ELL));
    (o_lval("expr", 0x1928DF37EFEC67D5LL) = o_get("expr", 0x1928DF37EFEC67D5LL).o_get("expr", 0x1928DF37EFEC67D5LL));
  }
  else {
    (o_lval("args", 0x4AF7CD17F976719ELL) = ScalarArrays::sa_[0]);
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 878 */
String c_js_new::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_new, js_new::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Array v_args;
  Variant v_arg;

  (v_args = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(37);
    Variant map38 = o_get("args", 0x4AF7CD17F976719ELL);
    for (ArrayIterPtr iter39 = map38.begin("js_new"); !iter39->end(); iter39->next()) {
      LOOP_COUNTER_CHECK(37);
      v_arg = iter39->second();
      {
        v_args.append((LINE(881,v_arg.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
      }
    }
  }
  return concat(concat_rev(LINE(883,x_implode(",", v_args)), (assignCallTemp(eo_1, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("jsrt::_new(", eo_1, ", array("))), "))");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 615 */
Variant c_js_throw::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_throw::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_throw::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_throw::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_throw::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_throw::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_throw::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_throw::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_throw)
ObjectData *c_js_throw::create(Variant v_expr) {
  init();
  t___construct(v_expr);
  return this;
}
ObjectData *c_js_throw::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_throw::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_throw::cloneImpl() {
  c_js_throw *obj = NEW(c_js_throw)();
  cloneSet(obj);
  return obj;
}
void c_js_throw::cloneSet(c_js_throw *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_throw::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_throw::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_throw::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_throw$os_get(const char *s) {
  return c_js_throw::os_get(s, -1);
}
Variant &cw_js_throw$os_lval(const char *s) {
  return c_js_throw::os_lval(s, -1);
}
Variant cw_js_throw$os_constant(const char *s) {
  return c_js_throw::os_constant(s);
}
Variant cw_js_throw$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_throw::os_invoke(c, s, params, -1, fatal);
}
void c_js_throw::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 617 */
void c_js_throw::t___construct(Variant v_expr) {
  INSTANCE_METHOD_INJECTION(js_throw, js_throw::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("expr", 0x1928DF37EFEC67D5LL) = v_expr);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 620 */
String c_js_throw::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_throw, js_throw::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return LINE(622,(assignCallTemp(eo_1, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("throw new js_exception(", eo_1, ");\n")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 286 */
Variant c_js_lsh::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_lsh::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_lsh::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_lsh::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_lsh::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_lsh::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_lsh::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_lsh::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_lsh)
ObjectData *c_js_lsh::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_lsh::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_lsh::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_lsh::cloneImpl() {
  c_js_lsh *obj = NEW(c_js_lsh)();
  cloneSet(obj);
  return obj;
}
void c_js_lsh::cloneSet(c_js_lsh *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_lsh::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_lsh::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_lsh::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_lsh$os_get(const char *s) {
  return c_js_lsh::os_get(s, -1);
}
Variant &cw_js_lsh$os_lval(const char *s) {
  return c_js_lsh::os_lval(s, -1);
}
Variant cw_js_lsh$os_constant(const char *s) {
  return c_js_lsh::os_constant(s);
}
Variant cw_js_lsh$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_lsh::os_invoke(c, s, params, -1, fatal);
}
void c_js_lsh::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 287 */
void c_js_lsh::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_lsh, js_lsh::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(287,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 305 */
Variant c_js_lte::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_lte::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_lte::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_lte::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_lte::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_lte::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_lte::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_lte::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_lte)
ObjectData *c_js_lte::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_lte::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_lte::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_lte::cloneImpl() {
  c_js_lte *obj = NEW(c_js_lte)();
  cloneSet(obj);
  return obj;
}
void c_js_lte::cloneSet(c_js_lte *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_lte::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_lte::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_lte::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_lte$os_get(const char *s) {
  return c_js_lte::os_get(s, -1);
}
Variant &cw_js_lte$os_lval(const char *s) {
  return c_js_lte::os_lval(s, -1);
}
Variant cw_js_lte$os_constant(const char *s) {
  return c_js_lte::os_constant(s);
}
Variant cw_js_lte$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_lte::os_invoke(c, s, params, -1, fatal);
}
void c_js_lte::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 306 */
void c_js_lte::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_lte, js_lte::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(306,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 172 */
Variant c_js_function_definition::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x1AF3893157128272LL, g->s_js_function_definition_DupIdin_function,
                  in_function);
      break;
    default:
      break;
  }
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_function_definition::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_function_definition::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_function_definition::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_function_definition::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_function_definition::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_function_definition::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_function_definition::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_function_definition)
ObjectData *c_js_function_definition::create(Variant v_a) {
  init();
  t___construct(v_a);
  return this;
}
ObjectData *c_js_function_definition::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_function_definition::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_function_definition::cloneImpl() {
  c_js_function_definition *obj = NEW(c_js_function_definition)();
  cloneSet(obj);
  return obj;
}
void c_js_function_definition::cloneSet(c_js_function_definition *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_function_definition::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x330AAAC697BBFB7ALL, function_emit) {
        return (t_function_emit());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x69354A6AAE7724CFLL, toplevel_emit) {
        return (t_toplevel_emit());
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_function_definition::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x330AAAC697BBFB7ALL, function_emit) {
        return (t_function_emit());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x69354A6AAE7724CFLL, toplevel_emit) {
        return (t_toplevel_emit());
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_function_definition::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_function_definition$os_get(const char *s) {
  return c_js_function_definition::os_get(s, -1);
}
Variant &cw_js_function_definition$os_lval(const char *s) {
  return c_js_function_definition::os_lval(s, -1);
}
Variant cw_js_function_definition$os_constant(const char *s) {
  return c_js_function_definition::os_constant(s);
}
Variant cw_js_function_definition$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_function_definition::os_invoke(c, s, params, -1, fatal);
}
void c_js_function_definition::init() {
  c_js_construct::init();
}
void c_js_function_definition::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_js_function_definition_DupIdin_function = 0LL;
}
void csi_js_function_definition() {
  c_js_function_definition::os_static_initializer();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 174 */
void c_js_function_definition::t___construct(Variant v_a) {
  INSTANCE_METHOD_INJECTION(js_function_definition, js_function_definition::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_1(v_a, lval(o_lval("id", 0x028B9FE0C4522BE2LL)), lval(o_lval("params", 0x6E4C9E151F20AC62LL)), lval(o_lval("body", 0x6F37689A8FD32EFALL)));
  (o_lval("phpid", 0x58490A6C2CF8B935LL) = LINE(176,c_jsc::t_gensym("jsrt_uf")));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 178 */
String c_js_function_definition::t_toplevel_emit() {
  INSTANCE_METHOD_INJECTION(js_function_definition, js_function_definition::toplevel_emit);
  String v_o;

  (v_o = LINE(179,concat3("function ", toString(o_get("phpid", 0x58490A6C2CF8B935LL)), "() {\n")));
  concat_assign(v_o, concat("  ", LINE(180,x_trim(toString(x_str_replace("\n", "\n  ", o_get("body", 0x6F37689A8FD32EFALL)))))));
  concat_assign(v_o, "\n}\n");
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 184 */
String c_js_function_definition::t_function_emit() {
  INSTANCE_METHOD_INJECTION(js_function_definition, js_function_definition::function_emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_id;
  String v_p;

  g->s_js_function_definition_DupIdin_function++;
  (o_lval("body", 0x6F37689A8FD32EFALL) = LINE(186,o_get("body", 0x6F37689A8FD32EFALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)));
  g->s_js_function_definition_DupIdin_function--;
  LINE(188,c_js_source::t_addfunctiondefinition(((Object)(this))));
  (v_id = "");
  {
    (v_id = LINE(191,concat3(",'", toString(o_get("id", 0x028B9FE0C4522BE2LL)), "'")));
  }
  (v_p = "");
  if (more(LINE(194,x_count(o_get("params", 0x6E4C9E151F20AC62LL))), 0LL)) {
    (v_p = LINE(195,(assignCallTemp(eo_1, x_implode("','", o_get("params", 0x6E4C9E151F20AC62LL))),concat3(",array('", eo_1, "')"))));
  }
  return LINE(197,concat6("jsrt::define_function('", toString(o_get("phpid", 0x58490A6C2CF8B935LL)), "'", v_id, v_p, ");\n"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 199 */
String c_js_function_definition::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_function_definition, js_function_definition::emit);
  LINE(201,c_js_source::t_addfunctionexpression(((Object)(this))));
  return LINE(203,concat3("jsrt::function_id('", toString(o_get("phpid", 0x58490A6C2CF8B935LL)), "')"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 241 */
Variant c_js_post_mm::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_post_mm::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_post_mm::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_post_mm::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_post_mm::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_post_mm::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_post_mm::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_post_mm::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_post_mm)
ObjectData *c_js_post_mm::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_post_mm::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_post_mm::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_post_mm::cloneImpl() {
  c_js_post_mm *obj = NEW(c_js_post_mm)();
  cloneSet(obj);
  return obj;
}
void c_js_post_mm::cloneSet(c_js_post_mm *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_post_mm::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_post_mm::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_post_mm::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_post_mm$os_get(const char *s) {
  return c_js_post_mm::os_get(s, -1);
}
Variant &cw_js_post_mm$os_lval(const char *s) {
  return c_js_post_mm::os_lval(s, -1);
}
Variant cw_js_post_mm$os_constant(const char *s) {
  return c_js_post_mm::os_constant(s);
}
Variant cw_js_post_mm$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_post_mm::os_invoke(c, s, params, -1, fatal);
}
void c_js_post_mm::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 242 */
void c_js_post_mm::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_post_mm, js_post_mm::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(242,c_js_unary_op::t___construct(func_get_args(num_args, Array(),args)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 538 */
Variant c_js_break::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_break::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_break::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_break::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_break::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_break::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_break::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_break::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_break)
ObjectData *c_js_break::create(Variant v_label) {
  init();
  t___construct(v_label);
  return this;
}
ObjectData *c_js_break::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_break::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_break::cloneImpl() {
  c_js_break *obj = NEW(c_js_break)();
  cloneSet(obj);
  return obj;
}
void c_js_break::cloneSet(c_js_break *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_break::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_break::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_break::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_break$os_get(const char *s) {
  return c_js_break::os_get(s, -1);
}
Variant &cw_js_break$os_lval(const char *s) {
  return c_js_break::os_lval(s, -1);
}
Variant cw_js_break$os_constant(const char *s) {
  return c_js_break::os_constant(s);
}
Variant cw_js_break$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_break::os_invoke(c, s, params, -1, fatal);
}
void c_js_break::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 539 */
void c_js_break::t___construct(Variant v_label) {
  INSTANCE_METHOD_INJECTION(js_break, js_break::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("label", 0x10AB80C11491BAADLL) = v_label);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 542 */
String c_js_break::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_break, js_break::emit);
  DECLARE_GLOBAL_VARIABLES(g);
  Numeric v_depth = 0;
  String v_o;

  if (equal(g->s_js_source_DupIdnest, 0LL)) {
    return "ERROR: break outside of a loop\n*************************\n\n";
  }
  if (!same(o_get("label", 0x10AB80C11491BAADLL), ";")) {
    (v_depth = g->s_js_source_DupIdnest - g->s_js_source_DupIdlabels.rvalAt(o_get("label", 0x10AB80C11491BAADLL)));
    (v_o = LINE(548,concat3("break ", toString(v_depth), ";\n")));
  }
  else {
    (v_o = "break;\n");
  }
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 487 */
Variant c_js_for_in::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_for_in::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_for_in::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_for_in::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_for_in::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_for_in::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_for_in::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_for_in::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_for_in)
ObjectData *c_js_for_in::create(int num_args, Variant v_one, Variant v_list, Variant v_statement, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_one, v_list, v_statement,args);
  return this;
}
ObjectData *c_js_for_in::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 3) return (create(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
  } else return this;
}
void c_js_for_in::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 3) (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
}
ObjectData *c_js_for_in::cloneImpl() {
  c_js_for_in *obj = NEW(c_js_for_in)();
  cloneSet(obj);
  return obj;
}
void c_js_for_in::cloneSet(c_js_for_in *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_for_in::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 3) return (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_for_in::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 3) return (t___construct(count, a0, a1, a2), null);
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, a2, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_for_in::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_for_in$os_get(const char *s) {
  return c_js_for_in::os_get(s, -1);
}
Variant &cw_js_for_in$os_lval(const char *s) {
  return c_js_for_in::os_lval(s, -1);
}
Variant cw_js_for_in$os_constant(const char *s) {
  return c_js_for_in::os_constant(s);
}
Variant cw_js_for_in$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_for_in::os_invoke(c, s, params, -1, fatal);
}
void c_js_for_in::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 488 */
void c_js_for_in::t___construct(int num_args, Variant v_one, Variant v_list, Variant v_statement, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_for_in, js_for_in::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_5(LINE(489,func_get_args(num_args, Array(ArrayInit(3).set(0, v_one).set(1, v_list).set(2, v_statement).create()),args)), lval(o_lval("one", 0x37301431385F1EDDLL)), lval(o_lval("list", 0x507C12C34EF2AF32LL)), lval(o_lval("statement", 0x50D02CF824CFB5E6LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 491 */
String c_js_for_in::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_for_in, js_for_in::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_key;
  String v_o;
  Variant v_v;

  (v_key = LINE(492,c_jsc::t_gensym("fv")));
  g->s_js_source_DupIdnest++;
  (v_o = LINE(494,(assignCallTemp(eo_1, toString(o_get("list", 0x507C12C34EF2AF32LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),assignCallTemp(eo_2, concat3(" as $", v_key, ") {\n")),concat3("foreach (", eo_1, eo_2))));
  if (equal(LINE(495,x_get_class(o_get("one", 0x37301431385F1EDDLL))), "js_var")) {
    (v_v = LINE(496,o_get("one", 0x37301431385F1EDDLL).o_invoke_few_args("emit_for", 0x2D9D8A64F609B13CLL, 0)));
  }
  else {
    (v_v = LINE(498,o_get("one", 0x37301431385F1EDDLL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 0)));
  }
  concat_assign(v_o, LINE(500,concat5("  jsrt::expr_assign(", toString(v_v), ", js_str($", v_key, "));\n")));
  concat_assign(v_o, LINE(501,(assignCallTemp(eo_1, x_trim(toString((assignCallTemp(eo_5, o_get("statement", 0x50D02CF824CFB5E6LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)),x_str_replace("\n", "\n  ", eo_5))))),concat3("  ", eo_1, "\n"))));
  concat_assign(v_o, "}");
  g->s_js_source_DupIdnest--;
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 625 */
Variant c_js_try::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_try::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_try::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_try::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_try::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_try::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_try::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_try::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_try)
ObjectData *c_js_try::create(int num_args, Variant v_code, Variant v_catch //  = null
, Variant v_final //  = null
, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_code, v_catch, v_final,args);
  return this;
}
ObjectData *c_js_try::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(count, params.rvalAt(0)));
    if (count == 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    if (count == 3) return (create(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
  } else return this;
}
void c_js_try::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(count, params.rvalAt(0)));
  if (count == 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  if (count == 3) (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
}
ObjectData *c_js_try::cloneImpl() {
  c_js_try *obj = NEW(c_js_try)();
  cloneSet(obj);
  return obj;
}
void c_js_try::cloneSet(c_js_try *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_try::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(count, params.rvalAt(0)), null);
        if (count == 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        if (count == 3) return (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)), null);
      }
      HASH_GUARD(0x69354A6AAE7724CFLL, toplevel_emit) {
        return (t_toplevel_emit());
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_try::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(count, a0), null);
        if (count == 2) return (t___construct(count, a0, a1), null);
        if (count == 3) return (t___construct(count, a0, a1, a2), null);
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, a2, params), null);
      }
      HASH_GUARD(0x69354A6AAE7724CFLL, toplevel_emit) {
        return (t_toplevel_emit());
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_try::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_try$os_get(const char *s) {
  return c_js_try::os_get(s, -1);
}
Variant &cw_js_try$os_lval(const char *s) {
  return c_js_try::os_lval(s, -1);
}
Variant cw_js_try$os_constant(const char *s) {
  return c_js_try::os_constant(s);
}
Variant cw_js_try$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_try::os_invoke(c, s, params, -1, fatal);
}
void c_js_try::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 626 */
void c_js_try::t___construct(int num_args, Variant v_code, Variant v_catch //  = null
, Variant v_final //  = null
, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_try, js_try::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_10(LINE(627,func_get_args(num_args, Array(ArrayInit(3).set(0, v_code).set(1, v_catch).set(2, v_final).create()),args)), lval(o_lval("body", 0x6F37689A8FD32EFALL)), lval(o_lval("catch", 0x75D861AC55370E77LL)), lval(o_lval("final", 0x5192930B2145036ELL)));
  (o_lval("id_try", 0x69EFFBCF2E113CB9LL) = LINE(628,c_jsc::t_gensym("jsrt_try")));
  (o_lval("id_catch", 0x1D68B096743C15FELL) = LINE(629,c_jsc::t_gensym("jsrt_catch")));
  (o_lval("id_finally", 0x02B28D4CF51BF497LL) = LINE(630,c_jsc::t_gensym("jsrt_finally")));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 632 */
String c_js_try::t_toplevel_emit() {
  INSTANCE_METHOD_INJECTION(js_try, js_try::toplevel_emit);
  String v_o;

  (v_o = LINE(633,concat3("function ", toString(o_get("id_try", 0x69EFFBCF2E113CB9LL)), "() {\n  try ")));
  concat_assign(v_o, LINE(635,x_trim(toString(x_str_replace("\n", "\n  ", o_get("body", 0x6F37689A8FD32EFALL))))));
  concat_assign(v_o, " catch (Exception $e) {\n    jsrt::$exception = $e;\n  }\n  return NULL;\n}\n");
  if (!equal(o_get("catch", 0x75D861AC55370E77LL), null)) {
    concat_assign(v_o, LINE(642,concat3("function ", toString(o_get("id_catch", 0x1D68B096743C15FELL)), "() {\n")));
    concat_assign(v_o, concat("  ", LINE(643,x_trim(toString(x_str_replace("\n", "\n  ", o_get("catch", 0x75D861AC55370E77LL)))))));
    concat_assign(v_o, "\n  return NULL;\n}\n");
  }
  if (!equal(o_get("final", 0x5192930B2145036ELL), null)) {
    concat_assign(v_o, LINE(648,concat3("function ", toString(o_get("id_finally", 0x02B28D4CF51BF497LL)), "() {\n")));
    concat_assign(v_o, concat("  ", LINE(649,x_trim(toString(x_str_replace("\n", "\n  ", o_get("final", 0x5192930B2145036ELL)))))));
    concat_assign(v_o, "\n  return NULL;\n}\n");
  }
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 655 */
String c_js_try::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_try, js_try::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_id;
  String v_ret;
  String v_tmp;
  String v_o;

  LINE(658,c_js_source::t_addfunctiondefinition(((Object)(this))));
  (v_id = (!equal(o_get("catch", 0x75D861AC55370E77LL), null)) ? ((Variant)(o_get("catch", 0x75D861AC55370E77LL).o_get("id", 0x028B9FE0C4522BE2LL))) : ((Variant)("")));
  (o_lval("body", 0x6F37689A8FD32EFALL) = LINE(660,o_get("body", 0x6F37689A8FD32EFALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)));
  if (!equal(o_get("catch", 0x75D861AC55370E77LL), null)) (o_lval("catch", 0x75D861AC55370E77LL) = LINE(661,o_get("catch", 0x75D861AC55370E77LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)));
  if (!equal(o_get("final", 0x5192930B2145036ELL), null)) (o_lval("final", 0x5192930B2145036ELL) = LINE(662,o_get("final", 0x5192930B2145036ELL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)));
  (v_ret = LINE(663,c_jsc::t_gensym("jsrt_ret")));
  (v_tmp = LINE(664,c_jsc::t_gensym("jsrt_tmp")));
  (v_o = concat_rev(LINE(658,(assignCallTemp(eo_1, LINE(672,concat5("$", v_ret, " = jsrt::trycatch($", v_tmp, ", "))),assignCallTemp(eo_2, concat((toString(!equal(o_get("catch", 0x75D861AC55370E77LL), null) ? ((Variant)(LINE(673,concat3("'", toString(o_get("id_catch", 0x1D68B096743C15FELL)), "'")))) : ((Variant)("NULL")))), ", ")),assignCallTemp(eo_3, (toString(!equal(o_get("final", 0x5192930B2145036ELL), null) ? ((Variant)(LINE(674,concat3("'", toString(o_get("id_finally", 0x02B28D4CF51BF497LL)), "'")))) : ((Variant)("NULL"))))),assignCallTemp(eo_4, concat((toString(!equal(o_get("catch", 0x75D861AC55370E77LL), null) ? ((Variant)(LINE(675,concat3(", '", toString(v_id), "'")))) : ((Variant)("")))), ");\n")),assignCallTemp(eo_5, LINE(676,concat5("if ($", v_ret, " != NULL) return $", v_ret, ";\n"))),concat6("();\n", eo_1, eo_2, eo_3, eo_4, eo_5))), LINE(658,concat4("$", v_tmp, " = ", toString(o_get("id_try", 0x69EFFBCF2E113CB9LL))))));
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 238 */
Variant c_js_post_pp::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_post_pp::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_post_pp::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_post_pp::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_post_pp::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_post_pp::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_post_pp::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_post_pp::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_post_pp)
ObjectData *c_js_post_pp::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_post_pp::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_post_pp::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_post_pp::cloneImpl() {
  c_js_post_pp *obj = NEW(c_js_post_pp)();
  cloneSet(obj);
  return obj;
}
void c_js_post_pp::cloneSet(c_js_post_pp *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_post_pp::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_post_pp::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_post_pp::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_post_pp$os_get(const char *s) {
  return c_js_post_pp::os_get(s, -1);
}
Variant &cw_js_post_pp$os_lval(const char *s) {
  return c_js_post_pp::os_lval(s, -1);
}
Variant cw_js_post_pp$os_constant(const char *s) {
  return c_js_post_pp::os_constant(s);
}
Variant cw_js_post_pp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_post_pp::os_invoke(c, s, params, -1, fatal);
}
void c_js_post_pp::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 239 */
void c_js_post_pp::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_post_pp, js_post_pp::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(239,c_js_unary_op::t___construct(func_get_args(num_args, Array(),args)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 265 */
Variant c_js_bit_not::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_bit_not::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_bit_not::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_bit_not::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_bit_not::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_bit_not::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_bit_not::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_bit_not::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_bit_not)
ObjectData *c_js_bit_not::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_bit_not::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_bit_not::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_bit_not::cloneImpl() {
  c_js_bit_not *obj = NEW(c_js_bit_not)();
  cloneSet(obj);
  return obj;
}
void c_js_bit_not::cloneSet(c_js_bit_not *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_bit_not::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_bit_not::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_bit_not::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_bit_not$os_get(const char *s) {
  return c_js_bit_not::os_get(s, -1);
}
Variant &cw_js_bit_not$os_lval(const char *s) {
  return c_js_bit_not::os_lval(s, -1);
}
Variant cw_js_bit_not$os_constant(const char *s) {
  return c_js_bit_not::os_constant(s);
}
Variant cw_js_bit_not$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_bit_not::os_invoke(c, s, params, -1, fatal);
}
void c_js_bit_not::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 266 */
void c_js_bit_not::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_bit_not, js_bit_not::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  LINE(266,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_unary_op::t___construct(eo_0, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 507 */
Variant c_js_label::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_label::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_label::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_label::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_label::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_label::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_label::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_label::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_label)
ObjectData *c_js_label::create(int num_args, Variant v_label, Variant v_block, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_label, v_block,args);
  return this;
}
ObjectData *c_js_label::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_js_label::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_js_label::cloneImpl() {
  c_js_label *obj = NEW(c_js_label)();
  cloneSet(obj);
  return obj;
}
void c_js_label::cloneSet(c_js_label *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_label::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_label::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_label::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_label$os_get(const char *s) {
  return c_js_label::os_get(s, -1);
}
Variant &cw_js_label$os_lval(const char *s) {
  return c_js_label::os_lval(s, -1);
}
Variant cw_js_label$os_constant(const char *s) {
  return c_js_label::os_constant(s);
}
Variant cw_js_label$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_label::os_invoke(c, s, params, -1, fatal);
}
void c_js_label::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 508 */
void c_js_label::t___construct(int num_args, Variant v_label, Variant v_block, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_label, js_label::__construct);
  bool oldInCtor = gasInCtor(true);
  Array v_p;

  df_lambda_6(LINE(509,func_get_args(num_args, Array(ArrayInit(2).set(0, v_label).set(1, v_block).create()),args)), lval(o_lval("label", 0x10AB80C11491BAADLL)), lval(o_lval("block", 0x037B2AC087556EF3LL)));
  (v_p = LINE(510,x_explode(":", toString(o_get("label", 0x10AB80C11491BAADLL)))));
  (o_lval("label", 0x10AB80C11491BAADLL) = v_p.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 513 */
Variant c_js_label::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_label, js_label::emit);
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_js_source_DupIdlabels.set(o_get("label", 0x10AB80C11491BAADLL), (g->s_js_source_DupIdnest));
  return LINE(518,o_get("block", 0x037B2AC087556EF3LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 581 */
Variant c_js_switch::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_switch::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_switch::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_switch::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_switch::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_switch::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_switch::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_switch::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_switch)
ObjectData *c_js_switch::create(int num_args, Variant v_expr, Variant v_block, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_expr, v_block,args);
  return this;
}
ObjectData *c_js_switch::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_js_switch::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_js_switch::cloneImpl() {
  c_js_switch *obj = NEW(c_js_switch)();
  cloneSet(obj);
  return obj;
}
void c_js_switch::cloneSet(c_js_switch *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_switch::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_switch::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_switch::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_switch$os_get(const char *s) {
  return c_js_switch::os_get(s, -1);
}
Variant &cw_js_switch$os_lval(const char *s) {
  return c_js_switch::os_lval(s, -1);
}
Variant cw_js_switch$os_constant(const char *s) {
  return c_js_switch::os_constant(s);
}
Variant cw_js_switch$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_switch::os_invoke(c, s, params, -1, fatal);
}
void c_js_switch::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 582 */
void c_js_switch::t___construct(int num_args, Variant v_expr, Variant v_block, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_switch, js_switch::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_8(LINE(583,func_get_args(num_args, Array(ArrayInit(2).set(0, v_expr).set(1, v_block).create()),args)), lval(o_lval("expr", 0x1928DF37EFEC67D5LL)), lval(o_lval("block", 0x037B2AC087556EF3LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 585 */
String c_js_switch::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_switch, js_switch::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_e;
  String v_o;
  Variant v_case;

  (v_e = LINE(586,c_jsc::t_gensym("jsrt_sw")));
  g->s_js_source_DupIdnest++;
  (v_o = LINE(588,(assignCallTemp(eo_1, v_e),assignCallTemp(eo_3, toString(o_get("expr", 0x1928DF37EFEC67D5LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat5("$", eo_1, " = ", eo_3, ";\n"))));
  concat_assign(v_o, "switch (true) {\n");
  {
    LOOP_COUNTER(40);
    Variant map41 = o_get("block", 0x037B2AC087556EF3LL);
    for (ArrayIterPtr iter42 = map41.begin("js_switch"); !iter42->end(); iter42->next()) {
      LOOP_COUNTER_CHECK(40);
      v_case = iter42->second();
      {
        (v_case.o_lval("e", 0x61B161496B7EA7EBLL) = v_e);
        concat_assign(v_o, toString(LINE(592,v_case.o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
      }
    }
  }
  concat_assign(v_o, "\n}\n");
  g->s_js_source_DupIdnest--;
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 472 */
Variant c_js_for::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_for::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_for::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_for::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_for::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_for::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_for::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_for::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_for)
ObjectData *c_js_for::create(int num_args, Variant v_init, Variant v_cond, Variant v_incr, Variant v_statement, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_init, v_cond, v_incr, v_statement,args);
  return this;
}
ObjectData *c_js_for::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 4) return (create(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.slice(4, count - 4, false)));
  } else return this;
}
void c_js_for::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 4) (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.slice(4, count - 4, false)));
}
ObjectData *c_js_for::cloneImpl() {
  c_js_for *obj = NEW(c_js_for)();
  cloneSet(obj);
  return obj;
}
void c_js_for::cloneSet(c_js_for *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_for::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 4) return (t___construct(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.slice(4, count - 4, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_for::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 4) return (t___construct(count, a0, a1, a2, a3), null);
        Array params;
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, a2, a3, params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_for::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_for$os_get(const char *s) {
  return c_js_for::os_get(s, -1);
}
Variant &cw_js_for$os_lval(const char *s) {
  return c_js_for::os_lval(s, -1);
}
Variant cw_js_for$os_constant(const char *s) {
  return c_js_for::os_constant(s);
}
Variant cw_js_for$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_for::os_invoke(c, s, params, -1, fatal);
}
void c_js_for::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 473 */
void c_js_for::t___construct(int num_args, Variant v_init, Variant v_cond, Variant v_incr, Variant v_statement, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_for, js_for::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_4(LINE(474,func_get_args(num_args, Array(ArrayInit(4).set(0, v_init).set(1, v_cond).set(2, v_incr).set(3, v_statement).create()),args)), lval(o_lval("init", 0x0C337193CA26E208LL)), lval(o_lval("cond", 0x682F5D755B5A03E7LL)), lval(o_lval("incr", 0x5CBBB5564C25648ALL)), lval(o_lval("statement", 0x50D02CF824CFB5E6LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 476 */
Variant c_js_for::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_for, js_for::emit);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_o;

  (v_o = toBoolean(o_get("init", 0x0C337193CA26E208LL)) ? ((Variant)(LINE(477,o_get("init", 0x0C337193CA26E208LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL)))) : ((Variant)("")));
  g->s_js_source_DupIdnest++;
  concat_assign(v_o, concat("for (;", (toString(toBoolean(o_get("cond", 0x682F5D755B5A03E7LL)) ? ((Variant)(LINE(479,(assignCallTemp(eo_1, toString(o_get("cond", 0x682F5D755B5A03E7LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))),concat3("js_bool(", eo_1, ")"))))) : ((Variant)(""))))));
  concat_assign(v_o, LINE(480,(assignCallTemp(eo_1, (toString(toBoolean(o_get("incr", 0x5CBBB5564C25648ALL)) ? ((Variant)(o_get("incr", 0x5CBBB5564C25648ALL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))) : ((Variant)(""))))),concat3(";", eo_1, ") {\n"))));
  concat_assign(v_o, toString(LINE(481,o_get("statement", 0x50D02CF824CFB5E6LL).o_invoke_few_args("emit", 0x5D69E86863B3EDD7LL, 1, 1LL))));
  concat_assign(v_o, "\n}\n");
  g->s_js_source_DupIdnest--;
  return v_o;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 323 */
Variant c_js_strict_equal::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_strict_equal::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_strict_equal::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_strict_equal::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_strict_equal::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_strict_equal::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_strict_equal::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_strict_equal::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_strict_equal)
ObjectData *c_js_strict_equal::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_strict_equal::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_strict_equal::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_strict_equal::cloneImpl() {
  c_js_strict_equal *obj = NEW(c_js_strict_equal)();
  cloneSet(obj);
  return obj;
}
void c_js_strict_equal::cloneSet(c_js_strict_equal *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_strict_equal::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_strict_equal::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_strict_equal::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_strict_equal$os_get(const char *s) {
  return c_js_strict_equal::os_get(s, -1);
}
Variant &cw_js_strict_equal$os_lval(const char *s) {
  return c_js_strict_equal::os_lval(s, -1);
}
Variant cw_js_strict_equal$os_constant(const char *s) {
  return c_js_strict_equal::os_constant(s);
}
Variant cw_js_strict_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_strict_equal::os_invoke(c, s, params, -1, fatal);
}
void c_js_strict_equal::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 324 */
void c_js_strict_equal::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_strict_equal, js_strict_equal::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(324,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 408 */
Variant c_js_nop::os_get(const char *s, int64 hash) {
  return c_js_construct::os_get(s, hash);
}
Variant &c_js_nop::os_lval(const char *s, int64 hash) {
  return c_js_construct::os_lval(s, hash);
}
void c_js_nop::o_get(ArrayElementVec &props) const {
  c_js_construct::o_get(props);
}
bool c_js_nop::o_exists(CStrRef s, int64 hash) const {
  return c_js_construct::o_exists(s, hash);
}
Variant c_js_nop::o_get(CStrRef s, int64 hash) {
  return c_js_construct::o_get(s, hash);
}
Variant c_js_nop::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_construct::o_set(s, hash, v, forInit);
}
Variant &c_js_nop::o_lval(CStrRef s, int64 hash) {
  return c_js_construct::o_lval(s, hash);
}
Variant c_js_nop::os_constant(const char *s) {
  return c_js_construct::os_constant(s);
}
IMPLEMENT_CLASS(js_nop)
ObjectData *c_js_nop::cloneImpl() {
  c_js_nop *obj = NEW(c_js_nop)();
  cloneSet(obj);
  return obj;
}
void c_js_nop::cloneSet(c_js_nop *clone) {
  c_js_construct::cloneSet(clone);
}
Variant c_js_nop::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke(s, params, hash, fatal);
}
Variant c_js_nop::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_construct::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_nop::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_construct::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_nop$os_get(const char *s) {
  return c_js_nop::os_get(s, -1);
}
Variant &cw_js_nop$os_lval(const char *s) {
  return c_js_nop::os_lval(s, -1);
}
Variant cw_js_nop$os_constant(const char *s) {
  return c_js_nop::os_constant(s);
}
Variant cw_js_nop$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_nop::os_invoke(c, s, params, -1, fatal);
}
void c_js_nop::init() {
  c_js_construct::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 409 */
String c_js_nop::t_emit(CVarRef v_w //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_nop, js_nop::emit);
  return "";
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 317 */
Variant c_js_equal::os_get(const char *s, int64 hash) {
  return c_js_binary_op::os_get(s, hash);
}
Variant &c_js_equal::os_lval(const char *s, int64 hash) {
  return c_js_binary_op::os_lval(s, hash);
}
void c_js_equal::o_get(ArrayElementVec &props) const {
  c_js_binary_op::o_get(props);
}
bool c_js_equal::o_exists(CStrRef s, int64 hash) const {
  return c_js_binary_op::o_exists(s, hash);
}
Variant c_js_equal::o_get(CStrRef s, int64 hash) {
  return c_js_binary_op::o_get(s, hash);
}
Variant c_js_equal::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_binary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_equal::o_lval(CStrRef s, int64 hash) {
  return c_js_binary_op::o_lval(s, hash);
}
Variant c_js_equal::os_constant(const char *s) {
  return c_js_binary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_equal)
ObjectData *c_js_equal::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_equal::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_equal::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_equal::cloneImpl() {
  c_js_equal *obj = NEW(c_js_equal)();
  cloneSet(obj);
  return obj;
}
void c_js_equal::cloneSet(c_js_equal *clone) {
  c_js_binary_op::cloneSet(clone);
}
Variant c_js_equal::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_equal::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_binary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_equal::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_binary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_equal$os_get(const char *s) {
  return c_js_equal::os_get(s, -1);
}
Variant &cw_js_equal$os_lval(const char *s) {
  return c_js_equal::os_lval(s, -1);
}
Variant cw_js_equal$os_constant(const char *s) {
  return c_js_equal::os_constant(s);
}
Variant cw_js_equal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_equal::os_invoke(c, s, params, -1, fatal);
}
void c_js_equal::init() {
  c_js_binary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 318 */
void c_js_equal::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_equal, js_equal::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  LINE(318,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_binary_op::t___construct(eo_0, 1LL, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 268 */
Variant c_js_not::os_get(const char *s, int64 hash) {
  return c_js_unary_op::os_get(s, hash);
}
Variant &c_js_not::os_lval(const char *s, int64 hash) {
  return c_js_unary_op::os_lval(s, hash);
}
void c_js_not::o_get(ArrayElementVec &props) const {
  c_js_unary_op::o_get(props);
}
bool c_js_not::o_exists(CStrRef s, int64 hash) const {
  return c_js_unary_op::o_exists(s, hash);
}
Variant c_js_not::o_get(CStrRef s, int64 hash) {
  return c_js_unary_op::o_get(s, hash);
}
Variant c_js_not::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_unary_op::o_set(s, hash, v, forInit);
}
Variant &c_js_not::o_lval(CStrRef s, int64 hash) {
  return c_js_unary_op::o_lval(s, hash);
}
Variant c_js_not::os_constant(const char *s) {
  return c_js_unary_op::os_constant(s);
}
IMPLEMENT_CLASS(js_not)
ObjectData *c_js_not::create(int num_args, Array args /* = Array() */) {
  init();
  t___construct(num_args, args);
  return this;
}
ObjectData *c_js_not::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create(count));
    return (create(count,params.slice(0, count - 0, false)));
  } else return this;
}
void c_js_not::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct(count));
  (t___construct(count,params.slice(0, count - 0, false)));
}
ObjectData *c_js_not::cloneImpl() {
  c_js_not *obj = NEW(c_js_not)();
  cloneSet(obj);
  return obj;
}
void c_js_not::cloneSet(c_js_not *clone) {
  c_js_unary_op::cloneSet(clone);
}
Variant c_js_not::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(count), null);
        return (t___construct(count,params.slice(0, count - 0, false)), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        int count = params.size();
        if (count <= 0) return (t_emit());
        return (t_emit(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke(s, params, hash, fatal);
}
Variant c_js_not::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,params), null);
      }
      HASH_GUARD(0x5D69E86863B3EDD7LL, emit) {
        if (count <= 0) return (t_emit());
        return (t_emit(a0));
      }
      break;
    default:
      break;
  }
  return c_js_unary_op::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_not::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_unary_op::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_not$os_get(const char *s) {
  return c_js_not::os_get(s, -1);
}
Variant &cw_js_not$os_lval(const char *s) {
  return c_js_not::os_lval(s, -1);
}
Variant cw_js_not$os_constant(const char *s) {
  return c_js_not::os_constant(s);
}
Variant cw_js_not$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_not::os_invoke(c, s, params, -1, fatal);
}
void c_js_not::init() {
  c_js_unary_op::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 269 */
void c_js_not::t___construct(int num_args, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_not, js_not::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  LINE(269,(assignCallTemp(eo_0, func_get_args(num_args, Array(),args)),c_js_unary_op::t___construct(eo_0, 1LL)));
  gasInCtor(oldInCtor);
} /* function */
Object co_js_continue(CArrRef params, bool init /* = true */) {
  return Object(p_js_continue(NEW(c_js_continue)())->dynCreate(params, init));
}
Object co_js_catch(CArrRef params, bool init /* = true */) {
  return Object(p_js_catch(NEW(c_js_catch)())->dynCreate(params, init));
}
Object co_js_modulo(CArrRef params, bool init /* = true */) {
  return Object(p_js_modulo(NEW(c_js_modulo)())->dynCreate(params, init));
}
Object co_js_ternary(CArrRef params, bool init /* = true */) {
  return Object(p_js_ternary(NEW(c_js_ternary)())->dynCreate(params, init));
}
Object co_js_print(CArrRef params, bool init /* = true */) {
  return Object(p_js_print(NEW(c_js_print)())->dynCreate(params, init));
}
Object co_js_construct(CArrRef params, bool init /* = true */) {
  return Object(p_js_construct(NEW(c_js_construct)())->dynCreate(params, init));
}
Object co_js_bit_and(CArrRef params, bool init /* = true */) {
  return Object(p_js_bit_and(NEW(c_js_bit_and)())->dynCreate(params, init));
}
Object co_js_bit_or(CArrRef params, bool init /* = true */) {
  return Object(p_js_bit_or(NEW(c_js_bit_or)())->dynCreate(params, init));
}
Object co_js_return(CArrRef params, bool init /* = true */) {
  return Object(p_js_return(NEW(c_js_return)())->dynCreate(params, init));
}
Object co_js_while(CArrRef params, bool init /* = true */) {
  return Object(p_js_while(NEW(c_js_while)())->dynCreate(params, init));
}
Object co_js_gte(CArrRef params, bool init /* = true */) {
  return Object(p_js_gte(NEW(c_js_gte)())->dynCreate(params, init));
}
Object co_js_literal_array(CArrRef params, bool init /* = true */) {
  return Object(p_js_literal_array(NEW(c_js_literal_array)())->dynCreate(params, init));
}
Object co_js_pre_mm(CArrRef params, bool init /* = true */) {
  return Object(p_js_pre_mm(NEW(c_js_pre_mm)())->dynCreate(params, init));
}
Object co_js_u_minus(CArrRef params, bool init /* = true */) {
  return Object(p_js_u_minus(NEW(c_js_u_minus)())->dynCreate(params, init));
}
Object co_js_program(CArrRef params, bool init /* = true */) {
  return Object(p_js_program(NEW(c_js_program)())->dynCreate(params, init));
}
Object co_js_bit_xor(CArrRef params, bool init /* = true */) {
  return Object(p_js_bit_xor(NEW(c_js_bit_xor)())->dynCreate(params, init));
}
Object co_js_compound_assign(CArrRef params, bool init /* = true */) {
  return Object(p_js_compound_assign(NEW(c_js_compound_assign)())->dynCreate(params, init));
}
Object co_js_delete(CArrRef params, bool init /* = true */) {
  return Object(p_js_delete(NEW(c_js_delete)())->dynCreate(params, init));
}
Object co_js_and(CArrRef params, bool init /* = true */) {
  return Object(p_js_and(NEW(c_js_and)())->dynCreate(params, init));
}
Object co_js_this(CArrRef params, bool init /* = true */) {
  return Object(p_js_this(NEW(c_js_this)())->dynCreate(params, init));
}
Object co_js_do(CArrRef params, bool init /* = true */) {
  return Object(p_js_do(NEW(c_js_do)())->dynCreate(params, init));
}
Object co_jsc(CArrRef params, bool init /* = true */) {
  return Object(p_jsc(NEW(c_jsc)())->dynCreate(params, init));
}
Object co_js_pre_pp(CArrRef params, bool init /* = true */) {
  return Object(p_js_pre_pp(NEW(c_js_pre_pp)())->dynCreate(params, init));
}
Object co_js_not_equal(CArrRef params, bool init /* = true */) {
  return Object(p_js_not_equal(NEW(c_js_not_equal)())->dynCreate(params, init));
}
Object co_js_accessor(CArrRef params, bool init /* = true */) {
  return Object(p_js_accessor(NEW(c_js_accessor)())->dynCreate(params, init));
}
Object co_js_block(CArrRef params, bool init /* = true */) {
  return Object(p_js_block(NEW(c_js_block)())->dynCreate(params, init));
}
Object co_js_literal_number(CArrRef params, bool init /* = true */) {
  return Object(p_js_literal_number(NEW(c_js_literal_number)())->dynCreate(params, init));
}
Object co_js_literal_boolean(CArrRef params, bool init /* = true */) {
  return Object(p_js_literal_boolean(NEW(c_js_literal_boolean)())->dynCreate(params, init));
}
Object co_js_if(CArrRef params, bool init /* = true */) {
  return Object(p_js_if(NEW(c_js_if)())->dynCreate(params, init));
}
Object co_js_gt(CArrRef params, bool init /* = true */) {
  return Object(p_js_gt(NEW(c_js_gt)())->dynCreate(params, init));
}
Object co_js_literal_string(CArrRef params, bool init /* = true */) {
  return Object(p_js_literal_string(NEW(c_js_literal_string)())->dynCreate(params, init));
}
Object co_js_call(CArrRef params, bool init /* = true */) {
  return Object(p_js_call(NEW(c_js_call)())->dynCreate(params, init));
}
Object co_js_minus(CArrRef params, bool init /* = true */) {
  return Object(p_js_minus(NEW(c_js_minus)())->dynCreate(params, init));
}
Object co_js_void(CArrRef params, bool init /* = true */) {
  return Object(p_js_void(NEW(c_js_void)())->dynCreate(params, init));
}
Object co_js_in(CArrRef params, bool init /* = true */) {
  return Object(p_js_in(NEW(c_js_in)())->dynCreate(params, init));
}
Object co_js_literal_object(CArrRef params, bool init /* = true */) {
  return Object(p_js_literal_object(NEW(c_js_literal_object)())->dynCreate(params, init));
}
Object co_js_ursh(CArrRef params, bool init /* = true */) {
  return Object(p_js_ursh(NEW(c_js_ursh)())->dynCreate(params, init));
}
Object co_js_binary_op(CArrRef params, bool init /* = true */) {
  return Object(p_js_binary_op(NEW(c_js_binary_op)())->dynCreate(params, init));
}
Object co_js_comma(CArrRef params, bool init /* = true */) {
  return Object(p_js_comma(NEW(c_js_comma)())->dynCreate(params, init));
}
Object co_js_multiply(CArrRef params, bool init /* = true */) {
  return Object(p_js_multiply(NEW(c_js_multiply)())->dynCreate(params, init));
}
Object co_js_strict_not_equal(CArrRef params, bool init /* = true */) {
  return Object(p_js_strict_not_equal(NEW(c_js_strict_not_equal)())->dynCreate(params, init));
}
Object co_js_lt(CArrRef params, bool init /* = true */) {
  return Object(p_js_lt(NEW(c_js_lt)())->dynCreate(params, init));
}
Object co_js_plus(CArrRef params, bool init /* = true */) {
  return Object(p_js_plus(NEW(c_js_plus)())->dynCreate(params, init));
}
Object co_js_instanceof(CArrRef params, bool init /* = true */) {
  return Object(p_js_instanceof(NEW(c_js_instanceof)())->dynCreate(params, init));
}
Object co_js_case(CArrRef params, bool init /* = true */) {
  return Object(p_js_case(NEW(c_js_case)())->dynCreate(params, init));
}
Object co_js_typeof(CArrRef params, bool init /* = true */) {
  return Object(p_js_typeof(NEW(c_js_typeof)())->dynCreate(params, init));
}
Object co_js_with(CArrRef params, bool init /* = true */) {
  return Object(p_js_with(NEW(c_js_with)())->dynCreate(params, init));
}
Object co_js_or(CArrRef params, bool init /* = true */) {
  return Object(p_js_or(NEW(c_js_or)())->dynCreate(params, init));
}
Object co_js_source(CArrRef params, bool init /* = true */) {
  return Object(p_js_source(NEW(c_js_source)())->dynCreate(params, init));
}
Object co_js_literal_null(CArrRef params, bool init /* = true */) {
  return Object(p_js_literal_null(NEW(c_js_literal_null)())->dynCreate(params, init));
}
Object co_js_rsh(CArrRef params, bool init /* = true */) {
  return Object(p_js_rsh(NEW(c_js_rsh)())->dynCreate(params, init));
}
Object co_js_statement(CArrRef params, bool init /* = true */) {
  return Object(p_js_statement(NEW(c_js_statement)())->dynCreate(params, init));
}
Object co_js_u_plus(CArrRef params, bool init /* = true */) {
  return Object(p_js_u_plus(NEW(c_js_u_plus)())->dynCreate(params, init));
}
Object co_js_var(CArrRef params, bool init /* = true */) {
  return Object(p_js_var(NEW(c_js_var)())->dynCreate(params, init));
}
Object co_js_unary_op(CArrRef params, bool init /* = true */) {
  return Object(p_js_unary_op(NEW(c_js_unary_op)())->dynCreate(params, init));
}
Object co_js_identifier(CArrRef params, bool init /* = true */) {
  return Object(p_js_identifier(NEW(c_js_identifier)())->dynCreate(params, init));
}
Object co_js_assign(CArrRef params, bool init /* = true */) {
  return Object(p_js_assign(NEW(c_js_assign)())->dynCreate(params, init));
}
Object co_js_divide(CArrRef params, bool init /* = true */) {
  return Object(p_js_divide(NEW(c_js_divide)())->dynCreate(params, init));
}
Object co_js_new(CArrRef params, bool init /* = true */) {
  return Object(p_js_new(NEW(c_js_new)())->dynCreate(params, init));
}
Object co_js_throw(CArrRef params, bool init /* = true */) {
  return Object(p_js_throw(NEW(c_js_throw)())->dynCreate(params, init));
}
Object co_js_lsh(CArrRef params, bool init /* = true */) {
  return Object(p_js_lsh(NEW(c_js_lsh)())->dynCreate(params, init));
}
Object co_js_lte(CArrRef params, bool init /* = true */) {
  return Object(p_js_lte(NEW(c_js_lte)())->dynCreate(params, init));
}
Object co_js_function_definition(CArrRef params, bool init /* = true */) {
  return Object(p_js_function_definition(NEW(c_js_function_definition)())->dynCreate(params, init));
}
Object co_js_post_mm(CArrRef params, bool init /* = true */) {
  return Object(p_js_post_mm(NEW(c_js_post_mm)())->dynCreate(params, init));
}
Object co_js_break(CArrRef params, bool init /* = true */) {
  return Object(p_js_break(NEW(c_js_break)())->dynCreate(params, init));
}
Object co_js_for_in(CArrRef params, bool init /* = true */) {
  return Object(p_js_for_in(NEW(c_js_for_in)())->dynCreate(params, init));
}
Object co_js_try(CArrRef params, bool init /* = true */) {
  return Object(p_js_try(NEW(c_js_try)())->dynCreate(params, init));
}
Object co_js_post_pp(CArrRef params, bool init /* = true */) {
  return Object(p_js_post_pp(NEW(c_js_post_pp)())->dynCreate(params, init));
}
Object co_js_bit_not(CArrRef params, bool init /* = true */) {
  return Object(p_js_bit_not(NEW(c_js_bit_not)())->dynCreate(params, init));
}
Object co_js_label(CArrRef params, bool init /* = true */) {
  return Object(p_js_label(NEW(c_js_label)())->dynCreate(params, init));
}
Object co_js_switch(CArrRef params, bool init /* = true */) {
  return Object(p_js_switch(NEW(c_js_switch)())->dynCreate(params, init));
}
Object co_js_for(CArrRef params, bool init /* = true */) {
  return Object(p_js_for(NEW(c_js_for)())->dynCreate(params, init));
}
Object co_js_strict_equal(CArrRef params, bool init /* = true */) {
  return Object(p_js_strict_equal(NEW(c_js_strict_equal)())->dynCreate(params, init));
}
Object co_js_nop(CArrRef params, bool init /* = true */) {
  return Object(p_js_nop(NEW(c_js_nop)())->dynCreate(params, init));
}
Object co_js_equal(CArrRef params, bool init /* = true */) {
  return Object(p_js_equal(NEW(c_js_equal)())->dynCreate(params, init));
}
Object co_js_not(CArrRef params, bool init /* = true */) {
  return Object(p_js_not(NEW(c_js_not)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$jsc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$jsc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,require(concat(x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php")), "/parse/parser.so.php"), true, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
