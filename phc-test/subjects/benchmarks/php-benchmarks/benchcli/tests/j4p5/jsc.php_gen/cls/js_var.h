
#ifndef __GENERATED_cls_js_var_h__
#define __GENERATED_cls_js_var_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 381 */
class c_js_var : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_var)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_var)
  DECLARE_CLASS(js_var, js_var, js_construct)
  void init();
  public: void t___construct(Variant v_args);
  public: ObjectData *create(Variant v_args);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_emit(CVarRef v_w = 0LL);
  public: static String ti_really_emit(const char* cls, CVarRef v_arr);
  public: String t_emit_for();
  public: static String t_really_emit(CVarRef v_arr) { return ti_really_emit("js_var", v_arr); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_var_h__
