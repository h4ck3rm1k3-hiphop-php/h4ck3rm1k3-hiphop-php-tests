
#ifndef __GENERATED_cls_js_try_h__
#define __GENERATED_cls_js_try_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 625 */
class c_js_try : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_try)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_try)
  DECLARE_CLASS(js_try, js_try, js_construct)
  void init();
  public: void t___construct(int num_args, Variant v_code, Variant v_catch = null, Variant v_final = null, Array args = Array());
  public: ObjectData *create(int num_args, Variant v_code, Variant v_catch = null, Variant v_final = null, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_toplevel_emit();
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_try_h__
