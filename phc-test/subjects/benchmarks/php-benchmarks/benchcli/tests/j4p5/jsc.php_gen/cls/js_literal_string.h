
#ifndef __GENERATED_cls_js_literal_string_h__
#define __GENERATED_cls_js_literal_string_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 754 */
class c_js_literal_string : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_literal_string)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_literal_string)
  DECLARE_CLASS(js_literal_string, js_literal_string, js_construct)
  void init();
  public: void t___construct(Variant v_a, Variant v_stripquotes = 1LL);
  public: ObjectData *create(Variant v_a, Variant v_stripquotes = 1LL);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_parse_string(CVarRef v_str);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_literal_string_h__
