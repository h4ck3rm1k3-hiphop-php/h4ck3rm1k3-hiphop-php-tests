
#ifndef __GENERATED_cls_js_literal_number_h__
#define __GENERATED_cls_js_literal_number_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 746 */
class c_js_literal_number : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_literal_number)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_literal_number)
  DECLARE_CLASS(js_literal_number, js_literal_number, js_construct)
  void init();
  public: void t___construct(Variant v_v);
  public: ObjectData *create(Variant v_v);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_literal_number_h__
