
#ifndef __GENERATED_cls_js_if_h__
#define __GENERATED_cls_js_if_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 434 */
class c_js_if : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_if)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_if)
  DECLARE_CLASS(js_if, js_if, js_construct)
  void init();
  public: void t___construct(Variant v_cond, Variant v_ifblock, Variant v_elseblock = null);
  public: ObjectData *create(Variant v_cond, Variant v_ifblock, Variant v_elseblock = null);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_if_h__
