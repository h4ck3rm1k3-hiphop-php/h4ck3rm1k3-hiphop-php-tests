
#ifndef __GENERATED_cls_js_literal_boolean_h__
#define __GENERATED_cls_js_literal_boolean_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 738 */
class c_js_literal_boolean : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_literal_boolean)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_literal_boolean)
  DECLARE_CLASS(js_literal_boolean, js_literal_boolean, js_construct)
  void init();
  public: void t___construct(Variant v_v);
  public: ObjectData *create(Variant v_v);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_literal_boolean_h__
