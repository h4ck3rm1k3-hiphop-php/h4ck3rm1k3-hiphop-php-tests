
#ifndef __GENERATED_cls_js_identifier_h__
#define __GENERATED_cls_js_identifier_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 694 */
class c_js_identifier : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_identifier)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_identifier)
  DECLARE_CLASS(js_identifier, js_identifier, js_construct)
  void init();
  public: void t___construct(Variant v_id);
  public: ObjectData *create(Variant v_id);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_wantvalue = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_identifier_h__
