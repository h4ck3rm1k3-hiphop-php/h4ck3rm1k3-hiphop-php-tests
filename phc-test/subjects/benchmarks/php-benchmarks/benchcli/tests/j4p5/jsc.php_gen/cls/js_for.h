
#ifndef __GENERATED_cls_js_for_h__
#define __GENERATED_cls_js_for_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 472 */
class c_js_for : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_for)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_for)
  DECLARE_CLASS(js_for, js_for, js_construct)
  void init();
  public: void t___construct(int num_args, Variant v_init, Variant v_cond, Variant v_incr, Variant v_statement, Array args = Array());
  public: ObjectData *create(int num_args, Variant v_init, Variant v_cond, Variant v_incr, Variant v_statement, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_emit(CVarRef v_w = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_for_h__
