
#ifndef __GENERATED_cls_js_accessor_h__
#define __GENERATED_cls_js_accessor_h__

#include <cls/js_construct.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsc.php line 858 */
class c_js_accessor : virtual public c_js_construct {
  BEGIN_CLASS_MAP(js_accessor)
    PARENT_CLASS(js_construct)
  END_CLASS_MAP(js_accessor)
  DECLARE_CLASS(js_accessor, js_accessor, js_construct)
  void init();
  public: void t___construct(int num_args, Variant v_obj, Variant v_member, Variant v_resolve, Array args = Array());
  public: ObjectData *create(int num_args, Variant v_obj, Variant v_member, Variant v_resolve, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_emit(CVarRef v_wantvalue = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_accessor_h__
