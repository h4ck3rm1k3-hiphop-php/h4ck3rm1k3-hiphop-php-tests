
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_JS_DEBUG = 1LL;
const StaticString k_JS_INLINE = "JS_INLINE";
const StaticString k_JS_DIRECT = "JS_DIRECT";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php line 41 */
Variant c_js::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_js::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_js::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_js::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_js::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_js::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_js::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_js::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(js)
ObjectData *c_js::cloneImpl() {
  c_js *obj = NEW(c_js)();
  cloneSet(obj);
  return obj;
}
void c_js::cloneSet(c_js *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_js::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_js::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js$os_get(const char *s) {
  return c_js::os_get(s, -1);
}
Variant &cw_js$os_lval(const char *s) {
  return c_js::os_lval(s, -1);
}
Variant cw_js$os_constant(const char *s) {
  return c_js::os_constant(s);
}
Variant cw_js$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js::os_invoke(c, s, params, -1, fatal);
}
void c_js::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php line 44 */
void c_js::ti_run(const char* cls, Variant v_src, Variant v_mode //  = k_JS_DIRECT
, Variant v_id //  = null
) {
  STATIC_METHOD_INJECTION(js, js::run);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_path;
  Variant v_t1;
  Variant v_php;
  Variant v_t2;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_src; Variant &v_mode; Variant &v_id; Variant &v_path; Variant &v_t1; Variant &v_php; Variant &v_t2;
    VariableTable(Variant &r_src, Variant &r_mode, Variant &r_id, Variant &r_path, Variant &r_t1, Variant &r_php, Variant &r_t2) : v_src(r_src), v_mode(r_mode), v_id(r_id), v_path(r_path), v_t1(r_t1), v_php(r_php), v_t2(r_t2) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 15) {
        case 1:
          HASH_RETURN(0x52B182B0A94E9C41LL, v_php,
                      php);
          break;
        case 2:
          HASH_RETURN(0x028B9FE0C4522BE2LL, v_id,
                      id);
          break;
        case 3:
          HASH_RETURN(0x2978C54B1DB00143LL, v_mode,
                      mode);
          break;
        case 4:
          HASH_RETURN(0x42DD5992F362B3C4LL, v_path,
                      path);
          break;
        case 10:
          HASH_RETURN(0x3F2BD3A200AD45AALL, v_src,
                      src);
          HASH_RETURN(0x45CC4E6F9FE2867ALL, v_t1,
                      t1);
          break;
        case 14:
          HASH_RETURN(0x39D9D6EECF6AE86ELL, v_t2,
                      t2);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_src, v_mode, v_id, v_path, v_t1, v_php, v_t2);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  if (!(LINE(48,x_file_exists(toString(get_global_variables()->k_JS_CACHE_DIR))))) {
    LINE(49,x_mkdir(toString(get_global_variables()->k_JS_CACHE_DIR), 511LL, true));
  }
  if (equal(v_id, null)) (v_id = LINE(52,x_md5(toString(v_src))));
  (v_path = LINE(53,concat4(toString(get_global_variables()->k_JS_CACHE_DIR), "/", toString(v_id), ".php")));
  {
    LINE(59,require((concat(x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php")), "/jsc.php")), true, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/"));
    if (equal(v_mode, k_JS_INLINE)) (v_src = concat("\?>", toString(v_src)));
    (v_t1 = LINE(62,x_microtime(toBoolean(1LL))));
    (v_php = LINE(63,throw_fatal("unknown class jsc", ((void*)NULL))));
    (v_t2 = LINE(64,x_microtime(toBoolean(2LL))));
    LINE(66,(assignCallTemp(eo_0, toString(v_path)),assignCallTemp(eo_1, concat3("<\?php\n", toString(v_php), "\n\?>")),x_file_put_contents(eo_0, eo_1)));
  }
  LINE(70,include(toString(v_path), false, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php line 74 */
void c_js::ti_init(const char* cls) {
  STATIC_METHOD_INJECTION(js, js::init);
  DECLARE_GLOBAL_VARIABLES(g);

  class VariableTable : public LVariableTable {
  public:
    ;
    VariableTable() {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      return lvalAt(str, hash);
    }
  } variableTable;
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  LINE(75,require((concat(x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php")), "/jsrt.php")), true, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/"));
  LINE(76,throw_fatal("unknown class jsrt", ((void*)NULL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php line 95 */
void c_js::ti_define(const char* cls, CStrRef v_objname, CArrRef v_funcarray, CVarRef v_vararray //  = null_variant
) {
  STATIC_METHOD_INJECTION(js, js::define);
  DECLARE_GLOBAL_VARIABLES(g);
  Object v_obj;
  Variant v_js;
  Variant v_php;
  Variant v_v;

  LINE(97,c_js::t_init());
  (v_obj = LINE(99,create_object("js_object", Array())));
  LINE(100,throw_fatal("unknown class jsrt", ((void*)NULL)));
  LINE(102,throw_fatal("unknown class jsrt", ((void*)NULL)));
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_funcarray.begin("js"); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_php = iter3.second();
      v_js = iter3.first();
      {
        LINE(104,throw_fatal("unknown class jsrt", ((void*)NULL)));
      }
    }
  }
  LINE(106,throw_fatal("unknown class jsrt", ((void*)NULL)));
  {
    LOOP_COUNTER(4);
    Variant map5 = toArray(v_vararray);
    for (ArrayIterPtr iter6 = map5.begin("js"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_php = iter6->second();
      v_js = iter6->first();
      {
        {
          bool tmp8 = (true);
          int tmp9 = -1;
          if (equal(tmp8, (LINE(111,x_is_bool(v_php))))) {
            tmp9 = 0;
          } else if (equal(tmp8, (LINE(112,x_is_string(v_php))))) {
            tmp9 = 1;
          } else if (equal(tmp8, (LINE(113,x_is_numeric(v_php))))) {
            tmp9 = 2;
          } else if (equal(tmp8, (LINE(114,x_is_null(v_php))))) {
            tmp9 = 3;
          } else if (equal(tmp8, (LINE(115,x_is_array(v_php))))) {
            tmp9 = 4;
          } else if (true) {
            tmp9 = 5;
          }
          switch (tmp9) {
          case 0:
            {
              (v_v = LINE(111,create_object("js_val", Array(ArrayInit(2).set(0, throw_fatal("unknown class constant js_val::BOOLEAN")).set(1, v_php).create()))));
              goto break7;
            }
          case 1:
            {
              (v_v = LINE(112,invoke_failed("js_str", Array(ArrayInit(1).set(0, ref(v_php)).create()), 0x00000000B708D4C8LL)));
              goto break7;
            }
          case 2:
            {
              (v_v = LINE(113,invoke_failed("js_int", Array(ArrayInit(1).set(0, ref(v_php)).create()), 0x000000002915BC88LL)));
              goto break7;
            }
          case 3:
            {
              (v_v = throw_fatal("unknown class jsrt"));
              goto break7;
            }
          case 4:
            {
            }
          case 5:
            {
              (v_v = throw_fatal("unknown class jsrt"));
              goto break7;
            }
          }
          break7:;
        }
        LINE(118,v_obj->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_js, v_v));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php line 33 */
void f_enum(int num_args, Array args /* = Array() */) {
  FUNCTION_INJECTION(enum);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_index __attribute__((__unused__)) = g->sv_enum_DupIdindex;
  bool &inited_sv_index __attribute__((__unused__)) = g->inited_sv_enum_DupIdindex;
  Variant v_c;

  if (!inited_sv_index) {
    (sv_index = 1LL);
    inited_sv_index = true;
  }
  {
    LOOP_COUNTER(10);
    Variant map11 = LINE(35,func_get_args(num_args, Array(),args));
    for (ArrayIterPtr iter12 = map11.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_c = iter12->second();
      {
        LINE(36,throw_fatal("bad define"));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php line 126 */
String f_highlight_linenum(CVarRef v_path) {
  FUNCTION_INJECTION(highlight_linenum);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Array v_data;
  String v_start;
  String v_end;
  int64 v_i = 0;
  String v_text;
  Variant v_line;

  (v_data = LINE(129,(assignCallTemp(eo_1, toString(x_highlight_file(toString(v_path), toBoolean(1LL)))),x_explode("<br />", eo_1))));
  (v_start = "<span style=\"color: black;\">");
  (v_end = "</span>");
  (v_i = 1LL);
  (v_text = "");
  {
    LOOP_COUNTER(13);
    for (ArrayIter iter15 = v_data.begin(); !iter15.end(); ++iter15) {
      LOOP_COUNTER_CHECK(13);
      v_line = iter15.second();
      {
        concat_assign(v_text, LINE(138,(assignCallTemp(eo_0, v_start),assignCallTemp(eo_1, toString(v_i)),assignCallTemp(eo_3, v_end),assignCallTemp(eo_4, toString(x_str_replace("\n", "", v_line))),concat6(eo_0, eo_1, " ", eo_3, eo_4, "\n"))));
        ++v_i;
      }
    }
  }
  return LINE(142,concat3("<pre style='border:1px dotted #aaa;'>", v_text, "</pre>"));
} /* function */
Object co_js(CArrRef params, bool init /* = true */) {
  return Object(p_js(NEW(c_js)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(31,g->declareConstant("JS_CACHE_DIR", g->k_JS_CACHE_DIR, concat((toString(toBoolean(x_getenv("TMP")) ? ((Variant)(x_getenv("TMP"))) : ((Variant)("/tmp")))), "/es4php")));
  ;
  LINE(39,f_enum(2, ScalarArrays::sa_[0]));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
