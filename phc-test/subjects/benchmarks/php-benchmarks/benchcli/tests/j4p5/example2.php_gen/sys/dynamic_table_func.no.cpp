
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_js_sha1(CArrRef params);
Variant i_js_add(CArrRef params);
static Variant invoke_case_1(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x71750186FC898DB1LL, js_sha1);
  HASH_INVOKE(0x1B4BD394071DBAC9LL, js_add);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[4])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 4; i++) funcTable[i] = &invoke_builtin;
    funcTable[1] = &invoke_case_1;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 3](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
