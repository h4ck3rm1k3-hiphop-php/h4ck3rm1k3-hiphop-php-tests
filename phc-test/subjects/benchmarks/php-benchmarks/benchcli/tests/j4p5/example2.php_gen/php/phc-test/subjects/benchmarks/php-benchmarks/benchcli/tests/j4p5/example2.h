
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_example2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_example2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/example2.fw.h>

// Declarations
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_js_sha1(Variant v_str);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$example2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_js_add(Variant v_a, Variant v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_example2_h__
