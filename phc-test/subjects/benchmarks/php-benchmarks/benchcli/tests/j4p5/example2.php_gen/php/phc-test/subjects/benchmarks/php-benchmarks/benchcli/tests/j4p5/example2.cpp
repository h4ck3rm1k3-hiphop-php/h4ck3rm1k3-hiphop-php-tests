
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/example2.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/example2.php line 25 */
Variant f_js_sha1(Variant v_str) {
  FUNCTION_INJECTION(js_sha1);
  return LINE(26,invoke_failed("js_str", Array(ArrayInit(1).set(0, ref(x_sha1(toString(invoke_failed("php_str", Array(ArrayInit(1).set(0, ref(v_str)).create()), 0x00000000B46FAB56LL))))).create()), 0x00000000B708D4C8LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/example2.php line 28 */
Variant f_js_add(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(js_add);
  return LINE(29,invoke_failed("js_int", Array(ArrayInit(1).set(0, plus_rev(invoke_failed("php_int", Array(ArrayInit(1).set(0, ref(v_b)).create()), 0x00000000A836C403LL), invoke_failed("php_int", Array(ArrayInit(1).set(0, ref(v_a)).create()), 0x00000000A836C403LL))).create()), 0x000000002915BC88LL));
} /* function */
Variant i_js_sha1(CArrRef params) {
  return (f_js_sha1(params.rvalAt(0)));
}
Variant i_js_add(CArrRef params) {
  return (f_js_add(params.rvalAt(0), params.rvalAt(1)));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$example2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/example2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$example2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_code __attribute__((__unused__)) = (variables != gVariables) ? variables->get("code") : g->GV(code);

  LINE(12,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_php(false, variables));
  (v_code = toString("\r\nprint(\"SHA-1('abc') = \"+external.sha1(\"abc\")+\"<br>\");\r\nprint(\"37 + PI = \"+external.add(37,external.PI)+\"<br>\");\r\n"));
  LINE(34,c_js::t_define("external", ScalarArrays::sa_[1], ScalarArrays::sa_[2]));
  LINE(37,c_js::t_run(v_code));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
