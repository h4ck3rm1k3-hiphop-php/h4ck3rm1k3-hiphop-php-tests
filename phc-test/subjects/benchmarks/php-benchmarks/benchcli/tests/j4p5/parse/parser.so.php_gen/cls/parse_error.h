
#ifndef __GENERATED_cls_parse_error_h__
#define __GENERATED_cls_parse_error_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 660 */
class c_parse_error : virtual public c_exception {
  BEGIN_CLASS_MAP(parse_error)
    PARENT_CLASS(exception)
  END_CLASS_MAP(parse_error)
  DECLARE_CLASS(parse_error, parse_error, exception)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parse_error_h__
