
#ifndef __GENERATED_cls_if_state_h__
#define __GENERATED_cls_if_state_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 405 */
class c_if_state : virtual public ObjectData {
  BEGIN_CLASS_MAP(if_state)
  END_CLASS_MAP(if_state)
  DECLARE_CLASS(if_state, if_state, ObjectData)
  void init();
  public: void t___construct(Variant v_id, Variant v_start, Variant v_final, Variant v_mark, Variant v_tDelta, Variant v_nDelta);
  public: ObjectData *create(Variant v_id, Variant v_start, Variant v_final, Variant v_mark, Variant v_tDelta, Variant v_nDelta);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add_step(CVarRef v_glyph, CVarRef v_step);
  public: Variant t_delta();
  public: bool t_is_initial();
  public: void t_set_default(CVarRef v_step);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_if_state_h__
