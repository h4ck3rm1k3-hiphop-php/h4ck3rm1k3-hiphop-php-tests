
#ifndef __GENERATED_cls_preg_scanner_h__
#define __GENERATED_cls_preg_scanner_h__

#include <cls/token_source.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 590 */
class c_preg_scanner : virtual public c_token_source {
  BEGIN_CLASS_MAP(preg_scanner)
    PARENT_CLASS(token_source)
  END_CLASS_MAP(preg_scanner)
  DECLARE_CLASS(preg_scanner, preg_scanner, token_source)
  void init();
  public: void t_report_instant_description();
  public: void t___construct(int num_args, Variant v_init_context, Variant v_p = null, Array args = Array());
  public: ObjectData *create(int num_args, Variant v_init_context, Variant v_p = null, Array args = Array());
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add_state(CVarRef v_name, CVarRef v_cluster);
  public: void t_start(CVarRef v_string);
  public: Variant t_next();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_preg_scanner_h__
