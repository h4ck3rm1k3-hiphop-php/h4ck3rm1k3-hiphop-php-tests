
#ifndef __GENERATED_cls_distinguishing_table_h__
#define __GENERATED_cls_distinguishing_table_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 443 */
class c_distinguishing_table : virtual public ObjectData {
  BEGIN_CLASS_MAP(distinguishing_table)
  END_CLASS_MAP(distinguishing_table)
  DECLARE_CLASS(distinguishing_table, distinguishing_table, ObjectData)
  void init();
  public: void t_distinguishing_table();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: String t_key(CVarRef v_s1, CVarRef v_s2);
  public: void t_distinguish(CVarRef v_s1, CVarRef v_s2);
  public: bool t_differ(CVarRef v_s1, CVarRef v_s2);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_distinguishing_table_h__
