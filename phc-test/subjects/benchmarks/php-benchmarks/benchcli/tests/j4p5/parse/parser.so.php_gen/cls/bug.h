
#ifndef __GENERATED_cls_bug_h__
#define __GENERATED_cls_bug_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 5 */
class c_bug : virtual public c_exception {
  BEGIN_CLASS_MAP(bug)
    PARENT_CLASS(exception)
  END_CLASS_MAP(bug)
  DECLARE_CLASS(bug, Bug, exception)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bug_h__
