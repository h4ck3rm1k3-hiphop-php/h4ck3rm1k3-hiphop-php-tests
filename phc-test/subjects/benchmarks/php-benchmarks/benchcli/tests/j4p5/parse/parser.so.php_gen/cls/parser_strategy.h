
#ifndef __GENERATED_cls_parser_strategy_h__
#define __GENERATED_cls_parser_strategy_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 751 */
class c_parser_strategy : virtual public ObjectData {
  BEGIN_CLASS_MAP(parser_strategy)
  END_CLASS_MAP(parser_strategy)
  DECLARE_CLASS(parser_strategy, parser_strategy, ObjectData)
  void init();
  // public: void t_stuck(CVarRef v_token, CVarRef v_lex, CVarRef v_stack) = 0;
  // public: void t_assert_done(CVarRef v_token, CVarRef v_lex) = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parser_strategy_h__
