
#ifndef __GENERATED_cls_grammar_h__
#define __GENERATED_cls_grammar_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 329 */
class c_grammar : virtual public ObjectData {
  BEGIN_CLASS_MAP(grammar)
  END_CLASS_MAP(grammar)
  DECLARE_CLASS(grammar, grammar, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add(p_production v_prod);
  public: bool t_has_rule(CVarRef v_head);
  public: Array t_make_dpda();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_grammar_h__
