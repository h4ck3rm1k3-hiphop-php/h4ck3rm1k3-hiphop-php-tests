
#ifndef __GENERATED_cls_parse_step_h__
#define __GENERATED_cls_parse_step_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 350 */
class c_parse_step : virtual public ObjectData {
  BEGIN_CLASS_MAP(parse_step)
  END_CLASS_MAP(parse_step)
  DECLARE_CLASS(parse_step, parse_step, ObjectData)
  void init();
  // public: void t_phrase() = 0;
  // public: void t_instruction() = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parse_step_h__
