
#ifndef __GENERATED_cls_recursive_descent_parser_h__
#define __GENERATED_cls_recursive_descent_parser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 105 */
class c_recursive_descent_parser : virtual public ObjectData {
  BEGIN_CLASS_MAP(recursive_descent_parser)
  END_CLASS_MAP(recursive_descent_parser)
  DECLARE_CLASS(recursive_descent_parser, recursive_descent_parser, ObjectData)
  void init();
  public: Variant m_peek;
  public: Variant m_lex;
  public: void t___construct(Variant v_lex);
  public: ObjectData *create(Variant v_lex);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_parse_terminal(CStrRef v_type);
  public: void t_bomb(CStrRef v_wanted, Variant v_got);
  public: bool t_can_parse_terminal(CStrRef v_type);
  public: Variant t_peek();
  public: Variant t_next();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_recursive_descent_parser_h__
