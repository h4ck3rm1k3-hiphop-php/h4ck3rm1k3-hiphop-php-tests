
#ifndef __GENERATED_cls_grammar_parser_h__
#define __GENERATED_cls_grammar_parser_h__

#include <cls/recursive_descent_parser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 143 */
class c_grammar_parser : virtual public c_recursive_descent_parser {
  BEGIN_CLASS_MAP(grammar_parser)
    PARENT_CLASS(recursive_descent_parser)
  END_CLASS_MAP(grammar_parser)
  DECLARE_CLASS(grammar_parser, grammar_parser, recursive_descent_parser)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: p_grammar t_parse_file(CVarRef v_name);
  public: p_grammar t_parse_grammar();
  public: p_production t_parse_production();
  public: bool t_can_parse_production();
  public: p_production_body t_parse_body();
  public: Variant t_parse_action();
  public: Variant t_parse_code();
  public: Variant t_parse_regex();
  public: Variant t_parse_term();
  public: bool t_can_parse_term();
  public: bool t_can_parse_factor();
  public: Variant t_parse_factor();
  public: Variant t_parse_alternates();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_grammar_parser_h__
