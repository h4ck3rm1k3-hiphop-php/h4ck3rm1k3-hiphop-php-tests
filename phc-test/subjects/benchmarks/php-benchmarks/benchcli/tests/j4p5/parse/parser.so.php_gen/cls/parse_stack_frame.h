
#ifndef __GENERATED_cls_parse_stack_frame_h__
#define __GENERATED_cls_parse_stack_frame_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 647 */
class c_parse_stack_frame : virtual public ObjectData {
  BEGIN_CLASS_MAP(parse_stack_frame)
  END_CLASS_MAP(parse_stack_frame)
  DECLARE_CLASS(parse_stack_frame, parse_stack_frame, ObjectData)
  void init();
  public: Variant m_symbol;
  public: Variant m_semantic;
  public: Variant m_state;
  public: void t___construct(Variant v_symbol, Variant v_state);
  public: ObjectData *create(Variant v_symbol, Variant v_state);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_shift(CVarRef v_semantic);
  public: void t_fold(CVarRef v_semantic);
  public: Variant t_semantic();
  public: String t_trace();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parse_stack_frame_h__
