
#ifndef __GENERATED_cls_set_h__
#define __GENERATED_cls_set_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 21 */
class c_set : virtual public ObjectData {
  BEGIN_CLASS_MAP(set)
  END_CLASS_MAP(set)
  DECLARE_CLASS(set, set, ObjectData)
  void init();
  public: void t_set(CVarRef v_list = ScalarArrays::sa_[0]);
  public: ObjectData *create(CVarRef v_list = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: bool t_has(CVarRef v_item);
  public: void t_add(CVarRef v_item);
  public: void t_del(CVarRef v_item);
  public: Variant t_all();
  public: Variant t_one();
  public: int t_count();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_set_h__
