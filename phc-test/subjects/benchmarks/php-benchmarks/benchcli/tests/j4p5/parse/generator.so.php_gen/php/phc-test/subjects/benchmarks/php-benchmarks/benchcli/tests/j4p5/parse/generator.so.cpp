
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 367 */
Variant c_reduce_step::os_get(const char *s, int64 hash) {
  return c_parse_step::os_get(s, hash);
}
Variant &c_reduce_step::os_lval(const char *s, int64 hash) {
  return c_parse_step::os_lval(s, hash);
}
void c_reduce_step::o_get(ArrayElementVec &props) const {
  c_parse_step::o_get(props);
}
bool c_reduce_step::o_exists(CStrRef s, int64 hash) const {
  return c_parse_step::o_exists(s, hash);
}
Variant c_reduce_step::o_get(CStrRef s, int64 hash) {
  return c_parse_step::o_get(s, hash);
}
Variant c_reduce_step::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_parse_step::o_set(s, hash, v, forInit);
}
Variant &c_reduce_step::o_lval(CStrRef s, int64 hash) {
  return c_parse_step::o_lval(s, hash);
}
Variant c_reduce_step::os_constant(const char *s) {
  return c_parse_step::os_constant(s);
}
IMPLEMENT_CLASS(reduce_step)
ObjectData *c_reduce_step::create(Variant v_mark) {
  init();
  t___construct(v_mark);
  return this;
}
ObjectData *c_reduce_step::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_reduce_step::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_reduce_step::cloneImpl() {
  c_reduce_step *obj = NEW(c_reduce_step)();
  cloneSet(obj);
  return obj;
}
void c_reduce_step::cloneSet(c_reduce_step *clone) {
  c_parse_step::cloneSet(clone);
}
Variant c_reduce_step::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke(s, params, hash, fatal);
}
Variant c_reduce_step::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_reduce_step::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parse_step::os_invoke(c, s, params, hash, fatal);
}
Variant cw_reduce_step$os_get(const char *s) {
  return c_reduce_step::os_get(s, -1);
}
Variant &cw_reduce_step$os_lval(const char *s) {
  return c_reduce_step::os_lval(s, -1);
}
Variant cw_reduce_step$os_constant(const char *s) {
  return c_reduce_step::os_constant(s);
}
Variant cw_reduce_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_reduce_step::os_invoke(c, s, params, -1, fatal);
}
void c_reduce_step::init() {
  c_parse_step::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 368 */
void c_reduce_step::t___construct(Variant v_mark) {
  INSTANCE_METHOD_INJECTION(reduce_step, reduce_step::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("mark", 0x19D2840C0AB0CFCFLL) = v_mark);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 371 */
String c_reduce_step::t_phrase() {
  INSTANCE_METHOD_INJECTION(reduce_step, reduce_step::phrase);
  return toString("reduce via code block ") + toString(o_get("mark", 0x19D2840C0AB0CFCFLL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 374 */
Array c_reduce_step::t_instruction() {
  INSTANCE_METHOD_INJECTION(reduce_step, reduce_step::instruction);
  return Array(ArrayInit(2).set(0, "do").set(1, o_get("mark", 0x19D2840C0AB0CFCFLL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 738 */
Variant c_preg_scanner_definition::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_preg_scanner_definition::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_preg_scanner_definition::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_preg_scanner_definition::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_preg_scanner_definition::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_preg_scanner_definition::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_preg_scanner_definition::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_preg_scanner_definition::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(preg_scanner_definition)
ObjectData *c_preg_scanner_definition::create(Variant v_deflist) {
  init();
  t___construct(v_deflist);
  return this;
}
ObjectData *c_preg_scanner_definition::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_preg_scanner_definition::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_preg_scanner_definition::cloneImpl() {
  c_preg_scanner_definition *obj = NEW(c_preg_scanner_definition)();
  cloneSet(obj);
  return obj;
}
void c_preg_scanner_definition::cloneSet(c_preg_scanner_definition *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_preg_scanner_definition::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x60AF06C7E0B22521LL, scanner) {
        return (t_scanner(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_preg_scanner_definition::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x60AF06C7E0B22521LL, scanner) {
        return (t_scanner(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_preg_scanner_definition::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_preg_scanner_definition$os_get(const char *s) {
  return c_preg_scanner_definition::os_get(s, -1);
}
Variant &cw_preg_scanner_definition$os_lval(const char *s) {
  return c_preg_scanner_definition::os_lval(s, -1);
}
Variant cw_preg_scanner_definition$os_constant(const char *s) {
  return c_preg_scanner_definition::os_constant(s);
}
Variant cw_preg_scanner_definition$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_preg_scanner_definition::os_invoke(c, s, params, -1, fatal);
}
void c_preg_scanner_definition::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 739 */
void c_preg_scanner_definition::t___construct(Variant v_deflist) {
  INSTANCE_METHOD_INJECTION(preg_scanner_definition, preg_scanner_definition::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant v_def;
  Variant v_type;
  Variant v_id;

  (o_lval("is", 0x2962B72623087B6FLL) = ScalarArrays::sa_[0]);
  (o_lval("pattern", 0x029C0A5F2FB04920LL) = ScalarArrays::sa_[0]);
  LINE(742,t_add_scope("s", "INITIAL"));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_deflist.begin("preg_scanner_definition"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_def = iter3->second();
      {
        (v_type = LINE(744,x_array_shift(ref(v_def))));
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_def.begin("preg_scanner_definition"); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_id = iter6->second();
            LINE(745,t_add_scope(v_type, v_id));
          }
        }
      }
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 748 */
void c_preg_scanner_definition::t_add_scope(CVarRef v_type, CVarRef v_id) {
  INSTANCE_METHOD_INJECTION(preg_scanner_definition, preg_scanner_definition::add_scope);
  if (equal(v_type, "s")) lval(o_lval("is", 0x2962B72623087B6FLL)).append((v_id));
  lval(o_lval("pattern", 0x029C0A5F2FB04920LL)).set(v_id, (ScalarArrays::sa_[0]));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 752 */
void c_preg_scanner_definition::t_add_rule(CVarRef v_scope_list, CVarRef v_rule) {
  INSTANCE_METHOD_INJECTION(preg_scanner_definition, preg_scanner_definition::add_rule);
  Variant v_s;

  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_scope_list.begin("preg_scanner_definition"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_s = iter9->second();
      {
        lval(lval(o_lval("pattern", 0x029C0A5F2FB04920LL)).lvalAt(v_s)).append((v_rule));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 757 */
void c_preg_scanner_definition::t_add_rule_list(CVarRef v_scope_list, CArrRef v_rule_list) {
  INSTANCE_METHOD_INJECTION(preg_scanner_definition, preg_scanner_definition::add_rule_list);
  Variant v_rule;

  {
    LOOP_COUNTER(10);
    for (ArrayIter iter12 = v_rule_list.begin("preg_scanner_definition"); !iter12.end(); ++iter12) {
      LOOP_COUNTER_CHECK(10);
      v_rule = iter12.second();
      LINE(758,t_add_rule(v_scope_list, v_rule));
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 760 */
void c_preg_scanner_definition::t_add_common_rule(CVarRef v_rule) {
  INSTANCE_METHOD_INJECTION(preg_scanner_definition, preg_scanner_definition::add_common_rule);
  LINE(761,t_add_rule(o_get("is", 0x2962B72623087B6FLL), v_rule));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 763 */
Object c_preg_scanner_definition::t_scanner(CVarRef v_init_context) {
  INSTANCE_METHOD_INJECTION(preg_scanner_definition, preg_scanner_definition::scanner);
  return LINE(764,create_object("preg_scanner", Array(ArrayInit(2).set(0, v_init_context).set(1, o_get("pattern", 0x029C0A5F2FB04920LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 35 */
Variant c_grammar_scanner::os_get(const char *s, int64 hash) {
  return c_preg_scanner::os_get(s, hash);
}
Variant &c_grammar_scanner::os_lval(const char *s, int64 hash) {
  return c_preg_scanner::os_lval(s, hash);
}
void c_grammar_scanner::o_get(ArrayElementVec &props) const {
  c_preg_scanner::o_get(props);
}
bool c_grammar_scanner::o_exists(CStrRef s, int64 hash) const {
  return c_preg_scanner::o_exists(s, hash);
}
Variant c_grammar_scanner::o_get(CStrRef s, int64 hash) {
  return c_preg_scanner::o_get(s, hash);
}
Variant c_grammar_scanner::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_preg_scanner::o_set(s, hash, v, forInit);
}
Variant &c_grammar_scanner::o_lval(CStrRef s, int64 hash) {
  return c_preg_scanner::o_lval(s, hash);
}
Variant c_grammar_scanner::os_constant(const char *s) {
  return c_preg_scanner::os_constant(s);
}
IMPLEMENT_CLASS(grammar_scanner)
ObjectData *c_grammar_scanner::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_grammar_scanner::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_grammar_scanner::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_grammar_scanner::cloneImpl() {
  c_grammar_scanner *obj = NEW(c_grammar_scanner)();
  cloneSet(obj);
  return obj;
}
void c_grammar_scanner::cloneSet(c_grammar_scanner *clone) {
  c_preg_scanner::cloneSet(clone);
}
Variant c_grammar_scanner::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_preg_scanner::o_invoke(s, params, hash, fatal);
}
Variant c_grammar_scanner::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_preg_scanner::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_grammar_scanner::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_preg_scanner::os_invoke(c, s, params, hash, fatal);
}
Variant cw_grammar_scanner$os_get(const char *s) {
  return c_grammar_scanner::os_get(s, -1);
}
Variant &cw_grammar_scanner$os_lval(const char *s) {
  return c_grammar_scanner::os_lval(s, -1);
}
Variant cw_grammar_scanner$os_constant(const char *s) {
  return c_grammar_scanner::os_constant(s);
}
Variant cw_grammar_scanner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_grammar_scanner::os_invoke(c, s, params, -1, fatal);
}
void c_grammar_scanner::init() {
  c_preg_scanner::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 36 */
void c_grammar_scanner::t___construct() {
  INSTANCE_METHOD_INJECTION(grammar_scanner, grammar_scanner::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  Variant eo_9;
  Variant eo_10;
  Variant eo_11;
  Variant v_oob;
  Variant v_ocb;
  Variant v_ob;

  LINE(37,throw_fatal("unknown class preg_scanner", ((void*)NULL)));
  (v_oob = LINE(38,f_mk_action("$context++; $state=\"code\";")));
  (v_ocb = LINE(39,f_mk_action("$context--; if ($context <= 0) $state = \"INITIAL\";")));
  (v_ob = LINE(41,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "({)").set(1, "openbrace").set(2, 0LL).set(3, ref(v_oob)).create()), 0x000000007E8888C9LL)));
  {
    LINE(57,o_root_invoke_few_args("add_state", 0x35E12657D280D9EALL, 2, "INITIAL", (assignCallTemp(eo_0, LINE(45,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\s+)").set(1, "whitespace").set(2, 1LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_1, LINE(46,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(#[^\\n]*)").set(1, "comment").set(2, 1LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_2, LINE(47,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(EPSILON)").set(1, "epsilon").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_3, LINE(48,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(c\\W|\\w+'*)").set(1, "token").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_4, LINE(49,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(:)").set(1, "colon").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_5, LINE(50,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\|)").set(1, "union").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_6, LINE(51,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\*)").set(1, "star").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_7, LINE(52,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\\?)").set(1, "hook").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_8, LINE(53,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\+)").set(1, "plus").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_9, LINE(54,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\()").set(1, "leftparen").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_10, LINE(55,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\))").set(1, "rightparen").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_11, v_ob),Array(ArrayInit(12).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, eo_4).set(5, eo_5).set(6, eo_6).set(7, eo_7).set(8, eo_8).set(9, eo_9).set(10, eo_10).set(11, eo_11).create()))));
  }
  {
    LINE(69,o_root_invoke_few_args("add_state", 0x35E12657D280D9EALL, 2, "code", (assignCallTemp(eo_0, LINE(62,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "('(\?:\\.|[^'])*')").set(1, "sstring").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_1, LINE(63,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\"(\?:\\.|[^\"])*\")").set(1, "dstring").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_2, LINE(64,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "((\?m)(\?:#|//).*\?$)").set(1, "linecomment").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_3, LINE(65,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(/\\*.*\?\\*/)").set(1, "blockcomment").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_4, v_ob),assignCallTemp(eo_5, LINE(67,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(})").set(1, "closebrace").set(2, 0LL).set(3, ref(v_ocb)).create()), 0x000000007E8888C9LL))),assignCallTemp(eo_6, LINE(68,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "([^'\"#/}{]+|.)").set(1, "php").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),Array(ArrayInit(7).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, eo_4).set(5, eo_5).set(6, eo_6).create()))));
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 392 */
Variant c_push_step::os_get(const char *s, int64 hash) {
  return c_parse_step::os_get(s, hash);
}
Variant &c_push_step::os_lval(const char *s, int64 hash) {
  return c_parse_step::os_lval(s, hash);
}
void c_push_step::o_get(ArrayElementVec &props) const {
  c_parse_step::o_get(props);
}
bool c_push_step::o_exists(CStrRef s, int64 hash) const {
  return c_parse_step::o_exists(s, hash);
}
Variant c_push_step::o_get(CStrRef s, int64 hash) {
  return c_parse_step::o_get(s, hash);
}
Variant c_push_step::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_parse_step::o_set(s, hash, v, forInit);
}
Variant &c_push_step::o_lval(CStrRef s, int64 hash) {
  return c_parse_step::o_lval(s, hash);
}
Variant c_push_step::os_constant(const char *s) {
  return c_parse_step::os_constant(s);
}
IMPLEMENT_CLASS(push_step)
ObjectData *c_push_step::create(Variant v_gamma, Variant v_after) {
  init();
  t___construct(v_gamma, v_after);
  return this;
}
ObjectData *c_push_step::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_push_step::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_push_step::cloneImpl() {
  c_push_step *obj = NEW(c_push_step)();
  cloneSet(obj);
  return obj;
}
void c_push_step::cloneSet(c_push_step *clone) {
  c_parse_step::cloneSet(clone);
}
Variant c_push_step::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke(s, params, hash, fatal);
}
Variant c_push_step::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_push_step::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parse_step::os_invoke(c, s, params, hash, fatal);
}
Variant cw_push_step$os_get(const char *s) {
  return c_push_step::os_get(s, -1);
}
Variant &cw_push_step$os_lval(const char *s) {
  return c_push_step::os_lval(s, -1);
}
Variant cw_push_step$os_constant(const char *s) {
  return c_push_step::os_constant(s);
}
Variant cw_push_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_push_step::os_invoke(c, s, params, -1, fatal);
}
void c_push_step::init() {
  c_parse_step::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 393 */
void c_push_step::t___construct(Variant v_gamma, Variant v_after) {
  INSTANCE_METHOD_INJECTION(push_step, push_step::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("gamma", 0x44582EDC1EB9CECBLL) = v_gamma);
  (o_lval("after", 0x000786008EFDF361LL) = v_after);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 397 */
String c_push_step::t_phrase() {
  INSTANCE_METHOD_INJECTION(push_step, push_step::phrase);
  return LINE(398,concat4("push to state ", toString(o_get("gamma", 0x44582EDC1EB9CECBLL)), ", returning to state ", toString(o_get("after", 0x000786008EFDF361LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 400 */
Array c_push_step::t_instruction() {
  INSTANCE_METHOD_INJECTION(push_step, push_step::instruction);
  return Array(ArrayInit(3).set(0, "push").set(1, o_get("gamma", 0x44582EDC1EB9CECBLL)).set(2, o_get("after", 0x000786008EFDF361LL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 433 */
Variant c_if_head::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_if_head::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_if_head::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_if_head::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_if_head::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_if_head::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_if_head::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_if_head::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(if_head)
ObjectData *c_if_head::create(Variant v_symbol, Variant v_start, Variant v_epsilon) {
  init();
  t___construct(v_symbol, v_start, v_epsilon);
  return this;
}
ObjectData *c_if_head::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_if_head::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_if_head::cloneImpl() {
  c_if_head *obj = NEW(c_if_head)();
  cloneSet(obj);
  return obj;
}
void c_if_head::cloneSet(c_if_head *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_if_head::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_if_head::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_if_head::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_if_head$os_get(const char *s) {
  return c_if_head::os_get(s, -1);
}
Variant &cw_if_head$os_lval(const char *s) {
  return c_if_head::os_lval(s, -1);
}
Variant cw_if_head$os_constant(const char *s) {
  return c_if_head::os_constant(s);
}
Variant cw_if_head$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_if_head::os_invoke(c, s, params, -1, fatal);
}
void c_if_head::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 434 */
void c_if_head::t___construct(Variant v_symbol, Variant v_start, Variant v_epsilon) {
  INSTANCE_METHOD_INJECTION(if_head, if_head::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("symbol", 0x4A4181CA6D57B493LL) = v_symbol);
  (o_lval("start", 0x00B621CE3C3982D5LL) = v_start);
  (o_lval("epsilon", 0x0861A5117A0CDD1ALL) = v_epsilon);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 379 */
Variant c_fold_step::os_get(const char *s, int64 hash) {
  return c_parse_step::os_get(s, hash);
}
Variant &c_fold_step::os_lval(const char *s, int64 hash) {
  return c_parse_step::os_lval(s, hash);
}
void c_fold_step::o_get(ArrayElementVec &props) const {
  c_parse_step::o_get(props);
}
bool c_fold_step::o_exists(CStrRef s, int64 hash) const {
  return c_parse_step::o_exists(s, hash);
}
Variant c_fold_step::o_get(CStrRef s, int64 hash) {
  return c_parse_step::o_get(s, hash);
}
Variant c_fold_step::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_parse_step::o_set(s, hash, v, forInit);
}
Variant &c_fold_step::o_lval(CStrRef s, int64 hash) {
  return c_parse_step::o_lval(s, hash);
}
Variant c_fold_step::os_constant(const char *s) {
  return c_parse_step::os_constant(s);
}
IMPLEMENT_CLASS(fold_step)
ObjectData *c_fold_step::create(Variant v_mark, Variant v_dest) {
  init();
  t___construct(v_mark, v_dest);
  return this;
}
ObjectData *c_fold_step::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_fold_step::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_fold_step::cloneImpl() {
  c_fold_step *obj = NEW(c_fold_step)();
  cloneSet(obj);
  return obj;
}
void c_fold_step::cloneSet(c_fold_step *clone) {
  c_parse_step::cloneSet(clone);
}
Variant c_fold_step::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke(s, params, hash, fatal);
}
Variant c_fold_step::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_fold_step::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parse_step::os_invoke(c, s, params, hash, fatal);
}
Variant cw_fold_step$os_get(const char *s) {
  return c_fold_step::os_get(s, -1);
}
Variant &cw_fold_step$os_lval(const char *s) {
  return c_fold_step::os_lval(s, -1);
}
Variant cw_fold_step$os_constant(const char *s) {
  return c_fold_step::os_constant(s);
}
Variant cw_fold_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_fold_step::os_invoke(c, s, params, -1, fatal);
}
void c_fold_step::init() {
  c_parse_step::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 380 */
void c_fold_step::t___construct(Variant v_mark, Variant v_dest) {
  INSTANCE_METHOD_INJECTION(fold_step, fold_step::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("mark", 0x19D2840C0AB0CFCFLL) = v_mark);
  (o_lval("dest", 0x0076370D6709A613LL) = v_dest);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 384 */
String c_fold_step::t_phrase() {
  INSTANCE_METHOD_INJECTION(fold_step, fold_step::phrase);
  return LINE(385,concat4("fold back to state ", toString(o_get("dest", 0x0076370D6709A613LL)), " via code block ", toString(o_get("mark", 0x19D2840C0AB0CFCFLL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 387 */
Array c_fold_step::t_instruction() {
  INSTANCE_METHOD_INJECTION(fold_step, fold_step::instruction);
  return Array(ArrayInit(3).set(0, "fold").set(1, o_get("mark", 0x19D2840C0AB0CFCFLL)).set(2, o_get("dest", 0x0076370D6709A613LL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 350 */
Variant c_parse_step::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parse_step::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parse_step::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_parse_step::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parse_step::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_parse_step::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parse_step::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parse_step::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parse_step)
ObjectData *c_parse_step::cloneImpl() {
  c_parse_step *obj = NEW(c_parse_step)();
  cloneSet(obj);
  return obj;
}
void c_parse_step::cloneSet(c_parse_step *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_parse_step::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parse_step::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parse_step::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parse_step$os_get(const char *s) {
  return c_parse_step::os_get(s, -1);
}
Variant &cw_parse_step$os_lval(const char *s) {
  return c_parse_step::os_lval(s, -1);
}
Variant cw_parse_step$os_constant(const char *s) {
  return c_parse_step::os_constant(s);
}
Variant cw_parse_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parse_step::os_invoke(c, s, params, -1, fatal);
}
void c_parse_step::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 351 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 352 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 329 */
Variant c_grammar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_grammar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_grammar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_grammar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_grammar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_grammar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_grammar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_grammar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(grammar)
ObjectData *c_grammar::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_grammar::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_grammar::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_grammar::cloneImpl() {
  c_grammar *obj = NEW(c_grammar)();
  cloneSet(obj);
  return obj;
}
void c_grammar::cloneSet(c_grammar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_grammar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x67CA6BF3DB8D9BBFLL, make_dpda) {
        return (t_make_dpda());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_grammar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x67CA6BF3DB8D9BBFLL, make_dpda) {
        return (t_make_dpda());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_grammar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_grammar$os_get(const char *s) {
  return c_grammar::os_get(s, -1);
}
Variant &cw_grammar$os_lval(const char *s) {
  return c_grammar::os_lval(s, -1);
}
Variant cw_grammar$os_constant(const char *s) {
  return c_grammar::os_constant(s);
}
Variant cw_grammar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_grammar::os_invoke(c, s, params, -1, fatal);
}
void c_grammar::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 330 */
void c_grammar::t___construct() {
  INSTANCE_METHOD_INJECTION(grammar, grammar::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("rules", 0x78D6949B972CFD25LL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 333 */
void c_grammar::t_add(p_production v_prod) {
  INSTANCE_METHOD_INJECTION(grammar, grammar::add);
  Variant v_head;

  (v_head = v_prod.o_get("head", 0x52803DB826DCBA8ALL));
  if (LINE(335,t_has_rule(v_head))) throw_exception(((Object)(p_exception(p_exception(NEWOBJ(c_exception)())->create(concat3("Already have a rule for ", toString(v_head), "."))))));
  LINE(336,v_prod->t_determinize());
  LINE(337,v_prod->t_minimize());
  lval(o_lval("rules", 0x78D6949B972CFD25LL)).set(v_head, (((Object)(v_prod))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 340 */
bool c_grammar::t_has_rule(CVarRef v_head) {
  INSTANCE_METHOD_INJECTION(grammar, grammar::has_rule);
  return isset(o_get("rules", 0x78D6949B972CFD25LL), v_head);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 344 */
Array c_grammar::t_make_dpda() {
  INSTANCE_METHOD_INJECTION(grammar, grammar::make_dpda);
  p_intermediate_parser_form v_i;

  ((Object)((v_i = ((Object)(LINE(345,p_intermediate_parser_form(p_intermediate_parser_form(NEWOBJ(c_intermediate_parser_form)())->create(o_get("rules", 0x78D6949B972CFD25LL)))))))));
  return LINE(346,v_i->t_compose_program());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 669 */
Variant c_metascanner::os_get(const char *s, int64 hash) {
  return c_preg_scanner::os_get(s, hash);
}
Variant &c_metascanner::os_lval(const char *s, int64 hash) {
  return c_preg_scanner::os_lval(s, hash);
}
void c_metascanner::o_get(ArrayElementVec &props) const {
  c_preg_scanner::o_get(props);
}
bool c_metascanner::o_exists(CStrRef s, int64 hash) const {
  return c_preg_scanner::o_exists(s, hash);
}
Variant c_metascanner::o_get(CStrRef s, int64 hash) {
  return c_preg_scanner::o_get(s, hash);
}
Variant c_metascanner::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_preg_scanner::o_set(s, hash, v, forInit);
}
Variant &c_metascanner::o_lval(CStrRef s, int64 hash) {
  return c_preg_scanner::o_lval(s, hash);
}
Variant c_metascanner::os_constant(const char *s) {
  return c_preg_scanner::os_constant(s);
}
IMPLEMENT_CLASS(metascanner)
ObjectData *c_metascanner::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_metascanner::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_metascanner::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_metascanner::cloneImpl() {
  c_metascanner *obj = NEW(c_metascanner)();
  cloneSet(obj);
  return obj;
}
void c_metascanner::cloneSet(c_metascanner *clone) {
  c_preg_scanner::cloneSet(clone);
}
Variant c_metascanner::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_preg_scanner::o_invoke(s, params, hash, fatal);
}
Variant c_metascanner::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_preg_scanner::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_metascanner::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_preg_scanner::os_invoke(c, s, params, hash, fatal);
}
Variant cw_metascanner$os_get(const char *s) {
  return c_metascanner::os_get(s, -1);
}
Variant &cw_metascanner$os_lval(const char *s) {
  return c_metascanner::os_lval(s, -1);
}
Variant cw_metascanner$os_constant(const char *s) {
  return c_metascanner::os_constant(s);
}
Variant cw_metascanner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_metascanner::os_invoke(c, s, params, -1, fatal);
}
void c_metascanner::init() {
  c_preg_scanner::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 670 */
void c_metascanner::t___construct() {
  INSTANCE_METHOD_INJECTION(metascanner, metascanner::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant v_ocb;
  Variant v_ccb;
  Variant v_m1;
  Variant v_m11;
  Variant v_go_rule;
  Variant v_go_regex;
  Variant v_test_reserved_word;
  Variant v_nl;
  Variant v_cb;
  Variant v_ws;
  Variant v_cm;
  Variant v_id;
  Variant v_dir;
  Variant v_div;
  Variant v_scope_tag;
  Variant v_ob;
  Variant v_regex;
  Variant v_modifier;
  Variant v_openbrace_rule;

  LINE(671,throw_fatal("unknown class preg_scanner", ((void*)NULL)));
  {
    (v_ocb = LINE(673,f_mk_action("$context++; $state=\"code\";")));
    (v_ccb = LINE(674,f_mk_action("$context--; if ($context <= 0) $state = \"rule\";")));
    (v_m1 = LINE(675,f_mk_action("$text = substr($text,1);")));
    (v_m11 = LINE(676,f_mk_action("$text = substr($text,1,strlen($text)-2);")));
    (v_go_rule = LINE(677,f_mk_action("$state = 'rule';")));
    (v_go_regex = LINE(678,f_mk_action("$state = 'regex';")));
    (v_test_reserved_word = LINE(679,f_mk_action("if ($text=='ignore') $type='ignore';")));
  }
  {
    (v_nl = LINE(682,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "([\\n\\r]+)").set(1, "newline").set(2, 1LL).set(3, ref(v_go_regex)).create()), 0x000000007E8888C9LL)));
    (v_cb = LINE(683,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(})").set(1, "closebrace").set(2, 0LL).set(3, ref(v_go_rule)).create()), 0x000000007E8888C9LL)));
    (v_ws = LINE(684,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\s+)").set(1, "whitespace").set(2, 1LL).set(3, "").create()), 0x000000007E8888C9LL)));
    (v_cm = LINE(685,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(#[^\\n\\r]*)").set(1, "comment").set(2, 1LL).set(3, "").create()), 0x000000007E8888C9LL)));
    (v_id = LINE(686,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\w+)").set(1, "identifier").set(2, 0LL).set(3, ref(v_test_reserved_word)).create()), 0x000000007E8888C9LL)));
    (v_dir = LINE(687,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(%(\?:\\w+))").set(1, "directive").set(2, 0LL).set(3, ref(v_m1)).create()), 0x000000007E8888C9LL)));
    (v_div = LINE(688,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(%%)").set(1, "divider").set(2, 0LL).set(3, ref(v_go_regex)).create()), 0x000000007E8888C9LL)));
    (v_scope_tag = LINE(689,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\<(\?:\\w+)>)").set(1, "scope_tag").set(2, 0LL).set(3, ref(v_m11)).create()), 0x000000007E8888C9LL)));
    (v_ob = LINE(690,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\{)").set(1, "openbrace").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL)));
    (v_regex = LINE(691,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\\S+)").set(1, "regex").set(2, 0LL).set(3, ref(v_go_rule)).create()), 0x000000007E8888C9LL)));
    (v_modifier = LINE(692,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(/(\?:[misxU]+))").set(1, "modifier").set(2, 0LL).set(3, ref(v_m1)).create()), 0x000000007E8888C9LL)));
    (v_openbrace_rule = LINE(693,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "({)").set(1, "openbrace").set(2, 0LL).set(3, ref(v_ocb)).create()), 0x000000007E8888C9LL)));
  }
  {
    LINE(705,o_root_invoke_few_args("add_state", 0x35E12657D280D9EALL, 2, "code", (assignCallTemp(eo_0, LINE(698,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "('(\?:\\.|[^'])*')").set(1, "sstring").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_1, LINE(699,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(\"(\?:\\.|[^\"])*\")").set(1, "dstring").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_2, LINE(700,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "((\?m)(\?:#|//).*\?$)").set(1, "linecomment").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_3, LINE(701,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(/\\*.*\?\\*/)").set(1, "blockcomment").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),assignCallTemp(eo_4, v_openbrace_rule),assignCallTemp(eo_5, LINE(703,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "(})").set(1, "closebrace").set(2, 0LL).set(3, ref(v_ccb)).create()), 0x000000007E8888C9LL))),assignCallTemp(eo_6, LINE(704,invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, "([^'\"#/}{]+|.)").set(1, "php").set(2, 0LL).set(3, "").create()), 0x000000007E8888C9LL))),Array(ArrayInit(7).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, eo_4).set(5, eo_5).set(6, eo_6).create()))));
  }
  {
    LINE(711,o_root_invoke_few_args("add_state", 0x35E12657D280D9EALL, 2, "INITIAL", Array(ArrayInit(5).set(0, v_dir).set(1, v_div).set(2, v_ws).set(3, v_cm).set(4, v_id).create())));
    LINE(716,o_root_invoke_few_args("add_state", 0x35E12657D280D9EALL, 2, "regex", Array(ArrayInit(8).set(0, v_nl).set(1, v_cb).set(2, v_ws).set(3, v_cm).set(4, v_id).set(5, v_scope_tag).set(6, v_ob).set(7, v_regex).create())));
    LINE(722,o_root_invoke_few_args("add_state", 0x35E12657D280D9EALL, 2, "rule", Array(ArrayInit(7).set(0, v_nl).set(1, v_cb).set(2, v_ws).set(3, v_cm).set(4, v_id).set(5, v_openbrace_rule).set(6, v_modifier).create())));
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 725 */
String c_metascanner::ti_make_regex(const char* cls, CVarRef v_expression, CVarRef v_flags) {
  STATIC_METHOD_INJECTION(metascanner, metascanner::make_regex);
  return LINE(729,concat5("((\?", toString(v_flags), ")", toString(v_expression), ")"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 731 */
Variant c_metascanner::ti_suitable_delimiter(const char* cls, CVarRef v_expression) {
  STATIC_METHOD_INJECTION(metascanner, metascanner::suitable_delimiter);
  Variant v_c;

  {
    LOOP_COUNTER(13);
    Variant map14 = ScalarArrays::sa_[1];
    for (ArrayIterPtr iter15 = map14.begin("metascanner"); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_c = iter15->second();
      {
        if (same(LINE(733,x_strpos(toString(v_expression), v_c)), false)) return v_c;
      }
    }
  }
  LINE(735,invoke_failed("bug", Array(ArrayInit(1).set(0, toString("Can't think of a suitable delimiter for expression: ") + toString(v_expression)).create()), 0x00000000F9AA490FLL));
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 303 */
Variant c_production::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_production::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_production::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_production::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_production::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_production::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_production::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_production::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(production)
ObjectData *c_production::create(CVarRef v_head) {
  init();
  t_production(v_head);
  return this;
}
ObjectData *c_production::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_production::cloneImpl() {
  c_production *obj = NEW(c_production)();
  cloneSet(obj);
  return obj;
}
void c_production::cloneSet(c_production *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_production::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x74F831EEDE5427E1LL, determinize) {
        return (t_determinize(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x240744AFD489095ALL, minimize) {
        return (t_minimize(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_production::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x74F831EEDE5427E1LL, determinize) {
        return (t_determinize(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x240744AFD489095ALL, minimize) {
        return (t_minimize(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_production::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_production$os_get(const char *s) {
  return c_production::os_get(s, -1);
}
Variant &cw_production$os_lval(const char *s) {
  return c_production::os_lval(s, -1);
}
Variant cw_production$os_constant(const char *s) {
  return c_production::os_constant(s);
}
Variant cw_production$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_production::os_invoke(c, s, params, -1, fatal);
}
void c_production::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 304 */
void c_production::t_production(CVarRef v_head) {
  INSTANCE_METHOD_INJECTION(production, production::production);
  bool oldInCtor = gasInCtor(true);
  (o_lval("head", 0x52803DB826DCBA8ALL) = v_head);
  (o_lval("branches", 0x40DF755730133CBFLL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 308 */
void c_production::t_add(p_production_body v_body) {
  INSTANCE_METHOD_INJECTION(production, production::add);
  lval(o_lval("branches", 0x40DF755730133CBFLL)).append((((Object)(v_body))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 311 */
void c_production::t_determinize() {
  INSTANCE_METHOD_INJECTION(production, production::determinize);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_them;
  Variant v_branch;
  int64 v_mark = 0;
  Variant v_nfa;
  Variant v_union;

  (v_them = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(16);
    Variant map17 = o_get("branches", 0x40DF755730133CBFLL);
    for (ArrayIterPtr iter18 = map17.begin("production"); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_branch = iter18->second();
      {
        (v_mark = LINE(314,f_gen_mark()));
        (v_nfa = v_branch.o_get("nfa", 0x436202EA6F83B1EBLL));
        lval(o_lval("code", 0x5B2CD7DDAB7A1DECLL)).set(v_mark, (v_branch.o_get("code", 0x5B2CD7DDAB7A1DECLL)));
        lval(v_nfa.o_lval("mark", 0x19D2840C0AB0CFCFLL)).set(v_nfa.o_get("final", 0x5192930B2145036ELL), (v_mark));
        v_them.append((v_nfa));
      }
    }
  }
  (v_union = LINE(320,invoke_failed("nfa_union", Array(ArrayInit(1).set(0, ref(v_them)).create()), 0x00000000AB96B737LL)));
  (o_lval("dfa", 0x215A370B1DA0C8ABLL) = LINE(321,v_union.o_invoke_few_args("determinize", 0x74F831EEDE5427E1LL, 0)));
  t___unset("branches");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 324 */
void c_production::t_minimize() {
  INSTANCE_METHOD_INJECTION(production, production::minimize);
  (o_lval("dfa", 0x215A370B1DA0C8ABLL) = LINE(325,o_get("dfa", 0x215A370B1DA0C8ABLL).o_invoke_few_args("minimize", 0x240744AFD489095ALL, 0)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 355 */
Variant c_eat_step::os_get(const char *s, int64 hash) {
  return c_parse_step::os_get(s, hash);
}
Variant &c_eat_step::os_lval(const char *s, int64 hash) {
  return c_parse_step::os_lval(s, hash);
}
void c_eat_step::o_get(ArrayElementVec &props) const {
  c_parse_step::o_get(props);
}
bool c_eat_step::o_exists(CStrRef s, int64 hash) const {
  return c_parse_step::o_exists(s, hash);
}
Variant c_eat_step::o_get(CStrRef s, int64 hash) {
  return c_parse_step::o_get(s, hash);
}
Variant c_eat_step::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_parse_step::o_set(s, hash, v, forInit);
}
Variant &c_eat_step::o_lval(CStrRef s, int64 hash) {
  return c_parse_step::o_lval(s, hash);
}
Variant c_eat_step::os_constant(const char *s) {
  return c_parse_step::os_constant(s);
}
IMPLEMENT_CLASS(eat_step)
ObjectData *c_eat_step::create(Variant v_label) {
  init();
  t___construct(v_label);
  return this;
}
ObjectData *c_eat_step::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_eat_step::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_eat_step::cloneImpl() {
  c_eat_step *obj = NEW(c_eat_step)();
  cloneSet(obj);
  return obj;
}
void c_eat_step::cloneSet(c_eat_step *clone) {
  c_parse_step::cloneSet(clone);
}
Variant c_eat_step::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke(s, params, hash, fatal);
}
Variant c_eat_step::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x06E1BDEA89231CBDLL, instruction) {
        return (t_instruction());
      }
      break;
    case 6:
      HASH_GUARD(0x567618960DB2F01ELL, phrase) {
        return (t_phrase());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_parse_step::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_eat_step::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parse_step::os_invoke(c, s, params, hash, fatal);
}
Variant cw_eat_step$os_get(const char *s) {
  return c_eat_step::os_get(s, -1);
}
Variant &cw_eat_step$os_lval(const char *s) {
  return c_eat_step::os_lval(s, -1);
}
Variant cw_eat_step$os_constant(const char *s) {
  return c_eat_step::os_constant(s);
}
Variant cw_eat_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_eat_step::os_invoke(c, s, params, -1, fatal);
}
void c_eat_step::init() {
  c_parse_step::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 356 */
void c_eat_step::t___construct(Variant v_label) {
  INSTANCE_METHOD_INJECTION(eat_step, eat_step::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("label", 0x10AB80C11491BAADLL) = v_label);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 359 */
String c_eat_step::t_phrase() {
  INSTANCE_METHOD_INJECTION(eat_step, eat_step::phrase);
  return concat("eat it and go to state ", toString(o_get("label", 0x10AB80C11491BAADLL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 362 */
Array c_eat_step::t_instruction() {
  INSTANCE_METHOD_INJECTION(eat_step, eat_step::instruction);
  return Array(ArrayInit(2).set(0, "go").set(1, o_get("label", 0x10AB80C11491BAADLL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 440 */
Variant c_intermediate_parser_form::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_intermediate_parser_form::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_intermediate_parser_form::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("action", m_action.isReferenced() ? ref(m_action) : m_action));
  props.push_back(NEW(ArrayElement)("head", m_head.isReferenced() ? ref(m_head) : m_head));
  props.push_back(NEW(ArrayElement)("label", m_label));
  props.push_back(NEW(ArrayElement)("Q", m_Q.isReferenced() ? ref(m_Q) : m_Q));
  c_ObjectData::o_get(props);
}
bool c_intermediate_parser_form::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x4CCA7F022EF68151LL, action, 6);
      break;
    case 2:
      HASH_EXISTS_STRING(0x52803DB826DCBA8ALL, head, 4);
      break;
    case 4:
      HASH_EXISTS_STRING(0x3993F2F5E215FB44LL, Q, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x10AB80C11491BAADLL, label, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_intermediate_parser_form::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x4CCA7F022EF68151LL, m_action,
                         action, 6);
      break;
    case 2:
      HASH_RETURN_STRING(0x52803DB826DCBA8ALL, m_head,
                         head, 4);
      break;
    case 4:
      HASH_RETURN_STRING(0x3993F2F5E215FB44LL, m_Q,
                         Q, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x10AB80C11491BAADLL, m_label,
                         label, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_intermediate_parser_form::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x4CCA7F022EF68151LL, m_action,
                      action, 6);
      break;
    case 2:
      HASH_SET_STRING(0x52803DB826DCBA8ALL, m_head,
                      head, 4);
      break;
    case 4:
      HASH_SET_STRING(0x3993F2F5E215FB44LL, m_Q,
                      Q, 1);
      break;
    case 5:
      HASH_SET_STRING(0x10AB80C11491BAADLL, m_label,
                      label, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_intermediate_parser_form::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x4CCA7F022EF68151LL, m_action,
                         action, 6);
      break;
    case 2:
      HASH_RETURN_STRING(0x52803DB826DCBA8ALL, m_head,
                         head, 4);
      break;
    case 4:
      HASH_RETURN_STRING(0x3993F2F5E215FB44LL, m_Q,
                         Q, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_intermediate_parser_form::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(intermediate_parser_form)
ObjectData *c_intermediate_parser_form::create(Variant v_rules) {
  init();
  t___construct(v_rules);
  return this;
}
ObjectData *c_intermediate_parser_form::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_intermediate_parser_form::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_intermediate_parser_form::cloneImpl() {
  c_intermediate_parser_form *obj = NEW(c_intermediate_parser_form)();
  cloneSet(obj);
  return obj;
}
void c_intermediate_parser_form::cloneSet(c_intermediate_parser_form *clone) {
  clone->m_action = m_action.isReferenced() ? ref(m_action) : m_action;
  clone->m_head = m_head.isReferenced() ? ref(m_head) : m_head;
  clone->m_label = m_label;
  clone->m_Q = m_Q.isReferenced() ? ref(m_Q) : m_Q;
  ObjectData::cloneSet(clone);
}
Variant c_intermediate_parser_form::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x73FDD2226BB0D75DLL, epsilon_for_state) {
        return (t_epsilon_for_state(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x466488F4D06C9CDFLL, eclose_of_pushes_from) {
        return (t_eclose_of_pushes_from(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_intermediate_parser_form::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 5:
      HASH_GUARD(0x73FDD2226BB0D75DLL, epsilon_for_state) {
        return (t_epsilon_for_state(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x466488F4D06C9CDFLL, eclose_of_pushes_from) {
        return (t_eclose_of_pushes_from(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_intermediate_parser_form::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_intermediate_parser_form$os_get(const char *s) {
  return c_intermediate_parser_form::os_get(s, -1);
}
Variant &cw_intermediate_parser_form$os_lval(const char *s) {
  return c_intermediate_parser_form::os_lval(s, -1);
}
Variant cw_intermediate_parser_form$os_constant(const char *s) {
  return c_intermediate_parser_form::os_constant(s);
}
Variant cw_intermediate_parser_form$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_intermediate_parser_form::os_invoke(c, s, params, -1, fatal);
}
void c_intermediate_parser_form::init() {
  m_action = null;
  m_head = null;
  m_label = null;
  m_Q = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 442 */
void c_intermediate_parser_form::t___construct(Variant v_rules) {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::__construct);
  bool oldInCtor = gasInCtor(true);
  Primitive v_head = 0;
  Variant v_prod;

  (o_lval("NT", 0x644F350DE02C70D6LL) = LINE(443,x_array_keys(v_rules)));
  (m_action = ScalarArrays::sa_[0]);
  (m_head = ScalarArrays::sa_[0]);
  (o_lval("symbol", 0x4A4181CA6D57B493LL) = ScalarArrays::sa_[0]);
  (m_Q = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(19);
    for (ArrayIterPtr iter21 = v_rules.begin("intermediate_parser_form"); !iter21->end(); iter21->next()) {
      LOOP_COUNTER_CHECK(19);
      v_prod = iter21->second();
      v_head = iter21->first();
      LINE(448,t_add_rule(v_head, v_prod));
    }
  }
  LINE(450,t_epsilon_extend_heads());
  LINE(451,t_compute_head_closures());
  LINE(452,t_add_shifts_and_pushes());
  LINE(453,t_add_pops_and_folds());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 455 */
void c_intermediate_parser_form::t_add_pops_and_folds() {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::add_pops_and_folds);
  Variant eo_0;
  Variant eo_1;
  Variant v_q;
  Variant v_start;
  Variant v_symbol;
  bool v_left_recursive = false;
  Variant v_dest;
  Variant v_front;
  Variant v_label;
  Variant v_glyph;
  Variant v_not_important;

  {
    LOOP_COUNTER(22);
    Variant map23 = m_Q;
    for (ArrayIterPtr iter24 = map23.begin("intermediate_parser_form"); !iter24->end(); iter24->next()) {
      LOOP_COUNTER_CHECK(22);
      v_q = iter24->second();
      if (toBoolean(v_q.o_get("final", 0x5192930B2145036ELL))) {
        LINE(458,v_q.o_invoke_few_args("set_default", 0x3D1685902EC9512FLL, 1, p_reduce_step(p_reduce_step(NEWOBJ(c_reduce_step)())->create(v_q.o_get("mark", 0x19D2840C0AB0CFCFLL)))));
        (v_start = m_Q.rvalAt(v_q.o_get("start", 0x00B621CE3C3982D5LL)));
        (v_symbol = o_get("symbol", 0x4A4181CA6D57B493LL).rvalAt(v_q.o_get("id", 0x028B9FE0C4522BE2LL)));
        (v_left_recursive = isset(v_start.o_get("nDelta", 0x2DD4E716AA366D03LL), v_symbol));
        if (v_left_recursive) {
          (v_dest = v_start.o_get("nDelta", 0x2DD4E716AA366D03LL).rvalAt(v_symbol));
          (v_front = LINE(471,t_token_eating_front(v_dest)));
          {
            LOOP_COUNTER(25);
            for (ArrayIterPtr iter27 = v_front.begin("intermediate_parser_form"); !iter27->end(); iter27->next()) {
              LOOP_COUNTER_CHECK(25);
              v_label = iter27->second();
              {
                {
                  LOOP_COUNTER(28);
                  Variant map29 = m_Q.rvalAt(v_label).o_get("tDelta", 0x4130FDB2E0784D79LL);
                  for (ArrayIterPtr iter30 = map29.begin("intermediate_parser_form"); !iter30->end(); iter30->next()) {
                    LOOP_COUNTER_CHECK(28);
                    v_not_important = iter30->second();
                    v_glyph = iter30->first();
                    {
                      (assignCallTemp(eo_0, ref(v_glyph)),assignCallTemp(eo_1, ref(LINE(475,p_fold_step(p_fold_step(NEWOBJ(c_fold_step)())->create(v_q.o_get("mark", 0x19D2840C0AB0CFCFLL), v_dest))))),v_q.o_invoke("add_step", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x0D36C46050BBEBD2LL));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 500 */
void c_intermediate_parser_form::t_add_shifts_and_pushes() {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::add_shifts_and_pushes);
  Variant eo_0;
  Variant eo_1;
  Variant v_q;
  Variant v_glyph;
  Variant v_label;
  Variant v_qSym;
  Primitive v_symbol = 0;
  Variant v_head;
  bool v_processing_self_recursion = false;

  {
    LOOP_COUNTER(31);
    Variant map32 = m_Q;
    for (ArrayIterPtr iter33 = map32.begin("intermediate_parser_form"); !iter33->end(); iter33->next()) {
      LOOP_COUNTER_CHECK(31);
      v_q = iter33->second();
      {
        {
          LOOP_COUNTER(34);
          Variant map35 = v_q.o_get("tDelta", 0x4130FDB2E0784D79LL);
          for (ArrayIterPtr iter36 = map35.begin("intermediate_parser_form"); !iter36->end(); iter36->next()) {
            LOOP_COUNTER_CHECK(34);
            v_label = iter36->second();
            v_glyph = iter36->first();
            (assignCallTemp(eo_0, ref(v_glyph)),assignCallTemp(eo_1, ref(LINE(502,p_eat_step(p_eat_step(NEWOBJ(c_eat_step)())->create(v_label))))),v_q.o_invoke("add_step", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x0D36C46050BBEBD2LL));
          }
        }
        (v_qSym = o_get("symbol", 0x4A4181CA6D57B493LL).rvalAt(v_q.o_get("id", 0x028B9FE0C4522BE2LL)));
        {
          LOOP_COUNTER(37);
          Variant map38 = v_q.o_get("nDelta", 0x2DD4E716AA366D03LL);
          for (ArrayIterPtr iter39 = map38.begin("intermediate_parser_form"); !iter39->end(); iter39->next()) {
            LOOP_COUNTER_CHECK(37);
            v_label = iter39->second();
            v_symbol = iter39->first();
            {
              (v_head = m_head.rvalAt(v_symbol));
              (v_processing_self_recursion = (equal(v_qSym, v_symbol)));
              if ((toBoolean(LINE(507,v_q.o_invoke_few_args("is_initial", 0x64586199BBCD6CB1LL, 0)))) && (v_processing_self_recursion)) {
                continue;
              }
              if (toBoolean(v_head.o_get("epsilon", 0x0861A5117A0CDD1ALL))) {
                LINE(516,v_q.o_invoke_few_args("set_default", 0x3D1685902EC9512FLL, 1, p_push_step(p_push_step(NEWOBJ(c_push_step)())->create(v_symbol, v_label))));
                if (((more(LINE(517,x_count(v_q.o_get("nDelta", 0x2DD4E716AA366D03LL))), 1LL))) || (toBoolean(x_count(v_q.o_get("tDelta", 0x4130FDB2E0784D79LL))))) {
                }
              }
              else {
                {
                  LOOP_COUNTER(40);
                  Variant map41 = v_head.o_get("lt_close", 0x061697A6F415D5D8LL);
                  for (ArrayIterPtr iter42 = map41.begin("intermediate_parser_form"); !iter42->end(); iter42->next()) {
                    LOOP_COUNTER_CHECK(40);
                    v_glyph = iter42->second();
                    {
                      (assignCallTemp(eo_0, ref(v_glyph)),assignCallTemp(eo_1, ref(LINE(523,p_push_step(p_push_step(NEWOBJ(c_push_step)())->create(v_symbol, v_label))))),v_q.o_invoke("add_step", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x0D36C46050BBEBD2LL));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 530 */
void c_intermediate_parser_form::t_epsilon_extend_heads() {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::epsilon_extend_heads);
  bool v_learned = false;
  Variant v_head;
  Variant v_label;

  {
    LOOP_COUNTER(43);
    do {
      LOOP_COUNTER_CHECK(43);
      {
        (v_learned = false);
        {
          LOOP_COUNTER(44);
          Variant map45 = m_head;
          for (ArrayIterPtr iter46 = map45.begin("intermediate_parser_form"); !iter46->end(); iter46->next()) {
            LOOP_COUNTER_CHECK(44);
            v_head = iter46->second();
            if (!(toBoolean(v_head.o_get("epsilon", 0x0861A5117A0CDD1ALL)))) {
              {
                LOOP_COUNTER(47);
                Variant map48 = LINE(539,t_epsilon_closure_for_state(v_head.o_get("start", 0x00B621CE3C3982D5LL)));
                for (ArrayIterPtr iter49 = map48.begin("intermediate_parser_form"); !iter49->end(); iter49->next()) {
                  LOOP_COUNTER_CHECK(47);
                  v_label = iter49->second();
                  {
                    if (toBoolean(m_Q.rvalAt(v_label).o_get("final", 0x5192930B2145036ELL))) {
                      (v_head.o_lval("epsilon", 0x0861A5117A0CDD1ALL) = true);
                      (v_learned = true);
                    }
                  }
                }
              }
            }
          }
        }
      }
    } while (v_learned);
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 549 */
Variant c_intermediate_parser_form::t_epsilon_for_state(CVarRef v_label) {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::epsilon_for_state);
  Variant v_epsilon;
  Primitive v_glyph = 0;
  Variant v_dest;

  (v_epsilon = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(50);
    Variant map51 = m_Q.rvalAt(v_label).o_get("nDelta", 0x2DD4E716AA366D03LL);
    for (ArrayIterPtr iter52 = map51.begin("intermediate_parser_form"); !iter52->end(); iter52->next()) {
      LOOP_COUNTER_CHECK(50);
      v_dest = iter52->second();
      v_glyph = iter52->first();
      if (toBoolean(m_head.rvalAt(v_glyph).o_get("epsilon", 0x0861A5117A0CDD1ALL))) v_epsilon.set(v_dest, (1LL));
    }
  }
  return LINE(553,x_array_keys(v_epsilon));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 555 */
Variant c_intermediate_parser_form::t_eclose_of_pushes_from(CVarRef v_label) {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::eclose_of_pushes_from);
  Variant v_pushes;
  Variant v_out;
  Variant v_symbol;

  (v_pushes = LINE(556,x_array_keys(m_Q.rvalAt(v_label).o_get("nDelta", 0x2DD4E716AA366D03LL))));
  (v_out = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(53);
    for (ArrayIterPtr iter55 = v_pushes.begin("intermediate_parser_form"); !iter55->end(); iter55->next()) {
      LOOP_COUNTER_CHECK(53);
      v_symbol = iter55->second();
      (v_out = LINE(558,x_array_merge(2, v_out, Array(ArrayInit(1).set(0, m_head.rvalAt(v_symbol).o_get("eclose", 0x41416ABDF406929ELL)).create()))));
    }
  }
  return v_out;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 561 */
Variant c_intermediate_parser_form::t_token_eating_front(CVarRef v_label) {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::token_eating_front);
  Variant v_queue;

  (v_queue = LINE(562,t_epsilon_closure_for_state(v_label)));
  return LINE(563,f_recursive_closure(Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "eclose_of_pushes_from").create()), v_queue));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 565 */
Variant c_intermediate_parser_form::t_epsilon_closure_for_state(CVarRef v_label) {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::epsilon_closure_for_state);
  return LINE(566,f_recursive_closure(Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "epsilon_for_state").create()), Array(ArrayInit(1).set(0, v_label).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 568 */
void c_intermediate_parser_form::ti_add_deltas(const char* cls, Variant v_set, CVarRef v_delta, CStrRef v_what) {
  STATIC_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::add_deltas);
  Primitive v_glyph = 0;
  Variant v_dest;

  {
    LOOP_COUNTER(56);
    for (ArrayIterPtr iter58 = v_delta.begin("intermediate_parser_form"); !iter58->end(); iter58->next()) {
      LOOP_COUNTER_CHECK(56);
      v_dest = iter58->second();
      v_glyph = iter58->first();
      {
        if (isset(v_set, v_glyph)) throw_exception(((Object)(LINE(570,p_exception(p_exception(NEWOBJ(c_exception)())->create(concat5("Epsilon Nondeterminism before ", toString(v_glyph), " while processing ", v_what, ".")))))));
        v_set.set(v_glyph, (v_dest));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 574 */
void c_intermediate_parser_form::t_compute_head_closures() {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::compute_head_closures);
  Variant v_head;
  Variant v_t_set;
  Variant v_n_set;
  Variant v_label;
  Variant v_q;

  {
    LOOP_COUNTER(59);
    Variant map60 = m_head;
    for (ArrayIterPtr iter61 = map60.begin("intermediate_parser_form"); !iter61->end(); iter61->next()) {
      LOOP_COUNTER_CHECK(59);
      v_head = iter61->second();
      {
        (v_head.o_lval("eclose", 0x41416ABDF406929ELL) = LINE(577,t_epsilon_closure_for_state(v_head.o_get("start", 0x00B621CE3C3982D5LL))));
        (v_t_set = ScalarArrays::sa_[0]);
        (v_n_set = ScalarArrays::sa_[0]);
        {
          LOOP_COUNTER(62);
          Variant map63 = v_head.o_get("eclose", 0x41416ABDF406929ELL);
          for (ArrayIterPtr iter64 = map63.begin("intermediate_parser_form"); !iter64->end(); iter64->next()) {
            LOOP_COUNTER_CHECK(62);
            v_label = iter64->second();
            {
              (v_q = m_Q.rvalAt(v_label));
              LINE(582,c_intermediate_parser_form::t_add_deltas(ref(v_t_set), v_q.o_get("tDelta", 0x4130FDB2E0784D79LL), toString("nonterminal ") + toString(v_head.o_get("symbol", 0x4A4181CA6D57B493LL))));
              LINE(583,c_intermediate_parser_form::t_add_deltas(ref(v_n_set), v_q.o_get("nDelta", 0x2DD4E716AA366D03LL), toString("nonterminal ") + toString(v_head.o_get("symbol", 0x4A4181CA6D57B493LL))));
            }
          }
        }
        (v_head.o_lval("t_set", 0x25BED83821720B26LL) = v_t_set);
        (v_head.o_lval("n_set", 0x2DE6AF43513BF754LL) = v_n_set);
      }
    }
  }
  {
    LOOP_COUNTER(65);
    Variant map66 = m_head;
    for (ArrayIterPtr iter67 = map66.begin("intermediate_parser_form"); !iter67->end(); iter67->next()) {
      LOOP_COUNTER_CHECK(65);
      v_head = iter67->second();
      LINE(589,t_compute_lt_closure(v_head));
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 591 */
void c_intermediate_parser_form::t_compute_lt_closure(Variant v_head) {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::compute_lt_closure);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_closure;
  Variant v_seen;
  Variant v_queue;
  Variant v_symbol;
  Variant v_H;

  (v_closure = v_head.o_get("t_set", 0x25BED83821720B26LL));
  (v_seen = Array(ArrayInit(1).set(0, v_head.o_get("symbol", 0x4A4181CA6D57B493LL), true).create()));
  (v_queue = LINE(594,x_array_keys(v_head.o_get("n_set", 0x2DE6AF43513BF754LL))));
  LOOP_COUNTER(68);
  {
    while (toBoolean(LINE(595,x_count(v_queue)))) {
      LOOP_COUNTER_CHECK(68);
      {
        (v_symbol = LINE(596,x_array_shift(ref(v_queue))));
        if (isset(v_seen, v_symbol)) continue;
        v_seen.set(v_symbol, (true));
        (v_H = m_head.rvalAt(v_symbol));
        LINE(600,(assignCallTemp(eo_0, ref(v_closure)),assignCallTemp(eo_1, v_H.o_get("t_set", 0x25BED83821720B26LL)),assignCallTemp(eo_2, concat4("LT closure for ", toString(v_head.o_get("symbol", 0x4A4181CA6D57B493LL)), ", looking at ", toString(v_H.o_get("symbol", 0x4A4181CA6D57B493LL)))),c_intermediate_parser_form::t_add_deltas(eo_0, eo_1, eo_2)));
        (v_queue = LINE(601,(assignCallTemp(eo_0, v_queue),assignCallTemp(eo_1, x_array_keys(v_H.o_get("n_set", 0x2DE6AF43513BF754LL))),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
      }
    }
  }
  (v_head.o_lval("lt_close", 0x061697A6F415D5D8LL) = LINE(603,x_array_keys(v_closure)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 605 */
void c_intermediate_parser_form::t_add_rule(Primitive v_head, CVarRef v_prod) {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::add_rule);
  Primitive v_mark = 0;
  Variant v_code;
  Variant v_dfa;
  Variant v_start;
  Variant v_q;
  Variant v_tDelta;
  Variant v_nDelta;
  Primitive v_glyph = 0;
  Variant v_label;

  {
    LOOP_COUNTER(69);
    Variant map70 = toObject(v_prod).o_get("code", 0x5B2CD7DDAB7A1DECLL);
    for (ArrayIterPtr iter71 = map70.begin("intermediate_parser_form"); !iter71->end(); iter71->next()) {
      LOOP_COUNTER_CHECK(69);
      v_code = iter71->second();
      v_mark = iter71->first();
      m_action.set(v_mark, (v_code));
    }
  }
  (v_dfa = toObject(v_prod).o_get("dfa", 0x215A370B1DA0C8ABLL));
  (v_start = v_dfa.o_get("initial", 0x4B073B3CA18015E7LL));
  m_head.set(v_head, (((Object)(LINE(609,p_if_head(p_if_head(NEWOBJ(c_if_head)())->create(v_head, v_start, v_dfa.o_get("final", 0x5192930B2145036ELL).rvalAt(v_start))))))));
  {
    LOOP_COUNTER(72);
    Variant map73 = v_dfa.o_get("states", 0x6FB528E40A4C1A15LL);
    for (ArrayIterPtr iter74 = map73.begin("intermediate_parser_form"); !iter74->end(); iter74->next()) {
      LOOP_COUNTER_CHECK(72);
      v_q = iter74->second();
      {
        (v_tDelta = ScalarArrays::sa_[0]);
        (v_nDelta = ScalarArrays::sa_[0]);
        {
          LOOP_COUNTER(75);
          Variant map76 = v_dfa.o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_q);
          for (ArrayIterPtr iter77 = map76.begin("intermediate_parser_form"); !iter77->end(); iter77->next()) {
            LOOP_COUNTER_CHECK(75);
            v_label = iter77->second();
            v_glyph = iter77->first();
            if (LINE(614,x_in_array(v_glyph, o_get("NT", 0x644F350DE02C70D6LL)))) {
              v_nDelta.set(v_glyph, (v_label));
            }
            else {
              v_tDelta.set(v_glyph, (v_label));
            }
          }
        }
        m_Q.set(v_q, (((Object)(LINE(626,p_if_state(p_if_state(NEWOBJ(c_if_state)())->create(v_q, v_dfa.o_get("initial", 0x4B073B3CA18015E7LL), v_dfa.o_get("final", 0x5192930B2145036ELL).rvalAt(v_q), v_dfa.o_get("mark", 0x19D2840C0AB0CFCFLL).rvalAt(v_q), v_tDelta, v_nDelta)))))));
        lval(o_lval("symbol", 0x4A4181CA6D57B493LL)).set(v_q, (v_head));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 630 */
Array c_intermediate_parser_form::t_compose_program() {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::compose_program);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Primitive v_k = 0;
  Variant v_body;

  {
    LOOP_COUNTER(78);
    Variant map79 = m_action;
    for (ArrayIterPtr iter80 = map79.begin("intermediate_parser_form"); !iter80->end(); iter80->next()) {
      LOOP_COUNTER_CHECK(78);
      v_body = iter80->second();
      v_k = iter80->first();
      {
        m_action.set(v_k, (LINE(632,(assignCallTemp(eo_1, x_preg_replace("/{(\\d+)}/", "$tokens[\\1]", v_body)),f_define_function("$tokens", eo_1)))));
      }
    }
  }
  return (assignCallTemp(eo_0, LINE(635,t_symbol_start_states())),assignCallTemp(eo_1, LINE(636,t_dpda_transition_function())),assignCallTemp(eo_2, m_action),Array(ArrayInit(3).set(0, "start", eo_0, 0x00B621CE3C3982D5LL).set(1, "delta", eo_1, 0x26E26E77DC67FF3FLL).set(2, "action", eo_2, 0x4CCA7F022EF68151LL).create()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 640 */
Variant c_intermediate_parser_form::t_dpda_transition_function() {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::dpda_transition_function);
  Variant v_delta;
  Primitive v_label = 0;
  Variant v_q;

  (v_delta = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(81);
    Variant map82 = m_Q;
    for (ArrayIterPtr iter83 = map82.begin("intermediate_parser_form"); !iter83->end(); iter83->next()) {
      LOOP_COUNTER_CHECK(81);
      v_q = iter83->second();
      v_label = iter83->first();
      v_delta.set(v_label, (LINE(642,v_q.o_invoke_few_args("delta", 0x0BB4BFEC21F2D246LL, 0))));
    }
  }
  return v_delta;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 645 */
Variant c_intermediate_parser_form::t_symbol_start_states() {
  INSTANCE_METHOD_INJECTION(intermediate_parser_form, intermediate_parser_form::symbol_start_states);
  Variant v_start;
  Primitive v_symbol = 0;
  Variant v_head;

  (v_start = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(84);
    Variant map85 = m_head;
    for (ArrayIterPtr iter86 = map85.begin("intermediate_parser_form"); !iter86->end(); iter86->next()) {
      LOOP_COUNTER_CHECK(84);
      v_head = iter86->second();
      v_symbol = iter86->first();
      v_start.set(v_symbol, (v_head.o_get("start", 0x00B621CE3C3982D5LL)));
    }
  }
  return v_start;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 296 */
Variant c_production_body::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_production_body::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_production_body::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_production_body::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_production_body::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_production_body::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_production_body::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_production_body::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(production_body)
ObjectData *c_production_body::create(CVarRef v_nfa, CVarRef v_action) {
  init();
  t_production_body(v_nfa, v_action);
  return this;
}
ObjectData *c_production_body::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
ObjectData *c_production_body::cloneImpl() {
  c_production_body *obj = NEW(c_production_body)();
  cloneSet(obj);
  return obj;
}
void c_production_body::cloneSet(c_production_body *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_production_body::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_production_body::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_production_body::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_production_body$os_get(const char *s) {
  return c_production_body::os_get(s, -1);
}
Variant &cw_production_body$os_lval(const char *s) {
  return c_production_body::os_lval(s, -1);
}
Variant cw_production_body$os_constant(const char *s) {
  return c_production_body::os_constant(s);
}
Variant cw_production_body$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_production_body::os_invoke(c, s, params, -1, fatal);
}
void c_production_body::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 297 */
void c_production_body::t_production_body(CVarRef v_nfa, CVarRef v_action) {
  INSTANCE_METHOD_INJECTION(production_body, production_body::production_body);
  bool oldInCtor = gasInCtor(true);
  (o_lval("nfa", 0x436202EA6F83B1EBLL) = v_nfa);
  (o_lval("code", 0x5B2CD7DDAB7A1DECLL) = LINE(299,x_preg_replace("/{(\\d+)}/", "$tokens[\\1]", v_action)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 105 */
Variant c_recursive_descent_parser::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_recursive_descent_parser::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_recursive_descent_parser::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("peek", m_peek.isReferenced() ? ref(m_peek) : m_peek));
  props.push_back(NEW(ArrayElement)("lex", m_lex.isReferenced() ? ref(m_lex) : m_lex));
  c_ObjectData::o_get(props);
}
bool c_recursive_descent_parser::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x4C667A4D924A91F8LL, peek, 4);
      break;
    case 1:
      HASH_EXISTS_STRING(0x4714454E685BA94DLL, lex, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_recursive_descent_parser::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x4C667A4D924A91F8LL, m_peek,
                         peek, 4);
      break;
    case 1:
      HASH_RETURN_STRING(0x4714454E685BA94DLL, m_lex,
                         lex, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_recursive_descent_parser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x4C667A4D924A91F8LL, m_peek,
                      peek, 4);
      break;
    case 1:
      HASH_SET_STRING(0x4714454E685BA94DLL, m_lex,
                      lex, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_recursive_descent_parser::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x4C667A4D924A91F8LL, m_peek,
                         peek, 4);
      break;
    case 1:
      HASH_RETURN_STRING(0x4714454E685BA94DLL, m_lex,
                         lex, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_recursive_descent_parser::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(recursive_descent_parser)
ObjectData *c_recursive_descent_parser::create(Variant v_lex) {
  init();
  t___construct(v_lex);
  return this;
}
ObjectData *c_recursive_descent_parser::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_recursive_descent_parser::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_recursive_descent_parser::cloneImpl() {
  c_recursive_descent_parser *obj = NEW(c_recursive_descent_parser)();
  cloneSet(obj);
  return obj;
}
void c_recursive_descent_parser::cloneSet(c_recursive_descent_parser *clone) {
  clone->m_peek = m_peek.isReferenced() ? ref(m_peek) : m_peek;
  clone->m_lex = m_lex.isReferenced() ? ref(m_lex) : m_lex;
  ObjectData::cloneSet(clone);
}
Variant c_recursive_descent_parser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_recursive_descent_parser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_recursive_descent_parser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_recursive_descent_parser$os_get(const char *s) {
  return c_recursive_descent_parser::os_get(s, -1);
}
Variant &cw_recursive_descent_parser$os_lval(const char *s) {
  return c_recursive_descent_parser::os_lval(s, -1);
}
Variant cw_recursive_descent_parser$os_constant(const char *s) {
  return c_recursive_descent_parser::os_constant(s);
}
Variant cw_recursive_descent_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_recursive_descent_parser::os_invoke(c, s, params, -1, fatal);
}
void c_recursive_descent_parser::init() {
  m_peek = null;
  m_lex = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 108 */
void c_recursive_descent_parser::t___construct(Variant v_lex) {
  INSTANCE_METHOD_INJECTION(recursive_descent_parser, recursive_descent_parser::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_lex = v_lex);
  (m_peek = null);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 112 */
Variant c_recursive_descent_parser::t_parse_terminal(CStrRef v_type) {
  INSTANCE_METHOD_INJECTION(recursive_descent_parser, recursive_descent_parser::parse_terminal);
  Variant v_t;

  (v_t = LINE(113,t_next()));
  if (!equal(v_t.o_get("type", 0x508FC7C8724A760ALL), v_type)) LINE(114,t_bomb(v_type, v_t));
  return v_t.o_get("text", 0x2A28A0084DD3A743LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 118 */
void c_recursive_descent_parser::t_bomb(CStrRef v_wanted, Variant v_got) {
  INSTANCE_METHOD_INJECTION(recursive_descent_parser, recursive_descent_parser::bomb);
  echo(LINE(119,concat3("Grammar Syntax Error - wanted ", v_wanted, ". Got:<br/>\n")));
  LINE(120,invoke_failed("pr", Array(ArrayInit(1).set(0, ref(v_got)).create()), 0x000000002703938CLL));
  LINE(121,m_lex.o_invoke_few_args("report_error", 0x5D0A00D8D1C24295LL, 0));
  throw_exception(LINE(122,create_object("parse_error", Array(ArrayInit(1).set(0, concat4("Grammar Syntax Error - wanted ", v_wanted, ". Got: ", toString(v_got.o_get("type", 0x508FC7C8724A760ALL)))).create()))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 125 */
bool c_recursive_descent_parser::t_can_parse_terminal(CStrRef v_type) {
  INSTANCE_METHOD_INJECTION(recursive_descent_parser, recursive_descent_parser::can_parse_terminal);
  return equal(LINE(126,t_peek()), v_type);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 129 */
Variant c_recursive_descent_parser::t_peek() {
  INSTANCE_METHOD_INJECTION(recursive_descent_parser, recursive_descent_parser::peek);
  if (same(m_peek, null)) (m_peek = LINE(130,m_lex.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0)));
  return m_peek.o_get("type", 0x508FC7C8724A760ALL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 133 */
Variant c_recursive_descent_parser::t_next() {
  INSTANCE_METHOD_INJECTION(recursive_descent_parser, recursive_descent_parser::next);
  Variant v_peek;

  if (same(m_peek, null)) return LINE(134,m_lex.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0));
  else {
    (v_peek = m_peek);
    (m_peek = null);
    return v_peek;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 405 */
Variant c_if_state::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_if_state::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_if_state::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_if_state::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_if_state::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_if_state::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_if_state::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_if_state::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(if_state)
ObjectData *c_if_state::create(Variant v_id, Variant v_start, Variant v_final, Variant v_mark, Variant v_tDelta, Variant v_nDelta) {
  init();
  t___construct(v_id, v_start, v_final, v_mark, v_tDelta, v_nDelta);
  return this;
}
ObjectData *c_if_state::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
  } else return this;
}
void c_if_state::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
}
ObjectData *c_if_state::cloneImpl() {
  c_if_state *obj = NEW(c_if_state)();
  cloneSet(obj);
  return obj;
}
void c_if_state::cloneSet(c_if_state *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_if_state::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x64586199BBCD6CB1LL, is_initial) {
        return (t_is_initial());
      }
      break;
    case 2:
      HASH_GUARD(0x0D36C46050BBEBD2LL, add_step) {
        return (t_add_step(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x0BB4BFEC21F2D246LL, delta) {
        return (t_delta());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)), null);
      }
      HASH_GUARD(0x3D1685902EC9512FLL, set_default) {
        return (t_set_default(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_if_state::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x64586199BBCD6CB1LL, is_initial) {
        return (t_is_initial());
      }
      break;
    case 2:
      HASH_GUARD(0x0D36C46050BBEBD2LL, add_step) {
        return (t_add_step(a0, a1), null);
      }
      break;
    case 6:
      HASH_GUARD(0x0BB4BFEC21F2D246LL, delta) {
        return (t_delta());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2, a3, a4, a5), null);
      }
      HASH_GUARD(0x3D1685902EC9512FLL, set_default) {
        return (t_set_default(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_if_state::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_if_state$os_get(const char *s) {
  return c_if_state::os_get(s, -1);
}
Variant &cw_if_state$os_lval(const char *s) {
  return c_if_state::os_lval(s, -1);
}
Variant cw_if_state$os_constant(const char *s) {
  return c_if_state::os_constant(s);
}
Variant cw_if_state$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_if_state::os_invoke(c, s, params, -1, fatal);
}
void c_if_state::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 406 */
void c_if_state::t___construct(Variant v_id, Variant v_start, Variant v_final, Variant v_mark, Variant v_tDelta, Variant v_nDelta) {
  INSTANCE_METHOD_INJECTION(if_state, if_state::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("id", 0x028B9FE0C4522BE2LL) = v_id);
  (o_lval("start", 0x00B621CE3C3982D5LL) = v_start);
  (o_lval("final", 0x5192930B2145036ELL) = v_final);
  (o_lval("mark", 0x19D2840C0AB0CFCFLL) = v_mark);
  (o_lval("tDelta", 0x4130FDB2E0784D79LL) = v_tDelta);
  (o_lval("nDelta", 0x2DD4E716AA366D03LL) = v_nDelta);
  (o_lval("steps", 0x0999F1ECD0C495C2LL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 415 */
void c_if_state::t_add_step(CVarRef v_glyph, CVarRef v_step) {
  INSTANCE_METHOD_INJECTION(if_state, if_state::add_step);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  if (isset(o_get("steps", 0x0999F1ECD0C495C2LL), v_glyph)) echo((concat(concat_rev(toString(LINE(420,toObject(v_step)->o_invoke_few_args("phrase", 0x567618960DB2F01ELL, 0))), LINE(419,(assignCallTemp(eo_1, toString(o_get("id", 0x028B9FE0C4522BE2LL))),assignCallTemp(eo_3, toString(LINE(418,o_get("steps", 0x0999F1ECD0C495C2LL).rvalAt(v_glyph).o_invoke_few_args("phrase", 0x567618960DB2F01ELL, 0)))),assignCallTemp(eo_4, LINE(419,concat3(" in the presence of ", toString(v_glyph), ", but is instead being told to "))),concat5("State ", eo_1, " thought that it should ", eo_3, eo_4)))), ".<br>\n")));
  lval(o_lval("steps", 0x0999F1ECD0C495C2LL)).set(v_glyph, (v_step));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 425 */
Variant c_if_state::t_delta() {
  INSTANCE_METHOD_INJECTION(if_state, if_state::delta);
  Variant v_delta;
  Primitive v_glyph = 0;
  Variant v_step;

  (v_delta = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(87);
    Variant map88 = o_get("steps", 0x0999F1ECD0C495C2LL);
    for (ArrayIterPtr iter89 = map88.begin("if_state"); !iter89->end(); iter89->next()) {
      LOOP_COUNTER_CHECK(87);
      v_step = iter89->second();
      v_glyph = iter89->first();
      v_delta.set(v_glyph, (LINE(427,v_step.o_invoke_few_args("instruction", 0x06E1BDEA89231CBDLL, 0))));
    }
  }
  return v_delta;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 430 */
bool c_if_state::t_is_initial() {
  INSTANCE_METHOD_INJECTION(if_state, if_state::is_initial);
  return equal(o_get("id", 0x028B9FE0C4522BE2LL), o_get("start", 0x00B621CE3C3982D5LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 431 */
void c_if_state::t_set_default(CVarRef v_step) {
  INSTANCE_METHOD_INJECTION(if_state, if_state::set_default);
  LINE(431,t_add_step("[default]", v_step));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 143 */
Variant c_grammar_parser::os_get(const char *s, int64 hash) {
  return c_recursive_descent_parser::os_get(s, hash);
}
Variant &c_grammar_parser::os_lval(const char *s, int64 hash) {
  return c_recursive_descent_parser::os_lval(s, hash);
}
void c_grammar_parser::o_get(ArrayElementVec &props) const {
  c_recursive_descent_parser::o_get(props);
}
bool c_grammar_parser::o_exists(CStrRef s, int64 hash) const {
  return c_recursive_descent_parser::o_exists(s, hash);
}
Variant c_grammar_parser::o_get(CStrRef s, int64 hash) {
  return c_recursive_descent_parser::o_get(s, hash);
}
Variant c_grammar_parser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_recursive_descent_parser::o_set(s, hash, v, forInit);
}
Variant &c_grammar_parser::o_lval(CStrRef s, int64 hash) {
  return c_recursive_descent_parser::o_lval(s, hash);
}
Variant c_grammar_parser::os_constant(const char *s) {
  return c_recursive_descent_parser::os_constant(s);
}
IMPLEMENT_CLASS(grammar_parser)
ObjectData *c_grammar_parser::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_grammar_parser::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_grammar_parser::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_grammar_parser::cloneImpl() {
  c_grammar_parser *obj = NEW(c_grammar_parser)();
  cloneSet(obj);
  return obj;
}
void c_grammar_parser::cloneSet(c_grammar_parser *clone) {
  c_recursive_descent_parser::cloneSet(clone);
}
Variant c_grammar_parser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_recursive_descent_parser::o_invoke(s, params, hash, fatal);
}
Variant c_grammar_parser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_recursive_descent_parser::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_grammar_parser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_recursive_descent_parser::os_invoke(c, s, params, hash, fatal);
}
Variant cw_grammar_parser$os_get(const char *s) {
  return c_grammar_parser::os_get(s, -1);
}
Variant &cw_grammar_parser$os_lval(const char *s) {
  return c_grammar_parser::os_lval(s, -1);
}
Variant cw_grammar_parser$os_constant(const char *s) {
  return c_grammar_parser::os_constant(s);
}
Variant cw_grammar_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_grammar_parser::os_invoke(c, s, params, -1, fatal);
}
void c_grammar_parser::init() {
  c_recursive_descent_parser::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 144 */
void c_grammar_parser::t___construct() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(145,c_recursive_descent_parser::t___construct(((Object)(p_grammar_scanner(p_grammar_scanner(NEWOBJ(c_grammar_scanner)())->create())))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 148 */
p_grammar c_grammar_parser::t_parse_file(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_file);
  LINE(149,m_lex.o_invoke_few_args("start", 0x3970634433FD3E52LL, 1, x_file_get_contents(toString(v_name))));
  return ((Object)(LINE(150,t_parse_grammar())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 153 */
p_grammar c_grammar_parser::t_parse_grammar() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_grammar);
  p_grammar v_g;
  p_production v_p;

  ((Object)((v_g = ((Object)(LINE(154,p_grammar(p_grammar(NEWOBJ(c_grammar)())->create())))))));
  LOOP_COUNTER(90);
  {
    while (LINE(155,t_can_parse_production())) {
      LOOP_COUNTER_CHECK(90);
      {
        ((Object)((v_p = ((Object)(LINE(156,t_parse_production()))))));
        LINE(157,v_g->t_add(((Object)(v_p))));
      }
    }
  }
  return ((Object)(v_g));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 162 */
p_production c_grammar_parser::t_parse_production() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_production);
  Variant v_head;
  p_production_body v_body;
  p_production v_p;

  (v_head = LINE(163,t_parse_terminal("token")));
  LINE(164,t_parse_terminal("colon"));
  ((Object)((v_body = ((Object)(LINE(165,t_parse_body()))))));
  ((Object)((v_p = ((Object)(LINE(166,p_production(p_production(NEWOBJ(c_production)())->create(v_head))))))));
  LINE(167,v_p->t_add(((Object)(v_body))));
  LOOP_COUNTER(91);
  {
    while (LINE(168,t_can_parse_terminal("union"))) {
      LOOP_COUNTER_CHECK(91);
      {
        LINE(169,t_parse_terminal("union"));
        LINE(170,v_p->t_add(((Object)(t_parse_body()))));
      }
    }
  }
  return ((Object)(v_p));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 175 */
bool c_grammar_parser::t_can_parse_production() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::can_parse_production);
  return LINE(177,t_can_parse_terminal("token"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 180 */
p_production_body c_grammar_parser::t_parse_body() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_body);
  Variant v_r;
  Variant v_a;

  (v_r = LINE(181,t_parse_regex()));
  (v_a = LINE(182,t_parse_action()));
  return ((Object)(LINE(183,p_production_body(p_production_body(NEWOBJ(c_production_body)())->create(v_r, v_a)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 186 */
Variant c_grammar_parser::t_parse_action() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_action);
  Variant v_code;

  LINE(188,t_parse_terminal("openbrace"));
  (v_code = LINE(190,t_parse_code()));
  LINE(191,t_parse_terminal("closebrace"));
  return v_code;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 195 */
Variant c_grammar_parser::t_parse_code() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_code);
  String v_code;
  Variant v_inside;
  Variant v_t;

  (v_code = "");
  LOOP_COUNTER(92);
  {
    while (toBoolean(1LL)) {
      LOOP_COUNTER_CHECK(92);
      {
        Variant tmp94 = (LINE(197,t_peek()));
        int tmp95 = -1;
        if (equal(tmp94, ("openbrace"))) {
          tmp95 = 0;
        } else if (equal(tmp94, ("php"))) {
          tmp95 = 1;
        } else if (equal(tmp94, ("sstring"))) {
          tmp95 = 2;
        } else if (equal(tmp94, ("dstring"))) {
          tmp95 = 3;
        } else if (equal(tmp94, ("linecomment"))) {
          tmp95 = 4;
        } else if (equal(tmp94, ("blockcomment"))) {
          tmp95 = 5;
        } else if (true) {
          tmp95 = 6;
        }
        switch (tmp95) {
        case 0:
          {
            LINE(199,t_parse_terminal("openbrace"));
            (v_inside = LINE(200,t_parse_code()));
            LINE(201,t_parse_terminal("closebrace"));
            concat_assign(v_code, LINE(202,concat3("{", toString(v_inside), "}")));
            goto break93;
          }
        case 1:
          {
          }
        case 2:
          {
          }
        case 3:
          {
          }
        case 4:
          {
          }
        case 5:
          {
            (v_t = LINE(210,t_next()));
            concat_assign(v_code, toString(v_t.o_get("text", 0x2A28A0084DD3A743LL)));
            goto break93;
          }
        case 6:
          {
            return v_code;
          }
        }
        break93:;
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 219 */
Variant c_grammar_parser::t_parse_regex() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_regex);
  Variant v_t;
  Variant v_them;

  (v_t = LINE(220,t_parse_term()));
  if (!(LINE(221,t_can_parse_term()))) return v_t;
  (v_them = Array(ArrayInit(1).set(0, v_t).create()));
  LOOP_COUNTER(96);
  {
    while (LINE(223,t_can_parse_term())) {
      LOOP_COUNTER_CHECK(96);
      {
        v_them.append((LINE(224,t_parse_term())));
      }
    }
  }
  return LINE(226,invoke_failed("nfa_concat", Array(ArrayInit(1).set(0, ref(v_them)).create()), 0x00000000C1CB8861LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 229 */
Variant c_grammar_parser::t_parse_term() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_term);
  Variant v_f;

  (v_f = LINE(230,t_parse_factor()));
  {
    Variant tmp98 = (LINE(231,t_peek()));
    int tmp99 = -1;
    if (equal(tmp98, ("star"))) {
      tmp99 = 0;
    } else if (equal(tmp98, ("plus"))) {
      tmp99 = 1;
    } else if (equal(tmp98, ("hook"))) {
      tmp99 = 2;
    }
    switch (tmp99) {
    case 0:
      {
        LINE(233,t_next());
        LINE(234,v_f.o_invoke_few_args("kleene", 0x64AD93107C8825ECLL, 0));
        goto break97;
      }
    case 1:
      {
        LINE(238,t_next());
        LINE(239,v_f.o_invoke_few_args("plus", 0x5E8B71F5814EF937LL, 0));
        goto break97;
      }
    case 2:
      {
        LINE(243,t_next());
        LINE(244,v_f.o_invoke_few_args("hook", 0x5EB06B2FF1963CAALL, 0));
        goto break97;
      }
    }
    break97:;
  }
  return v_f;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 250 */
bool c_grammar_parser::t_can_parse_term() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::can_parse_term);
  return LINE(251,t_can_parse_factor());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 254 */
bool c_grammar_parser::t_can_parse_factor() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::can_parse_factor);
  {
    Variant tmp101 = (LINE(255,t_peek()));
    int tmp102 = -1;
    if (equal(tmp101, ("leftparen"))) {
      tmp102 = 0;
    } else if (equal(tmp101, ("token"))) {
      tmp102 = 1;
    } else if (equal(tmp101, ("epsilon"))) {
      tmp102 = 2;
    }
    switch (tmp102) {
    case 0:
      {
      }
    case 1:
      {
      }
    case 2:
      {
        return true;
      }
    }
  }
  return false;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 264 */
Variant c_grammar_parser::t_parse_factor() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_factor);
  Variant v_a;
  Variant v_t;
  Object v_n;

  {
    Variant tmp104 = (LINE(265,t_peek()));
    int tmp105 = -1;
    if (equal(tmp104, ("leftparen"))) {
      tmp105 = 0;
    } else if (equal(tmp104, ("token"))) {
      tmp105 = 1;
    } else if (equal(tmp104, ("epsilon"))) {
      tmp105 = 2;
    }
    switch (tmp105) {
    case 0:
      {
        LINE(267,t_next());
        (v_a = LINE(268,t_parse_alternates()));
        LINE(269,t_parse_terminal("rightparen"));
        return v_a;
      }
    case 1:
      {
        (v_t = LINE(273,t_parse_terminal("token")));
        (v_n = LINE(274,create_object("enfa", Array())));
        v_n->o_invoke_few_args("recognize", 0x4AADC3C6A84B9CBCLL, 1, v_t);
        return v_n;
      }
    case 2:
      {
        (v_t = LINE(277,t_parse_terminal("epsilon")));
        (v_n = LINE(278,create_object("enfa", Array())));
        v_n->o_invoke_few_args("add_epsilon", 0x17753A1C02F94099LL, 2, v_n.o_lval("initial", 0x4B073B3CA18015E7LL), v_n.o_lval("final", 0x5192930B2145036ELL));
        return v_n;
      }
    }
  }
  f_exit("CFG/PG got stuck parsing factors.");
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 283 */
Variant c_grammar_parser::t_parse_alternates() {
  INSTANCE_METHOD_INJECTION(grammar_parser, grammar_parser::parse_alternates);
  Variant v_r;
  Variant v_them;

  (v_r = LINE(284,t_parse_regex()));
  if (!(LINE(285,t_can_parse_terminal("union")))) return v_r;
  (v_them = Array(ArrayInit(1).set(0, v_r).create()));
  LOOP_COUNTER(106);
  {
    while (LINE(287,t_can_parse_terminal("union"))) {
      LOOP_COUNTER_CHECK(106);
      {
        LINE(288,t_next());
        v_them.append((LINE(289,t_parse_regex())));
      }
    }
  }
  return LINE(291,invoke_failed("nfa_union", Array(ArrayInit(1).set(0, ref(v_them)).create()), 0x00000000AB96B737LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 20 */
Variant f_define_function(Variant v_args, Variant v_code) {
  FUNCTION_INJECTION(define_function);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_counter __attribute__((__unused__)) = g->sv_define_function_DupIdcounter;
  bool &inited_sv_counter __attribute__((__unused__)) = g->inited_sv_define_function_DupIdcounter;
  Variant v_counter;
  Variant &sv_cache __attribute__((__unused__)) = g->sv_define_function_DupIdcache;
  bool &inited_sv_cache __attribute__((__unused__)) = g->inited_sv_define_function_DupIdcache;
  Variant v_cache;
  Variant v_s;
  Variant v_name;
  Variant v_php;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_args; Variant &v_code; Variant &v_counter; Variant &v_cache; Variant &v_s; Variant &v_name; Variant &v_php;
    VariableTable(Variant &r_args, Variant &r_code, Variant &r_counter, Variant &r_cache, Variant &r_s, Variant &r_name, Variant &r_php) : v_args(r_args), v_code(r_code), v_counter(r_counter), v_cache(r_cache), v_s(r_s), v_name(r_name), v_php(r_php) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      DECLARE_GLOBAL_VARIABLES(g);
      if (hash < 0) hash = hash_string(s);
      switch (hash & 15) {
        case 0:
          HASH_RETURN(0x3E854EA2326AD070LL, v_cache,
                      cache);
          HASH_RETURN(0x0BC2CDE4BBFC9C10LL, v_s,
                      s);
          break;
        case 1:
          HASH_RETURN(0x52B182B0A94E9C41LL, v_php,
                      php);
          break;
        case 8:
          HASH_RETURN(0x1297A95AAC6F5998LL, v_counter,
                      counter);
          break;
        case 12:
          HASH_RETURN(0x5B2CD7DDAB7A1DECLL, v_code,
                      code);
          HASH_RETURN(0x0BCDB293DC3CBDDCLL, v_name,
                      name);
          break;
        case 14:
          HASH_RETURN(0x4AF7CD17F976719ELL, v_args,
                      args);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_args, v_code, v_counter, v_cache, v_s, v_name, v_php);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  v_counter = ref(sv_counter);
  if (!inited_sv_counter) {
    (v_counter = null);
    inited_sv_counter = true;
  }
  v_cache = ref(sv_cache);
  if (!inited_sv_cache) {
    (v_cache = ScalarArrays::sa_[0]);
    inited_sv_cache = true;
  }
  (v_s = LINE(23,concat3(toString(v_args), "--", toString(v_code))));
  if (!(isset(v_cache, v_s))) {
    v_counter++;
    (v_name = toString("__lambda_") + toString(v_counter));
    (v_php = concat("function ", LINE(27,concat6(toString(v_name), " (", toString(v_args), ") {\n", toString(v_code), "\n}\n"))));
    f_eval(toString(v_php));
    concat_assign(g->GV(func_def), toString(v_php));
    v_cache.set(v_s, (v_name));
  }
  return v_cache.rvalAt(v_s);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 658 */
Variant f_generate_scanner_from_file(CVarRef v_filename, Variant v_init_context) {
  FUNCTION_INJECTION(generate_scanner_from_file);
  Variant eo_0;
  Variant eo_1;
  Variant v_metascanner;

  (v_metascanner = ((Object)(LINE(659,p_metascanner(p_metascanner(NEWOBJ(c_metascanner)())->create())))));
  LINE(660,v_metascanner.o_invoke_few_args("start", 0x3970634433FD3E52LL, 1, x_file_get_contents(toString(v_filename))));
  return (assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, LINE(661,f_scanner_parser())),eo_1.toObject()->o_invoke_few_args("parse", 0x46463F1C3624CEDELL, 2, "scanner", v_metascanner)))),eo_0.o_invoke_few_args("scanner", 0x60AF06C7E0B22521LL, 1, v_init_context));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 653 */
Variant f_generate_parser_from_file(CStrRef v_filename) {
  FUNCTION_INJECTION(generate_parser_from_file);
  p_grammar_parser v_rdp;
  p_grammar v_g;

  ((Object)((v_rdp = ((Object)(LINE(654,p_grammar_parser(p_grammar_parser(NEWOBJ(c_grammar_parser)())->create())))))));
  ((Object)((v_g = ((Object)(LINE(655,v_rdp->t_parse_file(v_filename)))))));
  return LINE(656,v_g->t_make_dpda());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 663 */
String f_location_of_scanner_grammar() {
  FUNCTION_INJECTION(location_of_scanner_grammar);
  return concat(LINE(664,x_dirname(get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php"))), "/metascanner.y");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 16 */
Variant f_mk_action(CStrRef v_code) {
  FUNCTION_INJECTION(mk_action);
  return LINE(17,f_define_function("&$type, &$text, $match, &$state, &$context", v_code));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 76 */
Variant f_recursive_closure(CArrRef v_callable, Variant v_queue) {
  FUNCTION_INJECTION(recursive_closure);
  Variant v_seen;
  Variant v_subject;
  Variant v_element;

  LINE(77,invoke_failed("bug_unless", Array(ArrayInit(1).set(0, ref(x_is_callable(v_callable))).create()), 0x00000000BD51F3BDLL));
  LINE(78,invoke_failed("bug_unless", Array(ArrayInit(1).set(0, ref(x_is_array(v_queue))).create()), 0x00000000BD51F3BDLL));
  (v_seen = ScalarArrays::sa_[0]);
  LOOP_COUNTER(107);
  {
    while (!(empty(v_queue))) {
      LOOP_COUNTER_CHECK(107);
      {
        (v_subject = LINE(81,x_array_shift(ref(v_queue))));
        if (isset(v_seen, v_subject)) continue;
        v_seen.set(v_subject, (true));
        {
          LOOP_COUNTER(108);
          Variant map109 = LINE(84,x_call_user_func(2, v_callable, Array(ArrayInit(1).set(0, v_subject).create())));
          for (ArrayIterPtr iter110 = map109.begin(); !iter110->end(); iter110->next()) {
            LOOP_COUNTER_CHECK(108);
            v_element = iter110->second();
            v_queue.append((v_element));
          }
        }
      }
    }
  }
  return LINE(86,x_array_keys(v_seen));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 666 */
Object f_scanner_parser() {
  FUNCTION_INJECTION(scanner_parser);
  return LINE(667,create_object("easy_parser", Array(ArrayInit(1).set(0, f_generate_parser_from_file(f_location_of_scanner_grammar())).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 100 */
int64 f_gen_mark() {
  FUNCTION_INJECTION(gen_mark);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_k __attribute__((__unused__)) = g->sv_gen_mark_DupIdk;
  bool &inited_sv_k __attribute__((__unused__)) = g->inited_sv_gen_mark_DupIdk;
  if (!inited_sv_k) {
    (sv_k = 0LL);
    inited_sv_k = true;
  }
  return sv_k++;
} /* function */
Object co_reduce_step(CArrRef params, bool init /* = true */) {
  return Object(p_reduce_step(NEW(c_reduce_step)())->dynCreate(params, init));
}
Object co_preg_scanner_definition(CArrRef params, bool init /* = true */) {
  return Object(p_preg_scanner_definition(NEW(c_preg_scanner_definition)())->dynCreate(params, init));
}
Object co_grammar_scanner(CArrRef params, bool init /* = true */) {
  return Object(p_grammar_scanner(NEW(c_grammar_scanner)())->dynCreate(params, init));
}
Object co_push_step(CArrRef params, bool init /* = true */) {
  return Object(p_push_step(NEW(c_push_step)())->dynCreate(params, init));
}
Object co_if_head(CArrRef params, bool init /* = true */) {
  return Object(p_if_head(NEW(c_if_head)())->dynCreate(params, init));
}
Object co_fold_step(CArrRef params, bool init /* = true */) {
  return Object(p_fold_step(NEW(c_fold_step)())->dynCreate(params, init));
}
Object co_parse_step(CArrRef params, bool init /* = true */) {
  return Object(p_parse_step(NEW(c_parse_step)())->dynCreate(params, init));
}
Object co_grammar(CArrRef params, bool init /* = true */) {
  return Object(p_grammar(NEW(c_grammar)())->dynCreate(params, init));
}
Object co_metascanner(CArrRef params, bool init /* = true */) {
  return Object(p_metascanner(NEW(c_metascanner)())->dynCreate(params, init));
}
Object co_production(CArrRef params, bool init /* = true */) {
  return Object(p_production(NEW(c_production)())->dynCreate(params, init));
}
Object co_eat_step(CArrRef params, bool init /* = true */) {
  return Object(p_eat_step(NEW(c_eat_step)())->dynCreate(params, init));
}
Object co_intermediate_parser_form(CArrRef params, bool init /* = true */) {
  return Object(p_intermediate_parser_form(NEW(c_intermediate_parser_form)())->dynCreate(params, init));
}
Object co_production_body(CArrRef params, bool init /* = true */) {
  return Object(p_production_body(NEW(c_production_body)())->dynCreate(params, init));
}
Object co_recursive_descent_parser(CArrRef params, bool init /* = true */) {
  return Object(p_recursive_descent_parser(NEW(c_recursive_descent_parser)())->dynCreate(params, init));
}
Object co_if_state(CArrRef params, bool init /* = true */) {
  return Object(p_if_state(NEW(c_if_state)())->dynCreate(params, init));
}
Object co_grammar_parser(CArrRef params, bool init /* = true */) {
  return Object(p_grammar_parser(NEW(c_grammar_parser)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$generator_so_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$generator_so_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  (g->GV(func_def) = "");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
