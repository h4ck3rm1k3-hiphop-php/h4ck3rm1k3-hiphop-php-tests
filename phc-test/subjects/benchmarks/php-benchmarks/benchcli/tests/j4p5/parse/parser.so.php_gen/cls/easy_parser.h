
#ifndef __GENERATED_cls_easy_parser_h__
#define __GENERATED_cls_easy_parser_h__

#include <cls/parser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 732 */
class c_easy_parser : virtual public c_parser {
  BEGIN_CLASS_MAP(easy_parser)
    PARENT_CLASS(parser)
  END_CLASS_MAP(easy_parser)
  DECLARE_CLASS(easy_parser, easy_parser, parser)
  void init();
  public: void t___construct(Variant v_pda, Variant v_strategy = null);
  public: ObjectData *create(Variant v_pda, Variant v_strategy = null);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_reduce(CVarRef v_action, Variant v_tokens);
  public: Variant t_parse(CVarRef v_symbol, CVarRef v_lex, CVarRef v_strategy = null_variant);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_easy_parser_h__
