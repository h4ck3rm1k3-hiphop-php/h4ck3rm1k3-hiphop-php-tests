
#ifndef __GENERATED_cls_production_body_h__
#define __GENERATED_cls_production_body_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 296 */
class c_production_body : virtual public ObjectData {
  BEGIN_CLASS_MAP(production_body)
  END_CLASS_MAP(production_body)
  DECLARE_CLASS(production_body, production_body, ObjectData)
  void init();
  public: void t_production_body(CVarRef v_nfa, CVarRef v_action);
  public: ObjectData *create(CVarRef v_nfa, CVarRef v_action);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_production_body_h__
