
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "eat_step", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "fold_step", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "grammar", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "grammar_parser", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "grammar_scanner", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "if_head", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "if_state", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "intermediate_parser_form", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "metascanner", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "parse_step", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "preg_scanner_definition", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "production", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "production_body", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "push_step", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "recursive_descent_parser", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "reduce_step", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "define_function", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "gen_mark", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "generate_parser_from_file", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "generate_scanner_from_file", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "location_of_scanner_grammar", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "mk_action", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "recursive_closure", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  "scanner_parser", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
