
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_parser_so_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_parser_so_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.fw.h>

// Declarations
#include <cls/parse_stack_frame.h>
#include <cls/parse_error.h>
#include <cls/dfa.h>
#include <cls/set.h>
#include <cls/easy_parser.h>
#include <cls/token_source.h>
#include <cls/bug.h>
#include <cls/parser_strategy.h>
#include <cls/distinguishing_table.h>
#include <cls/parser.h>
#include <cls/token.h>
#include <cls/preg_scanner.h>
#include <cls/point.h>
#include <cls/stream.h>
#include <cls/state_set_labeler.h>
#include <cls/enfa.h>
#include <cls/default_parser_strategy.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_span(CStrRef v_class, CVarRef v_text, Variant v_title = "");
p_enfa f_nfa_union(CArrRef v_nfa_list);
void f_send_parse_error_css_styles();
Variant f_preg_pattern_test(CVarRef v_pattern, CVarRef v_string);
String f_gen_label();
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$parser_so_php(bool incOnce = false, LVariableTable* variables = NULL);
p_token f_null_token();
Array f_preg_pattern(CVarRef v_regex, CVarRef v_type, CVarRef v_ignore, CVarRef v_action);
p_enfa f_nfa_concat(CArrRef v_nfa_list);
void f_bug_unless(CVarRef v_assertion, CStrRef v_gripe = "Bug found.");
Object co_parse_stack_frame(CArrRef params, bool init = true);
Object co_parse_error(CArrRef params, bool init = true);
Object co_dfa(CArrRef params, bool init = true);
Object co_set(CArrRef params, bool init = true);
Object co_easy_parser(CArrRef params, bool init = true);
Object co_token_source(CArrRef params, bool init = true);
Object co_bug(CArrRef params, bool init = true);
Object co_parser_strategy(CArrRef params, bool init = true);
Object co_distinguishing_table(CArrRef params, bool init = true);
Object co_parser(CArrRef params, bool init = true);
Object co_token(CArrRef params, bool init = true);
Object co_preg_scanner(CArrRef params, bool init = true);
Object co_point(CArrRef params, bool init = true);
Object co_stream(CArrRef params, bool init = true);
Object co_state_set_labeler(CArrRef params, bool init = true);
Object co_enfa(CArrRef params, bool init = true);
Object co_default_parser_strategy(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_parser_so_h__
