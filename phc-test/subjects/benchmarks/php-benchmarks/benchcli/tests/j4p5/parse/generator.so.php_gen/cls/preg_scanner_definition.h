
#ifndef __GENERATED_cls_preg_scanner_definition_h__
#define __GENERATED_cls_preg_scanner_definition_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 738 */
class c_preg_scanner_definition : virtual public ObjectData {
  BEGIN_CLASS_MAP(preg_scanner_definition)
  END_CLASS_MAP(preg_scanner_definition)
  DECLARE_CLASS(preg_scanner_definition, preg_scanner_definition, ObjectData)
  void init();
  public: void t___construct(Variant v_deflist);
  public: ObjectData *create(Variant v_deflist);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add_scope(CVarRef v_type, CVarRef v_id);
  public: void t_add_rule(CVarRef v_scope_list, CVarRef v_rule);
  public: void t_add_rule_list(CVarRef v_scope_list, CArrRef v_rule_list);
  public: void t_add_common_rule(CVarRef v_rule);
  public: Object t_scanner(CVarRef v_init_context);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_preg_scanner_definition_h__
