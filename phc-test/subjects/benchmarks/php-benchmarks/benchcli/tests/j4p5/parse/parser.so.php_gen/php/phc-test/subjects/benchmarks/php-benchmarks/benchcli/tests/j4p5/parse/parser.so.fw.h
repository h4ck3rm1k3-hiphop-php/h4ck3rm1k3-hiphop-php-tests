
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_parser_so_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_parser_so_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_FA_NO_MARK;


// 2. Classes
FORWARD_DECLARE_CLASS(parse_stack_frame)
FORWARD_DECLARE_CLASS(parse_error)
FORWARD_DECLARE_CLASS(dfa)
FORWARD_DECLARE_CLASS(set)
FORWARD_DECLARE_CLASS(easy_parser)
FORWARD_DECLARE_CLASS(token_source)
FORWARD_DECLARE_CLASS(bug)
FORWARD_DECLARE_CLASS(parser_strategy)
FORWARD_DECLARE_CLASS(distinguishing_table)
FORWARD_DECLARE_CLASS(parser)
FORWARD_DECLARE_CLASS(token)
FORWARD_DECLARE_CLASS(preg_scanner)
FORWARD_DECLARE_CLASS(point)
FORWARD_DECLARE_CLASS(stream)
FORWARD_DECLARE_CLASS(state_set_labeler)
FORWARD_DECLARE_CLASS(enfa)
FORWARD_DECLARE_CLASS(default_parser_strategy)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_parser_so_fw_h__
