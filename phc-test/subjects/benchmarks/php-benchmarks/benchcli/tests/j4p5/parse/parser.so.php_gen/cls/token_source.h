
#ifndef __GENERATED_cls_token_source_h__
#define __GENERATED_cls_token_source_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 577 */
class c_token_source : virtual public ObjectData {
  BEGIN_CLASS_MAP(token_source)
  END_CLASS_MAP(token_source)
  DECLARE_CLASS(token_source, token_source, ObjectData)
  void init();
  // public: void t_next() = 0;
  // public: void t_report_instant_description() = 0;
  public: void t_report_error();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_token_source_h__
