
#ifndef __GENERATED_cls_parser_h__
#define __GENERATED_cls_parser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 661 */
class c_parser : virtual public ObjectData {
  BEGIN_CLASS_MAP(parser)
  END_CLASS_MAP(parser)
  DECLARE_CLASS(parser, parser, ObjectData)
  void init();
  public: void t___construct(Variant v_pda);
  public: ObjectData *create(Variant v_pda);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_report();
  public: Variant t_get_step(CVarRef v_label, CVarRef v_glyph);
  public: Variant t_parse(CVarRef v_symbol, Variant v_lex, CVarRef v_strategy = null_variant);
  public: p_parse_stack_frame t_frame(CVarRef v_symbol);
  // public: void t_reduce(CVarRef v_action, CVarRef v_tokens) = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parser_h__
