
#ifndef __GENERATED_cls_if_head_h__
#define __GENERATED_cls_if_head_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 433 */
class c_if_head : virtual public ObjectData {
  BEGIN_CLASS_MAP(if_head)
  END_CLASS_MAP(if_head)
  DECLARE_CLASS(if_head, if_head, ObjectData)
  void init();
  public: void t___construct(Variant v_symbol, Variant v_start, Variant v_epsilon);
  public: ObjectData *create(Variant v_symbol, Variant v_start, Variant v_epsilon);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_if_head_h__
