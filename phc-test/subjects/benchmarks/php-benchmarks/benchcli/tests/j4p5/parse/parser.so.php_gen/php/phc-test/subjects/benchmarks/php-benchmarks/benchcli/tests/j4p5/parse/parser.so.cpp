
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_FA_NO_MARK = 99999LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 647 */
Variant c_parse_stack_frame::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parse_stack_frame::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parse_stack_frame::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("symbol", m_symbol.isReferenced() ? ref(m_symbol) : m_symbol));
  props.push_back(NEW(ArrayElement)("semantic", m_semantic.isReferenced() ? ref(m_semantic) : m_semantic));
  props.push_back(NEW(ArrayElement)("state", m_state.isReferenced() ? ref(m_state) : m_state));
  c_ObjectData::o_get(props);
}
bool c_parse_stack_frame::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x04180EFD963495B1LL, semantic, 8);
      break;
    case 2:
      HASH_EXISTS_STRING(0x5C84C80672BA5E22LL, state, 5);
      break;
    case 3:
      HASH_EXISTS_STRING(0x4A4181CA6D57B493LL, symbol, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parse_stack_frame::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x04180EFD963495B1LL, m_semantic,
                         semantic, 8);
      break;
    case 2:
      HASH_RETURN_STRING(0x5C84C80672BA5E22LL, m_state,
                         state, 5);
      break;
    case 3:
      HASH_RETURN_STRING(0x4A4181CA6D57B493LL, m_symbol,
                         symbol, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_parse_stack_frame::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x04180EFD963495B1LL, m_semantic,
                      semantic, 8);
      break;
    case 2:
      HASH_SET_STRING(0x5C84C80672BA5E22LL, m_state,
                      state, 5);
      break;
    case 3:
      HASH_SET_STRING(0x4A4181CA6D57B493LL, m_symbol,
                      symbol, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parse_stack_frame::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x04180EFD963495B1LL, m_semantic,
                         semantic, 8);
      break;
    case 2:
      HASH_RETURN_STRING(0x5C84C80672BA5E22LL, m_state,
                         state, 5);
      break;
    case 3:
      HASH_RETURN_STRING(0x4A4181CA6D57B493LL, m_symbol,
                         symbol, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parse_stack_frame::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parse_stack_frame)
ObjectData *c_parse_stack_frame::create(Variant v_symbol, Variant v_state) {
  init();
  t___construct(v_symbol, v_state);
  return this;
}
ObjectData *c_parse_stack_frame::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_parse_stack_frame::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_parse_stack_frame::cloneImpl() {
  c_parse_stack_frame *obj = NEW(c_parse_stack_frame)();
  cloneSet(obj);
  return obj;
}
void c_parse_stack_frame::cloneSet(c_parse_stack_frame *clone) {
  clone->m_symbol = m_symbol.isReferenced() ? ref(m_symbol) : m_symbol;
  clone->m_semantic = m_semantic.isReferenced() ? ref(m_semantic) : m_semantic;
  clone->m_state = m_state.isReferenced() ? ref(m_state) : m_state;
  ObjectData::cloneSet(clone);
}
Variant c_parse_stack_frame::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x472B86802591D6D3LL, semantic) {
        return (t_semantic());
      }
      HASH_GUARD(0x5121BF0D83B6B243LL, fold) {
        return (t_fold(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x2A865A9CE4562C25LL, shift) {
        return (t_shift(params.rvalAt(0)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x3CCB986B2CF0A747LL, trace) {
        return (t_trace());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parse_stack_frame::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x472B86802591D6D3LL, semantic) {
        return (t_semantic());
      }
      HASH_GUARD(0x5121BF0D83B6B243LL, fold) {
        return (t_fold(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x2A865A9CE4562C25LL, shift) {
        return (t_shift(a0), null);
      }
      break;
    case 7:
      HASH_GUARD(0x3CCB986B2CF0A747LL, trace) {
        return (t_trace());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parse_stack_frame::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parse_stack_frame$os_get(const char *s) {
  return c_parse_stack_frame::os_get(s, -1);
}
Variant &cw_parse_stack_frame$os_lval(const char *s) {
  return c_parse_stack_frame::os_lval(s, -1);
}
Variant cw_parse_stack_frame$os_constant(const char *s) {
  return c_parse_stack_frame::os_constant(s);
}
Variant cw_parse_stack_frame$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parse_stack_frame::os_invoke(c, s, params, -1, fatal);
}
void c_parse_stack_frame::init() {
  m_symbol = null;
  m_semantic = null;
  m_state = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 650 */
void c_parse_stack_frame::t___construct(Variant v_symbol, Variant v_state) {
  INSTANCE_METHOD_INJECTION(parse_stack_frame, parse_stack_frame::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_symbol = v_symbol);
  (m_state = v_state);
  (m_semantic = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 655 */
void c_parse_stack_frame::t_shift(CVarRef v_semantic) {
  INSTANCE_METHOD_INJECTION(parse_stack_frame, parse_stack_frame::shift);
  m_semantic.append((v_semantic));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 656 */
void c_parse_stack_frame::t_fold(CVarRef v_semantic) {
  INSTANCE_METHOD_INJECTION(parse_stack_frame, parse_stack_frame::fold);
  (m_semantic = Array(ArrayInit(1).set(0, v_semantic).create()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 657 */
Variant c_parse_stack_frame::t_semantic() {
  INSTANCE_METHOD_INJECTION(parse_stack_frame, parse_stack_frame::semantic);
  return m_semantic;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 658 */
String c_parse_stack_frame::t_trace() {
  INSTANCE_METHOD_INJECTION(parse_stack_frame, parse_stack_frame::trace);
  return LINE(658,concat3(toString(m_symbol), " : ", toString(m_state)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 660 */
Variant c_parse_error::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_parse_error::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_parse_error::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_parse_error::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_parse_error::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_parse_error::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_parse_error::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_parse_error::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(parse_error)
ObjectData *c_parse_error::cloneImpl() {
  c_parse_error *obj = NEW(c_parse_error)();
  cloneSet(obj);
  return obj;
}
void c_parse_error::cloneSet(c_parse_error *clone) {
  c_exception::cloneSet(clone);
}
Variant c_parse_error::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_parse_error::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parse_error::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parse_error$os_get(const char *s) {
  return c_parse_error::os_get(s, -1);
}
Variant &cw_parse_error$os_lval(const char *s) {
  return c_parse_error::os_lval(s, -1);
}
Variant cw_parse_error$os_constant(const char *s) {
  return c_parse_error::os_constant(s);
}
Variant cw_parse_error$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parse_error::os_invoke(c, s, params, -1, fatal);
}
void c_parse_error::init() {
  c_exception::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 297 */
Variant c_dfa::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_dfa::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_dfa::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_dfa::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_dfa::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_dfa::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_dfa::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_dfa::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(dfa)
ObjectData *c_dfa::create() {
  init();
  t_dfa();
  return this;
}
ObjectData *c_dfa::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_dfa::cloneImpl() {
  c_dfa *obj = NEW(c_dfa)();
  cloneSet(obj);
  return obj;
}
void c_dfa::cloneSet(c_dfa *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_dfa::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_dfa::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_dfa::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_dfa$os_get(const char *s) {
  return c_dfa::os_get(s, -1);
}
Variant &cw_dfa$os_lval(const char *s) {
  return c_dfa::os_lval(s, -1);
}
Variant cw_dfa$os_constant(const char *s) {
  return c_dfa::os_constant(s);
}
Variant cw_dfa$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_dfa::os_invoke(c, s, params, -1, fatal);
}
void c_dfa::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 303 */
void c_dfa::t_dfa() {
  INSTANCE_METHOD_INJECTION(dfa, dfa::dfa);
  bool oldInCtor = gasInCtor(true);
  (o_lval("states", 0x6FB528E40A4C1A15LL) = ScalarArrays::sa_[0]);
  (o_lval("initial", 0x4B073B3CA18015E7LL) = "");
  (o_lval("final", 0x5192930B2145036ELL) = ScalarArrays::sa_[0]);
  (o_lval("delta", 0x26E26E77DC67FF3FLL) = ScalarArrays::sa_[0]);
  (o_lval("mark", 0x19D2840C0AB0CFCFLL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 314 */
Variant c_dfa::t_add_state(CVarRef v_label) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::add_state);
  if (LINE(315,t_has_state(v_label))) {
    f_exit("Trying to add existing state to an DFA.");
  }
  lval(o_lval("states", 0x6FB528E40A4C1A15LL)).append((v_label));
  lval(o_lval("final", 0x5192930B2145036ELL)).set(v_label, (false));
  lval(o_lval("delta", 0x26E26E77DC67FF3FLL)).set(v_label, (ScalarArrays::sa_[0]));
  lval(o_lval("mark", 0x19D2840C0AB0CFCFLL)).set(v_label, (99999LL /* FA_NO_MARK */));
  return v_label;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 325 */
bool c_dfa::t_has_state(CVarRef v_label) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::has_state);
  return isset(o_get("delta", 0x26E26E77DC67FF3FLL), v_label);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 329 */
void c_dfa::t_add_transition(CVarRef v_src, CVarRef v_glyph, CVarRef v_dest) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::add_transition);
  lval(lval(o_lval("delta", 0x26E26E77DC67FF3FLL)).lvalAt(v_src)).set(v_glyph, (v_dest));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 333 */
Variant c_dfa::t_step(CVarRef v_label, CVarRef v_glyph) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::step);
  return (silenceInc(), silenceDec(o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_label).rvalAt(v_glyph)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 337 */
Variant c_dfa::t_accepting(CVarRef v_label) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::accepting);
  return LINE(338,x_array_keys(o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_label)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 341 */
p_dfa c_dfa::t_minimize() {
  INSTANCE_METHOD_INJECTION(dfa, dfa::minimize);
  Variant v_map;
  Variant v_dist;
  Variant v_p;
  Variant v_q;
  p_dfa v_dfa;
  Primitive v_glyph = 0;

  (v_map = LINE(348,t_indistinguishable_state_map(t_table_fill())));
  (v_dist = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_map.begin("dfa"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_q = iter3->second();
      v_p = iter3->first();
      v_dist.set(v_q, (v_q));
    }
  }
  ((Object)((v_dfa = ((Object)(LINE(352,p_dfa(p_dfa(NEWOBJ(c_dfa)())->create())))))));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_dist.begin("dfa"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_p = iter6->second();
      LINE(353,v_dfa->t_add_state(v_p));
    }
  }
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_dist.begin("dfa"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_p = iter9->second();
      {
        {
          LOOP_COUNTER(10);
          Variant map11 = o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_p);
          for (ArrayIterPtr iter12 = map11.begin("dfa"); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_q = iter12->second();
            v_glyph = iter12->first();
            LINE(355,v_dfa->t_add_transition(v_p, v_glyph, v_map.rvalAt(v_q)));
          }
        }
        lval(v_dfa.o_lval("final", 0x5192930B2145036ELL)).set(v_p, (o_get("final", 0x5192930B2145036ELL).rvalAt(v_p)));
        lval(v_dfa.o_lval("mark", 0x19D2840C0AB0CFCFLL)).set(v_p, (o_get("mark", 0x19D2840C0AB0CFCFLL).rvalAt(v_p)));
      }
    }
  }
  (v_dfa.o_lval("initial", 0x4B073B3CA18015E7LL) = v_map.rvalAt(o_get("initial", 0x4B073B3CA18015E7LL)));
  return ((Object)(v_dfa));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 363 */
Variant c_dfa::t_indistinguishable_state_map(CVarRef v_table) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::indistinguishable_state_map);
  Variant v_map;
  p_set v_set;
  Variant v_p;
  Variant v_q;

  (v_map = ScalarArrays::sa_[0]);
  ((Object)((v_set = ((Object)(LINE(367,p_set(p_set(NEWOBJ(c_set)())->create(o_get("states", 0x6FB528E40A4C1A15LL)))))))));
  LOOP_COUNTER(13);
  {
    while (toBoolean(LINE(368,v_set->t_count()))) {
      LOOP_COUNTER_CHECK(13);
      {
        (v_p = LINE(369,v_set->t_one()));
        {
          LOOP_COUNTER(14);
          Variant map15 = LINE(370,v_set->t_all());
          for (ArrayIterPtr iter16 = map15.begin("dfa"); !iter16->end(); iter16->next()) {
            LOOP_COUNTER_CHECK(14);
            v_q = iter16->second();
            if (!(toBoolean(toObject(v_table)->o_invoke_few_args("differ", 0x23352D86EF8CE769LL, 2, v_p, v_q)))) {
              v_map.set(v_q, (v_p));
              LINE(372,v_set->t_del(v_q));
            }
          }
        }
      }
    }
  }
  return v_map;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 377 */
Variant c_dfa::t_table_fill() {
  INSTANCE_METHOD_INJECTION(dfa, dfa::table_fill);
  Variant v_table;
  Variant v_s1;
  Variant v_s2;

  (v_table = ((Object)(LINE(385,p_distinguishing_table(p_distinguishing_table(NEWOBJ(c_distinguishing_table)())->create())))));
  {
    LOOP_COUNTER(17);
    Variant map18 = o_get("states", 0x6FB528E40A4C1A15LL);
    for (ArrayIterPtr iter19 = map18.begin("dfa"); !iter19->end(); iter19->next()) {
      LOOP_COUNTER_CHECK(17);
      v_s1 = iter19->second();
      {
        LOOP_COUNTER(20);
        Variant map21 = o_get("states", 0x6FB528E40A4C1A15LL);
        for (ArrayIterPtr iter22 = map21.begin("dfa"); !iter22->end(); iter22->next()) {
          LOOP_COUNTER_CHECK(20);
          v_s2 = iter22->second();
          {
            if (!equal(o_get("mark", 0x19D2840C0AB0CFCFLL).rvalAt(v_s1), o_get("mark", 0x19D2840C0AB0CFCFLL).rvalAt(v_s2))) LINE(388,v_table.o_invoke_few_args("distinguish", 0x51E144C836A68A51LL, 2, v_s1, v_s2));
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(23);
    do {
      LOOP_COUNTER_CHECK(23);
      {
      }
    } while (!(LINE(392,t_filling_round(ref(v_table)))));
  }
  return v_table;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 397 */
bool c_dfa::t_filling_round(Variant v_table) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::filling_round);
  bool v_done = false;
  Variant v_s1;
  Variant v_s2;
  bool v_different = false;

  (v_done = true);
  {
    LOOP_COUNTER(24);
    Variant map25 = o_get("states", 0x6FB528E40A4C1A15LL);
    for (ArrayIterPtr iter26 = map25.begin("dfa"); !iter26->end(); iter26->next()) {
      LOOP_COUNTER_CHECK(24);
      v_s1 = iter26->second();
      {
        LOOP_COUNTER(27);
        Variant map28 = o_get("states", 0x6FB528E40A4C1A15LL);
        for (ArrayIterPtr iter29 = map28.begin("dfa"); !iter29->end(); iter29->next()) {
          LOOP_COUNTER_CHECK(27);
          v_s2 = iter29->second();
          {
            if (equal(v_s1, v_s2)) continue;
            if (!(toBoolean(LINE(402,toObject(v_table)->o_invoke_few_args("differ", 0x23352D86EF8CE769LL, 2, v_s1, v_s2))))) {
              (v_different = LINE(408,t_compare_states(v_s1, v_s2, v_table)));
              if (v_different) {
                LINE(410,toObject(v_table)->o_invoke_few_args("distinguish", 0x51E144C836A68A51LL, 2, v_s1, v_s2));
                (v_done = false);
                break;
              }
            }
          }
        }
      }
    }
  }
  return v_done;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 420 */
bool c_dfa::t_compare_states(CVarRef v_p, CVarRef v_q, CVarRef v_table) {
  INSTANCE_METHOD_INJECTION(dfa, dfa::compare_states);
  Variant eo_0;
  Variant eo_1;
  Variant v_sigma;
  Variant v_glyph;
  Variant v_p1;
  Variant v_q1;

  (v_sigma = LINE(421,x_array_unique((assignCallTemp(eo_0, t_accepting(v_p)),assignCallTemp(eo_1, t_accepting(v_q)),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))))));
  if (equal(v_p, v_q)) {
    return false;
  }
  {
    LOOP_COUNTER(30);
    for (ArrayIterPtr iter32 = v_sigma.begin("dfa"); !iter32->end(); iter32->next()) {
      LOOP_COUNTER_CHECK(30);
      v_glyph = iter32->second();
      {
        (v_p1 = LINE(429,t_step(v_p, v_glyph)));
        (v_q1 = LINE(430,t_step(v_q, v_glyph)));
        if ((!(((toBoolean(v_p1)) && (toBoolean(v_q1))))) || (toBoolean(LINE(431,toObject(v_table)->o_invoke_few_args("differ", 0x23352D86EF8CE769LL, 2, v_p1, v_q1))))) {
          return true;
        }
      }
    }
  }
  return false;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 21 */
Variant c_set::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_set::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_set::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_set::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_set::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_set::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_set::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_set::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(set)
ObjectData *c_set::create(CVarRef v_list //  = ScalarArrays::sa_[0]
) {
  init();
  t_set(v_list);
  return this;
}
ObjectData *c_set::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_set::cloneImpl() {
  c_set *obj = NEW(c_set)();
  cloneSet(obj);
  return obj;
}
void c_set::cloneSet(c_set *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_set::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_set::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_set::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_set$os_get(const char *s) {
  return c_set::os_get(s, -1);
}
Variant &cw_set$os_lval(const char *s) {
  return c_set::os_lval(s, -1);
}
Variant cw_set$os_constant(const char *s) {
  return c_set::os_constant(s);
}
Variant cw_set$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_set::os_invoke(c, s, params, -1, fatal);
}
void c_set::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 22 */
void c_set::t_set(CVarRef v_list //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(set, set::set);
  bool oldInCtor = gasInCtor(true);
  (o_lval("data", 0x30164401A9853128LL) = LINE(22,x_array_count_values(v_list)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 23 */
bool c_set::t_has(CVarRef v_item) {
  INSTANCE_METHOD_INJECTION(set, set::has);
  return isset(o_get("data", 0x30164401A9853128LL), v_item);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 24 */
void c_set::t_add(CVarRef v_item) {
  INSTANCE_METHOD_INJECTION(set, set::add);
  lval(o_lval("data", 0x30164401A9853128LL)).set(v_item, (true));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 25 */
void c_set::t_del(CVarRef v_item) {
  INSTANCE_METHOD_INJECTION(set, set::del);
  DECLARE_GLOBAL_VARIABLES(g);
  lval(o_lval("data", 0x30164401A9853128LL)).weakRemove(v_item);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 26 */
Variant c_set::t_all() {
  INSTANCE_METHOD_INJECTION(set, set::all);
  return LINE(26,x_array_keys(o_get("data", 0x30164401A9853128LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 27 */
Variant c_set::t_one() {
  INSTANCE_METHOD_INJECTION(set, set::one);
  return LINE(27,x_key(ref(lval(o_lval("data", 0x30164401A9853128LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 28 */
int c_set::t_count() {
  INSTANCE_METHOD_INJECTION(set, set::count);
  return LINE(28,x_count(o_get("data", 0x30164401A9853128LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 732 */
Variant c_easy_parser::os_get(const char *s, int64 hash) {
  return c_parser::os_get(s, hash);
}
Variant &c_easy_parser::os_lval(const char *s, int64 hash) {
  return c_parser::os_lval(s, hash);
}
void c_easy_parser::o_get(ArrayElementVec &props) const {
  c_parser::o_get(props);
}
bool c_easy_parser::o_exists(CStrRef s, int64 hash) const {
  return c_parser::o_exists(s, hash);
}
Variant c_easy_parser::o_get(CStrRef s, int64 hash) {
  return c_parser::o_get(s, hash);
}
Variant c_easy_parser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_parser::o_set(s, hash, v, forInit);
}
Variant &c_easy_parser::o_lval(CStrRef s, int64 hash) {
  return c_parser::o_lval(s, hash);
}
Variant c_easy_parser::os_constant(const char *s) {
  return c_parser::os_constant(s);
}
IMPLEMENT_CLASS(easy_parser)
ObjectData *c_easy_parser::create(Variant v_pda, Variant v_strategy //  = null
) {
  init();
  t___construct(v_pda, v_strategy);
  return this;
}
ObjectData *c_easy_parser::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_easy_parser::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_easy_parser::cloneImpl() {
  c_easy_parser *obj = NEW(c_easy_parser)();
  cloneSet(obj);
  return obj;
}
void c_easy_parser::cloneSet(c_easy_parser *clone) {
  c_parser::cloneSet(clone);
}
Variant c_easy_parser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x1B45B66F5DF8EE78LL, reduce) {
        return (t_reduce(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 6:
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        int count = params.size();
        if (count <= 2) return (t_parse(params.rvalAt(0), params.rvalAt(1)));
        return (t_parse(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_parser::o_invoke(s, params, hash, fatal);
}
Variant c_easy_parser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x1B45B66F5DF8EE78LL, reduce) {
        return (t_reduce(a0, a1));
      }
      break;
    case 6:
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        if (count <= 2) return (t_parse(a0, a1));
        return (t_parse(a0, a1, a2));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_parser::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_easy_parser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parser::os_invoke(c, s, params, hash, fatal);
}
Variant cw_easy_parser$os_get(const char *s) {
  return c_easy_parser::os_get(s, -1);
}
Variant &cw_easy_parser$os_lval(const char *s) {
  return c_easy_parser::os_lval(s, -1);
}
Variant cw_easy_parser$os_constant(const char *s) {
  return c_easy_parser::os_constant(s);
}
Variant cw_easy_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_easy_parser::os_invoke(c, s, params, -1, fatal);
}
void c_easy_parser::init() {
  c_parser::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 733 */
void c_easy_parser::t___construct(Variant v_pda, Variant v_strategy //  = null
) {
  INSTANCE_METHOD_INJECTION(easy_parser, easy_parser::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(734,c_parser::t___construct(v_pda));
  (o_lval("call", 0x1C670E9E50149F8CLL) = o_get("action", 0x4CCA7F022EF68151LL));
  (o_lval("strategy", 0x14CBB5B98CF4E610LL) = (toBoolean(v_strategy) ? ((Variant)(v_strategy)) : ((Variant)(((Object)(LINE(736,p_default_parser_strategy(p_default_parser_strategy(NEWOBJ(c_default_parser_strategy)())->create()))))))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 743 */
Variant c_easy_parser::t_reduce(CVarRef v_action, Variant v_tokens) {
  INSTANCE_METHOD_INJECTION(easy_parser, easy_parser::reduce);
  return LINE(744,invoke((toString(o_get("call", 0x1C670E9E50149F8CLL).rvalAt(v_action))), Array(ArrayInit(1).set(0, ref(v_tokens)).create()), -1));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 746 */
Variant c_easy_parser::t_parse(CVarRef v_symbol, CVarRef v_lex, CVarRef v_strategy //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(easy_parser, easy_parser::parse);
  return LINE(747,c_parser::t_parse(v_symbol, v_lex, o_get("strategy", 0x14CBB5B98CF4E610LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 577 */
Variant c_token_source::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_token_source::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_token_source::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_token_source::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_token_source::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_token_source::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_token_source::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_token_source::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(token_source)
ObjectData *c_token_source::cloneImpl() {
  c_token_source *obj = NEW(c_token_source)();
  cloneSet(obj);
  return obj;
}
void c_token_source::cloneSet(c_token_source *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_token_source::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D0A00D8D1C24295LL, report_error) {
        return (t_report_error(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_token_source::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5D0A00D8D1C24295LL, report_error) {
        return (t_report_error(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_token_source::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_token_source$os_get(const char *s) {
  return c_token_source::os_get(s, -1);
}
Variant &cw_token_source$os_lval(const char *s) {
  return c_token_source::os_lval(s, -1);
}
Variant cw_token_source$os_constant(const char *s) {
  return c_token_source::os_constant(s);
}
Variant cw_token_source$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_token_source::os_invoke(c, s, params, -1, fatal);
}
void c_token_source::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 578 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 579 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 580 */
void c_token_source::t_report_error() {
  INSTANCE_METHOD_INJECTION(token_source, token_source::report_error);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int64 v_i = 0;
  Variant v_tok;

  LINE(581,o_root_invoke_few_args("report_instant_description", 0x3255ACB6EB444E3DLL, 0));
  echo("The next few tokens are:<br/>\n");
  {
    LOOP_COUNTER(33);
    for ((v_i = 0LL); less(v_i, 15LL); v_i++) {
      LOOP_COUNTER_CHECK(33);
      {
        (v_tok = LINE(584,o_root_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0)));
        LINE(585,(assignCallTemp(eo_1, x_htmlspecialchars(toString(v_tok.o_get("text", 0x2A28A0084DD3A743LL)))),assignCallTemp(eo_2, v_tok.o_get("type", 0x508FC7C8724A760ALL)),f_span("term", eo_1, eo_2)));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 5 */
Variant c_bug::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_bug::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_bug::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_bug::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_bug::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_bug::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_bug::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_bug::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(bug)
ObjectData *c_bug::cloneImpl() {
  c_bug *obj = NEW(c_bug)();
  cloneSet(obj);
  return obj;
}
void c_bug::cloneSet(c_bug *clone) {
  c_exception::cloneSet(clone);
}
Variant c_bug::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_bug::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bug::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bug$os_get(const char *s) {
  return c_bug::os_get(s, -1);
}
Variant &cw_bug$os_lval(const char *s) {
  return c_bug::os_lval(s, -1);
}
Variant cw_bug$os_constant(const char *s) {
  return c_bug::os_constant(s);
}
Variant cw_bug$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bug::os_invoke(c, s, params, -1, fatal);
}
void c_bug::init() {
  c_exception::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 751 */
Variant c_parser_strategy::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parser_strategy::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parser_strategy::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_parser_strategy::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parser_strategy::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_parser_strategy::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parser_strategy::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parser_strategy::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parser_strategy)
ObjectData *c_parser_strategy::cloneImpl() {
  c_parser_strategy *obj = NEW(c_parser_strategy)();
  cloneSet(obj);
  return obj;
}
void c_parser_strategy::cloneSet(c_parser_strategy *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_parser_strategy::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parser_strategy::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parser_strategy::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parser_strategy$os_get(const char *s) {
  return c_parser_strategy::os_get(s, -1);
}
Variant &cw_parser_strategy$os_lval(const char *s) {
  return c_parser_strategy::os_lval(s, -1);
}
Variant cw_parser_strategy$os_constant(const char *s) {
  return c_parser_strategy::os_constant(s);
}
Variant cw_parser_strategy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parser_strategy::os_invoke(c, s, params, -1, fatal);
}
void c_parser_strategy::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 752 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 753 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 443 */
Variant c_distinguishing_table::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_distinguishing_table::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_distinguishing_table::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_distinguishing_table::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_distinguishing_table::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_distinguishing_table::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_distinguishing_table::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_distinguishing_table::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(distinguishing_table)
ObjectData *c_distinguishing_table::create() {
  init();
  t_distinguishing_table();
  return this;
}
ObjectData *c_distinguishing_table::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_distinguishing_table::cloneImpl() {
  c_distinguishing_table *obj = NEW(c_distinguishing_table)();
  cloneSet(obj);
  return obj;
}
void c_distinguishing_table::cloneSet(c_distinguishing_table *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_distinguishing_table::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x51E144C836A68A51LL, distinguish) {
        return (t_distinguish(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x23352D86EF8CE769LL, differ) {
        return (t_differ(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_distinguishing_table::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x51E144C836A68A51LL, distinguish) {
        return (t_distinguish(a0, a1), null);
      }
      HASH_GUARD(0x23352D86EF8CE769LL, differ) {
        return (t_differ(a0, a1));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_distinguishing_table::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_distinguishing_table$os_get(const char *s) {
  return c_distinguishing_table::os_get(s, -1);
}
Variant &cw_distinguishing_table$os_lval(const char *s) {
  return c_distinguishing_table::os_lval(s, -1);
}
Variant cw_distinguishing_table$os_constant(const char *s) {
  return c_distinguishing_table::os_constant(s);
}
Variant cw_distinguishing_table$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_distinguishing_table::os_invoke(c, s, params, -1, fatal);
}
void c_distinguishing_table::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 444 */
void c_distinguishing_table::t_distinguishing_table() {
  INSTANCE_METHOD_INJECTION(distinguishing_table, distinguishing_table::distinguishing_table);
  bool oldInCtor = gasInCtor(true);
  (o_lval("dist", 0x494C6F45FA605818LL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 447 */
String c_distinguishing_table::t_key(CVarRef v_s1, CVarRef v_s2) {
  INSTANCE_METHOD_INJECTION(distinguishing_table, distinguishing_table::key);
  Variant v_them;

  (v_them = Array(ArrayInit(2).set(0, v_s1).set(1, v_s2).create()));
  LINE(449,x_sort(ref(v_them)));
  return LINE(450,x_implode("|", v_them));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 452 */
void c_distinguishing_table::t_distinguish(CVarRef v_s1, CVarRef v_s2) {
  INSTANCE_METHOD_INJECTION(distinguishing_table, distinguishing_table::distinguish);
  String v_key;

  (v_key = LINE(453,t_key(v_s1, v_s2)));
  lval(o_lval("dist", 0x494C6F45FA605818LL)).set(v_key, (true));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 456 */
bool c_distinguishing_table::t_differ(CVarRef v_s1, CVarRef v_s2) {
  INSTANCE_METHOD_INJECTION(distinguishing_table, distinguishing_table::differ);
  String v_key;

  (v_key = LINE(457,t_key(v_s1, v_s2)));
  return isset(o_get("dist", 0x494C6F45FA605818LL), v_key);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 661 */
Variant c_parser::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parser::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parser::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_parser::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parser::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_parser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parser::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parser::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parser)
ObjectData *c_parser::create(Variant v_pda) {
  init();
  t___construct(v_pda);
  return this;
}
ObjectData *c_parser::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_parser::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_parser::cloneImpl() {
  c_parser *obj = NEW(c_parser)();
  cloneSet(obj);
  return obj;
}
void c_parser::cloneSet(c_parser *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_parser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        int count = params.size();
        if (count <= 2) return (t_parse(params.rvalAt(0), params.rvalAt(1)));
        return (t_parse(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        if (count <= 2) return (t_parse(a0, a1));
        return (t_parse(a0, a1, a2));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parser$os_get(const char *s) {
  return c_parser::os_get(s, -1);
}
Variant &cw_parser$os_lval(const char *s) {
  return c_parser::os_lval(s, -1);
}
Variant cw_parser$os_constant(const char *s) {
  return c_parser::os_constant(s);
}
Variant cw_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parser::os_invoke(c, s, params, -1, fatal);
}
void c_parser::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 662 */
void c_parser::t___construct(Variant v_pda) {
  INSTANCE_METHOD_INJECTION(parser, parser::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("pda", 0x5FFC78D94BC56CF5LL) = v_pda);
  (o_lval("action", 0x4CCA7F022EF68151LL) = v_pda.rvalAt("action", 0x4CCA7F022EF68151LL));
  (o_lval("start", 0x00B621CE3C3982D5LL) = v_pda.rvalAt("start", 0x00B621CE3C3982D5LL));
  (o_lval("delta", 0x26E26E77DC67FF3FLL) = v_pda.rvalAt("delta", 0x26E26E77DC67FF3FLL));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 668 */
void c_parser::t_report() {
  INSTANCE_METHOD_INJECTION(parser, parser::report);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Primitive v_label = 0;
  Variant v_d;
  Primitive v_glyph = 0;
  Variant v_step;

  LINE(670,invoke_failed("pr", Array(ArrayInit(1).set(0, ref(o_lval("start", 0x00B621CE3C3982D5LL))).create()), 0x000000002703938CLL));
  {
    LOOP_COUNTER(34);
    Variant map35 = o_get("delta", 0x26E26E77DC67FF3FLL);
    for (ArrayIterPtr iter36 = map35.begin("parser"); !iter36->end(); iter36->next()) {
      LOOP_COUNTER_CHECK(34);
      v_d = iter36->second();
      v_label = iter36->first();
      {
        echo(LINE(672,concat3("<h3>State ", toString(v_label), "</h3>")));
        {
          LOOP_COUNTER(37);
          for (ArrayIterPtr iter39 = v_d.begin("parser"); !iter39->end(); iter39->next()) {
            LOOP_COUNTER_CHECK(37);
            v_step = iter39->second();
            v_glyph = iter39->first();
            echo(LINE(673,(assignCallTemp(eo_0, toString(v_glyph)),assignCallTemp(eo_2, x_implode(":", v_step)),concat4(eo_0, " -&gt; ", eo_2, "<br>"))));
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 676 */
Variant c_parser::t_get_step(CVarRef v_label, CVarRef v_glyph) {
  INSTANCE_METHOD_INJECTION(parser, parser::get_step);
  Variant v_d;

  (v_d = o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_label));
  if (isset(v_d, v_glyph)) return v_d.rvalAt(v_glyph);
  if (isset(v_d, "[default]", 0x0DB9859697C7DD04LL)) return v_d.rvalAt("[default]", 0x0DB9859697C7DD04LL);
  return ScalarArrays::sa_[3];
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 682 */
Variant c_parser::t_parse(CVarRef v_symbol, Variant v_lex, CVarRef v_strategy //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(parser, parser::parse);
  Variant eo_0;
  Variant eo_1;
  Variant v_stack;
  Variant v_tos;
  Variant v_token;
  Variant v_step;
  Variant v_semantic;

  (v_stack = ScalarArrays::sa_[0]);
  (v_tos = ((Object)(LINE(684,t_frame(v_symbol)))));
  (v_token = LINE(685,v_lex.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0)));
  LOOP_COUNTER(40);
  {
    while (true) {
      LOOP_COUNTER_CHECK(40);
      {
        (v_step = LINE(687,t_get_step(v_tos.o_get("state", 0x5C84C80672BA5E22LL), v_token.o_get("type", 0x508FC7C8724A760ALL))));
        {
          Variant tmp42 = (v_step.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          int tmp43 = -1;
          if (equal(tmp42, ("go"))) {
            tmp43 = 0;
          } else if (equal(tmp42, ("do"))) {
            tmp43 = 1;
          } else if (equal(tmp42, ("push"))) {
            tmp43 = 2;
          } else if (equal(tmp42, ("fold"))) {
            tmp43 = 3;
          } else if (equal(tmp42, ("error"))) {
            tmp43 = 4;
          } else if (true) {
            tmp43 = 5;
          }
          switch (tmp43) {
          case 0:
            {
              LINE(691,v_tos.o_invoke_few_args("shift", 0x2A865A9CE4562C25LL, 1, v_token.o_lval("text", 0x2A28A0084DD3A743LL)));
              (v_tos.o_lval("state", 0x5C84C80672BA5E22LL) = v_step.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
              (v_token = LINE(693,v_lex.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0)));
              goto break41;
            }
          case 1:
            {
              (v_semantic = (assignCallTemp(eo_0, ref(v_step.refvalAt(1LL, 0x5BCA7C69B794F8CELL))),assignCallTemp(eo_1, ref(LINE(697,v_tos.o_invoke_few_args("semantic", 0x472B86802591D6D3LL, 0)))),o_root_invoke("reduce", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x1B45B66F5DF8EE78LL)));
              if (empty(v_stack)) {
                LINE(699,toObject(v_strategy)->o_invoke_few_args("assert_done", 0x23ECC1F85FE7997FLL, 2, v_token, v_lex));
                return v_semantic;
              }
              else {
                (v_tos = LINE(702,x_array_pop(ref(v_stack))));
                LINE(703,v_tos.o_invoke_few_args("shift", 0x2A865A9CE4562C25LL, 1, v_semantic));
              }
              goto break41;
            }
          case 2:
            {
              (v_tos.o_lval("state", 0x5C84C80672BA5E22LL) = v_step.rvalAt(2LL, 0x486AFCC090D5F98CLL));
              v_stack.append((v_tos));
              (v_tos = ((Object)(LINE(710,t_frame(v_step.rvalAt(1LL, 0x5BCA7C69B794F8CELL))))));
              goto break41;
            }
          case 3:
            {
              LINE(714,v_tos.o_invoke_few_args("fold", 0x5121BF0D83B6B243LL, 1, (assignCallTemp(eo_0, ref(v_step.refvalAt(1LL, 0x5BCA7C69B794F8CELL))),assignCallTemp(eo_1, ref(v_tos.o_invoke_few_args("semantic", 0x472B86802591D6D3LL, 0))),o_root_invoke("reduce", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x1B45B66F5DF8EE78LL))));
              (v_tos.o_lval("state", 0x5C84C80672BA5E22LL) = v_step.rvalAt(2LL, 0x486AFCC090D5F98CLL));
              goto break41;
            }
          case 4:
            {
              v_stack.append((v_tos));
              LINE(720,toObject(v_strategy)->o_invoke_few_args("stuck", 0x4DF50FCFC02BB0E2LL, 3, v_token, v_lex, v_stack));
              goto break41;
            }
          case 5:
            {
              throw_exception(((Object)(LINE(724,p_parse_error(p_parse_error(NEWOBJ(c_parse_error)())->create(concat3("Impossible. Bad PDA has ", toString(v_step.rvalAt(0, 0x77CFA1EEF01BCA90LL)), " instruction.")))))));
            }
          }
          break41:;
        }
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 728 */
p_parse_stack_frame c_parser::t_frame(CVarRef v_symbol) {
  INSTANCE_METHOD_INJECTION(parser, parser::frame);
  return ((Object)(LINE(728,p_parse_stack_frame(p_parse_stack_frame(NEWOBJ(c_parse_stack_frame)())->create(v_symbol, o_get("start", 0x00B621CE3C3982D5LL).rvalAt(v_symbol))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 729 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 510 */
Variant c_token::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_token::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_token::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_token::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_token::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_token::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_token::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_token::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(token)
ObjectData *c_token::create(Variant v_type, Variant v_text, Variant v_start, Variant v_stop) {
  init();
  t___construct(v_type, v_text, v_start, v_stop);
  return this;
}
ObjectData *c_token::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
void c_token::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
}
ObjectData *c_token::cloneImpl() {
  c_token *obj = NEW(c_token)();
  cloneSet(obj);
  return obj;
}
void c_token::cloneSet(c_token *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_token::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_token::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2, a3), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_token::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_token$os_get(const char *s) {
  return c_token::os_get(s, -1);
}
Variant &cw_token$os_lval(const char *s) {
  return c_token::os_lval(s, -1);
}
Variant cw_token$os_constant(const char *s) {
  return c_token::os_constant(s);
}
Variant cw_token$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_token::os_invoke(c, s, params, -1, fatal);
}
void c_token::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 511 */
void c_token::t___construct(Variant v_type, Variant v_text, Variant v_start, Variant v_stop) {
  INSTANCE_METHOD_INJECTION(token, token::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("type", 0x508FC7C8724A760ALL) = v_type);
  (o_lval("text", 0x2A28A0084DD3A743LL) = v_text);
  (o_lval("start", 0x00B621CE3C3982D5LL) = v_start);
  (o_lval("stop", 0x3B3356465699254CLL) = v_stop);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 590 */
Variant c_preg_scanner::os_get(const char *s, int64 hash) {
  return c_token_source::os_get(s, hash);
}
Variant &c_preg_scanner::os_lval(const char *s, int64 hash) {
  return c_token_source::os_lval(s, hash);
}
void c_preg_scanner::o_get(ArrayElementVec &props) const {
  c_token_source::o_get(props);
}
bool c_preg_scanner::o_exists(CStrRef s, int64 hash) const {
  return c_token_source::o_exists(s, hash);
}
Variant c_preg_scanner::o_get(CStrRef s, int64 hash) {
  return c_token_source::o_get(s, hash);
}
Variant c_preg_scanner::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_token_source::o_set(s, hash, v, forInit);
}
Variant &c_preg_scanner::o_lval(CStrRef s, int64 hash) {
  return c_token_source::o_lval(s, hash);
}
Variant c_preg_scanner::os_constant(const char *s) {
  return c_token_source::os_constant(s);
}
IMPLEMENT_CLASS(preg_scanner)
ObjectData *c_preg_scanner::create(int num_args, Variant v_init_context, Variant v_p //  = null
, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_init_context, v_p,args);
  return this;
}
ObjectData *c_preg_scanner::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(count, params.rvalAt(0)));
    if (count == 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_preg_scanner::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(count, params.rvalAt(0)));
  if (count == 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_preg_scanner::cloneImpl() {
  c_preg_scanner *obj = NEW(c_preg_scanner)();
  cloneSet(obj);
  return obj;
}
void c_preg_scanner::cloneSet(c_preg_scanner *clone) {
  c_token_source::cloneSet(clone);
}
Variant c_preg_scanner::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x5D0A00D8D1C24295LL, report_error) {
        return (t_report_error(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 13:
      HASH_GUARD(0x3255ACB6EB444E3DLL, report_instant_description) {
        return (t_report_instant_description(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(count, params.rvalAt(0)), null);
        if (count == 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      break;
    default:
      break;
  }
  return c_token_source::o_invoke(s, params, hash, fatal);
}
Variant c_preg_scanner::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x5D0A00D8D1C24295LL, report_error) {
        return (t_report_error(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 13:
      HASH_GUARD(0x3255ACB6EB444E3DLL, report_instant_description) {
        return (t_report_instant_description(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(count, a0), null);
        if (count == 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      break;
    default:
      break;
  }
  return c_token_source::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_preg_scanner::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_token_source::os_invoke(c, s, params, hash, fatal);
}
Variant cw_preg_scanner$os_get(const char *s) {
  return c_preg_scanner::os_get(s, -1);
}
Variant &cw_preg_scanner$os_lval(const char *s) {
  return c_preg_scanner::os_lval(s, -1);
}
Variant cw_preg_scanner$os_constant(const char *s) {
  return c_preg_scanner::os_constant(s);
}
Variant cw_preg_scanner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_preg_scanner::os_invoke(c, s, params, -1, fatal);
}
void c_preg_scanner::init() {
  c_token_source::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 591 */
void c_preg_scanner::t_report_instant_description() {
  INSTANCE_METHOD_INJECTION(preg_scanner, preg_scanner::report_instant_description);
  echo(LINE(592,concat3("Scanner State: ", toString(o_get("state", 0x5C84C80672BA5E22LL)), "<br/>\n")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 594 */
void c_preg_scanner::t___construct(int num_args, Variant v_init_context, Variant v_p //  = null
, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(preg_scanner, preg_scanner::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(595,f_bug_unless(num_args));
  (o_lval("pattern", 0x029C0A5F2FB04920LL) = toBoolean(v_p) ? ((Variant)(v_p)) : ((Variant)(ScalarArrays::sa_[2])));
  (o_lval("state", 0x5C84C80672BA5E22LL) = "INITIAL");
  (o_lval("init_context", 0x709F28D32598EA64LL) = v_init_context);
  (o_lval("context", 0x232B332B81CF7F0FLL) = v_init_context);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 601 */
void c_preg_scanner::t_add_state(CVarRef v_name, CVarRef v_cluster) {
  INSTANCE_METHOD_INJECTION(preg_scanner, preg_scanner::add_state);
  LINE(602,f_bug_unless(x_is_array(v_cluster)));
  lval(o_lval("pattern", 0x029C0A5F2FB04920LL)).set(v_name, (v_cluster));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 605 */
void c_preg_scanner::t_start(CVarRef v_string) {
  INSTANCE_METHOD_INJECTION(preg_scanner, preg_scanner::start);
  Primitive v_key = 0;
  Variant v_blah;
  String v_s;
  Variant v_pattern;

  (o_lval("context", 0x232B332B81CF7F0FLL) = o_get("init_context", 0x709F28D32598EA64LL));
  (o_lval("stream", 0x484439B66146CFC6LL) = ((Object)(LINE(607,p_stream(p_stream(NEWOBJ(c_stream)())->create(v_string))))));
  (o_lval("megaregexp", 0x39015AA1A1A505CCLL) = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(44);
    Variant map45 = o_get("pattern", 0x029C0A5F2FB04920LL);
    for (ArrayIterPtr iter46 = map45.begin("preg_scanner"); !iter46->end(); iter46->next()) {
      LOOP_COUNTER_CHECK(44);
      v_blah = iter46->second();
      v_key = iter46->first();
      {
        (v_s = "");
        {
          LOOP_COUNTER(47);
          Variant map48 = o_get("pattern", 0x029C0A5F2FB04920LL).rvalAt(v_key);
          for (ArrayIterPtr iter49 = map48.begin("preg_scanner"); !iter49->end(); iter49->next()) {
            LOOP_COUNTER_CHECK(47);
            v_pattern = iter49->second();
            {
              if (toBoolean(v_s)) concat_assign(v_s, "|");
              concat_assign(v_s, toString(v_pattern.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
            }
          }
        }
        (v_s = LINE(615,concat3("(", v_s, ")")));
        lval(o_lval("megaregexp", 0x39015AA1A1A505CCLL)).set(v_key, (v_s));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 619 */
Variant c_preg_scanner::t_next() {
  INSTANCE_METHOD_INJECTION(preg_scanner, preg_scanner::next);
  Variant eo_0;
  Variant eo_1;
  Variant v_start;
  Variant v_match;
  Variant v_text;
  Variant v_tmp;
  Numeric v_index = 0;
  Variant v_pattern;
  Variant v_type;
  Variant v_action;
  Variant v_stop;

  (v_start = LINE(620,o_get("stream", 0x484439B66146CFC6LL).o_invoke_few_args("pos", 0x60C4B9EEDBD5FA66LL, 0)));
  LINE(621,(assignCallTemp(eo_0, x_is_array(o_get("pattern", 0x029C0A5F2FB04920LL).rvalAt(o_get("state", 0x5C84C80672BA5E22LL)))),assignCallTemp(eo_1, concat("No state called ", toString(o_get("state", 0x5C84C80672BA5E22LL)))),f_bug_unless(eo_0, eo_1)));
  if (toBoolean((v_match = LINE(623,o_get("stream", 0x484439B66146CFC6LL).o_invoke_few_args("test", 0x37349B25A0ED29E7LL, 1, lval(o_lval("megaregexp", 0x39015AA1A1A505CCLL)).refvalAt(o_get("state", 0x5C84C80672BA5E22LL))))))) {
    (v_text = v_match.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
    (v_tmp = LINE(625,x_array_flip(v_match)));
    (v_index = v_tmp.rvalAt(v_text) - 1LL);
    (v_pattern = o_get("pattern", 0x029C0A5F2FB04920LL).rvalAt(o_get("state", 0x5C84C80672BA5E22LL)).rvalAt(v_index));
    (v_type = v_pattern.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
    (v_action = v_pattern.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL));
    if (toBoolean(v_action)) LINE(630,invoke(toString(v_action), Array(ArrayInit(5).set(0, ref(v_type)).set(1, ref(v_text)).set(2, ref(v_match)).set(3, ref(o_lval("state", 0x5C84C80672BA5E22LL))).set(4, ref(o_lval("context", 0x232B332B81CF7F0FLL))).create()), -1));
    if (toBoolean(v_pattern.rvalAt(2LL, 0x486AFCC090D5F98CLL))) return LINE(631,o_root_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0));
    (v_stop = LINE(632,o_get("stream", 0x484439B66146CFC6LL).o_invoke_few_args("pos", 0x60C4B9EEDBD5FA66LL, 0)));
    return ((Object)(LINE(633,p_token(p_token(NEWOBJ(c_token)())->create(v_type, v_text, v_start, v_stop)))));
  }
  return LINE(635,o_get("stream", 0x484439B66146CFC6LL).o_invoke_few_args("default_rule", 0x6192F7E5944AFEDELL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 570 */
Variant c_point::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_point::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_point::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_point::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_point::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_point::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_point::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_point::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(point)
ObjectData *c_point::create(Variant v_line, Variant v_col) {
  init();
  t___construct(v_line, v_col);
  return this;
}
ObjectData *c_point::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_point::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_point::cloneImpl() {
  c_point *obj = NEW(c_point)();
  cloneSet(obj);
  return obj;
}
void c_point::cloneSet(c_point *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_point::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_point::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_point::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_point$os_get(const char *s) {
  return c_point::os_get(s, -1);
}
Variant &cw_point$os_lval(const char *s) {
  return c_point::os_lval(s, -1);
}
Variant cw_point$os_constant(const char *s) {
  return c_point::os_constant(s);
}
Variant cw_point$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_point::os_invoke(c, s, params, -1, fatal);
}
void c_point::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 571 */
void c_point::t___construct(Variant v_line, Variant v_col) {
  INSTANCE_METHOD_INJECTION(point, point::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("line", 0x21093C71DDF8728CLL) = v_line);
  (o_lval("col", 0x09F865A7AB163A7BLL) = v_col);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 540 */
Variant c_stream::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_stream::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_stream::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_stream::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_stream::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_stream::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_stream::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_stream::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(stream)
ObjectData *c_stream::create(Variant v_string) {
  init();
  t___construct(v_string);
  return this;
}
ObjectData *c_stream::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_stream::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_stream::cloneImpl() {
  c_stream *obj = NEW(c_stream)();
  cloneSet(obj);
  return obj;
}
void c_stream::cloneSet(c_stream *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_stream::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 6:
      HASH_GUARD(0x6192F7E5944AFEDELL, default_rule) {
        return (t_default_rule());
      }
      HASH_GUARD(0x60C4B9EEDBD5FA66LL, pos) {
        return (t_pos());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_stream::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 6:
      HASH_GUARD(0x6192F7E5944AFEDELL, default_rule) {
        return (t_default_rule());
      }
      HASH_GUARD(0x60C4B9EEDBD5FA66LL, pos) {
        return (t_pos());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (t_test(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_stream::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_stream$os_get(const char *s) {
  return c_stream::os_get(s, -1);
}
Variant &cw_stream$os_lval(const char *s) {
  return c_stream::os_lval(s, -1);
}
Variant cw_stream$os_constant(const char *s) {
  return c_stream::os_constant(s);
}
Variant cw_stream$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_stream::os_invoke(c, s, params, -1, fatal);
}
void c_stream::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 541 */
void c_stream::t___construct(Variant v_string) {
  INSTANCE_METHOD_INJECTION(stream, stream::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("string", 0x2027864469AD4382LL) = v_string);
  (o_lval("col", 0x09F865A7AB163A7BLL) = 0LL);
  (o_lval("line", 0x21093C71DDF8728CLL) = 1LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 546 */
void c_stream::t_consume(CVarRef v_str) {
  INSTANCE_METHOD_INJECTION(stream, stream::consume);
  int v_len = 0;

  (v_len = LINE(547,x_strlen(toString(v_str))));
  (o_lval("string", 0x2027864469AD4382LL) = LINE(548,x_substr(toString(o_get("string", 0x2027864469AD4382LL)), v_len)));
  o_lval("col", 0x09F865A7AB163A7BLL) += v_len;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 551 */
Variant c_stream::t_test(CVarRef v_pattern) {
  INSTANCE_METHOD_INJECTION(stream, stream::test);
  Variant v_match;

  if (toBoolean((v_match = LINE(552,f_preg_pattern_test(v_pattern, o_get("string", 0x2027864469AD4382LL)))))) {
    LINE(553,t_consume(v_match.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
    return v_match;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 557 */
p_token c_stream::t_default_rule() {
  INSTANCE_METHOD_INJECTION(stream, stream::default_rule);
  p_point v_start;
  Variant v_ch;
  p_point v_stop;

  if (!(toBoolean(LINE(558,x_strlen(toString(o_get("string", 0x2027864469AD4382LL))))))) return ((Object)(f_null_token()));
  ((Object)((v_start = ((Object)(LINE(560,t_pos()))))));
  (v_ch = o_get("string", 0x2027864469AD4382LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  LINE(562,t_consume(v_ch));
  ((Object)((v_stop = ((Object)(LINE(563,t_pos()))))));
  return ((Object)(LINE(564,p_token(p_token(NEWOBJ(c_token)())->create(concat("c", toString(v_ch)), v_ch, ((Object)(v_start)), ((Object)(v_stop)))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 566 */
p_point c_stream::t_pos() {
  INSTANCE_METHOD_INJECTION(stream, stream::pos);
  return ((Object)(LINE(567,p_point(p_point(NEWOBJ(c_point)())->create(o_get("line", 0x21093C71DDF8728CLL), o_get("col", 0x09F865A7AB163A7BLL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 463 */
Variant c_state_set_labeler::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_state_set_labeler::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_state_set_labeler::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_state_set_labeler::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_state_set_labeler::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_state_set_labeler::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_state_set_labeler::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_state_set_labeler::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(state_set_labeler)
ObjectData *c_state_set_labeler::create() {
  init();
  t_state_set_labeler();
  return this;
}
ObjectData *c_state_set_labeler::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_state_set_labeler::cloneImpl() {
  c_state_set_labeler *obj = NEW(c_state_set_labeler)();
  cloneSet(obj);
  return obj;
}
void c_state_set_labeler::cloneSet(c_state_set_labeler *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_state_set_labeler::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_state_set_labeler::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_state_set_labeler::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_state_set_labeler$os_get(const char *s) {
  return c_state_set_labeler::os_get(s, -1);
}
Variant &cw_state_set_labeler$os_lval(const char *s) {
  return c_state_set_labeler::os_lval(s, -1);
}
Variant cw_state_set_labeler$os_constant(const char *s) {
  return c_state_set_labeler::os_constant(s);
}
Variant cw_state_set_labeler$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_state_set_labeler::os_invoke(c, s, params, -1, fatal);
}
void c_state_set_labeler::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 464 */
void c_state_set_labeler::t_state_set_labeler() {
  INSTANCE_METHOD_INJECTION(state_set_labeler, state_set_labeler::state_set_labeler);
  bool oldInCtor = gasInCtor(true);
  (o_lval("map", 0x06ED228005FEF6A3LL) = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 467 */
Variant c_state_set_labeler::t_label(Variant v_list) {
  INSTANCE_METHOD_INJECTION(state_set_labeler, state_set_labeler::label);
  String v_key;

  LINE(468,x_sort(ref(v_list)));
  (v_key = LINE(469,x_implode(":", v_list)));
  if (empty(o_get("map", 0x06ED228005FEF6A3LL), v_key)) lval(o_lval("map", 0x06ED228005FEF6A3LL)).set(v_key, (LINE(470,f_gen_label())));
  return o_get("map", 0x06ED228005FEF6A3LL).rvalAt(v_key);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 89 */
Variant c_enfa::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_enfa::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_enfa::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_enfa::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_enfa::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_enfa::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_enfa::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_enfa::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(enfa)
ObjectData *c_enfa::create() {
  init();
  t_enfa();
  return this;
}
ObjectData *c_enfa::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_enfa::cloneImpl() {
  c_enfa *obj = NEW(c_enfa)();
  cloneSet(obj);
  return obj;
}
void c_enfa::cloneSet(c_enfa *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_enfa::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_enfa::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_enfa::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_enfa$os_get(const char *s) {
  return c_enfa::os_get(s, -1);
}
Variant &cw_enfa$os_lval(const char *s) {
  return c_enfa::os_lval(s, -1);
}
Variant cw_enfa$os_constant(const char *s) {
  return c_enfa::os_constant(s);
}
Variant cw_enfa$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_enfa::os_invoke(c, s, params, -1, fatal);
}
void c_enfa::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 91 */
void c_enfa::t_enfa() {
  INSTANCE_METHOD_INJECTION(enfa, enfa::enfa);
  bool oldInCtor = gasInCtor(true);
  (o_lval("states", 0x6FB528E40A4C1A15LL) = ScalarArrays::sa_[0]);
  (o_lval("delta", 0x26E26E77DC67FF3FLL) = ScalarArrays::sa_[0]);
  (o_lval("epsilon", 0x0861A5117A0CDD1ALL) = ScalarArrays::sa_[0]);
  (o_lval("mark", 0x19D2840C0AB0CFCFLL) = ScalarArrays::sa_[0]);
  (o_lval("initial", 0x4B073B3CA18015E7LL) = LINE(101,t_add_state(f_gen_label())));
  (o_lval("final", 0x5192930B2145036ELL) = LINE(102,t_add_state(f_gen_label())));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 105 */
Variant c_enfa::t_eclose(CVarRef v_label_list) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::eclose);
  Variant v_states;
  Variant v_queue;
  Variant v_s;
  Variant v_t;

  (v_states = LINE(106,x_array_count_values(v_label_list)));
  (v_queue = LINE(107,x_array_keys(v_states)));
  LOOP_COUNTER(50);
  {
    while (more(LINE(108,x_count(v_queue)), 0LL)) {
      LOOP_COUNTER_CHECK(50);
      {
        (v_s = LINE(109,x_array_shift(ref(v_queue))));
        {
          LOOP_COUNTER(51);
          Variant map52 = o_get("epsilon", 0x0861A5117A0CDD1ALL).rvalAt(v_s);
          for (ArrayIterPtr iter53 = map52.begin("enfa"); !iter53->end(); iter53->next()) {
            LOOP_COUNTER_CHECK(51);
            v_t = iter53->second();
            if (!(isset(v_states, v_t))) {
              v_states.set(v_t, (true));
              v_queue.append((v_t));
            }
          }
        }
      }
    }
  }
  return LINE(115,x_array_keys(v_states));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 118 */
bool c_enfa::t_any_are_final(CVarRef v_label_list) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::any_are_final);
  return LINE(119,x_in_array(o_get("final", 0x5192930B2145036ELL), v_label_list));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 122 */
Variant c_enfa::t_best_mark(CVarRef v_label_list) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::best_mark);
  Variant v_mark;
  Variant v_label;

  (v_mark = 99999LL /* FA_NO_MARK */);
  {
    LOOP_COUNTER(54);
    for (ArrayIterPtr iter56 = v_label_list.begin("enfa"); !iter56->end(); iter56->next()) {
      LOOP_COUNTER_CHECK(54);
      v_label = iter56->second();
      {
        (v_mark = LINE(125,x_min(2, v_mark, Array(ArrayInit(1).set(0, o_get("mark", 0x19D2840C0AB0CFCFLL).rvalAt(v_label)).create()))));
      }
    }
  }
  return v_mark;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 130 */
String c_enfa::t_add_state(CStrRef v_label) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::add_state);
  if (isset(o_get("delta", 0x26E26E77DC67FF3FLL), v_label)) {
    f_exit("Trying to add existing state to an NFA.");
  }
  lval(o_lval("states", 0x6FB528E40A4C1A15LL)).append((v_label));
  lval(o_lval("delta", 0x26E26E77DC67FF3FLL)).set(v_label, (ScalarArrays::sa_[0]));
  lval(o_lval("epsilon", 0x0861A5117A0CDD1ALL)).set(v_label, (ScalarArrays::sa_[0]));
  lval(o_lval("mark", 0x19D2840C0AB0CFCFLL)).set(v_label, (99999LL /* FA_NO_MARK */));
  return v_label;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 141 */
void c_enfa::t_add_epsilon(CVarRef v_src, CVarRef v_dest) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::add_epsilon);
  lval(lval(o_lval("epsilon", 0x0861A5117A0CDD1ALL)).lvalAt(v_src)).append((v_dest));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 145 */
Variant c_enfa::t_start_states() {
  INSTANCE_METHOD_INJECTION(enfa, enfa::start_states);
  return LINE(146,t_eclose(Array(ArrayInit(1).set(0, o_get("initial", 0x4B073B3CA18015E7LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 149 */
void c_enfa::t_add_transition(CVarRef v_src, CVarRef v_glyph, CVarRef v_dest) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::add_transition);
  Variant v_lst;

  (v_lst = ref(lval(lval(o_lval("delta", 0x26E26E77DC67FF3FLL)).lvalAt(v_src))));
  if (empty(v_lst, v_glyph)) v_lst.set(v_glyph, (Array(ArrayInit(1).set(0, v_dest).create())));
  else lval(v_lst.lvalAt(v_glyph)).append((v_dest));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 155 */
Variant c_enfa::t_step(CVarRef v_label_list, CVarRef v_glyph) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::step);
  Variant v_out;
  Variant v_label;

  (v_out = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(57);
    for (ArrayIterPtr iter59 = v_label_list.begin("enfa"); !iter59->end(); iter59->next()) {
      LOOP_COUNTER_CHECK(57);
      v_label = iter59->second();
      {
        if (isset(o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_label), v_glyph)) {
          (v_out = LINE(159,x_array_merge(2, v_out, Array(ArrayInit(1).set(0, o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_label).rvalAt(v_glyph)).create()))));
        }
      }
    }
  }
  return LINE(162,t_eclose(v_out));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 165 */
Variant c_enfa::t_accepting(CVarRef v_label_list) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::accepting);
  Variant v_out;
  Variant v_label;

  (v_out = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(60);
    for (ArrayIterPtr iter62 = v_label_list.begin("enfa"); !iter62->end(); iter62->next()) {
      LOOP_COUNTER_CHECK(60);
      v_label = iter62->second();
      (v_out = LINE(174,x_array_merge(2, v_out, Array(ArrayInit(1).set(0, o_get("delta", 0x26E26E77DC67FF3FLL).rvalAt(v_label)).create()))));
    }
  }
  return LINE(175,x_array_keys(v_out));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 202 */
void c_enfa::t_recognize(CVarRef v_glyph) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::recognize);
  LINE(203,t_add_transition(o_get("initial", 0x4B073B3CA18015E7LL), v_glyph, o_get("final", 0x5192930B2145036ELL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 206 */
void c_enfa::t_plus() {
  INSTANCE_METHOD_INJECTION(enfa, enfa::plus);
  LINE(208,t_add_epsilon(o_get("final", 0x5192930B2145036ELL), o_get("initial", 0x4B073B3CA18015E7LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 211 */
void c_enfa::t_hook() {
  INSTANCE_METHOD_INJECTION(enfa, enfa::hook);
  LINE(213,t_add_epsilon(o_get("initial", 0x4B073B3CA18015E7LL), o_get("final", 0x5192930B2145036ELL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 216 */
void c_enfa::t_kleene() {
  INSTANCE_METHOD_INJECTION(enfa, enfa::kleene);
  LINE(218,t_hook());
  LINE(219,t_plus());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 222 */
void c_enfa::t_copy_in(CVarRef v_nfa) {
  INSTANCE_METHOD_INJECTION(enfa, enfa::copy_in);
  Variant v_part;

  {
    LOOP_COUNTER(63);
    Variant map64 = ScalarArrays::sa_[1];
    for (ArrayIterPtr iter65 = map64.begin("enfa"); !iter65->end(); iter65->next()) {
      LOOP_COUNTER_CHECK(63);
      v_part = iter65->second();
      {
        (o_lval(toString(v_part), -1LL) = LINE(226,x_array_merge(2, o_get(toString(v_part), -1LL), Array(ArrayInit(1).set(0, toObject(v_nfa).o_get(toString(v_part), -1LL)).create()))));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 230 */
p_dfa c_enfa::t_determinize() {
  INSTANCE_METHOD_INJECTION(enfa, enfa::determinize);
  p_state_set_labeler v_map;
  Variant v_start;
  Variant v_queue;
  p_dfa v_dfa;
  Variant v_i;
  Variant v_set;
  Variant v_label;
  Variant v_glyph;
  Variant v_dest;
  Variant v_dest_label;

  ((Object)((v_map = ((Object)(LINE(234,p_state_set_labeler(p_state_set_labeler(NEWOBJ(c_state_set_labeler)())->create())))))));
  (v_start = LINE(235,t_start_states()));
  (v_queue = Array(ArrayInit(1).set(0, v_start).create()));
  ((Object)((v_dfa = ((Object)(LINE(238,p_dfa(p_dfa(NEWOBJ(c_dfa)())->create())))))));
  (v_i = LINE(239,v_map->t_label(v_start)));
  LINE(240,v_dfa->t_add_state(v_i));
  (v_dfa.o_lval("initial", 0x4B073B3CA18015E7LL) = v_i);
  lval(v_dfa.o_lval("mark", 0x19D2840C0AB0CFCFLL)).set(v_i, (LINE(242,t_best_mark(v_start))));
  LOOP_COUNTER(66);
  {
    while (more(LINE(244,x_count(v_queue)), 0LL)) {
      LOOP_COUNTER_CHECK(66);
      {
        (v_set = LINE(245,x_array_shift(ref(v_queue))));
        (v_label = LINE(246,v_map->t_label(v_set)));
        {
          LOOP_COUNTER(67);
          Variant map68 = LINE(247,t_accepting(v_set));
          for (ArrayIterPtr iter69 = map68.begin("enfa"); !iter69->end(); iter69->next()) {
            LOOP_COUNTER_CHECK(67);
            v_glyph = iter69->second();
            {
              (v_dest = LINE(248,t_step(v_set, v_glyph)));
              (v_dest_label = LINE(249,v_map->t_label(v_dest)));
              if (!(LINE(250,v_dfa->t_has_state(v_dest_label)))) {
                LINE(251,v_dfa->t_add_state(v_dest_label));
                lval(v_dfa.o_lval("mark", 0x19D2840C0AB0CFCFLL)).set(v_dest_label, (LINE(252,t_best_mark(v_dest))));
                v_queue.append((v_dest));
              }
              LINE(255,v_dfa->t_add_transition(v_label, v_glyph, v_dest_label));
            }
          }
        }
        if (LINE(257,t_any_are_final(v_set))) lval(v_dfa.o_lval("final", 0x5192930B2145036ELL)).set(v_label, (true));
      }
    }
  }
  return ((Object)(v_dfa));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 766 */
Variant c_default_parser_strategy::os_get(const char *s, int64 hash) {
  return c_parser_strategy::os_get(s, hash);
}
Variant &c_default_parser_strategy::os_lval(const char *s, int64 hash) {
  return c_parser_strategy::os_lval(s, hash);
}
void c_default_parser_strategy::o_get(ArrayElementVec &props) const {
  c_parser_strategy::o_get(props);
}
bool c_default_parser_strategy::o_exists(CStrRef s, int64 hash) const {
  return c_parser_strategy::o_exists(s, hash);
}
Variant c_default_parser_strategy::o_get(CStrRef s, int64 hash) {
  return c_parser_strategy::o_get(s, hash);
}
Variant c_default_parser_strategy::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_parser_strategy::o_set(s, hash, v, forInit);
}
Variant &c_default_parser_strategy::o_lval(CStrRef s, int64 hash) {
  return c_parser_strategy::o_lval(s, hash);
}
Variant c_default_parser_strategy::os_constant(const char *s) {
  return c_parser_strategy::os_constant(s);
}
IMPLEMENT_CLASS(default_parser_strategy)
ObjectData *c_default_parser_strategy::cloneImpl() {
  c_default_parser_strategy *obj = NEW(c_default_parser_strategy)();
  cloneSet(obj);
  return obj;
}
void c_default_parser_strategy::cloneSet(c_default_parser_strategy *clone) {
  c_parser_strategy::cloneSet(clone);
}
Variant c_default_parser_strategy::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4DF50FCFC02BB0E2LL, stuck) {
        return (t_stuck(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x23ECC1F85FE7997FLL, assert_done) {
        return (t_assert_done(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_parser_strategy::o_invoke(s, params, hash, fatal);
}
Variant c_default_parser_strategy::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4DF50FCFC02BB0E2LL, stuck) {
        return (t_stuck(a0, a1, a2), null);
      }
      break;
    case 3:
      HASH_GUARD(0x23ECC1F85FE7997FLL, assert_done) {
        return (t_assert_done(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_parser_strategy::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_default_parser_strategy::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parser_strategy::os_invoke(c, s, params, hash, fatal);
}
Variant cw_default_parser_strategy$os_get(const char *s) {
  return c_default_parser_strategy::os_get(s, -1);
}
Variant &cw_default_parser_strategy$os_lval(const char *s) {
  return c_default_parser_strategy::os_lval(s, -1);
}
Variant cw_default_parser_strategy$os_constant(const char *s) {
  return c_default_parser_strategy::os_constant(s);
}
Variant cw_default_parser_strategy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_default_parser_strategy::os_invoke(c, s, params, -1, fatal);
}
void c_default_parser_strategy::init() {
  c_parser_strategy::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 767 */
void c_default_parser_strategy::t_stuck(CVarRef v_token, CVarRef v_lex, Variant v_stack) {
  INSTANCE_METHOD_INJECTION(default_parser_strategy, default_parser_strategy::stuck);
  Variant v_tos;

  LINE(768,f_send_parse_error_css_styles());
  echo("\t\t<hr/>The LR parser is stuck. Source and grammar do not agree.<br/>\r\n\t\tLooking at token:\r\n\t\t");
  LINE(773,f_span("term", toObject(v_token).o_get("text", 0x2A28A0084DD3A743LL), toObject(v_token).o_get("type", 0x508FC7C8724A760ALL)));
  echo(LINE(774,concat3(" [ ", toString(toObject(v_token).o_get("type", 0x508FC7C8724A760ALL)), " ]")));
  echo("<br/>\n");
  LINE(776,toObject(v_lex)->o_invoke_few_args("report_error", 0x5D0A00D8D1C24295LL, 0));
  echo("<hr/>\n");
  echo("Backtrace Follows:<br/>\n");
  LOOP_COUNTER(70);
  {
    while (toBoolean(LINE(780,x_count(v_stack)))) {
      LOOP_COUNTER_CHECK(70);
      {
        (v_tos = LINE(781,x_array_pop(ref(v_stack))));
        echo(concat(toString(LINE(782,v_tos.o_invoke_few_args("trace", 0x3CCB986B2CF0A747LL, 0))), "<br/>\n"));
      }
    }
  }
  throw_exception(((Object)(LINE(784,p_parse_error(p_parse_error(NEWOBJ(c_parse_error)())->create(concat3("Can't tell what to do with ", toString(toObject(v_token).o_get("type", 0x508FC7C8724A760ALL)), ".")))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 786 */
void c_default_parser_strategy::t_assert_done(Variant v_token, Variant v_lex) {
  INSTANCE_METHOD_INJECTION(default_parser_strategy, default_parser_strategy::assert_done);
  if (toBoolean(v_token.o_get("type", 0x508FC7C8724A760ALL))) LINE(787,o_root_invoke_few_args("stuck", 0x4DF50FCFC02BB0E2LL, 3, v_token, v_lex, Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 7 */
void f_span(CStrRef v_class, CVarRef v_text, Variant v_title //  = ""
) {
  FUNCTION_INJECTION(span);
  String v_extra;

  (v_title = LINE(8,x_htmlspecialchars(toString(v_title))));
  if (toBoolean(v_title)) (v_extra = LINE(9,concat3(" title=\"", toString(v_title), "\"")));
  else (v_extra = "");
  echo(concat("<span class=\"", LINE(11,concat6(v_class, "\"", v_extra, ">", toString(v_text), "</span>\n"))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 273 */
p_enfa f_nfa_union(CArrRef v_nfa_list) {
  FUNCTION_INJECTION(nfa_union);
  p_enfa v_out;
  Variant v_nfa;

  ((Object)((v_out = ((Object)(LINE(274,p_enfa(p_enfa(NEWOBJ(c_enfa)())->create())))))));
  {
    LOOP_COUNTER(71);
    for (ArrayIter iter73 = v_nfa_list.begin(); !iter73.end(); ++iter73) {
      LOOP_COUNTER_CHECK(71);
      v_nfa = iter73.second();
      {
        LINE(276,v_out->t_copy_in(v_nfa));
        LINE(277,v_out->t_add_epsilon(v_out.o_get("initial", 0x4B073B3CA18015E7LL), v_nfa.o_get("initial", 0x4B073B3CA18015E7LL)));
        LINE(278,v_out->t_add_epsilon(v_nfa.o_get("final", 0x5192930B2145036ELL), v_out.o_get("final", 0x5192930B2145036ELL)));
      }
    }
  }
  return ((Object)(v_out));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 755 */
void f_send_parse_error_css_styles() {
  FUNCTION_INJECTION(send_parse_error_css_styles);
  echo("\t");
  echo("<s");
  echo("tyle>\r\n\t.term { border: 1px solid green; margin: 2px; }\r\n\t.char { border: 1px solid red; margin: 2px; }\r\n\t.nonterm { border: 1px solid blue; margin: 10px; }\r\n\t.wierd { border: 1px solid purple; margin: 2px; }\r\n\tpre { line-height: 1.5; }\r\n\t</style>\r\n\t");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 536 */
Variant f_preg_pattern_test(CVarRef v_pattern, CVarRef v_string) {
  FUNCTION_INJECTION(preg_pattern_test);
  Variant v_match;

  if (toBoolean(LINE(537,x_preg_match(concat(toString(v_pattern), "A"), toString(v_string), ref(v_match))))) return v_match;
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 79 */
String f_gen_label() {
  FUNCTION_INJECTION(gen_label);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_count __attribute__((__unused__)) = g->sv_gen_label_DupIdcount;
  bool &inited_sv_count __attribute__((__unused__)) = g->inited_sv_gen_label_DupIdcount;
  if (!inited_sv_count) {
    (sv_count = 0LL);
    inited_sv_count = true;
  }
  sv_count++;
  return concat("s", toString(sv_count));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 518 */
p_token f_null_token() {
  FUNCTION_INJECTION(null_token);
  return ((Object)(LINE(518,p_token(p_token(NEWOBJ(c_token)())->create("", "", "", "")))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 533 */
Array f_preg_pattern(CVarRef v_regex, CVarRef v_type, CVarRef v_ignore, CVarRef v_action) {
  FUNCTION_INJECTION(preg_pattern);
  return Array(ArrayInit(4).set(0, v_regex).set(1, v_type).set(2, v_ignore).set(3, v_action).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 283 */
p_enfa f_nfa_concat(CArrRef v_nfa_list) {
  FUNCTION_INJECTION(nfa_concat);
  p_enfa v_out;
  Variant v_last_state;
  Variant v_nfa;

  ((Object)((v_out = ((Object)(LINE(284,p_enfa(p_enfa(NEWOBJ(c_enfa)())->create())))))));
  (v_last_state = v_out.o_get("initial", 0x4B073B3CA18015E7LL));
  {
    LOOP_COUNTER(74);
    for (ArrayIter iter76 = v_nfa_list.begin(); !iter76.end(); ++iter76) {
      LOOP_COUNTER_CHECK(74);
      v_nfa = iter76.second();
      {
        LINE(287,v_out->t_copy_in(v_nfa));
        LINE(288,v_out->t_add_epsilon(v_last_state, v_nfa.o_get("initial", 0x4B073B3CA18015E7LL)));
        (v_last_state = v_nfa.o_get("final", 0x5192930B2145036ELL));
      }
    }
  }
  LINE(291,v_out->t_add_epsilon(v_last_state, v_out.o_get("final", 0x5192930B2145036ELL)));
  return ((Object)(v_out));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 6 */
void f_bug_unless(CVarRef v_assertion, CStrRef v_gripe //  = "Bug found."
) {
  FUNCTION_INJECTION(bug_unless);
  if (!(toBoolean(v_assertion))) throw_exception(((Object)(LINE(6,p_bug(p_bug(NEWOBJ(c_bug)())->create(v_gripe))))));
} /* function */
Object co_parse_stack_frame(CArrRef params, bool init /* = true */) {
  return Object(p_parse_stack_frame(NEW(c_parse_stack_frame)())->dynCreate(params, init));
}
Object co_parse_error(CArrRef params, bool init /* = true */) {
  return Object(p_parse_error(NEW(c_parse_error)())->dynCreate(params, init));
}
Object co_dfa(CArrRef params, bool init /* = true */) {
  return Object(p_dfa(NEW(c_dfa)())->dynCreate(params, init));
}
Object co_set(CArrRef params, bool init /* = true */) {
  return Object(p_set(NEW(c_set)())->dynCreate(params, init));
}
Object co_easy_parser(CArrRef params, bool init /* = true */) {
  return Object(p_easy_parser(NEW(c_easy_parser)())->dynCreate(params, init));
}
Object co_token_source(CArrRef params, bool init /* = true */) {
  return Object(p_token_source(NEW(c_token_source)())->dynCreate(params, init));
}
Object co_bug(CArrRef params, bool init /* = true */) {
  return Object(p_bug(NEW(c_bug)())->dynCreate(params, init));
}
Object co_parser_strategy(CArrRef params, bool init /* = true */) {
  return Object(p_parser_strategy(NEW(c_parser_strategy)())->dynCreate(params, init));
}
Object co_distinguishing_table(CArrRef params, bool init /* = true */) {
  return Object(p_distinguishing_table(NEW(c_distinguishing_table)())->dynCreate(params, init));
}
Object co_parser(CArrRef params, bool init /* = true */) {
  return Object(p_parser(NEW(c_parser)())->dynCreate(params, init));
}
Object co_token(CArrRef params, bool init /* = true */) {
  return Object(p_token(NEW(c_token)())->dynCreate(params, init));
}
Object co_preg_scanner(CArrRef params, bool init /* = true */) {
  return Object(p_preg_scanner(NEW(c_preg_scanner)())->dynCreate(params, init));
}
Object co_point(CArrRef params, bool init /* = true */) {
  return Object(p_point(NEW(c_point)())->dynCreate(params, init));
}
Object co_stream(CArrRef params, bool init /* = true */) {
  return Object(p_stream(NEW(c_stream)())->dynCreate(params, init));
}
Object co_state_set_labeler(CArrRef params, bool init /* = true */) {
  return Object(p_state_set_labeler(NEW(c_state_set_labeler)())->dynCreate(params, init));
}
Object co_enfa(CArrRef params, bool init /* = true */) {
  return Object(p_enfa(NEW(c_enfa)())->dynCreate(params, init));
}
Object co_default_parser_strategy(CArrRef params, bool init /* = true */) {
  return Object(p_default_parser_strategy(NEW(c_default_parser_strategy)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$parser_so_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$parser_so_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  ;
  (g->GV(wasted) = 0LL);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
