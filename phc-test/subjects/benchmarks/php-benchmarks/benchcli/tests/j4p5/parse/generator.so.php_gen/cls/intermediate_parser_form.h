
#ifndef __GENERATED_cls_intermediate_parser_form_h__
#define __GENERATED_cls_intermediate_parser_form_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 440 */
class c_intermediate_parser_form : virtual public ObjectData {
  BEGIN_CLASS_MAP(intermediate_parser_form)
  END_CLASS_MAP(intermediate_parser_form)
  DECLARE_CLASS(intermediate_parser_form, intermediate_parser_form, ObjectData)
  void init();
  public: Variant m_action;
  public: Variant m_head;
  public: String m_label;
  public: Variant m_Q;
  public: void t___construct(Variant v_rules);
  public: ObjectData *create(Variant v_rules);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add_pops_and_folds();
  public: void t_add_shifts_and_pushes();
  public: void t_epsilon_extend_heads();
  public: Variant t_epsilon_for_state(CVarRef v_label);
  public: Variant t_eclose_of_pushes_from(CVarRef v_label);
  public: Variant t_token_eating_front(CVarRef v_label);
  public: Variant t_epsilon_closure_for_state(CVarRef v_label);
  public: static void ti_add_deltas(const char* cls, Variant v_set, CVarRef v_delta, CStrRef v_what);
  public: void t_compute_head_closures();
  public: void t_compute_lt_closure(Variant v_head);
  public: void t_add_rule(Primitive v_head, CVarRef v_prod);
  public: Array t_compose_program();
  public: Variant t_dpda_transition_function();
  public: Variant t_symbol_start_states();
  public: static void t_add_deltas(CVarRef v_set, CVarRef v_delta, CStrRef v_what) { ti_add_deltas("intermediate_parser_form", v_set, v_delta, v_what); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_intermediate_parser_form_h__
