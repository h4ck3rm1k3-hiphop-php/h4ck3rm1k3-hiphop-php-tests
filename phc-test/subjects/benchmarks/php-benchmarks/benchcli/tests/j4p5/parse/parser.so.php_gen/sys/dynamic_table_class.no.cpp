
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_parse_stack_frame(CArrRef params, bool init = true);
Variant cw_parse_stack_frame$os_get(const char *s);
Variant &cw_parse_stack_frame$os_lval(const char *s);
Variant cw_parse_stack_frame$os_constant(const char *s);
Variant cw_parse_stack_frame$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parse_error(CArrRef params, bool init = true);
Variant cw_parse_error$os_get(const char *s);
Variant &cw_parse_error$os_lval(const char *s);
Variant cw_parse_error$os_constant(const char *s);
Variant cw_parse_error$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_dfa(CArrRef params, bool init = true);
Variant cw_dfa$os_get(const char *s);
Variant &cw_dfa$os_lval(const char *s);
Variant cw_dfa$os_constant(const char *s);
Variant cw_dfa$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_set(CArrRef params, bool init = true);
Variant cw_set$os_get(const char *s);
Variant &cw_set$os_lval(const char *s);
Variant cw_set$os_constant(const char *s);
Variant cw_set$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_easy_parser(CArrRef params, bool init = true);
Variant cw_easy_parser$os_get(const char *s);
Variant &cw_easy_parser$os_lval(const char *s);
Variant cw_easy_parser$os_constant(const char *s);
Variant cw_easy_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_token_source(CArrRef params, bool init = true);
Variant cw_token_source$os_get(const char *s);
Variant &cw_token_source$os_lval(const char *s);
Variant cw_token_source$os_constant(const char *s);
Variant cw_token_source$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bug(CArrRef params, bool init = true);
Variant cw_bug$os_get(const char *s);
Variant &cw_bug$os_lval(const char *s);
Variant cw_bug$os_constant(const char *s);
Variant cw_bug$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parser_strategy(CArrRef params, bool init = true);
Variant cw_parser_strategy$os_get(const char *s);
Variant &cw_parser_strategy$os_lval(const char *s);
Variant cw_parser_strategy$os_constant(const char *s);
Variant cw_parser_strategy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_distinguishing_table(CArrRef params, bool init = true);
Variant cw_distinguishing_table$os_get(const char *s);
Variant &cw_distinguishing_table$os_lval(const char *s);
Variant cw_distinguishing_table$os_constant(const char *s);
Variant cw_distinguishing_table$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parser(CArrRef params, bool init = true);
Variant cw_parser$os_get(const char *s);
Variant &cw_parser$os_lval(const char *s);
Variant cw_parser$os_constant(const char *s);
Variant cw_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_token(CArrRef params, bool init = true);
Variant cw_token$os_get(const char *s);
Variant &cw_token$os_lval(const char *s);
Variant cw_token$os_constant(const char *s);
Variant cw_token$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_preg_scanner(CArrRef params, bool init = true);
Variant cw_preg_scanner$os_get(const char *s);
Variant &cw_preg_scanner$os_lval(const char *s);
Variant cw_preg_scanner$os_constant(const char *s);
Variant cw_preg_scanner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_point(CArrRef params, bool init = true);
Variant cw_point$os_get(const char *s);
Variant &cw_point$os_lval(const char *s);
Variant cw_point$os_constant(const char *s);
Variant cw_point$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stream(CArrRef params, bool init = true);
Variant cw_stream$os_get(const char *s);
Variant &cw_stream$os_lval(const char *s);
Variant cw_stream$os_constant(const char *s);
Variant cw_stream$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_state_set_labeler(CArrRef params, bool init = true);
Variant cw_state_set_labeler$os_get(const char *s);
Variant &cw_state_set_labeler$os_lval(const char *s);
Variant cw_state_set_labeler$os_constant(const char *s);
Variant cw_state_set_labeler$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_enfa(CArrRef params, bool init = true);
Variant cw_enfa$os_get(const char *s);
Variant &cw_enfa$os_lval(const char *s);
Variant cw_enfa$os_constant(const char *s);
Variant cw_enfa$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_default_parser_strategy(CArrRef params, bool init = true);
Variant cw_default_parser_strategy$os_get(const char *s);
Variant &cw_default_parser_strategy$os_lval(const char *s);
Variant cw_default_parser_strategy$os_constant(const char *s);
Variant cw_default_parser_strategy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_CREATE_OBJECT(0x76146CFA52290941LL, point);
      HASH_CREATE_OBJECT(0x43F9BC5E80066881LL, state_set_labeler);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x2158DFF1207C2342LL, parse_stack_frame);
      HASH_CREATE_OBJECT(0x0FCE062EAA2704C2LL, dfa);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x46AB10C92CB593C5LL, parse_error);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x3D8D65C5F9AA490FLL, bug);
      HASH_CREATE_OBJECT(0x465F5F4F142EDE8FLL, parser);
      break;
    case 20:
      HASH_CREATE_OBJECT(0x4972107E6BA8E754LL, stream);
      break;
    case 21:
      HASH_CREATE_OBJECT(0x2B4FAC87929E5255LL, parser_strategy);
      break;
    case 31:
      HASH_CREATE_OBJECT(0x6513AE01C317AF9FLL, token);
      break;
    case 33:
      HASH_CREATE_OBJECT(0x399A6427C2185621LL, set);
      break;
    case 40:
      HASH_CREATE_OBJECT(0x22C1D8B4E21BE328LL, enfa);
      HASH_CREATE_OBJECT(0x6E1ADC31A6009DE8LL, default_parser_strategy);
      break;
    case 47:
      HASH_CREATE_OBJECT(0x3C019446E93FE6AFLL, easy_parser);
      break;
    case 48:
      HASH_CREATE_OBJECT(0x43FD93EE4AD19AB0LL, distinguishing_table);
      break;
    case 55:
      HASH_CREATE_OBJECT(0x09FD59385182ABF7LL, preg_scanner);
      break;
    case 56:
      HASH_CREATE_OBJECT(0x0F55184C7E0875B8LL, token_source);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x76146CFA52290941LL, point);
      HASH_INVOKE_STATIC_METHOD(0x43F9BC5E80066881LL, state_set_labeler);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x2158DFF1207C2342LL, parse_stack_frame);
      HASH_INVOKE_STATIC_METHOD(0x0FCE062EAA2704C2LL, dfa);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x46AB10C92CB593C5LL, parse_error);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x3D8D65C5F9AA490FLL, bug);
      HASH_INVOKE_STATIC_METHOD(0x465F5F4F142EDE8FLL, parser);
      break;
    case 20:
      HASH_INVOKE_STATIC_METHOD(0x4972107E6BA8E754LL, stream);
      break;
    case 21:
      HASH_INVOKE_STATIC_METHOD(0x2B4FAC87929E5255LL, parser_strategy);
      break;
    case 31:
      HASH_INVOKE_STATIC_METHOD(0x6513AE01C317AF9FLL, token);
      break;
    case 33:
      HASH_INVOKE_STATIC_METHOD(0x399A6427C2185621LL, set);
      break;
    case 40:
      HASH_INVOKE_STATIC_METHOD(0x22C1D8B4E21BE328LL, enfa);
      HASH_INVOKE_STATIC_METHOD(0x6E1ADC31A6009DE8LL, default_parser_strategy);
      break;
    case 47:
      HASH_INVOKE_STATIC_METHOD(0x3C019446E93FE6AFLL, easy_parser);
      break;
    case 48:
      HASH_INVOKE_STATIC_METHOD(0x43FD93EE4AD19AB0LL, distinguishing_table);
      break;
    case 55:
      HASH_INVOKE_STATIC_METHOD(0x09FD59385182ABF7LL, preg_scanner);
      break;
    case 56:
      HASH_INVOKE_STATIC_METHOD(0x0F55184C7E0875B8LL, token_source);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x76146CFA52290941LL, point);
      HASH_GET_STATIC_PROPERTY(0x43F9BC5E80066881LL, state_set_labeler);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x2158DFF1207C2342LL, parse_stack_frame);
      HASH_GET_STATIC_PROPERTY(0x0FCE062EAA2704C2LL, dfa);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x46AB10C92CB593C5LL, parse_error);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x3D8D65C5F9AA490FLL, bug);
      HASH_GET_STATIC_PROPERTY(0x465F5F4F142EDE8FLL, parser);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY(0x4972107E6BA8E754LL, stream);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY(0x2B4FAC87929E5255LL, parser_strategy);
      break;
    case 31:
      HASH_GET_STATIC_PROPERTY(0x6513AE01C317AF9FLL, token);
      break;
    case 33:
      HASH_GET_STATIC_PROPERTY(0x399A6427C2185621LL, set);
      break;
    case 40:
      HASH_GET_STATIC_PROPERTY(0x22C1D8B4E21BE328LL, enfa);
      HASH_GET_STATIC_PROPERTY(0x6E1ADC31A6009DE8LL, default_parser_strategy);
      break;
    case 47:
      HASH_GET_STATIC_PROPERTY(0x3C019446E93FE6AFLL, easy_parser);
      break;
    case 48:
      HASH_GET_STATIC_PROPERTY(0x43FD93EE4AD19AB0LL, distinguishing_table);
      break;
    case 55:
      HASH_GET_STATIC_PROPERTY(0x09FD59385182ABF7LL, preg_scanner);
      break;
    case 56:
      HASH_GET_STATIC_PROPERTY(0x0F55184C7E0875B8LL, token_source);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x76146CFA52290941LL, point);
      HASH_GET_STATIC_PROPERTY_LV(0x43F9BC5E80066881LL, state_set_labeler);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x2158DFF1207C2342LL, parse_stack_frame);
      HASH_GET_STATIC_PROPERTY_LV(0x0FCE062EAA2704C2LL, dfa);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x46AB10C92CB593C5LL, parse_error);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x3D8D65C5F9AA490FLL, bug);
      HASH_GET_STATIC_PROPERTY_LV(0x465F5F4F142EDE8FLL, parser);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY_LV(0x4972107E6BA8E754LL, stream);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY_LV(0x2B4FAC87929E5255LL, parser_strategy);
      break;
    case 31:
      HASH_GET_STATIC_PROPERTY_LV(0x6513AE01C317AF9FLL, token);
      break;
    case 33:
      HASH_GET_STATIC_PROPERTY_LV(0x399A6427C2185621LL, set);
      break;
    case 40:
      HASH_GET_STATIC_PROPERTY_LV(0x22C1D8B4E21BE328LL, enfa);
      HASH_GET_STATIC_PROPERTY_LV(0x6E1ADC31A6009DE8LL, default_parser_strategy);
      break;
    case 47:
      HASH_GET_STATIC_PROPERTY_LV(0x3C019446E93FE6AFLL, easy_parser);
      break;
    case 48:
      HASH_GET_STATIC_PROPERTY_LV(0x43FD93EE4AD19AB0LL, distinguishing_table);
      break;
    case 55:
      HASH_GET_STATIC_PROPERTY_LV(0x09FD59385182ABF7LL, preg_scanner);
      break;
    case 56:
      HASH_GET_STATIC_PROPERTY_LV(0x0F55184C7E0875B8LL, token_source);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x76146CFA52290941LL, point);
      HASH_GET_CLASS_CONSTANT(0x43F9BC5E80066881LL, state_set_labeler);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x2158DFF1207C2342LL, parse_stack_frame);
      HASH_GET_CLASS_CONSTANT(0x0FCE062EAA2704C2LL, dfa);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x46AB10C92CB593C5LL, parse_error);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x3D8D65C5F9AA490FLL, bug);
      HASH_GET_CLASS_CONSTANT(0x465F5F4F142EDE8FLL, parser);
      break;
    case 20:
      HASH_GET_CLASS_CONSTANT(0x4972107E6BA8E754LL, stream);
      break;
    case 21:
      HASH_GET_CLASS_CONSTANT(0x2B4FAC87929E5255LL, parser_strategy);
      break;
    case 31:
      HASH_GET_CLASS_CONSTANT(0x6513AE01C317AF9FLL, token);
      break;
    case 33:
      HASH_GET_CLASS_CONSTANT(0x399A6427C2185621LL, set);
      break;
    case 40:
      HASH_GET_CLASS_CONSTANT(0x22C1D8B4E21BE328LL, enfa);
      HASH_GET_CLASS_CONSTANT(0x6E1ADC31A6009DE8LL, default_parser_strategy);
      break;
    case 47:
      HASH_GET_CLASS_CONSTANT(0x3C019446E93FE6AFLL, easy_parser);
      break;
    case 48:
      HASH_GET_CLASS_CONSTANT(0x43FD93EE4AD19AB0LL, distinguishing_table);
      break;
    case 55:
      HASH_GET_CLASS_CONSTANT(0x09FD59385182ABF7LL, preg_scanner);
      break;
    case 56:
      HASH_GET_CLASS_CONSTANT(0x0F55184C7E0875B8LL, token_source);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
