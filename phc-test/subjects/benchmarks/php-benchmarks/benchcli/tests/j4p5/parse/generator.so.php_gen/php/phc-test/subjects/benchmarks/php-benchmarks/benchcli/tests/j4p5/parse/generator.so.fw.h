
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_generator_so_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_generator_so_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(reduce_step)
FORWARD_DECLARE_CLASS(preg_scanner_definition)
FORWARD_DECLARE_CLASS(grammar_scanner)
FORWARD_DECLARE_CLASS(push_step)
FORWARD_DECLARE_CLASS(if_head)
FORWARD_DECLARE_CLASS(fold_step)
FORWARD_DECLARE_CLASS(parse_step)
FORWARD_DECLARE_CLASS(grammar)
FORWARD_DECLARE_CLASS(metascanner)
FORWARD_DECLARE_CLASS(production)
FORWARD_DECLARE_CLASS(eat_step)
FORWARD_DECLARE_CLASS(intermediate_parser_form)
FORWARD_DECLARE_CLASS(production_body)
FORWARD_DECLARE_CLASS(recursive_descent_parser)
FORWARD_DECLARE_CLASS(if_state)
FORWARD_DECLARE_CLASS(grammar_parser)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_generator_so_fw_h__
