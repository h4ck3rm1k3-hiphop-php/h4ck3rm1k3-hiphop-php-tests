
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_reduce_step(CArrRef params, bool init = true);
Variant cw_reduce_step$os_get(const char *s);
Variant &cw_reduce_step$os_lval(const char *s);
Variant cw_reduce_step$os_constant(const char *s);
Variant cw_reduce_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_preg_scanner_definition(CArrRef params, bool init = true);
Variant cw_preg_scanner_definition$os_get(const char *s);
Variant &cw_preg_scanner_definition$os_lval(const char *s);
Variant cw_preg_scanner_definition$os_constant(const char *s);
Variant cw_preg_scanner_definition$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_grammar_scanner(CArrRef params, bool init = true);
Variant cw_grammar_scanner$os_get(const char *s);
Variant &cw_grammar_scanner$os_lval(const char *s);
Variant cw_grammar_scanner$os_constant(const char *s);
Variant cw_grammar_scanner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_push_step(CArrRef params, bool init = true);
Variant cw_push_step$os_get(const char *s);
Variant &cw_push_step$os_lval(const char *s);
Variant cw_push_step$os_constant(const char *s);
Variant cw_push_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_if_head(CArrRef params, bool init = true);
Variant cw_if_head$os_get(const char *s);
Variant &cw_if_head$os_lval(const char *s);
Variant cw_if_head$os_constant(const char *s);
Variant cw_if_head$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fold_step(CArrRef params, bool init = true);
Variant cw_fold_step$os_get(const char *s);
Variant &cw_fold_step$os_lval(const char *s);
Variant cw_fold_step$os_constant(const char *s);
Variant cw_fold_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parse_step(CArrRef params, bool init = true);
Variant cw_parse_step$os_get(const char *s);
Variant &cw_parse_step$os_lval(const char *s);
Variant cw_parse_step$os_constant(const char *s);
Variant cw_parse_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_grammar(CArrRef params, bool init = true);
Variant cw_grammar$os_get(const char *s);
Variant &cw_grammar$os_lval(const char *s);
Variant cw_grammar$os_constant(const char *s);
Variant cw_grammar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_metascanner(CArrRef params, bool init = true);
Variant cw_metascanner$os_get(const char *s);
Variant &cw_metascanner$os_lval(const char *s);
Variant cw_metascanner$os_constant(const char *s);
Variant cw_metascanner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_production(CArrRef params, bool init = true);
Variant cw_production$os_get(const char *s);
Variant &cw_production$os_lval(const char *s);
Variant cw_production$os_constant(const char *s);
Variant cw_production$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_eat_step(CArrRef params, bool init = true);
Variant cw_eat_step$os_get(const char *s);
Variant &cw_eat_step$os_lval(const char *s);
Variant cw_eat_step$os_constant(const char *s);
Variant cw_eat_step$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_intermediate_parser_form(CArrRef params, bool init = true);
Variant cw_intermediate_parser_form$os_get(const char *s);
Variant &cw_intermediate_parser_form$os_lval(const char *s);
Variant cw_intermediate_parser_form$os_constant(const char *s);
Variant cw_intermediate_parser_form$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_production_body(CArrRef params, bool init = true);
Variant cw_production_body$os_get(const char *s);
Variant &cw_production_body$os_lval(const char *s);
Variant cw_production_body$os_constant(const char *s);
Variant cw_production_body$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_recursive_descent_parser(CArrRef params, bool init = true);
Variant cw_recursive_descent_parser$os_get(const char *s);
Variant &cw_recursive_descent_parser$os_lval(const char *s);
Variant cw_recursive_descent_parser$os_constant(const char *s);
Variant cw_recursive_descent_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_if_state(CArrRef params, bool init = true);
Variant cw_if_state$os_get(const char *s);
Variant &cw_if_state$os_lval(const char *s);
Variant cw_if_state$os_constant(const char *s);
Variant cw_if_state$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_grammar_parser(CArrRef params, bool init = true);
Variant cw_grammar_parser$os_get(const char *s);
Variant &cw_grammar_parser$os_lval(const char *s);
Variant cw_grammar_parser$os_constant(const char *s);
Variant cw_grammar_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 3:
      HASH_CREATE_OBJECT(0x3EA4071DAB40B643LL, preg_scanner_definition);
      break;
    case 8:
      HASH_CREATE_OBJECT(0x708AFE1C652E8588LL, metascanner);
      HASH_CREATE_OBJECT(0x64A4F28FB66E7768LL, eat_step);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x0F8CF1CBF9204ACCLL, grammar_scanner);
      break;
    case 16:
      HASH_CREATE_OBJECT(0x1FA895B54BA816D0LL, fold_step);
      HASH_CREATE_OBJECT(0x1560BE6FEB4ACD70LL, if_state);
      break;
    case 17:
      HASH_CREATE_OBJECT(0x364B31DA720A6131LL, grammar);
      break;
    case 18:
      HASH_CREATE_OBJECT(0x22B28D4313C913D2LL, parse_step);
      break;
    case 19:
      HASH_CREATE_OBJECT(0x52E9CDF6251253D3LL, if_head);
      break;
    case 20:
      HASH_CREATE_OBJECT(0x4C0C21CA685F54B4LL, production);
      break;
    case 22:
      HASH_CREATE_OBJECT(0x1D3B6BC53D4BB596LL, intermediate_parser_form);
      break;
    case 24:
      HASH_CREATE_OBJECT(0x368273C92FA35EF8LL, grammar_parser);
      break;
    case 27:
      HASH_CREATE_OBJECT(0x345522251A4AB57BLL, push_step);
      break;
    case 28:
      HASH_CREATE_OBJECT(0x2876D6D84B28AA7CLL, reduce_step);
      HASH_CREATE_OBJECT(0x2252B2647853665CLL, production_body);
      break;
    case 31:
      HASH_CREATE_OBJECT(0x08BE071C6338A11FLL, recursive_descent_parser);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x3EA4071DAB40B643LL, preg_scanner_definition);
      break;
    case 8:
      HASH_INVOKE_STATIC_METHOD(0x708AFE1C652E8588LL, metascanner);
      HASH_INVOKE_STATIC_METHOD(0x64A4F28FB66E7768LL, eat_step);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x0F8CF1CBF9204ACCLL, grammar_scanner);
      break;
    case 16:
      HASH_INVOKE_STATIC_METHOD(0x1FA895B54BA816D0LL, fold_step);
      HASH_INVOKE_STATIC_METHOD(0x1560BE6FEB4ACD70LL, if_state);
      break;
    case 17:
      HASH_INVOKE_STATIC_METHOD(0x364B31DA720A6131LL, grammar);
      break;
    case 18:
      HASH_INVOKE_STATIC_METHOD(0x22B28D4313C913D2LL, parse_step);
      break;
    case 19:
      HASH_INVOKE_STATIC_METHOD(0x52E9CDF6251253D3LL, if_head);
      break;
    case 20:
      HASH_INVOKE_STATIC_METHOD(0x4C0C21CA685F54B4LL, production);
      break;
    case 22:
      HASH_INVOKE_STATIC_METHOD(0x1D3B6BC53D4BB596LL, intermediate_parser_form);
      break;
    case 24:
      HASH_INVOKE_STATIC_METHOD(0x368273C92FA35EF8LL, grammar_parser);
      break;
    case 27:
      HASH_INVOKE_STATIC_METHOD(0x345522251A4AB57BLL, push_step);
      break;
    case 28:
      HASH_INVOKE_STATIC_METHOD(0x2876D6D84B28AA7CLL, reduce_step);
      HASH_INVOKE_STATIC_METHOD(0x2252B2647853665CLL, production_body);
      break;
    case 31:
      HASH_INVOKE_STATIC_METHOD(0x08BE071C6338A11FLL, recursive_descent_parser);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 3:
      HASH_GET_STATIC_PROPERTY(0x3EA4071DAB40B643LL, preg_scanner_definition);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY(0x708AFE1C652E8588LL, metascanner);
      HASH_GET_STATIC_PROPERTY(0x64A4F28FB66E7768LL, eat_step);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x0F8CF1CBF9204ACCLL, grammar_scanner);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY(0x1FA895B54BA816D0LL, fold_step);
      HASH_GET_STATIC_PROPERTY(0x1560BE6FEB4ACD70LL, if_state);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY(0x364B31DA720A6131LL, grammar);
      break;
    case 18:
      HASH_GET_STATIC_PROPERTY(0x22B28D4313C913D2LL, parse_step);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY(0x52E9CDF6251253D3LL, if_head);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY(0x4C0C21CA685F54B4LL, production);
      break;
    case 22:
      HASH_GET_STATIC_PROPERTY(0x1D3B6BC53D4BB596LL, intermediate_parser_form);
      break;
    case 24:
      HASH_GET_STATIC_PROPERTY(0x368273C92FA35EF8LL, grammar_parser);
      break;
    case 27:
      HASH_GET_STATIC_PROPERTY(0x345522251A4AB57BLL, push_step);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY(0x2876D6D84B28AA7CLL, reduce_step);
      HASH_GET_STATIC_PROPERTY(0x2252B2647853665CLL, production_body);
      break;
    case 31:
      HASH_GET_STATIC_PROPERTY(0x08BE071C6338A11FLL, recursive_descent_parser);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x3EA4071DAB40B643LL, preg_scanner_definition);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY_LV(0x708AFE1C652E8588LL, metascanner);
      HASH_GET_STATIC_PROPERTY_LV(0x64A4F28FB66E7768LL, eat_step);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x0F8CF1CBF9204ACCLL, grammar_scanner);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY_LV(0x1FA895B54BA816D0LL, fold_step);
      HASH_GET_STATIC_PROPERTY_LV(0x1560BE6FEB4ACD70LL, if_state);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY_LV(0x364B31DA720A6131LL, grammar);
      break;
    case 18:
      HASH_GET_STATIC_PROPERTY_LV(0x22B28D4313C913D2LL, parse_step);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY_LV(0x52E9CDF6251253D3LL, if_head);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY_LV(0x4C0C21CA685F54B4LL, production);
      break;
    case 22:
      HASH_GET_STATIC_PROPERTY_LV(0x1D3B6BC53D4BB596LL, intermediate_parser_form);
      break;
    case 24:
      HASH_GET_STATIC_PROPERTY_LV(0x368273C92FA35EF8LL, grammar_parser);
      break;
    case 27:
      HASH_GET_STATIC_PROPERTY_LV(0x345522251A4AB57BLL, push_step);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY_LV(0x2876D6D84B28AA7CLL, reduce_step);
      HASH_GET_STATIC_PROPERTY_LV(0x2252B2647853665CLL, production_body);
      break;
    case 31:
      HASH_GET_STATIC_PROPERTY_LV(0x08BE071C6338A11FLL, recursive_descent_parser);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 3:
      HASH_GET_CLASS_CONSTANT(0x3EA4071DAB40B643LL, preg_scanner_definition);
      break;
    case 8:
      HASH_GET_CLASS_CONSTANT(0x708AFE1C652E8588LL, metascanner);
      HASH_GET_CLASS_CONSTANT(0x64A4F28FB66E7768LL, eat_step);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x0F8CF1CBF9204ACCLL, grammar_scanner);
      break;
    case 16:
      HASH_GET_CLASS_CONSTANT(0x1FA895B54BA816D0LL, fold_step);
      HASH_GET_CLASS_CONSTANT(0x1560BE6FEB4ACD70LL, if_state);
      break;
    case 17:
      HASH_GET_CLASS_CONSTANT(0x364B31DA720A6131LL, grammar);
      break;
    case 18:
      HASH_GET_CLASS_CONSTANT(0x22B28D4313C913D2LL, parse_step);
      break;
    case 19:
      HASH_GET_CLASS_CONSTANT(0x52E9CDF6251253D3LL, if_head);
      break;
    case 20:
      HASH_GET_CLASS_CONSTANT(0x4C0C21CA685F54B4LL, production);
      break;
    case 22:
      HASH_GET_CLASS_CONSTANT(0x1D3B6BC53D4BB596LL, intermediate_parser_form);
      break;
    case 24:
      HASH_GET_CLASS_CONSTANT(0x368273C92FA35EF8LL, grammar_parser);
      break;
    case 27:
      HASH_GET_CLASS_CONSTANT(0x345522251A4AB57BLL, push_step);
      break;
    case 28:
      HASH_GET_CLASS_CONSTANT(0x2876D6D84B28AA7CLL, reduce_step);
      HASH_GET_CLASS_CONSTANT(0x2252B2647853665CLL, production_body);
      break;
    case 31:
      HASH_GET_CLASS_CONSTANT(0x08BE071C6338A11FLL, recursive_descent_parser);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
