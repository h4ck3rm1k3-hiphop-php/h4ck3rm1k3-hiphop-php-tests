
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/metascanner.y.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$metascanner_y(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/metascanner.y);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$metascanner_y;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("# Grammar for metascan\n\nscanner: definition_list divider {return new preg_scanner_definition({0});}\n| scanner rule {{0}->add_common_rule({1}); return {0};}\n| scanner scope {{0}->add_rule_list({1}[0], {1}[1]); return {0};}\n\ndefinition_list: definition* {return $tokens;}\ndefinition: directive identifier+ {return $tokens;}\n\nscope_list: scope_tag+ {return $tokens;}\nscope:\tscope_list rule {return array");
  echo("({0}, array({1}));}\n| scope_list openbrace rule_list closebrace {return array({0}, {2});}\n\nrule: regex modlist identifier behaviour action {return preg_pattern(metascanner::make_regex({0}, {1}), {2}, {3}, {4});}\nrule_list: rule* {return $tokens;}\nmodlist: modifier* {return implode('', $tokens);}\n\nbehaviour: EPSILON {return 0;}\n| ignore {return 1;}\n\naction: EPSILON {return '';}\n| openbrace code clo");
  echo("sebrace {return mk_action({1});}\n\ncode: (sstring|dstring|linecomment|blockcomment|php| openbrace code closebrace)* {return implode('', $tokens);}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
