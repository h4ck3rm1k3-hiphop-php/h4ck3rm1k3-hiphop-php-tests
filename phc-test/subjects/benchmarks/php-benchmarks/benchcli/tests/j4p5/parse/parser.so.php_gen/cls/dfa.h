
#ifndef __GENERATED_cls_dfa_h__
#define __GENERATED_cls_dfa_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 297 */
class c_dfa : virtual public ObjectData {
  BEGIN_CLASS_MAP(dfa)
  END_CLASS_MAP(dfa)
  DECLARE_CLASS(dfa, dfa, ObjectData)
  void init();
  public: void t_dfa();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_add_state(CVarRef v_label);
  public: bool t_has_state(CVarRef v_label);
  public: void t_add_transition(CVarRef v_src, CVarRef v_glyph, CVarRef v_dest);
  public: Variant t_step(CVarRef v_label, CVarRef v_glyph);
  public: Variant t_accepting(CVarRef v_label);
  public: p_dfa t_minimize();
  public: Variant t_indistinguishable_state_map(CVarRef v_table);
  public: Variant t_table_fill();
  public: bool t_filling_round(Variant v_table);
  public: bool t_compare_states(CVarRef v_p, CVarRef v_q, CVarRef v_table);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_dfa_h__
