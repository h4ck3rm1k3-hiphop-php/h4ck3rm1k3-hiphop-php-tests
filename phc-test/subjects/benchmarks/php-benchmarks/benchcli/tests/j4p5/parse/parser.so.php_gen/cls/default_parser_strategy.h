
#ifndef __GENERATED_cls_default_parser_strategy_h__
#define __GENERATED_cls_default_parser_strategy_h__

#include <cls/parser_strategy.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 766 */
class c_default_parser_strategy : virtual public c_parser_strategy {
  BEGIN_CLASS_MAP(default_parser_strategy)
    PARENT_CLASS(parser_strategy)
  END_CLASS_MAP(default_parser_strategy)
  DECLARE_CLASS(default_parser_strategy, default_parser_strategy, parser_strategy)
  void init();
  public: void t_stuck(CVarRef v_token, CVarRef v_lex, Variant v_stack);
  public: void t_assert_done(Variant v_token, Variant v_lex);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_default_parser_strategy_h__
