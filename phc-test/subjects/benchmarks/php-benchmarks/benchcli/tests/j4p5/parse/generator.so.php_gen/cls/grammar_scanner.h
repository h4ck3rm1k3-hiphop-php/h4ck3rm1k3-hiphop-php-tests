
#ifndef __GENERATED_cls_grammar_scanner_h__
#define __GENERATED_cls_grammar_scanner_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 35 */
class c_grammar_scanner : virtual public ObjectData {
  BEGIN_CLASS_MAP(grammar_scanner)
  END_CLASS_MAP(grammar_scanner)
  DECLARE_CLASS(grammar_scanner, grammar_scanner, preg_scanner)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_grammar_scanner_h__
