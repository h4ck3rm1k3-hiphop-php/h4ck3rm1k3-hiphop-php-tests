
#ifndef __GENERATED_cls_state_set_labeler_h__
#define __GENERATED_cls_state_set_labeler_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 463 */
class c_state_set_labeler : virtual public ObjectData {
  BEGIN_CLASS_MAP(state_set_labeler)
  END_CLASS_MAP(state_set_labeler)
  DECLARE_CLASS(state_set_labeler, state_set_labeler, ObjectData)
  void init();
  public: void t_state_set_labeler();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_label(Variant v_list);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_state_set_labeler_h__
