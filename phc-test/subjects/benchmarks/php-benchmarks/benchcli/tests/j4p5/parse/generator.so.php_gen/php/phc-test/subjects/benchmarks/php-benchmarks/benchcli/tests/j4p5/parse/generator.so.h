
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_generator_so_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_generator_so_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.fw.h>

// Declarations
#include <cls/reduce_step.h>
#include <cls/preg_scanner_definition.h>
#include <cls/grammar_scanner.h>
#include <cls/push_step.h>
#include <cls/if_head.h>
#include <cls/fold_step.h>
#include <cls/parse_step.h>
#include <cls/grammar.h>
#include <cls/metascanner.h>
#include <cls/production.h>
#include <cls/eat_step.h>
#include <cls/intermediate_parser_form.h>
#include <cls/production_body.h>
#include <cls/recursive_descent_parser.h>
#include <cls/if_state.h>
#include <cls/grammar_parser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_define_function(Variant v_args, Variant v_code);
Variant f_generate_scanner_from_file(CVarRef v_filename, Variant v_init_context);
Variant f_generate_parser_from_file(CStrRef v_filename);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$parse$generator_so_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_location_of_scanner_grammar();
Variant f_mk_action(CStrRef v_code);
Variant f_recursive_closure(CArrRef v_callable, Variant v_queue);
Object f_scanner_parser();
int64 f_gen_mark();
Object co_reduce_step(CArrRef params, bool init = true);
Object co_preg_scanner_definition(CArrRef params, bool init = true);
Object co_grammar_scanner(CArrRef params, bool init = true);
Object co_push_step(CArrRef params, bool init = true);
Object co_if_head(CArrRef params, bool init = true);
Object co_fold_step(CArrRef params, bool init = true);
Object co_parse_step(CArrRef params, bool init = true);
Object co_grammar(CArrRef params, bool init = true);
Object co_metascanner(CArrRef params, bool init = true);
Object co_production(CArrRef params, bool init = true);
Object co_eat_step(CArrRef params, bool init = true);
Object co_intermediate_parser_form(CArrRef params, bool init = true);
Object co_production_body(CArrRef params, bool init = true);
Object co_recursive_descent_parser(CArrRef params, bool init = true);
Object co_if_state(CArrRef params, bool init = true);
Object co_grammar_parser(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_parse_generator_so_h__
