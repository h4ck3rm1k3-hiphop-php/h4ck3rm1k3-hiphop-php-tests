
#ifndef __GENERATED_cls_enfa_h__
#define __GENERATED_cls_enfa_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 89 */
class c_enfa : virtual public ObjectData {
  BEGIN_CLASS_MAP(enfa)
  END_CLASS_MAP(enfa)
  DECLARE_CLASS(enfa, enfa, ObjectData)
  void init();
  public: void t_enfa();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_eclose(CVarRef v_label_list);
  public: bool t_any_are_final(CVarRef v_label_list);
  public: Variant t_best_mark(CVarRef v_label_list);
  public: String t_add_state(CStrRef v_label);
  public: void t_add_epsilon(CVarRef v_src, CVarRef v_dest);
  public: Variant t_start_states();
  public: void t_add_transition(CVarRef v_src, CVarRef v_glyph, CVarRef v_dest);
  public: Variant t_step(CVarRef v_label_list, CVarRef v_glyph);
  public: Variant t_accepting(CVarRef v_label_list);
  public: void t_recognize(CVarRef v_glyph);
  public: void t_plus();
  public: void t_hook();
  public: void t_kleene();
  public: void t_copy_in(CVarRef v_nfa);
  public: p_dfa t_determinize();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_enfa_h__
