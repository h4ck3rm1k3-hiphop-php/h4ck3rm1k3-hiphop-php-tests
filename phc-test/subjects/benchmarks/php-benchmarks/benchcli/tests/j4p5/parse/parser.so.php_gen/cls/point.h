
#ifndef __GENERATED_cls_point_h__
#define __GENERATED_cls_point_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 570 */
class c_point : virtual public ObjectData {
  BEGIN_CLASS_MAP(point)
  END_CLASS_MAP(point)
  DECLARE_CLASS(point, point, ObjectData)
  void init();
  public: void t___construct(Variant v_line, Variant v_col);
  public: ObjectData *create(Variant v_line, Variant v_col);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_point_h__
