
#ifndef __GENERATED_cls_metascanner_h__
#define __GENERATED_cls_metascanner_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 669 */
class c_metascanner : virtual public ObjectData {
  BEGIN_CLASS_MAP(metascanner)
  END_CLASS_MAP(metascanner)
  DECLARE_CLASS(metascanner, metascanner, preg_scanner)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static String ti_make_regex(const char* cls, CVarRef v_expression, CVarRef v_flags);
  public: static Variant ti_suitable_delimiter(const char* cls, CVarRef v_expression);
  public: static Variant t_suitable_delimiter(CVarRef v_expression) { return ti_suitable_delimiter("metascanner", v_expression); }
  public: static String t_make_regex(CVarRef v_expression, CVarRef v_flags) { return ti_make_regex("metascanner", v_expression, v_flags); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_metascanner_h__
