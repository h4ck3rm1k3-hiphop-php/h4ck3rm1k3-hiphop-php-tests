
#ifndef __GENERATED_cls_token_h__
#define __GENERATED_cls_token_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 510 */
class c_token : virtual public ObjectData {
  BEGIN_CLASS_MAP(token)
  END_CLASS_MAP(token)
  DECLARE_CLASS(token, token, ObjectData)
  void init();
  public: void t___construct(Variant v_type, Variant v_text, Variant v_start, Variant v_stop);
  public: ObjectData *create(Variant v_type, Variant v_text, Variant v_start, Variant v_stop);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_token_h__
