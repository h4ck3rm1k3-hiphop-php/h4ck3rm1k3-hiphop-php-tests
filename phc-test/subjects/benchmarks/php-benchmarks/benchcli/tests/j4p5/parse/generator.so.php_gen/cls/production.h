
#ifndef __GENERATED_cls_production_h__
#define __GENERATED_cls_production_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 303 */
class c_production : virtual public ObjectData {
  BEGIN_CLASS_MAP(production)
  END_CLASS_MAP(production)
  DECLARE_CLASS(production, production, ObjectData)
  void init();
  public: void t_production(CVarRef v_head);
  public: ObjectData *create(CVarRef v_head);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_add(p_production_body v_body);
  public: void t_determinize();
  public: void t_minimize();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_production_h__
