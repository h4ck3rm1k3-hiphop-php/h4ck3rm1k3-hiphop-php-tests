
#ifndef __GENERATED_cls_push_step_h__
#define __GENERATED_cls_push_step_h__

#include <cls/parse_step.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/generator.so.php line 392 */
class c_push_step : virtual public c_parse_step {
  BEGIN_CLASS_MAP(push_step)
    PARENT_CLASS(parse_step)
  END_CLASS_MAP(push_step)
  DECLARE_CLASS(push_step, push_step, parse_step)
  void init();
  public: void t___construct(Variant v_gamma, Variant v_after);
  public: ObjectData *create(Variant v_gamma, Variant v_after);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_phrase();
  public: Array t_instruction();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_push_step_h__
