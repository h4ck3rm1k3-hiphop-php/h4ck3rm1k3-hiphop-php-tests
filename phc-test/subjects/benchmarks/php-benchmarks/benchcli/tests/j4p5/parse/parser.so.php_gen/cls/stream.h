
#ifndef __GENERATED_cls_stream_h__
#define __GENERATED_cls_stream_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/parse/parser.so.php line 540 */
class c_stream : virtual public ObjectData {
  BEGIN_CLASS_MAP(stream)
  END_CLASS_MAP(stream)
  DECLARE_CLASS(stream, stream, ObjectData)
  void init();
  public: void t___construct(Variant v_string);
  public: ObjectData *create(Variant v_string);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_consume(CVarRef v_str);
  public: Variant t_test(CVarRef v_pattern);
  public: p_token t_default_rule();
  public: p_point t_pos();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_stream_h__
