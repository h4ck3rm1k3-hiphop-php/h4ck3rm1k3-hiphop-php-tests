
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.l.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_l(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.l);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_l;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("%x text\r\n%%\r\n\r\n/\\*.*\?\\*/\t/s\tmlcomment\tignore\r\n//[^\\x0A\\x0D]*\t\tslcomment\tignore\r\n[\\x0A\\x0D]\t\tnewline\t\tignore\r\n\\s+\t\t\twhitespace\tignore\r\n\r\n\\\?\\>\t\t\tT_SCRIPT_END {$state='text';}\r\n\\bfunction\\b\t\tT_FUNCTION\r\n\\bvar\\b\t\t\tT_VAR\r\n\\bdo\\b\t\t\tT_DO\r\n\\bwhile\\b\t\tT_WHILE\r\n\\bfor\\b\t\t\tT_FOR\r\n\\bin\\b\t\t\tT_IN\r\n\\bwith\\b\t\tT_WITH\r\n\\bswitch\\b\t\tT_SWITCH\r\n\\bcase\\b\t\tT_CASE\r\n\\bdefault\\b\t\tT_DEFAULT\r\n\\bthrow\\b\t\tT_THROW\r\n\\btry\\b\t\t\tT_TR");
  echo("Y\r\n\\bcatch\\b\t\tT_CATCH\r\n\\bfinally\\b\t\tT_FINALLY\r\n\\bcontinue\\b\t\tT_CONTINUE\r\n\\bbreak\\b\t\tT_BREAK\r\n\\breturn\\b\t\tT_RETURN\r\n\\bnew\\b\t\t\tT_NEW\r\n\\bdelete\\b\t\tT_DELETE\r\n\\bvoid\\b\t\tT_VOID\r\n\\btypeof\\b\t\tT_TYPEOF\r\n\\binstanceof\\b\t\tT_INSTANCEOF\r\n\\bnull\\b\t\tT_NULL\r\n\\btrue\\b\t\tT_TRUE\r\n\\bfalse\\b\t\tT_FALSE\r\n\\bif\\b\t\t\tT_IF\r\n\\bthen\\b\t\tT_THEN\r\n\\belse\\b\t\tT_ELSE\r\n\\bthis\\b\t\tT_THIS\r\n\\(\t\t\tT_LEFTPARENS\r\n\\)\t\t\tT_RIGHTPARENS\r\n\\{\t\t\tT_LEFTB");
  echo("RACE\r\n\\}\t\t\tT_RIGHTBRACE\r\n\\[\t\t\tT_LEFTBRACKET\r\n\\]\t\t\tT_RIGHTBRACKET\r\n\\.\t\t\tT_DOT\r\n,\t\t\tT_COMMA\r\n;\t\t\tT_SEMICOLON\r\n:\t\t\tT_COLON\r\n(\?:[*/%+&^|-]|>>>\?|");
  echo("<");
  echo("<)=\tT_ASSIGN\r\n===\t\t\tT_EQEQEQ\r\n==\t\t\tT_EQEQ\r\n=\t\t\tT_EQUAL\r\n\\+\\+\t\t\tT_PLUSPLUS\r\n\\+\t\t\tT_PLUS\r\n--\t\t\tT_MINUSMINUS\r\n-\t\t\tT_MINUS\r\n[~]\t\t\tT_TILDE\r\n!==\t\t\tT_BANGEQEQ\r\n!=\t\t\tT_BANGEQ\r\n[!]\t\t\tT_BANG\r\n[*]\t\t\tT_STAR\r\n[/]\t\t\tT_SLASH\r\n[%]\t\t\tT_PERCENT\r\n>>>\t\t\tT_GTGTGT\r\n");
  echo("<");
  echo("<\t\t\tT_LTLT\r\n>>\t\t\tT_GTGT\r\n<=\t\t\tT_LTEQ\r\n>=\t\t\tT_GTEQ\r\n<\t\t\tT_LT\r\n>\t\t\tT_GT\r\n[\\^]\t\t\tT_HAT\r\n[&][&]\t\t\tT_AMPAMP\r\n[&]\t\t\tT_AMP\r\n[|][|]\t\t\tT_PIPEPIPE\r\n[|]\t\t\tT_PIPE\r\n[\?]\t\t\tT_QUESTMARK\r\n[\\$_a-zA-Z](\?:\\w|\\$|_)*:\tT_LABEL\r\n[\\$_a-zA-Z](\?:\\w|\\$|_)*\tT_WORD\r\n\r\n[0][xX][0-9a-zA-Z]+\tT_HEXA { $text = hexdec($text); }\r\n(\?:(\?:[0]|[1-9]\\d*)\\.\\d*|\\.\\d+|(\?:[0]|[1-9]\\d*))(\?:[eE][-+]\?\\d+)\?\tT_DECIMAL { $text = $text-0; }\r\n'(\?:[^'\\\\]|");
  echo("\\\\.)*'|\"(\?:[^\"\\\\]|\\\\.)*\"\tT_STRING\r\n\r\n#-- (\?i:/[^/]*/[a-z]*)\tregexp\r\n\r\n<text>{\r\n\\<\\\?(\?:js)\?\t\tT_SCRIPT_BEGIN {$state='INITIAL';}\r\n\\<\\\?=\t\t\tT_SCRIPT_BEGIN_ECHO {$state='INITIAL';}\r\n(\?:[^<]|<[^\?])*\t\t/s\tT_TEXT\r\n}\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
