
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i___lambda_61(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_85(CArrRef params);
Variant i___lambda_86(CArrRef params);
static Variant invoke_case_8(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x4F7A62998A5D7508LL, __lambda_85);
  HASH_INVOKE(0x60E15C9D5C5B1808LL, __lambda_86);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_69(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_65(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_25(CArrRef params);
Variant i___lambda_75(CArrRef params);
static Variant invoke_case_14(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x2C9D722135D2510ELL, __lambda_25);
  HASH_INVOKE(0x036F38CD79E3D60ELL, __lambda_75);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_94(CArrRef params);
Variant i___lambda_137(CArrRef params);
static Variant invoke_case_18(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x24F37484D0AA6F12LL, __lambda_94);
  HASH_INVOKE(0x36C05A439FB71E12LL, __lambda_137);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_40(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_35(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_109(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_57(CArrRef params);
Variant i___lambda_66(CArrRef params);
Variant i___lambda_89(CArrRef params);
static Variant invoke_case_25(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x36A2EE98ED47E219LL, __lambda_57);
  HASH_INVOKE(0x357A59B9AB7B7119LL, __lambda_66);
  HASH_INVOKE(0x2C035B5D61CC3719LL, __lambda_89);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_58(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_118(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_140(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_103(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_112(CArrRef params);
Variant i___lambda_76(CArrRef params);
static Variant invoke_case_35(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x1506EEFF85119323LL, __lambda_112);
  HASH_INVOKE(0x2FBF32380D9D0623LL, __lambda_76);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_106(CArrRef params);
Variant i___lambda_96(CArrRef params);
static Variant invoke_case_37(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x0B19BA7F84420425LL, __lambda_106);
  HASH_INVOKE(0x1C6F977705F4DE25LL, __lambda_96);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_116(CArrRef params);
Variant i___lambda_139(CArrRef params);
static Variant invoke_case_43(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x2104071D33B1132BLL, __lambda_116);
  HASH_INVOKE(0x64EE61BDA5BBAC2BLL, __lambda_139);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_80(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_60(CArrRef params);
Variant i___lambda_56(CArrRef params);
static Variant invoke_case_48(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x7FD9C7926C142630LL, __lambda_60);
  HASH_INVOKE(0x7409A83737404930LL, __lambda_56);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_82(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_98(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_134(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_73(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_83(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_87(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_26(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_125(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_115(CArrRef params);
Variant i___lambda_138(CArrRef params);
static Variant invoke_case_65(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x5D3D089E8673F841LL, __lambda_115);
  HASH_INVOKE(0x2127633EF87C1541LL, __lambda_138);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_105(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_110(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_42(CArrRef params);
Variant i___lambda_78(CArrRef params);
static Variant invoke_case_69(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x46BD5E3B2298C345LL, __lambda_42);
  HASH_INVOKE(0x24F9971EEE4A3145LL, __lambda_78);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_64(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_36(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_102(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_53(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_132(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_27(CArrRef params);
Variant i___lambda_113(CArrRef params);
static Variant invoke_case_90(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x0830154916BC4C5ALL, __lambda_27);
  HASH_INVOKE(0x34265A2260645B5ALL, __lambda_113);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_99(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_128(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_38(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_48(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_34(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_44(CArrRef params);
Variant i___lambda_141(CArrRef params);
static Variant invoke_case_118(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x6A5F2C0AF1CD9176LL, __lambda_44);
  HASH_INVOKE(0x03F69C86F9384276LL, __lambda_141);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_71(CArrRef params);
Variant i___lambda_124(CArrRef params);
static Variant invoke_case_122(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x4A7D814D56DC007ALL, __lambda_71);
  HASH_INVOKE(0x461B63850487A37ALL, __lambda_124);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_49(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_91(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_114(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_33(CArrRef params);
Variant i___lambda_43(CArrRef params);
static Variant invoke_case_133(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x390CD558B6DF8685LL, __lambda_33);
  HASH_INVOKE(0x45A40CB147450085LL, __lambda_43);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_55(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_54(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_52(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_95(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_79(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_104(CArrRef params);
Variant i___lambda_123(CArrRef params);
Variant i___lambda_127(CArrRef params);
Variant i___lambda_144(CArrRef params);
static Variant invoke_case_147(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x37136940E1553A93LL, __lambda_104);
  HASH_INVOKE(0x26FBF8622932F893LL, __lambda_123);
  HASH_INVOKE(0x60A546D1EEDC4393LL, __lambda_127);
  HASH_INVOKE(0x0563C5C1801DA293LL, __lambda_144);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_77(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_131(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_135(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_31(CArrRef params);
Variant i___lambda_70(CArrRef params);
static Variant invoke_case_159(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x316484E3C9CEB29FLL, __lambda_31);
  HASH_INVOKE(0x11D5E8479FACFA9FLL, __lambda_70);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_32(CArrRef params);
Variant i___lambda_90(CArrRef params);
static Variant invoke_case_163(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x4EE0332517459BA3LL, __lambda_32);
  HASH_INVOKE(0x073B16CC60987DA3LL, __lambda_90);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_62(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_23(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_100(CArrRef params);
Variant i___lambda_107(CArrRef params);
Variant i___lambda_130(CArrRef params);
static Variant invoke_case_175(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x2F77FDD4563683AFLL, __lambda_100);
  HASH_INVOKE(0x70FE71FFBA77C4AFLL, __lambda_107);
  HASH_INVOKE(0x4B65A1BFE3B4C0AFLL, __lambda_130);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_29(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_46(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_136(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_122(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_24(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_67(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_47(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_120(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_142(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_119(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_121(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_97(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_81(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_133(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_39(CArrRef params);
Variant i___lambda_68(CArrRef params);
Variant i___lambda_93(CArrRef params);
static Variant invoke_case_214(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x280F32B821B180D6LL, __lambda_39);
  HASH_INVOKE(0x3E11E2ADCAE4C2D6LL, __lambda_68);
  HASH_INVOKE(0x2A17E64ED70FC0D6LL, __lambda_93);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_72(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_74(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_22(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_108(CArrRef params);
Variant i___lambda_88(CArrRef params);
static Variant invoke_case_225(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x596D03DA399D7BE1LL, __lambda_108);
  HASH_INVOKE(0x3FB9F937C9E87BE1LL, __lambda_88);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_30(CArrRef params);
Variant i___lambda_101(CArrRef params);
Variant i___lambda_143(CArrRef params);
static Variant invoke_case_229(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x2C14211E404110E5LL, __lambda_30);
  HASH_INVOKE(0x17E68FAED55DAEE5LL, __lambda_101);
  HASH_INVOKE(0x3BA4918CA47B64E5LL, __lambda_143);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_84(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_21(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_28(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_45(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_41(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_51(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_92(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_37(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_111(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_129(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i___lambda_50(CArrRef params);
Variant i___lambda_126(CArrRef params);
static Variant invoke_case_250(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x43A83BEB9BC2DBFALL, __lambda_50);
  HASH_INVOKE(0x4185DBAF138A8BFALL, __lambda_126);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_59(CArrRef params);
Variant i___lambda_117(CArrRef params);
static Variant invoke_case_252(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x38A2AE705F8245FCLL, __lambda_59);
  HASH_INVOKE(0x70D871CB1D2F6FFCLL, __lambda_117);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_63(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[256])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 256; i++) funcTable[i] = &invoke_builtin;
    funcTable[0] = &i___lambda_61;
    funcTable[8] = &invoke_case_8;
    funcTable[12] = &i___lambda_69;
    funcTable[13] = &i___lambda_65;
    funcTable[14] = &invoke_case_14;
    funcTable[18] = &invoke_case_18;
    funcTable[20] = &i___lambda_40;
    funcTable[22] = &i___lambda_35;
    funcTable[23] = &i___lambda_109;
    funcTable[25] = &invoke_case_25;
    funcTable[26] = &i___lambda_58;
    funcTable[28] = &i___lambda_118;
    funcTable[31] = &i___lambda_140;
    funcTable[34] = &i___lambda_103;
    funcTable[35] = &invoke_case_35;
    funcTable[37] = &invoke_case_37;
    funcTable[43] = &invoke_case_43;
    funcTable[45] = &i___lambda_80;
    funcTable[48] = &invoke_case_48;
    funcTable[49] = &i___lambda_82;
    funcTable[50] = &i___lambda_98;
    funcTable[51] = &i___lambda_134;
    funcTable[52] = &i___lambda_73;
    funcTable[53] = &i___lambda_83;
    funcTable[55] = &i___lambda_87;
    funcTable[58] = &i___lambda_26;
    funcTable[64] = &i___lambda_125;
    funcTable[65] = &invoke_case_65;
    funcTable[67] = &i___lambda_105;
    funcTable[68] = &i___lambda_110;
    funcTable[69] = &invoke_case_69;
    funcTable[74] = &i___lambda_64;
    funcTable[76] = &i___lambda_36;
    funcTable[79] = &i___lambda_102;
    funcTable[86] = &i___lambda_53;
    funcTable[88] = &i___lambda_132;
    funcTable[90] = &invoke_case_90;
    funcTable[95] = &i___lambda_99;
    funcTable[100] = &i___lambda_128;
    funcTable[104] = &i___lambda_38;
    funcTable[113] = &i___lambda_48;
    funcTable[116] = &i___lambda_34;
    funcTable[118] = &invoke_case_118;
    funcTable[122] = &invoke_case_122;
    funcTable[123] = &i___lambda_49;
    funcTable[124] = &i___lambda_91;
    funcTable[131] = &i___lambda_114;
    funcTable[133] = &invoke_case_133;
    funcTable[134] = &i___lambda_55;
    funcTable[137] = &i___lambda_54;
    funcTable[139] = &i___lambda_52;
    funcTable[144] = &i___lambda_95;
    funcTable[145] = &i___lambda_79;
    funcTable[147] = &invoke_case_147;
    funcTable[150] = &i___lambda_77;
    funcTable[154] = &i___lambda_131;
    funcTable[158] = &i___lambda_135;
    funcTable[159] = &invoke_case_159;
    funcTable[163] = &invoke_case_163;
    funcTable[165] = &i___lambda_62;
    funcTable[169] = &i___lambda_23;
    funcTable[175] = &invoke_case_175;
    funcTable[178] = &i___lambda_29;
    funcTable[188] = &i___lambda_46;
    funcTable[193] = &i___lambda_136;
    funcTable[196] = &i___lambda_122;
    funcTable[197] = &i___lambda_24;
    funcTable[198] = &i___lambda_67;
    funcTable[200] = &i___lambda_47;
    funcTable[202] = &i___lambda_120;
    funcTable[204] = &i___lambda_142;
    funcTable[206] = &i___lambda_119;
    funcTable[207] = &i___lambda_121;
    funcTable[209] = &i___lambda_97;
    funcTable[212] = &i___lambda_81;
    funcTable[213] = &i___lambda_133;
    funcTable[214] = &invoke_case_214;
    funcTable[218] = &i___lambda_72;
    funcTable[220] = &i___lambda_74;
    funcTable[221] = &i___lambda_22;
    funcTable[225] = &invoke_case_225;
    funcTable[229] = &invoke_case_229;
    funcTable[231] = &i___lambda_84;
    funcTable[234] = &i___lambda_21;
    funcTable[237] = &i___lambda_28;
    funcTable[239] = &i___lambda_45;
    funcTable[241] = &i___lambda_41;
    funcTable[242] = &i___lambda_51;
    funcTable[243] = &i___lambda_92;
    funcTable[244] = &i___lambda_37;
    funcTable[245] = &i___lambda_111;
    funcTable[248] = &i___lambda_129;
    funcTable[250] = &invoke_case_250;
    funcTable[252] = &invoke_case_252;
    funcTable[255] = &i___lambda_63;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 255](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
