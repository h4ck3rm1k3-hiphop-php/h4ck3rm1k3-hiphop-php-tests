
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 5 */
void f___lambda_1(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_1);
  v_context++;
  (v_state = "code");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 8 */
void f___lambda_2(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_2);
  v_context--;
  if (not_more(v_context, 0LL)) (v_state = "rule");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 11 */
void f___lambda_3(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_3);
  (v_text = LINE(12,x_substr(toString(v_text), toInt32(1LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 14 */
void f___lambda_4(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_4);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  (v_text = LINE(15,(assignCallTemp(eo_0, toString(v_text)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - 2LL)),x_substr(eo_0, toInt32(1LL), eo_2))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 17 */
void f___lambda_5(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_5);
  (v_state = "rule");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 20 */
void f___lambda_6(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_6);
  (v_state = "regex");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 23 */
void f___lambda_7(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_7);
  if (equal(v_text, "ignore")) (v_type = "ignore");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 26 */
void f___lambda_8(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_8);
  v_context--;
  if (not_more(v_context, 0LL)) (v_state = "INITIAL");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 29 */
Object f___lambda_9(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_9);
  return LINE(30,create_object("preg_scanner_definition", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 32 */
Variant f___lambda_10(Sequence v_tokens) {
  FUNCTION_INJECTION(__lambda_10);
  LINE(33,v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_invoke_few_args("add_common_rule", 0x28DE6112ACE64A48LL, 1, v_tokens.refvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 35 */
Variant f___lambda_11(Sequence v_tokens) {
  FUNCTION_INJECTION(__lambda_11);
  LINE(36,v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_invoke_few_args("add_rule_list", 0x0D64410D238CFFBELL, 2, lval(v_tokens.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).refvalAt(0LL, 0x77CFA1EEF01BCA90LL), lval(v_tokens.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).refvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 38 */
Variant f___lambda_12(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_12);
  return v_tokens;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 41 */
Array f___lambda_13(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_13);
  return Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 44 */
Array f___lambda_14(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_14);
  return Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 62 */
Variant f___lambda_20(Sequence v_tokens) {
  FUNCTION_INJECTION(__lambda_20);
  return LINE(63,invoke_failed("mk_action", Array(ArrayInit(1).set(0, ref(v_tokens.refvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()), 0x00000000D3646393LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 47 */
Variant f___lambda_15(Sequence v_tokens) {
  FUNCTION_INJECTION(__lambda_15);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  return LINE(48,(assignCallTemp(eo_0, ref(throw_fatal("unknown class metascanner", ((void*)NULL)))),assignCallTemp(eo_1, ref(v_tokens.refvalAt(2LL, 0x486AFCC090D5F98CLL))),assignCallTemp(eo_2, ref(v_tokens.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))),assignCallTemp(eo_3, ref(v_tokens.refvalAt(4LL, 0x6F2A25235E544A31LL))),invoke_failed("preg_pattern", Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()), 0x000000007E8888C9LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 65 */
void f___lambda_21(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_21);
  (v_state = "text");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 50 */
String f___lambda_16(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_16);
  return LINE(51,x_implode("", v_tokens));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 68 */
void f___lambda_22(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_22);
  (v_text = LINE(69,x_hexdec(toString(v_text))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 53 */
int64 f___lambda_17(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_17);
  return 0LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 71 */
void f___lambda_23(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_23);
  (v_text = v_text - 0LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 56 */
int64 f___lambda_18(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_18);
  return 1LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 74 */
void f___lambda_24(Variant v_type, Variant v_text, CVarRef v_match, Variant v_state, Variant v_context) {
  FUNCTION_INJECTION(__lambda_24);
  (v_state = "INITIAL");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 59 */
String f___lambda_19(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_19);
  return "";
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 92 */
Array f___lambda_30(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_30);
  return Array(ArrayInit(3).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).set(2, v_tokens.rvalAt(6LL, 0x26BF47194D7E8E12LL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 77 */
Object f___lambda_25(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_25);
  return LINE(78,create_object("js_program", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 95 */
Object f___lambda_31(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_31);
  return LINE(96,create_object("js_function_definition", Array(ArrayInit(1).set(0, Array(ArrayInit(3).set(0, "").set(1, ScalarArrays::sa_[0]).set(2, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).create())).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 80 */
Object f___lambda_26(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_26);
  return LINE(81,create_object("js_source", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 98 */
Object f___lambda_32(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_32);
  return LINE(99,create_object("js_function_definition", Array(ArrayInit(1).set(0, Array(ArrayInit(3).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, ScalarArrays::sa_[0]).set(2, v_tokens.rvalAt(5LL, 0x350AEB726A15D700LL)).create())).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 83 */
Variant f___lambda_27(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_27);
  LINE(84,v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_invoke_few_args("addStatement", 0x706B149686981DFELL, 1, v_tokens.refvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 101 */
Object f___lambda_33(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_33);
  return LINE(102,create_object("js_function_definition", Array(ArrayInit(1).set(0, Array(ArrayInit(3).set(0, "").set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(2, v_tokens.rvalAt(5LL, 0x350AEB726A15D700LL)).create())).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 86 */
Variant f___lambda_28(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_28);
  LINE(87,v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_invoke_few_args("addFunction", 0x48B5852A397D2D9FLL, 1, create_object("js_function_definition", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create()))));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 104 */
Object f___lambda_34(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_34);
  return LINE(105,create_object("js_function_definition", Array(ArrayInit(1).set(0, Array(ArrayInit(3).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).set(2, v_tokens.rvalAt(6LL, 0x26BF47194D7E8E12LL)).create())).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 89 */
Array f___lambda_29(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_29);
  return Array(ArrayInit(3).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, ScalarArrays::sa_[0]).set(2, v_tokens.rvalAt(5LL, 0x350AEB726A15D700LL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 122 */
Object f___lambda_40(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_40);
  return LINE(123,create_object("js_print", Array(ArrayInit(1).set(0, create_object("js_literal_string", Array(ArrayInit(2).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, 0LL).create()))).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 107 */
Array f___lambda_35(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_35);
  return Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 125 */
Object f___lambda_41(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_41);
  Variant eo_0;
  Variant eo_1;
  return LINE(127,create_object("js_print", Array(ArrayInit(1).set(0, (assignCallTemp(eo_0, ref(create_object("js_literal_string", Array(ArrayInit(2).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, 0LL).create())))),assignCallTemp(eo_1, ref(v_tokens.refvalAt(3LL, 0x135FDDF6A6BFBBDDLL))),invoke_failed("js_plus", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x000000000848337ELL))).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 110 */
Variant f___lambda_36(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_36);
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 130 */
Object f___lambda_42(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_42);
  return LINE(131,create_object("js_nop", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 113 */
Variant f___lambda_37(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_37);
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 133 */
Object f___lambda_43(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_43);
  return LINE(134,create_object("js_block", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 116 */
Object f___lambda_38(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_38);
  return LINE(117,create_object("js_nop", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 136 */
Array f___lambda_44(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_44);
  return Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 119 */
Object f___lambda_39(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_39);
  return LINE(120,create_object("js_print", Array(ArrayInit(1).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 156 */
Object f___lambda_50(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_50);
  return LINE(157,create_object("js_nop", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 139 */
Variant f___lambda_45(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_45);
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 159 */
Object f___lambda_51(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_51);
  return LINE(160,create_object("js_statement", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 142 */
Object f___lambda_46(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_46);
  return LINE(144,create_object("js_var", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 162 */
Object f___lambda_52(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_52);
  return LINE(164,create_object("js_if", Array(ArrayInit(3).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).set(2, v_tokens.rvalAt(6LL, 0x26BF47194D7E8E12LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 147 */
Array f___lambda_47(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_47);
  return Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 167 */
Object f___lambda_53(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_53);
  return LINE(169,create_object("js_if", Array(ArrayInit(2).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 150 */
Variant f___lambda_48(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_48);
  return (silenceInc(), silenceDec(Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 172 */
Object f___lambda_54(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_54);
  return LINE(173,create_object("js_do", Array(ArrayInit(2).set(0, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).set(1, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 153 */
Variant f___lambda_49(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_49);
  return v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 208 */
Variant f___lambda_60(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_60);
  return (silenceInc(), silenceDec(LINE(209,create_object("js_continue", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 175 */
Object f___lambda_55(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_55);
  return LINE(176,create_object("js_while", Array(ArrayInit(2).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 211 */
Variant f___lambda_61(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_61);
  return (silenceInc(), silenceDec(LINE(212,create_object("js_break", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 178 */
Object f___lambda_56(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_56);
  return LINE(179,create_object("js_for_in", Array(ArrayInit(3).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).set(2, v_tokens.rvalAt(6LL, 0x26BF47194D7E8E12LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 336 */
Object f___lambda_100(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_100);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return LINE(337,(assignCallTemp(eo_0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)),assignCallTemp(eo_1, create_object("js_identifier", Array(ArrayInit(1).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create()))),create_object("js_accessor", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, 0LL).create()))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 214 */
Variant f___lambda_62(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_62);
  return (silenceInc(), silenceDec(LINE(215,create_object("js_return", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 181 */
Object f___lambda_57(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_57);
  int64 v_k = 0;
  Variant v_a1;
  Variant v_a2;
  Variant v_a3;
  Variant v_a4;

  (v_k = 2LL);
  if (equal(v_tokens.rvalAt(v_k), ";")) {
    (v_a1 = LINE(184,create_object("js_nop", Array())));
    v_k++;
  }
  else {
    (v_a1 = v_tokens.rvalAt(v_k));
    v_k += 2LL;
  }
  if (equal(v_tokens.rvalAt(v_k), ";")) {
    (v_a2 = LINE(186,create_object("js_nop", Array())));
    v_k++;
  }
  else {
    (v_a2 = v_tokens.rvalAt(v_k));
    v_k += 2LL;
  }
  if (equal(v_tokens.rvalAt(v_k), ")")) {
    (v_a3 = LINE(188,create_object("js_nop", Array())));
    v_k++;
  }
  else {
    (v_a3 = v_tokens.rvalAt(v_k));
    v_k += 2LL;
  }
  (v_a4 = v_tokens.rvalAt(v_k));
  return LINE(191,create_object("js_for", Array(ArrayInit(4).set(0, v_a1).set(1, v_a2).set(2, v_a3).set(3, v_a4).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 339 */
Object f___lambda_101(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_101);
  return LINE(340,create_object("js_new", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 217 */
Object f___lambda_63(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_63);
  return LINE(218,create_object("js_with", Array(ArrayInit(2).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 194 */
Object f___lambda_58(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_58);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  int64 v_k = 0;
  Variant v_a2;
  Variant v_a3;
  Variant v_a4;

  (v_k = 5LL);
  if (equal(v_tokens.rvalAt(v_k), ";")) {
    (v_a2 = LINE(197,create_object("js_nop", Array())));
    v_k++;
  }
  else {
    (v_a2 = v_tokens.rvalAt(v_k));
    v_k += 2LL;
  }
  if (equal(v_tokens.rvalAt(v_k), ")")) {
    (v_a3 = LINE(199,create_object("js_nop", Array())));
    v_k++;
  }
  else {
    (v_a3 = v_tokens.rvalAt(v_k));
    v_k += 2LL;
  }
  (v_a4 = v_tokens.rvalAt(v_k));
  return LINE(202,(assignCallTemp(eo_0, create_object("js_var", Array(ArrayInit(1).set(0, v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).create()))),assignCallTemp(eo_1, v_a2),assignCallTemp(eo_2, v_a3),assignCallTemp(eo_3, v_a4),create_object("js_for", Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 342 */
Object f___lambda_102(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_102);
  return LINE(343,create_object("js_call", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 220 */
Object f___lambda_64(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_64);
  return LINE(222,create_object("js_switch", Array(ArrayInit(2).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 205 */
Object f___lambda_59(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_59);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return LINE(206,(assignCallTemp(eo_0, create_object("js_var", Array(ArrayInit(1).set(0, v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).create()))),assignCallTemp(eo_1, v_tokens.rvalAt(5LL, 0x350AEB726A15D700LL)),assignCallTemp(eo_2, v_tokens.rvalAt(7LL, 0x7D75B33B7AEB669DLL)),create_object("js_for_in", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, eo_2).create()))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 345 */
Array f___lambda_103(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_103);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 246 */
Object f___lambda_70(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_70);
  return LINE(247,create_object("js_case", Array(ArrayInit(2).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 225 */
Variant f___lambda_65(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_65);
  if (equal(v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "}")) return ScalarArrays::sa_[0];
  else return v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 348 */
Array f___lambda_104(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_104);
  return toArray(v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 249 */
Array f___lambda_71(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_71);
  return Array(ArrayInit(1).set(0, LINE(250,create_object("js_case", Array(ArrayInit(2).set(0, 0LL).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())))).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 228 */
Variant f___lambda_66(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_66);
  return v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 366 */
Object f___lambda_110(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_110);
  return LINE(367,create_object("js_typeof", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 351 */
Variant f___lambda_105(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_105);
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 252 */
Object f___lambda_72(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_72);
  return LINE(253,create_object("js_label", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 233 */
Variant f___lambda_67(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_67);
  return LINE(235,x_array_merge(2, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL), Array(ArrayInit(1).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 369 */
Object f___lambda_111(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_111);
  return LINE(370,create_object("js_pre_pp", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 354 */
Object f___lambda_106(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_106);
  return LINE(355,create_object("js_post_pp", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 255 */
Object f___lambda_73(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_73);
  return LINE(256,create_object("js_throw", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 238 */
Variant f___lambda_68(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_68);
  return LINE(240,x_array_merge(3, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL), Array(ArrayInit(2).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 372 */
Object f___lambda_112(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_112);
  return LINE(373,create_object("js_pre_mm", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 357 */
Object f___lambda_107(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_107);
  return LINE(358,create_object("js_post_mm", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 258 */
Object f___lambda_74(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_74);
  return LINE(259,create_object("js_try", Array(ArrayInit(3).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(2, null).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 243 */
Variant f___lambda_69(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_69);
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 375 */
Object f___lambda_113(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_113);
  return LINE(376,create_object("js_u_plus", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 360 */
Object f___lambda_108(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_108);
  return LINE(361,create_object("js_delete", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 276 */
Object f___lambda_80(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_80);
  return LINE(277,create_object("js_identifier", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 261 */
Object f___lambda_75(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_75);
  return LINE(262,create_object("js_try", Array(ArrayInit(3).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, null).set(2, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 378 */
Object f___lambda_114(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_114);
  return LINE(379,create_object("js_u_minus", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 363 */
Object f___lambda_109(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_109);
  return LINE(364,create_object("js_void", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 279 */
Variant f___lambda_81(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_81);
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 264 */
Object f___lambda_76(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_76);
  return LINE(265,create_object("js_try", Array(ArrayInit(3).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(2, v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 396 */
Object f___lambda_120(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_120);
  return LINE(397,create_object("js_plus", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 381 */
Object f___lambda_115(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_115);
  return LINE(382,create_object("js_bit_not", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 282 */
Object f___lambda_82(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_82);
  return LINE(283,create_object("js_literal_null", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 267 */
Object f___lambda_77(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_77);
  return LINE(268,create_object("js_catch", Array(ArrayInit(2).set(0, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(1, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 399 */
Object f___lambda_121(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_121);
  return LINE(400,create_object("js_minus", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 384 */
Object f___lambda_116(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_116);
  return LINE(385,create_object("js_not", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 285 */
Object f___lambda_83(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_83);
  return LINE(286,create_object("js_literal_boolean", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 270 */
Variant f___lambda_78(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_78);
  return v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 402 */
Object f___lambda_122(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_122);
  return LINE(403,create_object("js_lsh", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 387 */
Object f___lambda_117(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_117);
  return LINE(388,create_object("js_multiply", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 288 */
Object f___lambda_84(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_84);
  return LINE(289,create_object("js_literal_number", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 273 */
Object f___lambda_79(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_79);
  return LINE(274,create_object("js_this", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 405 */
Object f___lambda_123(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_123);
  return LINE(406,create_object("js_rsh", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 390 */
Object f___lambda_118(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_118);
  return LINE(391,create_object("js_divide", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 306 */
Object f___lambda_90(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_90);
  return LINE(307,create_object("js_literal_null", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 291 */
bool f___lambda_85(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_85);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 408 */
Object f___lambda_124(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_124);
  return LINE(409,create_object("js_ursh", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 393 */
Object f___lambda_119(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_119);
  return LINE(394,create_object("js_modulo", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 309 */
Object f___lambda_91(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_91);
  return LINE(310,create_object("js_literal_object", Array()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 294 */
bool f___lambda_86(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_86);
  return false;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 426 */
Object f___lambda_130(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_130);
  return LINE(427,create_object("js_instanceof", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 411 */
Variant f___lambda_125(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_125);
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 312 */
Object f___lambda_92(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_92);
  return LINE(313,create_object("js_literal_object", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 297 */
Object f___lambda_87(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_87);
  return LINE(298,create_object("js_literal_string", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 429 */
Object f___lambda_131(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_131);
  return LINE(430,create_object("js_in", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 414 */
Object f___lambda_126(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_126);
  return LINE(415,create_object("js_lt", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 315 */
Array f___lambda_93(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_93);
  return Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 300 */
Object f___lambda_88(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_88);
  return LINE(301,create_object("js_literal_array", Array(ArrayInit(1).set(0, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 432 */
Object f___lambda_132(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_132);
  return LINE(433,create_object("js_equal", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 417 */
Object f___lambda_127(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_127);
  return LINE(418,create_object("js_gt", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 318 */
Array f___lambda_94(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_94);
  Variant eo_0;
  Variant eo_1;
  Array v_p;

  (v_p = LINE(319,x_explode(":", toString(v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  return (assignCallTemp(eo_0, create_object("js_literal_string", Array(ArrayInit(2).set(0, v_p.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, 0LL).create()))),assignCallTemp(eo_1, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)),Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 303 */
Variant f___lambda_89(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_89);
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 435 */
Object f___lambda_133(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_133);
  return LINE(436,create_object("js_not_equal", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 420 */
Object f___lambda_128(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_128);
  return LINE(421,create_object("js_lte", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 321 */
Variant f___lambda_95(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_95);
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 438 */
Object f___lambda_134(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_134);
  return LINE(439,create_object("js_strict_equal", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 423 */
Object f___lambda_129(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_129);
  return LINE(424,create_object("js_gte", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 324 */
Variant f___lambda_96(Variant v_tokens) {
  FUNCTION_INJECTION(__lambda_96);
  Array v_p;

  (v_p = LINE(325,x_explode(":", toString(v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((create_object("js_literal_string", Array(ArrayInit(2).set(0, v_p.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, 0LL).create()))));
  lval(v_tokens.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).append((v_tokens.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
  return v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 456 */
Object f___lambda_140(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_140);
  return LINE(457,create_object("js_or", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 441 */
Object f___lambda_135(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_135);
  return LINE(442,create_object("js_strict_not_equal", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 327 */
Object f___lambda_97(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_97);
  return LINE(328,create_object("js_literal_string", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, 0LL).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 459 */
Object f___lambda_141(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_141);
  return LINE(460,create_object("js_ternary", Array(ArrayInit(3).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(2, v_tokens.rvalAt(4LL, 0x6F2A25235E544A31LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 444 */
Object f___lambda_136(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_136);
  return LINE(445,create_object("js_bit_and", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 330 */
Object f___lambda_98(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_98);
  return LINE(331,create_object("js_literal_number", Array(ArrayInit(1).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 462 */
Object f___lambda_142(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_142);
  return LINE(463,create_object("js_assign", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 447 */
Object f___lambda_137(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_137);
  return LINE(448,create_object("js_bit_xor", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 333 */
Object f___lambda_99(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_99);
  return LINE(334,create_object("js_accessor", Array(ArrayInit(3).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(2, 1LL).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 465 */
Object f___lambda_143(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_143);
  return LINE(466,create_object("js_compound_assign", Array(ArrayInit(3).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).set(2, v_tokens.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 450 */
Object f___lambda_138(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_138);
  return LINE(451,create_object("js_bit_or", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 468 */
Object f___lambda_144(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_144);
  return LINE(469,create_object("js_comma", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php line 453 */
Object f___lambda_139(CVarRef v_tokens) {
  FUNCTION_INJECTION(__lambda_139);
  return LINE(454,create_object("js_and", Array(ArrayInit(2).set(0, v_tokens.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1, v_tokens.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
} /* function */
Variant i___lambda_21(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x04FF94BA322D1BEALL, __lambda_21) {
    return (f___lambda_21(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), params.rvalAt(2), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_22(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x53A1C21FC44868DDLL, __lambda_22) {
    return (f___lambda_22(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), params.rvalAt(2), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_23(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4942840BA5B59FA9LL, __lambda_23) {
    return (f___lambda_23(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), params.rvalAt(2), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_24(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x791F064D8DA911C5LL, __lambda_24) {
    return (f___lambda_24(ref(const_cast<Array&>(params).lvalAt(0)), ref(const_cast<Array&>(params).lvalAt(1)), params.rvalAt(2), ref(const_cast<Array&>(params).lvalAt(3)), ref(const_cast<Array&>(params).lvalAt(4))), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_30(CArrRef params) {
  return (f___lambda_30(params.rvalAt(0)));
}
Variant i___lambda_25(CArrRef params) {
  return (f___lambda_25(params.rvalAt(0)));
}
Variant i___lambda_31(CArrRef params) {
  return (f___lambda_31(params.rvalAt(0)));
}
Variant i___lambda_26(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x025DDFC7859F693ALL, __lambda_26) {
    return (f___lambda_26(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_32(CArrRef params) {
  return (f___lambda_32(params.rvalAt(0)));
}
Variant i___lambda_27(CArrRef params) {
  return (f___lambda_27(params.rvalAt(0)));
}
Variant i___lambda_33(CArrRef params) {
  return (f___lambda_33(params.rvalAt(0)));
}
Variant i___lambda_28(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3032039A3455D8EDLL, __lambda_28) {
    return (f___lambda_28(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_34(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x50EFC4B1145E8974LL, __lambda_34) {
    return (f___lambda_34(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_29(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x25D2C58615BFBDB2LL, __lambda_29) {
    return (f___lambda_29(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_40(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2C76C056EDE38D14LL, __lambda_40) {
    return (f___lambda_40(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_35(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3A778DAD6BB6F216LL, __lambda_35) {
    return (f___lambda_35(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_41(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3861D058AC768AF1LL, __lambda_41) {
    return (f___lambda_41(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_36(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7ABF58EC90EDC04CLL, __lambda_36) {
    return (f___lambda_36(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_42(CArrRef params) {
  return (f___lambda_42(params.rvalAt(0)));
}
Variant i___lambda_37(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4B65CAAC9C083DF4LL, __lambda_37) {
    return (f___lambda_37(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_43(CArrRef params) {
  return (f___lambda_43(params.rvalAt(0)));
}
Variant i___lambda_38(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7049778973D5D868LL, __lambda_38) {
    return (f___lambda_38(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_44(CArrRef params) {
  return (f___lambda_44(params.rvalAt(0)));
}
Variant i___lambda_39(CArrRef params) {
  return (f___lambda_39(params.rvalAt(0)));
}
Variant i___lambda_50(CArrRef params) {
  return (f___lambda_50(params.rvalAt(0)));
}
Variant i___lambda_45(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3B9D48361F09ADEFLL, __lambda_45) {
    return (f___lambda_45(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_51(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5302A867FAAB50F2LL, __lambda_51) {
    return (f___lambda_51(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_46(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x44711EDCE26164BCLL, __lambda_46) {
    return (f___lambda_46(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_52(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3ADDCF790975D08BLL, __lambda_52) {
    return (f___lambda_52(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_47(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1A318C83322F65C8LL, __lambda_47) {
    return (f___lambda_47(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_53(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x45B5EA7A45CCE456LL, __lambda_53) {
    return (f___lambda_53(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_48(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x57E51D1E217D1071LL, __lambda_48) {
    return (f___lambda_48(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_54(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7638E7BD1FDE8C89LL, __lambda_54) {
    return (f___lambda_54(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_49(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2747C2EB548E797BLL, __lambda_49) {
    return (f___lambda_49(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_60(CArrRef params) {
  return (f___lambda_60(params.rvalAt(0)));
}
Variant i___lambda_55(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1F48192AD03ABC86LL, __lambda_55) {
    return (f___lambda_55(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_61(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1A9E688E3A645D00LL, __lambda_61) {
    return (f___lambda_61(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_56(CArrRef params) {
  return (f___lambda_56(params.rvalAt(0)));
}
Variant i___lambda_100(CArrRef params) {
  return (f___lambda_100(params.rvalAt(0)));
}
Variant i___lambda_62(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2757E0AB57D3E8A5LL, __lambda_62) {
    return (f___lambda_62(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_57(CArrRef params) {
  return (f___lambda_57(params.rvalAt(0)));
}
Variant i___lambda_101(CArrRef params) {
  return (f___lambda_101(params.rvalAt(0)));
}
Variant i___lambda_63(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x65D467B4D99DE0FFLL, __lambda_63) {
    return (f___lambda_63(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_58(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x01B88A2936A0241ALL, __lambda_58) {
    return (f___lambda_58(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_102(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x26F52C06000CB44FLL, __lambda_102) {
    return (f___lambda_102(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_64(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3C4C4F0B6429EB4ALL, __lambda_64) {
    return (f___lambda_64(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_59(CArrRef params) {
  return (f___lambda_59(params.rvalAt(0)));
}
Variant i___lambda_103(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x20D7FC4A4C128A22LL, __lambda_103) {
    return (f___lambda_103(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_70(CArrRef params) {
  return (f___lambda_70(params.rvalAt(0)));
}
Variant i___lambda_65(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x50290D02E5A35E0DLL, __lambda_65) {
    return (f___lambda_65(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_104(CArrRef params) {
  return (f___lambda_104(params.rvalAt(0)));
}
Variant i___lambda_71(CArrRef params) {
  return (f___lambda_71(params.rvalAt(0)));
}
Variant i___lambda_66(CArrRef params) {
  return (f___lambda_66(params.rvalAt(0)));
}
Variant i___lambda_110(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x49E49BED28347F44LL, __lambda_110) {
    return (f___lambda_110(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_105(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1136EA3B383A6243LL, __lambda_105) {
    return (f___lambda_105(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_72(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1BF058A91C9CDADALL, __lambda_72) {
    return (f___lambda_72(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_67(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x421832A67037AAC6LL, __lambda_67) {
    return (f___lambda_67(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_111(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x71D9951A457F31F5LL, __lambda_111) {
    return (f___lambda_111(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_106(CArrRef params) {
  return (f___lambda_106(params.rvalAt(0)));
}
Variant i___lambda_73(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3A1E0F3006EB2D34LL, __lambda_73) {
    return (f___lambda_73(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_68(CArrRef params) {
  return (f___lambda_68(params.rvalAt(0)));
}
Variant i___lambda_112(CArrRef params) {
  return (f___lambda_112(params.rvalAt(0)));
}
Variant i___lambda_107(CArrRef params) {
  return (f___lambda_107(params.rvalAt(0)));
}
Variant i___lambda_74(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7BE5D57CDECE3ADCLL, __lambda_74) {
    return (f___lambda_74(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_69(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1E796983846A6D0CLL, __lambda_69) {
    return (f___lambda_69(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_113(CArrRef params) {
  return (f___lambda_113(params.rvalAt(0)));
}
Variant i___lambda_108(CArrRef params) {
  return (f___lambda_108(params.rvalAt(0)));
}
Variant i___lambda_80(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x277EF1629B57CF2DLL, __lambda_80) {
    return (f___lambda_80(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_75(CArrRef params) {
  return (f___lambda_75(params.rvalAt(0)));
}
Variant i___lambda_114(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x15681B5D74F9EC83LL, __lambda_114) {
    return (f___lambda_114(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_109(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7C9A5DBF792E4917LL, __lambda_109) {
    return (f___lambda_109(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_81(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1AC4ADA913DEE5D4LL, __lambda_81) {
    return (f___lambda_81(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_76(CArrRef params) {
  return (f___lambda_76(params.rvalAt(0)));
}
Variant i___lambda_120(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x54B0E9B4C52F94CALL, __lambda_120) {
    return (f___lambda_120(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_115(CArrRef params) {
  return (f___lambda_115(params.rvalAt(0)));
}
Variant i___lambda_82(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3EE5139C118B9C31LL, __lambda_82) {
    return (f___lambda_82(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_77(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x12B6DA99C2E06F96LL, __lambda_77) {
    return (f___lambda_77(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_121(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6F4E035C7DF039CFLL, __lambda_121) {
    return (f___lambda_121(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_116(CArrRef params) {
  return (f___lambda_116(params.rvalAt(0)));
}
Variant i___lambda_83(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3A80AE68B48D9335LL, __lambda_83) {
    return (f___lambda_83(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_78(CArrRef params) {
  return (f___lambda_78(params.rvalAt(0)));
}
Variant i___lambda_122(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x327FFA586DCA87C4LL, __lambda_122) {
    return (f___lambda_122(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_117(CArrRef params) {
  return (f___lambda_117(params.rvalAt(0)));
}
Variant i___lambda_84(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6E52635C2FE442E7LL, __lambda_84) {
    return (f___lambda_84(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_79(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1969626AE19BDF91LL, __lambda_79) {
    return (f___lambda_79(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_123(CArrRef params) {
  return (f___lambda_123(params.rvalAt(0)));
}
Variant i___lambda_118(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5D344D9E0182551CLL, __lambda_118) {
    return (f___lambda_118(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_90(CArrRef params) {
  return (f___lambda_90(params.rvalAt(0)));
}
Variant i___lambda_85(CArrRef params) {
  return (f___lambda_85(params.rvalAt(0)));
}
Variant i___lambda_124(CArrRef params) {
  return (f___lambda_124(params.rvalAt(0)));
}
Variant i___lambda_119(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x679FF3B00A9128CELL, __lambda_119) {
    return (f___lambda_119(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_91(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x491141DE10EF097CLL, __lambda_91) {
    return (f___lambda_91(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_86(CArrRef params) {
  return (f___lambda_86(params.rvalAt(0)));
}
Variant i___lambda_130(CArrRef params) {
  return (f___lambda_130(params.rvalAt(0)));
}
Variant i___lambda_125(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x371A359D0A7B7E40LL, __lambda_125) {
    return (f___lambda_125(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_92(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0F8FE93CCF79F2F3LL, __lambda_92) {
    return (f___lambda_92(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_87(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2F1068A00E1E1637LL, __lambda_87) {
    return (f___lambda_87(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_131(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x49EB6849515A3B9ALL, __lambda_131) {
    return (f___lambda_131(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_126(CArrRef params) {
  return (f___lambda_126(params.rvalAt(0)));
}
Variant i___lambda_93(CArrRef params) {
  return (f___lambda_93(params.rvalAt(0)));
}
Variant i___lambda_88(CArrRef params) {
  return (f___lambda_88(params.rvalAt(0)));
}
Variant i___lambda_132(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2228868E20CEDF58LL, __lambda_132) {
    return (f___lambda_132(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_127(CArrRef params) {
  return (f___lambda_127(params.rvalAt(0)));
}
Variant i___lambda_94(CArrRef params) {
  return (f___lambda_94(params.rvalAt(0)));
}
Variant i___lambda_89(CArrRef params) {
  return (f___lambda_89(params.rvalAt(0)));
}
Variant i___lambda_133(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6A927B51EFACFCD5LL, __lambda_133) {
    return (f___lambda_133(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_128(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x03D2A0B72E6E6E64LL, __lambda_128) {
    return (f___lambda_128(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_95(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4368BB8F3D6E0D90LL, __lambda_95) {
    return (f___lambda_95(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_134(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x64754B963BB33333LL, __lambda_134) {
    return (f___lambda_134(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_129(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6F295700A5A535F8LL, __lambda_129) {
    return (f___lambda_129(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_96(CArrRef params) {
  return (f___lambda_96(params.rvalAt(0)));
}
Variant i___lambda_140(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0F5655AC1F25281FLL, __lambda_140) {
    return (f___lambda_140(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_135(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x544E535AD57BE69ELL, __lambda_135) {
    return (f___lambda_135(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_97(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x130C95146B6878D1LL, __lambda_97) {
    return (f___lambda_97(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_141(CArrRef params) {
  return (f___lambda_141(params.rvalAt(0)));
}
Variant i___lambda_136(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1835F6A381C73CC1LL, __lambda_136) {
    return (f___lambda_136(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_98(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6772A8D08AF5E832LL, __lambda_98) {
    return (f___lambda_98(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_142(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x47044AB1CA668ACCLL, __lambda_142) {
    return (f___lambda_142(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_137(CArrRef params) {
  return (f___lambda_137(params.rvalAt(0)));
}
Variant i___lambda_99(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x71E7D4973DB2765FLL, __lambda_99) {
    return (f___lambda_99(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i___lambda_143(CArrRef params) {
  return (f___lambda_143(params.rvalAt(0)));
}
Variant i___lambda_138(CArrRef params) {
  return (f___lambda_138(params.rvalAt(0)));
}
Variant i___lambda_144(CArrRef params) {
  return (f___lambda_144(params.rvalAt(0)));
}
Variant i___lambda_139(CArrRef params) {
  return (f___lambda_139(params.rvalAt(0)));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_ly_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.ly.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_ly_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_lexp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lexp") : g->GV(lexp);
  Variant &v_dpa __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dpa") : g->GV(dpa);

  (v_lexp = ScalarArrays::sa_[1]);
  (v_dpa = ScalarArrays::sa_[2]);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
