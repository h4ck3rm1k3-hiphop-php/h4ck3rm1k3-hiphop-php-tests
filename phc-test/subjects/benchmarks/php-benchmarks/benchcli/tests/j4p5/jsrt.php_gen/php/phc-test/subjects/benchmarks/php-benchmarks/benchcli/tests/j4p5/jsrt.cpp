
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3, Variant &p4, Variant &p5, Variant &p6) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  p4 = a0.rvalAt(4);
  p5 = a0.rvalAt(5);
  p6 = a0.rvalAt(6);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1846 */
Variant c_js_math::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_math::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_math::o_get(ArrayElementVec &props) const {
  c_js_object::o_get(props);
}
bool c_js_math::o_exists(CStrRef s, int64 hash) const {
  return c_js_object::o_exists(s, hash);
}
Variant c_js_math::o_get(CStrRef s, int64 hash) {
  return c_js_object::o_get(s, hash);
}
Variant c_js_math::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_math::o_lval(CStrRef s, int64 hash) {
  return c_js_object::o_lval(s, hash);
}
Variant c_js_math::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_math)
ObjectData *c_js_math::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_js_math::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_js_math::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_js_math::cloneImpl() {
  c_js_math *obj = NEW(c_js_math)();
  cloneSet(obj);
  return obj;
}
void c_js_math::cloneSet(c_js_math *clone) {
  c_js_object::cloneSet(clone);
}
Variant c_js_math::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 1:
      HASH_GUARD(0x767806D6F1053C81LL, sin) {
        return (ti_sin(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      HASH_GUARD(0x724011CF7C31AE83LL, sqrt) {
        return (ti_sqrt(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 14:
      HASH_GUARD(0x62A4D7A03F7C3B8ELL, ceil) {
        return (ti_ceil(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x25FA64929C619391LL, asin) {
        return (ti_asin(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x5F7940A713863813LL, floor) {
        return (ti_floor(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x55757E1242390913LL, cos) {
        return (ti_cos(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7186EF5EF0581695LL, exp) {
        return (ti_exp(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 27:
      HASH_GUARD(0x47279C717370B41BLL, acos) {
        return (ti_acos(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 31:
      HASH_GUARD(0x566465036CCBCD1FLL, min) {
        int count = params.size();
        if (count <= 2) return (ti_min(o_getClassName(),count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_min(o_getClassName(),count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 33:
      HASH_GUARD(0x0DF945F12533F0A1LL, abs) {
        return (ti_abs(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x14279BB1A6872A21LL, atan2) {
        return (ti_atan2(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 52:
      HASH_GUARD(0x0B7D52E2540ABB34LL, tan) {
        return (ti_tan(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x681CD0E7D9DB71BFLL, pow) {
        return (ti_pow(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 67:
      HASH_GUARD(0x0433140BB339DDC3LL, log) {
        return (ti_log(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 71:
      HASH_GUARD(0x3D3C53C94AFD9647LL, random) {
        return (ti_random(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 80:
      HASH_GUARD(0x4DDB82A3632FA850LL, atan) {
        return (ti_atan(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 82:
      HASH_GUARD(0x6FE3C5FF5E883BD2LL, round) {
        return (ti_round(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 84:
      HASH_GUARD(0x53AEFD595C044754LL, max) {
        int count = params.size();
        if (count <= 2) return (ti_max(o_getClassName(),count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_max(o_getClassName(),count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_math::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 1:
      HASH_GUARD(0x767806D6F1053C81LL, sin) {
        return (ti_sin(o_getClassName(), a0));
      }
      break;
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      HASH_GUARD(0x724011CF7C31AE83LL, sqrt) {
        return (ti_sqrt(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 14:
      HASH_GUARD(0x62A4D7A03F7C3B8ELL, ceil) {
        return (ti_ceil(o_getClassName(), a0));
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x25FA64929C619391LL, asin) {
        return (ti_asin(o_getClassName(), a0));
      }
      break;
    case 19:
      HASH_GUARD(0x5F7940A713863813LL, floor) {
        return (ti_floor(o_getClassName(), a0));
      }
      HASH_GUARD(0x55757E1242390913LL, cos) {
        return (ti_cos(o_getClassName(), a0));
      }
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      HASH_GUARD(0x7186EF5EF0581695LL, exp) {
        return (ti_exp(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 27:
      HASH_GUARD(0x47279C717370B41BLL, acos) {
        return (ti_acos(o_getClassName(), a0));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 31:
      HASH_GUARD(0x566465036CCBCD1FLL, min) {
        if (count <= 2) return (ti_min(o_getClassName(),count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_min(o_getClassName(),count,a0, a1, params));
      }
      break;
    case 33:
      HASH_GUARD(0x0DF945F12533F0A1LL, abs) {
        return (ti_abs(o_getClassName(), a0));
      }
      HASH_GUARD(0x14279BB1A6872A21LL, atan2) {
        return (ti_atan2(o_getClassName(), a0, a1));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 52:
      HASH_GUARD(0x0B7D52E2540ABB34LL, tan) {
        return (ti_tan(o_getClassName(), a0));
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x681CD0E7D9DB71BFLL, pow) {
        return (ti_pow(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 67:
      HASH_GUARD(0x0433140BB339DDC3LL, log) {
        return (ti_log(o_getClassName(), a0));
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 71:
      HASH_GUARD(0x3D3C53C94AFD9647LL, random) {
        return (ti_random(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 80:
      HASH_GUARD(0x4DDB82A3632FA850LL, atan) {
        return (ti_atan(o_getClassName(), a0));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 82:
      HASH_GUARD(0x6FE3C5FF5E883BD2LL, round) {
        return (ti_round(o_getClassName(), a0));
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 84:
      HASH_GUARD(0x53AEFD595C044754LL, max) {
        if (count <= 2) return (ti_max(o_getClassName(),count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_max(o_getClassName(),count,a0, a1, params));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_math::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x767806D6F1053C81LL, sin) {
        return (ti_sin(c, params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0433140BB339DDC3LL, log) {
        return (ti_log(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x724011CF7C31AE83LL, sqrt) {
        return (ti_sqrt(c, params.rvalAt(0)));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      break;
    case 7:
      HASH_GUARD(0x3D3C53C94AFD9647LL, random) {
        return (ti_random(c));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 14:
      HASH_GUARD(0x62A4D7A03F7C3B8ELL, ceil) {
        return (ti_ceil(c, params.rvalAt(0)));
      }
      break;
    case 16:
      HASH_GUARD(0x4DDB82A3632FA850LL, atan) {
        return (ti_atan(c, params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x25FA64929C619391LL, asin) {
        return (ti_asin(c, params.rvalAt(0)));
      }
      break;
    case 18:
      HASH_GUARD(0x6FE3C5FF5E883BD2LL, round) {
        return (ti_round(c, params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x5F7940A713863813LL, floor) {
        return (ti_floor(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x55757E1242390913LL, cos) {
        return (ti_cos(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 20:
      HASH_GUARD(0x53AEFD595C044754LL, max) {
        int count = params.size();
        if (count <= 2) return (ti_max(c,count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_max(c,count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x7186EF5EF0581695LL, exp) {
        return (ti_exp(c, params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x47279C717370B41BLL, acos) {
        return (ti_acos(c, params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x566465036CCBCD1FLL, min) {
        int count = params.size();
        if (count <= 2) return (ti_min(c,count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_min(c,count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 33:
      HASH_GUARD(0x0DF945F12533F0A1LL, abs) {
        return (ti_abs(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x14279BB1A6872A21LL, atan2) {
        return (ti_atan2(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    case 52:
      HASH_GUARD(0x0B7D52E2540ABB34LL, tan) {
        return (ti_tan(c, params.rvalAt(0)));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      break;
    case 63:
      HASH_GUARD(0x681CD0E7D9DB71BFLL, pow) {
        return (ti_pow(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_math$os_get(const char *s) {
  return c_js_math::os_get(s, -1);
}
Variant &cw_js_math$os_lval(const char *s) {
  return c_js_math::os_lval(s, -1);
}
Variant cw_js_math$os_constant(const char *s) {
  return c_js_math::os_constant(s);
}
Variant cw_js_math$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_math::os_invoke(c, s, params, -1, fatal);
}
void c_js_math::init() {
  c_js_object::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1847 */
void c_js_math::t___construct() {
  INSTANCE_METHOD_INJECTION(js_math, js_math::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(1848,c_js_object::t___construct("Math"));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1853 */
Variant c_js_math::ti_abs(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::abs);
  return LINE(1854,f_js_int(x_abs(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1856 */
Variant c_js_math::ti_acos(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::acos);
  return LINE(1857,f_js_int(x_acos(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1859 */
Variant c_js_math::ti_asin(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::asin);
  return LINE(1860,f_js_int(x_asin(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1862 */
Variant c_js_math::ti_atan(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::atan);
  return LINE(1863,f_js_int(x_atan(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1865 */
Variant c_js_math::ti_atan2(const char* cls, CVarRef v_y, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::atan2);
  Variant eo_0;
  Variant eo_1;
  return LINE(1866,f_js_int((assignCallTemp(eo_0, toDouble(toObject(v_y)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))),assignCallTemp(eo_1, toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))),x_atan2(eo_0, eo_1))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1868 */
Variant c_js_math::ti_ceil(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::ceil);
  return LINE(1869,f_js_int(x_ceil(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1871 */
Variant c_js_math::ti_cos(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::cos);
  return LINE(1872,f_js_int(x_cos(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1874 */
Variant c_js_math::ti_exp(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::exp);
  return LINE(1875,f_js_int(x_exp(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1877 */
Variant c_js_math::ti_floor(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::floor);
  return LINE(1878,f_js_int(x_floor(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1880 */
Variant c_js_math::ti_log(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::log);
  return LINE(1881,f_js_int(x_log(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1883 */
Variant c_js_math::ti_max(const char* cls, int num_args, CVarRef v_v1, CVarRef v_v2, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_math, js_math::max);
  DECLARE_GLOBAL_VARIABLES(g);
  Array v_args;
  Array v_arr;
  Variant v_arg;
  Variant v_v;

  (v_args = LINE(1884,func_get_args(num_args, Array(ArrayInit(2).set(0, v_v1).set(1, v_v2).create()),args)));
  if (equal(LINE(1885,x_count(v_args)), 0LL)) return f_js_int(x_log(toDouble(0LL)));
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_args.begin("js_math"); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_arg = iter3.second();
      {
        (v_v = LINE(1888,v_arg.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
        if (LINE(1889,x_is_nan(toDouble(v_v)))) return g->s_jsrt_DupIdnan;
        v_arr.append((v_v));
      }
    }
  }
  return LINE(1892,f_js_int(x_max(1, v_arr)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1894 */
Variant c_js_math::ti_min(const char* cls, int num_args, CVarRef v_v1, CVarRef v_v2, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_math, js_math::min);
  DECLARE_GLOBAL_VARIABLES(g);
  Array v_args;
  Array v_arr;
  Variant v_arg;
  Variant v_v;

  (v_args = LINE(1895,func_get_args(num_args, Array(ArrayInit(2).set(0, v_v1).set(1, v_v2).create()),args)));
  if (equal(LINE(1896,x_count(v_args)), 0LL)) return g->s_jsrt_DupIdinfinity;
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(4);
    for (ArrayIter iter6 = v_args.begin("js_math"); !iter6.end(); ++iter6) {
      LOOP_COUNTER_CHECK(4);
      v_arg = iter6.second();
      {
        (v_v = LINE(1899,v_arg.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
        if (LINE(1900,x_is_nan(toDouble(v_v)))) return g->s_jsrt_DupIdnan;
        v_arr.append((v_v));
      }
    }
  }
  return LINE(1903,f_js_int(x_min(1, v_arr)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1905 */
Variant c_js_math::ti_pow(const char* cls, CVarRef v_x, CVarRef v_y) {
  STATIC_METHOD_INJECTION(js_math, js_math::pow);
  Variant eo_0;
  Variant eo_1;
  return LINE(1906,f_js_int((assignCallTemp(eo_0, toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)),assignCallTemp(eo_1, toObject(v_y)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)),x_pow(eo_0, eo_1))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1908 */
Variant c_js_math::ti_random(const char* cls) {
  STATIC_METHOD_INJECTION(js_math, js_math::random);
  return LINE(1909,f_js_int(divide_rev(x_mt_getrandmax(), x_mt_rand())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1911 */
Variant c_js_math::ti_round(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::round);
  return LINE(1912,f_js_int(x_round(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1914 */
Variant c_js_math::ti_sin(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::sin);
  return LINE(1915,f_js_int(x_sin(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1917 */
Variant c_js_math::ti_sqrt(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::sqrt);
  return LINE(1918,f_js_int(x_sqrt(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1920 */
Variant c_js_math::ti_tan(const char* cls, CVarRef v_x) {
  STATIC_METHOD_INJECTION(js_math, js_math::tan);
  return LINE(1921,f_js_int(x_tan(toDouble(toObject(v_x)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2483 */
Variant c_js_attribute::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_js_attribute::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_js_attribute::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("value", m_value.isReferenced() ? ref(m_value) : m_value));
  props.push_back(NEW(ArrayElement)("readonly", m_readonly.isReferenced() ? ref(m_readonly) : m_readonly));
  props.push_back(NEW(ArrayElement)("dontenum", m_dontenum.isReferenced() ? ref(m_dontenum) : m_dontenum));
  props.push_back(NEW(ArrayElement)("dontdelete", m_dontdelete.isReferenced() ? ref(m_dontdelete) : m_dontdelete));
  c_ObjectData::o_get(props);
}
bool c_js_attribute::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x6B679DD227B5A4C0LL, dontenum, 8);
      HASH_EXISTS_STRING(0x3F7B1D875EA3B688LL, dontdelete, 10);
      break;
    case 1:
      HASH_EXISTS_STRING(0x69E7413AE0C88471LL, value, 5);
      break;
    case 5:
      HASH_EXISTS_STRING(0x52486FED557DE615LL, readonly, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_js_attribute::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x6B679DD227B5A4C0LL, m_dontenum,
                         dontenum, 8);
      HASH_RETURN_STRING(0x3F7B1D875EA3B688LL, m_dontdelete,
                         dontdelete, 10);
      break;
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 5:
      HASH_RETURN_STRING(0x52486FED557DE615LL, m_readonly,
                         readonly, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_js_attribute::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x6B679DD227B5A4C0LL, m_dontenum,
                      dontenum, 8);
      HASH_SET_STRING(0x3F7B1D875EA3B688LL, m_dontdelete,
                      dontdelete, 10);
      break;
    case 1:
      HASH_SET_STRING(0x69E7413AE0C88471LL, m_value,
                      value, 5);
      break;
    case 5:
      HASH_SET_STRING(0x52486FED557DE615LL, m_readonly,
                      readonly, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_js_attribute::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x6B679DD227B5A4C0LL, m_dontenum,
                         dontenum, 8);
      HASH_RETURN_STRING(0x3F7B1D875EA3B688LL, m_dontdelete,
                         dontdelete, 10);
      break;
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 5:
      HASH_RETURN_STRING(0x52486FED557DE615LL, m_readonly,
                         readonly, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_js_attribute::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(js_attribute)
ObjectData *c_js_attribute::create(Variant v_value, Variant v_ro //  = 0LL
, Variant v_de //  = 0LL
, Variant v_dd //  = 0LL
) {
  init();
  t___construct(v_value, v_ro, v_de, v_dd);
  return this;
}
ObjectData *c_js_attribute::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    if (count == 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
void c_js_attribute::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  if (count == 3) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
}
ObjectData *c_js_attribute::cloneImpl() {
  c_js_attribute *obj = NEW(c_js_attribute)();
  cloneSet(obj);
  return obj;
}
void c_js_attribute::cloneSet(c_js_attribute *clone) {
  clone->m_value = m_value.isReferenced() ? ref(m_value) : m_value;
  clone->m_readonly = m_readonly.isReferenced() ? ref(m_readonly) : m_readonly;
  clone->m_dontenum = m_dontenum.isReferenced() ? ref(m_dontenum) : m_dontenum;
  clone->m_dontdelete = m_dontdelete.isReferenced() ? ref(m_dontdelete) : m_dontdelete;
  ObjectData::cloneSet(clone);
}
Variant c_js_attribute::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        if (count == 3) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_js_attribute::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        if (count == 3) return (t___construct(a0, a1, a2), null);
        return (t___construct(a0, a1, a2, a3), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_attribute::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_attribute$os_get(const char *s) {
  return c_js_attribute::os_get(s, -1);
}
Variant &cw_js_attribute$os_lval(const char *s) {
  return c_js_attribute::os_lval(s, -1);
}
Variant cw_js_attribute$os_constant(const char *s) {
  return c_js_attribute::os_constant(s);
}
Variant cw_js_attribute$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_attribute::os_invoke(c, s, params, -1, fatal);
}
void c_js_attribute::init() {
  m_value = null;
  m_readonly = false;
  m_dontenum = false;
  m_dontdelete = false;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2488 */
void c_js_attribute::t___construct(Variant v_value, Variant v_ro //  = 0LL
, Variant v_de //  = 0LL
, Variant v_dd //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_attribute, js_attribute::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_value = v_value);
  (m_readonly = v_ro);
  (m_dontenum = v_de);
  (m_dontdelete = v_dd);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2376 */
Variant c_js_evalerror::os_get(const char *s, int64 hash) {
  return c_js_error::os_get(s, hash);
}
Variant &c_js_evalerror::os_lval(const char *s, int64 hash) {
  return c_js_error::os_lval(s, hash);
}
void c_js_evalerror::o_get(ArrayElementVec &props) const {
  c_js_error::o_get(props);
}
bool c_js_evalerror::o_exists(CStrRef s, int64 hash) const {
  return c_js_error::o_exists(s, hash);
}
Variant c_js_evalerror::o_get(CStrRef s, int64 hash) {
  return c_js_error::o_get(s, hash);
}
Variant c_js_evalerror::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_error::o_set(s, hash, v, forInit);
}
Variant &c_js_evalerror::o_lval(CStrRef s, int64 hash) {
  return c_js_error::o_lval(s, hash);
}
Variant c_js_evalerror::os_constant(const char *s) {
  return c_js_error::os_constant(s);
}
IMPLEMENT_CLASS(js_evalerror)
ObjectData *c_js_evalerror::create(Variant v_msg //  = ""
) {
  init();
  t___construct(v_msg);
  return this;
}
ObjectData *c_js_evalerror::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_evalerror::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_evalerror::cloneImpl() {
  c_js_evalerror *obj = NEW(c_js_evalerror)();
  cloneSet(obj);
  return obj;
}
void c_js_evalerror::cloneSet(c_js_evalerror *clone) {
  c_js_error::cloneSet(clone);
}
Variant c_js_evalerror::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke(s, params, hash, fatal);
}
Variant c_js_evalerror::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_evalerror::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_error::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_evalerror$os_get(const char *s) {
  return c_js_evalerror::os_get(s, -1);
}
Variant &cw_js_evalerror$os_lval(const char *s) {
  return c_js_evalerror::os_lval(s, -1);
}
Variant cw_js_evalerror$os_constant(const char *s) {
  return c_js_evalerror::os_constant(s);
}
Variant cw_js_evalerror$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_evalerror::os_invoke(c, s, params, -1, fatal);
}
void c_js_evalerror::init() {
  c_js_error::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2377 */
void c_js_evalerror::t___construct(Variant v_msg //  = ""
) {
  INSTANCE_METHOD_INJECTION(js_evalerror, js_evalerror::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(2378,c_js_error::t___construct("EvalError", g->s_jsrt_DupIdproto_evalerror, v_msg));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2383 */
p_js_evalerror c_js_evalerror::ti_object(const char* cls, CVarRef v_message) {
  STATIC_METHOD_INJECTION(js_evalerror, js_evalerror::object);
  return ((Object)(LINE(2384,p_js_evalerror(p_js_evalerror(NEWOBJ(c_js_evalerror)())->create(toObject(v_message)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0).o_get("value", 0x69E7413AE0C88471LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 15 */
Variant c_jsrt::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 2:
      HASH_RETURN(0x345241CAC8396B02LL, g->s_jsrt_DupIdfunctions,
                  functions);
      break;
    case 4:
      HASH_RETURN(0x3D1E3BCD102BFBC4LL, g->s_jsrt_DupIdtrue,
                  true);
      break;
    case 6:
      HASH_RETURN(0x50DFBE3129205286LL, g->s_jsrt_DupIdproto_rangeerror,
                  proto_rangeerror);
      break;
    case 7:
      HASH_RETURN(0x37FCF1EB68462987LL, g->s_jsrt_DupIdproto_string,
                  proto_string);
      break;
    case 12:
      HASH_RETURN(0x78C950DE8431398CLL, g->s_jsrt_DupIdnan,
                  nan);
      break;
    case 13:
      HASH_RETURN(0x6E8C4A31410A754DLL, g->s_jsrt_DupIdundefined,
                  undefined);
      HASH_RETURN(0x622CA871A23AFD0DLL, g->s_jsrt_DupIdproto_evalerror,
                  proto_evalerror);
      break;
    case 18:
      HASH_RETURN(0x781F7BB42A49E9D2LL, g->s_jsrt_DupIdproto_regexp,
                  proto_regexp);
      break;
    case 21:
      HASH_RETURN(0x54EE9164258D9D95LL, g->s_jsrt_DupIdproto_typeerror,
                  proto_typeerror);
      HASH_RETURN(0x491BDEA451C35C95LL, g->s_jsrt_DupIdproto_urierror,
                  proto_urierror);
      break;
    case 22:
      HASH_RETURN(0x51AFC518383E0E96LL, g->s_jsrt_DupIdcontexts,
                  contexts);
      HASH_RETURN(0x4B19ECD5B61B5296LL, g->s_jsrt_DupIdglobal,
                  global);
      HASH_RETURN(0x03CADA938C98C156LL, g->s_jsrt_DupIdfalse,
                  false);
      break;
    case 26:
      HASH_RETURN(0x5A3D94EF8EE3685ALL, g->s_jsrt_DupIdidcache,
                  idcache);
      break;
    case 28:
      HASH_RETURN(0x660E17AB29A8269CLL, g->s_jsrt_DupIdproto_date,
                  proto_date);
      break;
    case 29:
      HASH_RETURN(0x37301431385F1EDDLL, g->s_jsrt_DupIdone,
                  one);
      HASH_RETURN(0x731C01C01877A41DLL, g->s_jsrt_DupIdproto_syntaxerror,
                  proto_syntaxerror);
      break;
    case 31:
      HASH_RETURN(0x71B2735BDC18B55FLL, g->s_jsrt_DupIdproto_object,
                  proto_object);
      break;
    case 36:
      HASH_RETURN(0x751224D818156BA4LL, g->s_jsrt_DupIdinfinity,
                  infinity);
      break;
    case 37:
      HASH_RETURN(0x1E0FCD2B8E2E76A5LL, g->s_jsrt_DupIdproto_function,
                  proto_function);
      break;
    case 39:
      HASH_RETURN(0x08A675A821C34EA7LL, g->s_jsrt_DupIdproto_boolean,
                  proto_boolean);
      break;
    case 40:
      HASH_RETURN(0x4C80FC0451FE86E8LL, g->s_jsrt_DupIdzero,
                  zero);
      break;
    case 41:
      HASH_RETURN(0x130EFE075A19C169LL, g->s_jsrt_DupIdproto_error,
                  proto_error);
      break;
    case 44:
      HASH_RETURN(0x2FF6F3DFF927FF2CLL, g->s_jsrt_DupIdnull,
                  null);
      break;
    case 45:
      HASH_RETURN(0x71B534C6AE8717ADLL, g->s_jsrt_DupIdexception,
                  exception);
      HASH_RETURN(0x23FFEBEA91BD316DLL, g->s_jsrt_DupIdproto_array,
                  proto_array);
      break;
    case 55:
      HASH_RETURN(0x72E98E197FEC45B7LL, g->s_jsrt_DupIdproto_number,
                  proto_number);
      break;
    case 59:
      HASH_RETURN(0x7EFB5FCCFCADBE3BLL, g->s_jsrt_DupIdsortfn,
                  sortfn);
      HASH_RETURN(0x225F3AD74B419EFBLL, g->s_jsrt_DupIdproto_referenceerror,
                  proto_referenceerror);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_jsrt::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 2:
      HASH_RETURN(0x345241CAC8396B02LL, g->s_jsrt_DupIdfunctions,
                  functions);
      break;
    case 4:
      HASH_RETURN(0x3D1E3BCD102BFBC4LL, g->s_jsrt_DupIdtrue,
                  true);
      break;
    case 6:
      HASH_RETURN(0x50DFBE3129205286LL, g->s_jsrt_DupIdproto_rangeerror,
                  proto_rangeerror);
      break;
    case 7:
      HASH_RETURN(0x37FCF1EB68462987LL, g->s_jsrt_DupIdproto_string,
                  proto_string);
      break;
    case 12:
      HASH_RETURN(0x78C950DE8431398CLL, g->s_jsrt_DupIdnan,
                  nan);
      break;
    case 13:
      HASH_RETURN(0x6E8C4A31410A754DLL, g->s_jsrt_DupIdundefined,
                  undefined);
      HASH_RETURN(0x622CA871A23AFD0DLL, g->s_jsrt_DupIdproto_evalerror,
                  proto_evalerror);
      break;
    case 18:
      HASH_RETURN(0x781F7BB42A49E9D2LL, g->s_jsrt_DupIdproto_regexp,
                  proto_regexp);
      break;
    case 21:
      HASH_RETURN(0x54EE9164258D9D95LL, g->s_jsrt_DupIdproto_typeerror,
                  proto_typeerror);
      HASH_RETURN(0x491BDEA451C35C95LL, g->s_jsrt_DupIdproto_urierror,
                  proto_urierror);
      break;
    case 22:
      HASH_RETURN(0x51AFC518383E0E96LL, g->s_jsrt_DupIdcontexts,
                  contexts);
      HASH_RETURN(0x4B19ECD5B61B5296LL, g->s_jsrt_DupIdglobal,
                  global);
      HASH_RETURN(0x03CADA938C98C156LL, g->s_jsrt_DupIdfalse,
                  false);
      break;
    case 26:
      HASH_RETURN(0x5A3D94EF8EE3685ALL, g->s_jsrt_DupIdidcache,
                  idcache);
      break;
    case 28:
      HASH_RETURN(0x660E17AB29A8269CLL, g->s_jsrt_DupIdproto_date,
                  proto_date);
      break;
    case 29:
      HASH_RETURN(0x37301431385F1EDDLL, g->s_jsrt_DupIdone,
                  one);
      HASH_RETURN(0x731C01C01877A41DLL, g->s_jsrt_DupIdproto_syntaxerror,
                  proto_syntaxerror);
      break;
    case 31:
      HASH_RETURN(0x71B2735BDC18B55FLL, g->s_jsrt_DupIdproto_object,
                  proto_object);
      break;
    case 36:
      HASH_RETURN(0x751224D818156BA4LL, g->s_jsrt_DupIdinfinity,
                  infinity);
      break;
    case 37:
      HASH_RETURN(0x1E0FCD2B8E2E76A5LL, g->s_jsrt_DupIdproto_function,
                  proto_function);
      break;
    case 39:
      HASH_RETURN(0x08A675A821C34EA7LL, g->s_jsrt_DupIdproto_boolean,
                  proto_boolean);
      break;
    case 40:
      HASH_RETURN(0x4C80FC0451FE86E8LL, g->s_jsrt_DupIdzero,
                  zero);
      break;
    case 41:
      HASH_RETURN(0x130EFE075A19C169LL, g->s_jsrt_DupIdproto_error,
                  proto_error);
      break;
    case 44:
      HASH_RETURN(0x2FF6F3DFF927FF2CLL, g->s_jsrt_DupIdnull,
                  null);
      break;
    case 45:
      HASH_RETURN(0x71B534C6AE8717ADLL, g->s_jsrt_DupIdexception,
                  exception);
      HASH_RETURN(0x23FFEBEA91BD316DLL, g->s_jsrt_DupIdproto_array,
                  proto_array);
      break;
    case 55:
      HASH_RETURN(0x72E98E197FEC45B7LL, g->s_jsrt_DupIdproto_number,
                  proto_number);
      break;
    case 59:
      HASH_RETURN(0x7EFB5FCCFCADBE3BLL, g->s_jsrt_DupIdsortfn,
                  sortfn);
      HASH_RETURN(0x225F3AD74B419EFBLL, g->s_jsrt_DupIdproto_referenceerror,
                  proto_referenceerror);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_jsrt::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_jsrt::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_jsrt::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_jsrt::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_jsrt::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_jsrt::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(jsrt)
ObjectData *c_jsrt::cloneImpl() {
  c_jsrt *obj = NEW(c_jsrt)();
  cloneSet(obj);
  return obj;
}
void c_jsrt::cloneSet(c_jsrt *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_jsrt::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (ti_start(o_getClassName()), null);
      }
      HASH_GUARD(0x67CE1DE11CB439FALL, literal_array) {
        int count = params.size();
        if (count <= 0) return (ti_literal_array(o_getClassName(),count));
        return (ti_literal_array(o_getClassName(),count,params.slice(0, count - 0, false)));
      }
      break;
    case 4:
      HASH_GUARD(0x4D34052915EF41ACLL, call) {
        return (ti_call(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 5:
      HASH_GUARD(0x548D0802D1DB44ADLL, write) {
        int count = params.size();
        if (count <= 0) return (ti_write(o_getClassName(),count), null);
        return (ti_write(o_getClassName(),count,params.slice(0, count - 0, false)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_jsrt::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (ti_start(o_getClassName()), null);
      }
      HASH_GUARD(0x67CE1DE11CB439FALL, literal_array) {
        if (count <= 0) return (ti_literal_array(o_getClassName(),count));
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_literal_array(o_getClassName(),count,params));
      }
      break;
    case 4:
      HASH_GUARD(0x4D34052915EF41ACLL, call) {
        return (ti_call(o_getClassName(), a0, a1));
      }
      break;
    case 5:
      HASH_GUARD(0x548D0802D1DB44ADLL, write) {
        if (count <= 0) return (ti_write(o_getClassName(),count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_write(o_getClassName(),count,params), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_jsrt::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (ti_start(c), null);
      }
      HASH_GUARD(0x67CE1DE11CB439FALL, literal_array) {
        int count = params.size();
        if (count <= 0) return (ti_literal_array(c,count));
        return (ti_literal_array(c,count,params.slice(0, count - 0, false)));
      }
      break;
    case 4:
      HASH_GUARD(0x4D34052915EF41ACLL, call) {
        return (ti_call(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 5:
      HASH_GUARD(0x548D0802D1DB44ADLL, write) {
        int count = params.size();
        if (count <= 0) return (ti_write(c,count), null);
        return (ti_write(c,count,params.slice(0, count - 0, false)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_jsrt$os_get(const char *s) {
  return c_jsrt::os_get(s, -1);
}
Variant &cw_jsrt$os_lval(const char *s) {
  return c_jsrt::os_lval(s, -1);
}
Variant cw_jsrt$os_constant(const char *s) {
  return c_jsrt::os_constant(s);
}
Variant cw_jsrt$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_jsrt::os_invoke(c, s, params, -1, fatal);
}
void c_jsrt::init() {
}
void c_jsrt::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_jsrt_DupIdcontexts = null;
  g->s_jsrt_DupIdfunctions = null;
  g->s_jsrt_DupIdglobal = null;
  g->s_jsrt_DupIdzero = null;
  g->s_jsrt_DupIdone = null;
  g->s_jsrt_DupIdtrue = null;
  g->s_jsrt_DupIdfalse = null;
  g->s_jsrt_DupIdnull = null;
  g->s_jsrt_DupIdundefined = null;
  g->s_jsrt_DupIdnan = null;
  g->s_jsrt_DupIdinfinity = null;
  g->s_jsrt_DupIdexception = null;
  g->s_jsrt_DupIdsortfn = null;
  g->s_jsrt_DupIdproto_object = null;
  g->s_jsrt_DupIdproto_function = null;
  g->s_jsrt_DupIdproto_array = null;
  g->s_jsrt_DupIdproto_string = null;
  g->s_jsrt_DupIdproto_boolean = null;
  g->s_jsrt_DupIdproto_number = null;
  g->s_jsrt_DupIdproto_date = null;
  g->s_jsrt_DupIdproto_regexp = null;
  g->s_jsrt_DupIdproto_error = null;
  g->s_jsrt_DupIdproto_evalerror = null;
  g->s_jsrt_DupIdproto_rangeerror = null;
  g->s_jsrt_DupIdproto_referenceerror = null;
  g->s_jsrt_DupIdproto_syntaxerror = null;
  g->s_jsrt_DupIdproto_typeerror = null;
  g->s_jsrt_DupIdproto_urierror = null;
  g->s_jsrt_DupIdidcache = null;
}
void csi_jsrt() {
  c_jsrt::os_static_initializer();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 50 */
void c_jsrt::ti_start_once(const char* cls) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::start_once);
  DECLARE_GLOBAL_VARIABLES(g);
  if (equal(LINE(51,x_get_class(g->s_jsrt_DupIdglobal)), "js_object")) return;
  LINE(52,c_jsrt::t_start());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 55 */
void c_jsrt::ti_start(const char* cls) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::start);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_internal;
  Variant v_o;
  p_js_math v_math;

  (g->s_jsrt_DupIdglobal = ((Object)(LINE(57,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create())))));
  (g->s_jsrt_DupIdcontexts = Array(ArrayInit(1).set(0, ((Object)(LINE(59,p_js_context(p_js_context(NEWOBJ(c_js_context)())->create(g->s_jsrt_DupIdglobal, Array(ArrayInit(1).set(0, g->s_jsrt_DupIdglobal).create()), g->s_jsrt_DupIdglobal)))))).create()));
  (g->s_jsrt_DupIdfunctions = ScalarArrays::sa_[0]);
  (g->s_jsrt_DupIdnan = LINE(62,f_js_int(x_acos(1.01))));
  (g->s_jsrt_DupIdinfinity = LINE(63,f_js_int(negate(x_log(toDouble(0LL))))));
  (g->s_jsrt_DupIdundefined = ((Object)(LINE(64,p_js_val(p_js_val(NEWOBJ(c_js_val)())->create(2, 0LL /* js_val::UNDEFINED */, 0LL))))));
  (g->s_jsrt_DupIdnull = ((Object)(LINE(65,p_js_val(p_js_val(NEWOBJ(c_js_val)())->create(2, 1LL /* js_val::NULL */, 0LL))))));
  (g->s_jsrt_DupIdtrue = ((Object)(LINE(66,p_js_val(p_js_val(NEWOBJ(c_js_val)())->create(2, 2LL /* js_val::BOOLEAN */, true))))));
  (g->s_jsrt_DupIdfalse = ((Object)(LINE(67,p_js_val(p_js_val(NEWOBJ(c_js_val)())->create(2, 2LL /* js_val::BOOLEAN */, false))))));
  (g->s_jsrt_DupIdzero = LINE(68,f_js_int(0LL)));
  (g->s_jsrt_DupIdone = LINE(69,f_js_int(1LL)));
  (g->s_jsrt_DupIdexception = null);
  (g->s_jsrt_DupIdsortfn = null);
  (v_internal = ScalarArrays::sa_[1]);
  (g->s_jsrt_DupIdproto_object = (g->s_jsrt_DupIdproto_function = (g->s_jsrt_DupIdproto_array = (g->s_jsrt_DupIdproto_string = (g->s_jsrt_DupIdproto_boolean = (g->s_jsrt_DupIdproto_number = (g->s_jsrt_DupIdproto_date = (g->s_jsrt_DupIdproto_regexp = (g->s_jsrt_DupIdproto_error = (g->s_jsrt_DupIdproto_evalerror = (g->s_jsrt_DupIdproto_rangeerror = (g->s_jsrt_DupIdproto_referenceerror = (g->s_jsrt_DupIdproto_syntaxerror = (g->s_jsrt_DupIdproto_typeerror = (g->s_jsrt_DupIdproto_urierror = null)))))))))))))));
  (g->s_jsrt_DupIdproto_object = ((Object)(LINE(77,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create())))));
  LINE(78,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_object));
  LINE(79,c_jsrt::t_define_function(ScalarArrays::sa_[2], "toString"));
  LINE(80,c_jsrt::t_define_function(ScalarArrays::sa_[2], "toLocaleString"));
  LINE(81,c_jsrt::t_define_function(ScalarArrays::sa_[3], "valueOf"));
  LINE(82,c_jsrt::t_define_function(ScalarArrays::sa_[4], "hasOwnProperty", ScalarArrays::sa_[5]));
  LINE(83,c_jsrt::t_define_function(ScalarArrays::sa_[6], "isPrototypeOf", ScalarArrays::sa_[5]));
  LINE(84,c_jsrt::t_define_function(ScalarArrays::sa_[7], "propertyIsEnumerable", ScalarArrays::sa_[5]));
  LINE(85,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_function = ((Object)(LINE(86,p_js_function(p_js_function(NEWOBJ(c_js_function)())->create())))));
  LINE(87,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_function));
  LINE(88,c_jsrt::t_define_function(ScalarArrays::sa_[8], "toString"));
  LINE(89,c_jsrt::t_define_function(ScalarArrays::sa_[9], "apply", ScalarArrays::sa_[10]));
  LINE(90,c_jsrt::t_define_function(ScalarArrays::sa_[11], "call", ScalarArrays::sa_[12]));
  LINE(91,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_array = ((Object)(LINE(92,p_js_array(p_js_array(NEWOBJ(c_js_array)())->create())))));
  LINE(93,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_array));
  LINE(94,c_jsrt::t_define_function(ScalarArrays::sa_[13], "toString"));
  LINE(95,c_jsrt::t_define_function(ScalarArrays::sa_[14], "toLocaleString"));
  LINE(96,c_jsrt::t_define_function(ScalarArrays::sa_[15], "concat", ScalarArrays::sa_[16]));
  LINE(97,c_jsrt::t_define_function(ScalarArrays::sa_[17], "join", ScalarArrays::sa_[18]));
  LINE(98,c_jsrt::t_define_function(ScalarArrays::sa_[19], "pop"));
  LINE(99,c_jsrt::t_define_function(ScalarArrays::sa_[20], "push", ScalarArrays::sa_[16]));
  LINE(100,c_jsrt::t_define_function(ScalarArrays::sa_[21], "reverse"));
  LINE(101,c_jsrt::t_define_function(ScalarArrays::sa_[22], "shift"));
  LINE(102,c_jsrt::t_define_function(ScalarArrays::sa_[23], "slice", ScalarArrays::sa_[24]));
  LINE(103,c_jsrt::t_define_function(ScalarArrays::sa_[25], "sort", ScalarArrays::sa_[26]));
  LINE(104,c_jsrt::t_define_function(ScalarArrays::sa_[27], "splice", ScalarArrays::sa_[28]));
  LINE(105,c_jsrt::t_define_function(ScalarArrays::sa_[29], "unshift", ScalarArrays::sa_[16]));
  LINE(106,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_string = ((Object)(LINE(107,p_js_string(p_js_string(NEWOBJ(c_js_string)())->create())))));
  LINE(108,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_string));
  LINE(109,c_jsrt::t_define_function(ScalarArrays::sa_[30], "toString"));
  LINE(110,c_jsrt::t_define_function(ScalarArrays::sa_[30], "valueOf"));
  LINE(111,c_jsrt::t_define_function(ScalarArrays::sa_[31], "charAt", ScalarArrays::sa_[32]));
  LINE(112,c_jsrt::t_define_function(ScalarArrays::sa_[33], "charCodeAt", ScalarArrays::sa_[32]));
  LINE(113,c_jsrt::t_define_function(ScalarArrays::sa_[34], "concat", ScalarArrays::sa_[35]));
  LINE(114,c_jsrt::t_define_function(ScalarArrays::sa_[36], "indexOf", ScalarArrays::sa_[37]));
  LINE(115,c_jsrt::t_define_function(ScalarArrays::sa_[38], "lastIndexOf", ScalarArrays::sa_[37]));
  LINE(116,c_jsrt::t_define_function(ScalarArrays::sa_[39], "localeCompare", ScalarArrays::sa_[40]));
  LINE(117,c_jsrt::t_define_function(ScalarArrays::sa_[41], "match", ScalarArrays::sa_[42]));
  LINE(118,c_jsrt::t_define_function(ScalarArrays::sa_[43], "replace", ScalarArrays::sa_[44]));
  LINE(119,c_jsrt::t_define_function(ScalarArrays::sa_[45], "search", ScalarArrays::sa_[42]));
  LINE(120,c_jsrt::t_define_function(ScalarArrays::sa_[46], "slice", ScalarArrays::sa_[24]));
  LINE(121,c_jsrt::t_define_function(ScalarArrays::sa_[47], "split", ScalarArrays::sa_[48]));
  LINE(122,c_jsrt::t_define_function(ScalarArrays::sa_[49], "substr", ScalarArrays::sa_[50]));
  LINE(123,c_jsrt::t_define_function(ScalarArrays::sa_[51], "substring", ScalarArrays::sa_[24]));
  LINE(124,c_jsrt::t_define_function(ScalarArrays::sa_[52], "toLowerCase"));
  LINE(125,c_jsrt::t_define_function(ScalarArrays::sa_[53], "toLocaleLowerCase"));
  LINE(126,c_jsrt::t_define_function(ScalarArrays::sa_[54], "toUpperCase"));
  LINE(127,c_jsrt::t_define_function(ScalarArrays::sa_[55], "toLocaleUpperCase"));
  LINE(128,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_boolean = ((Object)(LINE(129,p_js_boolean(p_js_boolean(NEWOBJ(c_js_boolean)())->create())))));
  LINE(130,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_boolean));
  LINE(131,c_jsrt::t_define_function(ScalarArrays::sa_[56], "toString"));
  LINE(132,c_jsrt::t_define_function(ScalarArrays::sa_[57], "valueOf"));
  LINE(133,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_number = ((Object)(LINE(134,p_js_number(p_js_number(NEWOBJ(c_js_number)())->create())))));
  LINE(135,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_number));
  LINE(136,c_jsrt::t_define_function(ScalarArrays::sa_[58], "toString", ScalarArrays::sa_[59]));
  LINE(137,c_jsrt::t_define_function(ScalarArrays::sa_[58], "toLocaleString", ScalarArrays::sa_[59]));
  LINE(138,c_jsrt::t_define_function(ScalarArrays::sa_[60], "valueOf"));
  LINE(139,c_jsrt::t_define_function(ScalarArrays::sa_[61], "toFixed", ScalarArrays::sa_[62]));
  LINE(140,c_jsrt::t_define_function(ScalarArrays::sa_[63], "toExponential", ScalarArrays::sa_[62]));
  LINE(141,c_jsrt::t_define_function(ScalarArrays::sa_[64], "toPrecision", ScalarArrays::sa_[65]));
  LINE(142,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_date = ((Object)(LINE(143,p_js_date(p_js_date(NEWOBJ(c_js_date)())->create())))));
  LINE(144,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_date));
  LINE(145,c_jsrt::t_define_function(ScalarArrays::sa_[66], "toString"));
  LINE(146,c_jsrt::t_define_function(ScalarArrays::sa_[67], "toDateString"));
  LINE(147,c_jsrt::t_define_function(ScalarArrays::sa_[68], "toTimeString"));
  LINE(148,c_jsrt::t_define_function(ScalarArrays::sa_[69], "toLocaleString"));
  LINE(149,c_jsrt::t_define_function(ScalarArrays::sa_[70], "toLocaleDateString"));
  LINE(150,c_jsrt::t_define_function(ScalarArrays::sa_[71], "toLocaleTimeString"));
  LINE(151,c_jsrt::t_define_function(ScalarArrays::sa_[72], "valueOf"));
  LINE(152,c_jsrt::t_define_function(ScalarArrays::sa_[73], "getTime"));
  LINE(153,c_jsrt::t_define_function(ScalarArrays::sa_[74], "getFullYear"));
  LINE(154,c_jsrt::t_define_function(ScalarArrays::sa_[75], "getUTCFullYear"));
  LINE(155,c_jsrt::t_define_function(ScalarArrays::sa_[76], "getMonth"));
  LINE(156,c_jsrt::t_define_function(ScalarArrays::sa_[77], "getUTCMonth"));
  LINE(157,c_jsrt::t_define_function(ScalarArrays::sa_[78], "getDate"));
  LINE(158,c_jsrt::t_define_function(ScalarArrays::sa_[79], "getUTCDate"));
  LINE(159,c_jsrt::t_define_function(ScalarArrays::sa_[80], "getDay"));
  LINE(160,c_jsrt::t_define_function(ScalarArrays::sa_[81], "getUTCDay"));
  LINE(161,c_jsrt::t_define_function(ScalarArrays::sa_[82], "getHours"));
  LINE(162,c_jsrt::t_define_function(ScalarArrays::sa_[83], "getUTCHours"));
  LINE(163,c_jsrt::t_define_function(ScalarArrays::sa_[84], "getMinutes"));
  LINE(164,c_jsrt::t_define_function(ScalarArrays::sa_[85], "getUTCMinutes"));
  LINE(165,c_jsrt::t_define_function(ScalarArrays::sa_[86], "getSeconds"));
  LINE(166,c_jsrt::t_define_function(ScalarArrays::sa_[87], "getUTCSeconds"));
  LINE(167,c_jsrt::t_define_function(ScalarArrays::sa_[88], "getMilliseconds"));
  LINE(168,c_jsrt::t_define_function(ScalarArrays::sa_[89], "getUTCMilliseconds"));
  LINE(169,c_jsrt::t_define_function(ScalarArrays::sa_[90], "getTimezoneOffset"));
  LINE(170,c_jsrt::t_define_function(ScalarArrays::sa_[91], "setTime", ScalarArrays::sa_[92]));
  LINE(171,c_jsrt::t_define_function(ScalarArrays::sa_[93], "setMilliseconds", ScalarArrays::sa_[94]));
  LINE(172,c_jsrt::t_define_function(ScalarArrays::sa_[95], "setUTCMilliseconds", ScalarArrays::sa_[94]));
  LINE(173,c_jsrt::t_define_function(ScalarArrays::sa_[96], "setSeconds", ScalarArrays::sa_[97]));
  LINE(174,c_jsrt::t_define_function(ScalarArrays::sa_[98], "setUTCSeconds", ScalarArrays::sa_[97]));
  LINE(175,c_jsrt::t_define_function(ScalarArrays::sa_[99], "setMinutes", ScalarArrays::sa_[100]));
  LINE(176,c_jsrt::t_define_function(ScalarArrays::sa_[101], "setUTCMinutes", ScalarArrays::sa_[100]));
  LINE(177,c_jsrt::t_define_function(ScalarArrays::sa_[102], "setHours", ScalarArrays::sa_[103]));
  LINE(178,c_jsrt::t_define_function(ScalarArrays::sa_[104], "setUTCHours", ScalarArrays::sa_[103]));
  LINE(179,c_jsrt::t_define_function(ScalarArrays::sa_[105], "setDate", ScalarArrays::sa_[106]));
  LINE(180,c_jsrt::t_define_function(ScalarArrays::sa_[107], "setUTCDate", ScalarArrays::sa_[106]));
  LINE(181,c_jsrt::t_define_function(ScalarArrays::sa_[108], "setMonth", ScalarArrays::sa_[109]));
  LINE(182,c_jsrt::t_define_function(ScalarArrays::sa_[110], "setUTCMonth", ScalarArrays::sa_[109]));
  LINE(183,c_jsrt::t_define_function(ScalarArrays::sa_[111], "setFullYear", ScalarArrays::sa_[112]));
  LINE(184,c_jsrt::t_define_function(ScalarArrays::sa_[113], "setUTCFullYear", ScalarArrays::sa_[112]));
  LINE(185,c_jsrt::t_define_function(ScalarArrays::sa_[114], "toUTCString"));
  LINE(186,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_regexp = ((Object)(LINE(187,p_js_regexp(p_js_regexp(NEWOBJ(c_js_regexp)())->create())))));
  LINE(188,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_regexp));
  LINE(189,c_jsrt::t_define_function(ScalarArrays::sa_[115], "exec", ScalarArrays::sa_[35]));
  LINE(190,c_jsrt::t_define_function(ScalarArrays::sa_[116], "test", ScalarArrays::sa_[35]));
  LINE(191,c_jsrt::t_define_function(ScalarArrays::sa_[117], "toString"));
  LINE(192,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_error = ((Object)(LINE(193,p_js_error(p_js_error(NEWOBJ(c_js_error)())->create())))));
  (assignCallTemp(eo_1, ref(LINE(194,f_js_str("Error")))),g->s_jsrt_DupIdproto_error.o_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(195,f_js_str("")))),g->s_jsrt_DupIdproto_error.o_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  LINE(196,c_jsrt::t_push_context(g->s_jsrt_DupIdproto_error));
  LINE(197,c_jsrt::t_define_function(ScalarArrays::sa_[118], "toString"));
  LINE(198,c_jsrt::t_pop_context());
  (g->s_jsrt_DupIdproto_evalerror = ((Object)(LINE(199,p_js_evalerror(p_js_evalerror(NEWOBJ(c_js_evalerror)())->create())))));
  (assignCallTemp(eo_1, ref(LINE(200,f_js_str("EvalError")))),g->s_jsrt_DupIdproto_evalerror.o_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(201,f_js_str("")))),g->s_jsrt_DupIdproto_evalerror.o_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (g->s_jsrt_DupIdproto_rangeerror = ((Object)(LINE(202,p_js_rangeerror(p_js_rangeerror(NEWOBJ(c_js_rangeerror)())->create())))));
  (assignCallTemp(eo_1, ref(LINE(203,f_js_str("RangeError")))),g->s_jsrt_DupIdproto_rangeerror.o_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(204,f_js_str("")))),g->s_jsrt_DupIdproto_rangeerror.o_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (g->s_jsrt_DupIdproto_referenceerror = ((Object)(LINE(205,p_js_referenceerror(p_js_referenceerror(NEWOBJ(c_js_referenceerror)())->create())))));
  (assignCallTemp(eo_1, ref(LINE(206,f_js_str("ReferenceError")))),g->s_jsrt_DupIdproto_referenceerror.o_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(207,f_js_str("")))),g->s_jsrt_DupIdproto_referenceerror.o_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (g->s_jsrt_DupIdproto_syntaxerror = ((Object)(LINE(208,p_js_syntaxerror(p_js_syntaxerror(NEWOBJ(c_js_syntaxerror)())->create())))));
  (assignCallTemp(eo_1, ref(LINE(209,f_js_str("SyntaxError")))),g->s_jsrt_DupIdproto_syntaxerror.o_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(210,f_js_str("")))),g->s_jsrt_DupIdproto_syntaxerror.o_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (g->s_jsrt_DupIdproto_typeerror = ((Object)(LINE(211,p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create())))));
  (assignCallTemp(eo_1, ref(LINE(212,f_js_str("TypeError")))),g->s_jsrt_DupIdproto_typeerror.o_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(213,f_js_str("")))),g->s_jsrt_DupIdproto_typeerror.o_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (g->s_jsrt_DupIdproto_urierror = ((Object)(LINE(214,p_js_urierror(p_js_urierror(NEWOBJ(c_js_urierror)())->create())))));
  (assignCallTemp(eo_1, ref(LINE(215,f_js_str("URIError")))),g->s_jsrt_DupIdproto_urierror.o_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(216,f_js_str("")))),g->s_jsrt_DupIdproto_urierror.o_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  LINE(218,c_jsrt::t_define_variable("NaN", g->s_jsrt_DupIdnan));
  LINE(219,c_jsrt::t_define_variable("Infinity", g->s_jsrt_DupIdinfinity));
  LINE(220,c_jsrt::t_define_variable("undefined", g->s_jsrt_DupIdundefined));
  LINE(222,c_jsrt::t_define_function("jsi_eval", "eval"));
  LINE(223,c_jsrt::t_define_function("jsi_parseInt", "parseInt", ScalarArrays::sa_[119]));
  LINE(224,c_jsrt::t_define_function("jsi_parseFloat", "parseFloat", ScalarArrays::sa_[120]));
  LINE(225,c_jsrt::t_define_function("jsi_isNaN", "isNaN", ScalarArrays::sa_[5]));
  LINE(226,c_jsrt::t_define_function("jsi_isFinite", "isFinite", ScalarArrays::sa_[5]));
  LINE(227,c_jsrt::t_define_function("jsi_decodeURI", "decodeURI"));
  LINE(228,c_jsrt::t_define_function("jsi_decodeURIComponent", "decodeURIComponent"));
  LINE(229,c_jsrt::t_define_function("jsi_encodeURI", "encodeURI"));
  LINE(230,c_jsrt::t_define_function("jsi_encodeURIComponent", "encodeURIComponent"));
  (v_o = LINE(232,c_jsrt::t_define_function(ScalarArrays::sa_[121], "Object", ScalarArrays::sa_[5], g->s_jsrt_DupIdproto_object)));
  LINE(233,g->s_jsrt_DupIdproto_object.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(234,c_jsrt::t_define_function(ScalarArrays::sa_[122], "Function", ScalarArrays::sa_[5], g->s_jsrt_DupIdproto_function)));
  LINE(235,g->s_jsrt_DupIdproto_function.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(236,c_jsrt::t_define_function(ScalarArrays::sa_[123], "Array", ScalarArrays::sa_[5], g->s_jsrt_DupIdproto_array)));
  LINE(237,g->s_jsrt_DupIdproto_array.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(238,c_jsrt::t_define_function(ScalarArrays::sa_[124], "String", ScalarArrays::sa_[5], g->s_jsrt_DupIdproto_string)));
  LINE(239,c_jsrt::t_push_context(v_o));
  LINE(240,c_jsrt::t_define_function(ScalarArrays::sa_[125], "fromCharCode", ScalarArrays::sa_[126]));
  LINE(241,c_jsrt::t_pop_context());
  LINE(242,g->s_jsrt_DupIdproto_string.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(243,c_jsrt::t_define_function(ScalarArrays::sa_[127], "Boolean", ScalarArrays::sa_[5], g->s_jsrt_DupIdproto_boolean)));
  LINE(244,g->s_jsrt_DupIdproto_boolean.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(245,c_jsrt::t_define_function(ScalarArrays::sa_[128], "Number", ScalarArrays::sa_[5], g->s_jsrt_DupIdproto_number)));
  (assignCallTemp(eo_1, ref(LINE(246,f_js_int(1.7976931348623157E+308)))),assignCallTemp(eo_2, ref(v_internal)),v_o.o_invoke("put", Array(ArrayInit(3).set(0, "MAX_VALUE").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(247,f_js_int(4.9406564584124654E-324)))),assignCallTemp(eo_2, ref(v_internal)),v_o.o_invoke("put", Array(ArrayInit(3).set(0, "MIN_VALUE").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  LINE(248,v_o.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, "NaN", g->s_jsrt_DupIdnan, v_internal));
  (assignCallTemp(eo_1, ref(LINE(249,c_jsrt::t_expr_u_minus(g->s_jsrt_DupIdinfinity)))),assignCallTemp(eo_2, ref(v_internal)),v_o.o_invoke("put", Array(ArrayInit(3).set(0, "NEGATIVE_INFINITY").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  LINE(250,v_o.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, "POSITIVE_INFINITY", g->s_jsrt_DupIdinfinity, v_internal));
  LINE(251,g->s_jsrt_DupIdproto_number.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(252,c_jsrt::t_define_function(ScalarArrays::sa_[129], "Date", ScalarArrays::sa_[130], g->s_jsrt_DupIdproto_date)));
  LINE(253,c_jsrt::t_push_context(v_o));
  LINE(254,c_jsrt::t_define_function(ScalarArrays::sa_[131], "parse", ScalarArrays::sa_[35]));
  LINE(255,c_jsrt::t_define_function(ScalarArrays::sa_[132], "UTC", ScalarArrays::sa_[130]));
  LINE(256,c_jsrt::t_pop_context());
  LINE(257,g->s_jsrt_DupIdproto_date.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(258,c_jsrt::t_define_function(ScalarArrays::sa_[133], "RegExp", ScalarArrays::sa_[134], g->s_jsrt_DupIdproto_regexp)));
  LINE(259,g->s_jsrt_DupIdproto_regexp.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(260,c_jsrt::t_define_function(ScalarArrays::sa_[135], "Error", ScalarArrays::sa_[136], g->s_jsrt_DupIdproto_error)));
  LINE(261,g->s_jsrt_DupIdproto_error.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(262,c_jsrt::t_define_function(ScalarArrays::sa_[137], "EvalError", ScalarArrays::sa_[136], g->s_jsrt_DupIdproto_evalerror)));
  LINE(263,g->s_jsrt_DupIdproto_evalerror.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(264,c_jsrt::t_define_function(ScalarArrays::sa_[138], "RangeError", ScalarArrays::sa_[136], g->s_jsrt_DupIdproto_rangeerror)));
  LINE(265,g->s_jsrt_DupIdproto_rangeerror.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(266,c_jsrt::t_define_function(ScalarArrays::sa_[139], "ReferenceError", ScalarArrays::sa_[136], g->s_jsrt_DupIdproto_referenceerror)));
  LINE(267,g->s_jsrt_DupIdproto_referenceerror.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(268,c_jsrt::t_define_function(ScalarArrays::sa_[140], "SyntaxError", ScalarArrays::sa_[136], g->s_jsrt_DupIdproto_syntaxerror)));
  LINE(269,g->s_jsrt_DupIdproto_syntaxerror.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(270,c_jsrt::t_define_function(ScalarArrays::sa_[141], "TypeError", ScalarArrays::sa_[136], g->s_jsrt_DupIdproto_typeerror)));
  LINE(271,g->s_jsrt_DupIdproto_typeerror.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  (v_o = LINE(272,c_jsrt::t_define_function(ScalarArrays::sa_[142], "URIError", ScalarArrays::sa_[136], g->s_jsrt_DupIdproto_urierror)));
  LINE(273,g->s_jsrt_DupIdproto_urierror.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "constructor", v_o));
  ((Object)((v_math = ((Object)(LINE(274,p_js_math(p_js_math(NEWOBJ(c_js_math)())->create())))))));
  LINE(275,c_jsrt::t_define_variable("Math", ((Object)(v_math))));
  (assignCallTemp(eo_1, ref(LINE(276,f_js_int(2.7182818284589998 /* M_E */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "E").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(277,f_js_int(2.3025850929940002 /* M_LN10 */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "LN10").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(278,f_js_int(0.69314718055994995 /* M_LN2 */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "LN2").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(279,f_js_int(1.442695040889 /* M_LOG2E */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "LOG2E").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(280,f_js_int(0.43429448190324998 /* M_LOG10E */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "LOG10E").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(281,f_js_int(3.1415926535898002 /* M_PI */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "PI").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(282,f_js_int(0.70710678118655002 /* M_SQRT1_2 */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "SQRT1_2").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(283,f_js_int(1.4142135623731 /* M_SQRT2 */)))),assignCallTemp(eo_2, ref(v_internal)),v_math->o_invoke("put", Array(ArrayInit(3).set(0, "SQRT2").set(1, eo_1).set(2, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  LINE(284,c_jsrt::t_push_context(((Object)(v_math))));
  LINE(285,c_jsrt::t_define_function(ScalarArrays::sa_[143], "abs", ScalarArrays::sa_[144]));
  LINE(286,c_jsrt::t_define_function(ScalarArrays::sa_[145], "acos", ScalarArrays::sa_[144]));
  LINE(287,c_jsrt::t_define_function(ScalarArrays::sa_[146], "asin", ScalarArrays::sa_[144]));
  LINE(288,c_jsrt::t_define_function(ScalarArrays::sa_[147], "atan", ScalarArrays::sa_[144]));
  LINE(289,c_jsrt::t_define_function(ScalarArrays::sa_[148], "atan2", ScalarArrays::sa_[149]));
  LINE(290,c_jsrt::t_define_function(ScalarArrays::sa_[150], "ceil", ScalarArrays::sa_[144]));
  LINE(291,c_jsrt::t_define_function(ScalarArrays::sa_[151], "cos", ScalarArrays::sa_[144]));
  LINE(292,c_jsrt::t_define_function(ScalarArrays::sa_[152], "exp", ScalarArrays::sa_[144]));
  LINE(293,c_jsrt::t_define_function(ScalarArrays::sa_[153], "floor", ScalarArrays::sa_[144]));
  LINE(294,c_jsrt::t_define_function(ScalarArrays::sa_[154], "log", ScalarArrays::sa_[144]));
  LINE(295,c_jsrt::t_define_function(ScalarArrays::sa_[155], "max", ScalarArrays::sa_[156]));
  LINE(296,c_jsrt::t_define_function(ScalarArrays::sa_[157], "min", ScalarArrays::sa_[156]));
  LINE(297,c_jsrt::t_define_function(ScalarArrays::sa_[158], "pow", ScalarArrays::sa_[159]));
  LINE(298,c_jsrt::t_define_function(ScalarArrays::sa_[160], "random"));
  LINE(299,c_jsrt::t_define_function(ScalarArrays::sa_[161], "round", ScalarArrays::sa_[144]));
  LINE(300,c_jsrt::t_define_function(ScalarArrays::sa_[162], "sin", ScalarArrays::sa_[144]));
  LINE(301,c_jsrt::t_define_function(ScalarArrays::sa_[163], "sqrt", ScalarArrays::sa_[144]));
  LINE(302,c_jsrt::t_define_function(ScalarArrays::sa_[164], "tan", ScalarArrays::sa_[144]));
  LINE(303,c_jsrt::t_pop_context());
  LINE(305,c_jsrt::t_define_variable("global", g->s_jsrt_DupIdglobal));
  LINE(306,c_jsrt::t_define_function(ScalarArrays::sa_[165], "write"));
  LINE(307,c_jsrt::t_define_function(ScalarArrays::sa_[165], "print"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 310 */
void c_jsrt::ti_push_context(const char* cls, CVarRef v_obj) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::push_context);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(311,(assignCallTemp(eo_0, ref(g->s_jsrt_DupIdcontexts)),assignCallTemp(eo_1, ((Object)(p_js_context(p_js_context(NEWOBJ(c_js_context)())->create(g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("js_this", 0x3740A81EEE430C82LL), g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("scope_chain", 0x0413391407EE12A6LL), v_obj))))),x_array_unshift(2, eo_0, eo_1)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 313 */
void c_jsrt::ti_pop_context(const char* cls) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::pop_context);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(314,x_array_shift(ref(g->s_jsrt_DupIdcontexts)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 317 */
void c_jsrt::ti_push_scope(const char* cls, p_js_object v_obj) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::push_scope);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(318,x_array_unshift(2, ref(lval(lval(g->s_jsrt_DupIdcontexts.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("scope_chain", 0x0413391407EE12A6LL))), ((Object)(v_obj))));
  (g->s_jsrt_DupIdidcache = ScalarArrays::sa_[0]);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 321 */
void c_jsrt::ti_pop_scope(const char* cls) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::pop_scope);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(322,x_array_shift(ref(lval(lval(g->s_jsrt_DupIdcontexts.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).o_lval("scope_chain", 0x0413391407EE12A6LL)))));
  (g->s_jsrt_DupIdidcache = ScalarArrays::sa_[0]);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 326 */
Variant c_jsrt::ti_define_function(const char* cls, CVarRef v_phpname, Variant v_jsname //  = ""
, CArrRef v_args //  = ScalarArrays::sa_[0]
, Variant v_proto //  = null
) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::define_function);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_func;

  (v_func = ((Object)(LINE(327,p_js_function(p_js_function(NEWOBJ(c_js_function)())->create(v_jsname, v_phpname, v_args, g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("scope_chain", 0x0413391407EE12A6LL)))))));
  if (!equal(v_proto, null)) {
    LINE(329,v_func.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, "prototype", v_proto, Array(ArrayInit(3).set(0, "dontenum").set(1, "dontdelete").set(2, "readonly").create())));
  }
  LINE(331,g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("var", 0x0953EE1CC9042120LL).o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_jsname, v_func));
  if (LINE(332,x_is_string(v_phpname))) {
    g->s_jsrt_DupIdfunctions.set(v_phpname, (v_func));
  }
  return v_func;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 338 */
void c_jsrt::ti_define_variable(const char* cls, Variant v_name, Variant v_val //  = null
) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::define_variable);
  DECLARE_GLOBAL_VARIABLES(g);
  if (equal(v_val, null)) (v_val = g->s_jsrt_DupIdundefined);
  LINE(340,g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("var", 0x0953EE1CC9042120LL).o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_name, v_val));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 342 */
void c_jsrt::ti_define_variables(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::define_variables);
  Array v_args;
  Variant v_arg;

  (v_args = LINE(343,func_get_args(num_args, Array(),args)));
  {
    LOOP_COUNTER(7);
    for (ArrayIter iter9 = v_args.begin("jsrt"); !iter9.end(); ++iter9) {
      LOOP_COUNTER_CHECK(7);
      v_arg = iter9.second();
      {
        LINE(344,c_jsrt::t_define_variable(v_arg));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 347 */
Variant c_jsrt::ti_trycatch(const char* cls, Variant v_expr, CVarRef v_catch, CVarRef v_finally, Variant v_id //  = 0LL
) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::trycatch);
  DECLARE_GLOBAL_VARIABLES(g);
  p_js_object v_obj;
  Variant v_ret;

  if (LINE(348,f_js_thrown(g->s_jsrt_DupIdexception))) {
    if (!equal(v_expr, null)) {
      echo("TRYCATCH ERROR: INCONSISTENT STATE.<hr><br>");
    }
    if (!equal(v_catch, null)) {
      ((Object)((v_obj = ((Object)(LINE(355,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create())))))));
      LINE(356,v_obj->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, v_id, g->s_jsrt_DupIdexception.o_lval("value", 0x69E7413AE0C88471LL), Array(ArrayInit(1).set(0, "dontdelete").create())));
      (g->s_jsrt_DupIdexception = null);
      LINE(358,c_jsrt::t_push_scope(((Object)(v_obj))));
      (v_ret = LINE(359,invoke(toString(v_catch), Array(), -1)));
      LINE(360,c_jsrt::t_pop_scope());
      if (!equal(v_ret, null)) (v_expr = v_ret);
    }
  }
  if (!equal(v_finally, null)) {
    (v_ret = LINE(366,invoke(toString(v_finally), Array(), -1)));
    if (!equal(v_ret, null)) (v_expr = v_ret);
  }
  if (LINE(369,f_js_thrown(g->s_jsrt_DupIdexception))) {
    throw_exception(toObject(g->s_jsrt_DupIdexception));
  }
  return v_expr;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 375 */
Variant c_jsrt::ti_call(const char* cls, Variant v_method, Variant v_args) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::call);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_that;
  Variant v_obj;
  Variant v_ret;

  if (instanceOf(v_method, "js_ref")) {
    (v_that = v_method.o_get("base", 0x56FD8ECCBDFDD7A7LL));
    if (!(toBoolean(v_that))) {
      (v_that = g->s_jsrt_DupIdglobal);
      (v_method.o_lval("base", 0x56FD8ECCBDFDD7A7LL) = v_that);
    }
    (v_obj = LINE(384,v_method.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)));
  }
  else {
    (v_that = g->s_jsrt_DupIdglobal);
    (v_obj = v_method);
  }
  if (!(toBoolean(v_obj))) return g->s_jsrt_DupIdundefined;
  if (instanceOf(v_obj, "js_function")) {
    (g->s_jsrt_DupIdidcache = ScalarArrays::sa_[0]);
    (v_ret = LINE(393,v_obj.o_invoke_few_args("_call", 0x70E3A91CE45475DBLL, 2, v_that, v_args)));
    (g->s_jsrt_DupIdidcache = ScalarArrays::sa_[0]);
    return v_ret;
  }
  else {
    throw_exception(((Object)(LINE(397,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create("Cannot call an object that isn't a function"))))))))));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 401 */
Variant c_jsrt::ti__new(const char* cls, CVarRef v_constructor, Variant v_args) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::_new);
  Variant v_c;

  (v_c = v_constructor);
  if (!((instanceOf(v_c, "js_function")))) {
    throw_exception(((Object)(LINE(404,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_syntaxerror(p_js_syntaxerror(NEWOBJ(c_js_syntaxerror)())->create("invalid constructor"))))))))));
  }
  return LINE(406,v_c.o_invoke_few_args("construct", 0x081CF9AB49391818LL, 1, v_args));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 411 */
Variant c_jsrt::ti_id(const char* cls, Variant v_id) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::id);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_chain;
  Variant v_scope;

  if (!(isset(g->s_jsrt_DupIdidcache, v_id))) {
    (v_chain = g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("scope_chain", 0x0413391407EE12A6LL));
    {
      LOOP_COUNTER(10);
      for (ArrayIterPtr iter12 = v_chain.begin("jsrt"); !iter12->end(); iter12->next()) {
        LOOP_COUNTER_CHECK(10);
        v_scope = iter12->second();
        {
          if (toBoolean(LINE(416,v_scope.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_id)))) {
            g->s_jsrt_DupIdidcache.set(v_id, (((Object)(LINE(424,p_js_ref(p_js_ref(NEWOBJ(c_js_ref)())->create(v_scope, v_id)))))));
            return g->s_jsrt_DupIdidcache.rvalAt(v_id);
          }
        }
      }
    }
    return ((Object)(LINE(429,p_js_ref_null(p_js_ref_null(NEWOBJ(c_js_ref_null)())->create(v_id)))));
  }
  return g->s_jsrt_DupIdidcache.rvalAt(v_id);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 433 */
Variant c_jsrt::ti_idv(const char* cls, CVarRef v_id) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::idv);
  Variant eo_0;
  return (assignCallTemp(eo_0, toObject(LINE(434,c_jsrt::t_id(v_id)))),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 437 */
p_js_ref c_jsrt::ti_dot(const char* cls, Variant v_base, CVarRef v_prop) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::dot);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;

  (v_obj = v_base);
  if (equal(v_obj, g->s_jsrt_DupIdnull)) {
    echo("dot(NULL, xxx) DOES NOT COMPUTE. ABORT! <pre>");
    LINE(441,x_debug_print_backtrace());
    echo("</pre>");
  }
  if (!((instanceOf(v_prop, "js_ref")))) {
    (v_base = LINE(445,toObject(v_prop)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  }
  else {
    (v_base = toObject(v_prop).o_get("propName", 0x4DC47FEB2126D745LL));
  }
  return ((Object)(LINE(451,(assignCallTemp(eo_0, v_obj.o_invoke_few_args("toObject", 0x048BAB9757BD44B6LL, 0)),assignCallTemp(eo_1, v_base),p_js_ref(p_js_ref(NEWOBJ(c_js_ref)())->create(eo_0, eo_1))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 454 */
void c_jsrt::ti_debug(const char* cls, Object v_obj) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::debug);
  if (LINE(455,x_is_object(v_obj))) echo(toString(LINE(456,v_obj->o_invoke_few_args("toDebug", 0x7112568F140CF4BALL, 0))));
  else echo(LINE(458,concat3("[NOTANOBJECT=", toString(v_obj), "]")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 461 */
Variant c_jsrt::ti_dotv(const char* cls, CVarRef v_base, CVarRef v_prop) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::dotv);
  Variant eo_0;
  return (assignCallTemp(eo_0, LINE(463,c_jsrt::t_dot(v_base, v_prop))),eo_0.toObject().getTyped<c_js_ref>()->o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 466 */
Variant c_jsrt::ti_function_id(const char* cls, CVarRef v_phpname) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::function_id);
  DECLARE_GLOBAL_VARIABLES(g);
  if (isset(g->s_jsrt_DupIdfunctions, v_phpname)) return g->s_jsrt_DupIdfunctions.rvalAt(v_phpname);
  return g->s_jsrt_DupIdundefined;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 471 */
p_js_array c_jsrt::ti_literal_array(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::literal_array);
  Array v_args;
  p_js_array v_array;
  Variant v_arg;

  (v_args = LINE(472,func_get_args(num_args, Array(),args)));
  ((Object)((v_array = ((Object)(LINE(473,p_js_array(p_js_array(NEWOBJ(c_js_array)())->create())))))));
  {
    LOOP_COUNTER(13);
    for (ArrayIter iter15 = v_args.begin("jsrt"); !iter15.end(); ++iter15) {
      LOOP_COUNTER_CHECK(13);
      v_arg = iter15.second();
      {
        LINE(475,v_array->t__push(v_arg));
      }
    }
  }
  return ((Object)(v_array));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 480 */
p_js_object c_jsrt::ti_literal_object(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::literal_object);
  Variant v_args;
  p_js_object v_obj;
  int64 v_i = 0;

  (v_args = LINE(481,func_get_args(num_args, Array(),args)));
  ((Object)((v_obj = ((Object)(LINE(482,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create())))))));
  {
    LOOP_COUNTER(16);
    for ((v_i = 0LL); less(v_i, LINE(483,x_count(v_args))); v_i += 2LL) {
      LOOP_COUNTER_CHECK(16);
      {
        LINE(484,v_obj->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_args.rvalAt(v_i).o_lval("value", 0x69E7413AE0C88471LL), v_args.refvalAt(v_i + 1LL)));
      }
    }
  }
  return ((Object)(v_obj));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 489 */
Variant c_jsrt::ti_this(const char* cls) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::this);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("js_this", 0x3740A81EEE430C82LL));
  if (toBoolean(v_t)) return v_t;
  return g->s_jsrt_DupIdglobal;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 495 */
Variant c_jsrt::ti_expr_assign(const char* cls, Object v_left, Variant v_right, CVarRef v_op //  = null_variant
) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_assign);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  return (assignCallTemp(eo_0, (equal(v_op, null)) ? ((Variant)(v_right)) : ((Variant)(LINE(496,(assignCallTemp(eo_2, ref(v_left->o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0))),assignCallTemp(eo_3, ref(v_right)),c_jsrt::os_invoke("jsrt", toString(v_op), Array(ArrayInit(2).set(0, eo_2).set(1, eo_3).create()), -1)))))),v_left->o_invoke("putValue", Array(ArrayInit(2).set(0, eo_0).set(1, 1LL).create()), 0x33F5B388B34499F7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 499 */
Variant c_jsrt::ti_expr_comma(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_comma);
  return v_b;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 503 */
Variant c_jsrt::ti_expr_plus(const char* cls, Variant v_a, Variant v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_plus);
  (v_a = LINE(504,v_a.o_invoke_few_args("toPrimitive", 0x2F2C83503FD7D62BLL, 0)));
  (v_b = LINE(505,v_b.o_invoke_few_args("toPrimitive", 0x2F2C83503FD7D62BLL, 0)));
  if ((equal(v_a.o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */)) || (equal(v_b.o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */))) {
    (v_a = LINE(507,v_a.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
    (v_b = LINE(508,v_b.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
    return LINE(509,f_js_str(concat(toString(v_a.o_get("value", 0x69E7413AE0C88471LL)), toString(v_b.o_get("value", 0x69E7413AE0C88471LL)))));
  }
  else {
    (v_a = LINE(511,v_a.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
    (v_b = LINE(512,v_b.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
    return LINE(513,f_js_int(v_a.o_get("value", 0x69E7413AE0C88471LL) + v_b.o_get("value", 0x69E7413AE0C88471LL)));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 516 */
Variant c_jsrt::ti_expr_minus(const char* cls, Object v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_minus);
  return LINE(517,f_js_int(minus_rev(v_b->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL), v_a->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 519 */
Variant c_jsrt::ti_expr_divide(const char* cls, Variant v_a, Variant v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_divide);
  DECLARE_GLOBAL_VARIABLES(g);
  (v_a = LINE(520,v_a.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_b = LINE(521,v_b.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if ((LINE(522,x_is_nan(toDouble(v_a)))) || (x_is_nan(toDouble(v_b)))) return g->s_jsrt_DupIdnan;
  if ((LINE(523,x_is_infinite(toDouble(v_a)))) && (x_is_infinite(toDouble(v_b)))) return g->s_jsrt_DupIdnan;
  if (LINE(524,x_is_infinite(toDouble(v_a)))) return g->s_jsrt_DupIdinfinity;
  if (LINE(525,x_is_infinite(toDouble(v_b)))) return g->s_jsrt_DupIdzero;
  if ((equal(v_a, 0LL)) && (equal(v_b, 0LL))) return g->s_jsrt_DupIdnan;
  if (equal(v_b, 0LL)) return g->s_jsrt_DupIdinfinity;
  return (silenceInc(), silenceDec(LINE(528,f_js_int(divide(v_a, v_b)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 530 */
Variant c_jsrt::ti_expr_multiply(const char* cls, Object v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_multiply);
  return LINE(531,f_js_int(multiply_rev(v_b->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL), v_a->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 533 */
Variant c_jsrt::ti_expr_modulo(const char* cls, Object v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_modulo);
  return LINE(534,f_js_int(modulo_rev(toInt64(v_b->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)), toInt64(v_a->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 536 */
Variant c_jsrt::ti_expr_post_pp(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_post_pp);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return (assignCallTemp(eo_0, ref(LINE(537,f_js_int((assignCallTemp(eo_2, toObject(v_a->o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0))),eo_2.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL) + 1LL)))),v_a->o_invoke("putValue", Array(ArrayInit(2).set(0, eo_0).set(1, 2LL).create()), 0x33F5B388B34499F7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 539 */
Variant c_jsrt::ti_expr_post_mm(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_post_mm);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return (assignCallTemp(eo_0, ref(LINE(540,f_js_int((assignCallTemp(eo_2, toObject(v_a->o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0))),eo_2.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL) - 1LL)))),v_a->o_invoke("putValue", Array(ArrayInit(2).set(0, eo_0).set(1, 2LL).create()), 0x33F5B388B34499F7LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 542 */
Variant c_jsrt::ti_expr_delete(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_delete);
  DECLARE_GLOBAL_VARIABLES(g);
  if (!((instanceOf(v_a, "js_ref")))) return g->s_jsrt_DupIdtrue;
  (g->s_jsrt_DupIdidcache = ScalarArrays::sa_[0]);
  return LINE(546,v_a.o_get("base", 0x56FD8ECCBDFDD7A7LL).o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_a.o_lval("propName", 0x4DC47FEB2126D745LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 548 */
Variant c_jsrt::ti_expr_void(const char* cls, CVarRef v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_void);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->s_jsrt_DupIdundefined;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 551 */
Variant c_jsrt::ti_expr_typeof(const char* cls, Variant v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_typeof);
  DECLARE_GLOBAL_VARIABLES(g);
  if (instanceOf(v_a, "js_ref")) {
    if (equal(v_a.o_get("base", 0x56FD8ECCBDFDD7A7LL), null)) return g->s_jsrt_DupIdundefined;
  }
  (v_a = LINE(555,v_a.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)));
  {
    switch (toInt64(v_a.o_get("type", 0x508FC7C8724A760ALL))) {
    case 0LL:
      {
        return LINE(557,f_js_str("undefined"));
      }
    case 1LL:
      {
        return LINE(558,f_js_str("object"));
      }
    case 2LL:
      {
        return LINE(559,f_js_str("boolean"));
      }
    case 3LL:
      {
        return LINE(560,f_js_str("number"));
      }
    case 4LL:
      {
        return LINE(561,f_js_str("string"));
      }
    case 5LL:
      {
        if (instanceOf(v_a, "js_function")) {
          return LINE(564,f_js_str("function"));
        }
        else {
          return LINE(566,f_js_str("object"));
        }
      }
    }
  }
  return LINE(569,f_js_str("unknown"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 571 */
Variant c_jsrt::ti_expr_pre_pp(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_pre_pp);
  Variant eo_0;
  Variant v_v;

  (v_v = (assignCallTemp(eo_0, toObject(LINE(572,v_a->o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)))),eo_0.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
  (v_v = LINE(573,f_js_int(v_v.o_get("value", 0x69E7413AE0C88471LL) + 1LL)));
  LINE(574,v_a->o_invoke_few_args("putValue", 0x33F5B388B34499F7LL, 1, v_v));
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 577 */
Variant c_jsrt::ti_expr_pre_mm(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_pre_mm);
  Variant eo_0;
  Variant v_v;

  (v_v = (assignCallTemp(eo_0, toObject(LINE(578,v_a->o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)))),eo_0.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
  (v_v = LINE(579,f_js_int(v_v.o_get("value", 0x69E7413AE0C88471LL) - 1LL)));
  LINE(580,v_a->o_invoke_few_args("putValue", 0x33F5B388B34499F7LL, 1, v_v));
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 583 */
Variant c_jsrt::ti_expr_u_plus(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_u_plus);
  return LINE(584,v_a->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 586 */
Variant c_jsrt::ti_expr_u_minus(const char* cls, CVarRef v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_u_minus);
  Variant v_v;

  (v_v = LINE(587,toObject(v_a)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
  if (!(LINE(588,x_is_nan(toDouble(v_v.o_get("value", 0x69E7413AE0C88471LL)))))) {
    (v_v = LINE(589,f_js_int(negate(v_v.o_get("value", 0x69E7413AE0C88471LL)))));
  }
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 593 */
Variant c_jsrt::ti_expr_bit_not(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_bit_not);
  return LINE(594,f_js_int(~v_a->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 596 */
Variant c_jsrt::ti_expr_not(const char* cls, Object v_a) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_not);
  DECLARE_GLOBAL_VARIABLES(g);
  return (toBoolean(LINE(597,v_a->o_invoke_few_args("toBoolean", 0x52B7A8139051425DLL, 0)).o_get("value", 0x69E7413AE0C88471LL))) ? ((Variant)(g->s_jsrt_DupIdfalse)) : ((Variant)(g->s_jsrt_DupIdtrue));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 599 */
Variant c_jsrt::ti_expr_lsh(const char* cls, Variant v_a, Variant v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_lsh);
  Variant v_v;

  (v_a = LINE(600,v_a.o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0)));
  (v_b = LINE(601,v_b.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)));
  (v_v = LINE(602,f_js_int(toInt64(toInt64(v_a.o_get("value", 0x69E7413AE0C88471LL))) << (toInt64(bitwise_and(v_b.o_get("value", 0x69E7413AE0C88471LL), 31LL))))));
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 606 */
Variant c_jsrt::ti_expr_rsh(const char* cls, Object v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_rsh);
  return LINE(607,f_js_int(shift_right_rev(toInt64((toInt64(bitwise_and(v_b->o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0).o_get("value", 0x69E7413AE0C88471LL), 31LL)))), toInt64(v_a->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 609 */
Variant c_jsrt::ti_expr_ursh(const char* cls, Variant v_a, Variant v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_ursh);
  double v_i = 0.0;
  double v_k = 0.0;
  int64 v_c = 0;

  (v_a = LINE(610,v_a.o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_b = LINE(611,v_b.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_i = toDouble(toInt64(toInt64(v_a)) >> (toInt64(bitwise_and(v_b, 31LL)))));
  (v_k = 0.0);
  {
    LOOP_COUNTER(18);
    for ((v_c = 0LL); less(v_c, v_b); v_c++) {
      LOOP_COUNTER_CHECK(18);
      {
        v_i &= ~v_k;
        v_k >>= 1LL;
      }
    }
  }
  return LINE(620,f_js_int(v_i));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 622 */
Variant c_jsrt::ti_expr_lt(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_lt);
  return LINE(623,c_jsrt::t_cmp(v_a, v_b, 1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 625 */
Variant c_jsrt::ti_expr_gt(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_gt);
  return LINE(626,c_jsrt::t_cmp(v_b, v_a, 1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 628 */
Variant c_jsrt::ti_expr_lte(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_lte);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_v;

  (v_v = LINE(629,c_jsrt::t_cmp(v_b, v_a)));
  if ((equal(v_v, g->s_jsrt_DupIdtrue)) || (equal(v_v, g->s_jsrt_DupIdundefined))) return g->s_jsrt_DupIdfalse;
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 633 */
Variant c_jsrt::ti_expr_gte(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_gte);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_v;

  (v_v = LINE(634,c_jsrt::t_cmp(v_a, v_b)));
  if ((equal(v_v, g->s_jsrt_DupIdtrue)) || (equal(v_v, g->s_jsrt_DupIdundefined))) return g->s_jsrt_DupIdfalse;
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 638 */
Variant c_jsrt::ti_cmp(const char* cls, Variant v_a, Variant v_b, int64 v_f //  = 0LL
) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::cmp);
  DECLARE_GLOBAL_VARIABLES(g);
  (v_a = LINE(639,v_a.o_invoke_few_args("toPrimitive", 0x2F2C83503FD7D62BLL, 1, 3LL /* js_val::NUMBER */)));
  (v_b = LINE(640,v_b.o_invoke_few_args("toPrimitive", 0x2F2C83503FD7D62BLL, 1, 3LL /* js_val::NUMBER */)));
  if ((equal(v_a.o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */)) && (equal(v_b.o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */))) {
    if (same(LINE(642,x_strpos(toString(v_a.o_get("value", 0x69E7413AE0C88471LL)), v_b.o_get("value", 0x69E7413AE0C88471LL))), 0LL)) return g->s_jsrt_DupIdfalse;
    if (same(LINE(643,x_strpos(toString(v_b.o_get("value", 0x69E7413AE0C88471LL)), v_a.o_get("value", 0x69E7413AE0C88471LL))), 0LL)) return g->s_jsrt_DupIdtrue;
    return (less(v_a, v_b)) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse));
  }
  else {
    (v_a = LINE(646,v_a.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
    (v_b = LINE(647,v_b.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
    if ((LINE(648,x_is_nan(toDouble(v_a.o_get("value", 0x69E7413AE0C88471LL))))) || (x_is_nan(toDouble(v_b.o_get("value", 0x69E7413AE0C88471LL))))) return toBoolean(v_f) ? ((Variant)(g->s_jsrt_DupIdfalse)) : ((Variant)(g->s_jsrt_DupIdundefined));
    if (equal(v_a.o_get("value", 0x69E7413AE0C88471LL), v_b.o_get("value", 0x69E7413AE0C88471LL))) return g->s_jsrt_DupIdfalse;
    return (less(v_a.o_get("value", 0x69E7413AE0C88471LL), v_b.o_get("value", 0x69E7413AE0C88471LL))) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 659 */
Variant c_jsrt::ti_expr_instanceof(const char* cls, Variant v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_instanceof);
  DECLARE_GLOBAL_VARIABLES(g);
  if (!equal(v_b.o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */)) {
    echo(LINE(661,concat3("ERROR: TypeError Exception at line 661 in file ", get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php"), "<hr>")));
    return g->s_jsrt_DupIdundefined;
  }
  return LINE(664,v_b->o_invoke_few_args("hasInstance", 0x3B85D7AB6D6705F9LL, 1, v_a));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 666 */
Variant c_jsrt::ti_expr_in(const char* cls, Variant v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_in);
  DECLARE_GLOBAL_VARIABLES(g);
  if (!equal(v_b.o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */)) {
    echo(LINE(668,concat3("ERROR: TypeError Exception at line 668 in file ", get_source_filename("phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php"), "<hr>")));
    return g->s_jsrt_DupIdundefined;
  }
  (v_a = LINE(671,v_a.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
  return LINE(672,v_b->o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_a));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 674 */
Variant c_jsrt::ti_expr_equal(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_equal);
  return LINE(675,c_jsrt::t_abstract_equal(v_a, v_b));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 677 */
Variant c_jsrt::ti_expr_not_equal(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_not_equal);
  DECLARE_GLOBAL_VARIABLES(g);
  return toBoolean(LINE(678,c_jsrt::t_abstract_equal(v_a, v_b)).o_get("value", 0x69E7413AE0C88471LL)) ? ((Variant)(g->s_jsrt_DupIdfalse)) : ((Variant)(g->s_jsrt_DupIdtrue));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 680 */
Variant c_jsrt::ti_abstract_equal(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::abstract_equal);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  if (!equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), toObject(v_b).o_get("type", 0x508FC7C8724A760ALL))) {
    if ((equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 0LL /* js_val::UNDEFINED */)) && (equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 1LL /* js_val::NULL */))) return g->s_jsrt_DupIdtrue;
    if ((equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 0LL /* js_val::UNDEFINED */)) && (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 1LL /* js_val::NULL */))) return g->s_jsrt_DupIdtrue;
    if ((equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 3LL /* js_val::NUMBER */)) && (equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */))) return LINE(684,(assignCallTemp(eo_0, v_a),assignCallTemp(eo_1, toObject(v_b)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)),c_jsrt::t_abstract_equal(eo_0, eo_1)));
    if ((equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 3LL /* js_val::NUMBER */)) && (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */))) return LINE(685,(assignCallTemp(eo_0, toObject(v_a)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)),assignCallTemp(eo_1, v_b),c_jsrt::t_abstract_equal(eo_0, eo_1)));
    if (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 2LL /* js_val::BOOLEAN */)) return LINE(686,(assignCallTemp(eo_0, toObject(v_a)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)),assignCallTemp(eo_1, v_b),c_jsrt::t_abstract_equal(eo_0, eo_1)));
    if (equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 2LL /* js_val::BOOLEAN */)) return LINE(687,(assignCallTemp(eo_0, v_a),assignCallTemp(eo_1, toObject(v_b)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)),c_jsrt::t_abstract_equal(eo_0, eo_1)));
    if ((((equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 3LL /* js_val::NUMBER */)) || (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */)))) && (equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */))) return LINE(688,(assignCallTemp(eo_0, v_a),assignCallTemp(eo_1, toObject(v_b)->o_invoke_few_args("toPrimitive", 0x2F2C83503FD7D62BLL, 0)),c_jsrt::t_abstract_equal(eo_0, eo_1)));
    if ((((equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 3LL /* js_val::NUMBER */)) || (equal(toObject(v_b).o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */)))) && (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */))) return LINE(689,(assignCallTemp(eo_0, toObject(v_a)->o_invoke_few_args("toPrimitive", 0x2F2C83503FD7D62BLL, 0)),assignCallTemp(eo_1, v_b),c_jsrt::t_abstract_equal(eo_0, eo_1)));
    return g->s_jsrt_DupIdfalse;
  }
  else {
    if (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 0LL /* js_val::UNDEFINED */)) return g->s_jsrt_DupIdtrue;
    if (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 1LL /* js_val::NULL */)) return g->s_jsrt_DupIdtrue;
    if (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 3LL /* js_val::NUMBER */)) {
      if ((LINE(695,x_is_nan(toDouble(toObject(v_a).o_get("value", 0x69E7413AE0C88471LL))))) || (x_is_nan(toDouble(toObject(v_b).o_get("value", 0x69E7413AE0C88471LL))))) return g->s_jsrt_DupIdfalse;
    }
    if (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */)) {
      return (same(v_a, v_b)) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse));
    }
    return (equal(toObject(v_a).o_get("value", 0x69E7413AE0C88471LL), toObject(v_b).o_get("value", 0x69E7413AE0C88471LL))) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 703 */
Variant c_jsrt::ti_expr_strict_equal(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_strict_equal);
  Variant v_v;

  (v_v = LINE(704,c_jsrt::t_strict_equal(v_a, v_b)));
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 707 */
Variant c_jsrt::ti_expr_strict_not_equal(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_strict_not_equal);
  DECLARE_GLOBAL_VARIABLES(g);
  return toBoolean(LINE(708,c_jsrt::t_strict_equal(v_a, v_b)).o_get("value", 0x69E7413AE0C88471LL)) ? ((Variant)(g->s_jsrt_DupIdfalse)) : ((Variant)(g->s_jsrt_DupIdtrue));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 710 */
Variant c_jsrt::ti_strict_equal(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::strict_equal);
  DECLARE_GLOBAL_VARIABLES(g);
  if (!equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), toObject(v_b).o_get("type", 0x508FC7C8724A760ALL))) return g->s_jsrt_DupIdfalse;
  if ((equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 0LL /* js_val::UNDEFINED */)) || (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 1LL /* js_val::NULL */))) return g->s_jsrt_DupIdtrue;
  if (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 3LL /* js_val::NUMBER */)) {
    if ((LINE(714,x_is_nan(toDouble(toObject(v_a).o_get("value", 0x69E7413AE0C88471LL))))) || (x_is_nan(toDouble(toObject(v_b).o_get("value", 0x69E7413AE0C88471LL))))) return g->s_jsrt_DupIdfalse;
  }
  if (equal(toObject(v_a).o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */)) {
    return (same(v_a, v_b)) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse));
  }
  return (equal(toObject(v_a).o_get("value", 0x69E7413AE0C88471LL), toObject(v_b).o_get("value", 0x69E7413AE0C88471LL))) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 721 */
Variant c_jsrt::ti_expr_bit_and(const char* cls, Object v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_bit_and);
  return LINE(722,f_js_int(bitwise_and_rev(v_b->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL), v_a->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 724 */
Variant c_jsrt::ti_expr_bit_xor(const char* cls, Object v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_bit_xor);
  return LINE(725,f_js_int(bitwise_xor_rev(v_b->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL), v_a->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 727 */
Variant c_jsrt::ti_expr_bit_or(const char* cls, Object v_a, Object v_b) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::expr_bit_or);
  return LINE(728,f_js_int(bitwise_or_rev(v_b->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL), v_a->o_invoke_few_args("toInt32", 0x2994F14A3D82023FLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 730 */
void c_jsrt::ti_write(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(jsrt, jsrt::write);
  Array v_args;
  Variant v_arg;
  Variant v_s;

  (v_args = LINE(731,func_get_args(num_args, Array(),args)));
  {
    LOOP_COUNTER(19);
    for (ArrayIter iter21 = v_args.begin("jsrt"); !iter21.end(); ++iter21) {
      LOOP_COUNTER_CHECK(19);
      v_arg = iter21.second();
      {
        (v_s = LINE(733,v_arg.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
        echo(toString(v_s.o_get("value", 0x69E7413AE0C88471LL)));
      }
    }
  }
  LINE(737,x_flush());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2398 */
Variant c_js_referenceerror::os_get(const char *s, int64 hash) {
  return c_js_error::os_get(s, hash);
}
Variant &c_js_referenceerror::os_lval(const char *s, int64 hash) {
  return c_js_error::os_lval(s, hash);
}
void c_js_referenceerror::o_get(ArrayElementVec &props) const {
  c_js_error::o_get(props);
}
bool c_js_referenceerror::o_exists(CStrRef s, int64 hash) const {
  return c_js_error::o_exists(s, hash);
}
Variant c_js_referenceerror::o_get(CStrRef s, int64 hash) {
  return c_js_error::o_get(s, hash);
}
Variant c_js_referenceerror::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_error::o_set(s, hash, v, forInit);
}
Variant &c_js_referenceerror::o_lval(CStrRef s, int64 hash) {
  return c_js_error::o_lval(s, hash);
}
Variant c_js_referenceerror::os_constant(const char *s) {
  return c_js_error::os_constant(s);
}
IMPLEMENT_CLASS(js_referenceerror)
ObjectData *c_js_referenceerror::create(Variant v_msg //  = ""
) {
  init();
  t___construct(v_msg);
  return this;
}
ObjectData *c_js_referenceerror::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_referenceerror::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_referenceerror::cloneImpl() {
  c_js_referenceerror *obj = NEW(c_js_referenceerror)();
  cloneSet(obj);
  return obj;
}
void c_js_referenceerror::cloneSet(c_js_referenceerror *clone) {
  c_js_error::cloneSet(clone);
}
Variant c_js_referenceerror::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke(s, params, hash, fatal);
}
Variant c_js_referenceerror::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_referenceerror::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_error::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_referenceerror$os_get(const char *s) {
  return c_js_referenceerror::os_get(s, -1);
}
Variant &cw_js_referenceerror$os_lval(const char *s) {
  return c_js_referenceerror::os_lval(s, -1);
}
Variant cw_js_referenceerror$os_constant(const char *s) {
  return c_js_referenceerror::os_constant(s);
}
Variant cw_js_referenceerror$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_referenceerror::os_invoke(c, s, params, -1, fatal);
}
void c_js_referenceerror::init() {
  c_js_error::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2399 */
void c_js_referenceerror::t___construct(Variant v_msg //  = ""
) {
  INSTANCE_METHOD_INJECTION(js_referenceerror, js_referenceerror::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(2400,c_js_error::t___construct("ReferenceError", g->s_jsrt_DupIdproto_referenceerror, v_msg));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2405 */
p_js_referenceerror c_js_referenceerror::ti_object(const char* cls, CVarRef v_message) {
  STATIC_METHOD_INJECTION(js_referenceerror, js_referenceerror::object);
  return ((Object)(LINE(2406,p_js_referenceerror(p_js_referenceerror(NEWOBJ(c_js_referenceerror)())->create(toObject(v_message)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0).o_get("value", 0x69E7413AE0C88471LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1924 */
Variant c_js_date::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_date::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_date::o_get(ArrayElementVec &props) const {
  c_js_object::o_get(props);
}
bool c_js_date::o_exists(CStrRef s, int64 hash) const {
  return c_js_object::o_exists(s, hash);
}
Variant c_js_date::o_get(CStrRef s, int64 hash) {
  return c_js_object::o_get(s, hash);
}
Variant c_js_date::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_date::o_lval(CStrRef s, int64 hash) {
  return c_js_object::o_lval(s, hash);
}
Variant c_js_date::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_date)
ObjectData *c_js_date::create(Variant v_y //  = null
, Variant v_m //  = null
, Variant v_d //  = null
, Variant v_h //  = null
, Variant v_mn //  = null
, Variant v_s //  = null
, Variant v_ms //  = null
) {
  init();
  t___construct(v_y, v_m, v_d, v_h, v_mn, v_s, v_ms);
  return this;
}
ObjectData *c_js_date::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    if (count == 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    if (count == 4) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
    if (count == 5) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
    if (count == 6) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5), params.rvalAt(6)));
  } else return this;
}
void c_js_date::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  if (count == 3) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  if (count == 4) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  if (count == 5) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
  if (count == 6) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5), params.rvalAt(6)));
}
ObjectData *c_js_date::cloneImpl() {
  c_js_date *obj = NEW(c_js_date)();
  cloneSet(obj);
  return obj;
}
void c_js_date::cloneSet(c_js_date *clone) {
  c_js_object::cloneSet(clone);
}
Variant c_js_date::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 255) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x7BCF5E5732E35C07LL, setutcdate) {
        return (ti_setutcdate(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x3B9A0931F841B90ALL, getutcmilliseconds) {
        return (ti_getutcmilliseconds(o_getClassName()));
      }
      break;
    case 11:
      HASH_GUARD(0x67314943DC9D120BLL, setdate) {
        return (ti_setdate(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 19:
      HASH_GUARD(0x72E1BF7CA22BA613LL, setutcminutes) {
        return (ti_setutcminutes(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 20:
      HASH_GUARD(0x6FB9D84AA364CA14LL, utc) {
        return (ti_utc(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5), params.rvalAt(6)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 46:
      HASH_GUARD(0x4949BB2FE0E7A62ELL, tolocaledatestring) {
        return (ti_tolocaledatestring(o_getClassName()));
      }
      break;
    case 55:
      HASH_GUARD(0x2906684B6D458F37LL, setfullyear) {
        return (ti_setfullyear(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        int count = params.size();
        if (count <= 1) return (ti_object(o_getClassName(),count, params.rvalAt(0)));
        return (ti_object(o_getClassName(),count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 74:
      HASH_GUARD(0x41EECC85F1C3E44ALL, getday) {
        return (ti_getday(o_getClassName()));
      }
      break;
    case 80:
      HASH_GUARD(0x5323D09605613D50LL, getutcfullyear) {
        return (ti_getutcfullyear(o_getClassName()));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 88:
      HASH_GUARD(0x07CB96AB34F4C258LL, settime) {
        return (ti_settime(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        if (count == 3) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        if (count == 4) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
        if (count == 5) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
        if (count == 6) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5), params.rvalAt(6)), null);
      }
      break;
    case 97:
      HASH_GUARD(0x06070A0F2DE05961LL, getutcseconds) {
        return (ti_getutcseconds(o_getClassName()));
      }
      break;
    case 104:
      HASH_GUARD(0x05E9B0E5FBBEC068LL, tolocaletimestring) {
        return (ti_tolocaletimestring(o_getClassName()));
      }
      break;
    case 106:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 111:
      HASH_GUARD(0x02C705749FEB756FLL, gethours) {
        return (ti_gethours(o_getClassName()));
      }
      break;
    case 118:
      HASH_GUARD(0x4830004EEE242276LL, getutcdate) {
        return (ti_getutcdate(o_getClassName()));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    case 127:
      HASH_GUARD(0x4A602DA5B6E9F97FLL, setutcfullyear) {
        return (ti_setutcfullyear(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 129:
      HASH_GUARD(0x5DEBBDB8535DB881LL, gettime) {
        return (ti_gettime(o_getClassName()));
      }
      break;
    case 143:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      HASH_GUARD(0x2841BB7394AB228FLL, setmonth) {
        return (ti_setmonth(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 147:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x7BE3176D8F003A93LL, getutcmonth) {
        return (ti_getutcmonth(o_getClassName()));
      }
      break;
    case 154:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 158:
      HASH_GUARD(0x30D59793EBC0589ELL, setutcseconds) {
        return (ti_setutcseconds(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 159:
      HASH_GUARD(0x6282A0AA3869649FLL, getutchours) {
        return (ti_getutchours(o_getClassName()));
      }
      break;
    case 164:
      HASH_GUARD(0x1270E6FC678E49A4LL, toutcstring) {
        return (ti_toutcstring(o_getClassName()));
      }
      break;
    case 169:
      HASH_GUARD(0x77FCA713F5EBD6A9LL, sethours) {
        return (ti_sethours(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 172:
      HASH_GUARD(0x3BA8EC70545E75ACLL, getseconds) {
        return (ti_getseconds(o_getClassName()));
      }
      break;
    case 175:
      HASH_GUARD(0x65497D63C0D716AFLL, getdate) {
        return (ti_getdate(o_getClassName()));
      }
      break;
    case 176:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 178:
      HASH_GUARD(0x2DBB256B15EB31B2LL, gettimezoneoffset) {
        return (ti_gettimezoneoffset(o_getClassName()));
      }
      break;
    case 182:
      HASH_GUARD(0x3EF3FB4482E629B6LL, getmonth) {
        return (ti_getmonth(o_getClassName()));
      }
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 184:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 186:
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 191:
      HASH_GUARD(0x196F0A19A95866BFLL, setminutes) {
        return (ti_setminutes(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 194:
      HASH_GUARD(0x178BE502D6BA95C2LL, setmilliseconds) {
        return (ti_setmilliseconds(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 197:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 200:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 204:
      HASH_GUARD(0x67A605DDB46412CCLL, tolocalestring) {
        return (ti_tolocalestring(o_getClassName()));
      }
      break;
    case 211:
      HASH_GUARD(0x31414C3C64868CD3LL, getminutes) {
        return (ti_getminutes(o_getClassName()));
      }
      HASH_GUARD(0x2C7655DAA03982D3LL, totimestring) {
        return (ti_totimestring(o_getClassName()));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      break;
    case 213:
      HASH_GUARD(0x4928FC6250BE53D5LL, getfullyear) {
        return (ti_getfullyear(o_getClassName()));
      }
      break;
    case 218:
      HASH_GUARD(0x48072BFBFEDC9FDALL, setutcmonth) {
        return (ti_setutcmonth(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 220:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 221:
      HASH_GUARD(0x4AEB07E028F65EDDLL, setutchours) {
        return (ti_setutchours(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 222:
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        return (ti_parse(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 227:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 231:
      HASH_GUARD(0x492C310A8BE20BE7LL, setutcmilliseconds) {
        return (ti_setutcmilliseconds(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 234:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x2B481A7942D2A5EALL, getutcday) {
        return (ti_getutcday(o_getClassName()));
      }
      break;
    case 235:
      HASH_GUARD(0x6F71E01BEFE6AFEBLL, setseconds) {
        return (ti_setseconds(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 244:
      HASH_GUARD(0x530B9A14D9ADE0F4LL, todatestring) {
        return (ti_todatestring(o_getClassName()));
      }
      break;
    case 247:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 250:
      HASH_GUARD(0x4E64ABB986713BFALL, getutcminutes) {
        return (ti_getutcminutes(o_getClassName()));
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_date::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 255) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x7BCF5E5732E35C07LL, setutcdate) {
        return (ti_setutcdate(o_getClassName(), a0));
      }
      break;
    case 10:
      HASH_GUARD(0x3B9A0931F841B90ALL, getutcmilliseconds) {
        return (ti_getutcmilliseconds(o_getClassName()));
      }
      break;
    case 11:
      HASH_GUARD(0x67314943DC9D120BLL, setdate) {
        return (ti_setdate(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 19:
      HASH_GUARD(0x72E1BF7CA22BA613LL, setutcminutes) {
        return (ti_setutcminutes(o_getClassName(), a0, a1, a2));
      }
      break;
    case 20:
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 46:
      HASH_GUARD(0x4949BB2FE0E7A62ELL, tolocaledatestring) {
        return (ti_tolocaledatestring(o_getClassName()));
      }
      break;
    case 55:
      HASH_GUARD(0x2906684B6D458F37LL, setfullyear) {
        return (ti_setfullyear(o_getClassName(), a0, a1, a2));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        if (count <= 1) return (ti_object(o_getClassName(),count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_object(o_getClassName(),count,a0, params));
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 74:
      HASH_GUARD(0x41EECC85F1C3E44ALL, getday) {
        return (ti_getday(o_getClassName()));
      }
      break;
    case 80:
      HASH_GUARD(0x5323D09605613D50LL, getutcfullyear) {
        return (ti_getutcfullyear(o_getClassName()));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 88:
      HASH_GUARD(0x07CB96AB34F4C258LL, settime) {
        return (ti_settime(o_getClassName(), a0));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        if (count == 3) return (t___construct(a0, a1, a2), null);
        if (count == 4) return (t___construct(a0, a1, a2, a3), null);
        if (count == 5) return (t___construct(a0, a1, a2, a3, a4), null);
        return (t___construct(a0, a1, a2, a3, a4, a5), null);
      }
      break;
    case 97:
      HASH_GUARD(0x06070A0F2DE05961LL, getutcseconds) {
        return (ti_getutcseconds(o_getClassName()));
      }
      break;
    case 104:
      HASH_GUARD(0x05E9B0E5FBBEC068LL, tolocaletimestring) {
        return (ti_tolocaletimestring(o_getClassName()));
      }
      break;
    case 106:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 111:
      HASH_GUARD(0x02C705749FEB756FLL, gethours) {
        return (ti_gethours(o_getClassName()));
      }
      break;
    case 118:
      HASH_GUARD(0x4830004EEE242276LL, getutcdate) {
        return (ti_getutcdate(o_getClassName()));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    case 127:
      HASH_GUARD(0x4A602DA5B6E9F97FLL, setutcfullyear) {
        return (ti_setutcfullyear(o_getClassName(), a0, a1, a2));
      }
      break;
    case 129:
      HASH_GUARD(0x5DEBBDB8535DB881LL, gettime) {
        return (ti_gettime(o_getClassName()));
      }
      break;
    case 143:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      HASH_GUARD(0x2841BB7394AB228FLL, setmonth) {
        return (ti_setmonth(o_getClassName(), a0, a1));
      }
      break;
    case 147:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x7BE3176D8F003A93LL, getutcmonth) {
        return (ti_getutcmonth(o_getClassName()));
      }
      break;
    case 154:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 158:
      HASH_GUARD(0x30D59793EBC0589ELL, setutcseconds) {
        return (ti_setutcseconds(o_getClassName(), a0, a1));
      }
      break;
    case 159:
      HASH_GUARD(0x6282A0AA3869649FLL, getutchours) {
        return (ti_getutchours(o_getClassName()));
      }
      break;
    case 164:
      HASH_GUARD(0x1270E6FC678E49A4LL, toutcstring) {
        return (ti_toutcstring(o_getClassName()));
      }
      break;
    case 169:
      HASH_GUARD(0x77FCA713F5EBD6A9LL, sethours) {
        return (ti_sethours(o_getClassName(), a0, a1, a2, a3));
      }
      break;
    case 172:
      HASH_GUARD(0x3BA8EC70545E75ACLL, getseconds) {
        return (ti_getseconds(o_getClassName()));
      }
      break;
    case 175:
      HASH_GUARD(0x65497D63C0D716AFLL, getdate) {
        return (ti_getdate(o_getClassName()));
      }
      break;
    case 176:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 178:
      HASH_GUARD(0x2DBB256B15EB31B2LL, gettimezoneoffset) {
        return (ti_gettimezoneoffset(o_getClassName()));
      }
      break;
    case 182:
      HASH_GUARD(0x3EF3FB4482E629B6LL, getmonth) {
        return (ti_getmonth(o_getClassName()));
      }
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 184:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 186:
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 191:
      HASH_GUARD(0x196F0A19A95866BFLL, setminutes) {
        return (ti_setminutes(o_getClassName(), a0, a1, a2));
      }
      break;
    case 194:
      HASH_GUARD(0x178BE502D6BA95C2LL, setmilliseconds) {
        return (ti_setmilliseconds(o_getClassName(), a0));
      }
      break;
    case 197:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 200:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 204:
      HASH_GUARD(0x67A605DDB46412CCLL, tolocalestring) {
        return (ti_tolocalestring(o_getClassName()));
      }
      break;
    case 211:
      HASH_GUARD(0x31414C3C64868CD3LL, getminutes) {
        return (ti_getminutes(o_getClassName()));
      }
      HASH_GUARD(0x2C7655DAA03982D3LL, totimestring) {
        return (ti_totimestring(o_getClassName()));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      break;
    case 213:
      HASH_GUARD(0x4928FC6250BE53D5LL, getfullyear) {
        return (ti_getfullyear(o_getClassName()));
      }
      break;
    case 218:
      HASH_GUARD(0x48072BFBFEDC9FDALL, setutcmonth) {
        return (ti_setutcmonth(o_getClassName(), a0, a1));
      }
      break;
    case 220:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 221:
      HASH_GUARD(0x4AEB07E028F65EDDLL, setutchours) {
        return (ti_setutchours(o_getClassName(), a0, a1, a2, a3));
      }
      break;
    case 222:
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        return (ti_parse(o_getClassName(), a0));
      }
      break;
    case 227:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 231:
      HASH_GUARD(0x492C310A8BE20BE7LL, setutcmilliseconds) {
        return (ti_setutcmilliseconds(o_getClassName(), a0));
      }
      break;
    case 234:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x2B481A7942D2A5EALL, getutcday) {
        return (ti_getutcday(o_getClassName()));
      }
      break;
    case 235:
      HASH_GUARD(0x6F71E01BEFE6AFEBLL, setseconds) {
        return (ti_setseconds(o_getClassName(), a0, a1));
      }
      break;
    case 244:
      HASH_GUARD(0x530B9A14D9ADE0F4LL, todatestring) {
        return (ti_todatestring(o_getClassName()));
      }
      break;
    case 247:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 250:
      HASH_GUARD(0x4E64ABB986713BFALL, getutcminutes) {
        return (ti_getutcminutes(o_getClassName()));
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_date::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 1:
      HASH_GUARD(0x5DEBBDB8535DB881LL, gettime) {
        return (ti_gettime(c));
      }
      break;
    case 7:
      HASH_GUARD(0x7BCF5E5732E35C07LL, setutcdate) {
        return (ti_setutcdate(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x3B9A0931F841B90ALL, getutcmilliseconds) {
        return (ti_getutcmilliseconds(c));
      }
      break;
    case 11:
      HASH_GUARD(0x67314943DC9D120BLL, setdate) {
        return (ti_setdate(c, params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x2841BB7394AB228FLL, setmonth) {
        return (ti_setmonth(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 19:
      HASH_GUARD(0x72E1BF7CA22BA613LL, setutcminutes) {
        return (ti_setutcminutes(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x7BE3176D8F003A93LL, getutcmonth) {
        return (ti_getutcmonth(c));
      }
      break;
    case 20:
      HASH_GUARD(0x6FB9D84AA364CA14LL, utc) {
        return (ti_utc(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5), params.rvalAt(6)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 30:
      HASH_GUARD(0x30D59793EBC0589ELL, setutcseconds) {
        return (ti_setutcseconds(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 31:
      HASH_GUARD(0x6282A0AA3869649FLL, getutchours) {
        return (ti_getutchours(c));
      }
      break;
    case 36:
      HASH_GUARD(0x1270E6FC678E49A4LL, toutcstring) {
        return (ti_toutcstring(c));
      }
      break;
    case 41:
      HASH_GUARD(0x77FCA713F5EBD6A9LL, sethours) {
        return (ti_sethours(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 44:
      HASH_GUARD(0x3BA8EC70545E75ACLL, getseconds) {
        return (ti_getseconds(c));
      }
      break;
    case 46:
      HASH_GUARD(0x4949BB2FE0E7A62ELL, tolocaledatestring) {
        return (ti_tolocaledatestring(c));
      }
      break;
    case 47:
      HASH_GUARD(0x65497D63C0D716AFLL, getdate) {
        return (ti_getdate(c));
      }
      break;
    case 50:
      HASH_GUARD(0x2DBB256B15EB31B2LL, gettimezoneoffset) {
        return (ti_gettimezoneoffset(c));
      }
      break;
    case 54:
      HASH_GUARD(0x3EF3FB4482E629B6LL, getmonth) {
        return (ti_getmonth(c));
      }
      break;
    case 55:
      HASH_GUARD(0x2906684B6D458F37LL, setfullyear) {
        return (ti_setfullyear(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        int count = params.size();
        if (count <= 1) return (ti_object(c,count, params.rvalAt(0)));
        return (ti_object(c,count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 63:
      HASH_GUARD(0x196F0A19A95866BFLL, setminutes) {
        return (ti_setminutes(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 66:
      HASH_GUARD(0x178BE502D6BA95C2LL, setmilliseconds) {
        return (ti_setmilliseconds(c, params.rvalAt(0)));
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 74:
      HASH_GUARD(0x41EECC85F1C3E44ALL, getday) {
        return (ti_getday(c));
      }
      break;
    case 76:
      HASH_GUARD(0x67A605DDB46412CCLL, tolocalestring) {
        return (ti_tolocalestring(c));
      }
      break;
    case 80:
      HASH_GUARD(0x5323D09605613D50LL, getutcfullyear) {
        return (ti_getutcfullyear(c));
      }
      break;
    case 83:
      HASH_GUARD(0x31414C3C64868CD3LL, getminutes) {
        return (ti_getminutes(c));
      }
      HASH_GUARD(0x2C7655DAA03982D3LL, totimestring) {
        return (ti_totimestring(c));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 85:
      HASH_GUARD(0x4928FC6250BE53D5LL, getfullyear) {
        return (ti_getfullyear(c));
      }
      break;
    case 88:
      HASH_GUARD(0x07CB96AB34F4C258LL, settime) {
        return (ti_settime(c, params.rvalAt(0)));
      }
      break;
    case 90:
      HASH_GUARD(0x48072BFBFEDC9FDALL, setutcmonth) {
        return (ti_setutcmonth(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 93:
      HASH_GUARD(0x4AEB07E028F65EDDLL, setutchours) {
        return (ti_setutchours(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 94:
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        return (ti_parse(c, params.rvalAt(0)));
      }
      break;
    case 97:
      HASH_GUARD(0x06070A0F2DE05961LL, getutcseconds) {
        return (ti_getutcseconds(c));
      }
      break;
    case 103:
      HASH_GUARD(0x492C310A8BE20BE7LL, setutcmilliseconds) {
        return (ti_setutcmilliseconds(c, params.rvalAt(0)));
      }
      break;
    case 104:
      HASH_GUARD(0x05E9B0E5FBBEC068LL, tolocaletimestring) {
        return (ti_tolocaletimestring(c));
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x2B481A7942D2A5EALL, getutcday) {
        return (ti_getutcday(c));
      }
      break;
    case 107:
      HASH_GUARD(0x6F71E01BEFE6AFEBLL, setseconds) {
        return (ti_setseconds(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 111:
      HASH_GUARD(0x02C705749FEB756FLL, gethours) {
        return (ti_gethours(c));
      }
      break;
    case 116:
      HASH_GUARD(0x530B9A14D9ADE0F4LL, todatestring) {
        return (ti_todatestring(c));
      }
      break;
    case 118:
      HASH_GUARD(0x4830004EEE242276LL, getutcdate) {
        return (ti_getutcdate(c));
      }
      break;
    case 122:
      HASH_GUARD(0x4E64ABB986713BFALL, getutcminutes) {
        return (ti_getutcminutes(c));
      }
      break;
    case 127:
      HASH_GUARD(0x4A602DA5B6E9F97FLL, setutcfullyear) {
        return (ti_setutcfullyear(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_date$os_get(const char *s) {
  return c_js_date::os_get(s, -1);
}
Variant &cw_js_date$os_lval(const char *s) {
  return c_js_date::os_lval(s, -1);
}
Variant cw_js_date$os_constant(const char *s) {
  return c_js_date::os_constant(s);
}
Variant cw_js_date$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_date::os_invoke(c, s, params, -1, fatal);
}
void c_js_date::init() {
  c_js_object::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1925 */
void c_js_date::t___construct(Variant v_y //  = null
, Variant v_m //  = null
, Variant v_d //  = null
, Variant v_h //  = null
, Variant v_mn //  = null
, Variant v_s //  = null
, Variant v_ms //  = null
) {
  INSTANCE_METHOD_INJECTION(js_date, js_date::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_value;
  Variant v_v;
  double v_y2k = 0.0;

  LINE(1926,c_js_object::t___construct("Date", g->s_jsrt_DupIdproto_date));
  (v_y = (equal(v_y, null)) ? ((Variant)(g->s_jsrt_DupIdundefined)) : ((Variant)(v_y)));
  (v_m = (equal(v_m, null)) ? ((Variant)(g->s_jsrt_DupIdundefined)) : ((Variant)(v_m)));
  (v_d = (equal(v_d, null)) ? ((Variant)(g->s_jsrt_DupIdundefined)) : ((Variant)(v_d)));
  (v_h = (equal(v_h, null)) ? ((Variant)(g->s_jsrt_DupIdundefined)) : ((Variant)(v_h)));
  (v_mn = (equal(v_mn, null)) ? ((Variant)(g->s_jsrt_DupIdundefined)) : ((Variant)(v_mn)));
  (v_s = (equal(v_s, null)) ? ((Variant)(g->s_jsrt_DupIdundefined)) : ((Variant)(v_s)));
  (v_ms = (equal(v_ms, null)) ? ((Variant)(g->s_jsrt_DupIdundefined)) : ((Variant)(v_ms)));
  if (equal(v_y, g->s_jsrt_DupIdundefined)) {
    (v_value = LINE(1935,x_floor(toDouble(x_microtime(true) * 1000LL))));
  }
  else if (equal(v_m, g->s_jsrt_DupIdundefined)) {
    (v_v = LINE(1937,v_y.o_invoke_few_args("toPrimitive", 0x2F2C83503FD7D62BLL, 0)));
    if (equal(v_v.o_get("type", 0x508FC7C8724A760ALL), 4LL /* js_val::STRING */)) {
      (v_value = LINE(1939,x_strtotime(toString(v_v.o_get("value", 0x69E7413AE0C88471LL)))) * 1000LL);
    }
    else {
      (v_value = LINE(1941,v_v.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
    }
  }
  else {
    (v_y = LINE(1944,v_y.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
    (v_m = LINE(1945,v_m.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
    (v_d = (equal(v_d, g->s_jsrt_DupIdundefined)) ? ((Variant)(1LL)) : ((Variant)(LINE(1946,v_d.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
    (v_h = (equal(v_h, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1947,v_h.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
    (v_mn = (equal(v_mn, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1948,v_mn.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
    (v_s = (equal(v_s, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1949,v_s.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
    (v_ms = (equal(v_ms, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1950,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
    if (!(LINE(1951,x_is_nan(toDouble(v_y))))) {
      (v_y2k = LINE(1952,x_floor(toDouble(v_y))));
      if ((not_less(v_y2k, 0LL)) && (not_more(v_y2k, 99LL))) (v_y = 1900LL + v_y2k);
    }
    (v_value = 1000LL * LINE(1955,x_mktime(toInt32(v_h), toInt32(v_mn), toInt32(v_s), toInt32(v_m + 1LL), toInt32(v_d), toInt32(v_y))) + v_ms);
  }
  (m_value = v_value);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1962 */
Variant c_js_date::ti_object(const char* cls, int num_args, CVarRef v_value, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_date, js_date::object);
  Variant v_y;
  Variant v_m;
  Variant v_d;
  Variant v_h;
  Variant v_s;
  Variant v_ms;

  df_lambda_3(LINE(1963,func_get_args(num_args, Array(ArrayInit(1).set(0, v_value).create()),args)), v_y, v_m, v_d, v_h, v_m, v_s, v_ms);
  if (toBoolean(LINE(1964,c_js_function::t_isconstructor()))) {
    return ((Object)(LINE(1965,p_js_date(p_js_date(NEWOBJ(c_js_date)())->create(v_y, v_m, v_d, v_h, v_m, v_s, v_ms)))));
  }
  else {
    (v_d = ((Object)(LINE(1967,p_js_date(p_js_date(NEWOBJ(c_js_date)())->create(v_y, v_m, v_d, v_h, v_m, v_s, v_ms))))));
    return LINE(1968,v_d.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1971 */
Variant c_js_date::ti_parse(const char* cls, CVarRef v_v) {
  STATIC_METHOD_INJECTION(js_date, js_date::parse);
  return LINE(1972,f_js_int(x_strtotime(toString(toObject(v_v)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0).o_get("value", 0x69E7413AE0C88471LL))) * 1000LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1974 */
Variant c_js_date::ti_utc(const char* cls, Variant v_y, Variant v_m, Variant v_d, Variant v_h, Variant v_mn, Variant v_s, Variant v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::UTC);
  DECLARE_GLOBAL_VARIABLES(g);
  double v_y2k = 0.0;
  PlusOperand v_value = 0;

  (v_y = LINE(1975,v_y.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_m = LINE(1976,v_m.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_d = (equal(v_d, g->s_jsrt_DupIdundefined)) ? ((Variant)(1LL)) : ((Variant)(LINE(1977,v_d.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_h = (equal(v_h, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1978,v_h.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_mn = (equal(v_mn, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1979,v_mn.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_s = (equal(v_s, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1980,v_s.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_ms = (equal(v_ms, g->s_jsrt_DupIdundefined)) ? ((Variant)(0LL)) : ((Variant)(LINE(1981,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  if (!(LINE(1982,x_is_nan(toDouble(v_y))))) {
    (v_y2k = LINE(1983,x_floor(toDouble(v_y))));
    if ((not_less(v_y2k, 0LL)) && (not_more(v_y2k, 99LL))) (v_y = 1900LL + v_y2k);
  }
  (v_value = 1000LL * LINE(1986,x_gmmktime(toInt32(v_h), toInt32(v_mn), toInt32(v_s), toInt32(v_m + 1LL), toInt32(v_d), toInt32(v_y))) + v_ms);
  return LINE(1987,f_js_int(v_value));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1989 */
Variant c_js_date::ti_tostring(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::toString);
  Variant v_obj;

  (v_obj = LINE(1993,c_jsrt::t_this()));
  if (!equal(LINE(1994,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(1995,f_js_str(x_date("r", toInt64(divide(v_obj.o_get("value", 0x69E7413AE0C88471LL), 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1997 */
Variant c_js_date::ti_todatestring(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::toDateString);
  Variant v_obj;

  (v_obj = LINE(2001,c_jsrt::t_this()));
  if (!equal(LINE(2002,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(2003,f_js_str(x_date("D M j Y", toInt64(divide(v_obj.o_get("value", 0x69E7413AE0C88471LL), 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2005 */
Variant c_js_date::ti_totimestring(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::toTimeString);
  Variant v_obj;

  (v_obj = LINE(2008,c_jsrt::t_this()));
  if (!equal(LINE(2009,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(2010,f_js_str(x_date("G:i:s T", toInt64(divide(v_obj.o_get("value", 0x69E7413AE0C88471LL), 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2012 */
Variant c_js_date::ti_tolocalestring(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::toLocaleString);
  Variant v_obj;

  (v_obj = LINE(2016,c_jsrt::t_this()));
  if (!equal(LINE(2017,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(2018,f_js_str(x_strftime("%c", toInt64(divide(v_obj.o_get("value", 0x69E7413AE0C88471LL), 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2020 */
Variant c_js_date::ti_tolocaledatestring(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::toLocaleDateString);
  Variant v_obj;

  (v_obj = LINE(2021,c_jsrt::t_this()));
  if (!equal(LINE(2022,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(2023,f_js_str(x_strftime("%x", toInt64(divide(v_obj.o_get("value", 0x69E7413AE0C88471LL), 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2025 */
Variant c_js_date::ti_tolocaletimestring(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::toLocaleTimeString);
  Variant v_obj;

  (v_obj = LINE(2026,c_jsrt::t_this()));
  if (!equal(LINE(2027,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(2028,f_js_str(x_strftime("%X", toInt64(divide(v_obj.o_get("value", 0x69E7413AE0C88471LL), 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2030 */
Variant c_js_date::ti_valueof(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::valueOf);
  Variant v_obj;

  (v_obj = LINE(2031,c_jsrt::t_this()));
  if (!equal(LINE(2032,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(2033,f_js_int(v_obj.o_get("value", 0x69E7413AE0C88471LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2035 */
Variant c_js_date::ti_gettime(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getTime);
  return LINE(2036,c_js_date::t_valueof());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2038 */
Variant c_js_date::ti_getfullyear(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getFullYear);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2039,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2040,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2041,f_js_int(x_date("Y", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2043 */
Variant c_js_date::ti_getutcfullyear(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCFullYear);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2044,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2045,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2046,f_js_int(x_gmdate("Y", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2048 */
Variant c_js_date::ti_getmonth(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getMonth);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2049,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2050,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2051,f_js_int((Variant)(x_date("n", toInt64(divide(v_t, 1000LL)))) - 1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2053 */
Variant c_js_date::ti_getutcmonth(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCMonth);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2054,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2055,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2056,f_js_int((Variant)(x_gmdate("n", toInt64(divide(v_t, 1000LL)))) - 1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2058 */
Variant c_js_date::ti_getdate(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getDate);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2059,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2060,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2061,f_js_int(x_date("j", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2063 */
Variant c_js_date::ti_getutcdate(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCDate);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2064,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2065,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2066,f_js_int(x_gmdate("j", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2068 */
Variant c_js_date::ti_getday(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getDay);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2069,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2070,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2071,f_js_int(x_date("w", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2073 */
Variant c_js_date::ti_getutcday(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCDay);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2074,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2075,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2076,f_js_int(x_gmdate("w", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2078 */
Variant c_js_date::ti_gethours(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getHours);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2079,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2080,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2081,f_js_int(x_date("G", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2083 */
Variant c_js_date::ti_getutchours(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCHours);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2084,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2085,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2086,f_js_int(x_gmdate("G", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2088 */
Variant c_js_date::ti_getminutes(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getMinutes);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2089,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2090,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2091,f_js_int(x_date("i", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2093 */
Variant c_js_date::ti_getutcminutes(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCMinutes);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2094,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2095,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2096,f_js_int(x_gmdate("i", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2098 */
Variant c_js_date::ti_getseconds(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getSeconds);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2099,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2100,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2101,f_js_int(x_date("s", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2103 */
Variant c_js_date::ti_getutcseconds(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCSeconds);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2104,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2105,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2106,f_js_int(x_gmdate("s", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2108 */
Variant c_js_date::ti_getmillieconds(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getMillieconds);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2109,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2110,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2111,f_js_int(modulo(toInt64(v_t), 1000LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2113 */
Variant c_js_date::ti_getutcmilliseconds(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getUTCMilliseconds);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;

  (v_t = LINE(2114,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2115,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  return LINE(2116,f_js_int(modulo(toInt64(v_t), 1000LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2118 */
Variant c_js_date::ti_gettimezoneoffset(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::getTimezoneOffset);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_t;
  Variant v_s;

  (v_t = LINE(2119,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(2120,x_is_nan(toDouble(v_t)))) return g->s_jsrt_DupIdnan;
  (v_s = LINE(2121,x_gettimeofday()));
  return LINE(2122,f_js_int(v_t.rvalAt("minuteswest", 0x3154FA2DE4E49265LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2124 */
Variant c_js_date::ti_settime(const char* cls, CVarRef v_time) {
  STATIC_METHOD_INJECTION(js_date, js_date::setTime);
  Variant v_obj;
  Variant v_v;

  (v_obj = LINE(2125,c_jsrt::t_this()));
  if (!equal(LINE(2126,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_v = LINE(2127,toObject(v_time)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return LINE(2129,f_js_int(v_v));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2131 */
double c_js_date::ti_setmilliseconds(const char* cls, Variant v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setMilliseconds);
  Variant v_obj;
  Variant v_t;
  double v_v = 0.0;

  (v_obj = LINE(2132,c_jsrt::t_this()));
  if (!equal(LINE(2133,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = LINE(2134,c_js_date::t_valueof()).o_get("value", 0x69E7413AE0C88471LL));
  (v_ms = LINE(2135,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_v = LINE(2136,x_floor(toDouble(divide(v_t, 1000LL)))) * 1000LL + toDouble(v_ms));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2140 */
double c_js_date::ti_setutcmilliseconds(const char* cls, CVarRef v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setUTCMilliseconds);
  return LINE(2141,c_js_date::t_setmilliseconds(v_ms));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2143 */
double c_js_date::ti_setseconds(const char* cls, Variant v_s, Variant v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setSeconds);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  double v_v = 0.0;

  (v_obj = LINE(2144,c_jsrt::t_this()));
  if (!equal(LINE(2145,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_s = LINE(2147,v_s.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_ms = (equal(v_ms, g->s_jsrt_DupIdundefined)) ? ((Variant)((modulo(toInt64(v_t), 1000LL)))) : ((Variant)(LINE(2148,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2149,x_floor(toDouble(divide(v_t, 60000LL)))) * 60000LL + toDouble((1000LL * v_s + v_ms)));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2153 */
double c_js_date::ti_setutcseconds(const char* cls, CVarRef v_s, CVarRef v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setUTCSeconds);
  return LINE(2154,c_js_date::t_setseconds(v_s, v_ms));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2156 */
PlusOperand c_js_date::ti_setminutes(const char* cls, Variant v_min, Variant v_sec, Variant v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setMinutes);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2157,c_jsrt::t_this()));
  if (!equal(LINE(2158,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_min = LINE(2160,v_min.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_sec = (equal(v_sec, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2161,c_js_date::t_getseconds()))) : ((Variant)(v_sec.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_ms = (equal(v_ms, g->s_jsrt_DupIdundefined)) ? ((Variant)((modulo(toInt64(v_t), 1000LL)))) : ((Variant)(LINE(2162,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2164,(assignCallTemp(eo_0, toInt32(LINE(2163,c_js_date::t_gethours()))),assignCallTemp(eo_1, toInt32(v_min)),assignCallTemp(eo_2, toInt32(v_sec)),assignCallTemp(eo_3, toInt32(c_js_date::t_getmonth())),assignCallTemp(eo_4, toInt32(LINE(2164,c_js_date::t_getdate()))),assignCallTemp(eo_5, toInt32(throw_fatal("unknown method js_date::getyear", ((void*)NULL)))),x_mktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + v_ms);
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2168 */
PlusOperand c_js_date::ti_setutcminutes(const char* cls, Variant v_min, Variant v_sec, Variant v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setUTCMinutes);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2169,c_jsrt::t_this()));
  if (!equal(LINE(2170,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_min = LINE(2172,v_min.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_sec = (equal(v_sec, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2173,c_js_date::t_getutcseconds()))) : ((Variant)(v_sec.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_ms = (equal(v_ms, g->s_jsrt_DupIdundefined)) ? ((Variant)((modulo(toInt64(v_t), 1000LL)))) : ((Variant)(LINE(2174,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2176,(assignCallTemp(eo_0, toInt32(LINE(2175,c_js_date::t_getutchours()))),assignCallTemp(eo_1, toInt32(v_min)),assignCallTemp(eo_2, toInt32(v_sec)),assignCallTemp(eo_3, toInt32(c_js_date::t_getutcmonth())),assignCallTemp(eo_4, toInt32(LINE(2176,c_js_date::t_getutcdate()))),assignCallTemp(eo_5, toInt32(throw_fatal("unknown method js_date::getutcyear", ((void*)NULL)))),x_gmmktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + v_ms);
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2180 */
PlusOperand c_js_date::ti_sethours(const char* cls, Variant v_hour, Variant v_min, Variant v_sec, Variant v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setHours);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2181,c_jsrt::t_this()));
  if (!equal(LINE(2182,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_hour = LINE(2184,v_hour.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_min = (equal(v_min, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2185,c_js_date::t_getminutes()))) : ((Variant)(v_min.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_sec = (equal(v_sec, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2186,c_js_date::t_getseconds()))) : ((Variant)(v_sec.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_ms = (equal(v_ms, g->s_jsrt_DupIdundefined)) ? ((Variant)((modulo(toInt64(v_t), 1000LL)))) : ((Variant)(LINE(2187,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2189,(assignCallTemp(eo_0, toInt32(v_hour)),assignCallTemp(eo_1, toInt32(v_min)),assignCallTemp(eo_2, toInt32(v_sec)),assignCallTemp(eo_3, toInt32(LINE(2188,c_js_date::t_getmonth()))),assignCallTemp(eo_4, toInt32(LINE(2189,c_js_date::t_getdate()))),assignCallTemp(eo_5, toInt32(throw_fatal("unknown method js_date::getyear", ((void*)NULL)))),x_mktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + v_ms);
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2193 */
PlusOperand c_js_date::ti_setutchours(const char* cls, Variant v_hour, Variant v_min, Variant v_sec, Variant v_ms) {
  STATIC_METHOD_INJECTION(js_date, js_date::setUTCHours);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2194,c_jsrt::t_this()));
  if (!equal(LINE(2195,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_hour = LINE(2197,v_hour.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_min = (equal(v_min, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2198,c_js_date::t_getutcminutes()))) : ((Variant)(v_min.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_sec = (equal(v_sec, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2199,c_js_date::t_getutcseconds()))) : ((Variant)(v_sec.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_ms = (equal(v_ms, g->s_jsrt_DupIdundefined)) ? ((Variant)((modulo(toInt64(v_t), 1000LL)))) : ((Variant)(LINE(2200,v_ms.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2202,(assignCallTemp(eo_0, toInt32(v_hour)),assignCallTemp(eo_1, toInt32(v_min)),assignCallTemp(eo_2, toInt32(v_sec)),assignCallTemp(eo_3, toInt32(LINE(2201,c_js_date::t_getutcmonth()))),assignCallTemp(eo_4, toInt32(LINE(2202,c_js_date::t_getutcdate()))),assignCallTemp(eo_5, toInt32(throw_fatal("unknown method js_date::getutcyear", ((void*)NULL)))),x_gmmktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + v_ms);
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2206 */
PlusOperand c_js_date::ti_setdate(const char* cls, Variant v_date) {
  STATIC_METHOD_INJECTION(js_date, js_date::setDate);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2207,c_jsrt::t_this()));
  if (!equal(LINE(2208,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_date = LINE(2210,v_date.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_v = LINE(2212,(assignCallTemp(eo_0, toInt32(LINE(2211,c_js_date::t_gethours()))),assignCallTemp(eo_1, toInt32(c_js_date::t_getminutes())),assignCallTemp(eo_2, toInt32(c_js_date::t_getseconds())),assignCallTemp(eo_3, toInt32(LINE(2212,c_js_date::t_getmonth()))),assignCallTemp(eo_4, toInt32(v_date)),assignCallTemp(eo_5, toInt32(throw_fatal("unknown method js_date::getyear", ((void*)NULL)))),x_mktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + (modulo(toInt64(v_t), 1000LL)));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2216 */
PlusOperand c_js_date::ti_setutcdate(const char* cls, Variant v_date) {
  STATIC_METHOD_INJECTION(js_date, js_date::setUTCDate);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2217,c_jsrt::t_this()));
  if (!equal(LINE(2218,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_date = LINE(2220,v_date.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_v = LINE(2222,(assignCallTemp(eo_0, toInt32(LINE(2221,c_js_date::t_getutchours()))),assignCallTemp(eo_1, toInt32(c_js_date::t_getutcminutes())),assignCallTemp(eo_2, toInt32(c_js_date::t_getutcseconds())),assignCallTemp(eo_3, toInt32(LINE(2222,c_js_date::t_getutcmonth()))),assignCallTemp(eo_4, toInt32(v_date)),assignCallTemp(eo_5, toInt32(throw_fatal("unknown method js_date::getutcyear", ((void*)NULL)))),x_gmmktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + (modulo(toInt64(v_t), 1000LL)));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2226 */
PlusOperand c_js_date::ti_setmonth(const char* cls, Variant v_month, Variant v_date) {
  STATIC_METHOD_INJECTION(js_date, js_date::setMonth);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2227,c_jsrt::t_this()));
  if (!equal(LINE(2228,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_month = LINE(2230,v_month.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_date = (equal(v_date, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2231,c_js_date::t_getdate()))) : ((Variant)(v_date.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2233,(assignCallTemp(eo_0, toInt32(LINE(2232,c_js_date::t_gethours()))),assignCallTemp(eo_1, toInt32(c_js_date::t_getminutes())),assignCallTemp(eo_2, toInt32(c_js_date::t_getseconds())),assignCallTemp(eo_3, toInt32(v_month)),assignCallTemp(eo_4, toInt32(v_date)),assignCallTemp(eo_5, toInt32(LINE(2233,throw_fatal("unknown method js_date::getyear", ((void*)NULL))))),x_mktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + (modulo(toInt64(v_t), 1000LL)));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2237 */
PlusOperand c_js_date::ti_setutcmonth(const char* cls, Variant v_month, Variant v_date) {
  STATIC_METHOD_INJECTION(js_date, js_date::setUTCMonth);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2238,c_jsrt::t_this()));
  if (!equal(LINE(2239,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_month = LINE(2241,v_month.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_date = (equal(v_date, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2242,c_js_date::t_getutcdate()))) : ((Variant)(v_date.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2244,(assignCallTemp(eo_0, toInt32(LINE(2243,c_js_date::t_getutchours()))),assignCallTemp(eo_1, toInt32(c_js_date::t_getutcminutes())),assignCallTemp(eo_2, toInt32(c_js_date::t_getutcseconds())),assignCallTemp(eo_3, toInt32(v_month)),assignCallTemp(eo_4, toInt32(v_date)),assignCallTemp(eo_5, toInt32(LINE(2244,throw_fatal("unknown method js_date::getutcyear", ((void*)NULL))))),x_gmmktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + (modulo(toInt64(v_t), 1000LL)));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2248 */
PlusOperand c_js_date::ti_setfullyear(const char* cls, Variant v_year, Variant v_month, Variant v_date) {
  STATIC_METHOD_INJECTION(js_date, js_date::setFullYear);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2249,c_jsrt::t_this()));
  if (!equal(LINE(2250,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_year = LINE(2252,v_year.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_month = (equal(v_month, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2253,c_js_date::t_getmonth()))) : ((Variant)(v_month.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_date = (equal(v_date, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2254,c_js_date::t_getminutes()))) : ((Variant)(v_date.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2256,(assignCallTemp(eo_0, toInt32(LINE(2255,c_js_date::t_gethours()))),assignCallTemp(eo_1, toInt32(c_js_date::t_getdate())),assignCallTemp(eo_2, toInt32(c_js_date::t_getseconds())),assignCallTemp(eo_3, toInt32(v_month)),assignCallTemp(eo_4, toInt32(v_date)),assignCallTemp(eo_5, toInt32(v_year)),x_mktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + (modulo(toInt64(v_t), 1000LL)));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2260 */
PlusOperand c_js_date::ti_setutcfullyear(const char* cls, Variant v_year, Variant v_month, Variant v_date) {
  STATIC_METHOD_INJECTION(js_date, js_date::setUTCFullYear);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_t;
  PlusOperand v_v = 0;

  (v_obj = LINE(2261,c_jsrt::t_this()));
  if (!equal(LINE(2262,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  (v_year = LINE(2264,v_year.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_month = (equal(v_month, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2265,c_js_date::t_getutcmonth()))) : ((Variant)(v_month.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_date = (equal(v_date, g->s_jsrt_DupIdundefined)) ? ((Variant)(LINE(2266,c_js_date::t_getutcdate()))) : ((Variant)(v_date.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0).o_get("value", 0x69E7413AE0C88471LL))));
  (v_v = LINE(2268,(assignCallTemp(eo_0, toInt32(LINE(2267,c_js_date::t_getutchours()))),assignCallTemp(eo_1, toInt32(c_js_date::t_getutcminutes())),assignCallTemp(eo_2, toInt32(c_js_date::t_getutcseconds())),assignCallTemp(eo_3, toInt32(v_month)),assignCallTemp(eo_4, toInt32(v_date)),assignCallTemp(eo_5, toInt32(v_year)),x_gmmktime(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5))) * 1000LL + (modulo(toInt64(v_t), 1000LL)));
  (v_obj.o_lval("value", 0x69E7413AE0C88471LL) = v_v);
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2272 */
Variant c_js_date::ti_toutcstring(const char* cls) {
  STATIC_METHOD_INJECTION(js_date, js_date::toUTCString);
  Variant v_obj;
  Variant v_t;

  (v_obj = LINE(2273,c_jsrt::t_this()));
  if (!equal(LINE(2274,x_get_class(v_obj)), "js_date")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_t = v_obj.o_get("value", 0x69E7413AE0C88471LL));
  return LINE(2276,f_js_str(x_gmstrftime("%c", toInt64(divide(v_t, 1000LL)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2443 */
Variant c_js_ref::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_js_ref::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_js_ref::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("type", m_type.isReferenced() ? ref(m_type) : m_type));
  props.push_back(NEW(ArrayElement)("base", m_base.isReferenced() ? ref(m_base) : m_base));
  props.push_back(NEW(ArrayElement)("propName", m_propName.isReferenced() ? ref(m_propName) : m_propName));
  c_ObjectData::o_get(props);
}
bool c_js_ref::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_EXISTS_STRING(0x508FC7C8724A760ALL, type, 4);
      break;
    case 5:
      HASH_EXISTS_STRING(0x4DC47FEB2126D745LL, propName, 8);
      break;
    case 7:
      HASH_EXISTS_STRING(0x56FD8ECCBDFDD7A7LL, base, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_js_ref::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x508FC7C8724A760ALL, m_type,
                         type, 4);
      break;
    case 5:
      HASH_RETURN_STRING(0x4DC47FEB2126D745LL, m_propName,
                         propName, 8);
      break;
    case 7:
      HASH_RETURN_STRING(0x56FD8ECCBDFDD7A7LL, m_base,
                         base, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_js_ref::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_SET_STRING(0x508FC7C8724A760ALL, m_type,
                      type, 4);
      break;
    case 5:
      HASH_SET_STRING(0x4DC47FEB2126D745LL, m_propName,
                      propName, 8);
      break;
    case 7:
      HASH_SET_STRING(0x56FD8ECCBDFDD7A7LL, m_base,
                      base, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_js_ref::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x508FC7C8724A760ALL, m_type,
                         type, 4);
      break;
    case 5:
      HASH_RETURN_STRING(0x4DC47FEB2126D745LL, m_propName,
                         propName, 8);
      break;
    case 7:
      HASH_RETURN_STRING(0x56FD8ECCBDFDD7A7LL, m_base,
                         base, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_js_ref::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(js_ref)
ObjectData *c_js_ref::create(Variant v_base, Variant v_propName) {
  init();
  t___construct(v_base, v_propName);
  return this;
}
ObjectData *c_js_ref::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_ref::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_ref::cloneImpl() {
  c_js_ref *obj = NEW(c_js_ref)();
  cloneSet(obj);
  return obj;
}
void c_js_ref::cloneSet(c_js_ref *clone) {
  clone->m_type = m_type.isReferenced() ? ref(m_type) : m_type;
  clone->m_base = m_base.isReferenced() ? ref(m_base) : m_base;
  clone->m_propName = m_propName.isReferenced() ? ref(m_propName) : m_propName;
  ObjectData::cloneSet(clone);
}
Variant c_js_ref::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        int count = params.size();
        if (count <= 1) return (t_putvalue(params.rvalAt(0)));
        return (t_putvalue(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_js_ref::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        if (count <= 1) return (t_putvalue(a0));
        return (t_putvalue(a0, a1));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_ref::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_ref$os_get(const char *s) {
  return c_js_ref::os_get(s, -1);
}
Variant &cw_js_ref$os_lval(const char *s) {
  return c_js_ref::os_lval(s, -1);
}
Variant cw_js_ref$os_constant(const char *s) {
  return c_js_ref::os_constant(s);
}
Variant cw_js_ref$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_ref::os_invoke(c, s, params, -1, fatal);
}
void c_js_ref::init() {
  m_type = null;
  m_base = null;
  m_propName = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2447 */
void c_js_ref::t___construct(Variant v_base, Variant v_propName) {
  INSTANCE_METHOD_INJECTION(js_ref, js_ref::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_type = 6LL /* js_val::REF */);
  (m_base = v_base);
  (m_propName = v_propName);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2452 */
Variant c_js_ref::t_getvalue() {
  INSTANCE_METHOD_INJECTION(js_ref, js_ref::getValue);
  if (!(LINE(2453,x_is_object(m_base)))) {
    echo("<pre>");
    LINE(2455,x_debug_print_backtrace());
    echo("</pre>");
  }
  return LINE(2458,m_base.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, m_propName));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2460 */
Variant c_js_ref::t_putvalue(Variant v_w, CVarRef v_ret //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_ref, js_ref::putValue);
  Variant v_v;

  setNull(v_v);
  if (equal(v_ret, 2LL)) {
    (v_v = LINE(2463,m_base.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, m_propName)));
  }
  LINE(2465,m_base.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, m_propName, v_w));
  if (equal(v_ret, 1LL)) return v_w;
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 743 */
Variant c_js_context::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_js_context::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_js_context::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("js_this", m_js_this.isReferenced() ? ref(m_js_this) : m_js_this));
  props.push_back(NEW(ArrayElement)("scope_chain", m_scope_chain.isReferenced() ? ref(m_scope_chain) : m_scope_chain));
  props.push_back(NEW(ArrayElement)("var", m_var.isReferenced() ? ref(m_var) : m_var));
  c_ObjectData::o_get(props);
}
bool c_js_context::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x0953EE1CC9042120LL, var, 3);
      break;
    case 2:
      HASH_EXISTS_STRING(0x3740A81EEE430C82LL, js_this, 7);
      break;
    case 6:
      HASH_EXISTS_STRING(0x0413391407EE12A6LL, scope_chain, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_js_context::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x0953EE1CC9042120LL, m_var,
                         var, 3);
      break;
    case 2:
      HASH_RETURN_STRING(0x3740A81EEE430C82LL, m_js_this,
                         js_this, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x0413391407EE12A6LL, m_scope_chain,
                         scope_chain, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_js_context::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x0953EE1CC9042120LL, m_var,
                      var, 3);
      break;
    case 2:
      HASH_SET_STRING(0x3740A81EEE430C82LL, m_js_this,
                      js_this, 7);
      break;
    case 6:
      HASH_SET_STRING(0x0413391407EE12A6LL, m_scope_chain,
                      scope_chain, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_js_context::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x0953EE1CC9042120LL, m_var,
                         var, 3);
      break;
    case 2:
      HASH_RETURN_STRING(0x3740A81EEE430C82LL, m_js_this,
                         js_this, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x0413391407EE12A6LL, m_scope_chain,
                         scope_chain, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_js_context::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(js_context)
ObjectData *c_js_context::create(Variant v_that, Variant v_scope_chain, Variant v_var) {
  init();
  t___construct(v_that, v_scope_chain, v_var);
  return this;
}
ObjectData *c_js_context::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_js_context::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_js_context::cloneImpl() {
  c_js_context *obj = NEW(c_js_context)();
  cloneSet(obj);
  return obj;
}
void c_js_context::cloneSet(c_js_context *clone) {
  clone->m_js_this = m_js_this.isReferenced() ? ref(m_js_this) : m_js_this;
  clone->m_scope_chain = m_scope_chain.isReferenced() ? ref(m_scope_chain) : m_scope_chain;
  clone->m_var = m_var.isReferenced() ? ref(m_var) : m_var;
  ObjectData::cloneSet(clone);
}
Variant c_js_context::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_js_context::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_context::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_context$os_get(const char *s) {
  return c_js_context::os_get(s, -1);
}
Variant &cw_js_context$os_lval(const char *s) {
  return c_js_context::os_lval(s, -1);
}
Variant cw_js_context$os_constant(const char *s) {
  return c_js_context::os_constant(s);
}
Variant cw_js_context$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_context::os_invoke(c, s, params, -1, fatal);
}
void c_js_context::init() {
  m_js_this = null;
  m_scope_chain = null;
  m_var = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 748 */
void c_js_context::t___construct(Variant v_that, Variant v_scope_chain, Variant v_var) {
  INSTANCE_METHOD_INJECTION(js_context, js_context::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_js_this = v_that);
  (m_scope_chain = v_scope_chain);
  (m_var = v_var);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1239 */
Variant c_js_array::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_array::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_array::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("length", m_length.isReferenced() ? ref(m_length) : m_length));
  c_js_object::o_get(props);
}
bool c_js_array::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x2993E8EB119CAB21LL, length, 6);
      break;
    default:
      break;
  }
  return c_js_object::o_exists(s, hash);
}
Variant c_js_array::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x2993E8EB119CAB21LL, m_length,
                         length, 6);
      break;
    default:
      break;
  }
  return c_js_object::o_get(s, hash);
}
Variant c_js_array::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x2993E8EB119CAB21LL, m_length,
                      length, 6);
      break;
    default:
      break;
  }
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_array::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x2993E8EB119CAB21LL, m_length,
                         length, 6);
      break;
    default:
      break;
  }
  return c_js_object::o_lval(s, hash);
}
Variant c_js_array::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_array)
ObjectData *c_js_array::create(Variant v_len //  = 0LL
, Variant v_args //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_len, v_args);
  return this;
}
ObjectData *c_js_array::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_array::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_array::cloneImpl() {
  c_js_array *obj = NEW(c_js_array)();
  cloneSet(obj);
  return obj;
}
void c_js_array::cloneSet(c_js_array *clone) {
  clone->m_length = m_length.isReferenced() ? ref(m_length) : m_length;
  c_js_object::cloneSet(clone);
}
Variant c_js_array::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 16:
      HASH_GUARD(0x14AE5461C6BE5490LL, unshift) {
        int count = params.size();
        if (count <= 0) return (ti_unshift(o_getClassName(),count));
        return (ti_unshift(o_getClassName(),count,params.slice(0, count - 0, false)));
      }
      break;
    case 17:
      HASH_GUARD(0x0CCCAC649763E711LL, splice) {
        int count = params.size();
        if (count <= 2) return (ti_splice(o_getClassName(),count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_splice(o_getClassName(),count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x773C5A963CD2AC13LL, pop) {
        return (ti_pop(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 22:
      HASH_GUARD(0x5165C84CBFA87C16LL, reverse) {
        return (ti_reverse(o_getClassName()));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)));
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 37:
      HASH_GUARD(0x2A865A9CE4562C25LL, shift) {
        return (ti_shift(o_getClassName()));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x1F4984938E1DBB2ALL, sort) {
        return (ti_sort(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      HASH_GUARD(0x321E2BF5D878AA38LL, push) {
        int count = params.size();
        if (count <= 0) return (ti_push(o_getClassName(),count));
        return (ti_push(o_getClassName(),count,params.slice(0, count - 0, false)));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        int count = params.size();
        if (count <= 1) return (ti_object(o_getClassName(),count, params.rvalAt(0)));
        return (ti_object(o_getClassName(),count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 76:
      HASH_GUARD(0x67A605DDB46412CCLL, tolocalestring) {
        return (ti_tolocalestring(o_getClassName()));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 87:
      HASH_GUARD(0x255540B5A68F3557LL, slice) {
        return (ti_slice(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 113:
      HASH_GUARD(0x3FA954579B1B7B71LL, sort_helper) {
        return (ti_sort_helper(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 121:
      HASH_GUARD(0x09F8074DCD9353F9LL, concat) {
        int count = params.size();
        if (count <= 0) return (ti_concat(o_getClassName(),count));
        return (ti_concat(o_getClassName(),count,params.slice(0, count - 0, false)));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    case 125:
      HASH_GUARD(0x78EDB608B02A857DLL, join) {
        return (ti_join(o_getClassName(), params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_array::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 16:
      HASH_GUARD(0x14AE5461C6BE5490LL, unshift) {
        if (count <= 0) return (ti_unshift(o_getClassName(),count));
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_unshift(o_getClassName(),count,params));
      }
      break;
    case 17:
      HASH_GUARD(0x0CCCAC649763E711LL, splice) {
        if (count <= 2) return (ti_splice(o_getClassName(),count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_splice(o_getClassName(),count,a0, a1, params));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x773C5A963CD2AC13LL, pop) {
        return (ti_pop(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 22:
      HASH_GUARD(0x5165C84CBFA87C16LL, reverse) {
        return (ti_reverse(o_getClassName()));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1));
        return (t_put(a0, a1, a2));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 37:
      HASH_GUARD(0x2A865A9CE4562C25LL, shift) {
        return (ti_shift(o_getClassName()));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x1F4984938E1DBB2ALL, sort) {
        return (ti_sort(o_getClassName(), a0));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      HASH_GUARD(0x321E2BF5D878AA38LL, push) {
        if (count <= 0) return (ti_push(o_getClassName(),count));
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_push(o_getClassName(),count,params));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        if (count <= 1) return (ti_object(o_getClassName(),count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_object(o_getClassName(),count,a0, params));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 76:
      HASH_GUARD(0x67A605DDB46412CCLL, tolocalestring) {
        return (ti_tolocalestring(o_getClassName()));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 87:
      HASH_GUARD(0x255540B5A68F3557LL, slice) {
        return (ti_slice(o_getClassName(), a0, a1));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 113:
      HASH_GUARD(0x3FA954579B1B7B71LL, sort_helper) {
        return (ti_sort_helper(o_getClassName(), a0, a1));
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 121:
      HASH_GUARD(0x09F8074DCD9353F9LL, concat) {
        if (count <= 0) return (ti_concat(o_getClassName(),count));
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_concat(o_getClassName(),count,params));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    case 125:
      HASH_GUARD(0x78EDB608B02A857DLL, join) {
        return (ti_join(o_getClassName(), a0));
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_array::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x67A605DDB46412CCLL, tolocalestring) {
        return (ti_tolocalestring(c));
      }
      break;
    case 16:
      HASH_GUARD(0x14AE5461C6BE5490LL, unshift) {
        int count = params.size();
        if (count <= 0) return (ti_unshift(c,count));
        return (ti_unshift(c,count,params.slice(0, count - 0, false)));
      }
      break;
    case 17:
      HASH_GUARD(0x0CCCAC649763E711LL, splice) {
        int count = params.size();
        if (count <= 2) return (ti_splice(c,count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_splice(c,count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      HASH_GUARD(0x773C5A963CD2AC13LL, pop) {
        return (ti_pop(c));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 22:
      HASH_GUARD(0x5165C84CBFA87C16LL, reverse) {
        return (ti_reverse(c));
      }
      break;
    case 23:
      HASH_GUARD(0x255540B5A68F3557LL, slice) {
        return (ti_slice(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 37:
      HASH_GUARD(0x2A865A9CE4562C25LL, shift) {
        return (ti_shift(c));
      }
      break;
    case 42:
      HASH_GUARD(0x1F4984938E1DBB2ALL, sort) {
        return (ti_sort(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    case 49:
      HASH_GUARD(0x3FA954579B1B7B71LL, sort_helper) {
        return (ti_sort_helper(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 56:
      HASH_GUARD(0x321E2BF5D878AA38LL, push) {
        int count = params.size();
        if (count <= 0) return (ti_push(c,count));
        return (ti_push(c,count,params.slice(0, count - 0, false)));
      }
      break;
    case 57:
      HASH_GUARD(0x09F8074DCD9353F9LL, concat) {
        int count = params.size();
        if (count <= 0) return (ti_concat(c,count));
        return (ti_concat(c,count,params.slice(0, count - 0, false)));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        int count = params.size();
        if (count <= 1) return (ti_object(c,count, params.rvalAt(0)));
        return (ti_object(c,count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 61:
      HASH_GUARD(0x78EDB608B02A857DLL, join) {
        return (ti_join(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_array$os_get(const char *s) {
  return c_js_array::os_get(s, -1);
}
Variant &cw_js_array$os_lval(const char *s) {
  return c_js_array::os_lval(s, -1);
}
Variant cw_js_array$os_constant(const char *s) {
  return c_js_array::os_constant(s);
}
Variant cw_js_array$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_array::os_invoke(c, s, params, -1, fatal);
}
void c_js_array::init() {
  c_js_object::init();
  m_length = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1241 */
void c_js_array::t___construct(Variant v_len //  = 0LL
, Variant v_args //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(js_array, js_array::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_index;
  Variant v_value;

  LINE(1242,c_js_object::t___construct("Array", g->s_jsrt_DupIdproto_array));
  if (equal(v_len, 0LL)) (v_len = g->s_jsrt_DupIdzero);
  (m_length = v_len);
  {
    LOOP_COUNTER(22);
    for (ArrayIterPtr iter24 = v_args.begin("js_array"); !iter24->end(); iter24->next()) {
      LOOP_COUNTER_CHECK(22);
      v_value = iter24->second();
      v_index = iter24->first();
      {
        echo(LINE(1246,concat5("Setting ", toString(v_index), " to ", toString(v_value), "<br>")));
        LINE(1247,o_root_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_index, v_value));
      }
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1250 */
Variant c_js_array::t_defaultvalue(CVarRef v_iggy //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(js_array, js_array::defaultValue);
  Variant v_arr;
  int64 v_i = 0;
  Primitive v_index = 0;
  Variant v_value;
  String v_o;

  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(25);
    for ((v_i = 0LL); less(v_i, m_length.o_get("value", 0x69E7413AE0C88471LL)); v_i++) {
      LOOP_COUNTER_CHECK(25);
      v_arr.set(v_i, (""));
    }
  }
  {
    LOOP_COUNTER(26);
    Variant map27 = m_slots;
    for (ArrayIterPtr iter28 = map27.begin("js_array"); !iter28->end(); iter28->next()) {
      LOOP_COUNTER_CHECK(26);
      v_value = iter28->second();
      v_index = iter28->first();
      {
        if (LINE(1254,x_is_numeric(v_index))) v_arr.set(v_index, (v_value.o_get("value", 0x69E7413AE0C88471LL).o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0).o_get("value", 0x69E7413AE0C88471LL)));
      }
    }
  }
  (v_o = LINE(1256,x_implode(",", v_arr)));
  return LINE(1257,f_js_str(v_o));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1259 */
Variant c_js_array::t_get(Variant v_name) {
  INSTANCE_METHOD_INJECTION(js_array, js_array::get);
  (v_name = LINE(1260,x_strval(v_name)));
  if (equal(v_name, "length")) {
    return m_length;
  }
  else return LINE(1264,c_js_object::t_get(v_name));
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1266 */
Variant c_js_array::t_put(Variant v_name, Variant v_value, CVarRef v_opts //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(js_array, js_array::put);
  Primitive v_index = 0;

  (v_name = LINE(1267,x_strval(v_name)));
  if (equal(v_name, "length")) {
    if (less(v_value.o_get("value", 0x69E7413AE0C88471LL), m_length.o_get("value", 0x69E7413AE0C88471LL))) {
      {
        LOOP_COUNTER(29);
        Variant map30 = m_slots;
        for (ArrayIterPtr iter31 = map30.begin("js_array"); !iter31->end(); iter31->next()) {
          LOOP_COUNTER_CHECK(29);
          v_value = iter31->second();
          v_index = iter31->first();
          {
            if ((LINE(1274,x_is_numeric(v_index))) && (not_less(v_index, v_value.o_get("value", 0x69E7413AE0C88471LL)))) {
              LINE(1275,t_delete(v_index));
            }
          }
        }
      }
    }
    (m_length = v_value);
  }
  else {
    if (LINE(1281,x_is_numeric(v_name))) {
      if (not_less(v_name - 0LL, m_length.o_get("value", 0x69E7413AE0C88471LL))) {
        (m_length = LINE(1283,f_js_int(v_name + 1LL)));
      }
    }
    return (LINE(1286,c_js_object::t_put(v_name, v_value, v_opts)), null);
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1289 */
void c_js_array::t__push(Variant v_val) {
  INSTANCE_METHOD_INJECTION(js_array, js_array::_push);
  Variant v_v;

  (v_v = m_length.o_get("value", 0x69E7413AE0C88471LL));
  LINE(1291,o_root_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_v, v_val));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1294 */
Variant c_js_array::ti_tonativearray(const char* cls, CVarRef v_obj) {
  STATIC_METHOD_INJECTION(js_array, js_array::toNativeArray);
  Variant v_len;
  Variant v_arr;
  Variant v_i;

  (v_len = LINE(1295,toObject(v_obj)->o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")).o_get("value", 0x69E7413AE0C88471LL));
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(32);
    for ((v_i = 0LL); less(v_i, v_len); v_i++) {
      LOOP_COUNTER_CHECK(32);
      {
        v_arr.set(v_i, (LINE(1298,toObject(v_obj)->o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_i))));
      }
    }
  }
  return v_arr;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1305 */
Variant c_js_array::ti_object(const char* cls, int num_args, Variant v_value, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_array, js_array::object);
  p_js_array v_obj;
  Array v_contrived;

  if (((equal(LINE(1306,num_args), 1LL)) && (equal(v_value.o_get("type", 0x508FC7C8724A760ALL), 3LL /* js_val::NUMBER */))) && (equal(v_value.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0).o_get("value", 0x69E7413AE0C88471LL), v_value.o_get("value", 0x69E7413AE0C88471LL)))) {
    ((Object)((v_obj = ((Object)(LINE(1307,p_js_array(p_js_array(NEWOBJ(c_js_array)())->create())))))));
    LINE(1308,v_obj->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "length", v_value));
    return ((Object)(v_obj));
  }
  (v_contrived = LINE(1311,func_get_args(num_args, Array(ArrayInit(1).set(0, v_value).create()),args)));
  return LINE(1312,x_call_user_func_array(ScalarArrays::sa_[166], v_contrived));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1314 */
Variant c_js_array::ti_tostring(const char* cls) {
  STATIC_METHOD_INJECTION(js_array, js_array::toString);
  Variant v_obj;

  (v_obj = LINE(1315,c_jsrt::t_this()));
  if (!equal(LINE(1316,x_get_class(v_obj)), "js_array")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(1317,v_obj.o_invoke_few_args("defaultValue", 0x3C12431377CB2E03LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1319 */
Variant c_js_array::ti_tolocalestring(const char* cls) {
  STATIC_METHOD_INJECTION(js_array, js_array::toLocaleString);
  return LINE(1321,c_js_array::t_tostring());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1323 */
p_js_array c_js_array::ti_concat(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_array, js_array::concat);
  Variant eo_0;
  Variant eo_1;
  p_js_array v_array;
  Variant v_args;
  Variant v_this;
  Variant v_obj;
  Variant v_len;
  Variant v_k;

  ((Object)((v_array = ((Object)(LINE(1324,p_js_array(p_js_array(NEWOBJ(c_js_array)())->create())))))));
  (v_args = LINE(1325,func_get_args(num_args, Array(),args)));
  LINE(1326,(assignCallTemp(eo_0, ref(v_args)),assignCallTemp(eo_1, c_jsrt::os_invoke("jsrt", toString(v_this), Array(), -1)),x_array_unshift(2, eo_0, eo_1)));
  LOOP_COUNTER(33);
  {
    while (more(LINE(1327,x_count(v_args)), 0LL)) {
      LOOP_COUNTER_CHECK(33);
      {
        (v_obj = LINE(1328,x_array_shift(ref(v_args))));
        if (!equal(LINE(1329,x_get_class(v_obj)), "js_array")) {
          LINE(1330,v_array->t__push(v_obj));
        }
        else {
          (v_len = LINE(1332,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")).o_get("value", 0x69E7413AE0C88471LL));
          {
            LOOP_COUNTER(34);
            for ((v_k = 0LL); less(v_k, v_len); v_k++) {
              LOOP_COUNTER_CHECK(34);
              {
                if (toBoolean(LINE(1334,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_k)))) {
                  LINE(1335,v_array->t__push(v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_k)));
                }
              }
            }
          }
        }
      }
    }
  }
  return ((Object)(v_array));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1342 */
Variant c_js_array::ti_join(const char* cls, Variant v_sep) {
  STATIC_METHOD_INJECTION(js_array, js_array::join);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_len;
  Variant v_arr;
  Array v_arr2;
  Variant v_elem;

  (v_obj = LINE(1343,c_jsrt::t_this()));
  (v_len = (assignCallTemp(eo_0, toObject(LINE(1344,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (equal(v_sep, g->s_jsrt_DupIdundefined)) {
    (v_sep = ",");
  }
  else {
    (v_sep = LINE(1348,v_sep.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  }
  if (equal(v_len, 0LL)) return LINE(1350,f_js_str(""));
  (v_arr = LINE(1351,c_js_array::t_tonativearray(v_obj)));
  (v_arr2 = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(35);
    for (ArrayIterPtr iter37 = v_arr.begin("js_array"); !iter37->end(); iter37->next()) {
      LOOP_COUNTER_CHECK(35);
      v_elem = iter37->second();
      {
        LINE(1354,invoke_failed("array_push", Array(ArrayInit(1).set(0, ref(v_arr.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0))).create()), 0x000000001CE7D834LL));
      }
    }
  }
  return LINE(1356,f_js_str(x_implode(v_sep, v_arr2)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1358 */
Variant c_js_array::ti_pop(const char* cls) {
  STATIC_METHOD_INJECTION(js_array, js_array::pop);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_len;
  Variant v_index;
  Variant v_elem;

  (v_obj = LINE(1359,c_jsrt::t_this()));
  (v_len = (assignCallTemp(eo_0, toObject(LINE(1360,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)));
  if (equal(v_len.o_get("value", 0x69E7413AE0C88471LL), 0LL)) {
    LINE(1362,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "lengh", v_len));
    return g->s_jsrt_DupIdundefined;
  }
  (v_index = v_len.o_get("value", 0x69E7413AE0C88471LL) - 1LL);
  (v_elem = LINE(1366,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_index)));
  LINE(1367,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_index));
  (assignCallTemp(eo_2, ref(LINE(1368,f_js_int(v_index)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, "length").set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  return v_elem;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1371 */
Variant c_js_array::ti_push(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_array, js_array::push);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_obj;
  Variant v_n;
  Variant v_args;
  Variant v_arg;

  (v_obj = LINE(1372,c_jsrt::t_this()));
  (v_n = (assignCallTemp(eo_0, toObject(LINE(1373,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_args = LINE(1374,func_get_args(num_args, Array(),args)));
  LOOP_COUNTER(38);
  {
    while (more(LINE(1375,x_count(v_args)), 0LL)) {
      LOOP_COUNTER_CHECK(38);
      {
        (v_arg = LINE(1376,x_array_shift(ref(v_args))));
        LINE(1377,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_n, v_arg));
        v_n++;
      }
    }
  }
  (assignCallTemp(eo_2, ref(LINE(1380,f_js_int(v_n)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, "length").set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  return v_n;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1383 */
Variant c_js_array::ti_reverse(const char* cls) {
  STATIC_METHOD_INJECTION(js_array, js_array::reverse);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_obj;
  Variant v_len;
  double v_mid = 0.0;
  Variant v_k;
  Variant v_l;
  Variant v_a;

  (v_obj = LINE(1384,c_jsrt::t_this()));
  (v_len = (assignCallTemp(eo_0, toObject(LINE(1385,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_mid = LINE(1386,x_floor(toDouble(divide(v_len, 2LL)))));
  (v_k = 0LL);
  LOOP_COUNTER(39);
  {
    while (!equal(v_k, v_mid)) {
      LOOP_COUNTER_CHECK(39);
      {
        (v_l = v_len - v_k - 1LL);
        if (!(toBoolean(LINE(1390,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_k))))) {
          if (!(toBoolean(LINE(1391,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_l))))) {
            LINE(1392,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_k));
            LINE(1393,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_l));
          }
          else {
            (assignCallTemp(eo_1, ref(v_k)),assignCallTemp(eo_2, ref(LINE(1395,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_l)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
            LINE(1396,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_l));
          }
        }
        else {
          if (!(toBoolean(LINE(1399,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_l))))) {
            (assignCallTemp(eo_1, ref(v_l)),assignCallTemp(eo_2, ref(LINE(1400,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_k)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
            LINE(1401,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_k));
          }
          else {
            (v_a = LINE(1403,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_k)));
            (assignCallTemp(eo_1, ref(v_k)),assignCallTemp(eo_2, ref(LINE(1404,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_l)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
            LINE(1405,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_l, v_a));
          }
        }
        v_k++;
      }
    }
  }
  return v_obj;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1412 */
Variant c_js_array::ti_shift(const char* cls) {
  STATIC_METHOD_INJECTION(js_array, js_array::shift);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_len;
  Variant v_first;
  Variant v_k;

  (v_obj = LINE(1413,c_jsrt::t_this()));
  (v_len = (assignCallTemp(eo_0, toObject(LINE(1414,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (equal(v_len, 0LL)) {
    LINE(1416,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "length", 0LL));
    return g->s_jsrt_DupIdundefined;
  }
  (v_first = LINE(1419,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, 0LL)));
  (v_k = 1LL);
  LOOP_COUNTER(40);
  {
    while (!equal(v_k, v_len)) {
      LOOP_COUNTER_CHECK(40);
      {
        if (toBoolean(LINE(1422,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_k)))) {
          (assignCallTemp(eo_1, v_k - 1LL),assignCallTemp(eo_2, ref(LINE(1423,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_k)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
        }
        else {
          LINE(1425,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_k - 1LL));
        }
        v_k++;
      }
    }
  }
  LINE(1429,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_len - 1LL));
  LINE(1430,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "length", v_len - 1LL));
  return v_first;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1433 */
p_js_array c_js_array::ti_slice(const char* cls, Variant v_start, Variant v_end) {
  STATIC_METHOD_INJECTION(js_array, js_array::slice);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  p_js_array v_array;
  Variant v_len;
  Variant v_k;
  Variant v_n;

  (v_obj = LINE(1434,c_jsrt::t_this()));
  ((Object)((v_array = ((Object)(LINE(1435,p_js_array(p_js_array(NEWOBJ(c_js_array)())->create())))))));
  (v_len = (assignCallTemp(eo_0, toObject(LINE(1436,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_start = LINE(1437,v_start.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_k = (less(v_start, 0LL)) ? ((Variant)(LINE(1438,x_max(2, v_len + v_start, ScalarArrays::sa_[167])))) : ((Variant)(x_min(2, v_len, Array(ArrayInit(1).set(0, v_start).create())))));
  if (equal(v_end, g->s_jsrt_DupIdundefined)) (v_end = v_len);
  else (v_end = LINE(1439,v_end.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_end = (less(v_end, 0LL)) ? ((Variant)(LINE(1440,x_max(2, v_len + v_end, ScalarArrays::sa_[167])))) : ((Variant)(x_min(2, v_len, Array(ArrayInit(1).set(0, v_end).create())))));
  (v_n = 0LL);
  LOOP_COUNTER(41);
  {
    while (less(v_k, v_end)) {
      LOOP_COUNTER_CHECK(41);
      {
        if (toBoolean(LINE(1443,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_k)))) {
          (assignCallTemp(eo_1, ref(v_n)),assignCallTemp(eo_2, ref(LINE(1444,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_k)))),v_array->o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
        }
        v_k++;
        v_n++;
      }
    }
  }
  LINE(1449,v_array->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "length", v_n));
  return ((Object)(v_array));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1452 */
Variant c_js_array::ti_sort(const char* cls, CVarRef v_comparefn) {
  STATIC_METHOD_INJECTION(js_array, js_array::sort);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_arr;
  int v_len = 0;
  Variant v_i;

  (v_obj = LINE(1453,c_jsrt::t_this()));
  (v_arr = LINE(1454,c_js_array::t_tonativearray(v_obj)));
  (g->s_jsrt_DupIdsortfn = v_comparefn);
  LINE(1457,x_usort(ref(v_arr), ScalarArrays::sa_[168]));
  (g->s_jsrt_DupIdsortfn = null);
  (v_len = LINE(1459,x_count(v_arr)));
  {
    LOOP_COUNTER(42);
    for ((v_i = 0LL); less(v_i, v_len); v_i++) {
      LOOP_COUNTER_CHECK(42);
      {
        LINE(1461,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_i, v_arr.refvalAt(v_i)));
      }
    }
  }
  (assignCallTemp(eo_1, ref(LINE(1463,f_js_int(v_len)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, "length").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  return v_obj;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1466 */
Variant c_js_array::ti_sort_helper(const char* cls, Variant v_a, Variant v_b) {
  STATIC_METHOD_INJECTION(js_array, js_array::sort_helper);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  if (equal(v_a, g->s_jsrt_DupIdundefined)) {
    if (equal(v_b, g->s_jsrt_DupIdundefined)) {
      return 0LL;
    }
    else {
      return 1LL;
    }
  }
  else {
    if (equal(v_b, g->s_jsrt_DupIdundefined)) {
      return -1LL;
    }
  }
  if ((equal(g->s_jsrt_DupIdsortfn, null)) || (equal(g->s_jsrt_DupIdsortfn, g->s_jsrt_DupIdundefined))) {
    (v_a = LINE(1479,v_a.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
    (v_b = LINE(1480,v_b.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
    if (toBoolean(LINE(1481,f_js_bool(c_jsrt::t_expr_lt(v_a, v_b))))) return -1LL;
    if (toBoolean(LINE(1482,f_js_bool(c_jsrt::t_expr_gt(v_a, v_b))))) return 1LL;
    return 0LL;
  }
  else {
    return (assignCallTemp(eo_0, toObject(LINE(1485,g->s_jsrt_DupIdsortfn.o_invoke_few_args("_call", 0x70E3A91CE45475DBLL, 2, v_a, v_b)))),eo_0.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL);
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1488 */
p_js_array c_js_array::ti_splice(const char* cls, int num_args, Variant v_start, CVarRef v_deleteCount, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_array, js_array::splice);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant v_obj;
  Variant v_args;
  p_js_array v_array;
  Variant v_len;
  Variant v_count;
  Variant v_k;
  int v_nbitems = 0;
  Variant v_r22;
  Variant v_r23;
  Variant v_r39;
  Variant v_r40;

  (v_obj = LINE(1489,c_jsrt::t_this()));
  (v_args = LINE(1490,func_get_args(num_args, Array(ArrayInit(2).set(0, v_start).set(1, v_deleteCount).create()),args)));
  LINE(1491,x_array_shift(ref(v_args)));
  x_array_shift(ref(v_args));
  ((Object)((v_array = ((Object)(LINE(1492,p_js_array(p_js_array(NEWOBJ(c_js_array)())->create())))))));
  (v_len = (assignCallTemp(eo_0, toObject(LINE(1493,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_start = LINE(1494,v_start.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)));
  (v_start = (less(v_start, 0LL)) ? ((Variant)(LINE(1495,x_max(2, v_len + v_start, ScalarArrays::sa_[167])))) : ((Variant)(x_min(2, v_len, Array(ArrayInit(1).set(0, v_start).create())))));
  (v_count = LINE(1496,(assignCallTemp(eo_1, (assignCallTemp(eo_3, toObject(v_deleteCount)->o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)),x_max(2, eo_3, ScalarArrays::sa_[167]))),assignCallTemp(eo_2, v_len - v_start),x_min(2, eo_1, Array(ArrayInit(1).set(0, eo_2).create())))));
  (v_k = 0LL);
  LOOP_COUNTER(43);
  {
    while (!equal(v_k, v_count)) {
      LOOP_COUNTER_CHECK(43);
      {
        if (toBoolean(LINE(1499,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_start + v_k)))) {
          (assignCallTemp(eo_1, ref(v_k)),assignCallTemp(eo_2, ref(LINE(1500,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_start + v_k)))),v_array->o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
        }
        v_k++;
      }
    }
  }
  (assignCallTemp(eo_2, ref(LINE(1504,f_js_int(v_count)))),v_array->o_invoke("put", Array(ArrayInit(2).set(0, "length").set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  (v_nbitems = LINE(1505,x_count(v_args)));
  if (!equal(v_nbitems, v_count)) {
    if (not_more(v_nbitems, v_count)) {
      (v_k = v_start);
      LOOP_COUNTER(44);
      {
        while (!equal(v_k, v_len - v_count)) {
          LOOP_COUNTER_CHECK(44);
          {
            (v_r22 = v_k + v_count);
            (v_r23 = v_k + v_nbitems);
            if (toBoolean(LINE(1512,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_r22)))) {
              (assignCallTemp(eo_1, ref(v_r23)),assignCallTemp(eo_2, ref(LINE(1513,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_r22)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
            }
            else {
              LINE(1515,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_r23));
            }
            v_k++;
          }
        }
      }
      (v_k = v_len);
      LOOP_COUNTER(45);
      {
        while (!equal(v_k, v_len - v_count + v_nbitems)) {
          LOOP_COUNTER_CHECK(45);
          {
            LINE(1521,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_k - 1LL));
            v_k--;
          }
        }
      }
    }
    else {
      (v_k = v_len - v_count);
      LOOP_COUNTER(46);
      {
        while (!equal(v_k, v_start)) {
          LOOP_COUNTER_CHECK(46);
          {
            (v_r39 = v_k + v_count - 1LL);
            (v_r40 = v_k + v_nbitems - 1LL);
            if (toBoolean(LINE(1529,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_r39)))) {
              (assignCallTemp(eo_1, ref(v_r40)),assignCallTemp(eo_2, ref(LINE(1530,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_r39)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
            }
            else {
              LINE(1532,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_r40));
            }
            v_k--;
          }
        }
      }
    }
  }
  (v_k = v_start);
  LOOP_COUNTER(47);
  {
    while (more(LINE(1539,x_count(v_args)), 0LL)) {
      LOOP_COUNTER_CHECK(47);
      {
        (assignCallTemp(eo_1, v_k++),assignCallTemp(eo_2, ref(LINE(1540,x_array_shift(ref(v_args))))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
      }
    }
  }
  (assignCallTemp(eo_2, ref(LINE(1542,f_js_int(v_len - v_count + v_nbitems)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, "length").set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
  return ((Object)(v_array));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1545 */
Variant c_js_array::ti_unshift(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_array, js_array::unshift);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_obj;
  Variant v_len;
  Variant v_args;
  int v_nbitems = 0;
  Variant v_k;

  (v_obj = LINE(1546,c_jsrt::t_this()));
  (v_len = (assignCallTemp(eo_0, toObject(LINE(1547,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "length")))),eo_0.o_invoke_few_args("toUInt32", 0x3DED869A23F6201CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_args = LINE(1548,func_get_args(num_args, Array(),args)));
  (v_nbitems = LINE(1549,x_count(v_args)));
  (v_k = v_len);
  LOOP_COUNTER(48);
  {
    while (!equal(v_k, 0LL)) {
      LOOP_COUNTER_CHECK(48);
      {
        if (toBoolean(LINE(1552,v_obj.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, v_k - 1LL)))) {
          (assignCallTemp(eo_1, v_k + v_nbitems - 1LL),assignCallTemp(eo_2, ref(LINE(1553,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_k - 1LL)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
        }
        else {
          LINE(1555,v_obj.o_invoke_few_args("delete", 0x1C660E5BFA6F55D3LL, 1, v_k + v_nbitems - 1LL));
        }
        v_k--;
      }
    }
  }
  LOOP_COUNTER(49);
  {
    while (more(LINE(1559,x_count(v_args)), 0LL)) {
      LOOP_COUNTER_CHECK(49);
      {
        (assignCallTemp(eo_1, ref(v_k)),assignCallTemp(eo_2, ref(LINE(1560,x_array_shift(ref(v_args))))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()), 0x500F0D3CC9D8CC9ALL));
        v_k++;
      }
    }
  }
  LINE(1563,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "length", v_len + v_nbitems));
  return LINE(1564,f_js_int(v_len + v_nbitems));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1077 */
Variant c_js_function::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x4AA95713EFECB9DDLL, g->s_js_function_DupIdconstructor,
                  constructor);
      break;
    default:
      break;
  }
  return c_js_object::os_get(s, hash);
}
Variant &c_js_function::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x4AA95713EFECB9DDLL, g->s_js_function_DupIdconstructor,
                  constructor);
      break;
    default:
      break;
  }
  return c_js_object::os_lval(s, hash);
}
void c_js_function::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  props.push_back(NEW(ArrayElement)("phpname", m_phpname.isReferenced() ? ref(m_phpname) : m_phpname));
  props.push_back(NEW(ArrayElement)("args", m_args.isReferenced() ? ref(m_args) : m_args));
  props.push_back(NEW(ArrayElement)("scope", m_scope.isReferenced() ? ref(m_scope) : m_scope));
  c_js_object::o_get(props);
}
bool c_js_function::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      break;
    case 5:
      HASH_EXISTS_STRING(0x052824A24D6447E5LL, scope, 5);
      break;
    case 6:
      HASH_EXISTS_STRING(0x2B630FE459F2AD16LL, phpname, 7);
      HASH_EXISTS_STRING(0x4AF7CD17F976719ELL, args, 4);
      break;
    default:
      break;
  }
  return c_js_object::o_exists(s, hash);
}
Variant c_js_function::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    case 5:
      HASH_RETURN_STRING(0x052824A24D6447E5LL, m_scope,
                         scope, 5);
      break;
    case 6:
      HASH_RETURN_STRING(0x2B630FE459F2AD16LL, m_phpname,
                         phpname, 7);
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_js_object::o_get(s, hash);
}
Variant c_js_function::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      break;
    case 5:
      HASH_SET_STRING(0x052824A24D6447E5LL, m_scope,
                      scope, 5);
      break;
    case 6:
      HASH_SET_STRING(0x2B630FE459F2AD16LL, m_phpname,
                      phpname, 7);
      HASH_SET_STRING(0x4AF7CD17F976719ELL, m_args,
                      args, 4);
      break;
    default:
      break;
  }
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_function::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    case 5:
      HASH_RETURN_STRING(0x052824A24D6447E5LL, m_scope,
                         scope, 5);
      break;
    case 6:
      HASH_RETURN_STRING(0x2B630FE459F2AD16LL, m_phpname,
                         phpname, 7);
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_js_object::o_lval(s, hash);
}
Variant c_js_function::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_function)
ObjectData *c_js_function::create(Variant v_name //  = ""
, Variant v_phpname //  = "jsi_empty"
, Variant v_args //  = ScalarArrays::sa_[0]
, Variant v_scope //  = null
) {
  init();
  t___construct(v_name, v_phpname, v_args, v_scope);
  return this;
}
ObjectData *c_js_function::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    if (count == 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
void c_js_function::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  if (count == 3) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
}
ObjectData *c_js_function::cloneImpl() {
  c_js_function *obj = NEW(c_js_function)();
  cloneSet(obj);
  return obj;
}
void c_js_function::cloneSet(c_js_function *clone) {
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  clone->m_phpname = m_phpname.isReferenced() ? ref(m_phpname) : m_phpname;
  clone->m_args = m_args.isReferenced() ? ref(m_args) : m_args;
  clone->m_scope = m_scope.isReferenced() ? ref(m_scope) : m_scope;
  c_js_object::cloneSet(clone);
}
Variant c_js_function::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x02DAE20459D10F8ALL, func_call) {
        int count = params.size();
        if (count <= 1) return (ti_func_call(o_getClassName(),count, params.rvalAt(0)));
        return (ti_func_call(o_getClassName(),count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 24:
      HASH_GUARD(0x081CF9AB49391818LL, construct) {
        return (t_construct(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x6692FDC98094C599LL, func_apply) {
        return (ti_func_apply(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 31:
      HASH_GUARD(0x4A8DB595E42BB71FLL, func_object) {
        return (ti_func_object(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 57:
      HASH_GUARD(0x42EBEA91F36E9E39LL, func_tostring) {
        return (ti_func_tostring(o_getClassName()));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 91:
      HASH_GUARD(0x70E3A91CE45475DBLL, _call) {
        int count = params.size();
        if (count <= 1) return (t__call(params.rvalAt(0)));
        if (count == 2) return (t__call(params.rvalAt(0), params.rvalAt(1)));
        return (t__call(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        if (count == 3) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 121:
      HASH_GUARD(0x3B85D7AB6D6705F9LL, hasinstance) {
        return (t_hasinstance(params.rvalAt(0)));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_function::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 10:
      HASH_GUARD(0x02DAE20459D10F8ALL, func_call) {
        if (count <= 1) return (ti_func_call(o_getClassName(),count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_func_call(o_getClassName(),count,a0, params));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 24:
      HASH_GUARD(0x081CF9AB49391818LL, construct) {
        return (t_construct(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x6692FDC98094C599LL, func_apply) {
        return (ti_func_apply(o_getClassName(), a0, a1));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 31:
      HASH_GUARD(0x4A8DB595E42BB71FLL, func_object) {
        return (ti_func_object(o_getClassName(), a0), null);
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 57:
      HASH_GUARD(0x42EBEA91F36E9E39LL, func_tostring) {
        return (ti_func_tostring(o_getClassName()));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 91:
      HASH_GUARD(0x70E3A91CE45475DBLL, _call) {
        if (count <= 1) return (t__call(a0));
        if (count == 2) return (t__call(a0, a1));
        return (t__call(a0, a1, a2));
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        if (count == 3) return (t___construct(a0, a1, a2), null);
        return (t___construct(a0, a1, a2, a3), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 121:
      HASH_GUARD(0x3B85D7AB6D6705F9LL, hasinstance) {
        return (t_hasinstance(a0));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_function::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x02DAE20459D10F8ALL, func_call) {
        int count = params.size();
        if (count <= 1) return (ti_func_call(c,count, params.rvalAt(0)));
        return (ti_func_call(c,count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x6692FDC98094C599LL, func_apply) {
        return (ti_func_apply(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x42EBEA91F36E9E39LL, func_tostring) {
        return (ti_func_tostring(c));
      }
      break;
    case 26:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x4A8DB595E42BB71FLL, func_object) {
        return (ti_func_object(c, params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_function$os_get(const char *s) {
  return c_js_function::os_get(s, -1);
}
Variant &cw_js_function$os_lval(const char *s) {
  return c_js_function::os_lval(s, -1);
}
Variant cw_js_function$os_constant(const char *s) {
  return c_js_function::os_constant(s);
}
Variant cw_js_function$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_function::os_invoke(c, s, params, -1, fatal);
}
void c_js_function::init() {
  c_js_object::init();
  m_name = null;
  m_phpname = null;
  m_args = null;
  m_scope = ScalarArrays::sa_[0];
}
void c_js_function::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_js_function_DupIdconstructor = null;
}
void csi_js_function() {
  c_js_function::os_static_initializer();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1083 */
void c_js_function::t___construct(Variant v_name //  = ""
, Variant v_phpname //  = "jsi_empty"
, Variant v_args //  = ScalarArrays::sa_[0]
, Variant v_scope //  = null
) {
  INSTANCE_METHOD_INJECTION(js_function, js_function::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;

  LINE(1084,c_js_object::t___construct("Function", g->s_jsrt_DupIdproto_function));
  if (equal(v_scope, null)) (v_scope = g->s_jsrt_DupIdcontexts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("scope_chain", 0x0413391407EE12A6LL));
  (m_name = v_name);
  (m_phpname = v_phpname);
  (m_args = v_args);
  (m_scope = v_scope);
  (assignCallTemp(eo_1, ref(LINE(1090,(assignCallTemp(eo_4, x_count(v_args)),p_js_val(p_js_val(NEWOBJ(c_js_val)())->create(2, 3LL /* js_val::NUMBER */, eo_4)))))),o_root_invoke("put", Array(ArrayInit(3).set(0, "length").set(1, eo_1).set(2, Array(ArrayInit(3).set(0, "dontdelete").set(1, "readonly").set(2, "dontenum").create())).create()), 0x500F0D3CC9D8CC9ALL));
  (v_obj = ((Object)(LINE(1091,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create("Object"))))));
  LINE(1092,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, "constructor", this, Array(ArrayInit(1).set(0, "dontenum").create())));
  LINE(1093,o_root_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, "prototype", v_obj, Array(ArrayInit(1).set(0, "dontdelete").create())));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1095 */
Variant c_js_function::t_construct(CVarRef v_args) {
  INSTANCE_METHOD_INJECTION(js_function, js_function::construct);
  DECLARE_GLOBAL_VARIABLES(g);
  p_js_object v_obj;
  Variant v_proto;
  Variant v_v;

  ((Object)((v_obj = ((Object)(LINE(1096,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create("Object"))))))));
  (v_proto = LINE(1097,o_root_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "prototype")));
  if (equal(v_proto.o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */)) {
    (v_obj->m_prototype = v_proto);
  }
  else {
    (v_obj->m_prototype = g->s_jsrt_DupIdproto_object);
  }
  (v_v = LINE(1104,t__call(((Object)(v_obj)), v_args, 1LL)));
  if ((toBoolean(v_v)) && (equal(v_v.o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */))) return v_v;
  return ((Object)(v_obj));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1108 */
Variant c_js_function::t_defaultvalue(CVarRef v_iggy //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(js_function, js_function::defaultValue);
  String v_o;

  (v_o = LINE(1109,concat3("function ", toString(m_name), "(")));
  concat_assign(v_o, LINE(1110,x_implode(",", m_args)));
  concat_assign(v_o, ") {\n [ function body ] \n}\n");
  return LINE(1114,f_js_str(v_o));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1121 */
Variant c_js_function::t__call(CVarRef v_that, Variant v_args //  = ScalarArrays::sa_[0]
, CVarRef v_constructor //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_function, js_function::_call);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  p_js_object v_var;
  Variant v_arguments;
  int v_len = 0;
  Variant v_i;
  Variant v_scope;
  p_js_context v_context;
  Variant v_thrown;
  Variant v_v;
  p_exception v_e;

  (g->s_js_function_DupIdconstructor = v_constructor);
  ((Object)((v_var = ((Object)(LINE(1124,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create("Activation"))))))));
  (v_arguments = ((Object)(LINE(1126,p_js_object(p_js_object(NEWOBJ(c_js_object)())->create())))));
  LINE(1127,v_var->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "arguments", v_arguments));
  (v_len = LINE(1128,x_count(v_args)));
  {
    LOOP_COUNTER(50);
    for ((v_i = 0LL); less(v_i, LINE(1129,x_count(m_args))); v_i++) {
      LOOP_COUNTER_CHECK(50);
      {
        if (!(isset(v_args, v_i))) {
          v_args.set(v_i, (g->s_jsrt_DupIdundefined));
        }
        else {
          if (instanceOf(v_args.rvalAt(v_i), "js_ref")) {
            echo("<pre>");
            echo(LINE(1135,concat3("js_ref as ", toString(v_i), "-th argument of call\n")));
            LINE(1136,x_debug_print_backtrace());
            echo("</pre>");
          }
        }
        LINE(1141,v_var->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, m_args.refvalAt(v_i), v_args.refvalAt(v_i)));
        lval(v_arguments.o_lval("slots", 0x54140E0A541564C1LL)).set(m_args.rvalAt(v_i), (v_var->m_slots.rvalAt(m_args.rvalAt(v_i))));
        lval(v_arguments.o_lval("slots", 0x54140E0A541564C1LL)).set(v_i, (v_var->m_slots.rvalAt(m_args.rvalAt(v_i))));
      }
    }
  }
  if (more(v_len, LINE(1146,x_count(m_args)))) {
    {
      LOOP_COUNTER(51);
      for ((v_i = LINE(1148,x_count(m_args))); less(v_i, v_len); v_i++) {
        LOOP_COUNTER_CHECK(51);
        {
          LINE(1149,v_arguments.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, v_i, v_args.refvalAt(v_i)));
        }
      }
    }
  }
  LINE(1152,v_arguments.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, "callee", this, Array(ArrayInit(1).set(0, "dontenum").create())));
  (assignCallTemp(eo_1, ref(LINE(1153,p_js_val(p_js_val(NEWOBJ(c_js_val)())->create(2, 3LL /* js_val::NUMBER */, v_len))))),v_arguments.o_invoke("put", Array(ArrayInit(3).set(0, "length").set(1, eo_1).set(2, Array(ArrayInit(1).set(0, "dontenum").create())).create()), 0x500F0D3CC9D8CC9ALL));
  (v_scope = m_scope);
  LINE(1155,x_array_unshift(2, ref(v_scope), ((Object)(v_var))));
  ((Object)((v_context = ((Object)(LINE(1157,p_js_context(p_js_context(NEWOBJ(c_js_context)())->create(v_that, v_scope, ((Object)(v_var))))))))));
  LINE(1158,x_array_unshift(2, ref(g->s_jsrt_DupIdcontexts), ((Object)(v_context))));
  setNull(v_thrown);
  try {
    (v_v = LINE(1166,x_call_user_func_array(m_phpname, toArray(v_args))));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      (v_thrown = ((Object)(v_e)));
    } else {
      throw;
    }
  }
  LINE(1172,x_array_shift(ref(g->s_jsrt_DupIdcontexts)));
  if (!equal(v_thrown, null)) {
    throw_exception(toObject(v_thrown));
  }
  return toBoolean(v_v) ? ((Variant)(v_v)) : ((Variant)(g->s_jsrt_DupIdundefined));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1179 */
Variant c_js_function::t_hasinstance(Variant v_value) {
  INSTANCE_METHOD_INJECTION(js_function, js_function::hasInstance);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;

  if (!equal(v_value.o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */)) return g->s_jsrt_DupIdfalse;
  (v_obj = LINE(1181,o_root_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "prototype")));
  if (!equal(v_obj.o_get("type", 0x508FC7C8724A760ALL), 5LL /* js_val::OBJECT */)) throw_exception(((Object)(LINE(1182,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create("XXX"))))))))));
  {
    LOOP_COUNTER(52);
    do {
      LOOP_COUNTER_CHECK(52);
      {
        (v_value = v_value.o_get("prototype", 0x61340FF8BD9EDC7BLL));
        if (equal(v_value, null)) return g->s_jsrt_DupIdfalse;
        if (equal(v_obj, v_value)) return g->s_jsrt_DupIdtrue;
      }
    } while (true);
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1189 */
Variant c_js_function::ti_isconstructor(const char* cls) {
  STATIC_METHOD_INJECTION(js_function, js_function::isConstructor);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->s_js_function_DupIdconstructor;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1196 */
void c_js_function::ti_func_object(const char* cls, CVarRef v_value) {
  STATIC_METHOD_INJECTION(js_function, js_function::func_object);
  throw_exception(((Object)(LINE(1197,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_syntaxerror(p_js_syntaxerror(NEWOBJ(c_js_syntaxerror)())->create("new Function(..) not implemented"))))))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1199 */
Variant c_js_function::ti_func_tostring(const char* cls) {
  STATIC_METHOD_INJECTION(js_function, js_function::func_toString);
  Variant v_obj;

  (v_obj = LINE(1200,c_jsrt::t_this()));
  if (!equal(LINE(1201,x_get_class(v_obj)), "js_function")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(1202,v_obj.o_invoke_few_args("defaultValue", 0x3C12431377CB2E03LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1204 */
Variant c_js_function::ti_func_apply(const char* cls, Variant v_thisArg, Variant v_argArray) {
  STATIC_METHOD_INJECTION(js_function, js_function::func_apply);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_this;
  Variant v_obj;

  (v_obj = LINE(1205,c_jsrt::os_invoke("jsrt", toString(v_this), Array(), -1)));
  if (!equal(LINE(1206,x_get_class(v_obj)), "js_function")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  if ((equal(v_thisArg, g->s_jsrt_DupIdnull)) || (equal(v_thisArg, g->s_jsrt_DupIdundefined))) {
    (v_thisArg = g->s_jsrt_DupIdglobal);
  }
  else {
    (v_thisArg = LINE(1210,v_thisArg.o_invoke_few_args("toObject", 0x048BAB9757BD44B6LL, 0)));
  }
  if ((toBoolean((v_argArray = g->s_jsrt_DupIdnull))) || (equal(v_argArray, g->s_jsrt_DupIdundefined))) {
    (v_argArray = ScalarArrays::sa_[0]);
  }
  else {
    if (toBoolean(LINE(1216,v_argArray.o_invoke_few_args("hasProperty", 0x40C7B30DCB439C8FLL, 1, "length")))) {
      (v_argArray = LINE(1217,c_js_array::t_tonativearray(v_argArray)));
    }
    else {
      throw_exception(((Object)(LINE(1219,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create("second argument to apply() must be an array"))))))))));
    }
  }
  return LINE(1222,v_obj.o_invoke_few_args("_call", 0x70E3A91CE45475DBLL, 2, v_thisArg, v_argArray));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1224 */
Variant c_js_function::ti_func_call(const char* cls, int num_args, Variant v_thisArg, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_function, js_function::func_call);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_this;
  Variant v_obj;
  Variant v_args;

  (v_obj = LINE(1225,c_jsrt::os_invoke("jsrt", toString(v_this), Array(), -1)));
  if (!equal(LINE(1226,x_get_class(v_obj)), "js_function")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_args = LINE(1227,func_get_args(num_args, Array(ArrayInit(1).set(0, v_thisArg).create()),args)));
  LINE(1228,x_array_shift(ref(v_args)));
  if ((equal(v_thisArg, g->s_jsrt_DupIdnull)) || (equal(v_thisArg, g->s_jsrt_DupIdundefined))) {
    (v_thisArg = g->s_jsrt_DupIdglobal);
  }
  else {
    (v_thisArg = LINE(1232,v_thisArg.o_invoke_few_args("toObject", 0x048BAB9757BD44B6LL, 0)));
  }
  return LINE(1234,v_obj.o_invoke_few_args("_call", 0x70E3A91CE45475DBLL, 2, v_thisArg, v_args));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2358 */
Variant c_js_error::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_error::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_error::o_get(ArrayElementVec &props) const {
  c_js_object::o_get(props);
}
bool c_js_error::o_exists(CStrRef s, int64 hash) const {
  return c_js_object::o_exists(s, hash);
}
Variant c_js_error::o_get(CStrRef s, int64 hash) {
  return c_js_object::o_get(s, hash);
}
Variant c_js_error::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_error::o_lval(CStrRef s, int64 hash) {
  return c_js_object::o_lval(s, hash);
}
Variant c_js_error::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_error)
ObjectData *c_js_error::create(Variant v_class //  = "Error"
, Variant v_proto //  = null
, Variant v_msg //  = ""
) {
  init();
  t___construct(v_class, v_proto, v_msg);
  return this;
}
ObjectData *c_js_error::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_js_error::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_js_error::cloneImpl() {
  c_js_error *obj = NEW(c_js_error)();
  cloneSet(obj);
  return obj;
}
void c_js_error::cloneSet(c_js_error *clone) {
  c_js_object::cloneSet(clone);
}
Variant c_js_error::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_error::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_error::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_error$os_get(const char *s) {
  return c_js_error::os_get(s, -1);
}
Variant &cw_js_error$os_lval(const char *s) {
  return c_js_error::os_lval(s, -1);
}
Variant cw_js_error$os_constant(const char *s) {
  return c_js_error::os_constant(s);
}
Variant cw_js_error$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_error::os_invoke(c, s, params, -1, fatal);
}
void c_js_error::init() {
  c_js_object::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2359 */
void c_js_error::t___construct(Variant v_class //  = "Error"
, Variant v_proto //  = null
, Variant v_msg //  = ""
) {
  INSTANCE_METHOD_INJECTION(js_error, js_error::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(2360,c_js_object::t___construct(v_class, (equal(v_proto, null)) ? ((Variant)(g->s_jsrt_DupIdproto_error)) : ((Variant)(v_proto))));
  (assignCallTemp(eo_1, ref(LINE(2361,f_js_str(v_class)))),o_root_invoke("put", Array(ArrayInit(2).set(0, "name").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(2362,f_js_str(v_msg)))),o_root_invoke("put", Array(ArrayInit(2).set(0, "message").set(1, eo_1).create()), 0x500F0D3CC9D8CC9ALL));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2367 */
p_js_error c_js_error::ti_object(const char* cls, CVarRef v_message) {
  STATIC_METHOD_INJECTION(js_error, js_error::object);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return ((Object)(LINE(2368,(assignCallTemp(eo_2, toObject(v_message)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0).o_get("value", 0x69E7413AE0C88471LL)),p_js_error(p_js_error(NEWOBJ(c_js_error)())->create("Error", null, eo_2))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2370 */
Variant c_js_error::ti_tostring(const char* cls) {
  STATIC_METHOD_INJECTION(js_error, js_error::toString);
  Variant eo_0;
  Variant v_obj;

  (v_obj = LINE(2371,c_jsrt::t_this()));
  if (!((instanceOf(v_obj, "js_error")))) throw_exception(((Object)(LINE(2372,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(create_object("js_typeeror", Array())))))));
  return LINE(2373,f_js_str(concat_rev(toString((assignCallTemp(eo_0, toObject(v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "message"))),eo_0.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL)), concat(toString(x_get_class(v_obj)), ": "))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 768 */
const int64 q_js_val_UNDEFINED = 0LL;
const int64 q_js_val_NULL = 1LL;
const int64 q_js_val_BOOLEAN = 2LL;
const int64 q_js_val_NUMBER = 3LL;
const int64 q_js_val_STRING = 4LL;
const int64 q_js_val_OBJECT = 5LL;
const int64 q_js_val_REF = 6LL;
Variant c_js_val::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_js_val::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_js_val::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("type", m_type.isReferenced() ? ref(m_type) : m_type));
  props.push_back(NEW(ArrayElement)("value", m_value.isReferenced() ? ref(m_value) : m_value));
  c_ObjectData::o_get(props);
}
bool c_js_val::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x69E7413AE0C88471LL, value, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x508FC7C8724A760ALL, type, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_js_val::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x508FC7C8724A760ALL, m_type,
                         type, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_js_val::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x69E7413AE0C88471LL, m_value,
                      value, 5);
      break;
    case 2:
      HASH_SET_STRING(0x508FC7C8724A760ALL, m_type,
                      type, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_js_val::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x508FC7C8724A760ALL, m_type,
                         type, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_js_val::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 15) {
    case 1:
      HASH_RETURN(0x01F7BC290731BD81LL, q_js_val_BOOLEAN, BOOLEAN);
      break;
    case 3:
      HASH_RETURN(0x1CB0FF5DE5382423LL, q_js_val_UNDEFINED, UNDEFINED);
      HASH_RETURN(0x6F900C618583E4F3LL, q_js_val_NULL, NULL);
      break;
    case 4:
      HASH_RETURN(0x4FAA32115F163554LL, q_js_val_NUMBER, NUMBER);
      break;
    case 9:
      HASH_RETURN(0x15B369BE0D0C8149LL, q_js_val_STRING, STRING);
      break;
    case 10:
      HASH_RETURN(0x2F0C4AEA911ADA3ALL, q_js_val_OBJECT, OBJECT);
      break;
    case 14:
      HASH_RETURN(0x05FF98D1208F545ELL, q_js_val_REF, REF);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(js_val)
ObjectData *c_js_val::create(int num_args, Variant v_type, Variant v_value, Array args /* = Array() */) {
  init();
  t___construct(num_args, v_type, v_value,args);
  return this;
}
ObjectData *c_js_val::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(count, params.rvalAt(0), params.rvalAt(1)));
    return (create(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
  } else return this;
}
void c_js_val::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(count, params.rvalAt(0), params.rvalAt(1)));
  (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
}
ObjectData *c_js_val::cloneImpl() {
  c_js_val *obj = NEW(c_js_val)();
  cloneSet(obj);
  return obj;
}
void c_js_val::cloneSet(c_js_val *clone) {
  clone->m_type = m_type.isReferenced() ? ref(m_type) : m_type;
  clone->m_value = m_value.isReferenced() ? ref(m_value) : m_value;
  ObjectData::cloneSet(clone);
}
Variant c_js_val::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 3:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 11:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 16:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 22:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 23:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(count, params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)), null);
      }
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_js_val::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 3:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 11:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 16:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 22:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 23:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(count, a0, a1), null);
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t___construct(count,a0, a1, params), null);
      }
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_val::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_val$os_get(const char *s) {
  return c_js_val::os_get(s, -1);
}
Variant &cw_js_val$os_lval(const char *s) {
  return c_js_val::os_lval(s, -1);
}
Variant cw_js_val$os_constant(const char *s) {
  return c_js_val::os_constant(s);
}
Variant cw_js_val$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_val::os_invoke(c, s, params, -1, fatal);
}
void c_js_val::init() {
  m_type = null;
  m_value = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 778 */
void c_js_val::t___construct(int num_args, Variant v_type, Variant v_value, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(js_val, js_val::__construct);
  bool oldInCtor = gasInCtor(true);
  df_lambda_1(LINE(779,func_get_args(num_args, Array(ArrayInit(2).set(0, v_type).set(1, v_value).create()),args)), m_type, m_value);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 781 */
Variant c_js_val::t_toprimitive(Variant v_hint //  = null
) {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toPrimitive);
  Variant v_v;

  if (!equal(m_type, 5LL /* js_val::OBJECT */)) return ((Object)(this));
  if (!equal(v_hint, null)) {
    (v_v = LINE(784,o_root_invoke_few_args("defaultValue", 0x3C12431377CB2E03LL, 1, v_hint)));
  }
  else {
    (v_v = LINE(786,o_root_invoke_few_args("defaultValue", 0x3C12431377CB2E03LL, 0)));
  }
  return v_v;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 790 */
Variant c_js_val::t_toboolean() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toBoolean);
  DECLARE_GLOBAL_VARIABLES(g);
  {
    switch (toInt64(m_type)) {
    case 0LL:
      {
      }
    case 1LL:
      {
        return g->s_jsrt_DupIdfalse;
      }
    case 5LL:
      {
        return g->s_jsrt_DupIdtrue;
      }
    case 2LL:
      {
        return ((Object)(this));
      }
    case 3LL:
      {
        return ((equal(m_value, 0LL)) || (LINE(800,x_is_nan(toDouble(m_value))))) ? ((Variant)(g->s_jsrt_DupIdfalse)) : ((Variant)(g->s_jsrt_DupIdtrue));
      }
    case 4LL:
      {
        return (equal(LINE(802,x_strlen(toString(m_value))), 0LL)) ? ((Variant)(g->s_jsrt_DupIdfalse)) : ((Variant)(g->s_jsrt_DupIdtrue));
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 805 */
Variant c_js_val::t_tonumber() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toNumber);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  {
    switch (toInt64(m_type)) {
    case 0LL:
      {
        return g->s_jsrt_DupIdnan;
      }
    case 1LL:
      {
        return g->s_jsrt_DupIdzero;
      }
    case 2LL:
      {
        return toBoolean(m_value) ? ((Variant)(g->s_jsrt_DupIdone)) : ((Variant)(g->s_jsrt_DupIdzero));
      }
    case 3LL:
      {
        return ((Object)(this));
      }
    case 4LL:
      {
        return LINE(816,x_is_numeric(m_value)) ? ((Variant)(f_js_int(toDouble(m_value)))) : ((Variant)(g->s_jsrt_DupIdnan));
      }
    case 5LL:
      {
        return (assignCallTemp(eo_0, toObject(LINE(818,t_toprimitive(3LL /* js_val::NUMBER */)))),eo_0.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0));
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 821 */
Variant c_js_val::t_tointeger() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toInteger);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_v;

  (v_v = LINE(822,t_tonumber()));
  if (LINE(823,x_is_nan(toDouble(v_v.o_get("value", 0x69E7413AE0C88471LL))))) return g->s_jsrt_DupIdzero;
  if ((equal(v_v.o_get("value", 0x69E7413AE0C88471LL), 0LL)) || (LINE(824,x_is_infinite(toDouble(v_v.o_get("value", 0x69E7413AE0C88471LL)))))) return v_v;
  return LINE(825,f_js_int(multiply_rev(x_floor(toDouble(x_abs(v_v.o_get("value", 0x69E7413AE0C88471LL)))), divide(v_v.o_get("value", 0x69E7413AE0C88471LL), x_abs(v_v.o_get("value", 0x69E7413AE0C88471LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 827 */
Variant c_js_val::t_toint32() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toInt32);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_v;

  (v_v = LINE(828,t_tointeger()));
  if (LINE(829,x_is_infinite(toDouble(v_v.o_get("value", 0x69E7413AE0C88471LL))))) return g->s_jsrt_DupIdzero;
  return LINE(830,f_js_int(toInt64(v_v.o_get("value", 0x69E7413AE0C88471LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 832 */
Variant c_js_val::t_touint32() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toUInt32);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_v;

  (v_v = LINE(833,t_tointeger()));
  if (LINE(834,x_is_infinite(toDouble(v_v.o_get("value", 0x69E7413AE0C88471LL))))) return g->s_jsrt_DupIdzero;
  return LINE(835,f_js_int(x_bcmod(toString(v_v.o_get("value", 0x69E7413AE0C88471LL)), toString(4294967296LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 837 */
Variant c_js_val::t_touint16() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toUInt16);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_v;

  (v_v = LINE(838,t_tointeger()));
  if (LINE(839,x_is_infinite(toDouble(v_v.o_get("value", 0x69E7413AE0C88471LL))))) return g->s_jsrt_DupIdzero;
  return LINE(840,f_js_int(modulo(toInt64(v_v.o_get("value", 0x69E7413AE0C88471LL)), 65536LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 842 */
Variant c_js_val::t_tostr() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toStr);
  Variant eo_0;
  Variant eo_1;
  Variant v_v;

  {
    switch (toInt64(m_type)) {
    case 0LL:
      {
        return LINE(845,f_js_str("undefined"));
      }
    case 1LL:
      {
        return LINE(847,f_js_str("null"));
      }
    case 2LL:
      {
        return LINE(849,f_js_str(toBoolean(m_value) ? (("true")) : (("false"))));
      }
    case 4LL:
      {
        return ((Object)(this));
      }
    case 5LL:
      {
        return (assignCallTemp(eo_0, toObject(LINE(853,t_toprimitive(4LL /* js_val::STRING */)))),eo_0.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0));
      }
    case 3LL:
      {
        if (LINE(855,x_is_nan(toDouble(m_value)))) return f_js_str("NaN");
        if (equal(m_value, 0LL)) return LINE(856,f_js_str("0"));
        if (less(m_value, 0LL)) {
          (v_v = (assignCallTemp(eo_1, toObject(LINE(858,f_js_int(negate(m_value))))),eo_1.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
          return LINE(859,f_js_str(concat("-", toString(v_v.o_get("value", 0x69E7413AE0C88471LL)))));
        }
        if (LINE(861,x_is_infinite(toDouble(m_value)))) return f_js_str("Infinity");
        return LINE(862,f_js_str(toString(m_value)));
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 865 */
Variant c_js_val::t_toobject() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toObject);
  {
    switch (toInt64(m_type)) {
    case 0LL:
      {
      }
    case 1LL:
      {
        throw_exception(((Object)(LINE(869,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create("Cannot convert null or undefined to objects"))))))))));
        return null;
      }
    case 2LL:
      {
        return ((Object)(LINE(873,p_js_boolean(p_js_boolean(NEWOBJ(c_js_boolean)())->create(((Object)(this)))))));
      }
    case 3LL:
      {
        return ((Object)(LINE(875,p_js_number(p_js_number(NEWOBJ(c_js_number)())->create(((Object)(this)))))));
      }
    case 4LL:
      {
        return ((Object)(LINE(877,p_js_string(p_js_string(NEWOBJ(c_js_string)())->create(((Object)(this)))))));
      }
    case 5LL:
      {
        return ((Object)(this));
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 882 */
Variant c_js_val::t_todebug() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::toDebug);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_s;
  Primitive v_key = 0;
  Variant v_value;

  {
    switch (toInt64(m_type)) {
    case 0LL:
      {
        return "undefined";
      }
    case 1LL:
      {
        return "null";
      }
    case 2LL:
      {
        return toBoolean(m_value) ? (("true")) : (("false"));
      }
    case 3LL:
      {
        return m_value;
      }
    case 4LL:
      {
        return LINE(888,x_var_export(m_value, toBoolean(1LL)));
      }
    case 5LL:
      {
        (v_s = LINE(890,(assignCallTemp(eo_1, toString(x_get_class(((Object)(this))))),concat3("class: ", eo_1, "<br>"))));
        {
          LOOP_COUNTER(58);
          Variant map59 = o_get("slots", 0x54140E0A541564C1LL);
          for (ArrayIterPtr iter60 = map59.begin("js_val"); !iter60->end(); iter60->next()) {
            LOOP_COUNTER_CHECK(58);
            v_value = iter60->second();
            v_key = iter60->first();
            {
              concat_assign(v_s, LINE(892,concat3(toString(v_key) + toString(" => "), toString(v_value.o_get("value", 0x69E7413AE0C88471LL)), "<br>")));
            }
          }
        }
        return v_s;
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 897 */
p_js_val c_js_val::t_getvalue() {
  INSTANCE_METHOD_INJECTION(js_val, js_val::getValue);
  echo("##useless getValue##");
  LINE(899,x_flush());
  echo("<pre>");
  LINE(901,x_debug_print_backtrace());
  echo("</pre>");
  return ((Object)(this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 905 */
void c_js_val::t_putvalue(CVarRef v_w) {
  INSTANCE_METHOD_INJECTION(js_val, js_val::putValue);
  Variant v_v;

  throw_exception(((Object)(LINE(906,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_referenceerror(p_js_referenceerror(NEWOBJ(c_js_referenceerror)())->create(f_dump_object(v_v)))))))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1732 */
Variant c_js_boolean::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_boolean::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_boolean::o_get(ArrayElementVec &props) const {
  c_js_object::o_get(props);
}
bool c_js_boolean::o_exists(CStrRef s, int64 hash) const {
  return c_js_object::o_exists(s, hash);
}
Variant c_js_boolean::o_get(CStrRef s, int64 hash) {
  return c_js_object::o_get(s, hash);
}
Variant c_js_boolean::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_boolean::o_lval(CStrRef s, int64 hash) {
  return c_js_object::o_lval(s, hash);
}
Variant c_js_boolean::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_boolean)
ObjectData *c_js_boolean::create(Variant v_value //  = null
) {
  init();
  t___construct(v_value);
  return this;
}
ObjectData *c_js_boolean::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_boolean::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_boolean::cloneImpl() {
  c_js_boolean *obj = NEW(c_js_boolean)();
  cloneSet(obj);
  return obj;
}
void c_js_boolean::cloneSet(c_js_boolean *clone) {
  c_js_object::cloneSet(clone);
}
Variant c_js_boolean::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_boolean::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_boolean::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_boolean$os_get(const char *s) {
  return c_js_boolean::os_get(s, -1);
}
Variant &cw_js_boolean$os_lval(const char *s) {
  return c_js_boolean::os_lval(s, -1);
}
Variant cw_js_boolean$os_constant(const char *s) {
  return c_js_boolean::os_constant(s);
}
Variant cw_js_boolean$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_boolean::os_invoke(c, s, params, -1, fatal);
}
void c_js_boolean::init() {
  c_js_object::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1733 */
void c_js_boolean::t___construct(Variant v_value //  = null
) {
  INSTANCE_METHOD_INJECTION(js_boolean, js_boolean::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(1734,c_js_object::t___construct("Boolean", g->s_jsrt_DupIdproto_boolean));
  if (equal(v_value, null)) (v_value = g->s_jsrt_DupIdundefined);
  (m_value = LINE(1736,v_value.o_invoke_few_args("toBoolean", 0x52B7A8139051425DLL, 0)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1738 */
Variant c_js_boolean::t_defaultvalue(CVarRef v_iggy //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(js_boolean, js_boolean::defaultValue);
  return m_value;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1744 */
Variant c_js_boolean::ti_object(const char* cls, CVarRef v_value) {
  STATIC_METHOD_INJECTION(js_boolean, js_boolean::object);
  if (toBoolean(LINE(1745,c_js_function::t_isconstructor()))) {
    return ((Object)(LINE(1746,p_js_boolean(p_js_boolean(NEWOBJ(c_js_boolean)())->create(v_value)))));
  }
  else {
    return LINE(1748,toObject(v_value)->o_invoke_few_args("toBoolean", 0x52B7A8139051425DLL, 0));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1751 */
Variant c_js_boolean::ti_tostring(const char* cls) {
  STATIC_METHOD_INJECTION(js_boolean, js_boolean::toString);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;

  (v_obj = LINE(1752,c_jsrt::t_this()));
  if (!equal(LINE(1753,x_get_class(v_obj)), "js_boolean")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return equal(v_obj.o_get("value", 0x69E7413AE0C88471LL).o_get("value", 0x69E7413AE0C88471LL), g->s_jsrt_DupIdtrue) ? ((Variant)(LINE(1754,f_js_str("true")))) : ((Variant)(f_js_str("false")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1756 */
Variant c_js_boolean::ti_valueof(const char* cls) {
  STATIC_METHOD_INJECTION(js_boolean, js_boolean::valueOf);
  Variant v_obj;

  (v_obj = LINE(1757,c_jsrt::t_this()));
  if (!equal(LINE(1758,x_get_class(v_obj)), "js_boolean")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return v_obj.o_get("value", 0x69E7413AE0C88471LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2470 */
Variant c_js_ref_null::os_get(const char *s, int64 hash) {
  return c_js_ref::os_get(s, hash);
}
Variant &c_js_ref_null::os_lval(const char *s, int64 hash) {
  return c_js_ref::os_lval(s, hash);
}
void c_js_ref_null::o_get(ArrayElementVec &props) const {
  c_js_ref::o_get(props);
}
bool c_js_ref_null::o_exists(CStrRef s, int64 hash) const {
  return c_js_ref::o_exists(s, hash);
}
Variant c_js_ref_null::o_get(CStrRef s, int64 hash) {
  return c_js_ref::o_get(s, hash);
}
Variant c_js_ref_null::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_ref::o_set(s, hash, v, forInit);
}
Variant &c_js_ref_null::o_lval(CStrRef s, int64 hash) {
  return c_js_ref::o_lval(s, hash);
}
Variant c_js_ref_null::os_constant(const char *s) {
  return c_js_ref::os_constant(s);
}
IMPLEMENT_CLASS(js_ref_null)
ObjectData *c_js_ref_null::create(Variant v_propName) {
  init();
  t___construct(v_propName);
  return this;
}
ObjectData *c_js_ref_null::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_ref_null::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_ref_null::cloneImpl() {
  c_js_ref_null *obj = NEW(c_js_ref_null)();
  cloneSet(obj);
  return obj;
}
void c_js_ref_null::cloneSet(c_js_ref_null *clone) {
  c_js_ref::cloneSet(clone);
}
Variant c_js_ref_null::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        int count = params.size();
        if (count <= 1) return (t_putvalue(params.rvalAt(0)), null);
        return (t_putvalue(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_js_ref::o_invoke(s, params, hash, fatal);
}
Variant c_js_ref_null::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        if (count <= 1) return (t_putvalue(a0), null);
        return (t_putvalue(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_js_ref::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_ref_null::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_js_ref::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_ref_null$os_get(const char *s) {
  return c_js_ref_null::os_get(s, -1);
}
Variant &cw_js_ref_null$os_lval(const char *s) {
  return c_js_ref_null::os_lval(s, -1);
}
Variant cw_js_ref_null$os_constant(const char *s) {
  return c_js_ref_null::os_constant(s);
}
Variant cw_js_ref_null$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_ref_null::os_invoke(c, s, params, -1, fatal);
}
void c_js_ref_null::init() {
  c_js_ref::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2471 */
void c_js_ref_null::t___construct(Variant v_propName) {
  INSTANCE_METHOD_INJECTION(js_ref_null, js_ref_null::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(2472,c_js_ref::t___construct(null, v_propName));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2474 */
void c_js_ref_null::t_getvalue() {
  INSTANCE_METHOD_INJECTION(js_ref_null, js_ref_null::getValue);
  echo(LINE(2475,concat3("oops. trying to read ", toString(m_propName), ", but that's not defined.<hr>")));
  throw_exception(((Object)(LINE(2476,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_referenceerror(p_js_referenceerror(NEWOBJ(c_js_referenceerror)())->create(f_dump_object(((Object)(this)))))))))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2478 */
void c_js_ref_null::t_putvalue(Variant v_w, CVarRef v_ret //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(js_ref_null, js_ref_null::putValue);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(2479,g->s_jsrt_DupIdglobal.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, m_propName, v_w));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2280 */
Variant c_js_regexp::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_regexp::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_regexp::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("pattern", m_pattern.isReferenced() ? ref(m_pattern) : m_pattern));
  props.push_back(NEW(ArrayElement)("flags", m_flags.isReferenced() ? ref(m_flags) : m_flags));
  c_js_object::o_get(props);
}
bool c_js_regexp::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x029C0A5F2FB04920LL, pattern, 7);
      break;
    case 1:
      HASH_EXISTS_STRING(0x6AFDA85728FAE70DLL, flags, 5);
      break;
    default:
      break;
  }
  return c_js_object::o_exists(s, hash);
}
Variant c_js_regexp::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x029C0A5F2FB04920LL, m_pattern,
                         pattern, 7);
      break;
    case 1:
      HASH_RETURN_STRING(0x6AFDA85728FAE70DLL, m_flags,
                         flags, 5);
      break;
    default:
      break;
  }
  return c_js_object::o_get(s, hash);
}
Variant c_js_regexp::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x029C0A5F2FB04920LL, m_pattern,
                      pattern, 7);
      break;
    case 1:
      HASH_SET_STRING(0x6AFDA85728FAE70DLL, m_flags,
                      flags, 5);
      break;
    default:
      break;
  }
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_regexp::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x029C0A5F2FB04920LL, m_pattern,
                         pattern, 7);
      break;
    case 1:
      HASH_RETURN_STRING(0x6AFDA85728FAE70DLL, m_flags,
                         flags, 5);
      break;
    default:
      break;
  }
  return c_js_object::o_lval(s, hash);
}
Variant c_js_regexp::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_regexp)
ObjectData *c_js_regexp::create(Variant v_pattern //  = null
, Variant v_flags //  = null
) {
  init();
  t___construct(v_pattern, v_flags);
  return this;
}
ObjectData *c_js_regexp::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_js_regexp::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_js_regexp::cloneImpl() {
  c_js_regexp *obj = NEW(c_js_regexp)();
  cloneSet(obj);
  return obj;
}
void c_js_regexp::cloneSet(c_js_regexp *clone) {
  clone->m_pattern = m_pattern.isReferenced() ? ref(m_pattern) : m_pattern;
  clone->m_flags = m_flags.isReferenced() ? ref(m_flags) : m_flags;
  c_js_object::cloneSet(clone);
}
Variant c_js_regexp::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x4F1D1ED7B087208CLL, exec) {
        return (ti_exec(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 39:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (ti_test(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        int count = params.size();
        if (count <= 1) return (ti_object(o_getClassName(),count, params.rvalAt(0)));
        return (ti_object(o_getClassName(),count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_regexp::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x4F1D1ED7B087208CLL, exec) {
        return (ti_exec(o_getClassName(), a0));
      }
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 39:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (ti_test(o_getClassName(), a0));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        if (count <= 1) return (ti_object(o_getClassName(),count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_object(o_getClassName(),count,a0, params));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_regexp::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x37349B25A0ED29E7LL, test) {
        return (ti_test(c, params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        int count = params.size();
        if (count <= 1) return (ti_object(c,count, params.rvalAt(0)));
        return (ti_object(c,count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x4F1D1ED7B087208CLL, exec) {
        return (ti_exec(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_regexp$os_get(const char *s) {
  return c_js_regexp::os_get(s, -1);
}
Variant &cw_js_regexp$os_lval(const char *s) {
  return c_js_regexp::os_lval(s, -1);
}
Variant cw_js_regexp$os_constant(const char *s) {
  return c_js_regexp::os_constant(s);
}
Variant cw_js_regexp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_regexp::os_invoke(c, s, params, -1, fatal);
}
void c_js_regexp::init() {
  c_js_object::init();
  m_pattern = null;
  m_flags = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2283 */
void c_js_regexp::t___construct(Variant v_pattern //  = null
, Variant v_flags //  = null
) {
  INSTANCE_METHOD_INJECTION(js_regexp, js_regexp::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(2284,c_js_object::t___construct("RegExp", g->s_jsrt_DupIdproto_regexp));
  (m_pattern = v_pattern);
  (m_flags = v_flags);
  (assignCallTemp(eo_1, (!same(LINE(2287,x_strchr(toString(v_flags), "g")), false)) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse))),o_root_invoke("put", Array(ArrayInit(3).set(0, "global").set(1, eo_1).set(2, Array(ArrayInit(3).set(0, "dontdelete").set(1, "readonly").set(2, "dontenum").create())).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, (!same(LINE(2288,x_strchr(toString(v_flags), "i")), false)) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse))),o_root_invoke("put", Array(ArrayInit(3).set(0, "ignoreCase").set(1, eo_1).set(2, Array(ArrayInit(3).set(0, "dontdelete").set(1, "readonly").set(2, "dontenum").create())).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, (!same(LINE(2289,x_strchr(toString(v_flags), "m")), false)) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse))),o_root_invoke("put", Array(ArrayInit(3).set(0, "multiline").set(1, eo_1).set(2, Array(ArrayInit(3).set(0, "dontdelete").set(1, "readonly").set(2, "dontenum").create())).create()), 0x500F0D3CC9D8CC9ALL));
  (assignCallTemp(eo_1, ref(LINE(2290,f_js_str(v_pattern)))),o_root_invoke("put", Array(ArrayInit(3).set(0, "source").set(1, eo_1).set(2, Array(ArrayInit(3).set(0, "dontdelete").set(1, "readonly").set(2, "dontenum").create())).create()), 0x500F0D3CC9D8CC9ALL));
  LINE(2291,o_root_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 3, "lastIndex", g->s_jsrt_DupIdzero, Array(ArrayInit(2).set(0, "dontdelete").set(1, "dontenum").create())));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2296 */
Variant c_js_regexp::ti_object(const char* cls, int num_args, CVarRef v_value, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_regexp, js_regexp::object);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_pattern;
  Variant v_flags;

  df_lambda_4(LINE(2297,func_get_args(num_args, Array(ArrayInit(1).set(0, v_value).create()),args)), v_pattern, v_flags);
  if (((!(toBoolean(LINE(2298,c_js_function::t_isconstructor())))) && (equal(x_get_class(v_pattern), "js_regexp"))) && (equal(v_flags, g->s_jsrt_DupIdundefined))) {
    return v_pattern;
  }
  if (equal(LINE(2301,x_get_class(v_pattern)), "js_regexp")) {
    if (!equal(v_flags, g->s_jsrt_DupIdundefined)) {
      throw_exception(((Object)(LINE(2303,p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create())))))))));
    }
    (v_flags = v_pattern.o_get("flags", 0x6AFDA85728FAE70DLL));
    (v_pattern = v_pattern.o_get("pattern", 0x029C0A5F2FB04920LL));
  }
  else {
    (v_flags = (equal(v_flags, g->s_jsrt_DupIdundefined)) ? ((Variant)("")) : ((Variant)(LINE(2308,v_flags.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
    (v_pattern = (equal(v_pattern, g->s_jsrt_DupIdundefined)) ? ((Variant)("")) : ((Variant)(LINE(2309,v_pattern.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  }
  return ((Object)(LINE(2311,p_js_regexp(p_js_regexp(NEWOBJ(c_js_regexp)())->create(v_pattern, v_flags)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2313 */
Variant c_js_regexp::ti_exec(const char* cls, Variant v_str) {
  STATIC_METHOD_INJECTION(js_regexp, js_regexp::exec);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_s;
  int v_len = 0;
  Variant v_lastIndex;
  Variant v_i;
  Variant v_r;
  Variant v_e;
  Variant v_n;
  p_js_array v_array;

  (v_obj = LINE(2314,c_jsrt::t_this()));
  if (!equal(LINE(2315,x_get_class(v_obj)), "js_regexp")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_s = LINE(2316,v_str.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_len = LINE(2317,x_strlen(toString(v_s))));
  (v_lastIndex = (assignCallTemp(eo_0, toObject(LINE(2318,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "lastIndex")))),eo_0.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_i = v_lastIndex);
  if (equal((assignCallTemp(eo_1, toObject(LINE(2320,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "global")))),eo_1.o_invoke_few_args("toBoolean", 0x52B7A8139051425DLL, 0)).o_get("value", 0x69E7413AE0C88471LL), false)) (v_i = 0LL);
  {
    LOOP_COUNTER(61);
    do {
      LOOP_COUNTER_CHECK(61);
      {
        if ((less(v_i, 0LL)) || (more(v_i, v_len))) {
          LINE(2323,v_obj.o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "lastIndex", g->s_jsrt_DupIdzero));
          return g->s_jsrt_DupIdnull;
        }
        (v_r = LINE(2326,v_obj.o_invoke_few_args("match", 0x2B8782BF7206E093LL, 2, v_s, v_i)));
        v_i++;
      }
    } while (equal(v_r, null));
  }
  (v_e = v_r.rvalAt("endIndex", 0x214A95B5E4D176ECLL));
  (v_n = v_r.rvalAt("length", 0x2993E8EB119CAB21LL));
  if (equal((assignCallTemp(eo_2, toObject(LINE(2331,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "global")))),eo_2.o_invoke_few_args("toBoolean", 0x52B7A8139051425DLL, 0)).o_get("value", 0x69E7413AE0C88471LL), true)) {
    (assignCallTemp(eo_4, ref(LINE(2332,f_js_int(v_e)))),v_obj.o_invoke("put", Array(ArrayInit(2).set(0, "lastIndex").set(1, eo_4).create()), 0x500F0D3CC9D8CC9ALL));
  }
  ((Object)((v_array = ((Object)(LINE(2334,p_js_array(p_js_array(NEWOBJ(c_js_array)())->create())))))));
  (assignCallTemp(eo_4, ref(LINE(2335,f_js_int(v_i - 1LL)))),v_array->o_invoke("put", Array(ArrayInit(2).set(0, "index").set(1, eo_4).create()), 0x500F0D3CC9D8CC9ALL));
  LINE(2336,v_array->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "input", v_str));
  LINE(2337,v_array->o_invoke_few_args("put", 0x500F0D3CC9D8CC9ALL, 2, "length", v_n + 1LL));
  (assignCallTemp(eo_4, ref(LINE(2338,f_js_str(x_substr(toString(v_s), toInt32(v_i - 1LL), toInt32(v_e - v_i)))))),v_array->o_invoke("put", Array(ArrayInit(2).set(0, 0LL).set(1, eo_4).create()), 0x500F0D3CC9D8CC9ALL));
  {
    LOOP_COUNTER(62);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(62);
      {
        (assignCallTemp(eo_3, v_i + 1LL),assignCallTemp(eo_4, ref(LINE(2340,f_js_str(v_r.rvalAt(v_i))))),v_array->o_invoke("put", Array(ArrayInit(2).set(0, eo_3).set(1, eo_4).create()), 0x500F0D3CC9D8CC9ALL));
      }
    }
  }
  return ((Object)(v_array));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2344 */
Variant c_js_regexp::ti_test(const char* cls, CVarRef v_str) {
  STATIC_METHOD_INJECTION(js_regexp, js_regexp::test);
  DECLARE_GLOBAL_VARIABLES(g);
  return (!equal(LINE(2345,c_js_regexp::t_exec(v_str)), null)) ? ((Variant)(g->s_jsrt_DupIdtrue)) : ((Variant)(g->s_jsrt_DupIdfalse));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2347 */
Variant c_js_regexp::ti_tostring(const char* cls) {
  STATIC_METHOD_INJECTION(js_regexp, js_regexp::toString);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  String v_s;

  (v_obj = LINE(2348,c_jsrt::t_this()));
  if (!equal(LINE(2349,x_get_class(v_obj)), "js_regexp")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_s = LINE(2350,(assignCallTemp(eo_1, toString(x_str_replace(ScalarArrays::sa_[169], ScalarArrays::sa_[170], v_obj.o_get("pattern", 0x029C0A5F2FB04920LL)))),concat3("/", eo_1, "/"))));
  if (equal(LINE(2351,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "global")), g->s_jsrt_DupIdtrue)) concat_assign(v_s, "g");
  if (equal(LINE(2352,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "ignoreCase")), g->s_jsrt_DupIdtrue)) concat_assign(v_s, "i");
  if (equal(LINE(2353,v_obj.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, "multiline")), g->s_jsrt_DupIdtrue)) concat_assign(v_s, "m");
  return LINE(2354,f_js_str(v_s));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2409 */
Variant c_js_syntaxerror::os_get(const char *s, int64 hash) {
  return c_js_error::os_get(s, hash);
}
Variant &c_js_syntaxerror::os_lval(const char *s, int64 hash) {
  return c_js_error::os_lval(s, hash);
}
void c_js_syntaxerror::o_get(ArrayElementVec &props) const {
  c_js_error::o_get(props);
}
bool c_js_syntaxerror::o_exists(CStrRef s, int64 hash) const {
  return c_js_error::o_exists(s, hash);
}
Variant c_js_syntaxerror::o_get(CStrRef s, int64 hash) {
  return c_js_error::o_get(s, hash);
}
Variant c_js_syntaxerror::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_error::o_set(s, hash, v, forInit);
}
Variant &c_js_syntaxerror::o_lval(CStrRef s, int64 hash) {
  return c_js_error::o_lval(s, hash);
}
Variant c_js_syntaxerror::os_constant(const char *s) {
  return c_js_error::os_constant(s);
}
IMPLEMENT_CLASS(js_syntaxerror)
ObjectData *c_js_syntaxerror::create(Variant v_msg //  = ""
) {
  init();
  t___construct(v_msg);
  return this;
}
ObjectData *c_js_syntaxerror::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_syntaxerror::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_syntaxerror::cloneImpl() {
  c_js_syntaxerror *obj = NEW(c_js_syntaxerror)();
  cloneSet(obj);
  return obj;
}
void c_js_syntaxerror::cloneSet(c_js_syntaxerror *clone) {
  c_js_error::cloneSet(clone);
}
Variant c_js_syntaxerror::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke(s, params, hash, fatal);
}
Variant c_js_syntaxerror::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_syntaxerror::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_error::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_syntaxerror$os_get(const char *s) {
  return c_js_syntaxerror::os_get(s, -1);
}
Variant &cw_js_syntaxerror$os_lval(const char *s) {
  return c_js_syntaxerror::os_lval(s, -1);
}
Variant cw_js_syntaxerror$os_constant(const char *s) {
  return c_js_syntaxerror::os_constant(s);
}
Variant cw_js_syntaxerror$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_syntaxerror::os_invoke(c, s, params, -1, fatal);
}
void c_js_syntaxerror::init() {
  c_js_error::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2410 */
void c_js_syntaxerror::t___construct(Variant v_msg //  = ""
) {
  INSTANCE_METHOD_INJECTION(js_syntaxerror, js_syntaxerror::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(2411,c_js_error::t___construct("SyntaxError", g->s_jsrt_DupIdproto_syntaxerror, v_msg));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2416 */
p_js_syntaxerror c_js_syntaxerror::ti_object(const char* cls, CVarRef v_message) {
  STATIC_METHOD_INJECTION(js_syntaxerror, js_syntaxerror::object);
  return ((Object)(LINE(2417,p_js_syntaxerror(p_js_syntaxerror(NEWOBJ(c_js_syntaxerror)())->create(toObject(v_message)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0).o_get("value", 0x69E7413AE0C88471LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1763 */
Variant c_js_number::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_number::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_number::o_get(ArrayElementVec &props) const {
  c_js_object::o_get(props);
}
bool c_js_number::o_exists(CStrRef s, int64 hash) const {
  return c_js_object::o_exists(s, hash);
}
Variant c_js_number::o_get(CStrRef s, int64 hash) {
  return c_js_object::o_get(s, hash);
}
Variant c_js_number::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_number::o_lval(CStrRef s, int64 hash) {
  return c_js_object::o_lval(s, hash);
}
Variant c_js_number::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_number)
ObjectData *c_js_number::create(Variant v_value //  = null
) {
  init();
  t___construct(v_value);
  return this;
}
ObjectData *c_js_number::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_number::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_number::cloneImpl() {
  c_js_number *obj = NEW(c_js_number)();
  cloneSet(obj);
  return obj;
}
void c_js_number::cloneSet(c_js_number *clone) {
  c_js_object::cloneSet(clone);
}
Variant c_js_number::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x5C6E6B80A8E6E099LL, toprecision) {
        return (ti_toprecision(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        int count = params.size();
        if (count <= 0) return (ti_tostring(o_getClassName(),count));
        return (ti_tostring(o_getClassName(),count,params.slice(0, count - 0, false)));
      }
      break;
    case 85:
      HASH_GUARD(0x07311A008C1F78D5LL, toexponential) {
        return (ti_toexponential(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 121:
      HASH_GUARD(0x18D025D453809879LL, tofixed) {
        return (ti_tofixed(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_number::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 25:
      HASH_GUARD(0x5C6E6B80A8E6E099LL, toprecision) {
        return (ti_toprecision(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        if (count <= 0) return (ti_tostring(o_getClassName(),count));
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_tostring(o_getClassName(),count,params));
      }
      break;
    case 85:
      HASH_GUARD(0x07311A008C1F78D5LL, toexponential) {
        return (ti_toexponential(o_getClassName(), a0));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 121:
      HASH_GUARD(0x18D025D453809879LL, tofixed) {
        return (ti_tofixed(o_getClassName(), a0));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_number::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        int count = params.size();
        if (count <= 0) return (ti_tostring(c,count));
        return (ti_tostring(c,count,params.slice(0, count - 0, false)));
      }
      break;
    case 21:
      HASH_GUARD(0x07311A008C1F78D5LL, toexponential) {
        return (ti_toexponential(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x5C6E6B80A8E6E099LL, toprecision) {
        return (ti_toprecision(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x18D025D453809879LL, tofixed) {
        return (ti_tofixed(c, params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_number$os_get(const char *s) {
  return c_js_number::os_get(s, -1);
}
Variant &cw_js_number$os_lval(const char *s) {
  return c_js_number::os_lval(s, -1);
}
Variant cw_js_number$os_constant(const char *s) {
  return c_js_number::os_constant(s);
}
Variant cw_js_number$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_number::os_invoke(c, s, params, -1, fatal);
}
void c_js_number::init() {
  c_js_object::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1764 */
void c_js_number::t___construct(Variant v_value //  = null
) {
  INSTANCE_METHOD_INJECTION(js_number, js_number::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(1765,c_js_object::t___construct("Number", g->s_jsrt_DupIdproto_number));
  if (equal(v_value, null)) (v_value = g->s_jsrt_DupIdzero);
  (m_value = LINE(1767,v_value.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1769 */
Variant c_js_number::t_defaultvalue(CVarRef v_iggy //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(js_number, js_number::defaultValue);
  return m_value;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1775 */
Variant c_js_number::ti_object(const char* cls, CVarRef v_value) {
  STATIC_METHOD_INJECTION(js_number, js_number::object);
  if (toBoolean(LINE(1776,c_js_function::t_isconstructor()))) {
    return ((Object)(LINE(1777,p_js_number(p_js_number(NEWOBJ(c_js_number)())->create(v_value)))));
  }
  else {
    return LINE(1779,toObject(v_value)->o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1782 */
Variant c_js_number::ti_tostring(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_number, js_number::toString);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_radix;
  Variant v_obj;
  Variant v_x;
  String v_v;

  df_lambda_2(LINE(1783,func_get_args(num_args, Array(),args)), v_radix);
  (v_obj = LINE(1784,c_jsrt::t_this()));
  if (!equal(LINE(1785,x_get_class(v_obj)), "js_number")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  (v_x = LINE(1786,v_obj.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(1788,x_is_nan(toDouble(v_x)))) return f_js_str("NaN");
  if (equal(v_x, 0LL)) return LINE(1789,f_js_str("0"));
  if ((less(v_x, 0LL)) && (LINE(1790,x_is_infinite(toDouble(v_x))))) return f_js_str("-Infinity");
  if (LINE(1791,x_is_infinite(toDouble(v_x)))) return f_js_str("Infinity");
  (v_radix = (equal(v_radix, g->s_jsrt_DupIdundefined)) ? ((Variant)(10LL)) : ((Variant)(LINE(1793,v_radix.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL))));
  if (less(v_radix, 2LL) || more(v_radix, 36LL)) (v_radix = 10LL);
  (v_v = LINE(1795,x_base_convert(toString(v_x), 10LL, toInt64(v_radix))));
  if ((less(v_x, 0LL)) && (!equal(v_v.rvalAt(0LL), "-"))) (v_v = concat("-", v_v));
  return LINE(1797,f_js_str(v_v));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1799 */
Variant c_js_number::ti_valueof(const char* cls) {
  STATIC_METHOD_INJECTION(js_number, js_number::valueOf);
  Variant v_obj;

  (v_obj = LINE(1800,c_jsrt::t_this()));
  if (!equal(LINE(1801,x_get_class(v_obj)), "js_number")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return LINE(1802,v_obj.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1804 */
Variant c_js_number::ti_tofixed(const char* cls, CVarRef v_digits) {
  STATIC_METHOD_INJECTION(js_number, js_number::toFixed);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_obj;
  Variant v_f;
  Variant v_x;
  String v_s;
  Array v_k;

  (v_obj = LINE(1805,c_jsrt::t_this()));
  (v_f = LINE(1806,toObject(v_digits)->o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (less(v_f, 0LL) || more(v_f, 20LL)) throw_exception(toObject(LINE(1807,invoke_failed("js_exception", Array(ArrayInit(1).set(0, ref(invoke_failed("js_rangeerror", Array(), 0x0000000054173D4FLL))).create()), 0x00000000CBFFC6C8LL))));
  (v_x = LINE(1808,v_obj.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(1809,x_is_nan(toDouble(v_x)))) return f_js_str("NaN");
  if (LINE(1810,x_is_infinite(toDouble(v_x)))) return c_js_number::t_tostring(0);
  (v_s = LINE(1813,x_strval(v_x)));
  if (same(LINE(1814,x_strpos(v_s, ".")), false)) {
    return LINE(1815,f_js_str((assignCallTemp(eo_0, v_s),assignCallTemp(eo_2, x_str_repeat("0", toInt32(v_digits))),concat3(eo_0, ".", eo_2))));
  }
  (v_k = LINE(1817,x_explode(".", v_s)));
  if (more(v_f, LINE(1818,x_strlen(toString(v_k.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))))) {
    return LINE(1819,f_js_str((assignCallTemp(eo_0, toString(v_k.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))),assignCallTemp(eo_2, toString(v_k.rvalAt(1LL, 0x5BCA7C69B794F8CELL))),assignCallTemp(eo_3, (assignCallTemp(eo_5, toInt32(v_f - x_strlen(toString(v_k.rvalAt(1LL, 0x5BCA7C69B794F8CELL))))),x_str_repeat("0", eo_5))),concat4(eo_0, ".", eo_2, eo_3))));
  }
  else {
    return LINE(1821,f_js_str((assignCallTemp(eo_0, toString(v_k.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))),assignCallTemp(eo_2, toString(x_substr(toString(v_k.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(0LL), toInt32(v_f)))),concat3(eo_0, ".", eo_2))));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1824 */
Variant c_js_number::ti_toexponential(const char* cls, CVarRef v_digits) {
  STATIC_METHOD_INJECTION(js_number, js_number::toExponential);
  Variant eo_0;
  Variant eo_1;
  Variant v_obj;
  Variant v_f;
  Variant v_x;

  (v_obj = LINE(1825,c_jsrt::t_this()));
  (v_f = LINE(1826,toObject(v_digits)->o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (less(v_f, 0LL) || more(v_f, 20LL)) throw_exception(toObject(LINE(1827,invoke_failed("js_exception", Array(ArrayInit(1).set(0, ref(invoke_failed("js_rangeerror", Array(), 0x0000000054173D4FLL))).create()), 0x00000000CBFFC6C8LL))));
  (v_x = LINE(1828,v_obj.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(1829,x_is_nan(toDouble(v_x)))) return f_js_str("NaN");
  if (LINE(1830,x_is_infinite(toDouble(v_x)))) return c_js_number::t_tostring(0);
  return LINE(1831,f_js_str((assignCallTemp(eo_0, concat3("%.", (toString(1LL + v_f)), "e")),assignCallTemp(eo_1, v_x),x_sprintf(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1833 */
Variant c_js_number::ti_toprecision(const char* cls, CVarRef v_digits) {
  STATIC_METHOD_INJECTION(js_number, js_number::toPrecision);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_obj;
  Variant v_f;
  Variant v_x;

  (v_obj = LINE(1834,c_jsrt::t_this()));
  if (equal(v_digits, g->s_jsrt_DupIdundefined)) return LINE(1835,c_js_number::t_tostring(1, Array(ArrayInit(1).set(0, v_digits).create())));
  (v_f = LINE(1836,toObject(v_digits)->o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (less(v_f, 1LL) || more(v_f, 21LL)) throw_exception(toObject(LINE(1837,invoke_failed("js_exception", Array(ArrayInit(1).set(0, ref(invoke_failed("js_rangeerror", Array(), 0x0000000054173D4FLL))).create()), 0x00000000CBFFC6C8LL))));
  (v_x = LINE(1838,v_obj.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (LINE(1839,x_is_nan(toDouble(v_x)))) return f_js_str("NaN");
  if (LINE(1840,x_is_infinite(toDouble(v_x)))) return c_js_number::t_tostring(0);
  if (more(v_x, ((Variant)(toString("1e") + toString(v_f)) - 0LL)) || less(v_x, 9.9999999999999995E-7)) return LINE(1841,f_js_str((assignCallTemp(eo_0, concat3("%.", toString(v_f), "e")),assignCallTemp(eo_1, v_x),x_sprintf(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
  return LINE(1843,c_js_number::t_tofixed(v_digits));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2431 */
Variant c_js_urierror::os_get(const char *s, int64 hash) {
  return c_js_error::os_get(s, hash);
}
Variant &c_js_urierror::os_lval(const char *s, int64 hash) {
  return c_js_error::os_lval(s, hash);
}
void c_js_urierror::o_get(ArrayElementVec &props) const {
  c_js_error::o_get(props);
}
bool c_js_urierror::o_exists(CStrRef s, int64 hash) const {
  return c_js_error::o_exists(s, hash);
}
Variant c_js_urierror::o_get(CStrRef s, int64 hash) {
  return c_js_error::o_get(s, hash);
}
Variant c_js_urierror::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_error::o_set(s, hash, v, forInit);
}
Variant &c_js_urierror::o_lval(CStrRef s, int64 hash) {
  return c_js_error::o_lval(s, hash);
}
Variant c_js_urierror::os_constant(const char *s) {
  return c_js_error::os_constant(s);
}
IMPLEMENT_CLASS(js_urierror)
ObjectData *c_js_urierror::create(Variant v_msg //  = ""
) {
  init();
  t___construct(v_msg);
  return this;
}
ObjectData *c_js_urierror::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_urierror::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_urierror::cloneImpl() {
  c_js_urierror *obj = NEW(c_js_urierror)();
  cloneSet(obj);
  return obj;
}
void c_js_urierror::cloneSet(c_js_urierror *clone) {
  c_js_error::cloneSet(clone);
}
Variant c_js_urierror::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke(s, params, hash, fatal);
}
Variant c_js_urierror::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 3:
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 29:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 35:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 55:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 58:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    default:
      break;
  }
  return c_js_error::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_urierror::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      break;
    case 5:
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_js_error::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_urierror$os_get(const char *s) {
  return c_js_urierror::os_get(s, -1);
}
Variant &cw_js_urierror$os_lval(const char *s) {
  return c_js_urierror::os_lval(s, -1);
}
Variant cw_js_urierror$os_constant(const char *s) {
  return c_js_urierror::os_constant(s);
}
Variant cw_js_urierror$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_urierror::os_invoke(c, s, params, -1, fatal);
}
void c_js_urierror::init() {
  c_js_error::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2432 */
void c_js_urierror::t___construct(Variant v_msg //  = ""
) {
  INSTANCE_METHOD_INJECTION(js_urierror, js_urierror::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(2433,c_js_error::t___construct("URIError", g->s_jsrt_DupIdproto_urierror, v_msg));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 2438 */
p_js_urierror c_js_urierror::ti_object(const char* cls, CVarRef v_message) {
  STATIC_METHOD_INJECTION(js_urierror, js_urierror::object);
  return ((Object)(LINE(2439,p_js_urierror(p_js_urierror(NEWOBJ(c_js_urierror)())->create(toObject(v_message)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0).o_get("value", 0x69E7413AE0C88471LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1568 */
Variant c_js_string::os_get(const char *s, int64 hash) {
  return c_js_object::os_get(s, hash);
}
Variant &c_js_string::os_lval(const char *s, int64 hash) {
  return c_js_object::os_lval(s, hash);
}
void c_js_string::o_get(ArrayElementVec &props) const {
  c_js_object::o_get(props);
}
bool c_js_string::o_exists(CStrRef s, int64 hash) const {
  return c_js_object::o_exists(s, hash);
}
Variant c_js_string::o_get(CStrRef s, int64 hash) {
  return c_js_object::o_get(s, hash);
}
Variant c_js_string::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_js_object::o_set(s, hash, v, forInit);
}
Variant &c_js_string::o_lval(CStrRef s, int64 hash) {
  return c_js_object::o_lval(s, hash);
}
Variant c_js_string::os_constant(const char *s) {
  return c_js_object::os_constant(s);
}
IMPLEMENT_CLASS(js_string)
ObjectData *c_js_string::create(Variant v_value //  = null
) {
  init();
  t___construct(v_value);
  return this;
}
ObjectData *c_js_string::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_js_string::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_js_string::cloneImpl() {
  c_js_string *obj = NEW(c_js_string)();
  cloneSet(obj);
  return obj;
}
void c_js_string::cloneSet(c_js_string *clone) {
  c_js_object::cloneSet(clone);
}
Variant c_js_string::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x794972195C4F3503LL, localecompare) {
        return (ti_localecompare(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        int count = params.size();
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x5BE19573E82F848ALL, tolowercase) {
        return (ti_tolowercase(o_getClassName()));
      }
      break;
    case 11:
      HASH_GUARD(0x451A61D59FE8668BLL, charat) {
        return (ti_charat(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(params.rvalAt(0)));
      }
      HASH_GUARD(0x2B8782BF7206E093LL, match) {
        return (ti_match(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        int count = params.size();
        if (count <= 2) return (t_put(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_put(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 27:
      HASH_GUARD(0x0AC4D92ADC35BB1BLL, lastindexof) {
        return (ti_lastindexof(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 31:
      HASH_GUARD(0x3900A541CF3E599FLL, tolocaleuppercase) {
        return (ti_tolocaleuppercase(o_getClassName()));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0)));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        int count = params.size();
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(params.rvalAt(0)));
      }
      break;
    case 47:
      HASH_GUARD(0x45E450F6526A2CAFLL, substring) {
        return (ti_substring(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      HASH_GUARD(0x2E86294F9E00B1B8LL, fromcharcode) {
        int count = params.size();
        if (count <= 1) return (ti_fromcharcode(o_getClassName(),count, params.rvalAt(0)));
        return (ti_fromcharcode(o_getClassName(),count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x6C7CD1F942452DBFLL, touppercase) {
        return (ti_touppercase(o_getClassName()));
      }
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 66:
      HASH_GUARD(0x0030147962F9C942LL, tolocalelowercase) {
        return (ti_tolocalelowercase(o_getClassName()));
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x7CB0596B6CCFA945LL, split) {
        return (ti_split(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 70:
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (ti_replace(o_getClassName(), params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 75:
      HASH_GUARD(0x37F356F578FA394BLL, substr) {
        return (ti_substr(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(params.rvalAt(0)));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 87:
      HASH_GUARD(0x255540B5A68F3557LL, slice) {
        return (ti_slice(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 100:
      HASH_GUARD(0x6537B10BE6653AE4LL, charcodeat) {
        return (ti_charcodeat(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 102:
      HASH_GUARD(0x4C385230BFCC44E6LL, indexof) {
        return (ti_indexof(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), params.rvalAt(0)));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 108:
      HASH_GUARD(0x28FF22828CC2C4ECLL, search) {
        return (ti_search(o_getClassName(), params.rvalAt(0)), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(params.rvalAt(0)), null);
      }
      break;
    case 121:
      HASH_GUARD(0x09F8074DCD9353F9LL, concat) {
        int count = params.size();
        if (count <= 1) return (ti_concat(o_getClassName(),count, params.rvalAt(0)));
        return (ti_concat(o_getClassName(),count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke(s, params, hash, fatal);
}
Variant c_js_string::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 3:
      HASH_GUARD(0x794972195C4F3503LL, localecompare) {
        return (ti_localecompare(o_getClassName(), a0));
      }
      HASH_GUARD(0x3C12431377CB2E03LL, defaultvalue) {
        if (count <= 0) return (t_defaultvalue());
        return (t_defaultvalue(a0));
      }
      break;
    case 10:
      HASH_GUARD(0x5BE19573E82F848ALL, tolowercase) {
        return (ti_tolowercase(o_getClassName()));
      }
      break;
    case 11:
      HASH_GUARD(0x451A61D59FE8668BLL, charat) {
        return (ti_charat(o_getClassName(), a0));
      }
      break;
    case 12:
      HASH_GUARD(0x3155673280F79E0CLL, tonumber) {
        return (t_tonumber());
      }
      break;
    case 15:
      HASH_GUARD(0x40C7B30DCB439C8FLL, hasproperty) {
        return (t_hasproperty(a0));
      }
      break;
    case 19:
      HASH_GUARD(0x3B6F180556439C93LL, canput) {
        return (t_canput(a0));
      }
      HASH_GUARD(0x2B8782BF7206E093LL, match) {
        return (ti_match(o_getClassName(), a0));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(o_getClassName(), a0));
      }
      break;
    case 26:
      HASH_GUARD(0x500F0D3CC9D8CC9ALL, put) {
        if (count <= 2) return (t_put(a0, a1), null);
        return (t_put(a0, a1, a2), null);
      }
      break;
    case 27:
      HASH_GUARD(0x0AC4D92ADC35BB1BLL, lastindexof) {
        return (ti_lastindexof(o_getClassName(), a0, a1));
      }
      break;
    case 28:
      HASH_GUARD(0x3DED869A23F6201CLL, touint32) {
        return (t_touint32());
      }
      break;
    case 31:
      HASH_GUARD(0x3900A541CF3E599FLL, tolocaleuppercase) {
        return (ti_tolocaleuppercase(o_getClassName()));
      }
      break;
    case 40:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0));
      }
      break;
    case 43:
      HASH_GUARD(0x2F2C83503FD7D62BLL, toprimitive) {
        if (count <= 0) return (t_toprimitive());
        return (t_toprimitive(a0));
      }
      break;
    case 47:
      HASH_GUARD(0x45E450F6526A2CAFLL, substring) {
        return (ti_substring(o_getClassName(), a0, a1));
      }
      break;
    case 48:
      HASH_GUARD(0x673C107EF23EC4B0LL, touint16) {
        return (t_touint16());
      }
      break;
    case 54:
      HASH_GUARD(0x048BAB9757BD44B6LL, toobject) {
        return (t_toobject());
      }
      break;
    case 56:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      HASH_GUARD(0x2E86294F9E00B1B8LL, fromcharcode) {
        if (count <= 1) return (ti_fromcharcode(o_getClassName(),count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_fromcharcode(o_getClassName(),count,a0, params));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(o_getClassName(), a0));
      }
      HASH_GUARD(0x7112568F140CF4BALL, todebug) {
        return (t_todebug());
      }
      break;
    case 63:
      HASH_GUARD(0x6C7CD1F942452DBFLL, touppercase) {
        return (ti_touppercase(o_getClassName()));
      }
      HASH_GUARD(0x2994F14A3D82023FLL, toint32) {
        return (t_toint32());
      }
      break;
    case 66:
      HASH_GUARD(0x0030147962F9C942LL, tolocalelowercase) {
        return (ti_tolocalelowercase(o_getClassName()));
      }
      break;
    case 68:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 69:
      HASH_GUARD(0x7CB0596B6CCFA945LL, split) {
        return (ti_split(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(o_getClassName()));
      }
      break;
    case 70:
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (ti_replace(o_getClassName(), a0, a1), null);
      }
      break;
    case 72:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(o_getClassName(), a0));
      }
      break;
    case 75:
      HASH_GUARD(0x37F356F578FA394BLL, substr) {
        return (ti_substr(o_getClassName(), a0, a1));
      }
      break;
    case 81:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 83:
      HASH_GUARD(0x1C660E5BFA6F55D3LL, delete) {
        return (t_delete(a0));
      }
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(o_getClassName()));
      }
      break;
    case 87:
      HASH_GUARD(0x255540B5A68F3557LL, slice) {
        return (ti_slice(o_getClassName(), a0, a1));
      }
      break;
    case 90:
      HASH_GUARD(0x0274A0D333CA5D5ALL, tostr) {
        return (t_tostr());
      }
      break;
    case 92:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 93:
      HASH_GUARD(0x52B7A8139051425DLL, toboolean) {
        return (t_toboolean());
      }
      break;
    case 95:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 99:
      HASH_GUARD(0x56879BCEB40997E3LL, getvalue) {
        return (t_getvalue());
      }
      break;
    case 100:
      HASH_GUARD(0x6537B10BE6653AE4LL, charcodeat) {
        return (ti_charcodeat(o_getClassName(), a0));
      }
      break;
    case 102:
      HASH_GUARD(0x4C385230BFCC44E6LL, indexof) {
        return (ti_indexof(o_getClassName(), a0, a1));
      }
      break;
    case 106:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(o_getClassName(), a0));
      }
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 108:
      HASH_GUARD(0x28FF22828CC2C4ECLL, search) {
        return (ti_search(o_getClassName(), a0), null);
      }
      break;
    case 119:
      HASH_GUARD(0x33F5B388B34499F7LL, putvalue) {
        return (t_putvalue(a0), null);
      }
      break;
    case 121:
      HASH_GUARD(0x09F8074DCD9353F9LL, concat) {
        if (count <= 1) return (ti_concat(o_getClassName(),count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_concat(o_getClassName(),count,a0, params));
      }
      break;
    case 122:
      HASH_GUARD(0x246868594A0C657ALL, tointeger) {
        return (t_tointeger());
      }
      break;
    default:
      break;
  }
  return c_js_object::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_js_string::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 2:
      HASH_GUARD(0x0030147962F9C942LL, tolocalelowercase) {
        return (ti_tolocalelowercase(c));
      }
      break;
    case 3:
      HASH_GUARD(0x794972195C4F3503LL, localecompare) {
        return (ti_localecompare(c, params.rvalAt(0)));
      }
      break;
    case 5:
      HASH_GUARD(0x7CB0596B6CCFA945LL, split) {
        return (ti_split(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x4E4077ED548262C5LL, valueof) {
        return (ti_valueof(c));
      }
      break;
    case 6:
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (ti_replace(c, params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 8:
      HASH_GUARD(0x2311F4004B10A9C8LL, propertyisenumerable) {
        return (ti_propertyisenumerable(c, params.rvalAt(0)));
      }
      break;
    case 10:
      HASH_GUARD(0x5BE19573E82F848ALL, tolowercase) {
        return (ti_tolowercase(c));
      }
      break;
    case 11:
      HASH_GUARD(0x451A61D59FE8668BLL, charat) {
        return (ti_charat(c, params.rvalAt(0)));
      }
      HASH_GUARD(0x37F356F578FA394BLL, substr) {
        return (ti_substr(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 19:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (ti_tostring(c));
      }
      HASH_GUARD(0x2B8782BF7206E093LL, match) {
        return (ti_match(c, params.rvalAt(0)));
      }
      break;
    case 21:
      HASH_GUARD(0x26A0FE1D45784C15LL, isprototypeof) {
        return (ti_isprototypeof(c, params.rvalAt(0)));
      }
      break;
    case 23:
      HASH_GUARD(0x255540B5A68F3557LL, slice) {
        return (ti_slice(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 27:
      HASH_GUARD(0x0AC4D92ADC35BB1BLL, lastindexof) {
        return (ti_lastindexof(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 31:
      HASH_GUARD(0x3900A541CF3E599FLL, tolocaleuppercase) {
        return (ti_tolocaleuppercase(c));
      }
      break;
    case 36:
      HASH_GUARD(0x6537B10BE6653AE4LL, charcodeat) {
        return (ti_charcodeat(c, params.rvalAt(0)));
      }
      break;
    case 38:
      HASH_GUARD(0x4C385230BFCC44E6LL, indexof) {
        return (ti_indexof(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 42:
      HASH_GUARD(0x3F498A2A4324F0EALL, hasownproperty) {
        return (ti_hasownproperty(c, params.rvalAt(0)));
      }
      break;
    case 44:
      HASH_GUARD(0x28FF22828CC2C4ECLL, search) {
        return (ti_search(c, params.rvalAt(0)), null);
      }
      break;
    case 47:
      HASH_GUARD(0x45E450F6526A2CAFLL, substring) {
        return (ti_substring(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 56:
      HASH_GUARD(0x2E86294F9E00B1B8LL, fromcharcode) {
        int count = params.size();
        if (count <= 1) return (ti_fromcharcode(c,count, params.rvalAt(0)));
        return (ti_fromcharcode(c,count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 57:
      HASH_GUARD(0x09F8074DCD9353F9LL, concat) {
        int count = params.size();
        if (count <= 1) return (ti_concat(c,count, params.rvalAt(0)));
        return (ti_concat(c,count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 58:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (ti_object(c, params.rvalAt(0)));
      }
      break;
    case 63:
      HASH_GUARD(0x6C7CD1F942452DBFLL, touppercase) {
        return (ti_touppercase(c));
      }
      break;
    default:
      break;
  }
  return c_js_object::os_invoke(c, s, params, hash, fatal);
}
Variant cw_js_string$os_get(const char *s) {
  return c_js_string::os_get(s, -1);
}
Variant &cw_js_string$os_lval(const char *s) {
  return c_js_string::os_lval(s, -1);
}
Variant cw_js_string$os_constant(const char *s) {
  return c_js_string::os_constant(s);
}
Variant cw_js_string$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_js_string::os_invoke(c, s, params, -1, fatal);
}
void c_js_string::init() {
  c_js_object::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1569 */
void c_js_string::t___construct(Variant v_value //  = null
) {
  INSTANCE_METHOD_INJECTION(js_string, js_string::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  int v_len = 0;

  LINE(1570,c_js_object::t___construct("String", g->s_jsrt_DupIdproto_string));
  if ((equal(v_value, null)) || (equal(v_value, g->s_jsrt_DupIdundefined))) {
    (m_value = LINE(1572,f_js_str("")));
  }
  else {
    (m_value = LINE(1574,v_value.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)));
  }
  (v_len = LINE(1576,x_strlen(toString(m_value.o_get("value", 0x69E7413AE0C88471LL)))));
  if (!equal(g->s_jsrt_DupIdproto_string, null)) {
    (assignCallTemp(eo_1, ref(LINE(1578,f_js_int(v_len)))),o_root_invoke("put", Array(ArrayInit(3).set(0, "length").set(1, eo_1).set(2, Array(ArrayInit(3).set(0, "dontenum").set(1, "dontdelete").set(2, "readonly").create())).create()), 0x500F0D3CC9D8CC9ALL));
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1581 */
Variant c_js_string::t_defaultvalue(CVarRef v_iggy //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(js_string, js_string::defaultValue);
  return m_value;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1587 */
Variant c_js_string::ti_object(const char* cls, CVarRef v_value) {
  STATIC_METHOD_INJECTION(js_string, js_string::object);
  DECLARE_GLOBAL_VARIABLES(g);
  if (toBoolean(LINE(1588,c_js_function::t_isconstructor()))) {
    return ((Object)(LINE(1589,p_js_string(p_js_string(NEWOBJ(c_js_string)())->create(v_value)))));
  }
  else {
    if (equal(v_value, g->s_jsrt_DupIdundefined)) return LINE(1591,f_js_str(""));
    return LINE(1592,toObject(v_value)->o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1595 */
Variant c_js_string::ti_fromcharcode(const char* cls, int num_args, CVarRef v_c, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_string, js_string::fromCharCode);
  Array v_args;
  String v_s;
  Variant v_arg;
  Variant v_v;

  (v_args = LINE(1596,func_get_args(num_args, Array(ArrayInit(1).set(0, v_c).create()),args)));
  (v_s = "");
  {
    LOOP_COUNTER(63);
    for (ArrayIter iter65 = v_args.begin("js_string"); !iter65.end(); ++iter65) {
      LOOP_COUNTER_CHECK(63);
      v_arg = iter65.second();
      {
        (v_v = LINE(1599,v_arg.o_invoke_few_args("toUInt16", 0x673C107EF23EC4B0LL, 0)).o_get("value", 0x69E7413AE0C88471LL));
        concat_assign(v_s, LINE(1600,x_chr(toInt64(v_v))));
      }
    }
  }
  return LINE(1602,f_js_str(v_s));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1604 */
Variant c_js_string::ti_tostring(const char* cls) {
  STATIC_METHOD_INJECTION(js_string, js_string::toString);
  Variant v_obj;

  (v_obj = LINE(1605,c_jsrt::t_this()));
  if (!equal(LINE(1606,x_get_class(v_obj)), "js_string")) throw_exception(((Object)(p_js_exception(p_js_exception(NEWOBJ(c_js_exception)())->create(((Object)(p_js_typeerror(p_js_typeerror(NEWOBJ(c_js_typeerror)())->create()))))))));
  return v_obj.o_get("value", 0x69E7413AE0C88471LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1609 */
Variant c_js_string::ti_charat(const char* cls, Variant v_pos) {
  STATIC_METHOD_INJECTION(js_string, js_string::charAt);
  Variant eo_0;
  Variant v_str;

  (v_str = (assignCallTemp(eo_0, toObject(LINE(1610,c_jsrt::t_this()))),eo_0.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_pos = LINE(1611,v_pos.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (less(v_pos, 0LL) || not_more(LINE(1612,x_strlen(toString(v_str))), v_pos)) return f_js_str("");
  return LINE(1613,f_js_str(v_str.rvalAt(v_pos)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1615 */
Variant c_js_string::ti_charcodeat(const char* cls, Variant v_pos) {
  STATIC_METHOD_INJECTION(js_string, js_string::charCodeAt);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_str;

  (v_str = (assignCallTemp(eo_0, toObject(LINE(1616,c_jsrt::t_this()))),eo_0.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_pos = LINE(1617,v_pos.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  if (less(v_pos, 0LL) || not_more(LINE(1618,x_strlen(toString(v_str))), v_pos)) return g->s_jsrt_DupIdnan;
  return LINE(1619,f_js_int(x_ord(toString(v_str.rvalAt(v_pos)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1621 */
Variant c_js_string::ti_concat(const char* cls, int num_args, Variant v_str, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(js_string, js_string::concat);
  Variant eo_0;
  Array v_args;
  Variant v_arg;

  (v_str = (assignCallTemp(eo_0, toObject(LINE(1622,c_jsrt::t_this()))),eo_0.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_args = LINE(1623,func_get_args(num_args, Array(ArrayInit(1).set(0, v_str).create()),args)));
  {
    LOOP_COUNTER(66);
    for (ArrayIter iter68 = v_args.begin("js_string"); !iter68.end(); ++iter68) {
      LOOP_COUNTER_CHECK(66);
      v_arg = iter68.second();
      {
        concat_assign(v_str, toString(LINE(1625,v_arg.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL)));
      }
    }
  }
  return LINE(1627,f_js_str(v_str));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1629 */
Variant c_js_string::ti_indexof(const char* cls, Variant v_str, Variant v_pos) {
  STATIC_METHOD_INJECTION(js_string, js_string::indexOf);
  Variant eo_0;
  Variant v_obj;
  Variant v_v;

  (v_obj = (assignCallTemp(eo_0, toObject(LINE(1630,c_jsrt::t_this()))),eo_0.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_str = LINE(1631,v_str.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_pos = LINE(1632,v_pos.o_invoke_few_args("toInteger", 0x246868594A0C657ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_v = LINE(1633,x_strpos(toString(v_obj), v_str, toInt32(v_pos))));
  if (same(v_v, false)) return LINE(1634,f_js_int(-1LL));
  return LINE(1635,f_js_int(v_v));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/jsrt.php line 1637 */
Variant c_js_string::ti_lastindexof(const char* cls, Variant v_str, Variant v_pos) {
  STATIC_METHOD_INJECTION(js_string, js_string::lastIndexOf);
  Variant eo_0;
  Variant v_obj;
  Variant v_v;

  (v_obj = (assignCallTemp(eo_0, toObject(LINE(1638,c_jsrt::t_this()))),eo_0.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_str = LINE(1639,v_str.o_invoke_few_args("toStr", 0x0274A0D333CA5D5ALL, 0)).o_get("value", 0x69E7413AE0C88471LL));
  (v_pos = LINE(1640,v_pos.o_invoke_few_args("toNumber", 0x3155673280F79E0CLL, 0)).o_get("value", 0x69E7413AE0C88471LL