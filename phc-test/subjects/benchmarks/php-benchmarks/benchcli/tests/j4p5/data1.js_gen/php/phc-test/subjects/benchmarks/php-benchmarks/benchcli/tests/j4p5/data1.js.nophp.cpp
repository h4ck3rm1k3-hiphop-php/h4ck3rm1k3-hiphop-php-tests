
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/data1.js.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$data1_js(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/data1.js);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$data1_js;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("/* define a few useful functions */\r\nMath.rand = function(l) {\r\n  return Math.floor(Math.random()*l);\r\n};\r\nString.prototype.randomChar = function() {\r\n  var pos = Math.rand(this.length);\r\n  return this.charAt(pos);\r\n};\r\nfunction printbr(str) {\r\n    // We don't want any output.\r\n\treturn true;\r\n}\r\n/* contrived function to use closures. */\r\nfunction oper(op, y) {\r\n  switch (op) {\r\n    case '+': retur");
  echo("n function(x) { return x+y; };\r\n    case '-': return function(x) { return x-y; };\r\n    case '/': return function(x) { return x/y; };\r\n    case '*': return function(x) { return x*y; };\r\n    case '%': return function(x) { return x%y; };\r\n    case 'r': return function(x) { return oper(\"+-/*%\".randomChar(), y)(x); };\r\n    default:\r\n      print(\"not idea what to do with [\"+op+\"]<br>\");\r\n      return fun");
  echo("ction(x) { return x; };\r\n  }\r\n}\r\nvar mod256 = oper(\"%\", 256);\r\nvar inc = oper(\"+\", 1);\r\nvar half =oper(\"/\", 2);\r\nprintbr(\"37812 mod256 = \"+mod256(37812));\r\nprintbr(\"5 +1 = \"+inc(5));\r\nprintbr(\"half of 894 is \"+half(894));\r\n\r\n//\r\n\r\nvar rand13=oper(\"r\",13);\r\ndo {\r\n  printbr(\"49 \? 13 = \"+rand13(49));\r\n} while (rand13(13)!=1);");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
