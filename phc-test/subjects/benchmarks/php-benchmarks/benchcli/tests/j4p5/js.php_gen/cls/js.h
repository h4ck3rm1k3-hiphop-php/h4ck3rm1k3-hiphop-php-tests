
#ifndef __GENERATED_cls_js_h__
#define __GENERATED_cls_js_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php line 41 */
class c_js : virtual public ObjectData {
  BEGIN_CLASS_MAP(js)
  END_CLASS_MAP(js)
  DECLARE_CLASS(js, js, ObjectData)
  void init();
  public: static void ti_run(const char* cls, Variant v_src, Variant v_mode = k_JS_DIRECT, Variant v_id = null);
  public: static void ti_init(const char* cls);
  public: static void ti_define(const char* cls, CVarRef v_objname, CArrRef v_funcarray, CVarRef v_vararray = null_variant);
  public: static void t_define(CVarRef v_objname, CArrRef v_funcarray, CVarRef v_vararray = null_variant) { ti_define("js", v_objname, v_funcarray, v_vararray); }
  public: static void t_run(CVarRef v_src, CVarRef v_mode = k_JS_DIRECT, CVarRef v_id = null_variant) { ti_run("js", v_src, v_mode, v_id); }
  public: static void t_init() { ti_init("js"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_js_h__
