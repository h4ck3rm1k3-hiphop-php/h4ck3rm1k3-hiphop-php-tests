
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_js_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_js_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.fw.h>

// Declarations
#include <cls/js.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_enum(int num_args, Array args = Array());
String f_highlight_linenum(CVarRef v_path);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_js(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_j4p5_js_h__
