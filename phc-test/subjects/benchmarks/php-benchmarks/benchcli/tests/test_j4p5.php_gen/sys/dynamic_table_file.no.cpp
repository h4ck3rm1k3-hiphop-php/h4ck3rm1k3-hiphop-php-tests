
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_j4p5_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_INCLUDE(0x1CA9987E4FF28929LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_j4p5.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_j4p5_php);
      HASH_INCLUDE(0x227604EDB96C6A7DLL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
