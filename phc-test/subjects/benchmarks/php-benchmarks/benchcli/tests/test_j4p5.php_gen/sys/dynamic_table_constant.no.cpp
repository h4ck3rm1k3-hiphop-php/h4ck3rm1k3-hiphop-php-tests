
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_JS_DEBUG;
extern const StaticString k_JS_INLINE;
extern const StaticString k_JS_DIRECT;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x5C98FE8C71508EC0LL, k_JS_DEBUG, JS_DEBUG);
      break;
    case 1:
      HASH_RETURN(0x26718D221C6809A9LL, g->k_JS_CACHE_DIR, JS_CACHE_DIR);
      break;
    case 2:
      HASH_RETURN(0x0FBB23A8298B0212LL, k_JS_DIRECT, JS_DIRECT);
      break;
    case 7:
      HASH_RETURN(0x56A246F18C1B0DB7LL, k_JS_INLINE, JS_INLINE);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
