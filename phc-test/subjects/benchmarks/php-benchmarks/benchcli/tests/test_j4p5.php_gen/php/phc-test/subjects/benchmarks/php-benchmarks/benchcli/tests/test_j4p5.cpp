
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/js.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_j4p5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_j4p5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_j4p5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_j4p5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_code __attribute__((__unused__)) = (variables != gVariables) ? variables->get("code") : g->GV(code);

  LINE(13,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$j4p5$js_php(true, variables));
  (v_code = LINE(15,x_file_get_contents("test/subjects/benchmarks/php-benchmarks/benchcli/tests/j4p5/data1.js")));
  LINE(17,c_js::t_run(v_code));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
