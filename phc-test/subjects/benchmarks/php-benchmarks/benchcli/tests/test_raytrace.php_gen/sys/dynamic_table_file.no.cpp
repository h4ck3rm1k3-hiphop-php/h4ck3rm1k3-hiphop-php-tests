
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_raytrace_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Sphere_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Solid_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Plane_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Chessboard_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Light_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Engine_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Vector_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Background_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Color_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$IntersectionInfo_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 0:
      HASH_INCLUDE(0x20F1B0C38F280040LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Color_php);
      break;
    case 1:
      HASH_INCLUDE(0x3C43B8B60CE9B6A1LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Vector_php);
      break;
    case 4:
      HASH_INCLUDE(0x419DBDA0F05AD804LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Solid_php);
      break;
    case 8:
      HASH_INCLUDE(0x376FC81FD2905728LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Chessboard_php);
      HASH_INCLUDE(0x18F76F61C74CA688LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php);
      break;
    case 9:
      HASH_INCLUDE(0x172BFD78EE60CFA9LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_raytrace.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_raytrace_php);
      break;
    case 12:
      HASH_INCLUDE(0x7DF106999833A18CLL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$IntersectionInfo_php);
      break;
    case 14:
      HASH_INCLUDE(0x5B6291D7794706CELL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Background_php);
      break;
    case 17:
      HASH_INCLUDE(0x69132964327EA0D1LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Sphere_php);
      break;
    case 22:
      HASH_INCLUDE(0x05D1A45A4735A856LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Engine_php);
      HASH_INCLUDE(0x61B326FC13F12716LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php);
      break;
    case 23:
      HASH_INCLUDE(0x541E7537910AD757LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Light_php);
      break;
    case 27:
      HASH_INCLUDE(0x5CAA536C541B74FBLL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Plane_php);
      break;
    case 29:
      HASH_INCLUDE(0x28ABCFD63EBCE23DLL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
