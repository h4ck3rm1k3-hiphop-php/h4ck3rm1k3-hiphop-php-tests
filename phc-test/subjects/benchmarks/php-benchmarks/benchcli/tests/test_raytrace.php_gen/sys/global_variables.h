
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(i)
  END_GVS(1)

  // Dynamic Constants

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_raytrace_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Sphere_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Solid_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Plane_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Chessboard_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Light_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Engine_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Vector_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Background_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Color_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$IntersectionInfo_php;
  bool run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 13;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[3];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
