
#ifndef __GENERATED_cls_raytracer_color_h__
#define __GENERATED_cls_raytracer_color_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 2 */
class c_raytracer_color : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_color)
  END_CLASS_MAP(raytracer_color)
  DECLARE_CLASS(raytracer_color, RayTracer_Color, ObjectData)
  void init();
  public: Variant m_red;
  public: Variant m_green;
  public: Variant m_blue;
  public: void t___construct(Variant v_r = 0.0, Variant v_g = 0.0, Variant v_b = 0.0);
  public: ObjectData *create(Variant v_r = 0.0, Variant v_g = 0.0, Variant v_b = 0.0);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static p_raytracer_color ti_add(const char* cls, p_raytracer_color v_c1, p_raytracer_color v_c2);
  public: static p_raytracer_color ti_addscalar(const char* cls, p_raytracer_color v_c1, double v_s);
  public: static p_raytracer_color ti_subtract(const char* cls, p_raytracer_color v_c1, p_raytracer_color v_c2);
  public: static p_raytracer_color ti_multiply(const char* cls, CVarRef v_c1, p_raytracer_color v_c2);
  public: static p_raytracer_color ti_multiplyscalar(const char* cls, CVarRef v_c1, CVarRef v_f);
  public: static p_raytracer_color ti_dividefactor(const char* cls, p_raytracer_color v_c1, CVarRef v_f);
  public: void t_limit();
  public: PlusOperand t_distance(p_raytracer_color v_color);
  public: static p_raytracer_color ti_blend(const char* cls, p_raytracer_color v_c1, CVarRef v_c2, CVarRef v_w);
  public: int64 t_brightness();
  public: String t___tostring();
  public: static p_raytracer_color t_dividefactor(p_raytracer_color v_c1, CVarRef v_f) { return ti_dividefactor("raytracer_color", v_c1, v_f); }
  public: static p_raytracer_color t_addscalar(p_raytracer_color v_c1, double v_s) { return ti_addscalar("raytracer_color", v_c1, v_s); }
  public: static p_raytracer_color t_subtract(p_raytracer_color v_c1, p_raytracer_color v_c2) { return ti_subtract("raytracer_color", v_c1, v_c2); }
  public: static p_raytracer_color t_add(p_raytracer_color v_c1, p_raytracer_color v_c2) { return ti_add("raytracer_color", v_c1, v_c2); }
  public: static p_raytracer_color t_blend(p_raytracer_color v_c1, CVarRef v_c2, CVarRef v_w) { return ti_blend("raytracer_color", v_c1, v_c2, v_w); }
  public: static p_raytracer_color t_multiply(CVarRef v_c1, p_raytracer_color v_c2) { return ti_multiply("raytracer_color", v_c1, v_c2); }
  public: static p_raytracer_color t_multiplyscalar(CVarRef v_c1, CVarRef v_f) { return ti_multiplyscalar("raytracer_color", v_c1, v_f); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_color_h__
