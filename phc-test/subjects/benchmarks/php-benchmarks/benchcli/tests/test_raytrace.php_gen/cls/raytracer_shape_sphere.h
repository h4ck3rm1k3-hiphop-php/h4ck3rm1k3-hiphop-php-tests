
#ifndef __GENERATED_cls_raytracer_shape_sphere_h__
#define __GENERATED_cls_raytracer_shape_sphere_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php line 2 */
class c_raytracer_shape_sphere : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_shape_sphere)
  END_CLASS_MAP(raytracer_shape_sphere)
  DECLARE_CLASS(raytracer_shape_sphere, RayTracer_Shape_Sphere, ObjectData)
  void init();
  public: void t___construct(p_raytracer_vector v_pos, Variant v_radius, p_raytracer_material_solid v_material);
  public: ObjectData *create(p_raytracer_vector v_pos, Variant v_radius, p_raytracer_material_solid v_material);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: p_raytracer_intersectioninfo t_intersect(CVarRef v_ray);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_shape_sphere_h__
