
#ifndef __GENERATED_cls_raytracer_vector_h__
#define __GENERATED_cls_raytracer_vector_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 2 */
class c_raytracer_vector : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_vector)
  END_CLASS_MAP(raytracer_vector)
  DECLARE_CLASS(raytracer_vector, RayTracer_Vector, ObjectData)
  void init();
  public: Variant m_x;
  public: Variant m_y;
  public: Variant m_z;
  public: void t___construct(Variant v_x = 0.0, Variant v_y = 0.0, Variant v_z = 0.0);
  public: ObjectData *create(Variant v_x = 0.0, Variant v_y = 0.0, Variant v_z = 0.0);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_copy(p_raytracer_vector v_vector);
  public: p_raytracer_vector t_normalize();
  public: double t_magnitude();
  public: p_raytracer_vector t_cross(CVarRef v_w);
  public: PlusOperand t_dot(CVarRef v_w);
  public: static p_raytracer_vector ti_add(const char* cls, CVarRef v_v, CVarRef v_w);
  public: static p_raytracer_vector ti_subtract(const char* cls, CVarRef v_v, CVarRef v_w);
  public: static p_raytracer_vector ti_multiplyvector(const char* cls, p_raytracer_vector v_v, p_raytracer_vector v_w);
  public: static p_raytracer_vector ti_multiplyscalar(const char* cls, CVarRef v_v, CVarRef v_w);
  public: String t___tostring();
  public: static p_raytracer_vector t_subtract(CVarRef v_v, CVarRef v_w) { return ti_subtract("raytracer_vector", v_v, v_w); }
  public: static p_raytracer_vector t_add(CVarRef v_v, CVarRef v_w) { return ti_add("raytracer_vector", v_v, v_w); }
  public: static p_raytracer_vector t_multiplyvector(p_raytracer_vector v_v, p_raytracer_vector v_w) { return ti_multiplyvector("raytracer_vector", v_v, v_w); }
  public: static p_raytracer_vector t_multiplyscalar(CVarRef v_v, CVarRef v_w) { return ti_multiplyscalar("raytracer_vector", v_v, v_w); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_vector_h__
