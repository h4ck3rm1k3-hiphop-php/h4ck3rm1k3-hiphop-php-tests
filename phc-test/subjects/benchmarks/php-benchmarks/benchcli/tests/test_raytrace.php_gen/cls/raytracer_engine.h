
#ifndef __GENERATED_cls_raytracer_engine_h__
#define __GENERATED_cls_raytracer_engine_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 3 */
class c_raytracer_engine : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_engine)
  END_CLASS_MAP(raytracer_engine)
  DECLARE_CLASS(raytracer_engine, RayTracer_Engine, ObjectData)
  void init();
  public: Variant m_canvas;
  public: void t___construct(Variant v_options = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_options = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_setpixel(int64 v_x, int64 v_y, CVarRef v_color);
  public: void t_renderscene(p_raytracer_scene v_scene, CVarRef v_canvas);
  public: Variant t_getpixelcolor(CVarRef v_ray, p_raytracer_scene v_scene);
  public: Variant t_testintersection(Variant v_ray, p_raytracer_scene v_scene, CVarRef v_exclude);
  public: p_raytracer_ray t_getreflectionray(CVarRef v_P, CVarRef v_N, Variant v_V);
  public: p_raytracer_color t_raytrace(CVarRef v_info, CVarRef v_ray, p_raytracer_scene v_scene, int64 v_depth);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_engine_h__
