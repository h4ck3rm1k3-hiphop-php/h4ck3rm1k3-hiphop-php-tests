
#ifndef __GENERATED_cls_raytracer_intersectioninfo_h__
#define __GENERATED_cls_raytracer_intersectioninfo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.php line 2 */
class c_raytracer_intersectioninfo : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_intersectioninfo)
  END_CLASS_MAP(raytracer_intersectioninfo)
  DECLARE_CLASS(raytracer_intersectioninfo, RayTracer_IntersectionInfo, ObjectData)
  void init();
  public: bool m_isHit;
  public: Variant m_hitCount;
  public: Variant m_shape;
  public: Variant m_position;
  public: Variant m_normal;
  public: Variant m_color;
  public: Variant m_distance;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_intersectioninfo_h__
