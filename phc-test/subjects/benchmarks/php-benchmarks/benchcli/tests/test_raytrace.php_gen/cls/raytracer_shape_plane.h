
#ifndef __GENERATED_cls_raytracer_shape_plane_h__
#define __GENERATED_cls_raytracer_shape_plane_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php line 3 */
class c_raytracer_shape_plane : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_shape_plane)
  END_CLASS_MAP(raytracer_shape_plane)
  DECLARE_CLASS(raytracer_shape_plane, RayTracer_Shape_Plane, raytracer_shape_baseshape)
  void init();
  public: Variant m_d;
  public: void t___construct(p_raytracer_vector v_pos, Variant v_d, p_raytracer_material_chessboard v_material);
  public: ObjectData *create(p_raytracer_vector v_pos, Variant v_d, p_raytracer_material_chessboard v_material);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: p_raytracer_intersectioninfo t_intersect(CVarRef v_ray);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_shape_plane_h__
