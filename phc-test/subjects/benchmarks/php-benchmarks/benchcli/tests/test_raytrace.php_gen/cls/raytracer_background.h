
#ifndef __GENERATED_cls_raytracer_background_h__
#define __GENERATED_cls_raytracer_background_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.php line 2 */
class c_raytracer_background : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_background)
  END_CLASS_MAP(raytracer_background)
  DECLARE_CLASS(raytracer_background, RayTracer_Background, ObjectData)
  void init();
  public: Variant m_color;
  public: Variant m_ambience;
  public: void t___construct(p_raytracer_color v_color, Variant v_ambience);
  public: ObjectData *create(p_raytracer_color v_color, Variant v_ambience);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_background_h__
