
#ifndef __GENERATED_cls_raytracer_light_h__
#define __GENERATED_cls_raytracer_light_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php line 2 */
class c_raytracer_light : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_light)
  END_CLASS_MAP(raytracer_light)
  DECLARE_CLASS(raytracer_light, RayTracer_Light, ObjectData)
  void init();
  public: Variant m_position;
  public: Variant m_color;
  public: Variant m_intensity;
  public: void t___construct(p_raytracer_vector v_pos, p_raytracer_color v_color, Variant v_intensity = 10.0);
  public: ObjectData *create(p_raytracer_vector v_pos, p_raytracer_color v_color, Variant v_intensity = 10.0);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Numeric t_getintensity(CVarRef v_distance);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_light_h__
