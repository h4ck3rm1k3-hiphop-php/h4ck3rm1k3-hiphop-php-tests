
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php line 2 */
Variant c_raytracer_shape_sphere::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_shape_sphere::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_shape_sphere::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_raytracer_shape_sphere::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_shape_sphere::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_shape_sphere::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_shape_sphere::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_shape_sphere::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_shape_sphere)
ObjectData *c_raytracer_shape_sphere::create(p_raytracer_vector v_pos, Variant v_radius, p_raytracer_material_solid v_material) {
  init();
  t___construct(v_pos, v_radius, v_material);
  return this;
}
ObjectData *c_raytracer_shape_sphere::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_raytracer_shape_sphere::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_raytracer_shape_sphere::cloneImpl() {
  c_raytracer_shape_sphere *obj = NEW(c_raytracer_shape_sphere)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_shape_sphere::cloneSet(c_raytracer_shape_sphere *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_shape_sphere::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_shape_sphere::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_shape_sphere::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_shape_sphere$os_get(const char *s) {
  return c_raytracer_shape_sphere::os_get(s, -1);
}
Variant &cw_raytracer_shape_sphere$os_lval(const char *s) {
  return c_raytracer_shape_sphere::os_lval(s, -1);
}
Variant cw_raytracer_shape_sphere$os_constant(const char *s) {
  return c_raytracer_shape_sphere::os_constant(s);
}
Variant cw_raytracer_shape_sphere$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_shape_sphere::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_shape_sphere::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php line 11 */
void c_raytracer_shape_sphere::t___construct(p_raytracer_vector v_pos, Variant v_radius, p_raytracer_material_solid v_material) {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_Sphere, RayTracer_Shape_Sphere::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("radius", 0x7FC3CA32D2D213D5LL) = v_radius);
  (o_lval("position", 0x461E5B8BBDC024D0LL) = ((Object)(v_pos)));
  (o_lval("material", 0x43CC34CAFFD815F7LL) = ((Object)(v_material)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php line 18 */
p_raytracer_intersectioninfo c_raytracer_shape_sphere::t_intersect(CVarRef v_ray) {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_Sphere, RayTracer_Shape_Sphere::intersect);
  Variant eo_0;
  Variant eo_1;
  p_raytracer_intersectioninfo v_info;
  Variant v_dst;
  Variant v_B;
  Numeric v_C = 0;
  Numeric v_D = 0;

  ((Object)((v_info = ((Object)(LINE(20,p_raytracer_intersectioninfo(p_raytracer_intersectioninfo(NEWOBJ(c_raytracer_intersectioninfo)())->create())))))));
  (v_info->m_shape = ((Object)(this)));
  (v_dst = ((Object)(LINE(23,c_raytracer_vector::t_subtract(toObject(v_ray).o_get("position", 0x461E5B8BBDC024D0LL), o_get("position", 0x461E5B8BBDC024D0LL))))));
  (v_B = LINE(25,v_dst.o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, toObject(v_ray).o_lval("direction", 0x10014728FF258839LL))));
  (v_C = LINE(26,v_dst.o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, v_dst)) - (o_get("radius", 0x7FC3CA32D2D213D5LL) * o_get("radius", 0x7FC3CA32D2D213D5LL)));
  (v_D = (v_B * v_B) - v_C);
  if (more(v_D, 0LL)) {
    (v_info->m_isHit = true);
    (v_info->m_distance = toDouble((negate(v_B))) - LINE(31,x_sqrt(toDouble(v_D))));
    (v_info->m_position = ((Object)(LINE(38,(assignCallTemp(eo_0, toObject(v_ray).o_get("position", 0x461E5B8BBDC024D0LL)),assignCallTemp(eo_1, ((Object)(LINE(37,c_raytracer_vector::t_multiplyscalar(toObject(v_ray).o_get("direction", 0x10014728FF258839LL), v_info->m_distance))))),c_raytracer_vector::t_add(eo_0, eo_1))))));
    (v_info->m_normal = ((Object)((assignCallTemp(eo_0, LINE(42,c_raytracer_vector::t_subtract(v_info->m_position, o_get("position", 0x461E5B8BBDC024D0LL)))),eo_0.toObject().getTyped<c_raytracer_vector>()->t_normalize()))));
    (v_info->m_color = LINE(44,o_get("material", 0x43CC34CAFFD815F7LL).o_invoke_few_args("getColor", 0x7D6618C8952BA624LL, 2, 0LL, 0LL)));
  }
  else {
    (v_info->m_isHit = false);
  }
  return ((Object)(v_info));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php line 51 */
String c_raytracer_shape_sphere::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_Sphere, RayTracer_Shape_Sphere::__toString);
  return LINE(53,concat5("Sphere [position=", toString(o_get("position", 0x461E5B8BBDC024D0LL)), ", radius=", toString(o_get("radius", 0x7FC3CA32D2D213D5LL)), "]"));
} /* function */
Object co_raytracer_shape_sphere(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_shape_sphere(NEW(c_raytracer_shape_sphere)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Sphere_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Sphere_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
