
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_raytrace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_raytrace.php line 10 */
void f_renderscene() {
  FUNCTION_INJECTION(renderScene);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  p_raytracer_scene v_scene;
  p_raytracer_shape_sphere v_sphere;
  p_raytracer_shape_sphere v_sphere1;
  p_raytracer_vector v_v;
  p_raytracer_shape_plane v_plane;
  p_raytracer_light v_light;
  p_raytracer_light v_light1;
  int64 v_imageWidth = 0;
  int64 v_imageHeight = 0;
  Variant v_pixelSize;
  bool v_renderDiffuse = false;
  bool v_renderShadows = false;
  bool v_renderHighlights = false;
  bool v_renderReflections = false;
  int64 v_rayDepth = 0;
  p_raytracer_engine v_raytracer;
  Variant v_im;

  ((Object)((v_scene = ((Object)(LINE(11,p_raytracer_scene(p_raytracer_scene(NEWOBJ(c_raytracer_scene)())->create())))))));
  (v_scene->m_camera = ((Object)(LINE(17,(assignCallTemp(eo_0, ((Object)(LINE(14,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0LL, 0LL, -15LL)))))),assignCallTemp(eo_1, ((Object)(LINE(15,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(-0.20000000000000001, 0LL, 5LL)))))),assignCallTemp(eo_2, ((Object)(LINE(16,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0LL, 1LL, 0LL)))))),p_raytracer_camera(p_raytracer_camera(NEWOBJ(c_raytracer_camera)())->create(eo_0, eo_1, eo_2)))))));
  (v_scene->m_background = ((Object)(LINE(22,(assignCallTemp(eo_0, ((Object)(LINE(20,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.5, 0.5, 0.5)))))),p_raytracer_background(p_raytracer_background(NEWOBJ(c_raytracer_background)())->create(eo_0, 0.40000000000000002)))))));
  ((Object)((v_sphere = ((Object)(LINE(34,(assignCallTemp(eo_0, ((Object)(LINE(25,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(-1.5, 1.5, 2LL)))))),assignCallTemp(eo_2, ((Object)(LINE(33,(assignCallTemp(eo_3, ((Object)(LINE(28,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0.5, 0.5)))))),p_raytracer_material_solid(p_raytracer_material_solid(NEWOBJ(c_raytracer_material_solid)())->create(eo_3, 0.29999999999999999, 0.0, 0.0, 2.0))))))),p_raytracer_shape_sphere(p_raytracer_shape_sphere(NEWOBJ(c_raytracer_shape_sphere)())->create(eo_0, 1.5, eo_2)))))))));
  ((Object)((v_sphere1 = ((Object)(LINE(46,(assignCallTemp(eo_0, ((Object)(LINE(37,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(1LL, 0.25, 1LL)))))),assignCallTemp(eo_2, ((Object)(LINE(45,(assignCallTemp(eo_3, ((Object)(LINE(40,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.90000000000000002, 0.90000000000000002, 0.90000000000000002)))))),p_raytracer_material_solid(p_raytracer_material_solid(NEWOBJ(c_raytracer_material_solid)())->create(eo_3, 0.10000000000000001, 0.0, 0.0, 1.5))))))),p_raytracer_shape_sphere(p_raytracer_shape_sphere(NEWOBJ(c_raytracer_shape_sphere)())->create(eo_0, 0.5, eo_2)))))))));
  ((Object)((v_v = ((Object)(LINE(48,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0.10000000000000001, 0.90000000000000002, -0.5))))))));
  ((Object)((v_plane = ((Object)(LINE(60,(assignCallTemp(eo_0, ((Object)(LINE(50,v_v->t_normalize())))),assignCallTemp(eo_2, ((Object)(LINE(59,(assignCallTemp(eo_3, ((Object)(LINE(53,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(1.0, 1.0, 1.0)))))),assignCallTemp(eo_4, ((Object)(LINE(54,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.0, 0.0, 0.0)))))),p_raytracer_material_chessboard(p_raytracer_material_chessboard(NEWOBJ(c_raytracer_material_chessboard)())->create(eo_3, eo_4, 0.20000000000000001, 0.0, 1.0, 0.69999999999999996))))))),p_raytracer_shape_plane(p_raytracer_shape_plane(NEWOBJ(c_raytracer_shape_plane)())->create(eo_0, 1.2, eo_2)))))))));
  v_scene->m_shapes.append((((Object)(v_plane))));
  v_scene->m_shapes.append((((Object)(v_sphere))));
  v_scene->m_shapes.append((((Object)(v_sphere1))));
  ((Object)((v_light = ((Object)(LINE(69,(assignCallTemp(eo_0, ((Object)(LINE(67,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(5LL, 10LL, -1LL)))))),assignCallTemp(eo_1, ((Object)(LINE(68,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.80000000000000004, 0.80000000000000004, 0.80000000000000004)))))),p_raytracer_light(p_raytracer_light(NEWOBJ(c_raytracer_light)())->create(eo_0, eo_1)))))))));
  ((Object)((v_light1 = ((Object)(LINE(75,(assignCallTemp(eo_0, ((Object)(LINE(72,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(-3LL, 5LL, -15LL)))))),assignCallTemp(eo_1, ((Object)(LINE(73,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.80000000000000004, 0.80000000000000004, 0.80000000000000004)))))),p_raytracer_light(p_raytracer_light(NEWOBJ(c_raytracer_light)())->create(eo_0, eo_1, 100LL)))))))));
  v_scene->m_lights.append((((Object)(v_light))));
  v_scene->m_lights.append((((Object)(v_light1))));
  (v_imageWidth = 100LL);
  (v_imageHeight = 100LL);
  (v_pixelSize = LINE(82,x_split(",", "5,5")));
  (v_renderDiffuse = true);
  (v_renderShadows = true);
  (v_renderHighlights = true);
  (v_renderReflections = true);
  (v_rayDepth = 2LL);
  ((Object)((v_raytracer = ((Object)(LINE(101,p_raytracer_engine(p_raytracer_engine(NEWOBJ(c_raytracer_engine)())->create(Array(ArrayInit(9).set(0, "canvasWidth", v_imageWidth, 0x7A7A553887A71393LL).set(1, "canvasHeight", v_imageHeight, 0x2E4AA2F9F8F66281LL).set(2, "pixelWidth", v_pixelSize.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0x7BBED918FC15A794LL).set(3, "pixelHeight", v_pixelSize.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 0x65EFBEE0F61CEE96LL).set(4, "renderDiffuse", v_renderDiffuse, 0x1ADB02124D536C52LL).set(5, "renderHighlights", v_renderHighlights, 0x2B868E346BBAA931LL).set(6, "renderShadows", v_renderShadows, 0x5C84DC2B748EC6C5LL).set(7, "renderReflections", v_renderReflections, 0x1F4A803394A32D9CLL).set(8, "rayDepth", v_rayDepth, 0x3BD45DC301CC0535LL).create())))))))));
  setNull(v_im);
  invoke_too_many_args("renderscene", (1), ((LINE(106,v_raytracer->t_renderscene(((Object)(v_scene)), v_im)), null)));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_raytrace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_raytrace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_raytrace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  LINE(2,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php(true, variables));
  LINE(3,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Sphere_php(true, variables));
  LINE(4,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Solid_php(true, variables));
  LINE(5,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Plane_php(true, variables));
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Chessboard_php(true, variables));
  LINE(7,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Light_php(true, variables));
  LINE(8,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Engine_php(true, variables));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 6LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      LINE(126,f_renderscene());
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
