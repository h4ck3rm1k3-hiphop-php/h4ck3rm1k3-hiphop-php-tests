
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Scene_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Scene_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.fw.h>

// Declarations
#include <cls/raytracer_scene.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_raytracer_scene(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Scene_h__
