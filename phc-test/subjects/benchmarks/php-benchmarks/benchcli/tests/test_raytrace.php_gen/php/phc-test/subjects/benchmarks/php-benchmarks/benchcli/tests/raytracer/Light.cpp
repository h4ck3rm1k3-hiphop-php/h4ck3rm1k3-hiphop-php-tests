
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_x = "x";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php line 2 */
Variant c_raytracer_light::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_light::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_light::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("position", m_position.isReferenced() ? ref(m_position) : m_position));
  props.push_back(NEW(ArrayElement)("color", m_color.isReferenced() ? ref(m_color) : m_color));
  props.push_back(NEW(ArrayElement)("intensity", m_intensity.isReferenced() ? ref(m_intensity) : m_intensity));
  c_ObjectData::o_get(props);
}
bool c_raytracer_light::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x461E5B8BBDC024D0LL, position, 8);
      break;
    case 5:
      HASH_EXISTS_STRING(0x4311D5A7FE681DADLL, intensity, 9);
      break;
    case 7:
      HASH_EXISTS_STRING(0x1B20384A65BB9927LL, color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_light::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 5:
      HASH_RETURN_STRING(0x4311D5A7FE681DADLL, m_intensity,
                         intensity, 9);
      break;
    case 7:
      HASH_RETURN_STRING(0x1B20384A65BB9927LL, m_color,
                         color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_light::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x461E5B8BBDC024D0LL, m_position,
                      position, 8);
      break;
    case 5:
      HASH_SET_STRING(0x4311D5A7FE681DADLL, m_intensity,
                      intensity, 9);
      break;
    case 7:
      HASH_SET_STRING(0x1B20384A65BB9927LL, m_color,
                      color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_light::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 5:
      HASH_RETURN_STRING(0x4311D5A7FE681DADLL, m_intensity,
                         intensity, 9);
      break;
    case 7:
      HASH_RETURN_STRING(0x1B20384A65BB9927LL, m_color,
                         color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_light::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_light)
ObjectData *c_raytracer_light::create(p_raytracer_vector v_pos, p_raytracer_color v_color, Variant v_intensity //  = 10.0
) {
  init();
  t___construct(v_pos, v_color, v_intensity);
  return this;
}
ObjectData *c_raytracer_light::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_raytracer_light::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_raytracer_light::cloneImpl() {
  c_raytracer_light *obj = NEW(c_raytracer_light)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_light::cloneSet(c_raytracer_light *clone) {
  clone->m_position = m_position.isReferenced() ? ref(m_position) : m_position;
  clone->m_color = m_color.isReferenced() ? ref(m_color) : m_color;
  clone->m_intensity = m_intensity.isReferenced() ? ref(m_intensity) : m_intensity;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_light::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_light::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_light::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_light$os_get(const char *s) {
  return c_raytracer_light::os_get(s, -1);
}
Variant &cw_raytracer_light$os_lval(const char *s) {
  return c_raytracer_light::os_lval(s, -1);
}
Variant cw_raytracer_light$os_constant(const char *s) {
  return c_raytracer_light::os_constant(s);
}
Variant cw_raytracer_light$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_light::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_light::init() {
  m_position = null;
  m_color = null;
  m_intensity = 10.0;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php line 14 */
void c_raytracer_light::t___construct(p_raytracer_vector v_pos, p_raytracer_color v_color, Variant v_intensity //  = 10.0
) {
  INSTANCE_METHOD_INJECTION(RayTracer_Light, RayTracer_Light::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_position = ((Object)(v_pos)));
  (m_color = ((Object)(v_color)));
  (m_intensity = v_intensity);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php line 21 */
Numeric c_raytracer_light::t_getintensity(CVarRef v_distance) {
  INSTANCE_METHOD_INJECTION(RayTracer_Light, RayTracer_Light::getIntensity);
  Variant v_strength;

  if (not_less(v_distance, m_intensity)) return 0LL;
  return LINE(24,x_pow(divide((m_intensity - v_distance), v_strength), 0.20000000000000001));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php line 27 */
String c_raytracer_light::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Light, RayTracer_Light::__toString);
  return LINE(29,concat3(toString((Variant)(concat(toString((Variant)(concat3("Light [", toString(m_position), k_x)) + (Variant)(",")), toString(m_position.o_get("y", 0x4F56B733A4DFC78ALL)))) + (Variant)(",")), toString(m_position.o_get("z", 0x62A103F6518DE2B3LL)), "]"));
} /* function */
Object co_raytracer_light(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_light(NEW(c_raytracer_light)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Light_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Light_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
