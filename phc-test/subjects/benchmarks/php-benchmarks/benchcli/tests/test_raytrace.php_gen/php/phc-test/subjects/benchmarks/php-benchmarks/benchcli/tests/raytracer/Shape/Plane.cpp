
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php line 3 */
Variant c_raytracer_shape_plane::os_get(const char *s, int64 hash) {
  return c_raytracer_shape_baseshape::os_get(s, hash);
}
Variant &c_raytracer_shape_plane::os_lval(const char *s, int64 hash) {
  return c_raytracer_shape_baseshape::os_lval(s, hash);
}
void c_raytracer_shape_plane::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("d", m_d.isReferenced() ? ref(m_d) : m_d));
  c_raytracer_shape_baseshape::o_get(props);
}
bool c_raytracer_shape_plane::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    default:
      break;
  }
  return c_raytracer_shape_baseshape::o_exists(s, hash);
}
Variant c_raytracer_shape_plane::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    default:
      break;
  }
  return c_raytracer_shape_baseshape::o_get(s, hash);
}
Variant c_raytracer_shape_plane::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    default:
      break;
  }
  return c_raytracer_shape_baseshape::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_shape_plane::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    default:
      break;
  }
  return c_raytracer_shape_baseshape::o_lval(s, hash);
}
Variant c_raytracer_shape_plane::os_constant(const char *s) {
  return c_raytracer_shape_baseshape::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_shape_plane)
ObjectData *c_raytracer_shape_plane::create(p_raytracer_vector v_pos, Variant v_d, p_raytracer_material_chessboard v_material) {
  init();
  t___construct(v_pos, v_d, v_material);
  return this;
}
ObjectData *c_raytracer_shape_plane::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_raytracer_shape_plane::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_raytracer_shape_plane::cloneImpl() {
  c_raytracer_shape_plane *obj = NEW(c_raytracer_shape_plane)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_shape_plane::cloneSet(c_raytracer_shape_plane *clone) {
  clone->m_d = m_d.isReferenced() ? ref(m_d) : m_d;
  c_raytracer_shape_baseshape::cloneSet(clone);
}
Variant c_raytracer_shape_plane::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_raytracer_shape_baseshape::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_shape_plane::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4A3F7CCEE998444ALL, intersect) {
        return (t_intersect(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_raytracer_shape_baseshape::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_shape_plane::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_raytracer_shape_baseshape::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_shape_plane$os_get(const char *s) {
  return c_raytracer_shape_plane::os_get(s, -1);
}
Variant &cw_raytracer_shape_plane$os_lval(const char *s) {
  return c_raytracer_shape_plane::os_lval(s, -1);
}
Variant cw_raytracer_shape_plane$os_constant(const char *s) {
  return c_raytracer_shape_plane::os_constant(s);
}
Variant cw_raytracer_shape_plane$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_shape_plane::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_shape_plane::init() {
  c_raytracer_shape_baseshape::init();
  m_d = 0.0;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php line 6 */
void c_raytracer_shape_plane::t___construct(p_raytracer_vector v_pos, Variant v_d, p_raytracer_material_chessboard v_material) {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_Plane, RayTracer_Shape_Plane::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("position", 0x461E5B8BBDC024D0LL) = ((Object)(v_pos)));
  (m_d = v_d);
  (o_lval("material", 0x43CC34CAFFD815F7LL) = ((Object)(v_material)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php line 13 */
p_raytracer_intersectioninfo c_raytracer_shape_plane::t_intersect(CVarRef v_ray) {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_Plane, RayTracer_Shape_Plane::intersect);
  Variant eo_0;
  Variant eo_1;
  p_raytracer_intersectioninfo v_info;
  Variant v_Vd;
  Numeric v_t = 0;
  Variant v_vU;
  Variant v_vV;
  Variant v_u;
  Variant v_v;

  ((Object)((v_info = ((Object)(LINE(15,p_raytracer_intersectioninfo(p_raytracer_intersectioninfo(NEWOBJ(c_raytracer_intersectioninfo)())->create())))))));
  (v_Vd = LINE(17,o_get("position", 0x461E5B8BBDC024D0LL).o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, toObject(v_ray).o_lval("direction", 0x10014728FF258839LL))));
  if (equal(v_Vd, 0LL)) return ((Object)(v_info));
  (v_t = divide(negate((LINE(20,o_get("position", 0x461E5B8BBDC024D0LL).o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, toObject(v_ray).o_lval("position", 0x461E5B8BBDC024D0LL))) + m_d)), v_Vd));
  if (not_more(v_t, 0LL)) return ((Object)(v_info));
  (v_info->m_shape = ((Object)(this)));
  (v_info->m_isHit = true);
  (v_info->m_position = ((Object)(LINE(31,(assignCallTemp(eo_0, toObject(v_ray).o_get("position", 0x461E5B8BBDC024D0LL)),assignCallTemp(eo_1, ((Object)(LINE(30,c_raytracer_vector::t_multiplyscalar(toObject(v_ray).o_get("direction", 0x10014728FF258839LL), v_t))))),c_raytracer_vector::t_add(eo_0, eo_1))))));
  (v_info->m_normal = o_get("position", 0x461E5B8BBDC024D0LL));
  (v_info->m_distance = v_t);
  if (toBoolean(o_get("material", 0x43CC34CAFFD815F7LL).o_get("hasTexture", 0x6C2EAE64D6A215AALL))) {
    (v_vU = ((Object)(LINE(36,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(o_get("position", 0x461E5B8BBDC024D0LL).o_get("y", 0x4F56B733A4DFC78ALL), o_get("position", 0x461E5B8BBDC024D0LL).o_get("z", 0x62A103F6518DE2B3LL), negate(o_get("position", 0x461E5B8BBDC024D0LL).o_get("x", 0x04BFC205E59FA416LL))))))));
    (v_vV = LINE(37,v_vU.o_invoke_few_args("cross", 0x4968E44FD6B195A6LL, 1, o_lval("position", 0x461E5B8BBDC024D0LL))));
    (v_u = LINE(38,v_info->m_position.o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, v_vU)));
    (v_v = LINE(39,v_info->m_position.o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, v_vV)));
    (v_info->m_color = LINE(40,o_get("material", 0x43CC34CAFFD815F7LL).o_invoke_few_args("getColor", 0x7D6618C8952BA624LL, 2, v_u, v_v)));
  }
  else {
    (v_info->m_color = LINE(42,o_get("material", 0x43CC34CAFFD815F7LL).o_invoke_few_args("getColor", 0x7D6618C8952BA624LL, 2, 0LL, 0LL)));
  }
  return ((Object)(v_info));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php line 47 */
String c_raytracer_shape_plane::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_Plane, RayTracer_Shape_Plane::__toString);
  return LINE(49,concat5("Plane [", toString(o_get("position", 0x461E5B8BBDC024D0LL)), ", d=", toString(m_d), "]"));
} /* function */
Object co_raytracer_shape_plane(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_shape_plane(NEW(c_raytracer_shape_plane)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Plane_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Plane_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,require("raytracer/Shape/BaseShape.php", true, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
