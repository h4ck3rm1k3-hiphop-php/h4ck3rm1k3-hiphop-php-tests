
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Scene_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Scene_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(raytracer_scene)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.fw.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.fw.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.fw.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.fw.h>

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Scene_fw_h__
