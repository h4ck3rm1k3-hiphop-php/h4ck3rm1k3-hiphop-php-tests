
#ifndef __GENERATED_cls_raytracer_ray_h__
#define __GENERATED_cls_raytracer_ray_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php line 2 */
class c_raytracer_ray : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_ray)
  END_CLASS_MAP(raytracer_ray)
  DECLARE_CLASS(raytracer_ray, RayTracer_Ray, ObjectData)
  void init();
  public: Variant m_position;
  public: Variant m_direction;
  public: void t___construct(Variant v_pos, Variant v_dir);
  public: ObjectData *create(Variant v_pos, Variant v_dir);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_ray_h__
