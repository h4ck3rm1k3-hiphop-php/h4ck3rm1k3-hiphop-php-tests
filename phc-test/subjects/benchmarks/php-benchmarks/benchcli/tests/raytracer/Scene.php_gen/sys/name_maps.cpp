
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "raytracer_background", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.php",
  "raytracer_camera", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php",
  "raytracer_color", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php",
  "raytracer_ray", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php",
  "raytracer_scene", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.php",
  "raytracer_vector", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
