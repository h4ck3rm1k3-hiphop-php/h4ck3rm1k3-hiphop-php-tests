
#ifndef __GENERATED_cls_raytracer_scene_h__
#define __GENERATED_cls_raytracer_scene_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.php line 7 */
class c_raytracer_scene : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_scene)
  END_CLASS_MAP(raytracer_scene)
  DECLARE_CLASS(raytracer_scene, RayTracer_Scene, ObjectData)
  void init();
  public: Variant m_camera;
  public: Variant m_shapes;
  public: Variant m_lights;
  public: Variant m_background;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_scene_h__
