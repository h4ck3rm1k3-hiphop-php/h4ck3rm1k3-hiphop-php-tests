
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 2 */
Variant c_raytracer_color::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_color::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_color::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("red", m_red.isReferenced() ? ref(m_red) : m_red));
  props.push_back(NEW(ArrayElement)("green", m_green.isReferenced() ? ref(m_green) : m_green));
  props.push_back(NEW(ArrayElement)("blue", m_blue.isReferenced() ? ref(m_blue) : m_blue));
  c_ObjectData::o_get(props);
}
bool c_raytracer_color::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x78415B637DF28CD9LL, green, 5);
      break;
    case 4:
      HASH_EXISTS_STRING(0x2D82AF0F4EA2D20CLL, red, 3);
      break;
    case 7:
      HASH_EXISTS_STRING(0x565BF9A195324467LL, blue, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_color::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x78415B637DF28CD9LL, m_green,
                         green, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x2D82AF0F4EA2D20CLL, m_red,
                         red, 3);
      break;
    case 7:
      HASH_RETURN_STRING(0x565BF9A195324467LL, m_blue,
                         blue, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_color::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x78415B637DF28CD9LL, m_green,
                      green, 5);
      break;
    case 4:
      HASH_SET_STRING(0x2D82AF0F4EA2D20CLL, m_red,
                      red, 3);
      break;
    case 7:
      HASH_SET_STRING(0x565BF9A195324467LL, m_blue,
                      blue, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_color::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x78415B637DF28CD9LL, m_green,
                         green, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x2D82AF0F4EA2D20CLL, m_red,
                         red, 3);
      break;
    case 7:
      HASH_RETURN_STRING(0x565BF9A195324467LL, m_blue,
                         blue, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_color::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_color)
ObjectData *c_raytracer_color::create(Variant v_r //  = 0.0
, Variant v_g //  = 0.0
, Variant v_b //  = 0.0
) {
  init();
  t___construct(v_r, v_g, v_b);
  return this;
}
ObjectData *c_raytracer_color::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_raytracer_color::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_raytracer_color::cloneImpl() {
  c_raytracer_color *obj = NEW(c_raytracer_color)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_color::cloneSet(c_raytracer_color *clone) {
  clone->m_red = m_red.isReferenced() ? ref(m_red) : m_red;
  clone->m_green = m_green.isReferenced() ? ref(m_green) : m_green;
  clone->m_blue = m_blue.isReferenced() ? ref(m_blue) : m_blue;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_color::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_color::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_color::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_color$os_get(const char *s) {
  return c_raytracer_color::os_get(s, -1);
}
Variant &cw_raytracer_color$os_lval(const char *s) {
  return c_raytracer_color::os_lval(s, -1);
}
Variant cw_raytracer_color$os_constant(const char *s) {
  return c_raytracer_color::os_constant(s);
}
Variant cw_raytracer_color$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_color::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_color::init() {
  m_red = 0.0;
  m_green = 0.0;
  m_blue = 0.0;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 7 */
void c_raytracer_color::t___construct(Variant v_r //  = 0.0
, Variant v_g //  = 0.0
, Variant v_b //  = 0.0
) {
  INSTANCE_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_red = v_r);
  (m_green = v_g);
  (m_blue = v_b);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 14 */
p_raytracer_color c_raytracer_color::ti_add(const char* cls, p_raytracer_color v_c1, p_raytracer_color v_c2) {
  STATIC_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::add);
  p_raytracer_color v_result;

  ((Object)((v_result = ((Object)(LINE(16,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0LL, 0LL))))))));
  (v_result->m_red = v_c1->m_red + v_c2->m_red);
  (v_result->m_green = v_c1->m_green + v_c2->m_green);
  (v_result->m_blue = v_c1->m_blue + v_c2->m_blue);
  return ((Object)(v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 32 */
p_raytracer_color c_raytracer_color::ti_addscalar(const char* cls, p_raytracer_color v_c1, CVarRef v_s) {
  STATIC_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::addScalar);
  p_raytracer_color v_result;

  ((Object)((v_result = ((Object)(LINE(34,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0LL, 0LL))))))));
  (v_result->m_red = v_c1->m_red + v_s);
  (v_result->m_green = v_c1->m_green + v_s);
  (v_result->m_blue = v_c1->m_blue + v_s);
  LINE(40,v_result->t_limit());
  return ((Object)(v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 45 */
p_raytracer_color c_raytracer_color::ti_subtract(const char* cls, p_raytracer_color v_c1, p_raytracer_color v_c2) {
  STATIC_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::subtract);
  p_raytracer_color v_result;

  ((Object)((v_result = ((Object)(LINE(47,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0LL, 0LL))))))));
  (v_result->m_red = v_c1->m_red - v_c2->m_red);
  (v_result->m_green = v_c1->m_green - v_c2->m_green);
  (v_result->m_blue = v_c1->m_blue - v_c2->m_blue);
  return ((Object)(v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 56 */
p_raytracer_color c_raytracer_color::ti_multiply(const char* cls, p_raytracer_color v_c1, p_raytracer_color v_c2) {
  STATIC_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::multiply);
  p_raytracer_color v_result;

  ((Object)((v_result = ((Object)(LINE(58,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0LL, 0LL))))))));
  (v_result->m_red = v_c1->m_red * v_c2->m_red);
  (v_result->m_green = v_c1->m_green * v_c2->m_green);
  (v_result->m_blue = v_c1->m_blue * v_c2->m_blue);
  return ((Object)(v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 74 */
p_raytracer_color c_raytracer_color::ti_multiplyscalar(const char* cls, p_raytracer_color v_c1, CVarRef v_f) {
  STATIC_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::multiplyScalar);
  p_raytracer_color v_result;

  ((Object)((v_result = ((Object)(LINE(76,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0LL, 0LL))))))));
  (v_result->m_red = v_c1->m_red * v_f);
  (v_result->m_green = v_c1->m_green * v_f);
  (v_result->m_blue = v_c1->m_blue * v_f);
  return ((Object)(v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 91 */
p_raytracer_color c_raytracer_color::ti_dividefactor(const char* cls, p_raytracer_color v_c1, CVarRef v_f) {
  STATIC_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::divideFactor);
  p_raytracer_color v_result;

  ((Object)((v_result = ((Object)(LINE(93,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0LL, 0LL))))))));
  (v_result->m_red = divide(v_c1->m_red, v_f));
  (v_result->m_green = divide(v_c1->m_green, v_f));
  (v_result->m_blue = divide(v_c1->m_blue, v_f));
  return ((Object)(v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 102 */
void c_raytracer_color::t_limit() {
  INSTANCE_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::limit);
  (m_red = (more(m_red, 0.0)) ? ((Variant)(((more(m_red, 1.0)) ? ((Variant)(1.0)) : ((Variant)(m_red))))) : ((Variant)(0.0)));
  (m_green = (more(m_green, 0.0)) ? ((Variant)(((more(m_green, 1.0)) ? ((Variant)(1.0)) : ((Variant)(m_green))))) : ((Variant)(0.0)));
  (m_blue = (more(m_blue, 0.0)) ? ((Variant)(((more(m_blue, 1.0)) ? ((Variant)(1.0)) : ((Variant)(m_blue))))) : ((Variant)(0.0)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 109 */
PlusOperand c_raytracer_color::t_distance(p_raytracer_color v_color) {
  INSTANCE_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::distance);
  PlusOperand v_d = 0;

  (v_d = plus_rev(LINE(111,x_abs(m_blue - v_color->m_blue)), plus_rev(x_abs(m_green - v_color->m_green), x_abs(m_red - v_color->m_red))));
  return v_d;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 123 */
p_raytracer_color c_raytracer_color::ti_blend(const char* cls, p_raytracer_color v_c1, p_raytracer_color v_c2, CVarRef v_w) {
  STATIC_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::blend);
  Variant eo_0;
  Variant eo_1;
  p_raytracer_color v_result;

  ((Object)((v_result = ((Object)(LINE(128,(assignCallTemp(eo_0, ((Object)(LINE(126,c_raytracer_color::t_multiplyscalar(((Object)(v_c1)), 1LL - v_w))))),assignCallTemp(eo_1, ((Object)(LINE(127,c_raytracer_color::t_multiplyscalar(((Object)(v_c2)), v_w))))),c_raytracer_color::t_add(eo_0, eo_1))))))));
  return ((Object)(v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 132 */
int64 c_raytracer_color::t_brightness() {
  INSTANCE_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::brightness);
  double v_r = 0.0;
  double v_g = 0.0;
  double v_b = 0.0;

  (v_r = LINE(134,x_floor(toDouble(m_red * 255LL))));
  (v_g = LINE(135,x_floor(toDouble(m_green * 255LL))));
  (v_b = LINE(136,x_floor(toDouble(m_blue * 255LL))));
  return toInt64((toInt64(v_r * 77LL + v_g * 150LL + v_b * 29LL))) >> 8LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php line 140 */
String c_raytracer_color::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Color, RayTracer_Color::__toString);
  double v_r = 0.0;
  double v_g = 0.0;
  double v_b = 0.0;

  (v_r = LINE(142,x_floor(toDouble(m_red * 255LL))));
  (v_g = LINE(143,x_floor(toDouble(m_green * 255LL))));
  (v_b = LINE(144,x_floor(toDouble(m_blue * 255LL))));
  return toString((Variant)("rgb(") + v_r + (Variant)(",") + v_g + (Variant)(",") + v_b + (Variant)(")"));
} /* function */
Object co_raytracer_color(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_color(NEW(c_raytracer_color)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Color_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Color_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
