
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php line 2 */
Variant c_raytracer_camera::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_camera::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_camera::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("position", m_position.isReferenced() ? ref(m_position) : m_position));
  props.push_back(NEW(ArrayElement)("lookAt", m_lookAt.isReferenced() ? ref(m_lookAt) : m_lookAt));
  props.push_back(NEW(ArrayElement)("equator", m_equator.isReferenced() ? ref(m_equator) : m_equator));
  props.push_back(NEW(ArrayElement)("up", m_up.isReferenced() ? ref(m_up) : m_up));
  props.push_back(NEW(ArrayElement)("screen", m_screen.isReferenced() ? ref(m_screen) : m_screen));
  c_ObjectData::o_get(props);
}
bool c_raytracer_camera::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x461E5B8BBDC024D0LL, position, 8);
      break;
    case 3:
      HASH_EXISTS_STRING(0x7AA2774AA6686F03LL, equator, 7);
      HASH_EXISTS_STRING(0x4BB538DAE72A3773LL, screen, 6);
      break;
    case 9:
      HASH_EXISTS_STRING(0x257450CBA7518AB9LL, lookAt, 6);
      break;
    case 15:
      HASH_EXISTS_STRING(0x43DC7E06773B3EEFLL, up, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_camera::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x7AA2774AA6686F03LL, m_equator,
                         equator, 7);
      HASH_RETURN_STRING(0x4BB538DAE72A3773LL, m_screen,
                         screen, 6);
      break;
    case 9:
      HASH_RETURN_STRING(0x257450CBA7518AB9LL, m_lookAt,
                         lookAt, 6);
      break;
    case 15:
      HASH_RETURN_STRING(0x43DC7E06773B3EEFLL, m_up,
                         up, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_camera::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x461E5B8BBDC024D0LL, m_position,
                      position, 8);
      break;
    case 3:
      HASH_SET_STRING(0x7AA2774AA6686F03LL, m_equator,
                      equator, 7);
      HASH_SET_STRING(0x4BB538DAE72A3773LL, m_screen,
                      screen, 6);
      break;
    case 9:
      HASH_SET_STRING(0x257450CBA7518AB9LL, m_lookAt,
                      lookAt, 6);
      break;
    case 15:
      HASH_SET_STRING(0x43DC7E06773B3EEFLL, m_up,
                      up, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_camera::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x7AA2774AA6686F03LL, m_equator,
                         equator, 7);
      HASH_RETURN_STRING(0x4BB538DAE72A3773LL, m_screen,
                         screen, 6);
      break;
    case 9:
      HASH_RETURN_STRING(0x257450CBA7518AB9LL, m_lookAt,
                         lookAt, 6);
      break;
    case 15:
      HASH_RETURN_STRING(0x43DC7E06773B3EEFLL, m_up,
                         up, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_camera::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_camera)
ObjectData *c_raytracer_camera::create(p_raytracer_vector v_pos, p_raytracer_vector v_lookAt, p_raytracer_vector v_up) {
  init();
  t___construct(v_pos, v_lookAt, v_up);
  return this;
}
ObjectData *c_raytracer_camera::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_raytracer_camera::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_raytracer_camera::cloneImpl() {
  c_raytracer_camera *obj = NEW(c_raytracer_camera)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_camera::cloneSet(c_raytracer_camera *clone) {
  clone->m_position = m_position.isReferenced() ? ref(m_position) : m_position;
  clone->m_lookAt = m_lookAt.isReferenced() ? ref(m_lookAt) : m_lookAt;
  clone->m_equator = m_equator.isReferenced() ? ref(m_equator) : m_equator;
  clone->m_up = m_up.isReferenced() ? ref(m_up) : m_up;
  clone->m_screen = m_screen.isReferenced() ? ref(m_screen) : m_screen;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_camera::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_camera::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_camera::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_camera$os_get(const char *s) {
  return c_raytracer_camera::os_get(s, -1);
}
Variant &cw_raytracer_camera$os_lval(const char *s) {
  return c_raytracer_camera::os_lval(s, -1);
}
Variant cw_raytracer_camera$os_constant(const char *s) {
  return c_raytracer_camera::os_constant(s);
}
Variant cw_raytracer_camera$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_camera::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_camera::init() {
  m_position = null;
  m_lookAt = null;
  m_equator = null;
  m_up = null;
  m_screen = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php line 9 */
void c_raytracer_camera::t___construct(p_raytracer_vector v_pos, p_raytracer_vector v_lookAt, p_raytracer_vector v_up) {
  INSTANCE_METHOD_INJECTION(RayTracer_Camera, RayTracer_Camera::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  (m_up = ((Object)(LINE(11,v_up->t_normalize()))));
  (m_position = ((Object)(v_pos)));
  (m_lookAt = ((Object)(v_lookAt)));
  (m_equator = ((Object)((assignCallTemp(eo_0, LINE(14,v_lookAt->t_normalize())),eo_0.toObject().getTyped<c_raytracer_vector>()->t_cross(m_up)))));
  (m_screen = ((Object)(LINE(15,c_raytracer_vector::t_add(m_position, m_lookAt)))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php line 20 */
Variant c_raytracer_camera::t_getray(Variant v_vx, Variant v_vy) {
  INSTANCE_METHOD_INJECTION(RayTracer_Camera, RayTracer_Camera::getRay);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_pos;
  Variant v_dir;
  Variant v_ray;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_vx; Variant &v_vy; Variant &v_pos; Variant &v_dir; Variant &v_ray;
    VariableTable(Variant &r_vx, Variant &r_vy, Variant &r_pos, Variant &r_dir, Variant &r_ray) : v_vx(r_vx), v_vy(r_vy), v_pos(r_pos), v_dir(r_dir), v_ray(r_ray) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 15) {
        case 7:
          HASH_RETURN(0x389AC79A7A842A27LL, v_vx,
                      vx);
          HASH_RETURN(0x56670E8861466997LL, v_ray,
                      ray);
          break;
        case 11:
          HASH_RETURN(0x51B575986A4B460BLL, v_vy,
                      vy);
          break;
        case 14:
          HASH_RETURN(0x277B6D36F75741AELL, v_pos,
                      pos);
          break;
        case 15:
          HASH_RETURN(0x522821F5063592CFLL, v_dir,
                      dir);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_vx, v_vy, v_pos, v_dir, v_ray);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_pos = ((Object)(LINE(28,(assignCallTemp(eo_0, m_screen),assignCallTemp(eo_1, ((Object)(LINE(27,(assignCallTemp(eo_2, ((Object)(LINE(25,c_raytracer_vector::t_multiplyscalar(m_equator, v_vx))))),assignCallTemp(eo_3, ((Object)(LINE(26,c_raytracer_vector::t_multiplyscalar(m_up, v_vy))))),c_raytracer_vector::t_subtract(eo_2, eo_3)))))),c_raytracer_vector::t_subtract(eo_0, eo_1))))));
  (v_pos.o_lval("y", 0x4F56B733A4DFC78ALL) = v_pos.o_get("y", 0x4F56B733A4DFC78ALL) * -1LL);
  (v_dir = ((Object)(LINE(33,c_raytracer_vector::t_subtract(v_pos, m_position)))));
  LINE(34,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php(true, variables));
  (v_ray = ((Object)(LINE(35,(assignCallTemp(eo_0, v_pos),assignCallTemp(eo_1, v_dir.o_invoke_few_args("normalize", 0x11A2D4B742CB6C5ALL, 0)),p_raytracer_ray(p_raytracer_ray(NEWOBJ(c_raytracer_ray)())->create(eo_0, eo_1)))))));
  return v_ray;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php line 40 */
String c_raytracer_camera::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Camera, RayTracer_Camera::__toString);
  return "Ray []";
} /* function */
Object co_raytracer_camera(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_camera(NEW(c_raytracer_camera)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
