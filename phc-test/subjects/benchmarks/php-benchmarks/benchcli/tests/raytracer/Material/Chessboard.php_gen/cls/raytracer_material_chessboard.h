
#ifndef __GENERATED_cls_raytracer_material_chessboard_h__
#define __GENERATED_cls_raytracer_material_chessboard_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php line 2 */
class c_raytracer_material_chessboard : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_material_chessboard)
  END_CLASS_MAP(raytracer_material_chessboard)
  DECLARE_CLASS(raytracer_material_chessboard, RayTracer_Material_Chessboard, raytracer_material_basematerial)
  void init();
  public: Variant m_colorEven;
  public: Variant m_colorOdd;
  public: Variant m_density;
  public: void t___construct(Variant v_colorEven, Variant v_colorOdd, Variant v_reflection, Variant v_transparency, Variant v_gloss, Variant v_density);
  public: ObjectData *create(Variant v_colorEven, Variant v_colorOdd, Variant v_reflection, Variant v_transparency, Variant v_gloss, Variant v_density);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_getcolor(CVarRef v_u, CVarRef v_v);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_material_chessboard_h__
