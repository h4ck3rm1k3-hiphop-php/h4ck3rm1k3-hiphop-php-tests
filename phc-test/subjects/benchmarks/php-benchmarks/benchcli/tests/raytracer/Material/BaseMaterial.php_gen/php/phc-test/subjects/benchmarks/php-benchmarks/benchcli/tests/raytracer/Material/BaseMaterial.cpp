
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.php line 2 */
Variant c_raytracer_material_basematerial::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_material_basematerial::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_material_basematerial::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("gloss", m_gloss));
  props.push_back(NEW(ArrayElement)("transparency", m_transparency));
  props.push_back(NEW(ArrayElement)("reflection", m_reflection));
  props.push_back(NEW(ArrayElement)("refraction", m_refraction));
  props.push_back(NEW(ArrayElement)("hasTexture", m_hasTexture));
  c_ObjectData::o_get(props);
}
bool c_raytracer_material_basematerial::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_EXISTS_STRING(0x0309BEA034533891LL, transparency, 12);
      break;
    case 10:
      HASH_EXISTS_STRING(0x6C751D32BD23BF1ALL, gloss, 5);
      HASH_EXISTS_STRING(0x6C2EAE64D6A215AALL, hasTexture, 10);
      break;
    case 12:
      HASH_EXISTS_STRING(0x7BC1B6C911DA947CLL, refraction, 10);
      break;
    case 15:
      HASH_EXISTS_STRING(0x6F1A43B7D5277CDFLL, reflection, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_material_basematerial::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x0309BEA034533891LL, m_transparency,
                         transparency, 12);
      break;
    case 10:
      HASH_RETURN_STRING(0x6C751D32BD23BF1ALL, m_gloss,
                         gloss, 5);
      HASH_RETURN_STRING(0x6C2EAE64D6A215AALL, m_hasTexture,
                         hasTexture, 10);
      break;
    case 12:
      HASH_RETURN_STRING(0x7BC1B6C911DA947CLL, m_refraction,
                         refraction, 10);
      break;
    case 15:
      HASH_RETURN_STRING(0x6F1A43B7D5277CDFLL, m_reflection,
                         reflection, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_material_basematerial::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_SET_STRING(0x0309BEA034533891LL, m_transparency,
                      transparency, 12);
      break;
    case 10:
      HASH_SET_STRING(0x6C751D32BD23BF1ALL, m_gloss,
                      gloss, 5);
      HASH_SET_STRING(0x6C2EAE64D6A215AALL, m_hasTexture,
                      hasTexture, 10);
      break;
    case 12:
      HASH_SET_STRING(0x7BC1B6C911DA947CLL, m_refraction,
                      refraction, 10);
      break;
    case 15:
      HASH_SET_STRING(0x6F1A43B7D5277CDFLL, m_reflection,
                      reflection, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_material_basematerial::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_material_basematerial::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_material_basematerial)
ObjectData *c_raytracer_material_basematerial::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_raytracer_material_basematerial::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_raytracer_material_basematerial::cloneImpl() {
  c_raytracer_material_basematerial *obj = NEW(c_raytracer_material_basematerial)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_material_basematerial::cloneSet(c_raytracer_material_basematerial *clone) {
  clone->m_gloss = m_gloss;
  clone->m_transparency = m_transparency;
  clone->m_reflection = m_reflection;
  clone->m_refraction = m_refraction;
  clone->m_hasTexture = m_hasTexture;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_material_basematerial::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_material_basematerial::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_material_basematerial::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_material_basematerial$os_get(const char *s) {
  return c_raytracer_material_basematerial::os_get(s, -1);
}
Variant &cw_raytracer_material_basematerial$os_lval(const char *s) {
  return c_raytracer_material_basematerial::os_lval(s, -1);
}
Variant cw_raytracer_material_basematerial$os_constant(const char *s) {
  return c_raytracer_material_basematerial::os_constant(s);
}
Variant cw_raytracer_material_basematerial$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_material_basematerial::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_material_basematerial::init() {
  m_gloss = 2.0;
  m_transparency = 0.0;
  m_reflection = 0.0;
  m_refraction = 0.5;
  m_hasTexture = false;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.php line 10 */
void c_raytracer_material_basematerial::t___construct() {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_BaseMaterial, RayTracer_Material_BaseMaterial::__construct);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.php line 21 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.php line 23 */
double c_raytracer_material_basematerial::t_wrapup(double v_t) {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_BaseMaterial, RayTracer_Material_BaseMaterial::wrapUp);
  (v_t = LINE(25,x_fmod(v_t, 2.0)));
  if (less(v_t, -1LL)) v_t += 2.0;
  if (not_less(v_t, 1LL)) v_t -= 2.0;
  return v_t;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.php line 31 */
String c_raytracer_material_basematerial::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_BaseMaterial, RayTracer_Material_BaseMaterial::__toString);
  return concat("Material [gloss=", LINE(33,concat6(toString(m_gloss), ", transparency=", toString(m_transparency), ", hasTexture=", toString(m_hasTexture), "]")));
} /* function */
Object co_raytracer_material_basematerial(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_material_basematerial(NEW(c_raytracer_material_basematerial)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$BaseMaterial_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$BaseMaterial_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
