
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php line 2 */
Variant c_raytracer_material_chessboard::os_get(const char *s, int64 hash) {
  return c_raytracer_material_basematerial::os_get(s, hash);
}
Variant &c_raytracer_material_chessboard::os_lval(const char *s, int64 hash) {
  return c_raytracer_material_basematerial::os_lval(s, hash);
}
void c_raytracer_material_chessboard::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("colorEven", m_colorEven.isReferenced() ? ref(m_colorEven) : m_colorEven));
  props.push_back(NEW(ArrayElement)("colorOdd", m_colorOdd.isReferenced() ? ref(m_colorOdd) : m_colorOdd));
  props.push_back(NEW(ArrayElement)("density", m_density.isReferenced() ? ref(m_density) : m_density));
  c_raytracer_material_basematerial::o_get(props);
}
bool c_raytracer_material_chessboard::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x67CC17C722755C20LL, colorOdd, 8);
      break;
    case 4:
      HASH_EXISTS_STRING(0x740B8F8748C4B55CLL, colorEven, 9);
      HASH_EXISTS_STRING(0x571AAE84CDFEB424LL, density, 7);
      break;
    default:
      break;
  }
  return c_raytracer_material_basematerial::o_exists(s, hash);
}
Variant c_raytracer_material_chessboard::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x67CC17C722755C20LL, m_colorOdd,
                         colorOdd, 8);
      break;
    case 4:
      HASH_RETURN_STRING(0x740B8F8748C4B55CLL, m_colorEven,
                         colorEven, 9);
      HASH_RETURN_STRING(0x571AAE84CDFEB424LL, m_density,
                         density, 7);
      break;
    default:
      break;
  }
  return c_raytracer_material_basematerial::o_get(s, hash);
}
Variant c_raytracer_material_chessboard::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x67CC17C722755C20LL, m_colorOdd,
                      colorOdd, 8);
      break;
    case 4:
      HASH_SET_STRING(0x740B8F8748C4B55CLL, m_colorEven,
                      colorEven, 9);
      HASH_SET_STRING(0x571AAE84CDFEB424LL, m_density,
                      density, 7);
      break;
    default:
      break;
  }
  return c_raytracer_material_basematerial::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_material_chessboard::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x67CC17C722755C20LL, m_colorOdd,
                         colorOdd, 8);
      break;
    case 4:
      HASH_RETURN_STRING(0x740B8F8748C4B55CLL, m_colorEven,
                         colorEven, 9);
      HASH_RETURN_STRING(0x571AAE84CDFEB424LL, m_density,
                         density, 7);
      break;
    default:
      break;
  }
  return c_raytracer_material_basematerial::o_lval(s, hash);
}
Variant c_raytracer_material_chessboard::os_constant(const char *s) {
  return c_raytracer_material_basematerial::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_material_chessboard)
ObjectData *c_raytracer_material_chessboard::create(Variant v_colorEven, Variant v_colorOdd, Variant v_reflection, Variant v_transparency, Variant v_gloss, Variant v_density) {
  init();
  t___construct(v_colorEven, v_colorOdd, v_reflection, v_transparency, v_gloss, v_density);
  return this;
}
ObjectData *c_raytracer_material_chessboard::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
  } else return this;
}
ObjectData *c_raytracer_material_chessboard::cloneImpl() {
  c_raytracer_material_chessboard *obj = NEW(c_raytracer_material_chessboard)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_material_chessboard::cloneSet(c_raytracer_material_chessboard *clone) {
  clone->m_colorEven = m_colorEven.isReferenced() ? ref(m_colorEven) : m_colorEven;
  clone->m_colorOdd = m_colorOdd.isReferenced() ? ref(m_colorOdd) : m_colorOdd;
  clone->m_density = m_density.isReferenced() ? ref(m_density) : m_density;
  c_raytracer_material_basematerial::cloneSet(clone);
}
Variant c_raytracer_material_chessboard::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_raytracer_material_basematerial::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_material_chessboard::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_raytracer_material_basematerial::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_material_chessboard::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_raytracer_material_basematerial::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_material_chessboard$os_get(const char *s) {
  return c_raytracer_material_chessboard::os_get(s, -1);
}
Variant &cw_raytracer_material_chessboard$os_lval(const char *s) {
  return c_raytracer_material_chessboard::os_lval(s, -1);
}
Variant cw_raytracer_material_chessboard$os_constant(const char *s) {
  return c_raytracer_material_chessboard::os_constant(s);
}
Variant cw_raytracer_material_chessboard$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_material_chessboard::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_material_chessboard::init() {
  c_raytracer_material_basematerial::init();
  m_colorEven = null;
  m_colorOdd = null;
  m_density = 0.5;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php line 7 */
void c_raytracer_material_chessboard::t___construct(Variant v_colorEven, Variant v_colorOdd, Variant v_reflection, Variant v_transparency, Variant v_gloss, Variant v_density) {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_Chessboard, RayTracer_Material_Chessboard::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_colorEven = v_colorEven);
  (m_colorOdd = v_colorOdd);
  (o_lval("reflection", 0x6F1A43B7D5277CDFLL) = v_reflection);
  (o_lval("transparency", 0x0309BEA034533891LL) = v_transparency);
  (o_lval("gloss", 0x6C751D32BD23BF1ALL) = v_gloss);
  (m_density = v_density);
  (o_lval("hasTexture", 0x6C2EAE64D6A215AALL) = true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php line 18 */
Variant c_raytracer_material_chessboard::t_getcolor(CVarRef v_u, CVarRef v_v) {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_Chessboard, RayTracer_Material_Chessboard::getColor);
  Numeric v_t = 0;

  (v_t = multiply_rev(LINE(20,o_root_invoke_few_args("wrapUp", 0x40ED9DDFEBBD611ALL, 1, v_v * m_density)), o_root_invoke_few_args("wrapUp", 0x40ED9DDFEBBD611ALL, 1, v_u * m_density)));
  if (less(v_t, 0.0)) return m_colorEven;
  else return m_colorOdd;
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php line 28 */
String c_raytracer_material_chessboard::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_Chessboard, RayTracer_Material_Chessboard::__toString);
  return concat("ChessMaterial [gloss=", LINE(30,concat6(toString(o_get("gloss", 0x6C751D32BD23BF1ALL)), ", transparency=", toString(o_get("transparency", 0x0309BEA034533891LL)), ", hasTexture=", toString(o_get("hasTexture", 0x6C2EAE64D6A215AALL)), "]")));
} /* function */
Object co_raytracer_material_chessboard(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_material_chessboard(NEW(c_raytracer_material_chessboard)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Chessboard_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Chessboard_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
