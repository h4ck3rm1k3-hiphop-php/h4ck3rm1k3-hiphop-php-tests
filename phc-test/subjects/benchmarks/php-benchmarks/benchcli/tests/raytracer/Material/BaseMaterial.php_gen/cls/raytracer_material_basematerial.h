
#ifndef __GENERATED_cls_raytracer_material_basematerial_h__
#define __GENERATED_cls_raytracer_material_basematerial_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/BaseMaterial.php line 2 */
class c_raytracer_material_basematerial : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_material_basematerial)
  END_CLASS_MAP(raytracer_material_basematerial)
  DECLARE_CLASS(raytracer_material_basematerial, RayTracer_Material_BaseMaterial, ObjectData)
  void init();
  public: double m_gloss;
  public: double m_transparency;
  public: double m_reflection;
  public: double m_refraction;
  public: bool m_hasTexture;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  // public: void t_getcolor(CVarRef v_u, CVarRef v_v) = 0;
  public: double t_wrapup(double v_t);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_material_basematerial_h__
