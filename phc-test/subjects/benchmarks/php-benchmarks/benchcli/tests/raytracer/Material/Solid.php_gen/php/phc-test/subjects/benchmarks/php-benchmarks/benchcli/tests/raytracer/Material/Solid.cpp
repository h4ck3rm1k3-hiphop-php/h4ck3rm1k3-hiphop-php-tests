
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php line 4 */
Variant c_raytracer_material_solid::os_get(const char *s, int64 hash) {
  return c_raytracer_material_basematerial::os_get(s, hash);
}
Variant &c_raytracer_material_solid::os_lval(const char *s, int64 hash) {
  return c_raytracer_material_basematerial::os_lval(s, hash);
}
void c_raytracer_material_solid::o_get(ArrayElementVec &props) const {
  c_raytracer_material_basematerial::o_get(props);
}
bool c_raytracer_material_solid::o_exists(CStrRef s, int64 hash) const {
  return c_raytracer_material_basematerial::o_exists(s, hash);
}
Variant c_raytracer_material_solid::o_get(CStrRef s, int64 hash) {
  return c_raytracer_material_basematerial::o_get(s, hash);
}
Variant c_raytracer_material_solid::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_raytracer_material_basematerial::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_material_solid::o_lval(CStrRef s, int64 hash) {
  return c_raytracer_material_basematerial::o_lval(s, hash);
}
Variant c_raytracer_material_solid::os_constant(const char *s) {
  return c_raytracer_material_basematerial::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_material_solid)
ObjectData *c_raytracer_material_solid::create(Variant v_color, Variant v_reflection, Variant v_refraction, Variant v_transparency, Variant v_gloss) {
  init();
  t___construct(v_color, v_reflection, v_refraction, v_transparency, v_gloss);
  return this;
}
ObjectData *c_raytracer_material_solid::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
  } else return this;
}
ObjectData *c_raytracer_material_solid::cloneImpl() {
  c_raytracer_material_solid *obj = NEW(c_raytracer_material_solid)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_material_solid::cloneSet(c_raytracer_material_solid *clone) {
  c_raytracer_material_basematerial::cloneSet(clone);
}
Variant c_raytracer_material_solid::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_raytracer_material_basematerial::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_material_solid::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_raytracer_material_basematerial::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_material_solid::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_raytracer_material_basematerial::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_material_solid$os_get(const char *s) {
  return c_raytracer_material_solid::os_get(s, -1);
}
Variant &cw_raytracer_material_solid$os_lval(const char *s) {
  return c_raytracer_material_solid::os_lval(s, -1);
}
Variant cw_raytracer_material_solid$os_constant(const char *s) {
  return c_raytracer_material_solid::os_constant(s);
}
Variant cw_raytracer_material_solid$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_material_solid::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_material_solid::init() {
  c_raytracer_material_basematerial::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php line 6 */
void c_raytracer_material_solid::t___construct(Variant v_color, Variant v_reflection, Variant v_refraction, Variant v_transparency, Variant v_gloss) {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_Solid, RayTracer_Material_Solid::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("color", 0x1B20384A65BB9927LL) = v_color);
  (o_lval("reflection", 0x6F1A43B7D5277CDFLL) = v_reflection);
  (o_lval("transparency", 0x0309BEA034533891LL) = v_transparency);
  (o_lval("gloss", 0x6C751D32BD23BF1ALL) = v_gloss);
  (o_lval("hasTexture", 0x6C2EAE64D6A215AALL) = false);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php line 15 */
Variant c_raytracer_material_solid::t_getcolor(CVarRef v_u, CVarRef v_v) {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_Solid, RayTracer_Material_Solid::getColor);
  return o_get("color", 0x1B20384A65BB9927LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php line 19 */
String c_raytracer_material_solid::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Material_Solid, RayTracer_Material_Solid::__toString);
  return concat("SolidMaterial [gloss=", LINE(21,concat6(toString(o_get("gloss", 0x6C751D32BD23BF1ALL)), ", transparency=", toString(o_get("transparency", 0x0309BEA034533891LL)), ", hasTexture=", toString(o_get("hasTexture", 0x6C2EAE64D6A215AALL)), "]")));
} /* function */
Object co_raytracer_material_solid(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_material_solid(NEW(c_raytracer_material_solid)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Solid_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Solid_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,require("raytracer/Material/BaseMaterial.php", true, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
