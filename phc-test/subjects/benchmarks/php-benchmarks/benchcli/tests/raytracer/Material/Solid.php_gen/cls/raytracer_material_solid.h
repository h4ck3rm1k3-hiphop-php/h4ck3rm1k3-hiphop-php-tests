
#ifndef __GENERATED_cls_raytracer_material_solid_h__
#define __GENERATED_cls_raytracer_material_solid_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php line 4 */
class c_raytracer_material_solid : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_material_solid)
  END_CLASS_MAP(raytracer_material_solid)
  DECLARE_CLASS(raytracer_material_solid, RayTracer_Material_Solid, raytracer_material_basematerial)
  void init();
  public: void t___construct(Variant v_color, Variant v_reflection, Variant v_refraction, Variant v_transparency, Variant v_gloss);
  public: ObjectData *create(Variant v_color, Variant v_reflection, Variant v_refraction, Variant v_transparency, Variant v_gloss);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_getcolor(CVarRef v_u, CVarRef v_v);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_material_solid_h__
