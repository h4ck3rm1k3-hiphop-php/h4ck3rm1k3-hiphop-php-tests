
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.php line 7 */
Variant c_raytracer_scene::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_scene::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_scene::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("camera", m_camera.isReferenced() ? ref(m_camera) : m_camera));
  props.push_back(NEW(ArrayElement)("shapes", m_shapes.isReferenced() ? ref(m_shapes) : m_shapes));
  props.push_back(NEW(ArrayElement)("lights", m_lights.isReferenced() ? ref(m_lights) : m_lights));
  props.push_back(NEW(ArrayElement)("background", m_background.isReferenced() ? ref(m_background) : m_background));
  c_ObjectData::o_get(props);
}
bool c_raytracer_scene::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x7F6800BD02314708LL, shapes, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x785B3CA80D6C5903LL, camera, 6);
      HASH_EXISTS_STRING(0x7FDAF02B1B44631BLL, lights, 6);
      break;
    case 4:
      HASH_EXISTS_STRING(0x5E5EB993A1FE90FCLL, background, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_scene::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x7F6800BD02314708LL, m_shapes,
                         shapes, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x785B3CA80D6C5903LL, m_camera,
                         camera, 6);
      HASH_RETURN_STRING(0x7FDAF02B1B44631BLL, m_lights,
                         lights, 6);
      break;
    case 4:
      HASH_RETURN_STRING(0x5E5EB993A1FE90FCLL, m_background,
                         background, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_scene::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x7F6800BD02314708LL, m_shapes,
                      shapes, 6);
      break;
    case 3:
      HASH_SET_STRING(0x785B3CA80D6C5903LL, m_camera,
                      camera, 6);
      HASH_SET_STRING(0x7FDAF02B1B44631BLL, m_lights,
                      lights, 6);
      break;
    case 4:
      HASH_SET_STRING(0x5E5EB993A1FE90FCLL, m_background,
                      background, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_scene::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x7F6800BD02314708LL, m_shapes,
                         shapes, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x785B3CA80D6C5903LL, m_camera,
                         camera, 6);
      HASH_RETURN_STRING(0x7FDAF02B1B44631BLL, m_lights,
                         lights, 6);
      break;
    case 4:
      HASH_RETURN_STRING(0x5E5EB993A1FE90FCLL, m_background,
                         background, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_scene::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_scene)
ObjectData *c_raytracer_scene::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_raytracer_scene::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_raytracer_scene::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_raytracer_scene::cloneImpl() {
  c_raytracer_scene *obj = NEW(c_raytracer_scene)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_scene::cloneSet(c_raytracer_scene *clone) {
  clone->m_camera = m_camera.isReferenced() ? ref(m_camera) : m_camera;
  clone->m_shapes = m_shapes.isReferenced() ? ref(m_shapes) : m_shapes;
  clone->m_lights = m_lights.isReferenced() ? ref(m_lights) : m_lights;
  clone->m_background = m_background.isReferenced() ? ref(m_background) : m_background;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_scene::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_scene::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_scene::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_scene$os_get(const char *s) {
  return c_raytracer_scene::os_get(s, -1);
}
Variant &cw_raytracer_scene$os_lval(const char *s) {
  return c_raytracer_scene::os_lval(s, -1);
}
Variant cw_raytracer_scene$os_constant(const char *s) {
  return c_raytracer_scene::os_constant(s);
}
Variant cw_raytracer_scene$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_scene::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_scene::init() {
  m_camera = null;
  m_shapes = null;
  m_lights = null;
  m_background = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.php line 21 */
void c_raytracer_scene::t___construct() {
  INSTANCE_METHOD_INJECTION(RayTracer_Scene, RayTracer_Scene::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  (m_camera = ((Object)(LINE(27,(assignCallTemp(eo_0, ((Object)(LINE(24,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0LL, 0LL, -5LL)))))),assignCallTemp(eo_1, ((Object)(LINE(25,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0LL, 0LL, 1LL)))))),assignCallTemp(eo_2, ((Object)(LINE(26,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0LL, 1LL, 0LL)))))),p_raytracer_camera(p_raytracer_camera(NEWOBJ(c_raytracer_camera)())->create(eo_0, eo_1, eo_2)))))));
  (m_shapes = ScalarArrays::sa_[0]);
  (m_lights = ScalarArrays::sa_[0]);
  (m_background = ((Object)(LINE(30,(assignCallTemp(eo_0, ((Object)(p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0LL, 0.5))))),p_raytracer_background(p_raytracer_background(NEWOBJ(c_raytracer_background)())->create(eo_0, 0.20000000000000001)))))));
  gasInCtor(oldInCtor);
} /* function */
Object co_raytracer_scene(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_scene(NEW(c_raytracer_scene)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php(true, variables));
  LINE(3,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Vector_php(true, variables));
  LINE(4,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Background_php(true, variables));
  LINE(5,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Color_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
