
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_IntersectionInfo_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_IntersectionInfo_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.fw.h>

// Declarations
#include <cls/raytracer_intersectioninfo.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$IntersectionInfo_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_raytracer_intersectioninfo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_IntersectionInfo_h__
