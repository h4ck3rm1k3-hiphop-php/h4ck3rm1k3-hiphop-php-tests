
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 2 */
Variant c_raytracer_vector::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_vector::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_vector::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("x", m_x.isReferenced() ? ref(m_x) : m_x));
  props.push_back(NEW(ArrayElement)("y", m_y.isReferenced() ? ref(m_y) : m_y));
  props.push_back(NEW(ArrayElement)("z", m_z.isReferenced() ? ref(m_z) : m_z));
  c_ObjectData::o_get(props);
}
bool c_raytracer_vector::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_EXISTS_STRING(0x4F56B733A4DFC78ALL, y, 1);
      break;
    case 3:
      HASH_EXISTS_STRING(0x62A103F6518DE2B3LL, z, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_vector::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_vector::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_SET_STRING(0x4F56B733A4DFC78ALL, m_y,
                      y, 1);
      break;
    case 3:
      HASH_SET_STRING(0x62A103F6518DE2B3LL, m_z,
                      z, 1);
      break;
    case 6:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_vector::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x4F56B733A4DFC78ALL, m_y,
                         y, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x62A103F6518DE2B3LL, m_z,
                         z, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_vector::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_vector)
ObjectData *c_raytracer_vector::create(Variant v_x //  = 0.0
, Variant v_y //  = 0.0
, Variant v_z //  = 0.0
) {
  init();
  t___construct(v_x, v_y, v_z);
  return this;
}
ObjectData *c_raytracer_vector::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_raytracer_vector::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  if (count == 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_raytracer_vector::cloneImpl() {
  c_raytracer_vector *obj = NEW(c_raytracer_vector)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_vector::cloneSet(c_raytracer_vector *clone) {
  clone->m_x = m_x.isReferenced() ? ref(m_x) : m_x;
  clone->m_y = m_y.isReferenced() ? ref(m_y) : m_y;
  clone->m_z = m_z.isReferenced() ? ref(m_z) : m_z;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_vector::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x11A2D4B742CB6C5ALL, normalize) {
        return (t_normalize());
      }
      break;
    case 6:
      HASH_GUARD(0x4968E44FD6B195A6LL, cross) {
        return (t_cross(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      HASH_GUARD(0x3040FFCBB68C33A7LL, dot) {
        return (t_dot(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_vector::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x11A2D4B742CB6C5ALL, normalize) {
        return (t_normalize());
      }
      break;
    case 6:
      HASH_GUARD(0x4968E44FD6B195A6LL, cross) {
        return (t_cross(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      HASH_GUARD(0x3040FFCBB68C33A7LL, dot) {
        return (t_dot(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_vector::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_vector$os_get(const char *s) {
  return c_raytracer_vector::os_get(s, -1);
}
Variant &cw_raytracer_vector$os_lval(const char *s) {
  return c_raytracer_vector::os_lval(s, -1);
}
Variant cw_raytracer_vector$os_constant(const char *s) {
  return c_raytracer_vector::os_constant(s);
}
Variant cw_raytracer_vector$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_vector::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_vector::init() {
  m_x = 0.0;
  m_y = 0.0;
  m_z = 0.0;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 14 */
void c_raytracer_vector::t___construct(Variant v_x //  = 0.0
, Variant v_y //  = 0.0
, Variant v_z //  = 0.0
) {
  INSTANCE_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_x = v_x);
  (m_y = v_y);
  (m_z = v_z);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 20 */
void c_raytracer_vector::t_copy(p_raytracer_vector v_vector) {
  INSTANCE_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::copy);
  (m_x = v_vector->m_x);
  (m_y = v_vector->m_y);
  (m_z = v_vector->m_z);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 26 */
p_raytracer_vector c_raytracer_vector::t_normalize() {
  INSTANCE_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::normalize);
  double v_m = 0.0;

  (v_m = LINE(28,t_magnitude()));
  return ((Object)(LINE(29,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(divide(m_x, v_m), divide(m_y, v_m), divide(m_z, v_m))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 32 */
double c_raytracer_vector::t_magnitude() {
  INSTANCE_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::magnitude);
  return LINE(34,x_sqrt(toDouble((m_x * m_x) + (m_y * m_y) + (m_z * m_z))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 37 */
p_raytracer_vector c_raytracer_vector::t_cross(CVarRef v_w) {
  INSTANCE_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::cross);
  return ((Object)(LINE(43,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(negate(m_z) * toObject(v_w).o_get("y", 0x4F56B733A4DFC78ALL) + m_y * toObject(v_w).o_get("z", 0x62A103F6518DE2B3LL), m_z * toObject(v_w).o_get("x", 0x04BFC205E59FA416LL) - m_x * toObject(v_w).o_get("z", 0x62A103F6518DE2B3LL), negate(m_y) * toObject(v_w).o_get("x", 0x04BFC205E59FA416LL) + m_x * toObject(v_w).o_get("y", 0x4F56B733A4DFC78ALL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 46 */
PlusOperand c_raytracer_vector::t_dot(CVarRef v_w) {
  INSTANCE_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::dot);
  return m_x * toObject(v_w).o_get("x", 0x04BFC205E59FA416LL) + m_y * toObject(v_w).o_get("y", 0x4F56B733A4DFC78ALL) + m_z * toObject(v_w).o_get("z", 0x62A103F6518DE2B3LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 51 */
p_raytracer_vector c_raytracer_vector::ti_add(const char* cls, CVarRef v_v, CVarRef v_w) {
  STATIC_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::add);
  return ((Object)(LINE(53,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(toObject(v_w).o_get("x", 0x04BFC205E59FA416LL) + toObject(v_v).o_get("x", 0x04BFC205E59FA416LL), toObject(v_w).o_get("y", 0x4F56B733A4DFC78ALL) + toObject(v_v).o_get("y", 0x4F56B733A4DFC78ALL), toObject(v_w).o_get("z", 0x62A103F6518DE2B3LL) + toObject(v_v).o_get("z", 0x62A103F6518DE2B3LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 56 */
p_raytracer_vector c_raytracer_vector::ti_subtract(const char* cls, CVarRef v_v, CVarRef v_w) {
  STATIC_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::subtract);
  return ((Object)(LINE(58,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(toObject(v_v).o_get("x", 0x04BFC205E59FA416LL) - toObject(v_w).o_get("x", 0x04BFC205E59FA416LL), toObject(v_v).o_get("y", 0x4F56B733A4DFC78ALL) - toObject(v_w).o_get("y", 0x4F56B733A4DFC78ALL), toObject(v_v).o_get("z", 0x62A103F6518DE2B3LL) - toObject(v_w).o_get("z", 0x62A103F6518DE2B3LL))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 61 */
p_raytracer_vector c_raytracer_vector::ti_multiplyvector(const char* cls, p_raytracer_vector v_v, p_raytracer_vector v_w) {
  STATIC_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::multiplyVector);
  return ((Object)(LINE(63,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(v_v->m_x * v_w->m_x, v_v->m_y * v_w->m_y, v_v->m_z * v_w->m_z)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 73 */
p_raytracer_vector c_raytracer_vector::ti_multiplyscalar(const char* cls, CVarRef v_v, CVarRef v_w) {
  STATIC_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::multiplyScalar);
  return ((Object)(LINE(75,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(toObject(v_v).o_get("x", 0x04BFC205E59FA416LL) * v_w, toObject(v_v).o_get("y", 0x4F56B733A4DFC78ALL) * v_w, toObject(v_v).o_get("z", 0x62A103F6518DE2B3LL) * v_w)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php line 78 */
String c_raytracer_vector::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Vector, RayTracer_Vector::__toString);
  return concat("Vector [", LINE(80,concat6(toString(m_x), ",", toString(m_y), ",", toString(m_z), "]")));
} /* function */
Object co_raytracer_vector(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_vector(NEW(c_raytracer_vector)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Vector_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Vector_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
