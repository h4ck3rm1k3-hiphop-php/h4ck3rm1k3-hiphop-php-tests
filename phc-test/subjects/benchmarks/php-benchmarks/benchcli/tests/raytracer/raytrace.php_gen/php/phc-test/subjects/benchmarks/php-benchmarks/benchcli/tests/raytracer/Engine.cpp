
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 3 */
Variant c_raytracer_engine::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_engine::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_engine::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("canvas", m_canvas.isReferenced() ? ref(m_canvas) : m_canvas));
  c_ObjectData::o_get(props);
}
bool c_raytracer_engine::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x228C11FD0E541CFDLL, canvas, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_engine::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x228C11FD0E541CFDLL, m_canvas,
                         canvas, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_engine::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x228C11FD0E541CFDLL, m_canvas,
                      canvas, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_engine::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x228C11FD0E541CFDLL, m_canvas,
                         canvas, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_engine::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_engine)
ObjectData *c_raytracer_engine::create(Variant v_options //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_options);
  return this;
}
ObjectData *c_raytracer_engine::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_raytracer_engine::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_raytracer_engine::cloneImpl() {
  c_raytracer_engine *obj = NEW(c_raytracer_engine)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_engine::cloneSet(c_raytracer_engine *clone) {
  clone->m_canvas = m_canvas.isReferenced() ? ref(m_canvas) : m_canvas;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_engine::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x53BA28D6F214D0D0LL, renderscene) {
        return (t_renderscene(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_engine::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x53BA28D6F214D0D0LL, renderscene) {
        return (t_renderscene(a0, a1), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_engine::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_engine$os_get(const char *s) {
  return c_raytracer_engine::os_get(s, -1);
}
Variant &cw_raytracer_engine$os_lval(const char *s) {
  return c_raytracer_engine::os_lval(s, -1);
}
Variant cw_raytracer_engine$os_constant(const char *s) {
  return c_raytracer_engine::os_constant(s);
}
Variant cw_raytracer_engine$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_engine::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_engine::init() {
  m_canvas = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 6 */
void c_raytracer_engine::t___construct(Variant v_options //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(RayTracer_Engine, RayTracer_Engine::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("options", 0x7C17922060DCA1EALL) = LINE(18,x_array_merge(2, ScalarArrays::sa_[1], Array(ArrayInit(1).set(0, v_options).create()))));
  lval(lval(o_lval("options", 0x7C17922060DCA1EALL)).lvalAt("canvasHeight", 0x2E4AA2F9F8F66281LL)) /= o_get("options", 0x7C17922060DCA1EALL).rvalAt("pixelHeight", 0x65EFBEE0F61CEE96LL);
  lval(lval(o_lval("options", 0x7C17922060DCA1EALL)).lvalAt("canvasWidth", 0x7A7A553887A71393LL)) /= o_get("options", 0x7C17922060DCA1EALL).rvalAt("pixelWidth", 0x7BBED918FC15A794LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 26 */
void c_raytracer_engine::t_setpixel(int64 v_x, int64 v_y, CVarRef v_color) {
  INSTANCE_METHOD_INJECTION(RayTracer_Engine, RayTracer_Engine::setPixel);
  Variant v_pxW;
  Variant v_pxH;
  Variant v_colorId;
  Numeric v_x1 = 0;
  Numeric v_y1 = 0;

  (v_pxW = o_get("options", 0x7C17922060DCA1EALL).rvalAt("pixelWidth", 0x7BBED918FC15A794LL));
  (v_pxH = o_get("options", 0x7C17922060DCA1EALL).rvalAt("pixelHeight", 0x65EFBEE0F61CEE96LL));
  if (toBoolean(m_canvas)) {
    (v_colorId = LINE(31,x_imagecolorallocate(toObject(m_canvas), toInt32(toObject(v_color).o_get("red", 0x2D82AF0F4EA2D20CLL) * 255LL), toInt32(toObject(v_color).o_get("green", 0x78415B637DF28CD9LL) * 255LL), toInt32(toObject(v_color).o_get("blue", 0x565BF9A195324467LL) * 255LL))));
    (v_x1 = v_x * v_pxW);
    (v_y1 = v_y * v_pxH);
    LINE(34,x_imagefilledrectangle(toObject(m_canvas), toInt32(v_x1), toInt32(v_y1), toInt32(v_x1 + v_pxW), toInt32(v_y1 + v_pxH), toInt32(v_colorId)));
  }
  else {
    if (same(v_x, v_y)) {
      o_lval("checkNumber", 0x2B1FCE28CB1522D4LL) += LINE(41,toObject(v_color)->o_invoke_few_args("brightness", 0x47FA63D13C3328CBLL, 0));
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 47 */
void c_raytracer_engine::t_renderscene(p_raytracer_scene v_scene, CVarRef v_canvas) {
  INSTANCE_METHOD_INJECTION(RayTracer_Engine, RayTracer_Engine::renderScene);
  Variant v_canvasHeight;
  Variant v_canvasWidth;
  int64 v_y = 0;
  int64 v_x = 0;
  Variant v_yp;
  Variant v_xp;
  Variant v_ray;
  Variant v_color;

  (o_lval("checkNumber", 0x2B1FCE28CB1522D4LL) = 0LL);
  if (toBoolean(v_canvas)) {
    (m_canvas = v_canvas);
  }
  else {
    (m_canvas = null);
  }
  (v_canvasHeight = o_get("options", 0x7C17922060DCA1EALL).rvalAt("canvasHeight", 0x2E4AA2F9F8F66281LL));
  (v_canvasWidth = o_get("options", 0x7C17922060DCA1EALL).rvalAt("canvasWidth", 0x7A7A553887A71393LL));
  {
    LOOP_COUNTER(1);
    for ((v_y = 0LL); less(v_y, v_canvasHeight); v_y++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_x = 0LL); less(v_x, v_canvasWidth); v_x++) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_yp = divide(v_y * 1.0, v_canvasHeight) * 2LL - 1LL);
              (v_xp = divide(v_x * 1.0, v_canvasWidth) * 2LL - 1LL);
              (v_ray = LINE(66,v_scene->m_camera.o_invoke_few_args("getRay", 0x288561B37F98ABEALL, 2, v_xp, v_yp)));
              (v_color = LINE(68,t_getpixelcolor(v_ray, ((Object)(v_scene)))));
              LINE(70,t_setpixel(v_x, v_y, v_color));
            }
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 80 */
Variant c_raytracer_engine::t_getpixelcolor(CVarRef v_ray, p_raytracer_scene v_scene) {
  INSTANCE_METHOD_INJECTION(RayTracer_Engine, RayTracer_Engine::getPixelColor);
  Variant v_info;
  p_raytracer_color v_color;

  (v_info = LINE(82,t_testintersection(v_ray, ((Object)(v_scene)), null)));
  if (toBoolean(v_info.o_get("isHit", 0x50D9C879A5A6A64FLL))) {
    ((Object)((v_color = ((Object)(LINE(84,t_raytrace(v_info, v_ray, ((Object)(v_scene)), 0LL)))))));
    return ((Object)(v_color));
  }
  return v_scene->m_background.o_get("color", 0x1B20384A65BB9927LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 90 */
Variant c_raytracer_engine::t_testintersection(Variant v_ray, p_raytracer_scene v_scene, CVarRef v_exclude) {
  INSTANCE_METHOD_INJECTION(RayTracer_Engine, RayTracer_Engine::testIntersection);
  int64 v_hits = 0;
  Variant v_best;
  Variant v_shape;
  Variant v_info;

  (v_hits = 0LL);
  (v_best = ((Object)(LINE(93,p_raytracer_intersectioninfo(p_raytracer_intersectioninfo(NEWOBJ(c_raytracer_intersectioninfo)())->create())))));
  (v_best.o_lval("distance", 0x1E9F6AF49FD61031LL) = 9223372036854775800.0);
  {
    LOOP_COUNTER(3);
    Variant map4 = v_scene->m_shapes;
    for (ArrayIterPtr iter5 = map4.begin("raytracer_engine"); !iter5->end(); iter5->next()) {
      LOOP_COUNTER_CHECK(3);
      v_shape = iter5->second();
      {
        if (!equal(v_shape, v_exclude)) {
          (v_info = LINE(99,v_shape.o_invoke_few_args("intersect", 0x4A3F7CCEE998444ALL, 1, v_ray)));
          if (toBoolean(v_info.o_get("isHit", 0x50D9C879A5A6A64FLL)) && not_less(v_info.o_get("distance", 0x1E9F6AF49FD61031LL), 0LL) && less(v_info.o_get("distance", 0x1E9F6AF49FD61031LL), v_best.o_get("distance", 0x1E9F6AF49FD61031LL))) {
            (v_best = v_info);
            v_hits++;
          }
        }
      }
    }
  }
  (v_best.o_lval("hitCount", 0x309E00625B958904LL) = v_hits);
  return v_best;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 110 */
p_raytracer_ray c_raytracer_engine::t_getreflectionray(CVarRef v_P, CVarRef v_N, Variant v_V) {
  INSTANCE_METHOD_INJECTION(RayTracer_Engine, RayTracer_Engine::getReflectionRay);
  Variant eo_0;
  Variant eo_1;
  Numeric v_c1 = 0;
  p_raytracer_vector v_R1;

  (v_c1 = negate(LINE(112,toObject(v_N)->o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, v_V))));
  ((Object)((v_R1 = ((Object)(LINE(116,(assignCallTemp(eo_0, ((Object)(LINE(114,c_raytracer_vector::t_multiplyscalar(v_N, 2LL * v_c1))))),assignCallTemp(eo_1, v_V),c_raytracer_vector::t_add(eo_0, eo_1))))))));
  return ((Object)(LINE(117,p_raytracer_ray(p_raytracer_ray(NEWOBJ(c_raytracer_ray)())->create(v_P, ((Object)(v_R1)))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php line 120 */
p_raytracer_color c_raytracer_engine::t_raytrace(CVarRef v_info, CVarRef v_ray, p_raytracer_scene v_scene, int64 v_depth) {
  INSTANCE_METHOD_INJECTION(RayTracer_Engine, RayTracer_Engine::rayTrace);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  p_raytracer_color v_color;
  Numeric v_shininess = 0;
  Variant v_light;
  p_raytracer_vector v_v;
  PlusOperand v_L = 0;
  p_raytracer_ray v_reflectionRay;
  Variant v_refl;
  Variant v_shadowInfo;
  p_raytracer_ray v_shadowRay;
  p_raytracer_color v_vA;
  double v_dB = 0.0;
  p_raytracer_vector v_Lv;
  p_raytracer_vector v_E;
  Variant v_H;
  Numeric v_glossWeight = 0;

  ((Object)((v_color = ((Object)(LINE(123,c_raytracer_color::t_multiplyscalar(toObject(v_info).o_get("color", 0x1B20384A65BB9927LL), v_scene->m_background.o_get("ambience", 0x1F620AA1E6D91FE4LL))))))));
  (v_shininess = LINE(124,x_pow(10LL, toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL).o_get("material", 0x43CC34CAFFD815F7LL).o_get("gloss", 0x6C751D32BD23BF1ALL) + 1LL)));
  {
    LOOP_COUNTER(6);
    Variant map7 = v_scene->m_lights;
    for (ArrayIterPtr iter8 = map7.begin("raytracer_engine"); !iter8->end(); iter8->next()) {
      LOOP_COUNTER_CHECK(6);
      v_light = iter8->second();
      {
        ((Object)((v_v = ((Object)((assignCallTemp(eo_0, LINE(132,c_raytracer_vector::t_subtract(v_light.o_get("position", 0x461E5B8BBDC024D0LL), toObject(v_info).o_get("position", 0x461E5B8BBDC024D0LL)))),eo_0.toObject().getTyped<c_raytracer_vector>()->t_normalize()))))));
        if (toBoolean(o_get("options", 0x7C17922060DCA1EALL).rvalAt("renderDiffuse", 0x1ADB02124D536C52LL))) {
          (v_L = LINE(135,v_v->t_dot(toObject(v_info).o_get("normal", 0x5847C6837EFC3014LL))));
          if (more(v_L, 0.0)) {
            ((Object)((v_color = ((Object)(LINE(146,(assignCallTemp(eo_1, ((Object)(v_color))),assignCallTemp(eo_2, ((Object)(LINE(145,(assignCallTemp(eo_3, toObject(v_info).o_get("color", 0x1B20384A65BB9927LL)),assignCallTemp(eo_4, ((Object)(LINE(144,c_raytracer_color::t_multiplyscalar(v_light.o_get("color", 0x1B20384A65BB9927LL), v_L))))),c_raytracer_color::t_multiply(eo_3, eo_4)))))),c_raytracer_color::t_add(eo_1, eo_2))))))));
          }
        }
        if (not_more(v_depth, o_get("options", 0x7C17922060DCA1EALL).rvalAt("rayDepth", 0x3BD45DC301CC0535LL))) {
          if (toBoolean(o_get("options", 0x7C17922060DCA1EALL).rvalAt("renderReflections", 0x1F4A803394A32D9CLL)) && more(toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL).o_get("material", 0x43CC34CAFFD815F7LL).o_get("reflection", 0x6F1A43B7D5277CDFLL), 0LL)) {
            ((Object)((v_reflectionRay = ((Object)(LINE(157,t_getreflectionray(toObject(v_info).o_get("position", 0x461E5B8BBDC024D0LL), toObject(v_info).o_get("normal", 0x5847C6837EFC3014LL), toObject(v_ray).o_get("direction", 0x10014728FF258839LL))))))));
            (v_refl = LINE(158,t_testintersection(((Object)(v_reflectionRay)), ((Object)(v_scene)), toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL))));
            if (toBoolean(v_refl.o_get("isHit", 0x50D9C879A5A6A64FLL)) && more(v_refl.o_get("distance", 0x1E9F6AF49FD61031LL), 0LL)) {
              (v_refl.o_lval("color", 0x1B20384A65BB9927LL) = ((Object)(LINE(161,t_raytrace(v_refl, ((Object)(v_reflectionRay)), ((Object)(v_scene)), v_depth + 1LL)))));
            }
            else {
              (v_refl.o_lval("color", 0x1B20384A65BB9927LL) = v_scene->m_background.o_get("color", 0x1B20384A65BB9927LL));
            }
            ((Object)((v_color = ((Object)(LINE(170,c_raytracer_color::t_blend(((Object)(v_color)), v_refl.o_get("color", 0x1B20384A65BB9927LL), toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL).o_get("material", 0x43CC34CAFFD815F7LL).o_get("reflection", 0x6F1A43B7D5277CDFLL))))))));
          }
        }
        (v_shadowInfo = ((Object)(LINE(179,p_raytracer_intersectioninfo(p_raytracer_intersectioninfo(NEWOBJ(c_raytracer_intersectioninfo)())->create())))));
        if (toBoolean(o_get("options", 0x7C17922060DCA1EALL).rvalAt("renderShadows", 0x5C84DC2B748EC6C5LL))) {
          ((Object)((v_shadowRay = ((Object)(LINE(182,p_raytracer_ray(p_raytracer_ray(NEWOBJ(c_raytracer_ray)())->create(toObject(v_info).o_get("position", 0x461E5B8BBDC024D0LL), ((Object)(v_v))))))))));
          (v_shadowInfo = LINE(184,t_testintersection(((Object)(v_shadowRay)), ((Object)(v_scene)), toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL))));
          if (toBoolean(v_shadowInfo.o_get("isHit", 0x50D9C879A5A6A64FLL)) && !equal(v_shadowInfo.o_get("shape", 0x51D6F99D8658AB49LL), toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL))) {
            ((Object)((v_vA = ((Object)(LINE(186,c_raytracer_color::t_multiplyscalar(((Object)(v_color)), 0.5)))))));
            (v_dB = (0.5 * toDouble(LINE(187,x_pow(v_shadowInfo.o_get("shape", 0x51D6F99D8658AB49LL).o_get("material", 0x43CC34CAFFD815F7LL).o_get("transparency", 0x0309BEA034533891LL), 0.5)))));
            ((Object)((v_color = ((Object)(LINE(188,c_raytracer_color::t_addscalar(((Object)(v_vA)), v_dB)))))));
          }
        }
        if (toBoolean(o_get("options", 0x7C17922060DCA1EALL).rvalAt("renderHighlights", 0x2B868E346BBAA931LL)) && !(toBoolean(v_shadowInfo.o_get("isHit", 0x50D9C879A5A6A64FLL))) && more(toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL).o_get("material", 0x43CC34CAFFD815F7LL).o_get("gloss", 0x6C751D32BD23BF1ALL), 0LL)) {
          ((Object)((v_Lv = ((Object)((assignCallTemp(eo_1, LINE(197,c_raytracer_vector::t_subtract(toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL).o_get("position", 0x461E5B8BBDC024D0LL), v_light.o_get("position", 0x461E5B8BBDC024D0LL)))),eo_1.toObject().getTyped<c_raytracer_vector>()->t_normalize()))))));
          ((Object)((v_E = ((Object)((assignCallTemp(eo_2, LINE(202,c_raytracer_vector::t_subtract(v_scene->m_camera.o_get("position", 0x461E5B8BBDC024D0LL), toObject(v_info).o_get("shape", 0x51D6F99D8658AB49LL).o_get("position", 0x461E5B8BBDC024D0LL)))),eo_2.toObject().getTyped<c_raytracer_vector>()->t_normalize()))))));
          (v_H = ((Object)((assignCallTemp(eo_3, LINE(207,c_raytracer_vector::t_subtract(((Object)(v_E)), ((Object)(v_Lv))))),eo_3.toObject().getTyped<c_raytracer_vector>()->t_normalize()))));
          (v_glossWeight = LINE(209,(assignCallTemp(eo_4, (assignCallTemp(eo_6, toObject(v_info).o_get("normal", 0x5847C6837EFC3014LL).o_invoke_few_args("dot", 0x3040FFCBB68C33A7LL, 1, v_H)),x_max(2, eo_6, ScalarArrays::sa_[2]))),assignCallTemp(eo_5, v_shininess),x_pow(eo_4, eo_5))));
          ((Object)((v_color = ((Object)(LINE(213,(assignCallTemp(eo_4, ((Object)(LINE(211,c_raytracer_color::t_multiplyscalar(v_light.o_get("color", 0x1B20384A65BB9927LL), v_glossWeight))))),assignCallTemp(eo_5, ((Object)(v_color))),c_raytracer_color::t_add(eo_4, eo_5))))))));
        }
      }
    }
  }
  LINE(216,v_color->t_limit());
  return ((Object)(v_color));
} /* function */
Object co_raytracer_engine(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_engine(NEW(c_raytracer_engine)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Engine_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Engine_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$IntersectionInfo_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
