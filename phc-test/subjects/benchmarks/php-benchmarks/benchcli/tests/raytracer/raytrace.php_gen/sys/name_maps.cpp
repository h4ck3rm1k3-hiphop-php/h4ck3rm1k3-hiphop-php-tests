
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "raytracer_background", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.php",
  "raytracer_camera", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php",
  "raytracer_color", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.php",
  "raytracer_engine", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.php",
  "raytracer_intersectioninfo", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.php",
  "raytracer_light", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.php",
  "raytracer_material_chessboard", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.php",
  "raytracer_material_solid", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.php",
  "raytracer_ray", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php",
  "raytracer_scene", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.php",
  "raytracer_shape_plane", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.php",
  "raytracer_shape_sphere", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.php",
  "raytracer_vector", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "renderscene", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/raytrace.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
