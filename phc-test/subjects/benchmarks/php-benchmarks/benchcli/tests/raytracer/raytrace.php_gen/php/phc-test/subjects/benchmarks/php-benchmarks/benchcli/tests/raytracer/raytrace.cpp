
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Color.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Engine.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Light.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Chessboard.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Material/Solid.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Scene.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Plane.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/Sphere.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Vector.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/raytrace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/raytrace.php line 3 */
void f_renderscene() {
  FUNCTION_INJECTION(renderScene);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_scene;
  Variant v_sphere;
  Variant v_sphere1;
  Variant v_v;
  Variant v_plane;
  Variant v_light;
  Variant v_light1;
  Variant v_imageWidth;
  Variant v_imageHeight;
  Variant v_pixelSize;
  Variant v_renderDiffuse;
  Variant v_renderShadows;
  Variant v_renderHighlights;
  Variant v_renderReflections;
  Variant v_rayDepth;
  Variant v_raytracer;
  Variant v_im;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_scene; Variant &v_sphere; Variant &v_sphere1; Variant &v_v; Variant &v_plane; Variant &v_light; Variant &v_light1; Variant &v_imageWidth; Variant &v_imageHeight; Variant &v_pixelSize; Variant &v_renderDiffuse; Variant &v_renderShadows; Variant &v_renderHighlights; Variant &v_renderReflections; Variant &v_rayDepth; Variant &v_raytracer; Variant &v_im;
    VariableTable(Variant &r_scene, Variant &r_sphere, Variant &r_sphere1, Variant &r_v, Variant &r_plane, Variant &r_light, Variant &r_light1, Variant &r_imageWidth, Variant &r_imageHeight, Variant &r_pixelSize, Variant &r_renderDiffuse, Variant &r_renderShadows, Variant &r_renderHighlights, Variant &r_renderReflections, Variant &r_rayDepth, Variant &r_raytracer, Variant &r_im) : v_scene(r_scene), v_sphere(r_sphere), v_sphere1(r_sphere1), v_v(r_v), v_plane(r_plane), v_light(r_light), v_light1(r_light1), v_imageWidth(r_imageWidth), v_imageHeight(r_imageHeight), v_pixelSize(r_pixelSize), v_renderDiffuse(r_renderDiffuse), v_renderShadows(r_renderShadows), v_renderHighlights(r_renderHighlights), v_renderReflections(r_renderReflections), v_rayDepth(r_rayDepth), v_raytracer(r_raytracer), v_im(r_im) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 63) {
        case 5:
          HASH_RETURN(0x5C84DC2B748EC6C5LL, v_renderShadows,
                      renderShadows);
          break;
        case 18:
          HASH_RETURN(0x1ADB02124D536C52LL, v_renderDiffuse,
                      renderDiffuse);
          break;
        case 19:
          HASH_RETURN(0x3F2BA2EBC2E2FC93LL, v_scene,
                      scene);
          break;
        case 25:
          HASH_RETURN(0x70EF267415780B19LL, v_imageWidth,
                      imageWidth);
          break;
        case 28:
          HASH_RETURN(0x1F4A803394A32D9CLL, v_renderReflections,
                      renderReflections);
          break;
        case 35:
          HASH_RETURN(0x74F376BCF18134E3LL, v_raytracer,
                      raytracer);
          break;
        case 38:
          HASH_RETURN(0x1FE3F358CC0673A6LL, v_plane,
                      plane);
          break;
        case 43:
          HASH_RETURN(0x3E36C8B08EF16FEBLL, v_sphere,
                      sphere);
          HASH_RETURN(0x3C2F961831E4EF6BLL, v_v,
                      v);
          HASH_RETURN(0x6A48C1C5F35972EBLL, v_light,
                      light);
          break;
        case 44:
          HASH_RETURN(0x0F10BB8BD97DD36CLL, v_im,
                      im);
          break;
        case 48:
          HASH_RETURN(0x3DA12E7929919DB0LL, v_imageHeight,
                      imageHeight);
          break;
        case 49:
          HASH_RETURN(0x2B868E346BBAA931LL, v_renderHighlights,
                      renderHighlights);
          break;
        case 51:
          HASH_RETURN(0x20A3812A7A1059F3LL, v_sphere1,
                      sphere1);
          break;
        case 53:
          HASH_RETURN(0x3BD45DC301CC0535LL, v_rayDepth,
                      rayDepth);
          break;
        case 55:
          HASH_RETURN(0x69D2A8F5CE7D2CB7LL, v_pixelSize,
                      pixelSize);
          break;
        case 59:
          HASH_RETURN(0x5D79543B316B1ABBLL, v_light1,
                      light1);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_scene, v_sphere, v_sphere1, v_v, v_plane, v_light, v_light1, v_imageWidth, v_imageHeight, v_pixelSize, v_renderDiffuse, v_renderShadows, v_renderHighlights, v_renderReflections, v_rayDepth, v_raytracer, v_im);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  LINE(4,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Scene_php(true, variables));
  (v_scene = ((Object)(LINE(5,p_raytracer_scene(p_raytracer_scene(NEWOBJ(c_raytracer_scene)())->create())))));
  (v_scene.o_lval("camera", 0x785B3CA80D6C5903LL) = ((Object)(LINE(11,(assignCallTemp(eo_0, ((Object)(LINE(8,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0LL, 0LL, -15LL)))))),assignCallTemp(eo_1, ((Object)(LINE(9,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(-0.20000000000000001, 0LL, 5LL)))))),assignCallTemp(eo_2, ((Object)(LINE(10,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0LL, 1LL, 0LL)))))),p_raytracer_camera(p_raytracer_camera(NEWOBJ(c_raytracer_camera)())->create(eo_0, eo_1, eo_2)))))));
  (v_scene.o_lval("background", 0x5E5EB993A1FE90FCLL) = ((Object)(LINE(16,(assignCallTemp(eo_0, ((Object)(LINE(14,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.5, 0.5, 0.5)))))),p_raytracer_background(p_raytracer_background(NEWOBJ(c_raytracer_background)())->create(eo_0, 0.40000000000000002)))))));
  LINE(18,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Sphere_php(true, variables));
  LINE(19,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Solid_php(true, variables));
  (v_sphere = ((Object)(LINE(30,(assignCallTemp(eo_0, ((Object)(LINE(21,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(-1.5, 1.5, 2LL)))))),assignCallTemp(eo_2, ((Object)(LINE(29,(assignCallTemp(eo_3, ((Object)(LINE(24,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0LL, 0.5, 0.5)))))),p_raytracer_material_solid(p_raytracer_material_solid(NEWOBJ(c_raytracer_material_solid)())->create(eo_3, 0.29999999999999999, 0.0, 0.0, 2.0))))))),p_raytracer_shape_sphere(p_raytracer_shape_sphere(NEWOBJ(c_raytracer_shape_sphere)())->create(eo_0, 1.5, eo_2)))))));
  (v_sphere1 = ((Object)(LINE(42,(assignCallTemp(eo_0, ((Object)(LINE(33,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(1LL, 0.25, 1LL)))))),assignCallTemp(eo_2, ((Object)(LINE(41,(assignCallTemp(eo_3, ((Object)(LINE(36,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.90000000000000002, 0.90000000000000002, 0.90000000000000002)))))),p_raytracer_material_solid(p_raytracer_material_solid(NEWOBJ(c_raytracer_material_solid)())->create(eo_3, 0.10000000000000001, 0.0, 0.0, 1.5))))))),p_raytracer_shape_sphere(p_raytracer_shape_sphere(NEWOBJ(c_raytracer_shape_sphere)())->create(eo_0, 0.5, eo_2)))))));
  (v_v = ((Object)(LINE(44,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(0.10000000000000001, 0.90000000000000002, -0.5))))));
  LINE(45,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$Plane_php(true, variables));
  LINE(46,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Material$Chessboard_php(true, variables));
  (v_plane = ((Object)(LINE(58,(assignCallTemp(eo_0, LINE(48,v_v.o_invoke_few_args("normalize", 0x11A2D4B742CB6C5ALL, 0))),assignCallTemp(eo_2, ((Object)(LINE(57,(assignCallTemp(eo_3, ((Object)(LINE(51,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(1.0, 1.0, 1.0)))))),assignCallTemp(eo_4, ((Object)(LINE(52,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.0, 0.0, 0.0)))))),p_raytracer_material_chessboard(p_raytracer_material_chessboard(NEWOBJ(c_raytracer_material_chessboard)())->create(eo_3, eo_4, 0.20000000000000001, 0.0, 1.0, 0.69999999999999996))))))),p_raytracer_shape_plane(p_raytracer_shape_plane(NEWOBJ(c_raytracer_shape_plane)())->create(eo_0, 1.2, eo_2)))))));
  lval(v_scene.o_lval("shapes", 0x7F6800BD02314708LL)).append((v_plane));
  lval(v_scene.o_lval("shapes", 0x7F6800BD02314708LL)).append((v_sphere));
  lval(v_scene.o_lval("shapes", 0x7F6800BD02314708LL)).append((v_sphere1));
  LINE(64,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Light_php(true, variables));
  (v_light = ((Object)(LINE(68,(assignCallTemp(eo_0, ((Object)(LINE(66,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(5LL, 10LL, -1LL)))))),assignCallTemp(eo_1, ((Object)(LINE(67,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.80000000000000004, 0.80000000000000004, 0.80000000000000004)))))),p_raytracer_light(p_raytracer_light(NEWOBJ(c_raytracer_light)())->create(eo_0, eo_1)))))));
  (v_light1 = ((Object)(LINE(74,(assignCallTemp(eo_0, ((Object)(LINE(71,p_raytracer_vector(p_raytracer_vector(NEWOBJ(c_raytracer_vector)())->create(-3LL, 5LL, -15LL)))))),assignCallTemp(eo_1, ((Object)(LINE(72,p_raytracer_color(p_raytracer_color(NEWOBJ(c_raytracer_color)())->create(0.80000000000000004, 0.80000000000000004, 0.80000000000000004)))))),p_raytracer_light(p_raytracer_light(NEWOBJ(c_raytracer_light)())->create(eo_0, eo_1, 100LL)))))));
  lval(v_scene.o_lval("lights", 0x7FDAF02B1B44631BLL)).append((v_light));
  lval(v_scene.o_lval("lights", 0x7FDAF02B1B44631BLL)).append((v_light1));
  (v_imageWidth = 100LL);
  (v_imageHeight = 100LL);
  (v_pixelSize = LINE(81,x_split(",", "5,5")));
  (v_renderDiffuse = true);
  (v_renderShadows = true);
  (v_renderHighlights = true);
  (v_renderReflections = true);
  (v_rayDepth = 2LL);
  LINE(88,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Engine_php(true, variables));
  (v_raytracer = ((Object)(LINE(101,p_raytracer_engine(p_raytracer_engine(NEWOBJ(c_raytracer_engine)())->create(Array(ArrayInit(9).set(0, "canvasWidth", v_imageWidth, 0x7A7A553887A71393LL).set(1, "canvasHeight", v_imageHeight, 0x2E4AA2F9F8F66281LL).set(2, "pixelWidth", v_pixelSize.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0x7BBED918FC15A794LL).set(3, "pixelHeight", v_pixelSize.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 0x65EFBEE0F61CEE96LL).set(4, "renderDiffuse", v_renderDiffuse, 0x1ADB02124D536C52LL).set(5, "renderHighlights", v_renderHighlights, 0x2B868E346BBAA931LL).set(6, "renderShadows", v_renderShadows, 0x5C84DC2B748EC6C5LL).set(7, "renderReflections", v_renderReflections, 0x1F4A803394A32D9CLL).set(8, "rayDepth", v_rayDepth, 0x3BD45DC301CC0535LL).create())))))));
  setNull(v_im);
  LINE(106,v_raytracer.o_invoke_few_args("renderScene", 0x53BA28D6F214D0D0LL, 3, v_scene, v_im, 0LL));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$raytrace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/raytrace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$raytrace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_elapsed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("elapsed") : g->GV(elapsed);
  Variant &v_start __attribute__((__unused__)) = (variables != gVariables) ? variables->get("start") : g->GV(start);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_usec __attribute__((__unused__)) = (variables != gVariables) ? variables->get("usec") : g->GV(usec);

  (v_elapsed = 0LL);
  (v_start = LINE(115,x_microtime(true)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 6LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(118,f_renderscene());
      }
    }
  }
  (v_usec = LINE(120,x_microtime(true)) - v_start);
  echo(concat((toString(v_usec * 1000LL)), "ms\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
