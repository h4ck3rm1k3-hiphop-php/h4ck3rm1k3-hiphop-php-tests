
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Shape_BaseShape_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Shape_BaseShape_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/BaseShape.fw.h>

// Declarations
#include <cls/raytracer_shape_baseshape.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$BaseShape_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_raytracer_shape_baseshape(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_raytracer_Shape_BaseShape_h__
