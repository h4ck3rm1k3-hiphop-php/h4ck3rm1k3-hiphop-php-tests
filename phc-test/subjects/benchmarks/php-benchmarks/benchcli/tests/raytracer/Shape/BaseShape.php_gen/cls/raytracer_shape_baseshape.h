
#ifndef __GENERATED_cls_raytracer_shape_baseshape_h__
#define __GENERATED_cls_raytracer_shape_baseshape_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/BaseShape.php line 2 */
class c_raytracer_shape_baseshape : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_shape_baseshape)
  END_CLASS_MAP(raytracer_shape_baseshape)
  DECLARE_CLASS(raytracer_shape_baseshape, RayTracer_Shape_BaseShape, ObjectData)
  void init();
  public: Variant m_position;
  public: Variant m_material;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_shape_baseshape_h__
