
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/BaseShape.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/BaseShape.php line 2 */
Variant c_raytracer_shape_baseshape::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_shape_baseshape::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_shape_baseshape::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("position", m_position.isReferenced() ? ref(m_position) : m_position));
  props.push_back(NEW(ArrayElement)("material", m_material.isReferenced() ? ref(m_material) : m_material));
  c_ObjectData::o_get(props);
}
bool c_raytracer_shape_baseshape::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x461E5B8BBDC024D0LL, position, 8);
      break;
    case 3:
      HASH_EXISTS_STRING(0x43CC34CAFFD815F7LL, material, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_shape_baseshape::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x43CC34CAFFD815F7LL, m_material,
                         material, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_shape_baseshape::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x461E5B8BBDC024D0LL, m_position,
                      position, 8);
      break;
    case 3:
      HASH_SET_STRING(0x43CC34CAFFD815F7LL, m_material,
                      material, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_shape_baseshape::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x43CC34CAFFD815F7LL, m_material,
                         material, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_shape_baseshape::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_shape_baseshape)
ObjectData *c_raytracer_shape_baseshape::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_raytracer_shape_baseshape::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_raytracer_shape_baseshape::cloneImpl() {
  c_raytracer_shape_baseshape *obj = NEW(c_raytracer_shape_baseshape)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_shape_baseshape::cloneSet(c_raytracer_shape_baseshape *clone) {
  clone->m_position = m_position.isReferenced() ? ref(m_position) : m_position;
  clone->m_material = m_material.isReferenced() ? ref(m_material) : m_material;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_shape_baseshape::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_shape_baseshape::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_shape_baseshape::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_shape_baseshape$os_get(const char *s) {
  return c_raytracer_shape_baseshape::os_get(s, -1);
}
Variant &cw_raytracer_shape_baseshape$os_lval(const char *s) {
  return c_raytracer_shape_baseshape::os_lval(s, -1);
}
Variant cw_raytracer_shape_baseshape$os_constant(const char *s) {
  return c_raytracer_shape_baseshape::os_constant(s);
}
Variant cw_raytracer_shape_baseshape$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_shape_baseshape::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_shape_baseshape::init() {
  m_position = null;
  m_material = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/BaseShape.php line 16 */
void c_raytracer_shape_baseshape::t___construct() {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_BaseShape, RayTracer_Shape_BaseShape::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  (m_position = LINE(18,create_object("raytracer_vector", Array(ArrayInit(3).set(0, 0.0).set(1, 0.0).set(2, 0.0).create()))));
  (m_material = LINE(24,(assignCallTemp(eo_0, LINE(20,create_object("raytracer_color", Array(ArrayInit(3).set(0, 1.0).set(1, 0.0).set(2, 1.0).create())))),create_object("raytracer_material_solid", Array(ArrayInit(4).set(0, eo_0).set(1, 0.0).set(2, 0.0).set(3, 0.0).create())))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/BaseShape.php line 27 */
String c_raytracer_shape_baseshape::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Shape_BaseShape, RayTracer_Shape_BaseShape::__toString);
  return concat("Material [gloss=", LINE(29,concat6(toString(o_get("gloss", 0x6C751D32BD23BF1ALL)), ", transparency=", toString(o_get("transparency", 0x0309BEA034533891LL)), ", hasTexture=", toString(o_get("hasTexture", 0x6C2EAE64D6A215AALL)), "]")));
} /* function */
Object co_raytracer_shape_baseshape(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_shape_baseshape(NEW(c_raytracer_shape_baseshape)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$BaseShape_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Shape/BaseShape.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Shape$BaseShape_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
