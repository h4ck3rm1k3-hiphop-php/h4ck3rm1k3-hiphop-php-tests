
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_INCLUDE(0x18F76F61C74CA688LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Camera_php);
      break;
    case 2:
      HASH_INCLUDE(0x61B326FC13F12716LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
