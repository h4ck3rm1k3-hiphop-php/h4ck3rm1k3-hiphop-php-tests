
#ifndef __GENERATED_cls_raytracer_camera_h__
#define __GENERATED_cls_raytracer_camera_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Camera.php line 2 */
class c_raytracer_camera : virtual public ObjectData {
  BEGIN_CLASS_MAP(raytracer_camera)
  END_CLASS_MAP(raytracer_camera)
  DECLARE_CLASS(raytracer_camera, RayTracer_Camera, ObjectData)
  void init();
  public: Variant m_position;
  public: Variant m_lookAt;
  public: Variant m_equator;
  public: Variant m_up;
  public: Variant m_screen;
  public: void t___construct(Variant v_pos, Object v_lookAt, Object v_up);
  public: ObjectData *create(Variant v_pos, Object v_lookAt, Object v_up);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_getray(Variant v_vx, Variant v_vy);
  public: String t___tostring();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_raytracer_camera_h__
