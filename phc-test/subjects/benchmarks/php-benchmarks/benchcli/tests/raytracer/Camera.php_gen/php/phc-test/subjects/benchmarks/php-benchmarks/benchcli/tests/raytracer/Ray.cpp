
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php line 2 */
Variant c_raytracer_ray::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_ray::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_ray::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("position", m_position.isReferenced() ? ref(m_position) : m_position));
  props.push_back(NEW(ArrayElement)("direction", m_direction.isReferenced() ? ref(m_direction) : m_direction));
  c_ObjectData::o_get(props);
}
bool c_raytracer_ray::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x461E5B8BBDC024D0LL, position, 8);
      break;
    case 1:
      HASH_EXISTS_STRING(0x10014728FF258839LL, direction, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_ray::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 1:
      HASH_RETURN_STRING(0x10014728FF258839LL, m_direction,
                         direction, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_ray::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x461E5B8BBDC024D0LL, m_position,
                      position, 8);
      break;
    case 1:
      HASH_SET_STRING(0x10014728FF258839LL, m_direction,
                      direction, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_ray::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 1:
      HASH_RETURN_STRING(0x10014728FF258839LL, m_direction,
                         direction, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_ray::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_ray)
ObjectData *c_raytracer_ray::create(Variant v_pos, Variant v_dir) {
  init();
  t___construct(v_pos, v_dir);
  return this;
}
ObjectData *c_raytracer_ray::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_raytracer_ray::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_raytracer_ray::cloneImpl() {
  c_raytracer_ray *obj = NEW(c_raytracer_ray)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_ray::cloneSet(c_raytracer_ray *clone) {
  clone->m_position = m_position.isReferenced() ? ref(m_position) : m_position;
  clone->m_direction = m_direction.isReferenced() ? ref(m_direction) : m_direction;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_ray::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_ray::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_ray::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_ray$os_get(const char *s) {
  return c_raytracer_ray::os_get(s, -1);
}
Variant &cw_raytracer_ray$os_lval(const char *s) {
  return c_raytracer_ray::os_lval(s, -1);
}
Variant cw_raytracer_ray$os_constant(const char *s) {
  return c_raytracer_ray::os_constant(s);
}
Variant cw_raytracer_ray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_ray::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_ray::init() {
  m_position = null;
  m_direction = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php line 6 */
void c_raytracer_ray::t___construct(Variant v_pos, Variant v_dir) {
  INSTANCE_METHOD_INJECTION(RayTracer_Ray, RayTracer_Ray::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_position = v_pos);
  (m_direction = v_dir);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php line 12 */
String c_raytracer_ray::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_Ray, RayTracer_Ray::__toString);
  return toString((Variant)(concat(toString((Variant)(concat("Ray [", toString(m_position))) + (Variant)(",")), toString(m_direction))) + (Variant)("]"));
} /* function */
Object co_raytracer_ray(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_ray(NEW(c_raytracer_ray)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Ray.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Ray_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
