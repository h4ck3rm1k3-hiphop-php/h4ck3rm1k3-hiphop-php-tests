
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_raytracer_engine(CArrRef params, bool init = true);
Variant cw_raytracer_engine$os_get(const char *s);
Variant &cw_raytracer_engine$os_lval(const char *s);
Variant cw_raytracer_engine$os_constant(const char *s);
Variant cw_raytracer_engine$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_raytracer_intersectioninfo(CArrRef params, bool init = true);
Variant cw_raytracer_intersectioninfo$os_get(const char *s);
Variant &cw_raytracer_intersectioninfo$os_lval(const char *s);
Variant cw_raytracer_intersectioninfo$os_constant(const char *s);
Variant cw_raytracer_intersectioninfo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_CREATE_OBJECT(0x565707C7E0CFEA62LL, raytracer_intersectioninfo);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x6CDE83C3C89CA65BLL, raytracer_engine);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x565707C7E0CFEA62LL, raytracer_intersectioninfo);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x6CDE83C3C89CA65BLL, raytracer_engine);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x565707C7E0CFEA62LL, raytracer_intersectioninfo);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x6CDE83C3C89CA65BLL, raytracer_engine);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x565707C7E0CFEA62LL, raytracer_intersectioninfo);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x6CDE83C3C89CA65BLL, raytracer_engine);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x565707C7E0CFEA62LL, raytracer_intersectioninfo);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x6CDE83C3C89CA65BLL, raytracer_engine);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
