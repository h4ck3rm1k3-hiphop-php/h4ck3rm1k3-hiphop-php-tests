
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.php line 2 */
Variant c_raytracer_intersectioninfo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_intersectioninfo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_intersectioninfo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("isHit", m_isHit));
  props.push_back(NEW(ArrayElement)("hitCount", m_hitCount.isReferenced() ? ref(m_hitCount) : m_hitCount));
  props.push_back(NEW(ArrayElement)("shape", m_shape.isReferenced() ? ref(m_shape) : m_shape));
  props.push_back(NEW(ArrayElement)("position", m_position.isReferenced() ? ref(m_position) : m_position));
  props.push_back(NEW(ArrayElement)("normal", m_normal.isReferenced() ? ref(m_normal) : m_normal));
  props.push_back(NEW(ArrayElement)("color", m_color.isReferenced() ? ref(m_color) : m_color));
  props.push_back(NEW(ArrayElement)("distance", m_distance.isReferenced() ? ref(m_distance) : m_distance));
  c_ObjectData::o_get(props);
}
bool c_raytracer_intersectioninfo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x461E5B8BBDC024D0LL, position, 8);
      break;
    case 1:
      HASH_EXISTS_STRING(0x1E9F6AF49FD61031LL, distance, 8);
      break;
    case 4:
      HASH_EXISTS_STRING(0x309E00625B958904LL, hitCount, 8);
      HASH_EXISTS_STRING(0x5847C6837EFC3014LL, normal, 6);
      break;
    case 7:
      HASH_EXISTS_STRING(0x1B20384A65BB9927LL, color, 5);
      break;
    case 9:
      HASH_EXISTS_STRING(0x51D6F99D8658AB49LL, shape, 5);
      break;
    case 15:
      HASH_EXISTS_STRING(0x50D9C879A5A6A64FLL, isHit, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_intersectioninfo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 1:
      HASH_RETURN_STRING(0x1E9F6AF49FD61031LL, m_distance,
                         distance, 8);
      break;
    case 4:
      HASH_RETURN_STRING(0x309E00625B958904LL, m_hitCount,
                         hitCount, 8);
      HASH_RETURN_STRING(0x5847C6837EFC3014LL, m_normal,
                         normal, 6);
      break;
    case 7:
      HASH_RETURN_STRING(0x1B20384A65BB9927LL, m_color,
                         color, 5);
      break;
    case 9:
      HASH_RETURN_STRING(0x51D6F99D8658AB49LL, m_shape,
                         shape, 5);
      break;
    case 15:
      HASH_RETURN_STRING(0x50D9C879A5A6A64FLL, m_isHit,
                         isHit, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_intersectioninfo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x461E5B8BBDC024D0LL, m_position,
                      position, 8);
      break;
    case 1:
      HASH_SET_STRING(0x1E9F6AF49FD61031LL, m_distance,
                      distance, 8);
      break;
    case 4:
      HASH_SET_STRING(0x309E00625B958904LL, m_hitCount,
                      hitCount, 8);
      HASH_SET_STRING(0x5847C6837EFC3014LL, m_normal,
                      normal, 6);
      break;
    case 7:
      HASH_SET_STRING(0x1B20384A65BB9927LL, m_color,
                      color, 5);
      break;
    case 9:
      HASH_SET_STRING(0x51D6F99D8658AB49LL, m_shape,
                      shape, 5);
      break;
    case 15:
      HASH_SET_STRING(0x50D9C879A5A6A64FLL, m_isHit,
                      isHit, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_intersectioninfo::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x461E5B8BBDC024D0LL, m_position,
                         position, 8);
      break;
    case 1:
      HASH_RETURN_STRING(0x1E9F6AF49FD61031LL, m_distance,
                         distance, 8);
      break;
    case 4:
      HASH_RETURN_STRING(0x309E00625B958904LL, m_hitCount,
                         hitCount, 8);
      HASH_RETURN_STRING(0x5847C6837EFC3014LL, m_normal,
                         normal, 6);
      break;
    case 7:
      HASH_RETURN_STRING(0x1B20384A65BB9927LL, m_color,
                         color, 5);
      break;
    case 9:
      HASH_RETURN_STRING(0x51D6F99D8658AB49LL, m_shape,
                         shape, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_intersectioninfo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_intersectioninfo)
ObjectData *c_raytracer_intersectioninfo::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_raytracer_intersectioninfo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_raytracer_intersectioninfo::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_raytracer_intersectioninfo::cloneImpl() {
  c_raytracer_intersectioninfo *obj = NEW(c_raytracer_intersectioninfo)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_intersectioninfo::cloneSet(c_raytracer_intersectioninfo *clone) {
  clone->m_isHit = m_isHit;
  clone->m_hitCount = m_hitCount.isReferenced() ? ref(m_hitCount) : m_hitCount;
  clone->m_shape = m_shape.isReferenced() ? ref(m_shape) : m_shape;
  clone->m_position = m_position.isReferenced() ? ref(m_position) : m_position;
  clone->m_normal = m_normal.isReferenced() ? ref(m_normal) : m_normal;
  clone->m_color = m_color.isReferenced() ? ref(m_color) : m_color;
  clone->m_distance = m_distance.isReferenced() ? ref(m_distance) : m_distance;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_intersectioninfo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_intersectioninfo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_intersectioninfo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_intersectioninfo$os_get(const char *s) {
  return c_raytracer_intersectioninfo::os_get(s, -1);
}
Variant &cw_raytracer_intersectioninfo$os_lval(const char *s) {
  return c_raytracer_intersectioninfo::os_lval(s, -1);
}
Variant cw_raytracer_intersectioninfo$os_constant(const char *s) {
  return c_raytracer_intersectioninfo::os_constant(s);
}
Variant cw_raytracer_intersectioninfo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_intersectioninfo::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_intersectioninfo::init() {
  m_isHit = false;
  m_hitCount = 0LL;
  m_shape = null;
  m_position = null;
  m_normal = null;
  m_color = null;
  m_distance = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.php line 11 */
void c_raytracer_intersectioninfo::t___construct() {
  INSTANCE_METHOD_INJECTION(RayTracer_IntersectionInfo, RayTracer_IntersectionInfo::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_color = LINE(13,create_object("raytracer_color", Array(ArrayInit(3).set(0, 0LL).set(1, 0LL).set(2, 0LL).create()))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.php line 17 */
String c_raytracer_intersectioninfo::t___tostring() {
  INSTANCE_METHOD_INJECTION(RayTracer_IntersectionInfo, RayTracer_IntersectionInfo::__toString);
  return LINE(19,concat3("Intersection [", toString(m_position), "]"));
} /* function */
Object co_raytracer_intersectioninfo(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_intersectioninfo(NEW(c_raytracer_intersectioninfo)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$IntersectionInfo_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/IntersectionInfo.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$IntersectionInfo_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
