
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.php line 2 */
Variant c_raytracer_background::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_raytracer_background::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_raytracer_background::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("color", m_color.isReferenced() ? ref(m_color) : m_color));
  props.push_back(NEW(ArrayElement)("ambience", m_ambience.isReferenced() ? ref(m_ambience) : m_ambience));
  c_ObjectData::o_get(props);
}
bool c_raytracer_background::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x1F620AA1E6D91FE4LL, ambience, 8);
      break;
    case 3:
      HASH_EXISTS_STRING(0x1B20384A65BB9927LL, color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_raytracer_background::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x1F620AA1E6D91FE4LL, m_ambience,
                         ambience, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x1B20384A65BB9927LL, m_color,
                         color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_raytracer_background::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x1F620AA1E6D91FE4LL, m_ambience,
                      ambience, 8);
      break;
    case 3:
      HASH_SET_STRING(0x1B20384A65BB9927LL, m_color,
                      color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_raytracer_background::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x1F620AA1E6D91FE4LL, m_ambience,
                         ambience, 8);
      break;
    case 3:
      HASH_RETURN_STRING(0x1B20384A65BB9927LL, m_color,
                         color, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_raytracer_background::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(raytracer_background)
ObjectData *c_raytracer_background::create(Variant v_color, Variant v_ambience) {
  init();
  t___construct(v_color, v_ambience);
  return this;
}
ObjectData *c_raytracer_background::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
ObjectData *c_raytracer_background::cloneImpl() {
  c_raytracer_background *obj = NEW(c_raytracer_background)();
  cloneSet(obj);
  return obj;
}
void c_raytracer_background::cloneSet(c_raytracer_background *clone) {
  clone->m_color = m_color.isReferenced() ? ref(m_color) : m_color;
  clone->m_ambience = m_ambience.isReferenced() ? ref(m_ambience) : m_ambience;
  ObjectData::cloneSet(clone);
}
Variant c_raytracer_background::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_raytracer_background::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_raytracer_background::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_raytracer_background$os_get(const char *s) {
  return c_raytracer_background::os_get(s, -1);
}
Variant &cw_raytracer_background$os_lval(const char *s) {
  return c_raytracer_background::os_lval(s, -1);
}
Variant cw_raytracer_background$os_constant(const char *s) {
  return c_raytracer_background::os_constant(s);
}
Variant cw_raytracer_background$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_raytracer_background::os_invoke(c, s, params, -1, fatal);
}
void c_raytracer_background::init() {
  m_color = null;
  m_ambience = 0.0;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.php line 12 */
void c_raytracer_background::t___construct(Variant v_color, Variant v_ambience) {
  INSTANCE_METHOD_INJECTION(RayTracer_Background, RayTracer_Background::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_color = v_color);
  (m_ambience = v_ambience);
  gasInCtor(oldInCtor);
} /* function */
Object co_raytracer_background(CArrRef params, bool init /* = true */) {
  return Object(p_raytracer_background(NEW(c_raytracer_background)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Background_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/Background.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$raytracer$Background_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
