
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "binl2hex", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "bit_rol", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "core_md5", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "hex_md5", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "md5_cmn", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "md5_ff", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "md5_gg", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "md5_hh", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "md5_ii", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "safe_add", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "str2binl", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  "zerofill", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
