
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_md5_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_md5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Primitive f_md5_cmn(Primitive v_q, CVarRef v_a, CVarRef v_b, CVarRef v_x, int64 v_s, int64 v_t);
String f_hex_md5(CVarRef v_s);
String f_binl2hex(CArrRef v_binarray);
Array f_core_md5(Variant v_x, Numeric v_len);
Primitive f_bit_rol(Primitive v_num, int64 v_cnt);
Primitive f_md5_ff(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t);
Primitive f_md5_gg(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t);
Primitive f_md5_hh(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t);
Variant f_str2binl(CVarRef v_str);
Primitive f_md5_ii(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t);
Variant f_zerofill(Variant v_a, Numeric v_b);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_md5_php(bool incOnce = false, LVariableTable* variables = NULL);
Primitive f_safe_add(CVarRef v_x, CVarRef v_y);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_md5_h__
