
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 168 */
Primitive f_md5_cmn(Primitive v_q, CVarRef v_a, CVarRef v_b, CVarRef v_x, int64 v_s, int64 v_t) {
  FUNCTION_INJECTION(md5_cmn);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  return LINE(170,(assignCallTemp(eo_0, (assignCallTemp(eo_2, (assignCallTemp(eo_4, f_safe_add(v_a, v_q)),assignCallTemp(eo_5, f_safe_add(v_x, v_t)),f_safe_add(eo_4, eo_5))),assignCallTemp(eo_3, v_s),f_bit_rol(eo_2, eo_3))),assignCallTemp(eo_1, v_b),f_safe_add(eo_0, eo_1)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 24 */
String f_hex_md5(CVarRef v_s) {
  FUNCTION_INJECTION(hex_md5);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_chrsz __attribute__((__unused__)) = g->GV(chrsz);
  return LINE(27,f_binl2hex((assignCallTemp(eo_0, f_str2binl(v_s)),assignCallTemp(eo_1, x_strlen(toString(v_s)) * gv_chrsz),f_core_md5(eo_0, eo_1))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 231 */
String f_binl2hex(CArrRef v_binarray) {
  FUNCTION_INJECTION(binl2hex);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_hexcase __attribute__((__unused__)) = g->GV(hexcase);
  Variant v_hex_tab;
  String v_str;
  int64 v_i = 0;

  (v_hex_tab = toBoolean(gv_hexcase) ? (("0123456789ABCDEF")) : (("0123456789abcdef")));
  (v_str = "");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(238,x_count(v_binarray)) * 4LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_str, concat(toString(v_hex_tab.rvalAt(bitwise_and((toInt64(toInt64(v_binarray.rvalAt(toInt64(v_i) >> 2LL))) >> (toInt64((modulo(v_i, 4LL)) * 8LL + 4LL))), 15LL))), toString(v_hex_tab.rvalAt(bitwise_and((toInt64(toInt64(v_binarray.rvalAt(toInt64(v_i) >> 2LL))) >> (toInt64((modulo(v_i, 4LL)) * 8LL))), 15LL)))));
      }
    }
  }
  return v_str;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 62 */
Array f_core_md5(Variant v_x, Numeric v_len) {
  FUNCTION_INJECTION(core_md5);
  Variant v_a;
  Variant v_b;
  Variant v_c;
  Variant v_d;
  int64 v_i = 0;
  Variant v_olda;
  Variant v_oldb;
  Variant v_oldc;
  Variant v_oldd;
  int64 v_j = 0;

  v_x.set(toInt64(toInt64(v_len)) >> 5LL, (null));
  lval(v_x.lvalAt(toInt64(toInt64(v_len)) >> 5LL)) |= toInt64(128LL) << (toInt64(modulo(toInt64((toInt64(v_len))), 32LL)));
  v_x.set((toInt64(toInt64(LINE(68,f_zerofill((v_len + 64LL), 9LL)))) << 4LL) + 14LL, (v_len));
  (v_a = 1732584193LL);
  (v_b = -271733879LL);
  (v_c = -1732584194LL);
  (v_d = 271733878LL);
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, LINE(75,x_count(v_x))); v_i += 16LL) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_olda = v_a);
        (v_oldb = v_b);
        (v_oldc = v_c);
        (v_oldd = v_d);
        {
          LOOP_COUNTER(3);
          for ((v_j = 0LL); not_more(v_j, 15LL); v_j++) {
            LOOP_COUNTER_CHECK(3);
            {
              if (!(isset(v_x, v_i + v_j))) v_x.set(v_i + v_j, (null));
            }
          }
        }
        (v_a = LINE(88,f_md5_ff(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i), 7LL, -680876936LL)));
        (v_d = LINE(89,f_md5_ff(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 1LL), 12LL, -389564586LL)));
        (v_c = LINE(90,f_md5_ff(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 2LL), 17LL, 606105819LL)));
        (v_b = LINE(91,f_md5_ff(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 3LL), 22LL, -1044525330LL)));
        (v_a = LINE(92,f_md5_ff(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 4LL), 7LL, -176418897LL)));
        (v_d = LINE(93,f_md5_ff(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 5LL), 12LL, 1200080426LL)));
        (v_c = LINE(94,f_md5_ff(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 6LL), 17LL, -1473231341LL)));
        (v_b = LINE(95,f_md5_ff(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 7LL), 22LL, -45705983LL)));
        (v_a = LINE(96,f_md5_ff(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 8LL), 7LL, 1770035416LL)));
        (v_d = LINE(97,f_md5_ff(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 9LL), 12LL, -1958414417LL)));
        (v_c = LINE(98,f_md5_ff(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 10LL), 17LL, -42063LL)));
        (v_b = LINE(99,f_md5_ff(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 11LL), 22LL, -1990404162LL)));
        (v_a = LINE(100,f_md5_ff(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 12LL), 7LL, 1804603682LL)));
        (v_d = LINE(101,f_md5_ff(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 13LL), 12LL, -40341101LL)));
        (v_c = LINE(102,f_md5_ff(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 14LL), 17LL, -1502002290LL)));
        (v_b = LINE(103,f_md5_ff(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 15LL), 22LL, 1236535329LL)));
        (v_a = LINE(105,f_md5_gg(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 1LL), 5LL, -165796510LL)));
        (v_d = LINE(106,f_md5_gg(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 6LL), 9LL, -1069501632LL)));
        (v_c = LINE(107,f_md5_gg(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 11LL), 14LL, 643717713LL)));
        (v_b = LINE(108,f_md5_gg(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i), 20LL, -373897302LL)));
        (v_a = LINE(109,f_md5_gg(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 5LL), 5LL, -701558691LL)));
        (v_d = LINE(110,f_md5_gg(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 10LL), 9LL, 38016083LL)));
        (v_c = LINE(111,f_md5_gg(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 15LL), 14LL, -660478335LL)));
        (v_b = LINE(112,f_md5_gg(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 4LL), 20LL, -405537848LL)));
        (v_a = LINE(113,f_md5_gg(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 9LL), 5LL, 568446438LL)));
        (v_d = LINE(114,f_md5_gg(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 14LL), 9LL, -1019803690LL)));
        (v_c = LINE(115,f_md5_gg(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 3LL), 14LL, -187363961LL)));
        (v_b = LINE(116,f_md5_gg(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 8LL), 20LL, 1163531501LL)));
        (v_a = LINE(117,f_md5_gg(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 13LL), 5LL, -1444681467LL)));
        (v_d = LINE(118,f_md5_gg(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 2LL), 9LL, -51403784LL)));
        (v_c = LINE(119,f_md5_gg(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 7LL), 14LL, 1735328473LL)));
        (v_b = LINE(120,f_md5_gg(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 12LL), 20LL, -1926607734LL)));
        (v_a = LINE(122,f_md5_hh(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 5LL), 4LL, -378558LL)));
        (v_d = LINE(123,f_md5_hh(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 8LL), 11LL, -2022574463LL)));
        (v_c = LINE(124,f_md5_hh(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 11LL), 16LL, 1839030562LL)));
        (v_b = LINE(125,f_md5_hh(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 14LL), 23LL, -35309556LL)));
        (v_a = LINE(126,f_md5_hh(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 1LL), 4LL, -1530992060LL)));
        (v_d = LINE(127,f_md5_hh(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 4LL), 11LL, 1272893353LL)));
        (v_c = LINE(128,f_md5_hh(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 7LL), 16LL, -155497632LL)));
        (v_b = LINE(129,f_md5_hh(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 10LL), 23LL, -1094730640LL)));
        (v_a = LINE(130,f_md5_hh(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 13LL), 4LL, 681279174LL)));
        (v_d = LINE(131,f_md5_hh(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i), 11LL, -358537222LL)));
        (v_c = LINE(132,f_md5_hh(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 3LL), 16LL, -722521979LL)));
        (v_b = LINE(133,f_md5_hh(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 6LL), 23LL, 76029189LL)));
        (v_a = LINE(134,f_md5_hh(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 9LL), 4LL, -640364487LL)));
        (v_d = LINE(135,f_md5_hh(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 12LL), 11LL, -421815835LL)));
        (v_c = LINE(136,f_md5_hh(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 15LL), 16LL, 530742520LL)));
        (v_b = LINE(137,f_md5_hh(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 2LL), 23LL, -995338651LL)));
        (v_a = LINE(139,f_md5_ii(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i), 6LL, -198630844LL)));
        (v_d = LINE(140,f_md5_ii(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 7LL), 10LL, 1126891415LL)));
        (v_c = LINE(141,f_md5_ii(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 14LL), 15LL, -1416354905LL)));
        (v_b = LINE(142,f_md5_ii(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 5LL), 21LL, -57434055LL)));
        (v_a = LINE(143,f_md5_ii(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 12LL), 6LL, 1700485571LL)));
        (v_d = LINE(144,f_md5_ii(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 3LL), 10LL, -1894986606LL)));
        (v_c = LINE(145,f_md5_ii(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 10LL), 15LL, -1051523LL)));
        (v_b = LINE(146,f_md5_ii(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 1LL), 21LL, -2054922799LL)));
        (v_a = LINE(147,f_md5_ii(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 8LL), 6LL, 1873313359LL)));
        (v_d = LINE(148,f_md5_ii(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 15LL), 10LL, -30611744LL)));
        (v_c = LINE(149,f_md5_ii(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 6LL), 15LL, -1560198380LL)));
        (v_b = LINE(150,f_md5_ii(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 13LL), 21LL, 1309151649LL)));
        (v_a = LINE(151,f_md5_ii(v_a, v_b, v_c, v_d, v_x.rvalAt(v_i + 4LL), 6LL, -145523070LL)));
        (v_d = LINE(152,f_md5_ii(v_d, v_a, v_b, v_c, v_x.rvalAt(v_i + 11LL), 10LL, -1120210379LL)));
        (v_c = LINE(153,f_md5_ii(v_c, v_d, v_a, v_b, v_x.rvalAt(v_i + 2LL), 15LL, 718787259LL)));
        (v_b = LINE(154,f_md5_ii(v_b, v_c, v_d, v_a, v_x.rvalAt(v_i + 9LL), 21LL, -343485551LL)));
        (v_a = LINE(156,f_safe_add(v_a, v_olda)));
        (v_b = LINE(157,f_safe_add(v_b, v_oldb)));
        (v_c = LINE(158,f_safe_add(v_c, v_oldc)));
        (v_d = LINE(159,f_safe_add(v_d, v_oldd)));
      }
    }
  }
  return Array(ArrayInit(4).set(0, v_a).set(1, v_b).set(2, v_c).set(3, v_d).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 203 */
Primitive f_bit_rol(Primitive v_num, int64 v_cnt) {
  FUNCTION_INJECTION(bit_rol);
  return bitwise_or((toInt64(toInt64(v_num)) << v_cnt), (LINE(205,f_zerofill(v_num, (32LL - v_cnt)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 172 */
Primitive f_md5_ff(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t) {
  FUNCTION_INJECTION(md5_ff);
  return LINE(174,f_md5_cmn(bitwise_or((bitwise_and(v_b, v_c)), (bitwise_and((~v_b), v_d))), v_a, v_b, v_x, v_s, v_t));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 176 */
Primitive f_md5_gg(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t) {
  FUNCTION_INJECTION(md5_gg);
  return LINE(178,f_md5_cmn(bitwise_or((bitwise_and(v_b, v_d)), (bitwise_and(v_c, (~v_d)))), v_a, v_b, v_x, v_s, v_t));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 180 */
Primitive f_md5_hh(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t) {
  FUNCTION_INJECTION(md5_hh);
  return LINE(182,f_md5_cmn(bitwise_xor(bitwise_xor(v_b, v_c), v_d), v_a, v_b, v_x, v_s, v_t));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 212 */
Variant f_str2binl(CVarRef v_str) {
  FUNCTION_INJECTION(str2binl);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_chrsz __attribute__((__unused__)) = g->GV(chrsz);
  Variant v_bin;
  int64 v_mask = 0;
  Variant v_i;

  (v_bin = ScalarArrays::sa_[0]);
  (v_mask = (toInt64(1LL) << toInt64(gv_chrsz)) - 1LL);
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, LINE(219,x_strlen(toString(v_str))) * gv_chrsz); v_i += gv_chrsz) {
      LOOP_COUNTER_CHECK(4);
      {
        (silenceInc(), silenceDec(lval(v_bin.lvalAt(toInt64(toInt64(v_i)) >> 5LL)) |= toInt64((toInt64(bitwise_and(LINE(222,x_ord(toString(v_str.rvalAt(divide(v_i, gv_chrsz))))), v_mask)))) << (toInt64(modulo(toInt64(v_i), 32LL)))));
      }
    }
  }
  return v_bin;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 184 */
Primitive f_md5_ii(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d, CVarRef v_x, int64 v_s, int64 v_t) {
  FUNCTION_INJECTION(md5_ii);
  return LINE(186,f_md5_cmn(bitwise_xor(v_c, (bitwise_or(v_b, (~v_d)))), v_a, v_b, v_x, v_s, v_t));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 41 */
Variant f_zerofill(Variant v_a, Numeric v_b) {
  FUNCTION_INJECTION(zeroFill);
  Variant v_z;

  (v_z = LINE(43,x_hexdec(toString(80000000LL))));
  if (toBoolean(bitwise_and(v_z, v_a))) {
    (v_a = (toInt64(toInt64(v_a)) >> 1LL));
    v_a &= (~v_z);
    v_a |= 1073741824LL;
    (v_a = (toInt64(toInt64(v_a)) >> (toInt64(v_b - 1LL))));
  }
  else {
    (v_a = (toInt64(toInt64(v_a)) >> toInt64(v_b)));
  }
  return v_a;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php line 193 */
Primitive f_safe_add(CVarRef v_x, CVarRef v_y) {
  FUNCTION_INJECTION(safe_add);
  PlusOperand v_lsw = 0;
  int64 v_msw = 0;

  (v_lsw = (bitwise_and(v_x, 65535LL)) + (bitwise_and(v_y, 65535LL)));
  (v_msw = (toInt64(toInt64(v_x)) >> 16LL) + (toInt64(toInt64(v_y)) >> 16LL) + (toInt64(toInt64(v_lsw)) >> 16LL));
  return bitwise_or((toInt64(v_msw) << 16LL), (bitwise_and(v_lsw, 65535LL)));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_md5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_md5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_hexcase __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hexcase") : g->GV(hexcase);
  Variant &v_b64pad __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b64pad") : g->GV(b64pad);
  Variant &v_chrsz __attribute__((__unused__)) = (variables != gVariables) ? variables->get("chrsz") : g->GV(chrsz);
  Variant &v_plainText __attribute__((__unused__)) = (variables != gVariables) ? variables->get("plainText") : g->GV(plainText);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_md5Output __attribute__((__unused__)) = (variables != gVariables) ? variables->get("md5Output") : g->GV(md5Output);

  (v_hexcase = 0LL);
  (v_b64pad = "");
  (v_chrsz = 8LL);
  (v_plainText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ornare pharetra ligula, sed facilisis magna suscipit a. Etiam quis nisi erat, eu feugiat mi. Cras eu odio sem. Proin porttitor lacinia convallis. Duis nec magna massa. Aliquam facilisis, odio eu placerat feugiat, lorem odio accumsan lacus, at fermentum magna ipsum et velit. Vivamus lobortis commodo dolor nec vestibulum. Ut ac diam non sapien sagittis scelerisque. Praesent vitae consequat purus. Suspendisse bibendum varius sapien, quis malesuada lacus mollis bibendum. Sed cursus mattis massa ut pulvinar. Praesent sed felis massa. Duis suscipit ornare purus sit amet posuere. Nullam volutpat nulla a nisi dapibus blandit. Donec at blandit massa. Nam semper, urna nec eleifend faucibus, metus velit semper arcu, at ornare purus sem et diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec elementum, lectus eu laoreet congue, justo dolor blandit leo, eget commodo erat eros non augue. ");
  {
    LOOP_COUNTER(5);
    for ((v_i = 0LL); less(v_i, 4LL); v_i++) {
      LOOP_COUNTER_CHECK(5);
      {
        concat_assign(v_plainText, toString(v_plainText));
      }
    }
  }
  (v_md5Output = LINE(254,f_hex_md5(v_plainText)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
