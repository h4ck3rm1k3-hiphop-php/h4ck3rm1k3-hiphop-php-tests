
#ifndef __GENERATED_cls_nullexp_h__
#define __GENERATED_cls_nullexp_h__

#include <cls/biginteger.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1723 */
class c_nullexp : virtual public c_biginteger {
  BEGIN_CLASS_MAP(nullexp)
    PARENT_CLASS(biginteger)
  END_CLASS_MAP(nullexp)
  DECLARE_CLASS(nullexp, NullExp, biginteger)
  void init();
  public: Variant t_revert(CVarRef v_x);
  public: Variant t_convert(CVarRef v_x);
  public: void t_multo(CVarRef v_x, Variant v_y, Variant v_r);
  public: void t_sqrto(CVarRef v_x, Variant v_r);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_nullexp_h__
