
#ifndef __GENERATED_cls_barrett_h__
#define __GENERATED_cls_barrett_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1743 */
class c_barrett : virtual public ObjectData {
  BEGIN_CLASS_MAP(barrett)
  END_CLASS_MAP(barrett)
  DECLARE_CLASS(barrett, Barrett, ObjectData)
  void init();
  public: Variant m_q3;
  public: Variant m_r2;
  public: Variant m_m;
  public: Variant m_mu;
  public: void t_barrett(Variant v_m);
  public: ObjectData *create(Variant v_m);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_convert(CVarRef v_x);
  public: Variant t_revert(CVarRef v_x);
  public: void t_reduce(Variant v_x);
  public: void t_sqrto(CVarRef v_x, Variant v_r);
  public: void t_multo(CVarRef v_x, Variant v_y, Variant v_r);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_barrett_h__
