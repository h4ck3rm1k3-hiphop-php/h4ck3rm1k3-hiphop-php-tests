
#ifndef __GENERATED_cls_arcfour_h__
#define __GENERATED_cls_arcfour_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1572 */
class c_arcfour : virtual public ObjectData {
  BEGIN_CLASS_MAP(arcfour)
  END_CLASS_MAP(arcfour)
  DECLARE_CLASS(arcfour, Arcfour, ObjectData)
  void init();
  public: Variant m_i;
  public: Variant m_j;
  public: Array m_S;
  public: void t_init(CVarRef v_key);
  public: Variant t_next();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_arcfour_h__
