
#ifndef __GENERATED_cls_rsakey_h__
#define __GENERATED_cls_rsakey_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1959 */
class c_rsakey : virtual public ObjectData {
  BEGIN_CLASS_MAP(rsakey)
  END_CLASS_MAP(rsakey)
  DECLARE_CLASS(rsakey, RSAKey, ObjectData)
  void init();
  public: Variant m_n;
  public: Variant m_e;
  public: Variant m_d;
  public: Variant m_p;
  public: Variant m_q;
  public: Variant m_dmp1;
  public: Variant m_dmq1;
  public: Variant m_coeff;
  public: void t_setpublic(CVarRef v_N, CVarRef v_E);
  public: Variant t_dopublic(CVarRef v_x);
  public: Variant t_encrypt(CVarRef v_text);
  public: void t_setprivate(CVarRef v_N, CVarRef v_E, CVarRef v_D);
  public: void t_setprivateex(CVarRef v_N, CVarRef v_E, CVarRef v_D, CVarRef v_P, CVarRef v_Q, CVarRef v_DP, CVarRef v_DQ, CVarRef v_C);
  public: void t_generate(CVarRef v_B, CVarRef v_E);
  public: Variant t_doprivate(p_biginteger v_x);
  public: Variant t_decrypt(CVarRef v_ctext);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_rsakey_h__
