
#ifndef __GENERATED_cls_biginteger_h__
#define __GENERATED_cls_biginteger_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 158 */
class c_biginteger : virtual public ObjectData {
  BEGIN_CLASS_MAP(biginteger)
  END_CLASS_MAP(biginteger)
  DECLARE_CLASS(biginteger, BigInteger, ObjectData)
  void init();
  public: Variant m_array;
  public: Variant m_t;
  public: Variant m_s;
  public: Array m_lowprimes;
  public: Variant m_lplim;
  public: void t_biginteger(CVarRef v_a, CVarRef v_b = null_variant, CVarRef v_c = null_variant);
  public: ObjectData *create(CVarRef v_a, CVarRef v_b = null_variant, CVarRef v_c = null_variant);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_am(Variant v_i, CVarRef v_x, CVarRef v_w, Variant v_j, Variant v_c, Variant v_n);
  public: void t_copyto(Variant v_r);
  public: void t_fromint(CVarRef v_x);
  public: void t_fromstring(CVarRef v_s, CVarRef v_b);
  public: void t_clamp();
  public: Variant t_tostring(Variant v_b);
  public: Variant t_negate();
  public: Variant t_abs();
  public: Numeric t_compareto(CVarRef v_a);
  public: Numeric t_bitlength();
  public: void t_dlshiftto(CVarRef v_n, Variant v_r);
  public: void t_drshiftto(CVarRef v_n, Variant v_r);
  public: void t_lshiftto(CVarRef v_n, Variant v_r);
  public: void t_rshiftto(CVarRef v_n, Variant v_r);
  public: void t_subto(CVarRef v_a, Variant v_r);
  public: void t_multiplyto(CVarRef v_a, Variant v_r);
  public: void t_squareto(Variant v_r);
  public: void t_divremto(CVarRef v_m, Variant v_q, Variant v_r);
  public: Variant t_mod(Variant v_a);
  public: Numeric t_invdigit();
  public: bool t_iseven();
  public: Variant t_exp(CVarRef v_e, CVarRef v_z);
  public: Variant t_modpowint(CVarRef v_e, CVarRef v_m);
  public: p_biginteger t_bnpclone();
  public: Variant t_intvalue();
  public: Variant t_bytevalue();
  public: Variant t_shortvalue();
  public: double t_chunksize(CVarRef v_r);
  public: Variant t_signum();
  public: String t_toradix(Variant v_b);
  public: void t_fromradix(CVarRef v_s, Variant v_b);
  public: void t_fromnumber(CVarRef v_a, CVarRef v_b, CVarRef v_c);
  public: Variant t_tobytearray();
  public: bool t_equals(CVarRef v_a);
  public: Variant t_min(CVarRef v_a);
  public: Variant t_max(CVarRef v_a);
  public: void t_bitwiseto(CVarRef v_a, CVarRef v_op, Variant v_r);
  public: Primitive t_op_and(CVarRef v_x, CVarRef v_y);
  public: String t_band(CVarRef v_a);
  public: Primitive t_op_or(CVarRef v_x, CVarRef v_y);
  public: String t_bor(CVarRef v_a);
  public: Primitive t_op_xor(CVarRef v_x, CVarRef v_y);
  public: p_biginteger t_bxor(CVarRef v_a);
  public: Primitive t_op_andnot(CVarRef v_x, CVarRef v_y);
  public: String t_andnot(CVarRef v_a);
  public: p_biginteger t_not();
  public: p_biginteger t_shiftleft(CVarRef v_n);
  public: p_biginteger t_shiftright(CVarRef v_n);
  public: Numeric t_getlowestsetbit();
  public: int64 t_bitcount();
  public: bool t_testbit(Numeric v_n);
  public: Variant t_changebit(Variant v_n, CVarRef v_op);
  public: Variant t_setbit(CVarRef v_n);
  public: Variant t_clearbit(CVarRef v_n);
  public: Variant t_flipbit(CVarRef v_n);
  public: void t_addto(CVarRef v_a, p_biginteger v_r);
  public: p_biginteger t_add(CVarRef v_a);
  public: p_biginteger t_subtract(CVarRef v_a);
  public: p_biginteger t_multiply(CVarRef v_a);
  public: p_biginteger t_divide(CVarRef v_a);
  public: p_biginteger t_remainder(CVarRef v_a);
  public: Array t_divideandremainder(CVarRef v_a);
  public: void t_dmultiply(Numeric v_n);
  public: void t_daddoffset(CVarRef v_n, Variant v_w);
  public: Variant t_pow(CVarRef v_e);
  public: void t_multiplylowerto(CVarRef v_a, CVarRef v_n, Variant v_r);
  public: void t_multiplyupperto(CVarRef v_a, Variant v_n, Variant v_r);
  public: Variant t_modpow(CVarRef v_e, CVarRef v_m);
  public: Variant t_gcd(CVarRef v_a);
  public: Variant t_modint(CVarRef v_n);
  public: Variant t_modinverse(CVarRef v_m);
  public: Variant t_isprobableprime(Variant v_t);
  public: bool t_millerrabin(Variant v_t);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_biginteger_h__
