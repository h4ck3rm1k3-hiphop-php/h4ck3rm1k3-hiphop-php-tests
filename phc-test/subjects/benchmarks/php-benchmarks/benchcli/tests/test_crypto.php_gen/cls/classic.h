
#ifndef __GENERATED_cls_classic_h__
#define __GENERATED_cls_classic_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1604 */
class c_classic : virtual public ObjectData {
  BEGIN_CLASS_MAP(classic)
  END_CLASS_MAP(classic)
  DECLARE_CLASS(classic, Classic, ObjectData)
  void init();
  public: Variant m_m;
  public: void t_classic(CVarRef v_m);
  public: ObjectData *create(CVarRef v_m);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_convert(CVarRef v_x);
  public: Variant t_revert(CVarRef v_x);
  public: void t_reduce(Variant v_x);
  public: void t_multo(CVarRef v_x, Variant v_y, Variant v_r);
  public: void t_sqrto(CVarRef v_x, Variant v_r);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_classic_h__
