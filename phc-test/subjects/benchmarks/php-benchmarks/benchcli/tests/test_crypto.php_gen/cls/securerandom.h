
#ifndef __GENERATED_cls_securerandom_h__
#define __GENERATED_cls_securerandom_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1877 */
class c_securerandom : virtual public ObjectData {
  BEGIN_CLASS_MAP(securerandom)
  END_CLASS_MAP(securerandom)
  DECLARE_CLASS(securerandom, SecureRandom, ObjectData)
  void init();
  public: Variant m_rng;
  public: void t_securerandom(CVarRef v_rng);
  public: ObjectData *create(CVarRef v_rng);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_nextbytes(Variant v_ba);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_securerandom_h__
