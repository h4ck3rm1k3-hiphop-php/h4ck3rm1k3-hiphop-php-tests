
#ifndef __GENERATED_cls_montgomery_h__
#define __GENERATED_cls_montgomery_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1639 */
class c_montgomery : virtual public ObjectData {
  BEGIN_CLASS_MAP(montgomery)
  END_CLASS_MAP(montgomery)
  DECLARE_CLASS(montgomery, Montgomery, ObjectData)
  void init();
  public: Variant m_m;
  public: Variant m_mp;
  public: String m_mp1;
  public: Variant m_mph;
  public: Variant m_um;
  public: Variant m_mt2;
  public: void t_montgomery(CVarRef v_m);
  public: ObjectData *create(CVarRef v_m);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_convert(CVarRef v_x);
  public: Variant t_revert(CVarRef v_x);
  public: void t_reduce(Variant v_x);
  public: void t_sqrto(CVarRef v_x, Variant v_r);
  public: void t_multo(CVarRef v_x, Variant v_y, Variant v_r);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_montgomery_h__
