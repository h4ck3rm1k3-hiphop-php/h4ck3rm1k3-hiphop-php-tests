
#ifndef __GENERATED_cls_rng_h__
#define __GENERATED_cls_rng_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1819 */
class c_rng : virtual public ObjectData {
  BEGIN_CLASS_MAP(rng)
  END_CLASS_MAP(rng)
  DECLARE_CLASS(rng, rng, ObjectData)
  void init();
  public: Variant m_state;
  public: Variant m_pool;
  public: Variant m_pptr;
  public: int64 m_psize;
  public: void t_init();
  public: void t_seed_int(int64 v_x);
  public: void t_seed_time();
  public: Variant t_get_byte();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_rng_h__
