
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      HASH_RETURN(0x3BD183B0C5295081LL, g->GV(rngc),
                  rngc);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 8:
      HASH_RETURN(0x5E77905BACA8A9C8LL, g->GV(BigIntegerOne),
                  BigIntegerOne);
      HASH_RETURN(0x0D0BDB510B1EB008LL, g->GV(BI_RC),
                  BI_RC);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 10:
      HASH_RETURN(0x76AC0070B088944ALL, g->GV(dmq1Value),
                  dmq1Value);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      HASH_RETURN(0x028C9FAAEC5CF8CELL, g->GV(dmp1Value),
                  dmp1Value);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 18:
      HASH_RETURN(0x486D63DC5980D9D2LL, g->GV(vv),
                  vv);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 24:
      HASH_RETURN(0x3CA26D2DC6297A98LL, g->GV(nValue),
                  nValue);
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 27:
      HASH_RETURN(0x6A455502A98D3CDBLL, g->GV(pValue),
                  pValue);
      break;
    case 29:
      HASH_RETURN(0x1D985DF335561F9DLL, g->GV(dValue),
                  dValue);
      HASH_RETURN(0x4D26D167066BB11DLL, g->GV(TEXT),
                  TEXT);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 37:
      HASH_RETURN(0x4D4F807C55E8E0A5LL, g->GV(encrypted),
                  encrypted);
      break;
    case 42:
      HASH_RETURN(0x1E7E2FFC87683D6ALL, g->GV(rr),
                  rr);
      HASH_RETURN(0x315336B3AF846EAALL, g->GV(coeffValue),
                  coeffValue);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 52:
      HASH_RETURN(0x5461302AF4064BB4LL, g->GV(eValue),
                  eValue);
      break;
    case 54:
      HASH_RETURN(0x45AFC3012C80F236LL, g->GV(BI_RM),
                  BI_RM);
      break;
    case 57:
      HASH_RETURN(0x1B39E60A4DCD10B9LL, g->GV(BigIntegerZero),
                  BigIntegerZero);
      HASH_RETURN(0x5A030C5FDD893039LL, g->GV(qValue),
                  qValue);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
