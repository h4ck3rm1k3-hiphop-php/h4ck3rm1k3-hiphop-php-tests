
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_op_andnot;
extern const int64 k_BI_F1;
extern const int64 k_BI_F2;
extern const int64 k_BI_DB;
extern const StaticString k_op_and;
extern const int64 k_BI_DM;
extern const StaticString k_op_xor;
extern const int64 k_BI_DV;
extern const int64 k_BI_FP;
extern const StaticString k_p;
extern const StaticString k_r;
extern const StaticString k_x;
extern const StaticString k_op_or;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 31) {
    case 1:
      HASH_RETURN(0x7C0C59F7C6D4B601LL, k_op_xor, op_xor);
      break;
    case 2:
      HASH_RETURN(0x4237321A192D0E42LL, g->k_BI_FV, BI_FV);
      HASH_RETURN(0x785E4693E5CC82E2LL, k_BI_F2, BI_F2);
      break;
    case 6:
      HASH_RETURN(0x77F632A4E34F1526LL, k_p, p);
      break;
    case 11:
      HASH_RETURN(0x514930F2D4ED24EBLL, k_BI_FP, BI_FP);
      break;
    case 13:
      HASH_RETURN(0x653EB62CD6F1410DLL, k_BI_DB, BI_DB);
      break;
    case 14:
      HASH_RETURN(0x1B03C11F3B2D4B0ELL, k_op_and, op_and);
      break;
    case 15:
      HASH_RETURN(0x15AD2947A3DE61EFLL, k_op_andnot, op_andnot);
      break;
    case 17:
      HASH_RETURN(0x4A3DEAD1EAB6E851LL, k_BI_F1, BI_F1);
      break;
    case 22:
      HASH_RETURN(0x38F8526CC46AD056LL, k_BI_DM, BI_DM);
      HASH_RETURN(0x6ECE84440D42ECF6LL, k_r, r);
      HASH_RETURN(0x04BFC205E59FA416LL, k_x, x);
      break;
    case 24:
      HASH_RETURN(0x57F5A065454A0338LL, k_op_or, op_or);
      break;
    case 29:
      HASH_RETURN(0x46A64D3A6D52891DLL, k_BI_DV, BI_DV);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
