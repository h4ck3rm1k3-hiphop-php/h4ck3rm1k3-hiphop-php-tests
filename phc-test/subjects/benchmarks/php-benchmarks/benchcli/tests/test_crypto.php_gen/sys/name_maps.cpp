
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "arcfour", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "barrett", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "biginteger", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "classic", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "montgomery", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "nullexp", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "rng", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "rsakey", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "securerandom", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "byte2hex", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "cbit", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "decrypt", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "encrypt", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "int2char", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "intat", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "lbit", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "linebrk", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "nbi", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "nbits", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "nbv", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "parsebigint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "pkcs1pad2", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "pkcs1unpad2", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "prng_newstate", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "setlength", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  "urs", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
