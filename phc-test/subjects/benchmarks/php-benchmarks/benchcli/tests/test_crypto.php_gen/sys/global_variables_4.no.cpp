
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "BigIntegerZero",
    "BigIntegerOne",
    "BI_RM",
    "BI_RC",
    "rr",
    "vv",
    "rngc",
    "nValue",
    "eValue",
    "dValue",
    "pValue",
    "qValue",
    "dmp1Value",
    "dmq1Value",
    "coeffValue",
    "TEXT",
    "encrypted",
    "i",
  };
  if (idx >= 0 && idx < 30) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(BigIntegerZero);
    case 13: return GV(BigIntegerOne);
    case 14: return GV(BI_RM);
    case 15: return GV(BI_RC);
    case 16: return GV(rr);
    case 17: return GV(vv);
    case 18: return GV(rngc);
    case 19: return GV(nValue);
    case 20: return GV(eValue);
    case 21: return GV(dValue);
    case 22: return GV(pValue);
    case 23: return GV(qValue);
    case 24: return GV(dmp1Value);
    case 25: return GV(dmq1Value);
    case 26: return GV(coeffValue);
    case 27: return GV(TEXT);
    case 28: return GV(encrypted);
    case 29: return GV(i);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
