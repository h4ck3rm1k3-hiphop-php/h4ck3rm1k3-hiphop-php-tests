
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_montgomery(CArrRef params, bool init = true);
Variant cw_montgomery$os_get(const char *s);
Variant &cw_montgomery$os_lval(const char *s);
Variant cw_montgomery$os_constant(const char *s);
Variant cw_montgomery$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_securerandom(CArrRef params, bool init = true);
Variant cw_securerandom$os_get(const char *s);
Variant &cw_securerandom$os_lval(const char *s);
Variant cw_securerandom$os_constant(const char *s);
Variant cw_securerandom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_rng(CArrRef params, bool init = true);
Variant cw_rng$os_get(const char *s);
Variant &cw_rng$os_lval(const char *s);
Variant cw_rng$os_constant(const char *s);
Variant cw_rng$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_rsakey(CArrRef params, bool init = true);
Variant cw_rsakey$os_get(const char *s);
Variant &cw_rsakey$os_lval(const char *s);
Variant cw_rsakey$os_constant(const char *s);
Variant cw_rsakey$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_barrett(CArrRef params, bool init = true);
Variant cw_barrett$os_get(const char *s);
Variant &cw_barrett$os_lval(const char *s);
Variant cw_barrett$os_constant(const char *s);
Variant cw_barrett$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_nullexp(CArrRef params, bool init = true);
Variant cw_nullexp$os_get(const char *s);
Variant &cw_nullexp$os_lval(const char *s);
Variant cw_nullexp$os_constant(const char *s);
Variant cw_nullexp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_classic(CArrRef params, bool init = true);
Variant cw_classic$os_get(const char *s);
Variant &cw_classic$os_lval(const char *s);
Variant cw_classic$os_constant(const char *s);
Variant cw_classic$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_biginteger(CArrRef params, bool init = true);
Variant cw_biginteger$os_get(const char *s);
Variant &cw_biginteger$os_lval(const char *s);
Variant cw_biginteger$os_constant(const char *s);
Variant cw_biginteger$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_arcfour(CArrRef params, bool init = true);
Variant cw_arcfour$os_get(const char *s);
Variant &cw_arcfour$os_lval(const char *s);
Variant cw_arcfour$os_constant(const char *s);
Variant cw_arcfour$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 12:
      HASH_CREATE_OBJECT(0x4B3C4D0895FF7A4CLL, biginteger);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x2EC6F9CE7E08C7CFLL, arcfour);
      break;
    case 16:
      HASH_CREATE_OBJECT(0x0E4A91FD9A25E410LL, montgomery);
      break;
    case 18:
      HASH_CREATE_OBJECT(0x394FC9ECBC015312LL, nullexp);
      break;
    case 19:
      HASH_CREATE_OBJECT(0x6F97E8118AE21E73LL, rng);
      break;
    case 21:
      HASH_CREATE_OBJECT(0x793FC3232EF035F5LL, rsakey);
      HASH_CREATE_OBJECT(0x7606D2352DACF1F5LL, barrett);
      break;
    case 29:
      HASH_CREATE_OBJECT(0x62DE68E4E33F503DLL, securerandom);
      break;
    case 30:
      HASH_CREATE_OBJECT(0x767AC746EEE93B7ELL, classic);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x4B3C4D0895FF7A4CLL, biginteger);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x2EC6F9CE7E08C7CFLL, arcfour);
      break;
    case 16:
      HASH_INVOKE_STATIC_METHOD(0x0E4A91FD9A25E410LL, montgomery);
      break;
    case 18:
      HASH_INVOKE_STATIC_METHOD(0x394FC9ECBC015312LL, nullexp);
      break;
    case 19:
      HASH_INVOKE_STATIC_METHOD(0x6F97E8118AE21E73LL, rng);
      break;
    case 21:
      HASH_INVOKE_STATIC_METHOD(0x793FC3232EF035F5LL, rsakey);
      HASH_INVOKE_STATIC_METHOD(0x7606D2352DACF1F5LL, barrett);
      break;
    case 29:
      HASH_INVOKE_STATIC_METHOD(0x62DE68E4E33F503DLL, securerandom);
      break;
    case 30:
      HASH_INVOKE_STATIC_METHOD(0x767AC746EEE93B7ELL, classic);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 12:
      HASH_GET_STATIC_PROPERTY(0x4B3C4D0895FF7A4CLL, biginteger);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x2EC6F9CE7E08C7CFLL, arcfour);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY(0x0E4A91FD9A25E410LL, montgomery);
      break;
    case 18:
      HASH_GET_STATIC_PROPERTY(0x394FC9ECBC015312LL, nullexp);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY(0x6F97E8118AE21E73LL, rng);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY(0x793FC3232EF035F5LL, rsakey);
      HASH_GET_STATIC_PROPERTY(0x7606D2352DACF1F5LL, barrett);
      break;
    case 29:
      HASH_GET_STATIC_PROPERTY(0x62DE68E4E33F503DLL, securerandom);
      break;
    case 30:
      HASH_GET_STATIC_PROPERTY(0x767AC746EEE93B7ELL, classic);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x4B3C4D0895FF7A4CLL, biginteger);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x2EC6F9CE7E08C7CFLL, arcfour);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY_LV(0x0E4A91FD9A25E410LL, montgomery);
      break;
    case 18:
      HASH_GET_STATIC_PROPERTY_LV(0x394FC9ECBC015312LL, nullexp);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY_LV(0x6F97E8118AE21E73LL, rng);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY_LV(0x793FC3232EF035F5LL, rsakey);
      HASH_GET_STATIC_PROPERTY_LV(0x7606D2352DACF1F5LL, barrett);
      break;
    case 29:
      HASH_GET_STATIC_PROPERTY_LV(0x62DE68E4E33F503DLL, securerandom);
      break;
    case 30:
      HASH_GET_STATIC_PROPERTY_LV(0x767AC746EEE93B7ELL, classic);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 12:
      HASH_GET_CLASS_CONSTANT(0x4B3C4D0895FF7A4CLL, biginteger);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x2EC6F9CE7E08C7CFLL, arcfour);
      break;
    case 16:
      HASH_GET_CLASS_CONSTANT(0x0E4A91FD9A25E410LL, montgomery);
      break;
    case 18:
      HASH_GET_CLASS_CONSTANT(0x394FC9ECBC015312LL, nullexp);
      break;
    case 19:
      HASH_GET_CLASS_CONSTANT(0x6F97E8118AE21E73LL, rng);
      break;
    case 21:
      HASH_GET_CLASS_CONSTANT(0x793FC3232EF035F5LL, rsakey);
      HASH_GET_CLASS_CONSTANT(0x7606D2352DACF1F5LL, barrett);
      break;
    case 29:
      HASH_GET_CLASS_CONSTANT(0x62DE68E4E33F503DLL, securerandom);
      break;
    case 30:
      HASH_GET_CLASS_CONSTANT(0x767AC746EEE93B7ELL, classic);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
