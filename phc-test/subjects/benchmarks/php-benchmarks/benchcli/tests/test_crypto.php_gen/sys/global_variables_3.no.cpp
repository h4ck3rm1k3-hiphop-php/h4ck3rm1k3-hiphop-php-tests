
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x3BD183B0C5295081LL, rngc, 18);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 8:
      HASH_INDEX(0x5E77905BACA8A9C8LL, BigIntegerOne, 13);
      HASH_INDEX(0x0D0BDB510B1EB008LL, BI_RC, 15);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 10:
      HASH_INDEX(0x76AC0070B088944ALL, dmq1Value, 25);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      HASH_INDEX(0x028C9FAAEC5CF8CELL, dmp1Value, 24);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 18:
      HASH_INDEX(0x486D63DC5980D9D2LL, vv, 17);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 24:
      HASH_INDEX(0x3CA26D2DC6297A98LL, nValue, 19);
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 29);
      break;
    case 27:
      HASH_INDEX(0x6A455502A98D3CDBLL, pValue, 22);
      break;
    case 29:
      HASH_INDEX(0x1D985DF335561F9DLL, dValue, 21);
      HASH_INDEX(0x4D26D167066BB11DLL, TEXT, 27);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 37:
      HASH_INDEX(0x4D4F807C55E8E0A5LL, encrypted, 28);
      break;
    case 42:
      HASH_INDEX(0x1E7E2FFC87683D6ALL, rr, 16);
      HASH_INDEX(0x315336B3AF846EAALL, coeffValue, 26);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 52:
      HASH_INDEX(0x5461302AF4064BB4LL, eValue, 20);
      break;
    case 54:
      HASH_INDEX(0x45AFC3012C80F236LL, BI_RM, 14);
      break;
    case 57:
      HASH_INDEX(0x1B39E60A4DCD10B9LL, BigIntegerZero, 12);
      HASH_INDEX(0x5A030C5FDD893039LL, qValue, 23);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 30) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
