
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.fw.h>

// Declarations
#include <cls/montgomery.h>
#include <cls/securerandom.h>
#include <cls/rng.h>
#include <cls/rsakey.h>
#include <cls/barrett.h>
#include <cls/nullexp.h>
#include <cls/classic.h>
#include <cls/biginteger.h>
#include <cls/arcfour.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_byte2hex(CVarRef v_b);
int64 f_cbit(Variant v_x);
Variant f_pkcs1unpad2(CVarRef v_d, int64 v_n);
p_biginteger f_parsebigint(CVarRef v_str, int64 v_r);
p_biginteger f_nbi();
Variant f_pkcs1pad2(CVarRef v_s, int64 v_n);
p_biginteger f_nbv(Numeric v_i);
Variant f_intat(CVarRef v_s, int64 v_i);
String f_linebrk(CVarRef v_s, CVarRef v_n);
void f_encrypt();
Variant f_int2char(CVarRef v_n);
Variant f_urs(Variant v_a, int64 v_b);
void f_decrypt();
int64 f_nbits(Variant v_x);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_lbit(Variant v_x);
p_arcfour f_prng_newstate();
void f_setlength(Variant v_array, int64 v_length);
Object co_montgomery(CArrRef params, bool init = true);
Object co_securerandom(CArrRef params, bool init = true);
Object co_rng(CArrRef params, bool init = true);
Object co_rsakey(CArrRef params, bool init = true);
Object co_barrett(CArrRef params, bool init = true);
Object co_nullexp(CArrRef params, bool init = true);
Object co_classic(CArrRef params, bool init = true);
Object co_biginteger(CArrRef params, bool init = true);
Object co_arcfour(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_h__
