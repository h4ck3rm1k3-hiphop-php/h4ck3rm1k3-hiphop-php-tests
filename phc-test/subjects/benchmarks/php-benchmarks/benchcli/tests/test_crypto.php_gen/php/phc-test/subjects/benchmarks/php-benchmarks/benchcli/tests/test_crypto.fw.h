
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_op_andnot;
extern const int64 k_BI_F1;
extern const int64 k_BI_F2;
extern const int64 k_BI_DB;
extern const StaticString k_op_and;
extern const int64 k_BI_DM;
extern const StaticString k_op_xor;
extern const int64 k_BI_DV;
extern const int64 k_BI_FP;
extern const StaticString k_p;
extern const StaticString k_r;
extern const StaticString k_x;
extern const StaticString k_op_or;


// 2. Classes
FORWARD_DECLARE_CLASS(montgomery)
FORWARD_DECLARE_CLASS(securerandom)
FORWARD_DECLARE_CLASS(rng)
FORWARD_DECLARE_CLASS(rsakey)
FORWARD_DECLARE_CLASS(barrett)
FORWARD_DECLARE_CLASS(nullexp)
FORWARD_DECLARE_CLASS(classic)
FORWARD_DECLARE_CLASS(biginteger)
FORWARD_DECLARE_CLASS(arcfour)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_fw_h__
