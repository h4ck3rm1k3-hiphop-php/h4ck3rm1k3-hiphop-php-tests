
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_op_andnot = "op_andnot";
const int64 k_BI_F1 = 24LL;
const int64 k_BI_F2 = 4LL;
const int64 k_BI_DB = 28LL;
const StaticString k_op_and = "op_and";
const int64 k_BI_DM = 268435455LL;
const StaticString k_op_xor = "op_xor";
const int64 k_BI_DV = 268435456LL;
const int64 k_BI_FP = 52LL;
const StaticString k_p = "p";
const StaticString k_r = "r";
const StaticString k_x = "x";
const StaticString k_op_or = "op_or";

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1639 */
Variant c_montgomery::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_montgomery::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_montgomery::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("m", m_m.isReferenced() ? ref(m_m) : m_m));
  props.push_back(NEW(ArrayElement)("mp", m_mp.isReferenced() ? ref(m_mp) : m_mp));
  props.push_back(NEW(ArrayElement)("mp1", m_mp1));
  props.push_back(NEW(ArrayElement)("mph", m_mph.isReferenced() ? ref(m_mph) : m_mph));
  props.push_back(NEW(ArrayElement)("um", m_um.isReferenced() ? ref(m_um) : m_um));
  props.push_back(NEW(ArrayElement)("mt2", m_mt2.isReferenced() ? ref(m_mt2) : m_mt2));
  c_ObjectData::o_get(props);
}
bool c_montgomery::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_EXISTS_STRING(0x2DABF7BF8C5105D2LL, mp1, 3);
      break;
    case 5:
      HASH_EXISTS_STRING(0x6B3D06D27AC28B65LL, mph, 3);
      break;
    case 7:
      HASH_EXISTS_STRING(0x0B0F1EE943469FE7LL, mt2, 3);
      break;
    case 11:
      HASH_EXISTS_STRING(0x7A0C10E3609EA04BLL, m, 1);
      HASH_EXISTS_STRING(0x2C857EE5BEB47F3BLL, um, 2);
      break;
    case 13:
      HASH_EXISTS_STRING(0x2479A7120216246DLL, mp, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_montgomery::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x2DABF7BF8C5105D2LL, m_mp1,
                         mp1, 3);
      break;
    case 5:
      HASH_RETURN_STRING(0x6B3D06D27AC28B65LL, m_mph,
                         mph, 3);
      break;
    case 7:
      HASH_RETURN_STRING(0x0B0F1EE943469FE7LL, m_mt2,
                         mt2, 3);
      break;
    case 11:
      HASH_RETURN_STRING(0x7A0C10E3609EA04BLL, m_m,
                         m, 1);
      HASH_RETURN_STRING(0x2C857EE5BEB47F3BLL, m_um,
                         um, 2);
      break;
    case 13:
      HASH_RETURN_STRING(0x2479A7120216246DLL, m_mp,
                         mp, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_montgomery::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_SET_STRING(0x2DABF7BF8C5105D2LL, m_mp1,
                      mp1, 3);
      break;
    case 5:
      HASH_SET_STRING(0x6B3D06D27AC28B65LL, m_mph,
                      mph, 3);
      break;
    case 7:
      HASH_SET_STRING(0x0B0F1EE943469FE7LL, m_mt2,
                      mt2, 3);
      break;
    case 11:
      HASH_SET_STRING(0x7A0C10E3609EA04BLL, m_m,
                      m, 1);
      HASH_SET_STRING(0x2C857EE5BEB47F3BLL, m_um,
                      um, 2);
      break;
    case 13:
      HASH_SET_STRING(0x2479A7120216246DLL, m_mp,
                      mp, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_montgomery::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_RETURN_STRING(0x6B3D06D27AC28B65LL, m_mph,
                         mph, 3);
      break;
    case 7:
      HASH_RETURN_STRING(0x0B0F1EE943469FE7LL, m_mt2,
                         mt2, 3);
      break;
    case 11:
      HASH_RETURN_STRING(0x7A0C10E3609EA04BLL, m_m,
                         m, 1);
      HASH_RETURN_STRING(0x2C857EE5BEB47F3BLL, m_um,
                         um, 2);
      break;
    case 13:
      HASH_RETURN_STRING(0x2479A7120216246DLL, m_mp,
                         mp, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_montgomery::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(montgomery)
ObjectData *c_montgomery::create(CVarRef v_m) {
  init();
  t_montgomery(v_m);
  return this;
}
ObjectData *c_montgomery::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_montgomery::cloneImpl() {
  c_montgomery *obj = NEW(c_montgomery)();
  cloneSet(obj);
  return obj;
}
void c_montgomery::cloneSet(c_montgomery *clone) {
  clone->m_m = m_m.isReferenced() ? ref(m_m) : m_m;
  clone->m_mp = m_mp.isReferenced() ? ref(m_mp) : m_mp;
  clone->m_mp1 = m_mp1;
  clone->m_mph = m_mph.isReferenced() ? ref(m_mph) : m_mph;
  clone->m_um = m_um.isReferenced() ? ref(m_um) : m_um;
  clone->m_mt2 = m_mt2.isReferenced() ? ref(m_mt2) : m_mt2;
  ObjectData::cloneSet(clone);
}
Variant c_montgomery::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_montgomery::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(a0, a1, a2), null);
      }
      break;
    case 3:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_montgomery::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_montgomery$os_get(const char *s) {
  return c_montgomery::os_get(s, -1);
}
Variant &cw_montgomery$os_lval(const char *s) {
  return c_montgomery::os_lval(s, -1);
}
Variant cw_montgomery$os_constant(const char *s) {
  return c_montgomery::os_constant(s);
}
Variant cw_montgomery$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_montgomery::os_invoke(c, s, params, -1, fatal);
}
void c_montgomery::init() {
  m_m = null;
  m_mp = null;
  m_mp1 = null;
  m_mph = null;
  m_um = null;
  m_mt2 = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1648 */
void c_montgomery::t_montgomery(CVarRef v_m) {
  INSTANCE_METHOD_INJECTION(Montgomery, Montgomery::Montgomery);
  bool oldInCtor = gasInCtor(true);
  (m_m = v_m);
  (m_mp = LINE(1651,toObject(v_m)->o_invoke_few_args("invDigit", 0x4D1CA98174E44193LL, 0)));
  (o_lval("mpl", 0x2833A2BE41B718EBLL) = bitwise_and(m_mp, 32767LL));
  (m_mph = toInt64(toInt64(m_mp)) >> 15LL);
  (m_um = 8191LL);
  (m_mt2 = 2LL * toObject(v_m).o_get("t", 0x11C9D2391242AEBCLL));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1659 */
Variant c_montgomery::t_convert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(Montgomery, Montgomery::convert);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_r;

  (v_r = ((Object)(LINE(1661,f_nbi()))));
  (assignCallTemp(eo_0, toObject(LINE(1662,toObject(v_x)->o_invoke_few_args("abs", 0x0DF945F12533F0A1LL, 0)))),eo_0.o_invoke_few_args("dlShiftTo", 0x622C0CD1ED7971A7LL, 2, m_m.o_lval("t", 0x11C9D2391242AEBCLL), v_r));
  LINE(1664,v_r.o_invoke_few_args("divRemTo", 0x404D2534F5F9778ALL, 3, m_m, null, v_r));
  if (less(toObject(v_x).o_get("s", 0x0BC2CDE4BBFC9C10LL), 0LL) && more(LINE(1665,v_r.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerZero))), 0LL)) {
    LINE(1666,m_m.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_r, v_r));
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1672 */
Variant c_montgomery::t_revert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(Montgomery, Montgomery::revert);
  Variant v_r;

  (v_r = ((Object)(LINE(1674,f_nbi()))));
  LINE(1675,toObject(v_x)->o_invoke_few_args("copyTo", 0x0D9AF4B657C6CDCBLL, 1, v_r));
  LINE(1677,t_reduce(v_r));
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1682 */
void c_montgomery::t_reduce(Variant v_x) {
  INSTANCE_METHOD_INJECTION(Montgomery, Montgomery::reduce);
  Variant v_x_array;
  Variant v_i;
  Variant v_j;
  Variant v_u0;

  (v_x_array = ref(lval(v_x.o_lval("array", 0x063D35168C04B960LL))));
  LOOP_COUNTER(1);
  {
    while (not_more(v_x.o_get("t", 0x11C9D2391242AEBCLL), m_mt2)) {
      LOOP_COUNTER_CHECK(1);
      {
        v_x_array.set(lval(v_x.o_lval("t", 0x11C9D2391242AEBCLL))++, (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, m_m.o_get("t", 0x11C9D2391242AEBCLL)); ++v_i) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_j = bitwise_and(v_x_array.rvalAt(v_i), 32767LL));
        (v_u0 = bitwise_and((v_j * o_get("mpl", 0x2833A2BE41B718EBLL) + (toInt64((toInt64(bitwise_and((v_j * m_mph + (toInt64(toInt64(v_x_array.rvalAt(v_i))) >> 15LL) * o_get("mpl", 0x2833A2BE41B718EBLL)), m_um)))) << 15LL)), 268435455LL /* BI_DM */));
        (v_j = v_i + m_m.o_get("t", 0x11C9D2391242AEBCLL));
        lval(v_x_array.lvalAt(v_j)) += LINE(1695,m_m.o_invoke_few_args("am", 0x49B5ED5DCFBF58FDLL, 6, 0LL, v_u0, v_x, v_i, 0LL, m_m.o_lval("t", 0x11C9D2391242AEBCLL)));
        LOOP_COUNTER(3);
        {
          while (not_less(v_x_array.rvalAt(v_j), 268435456LL /* BI_DV */)) {
            LOOP_COUNTER_CHECK(3);
            {
              lval(v_x_array.lvalAt(v_j)) -= 268435456LL /* BI_DV */;
              lval(v_x_array.lvalAt(++v_j))++;
            }
          }
        }
      }
    }
  }
  LINE(1702,v_x.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
  LINE(1703,v_x.o_invoke_few_args("drShiftTo", 0x47DB44AC0B6E049ELL, 2, m_m.o_lval("t", 0x11C9D2391242AEBCLL), v_x));
  if (not_less(LINE(1704,v_x.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, m_m)), 0LL)) {
    LINE(1705,v_x.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, m_m, v_x));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1710 */
void c_montgomery::t_sqrto(CVarRef v_x, Variant v_r) {
  INSTANCE_METHOD_INJECTION(Montgomery, Montgomery::sqrTo);
  LINE(1711,toObject(v_x)->o_invoke_few_args("squareTo", 0x61AF801E09D83247LL, 1, v_r));
  LINE(1712,t_reduce(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1716 */
void c_montgomery::t_multo(CVarRef v_x, Variant v_y, Variant v_r) {
  INSTANCE_METHOD_INJECTION(Montgomery, Montgomery::mulTo);
  LINE(1718,toObject(v_x)->o_invoke_few_args("multiplyTo", 0x7054CED73B51F0DDLL, 2, v_y, v_r));
  LINE(1719,t_reduce(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1877 */
Variant c_securerandom::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_securerandom::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_securerandom::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("rng", m_rng.isReferenced() ? ref(m_rng) : m_rng));
  c_ObjectData::o_get(props);
}
bool c_securerandom::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x354AB76845B794E0LL, rng, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_securerandom::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x354AB76845B794E0LL, m_rng,
                         rng, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_securerandom::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x354AB76845B794E0LL, m_rng,
                      rng, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_securerandom::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x354AB76845B794E0LL, m_rng,
                         rng, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_securerandom::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(securerandom)
ObjectData *c_securerandom::create(CVarRef v_rng) {
  init();
  t_securerandom(v_rng);
  return this;
}
ObjectData *c_securerandom::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_securerandom::cloneImpl() {
  c_securerandom *obj = NEW(c_securerandom)();
  cloneSet(obj);
  return obj;
}
void c_securerandom::cloneSet(c_securerandom *clone) {
  clone->m_rng = m_rng.isReferenced() ? ref(m_rng) : m_rng;
  ObjectData::cloneSet(clone);
}
Variant c_securerandom::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x039723D7E38DED4ALL, nextbytes) {
        return (t_nextbytes(ref(const_cast<Array&>(params).lvalAt(0))), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_securerandom::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x039723D7E38DED4ALL, nextbytes) {
        return (t_nextbytes(ref(a0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_securerandom::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_securerandom$os_get(const char *s) {
  return c_securerandom::os_get(s, -1);
}
Variant &cw_securerandom$os_lval(const char *s) {
  return c_securerandom::os_lval(s, -1);
}
Variant cw_securerandom$os_constant(const char *s) {
  return c_securerandom::os_constant(s);
}
Variant cw_securerandom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_securerandom::os_invoke(c, s, params, -1, fatal);
}
void c_securerandom::init() {
  m_rng = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1880 */
void c_securerandom::t_securerandom(CVarRef v_rng) {
  INSTANCE_METHOD_INJECTION(SecureRandom, SecureRandom::SecureRandom);
  bool oldInCtor = gasInCtor(true);
  (m_rng = v_rng);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1884 */
void c_securerandom::t_nextbytes(Variant v_ba) {
  INSTANCE_METHOD_INJECTION(SecureRandom, SecureRandom::nextBytes);
  int64 v_i = 0;

  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, LINE(1885,x_count(v_ba))); ++v_i) {
      LOOP_COUNTER_CHECK(4);
      {
        v_ba.set(v_i, (LINE(1886,m_rng.o_invoke_few_args("get_byte", 0x4141B50ABDF940D2LL, 0))));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1819 */
Variant c_rng::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_rng::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_rng::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("state", m_state.isReferenced() ? ref(m_state) : m_state));
  props.push_back(NEW(ArrayElement)("pool", m_pool.isReferenced() ? ref(m_pool) : m_pool));
  props.push_back(NEW(ArrayElement)("pptr", m_pptr.isReferenced() ? ref(m_pptr) : m_pptr));
  props.push_back(NEW(ArrayElement)("psize", m_psize));
  c_ObjectData::o_get(props);
}
bool c_rng::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x712D36E026A6A0B9LL, pool, 4);
      break;
    case 2:
      HASH_EXISTS_STRING(0x5C84C80672BA5E22LL, state, 5);
      break;
    case 4:
      HASH_EXISTS_STRING(0x58BA027C56572D0CLL, psize, 5);
      break;
    case 7:
      HASH_EXISTS_STRING(0x35A32E49E56E8D9FLL, pptr, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_rng::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x712D36E026A6A0B9LL, m_pool,
                         pool, 4);
      break;
    case 2:
      HASH_RETURN_STRING(0x5C84C80672BA5E22LL, m_state,
                         state, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x58BA027C56572D0CLL, m_psize,
                         psize, 5);
      break;
    case 7:
      HASH_RETURN_STRING(0x35A32E49E56E8D9FLL, m_pptr,
                         pptr, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_rng::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x712D36E026A6A0B9LL, m_pool,
                      pool, 4);
      break;
    case 2:
      HASH_SET_STRING(0x5C84C80672BA5E22LL, m_state,
                      state, 5);
      break;
    case 4:
      HASH_SET_STRING(0x58BA027C56572D0CLL, m_psize,
                      psize, 5);
      break;
    case 7:
      HASH_SET_STRING(0x35A32E49E56E8D9FLL, m_pptr,
                      pptr, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_rng::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x712D36E026A6A0B9LL, m_pool,
                         pool, 4);
      break;
    case 2:
      HASH_RETURN_STRING(0x5C84C80672BA5E22LL, m_state,
                         state, 5);
      break;
    case 7:
      HASH_RETURN_STRING(0x35A32E49E56E8D9FLL, m_pptr,
                         pptr, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_rng::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(rng)
ObjectData *c_rng::cloneImpl() {
  c_rng *obj = NEW(c_rng)();
  cloneSet(obj);
  return obj;
}
void c_rng::cloneSet(c_rng *clone) {
  clone->m_state = m_state.isReferenced() ? ref(m_state) : m_state;
  clone->m_pool = m_pool.isReferenced() ? ref(m_pool) : m_pool;
  clone->m_pptr = m_pptr.isReferenced() ? ref(m_pptr) : m_pptr;
  clone->m_psize = m_psize;
  ObjectData::cloneSet(clone);
}
Variant c_rng::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4141B50ABDF940D2LL, get_byte) {
        return (t_get_byte());
      }
      break;
    case 3:
      HASH_GUARD(0x614F97E16B435A03LL, init) {
        return (t_init(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_rng::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x4141B50ABDF940D2LL, get_byte) {
        return (t_get_byte());
      }
      break;
    case 3:
      HASH_GUARD(0x614F97E16B435A03LL, init) {
        return (t_init(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_rng::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_rng$os_get(const char *s) {
  return c_rng::os_get(s, -1);
}
Variant &cw_rng$os_lval(const char *s) {
  return c_rng::os_lval(s, -1);
}
Variant cw_rng$os_constant(const char *s) {
  return c_rng::os_constant(s);
}
Variant cw_rng$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_rng::os_invoke(c, s, params, -1, fatal);
}
void c_rng::init() {
  m_state = null;
  m_pool = null;
  m_pptr = null;
  m_psize = 256LL;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1825 */
void c_rng::t_init() {
  INSTANCE_METHOD_INJECTION(rng, rng::init);
  double v_t = 0.0;

  if (LINE(1827,x_is_null(m_pool))) {
    (m_pool = ScalarArrays::sa_[0]);
    (m_pptr = 0LL);
    LOOP_COUNTER(5);
    {
      while (less(m_pptr, m_psize)) {
        LOOP_COUNTER_CHECK(5);
        {
          (v_t = LINE(1831,x_floor(toDouble(x_rand(0LL, 65536LL)))));
          m_pool.set(m_pptr++, (LINE(1832,f_urs(v_t, 8LL))));
          m_pool.set(m_pptr++, (bitwise_and(v_t, 255LL)));
        }
      }
    }
    (m_pptr = 0LL);
    LINE(1837,t_seed_time());
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1840 */
void c_rng::t_seed_int(int64 v_x) {
  INSTANCE_METHOD_INJECTION(rng, rng::seed_int);
  lval(m_pool.lvalAt(m_pptr++)) ^= bitwise_and(v_x, 255LL);
  lval(m_pool.lvalAt(m_pptr++)) ^= bitwise_and((toInt64(v_x) >> 8LL), 255LL);
  lval(m_pool.lvalAt(m_pptr++)) ^= bitwise_and((toInt64(v_x) >> 16LL), 255LL);
  lval(m_pool.lvalAt(m_pptr++)) ^= bitwise_and((toInt64(v_x) >> 24LL), 255LL);
  if (not_less(m_pptr, m_psize)) {
    m_pptr -= m_psize;
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1850 */
void c_rng::t_seed_time() {
  INSTANCE_METHOD_INJECTION(rng, rng::seed_time);
  LINE(1852,t_seed_int(1122926989487LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1856 */
Variant c_rng::t_get_byte() {
  INSTANCE_METHOD_INJECTION(rng, rng::get_byte);
  if (LINE(1858,x_is_null(m_state))) {
    LINE(1860,t_seed_time());
    (m_state = ((Object)(LINE(1861,f_prng_newstate()))));
    LINE(1862,m_state.o_invoke_few_args("init", 0x614F97E16B435A03LL, 1, m_pool));
    {
      LOOP_COUNTER(6);
      for ((m_pptr = 0LL); less(m_pptr, LINE(1864,x_count(m_pool))); ++m_pptr) {
        LOOP_COUNTER_CHECK(6);
        {
          m_pool.set(m_pptr, (0LL));
        }
      }
    }
    (m_pptr = 0LL);
  }
  return LINE(1872,m_state.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1959 */
Variant c_rsakey::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_rsakey::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_rsakey::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("n", m_n.isReferenced() ? ref(m_n) : m_n));
  props.push_back(NEW(ArrayElement)("e", m_e.isReferenced() ? ref(m_e) : m_e));
  props.push_back(NEW(ArrayElement)("d", m_d.isReferenced() ? ref(m_d) : m_d));
  props.push_back(NEW(ArrayElement)("p", m_p.isReferenced() ? ref(m_p) : m_p));
  props.push_back(NEW(ArrayElement)("q", m_q.isReferenced() ? ref(m_q) : m_q));
  props.push_back(NEW(ArrayElement)("dmp1", m_dmp1.isReferenced() ? ref(m_dmp1) : m_dmp1));
  props.push_back(NEW(ArrayElement)("dmq1", m_dmq1.isReferenced() ? ref(m_dmq1) : m_dmq1));
  props.push_back(NEW(ArrayElement)("coeff", m_coeff.isReferenced() ? ref(m_coeff) : m_coeff));
  c_ObjectData::o_get(props);
}
bool c_rsakey::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_EXISTS_STRING(0x0F4E2ED3B2697451LL, dmp1, 4);
      break;
    case 2:
      HASH_EXISTS_STRING(0x67B0DD2622623452LL, dmq1, 4);
      break;
    case 3:
      HASH_EXISTS_STRING(0x05CAFC91A9B37763LL, q, 1);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x77F632A4E34F1526LL, p, 1);
      break;
    case 9:
      HASH_EXISTS_STRING(0x117B8667E4662809LL, n, 1);
      break;
    case 11:
      HASH_EXISTS_STRING(0x61B161496B7EA7EBLL, e, 1);
      break;
    case 13:
      HASH_EXISTS_STRING(0x39A89D5FAB568DFDLL, coeff, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_rsakey::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x0F4E2ED3B2697451LL, m_dmp1,
                         dmp1, 4);
      break;
    case 2:
      HASH_RETURN_STRING(0x67B0DD2622623452LL, m_dmq1,
                         dmq1, 4);
      break;
    case 3:
      HASH_RETURN_STRING(0x05CAFC91A9B37763LL, m_q,
                         q, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x77F632A4E34F1526LL, m_p,
                         p, 1);
      break;
    case 9:
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      break;
    case 13:
      HASH_RETURN_STRING(0x39A89D5FAB568DFDLL, m_coeff,
                         coeff, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_rsakey::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_SET_STRING(0x0F4E2ED3B2697451LL, m_dmp1,
                      dmp1, 4);
      break;
    case 2:
      HASH_SET_STRING(0x67B0DD2622623452LL, m_dmq1,
                      dmq1, 4);
      break;
    case 3:
      HASH_SET_STRING(0x05CAFC91A9B37763LL, m_q,
                      q, 1);
      break;
    case 4:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    case 6:
      HASH_SET_STRING(0x77F632A4E34F1526LL, m_p,
                      p, 1);
      break;
    case 9:
      HASH_SET_STRING(0x117B8667E4662809LL, m_n,
                      n, 1);
      break;
    case 11:
      HASH_SET_STRING(0x61B161496B7EA7EBLL, m_e,
                      e, 1);
      break;
    case 13:
      HASH_SET_STRING(0x39A89D5FAB568DFDLL, m_coeff,
                      coeff, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_rsakey::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x0F4E2ED3B2697451LL, m_dmp1,
                         dmp1, 4);
      break;
    case 2:
      HASH_RETURN_STRING(0x67B0DD2622623452LL, m_dmq1,
                         dmq1, 4);
      break;
    case 3:
      HASH_RETURN_STRING(0x05CAFC91A9B37763LL, m_q,
                         q, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x77F632A4E34F1526LL, m_p,
                         p, 1);
      break;
    case 9:
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    case 11:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      break;
    case 13:
      HASH_RETURN_STRING(0x39A89D5FAB568DFDLL, m_coeff,
                         coeff, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_rsakey::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(rsakey)
ObjectData *c_rsakey::cloneImpl() {
  c_rsakey *obj = NEW(c_rsakey)();
  cloneSet(obj);
  return obj;
}
void c_rsakey::cloneSet(c_rsakey *clone) {
  clone->m_n = m_n.isReferenced() ? ref(m_n) : m_n;
  clone->m_e = m_e.isReferenced() ? ref(m_e) : m_e;
  clone->m_d = m_d.isReferenced() ? ref(m_d) : m_d;
  clone->m_p = m_p.isReferenced() ? ref(m_p) : m_p;
  clone->m_q = m_q.isReferenced() ? ref(m_q) : m_q;
  clone->m_dmp1 = m_dmp1.isReferenced() ? ref(m_dmp1) : m_dmp1;
  clone->m_dmq1 = m_dmq1.isReferenced() ? ref(m_dmq1) : m_dmq1;
  clone->m_coeff = m_coeff.isReferenced() ? ref(m_coeff) : m_coeff;
  ObjectData::cloneSet(clone);
}
Variant c_rsakey::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_rsakey::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_rsakey::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_rsakey$os_get(const char *s) {
  return c_rsakey::os_get(s, -1);
}
Variant &cw_rsakey$os_lval(const char *s) {
  return c_rsakey::os_lval(s, -1);
}
Variant cw_rsakey$os_constant(const char *s) {
  return c_rsakey::os_constant(s);
}
Variant cw_rsakey$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_rsakey::os_invoke(c, s, params, -1, fatal);
}
void c_rsakey::init() {
  m_n = null;
  m_e = 0LL;
  m_d = null;
  m_p = null;
  m_q = null;
  m_dmp1 = null;
  m_dmq1 = null;
  m_coeff = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1970 */
void c_rsakey::t_setpublic(CVarRef v_N, CVarRef v_E) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::setPublic);
  if (!(LINE(1971,x_is_null(v_N))) && !(x_is_null(v_E)) && more(x_count(v_N), 0LL) && more(x_count(v_E), 0LL)) {
    (m_n = ((Object)(LINE(1972,f_parsebigint(v_N, 16LL)))));
    (m_e = LINE(1973,x_intval(v_E, 16LL)));
  }
  else {
    echo("Invalid RSA public key");
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1980 */
Variant c_rsakey::t_dopublic(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::doPublic);
  return LINE(1981,toObject(v_x)->o_invoke_few_args("modPowInt", 0x200163CFF3908AE1LL, 2, m_e, m_n));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1985 */
Variant c_rsakey::t_encrypt(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::encrypt);
  Variant eo_0;
  Variant eo_1;
  Variant v_m;
  Variant v_c;
  Variant v_h;

  (v_m = LINE(1986,(assignCallTemp(eo_0, v_text),assignCallTemp(eo_1, toInt64((toInt64(m_n.o_invoke_few_args("bitLength", 0x3FD30C5DB7F40944LL, 0) + 7LL))) >> 3LL),f_pkcs1pad2(eo_0, eo_1))));
  if (LINE(1987,x_is_null(v_m))) {
    return null;
  }
  (v_c = LINE(1990,t_dopublic(v_m)));
  if (LINE(1991,x_is_null(v_c))) {
    return null;
  }
  (v_h = LINE(1996,v_c.o_invoke_few_args("toString", 0x6271FDA592D5EF53LL, 1, 16LL)));
  if (equal((bitwise_and(LINE(1998,x_strlen(toString(v_h))), 1LL)), 0LL)) {
    return v_h;
  }
  else {
    return concat("0", toString(v_h));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 2006 */
void c_rsakey::t_setprivate(CVarRef v_N, CVarRef v_E, CVarRef v_D) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::setPrivate);
  if (!(LINE(2007,x_is_null(v_N))) && x_is_null(v_E) && more(x_count(v_N), 0LL) && more(x_count(v_E), 0LL)) {
    (m_n = ((Object)(LINE(2008,f_parsebigint(v_N, 16LL)))));
    (m_e = LINE(2009,x_intval(v_E, 16LL)));
    (m_d = ((Object)(LINE(2010,f_parsebigint(v_D, 16LL)))));
  }
  else {
    echo("Invalid RSA private key");
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 2017 */
void c_rsakey::t_setprivateex(CVarRef v_N, CVarRef v_E, CVarRef v_D, CVarRef v_P, CVarRef v_Q, CVarRef v_DP, CVarRef v_DQ, CVarRef v_C) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::setPrivateEx);
  if (!(LINE(2018,x_is_null(v_N))) && !(x_is_null(v_E)) && more(x_count(v_N), 0LL) && more(x_count(v_E), 0LL)) {
    (m_n = ((Object)(LINE(2019,f_parsebigint(v_N, 16LL)))));
    (m_e = LINE(2020,x_intval(v_E, 16LL)));
    (m_d = ((Object)(LINE(2021,f_parsebigint(v_D, 16LL)))));
    (m_p = ((Object)(LINE(2022,f_parsebigint(v_P, 16LL)))));
    (m_q = ((Object)(LINE(2023,f_parsebigint(v_Q, 16LL)))));
    (m_dmp1 = ((Object)(LINE(2024,f_parsebigint(v_DP, 16LL)))));
    (m_dmq1 = ((Object)(LINE(2025,f_parsebigint(v_DQ, 16LL)))));
    (m_coeff = ((Object)(LINE(2026,f_parsebigint(v_C, 16LL)))));
  }
  else {
    LINE(2028,invoke_failed("alert", Array(ArrayInit(1).set(0, "Invalid RSA private key").create()), 0x00000000689ADC7DLL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 2032 */
void c_rsakey::t_generate(CVarRef v_B, CVarRef v_E) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::generate);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  p_securerandom v_rng;
  int64 v_qs = 0;
  Variant v_ee;
  Variant v_t;
  Variant v_p1;
  Variant v_q1;
  Variant v_phi;

  ((Object)((v_rng = ((Object)(LINE(2033,p_securerandom(p_securerandom(NEWOBJ(c_securerandom)())->create(g->GV(rngc)))))))));
  (v_qs = toInt64(toInt64(v_B)) >> 1LL);
  (m_e = LINE(2035,x_intval(v_E, 16LL)));
  (v_ee = ((Object)(LINE(2036,p_biginteger(p_biginteger(NEWOBJ(c_biginteger)())->create(v_E, 16LL))))));
  {
    LOOP_COUNTER(7);
    for (; ; ) {
      LOOP_COUNTER_CHECK(7);
      {
        {
          LOOP_COUNTER(8);
          for (; ; ) {
            LOOP_COUNTER_CHECK(8);
            {
              (m_p = ((Object)(LINE(2039,p_biginteger(p_biginteger(NEWOBJ(c_biginteger)())->create(v_B - v_qs, 1LL, ((Object)(v_rng))))))));
              if (equal((assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, toObject(LINE(2040,m_p.o_invoke_few_args("subtract", 0x5FB593FF9B781E19LL, 1, g->GV(BigIntegerOne))))),eo_1.o_invoke_few_args("gcd", 0x6659447207E861A1LL, 1, v_ee)))),eo_0.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerOne))), 0LL) && toBoolean(m_p.o_invoke_few_args("isProbablePrime", 0x759A6AA43D2247ECLL, 1, 10LL))) break;
            }
          }
        }
        {
          LOOP_COUNTER(9);
          for (; ; ) {
            LOOP_COUNTER_CHECK(9);
            {
              (m_q = ((Object)(LINE(2043,p_biginteger(p_biginteger(NEWOBJ(c_biginteger)())->create(v_qs, 1LL, ((Object)(v_rng))))))));
              if (equal((assignCallTemp(eo_2, toObject((assignCallTemp(eo_3, toObject(LINE(2044,m_q.o_invoke_few_args("subtract", 0x5FB593FF9B781E19LL, 1, g->GV(BigIntegerOne))))),eo_3.o_invoke_few_args("gcd", 0x6659447207E861A1LL, 1, v_ee)))),eo_2.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerOne))), 0LL) && toBoolean(m_q.o_invoke_few_args("isProbablePrime", 0x759A6AA43D2247ECLL, 1, 10LL))) break;
            }
          }
        }
        if (not_more(LINE(2046,m_p.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, m_q)), 0LL)) {
          (v_t = m_p);
          (m_p = m_q);
          (m_q = v_t);
        }
        (v_p1 = LINE(2051,m_p.o_invoke_few_args("subtract", 0x5FB593FF9B781E19LL, 1, g->GV(BigIntegerOne))));
        (v_q1 = LINE(2052,m_q.o_invoke_few_args("subtract", 0x5FB593FF9B781E19LL, 1, g->GV(BigIntegerOne))));
        (v_phi = LINE(2053,v_p1.o_invoke_few_args("multiply", 0x4D38F3151376E610LL, 1, v_q1)));
        if (equal((assignCallTemp(eo_4, toObject(LINE(2054,v_phi.o_invoke_few_args("gcd", 0x6659447207E861A1LL, 1, v_ee)))),eo_4.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerOne))), 0LL)) {
          (m_n = LINE(2055,m_p.o_invoke_few_args("multiply", 0x4D38F3151376E610LL, 1, m_q)));
          (m_d = LINE(2056,v_ee.o_invoke_few_args("modInverse", 0x5BC133D9B54F0933LL, 1, v_phi)));
          (m_dmp1 = LINE(2057,m_d.o_invoke_few_args("mod", 0x71F28DB4D137ADEALL, 1, v_p1)));
          (m_dmq1 = LINE(2058,m_d.o_invoke_few_args("mod", 0x71F28DB4D137ADEALL, 1, v_q1)));
          (m_coeff = LINE(2059,m_q.o_invoke_few_args("modInverse", 0x5BC133D9B54F0933LL, 1, m_p)));
          break;
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 2066 */
Variant c_rsakey::t_doprivate(p_biginteger v_x) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::doPrivate);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_xp;
  Variant v_xq;

  if (LINE(2067,x_is_null(m_p)) || x_is_null(m_q)) {
    return LINE(2068,v_x->t_modpow(m_d, m_n));
  }
  (v_xp = (assignCallTemp(eo_0, toObject(LINE(2072,v_x->t_mod(m_p)))),eo_0.o_invoke_few_args("modPow", 0x5E5702D2FB8BD430LL, 2, m_dmp1, m_p)));
  (v_xq = (assignCallTemp(eo_1, toObject(LINE(2073,v_x->t_mod(m_q)))),eo_1.o_invoke_few_args("modPow", 0x5E5702D2FB8BD430LL, 2, m_dmq1, m_q)));
  LOOP_COUNTER(10);
  {
    while (less(LINE(2075,v_xp.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_xq)), 0LL)) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_xp = LINE(2076,v_xp.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, m_p)));
      }
    }
  }
  return (assignCallTemp(eo_2, toObject((assignCallTemp(eo_3, toObject((assignCallTemp(eo_4, toObject((assignCallTemp(eo_5, toObject(LINE(2079,v_xp.o_invoke_few_args("subtract", 0x5FB593FF9B781E19LL, 1, v_xq)))),eo_5.o_invoke_few_args("multiply", 0x4D38F3151376E610LL, 1, m_coeff)))),eo_4.o_invoke_few_args("mod", 0x71F28DB4D137ADEALL, 1, m_p)))),eo_3.o_invoke_few_args("multiply", 0x4D38F3151376E610LL, 1, m_q)))),eo_2.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_xq));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 2083 */
Variant c_rsakey::t_decrypt(CVarRef v_ctext) {
  INSTANCE_METHOD_INJECTION(RSAKey, RSAKey::decrypt);
  Variant eo_0;
  Variant eo_1;
  p_biginteger v_c;
  Variant v_m;

  ((Object)((v_c = ((Object)(LINE(2084,f_parsebigint(v_ctext, 16LL)))))));
  (v_m = LINE(2085,t_doprivate(((Object)(v_c)))));
  if (LINE(2086,x_is_null(v_m))) {
    return null;
  }
  return LINE(2089,(assignCallTemp(eo_0, v_m),assignCallTemp(eo_1, toInt64((toInt64(m_n.o_invoke_few_args("bitLength", 0x3FD30C5DB7F40944LL, 0) + 7LL))) >> 3LL),f_pkcs1unpad2(eo_0, eo_1)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1743 */
Variant c_barrett::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_barrett::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_barrett::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("q3", m_q3.isReferenced() ? ref(m_q3) : m_q3));
  props.push_back(NEW(ArrayElement)("r2", m_r2.isReferenced() ? ref(m_r2) : m_r2));
  props.push_back(NEW(ArrayElement)("m", m_m.isReferenced() ? ref(m_m) : m_m));
  props.push_back(NEW(ArrayElement)("mu", m_mu.isReferenced() ? ref(m_mu) : m_mu));
  c_ObjectData::o_get(props);
}
bool c_barrett::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x41D0CEE6DA79ACE1LL, r2, 2);
      break;
    case 3:
      HASH_EXISTS_STRING(0x7A0C10E3609EA04BLL, m, 1);
      break;
    case 4:
      HASH_EXISTS_STRING(0x70474B28D43B80F4LL, mu, 2);
      break;
    case 5:
      HASH_EXISTS_STRING(0x087A1402E96A67BDLL, q3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_barrett::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x41D0CEE6DA79ACE1LL, m_r2,
                         r2, 2);
      break;
    case 3:
      HASH_RETURN_STRING(0x7A0C10E3609EA04BLL, m_m,
                         m, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x70474B28D43B80F4LL, m_mu,
                         mu, 2);
      break;
    case 5:
      HASH_RETURN_STRING(0x087A1402E96A67BDLL, m_q3,
                         q3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_barrett::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x41D0CEE6DA79ACE1LL, m_r2,
                      r2, 2);
      break;
    case 3:
      HASH_SET_STRING(0x7A0C10E3609EA04BLL, m_m,
                      m, 1);
      break;
    case 4:
      HASH_SET_STRING(0x70474B28D43B80F4LL, m_mu,
                      mu, 2);
      break;
    case 5:
      HASH_SET_STRING(0x087A1402E96A67BDLL, m_q3,
                      q3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_barrett::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x41D0CEE6DA79ACE1LL, m_r2,
                         r2, 2);
      break;
    case 3:
      HASH_RETURN_STRING(0x7A0C10E3609EA04BLL, m_m,
                         m, 1);
      break;
    case 4:
      HASH_RETURN_STRING(0x70474B28D43B80F4LL, m_mu,
                         mu, 2);
      break;
    case 5:
      HASH_RETURN_STRING(0x087A1402E96A67BDLL, m_q3,
                         q3, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_barrett::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(barrett)
ObjectData *c_barrett::create(Variant v_m) {
  init();
  t_barrett(v_m);
  return this;
}
ObjectData *c_barrett::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_barrett::cloneImpl() {
  c_barrett *obj = NEW(c_barrett)();
  cloneSet(obj);
  return obj;
}
void c_barrett::cloneSet(c_barrett *clone) {
  clone->m_q3 = m_q3.isReferenced() ? ref(m_q3) : m_q3;
  clone->m_r2 = m_r2.isReferenced() ? ref(m_r2) : m_r2;
  clone->m_m = m_m.isReferenced() ? ref(m_m) : m_m;
  clone->m_mu = m_mu.isReferenced() ? ref(m_mu) : m_mu;
  ObjectData::cloneSet(clone);
}
Variant c_barrett::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_barrett::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(a0, a1, a2), null);
      }
      break;
    case 3:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_barrett::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_barrett$os_get(const char *s) {
  return c_barrett::os_get(s, -1);
}
Variant &cw_barrett$os_lval(const char *s) {
  return c_barrett::os_lval(s, -1);
}
Variant cw_barrett$os_constant(const char *s) {
  return c_barrett::os_constant(s);
}
Variant cw_barrett$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_barrett::os_invoke(c, s, params, -1, fatal);
}
void c_barrett::init() {
  m_q3 = null;
  m_r2 = null;
  m_m = null;
  m_mu = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1749 */
void c_barrett::t_barrett(Variant v_m) {
  INSTANCE_METHOD_INJECTION(Barrett, Barrett::Barrett);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  (m_r2 = ((Object)(LINE(1752,f_nbi()))));
  (m_q3 = ((Object)(LINE(1753,f_nbi()))));
  LINE(1754,g->GV(BigIntegerOne).o_invoke_few_args("dlShiftTo", 0x622C0CD1ED7971A7LL, 2, 2LL * v_m.o_get("t", 0x11C9D2391242AEBCLL), m_r2));
  (m_mu = LINE(1755,m_r2.o_invoke_few_args("divide", 0x3FA60FDDD196E470LL, 1, v_m)));
  (m_m = v_m);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1758 */
Variant c_barrett::t_convert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(Barrett, Barrett::convert);
  Variant v_r;

  if (less(toObject(v_x).o_get("s", 0x0BC2CDE4BBFC9C10LL), 0LL) || more(toObject(v_x).o_get("t", 0x11C9D2391242AEBCLL), 2LL * m_m.o_get("t", 0x11C9D2391242AEBCLL))) {
    return LINE(1761,toObject(v_x)->o_invoke_few_args("mod", 0x71F28DB4D137ADEALL, 1, m_m));
  }
  else if (less(LINE(1762,toObject(v_x)->o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, m_m)), 0LL)) {
    return k_x;
  }
  else {
    (v_r = ((Object)(LINE(1765,f_nbi()))));
    LINE(1766,toObject(v_x)->o_invoke_few_args("copyTo", 0x0D9AF4B657C6CDCBLL, 1, v_r));
    LINE(1767,t_reduce(v_r));
    return v_r;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1772 */
Variant c_barrett::t_revert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(Barrett, Barrett::revert);
  return v_x;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1778 */
void c_barrett::t_reduce(Variant v_x) {
  INSTANCE_METHOD_INJECTION(Barrett, Barrett::reduce);
  LINE(1779,v_x.o_invoke_few_args("drShiftTo", 0x47DB44AC0B6E049ELL, 2, m_m.o_get("t", 0x11C9D2391242AEBCLL) - 1LL, m_r2));
  if (more(v_x.o_get("t", 0x11C9D2391242AEBCLL), m_m.o_get("t", 0x11C9D2391242AEBCLL) + 1LL)) {
    (v_x.o_lval("t", 0x11C9D2391242AEBCLL) = m_m.o_get("t", 0x11C9D2391242AEBCLL) + 1LL);
    LINE(1783,v_x.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
  }
  LINE(1785,m_mu.o_invoke_few_args("multiplyUpperTo", 0x6EE369CD0EC2128CLL, 3, m_r2, m_m.o_get("t", 0x11C9D2391242AEBCLL) + 1LL, m_q3));
  LINE(1786,m_m.o_invoke_few_args("multiplyLowerTo", 0x6A786C55DECF931DLL, 3, m_q3, m_m.o_get("t", 0x11C9D2391242AEBCLL) + 1LL, m_r2));
  LOOP_COUNTER(11);
  {
    while (less(LINE(1787,v_x.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, m_r2)), 0LL)) {
      LOOP_COUNTER_CHECK(11);
      {
        LINE(1788,v_x.o_invoke_few_args("dAddOffset", 0x62D0FAFC642EAEE6LL, 2, 1LL, m_m.o_get("t", 0x11C9D2391242AEBCLL) + 1LL));
      }
    }
  }
  LINE(1790,v_x.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, m_r2, v_x));
  LOOP_COUNTER(12);
  {
    while (not_less(LINE(1791,v_x.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, m_m)), 0LL)) {
      LOOP_COUNTER_CHECK(12);
      {
        LINE(1792,v_x.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, m_m, v_x));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1797 */
void c_barrett::t_sqrto(CVarRef v_x, Variant v_r) {
  INSTANCE_METHOD_INJECTION(Barrett, Barrett::sqrTo);
  LINE(1799,toObject(v_x)->o_invoke_few_args("squareTo", 0x61AF801E09D83247LL, 1, v_r));
  LINE(1800,t_reduce(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1804 */
void c_barrett::t_multo(CVarRef v_x, Variant v_y, Variant v_r) {
  INSTANCE_METHOD_INJECTION(Barrett, Barrett::mulTo);
  LINE(1806,toObject(v_x)->o_invoke_few_args("multiplyTo", 0x7054CED73B51F0DDLL, 2, v_y, v_r));
  LINE(1807,t_reduce(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1723 */
Variant c_nullexp::os_get(const char *s, int64 hash) {
  return c_biginteger::os_get(s, hash);
}
Variant &c_nullexp::os_lval(const char *s, int64 hash) {
  return c_biginteger::os_lval(s, hash);
}
void c_nullexp::o_get(ArrayElementVec &props) const {
  c_biginteger::o_get(props);
}
bool c_nullexp::o_exists(CStrRef s, int64 hash) const {
  return c_biginteger::o_exists(s, hash);
}
Variant c_nullexp::o_get(CStrRef s, int64 hash) {
  return c_biginteger::o_get(s, hash);
}
Variant c_nullexp::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_biginteger::o_set(s, hash, v, forInit);
}
Variant &c_nullexp::o_lval(CStrRef s, int64 hash) {
  return c_biginteger::o_lval(s, hash);
}
Variant c_nullexp::os_constant(const char *s) {
  return c_biginteger::os_constant(s);
}
IMPLEMENT_CLASS(nullexp)
ObjectData *c_nullexp::cloneImpl() {
  c_nullexp *obj = NEW(c_nullexp)();
  cloneSet(obj);
  return obj;
}
void c_nullexp::cloneSet(c_nullexp *clone) {
  c_biginteger::cloneSet(clone);
}
Variant c_nullexp::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 2:
      HASH_GUARD(0x3D03B239A1E9E902LL, tobytearray) {
        return (t_tobytearray());
      }
      break;
    case 10:
      HASH_GUARD(0x404D2534F5F9778ALL, divremto) {
        return (t_divremto(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 11:
      HASH_GUARD(0x3B2D21057B10BC8BLL, iseven) {
        return (t_iseven());
      }
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x6EE369CD0EC2128CLL, multiplyupperto) {
        return (t_multiplyupperto(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0BFA87DE111EEA8DLL, subto) {
        return (t_subto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 16:
      HASH_GUARD(0x4D38F3151376E610LL, multiply) {
        return (t_multiply(params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x4D1CA98174E44193LL, invdigit) {
        return (t_invdigit());
      }
      break;
    case 25:
      HASH_GUARD(0x5FB593FF9B781E19LL, subtract) {
        return (t_subtract(params.rvalAt(0)));
      }
      break;
    case 29:
      HASH_GUARD(0x6A786C55DECF931DLL, multiplylowerto) {
        return (t_multiplylowerto(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 30:
      HASH_GUARD(0x47DB44AC0B6E049ELL, drshiftto) {
        return (t_drshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 32:
      HASH_GUARD(0x29C480721C97FCA0LL, rshiftto) {
        return (t_rshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 33:
      HASH_GUARD(0x6659447207E861A1LL, gcd) {
        return (t_gcd(params.rvalAt(0)));
      }
      HASH_GUARD(0x0DF945F12533F0A1LL, abs) {
        return (t_abs());
      }
      break;
    case 36:
      HASH_GUARD(0x04CF69C0F8C389A4LL, negate) {
        return (t_negate());
      }
      break;
    case 39:
      HASH_GUARD(0x622C0CD1ED7971A7LL, dlshiftto) {
        return (t_dlshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 45:
      HASH_GUARD(0x461BBC73FE1061ADLL, compareto) {
        return (t_compareto(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x5E5702D2FB8BD430LL, modpow) {
        return (t_modpow(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 51:
      HASH_GUARD(0x5BC133D9B54F0933LL, modinverse) {
        return (t_modinverse(params.rvalAt(0)));
      }
      HASH_GUARD(0x36924875887B5FB3LL, getlowestsetbit) {
        return (t_getlowestsetbit());
      }
      break;
    case 59:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(params.rvalAt(0)));
      }
      break;
    case 68:
      HASH_GUARD(0x3FD30C5DB7F40944LL, bitlength) {
        return (t_bitlength());
      }
      break;
    case 71:
      HASH_GUARD(0x793171D3BD2BCBC7LL, millerrabin) {
        return (t_millerrabin(params.rvalAt(0)));
      }
      HASH_GUARD(0x61AF801E09D83247LL, squareto) {
        return (t_squareto(params.rvalAt(0)), null);
      }
      break;
    case 74:
      HASH_GUARD(0x7B1983912339EFCALL, shiftleft) {
        return (t_shiftleft(params.rvalAt(0)));
      }
      break;
    case 75:
      HASH_GUARD(0x0D9AF4B657C6CDCBLL, copyto) {
        return (t_copyto(params.rvalAt(0)), null);
      }
      break;
    case 76:
      HASH_GUARD(0x755E1F76CC554ACCLL, lshiftto) {
        return (t_lshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 80:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 83:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (t_tostring(params.rvalAt(0)));
      }
      break;
    case 87:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 93:
      HASH_GUARD(0x7054CED73B51F0DDLL, multiplyto) {
        return (t_multiplyto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 97:
      HASH_GUARD(0x608C35ACFE8AE4E1LL, fromint) {
        return (t_fromint(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x200163CFF3908AE1LL, modpowint) {
        return (t_modpowint(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 99:
      HASH_GUARD(0x0D241492DD854D63LL, signum) {
        return (t_signum());
      }
      break;
    case 102:
      HASH_GUARD(0x62D0FAFC642EAEE6LL, daddoffset) {
        return (t_daddoffset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 104:
      HASH_GUARD(0x432DC7FA5F22DFE8LL, shiftright) {
        return (t_shiftright(params.rvalAt(0)));
      }
      break;
    case 106:
      HASH_GUARD(0x71F28DB4D137ADEALL, mod) {
        return (t_mod(params.rvalAt(0)));
      }
      break;
    case 108:
      HASH_GUARD(0x759A6AA43D2247ECLL, isprobableprime) {
        return (t_isprobableprime(params.rvalAt(0)));
      }
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(params.rvalAt(0)));
      }
      break;
    case 109:
      HASH_GUARD(0x55023DA5CE74B86DLL, modint) {
        return (t_modint(params.rvalAt(0)));
      }
      break;
    case 112:
      HASH_GUARD(0x3FA60FDDD196E470LL, divide) {
        return (t_divide(params.rvalAt(0)));
      }
      break;
    case 115:
      HASH_GUARD(0x443169EBF71193F3LL, clamp) {
        return (t_clamp(), null);
      }
      break;
    case 125:
      HASH_GUARD(0x49B5ED5DCFBF58FDLL, am) {
        return (t_am(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
      }
      break;
    default:
      break;
  }
  return c_biginteger::o_invoke(s, params, hash, fatal);
}
Variant c_nullexp::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 2:
      HASH_GUARD(0x3D03B239A1E9E902LL, tobytearray) {
        return (t_tobytearray());
      }
      break;
    case 10:
      HASH_GUARD(0x404D2534F5F9778ALL, divremto) {
        return (t_divremto(a0, a1, a2), null);
      }
      break;
    case 11:
      HASH_GUARD(0x3B2D21057B10BC8BLL, iseven) {
        return (t_iseven());
      }
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(a0));
      }
      break;
    case 12:
      HASH_GUARD(0x6EE369CD0EC2128CLL, multiplyupperto) {
        return (t_multiplyupperto(a0, a1, a2), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0BFA87DE111EEA8DLL, subto) {
        return (t_subto(a0, a1), null);
      }
      break;
    case 16:
      HASH_GUARD(0x4D38F3151376E610LL, multiply) {
        return (t_multiply(a0));
      }
      break;
    case 19:
      HASH_GUARD(0x4D1CA98174E44193LL, invdigit) {
        return (t_invdigit());
      }
      break;
    case 25:
      HASH_GUARD(0x5FB593FF9B781E19LL, subtract) {
        return (t_subtract(a0));
      }
      break;
    case 29:
      HASH_GUARD(0x6A786C55DECF931DLL, multiplylowerto) {
        return (t_multiplylowerto(a0, a1, a2), null);
      }
      break;
    case 30:
      HASH_GUARD(0x47DB44AC0B6E049ELL, drshiftto) {
        return (t_drshiftto(a0, a1), null);
      }
      break;
    case 32:
      HASH_GUARD(0x29C480721C97FCA0LL, rshiftto) {
        return (t_rshiftto(a0, a1), null);
      }
      break;
    case 33:
      HASH_GUARD(0x6659447207E861A1LL, gcd) {
        return (t_gcd(a0));
      }
      HASH_GUARD(0x0DF945F12533F0A1LL, abs) {
        return (t_abs());
      }
      break;
    case 36:
      HASH_GUARD(0x04CF69C0F8C389A4LL, negate) {
        return (t_negate());
      }
      break;
    case 39:
      HASH_GUARD(0x622C0CD1ED7971A7LL, dlshiftto) {
        return (t_dlshiftto(a0, a1), null);
      }
      break;
    case 45:
      HASH_GUARD(0x461BBC73FE1061ADLL, compareto) {
        return (t_compareto(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x5E5702D2FB8BD430LL, modpow) {
        return (t_modpow(a0, a1));
      }
      break;
    case 51:
      HASH_GUARD(0x5BC133D9B54F0933LL, modinverse) {
        return (t_modinverse(a0));
      }
      HASH_GUARD(0x36924875887B5FB3LL, getlowestsetbit) {
        return (t_getlowestsetbit());
      }
      break;
    case 59:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(a0));
      }
      break;
    case 68:
      HASH_GUARD(0x3FD30C5DB7F40944LL, bitlength) {
        return (t_bitlength());
      }
      break;
    case 71:
      HASH_GUARD(0x793171D3BD2BCBC7LL, millerrabin) {
        return (t_millerrabin(a0));
      }
      HASH_GUARD(0x61AF801E09D83247LL, squareto) {
        return (t_squareto(a0), null);
      }
      break;
    case 74:
      HASH_GUARD(0x7B1983912339EFCALL, shiftleft) {
        return (t_shiftleft(a0));
      }
      break;
    case 75:
      HASH_GUARD(0x0D9AF4B657C6CDCBLL, copyto) {
        return (t_copyto(a0), null);
      }
      break;
    case 76:
      HASH_GUARD(0x755E1F76CC554ACCLL, lshiftto) {
        return (t_lshiftto(a0, a1), null);
      }
      break;
    case 80:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(a0, a1, a2), null);
      }
      break;
    case 83:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (t_tostring(a0));
      }
      break;
    case 87:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(a0, a1), null);
      }
      break;
    case 93:
      HASH_GUARD(0x7054CED73B51F0DDLL, multiplyto) {
        return (t_multiplyto(a0, a1), null);
      }
      break;
    case 97:
      HASH_GUARD(0x608C35ACFE8AE4E1LL, fromint) {
        return (t_fromint(a0), null);
      }
      HASH_GUARD(0x200163CFF3908AE1LL, modpowint) {
        return (t_modpowint(a0, a1));
      }
      break;
    case 99:
      HASH_GUARD(0x0D241492DD854D63LL, signum) {
        return (t_signum());
      }
      break;
    case 102:
      HASH_GUARD(0x62D0FAFC642EAEE6LL, daddoffset) {
        return (t_daddoffset(a0, a1), null);
      }
      break;
    case 104:
      HASH_GUARD(0x432DC7FA5F22DFE8LL, shiftright) {
        return (t_shiftright(a0));
      }
      break;
    case 106:
      HASH_GUARD(0x71F28DB4D137ADEALL, mod) {
        return (t_mod(a0));
      }
      break;
    case 108:
      HASH_GUARD(0x759A6AA43D2247ECLL, isprobableprime) {
        return (t_isprobableprime(a0));
      }
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(a0));
      }
      break;
    case 109:
      HASH_GUARD(0x55023DA5CE74B86DLL, modint) {
        return (t_modint(a0));
      }
      break;
    case 112:
      HASH_GUARD(0x3FA60FDDD196E470LL, divide) {
        return (t_divide(a0));
      }
      break;
    case 115:
      HASH_GUARD(0x443169EBF71193F3LL, clamp) {
        return (t_clamp(), null);
      }
      break;
    case 125:
      HASH_GUARD(0x49B5ED5DCFBF58FDLL, am) {
        return (t_am(a0, a1, a2, a3, a4, a5));
      }
      break;
    default:
      break;
  }
  return c_biginteger::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_nullexp::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_biginteger::os_invoke(c, s, params, hash, fatal);
}
Variant cw_nullexp$os_get(const char *s) {
  return c_nullexp::os_get(s, -1);
}
Variant &cw_nullexp$os_lval(const char *s) {
  return c_nullexp::os_lval(s, -1);
}
Variant cw_nullexp$os_constant(const char *s) {
  return c_nullexp::os_constant(s);
}
Variant cw_nullexp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_nullexp::os_invoke(c, s, params, -1, fatal);
}
void c_nullexp::init() {
  c_biginteger::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1725 */
Variant c_nullexp::t_revert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(NullExp, NullExp::revert);
  return v_x;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1729 */
Variant c_nullexp::t_convert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(NullExp, NullExp::convert);
  return v_x;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1733 */
void c_nullexp::t_multo(CVarRef v_x, Variant v_y, Variant v_r) {
  INSTANCE_METHOD_INJECTION(NullExp, NullExp::mulTo);
  LINE(1735,toObject(v_x)->o_invoke_few_args("multiplyTo", 0x7054CED73B51F0DDLL, 2, v_y, v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1737 */
void c_nullexp::t_sqrto(CVarRef v_x, Variant v_r) {
  INSTANCE_METHOD_INJECTION(NullExp, NullExp::sqrTo);
  LINE(1739,toObject(v_x)->o_invoke_few_args("squareTo", 0x61AF801E09D83247LL, 1, v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1604 */
Variant c_classic::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_classic::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_classic::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("m", m_m.isReferenced() ? ref(m_m) : m_m));
  c_ObjectData::o_get(props);
}
bool c_classic::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x7A0C10E3609EA04BLL, m, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_classic::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x7A0C10E3609EA04BLL, m_m,
                         m, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_classic::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x7A0C10E3609EA04BLL, m_m,
                      m, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_classic::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x7A0C10E3609EA04BLL, m_m,
                         m, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_classic::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(classic)
ObjectData *c_classic::create(CVarRef v_m) {
  init();
  t_classic(v_m);
  return this;
}
ObjectData *c_classic::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_classic::cloneImpl() {
  c_classic *obj = NEW(c_classic)();
  cloneSet(obj);
  return obj;
}
void c_classic::cloneSet(c_classic *clone) {
  clone->m_m = m_m.isReferenced() ? ref(m_m) : m_m;
  ObjectData::cloneSet(clone);
}
Variant c_classic::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_classic::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x6B055F0101856950LL, multo) {
        return (t_multo(a0, a1, a2), null);
      }
      break;
    case 3:
      HASH_GUARD(0x2380F10632B403BBLL, convert) {
        return (t_convert(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x3A1B838A356694ECLL, revert) {
        return (t_revert(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x31D5522D95E8F1D7LL, sqrto) {
        return (t_sqrto(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_classic::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_classic$os_get(const char *s) {
  return c_classic::os_get(s, -1);
}
Variant &cw_classic$os_lval(const char *s) {
  return c_classic::os_lval(s, -1);
}
Variant cw_classic$os_constant(const char *s) {
  return c_classic::os_constant(s);
}
Variant cw_classic$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_classic::os_invoke(c, s, params, -1, fatal);
}
void c_classic::init() {
  m_m = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1608 */
void c_classic::t_classic(CVarRef v_m) {
  INSTANCE_METHOD_INJECTION(Classic, Classic::Classic);
  bool oldInCtor = gasInCtor(true);
  (m_m = v_m);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1612 */
Variant c_classic::t_convert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(Classic, Classic::convert);
  if (less(toObject(v_x).o_get("s", 0x0BC2CDE4BBFC9C10LL), 0LL) || not_less(LINE(1614,toObject(v_x)->o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, m_m)), 0LL)) {
    return LINE(1615,toObject(v_x)->o_invoke_few_args("mod", 0x71F28DB4D137ADEALL, 1, m_m));
  }
  else {
    return v_x;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1620 */
Variant c_classic::t_revert(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(Classic, Classic::revert);
  return v_x;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1624 */
void c_classic::t_reduce(Variant v_x) {
  INSTANCE_METHOD_INJECTION(Classic, Classic::reduce);
  LINE(1626,v_x.o_invoke_few_args("divRemTo", 0x404D2534F5F9778ALL, 3, m_m, null, v_x));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1628 */
void c_classic::t_multo(CVarRef v_x, Variant v_y, Variant v_r) {
  INSTANCE_METHOD_INJECTION(Classic, Classic::mulTo);
  LINE(1630,toObject(v_x)->o_invoke_few_args("multiplyTo", 0x7054CED73B51F0DDLL, 2, v_y, v_r));
  LINE(1631,t_reduce(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1633 */
void c_classic::t_sqrto(CVarRef v_x, Variant v_r) {
  INSTANCE_METHOD_INJECTION(Classic, Classic::sqrTo);
  LINE(1635,toObject(v_x)->o_invoke_few_args("squareTo", 0x61AF801E09D83247LL, 1, v_r));
  LINE(1636,t_reduce(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 158 */
Variant c_biginteger::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_biginteger::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_biginteger::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("array", m_array.isReferenced() ? ref(m_array) : m_array));
  props.push_back(NEW(ArrayElement)("t", m_t.isReferenced() ? ref(m_t) : m_t));
  props.push_back(NEW(ArrayElement)("s", m_s.isReferenced() ? ref(m_s) : m_s));
  props.push_back(NEW(ArrayElement)("lowprimes", m_lowprimes));
  props.push_back(NEW(ArrayElement)("lplim", m_lplim.isReferenced() ? ref(m_lplim) : m_lplim));
  c_ObjectData::o_get(props);
}
bool c_biginteger::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x063D35168C04B960LL, array, 5);
      HASH_EXISTS_STRING(0x0BC2CDE4BBFC9C10LL, s, 1);
      break;
    case 9:
      HASH_EXISTS_STRING(0x145BA0334EA8D3B9LL, lowprimes, 9);
      break;
    case 10:
      HASH_EXISTS_STRING(0x49A41405B440CC3ALL, lplim, 5);
      break;
    case 12:
      HASH_EXISTS_STRING(0x11C9D2391242AEBCLL, t, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_biginteger::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x063D35168C04B960LL, m_array,
                         array, 5);
      HASH_RETURN_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                         s, 1);
      break;
    case 9:
      HASH_RETURN_STRING(0x145BA0334EA8D3B9LL, m_lowprimes,
                         lowprimes, 9);
      break;
    case 10:
      HASH_RETURN_STRING(0x49A41405B440CC3ALL, m_lplim,
                         lplim, 5);
      break;
    case 12:
      HASH_RETURN_STRING(0x11C9D2391242AEBCLL, m_t,
                         t, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_biginteger::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x063D35168C04B960LL, m_array,
                      array, 5);
      HASH_SET_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                      s, 1);
      break;
    case 9:
      HASH_SET_STRING(0x145BA0334EA8D3B9LL, m_lowprimes,
                      lowprimes, 9);
      break;
    case 10:
      HASH_SET_STRING(0x49A41405B440CC3ALL, m_lplim,
                      lplim, 5);
      break;
    case 12:
      HASH_SET_STRING(0x11C9D2391242AEBCLL, m_t,
                      t, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_biginteger::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x063D35168C04B960LL, m_array,
                         array, 5);
      HASH_RETURN_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                         s, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x49A41405B440CC3ALL, m_lplim,
                         lplim, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x11C9D2391242AEBCLL, m_t,
                         t, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_biginteger::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(biginteger)
ObjectData *c_biginteger::create(CVarRef v_a, CVarRef v_b //  = null_variant
, CVarRef v_c //  = null_variant
) {
  init();
  t_biginteger(v_a, v_b, v_c);
  return this;
}
ObjectData *c_biginteger::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
ObjectData *c_biginteger::cloneImpl() {
  c_biginteger *obj = NEW(c_biginteger)();
  cloneSet(obj);
  return obj;
}
void c_biginteger::cloneSet(c_biginteger *clone) {
  clone->m_array = m_array.isReferenced() ? ref(m_array) : m_array;
  clone->m_t = m_t.isReferenced() ? ref(m_t) : m_t;
  clone->m_s = m_s.isReferenced() ? ref(m_s) : m_s;
  clone->m_lowprimes = m_lowprimes;
  clone->m_lplim = m_lplim.isReferenced() ? ref(m_lplim) : m_lplim;
  ObjectData::cloneSet(clone);
}
Variant c_biginteger::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 2:
      HASH_GUARD(0x3D03B239A1E9E902LL, tobytearray) {
        return (t_tobytearray());
      }
      break;
    case 10:
      HASH_GUARD(0x404D2534F5F9778ALL, divremto) {
        return (t_divremto(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 11:
      HASH_GUARD(0x3B2D21057B10BC8BLL, iseven) {
        return (t_iseven());
      }
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x6EE369CD0EC2128CLL, multiplyupperto) {
        return (t_multiplyupperto(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0BFA87DE111EEA8DLL, subto) {
        return (t_subto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 16:
      HASH_GUARD(0x4D38F3151376E610LL, multiply) {
        return (t_multiply(params.rvalAt(0)));
      }
      break;
    case 19:
      HASH_GUARD(0x4D1CA98174E44193LL, invdigit) {
        return (t_invdigit());
      }
      break;
    case 25:
      HASH_GUARD(0x5FB593FF9B781E19LL, subtract) {
        return (t_subtract(params.rvalAt(0)));
      }
      break;
    case 29:
      HASH_GUARD(0x6A786C55DECF931DLL, multiplylowerto) {
        return (t_multiplylowerto(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 30:
      HASH_GUARD(0x47DB44AC0B6E049ELL, drshiftto) {
        return (t_drshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 32:
      HASH_GUARD(0x29C480721C97FCA0LL, rshiftto) {
        return (t_rshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 33:
      HASH_GUARD(0x6659447207E861A1LL, gcd) {
        return (t_gcd(params.rvalAt(0)));
      }
      HASH_GUARD(0x0DF945F12533F0A1LL, abs) {
        return (t_abs());
      }
      break;
    case 36:
      HASH_GUARD(0x04CF69C0F8C389A4LL, negate) {
        return (t_negate());
      }
      break;
    case 39:
      HASH_GUARD(0x622C0CD1ED7971A7LL, dlshiftto) {
        return (t_dlshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 45:
      HASH_GUARD(0x461BBC73FE1061ADLL, compareto) {
        return (t_compareto(params.rvalAt(0)));
      }
      break;
    case 48:
      HASH_GUARD(0x5E5702D2FB8BD430LL, modpow) {
        return (t_modpow(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 51:
      HASH_GUARD(0x5BC133D9B54F0933LL, modinverse) {
        return (t_modinverse(params.rvalAt(0)));
      }
      HASH_GUARD(0x36924875887B5FB3LL, getlowestsetbit) {
        return (t_getlowestsetbit());
      }
      break;
    case 68:
      HASH_GUARD(0x3FD30C5DB7F40944LL, bitlength) {
        return (t_bitlength());
      }
      break;
    case 71:
      HASH_GUARD(0x793171D3BD2BCBC7LL, millerrabin) {
        return (t_millerrabin(params.rvalAt(0)));
      }
      HASH_GUARD(0x61AF801E09D83247LL, squareto) {
        return (t_squareto(params.rvalAt(0)), null);
      }
      break;
    case 74:
      HASH_GUARD(0x7B1983912339EFCALL, shiftleft) {
        return (t_shiftleft(params.rvalAt(0)));
      }
      break;
    case 75:
      HASH_GUARD(0x0D9AF4B657C6CDCBLL, copyto) {
        return (t_copyto(params.rvalAt(0)), null);
      }
      break;
    case 76:
      HASH_GUARD(0x755E1F76CC554ACCLL, lshiftto) {
        return (t_lshiftto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 83:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (t_tostring(params.rvalAt(0)));
      }
      break;
    case 93:
      HASH_GUARD(0x7054CED73B51F0DDLL, multiplyto) {
        return (t_multiplyto(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 97:
      HASH_GUARD(0x608C35ACFE8AE4E1LL, fromint) {
        return (t_fromint(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x200163CFF3908AE1LL, modpowint) {
        return (t_modpowint(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 99:
      HASH_GUARD(0x0D241492DD854D63LL, signum) {
        return (t_signum());
      }
      break;
    case 102:
      HASH_GUARD(0x62D0FAFC642EAEE6LL, daddoffset) {
        return (t_daddoffset(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 104:
      HASH_GUARD(0x432DC7FA5F22DFE8LL, shiftright) {
        return (t_shiftright(params.rvalAt(0)));
      }
      break;
    case 106:
      HASH_GUARD(0x71F28DB4D137ADEALL, mod) {
        return (t_mod(params.rvalAt(0)));
      }
      break;
    case 108:
      HASH_GUARD(0x759A6AA43D2247ECLL, isprobableprime) {
        return (t_isprobableprime(params.rvalAt(0)));
      }
      break;
    case 109:
      HASH_GUARD(0x55023DA5CE74B86DLL, modint) {
        return (t_modint(params.rvalAt(0)));
      }
      break;
    case 112:
      HASH_GUARD(0x3FA60FDDD196E470LL, divide) {
        return (t_divide(params.rvalAt(0)));
      }
      break;
    case 115:
      HASH_GUARD(0x443169EBF71193F3LL, clamp) {
        return (t_clamp(), null);
      }
      break;
    case 125:
      HASH_GUARD(0x49B5ED5DCFBF58FDLL, am) {
        return (t_am(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_biginteger::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 2:
      HASH_GUARD(0x3D03B239A1E9E902LL, tobytearray) {
        return (t_tobytearray());
      }
      break;
    case 10:
      HASH_GUARD(0x404D2534F5F9778ALL, divremto) {
        return (t_divremto(a0, a1, a2), null);
      }
      break;
    case 11:
      HASH_GUARD(0x3B2D21057B10BC8BLL, iseven) {
        return (t_iseven());
      }
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(a0));
      }
      break;
    case 12:
      HASH_GUARD(0x6EE369CD0EC2128CLL, multiplyupperto) {
        return (t_multiplyupperto(a0, a1, a2), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0BFA87DE111EEA8DLL, subto) {
        return (t_subto(a0, a1), null);
      }
      break;
    case 16:
      HASH_GUARD(0x4D38F3151376E610LL, multiply) {
        return (t_multiply(a0));
      }
      break;
    case 19:
      HASH_GUARD(0x4D1CA98174E44193LL, invdigit) {
        return (t_invdigit());
      }
      break;
    case 25:
      HASH_GUARD(0x5FB593FF9B781E19LL, subtract) {
        return (t_subtract(a0));
      }
      break;
    case 29:
      HASH_GUARD(0x6A786C55DECF931DLL, multiplylowerto) {
        return (t_multiplylowerto(a0, a1, a2), null);
      }
      break;
    case 30:
      HASH_GUARD(0x47DB44AC0B6E049ELL, drshiftto) {
        return (t_drshiftto(a0, a1), null);
      }
      break;
    case 32:
      HASH_GUARD(0x29C480721C97FCA0LL, rshiftto) {
        return (t_rshiftto(a0, a1), null);
      }
      break;
    case 33:
      HASH_GUARD(0x6659447207E861A1LL, gcd) {
        return (t_gcd(a0));
      }
      HASH_GUARD(0x0DF945F12533F0A1LL, abs) {
        return (t_abs());
      }
      break;
    case 36:
      HASH_GUARD(0x04CF69C0F8C389A4LL, negate) {
        return (t_negate());
      }
      break;
    case 39:
      HASH_GUARD(0x622C0CD1ED7971A7LL, dlshiftto) {
        return (t_dlshiftto(a0, a1), null);
      }
      break;
    case 45:
      HASH_GUARD(0x461BBC73FE1061ADLL, compareto) {
        return (t_compareto(a0));
      }
      break;
    case 48:
      HASH_GUARD(0x5E5702D2FB8BD430LL, modpow) {
        return (t_modpow(a0, a1));
      }
      break;
    case 51:
      HASH_GUARD(0x5BC133D9B54F0933LL, modinverse) {
        return (t_modinverse(a0));
      }
      HASH_GUARD(0x36924875887B5FB3LL, getlowestsetbit) {
        return (t_getlowestsetbit());
      }
      break;
    case 68:
      HASH_GUARD(0x3FD30C5DB7F40944LL, bitlength) {
        return (t_bitlength());
      }
      break;
    case 71:
      HASH_GUARD(0x793171D3BD2BCBC7LL, millerrabin) {
        return (t_millerrabin(a0));
      }
      HASH_GUARD(0x61AF801E09D83247LL, squareto) {
        return (t_squareto(a0), null);
      }
      break;
    case 74:
      HASH_GUARD(0x7B1983912339EFCALL, shiftleft) {
        return (t_shiftleft(a0));
      }
      break;
    case 75:
      HASH_GUARD(0x0D9AF4B657C6CDCBLL, copyto) {
        return (t_copyto(a0), null);
      }
      break;
    case 76:
      HASH_GUARD(0x755E1F76CC554ACCLL, lshiftto) {
        return (t_lshiftto(a0, a1), null);
      }
      break;
    case 83:
      HASH_GUARD(0x6271FDA592D5EF53LL, tostring) {
        return (t_tostring(a0));
      }
      break;
    case 93:
      HASH_GUARD(0x7054CED73B51F0DDLL, multiplyto) {
        return (t_multiplyto(a0, a1), null);
      }
      break;
    case 97:
      HASH_GUARD(0x608C35ACFE8AE4E1LL, fromint) {
        return (t_fromint(a0), null);
      }
      HASH_GUARD(0x200163CFF3908AE1LL, modpowint) {
        return (t_modpowint(a0, a1));
      }
      break;
    case 99:
      HASH_GUARD(0x0D241492DD854D63LL, signum) {
        return (t_signum());
      }
      break;
    case 102:
      HASH_GUARD(0x62D0FAFC642EAEE6LL, daddoffset) {
        return (t_daddoffset(a0, a1), null);
      }
      break;
    case 104:
      HASH_GUARD(0x432DC7FA5F22DFE8LL, shiftright) {
        return (t_shiftright(a0));
      }
      break;
    case 106:
      HASH_GUARD(0x71F28DB4D137ADEALL, mod) {
        return (t_mod(a0));
      }
      break;
    case 108:
      HASH_GUARD(0x759A6AA43D2247ECLL, isprobableprime) {
        return (t_isprobableprime(a0));
      }
      break;
    case 109:
      HASH_GUARD(0x55023DA5CE74B86DLL, modint) {
        return (t_modint(a0));
      }
      break;
    case 112:
      HASH_GUARD(0x3FA60FDDD196E470LL, divide) {
        return (t_divide(a0));
      }
      break;
    case 115:
      HASH_GUARD(0x443169EBF71193F3LL, clamp) {
        return (t_clamp(), null);
      }
      break;
    case 125:
      HASH_GUARD(0x49B5ED5DCFBF58FDLL, am) {
        return (t_am(a0, a1, a2, a3, a4, a5));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_biginteger::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_biginteger$os_get(const char *s) {
  return c_biginteger::os_get(s, -1);
}
Variant &cw_biginteger$os_lval(const char *s) {
  return c_biginteger::os_lval(s, -1);
}
Variant cw_biginteger$os_constant(const char *s) {
  return c_biginteger::os_constant(s);
}
Variant cw_biginteger$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_biginteger::os_invoke(c, s, params, -1, fatal);
}
void c_biginteger::init() {
  m_array = null;
  m_t = null;
  m_s = null;
  m_lowprimes = ScalarArrays::sa_[1];
  m_lplim = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 166 */
void c_biginteger::t_biginteger(CVarRef v_a, CVarRef v_b //  = null_variant
, CVarRef v_c //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::BigInteger);
  bool oldInCtor = gasInCtor(true);
  (m_array = ScalarArrays::sa_[0]);
  if (!(LINE(169,x_is_null(v_a)))) {
    if (equal("integer", LINE(170,x_gettype(v_a)))) {
      LINE(171,t_fromnumber(v_a, v_b, v_c));
    }
    else if (LINE(172,x_is_null(v_b)) && !equal("string", x_gettype(v_a))) {
      LINE(173,t_fromstring(v_a, 256LL));
    }
    else {
      LINE(175,t_fromstring(v_a, v_b));
    }
  }
  (m_lplim = divide(67108864LL, m_lowprimes.rvalAt(LINE(179,x_count(m_lowprimes)) - 1LL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 181 */
Variant c_biginteger::t_am(Variant v_i, CVarRef v_x, CVarRef v_w, Variant v_j, Variant v_c, Variant v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::am);
  Variant v_this_array;
  Variant v_w_array;
  Primitive v_xl = 0;
  int64 v_xh = 0;
  Variant v_l;
  int64 v_h = 0;
  PlusOperand v_m = 0;

  (v_this_array = ref(lval(m_array)));
  (v_w_array = ref(lval(toObject(v_w).o_lval("array", 0x063D35168C04B960LL))));
  (v_xl = bitwise_and(v_x, 16383LL));
  (v_xh = toInt64(toInt64(v_x)) >> 14LL);
  LOOP_COUNTER(13);
  {
    while (not_less(--v_n, 0LL)) {
      LOOP_COUNTER_CHECK(13);
      {
        (v_l = bitwise_and(v_this_array.rvalAt(v_i), 16383LL));
        (v_h = toInt64(toInt64(v_this_array.rvalAt(v_i++))) >> 14LL);
        (v_m = v_xh * v_l + v_h * v_xl);
        (v_l = v_xl * v_l + (toInt64((toInt64(bitwise_and(v_m, 16383LL)))) << 14LL) + v_w_array.rvalAt(v_j) + v_c);
        (v_c = (toInt64(toInt64(v_l)) >> 28LL) + (toInt64(toInt64(v_m)) >> 14LL) + v_xh * v_h);
        v_w_array.set(v_j++, (bitwise_and(v_l, 268435455LL)));
      }
    }
  }
  return v_c;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 197 */
void c_biginteger::t_copyto(Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::copyTo);
  Variant v_this_array;
  Variant v_r_array;
  Numeric v_i = 0;

  (v_this_array = ref(lval(m_array)));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  {
    LOOP_COUNTER(14);
    for ((v_i = m_t - 1LL); not_less(v_i, 0LL); --v_i) {
      LOOP_COUNTER_CHECK(14);
      {
        v_r_array.set(v_i, (v_this_array.rvalAt(v_i)));
      }
    }
  }
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = m_t);
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = m_s);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 208 */
void c_biginteger::t_fromint(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::fromInt);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  (m_t = 1LL);
  (m_s = (less(v_x, 0LL)) ? ((-1LL)) : ((0LL)));
  if (more(v_x, 0LL)) {
    v_this_array.set(0LL, (v_x), 0x77CFA1EEF01BCA90LL);
  }
  else if (less(v_x, -1LL)) {
    v_this_array.set(0LL, (v_x + 268435456LL /* BI_DV */), 0x77CFA1EEF01BCA90LL);
  }
  else {
    (m_t = 0LL);
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 221 */
void c_biginteger::t_fromstring(CVarRef v_s, CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::fromString);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_this_array;
  int64 v_k = 0;
  int v_i = 0;
  bool v_mi = false;
  int64 v_sh = 0;
  Variant v_x;

  (v_this_array = ref(lval(m_array)));
  if (equal(v_b, 16LL)) {
    (v_k = 4LL);
  }
  else if (equal(v_b, 8LL)) {
    (v_k = 3LL);
  }
  else if (equal(v_b, 256LL)) {
    (v_k = 8LL);
  }
  else if (equal(v_b, 2LL)) {
    (v_k = 1LL);
  }
  else if (equal(v_b, 32LL)) {
    (v_k = 5LL);
  }
  else if (equal(v_b, 4LL)) {
    (v_k = 2LL);
  }
  else {
    LINE(237,t_fromradix(v_s, v_b));
    return;
  }
  (m_t = 0LL);
  (m_s = 0LL);
  if (LINE(242,x_is_array(v_s))) {
    (v_i = LINE(243,x_count(v_s)));
  }
  else {
    (v_i = LINE(245,x_strlen(toString(v_s))));
  }
  (v_mi = false);
  (v_sh = 0LL);
  LOOP_COUNTER(15);
  {
    while (not_less(--v_i, 0LL)) {
      LOOP_COUNTER_CHECK(15);
      {
        (v_x = (equal(v_k, 8LL)) ? ((Variant)(bitwise_and(v_s.rvalAt(v_i), 255LL))) : ((Variant)(LINE(250,f_intat(v_s, toInt64(v_i))))));
        if (less(v_x, 0LL)) {
          if (equal(v_s.rvalAt(v_i), "-")) {
            (v_mi = true);
            continue;
          }
          (v_mi = false);
        }
        if (equal(v_sh, 0LL)) {
          v_this_array.set(m_t++, (v_x));
        }
        else if (more(v_sh + v_k, 28LL /* BI_DB */)) {
          lval(v_this_array.lvalAt(m_t - 1LL)) |= toInt64((toInt64(bitwise_and(v_x, ((toInt64(1LL) << (28LL /* BI_DB */ - v_sh)) - 1LL))))) << v_sh;
          v_this_array.set(m_t++, ((toInt64(toInt64(v_x)) >> (28LL /* BI_DB */ - v_sh))));
        }
        else {
          lval(v_this_array.lvalAt(m_t - 1LL)) |= toInt64(toInt64(v_x)) << v_sh;
        }
        v_sh += v_k;
        if (not_less(v_sh, 28LL /* BI_DB */)) {
          v_sh -= 28LL /* BI_DB */;
        }
      }
    }
  }
  if (equal(v_k, 8LL) && !equal((bitwise_and(v_s.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 128LL)), 0LL)) {
    (m_s = -1LL);
    if (more(v_sh, 0LL)) {
      lval(v_this_array.lvalAt(m_t - 1LL)) |= toInt64(((toInt64(1LL) << (28LL /* BI_DB */ - v_sh)) - 1LL)) << v_sh;
    }
  }
  LINE(277,t_clamp());
  if (v_mi) {
    LINE(279,g->GV(BigIntegerZero).o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, this, this));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 282 */
void c_biginteger::t_clamp() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::clamp);
  Variant v_this_array;
  Primitive v_c = 0;

  (v_this_array = ref(lval(m_array)));
  (v_c = bitwise_and(m_s, 268435455LL /* BI_DM */));
  LOOP_COUNTER(16);
  {
    while (more(m_t, 0LL) && equal(v_this_array.rvalAt(m_t - 1LL), v_c)) {
      LOOP_COUNTER_CHECK(16);
      {
        --m_t;
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 290 */
Variant c_biginteger::t_tostring(Variant v_b) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::toString);
  Variant eo_0;
  Variant v_this_array;
  int64 v_k = 0;
  int64 v_km = 0;
  bool v_m = false;
  Variant v_r;
  Variant v_i;
  Numeric v_p = 0;
  Variant v_d;

  (v_this_array = ref(lval(m_array)));
  if (less(m_s, 0LL)) {
    return (Variant)("-") + (assignCallTemp(eo_0, toObject(LINE(294,t_negate()))),eo_0.o_invoke_few_args("toString", 0x6271FDA592D5EF53LL, 1, v_b));
  }
  if (equal(v_b, 16LL)) {
    (v_k = 4LL);
  }
  else if (equal(v_b, 8LL)) {
    (v_k = 3LL);
  }
  else if (equal(v_b, 2LL)) {
    (v_k = 1LL);
  }
  else if (equal(v_b, 32LL)) {
    (v_k = 5LL);
  }
  else if (equal(v_b, 4LL)) {
    (v_k = 2LL);
  }
  else {
    return LINE(307,t_toradix(v_b));
  }
  (v_km = (toInt64(1LL) << v_k) - 1LL);
  (v_m = false);
  (v_r = "");
  (v_i = m_t);
  (v_p = 28LL /* BI_DB */ - modulo((toInt64(v_i * 28LL /* BI_DB */)), v_k));
  if (more(v_i--, 0LL)) {
    if (less(v_p, 28LL /* BI_DB */) && more(((v_d = toInt64(toInt64(v_this_array.rvalAt(v_i))) >> toInt64(k_p))), 0LL)) {
      (v_m = true);
      (v_r = LINE(317,f_int2char(v_d)));
    }
    LOOP_COUNTER(17);
    {
      while (not_less(v_i, 0LL)) {
        LOOP_COUNTER_CHECK(17);
        {
          if (less(v_p, v_k)) {
            (v_d = toInt64((toInt64(bitwise_and(v_this_array.rvalAt(v_i), ((toInt64(1LL) << toInt64(v_p)) - 1LL))))) << (toInt64(v_k - v_p)));
            v_d |= shift_right_rev(toInt64((toInt64(v_p += 28LL /* BI_DB */ - v_k))), toInt64(v_this_array.rvalAt(--v_i)));
          }
          else {
            (v_d = bitwise_and((toInt64(toInt64(v_this_array.rvalAt(v_i))) >> (toInt64(v_p -= v_k))), v_km));
            if (not_more(v_p, 0LL)) {
              v_p += 28LL /* BI_DB */;
              --v_i;
            }
          }
          if (more(v_d, 0LL)) {
            (v_m = true);
          }
          if (v_m) {
            concat_assign(v_r, toString(LINE(334,f_int2char(v_d))));
          }
        }
      }
    }
  }
  return v_m ? ((Variant)(v_r)) : ((Variant)("0"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 341 */
Variant c_biginteger::t_negate() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::negate);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_r;

  (v_r = ((Object)(LINE(343,f_nbi()))));
  LINE(344,g->GV(BigIntegerZero).o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, this, v_r));
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 347 */
Variant c_biginteger::t_abs() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::abs);
  return (less(m_s, 0LL)) ? ((Variant)(LINE(349,t_negate()))) : ((Variant)(((Object)(this))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 351 */
Numeric c_biginteger::t_compareto(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::compareTo);
  Variant v_this_array;
  Variant v_a_array;
  Numeric v_r = 0;
  Variant v_i;

  (v_this_array = ref(lval(m_array)));
  (v_a_array = ref(lval(toObject(v_a).o_lval("array", 0x063D35168C04B960LL))));
  (v_r = m_s - toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL));
  if (!equal(v_r, 0LL)) {
    return v_r;
  }
  (v_i = m_t);
  (v_r = v_i - toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL));
  if (!equal(v_r, 0LL)) {
    return v_r;
  }
  LOOP_COUNTER(18);
  {
    while (not_less(--v_i, 0LL)) {
      LOOP_COUNTER_CHECK(18);
      {
        if (!equal(((v_r = v_this_array.rvalAt(v_i) - v_a_array.rvalAt(v_i))), 0LL)) {
          return v_r;
        }
      }
    }
  }
  return 0LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 371 */
Numeric c_biginteger::t_bitlength() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::bitLength);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  if (not_more(m_t, 0LL)) {
    return 0LL;
  }
  return 28LL /* BI_DB */ * (m_t - 1LL) + LINE(377,f_nbits(bitwise_xor(v_this_array.rvalAt(m_t - 1LL), (bitwise_and(m_s, 268435455LL /* BI_DM */)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 381 */
void c_biginteger::t_dlshiftto(CVarRef v_n, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::dlShiftTo);
  Variant v_this_array;
  Variant v_r_array;
  Numeric v_i = 0;

  (v_this_array = ref(lval(m_array)));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  {
    LOOP_COUNTER(19);
    for ((v_i = m_t - 1LL); not_less(v_i, 0LL); --v_i) {
      LOOP_COUNTER_CHECK(19);
      {
        v_r_array.set(v_i + v_n, (v_this_array.rvalAt(v_i)));
      }
    }
  }
  {
    LOOP_COUNTER(20);
    for ((v_i = v_n - 1LL); not_less(v_i, 0LL); --v_i) {
      LOOP_COUNTER_CHECK(20);
      {
        v_r_array.set(v_i, (0LL));
      }
    }
  }
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = m_t + v_n);
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = m_s);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 399 */
void c_biginteger::t_drshiftto(CVarRef v_n, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::drShiftTo);
  Variant v_this_array;
  Variant v_r_array;
  Variant v_i;

  (v_this_array = ref(lval(m_array)));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  {
    LOOP_COUNTER(21);
    for ((v_i = v_n); less(v_i, m_t); ++v_i) {
      LOOP_COUNTER_CHECK(21);
      {
        v_r_array.set(v_i - v_n, (v_this_array.rvalAt(v_i)));
      }
    }
  }
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = LINE(406,x_max(2, m_t - v_n, ScalarArrays::sa_[2])));
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = m_s);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 409 */
void c_biginteger::t_lshiftto(CVarRef v_n, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::lShiftTo);
  Variant v_this_array;
  Variant v_r_array;
  Numeric v_bs = 0;
  Numeric v_cbs = 0;
  int64 v_bm = 0;
  double v_ds = 0.0;
  Variant v_c;
  Numeric v_i = 0;

  (v_this_array = ref(lval(m_array)));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_bs = modulo(toInt64(v_n), 28LL /* BI_DB */));
  (v_cbs = 28LL /* BI_DB */ - v_bs);
  (v_bm = (toInt64(1LL) << toInt64(v_cbs)) - 1LL);
  (v_ds = LINE(416,x_floor(toDouble(divide(v_n, 28LL /* BI_DB */)))));
  (v_c = bitwise_and((toInt64(toInt64(m_s)) << toInt64(v_bs)), 268435455LL /* BI_DM */));
  {
    LOOP_COUNTER(22);
    for ((v_i = m_t - 1LL); not_less(v_i, 0LL); --v_i) {
      LOOP_COUNTER_CHECK(22);
      {
        v_r_array.set(toDouble(v_i) + v_ds + 1LL, (bitwise_or((toInt64(toInt64(v_this_array.rvalAt(v_i))) >> toInt64(v_cbs)), v_c)));
        (v_c = toInt64((toInt64(bitwise_and(v_this_array.rvalAt(v_i), v_bm)))) << toInt64(v_bs));
      }
    }
  }
  {
    LOOP_COUNTER(23);
    for ((v_i = v_ds - 1LL); not_less(v_i, 0LL); --v_i) {
      LOOP_COUNTER_CHECK(23);
      {
        v_r_array.set(v_i, (0LL));
      }
    }
  }
  v_r_array.set(v_ds, (v_c));
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = toDouble(m_t) + v_ds + 1LL);
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = m_s);
  LINE(430,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 432 */
void c_biginteger::t_rshiftto(CVarRef v_n, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::rShiftTo);
  Variant v_this_array;
  Variant v_r_array;
  double v_ds = 0.0;
  Numeric v_bs = 0;
  Numeric v_cbs = 0;
  int64 v_bm = 0;
  double v_i = 0.0;

  (v_this_array = ref(lval(m_array)));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = m_s);
  (v_ds = LINE(437,x_floor(toDouble(divide(v_n, 28LL /* BI_DB */)))));
  if (not_less(v_ds, m_t)) {
    (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = 0LL);
    return;
  }
  (v_bs = modulo(toInt64(v_n), 28LL /* BI_DB */));
  (v_cbs = 28LL /* BI_DB */ - v_bs);
  (v_bm = (toInt64(1LL) << toInt64(v_bs)) - 1LL);
  v_r_array.set(0LL, (toInt64(toInt64(v_this_array.rvalAt(v_ds))) >> toInt64(v_bs)), 0x77CFA1EEF01BCA90LL);
  {
    LOOP_COUNTER(24);
    for ((v_i = v_ds + 1LL); less(v_i, m_t); ++v_i) {
      LOOP_COUNTER_CHECK(24);
      {
        lval(v_r_array.lvalAt(v_i - v_ds - 1LL)) |= toInt64((toInt64(bitwise_and(v_this_array.rvalAt(v_i), v_bm)))) << toInt64(v_cbs);
        v_r_array.set(v_i - v_ds, (toInt64(toInt64(m_array.rvalAt(v_i))) >> toInt64(v_bs)));
      }
    }
  }
  if (more(v_bs, 0LL)) {
    lval(v_r_array.lvalAt(toDouble(m_t) - v_ds - 1LL)) |= toInt64((toInt64(bitwise_and(m_s, v_bm)))) << toInt64(v_cbs);
  }
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = toDouble(m_t) - v_ds);
  LINE(454,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 457 */
void c_biginteger::t_subto(CVarRef v_a, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::subTo);
  Variant v_this_array;
  Variant v_r_array;
  Variant v_a_array;
  int64 v_i = 0;
  Variant v_c;
  Variant v_m;

  (v_this_array = ref(lval(m_array)));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_a_array = ref(lval(toObject(v_a).o_lval("array", 0x063D35168C04B960LL))));
  (v_i = 0LL);
  (v_c = 0LL);
  (v_m = LINE(464,x_min(2, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), Array(ArrayInit(1).set(0, m_t).create()))));
  LOOP_COUNTER(25);
  {
    while (less(v_i, v_m)) {
      LOOP_COUNTER_CHECK(25);
      {
        v_c += v_this_array.rvalAt(v_i) - v_a_array.rvalAt(v_i);
        v_r_array.set(v_i++, (bitwise_and(v_c, 268435455LL /* BI_DM */)));
        v_c >>= 28LL /* BI_DB */;
      }
    }
  }
  if (less(toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), m_t)) {
    v_c -= toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL);
    LOOP_COUNTER(26);
    {
      while (less(v_i, m_t)) {
        LOOP_COUNTER_CHECK(26);
        {
          v_c += v_this_array.rvalAt(v_i);
          v_r_array.set(v_i++, (bitwise_and(v_c, 268435455LL /* BI_DM */)));
          v_c >>= 268435455LL /* BI_DM */;
        }
      }
    }
    v_c += m_s;
  }
  else {
    v_c += m_s;
    LOOP_COUNTER(27);
    {
      while (less(v_i, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL))) {
        LOOP_COUNTER_CHECK(27);
        {
          v_c -= v_a_array.rvalAt(v_i);
          v_r_array.set(v_i++, (bitwise_and(v_c, 268435455LL /* BI_DM */)));
          v_c >>= 268435455LL /* BI_DM */;
        }
      }
    }
    v_c -= toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL);
  }
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = (less(v_c, 0LL)) ? ((-1LL)) : ((0LL)));
  if (less(v_c, -1LL)) {
    v_r_array.set(v_i++, (268435456LL /* BI_DV */ + v_c));
  }
  else if (more(v_c, 0LL)) {
    v_r_array.set(v_i++, (v_c));
  }
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = v_i);
  LINE(494,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 496 */
void c_biginteger::t_multiplyto(CVarRef v_a, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::multiplyTo);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_this_array;
  Variant v_r_array;
  Variant v_x;
  Variant v_y;
  Variant v_y_array;
  Variant v_i;

  (v_this_array = ref(lval(m_array)));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_x = LINE(500,t_abs()));
  (v_y = LINE(501,toObject(v_a)->o_invoke_few_args("abs", 0x0DF945F12533F0A1LL, 0)));
  (v_y_array = ref(lval(v_y.o_lval("array", 0x063D35168C04B960LL))));
  (v_i = v_x.o_get("t", 0x11C9D2391242AEBCLL));
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = v_i + v_y.o_get("t", 0x11C9D2391242AEBCLL));
  LOOP_COUNTER(28);
  {
    while (not_less(--v_i, 0LL)) {
      LOOP_COUNTER_CHECK(28);
      {
        v_r_array.set(v_i, (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(29);
    for ((v_i = 0LL); less(v_i, v_y.o_get("t", 0x11C9D2391242AEBCLL)); ++v_i) {
      LOOP_COUNTER_CHECK(29);
      {
        v_r_array.set(v_i + v_x.o_get("t", 0x11C9D2391242AEBCLL), (LINE(510,v_x.o_invoke_few_args("am", 0x49B5ED5DCFBF58FDLL, 6, 0LL, v_y_array.refvalAt(v_i), v_r, v_i, 0LL, v_x.o_lval("t", 0x11C9D2391242AEBCLL)))));
      }
    }
  }
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = 0LL);
  LINE(513,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
  if (!equal(m_s, toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL))) {
    LINE(515,g->GV(BigIntegerZero).o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_r, v_r));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 519 */
void c_biginteger::t_squareto(Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::squareTo);
  Variant v_x;
  Variant v_x_array;
  Variant v_r_array;
  Variant v_i;
  Variant v_c;

  (v_x = LINE(521,t_abs()));
  (v_x_array = ref(lval(v_x.o_lval("array", 0x063D35168C04B960LL))));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_i = (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = 2LL * v_x.o_get("t", 0x11C9D2391242AEBCLL)));
  LOOP_COUNTER(30);
  {
    while (not_less(--v_i, 0LL)) {
      LOOP_COUNTER_CHECK(30);
      {
        v_r_array.set(v_i, (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(31);
    for ((v_i = 0LL); less(v_i, v_x.o_get("t", 0x11C9D2391242AEBCLL) - 1LL); ++v_i) {
      LOOP_COUNTER_CHECK(31);
      {
        (v_c = LINE(531,v_x.o_invoke_few_args("am", 0x49B5ED5DCFBF58FDLL, 6, v_i, v_x_array.refvalAt(v_i), v_r, 2LL * v_i, 0LL, 1LL)));
        if (not_less((lval(v_r_array.lvalAt(v_i + v_x.o_get("t", 0x11C9D2391242AEBCLL))) += LINE(533,v_x.o_invoke_few_args("am", 0x49B5ED5DCFBF58FDLL, 6, v_i + 1LL, 2LL * v_x_array.rvalAt(v_i), v_r, 2LL * v_i + 1LL, v_c, v_x.o_get("t", 0x11C9D2391242AEBCLL) - v_i - 1LL))), 268435456LL /* BI_DV */)) {
          lval(v_r_array.lvalAt(v_i + v_x.o_get("t", 0x11C9D2391242AEBCLL))) -= 268435456LL /* BI_DV */;
          v_r_array.set(v_i + v_x.o_get("t", 0x11C9D2391242AEBCLL) + 1LL, (1LL));
        }
      }
    }
  }
  if (more(v_r.o_get("t", 0x11C9D2391242AEBCLL), 0LL)) {
    lval(v_r_array.lvalAt(v_r.o_get("t", 0x11C9D2391242AEBCLL) - 1LL)) += LINE(539,v_x.o_invoke_few_args("am", 0x49B5ED5DCFBF58FDLL, 6, v_i, v_x_array.refvalAt(v_i), v_r, 2LL * v_i, 0LL, 1LL));
  }
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = 0LL);
  LINE(542,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 546 */
void c_biginteger::t_divremto(CVarRef v_m, Variant v_q, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::divRemTo);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_pm;
  Variant v_pt;
  Variant v_y;
  Variant v_ts;
  Variant v_ms;
  Variant v_pm_array;
  Variant v_nsh;
  Variant v_ys;
  Variant v_y_array;
  Variant v_y0;
  Numeric v_yt = 0;
  Numeric v_d1 = 0;
  Numeric v_d2 = 0;
  int64 v_e = 0;
  Variant v_i;
  Variant v_j;
  Variant v_t;
  Variant v_r_array;
  Variant v_qd;

  (v_pm = LINE(548,toObject(v_m)->o_invoke_few_args("abs", 0x0DF945F12533F0A1LL, 0)));
  if (not_more(v_pm.o_get("t", 0x11C9D2391242AEBCLL), 0LL)) {
    return;
  }
  (v_pt = LINE(553,t_abs()));
  if (less(v_pt.o_get("t", 0x11C9D2391242AEBCLL), v_pm.o_get("t", 0x11C9D2391242AEBCLL))) {
    if (!(LINE(555,x_is_null(v_q)))) {
      LINE(556,v_q.o_invoke_few_args("fromInt", 0x608C35ACFE8AE4E1LL, 1, 0LL));
    }
    if (!(LINE(558,x_is_null(v_r)))) {
      LINE(559,t_copyto(v_r));
    }
    return;
  }
  if (LINE(563,x_is_null(v_r))) {
    (v_r = ((Object)(LINE(564,f_nbi()))));
  }
  (v_y = ((Object)(LINE(566,f_nbi()))));
  (v_ts = m_s);
  (v_ms = toObject(v_m).o_get("s", 0x0BC2CDE4BBFC9C10LL));
  (v_pm_array = ref(lval(v_pm.o_lval("array", 0x063D35168C04B960LL))));
  (v_nsh = 28LL /* BI_DB */ - LINE(570,f_nbits(v_pm_array.rvalAt(v_pm.o_get("t", 0x11C9D2391242AEBCLL) - 1LL))));
  if (more(v_nsh, 0LL)) {
    LINE(573,v_pm.o_invoke_few_args("lShiftTo", 0x755E1F76CC554ACCLL, 2, v_nsh, v_y));
    LINE(574,v_pt.o_invoke_few_args("lShiftTo", 0x755E1F76CC554ACCLL, 2, v_nsh, v_r));
  }
  else {
    LINE(577,v_pm.o_invoke_few_args("copyTo", 0x0D9AF4B657C6CDCBLL, 1, v_y));
    LINE(578,v_pt.o_invoke_few_args("copyTo", 0x0D9AF4B657C6CDCBLL, 1, v_r));
  }
  (v_ys = v_y.o_get("t", 0x11C9D2391242AEBCLL));
  (v_y_array = ref(lval(v_y.o_lval("array", 0x063D35168C04B960LL))));
  (v_y0 = v_y_array.rvalAt(v_ys - 1LL));
  if (equal(v_y0, 0LL)) {
    return;
  }
  (v_yt = v_y0 * 16777216LL + ((more(v_ys, 1LL)) ? ((toInt64(toInt64(v_y_array.rvalAt(v_ys - 2LL))) >> 4LL /* BI_F2 */)) : ((0LL))));
  (v_d1 = divide(get_global_variables()->k_BI_FV, v_yt));
  (v_d2 = divide(16777216LL, v_yt));
  (v_e = 16LL);
  (v_i = v_r.o_get("t", 0x11C9D2391242AEBCLL));
  (v_j = v_i - v_ys);
  (v_t = (LINE(594,x_is_null(v_q))) ? ((Variant)(((Object)(f_nbi())))) : ((Variant)(v_q)));
  LINE(595,v_y.o_invoke_few_args("dlShiftTo", 0x622C0CD1ED7971A7LL, 2, v_j, v_t));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  if (not_less(LINE(598,v_r.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_t)), 0LL)) {
    v_r_array.set(lval(v_r.o_lval("t", 0x11C9D2391242AEBCLL))++, (1LL));
    LINE(600,v_r.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_t, v_r));
  }
  LINE(602,g->GV(BigIntegerOne).o_invoke_few_args("dlShiftTo", 0x622C0CD1ED7971A7LL, 2, v_ys, v_t));
  LINE(605,v_t.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_y, v_y));
  LOOP_COUNTER(32);
  {
    while (less(v_y.o_get("t", 0x11C9D2391242AEBCLL), v_ys)) {
      LOOP_COUNTER_CHECK(32);
      {
        v_y_array.set(lval(v_y.o_lval("t", 0x11C9D2391242AEBCLL))++, (0LL));
      }
    }
  }
  LOOP_COUNTER(33);
  {
    while (not_less(--v_j, 0LL)) {
      LOOP_COUNTER_CHECK(33);
      {
        (v_qd = (equal(v_r_array.rvalAt(--v_i), v_y0)) ? ((Variant)(268435455LL /* BI_DM */)) : ((Variant)(LINE(613,x_floor(toDouble(v_r_array.rvalAt(v_i) * v_d1 + (v_r_array.rvalAt(v_i - 1LL) + v_e) * v_d2))))));
        if (less((lval(v_r_array.lvalAt(v_i)) += LINE(614,v_y.o_invoke_few_args("am", 0x49B5ED5DCFBF58FDLL, 6, 0LL, v_qd, v_r, v_j, 0LL, v_ys))), v_qd)) {
          LINE(616,v_y.o_invoke_few_args("dlShiftTo", 0x622C0CD1ED7971A7LL, 2, v_j, v_t));
          LINE(617,v_r.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_t, v_r));
          LOOP_COUNTER(34);
          {
            while (less(v_r_array.rvalAt(v_i), --v_qd)) {
              LOOP_COUNTER_CHECK(34);
              {
                LINE(619,v_r.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_t, v_r));
              }
            }
          }
        }
      }
    }
  }
  if (!(LINE(624,x_is_null(v_q)))) {
    LINE(625,v_r.o_invoke_few_args("drShiftTo", 0x47DB44AC0B6E049ELL, 2, v_ys, v_q));
    if (!equal(v_ts, v_ms)) {
      LINE(627,g->GV(BigIntegerZero).o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_q, v_q));
    }
  }
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = v_ys);
  LINE(631,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
  if (more(v_nsh, 0LL)) {
    LINE(633,v_r.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, v_nsh, v_r));
  }
  if (less(v_ts, 0LL)) {
    LINE(636,g->GV(BigIntegerZero).o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_r, v_r));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 641 */
Variant c_biginteger::t_mod(Variant v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::mod);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_r;

  (v_r = ((Object)(LINE(643,f_nbi()))));
  (assignCallTemp(eo_0, toObject(LINE(645,t_abs()))),eo_0.o_invoke_few_args("divRemTo", 0x404D2534F5F9778ALL, 3, v_a, null, v_r));
  if (less(m_s, 0LL) && more(LINE(646,v_r.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerZero))), 0LL)) {
    LINE(648,v_a.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_r, v_r));
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 666 */
Numeric c_biginteger::t_invdigit() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::invDigit);
  Variant v_this_array;
  Variant v_x;
  Variant v_y;

  (v_this_array = ref(lval(m_array)));
  if (less(m_t, 1LL)) {
    return 0LL;
  }
  (v_x = v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  if (equal((bitwise_and(v_x, 1LL)), 0LL)) {
    return 0LL;
  }
  (v_y = bitwise_and(v_x, 3LL));
  (v_y = bitwise_and((v_y * (2LL - (bitwise_and(v_x, 15LL)) * v_y)), 15LL));
  (v_y = bitwise_and((v_y * (2LL - (bitwise_and(v_x, 255LL)) * v_y)), 255LL));
  (v_y = bitwise_and((v_y * (2LL - (bitwise_and(((bitwise_and(v_x, 65535LL)) * v_y), 65535LL)))), 65535LL));
  (v_y = modulo((toInt64(v_y * (2LL - modulo(toInt64(v_x * v_y), 268435456LL /* BI_DV */)))), 268435456LL /* BI_DV */));
  return (more(v_y, 0LL)) ? ((268435456LL /* BI_DV */ - v_y)) : ((negate(v_y)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 687 */
bool c_biginteger::t_iseven() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::isEven);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  return equal(((more(m_t, 0LL)) ? ((Variant)((bitwise_and(v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 1LL)))) : ((Variant)(m_s))), 0LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 694 */
Variant c_biginteger::t_exp(CVarRef v_e, CVarRef v_z) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::exp);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_r;
  Variant v_r2;
  Variant v_g;
  int64 v_i = 0;
  Variant v_t;

  if (more(v_e, 0.0) || less(v_e, 1LL)) {
    return g->GV(BigIntegerOne);
  }
  (v_r = ((Object)(LINE(699,f_nbi()))));
  (v_r2 = ((Object)(LINE(700,f_nbi()))));
  (v_g = LINE(701,toObject(v_z)->o_invoke_few_args("convert", 0x2380F10632B403BBLL, 1, this)));
  (v_i = LINE(702,f_nbits(v_e)) - 1LL);
  LINE(703,v_g.o_invoke_few_args("copyTo", 0x0D9AF4B657C6CDCBLL, 1, v_r));
  LOOP_COUNTER(35);
  {
    while (not_less(--v_i, 0LL)) {
      LOOP_COUNTER_CHECK(35);
      {
        LINE(705,toObject(v_z)->o_invoke_few_args("sqrTo", 0x31D5522D95E8F1D7LL, 2, v_r, v_r2));
        if (more((bitwise_and(v_e, (toInt64(1LL) << v_i))), 0LL)) {
          LINE(707,toObject(v_z)->o_invoke_few_args("mulTo", 0x6B055F0101856950LL, 3, v_r2, v_g, v_r));
        }
        else {
          (v_t = v_r);
          (v_r = v_r2);
          (v_r2 = v_t);
        }
      }
    }
  }
  return LINE(714,toObject(v_z)->o_invoke_few_args("revert", 0x3A1B838A356694ECLL, 1, v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 718 */
Variant c_biginteger::t_modpowint(CVarRef v_e, CVarRef v_m) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::modPowInt);
  Variant v_z;

  if (less(v_e, 256LL) || toBoolean(LINE(720,toObject(v_m)->o_invoke_few_args("isEven", 0x3B2D21057B10BC8BLL, 0)))) {
    (v_z = ((Object)(LINE(721,p_classic(p_classic(NEWOBJ(c_classic)())->create(v_m))))));
  }
  else {
    (v_z = ((Object)(LINE(723,p_montgomery(p_montgomery(NEWOBJ(c_montgomery)())->create(v_m))))));
  }
  return LINE(725,t_exp(v_e, v_z));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 728 */
p_biginteger c_biginteger::t_bnpclone() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::bnpClone);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(730,f_nbi()))))));
  LINE(731,t_copyto(((Object)(v_r))));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 735 */
Variant c_biginteger::t_intvalue() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::intValue);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  if (less(m_s, 0LL)) {
    if (equal(m_t, 1LL)) {
      return v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL) - 268435456LL /* BI_DV */;
    }
    else if (equal(m_t, 0LL)) {
      return -1LL;
    }
  }
  else if (equal(m_t, 1LL)) {
    return v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
  }
  else if (equal(m_t, 0LL)) {
    return 0LL;
  }
  return bitwise_or((toInt64((toInt64(bitwise_and(v_this_array.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 15LL)))) << 28LL /* BI_DB */), v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 754 */
Variant c_biginteger::t_bytevalue() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::byteValue);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  return (equal(m_t, 0LL)) ? ((Variant)(m_s)) : ((Variant)(toInt64((toInt64(toInt64(v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) << 24LL)) >> 24LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 761 */
Variant c_biginteger::t_shortvalue() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::shortValue);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  return (equal(m_t, 0LL)) ? ((Variant)(m_s)) : ((Variant)(toInt64((toInt64(toInt64(v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) << 16LL)) >> 16LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 768 */
double c_biginteger::t_chunksize(CVarRef v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::chunkSize);
  return LINE(770,x_floor(toDouble(divide_rev(x_log(toDouble(v_r)), x_log(toDouble(2LL)) * 28LL /* BI_DB */))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 774 */
Variant c_biginteger::t_signum() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::signum);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  if (less(m_s, 0LL)) {
    return -1LL;
  }
  else if (not_more(m_t, 0LL) || (equal(m_t, 1LL) && not_more(v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0LL))) {
    return 0LL;
  }
  else {
    return 1LL;
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 787 */
String c_biginteger::t_toradix(Variant v_b) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::toRadix);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  double v_cs = 0.0;
  Numeric v_a = 0;
  p_biginteger v_d;
  p_biginteger v_y;
  p_biginteger v_z;
  String v_r;

  if (LINE(789,x_is_null(v_b))) {
    (v_b = 10LL);
  }
  if (equal(LINE(792,t_signum()), 0LL) || less(v_b, 2LL) || more(v_b, 36LL)) {
    return "0";
  }
  (v_cs = LINE(795,t_chunksize(v_b)));
  (v_a = LINE(796,x_pow(v_b, v_cs)));
  ((Object)((v_d = ((Object)(LINE(797,f_nbv(v_a)))))));
  ((Object)((v_y = ((Object)(LINE(798,f_nbi()))))));
  ((Object)((v_z = ((Object)(LINE(799,f_nbi()))))));
  (v_r = "");
  LINE(801,t_divremto(((Object)(v_d)), ((Object)(v_y)), ((Object)(v_z))));
  LOOP_COUNTER(36);
  {
    while (more(LINE(802,v_y->t_signum()), 0LL)) {
      LOOP_COUNTER_CHECK(36);
      {
        (v_r = concat(toString(LINE(804,(assignCallTemp(eo_0, (assignCallTemp(eo_2, (toString(v_a + v_z->t_intvalue()))),assignCallTemp(eo_4, toInt64(v_b)),x_base_convert(eo_2, 10LL, eo_4))),x_substr(eo_0, toInt32(1LL))))), v_r));
        LINE(805,v_y->t_divremto(((Object)(v_d)), ((Object)(v_y)), ((Object)(v_z))));
      }
    }
  }
  return concat(LINE(807,(assignCallTemp(eo_0, toString(v_z->t_intvalue())),assignCallTemp(eo_2, toInt64(v_b)),x_base_convert(eo_0, 10LL, eo_2))), v_r);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 811 */
void c_biginteger::t_fromradix(CVarRef v_s, Variant v_b) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::fromRadix);
  DECLARE_GLOBAL_VARIABLES(g);
  double v_cs = 0.0;
  Numeric v_d = 0;
  bool v_mi = false;
  int64 v_i = 0;
  Variant v_x;
  Variant v_w;
  Variant v_j;

  LINE(812,t_fromint(0LL));
  if (LINE(813,x_is_null(v_b))) {
    (v_b = 10LL);
  }
  (v_cs = LINE(816,t_chunksize(v_b)));
  (v_d = LINE(817,x_pow(v_b, v_cs)));
  (v_mi = false);
  {
    LOOP_COUNTER(37);
    for ((v_i = 0LL); less(v_i, toObject(v_s).o_get("length", 0x2993E8EB119CAB21LL)); ++v_i) {
      LOOP_COUNTER_CHECK(37);
      {
        (v_x = LINE(820,f_intat(v_s, v_i)));
        if (less(v_x, 0LL)) {
          if (equal(v_s.rvalAt(v_i), "-") && equal(LINE(822,t_signum()), 0LL)) {
            (v_mi = true);
          }
          continue;
        }
        (v_w = v_b * v_w + v_x);
        if (not_less(++v_j, v_cs)) {
          LINE(829,t_dmultiply(v_d));
          LINE(830,t_daddoffset(v_w, 0LL));
          (v_j = 0LL);
          (v_w = 0LL);
        }
      }
    }
  }
  if (more(v_j, 0LL)) {
    LINE(836,t_dmultiply(x_pow(v_b, v_j)));
    LINE(837,t_daddoffset(v_w, 0LL));
  }
  if (v_mi) {
    concat(toString(g->GV(BigIntegerZero)), toString(LINE(840,invoke_failed("subto", Array(ArrayInit(2).set(0, ref(this)).set(1, ref(this)).create()), 0x00000000111EEA8DLL))));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 845 */
void c_biginteger::t_fromnumber(CVarRef v_a, CVarRef v_b, CVarRef v_c) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::fromNumber);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_x;
  Primitive v_t = 0;

  if (equal("number", LINE(846,x_gettype(v_b)))) {
    if (less(v_a, 2LL)) {
      LINE(849,t_fromint(1LL));
    }
    else {
      LINE(851,o_root_invoke_few_args("fromNumber", 0x6D2D00D67FD472AELL, 2, v_a, v_c));
      if (!(LINE(852,t_testbit(v_a - 1LL)))) {
        (assignCallTemp(eo_0, LINE(853,g->GV(BigIntegerOne).o_invoke_few_args("shiftLeft", 0x7B1983912339EFCALL, 1, v_a - 1LL))),assignCallTemp(eo_1, k_op_or),assignCallTemp(eo_2, ((Object)(this))),t_bitwiseto(eo_0, eo_1, eo_2));
      }
      if (LINE(855,t_iseven())) {
        LINE(856,t_daddoffset(1LL, 0LL));
      }
      LOOP_COUNTER(38);
      {
        while (!(toBoolean(LINE(858,t_isprobableprime(v_b))))) {
          LOOP_COUNTER_CHECK(38);
          {
            LINE(859,t_daddoffset(2LL, 0LL));
            if (more(LINE(860,t_bitlength()), v_a)) {
              (assignCallTemp(eo_0, LINE(861,g->GV(BigIntegerOne).o_invoke_few_args("shiftLeft", 0x7B1983912339EFCALL, 1, v_a - 1LL))),assignCallTemp(eo_1, ((Object)(this))),t_subto(eo_0, eo_1));
            }
          }
        }
      }
    }
  }
  else {
    (v_x = ScalarArrays::sa_[0]);
    (v_t = bitwise_and(v_a, 7LL));
    LINE(870,f_setlength(ref(v_x), ((toInt64(toInt64(v_a)) >> 3LL) + 1LL)));
    LINE(871,toObject(v_b)->o_invoke_few_args("nextBytes", 0x039723D7E38DED4ALL, 1, v_x));
    if (more(v_t, 0LL)) {
      lval(v_x.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)) &= ((toInt64(1LL) << toInt64(v_t)) - 1LL);
    }
    else {
      v_x.set(0LL, (0LL), 0x77CFA1EEF01BCA90LL);
    }
    LINE(877,t_fromstring(v_x, 256LL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 881 */
Variant c_biginteger::t_tobytearray() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::toByteArray);
  Variant v_this_array;
  Variant v_i;
  Variant v_r;
  Numeric v_p = 0;
  int64 v_k = 0;
  Variant v_d;

  (v_this_array = ref(lval(m_array)));
  (v_i = m_t);
  (v_r = ScalarArrays::sa_[0]);
  v_r.set(0LL, (m_s), 0x77CFA1EEF01BCA90LL);
  (v_p = 28LL /* BI_DB */ - modulo((toInt64(v_i * 28LL /* BI_DB */)), 8LL));
  (v_k = 0LL);
  (v_d = "");
  if (more(v_i--, 0LL)) {
    if (less(v_p, 28LL /* BI_DB */) && !equal(((v_d = toInt64(toInt64(v_this_array.rvalAt(v_i))) >> toInt64(v_p))), toInt64((toInt64(bitwise_and(m_s, 268435455LL /* BI_DM */)))) >> toInt64(v_p))) {
      v_r.set(v_k++, (bitwise_or(v_d, (toInt64(toInt64(m_s)) << (toInt64(28LL /* BI_DB */ - v_p))))));
    }
    LOOP_COUNTER(39);
    {
      while (not_less(v_i, 0LL)) {
        LOOP_COUNTER_CHECK(39);
        {
          if (less(v_p, 8LL)) {
            (v_d = toInt64((toInt64(bitwise_and(v_this_array.rvalAt(v_i), ((toInt64(1LL) << toInt64(v_p)) - 1LL))))) << (toInt64(8LL - v_p)));
            v_d |= shift_right_rev(toInt64((toInt64(v_p += 20LL))), toInt64(v_this_array.rvalAt(--v_i)));
          }
          else {
            (v_d = bitwise_and((toInt64(toInt64(v_this_array.rvalAt(v_i))) >> (toInt64(v_p -= 8LL))), 255LL));
            if (not_more(v_p, 0LL)) {
              v_p += 28LL /* BI_DB */;
              --v_i;
            }
          }
          if (!equal((bitwise_and(v_d, 128LL)), 0LL)) {
            v_d |= -256LL;
          }
          if (equal(v_k, 0LL) && !equal((bitwise_and(m_s, 128LL)), (bitwise_and(v_d, 128LL)))) {
            ++v_k;
          }
          if (more(v_k, 0LL) || !equal(v_d, m_s)) {
            v_r.set(v_k++, (v_d));
          }
        }
      }
    }
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 919 */
bool c_biginteger::t_equals(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::equals);
  return (equal(LINE(921,t_compareto(v_a)), 0LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 923 */
Variant c_biginteger::t_min(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::min);
  return (less(LINE(925,t_compareto(v_a)), 0LL)) ? ((Variant)(((Object)(this)))) : ((Variant)(v_a));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 927 */
Variant c_biginteger::t_max(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::max);
  return (more(LINE(929,t_compareto(v_a)), 0LL)) ? ((Variant)(((Object)(this)))) : ((Variant)(v_a));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 932 */
void c_biginteger::t_bitwiseto(CVarRef v_a, CVarRef v_op, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::bitwiseTo);
  Variant v_this_array;
  Variant v_a_array;
  Variant v_r_array;
  Variant v_m;
  Variant v_i;
  Variant v_f;

  (v_this_array = ref(lval(m_array)));
  (v_a_array = ref(lval(toObject(v_a).o_lval("array", 0x063D35168C04B960LL))));
  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_m = LINE(936,x_min(2, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), Array(ArrayInit(1).set(0, m_t).create()))));
  {
    LOOP_COUNTER(40);
    for ((v_i = 0LL); less(v_i, v_m); ++v_i) {
      LOOP_COUNTER_CHECK(40);
      {
        v_r_array.set(v_i, (LINE(938,invoke(toString(v_op), Array(ArrayInit(2).set(0, ref(v_this_array.refvalAt(v_i))).set(1, ref(v_a_array.refvalAt(v_i))).create()), -1))));
      }
    }
  }
  if (less(toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), m_t)) {
    (v_f = bitwise_and(toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL), 268435455LL /* BI_DM */));
    {
      LOOP_COUNTER(41);
      for ((v_i = v_m); less(v_i, m_t); ++v_i) {
        LOOP_COUNTER_CHECK(41);
        {
          v_r_array.set(v_i, (LINE(943,invoke(toString(v_op), Array(ArrayInit(2).set(0, ref(v_this_array.refvalAt(v_i))).set(1, ref(v_f)).create()), -1))));
        }
      }
    }
    (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = m_t);
  }
  else {
    (v_f = bitwise_and(m_s, 268435455LL /* BI_DM */));
    {
      LOOP_COUNTER(42);
      for ((v_i = v_m); less(v_i, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL)); ++v_i) {
        LOOP_COUNTER_CHECK(42);
        {
          v_r_array.set(v_i, (LINE(949,invoke(toString(v_op), Array(ArrayInit(2).set(0, ref(v_f)).set(1, ref(v_a_array.refvalAt(v_i))).create()), -1))));
        }
      }
    }
    (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL));
  }
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = LINE(953,invoke(toString(v_op), Array(ArrayInit(2).set(0, ref(m_s)).set(1, ref(toObject(v_a).o_lval("s", 0x0BC2CDE4BBFC9C10LL))).create()), -1)));
  LINE(954,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 958 */
Primitive c_biginteger::t_op_and(CVarRef v_x, CVarRef v_y) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::op_and);
  return bitwise_and(v_x, v_y);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 962 */
String c_biginteger::t_band(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::band);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(964,f_nbi()))))));
  LINE(965,t_bitwiseto(v_a, k_op_and, ((Object)(v_r))));
  return k_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 970 */
Primitive c_biginteger::t_op_or(CVarRef v_x, CVarRef v_y) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::op_or);
  return bitwise_or(v_x, v_y);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 974 */
String c_biginteger::t_bor(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::bor);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(976,f_nbi()))))));
  LINE(977,t_bitwiseto(v_a, k_op_or, ((Object)(v_r))));
  return k_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 982 */
Primitive c_biginteger::t_op_xor(CVarRef v_x, CVarRef v_y) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::op_xor);
  return bitwise_xor(v_x, v_y);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 986 */
p_biginteger c_biginteger::t_bxor(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::bxor);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(988,f_nbi()))))));
  LINE(989,t_bitwiseto(v_a, k_op_xor, ((Object)(v_r))));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 994 */
Primitive c_biginteger::t_op_andnot(CVarRef v_x, CVarRef v_y) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::op_andnot);
  return bitwise_and(v_x, ~v_y);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 998 */
String c_biginteger::t_andnot(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::andNot);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1000,f_nbi()))))));
  LINE(1001,t_bitwiseto(v_a, k_op_andnot, ((Object)(v_r))));
  return k_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1006 */
p_biginteger c_biginteger::t_not() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::not);
  Variant v_this_array;
  p_biginteger v_r;
  Variant v_r_array;
  int64 v_i = 0;

  (v_this_array = ref(lval(m_array)));
  ((Object)((v_r = ((Object)(LINE(1009,f_nbi()))))));
  (v_r_array = ref(lval(v_r->m_array)));
  {
    LOOP_COUNTER(43);
    for ((v_i = 0LL); less(v_i, m_t); ++v_i) {
      LOOP_COUNTER_CHECK(43);
      {
        v_r_array.set(v_i, (bitwise_and(268435455LL /* BI_DM */, ~v_this_array.rvalAt(v_i))));
      }
    }
  }
  (v_r->m_t = m_t);
  (v_r->m_s = ~m_s);
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1021 */
p_biginteger c_biginteger::t_shiftleft(CVarRef v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::shiftLeft);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1023,f_nbi()))))));
  if (less(v_n, 0LL)) {
    LINE(1025,t_rshiftto(negate(v_n), ((Object)(v_r))));
  }
  else {
    LINE(1027,t_lshiftto(v_n, ((Object)(v_r))));
  }
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1033 */
p_biginteger c_biginteger::t_shiftright(CVarRef v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::shiftRight);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1035,f_nbi()))))));
  if (less(v_n, 0LL)) {
    LINE(1037,t_lshiftto(negate(v_n), ((Object)(v_r))));
  }
  else {
    LINE(1039,t_rshiftto(v_n, ((Object)(v_r))));
  }
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1045 */
Numeric c_biginteger::t_getlowestsetbit() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::getLowestSetBit);
  Variant v_this_array;
  int64 v_i = 0;

  (v_this_array = ref(lval(m_array)));
  {
    LOOP_COUNTER(44);
    for ((v_i = 0LL); less(v_i, m_t); ++v_i) {
      LOOP_COUNTER_CHECK(44);
      {
        if (!equal(v_this_array.rvalAt(v_i), 0LL)) {
          return v_i * 28LL /* BI_DB */ + LINE(1051,f_lbit(v_this_array.rvalAt(v_i)));
        }
      }
    }
  }
  if (less(m_s, 0LL)) {
    return m_t * 28LL /* BI_DB */;
  }
  return -1LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1061 */
int64 c_biginteger::t_bitcount() {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::bitCount);
  int64 v_r = 0;
  Variant v_s;
  Primitive v_x = 0;
  int64 v_i = 0;
  Variant v_this_array;

  (v_r = 0LL);
  (v_x = bitwise_and(o_get(toString(v_s), -1LL), 268435455LL /* BI_DM */));
  {
    LOOP_COUNTER(45);
    for ((v_i = 0LL); less(v_i, m_t); ++v_i) {
      LOOP_COUNTER_CHECK(45);
      {
        v_r += LINE(1066,f_cbit(bitwise_xor(v_this_array.rvalAt(v_i), v_x)));
      }
    }
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1072 */
bool c_biginteger::t_testbit(Numeric v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::testBit);
  Variant v_this_array;
  double v_j = 0.0;

  (v_this_array = ref(lval(m_array)));
  (v_j = LINE(1075,x_floor(toDouble(divide(v_n, 28LL /* BI_DB */)))));
  if (not_less(v_j, m_t)) {
    return (!equal(m_s, 0LL));
  }
  return (!equal((bitwise_and(v_this_array.rvalAt(v_j), (toInt64(1LL) << (toInt64(modulo(toInt64(v_n), 28LL /* BI_DB */)))))), 0LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1083 */
Variant c_biginteger::t_changebit(Variant v_n, CVarRef v_op) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::changeBit);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_r;

  (v_r = LINE(1085,g->GV(BigIntegerOne).o_invoke_few_args("shiftLeft", 0x7B1983912339EFCALL, 1, v_n)));
  LINE(1086,t_bitwiseto(v_r, v_op, v_r));
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1091 */
Variant c_biginteger::t_setbit(CVarRef v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::setBit);
  return LINE(1093,t_changebit(v_n, k_op_or));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1097 */
Variant c_biginteger::t_clearbit(CVarRef v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::clearBit);
  return LINE(1099,t_changebit(v_n, k_op_andnot));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1103 */
Variant c_biginteger::t_flipbit(CVarRef v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::flipBit);
  Variant v_op_xor;

  return LINE(1105,t_changebit(v_n, v_op_xor));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1109 */
void c_biginteger::t_addto(CVarRef v_a, p_biginteger v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::addTo);
  Variant v_this_array;
  Variant v_a_array;
  Variant v_r_array;
  int64 v_i = 0;
  Variant v_c;
  Variant v_m;

  (v_this_array = ref(lval(m_array)));
  (v_a_array = ref(lval(toObject(v_a).o_lval("array", 0x063D35168C04B960LL))));
  (v_r_array = ref(lval(v_r->m_array)));
  (v_i = 0LL);
  (v_c = 0LL);
  (v_m = LINE(1116,x_min(2, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), Array(ArrayInit(1).set(0, m_t).create()))));
  LOOP_COUNTER(46);
  {
    while (less(v_i, v_m)) {
      LOOP_COUNTER_CHECK(46);
      {
        v_c += v_this_array.rvalAt(v_i) + v_a_array.rvalAt(v_i);
        v_r_array.set(v_i++, (bitwise_and(v_c, 268435455LL /* BI_DM */)));
        v_c >>= 28LL /* BI_DB */;
      }
    }
  }
  if (less(toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), m_t)) {
    v_c += toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL);
    LOOP_COUNTER(47);
    {
      while (less(v_i, m_t)) {
        LOOP_COUNTER_CHECK(47);
        {
          v_c += v_this_array.rvalAt(v_i);
          v_r_array.set(v_i++, (bitwise_and(v_c, 268435455LL /* BI_DM */)));
          v_c >>= 28LL /* BI_DB */;
        }
      }
    }
    v_c += m_s;
  }
  else {
    v_c += m_s;
    LOOP_COUNTER(48);
    {
      while (less(v_i, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL))) {
        LOOP_COUNTER_CHECK(48);
        {
          v_c += v_a_array.rvalAt(v_i);
          v_r_array.set(v_i++, (bitwise_and(v_c, 268435455LL /* BI_DM */)));
          v_c >>= 28LL /* BI_DB */;
        }
      }
    }
    v_c += toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL);
  }
  (v_r->m_s = (less(v_c, 0LL)) ? ((-1LL)) : ((0LL)));
  if (more(v_c, 0LL)) {
    v_r_array.set(v_i++, (v_c));
  }
  else if (less(v_c, -1LL)) {
    v_r_array.set(v_i++, (268435456LL /* BI_DV */ + v_c));
  }
  (v_r->m_t = v_i);
  LINE(1146,v_r->t_clamp());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1150 */
p_biginteger c_biginteger::t_add(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::add);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1152,f_nbi()))))));
  LINE(1153,t_addto(v_a, ((Object)(v_r))));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1158 */
p_biginteger c_biginteger::t_subtract(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::subtract);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1160,f_nbi()))))));
  LINE(1161,t_subto(v_a, ((Object)(v_r))));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1166 */
p_biginteger c_biginteger::t_multiply(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::multiply);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1168,f_nbi()))))));
  LINE(1169,t_multiplyto(v_a, ((Object)(v_r))));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1174 */
p_biginteger c_biginteger::t_divide(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::divide);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1176,f_nbi()))))));
  LINE(1177,t_divremto(v_a, ((Object)(v_r)), null));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1182 */
p_biginteger c_biginteger::t_remainder(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::remainder);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(1184,f_nbi()))))));
  LINE(1185,t_divremto(v_a, null, ((Object)(v_r))));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1190 */
Array c_biginteger::t_divideandremainder(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::divideAndRemainder);
  p_biginteger v_q;
  p_biginteger v_r;

  ((Object)((v_q = ((Object)(LINE(1192,f_nbi()))))));
  ((Object)((v_r = ((Object)(LINE(1193,f_nbi()))))));
  LINE(1194,t_divremto(v_a, ((Object)(v_q)), ((Object)(v_r))));
  return Array(ArrayInit(2).set(0, ((Object)(v_q))).set(1, ((Object)(v_r))).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1199 */
void c_biginteger::t_dmultiply(Numeric v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::dMultiply);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  v_this_array.set(m_t, (LINE(1202,t_am(0LL, v_n - 1LL, ((Object)(this)), 0LL, 0LL, m_t))));
  ++m_t;
  LINE(1204,t_clamp());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1208 */
void c_biginteger::t_daddoffset(CVarRef v_n, Variant v_w) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::dAddOffset);
  Variant v_this_array;

  (v_this_array = ref(lval(m_array)));
  LOOP_COUNTER(49);
  {
    while (not_more(m_t, v_w)) {
      LOOP_COUNTER_CHECK(49);
      {
        v_this_array.set(m_t++, (0LL));
      }
    }
  }
  lval(v_this_array.lvalAt(v_w)) += v_n;
  LOOP_COUNTER(50);
  {
    while (not_less(v_this_array.rvalAt(v_w), 268435456LL /* BI_DV */)) {
      LOOP_COUNTER_CHECK(50);
      {
        lval(v_this_array.lvalAt(v_w)) -= 268435456LL /* BI_DV */;
        if (not_less(++v_w, m_t)) {
          v_this_array.set(m_t++, (0LL));
        }
        ++lval(v_this_array.lvalAt(v_w));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1224 */
Variant c_biginteger::t_pow(CVarRef v_e) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::pow);
  Variant eo_0;
  Variant eo_1;
  return (assignCallTemp(eo_0, v_e),assignCallTemp(eo_1, ((Object)(LINE(1226,create_object("nullexp", Array()))))),t_exp(eo_0, eo_1));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1231 */
void c_biginteger::t_multiplylowerto(CVarRef v_a, CVarRef v_n, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::multiplyLowerTo);
  Variant v_r_array;
  Variant v_a_array;
  Variant v_i;
  Variant v_j;

  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_a_array = ref(lval(toObject(v_a).o_lval("array", 0x063D35168C04B960LL))));
  (v_i = LINE(1235,x_min(2, m_t + toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), Array(ArrayInit(1).set(0, v_n).create()))));
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = 0LL);
  (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = v_i);
  LOOP_COUNTER(51);
  {
    while (more(v_i, 0LL)) {
      LOOP_COUNTER_CHECK(51);
      {
        v_r_array.set(--v_i, (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(52);
    for ((v_j = v_r.o_get("t", 0x11C9D2391242AEBCLL) - m_t); less(v_i, v_j); ++v_i) {
      LOOP_COUNTER_CHECK(52);
      {
        v_r_array.set(v_i + m_t, (LINE(1242,t_am(0LL, v_a_array.rvalAt(v_i), v_r, v_i, 0LL, m_t))));
      }
    }
  }
  {
    LOOP_COUNTER(53);
    for ((v_j = LINE(1244,x_min(2, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL), Array(ArrayInit(1).set(0, v_n).create())))); less(v_i, v_j); ++v_i) {
      LOOP_COUNTER_CHECK(53);
      {
        LINE(1245,t_am(0LL, v_a_array.rvalAt(v_i), v_r, v_i, 0LL, v_n - v_i));
      }
    }
  }
  LINE(1247,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1252 */
void c_biginteger::t_multiplyupperto(CVarRef v_a, Variant v_n, Variant v_r) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::multiplyUpperTo);
  Variant v_r_array;
  Variant v_a_array;
  Variant v_i;

  (v_r_array = ref(lval(v_r.o_lval("array", 0x063D35168C04B960LL))));
  (v_a_array = ref(lval(toObject(v_a).o_lval("array", 0x063D35168C04B960LL))));
  --v_n;
  (v_i = (v_r.o_lval("t", 0x11C9D2391242AEBCLL) = m_t + toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL) - v_n));
  (v_r.o_lval("s", 0x0BC2CDE4BBFC9C10LL) = 0LL);
  LOOP_COUNTER(54);
  {
    while (not_less(--v_i, 0LL)) {
      LOOP_COUNTER_CHECK(54);
      {
        v_r_array.set(v_i, (0LL));
      }
    }
  }
  {
    LOOP_COUNTER(55);
    for ((v_i = LINE(1262,x_max(2, v_n - m_t, ScalarArrays::sa_[2]))); less(v_i, toObject(v_a).o_get("t", 0x11C9D2391242AEBCLL)); ++v_i) {
      LOOP_COUNTER_CHECK(55);
      {
        v_r_array.set(m_t + v_i - v_n, (LINE(1264,t_am(v_n - v_i, v_a_array.rvalAt(v_i), v_r, 0LL, 0LL, m_t + v_i - v_n))));
      }
    }
  }
  LINE(1266,v_r.o_invoke_few_args("clamp", 0x443169EBF71193F3LL, 0));
  LINE(1267,v_r.o_invoke_few_args("drShiftTo", 0x47DB44AC0B6E049ELL, 2, 1LL, v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1271 */
Variant c_biginteger::t_modpow(CVarRef v_e, CVarRef v_m) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::modPow);
  Variant v_e_array;
  Variant v_i;
  Variant v_r;
  int64 v_k = 0;
  Variant v_z;
  Variant v_g;
  int64 v_n = 0;
  int64 v_k1 = 0;
  int64 v_km = 0;
  Variant v_g2;
  Numeric v_j = 0;
  bool v_is1 = false;
  Variant v_r2;
  Variant v_w;
  Variant v_t;

  (v_e_array = ref(lval(toObject(v_e).o_lval("array", 0x063D35168C04B960LL))));
  (v_i = LINE(1274,toObject(v_e)->o_invoke_few_args("bitLength", 0x3FD30C5DB7F40944LL, 0)));
  (v_r = ((Object)(LINE(1275,f_nbv(1LL)))));
  if (not_more(v_i, 0LL)) {
    return v_r;
  }
  else if (less(v_i, 18LL)) {
    (v_k = 1LL);
  }
  else if (less(v_i, 48LL)) {
    (v_k = 3LL);
  }
  else if (less(v_i, 144LL)) {
    (v_k = 4LL);
  }
  else if (less(v_i, 768LL)) {
    (v_k = 5LL);
  }
  else {
    (v_k = 6LL);
  }
  if (less(v_i, 8LL)) {
    (v_z = ((Object)(LINE(1290,p_classic(p_classic(NEWOBJ(c_classic)())->create(v_m))))));
  }
  else if (toBoolean(LINE(1291,toObject(v_m)->o_invoke_few_args("isEven", 0x3B2D21057B10BC8BLL, 0)))) {
    (v_z = LINE(1292,create_object("barret", Array(ArrayInit(1).set(0, v_m).create()))));
  }
  else {
    (v_z = ((Object)(LINE(1294,p_montgomery(p_montgomery(NEWOBJ(c_montgomery)())->create(v_m))))));
  }
  (v_g = ScalarArrays::sa_[0]);
  (v_n = 3LL);
  (v_k1 = v_k - 1LL);
  (v_km = (toInt64(1LL) << v_k) - 1LL);
  v_g.set(1LL, (LINE(1301,v_z.o_invoke_few_args("convert", 0x2380F10632B403BBLL, 1, this))), 0x5BCA7C69B794F8CELL);
  if (more(v_k, 1LL)) {
    (v_g2 = ((Object)(LINE(1303,f_nbi()))));
    LINE(1304,v_z.o_invoke_few_args("sqrTo", 0x31D5522D95E8F1D7LL, 2, v_g.refvalAt(1LL, 0x5BCA7C69B794F8CELL), v_g2));
    LOOP_COUNTER(56);
    {
      while (not_more(v_n, v_km)) {
        LOOP_COUNTER_CHECK(56);
        {
          v_g.set(v_n, (((Object)(LINE(1306,f_nbi())))));
          LINE(1307,v_z.o_invoke_few_args("mulTo", 0x6B055F0101856950LL, 3, v_g2, v_g.refvalAt(v_n - 2LL), v_g.refvalAt(v_n)));
          v_n += 2LL;
        }
      }
    }
  }
  (v_j = toObject(v_e).o_get("t", 0x11C9D2391242AEBCLL) - 1LL);
  (v_is1 = true);
  (v_r2 = ((Object)(LINE(1313,f_nbi()))));
  (v_i = LINE(1314,f_nbits(v_e_array.rvalAt(v_j))) - 1LL);
  LOOP_COUNTER(57);
  {
    while (not_less(v_j, 0LL)) {
      LOOP_COUNTER_CHECK(57);
      {
        if (not_less(v_i, v_k1)) {
          (v_w = bitwise_and((toInt64(toInt64(v_e_array.rvalAt(v_j))) >> (toInt64(v_i - v_k1))), v_km));
        }
        else {
          (v_w = toInt64((toInt64(bitwise_and(v_e_array.rvalAt(v_j), ((toInt64(1LL) << (toInt64(v_i + 1LL))) - 1LL))))) << (toInt64(v_k1 - v_i)));
          if (more(v_j, 0LL)) {
            v_w |= toInt64(toInt64(v_e_array.rvalAt(v_j - 1LL))) >> (toInt64(28LL /* BI_DB */ + v_i - v_k1));
          }
        }
        (v_n = v_k);
        LOOP_COUNTER(58);
        {
          while (equal((bitwise_and(v_w, 1LL)), 0LL)) {
            LOOP_COUNTER_CHECK(58);
            {
              v_w >>= 1LL;
              --v_n;
            }
          }
        }
        if (less((v_i -= v_n), 0LL)) {
          v_i += 28LL /* BI_DB */;
          --v_j;
        }
        if (v_is1) {
          LINE(1336,v_g.rvalAt(v_w).o_invoke_few_args("copyTo", 0x0D9AF4B657C6CDCBLL, 1, v_r));
          (v_is1 = false);
        }
        else {
          LOOP_COUNTER(59);
          {
            while (more(v_n, 1LL)) {
              LOOP_COUNTER_CHECK(59);
              {
                LINE(1341,v_z.o_invoke_few_args("sqrTo", 0x31D5522D95E8F1D7LL, 2, v_r, v_r2));
                LINE(1342,v_z.o_invoke_few_args("sqrTo", 0x31D5522D95E8F1D7LL, 2, v_r2, v_r));
                v_n -= 2LL;
              }
            }
          }
          if (more(v_n, 0LL)) {
            LINE(1345,v_z.o_invoke_few_args("sqrTo", 0x31D5522D95E8F1D7LL, 2, v_r, v_r2));
          }
          else {
            (v_t = v_r);
            (v_r = v_r2);
            (v_r2 = v_t);
          }
          LINE(1351,v_z.o_invoke_few_args("mulTo", 0x6B055F0101856950LL, 3, v_r2, v_g.refvalAt(v_w), v_r));
        }
        LOOP_COUNTER(60);
        {
          while (not_less(v_j, 0LL) && equal((bitwise_and(v_e_array.rvalAt(v_j), (toInt64(1LL) << toInt64(v_i)))), 0LL)) {
            LOOP_COUNTER_CHECK(60);
            {
              LINE(1355,v_z.o_invoke_few_args("sqrTo", 0x31D5522D95E8F1D7LL, 2, v_r, v_r2));
              (v_t = v_r);
              (v_r = v_r2);
              (v_r2 = v_t);
              if (less(--v_i, 0LL)) {
                (v_i = 27LL);
                --v_j;
              }
            }
          }
        }
      }
    }
  }
  return LINE(1364,v_z.o_invoke_few_args("revert", 0x3A1B838A356694ECLL, 1, v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1368 */
Variant c_biginteger::t_gcd(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::gcd);
  Variant v_x;
  Variant v_y;
  Variant v_t;
  Variant v_i;
  Variant v_g;

  (v_x = (less(m_s, 0LL)) ? ((Variant)(LINE(1370,t_negate()))) : ((Variant)(o_root_invoke_few_args("clone", 0x4383223BFE9C076CLL, 0))));
  (v_y = (less(toObject(v_a).o_get("s", 0x0BC2CDE4BBFC9C10LL), 0LL)) ? ((Variant)(LINE(1371,toObject(v_a)->o_invoke_few_args("negate", 0x04CF69C0F8C389A4LL, 0)))) : ((Variant)(toObject(v_a)->o_invoke_few_args("clone", 0x4383223BFE9C076CLL, 0))));
  if (less(LINE(1372,v_x.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_y)), 0LL)) {
    (v_t = v_x);
    (v_x = v_y);
    (v_y = v_t);
  }
  (v_i = LINE(1377,v_x.o_invoke_few_args("getLowestSetBit", 0x36924875887B5FB3LL, 0)));
  (v_g = LINE(1378,v_y.o_invoke_few_args("getLowestSetBit", 0x36924875887B5FB3LL, 0)));
  if (less(v_g, 0LL)) {
    return v_x;
  }
  if (less(v_i, v_g)) {
    (v_g = v_i);
  }
  if (more(v_g, 0LL)) {
    LINE(1386,v_x.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, v_g, v_x));
    LINE(1387,v_y.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, v_g, v_y));
  }
  LOOP_COUNTER(61);
  {
    while (more(LINE(1389,v_x.o_invoke_few_args("signum", 0x0D241492DD854D63LL, 0)), 0LL)) {
      LOOP_COUNTER_CHECK(61);
      {
        if (more(((v_i = LINE(1390,v_x.o_invoke_few_args("getLowestSetBit", 0x36924875887B5FB3LL, 0)))), 0LL)) {
          LINE(1391,v_x.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, v_i, v_x));
        }
        if (more(((v_i = LINE(1393,v_y.o_invoke_few_args("getLowestSetBit", 0x36924875887B5FB3LL, 0)))), 0LL)) {
          LINE(1394,v_y.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, v_i, v_y));
        }
        if (not_less(LINE(1396,v_x.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_y)), 0LL)) {
          LINE(1397,v_x.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_y, v_x));
          LINE(1398,v_x.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, 1LL, v_x));
        }
        else {
          LINE(1400,v_y.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_x, v_y));
          LINE(1401,v_y.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, 1LL, v_y));
        }
      }
    }
  }
  if (more(v_g, 0LL)) {
    LINE(1405,v_y.o_invoke_few_args("lShiftTo", 0x755E1F76CC554ACCLL, 2, v_g, v_y));
  }
  return v_y;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1411 */
Variant c_biginteger::t_modint(CVarRef v_n) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::modInt);
  Variant v_this_array;
  Numeric v_d = 0;
  Variant v_r;
  Numeric v_i = 0;

  (v_this_array = ref(lval(m_array)));
  if (not_more(v_n, 0LL)) {
    return 0LL;
  }
  (v_d = modulo(268435456LL /* BI_DV */, toInt64(v_n)));
  (v_r = (less(m_s, 0LL)) ? ((Variant)(v_n - 1LL)) : ((Variant)(0LL)));
  if (more(m_t, 0LL)) {
    if (equal(v_d, 0LL)) {
      (v_r = modulo(toInt64(v_this_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toInt64(v_n)));
    }
    else {
      {
        LOOP_COUNTER(62);
        for ((v_i = m_t - 1LL); not_less(v_i, 0LL); --v_i) {
          LOOP_COUNTER_CHECK(62);
          {
            (v_r = modulo((toInt64(v_d * v_r + v_this_array.rvalAt(v_i))), toInt64(v_n)));
          }
        }
      }
    }
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1434 */
Variant c_biginteger::t_modinverse(CVarRef v_m) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::ModInverse);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_ac;
  Variant v_u;
  Variant v_v;
  p_biginteger v_a;
  p_biginteger v_b;
  p_biginteger v_c;
  p_biginteger v_d;

  (v_ac = LINE(1435,toObject(v_m)->o_invoke_few_args("isEven", 0x3B2D21057B10BC8BLL, 0)));
  if ((LINE(1436,t_iseven()) && toBoolean(v_ac)) || equal(toObject(v_m)->o_invoke_few_args("signum", 0x0D241492DD854D63LL, 0), 0LL)) {
    return g->GV(BigIntegerZero);
  }
  (v_u = LINE(1439,toObject(v_m)->o_invoke_few_args("clone", 0x4383223BFE9C076CLL, 0)));
  (v_v = LINE(1440,o_root_invoke_few_args("clone", 0x4383223BFE9C076CLL, 0)));
  ((Object)((v_a = ((Object)(LINE(1441,f_nbv(1LL)))))));
  ((Object)((v_b = ((Object)(LINE(1442,f_nbv(0LL)))))));
  ((Object)((v_c = ((Object)(LINE(1443,f_nbv(0LL)))))));
  ((Object)((v_d = ((Object)(LINE(1444,f_nbv(1LL)))))));
  LOOP_COUNTER(63);
  {
    while (!equal(LINE(1445,v_u.o_invoke_few_args("signum", 0x0D241492DD854D63LL, 0)), 0LL)) {
      LOOP_COUNTER_CHECK(63);
      {
        LOOP_COUNTER(64);
        {
          while (toBoolean(LINE(1446,v_u.o_invoke_few_args("isEven", 0x3B2D21057B10BC8BLL, 0)))) {
            LOOP_COUNTER_CHECK(64);
            {
              LINE(1447,v_u.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, 1LL, v_u));
              if (toBoolean(v_ac)) {
                if (!(LINE(1449,v_a->t_iseven())) || !(v_b->t_iseven())) {
                  LINE(1450,v_a->t_addto(((Object)(this)), ((Object)(v_a))));
                  LINE(1451,v_b->t_subto(v_m, ((Object)(v_b))));
                }
                LINE(1453,v_a->t_rshiftto(1LL, ((Object)(v_a))));
              }
              else if (!(LINE(1454,v_b->t_iseven()))) {
                LINE(1455,v_b->t_subto(v_m, ((Object)(v_b))));
              }
              LINE(1457,v_b->t_rshiftto(1LL, ((Object)(v_b))));
            }
          }
        }
        LOOP_COUNTER(65);
        {
          while (toBoolean(LINE(1459,v_v.o_invoke_few_args("isEven", 0x3B2D21057B10BC8BLL, 0)))) {
            LOOP_COUNTER_CHECK(65);
            {
              LINE(1460,v_v.o_invoke_few_args("rShiftTo", 0x29C480721C97FCA0LL, 2, 1LL, v_v));
              if (toBoolean(v_ac)) {
                if (!(LINE(1462,v_c->t_iseven())) || !(v_d->t_iseven())) {
                  LINE(1463,v_c->t_addto(((Object)(this)), ((Object)(v_c))));
                  LINE(1464,v_d->t_subto(v_m, ((Object)(v_d))));
                }
                LINE(1466,v_c->t_rshiftto(1LL, ((Object)(v_c))));
              }
              else if (!(LINE(1467,v_d->t_iseven()))) {
                LINE(1468,v_d->t_subto(v_m, ((Object)(v_d))));
              }
              LINE(1470,v_d->t_rshiftto(1LL, ((Object)(v_d))));
            }
          }
        }
        if (not_less(LINE(1472,v_u.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_v)), 0LL)) {
          LINE(1473,v_u.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_v, v_u));
          if (toBoolean(v_ac)) {
            LINE(1475,v_a->t_subto(((Object)(v_c)), ((Object)(v_a))));
          }
          LINE(1477,v_b->t_subto(((Object)(v_d)), ((Object)(v_b))));
        }
        else {
          LINE(1479,v_v.o_invoke_few_args("subTo", 0x0BFA87DE111EEA8DLL, 2, v_u, v_v));
          if (toBoolean(v_ac)) {
            LINE(1481,v_c->t_subto(((Object)(v_a)), ((Object)(v_c))));
          }
          LINE(1483,v_d->t_subto(((Object)(v_b)), ((Object)(v_d))));
        }
      }
    }
  }
  if (!equal(LINE(1486,v_v.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerOne))), 0LL)) {
    return g->GV(BigIntegerZero);
  }
  if (not_less(LINE(1489,v_d->t_compareto(v_m)), 0LL)) {
    return ((Object)(LINE(1490,v_d->t_subtract(v_m))));
  }
  if (less(LINE(1492,v_d->t_signum()), 0LL)) {
    LINE(1493,v_d->t_addto(v_m, ((Object)(v_d))));
  }
  else {
    return ((Object)(v_d));
  }
  if (less(LINE(1497,v_d->t_signum()), 0LL)) {
    return ((Object)(LINE(1498,v_d->t_add(v_m))));
  }
  else {
    return ((Object)(v_d));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1505 */
Variant c_biginteger::t_isprobableprime(Variant v_t) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::IsProbablePrime);
  Variant v_x;
  Variant v_x_array;
  int64 v_i = 0;
  Variant v_m;
  int64 v_j = 0;

  (v_x = LINE(1506,t_abs()));
  (v_x_array = ref(lval(v_x.o_lval("array", 0x063D35168C04B960LL))));
  if (equal(v_x.o_get("t", 0x11C9D2391242AEBCLL), 1LL) && not_more(v_x_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), m_lowprimes.rvalAt(LINE(1508,x_count(m_lowprimes)) - 1LL))) {
    {
      LOOP_COUNTER(66);
      for ((v_i = 0LL); less(v_i, LINE(1509,x_count(m_lowprimes))); ++v_i) {
        LOOP_COUNTER_CHECK(66);
        {
          if (equal(v_x_array.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), m_lowprimes.rvalAt(v_i))) {
            return true;
          }
        }
      }
    }
    return false;
  }
  if (toBoolean(LINE(1516,v_x.o_invoke_few_args("isEven", 0x3B2D21057B10BC8BLL, 0)))) {
    return false;
  }
  (v_i = 1LL);
  LOOP_COUNTER(67);
  {
    while (less(v_i, LINE(1520,x_count(m_lowprimes)))) {
      LOOP_COUNTER_CHECK(67);
      {
        (v_m = m_lowprimes.rvalAt(v_i));
        (v_j = v_i + 1LL);
        LOOP_COUNTER(68);
        {
          while (less(v_j, LINE(1523,x_count(m_lowprimes))) && less(v_m, m_lplim)) {
            LOOP_COUNTER_CHECK(68);
            {
              v_m *= m_lowprimes.rvalAt(v_j++);
            }
          }
        }
        (v_m = LINE(1526,v_x.o_invoke_few_args("modInt", 0x55023DA5CE74B86DLL, 1, v_m)));
        LOOP_COUNTER(69);
        {
          while (less(v_i, v_j)) {
            LOOP_COUNTER_CHECK(69);
            {
              if (equal(modulo(toInt64(v_m), toInt64(m_lowprimes.rvalAt(v_i++))), 0LL)) {
                return false;
              }
            }
          }
        }
      }
    }
  }
  return LINE(1533,v_x.o_invoke_few_args("millerRabin", 0x793171D3BD2BCBC7LL, 1, v_t));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1537 */
bool c_biginteger::t_millerrabin(Variant v_t) {
  INSTANCE_METHOD_INJECTION(BigInteger, BigInteger::MillerRabin);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_n1;
  Variant v_k;
  Variant v_r;
  p_biginteger v_a;
  int64 v_i = 0;
  Variant v_y;
  int64 v_j = 0;

  (v_n1 = ((Object)(LINE(1539,t_subtract(g->GV(BigIntegerOne))))));
  (v_k = LINE(1540,v_n1.o_invoke_few_args("getLowestSetBit", 0x36924875887B5FB3LL, 0)));
  if (not_more(v_k, 0LL)) {
    return false;
  }
  (v_r = LINE(1544,v_n1.o_invoke_few_args("shiftRight", 0x432DC7FA5F22DFE8LL, 1, v_k)));
  (v_t = toInt64((toInt64(v_t + 1LL))) >> 1LL);
  if (more(v_t, LINE(1546,x_count(m_lowprimes)))) {
    (v_t = LINE(1547,x_count(m_lowprimes)));
  }
  ((Object)((v_a = ((Object)(LINE(1549,f_nbi()))))));
  {
    LOOP_COUNTER(70);
    for ((v_i = 0LL); less(v_i, v_t); ++v_i) {
      LOOP_COUNTER_CHECK(70);
      {
        LINE(1551,v_a->t_fromint(m_lowprimes.rvalAt(v_i)));
        (v_y = LINE(1552,v_a->t_modpow(v_r, ((Object)(this)))));
        if (!equal(LINE(1553,v_y.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerOne))), 0LL) && !equal(v_y.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_n1), 0LL)) {
          (v_j = 1LL);
          LOOP_COUNTER(71);
          {
            while (less(v_j++, v_k) && !equal(LINE(1555,v_y.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_n1)), 0LL)) {
              LOOP_COUNTER_CHECK(71);
              {
                (v_y = LINE(1556,v_y.o_invoke_few_args("modPowInt", 0x200163CFF3908AE1LL, 2, 2LL, this)));
                if (equal(LINE(1557,v_y.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, g->GV(BigIntegerOne))), 0LL)) {
                  return false;
                }
              }
            }
          }
          if (!equal(LINE(1561,v_y.o_invoke_few_args("compareTo", 0x461BBC73FE1061ADLL, 1, v_n1)), 0LL)) {
            return false;
          }
        }
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1572 */
Variant c_arcfour::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_arcfour::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_arcfour::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("i", m_i.isReferenced() ? ref(m_i) : m_i));
  props.push_back(NEW(ArrayElement)("j", m_j.isReferenced() ? ref(m_j) : m_j));
  props.push_back(NEW(ArrayElement)("S", m_S));
  c_ObjectData::o_get(props);
}
bool c_arcfour::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x0EB22EDA95766E98LL, i, 1);
      break;
    case 3:
      HASH_EXISTS_STRING(0x6EBE717239E1319BLL, S, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x4B27011F259D2446LL, j, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_arcfour::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x0EB22EDA95766E98LL, m_i,
                         i, 1);
      break;
    case 3:
      HASH_RETURN_STRING(0x6EBE717239E1319BLL, m_S,
                         S, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x4B27011F259D2446LL, m_j,
                         j, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_arcfour::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x0EB22EDA95766E98LL, m_i,
                      i, 1);
      break;
    case 3:
      HASH_SET_STRING(0x6EBE717239E1319BLL, m_S,
                      S, 1);
      break;
    case 6:
      HASH_SET_STRING(0x4B27011F259D2446LL, m_j,
                      j, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_arcfour::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x0EB22EDA95766E98LL, m_i,
                         i, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x4B27011F259D2446LL, m_j,
                         j, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_arcfour::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(arcfour)
ObjectData *c_arcfour::cloneImpl() {
  c_arcfour *obj = NEW(c_arcfour)();
  cloneSet(obj);
  return obj;
}
void c_arcfour::cloneSet(c_arcfour *clone) {
  clone->m_i = m_i.isReferenced() ? ref(m_i) : m_i;
  clone->m_j = m_j.isReferenced() ? ref(m_j) : m_j;
  clone->m_S = m_S;
  ObjectData::cloneSet(clone);
}
Variant c_arcfour::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 3:
      HASH_GUARD(0x614F97E16B435A03LL, init) {
        return (t_init(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_arcfour::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 3:
      HASH_GUARD(0x614F97E16B435A03LL, init) {
        return (t_init(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_arcfour::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_arcfour$os_get(const char *s) {
  return c_arcfour::os_get(s, -1);
}
Variant &cw_arcfour$os_lval(const char *s) {
  return c_arcfour::os_lval(s, -1);
}
Variant cw_arcfour$os_constant(const char *s) {
  return c_arcfour::os_constant(s);
}
Variant cw_arcfour$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_arcfour::os_invoke(c, s, params, -1, fatal);
}
void c_arcfour::init() {
  m_i = null;
  m_j = null;
  m_S = ScalarArrays::sa_[0];
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1578 */
void c_arcfour::t_init(CVarRef v_key) {
  INSTANCE_METHOD_INJECTION(Arcfour, Arcfour::init);
  int64 v_i = 0;
  Variant v_j;
  Variant v_t;

  {
    LOOP_COUNTER(72);
    for ((v_i = 0LL); less(v_i, 256LL); ++v_i) {
      LOOP_COUNTER_CHECK(72);
      {
        m_S.set(v_i, (v_i));
      }
    }
  }
  (v_j = 0LL);
  {
    LOOP_COUNTER(73);
    for ((v_i = 0LL); less(v_i, 256LL); ++v_i) {
      LOOP_COUNTER_CHECK(73);
      {
        (v_j = bitwise_and((v_j + m_S.rvalAt(v_i) + v_key.rvalAt(modulo(v_i, toInt64(LINE(1584,x_count(v_key)))))), 255LL));
        (v_t = m_S.rvalAt(v_i));
        m_S.set(v_i, (m_S.rvalAt(v_j)));
        m_S.set(v_j, (v_t));
      }
    }
  }
  (m_i = 0LL);
  (m_j = 0LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1592 */
Variant c_arcfour::t_next() {
  INSTANCE_METHOD_INJECTION(Arcfour, Arcfour::next);
  Variant v_t;

  (m_i = bitwise_and((m_i + 1LL), 255LL));
  (m_j = bitwise_and((m_j + m_S.rvalAt(m_i)), 255LL));
  (v_t = m_S.rvalAt(m_i));
  m_S.set(m_i, (m_S.rvalAt(m_j)));
  m_S.set(m_j, (v_t));
  return m_S.rvalAt(bitwise_and((v_t + m_S.rvalAt(m_i)), 255LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1900 */
Variant f_byte2hex(CVarRef v_b) {
  FUNCTION_INJECTION(byte2Hex);
  if (less(v_b, 16LL)) {
    return concat("0", LINE(1902,x_base_convert(toString(v_b), 2LL, 16LL)));
  }
  else {
    return LINE(1906,x_base_convert(toString(v_b), 2LL, 16LL));
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 104 */
int64 f_cbit(Variant v_x) {
  FUNCTION_INJECTION(cbit);
  int64 v_r = 0;

  (v_r = 0LL);
  LOOP_COUNTER(74);
  {
    while (!equal(v_x, 0LL)) {
      LOOP_COUNTER_CHECK(74);
      {
        v_x &= v_x - 1LL;
        ++v_r;
      }
    }
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1937 */
Variant f_pkcs1unpad2(CVarRef v_d, int64 v_n) {
  FUNCTION_INJECTION(pkcs1unpad2);
  Variant v_b;
  int64 v_i = 0;
  String v_ret;

  (v_b = LINE(1938,toObject(v_d)->o_invoke_few_args("toByteArray", 0x3D03B239A1E9E902LL, 0)));
  (v_i = 0LL);
  LOOP_COUNTER(75);
  {
    while (less(v_i, LINE(1940,x_count(v_b))) && equal(v_b.rvalAt(v_i), 0LL)) {
      LOOP_COUNTER_CHECK(75);
      {
        ++v_i;
      }
    }
  }
  if (!equal(LINE(1944,x_count(v_b)) - v_i, v_n - 1LL) || !equal(v_b.rvalAt(v_i), 2LL)) {
    return null;
  }
  ++v_i;
  LOOP_COUNTER(76);
  {
    while (!equal(v_b.rvalAt(v_i), 0LL)) {
      LOOP_COUNTER_CHECK(76);
      {
        if (not_less_rev(LINE(1949,x_count(v_b)), ++v_i)) {
          return null;
        }
      }
    }
  }
  (v_ret = "");
  LOOP_COUNTER(77);
  {
    while (less_rev(LINE(1954,x_count(v_b)), ++v_i)) {
      LOOP_COUNTER_CHECK(77);
      {
        concat_assign(v_ret, LINE(1955,x_chr(toInt64(v_b.rvalAt(v_i)))));
      }
    }
  }
  return v_ret;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 153 */
p_biginteger f_parsebigint(CVarRef v_str, int64 v_r) {
  FUNCTION_INJECTION(parseBigInt);
  return ((Object)(LINE(154,p_biginteger(p_biginteger(NEWOBJ(c_biginteger)())->create(v_str, v_r)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 56 */
p_biginteger f_nbi() {
  FUNCTION_INJECTION(nbi);
  return ((Object)(LINE(57,p_biginteger(p_biginteger(NEWOBJ(c_biginteger)())->create(null, null, null)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1911 */
Variant f_pkcs1pad2(CVarRef v_s, int64 v_n) {
  FUNCTION_INJECTION(pkcs1pad2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_ba;
  int64 v_i = 0;
  p_securerandom v_rng;
  Variant v_x;

  if (less(v_n, LINE(1912,x_strlen(toString(v_s))) + 11LL)) {
    echo("Message too long for RSA");
    return null;
  }
  (v_ba = ScalarArrays::sa_[0]);
  (v_i = LINE(1917,x_strlen(toString(v_s))) - 1LL);
  LOOP_COUNTER(78);
  {
    while (not_less(v_i, 0LL) && more(v_n, 0LL)) {
      LOOP_COUNTER_CHECK(78);
      {
        v_ba.set(--v_n, (LINE(1920,x_ord(toString(v_s.rvalAt(v_i--))))));
      }
    }
  }
  v_ba.set(--v_n, (0LL));
  ((Object)((v_rng = ((Object)(LINE(1923,p_securerandom(p_securerandom(NEWOBJ(c_securerandom)())->create(g->GV(rngc)))))))));
  (v_x = ScalarArrays::sa_[0]);
  LOOP_COUNTER(79);
  {
    while (more(v_n, 2LL)) {
      LOOP_COUNTER_CHECK(79);
      {
        v_x.set(0LL, (0LL), 0x77CFA1EEF01BCA90LL);
        LOOP_COUNTER(80);
        {
          while (equal(v_x.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0LL)) {
            LOOP_COUNTER_CHECK(80);
            {
              LINE(1928,v_rng->t_nextbytes(ref(v_x)));
            }
          }
        }
        v_ba.set(--v_n, (v_x.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
      }
    }
  }
  v_ba.set(--v_n, (2LL));
  v_ba.set(--v_n, (0LL));
  return ((Object)(LINE(1934,p_biginteger(p_biginteger(NEWOBJ(c_biginteger)())->create(v_ba)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 61 */
p_biginteger f_nbv(Numeric v_i) {
  FUNCTION_INJECTION(nbv);
  p_biginteger v_r;

  ((Object)((v_r = ((Object)(LINE(62,f_nbi()))))));
  LINE(63,v_r->t_fromint(v_i));
  return ((Object)(v_r));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 71 */
Variant f_intat(CVarRef v_s, int64 v_i) {
  FUNCTION_INJECTION(intAt);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_c;

  (v_c = g->GV(BI_RC).rvalAt(LINE(73,x_ord(toString(v_s.rvalAt(v_i))))));
  return (LINE(74,x_is_null(v_c))) ? ((Variant)(-1LL)) : ((Variant)(v_c));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1891 */
String f_linebrk(CVarRef v_s, CVarRef v_n) {
  FUNCTION_INJECTION(linebrk);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_ret;
  Variant v_i;

  (v_ret = "");
  (v_i = 0LL);
  LOOP_COUNTER(81);
  {
    while (less(v_i + v_n, LINE(1894,x_count(v_s)))) {
      LOOP_COUNTER_CHECK(81);
      {
        concat_assign(v_ret, concat(toString(LINE(1895,x_substr(toString(v_s), toInt32(v_i), toInt32(v_i + v_n)))), "\n"));
        v_i += v_n;
      }
    }
  }
  return concat(v_ret, toString(LINE(1898,(assignCallTemp(eo_0, toString(v_s)),assignCallTemp(eo_1, toInt32(v_i)),assignCallTemp(eo_2, x_count(v_s)),x_substr(eo_0, eo_1, eo_2)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 2104 */
void f_encrypt() {
  FUNCTION_INJECTION(encrypt);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_nValue __attribute__((__unused__)) = g->GV(nValue);
  Variant &gv_eValue __attribute__((__unused__)) = g->GV(eValue);
  Variant &gv_dValue __attribute__((__unused__)) = g->GV(dValue);
  Variant &gv_pValue __attribute__((__unused__)) = g->GV(pValue);
  Variant &gv_qValue __attribute__((__unused__)) = g->GV(qValue);
  Variant &gv_dmp1Value __attribute__((__unused__)) = g->GV(dmp1Value);
  Variant &gv_dmq1Value __attribute__((__unused__)) = g->GV(dmq1Value);
  Variant &gv_coeffValue __attribute__((__unused__)) = g->GV(coeffValue);
  Variant &gv_TEXT __attribute__((__unused__)) = g->GV(TEXT);
  Variant &gv_encrypted __attribute__((__unused__)) = g->GV(encrypted);
  p_rsakey v_RSA;

  {
  }
  ((Object)((v_RSA = ((Object)(LINE(2106,p_rsakey(p_rsakey(NEWOBJ(c_rsakey)())->create())))))));
  LINE(2107,v_RSA->t_setpublic(gv_nValue, gv_eValue));
  LINE(2108,v_RSA->t_setprivateex(gv_nValue, gv_eValue, gv_dValue, gv_pValue, gv_qValue, gv_dmp1Value, gv_dmq1Value, gv_coeffValue));
  (gv_encrypted = LINE(2109,v_RSA->t_encrypt(gv_TEXT)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 67 */
Variant f_int2char(CVarRef v_n) {
  FUNCTION_INJECTION(int2char);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->GV(BI_RM).rvalAt(v_n);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 44 */
Variant f_urs(Variant v_a, int64 v_b) {
  FUNCTION_INJECTION(urs);
  v_a &= 0.0;
  v_b &= 31LL;
  if (toBoolean(bitwise_and(v_a, 0.0)) && more(v_b, 0LL)) {
    (v_a = bitwise_and((toInt64(toInt64(v_a)) >> 1LL), 2147483647LL));
    (v_a = toInt64(toInt64(v_a)) >> (v_b - 1LL));
  }
  else {
    (v_a = (toInt64(toInt64(v_a)) >> v_b));
  }
  return v_a;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 2111 */
void f_decrypt() {
  FUNCTION_INJECTION(decrypt);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_nValue __attribute__((__unused__)) = g->GV(nValue);
  Variant &gv_eValue __attribute__((__unused__)) = g->GV(eValue);
  Variant &gv_dValue __attribute__((__unused__)) = g->GV(dValue);
  Variant &gv_pValue __attribute__((__unused__)) = g->GV(pValue);
  Variant &gv_qValue __attribute__((__unused__)) = g->GV(qValue);
  Variant &gv_dmp1Value __attribute__((__unused__)) = g->GV(dmp1Value);
  Variant &gv_dmq1Value __attribute__((__unused__)) = g->GV(dmq1Value);
  Variant &gv_coeffValue __attribute__((__unused__)) = g->GV(coeffValue);
  Variant &gv_TEXT __attribute__((__unused__)) = g->GV(TEXT);
  Variant &gv_encrypted __attribute__((__unused__)) = g->GV(encrypted);
  p_rsakey v_RSA;
  Variant v_decrypted;

  {
  }
  ((Object)((v_RSA = ((Object)(LINE(2113,p_rsakey(p_rsakey(NEWOBJ(c_rsakey)())->create())))))));
  LINE(2114,v_RSA->t_setpublic(gv_nValue, gv_eValue));
  LINE(2115,v_RSA->t_setprivateex(gv_nValue, gv_eValue, gv_dValue, gv_pValue, gv_qValue, gv_dmp1Value, gv_dmq1Value, gv_coeffValue));
  (v_decrypted = LINE(2116,v_RSA->t_decrypt(gv_encrypted)));
  if (!equal(v_decrypted, gv_TEXT)) {
    echo("Crypto operation failed");
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 77 */
int64 f_nbits(Variant v_x) {
  FUNCTION_INJECTION(nbits);
  int64 v_r = 0;
  Variant v_t;

  (v_r = 1LL);
  if (!equal(((v_t = LINE(80,f_urs(v_x, 16LL)))), 0LL)) {
    (v_x = v_t);
    v_r += 16LL;
  }
  if (!equal(((v_t = toInt64(toInt64(v_x)) >> 8LL)), 0LL)) {
    (v_x = v_t);
    v_r += 8LL;
  }
  if (!equal(((v_t = toInt64(toInt64(v_x)) >> 4LL)), 0LL)) {
    (v_x = v_t);
    v_r += 4LL;
  }
  if (!equal(((v_t = toInt64(toInt64(v_x)) >> 2LL)), 0LL)) {
    (v_x = v_t);
    v_r += 2LL;
  }
  if (!equal(((v_t = toInt64(toInt64(v_x)) >> 1LL)), 0LL)) {
    (v_x = v_t);
    v_r += 1LL;
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 115 */
int64 f_lbit(Variant v_x) {
  FUNCTION_INJECTION(lbit);
  int64 v_r = 0;

  if (equal(v_x, 0LL)) {
    return -1LL;
  }
  (v_r = 0LL);
  if (equal((bitwise_and(v_x, 65535LL)), 0LL)) {
    v_x >>= 16LL;
    v_r += 16LL;
  }
  if (equal((bitwise_and(v_x, 255LL)), 0LL)) {
    v_x >>= 8LL;
    v_r += 8LL;
  }
  if (equal((bitwise_and(v_x, 15LL)), 0LL)) {
    v_x >>= 4LL;
    v_r += 4LL;
  }
  if (equal((bitwise_and(v_x, 3LL)), 0LL)) {
    v_x >>= 2LL;
    v_r += 2LL;
  }
  if (equal((bitwise_and(v_x, 1LL)), 0LL)) {
    ++v_r;
  }
  return v_r;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 1813 */
p_arcfour f_prng_newstate() {
  FUNCTION_INJECTION(prng_newstate);
  return ((Object)(LINE(1814,p_arcfour(p_arcfour(NEWOBJ(c_arcfour)())->create()))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php line 146 */
void f_setlength(Variant v_array, int64 v_length) {
  FUNCTION_INJECTION(setLength);
  int64 v_i = 0;

  {
    LOOP_COUNTER(82);
    for ((v_i = 0LL); less(v_i, v_length); v_i++) {
      LOOP_COUNTER_CHECK(82);
      {
        v_array.append((""));
      }
    }
  }
} /* function */
Object co_montgomery(CArrRef params, bool init /* = true */) {
  return Object(p_montgomery(NEW(c_montgomery)())->dynCreate(params, init));
}
Object co_securerandom(CArrRef params, bool init /* = true */) {
  return Object(p_securerandom(NEW(c_securerandom)())->dynCreate(params, init));
}
Object co_rng(CArrRef params, bool init /* = true */) {
  return Object(p_rng(NEW(c_rng)())->dynCreate(params, init));
}
Object co_rsakey(CArrRef params, bool init /* = true */) {
  return Object(p_rsakey(NEW(c_rsakey)())->dynCreate(params, init));
}
Object co_barrett(CArrRef params, bool init /* = true */) {
  return Object(p_barrett(NEW(c_barrett)())->dynCreate(params, init));
}
Object co_nullexp(CArrRef params, bool init /* = true */) {
  return Object(p_nullexp(NEW(c_nullexp)())->dynCreate(params, init));
}
Object co_classic(CArrRef params, bool init /* = true */) {
  return Object(p_classic(NEW(c_classic)())->dynCreate(params, init));
}
Object co_biginteger(CArrRef params, bool init /* = true */) {
  return Object(p_biginteger(NEW(c_biginteger)())->dynCreate(params, init));
}
Object co_arcfour(CArrRef params, bool init /* = true */) {
  return Object(p_arcfour(NEW(c_arcfour)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_BigIntegerZero __attribute__((__unused__)) = (variables != gVariables) ? variables->get("BigIntegerZero") : g->GV(BigIntegerZero);
  Variant &v_BigIntegerOne __attribute__((__unused__)) = (variables != gVariables) ? variables->get("BigIntegerOne") : g->GV(BigIntegerOne);
  Variant &v_BI_RM __attribute__((__unused__)) = (variables != gVariables) ? variables->get("BI_RM") : g->GV(BI_RM);
  Variant &v_BI_RC __attribute__((__unused__)) = (variables != gVariables) ? variables->get("BI_RC") : g->GV(BI_RC);
  Variant &v_rr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rr") : g->GV(rr);
  Variant &v_vv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("vv") : g->GV(vv);
  Variant &v_rngc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rngc") : g->GV(rngc);
  Variant &v_nValue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("nValue") : g->GV(nValue);
  Variant &v_eValue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("eValue") : g->GV(eValue);
  Variant &v_dValue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dValue") : g->GV(dValue);
  Variant &v_pValue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pValue") : g->GV(pValue);
  Variant &v_qValue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("qValue") : g->GV(qValue);
  Variant &v_dmp1Value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dmp1Value") : g->GV(dmp1Value);
  Variant &v_dmq1Value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dmq1Value") : g->GV(dmq1Value);
  Variant &v_coeffValue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("coeffValue") : g->GV(coeffValue);
  Variant &v_TEXT __attribute__((__unused__)) = (variables != gVariables) ? variables->get("TEXT") : g->GV(TEXT);
  Variant &v_encrypted __attribute__((__unused__)) = (variables != gVariables) ? variables->get("encrypted") : g->GV(encrypted);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  LINE(2,x_error_reporting(toInt32(8191LL)));
  (v_BigIntegerZero = ((Object)(LINE(4,f_nbv(0LL)))));
  (v_BigIntegerOne = ((Object)(LINE(5,f_nbv(1LL)))));
  ;
  ;
  ;
  ;
  LINE(12,g->declareConstant("BI_FV", g->k_BI_FV, x_pow(2LL, 52LL /* BI_FP */)));
  ;
  ;
  (v_BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz");
  (v_BI_RC = ScalarArrays::sa_[0]);
  (v_rr = LINE(19,x_ord("0")));
  (v_vv = "");
  {
    LOOP_COUNTER(83);
    for ((v_vv = 0LL); not_more(v_vv, 9LL); ++v_vv) {
      LOOP_COUNTER_CHECK(83);
      {
        v_BI_RC.set(v_rr++, (v_vv));
      }
    }
  }
  (v_rr = LINE(24,x_ord("a")));
  {
    LOOP_COUNTER(84);
    for ((v_vv = 10LL); less(v_vv, 36LL); ++v_vv) {
      LOOP_COUNTER_CHECK(84);
      {
        v_BI_RC.set(v_rr++, (v_vv));
      }
    }
  }
  (v_rr = LINE(28,x_ord("A")));
  {
    LOOP_COUNTER(85);
    for ((v_vv = 10LL); less(v_vv, 36LL); ++v_vv) {
      LOOP_COUNTER_CHECK(85);
      {
        v_BI_RC.set(v_rr++, (v_vv));
      }
    }
  }
  (v_rngc = ((Object)(LINE(32,p_rng(p_rng(NEWOBJ(c_rng)())->create())))));
  LINE(33,v_rngc.o_invoke_few_args("init", 0x614F97E16B435A03LL, 0));
  (v_nValue = "a5261939975948bb7a58dffe5ff54e65f0498f9175f5a09288810b8975871e99af3b5dd94057b0fc07535f5f97444504fa35169d461d0d30cf0192e307727c065168c788771c561a9400fb49175e9e6aa4e23fe11af69e9412dd23b0cb6684c4c2429bce139e848ab26d0829073351f4acd36074eafd036a5eb83359d2a698d3");
  (v_eValue = "10001");
  (v_dValue = "8e9912f6d3645894e8d38cb58c0db81ff516cf4c7e5a14c7f1eddb1459d2cded4d8d293fc97aee6aefb861859c8b6a3d1dfe710463e1f9ddc72048c09751971c4a580aa51eb523357a3cc48d31cfad1d4a165066ed92d4748fb6571211da5cb14bc11b6e2df7c1a559e6d5ac1cd5c94703a22891464fba23d0d965086277a161");
  (v_pValue = "d090ce58a92c75233a6486cb0a9209bf3583b64f540c76f5294bb97d285eed33aec220bde14b2417951178ac152ceab6da7090905b478195498b352048f15e7d");
  (v_qValue = "cab575dc652bb66df15a0359609d51d1db184750c00c6698b90ef3465c99655103edbf0d54c56aec0ce3c4d22592338092a126a0cc49f65a4a30d222b411e58f");
  (v_dmp1Value = "1a24bca8e273df2f0e47c199bbf678604e7df7215480c77c8db39f49b000ce2cf7500038acfff5433b7d582a01f1826e6f4d42e1c57f5e1fef7b12aabc59fd25");
  (v_dmq1Value = "3d06982efbbe47339e1f6d36b1216b8a741d410b0c662f54f7118b27b9a4ec9d914337eb39841d8666f3034408cf94f5b62f11c402fc994fe15a05493150d9fd");
  (v_coeffValue = "3a3e731acd8960b7ff9eb81a7ff93bd1cfa74cbd56987db58b4594fb09c09084db1734c8143f98b602b981aaa9243ca28deb69b5b280ee8dcee0fd2625e53250");
  (v_TEXT = "Brothers what we do in life echoes in eternity");
  (v_encrypted = "");
  {
    LOOP_COUNTER(86);
    for ((v_i = 0LL); less(v_i, 7LL); v_i++) {
      LOOP_COUNTER_CHECK(86);
      {
        LINE(2124,f_encrypt());
        LINE(2125,f_decrypt());
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
