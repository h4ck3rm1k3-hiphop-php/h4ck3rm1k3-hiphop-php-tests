
#ifndef __GENERATED_cls_whirlmemory_h__
#define __GENERATED_cls_whirlmemory_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 38 */
class c_whirlmemory : virtual public ObjectData {
  BEGIN_CLASS_MAP(whirlmemory)
  END_CLASS_MAP(whirlmemory)
  DECLARE_CLASS(whirlmemory, WhirlMemory, ObjectData)
  void init();
  public: Variant m__values;
  public: Variant m__position;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t__init();
  public: static Variant ti_instance(const char* cls);
  public: void t_moveby(CVarRef v_count);
  public: Variant t_getvalue();
  public: void t_setvalue(CVarRef v_data);
  public: static Variant t_instance() { return ti_instance("whirlmemory"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_whirlmemory_h__
