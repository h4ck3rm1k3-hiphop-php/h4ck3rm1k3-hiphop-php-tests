
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_whirl.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_whirl_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_whirl.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_whirl_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_whirl __attribute__((__unused__)) = (variables != gVariables) ? variables->get("whirl") : g->GV(whirl);

  LINE(10,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlParser_php(true, variables));
  (v_filename = "test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/keymaker.wr");
  (v_whirl = ((Object)(LINE(13,p_whirlparser(p_whirlparser(NEWOBJ(c_whirlparser)())->create())))));
  LINE(14,v_whirl.o_invoke_few_args("loadfile", 0x6CB75C28290C8A9ELL, 1, v_filename));
  LINE(15,v_whirl.o_invoke_few_args("parse", 0x46463F1C3624CEDELL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
