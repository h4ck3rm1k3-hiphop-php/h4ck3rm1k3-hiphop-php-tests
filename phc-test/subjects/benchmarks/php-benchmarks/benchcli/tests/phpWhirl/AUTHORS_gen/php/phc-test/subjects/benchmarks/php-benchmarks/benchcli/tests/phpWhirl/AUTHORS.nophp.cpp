
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/AUTHORS.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$AUTHORS(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/AUTHORS);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$AUTHORS;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Core:\n\nJakob Westhoff <jakob@westhoffswelt.de>\n\t- main work\n\n\nThanks to:\n\nBigZaphod ");
  echo("<S");
  echo("ean@fifthhace.com>\n\t- for inventing the Whirl Language and writing the first\n\t  c++ interpreter for it.\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
