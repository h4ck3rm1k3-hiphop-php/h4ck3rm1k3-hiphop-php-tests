
#ifndef __GENERATED_cls_whirlparser_h__
#define __GENERATED_cls_whirlparser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 42 */
class c_whirlparser : virtual public ObjectData {
  BEGIN_CLASS_MAP(whirlparser)
  END_CLASS_MAP(whirlparser)
  DECLARE_CLASS(whirlparser, WhirlParser, ObjectData)
  void init();
  public: Array m__LANGUAGE;
  public: Variant m__WhirlMemory;
  public: Variant m__program;
  public: Variant m__currentPosition;
  public: Variant m__rings;
  public: Variant m__currentRing;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t__init();
  public: static Variant ti_instance(const char* cls);
  public: void t__setnextringactive();
  public: Variant t__cleancode(CVarRef v_code);
  public: Array t__preparecode(CVarRef v_code);
  public: void t_load(CVarRef v_code);
  public: void t_loadfile(CVarRef v_filename);
  public: void t_parse();
  public: void t_programexit();
  public: void t_padd(CVarRef v_count);
  public: Numeric t_getinstructionnumber();
  public: String t_input();
  public: void t_output(CVarRef v_output);
  public: static Variant t_instance() { return ti_instance("whirlparser"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_whirlparser_h__
