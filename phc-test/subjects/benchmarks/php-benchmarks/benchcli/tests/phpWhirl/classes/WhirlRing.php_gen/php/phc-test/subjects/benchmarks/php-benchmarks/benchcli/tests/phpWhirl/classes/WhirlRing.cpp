
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 38 */
Variant c_whirlring::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_whirlring::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_whirlring::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_value", m__value.isReferenced() ? ref(m__value) : m__value));
  props.push_back(NEW(ArrayElement)("_position", m__position.isReferenced() ? ref(m__position) : m__position));
  props.push_back(NEW(ArrayElement)("_direction", m__direction.isReferenced() ? ref(m__direction) : m__direction));
  props.push_back(NEW(ArrayElement)("_commands", m__commands));
  c_ObjectData::o_get(props);
}
bool c_whirlring::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x4AB1F665416CA431LL, _position, 9);
      HASH_EXISTS_STRING(0x5AFB664A3CDEC0F1LL, _direction, 10);
      HASH_EXISTS_STRING(0x5F620914C259D0D9LL, _commands, 9);
      break;
    case 4:
      HASH_EXISTS_STRING(0x18CE1079F757C074LL, _value, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_whirlring::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x4AB1F665416CA431LL, m__position,
                         _position, 9);
      HASH_RETURN_STRING(0x5AFB664A3CDEC0F1LL, m__direction,
                         _direction, 10);
      HASH_RETURN_STRING(0x5F620914C259D0D9LL, m__commands,
                         _commands, 9);
      break;
    case 4:
      HASH_RETURN_STRING(0x18CE1079F757C074LL, m__value,
                         _value, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_whirlring::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x4AB1F665416CA431LL, m__position,
                      _position, 9);
      HASH_SET_STRING(0x5AFB664A3CDEC0F1LL, m__direction,
                      _direction, 10);
      HASH_SET_STRING(0x5F620914C259D0D9LL, m__commands,
                      _commands, 9);
      break;
    case 4:
      HASH_SET_STRING(0x18CE1079F757C074LL, m__value,
                      _value, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_whirlring::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x4AB1F665416CA431LL, m__position,
                         _position, 9);
      HASH_RETURN_STRING(0x5AFB664A3CDEC0F1LL, m__direction,
                         _direction, 10);
      break;
    case 4:
      HASH_RETURN_STRING(0x18CE1079F757C074LL, m__value,
                         _value, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_whirlring::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(whirlring)
ObjectData *c_whirlring::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_whirlring::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_whirlring::cloneImpl() {
  c_whirlring *obj = NEW(c_whirlring)();
  cloneSet(obj);
  return obj;
}
void c_whirlring::cloneSet(c_whirlring *clone) {
  clone->m__value = m__value.isReferenced() ? ref(m__value) : m__value;
  clone->m__position = m__position.isReferenced() ? ref(m__position) : m__position;
  clone->m__direction = m__direction.isReferenced() ? ref(m__direction) : m__direction;
  clone->m__commands = m__commands;
  ObjectData::cloneSet(clone);
}
Variant c_whirlring::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_whirlring::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_whirlring::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_whirlring$os_get(const char *s) {
  return c_whirlring::os_get(s, -1);
}
Variant &cw_whirlring$os_lval(const char *s) {
  return c_whirlring::os_lval(s, -1);
}
Variant cw_whirlring$os_constant(const char *s) {
  return c_whirlring::os_constant(s);
}
Variant cw_whirlring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_whirlring::os_invoke(c, s, params, -1, fatal);
}
void c_whirlring::init() {
  m__value = null;
  m__position = null;
  m__direction = null;
  m__commands = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 44 */
void c_whirlring::t___construct() {
  INSTANCE_METHOD_INJECTION(WhirlRing, WhirlRing::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(45,t__init());
  LINE(46,o_root_invoke_few_args("initCommandsArray", 0x78E78D245AC277D8LL, 0));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 49 */
void c_whirlring::t__init() {
  INSTANCE_METHOD_INJECTION(WhirlRing, WhirlRing::_init);
  (m__value = 0LL);
  (m__position = 0LL);
  (m__direction = 1LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 55 */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 57 */
void c_whirlring::t__setvalue(CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(WhirlRing, WhirlRing::_setValue);
  (m__value = v_value);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 61 */
Variant c_whirlring::t__getvalue() {
  INSTANCE_METHOD_INJECTION(WhirlRing, WhirlRing::_getValue);
  return m__value;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 65 */
void c_whirlring::t_reversedirection() {
  INSTANCE_METHOD_INJECTION(WhirlRing, WhirlRing::reverseDirection);
  if (same(m__direction, 1LL)) {
    (m__direction = -1LL);
  }
  else {
    (m__direction = 1LL);
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 75 */
void c_whirlring::t_rotate() {
  INSTANCE_METHOD_INJECTION(WhirlRing, WhirlRing::rotate);
  m__position += m__direction;
  if (less(m__position, 0LL)) {
    (m__position = LINE(78,x_count(m__commands)) - 1LL);
  }
  else if (more(m__position, LINE(79,x_count(m__commands)) - 1LL)) {
    (m__position = 0LL);
  }
} /* function */
Object co_whirlring(CArrRef params, bool init /* = true */) {
  return Object(p_whirlring(NEW(c_whirlring)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlRing_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlRing_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
