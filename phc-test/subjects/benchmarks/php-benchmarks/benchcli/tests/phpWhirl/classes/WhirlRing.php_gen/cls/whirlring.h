
#ifndef __GENERATED_cls_whirlring_h__
#define __GENERATED_cls_whirlring_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.php line 38 */
class c_whirlring : virtual public ObjectData {
  BEGIN_CLASS_MAP(whirlring)
  END_CLASS_MAP(whirlring)
  DECLARE_CLASS(whirlring, WhirlRing, ObjectData)
  void init();
  public: Variant m__value;
  public: Variant m__position;
  public: Variant m__direction;
  public: String m__commands;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t__init();
  // public: void t_initcommandsarray() = 0;
  public: void t__setvalue(CVarRef v_value);
  public: Variant t__getvalue();
  public: void t_reversedirection();
  public: void t_rotate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_whirlring_h__
