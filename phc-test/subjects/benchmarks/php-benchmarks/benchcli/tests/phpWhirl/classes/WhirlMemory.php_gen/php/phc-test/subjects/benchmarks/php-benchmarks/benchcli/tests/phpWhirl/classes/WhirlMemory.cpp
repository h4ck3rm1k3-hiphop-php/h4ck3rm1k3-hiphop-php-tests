
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 38 */
Variant c_whirlmemory::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x48C0A6C6043C78F4LL, g->s_whirlmemory_DupId_instance,
                  _instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_whirlmemory::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x48C0A6C6043C78F4LL, g->s_whirlmemory_DupId_instance,
                  _instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_whirlmemory::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_values", m__values.isReferenced() ? ref(m__values) : m__values));
  props.push_back(NEW(ArrayElement)("_position", m__position.isReferenced() ? ref(m__position) : m__position));
  c_ObjectData::o_get(props);
}
bool c_whirlmemory::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x4AB1F665416CA431LL, _position, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x51687271F0BF0122LL, _values, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_whirlmemory::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x4AB1F665416CA431LL, m__position,
                         _position, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x51687271F0BF0122LL, m__values,
                         _values, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_whirlmemory::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x4AB1F665416CA431LL, m__position,
                      _position, 9);
      break;
    case 2:
      HASH_SET_STRING(0x51687271F0BF0122LL, m__values,
                      _values, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_whirlmemory::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x4AB1F665416CA431LL, m__position,
                         _position, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x51687271F0BF0122LL, m__values,
                         _values, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_whirlmemory::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(whirlmemory)
ObjectData *c_whirlmemory::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_whirlmemory::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_whirlmemory::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_whirlmemory::cloneImpl() {
  c_whirlmemory *obj = NEW(c_whirlmemory)();
  cloneSet(obj);
  return obj;
}
void c_whirlmemory::cloneSet(c_whirlmemory *clone) {
  clone->m__values = m__values.isReferenced() ? ref(m__values) : m__values;
  clone->m__position = m__position.isReferenced() ? ref(m__position) : m__position;
  ObjectData::cloneSet(clone);
}
Variant c_whirlmemory::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_whirlmemory::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_whirlmemory::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_whirlmemory$os_get(const char *s) {
  return c_whirlmemory::os_get(s, -1);
}
Variant &cw_whirlmemory$os_lval(const char *s) {
  return c_whirlmemory::os_lval(s, -1);
}
Variant cw_whirlmemory$os_constant(const char *s) {
  return c_whirlmemory::os_constant(s);
}
Variant cw_whirlmemory$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_whirlmemory::os_invoke(c, s, params, -1, fatal);
}
void c_whirlmemory::init() {
  m__values = null;
  m__position = null;
}
void c_whirlmemory::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_whirlmemory_DupId_instance = null;
}
void csi_whirlmemory() {
  c_whirlmemory::os_static_initializer();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 44 */
void c_whirlmemory::t___construct() {
  INSTANCE_METHOD_INJECTION(WhirlMemory, WhirlMemory::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(45,t__init());
  if (!(isset(g->s_whirlmemory_DupId_instance))) {
    (g->s_whirlmemory_DupId_instance = ((Object)(this)));
  }
  else {
    f_exit("WhirlMemory is a Singleton, you can only instantiate one object of this class. Please use instance() instead.");
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 53 */
void c_whirlmemory::t__init() {
  INSTANCE_METHOD_INJECTION(WhirlMemory, WhirlMemory::_init);
  (m__values = ScalarArrays::sa_[0]);
  (m__position = 0LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 58 */
Variant c_whirlmemory::ti_instance(const char* cls) {
  STATIC_METHOD_INJECTION(WhirlMemory, WhirlMemory::instance);
  DECLARE_GLOBAL_VARIABLES(g);
  if (!(isset(g->s_whirlmemory_DupId_instance))) {
    (g->s_whirlmemory_DupId_instance = ((Object)(LINE(60,p_whirlmemory(p_whirlmemory(NEWOBJ(c_whirlmemory)())->create())))));
  }
  return g->s_whirlmemory_DupId_instance;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 65 */
void c_whirlmemory::t_moveby(CVarRef v_count) {
  INSTANCE_METHOD_INJECTION(WhirlMemory, WhirlMemory::moveby);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  m__position += v_count;
  if (less(m__position, 0LL)) {
    f_exit(LINE(68,(assignCallTemp(eo_1, toString((assignCallTemp(eo_3, toObject(throw_fatal("unknown class whirlparser", ((void*)NULL)))),eo_3.o_invoke_few_args("getInstructionNumber", 0x264763628329C784LL, 0)))),concat3("Error in instruction ", eo_1, ": Memory index out of bounds. The memory index was decreased beyond zero."))));
  }
  if (!(isset(m__values, m__position))) {
    m__values.set(m__position, (0LL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 75 */
Variant c_whirlmemory::t_getvalue() {
  INSTANCE_METHOD_INJECTION(WhirlMemory, WhirlMemory::getValue);
  return m__values.rvalAt(m__position);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php line 82 */
void c_whirlmemory::t_setvalue(CVarRef v_data) {
  INSTANCE_METHOD_INJECTION(WhirlMemory, WhirlMemory::setValue);
  m__values.set(m__position, (v_data));
} /* function */
Object co_whirlmemory(CArrRef params, bool init /* = true */) {
  return Object(p_whirlmemory(NEW(c_whirlmemory)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlMemory_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlMemory_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
