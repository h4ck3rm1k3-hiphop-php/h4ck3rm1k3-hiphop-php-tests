
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_whirlopsring(CArrRef params, bool init = true);
Variant cw_whirlopsring$os_get(const char *s);
Variant &cw_whirlopsring$os_lval(const char *s);
Variant cw_whirlopsring$os_constant(const char *s);
Variant cw_whirlopsring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_whirlring(CArrRef params, bool init = true);
Variant cw_whirlring$os_get(const char *s);
Variant &cw_whirlring$os_lval(const char *s);
Variant cw_whirlring$os_constant(const char *s);
Variant cw_whirlring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_whirlmemory(CArrRef params, bool init = true);
Variant cw_whirlmemory$os_get(const char *s);
Variant &cw_whirlmemory$os_lval(const char *s);
Variant cw_whirlmemory$os_constant(const char *s);
Variant cw_whirlmemory$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_whirlparser(CArrRef params, bool init = true);
Variant cw_whirlparser$os_get(const char *s);
Variant &cw_whirlparser$os_lval(const char *s);
Variant cw_whirlparser$os_constant(const char *s);
Variant cw_whirlparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_whirlmathring(CArrRef params, bool init = true);
Variant cw_whirlmathring$os_get(const char *s);
Variant &cw_whirlmathring$os_lval(const char *s);
Variant cw_whirlmathring$os_constant(const char *s);
Variant cw_whirlmathring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_CREATE_OBJECT(0x6E8B3ABA5FD99262LL, whirlmathring);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x5B27A395AB59EC07LL, whirlring);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x23152A92A99B412ALL, whirlmemory);
      break;
    case 11:
      HASH_CREATE_OBJECT(0x55E7F3188D38B11BLL, whirlparser);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x03F257C8B9FB776CLL, whirlopsring);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x6E8B3ABA5FD99262LL, whirlmathring);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x5B27A395AB59EC07LL, whirlring);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x23152A92A99B412ALL, whirlmemory);
      break;
    case 11:
      HASH_INVOKE_STATIC_METHOD(0x55E7F3188D38B11BLL, whirlparser);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x03F257C8B9FB776CLL, whirlopsring);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x6E8B3ABA5FD99262LL, whirlmathring);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x5B27A395AB59EC07LL, whirlring);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x23152A92A99B412ALL, whirlmemory);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY(0x55E7F3188D38B11BLL, whirlparser);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x03F257C8B9FB776CLL, whirlopsring);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x6E8B3ABA5FD99262LL, whirlmathring);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x5B27A395AB59EC07LL, whirlring);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x23152A92A99B412ALL, whirlmemory);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY_LV(0x55E7F3188D38B11BLL, whirlparser);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x03F257C8B9FB776CLL, whirlopsring);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x6E8B3ABA5FD99262LL, whirlmathring);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x5B27A395AB59EC07LL, whirlring);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x23152A92A99B412ALL, whirlmemory);
      break;
    case 11:
      HASH_GET_CLASS_CONSTANT(0x55E7F3188D38B11BLL, whirlparser);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x03F257C8B9FB776CLL, whirlopsring);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
