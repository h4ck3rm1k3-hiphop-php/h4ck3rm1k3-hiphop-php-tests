
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 41 */
Variant c_whirlmathring::os_get(const char *s, int64 hash) {
  return c_whirlring::os_get(s, hash);
}
Variant &c_whirlmathring::os_lval(const char *s, int64 hash) {
  return c_whirlring::os_lval(s, hash);
}
void c_whirlmathring::o_get(ArrayElementVec &props) const {
  c_whirlring::o_get(props);
}
bool c_whirlmathring::o_exists(CStrRef s, int64 hash) const {
  return c_whirlring::o_exists(s, hash);
}
Variant c_whirlmathring::o_get(CStrRef s, int64 hash) {
  return c_whirlring::o_get(s, hash);
}
Variant c_whirlmathring::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_whirlring::o_set(s, hash, v, forInit);
}
Variant &c_whirlmathring::o_lval(CStrRef s, int64 hash) {
  return c_whirlring::o_lval(s, hash);
}
Variant c_whirlmathring::os_constant(const char *s) {
  return c_whirlring::os_constant(s);
}
IMPLEMENT_CLASS(whirlmathring)
ObjectData *c_whirlmathring::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_whirlmathring::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_whirlmathring::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_whirlmathring::cloneImpl() {
  c_whirlmathring *obj = NEW(c_whirlmathring)();
  cloneSet(obj);
  return obj;
}
void c_whirlmathring::cloneSet(c_whirlmathring *clone) {
  c_whirlring::cloneSet(clone);
}
Variant c_whirlmathring::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 7:
      HASH_GUARD(0x58CA90E42B3D6957LL, reversedirection) {
        return (t_reversedirection(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x78E78D245AC277D8LL, initcommandsarray) {
        return (t_initcommandsarray(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x6770944C6053865BLL, rotate) {
        return (t_rotate(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_whirlring::o_invoke(s, params, hash, fatal);
}
Variant c_whirlmathring::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 7:
      HASH_GUARD(0x58CA90E42B3D6957LL, reversedirection) {
        return (t_reversedirection(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x78E78D245AC277D8LL, initcommandsarray) {
        return (t_initcommandsarray(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x6770944C6053865BLL, rotate) {
        return (t_rotate(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_whirlring::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_whirlmathring::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_whirlring::os_invoke(c, s, params, hash, fatal);
}
Variant cw_whirlmathring$os_get(const char *s) {
  return c_whirlmathring::os_get(s, -1);
}
Variant &cw_whirlmathring$os_lval(const char *s) {
  return c_whirlmathring::os_lval(s, -1);
}
Variant cw_whirlmathring$os_constant(const char *s) {
  return c_whirlmathring::os_constant(s);
}
Variant cw_whirlmathring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_whirlmathring::os_invoke(c, s, params, -1, fatal);
}
void c_whirlmathring::init() {
  c_whirlring::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 43 */
void c_whirlmathring::t___construct() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(44,c_whirlring::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 47 */
void c_whirlmathring::t_initcommandsarray() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::initCommandsArray);
  (m__commands = ScalarArrays::sa_[3]);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 51 */
void c_whirlmathring::t_ringfunc_noop() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Noop);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 55 */
void c_whirlmathring::t_ringfunc_load() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Load);
  Variant eo_0;
  LINE(56,t__setvalue((assignCallTemp(eo_0, toObject(c_whirlmemory::t_instance())),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 59 */
void c_whirlmathring::t_ringfunc_store() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Store);
  Variant eo_0;
  (assignCallTemp(eo_0, toObject(LINE(60,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("setValue", 0x36FBED35008C8DB5LL, 1, t__getvalue()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 63 */
void c_whirlmathring::t_ringfunc_add() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Add);
  Variant eo_0;
  LINE(64,t__setvalue(plus_rev((assignCallTemp(eo_0, toObject(c_whirlmemory::t_instance())),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), t__getvalue())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 67 */
void c_whirlmathring::t_ringfunc_mult() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Mult);
  Variant eo_0;
  LINE(68,t__setvalue(multiply_rev((assignCallTemp(eo_0, toObject(c_whirlmemory::t_instance())),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), t__getvalue())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 71 */
void c_whirlmathring::t_ringfunc_div() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Div);
  Variant eo_0;
  LINE(72,t__setvalue(divide_rev((assignCallTemp(eo_0, toObject(c_whirlmemory::t_instance())),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), t__getvalue())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 75 */
void c_whirlmathring::t_ringfunc_zero() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Zero);
  LINE(76,t__setvalue(0LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 79 */
void c_whirlmathring::t_ringfunc_less() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Less);
  Variant eo_0;
  if (less_rev((assignCallTemp(eo_0, toObject(LINE(80,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), t__getvalue())) {
    LINE(81,t__setvalue(1LL));
  }
  else {
    LINE(83,t__setvalue(0LL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 87 */
void c_whirlmathring::t_ringfunc_greater() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Greater);
  Variant eo_0;
  if (more_rev((assignCallTemp(eo_0, toObject(LINE(88,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), t__getvalue())) {
    LINE(89,t__setvalue(1LL));
  }
  else {
    LINE(91,t__setvalue(0LL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 95 */
void c_whirlmathring::t_ringfunc_equal() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Equal);
  Variant eo_0;
  if (same_rev((assignCallTemp(eo_0, toObject(LINE(96,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), t__getvalue())) {
    LINE(97,t__setvalue(1LL));
  }
  else {
    LINE(99,t__setvalue(0LL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 103 */
void c_whirlmathring::t_ringfunc_not() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Not);
  if (!same(LINE(104,t__getvalue()), 0LL)) {
    LINE(105,t__setvalue(0LL));
  }
  else {
    LINE(107,t__setvalue(1LL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 111 */
void c_whirlmathring::t_ringfunc_neg() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::RINGFUNC_Neg);
  LINE(112,t__setvalue(t__getvalue() * -1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 115 */
Variant c_whirlmathring::t_execute() {
  INSTANCE_METHOD_INJECTION(WhirlMathRing, WhirlMathRing::execute);
  {
    Variant tmp2 = (m__commands.rvalAt(m__position));
    int tmp3 = -1;
    if (equal(tmp2, ("Noop"))) {
      tmp3 = 0;
    } else if (equal(tmp2, ("Load"))) {
      tmp3 = 1;
    } else if (equal(tmp2, ("Store"))) {
      tmp3 = 2;
    } else if (equal(tmp2, ("Add"))) {
      tmp3 = 3;
    } else if (equal(tmp2, ("Mult"))) {
      tmp3 = 4;
    } else if (equal(tmp2, ("Div"))) {
      tmp3 = 5;
    } else if (equal(tmp2, ("Zero"))) {
      tmp3 = 6;
    } else if (equal(tmp2, ("Less"))) {
      tmp3 = 7;
    } else if (equal(tmp2, ("Greater"))) {
      tmp3 = 8;
    } else if (equal(tmp2, ("Equal"))) {
      tmp3 = 9;
    } else if (equal(tmp2, ("Not"))) {
      tmp3 = 10;
    } else if (equal(tmp2, ("Neg"))) {
      tmp3 = 11;
    }
    switch (tmp3) {
    case 0:
      {
        return (LINE(119,t_ringfunc_noop()), null);
      }
    case 1:
      {
        return (LINE(122,t_ringfunc_load()), null);
      }
    case 2:
      {
        return (LINE(125,t_ringfunc_store()), null);
      }
    case 3:
      {
        return (LINE(128,t_ringfunc_add()), null);
      }
    case 4:
      {
        return (LINE(131,t_ringfunc_mult()), null);
      }
    case 5:
      {
        return (LINE(134,t_ringfunc_div()), null);
      }
    case 6:
      {
        return (LINE(137,t_ringfunc_zero()), null);
      }
    case 7:
      {
        return (LINE(140,t_ringfunc_less()), null);
      }
    case 8:
      {
        return (LINE(143,t_ringfunc_greater()), null);
      }
    case 9:
      {
        return (LINE(146,t_ringfunc_equal()), null);
      }
    case 10:
      {
        return (LINE(149,t_ringfunc_not()), null);
      }
    case 11:
      {
        return (LINE(152,t_ringfunc_neg()), null);
      }
    }
  }
  return null;
} /* function */
Object co_whirlmathring(CArrRef params, bool init /* = true */) {
  return Object(p_whirlmathring(NEW(c_whirlmathring)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlMathRing_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlMathRing_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(38,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlRing_php(true, variables));
  LINE(39,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlParser_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
