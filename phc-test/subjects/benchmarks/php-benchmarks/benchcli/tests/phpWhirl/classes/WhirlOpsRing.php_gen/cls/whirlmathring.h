
#ifndef __GENERATED_cls_whirlmathring_h__
#define __GENERATED_cls_whirlmathring_h__

#include <cls/whirlring.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.php line 41 */
class c_whirlmathring : virtual public c_whirlring {
  BEGIN_CLASS_MAP(whirlmathring)
    PARENT_CLASS(whirlring)
  END_CLASS_MAP(whirlmathring)
  DECLARE_CLASS(whirlmathring, WhirlMathRing, whirlring)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_initcommandsarray();
  public: void t_ringfunc_noop();
  public: void t_ringfunc_load();
  public: void t_ringfunc_store();
  public: void t_ringfunc_add();
  public: void t_ringfunc_mult();
  public: void t_ringfunc_div();
  public: void t_ringfunc_zero();
  public: void t_ringfunc_less();
  public: void t_ringfunc_greater();
  public: void t_ringfunc_equal();
  public: void t_ringfunc_not();
  public: void t_ringfunc_neg();
  public: Variant t_execute();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_whirlmathring_h__
