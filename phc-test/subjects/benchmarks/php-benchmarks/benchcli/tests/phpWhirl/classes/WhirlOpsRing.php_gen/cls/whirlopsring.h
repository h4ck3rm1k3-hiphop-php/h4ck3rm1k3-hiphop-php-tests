
#ifndef __GENERATED_cls_whirlopsring_h__
#define __GENERATED_cls_whirlopsring_h__

#include <cls/whirlring.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 41 */
class c_whirlopsring : virtual public c_whirlring {
  BEGIN_CLASS_MAP(whirlopsring)
    PARENT_CLASS(whirlring)
  END_CLASS_MAP(whirlopsring)
  DECLARE_CLASS(whirlopsring, WhirlOpsRing, whirlring)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_initcommandsarray();
  public: void t_ringfunc_noop();
  public: void t_ringfunc_exit();
  public: void t_ringfunc_one();
  public: void t_ringfunc_zero();
  public: void t_ringfunc_load();
  public: void t_ringfunc_store();
  public: void t_ringfunc_padd();
  public: void t_ringfunc_dadd();
  public: void t_ringfunc_logic();
  public: void t_ringfunc_if();
  public: void t_ringfunc_intio();
  public: void t_ringfunc_ascio();
  public: Variant t_execute();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_whirlopsring_h__
