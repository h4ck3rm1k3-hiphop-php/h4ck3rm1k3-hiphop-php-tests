
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/whirl.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$whirl_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/whirl.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$whirl_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_whirl __attribute__((__unused__)) = (variables != gVariables) ? variables->get("whirl") : g->GV(whirl);

  LINE(39,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlParser_php(true, variables));
  {
    echo("PHP Whirl Interpreter by Jakob Westhoff <jakob@westhoffswelt.de> V1.0");
    echo("\n");
  }
  if (less(v_argc, 2LL)) {
    {
      echo("Usage: ");
      echo(toString(v_argv.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
      echo(" [-debug] Whirlfile.wrl");
      echo("\n");
    }
    f_exit();
  }
  if (!(toBoolean(LINE(48,x_strcasecmp(toString(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), "-debug"))))) {
    LINE(49,g->declareConstant("DEBUG", g->k_DEBUG, true));
    (v_filename = v_argv.rvalAt(2LL, 0x486AFCC090D5F98CLL));
  }
  else {
    (v_filename = v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  }
  {
    echo("Loading and parsing the program...");
    echo("\n");
  }
  (v_whirl = ((Object)(LINE(56,p_whirlparser(p_whirlparser(NEWOBJ(c_whirlparser)())->create())))));
  LINE(57,v_whirl.o_invoke_few_args("loadfile", 0x6CB75C28290C8A9ELL, 1, v_filename));
  LINE(58,v_whirl.o_invoke_few_args("parse", 0x46463F1C3624CEDELL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
