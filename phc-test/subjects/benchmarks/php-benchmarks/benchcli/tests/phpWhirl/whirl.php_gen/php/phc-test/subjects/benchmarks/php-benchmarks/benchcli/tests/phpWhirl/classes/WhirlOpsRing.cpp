
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlRing.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 41 */
Variant c_whirlopsring::os_get(const char *s, int64 hash) {
  return c_whirlring::os_get(s, hash);
}
Variant &c_whirlopsring::os_lval(const char *s, int64 hash) {
  return c_whirlring::os_lval(s, hash);
}
void c_whirlopsring::o_get(ArrayElementVec &props) const {
  c_whirlring::o_get(props);
}
bool c_whirlopsring::o_exists(CStrRef s, int64 hash) const {
  return c_whirlring::o_exists(s, hash);
}
Variant c_whirlopsring::o_get(CStrRef s, int64 hash) {
  return c_whirlring::o_get(s, hash);
}
Variant c_whirlopsring::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_whirlring::o_set(s, hash, v, forInit);
}
Variant &c_whirlopsring::o_lval(CStrRef s, int64 hash) {
  return c_whirlring::o_lval(s, hash);
}
Variant c_whirlopsring::os_constant(const char *s) {
  return c_whirlring::os_constant(s);
}
IMPLEMENT_CLASS(whirlopsring)
ObjectData *c_whirlopsring::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_whirlopsring::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_whirlopsring::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_whirlopsring::cloneImpl() {
  c_whirlopsring *obj = NEW(c_whirlopsring)();
  cloneSet(obj);
  return obj;
}
void c_whirlopsring::cloneSet(c_whirlopsring *clone) {
  c_whirlring::cloneSet(clone);
}
Variant c_whirlopsring::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 7:
      HASH_GUARD(0x58CA90E42B3D6957LL, reversedirection) {
        return (t_reversedirection(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x78E78D245AC277D8LL, initcommandsarray) {
        return (t_initcommandsarray(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x6770944C6053865BLL, rotate) {
        return (t_rotate(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_whirlring::o_invoke(s, params, hash, fatal);
}
Variant c_whirlopsring::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 7:
      HASH_GUARD(0x58CA90E42B3D6957LL, reversedirection) {
        return (t_reversedirection(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x78E78D245AC277D8LL, initcommandsarray) {
        return (t_initcommandsarray(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x6770944C6053865BLL, rotate) {
        return (t_rotate(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_whirlring::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_whirlopsring::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_whirlring::os_invoke(c, s, params, hash, fatal);
}
Variant cw_whirlopsring$os_get(const char *s) {
  return c_whirlopsring::os_get(s, -1);
}
Variant &cw_whirlopsring$os_lval(const char *s) {
  return c_whirlopsring::os_lval(s, -1);
}
Variant cw_whirlopsring$os_constant(const char *s) {
  return c_whirlopsring::os_constant(s);
}
Variant cw_whirlopsring$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_whirlopsring::os_invoke(c, s, params, -1, fatal);
}
void c_whirlopsring::init() {
  c_whirlring::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 43 */
void c_whirlopsring::t___construct() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(44,c_whirlring::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 47 */
void c_whirlopsring::t_initcommandsarray() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::initCommandsArray);
  (m__commands = ScalarArrays::sa_[4]);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 51 */
void c_whirlopsring::t_ringfunc_noop() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_Noop);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 55 */
void c_whirlopsring::t_ringfunc_exit() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_Exit);
  Variant eo_0;
  (assignCallTemp(eo_0, toObject(LINE(56,c_whirlparser::t_instance()))),eo_0.o_invoke_few_args("programExit", 0x365431D309BD6282LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 59 */
void c_whirlopsring::t_ringfunc_one() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_One);
  LINE(60,t__setvalue(1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 63 */
void c_whirlopsring::t_ringfunc_zero() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_Zero);
  LINE(64,t__setvalue(0LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 67 */
void c_whirlopsring::t_ringfunc_load() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_Load);
  Variant eo_0;
  LINE(68,t__setvalue((assignCallTemp(eo_0, toObject(c_whirlmemory::t_instance())),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 71 */
void c_whirlopsring::t_ringfunc_store() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_Store);
  Variant eo_0;
  (assignCallTemp(eo_0, toObject(LINE(72,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("setValue", 0x36FBED35008C8DB5LL, 1, t__getvalue()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 75 */
void c_whirlopsring::t_ringfunc_padd() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_PAdd);
  Variant eo_0;
  (assignCallTemp(eo_0, toObject(LINE(76,c_whirlparser::t_instance()))),eo_0.o_invoke_few_args("PAdd", 0x5DF14CDA65F9CFF7LL, 1, t__getvalue()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 79 */
void c_whirlopsring::t_ringfunc_dadd() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_DAdd);
  Variant eo_0;
  (assignCallTemp(eo_0, toObject(LINE(80,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("moveby", 0x5034C90507A74BACLL, 1, t__getvalue()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 83 */
void c_whirlopsring::t_ringfunc_logic() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_Logic);
  Variant eo_0;
  if (!same((assignCallTemp(eo_0, toObject(LINE(84,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), 0LL)) {
    LINE(85,t__setvalue(toInt64((toBoolean(t__getvalue()) && toBoolean(1LL)))));
  }
  else {
    LINE(87,t__setvalue(0LL));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 92 */
void c_whirlopsring::t_ringfunc_if() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_If);
  Variant eo_0;
  Variant eo_1;
  if (!same((assignCallTemp(eo_0, toObject(LINE(93,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)), 0LL)) {
    (assignCallTemp(eo_1, toObject(LINE(94,c_whirlparser::t_instance()))),eo_1.o_invoke_few_args("PAdd", 0x5DF14CDA65F9CFF7LL, 1, t__getvalue()));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 98 */
void c_whirlopsring::t_ringfunc_intio() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_IntIO);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  if (same(LINE(99,t__getvalue()), 0LL)) {
    (assignCallTemp(eo_0, toObject(LINE(100,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("setValue", 0x36FBED35008C8DB5LL, 1, toInt64((assignCallTemp(eo_1, toObject(c_whirlparser::t_instance())),eo_1.o_invoke_few_args("input", 0x122579DD07C4343ELL, 0)))));
  }
  else {
    (assignCallTemp(eo_2, toObject(LINE(102,c_whirlparser::t_instance()))),eo_2.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 1, toInt64((assignCallTemp(eo_3, toObject(c_whirlmemory::t_instance())),eo_3.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0)))));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 106 */
void c_whirlopsring::t_ringfunc_ascio() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::RINGFUNC_AscIO);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  if (same(LINE(107,t__getvalue()), 0LL)) {
    (assignCallTemp(eo_0, toObject(LINE(108,c_whirlmemory::t_instance()))),eo_0.o_invoke_few_args("setValue", 0x36FBED35008C8DB5LL, 1, toInt64(x_ord(toString((assignCallTemp(eo_1, toObject(c_whirlparser::t_instance())),eo_1.o_invoke_few_args("input", 0x122579DD07C4343ELL, 0)))))));
  }
  else {
    (assignCallTemp(eo_2, toObject(LINE(110,c_whirlparser::t_instance()))),eo_2.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 1, x_chr(toInt64((assignCallTemp(eo_3, toObject(c_whirlmemory::t_instance())),eo_3.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 0))))));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php line 114 */
Variant c_whirlopsring::t_execute() {
  INSTANCE_METHOD_INJECTION(WhirlOpsRing, WhirlOpsRing::execute);
  {
    Variant tmp2 = (m__commands.rvalAt(m__position));
    int tmp3 = -1;
    if (equal(tmp2, ("AscIO"))) {
      tmp3 = 0;
    } else if (equal(tmp2, ("Noop"))) {
      tmp3 = 1;
    } else if (equal(tmp2, ("Exit"))) {
      tmp3 = 2;
    } else if (equal(tmp2, ("One"))) {
      tmp3 = 3;
    } else if (equal(tmp2, ("Zero"))) {
      tmp3 = 4;
    } else if (equal(tmp2, ("Load"))) {
      tmp3 = 5;
    } else if (equal(tmp2, ("Store"))) {
      tmp3 = 6;
    } else if (equal(tmp2, ("PAdd"))) {
      tmp3 = 7;
    } else if (equal(tmp2, ("DAdd"))) {
      tmp3 = 8;
    } else if (equal(tmp2, ("Logic"))) {
      tmp3 = 9;
    } else if (equal(tmp2, ("If"))) {
      tmp3 = 10;
    } else if (equal(tmp2, ("IntIO"))) {
      tmp3 = 11;
    }
    switch (tmp3) {
    case 0:
      {
        return (LINE(118,t_ringfunc_ascio()), null);
      }
    case 1:
      {
        return (LINE(120,t_ringfunc_noop()), null);
      }
    case 2:
      {
        return (LINE(123,t_ringfunc_exit()), null);
      }
    case 3:
      {
        return (LINE(126,t_ringfunc_one()), null);
      }
    case 4:
      {
        return (LINE(129,t_ringfunc_zero()), null);
      }
    case 5:
      {
        return (LINE(132,t_ringfunc_load()), null);
      }
    case 6:
      {
        return (LINE(135,t_ringfunc_store()), null);
      }
    case 7:
      {
        return (LINE(138,t_ringfunc_padd()), null);
      }
    case 8:
      {
        return (LINE(141,t_ringfunc_dadd()), null);
      }
    case 9:
      {
        return (LINE(144,t_ringfunc_logic()), null);
      }
    case 10:
      {
        return (LINE(147,t_ringfunc_if()), null);
      }
    case 11:
      {
        return (LINE(150,t_ringfunc_intio()), null);
      }
    }
  }
  return null;
} /* function */
Object co_whirlopsring(CArrRef params, bool init /* = true */) {
  return Object(p_whirlopsring(NEW(c_whirlopsring)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlOpsRing_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlOpsRing_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(38,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlRing_php(true, variables));
  LINE(39,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlParser_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
