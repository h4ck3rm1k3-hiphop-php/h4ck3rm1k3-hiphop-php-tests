
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMathRing.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlMemory.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlOpsRing.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 42 */
Variant c_whirlparser::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x48C0A6C6043C78F4LL, g->s_whirlparser_DupId_instance,
                  _instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_whirlparser::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x48C0A6C6043C78F4LL, g->s_whirlparser_DupId_instance,
                  _instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_whirlparser::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_LANGUAGE", m__LANGUAGE));
  props.push_back(NEW(ArrayElement)("_WhirlMemory", m__WhirlMemory.isReferenced() ? ref(m__WhirlMemory) : m__WhirlMemory));
  props.push_back(NEW(ArrayElement)("_program", m__program.isReferenced() ? ref(m__program) : m__program));
  props.push_back(NEW(ArrayElement)("_currentPosition", m__currentPosition.isReferenced() ? ref(m__currentPosition) : m__currentPosition));
  props.push_back(NEW(ArrayElement)("_rings", m__rings.isReferenced() ? ref(m__rings) : m__rings));
  props.push_back(NEW(ArrayElement)("_currentRing", m__currentRing.isReferenced() ? ref(m__currentRing) : m__currentRing));
  c_ObjectData::o_get(props);
}
bool c_whirlparser::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x745207BE04E5B3D0LL, _WhirlMemory, 12);
      HASH_EXISTS_STRING(0x477A8D50F19FB5F0LL, _currentPosition, 16);
      break;
    case 1:
      HASH_EXISTS_STRING(0x6B34C4BFDD180781LL, _rings, 6);
      break;
    case 5:
      HASH_EXISTS_STRING(0x6509C8AD591E1645LL, _currentRing, 12);
      break;
    case 8:
      HASH_EXISTS_STRING(0x2DD751EED95CCB88LL, _LANGUAGE, 9);
      break;
    case 11:
      HASH_EXISTS_STRING(0x37B4903B2D7AE8BBLL, _program, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_whirlparser::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x745207BE04E5B3D0LL, m__WhirlMemory,
                         _WhirlMemory, 12);
      HASH_RETURN_STRING(0x477A8D50F19FB5F0LL, m__currentPosition,
                         _currentPosition, 16);
      break;
    case 1:
      HASH_RETURN_STRING(0x6B34C4BFDD180781LL, m__rings,
                         _rings, 6);
      break;
    case 5:
      HASH_RETURN_STRING(0x6509C8AD591E1645LL, m__currentRing,
                         _currentRing, 12);
      break;
    case 8:
      HASH_RETURN_STRING(0x2DD751EED95CCB88LL, m__LANGUAGE,
                         _LANGUAGE, 9);
      break;
    case 11:
      HASH_RETURN_STRING(0x37B4903B2D7AE8BBLL, m__program,
                         _program, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_whirlparser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x745207BE04E5B3D0LL, m__WhirlMemory,
                      _WhirlMemory, 12);
      HASH_SET_STRING(0x477A8D50F19FB5F0LL, m__currentPosition,
                      _currentPosition, 16);
      break;
    case 1:
      HASH_SET_STRING(0x6B34C4BFDD180781LL, m__rings,
                      _rings, 6);
      break;
    case 5:
      HASH_SET_STRING(0x6509C8AD591E1645LL, m__currentRing,
                      _currentRing, 12);
      break;
    case 8:
      HASH_SET_STRING(0x2DD751EED95CCB88LL, m__LANGUAGE,
                      _LANGUAGE, 9);
      break;
    case 11:
      HASH_SET_STRING(0x37B4903B2D7AE8BBLL, m__program,
                      _program, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_whirlparser::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x745207BE04E5B3D0LL, m__WhirlMemory,
                         _WhirlMemory, 12);
      HASH_RETURN_STRING(0x477A8D50F19FB5F0LL, m__currentPosition,
                         _currentPosition, 16);
      break;
    case 1:
      HASH_RETURN_STRING(0x6B34C4BFDD180781LL, m__rings,
                         _rings, 6);
      break;
    case 5:
      HASH_RETURN_STRING(0x6509C8AD591E1645LL, m__currentRing,
                         _currentRing, 12);
      break;
    case 11:
      HASH_RETURN_STRING(0x37B4903B2D7AE8BBLL, m__program,
                         _program, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_whirlparser::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(whirlparser)
ObjectData *c_whirlparser::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_whirlparser::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_whirlparser::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_whirlparser::cloneImpl() {
  c_whirlparser *obj = NEW(c_whirlparser)();
  cloneSet(obj);
  return obj;
}
void c_whirlparser::cloneSet(c_whirlparser *clone) {
  clone->m__LANGUAGE = m__LANGUAGE;
  clone->m__WhirlMemory = m__WhirlMemory.isReferenced() ? ref(m__WhirlMemory) : m__WhirlMemory;
  clone->m__program = m__program.isReferenced() ? ref(m__program) : m__program;
  clone->m__currentPosition = m__currentPosition.isReferenced() ? ref(m__currentPosition) : m__currentPosition;
  clone->m__rings = m__rings.isReferenced() ? ref(m__rings) : m__rings;
  clone->m__currentRing = m__currentRing.isReferenced() ? ref(m__currentRing) : m__currentRing;
  ObjectData::cloneSet(clone);
}
Variant c_whirlparser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 2:
      HASH_GUARD(0x365431D309BD6282LL, programexit) {
        return (t_programexit(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x264763628329C784LL, getinstructionnumber) {
        return (t_getinstructionnumber());
      }
      break;
    case 23:
      HASH_GUARD(0x5DF14CDA65F9CFF7LL, padd) {
        return (t_padd(params.rvalAt(0)), null);
      }
      break;
    case 30:
      HASH_GUARD(0x122579DD07C4343ELL, input) {
        return (t_input());
      }
      HASH_GUARD(0x6CB75C28290C8A9ELL, loadfile) {
        return (t_loadfile(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        return (t_parse(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x79B7A5774A0943DFLL, load) {
        return (t_load(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_whirlparser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 2:
      HASH_GUARD(0x365431D309BD6282LL, programexit) {
        return (t_programexit(), null);
      }
      break;
    case 4:
      HASH_GUARD(0x264763628329C784LL, getinstructionnumber) {
        return (t_getinstructionnumber());
      }
      break;
    case 23:
      HASH_GUARD(0x5DF14CDA65F9CFF7LL, padd) {
        return (t_padd(a0), null);
      }
      break;
    case 30:
      HASH_GUARD(0x122579DD07C4343ELL, input) {
        return (t_input());
      }
      HASH_GUARD(0x6CB75C28290C8A9ELL, loadfile) {
        return (t_loadfile(a0), null);
      }
      HASH_GUARD(0x46463F1C3624CEDELL, parse) {
        return (t_parse(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x79B7A5774A0943DFLL, load) {
        return (t_load(a0), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_whirlparser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_whirlparser$os_get(const char *s) {
  return c_whirlparser::os_get(s, -1);
}
Variant &cw_whirlparser$os_lval(const char *s) {
  return c_whirlparser::os_lval(s, -1);
}
Variant cw_whirlparser$os_constant(const char *s) {
  return c_whirlparser::os_constant(s);
}
Variant cw_whirlparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_whirlparser::os_invoke(c, s, params, -1, fatal);
}
void c_whirlparser::init() {
  m__LANGUAGE = ScalarArrays::sa_[1];
  m__WhirlMemory = null;
  m__program = null;
  m__currentPosition = null;
  m__rings = null;
  m__currentRing = null;
}
void c_whirlparser::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_whirlparser_DupId_instance = null;
}
void csi_whirlparser() {
  c_whirlparser::os_static_initializer();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 54 */
void c_whirlparser::t___construct() {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(55,t__init());
  if (!(isset(g->s_whirlparser_DupId_instance))) {
    (g->s_whirlparser_DupId_instance = ((Object)(this)));
  }
  else {
    f_exit("WhirlParser is a Singleton, you can only instantiate one object of this class. Please use instance() instead.");
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 63 */
void c_whirlparser::t__init() {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::_init);
  Variant eo_0;
  Variant eo_1;
  (m__WhirlMemory = LINE(64,c_whirlmemory::t_instance()));
  (m__program = ScalarArrays::sa_[2]);
  (m__rings = (assignCallTemp(eo_0, ((Object)(LINE(66,p_whirlmathring(p_whirlmathring(NEWOBJ(c_whirlmathring)())->create()))))),assignCallTemp(eo_1, ((Object)(p_whirlopsring(p_whirlopsring(NEWOBJ(c_whirlopsring)())->create())))),Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create())));
  (m__currentRing = 0LL);
  (m__currentPosition = 0LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 71 */
Variant c_whirlparser::ti_instance(const char* cls) {
  STATIC_METHOD_INJECTION(WhirlParser, WhirlParser::instance);
  DECLARE_GLOBAL_VARIABLES(g);
  if (!(isset(g->s_whirlparser_DupId_instance))) {
    (g->s_whirlparser_DupId_instance = ((Object)(LINE(73,p_whirlparser(p_whirlparser(NEWOBJ(c_whirlparser)())->create())))));
  }
  return g->s_whirlparser_DupId_instance;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 78 */
void c_whirlparser::t__setnextringactive() {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::_setNextRingActive);
  m__currentRing++;
  if (more(m__currentRing, LINE(80,x_count(m__rings)) - 1LL)) {
    (m__currentRing = 0LL);
  }
  if (LINE(83,x_defined("DEBUG"))) {
    echo("changed to ring: ");
    echo(toString(m__currentRing));
    echo("\n");
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 87 */
Variant c_whirlparser::t__cleancode(CVarRef v_code) {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::_cleanCode);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return LINE(89,(assignCallTemp(eo_0, concat4("/[^", toString(m__LANGUAGE.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(m__LANGUAGE.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), "]/")),assignCallTemp(eo_2, v_code),x_preg_replace(eo_0, "", eo_2)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 92 */
Array c_whirlparser::t__preparecode(CVarRef v_code) {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::_prepareCode);
  Array v_preparedCode;
  Variant v_instruction;

  (v_preparedCode = ScalarArrays::sa_[2]);
  {
    LOOP_COUNTER(1);
    Variant map2 = LINE(94,x_preg_split("//", v_code));
    for (ArrayIterPtr iter3 = map2.begin("whirlparser"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_instruction = iter3->second();
      {
        if (same(v_instruction, m__LANGUAGE.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) {
          v_preparedCode.append((false));
        }
        else if (same(v_instruction, m__LANGUAGE.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) {
          v_preparedCode.append((true));
        }
      }
    }
  }
  return v_preparedCode;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 104 */
void c_whirlparser::t_load(CVarRef v_code) {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::load);
  (m__program = LINE(105,t__preparecode(t__cleancode(v_code))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 108 */
void c_whirlparser::t_loadfile(CVarRef v_filename) {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::loadFile);
  if (!(LINE(109,x_file_exists(toString(v_filename))))) {
    f_exit(LINE(110,concat3("An Error occured while loading a source file: The file ", toString(v_filename), " does not exist.")));
  }
  if (!(LINE(112,x_is_readable(toString(v_filename))))) {
    f_exit(LINE(113,concat3("An Error occured while loading a source file: The file ", toString(v_filename), " is not readable.")));
  }
  (m__program = LINE(115,t__preparecode(t__cleancode(x_file_get_contents(toString(v_filename))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 118 */
void c_whirlparser::t_parse() {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::parse);
  Variant v_instruction;
  bool v_execute = false;

  if (same(LINE(119,x_count(m__program)), 0LL)) {
    f_exit("An Error occured while parsing: No program code has been supplied.");
  }
  if (LINE(123,x_defined("DEBUG"))) {
    echo("Executing the following Code:\n");
    {
      LOOP_COUNTER(4);
      Variant map5 = m__program;
      for (ArrayIterPtr iter6 = map5.begin("whirlparser"); !iter6->end(); iter6->next()) {
        LOOP_COUNTER_CHECK(4);
        v_instruction = iter6->second();
        {
          if (toBoolean(v_instruction)) echo(toString(m__LANGUAGE.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
          else echo(toString(m__LANGUAGE.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
        }
      }
    }
    echo("\n");
  }
  (v_execute = false);
  LOOP_COUNTER(7);
  {
    while (less(m__currentPosition, LINE(138,x_count(m__program)))) {
      LOOP_COUNTER_CHECK(7);
      {
        if (toBoolean(m__program.rvalAt(m__currentPosition))) {
          LINE(141,m__rings.rvalAt(m__currentRing).o_invoke_few_args("rotate", 0x6770944C6053865BLL, 0));
          (v_execute = false);
        }
        else {
          LINE(145,m__rings.rvalAt(m__currentRing).o_invoke_few_args("reverseDirection", 0x58CA90E42B3D6957LL, 0));
          if (v_execute) {
            LINE(147,m__rings.rvalAt(m__currentRing).o_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
            LINE(148,t__setnextringactive());
            (v_execute = false);
          }
          else {
            (v_execute = true);
          }
        }
        m__currentPosition++;
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 159 */
void c_whirlparser::t_programexit() {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::programExit);
  (m__currentPosition = LINE(161,x_count(m__program)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 164 */
void c_whirlparser::t_padd(CVarRef v_count) {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::PAdd);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  m__currentPosition += v_count - 1LL;
  if (less(m__currentPosition, -1LL)) {
    f_exit(LINE(168,(assignCallTemp(eo_1, toString(t_getinstructionnumber())),concat3("Error in Instruction ", eo_1, ": Program position out of Bounds. The program position was decreased beyond zero."))));
  }
  else if (more(m__currentPosition, LINE(169,x_count(m__program)))) {
    f_exit(LINE(170,(assignCallTemp(eo_1, toString(t_getinstructionnumber())),concat3("Error in Instruction ", eo_1, ": Program position out of Bounds. The program position was increased beyond the end of file."))));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 174 */
Numeric c_whirlparser::t_getinstructionnumber() {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::getInstructionNumber);
  return m__currentPosition + 1LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 178 */
String c_whirlparser::t_input() {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::input);
  return LINE(179,x_trim(toString(x_fgets(toObject(STDIN)))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php line 182 */
void c_whirlparser::t_output(CVarRef v_output) {
  INSTANCE_METHOD_INJECTION(WhirlParser, WhirlParser::output);
  print(toString((toString(v_output))));
} /* function */
Object co_whirlparser(CArrRef params, bool init /* = true */) {
  return Object(p_whirlparser(NEW(c_whirlparser)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlParser_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/classes/WhirlParser.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlParser_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(38,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlMemory_php(true, variables));
  LINE(39,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlOpsRing_php(true, variables));
  LINE(40,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$phpWhirl$classes$WhirlMathRing_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
