
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php line 6 */
Variant c_gaussjordan::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_gaussjordan::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_gaussjordan::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("eq", m_eq.isReferenced() ? ref(m_eq) : m_eq));
  props.push_back(NEW(ArrayElement)("nc", m_nc.isReferenced() ? ref(m_nc) : m_nc));
  props.push_back(NEW(ArrayElement)("s", m_s.isReferenced() ? ref(m_s) : m_s));
  props.push_back(NEW(ArrayElement)("r", m_r.isReferenced() ? ref(m_r) : m_r));
  props.push_back(NEW(ArrayElement)("grade", m_grade.isReferenced() ? ref(m_grade) : m_grade));
  props.push_back(NEW(ArrayElement)("solution", m_solution));
  c_ObjectData::o_get(props);
}
bool c_gaussjordan::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_EXISTS_STRING(0x2244F2E5CD8A86B0LL, eq, 2);
      HASH_EXISTS_STRING(0x0BC2CDE4BBFC9C10LL, s, 1);
      break;
    case 6:
      HASH_EXISTS_STRING(0x6ECE84440D42ECF6LL, r, 1);
      break;
    case 7:
      HASH_EXISTS_STRING(0x49C1D6F824DD47C7LL, grade, 5);
      break;
    case 12:
      HASH_EXISTS_STRING(0x517E6DC76FCBC3BCLL, solution, 8);
      break;
    case 13:
      HASH_EXISTS_STRING(0x66DE2313CE94825DLL, nc, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_gaussjordan::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x2244F2E5CD8A86B0LL, m_eq,
                         eq, 2);
      HASH_RETURN_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                         s, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x6ECE84440D42ECF6LL, m_r,
                         r, 1);
      break;
    case 7:
      HASH_RETURN_STRING(0x49C1D6F824DD47C7LL, m_grade,
                         grade, 5);
      break;
    case 12:
      HASH_RETURN_STRING(0x517E6DC76FCBC3BCLL, m_solution,
                         solution, 8);
      break;
    case 13:
      HASH_RETURN_STRING(0x66DE2313CE94825DLL, m_nc,
                         nc, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_gaussjordan::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_SET_STRING(0x2244F2E5CD8A86B0LL, m_eq,
                      eq, 2);
      HASH_SET_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                      s, 1);
      break;
    case 6:
      HASH_SET_STRING(0x6ECE84440D42ECF6LL, m_r,
                      r, 1);
      break;
    case 7:
      HASH_SET_STRING(0x49C1D6F824DD47C7LL, m_grade,
                      grade, 5);
      break;
    case 12:
      HASH_SET_STRING(0x517E6DC76FCBC3BCLL, m_solution,
                      solution, 8);
      break;
    case 13:
      HASH_SET_STRING(0x66DE2313CE94825DLL, m_nc,
                      nc, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_gaussjordan::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 0:
      HASH_RETURN_STRING(0x2244F2E5CD8A86B0LL, m_eq,
                         eq, 2);
      HASH_RETURN_STRING(0x0BC2CDE4BBFC9C10LL, m_s,
                         s, 1);
      break;
    case 6:
      HASH_RETURN_STRING(0x6ECE84440D42ECF6LL, m_r,
                         r, 1);
      break;
    case 7:
      HASH_RETURN_STRING(0x49C1D6F824DD47C7LL, m_grade,
                         grade, 5);
      break;
    case 13:
      HASH_RETURN_STRING(0x66DE2313CE94825DLL, m_nc,
                         nc, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_gaussjordan::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(gaussjordan)
ObjectData *c_gaussjordan::create(Variant v_eq, Variant v_nc, Sequence v_s, Sequence v_r) {
  init();
  t___construct(v_eq, v_nc, v_s, v_r);
  return this;
}
ObjectData *c_gaussjordan::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
ObjectData *c_gaussjordan::cloneImpl() {
  c_gaussjordan *obj = NEW(c_gaussjordan)();
  cloneSet(obj);
  return obj;
}
void c_gaussjordan::cloneSet(c_gaussjordan *clone) {
  clone->m_eq = m_eq.isReferenced() ? ref(m_eq) : m_eq;
  clone->m_nc = m_nc.isReferenced() ? ref(m_nc) : m_nc;
  clone->m_s = m_s.isReferenced() ? ref(m_s) : m_s;
  clone->m_r = m_r.isReferenced() ? ref(m_r) : m_r;
  clone->m_grade = m_grade.isReferenced() ? ref(m_grade) : m_grade;
  clone->m_solution = m_solution;
  ObjectData::cloneSet(clone);
}
Variant c_gaussjordan::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_gaussjordan::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_gaussjordan::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_gaussjordan$os_get(const char *s) {
  return c_gaussjordan::os_get(s, -1);
}
Variant &cw_gaussjordan$os_lval(const char *s) {
  return c_gaussjordan::os_lval(s, -1);
}
Variant cw_gaussjordan$os_constant(const char *s) {
  return c_gaussjordan::os_constant(s);
}
Variant cw_gaussjordan$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_gaussjordan::os_invoke(c, s, params, -1, fatal);
}
void c_gaussjordan::init() {
  m_eq = null;
  m_nc = null;
  m_s = null;
  m_r = null;
  m_grade = null;
  m_solution = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php line 17 */
void c_gaussjordan::t___construct(Variant v_eq, Variant v_nc, Sequence v_s, Sequence v_r) {
  INSTANCE_METHOD_INJECTION(GaussJordan, GaussJordan::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_eq = v_eq);
  (m_nc = v_nc);
  (m_s = v_s);
  (m_r = v_r);
  ;
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php line 30 */
void c_gaussjordan::t_process() {
  INSTANCE_METHOD_INJECTION(GaussJordan, GaussJordan::process);
  int64 v_d = 0;
  Numeric v_tmp = 0;
  int64 v_eqi = 0;
  Array v_ca;
  Array v_cv;
  Numeric v_c = 0;

  {
    LOOP_COUNTER(1);
    for ((v_d = 0LL); (less(v_d, m_eq)) && (less(v_d, m_nc)); v_d++) {
      LOOP_COUNTER_CHECK(1);
      {
        if (toBoolean(m_s.rvalAt(v_d).rvalAt(v_d))) {
          (v_tmp = divide(1LL, m_s.rvalAt(v_d).rvalAt(v_d)));
          LINE(35,t_multiplicate(v_d, v_tmp));
        }
        {
          LOOP_COUNTER(2);
          for ((v_eqi = 0LL); less(v_eqi, m_eq); v_eqi++) {
            LOOP_COUNTER_CHECK(2);
            {
              if (equal(v_eqi, v_d)) continue;
              (v_tmp = -1LL * m_s.rvalAt(v_eqi).rvalAt(v_d));
              LINE(40,t_multsum(v_d, v_tmp, v_eqi));
            }
          }
        }
      }
    }
  }
  (v_ca = ScalarArrays::sa_[0]);
  (v_cv = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(3);
    for ((v_eqi = 0LL); less(v_eqi, m_eq); v_eqi++) {
      LOOP_COUNTER_CHECK(3);
      {
        if ((!equal(LINE(46,x_min(1, m_s.rvalAt(v_eqi))), 0LL)) || (!equal(x_max(1, m_s.rvalAt(v_eqi)), 0LL))) v_cv.append((v_eqi));
        if ((LINE(48,x_in_array(v_eqi, v_cv))) || (toBoolean(m_r.rvalAt(v_eqi)))) v_ca.append((v_eqi));
      }
    }
  }
  (v_c = minus_rev(LINE(51,x_count(v_cv)), x_count(v_ca)));
  if (toBoolean(v_c)) (m_r = null);
  else {
    (m_grade = m_nc - LINE(56,x_count(v_cv)));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php line 67 */
void c_gaussjordan::t_multiplicate(int64 v_row, Numeric v_value) {
  INSTANCE_METHOD_INJECTION(GaussJordan, GaussJordan::multiplicate);
  int64 v_nci = 0;

  {
    LOOP_COUNTER(4);
    for ((v_nci = 0LL); less(v_nci, m_nc); v_nci++) {
      LOOP_COUNTER_CHECK(4);
      {
        lval(lval(m_s.lvalAt(v_row)).lvalAt(v_nci)) *= v_value;
      }
    }
  }
  lval(m_r.lvalAt(v_row)) *= v_value;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php line 84 */
void c_gaussjordan::t_multsum(int64 v_origin, Numeric v_value, int64 v_dest) {
  INSTANCE_METHOD_INJECTION(GaussJordan, GaussJordan::multsum);
  int64 v_nci = 0;

  {
    LOOP_COUNTER(5);
    for ((v_nci = 0LL); less(v_nci, m_nc); v_nci++) {
      LOOP_COUNTER_CHECK(5);
      {
        lval(lval(m_s.lvalAt(v_dest)).lvalAt(v_nci)) += (m_s.rvalAt(v_origin).rvalAt(v_nci) * v_value);
      }
    }
  }
  lval(m_r.lvalAt(v_dest)) += (m_r.rvalAt(v_origin) * v_value);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php line 100 */
void f_run(int64 v_n) {
  FUNCTION_INJECTION(run);
  int64 v_cols = 0;
  int64 v_rows = 0;
  int64 v_seed = 0;
  int64 v_i = 0;
  int64 v_j = 0;
  Sequence v_s;
  Sequence v_r;
  p_gaussjordan v_gauss;

  (v_rows = (v_cols = v_n));
  (v_seed = 20090522LL);
  LINE(105,x_srand(v_seed));
  {
    LOOP_COUNTER(6);
    for ((v_i = 0LL); less(v_i, v_rows); v_i++) {
      LOOP_COUNTER_CHECK(6);
      {
        {
          LOOP_COUNTER(7);
          for ((v_j = 0LL); less(v_j, v_cols); v_j++) {
            LOOP_COUNTER_CHECK(7);
            {
              lval(v_s.lvalAt(v_i)).set(v_j, (LINE(110,x_rand(0LL, 50LL))));
            }
          }
        }
        v_r.set(v_i, (LINE(112,x_rand(200LL, 1000LL))));
      }
    }
  }
  ((Object)((v_gauss = ((Object)(LINE(114,p_gaussjordan(p_gaussjordan(NEWOBJ(c_gaussjordan)())->create(v_rows, v_cols, v_s, v_r))))))));
  LINE(115,v_gauss->t_process());
} /* function */
Object co_gaussjordan(CArrRef params, bool init /* = true */) {
  return Object(p_gaussjordan(NEW(c_gaussjordan)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_gaussjordan_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_gaussjordan_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_error_reporting(toInt32(8191LL)));
  LINE(120,f_run(100LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
