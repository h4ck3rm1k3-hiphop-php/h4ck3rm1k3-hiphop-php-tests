
#ifndef __GENERATED_cls_gaussjordan_h__
#define __GENERATED_cls_gaussjordan_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_gaussjordan.php line 6 */
class c_gaussjordan : virtual public ObjectData {
  BEGIN_CLASS_MAP(gaussjordan)
  END_CLASS_MAP(gaussjordan)
  DECLARE_CLASS(gaussjordan, GaussJordan, ObjectData)
  void init();
  public: Variant m_eq;
  public: Variant m_nc;
  public: Variant m_s;
  public: Variant m_r;
  public: Variant m_grade;
  public: String m_solution;
  public: void t___construct(Variant v_eq, Variant v_nc, Sequence v_s, Sequence v_r);
  public: ObjectData *create(Variant v_eq, Variant v_nc, Sequence v_s, Sequence v_r);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_process();
  public: void t_multiplicate(int64 v_row, Numeric v_value);
  public: void t_multsum(int64 v_origin, Numeric v_value, int64 v_dest);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_gaussjordan_h__
