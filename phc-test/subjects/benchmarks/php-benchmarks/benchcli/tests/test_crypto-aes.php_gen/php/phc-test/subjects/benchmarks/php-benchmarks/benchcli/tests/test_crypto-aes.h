
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_aes_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_aes_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_shiftrows(Variant v_s, int64 v_Nb);
String f_aesdecryptctr(Variant v_ciphertext, CVarRef v_password, int64 v_nBits);
Variant f_encodebase64(Variant v_str);
Variant f_decodeutf8(Variant v_str);
Variant f_subbytes(Variant v_s, int64 v_Nb);
Variant f_rotword(Variant v_w);
Variant f_cipher(CVarRef v_input, CVarRef v_w);
Variant f_unescctrlchars(CVarRef v_str);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_aes_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_bytearraytohexstr(CVarRef v_b);
String f_aesencryptctr(CVarRef v_plaintext, CVarRef v_password, int64 v_nBits);
Variant f_encodeutf8(Variant v_str);
Variant f_addroundkey(Variant v_state, CVarRef v_w, Numeric v_rnd, int64 v_Nb);
Variant f_escctrlchars(CVarRef v_str);
Variant f_decodebase64(CVarRef v_str);
Variant f_mixcolumns(Variant v_s, int64 v_Nb);
Variant f_zerofill(Variant v_a, int64 v_b);
Variant f_subword(Variant v_w);
Variant f_keyexpansion(CVarRef v_key);
void f_setlength(Variant v_array, Numeric v_length);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_crypto_aes_h__
