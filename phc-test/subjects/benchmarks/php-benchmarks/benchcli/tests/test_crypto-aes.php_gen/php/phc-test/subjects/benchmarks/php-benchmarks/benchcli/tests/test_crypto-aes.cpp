
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 111 */
Variant f_shiftrows(Variant v_s, int64 v_Nb) {
  FUNCTION_INJECTION(ShiftRows);
  Variant v_t;
  int64 v_r = 0;
  int64 v_c = 0;

  (v_t = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(1);
    for ((v_r = 1LL); less(v_r, 4LL); v_r++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_c = 0LL); less(v_c, 4LL); v_c++) {
            LOOP_COUNTER_CHECK(2);
            {
              v_t.set(v_c, (v_s.rvalAt(v_r).rvalAt(modulo((v_c + v_r), v_Nb))));
            }
          }
        }
        {
          LOOP_COUNTER(3);
          for ((v_c = 0LL); less(v_c, 4LL); v_c++) {
            LOOP_COUNTER_CHECK(3);
            {
              lval(v_s.lvalAt(v_r)).set(v_c, (v_t.rvalAt(v_c)));
            }
          }
        }
      }
    }
  }
  return v_s;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 382 */
String f_aesdecryptctr(Variant v_ciphertext, CVarRef v_password, int64 v_nBits) {
  FUNCTION_INJECTION(AESDecryptCtr);
  Variant eo_0;
  Variant eo_1;
  Numeric v_nBytes = 0;
  Variant v_pwBytes;
  int64 v_i = 0;
  Variant v_pwKeySchedule;
  Variant v_key;
  Variant v_keySchedule;
  int64 v_blockSize = 0;
  Variant v_counterBlock;
  Variant v_ctrTxt;
  Variant v_plaintext;
  int64 v_b = 0;
  int64 v_c = 0;
  Variant v_cipherCntr;
  String v_pt;
  int64 v_ciphertextByte = 0;
  Primitive v_plaintextByte = 0;

  if (!((equal(v_nBits, 128LL) || equal(v_nBits, 192LL) || equal(v_nBits, 256LL)))) {
    return "";
  }
  (v_nBytes = divide(v_nBits, 8LL));
  LINE(390,f_setlength(ref(v_pwBytes), v_nBytes));
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_nBytes); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        v_pwBytes.set(v_i, (bitwise_and(LINE(394,x_ord(toString(v_password.rvalAt(v_i)))), 255LL)));
      }
    }
  }
  (v_pwKeySchedule = LINE(397,f_keyexpansion(v_pwBytes)));
  (v_key = LINE(398,f_cipher(v_pwBytes, v_pwKeySchedule)));
  (v_key = LINE(400,(assignCallTemp(eo_0, v_key),assignCallTemp(eo_1, x_array_slice(v_key, toInt32(0LL), v_nBytes - 16LL)),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
  (v_keySchedule = LINE(402,f_keyexpansion(v_key)));
  (v_ciphertext = LINE(404,x_explode("-", toString(v_ciphertext))));
  (v_blockSize = 16LL);
  LINE(408,f_setlength(ref(v_counterBlock), v_blockSize));
  (v_ctrTxt = LINE(410,f_unescctrlchars(v_ciphertext.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  {
    LOOP_COUNTER(5);
    for ((v_i = 0LL); less(v_i, 8LL); v_i++) {
      LOOP_COUNTER_CHECK(5);
      {
        v_counterBlock.set(v_i, (LINE(414,x_ord(toString(v_ctrTxt.rvalAt(v_i))))));
      }
    }
  }
  LINE(417,(assignCallTemp(eo_0, ref(v_plaintext)),assignCallTemp(eo_1, x_count(v_ciphertext) - 1LL),f_setlength(eo_0, eo_1)));
  {
    LOOP_COUNTER(6);
    for ((v_b = 1LL); less(v_b, LINE(419,x_count(v_ciphertext))); v_b++) {
      LOOP_COUNTER_CHECK(6);
      {
        {
          LOOP_COUNTER(7);
          for ((v_c = 0LL); less(v_c, 4LL); v_c++) {
            LOOP_COUNTER_CHECK(7);
            {
              v_counterBlock.set(15LL - v_c, (bitwise_and(LINE(424,f_zerofill((v_b - 1LL), v_c * 8LL)), 255LL)));
            }
          }
        }
        {
          LOOP_COUNTER(8);
          for ((v_c = 0LL); less(v_c, 4LL); v_c++) {
            LOOP_COUNTER_CHECK(8);
            {
              v_counterBlock.set(15LL - v_c - 4LL, (bitwise_and(LINE(429,f_zerofill((divide(v_b, 4294967296LL) - 1LL), v_c * 8LL)), 255LL)));
            }
          }
        }
        (v_cipherCntr = LINE(432,f_cipher(v_counterBlock, v_keySchedule)));
        v_ciphertext.set(v_b, (LINE(434,f_unescctrlchars(v_ciphertext.rvalAt(v_b)))));
        (v_pt = "");
        {
          LOOP_COUNTER(9);
          for ((v_i = 0LL); less(v_i, LINE(437,x_strlen(toString(v_ciphertext.rvalAt(v_b))))); v_i++) {
            LOOP_COUNTER_CHECK(9);
            {
              (v_ciphertextByte = LINE(440,x_ord(toString(v_ciphertext.rvalAt(v_b).rvalAt(v_i)))));
              (v_plaintextByte = bitwise_xor(v_ciphertextByte, v_cipherCntr.rvalAt(v_i)));
              concat_assign(v_pt, LINE(442,x_chr(toInt64(v_plaintextByte))));
            }
          }
        }
        v_plaintext.set(v_b - 1LL, (v_pt));
      }
    }
  }
  return LINE(449,x_implode("", v_plaintext));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 475 */
Variant f_encodebase64(Variant v_str) {
  FUNCTION_INJECTION(encodeBase64);
  int64 v_i = 0;
  Variant v_enc;
  int64 v_o1 = 0;
  int64 v_o2 = 0;
  int64 v_o3 = 0;
  Primitive v_bits = 0;
  Primitive v_h1 = 0;
  Primitive v_h2 = 0;
  Variant v_h3;
  Variant v_h4;
  Variant v_b64;

  (v_i = 0LL);
  (v_enc = "");
  (v_str = LINE(480,f_encodeutf8(v_str)));
  {
    LOOP_COUNTER(10);
    do {
      LOOP_COUNTER_CHECK(10);
      {
        (v_o1 = LINE(483,x_ord(toString(v_str.rvalAt(v_i++)))));
        (v_o2 = LINE(484,x_ord(toString(v_str.rvalAt(v_i++)))));
        (v_o3 = LINE(485,x_ord(toString(v_str.rvalAt(v_i++)))));
        (v_bits = bitwise_or(bitwise_or(toInt64(v_o1) << 16LL, toInt64(v_o2) << 8LL), v_o3));
        (v_h1 = bitwise_and(toInt64(toInt64(v_bits)) >> 18LL, 63LL));
        (v_h2 = bitwise_and(toInt64(toInt64(v_bits)) >> 12LL, 63LL));
        (v_h3 = bitwise_and(toInt64(toInt64(v_bits)) >> 6LL, 63LL));
        (v_h4 = bitwise_and(v_bits, 63LL));
        if (LINE(495,x_is_nan(toDouble(v_o3)))) (v_h4 = 64LL);
        if (LINE(496,x_is_nan(toDouble(v_o2)))) (v_h3 = 64LL);
        v_enc += v_b64.rvalAt(v_h1) + v_b64.rvalAt(v_h2) + v_b64.rvalAt(v_h3) + v_b64.rvalAt(v_h4);
      }
    } while (less(v_i, LINE(500,x_strlen(toString(v_str)))));
  }
  return v_enc;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 549 */
Variant f_decodeutf8(Variant v_str) {
  FUNCTION_INJECTION(decodeUTF8);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  (v_str = LINE(551,(assignCallTemp(eo_1, x_chr(toInt64(bitwise_or_rev(bitwise_and(x_ord(toString(v_str.rvalAt(1LL, 0x5BCA7C69B794F8CELL))), 63LL), toInt64((toInt64(bitwise_and(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), 31LL)))) << 6LL)))),assignCallTemp(eo_2, v_str),x_preg_replace("/[\\u00c0-\\u00df][\\u0080-\\u00bf]/g", eo_1, eo_2))));
  (v_str = LINE(552,(assignCallTemp(eo_1, x_chr(toInt64(bitwise_or_rev(bitwise_and(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), 63LL), bitwise_or_rev((bitwise_and(x_ord(toString(v_str.rvalAt(1LL, 0x5BCA7C69B794F8CELL))), 4032LL)), toInt64((toInt64(bitwise_and(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), 15LL)))) << 12LL))))),assignCallTemp(eo_2, v_str),x_preg_replace("/[\\u00e0-\\u00ef][\\u0080-\\u00bf][\\u0080-\\u00bf]/g", eo_1, eo_2))));
  return v_str;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 95 */
Variant f_subbytes(Variant v_s, int64 v_Nb) {
  FUNCTION_INJECTION(SubBytes);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_Sbox __attribute__((__unused__)) = g->GV(Sbox);
  int64 v_r = 0;
  int64 v_c = 0;

  {
    LOOP_COUNTER(11);
    for ((v_r = 0LL); less(v_r, 4LL); v_r++) {
      LOOP_COUNTER_CHECK(11);
      {
        {
          LOOP_COUNTER(12);
          for ((v_c = 0LL); less(v_c, v_Nb); v_c++) {
            LOOP_COUNTER_CHECK(12);
            {
              lval(v_s.lvalAt(v_r)).set(v_c, (gv_Sbox.rvalAt(v_s.rvalAt(v_r).rvalAt(v_c))));
            }
          }
        }
      }
    }
  }
  return v_s;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 234 */
Variant f_rotword(Variant v_w) {
  FUNCTION_INJECTION(RotWord);
  int64 v_i = 0;

  v_w.set(4LL, (v_w.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x6F2A25235E544A31LL);
  {
    LOOP_COUNTER(13);
    for ((v_i = 0LL); less(v_i, 4LL); v_i++) {
      LOOP_COUNTER_CHECK(13);
      {
        v_w.set(v_i, (v_w.rvalAt(v_i + 1LL)));
      }
    }
  }
  return v_w;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 59 */
Variant f_cipher(CVarRef v_input, CVarRef v_w) {
  FUNCTION_INJECTION(Cipher);
  int64 v_Nb = 0;
  Numeric v_Nr = 0;
  Variant v_state;
  int64 v_i = 0;
  int64 v_round = 0;
  Variant v_output;

  (v_Nb = 4LL);
  (v_Nr = divide(LINE(62,x_count(v_w)), v_Nb) - 1LL);
  (v_state = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(14);
    for ((v_i = 0LL); less(v_i, 4LL * v_Nb); v_i++) {
      LOOP_COUNTER_CHECK(14);
      {
        lval(v_state.lvalAt(modulo(v_i, 4LL))).set(LINE(67,x_floor(toDouble(divide(v_i, 4LL)))), (v_input.rvalAt(v_i)));
      }
    }
  }
  (v_state = LINE(70,f_addroundkey(v_state, v_w, 0LL, v_Nb)));
  {
    LOOP_COUNTER(15);
    for ((v_round = 1LL); less(v_round, v_Nr); v_round++) {
      LOOP_COUNTER_CHECK(15);
      {
        (v_state = LINE(74,f_subbytes(v_state, v_Nb)));
        (v_state = LINE(75,f_shiftrows(v_state, v_Nb)));
        (v_state = LINE(76,f_mixcolumns(v_state, v_Nb)));
        (v_state = LINE(77,f_addroundkey(v_state, v_w, v_round, v_Nb)));
      }
    }
  }
  (v_state = LINE(80,f_subbytes(v_state, v_Nb)));
  (v_state = LINE(81,f_shiftrows(v_state, v_Nb)));
  (v_state = LINE(82,f_addroundkey(v_state, v_w, v_Nr, v_Nb)));
  (v_output = Array(ArrayInit(1).set(0, 4LL * v_Nb).create()));
  {
    LOOP_COUNTER(16);
    for ((v_i = 0LL); less(v_i, 4LL * v_Nb); v_i++) {
      LOOP_COUNTER_CHECK(16);
      {
        v_output.set(v_i, (v_state.rvalAt(modulo(v_i, 4LL)).rvalAt(LINE(88,x_floor(toDouble(divide(v_i, 4LL)))))));
      }
    }
  }
  return v_output;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 462 */
Variant f_unescctrlchars(CVarRef v_str) {
  FUNCTION_INJECTION(unescCtrlChars);
  return LINE(464,x_preg_replace("/(!\\d\\d\?\\d\?!)/ie", "chr(substr(\"$1\", 1, -1))", v_str));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 558 */
String f_bytearraytohexstr(CVarRef v_b) {
  FUNCTION_INJECTION(byteArrayToHexStr);
  String v_s;
  int64 v_i = 0;

  (v_s = "");
  {
    LOOP_COUNTER(17);
    for ((v_i = 0LL); less(v_i, LINE(561,x_strlen(toString(v_b)))); v_i++) {
      LOOP_COUNTER_CHECK(17);
      {
        v_s += concat(LINE(563,x_strval(v_b.rvalAt(v_i))), " ");
      }
    }
  }
  return v_s;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 287 */
String f_aesencryptctr(CVarRef v_plaintext, CVarRef v_password, int64 v_nBits) {
  FUNCTION_INJECTION(AESEncryptCtr);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Numeric v_nBytes = 0;
  Variant v_pwBytes;
  int64 v_i = 0;
  Variant v_key;
  int64 v_blockSize = 0;
  Variant v_counterBlock;
  int64 v_nonce = 0;
  Variant v_keySchedule;
  double v_blockCount = 0.0;
  Variant v_ciphertext;
  int64 v_b = 0;
  int64 v_c = 0;
  Variant v_cipherCntr;
  Variant v_blockLength;
  String v_ct;
  int64 v_plaintextByte = 0;
  Primitive v_cipherByte = 0;
  Variant v_ctrTxt;

  if (!((equal(v_nBits, 128LL) || equal(v_nBits, 192LL) || equal(v_nBits, 256LL)))) {
    return "";
  }
  (v_nBytes = divide(v_nBits, 8LL));
  (v_pwBytes = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(18);
    for ((v_i = 0LL); less(v_i, v_nBytes); v_i++) {
      LOOP_COUNTER_CHECK(18);
      {
        v_pwBytes.set(v_i, (bitwise_and(LINE(302,x_ord(toString(v_password.rvalAt(v_i)))), 255LL)));
      }
    }
  }
  (v_key = LINE(305,(assignCallTemp(eo_0, v_pwBytes),assignCallTemp(eo_1, f_keyexpansion(v_pwBytes)),f_cipher(eo_0, eo_1))));
  (v_key = LINE(306,(assignCallTemp(eo_0, v_key),assignCallTemp(eo_1, x_array_slice(v_key, toInt32(0LL), v_nBytes - 16LL)),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
  (v_blockSize = 16LL);
  LINE(311,f_setlength(ref(v_counterBlock), v_blockSize));
  (v_nonce = 1262304001LL);
  {
    LOOP_COUNTER(19);
    for ((v_i = 0LL); less(v_i, 4LL); v_i++) {
      LOOP_COUNTER_CHECK(19);
      {
        v_counterBlock.set(v_i, (bitwise_and(LINE(317,f_zerofill(v_nonce, v_i * 8LL)), 255LL)));
      }
    }
  }
  {
    LOOP_COUNTER(20);
    for ((v_i = 0LL); less(v_i, 4LL); v_i++) {
      LOOP_COUNTER_CHECK(20);
      {
        v_counterBlock.set(v_i + 4LL, (bitwise_and(LINE(321,f_zerofill(divide(v_nonce, 4294967296LL), v_i * 8LL)), 255LL)));
      }
    }
  }
  (v_keySchedule = LINE(325,f_keyexpansion(v_key)));
  (v_blockCount = LINE(326,x_ceil(toDouble(divide(x_strlen(toString(v_plaintext)), v_blockSize)))));
  (v_ciphertext = Array(ArrayInit(1).set(0, v_blockCount).create()));
  {
    LOOP_COUNTER(21);
    for ((v_b = 0LL); less(v_b, v_blockCount); v_b++) {
      LOOP_COUNTER_CHECK(21);
      {
        {
          LOOP_COUNTER(22);
          for ((v_c = 0LL); less(v_c, 4LL); v_c++) {
            LOOP_COUNTER_CHECK(22);
            {
              v_counterBlock.set(15LL - v_c, (bitwise_and(LINE(335,f_zerofill(v_b, v_c * 8LL)), 255LL)));
            }
          }
        }
        {
          LOOP_COUNTER(23);
          for ((v_c = 0LL); less(v_c, 4LL); v_c++) {
            LOOP_COUNTER_CHECK(23);
            {
              v_counterBlock.set(15LL - v_c - 4LL, (LINE(340,f_zerofill(divide(v_b, 4294967296LL), v_c * 8LL))));
            }
          }
        }
        (v_cipherCntr = LINE(343,f_cipher(v_counterBlock, v_keySchedule)));
        (v_blockLength = less(v_b, v_blockCount - 1LL) ? ((Variant)(v_blockSize)) : ((Variant)(modulo((LINE(346,x_strlen(toString(v_plaintext))) - 1LL), v_blockSize) + 1LL)));
        (v_ct = "");
        {
          LOOP_COUNTER(24);
          for ((v_i = 0LL); less(v_i, v_blockLength); v_i++) {
            LOOP_COUNTER_CHECK(24);
            {
              (v_plaintextByte = LINE(353,x_ord(toString(v_plaintext.rvalAt(v_b * v_blockSize + v_i)))));
              (v_cipherByte = bitwise_xor(v_plaintextByte, v_cipherCntr.rvalAt(v_i)));
              concat_assign(v_ct, LINE(355,x_chr(toInt64(v_cipherByte))));
            }
          }
        }
        v_ciphertext.set(v_b, (LINE(359,f_escctrlchars(v_ct))));
      }
    }
  }
  (v_ctrTxt = "");
  {
    LOOP_COUNTER(25);
    for ((v_i = 0LL); less(v_i, 8LL); v_i++) {
      LOOP_COUNTER_CHECK(25);
      {
        concat_assign(v_ctrTxt, LINE(366,x_chr(toInt64(v_counterBlock.rvalAt(v_i)))));
      }
    }
  }
  (v_ctrTxt = LINE(369,f_escctrlchars(v_ctrTxt)));
  return LINE(372,(assignCallTemp(eo_0, toString(v_ctrTxt)),assignCallTemp(eo_2, x_implode("-", v_ciphertext)),concat3(eo_0, "-", eo_2)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 540 */
Variant f_encodeutf8(Variant v_str) {
  FUNCTION_INJECTION(encodeUTF8);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  (v_str = LINE(542,(assignCallTemp(eo_1, x_chr(toInt64(bitwise_or_rev(bitwise_and(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), 63LL), bitwise_or(192LL, toInt64(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))) >> toInt64("6128")))))),assignCallTemp(eo_2, v_str),x_preg_replace("/[\\u0080-\\u07ff]/g", eo_1, eo_2))));
  (v_str = LINE(543,(assignCallTemp(eo_1, x_chr(toInt64(bitwise_or_rev(bitwise_and(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), 63LL), bitwise_or_rev(bitwise_and(toInt64(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))) >> 6LL, "63128"), bitwise_or(224LL, toInt64(x_ord(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))) >> toInt64("12128"))))))),assignCallTemp(eo_2, v_str),x_preg_replace("/[\\u0800-\\uffff]/g", eo_1, eo_2))));
  return v_str;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 157 */
Variant f_addroundkey(Variant v_state, CVarRef v_w, Numeric v_rnd, int64 v_Nb) {
  FUNCTION_INJECTION(AddRoundKey);
  int64 v_r = 0;
  int64 v_c = 0;

  {
    LOOP_COUNTER(26);
    for ((v_r = 0LL); less(v_r, 4LL); v_r++) {
      LOOP_COUNTER_CHECK(26);
      {
        {
          LOOP_COUNTER(27);
          for ((v_c = 0LL); less(v_c, v_Nb); v_c++) {
            LOOP_COUNTER_CHECK(27);
            {
              lval(lval(v_state.lvalAt(v_r)).lvalAt(v_c)) ^= v_w.rvalAt(v_rnd * 4LL + v_c).rvalAt(v_r);
            }
          }
        }
      }
    }
  }
  return v_state;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 455 */
Variant f_escctrlchars(CVarRef v_str) {
  FUNCTION_INJECTION(escCtrlChars);
  return LINE(457,x_preg_replace("/([\\0\\t\\n\\v\\f\\r\\xa0'\"!-])/ie", "\"!\".ord(substr(\"$1\",0,1)).\"!\"", v_str));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 505 */
Variant f_decodebase64(CVarRef v_str) {
  FUNCTION_INJECTION(decodeBase64);
  Variant eo_0;
  Variant eo_1;
  int64 v_i = 0;
  String v_enc;
  Variant v_b64;
  Variant v_h1;
  Variant v_h2;
  Variant v_h3;
  Variant v_h4;
  Primitive v_bits = 0;
  Primitive v_o1 = 0;
  Primitive v_o2 = 0;
  Primitive v_o3 = 0;

  (v_i = 0LL);
  (v_enc = "");
  {
    LOOP_COUNTER(28);
    do {
      LOOP_COUNTER_CHECK(28);
      {
        (v_h1 = LINE(511,(assignCallTemp(eo_0, toString(v_b64)),assignCallTemp(eo_1, v_str.rvalAt(v_i++)),x_strpos(eo_0, eo_1))));
        (v_h2 = LINE(512,(assignCallTemp(eo_0, toString(v_b64)),assignCallTemp(eo_1, v_str.rvalAt(v_i++)),x_strpos(eo_0, eo_1))));
        (v_h3 = LINE(513,(assignCallTemp(eo_0, toString(v_b64)),assignCallTemp(eo_1, v_str.rvalAt(v_i++)),x_strpos(eo_0, eo_1))));
        (v_h4 = LINE(514,(assignCallTemp(eo_0, toString(v_b64)),assignCallTemp(eo_1, v_str.rvalAt(v_i++)),x_strpos(eo_0, eo_1))));
        (v_bits = bitwise_or(bitwise_or(bitwise_or(toInt64(toInt64(v_h1)) << 18LL, toInt64(toInt64(v_h2)) << 12LL), toInt64(toInt64(v_h3)) << 6LL), v_h4));
        (v_o1 = bitwise_and(toInt64(toInt64(v_bits)) >> 16LL, 255LL));
        (v_o2 = bitwise_and(toInt64(toInt64(v_bits)) >> 8LL, 255LL));
        (v_o3 = bitwise_and(v_bits, 255LL));
        if (equal(v_h3, 64LL)) {
          v_enc += LINE(524,x_chr(toInt64(v_o1)));
        }
        else if (equal(v_h4, 64LL)) {
          v_enc += LINE(528,x_chr(toInt64(concat(toString(v_o1), toString(v_o2)))));
        }
        else {
          v_enc += LINE(532,x_chr(toInt64(concat3(toString(v_o1), toString(v_o2), toString(v_o3)))));
        }
      }
    } while (less(v_i, LINE(534,x_strlen(toString(v_str)))));
  }
  return LINE(536,f_decodeutf8(v_enc));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 133 */
Variant f_mixcolumns(Variant v_s, int64 v_Nb) {
  FUNCTION_INJECTION(MixColumns);
  int64 v_c = 0;
  Variant v_a;
  Variant v_b;
  int64 v_i = 0;

  {
    LOOP_COUNTER(29);
    for ((v_c = 0LL); less(v_c, 4LL); v_c++) {
      LOOP_COUNTER_CHECK(29);
      {
        (v_a = ScalarArrays::sa_[1]);
        (v_b = ScalarArrays::sa_[1]);
        {
          LOOP_COUNTER(30);
          for ((v_i = 0LL); less(v_i, 4LL); v_i++) {
            LOOP_COUNTER_CHECK(30);
            {
              v_a.set(v_i, (v_s.rvalAt(v_i).rvalAt(v_c)));
              v_b.set(v_i, (toBoolean(bitwise_and(v_s.rvalAt(v_i).rvalAt(v_c), 128LL)) ? ((Variant)(bitwise_xor(toInt64(toInt64(v_s.rvalAt(v_i).rvalAt(v_c))) << 1LL, 283LL))) : ((Variant)(toInt64(toInt64(v_s.rvalAt(v_i).rvalAt(v_c))) << 1LL))));
            }
          }
        }
        lval(v_s.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(v_c, (bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), v_b.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))));
        lval(v_s.lvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(v_c, (bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_b.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), v_b.rvalAt(2LL, 0x486AFCC090D5F98CLL)), v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))));
        lval(v_s.lvalAt(2LL, 0x486AFCC090D5F98CLL)).set(v_c, (bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), v_b.rvalAt(2LL, 0x486AFCC090D5F98CLL)), v_a.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), v_b.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))));
        lval(v_s.lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)).set(v_c, (bitwise_xor(bitwise_xor(bitwise_xor(bitwise_xor(v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_b.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), v_a.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), v_a.rvalAt(2LL, 0x486AFCC090D5F98CLL)), v_b.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))));
      }
    }
  }
  return v_s;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 19 */
Variant f_zerofill(Variant v_a, int64 v_b) {
  FUNCTION_INJECTION(zeroFill);
  Variant v_z;

  (v_z = LINE(21,x_hexdec(toString(80000000LL))));
  if (toBoolean(bitwise_and(v_z, v_a))) {
    (v_a = (toInt64(toInt64(v_a)) >> 1LL));
    v_a &= (~v_z);
    v_a |= 1073741824LL;
    (v_a = (toInt64(toInt64(v_a)) >> (v_b - 1LL)));
  }
  else {
    (v_a = (toInt64(toInt64(v_a)) >> v_b));
  }
  return v_a;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 221 */
Variant f_subword(Variant v_w) {
  FUNCTION_INJECTION(SubWord);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_Sbox __attribute__((__unused__)) = g->GV(Sbox);
  int64 v_i = 0;

  {
    LOOP_COUNTER(31);
    for ((v_i = 0LL); less(v_i, 4LL); v_i++) {
      LOOP_COUNTER_CHECK(31);
      {
        v_w.set(v_i, (gv_Sbox.rvalAt(v_w.rvalAt(v_i))));
      }
    }
  }
  return v_w;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 171 */
Variant f_keyexpansion(CVarRef v_key) {
  FUNCTION_INJECTION(KeyExpansion);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_Rcon __attribute__((__unused__)) = g->GV(Rcon);
  int64 v_Nb = 0;
  Numeric v_Nk = 0;
  Numeric v_Nr = 0;
  Variant v_w;
  Variant v_temp;
  Numeric v_i = 0;
  Array v_r;
  int64 v_t = 0;

  (v_Nb = 4LL);
  (v_Nk = divide(LINE(176,x_count(v_key)), 4LL));
  (v_Nr = v_Nk + 6LL);
  (v_w = Array(ArrayInit(1).set(0, v_Nb * (v_Nr + 1LL)).create()));
  LINE(180,f_setlength(ref(v_temp), 4LL));
  {
    LOOP_COUNTER(32);
    for ((v_i = 0LL); less(v_i, v_Nk); v_i++) {
      LOOP_COUNTER_CHECK(32);
      {
        (v_r = Array(ArrayInit(4).set(0, v_key.rvalAt(4LL * v_i)).set(1, v_key.rvalAt(4LL * v_i + 1LL)).set(2, v_key.rvalAt(4LL * v_i + 2LL)).set(3, v_key.rvalAt(4LL * v_i + 3LL)).create()));
        v_w.set(v_i, (v_r));
      }
    }
  }
  {
    LOOP_COUNTER(33);
    for ((v_i = v_Nk); less(v_i, (v_Nb * (v_Nr + 1LL))); v_i++) {
      LOOP_COUNTER_CHECK(33);
      {
        v_w.set(v_i, (ScalarArrays::sa_[1]));
        {
          LOOP_COUNTER(34);
          for ((v_t = 0LL); less(v_t, 4LL); v_t++) {
            LOOP_COUNTER_CHECK(34);
            {
              v_temp.set(v_t, (v_w.rvalAt(v_i - 1LL).rvalAt(v_t)));
            }
          }
        }
        if (equal(modulo(toInt64(v_i), toInt64(v_Nk)), 0LL)) {
          (v_temp = LINE(199,f_subword(f_rotword(v_temp))));
          {
            LOOP_COUNTER(35);
            for ((v_t = 0LL); less(v_t, 4LL); v_t++) {
              LOOP_COUNTER_CHECK(35);
              {
                lval(v_temp.lvalAt(v_t)) ^= gv_Rcon.rvalAt(divide(v_i, v_Nk)).rvalAt(v_t);
              }
            }
          }
        }
        else if (more(v_Nk, 6LL) && equal(modulo(toInt64(v_i), toInt64(v_Nk)), 4LL)) {
          (v_temp = LINE(208,f_subword(v_temp)));
        }
        {
          LOOP_COUNTER(36);
          for ((v_t = 0LL); less(v_t, 4LL); v_t++) {
            LOOP_COUNTER_CHECK(36);
            {
              lval(v_w.lvalAt(v_i)).set(v_t, (bitwise_xor(v_w.rvalAt(v_i - v_Nk).rvalAt(v_t), v_temp.rvalAt(v_t))));
            }
          }
        }
      }
    }
  }
  return v_w;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php line 40 */
void f_setlength(Variant v_array, Numeric v_length) {
  FUNCTION_INJECTION(setLength);
  int64 v_i = 0;

  {
    LOOP_COUNTER(37);
    for ((v_i = 0LL); less(v_i, v_length); v_i++) {
      LOOP_COUNTER_CHECK(37);
      {
        v_array.append((""));
      }
    }
  }
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_aes_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_crypto_aes_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_Sbox __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Sbox") : g->GV(Sbox);
  Variant &v_Rcon __attribute__((__unused__)) = (variables != gVariables) ? variables->get("Rcon") : g->GV(Rcon);
  Variant &v_b64 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b64") : g->GV(b64);
  Variant &v_plainText __attribute__((__unused__)) = (variables != gVariables) ? variables->get("plainText") : g->GV(plainText);
  Variant &v_password __attribute__((__unused__)) = (variables != gVariables) ? variables->get("password") : g->GV(password);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_cipherText __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cipherText") : g->GV(cipherText);
  Variant &v_decryptedText __attribute__((__unused__)) = (variables != gVariables) ? variables->get("decryptedText") : g->GV(decryptedText);

  LINE(3,x_error_reporting(toInt32(8191LL)));
  (v_Sbox = ScalarArrays::sa_[2]);
  (v_Rcon = ScalarArrays::sa_[3]);
  (v_b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=");
  (v_plainText = toString("abcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890.\nabcdefghijklmnopqrstuvwxyz,-ABCDEFGHIJKLMNOPQRSTUVWXYZ,-1234567890."));
  (v_password = "SuperSecretThatIsVeryVeryVeryLong");
  {
    LOOP_COUNTER(38);
    for ((v_i = 0LL); not_less(v_i, 5LL); v_i++) {
      LOOP_COUNTER_CHECK(38);
      {
        concat_assign(v_plainText, toString(v_plainText));
      }
    }
  }
  (v_cipherText = LINE(621,f_aesencryptctr(v_plainText, v_password, 256LL)));
  (v_decryptedText = LINE(622,f_aesdecryptctr(v_cipherText, v_password, 256LL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
