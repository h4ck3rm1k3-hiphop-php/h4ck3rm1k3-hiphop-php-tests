
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "addroundkey", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "aesdecryptctr", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "aesencryptctr", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "bytearraytohexstr", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "cipher", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "decodebase64", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "decodeutf8", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "encodebase64", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "encodeutf8", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "escctrlchars", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "keyexpansion", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "mixcolumns", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "rotword", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "setlength", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "shiftrows", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "subbytes", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "subword", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "unescctrlchars", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  "zerofill", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
