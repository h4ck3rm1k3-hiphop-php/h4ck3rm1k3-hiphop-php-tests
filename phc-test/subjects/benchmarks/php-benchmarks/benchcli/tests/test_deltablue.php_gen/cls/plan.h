
#ifndef __GENERATED_cls_plan_h__
#define __GENERATED_cls_plan_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 557 */
class c_plan : virtual public ObjectData {
  BEGIN_CLASS_MAP(plan)
  END_CLASS_MAP(plan)
  DECLARE_CLASS(plan, Plan, ObjectData)
  void init();
  public: Variant m_v;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addconstraint(Variant v_c);
  public: Variant t_size();
  public: Variant t_constraintat(Variant v_index);
  public: void t_execute();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_plan_h__
