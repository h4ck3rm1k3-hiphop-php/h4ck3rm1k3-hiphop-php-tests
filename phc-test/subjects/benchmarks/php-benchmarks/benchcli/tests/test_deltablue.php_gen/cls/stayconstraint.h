
#ifndef __GENERATED_cls_stayconstraint_h__
#define __GENERATED_cls_stayconstraint_h__

#include <cls/unaryconstraint.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 336 */
class c_stayconstraint : virtual public c_unaryconstraint {
  BEGIN_CLASS_MAP(stayconstraint)
    PARENT_CLASS(constraint)
    PARENT_CLASS(unaryconstraint)
  END_CLASS_MAP(stayconstraint)
  DECLARE_CLASS(stayconstraint, StayConstraint, unaryconstraint)
  void init();
  public: void t___construct(Variant v_v, Variant v_str);
  public: ObjectData *create(Variant v_v, Variant v_str);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_execute();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_stayconstraint_h__
