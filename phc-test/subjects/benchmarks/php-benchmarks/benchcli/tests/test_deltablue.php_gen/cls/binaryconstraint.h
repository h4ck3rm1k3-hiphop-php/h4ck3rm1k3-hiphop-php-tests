
#ifndef __GENERATED_cls_binaryconstraint_h__
#define __GENERATED_cls_binaryconstraint_h__

#include <cls/constraint.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 357 */
class c_binaryconstraint : virtual public c_constraint {
  BEGIN_CLASS_MAP(binaryconstraint)
    PARENT_CLASS(constraint)
  END_CLASS_MAP(binaryconstraint)
  DECLARE_CLASS(binaryconstraint, BinaryConstraint, constraint)
  void init();
  public: Variant m_v1;
  public: Variant m_v2;
  public: Variant m_direction;
  public: int64 m_backward;
  public: int64 m_nodirection;
  public: int64 m_forward;
  public: void t___construct(Variant v_var1, Variant v_var2, Variant v_strength);
  public: ObjectData *create(Variant v_var1, Variant v_var2, Variant v_strength);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: bool t_issatisfied();
  public: void t_addtograph();
  public: void t_removefromgraph();
  public: void t_choosemethod(CVarRef v_mark);
  public: void t_markunsatisfied();
  public: void t_markinputs(CVarRef v_mark);
  public: bool t_inputsknown(CVarRef v_mark);
  public: Variant t_output();
  public: Variant t_input();
  public: void t_recalculate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_binaryconstraint_h__
