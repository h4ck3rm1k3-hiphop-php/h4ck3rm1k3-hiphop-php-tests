
#ifndef __GENERATED_cls_unaryconstraint_h__
#define __GENERATED_cls_unaryconstraint_h__

#include <cls/constraint.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 249 */
class c_unaryconstraint : virtual public c_constraint {
  BEGIN_CLASS_MAP(unaryconstraint)
    PARENT_CLASS(constraint)
  END_CLASS_MAP(unaryconstraint)
  DECLARE_CLASS(unaryconstraint, UnaryConstraint, constraint)
  void init();
  public: Variant m_myOutput;
  public: Variant m_satisfied;
  public: void t___construct(Variant v_v, Variant v_strength);
  public: ObjectData *create(Variant v_v, Variant v_strength);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_issatisfied();
  public: void t_markunsatisfied();
  public: Variant t_output();
  public: void t_addtograph();
  public: void t_removefromgraph();
  public: void t_choosemethod(CVarRef v_mark);
  public: void t_markinputs(CVarRef v_mark);
  public: bool t_inputsknown(CVarRef v_mark);
  public: void t_recalculate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_unaryconstraint_h__
