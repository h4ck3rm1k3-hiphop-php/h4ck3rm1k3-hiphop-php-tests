
#ifndef __GENERATED_cls_planner_h__
#define __GENERATED_cls_planner_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 585 */
class c_planner : virtual public ObjectData {
  BEGIN_CLASS_MAP(planner)
  END_CLASS_MAP(planner)
  DECLARE_CLASS(planner, Planner, ObjectData)
  void init();
  public: int64 m_currentMark;
  public: int64 t_newmark();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_incrementaladd(CVarRef v_c);
  public: void t_incrementalremove(CVarRef v_c);
  public: bool t_addpropagate(CVarRef v_c, CVarRef v_mark);
  public: void t_propagatefrom(CVarRef v_v);
  public: p_orderedcollection t_removepropagatefrom(Variant v_out);
  public: p_plan t_extractplanfromconstraints(CVarRef v_constraints);
  public: p_plan t_makeplan(p_orderedcollection v_sources);
  public: void t_addconstraintsconsumingto(CVarRef v_v, p_orderedcollection v_coll);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_planner_h__
