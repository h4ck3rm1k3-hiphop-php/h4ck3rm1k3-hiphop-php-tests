
#ifndef __GENERATED_cls_equalityconstraint_h__
#define __GENERATED_cls_equalityconstraint_h__

#include <cls/binaryconstraint.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 458 */
class c_equalityconstraint : virtual public c_binaryconstraint {
  BEGIN_CLASS_MAP(equalityconstraint)
    PARENT_CLASS(constraint)
    PARENT_CLASS(binaryconstraint)
  END_CLASS_MAP(equalityconstraint)
  DECLARE_CLASS(equalityconstraint, EqualityConstraint, binaryconstraint)
  void init();
  public: void t___construct(Variant v_var1, Variant v_var2, Variant v_strength);
  public: ObjectData *create(Variant v_var1, Variant v_var2, Variant v_strength);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_execute();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_equalityconstraint_h__
