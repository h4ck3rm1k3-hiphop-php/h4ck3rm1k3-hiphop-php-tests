
#ifndef __GENERATED_cls_constraint_h__
#define __GENERATED_cls_constraint_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 175 */
class c_constraint : virtual public ObjectData {
  BEGIN_CLASS_MAP(constraint)
  END_CLASS_MAP(constraint)
  DECLARE_CLASS(constraint, Constraint, ObjectData)
  void init();
  public: Variant m_strength;
  public: void t___construct(Variant v_strength);
  public: ObjectData *create(Variant v_strength);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: bool t_isinput();
  public: void t_addconstraint();
  public: void t_destroyconstraint();
  public: Variant t_satisfy(Variant v_mark);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_constraint_h__
