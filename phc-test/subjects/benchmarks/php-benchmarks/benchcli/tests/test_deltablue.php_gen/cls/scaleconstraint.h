
#ifndef __GENERATED_cls_scaleconstraint_h__
#define __GENERATED_cls_scaleconstraint_h__

#include <cls/binaryconstraint.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 480 */
class c_scaleconstraint : virtual public c_binaryconstraint {
  BEGIN_CLASS_MAP(scaleconstraint)
    PARENT_CLASS(constraint)
    PARENT_CLASS(binaryconstraint)
  END_CLASS_MAP(scaleconstraint)
  DECLARE_CLASS(scaleconstraint, ScaleConstraint, binaryconstraint)
  void init();
  public: Variant m_scale;
  public: Variant m_offset;
  public: Variant m_planner;
  public: void t___construct(Variant v_src, Variant v_scale, Variant v_offset, Variant v_dest, Variant v_strength);
  public: ObjectData *create(Variant v_src, Variant v_scale, Variant v_offset, Variant v_dest, Variant v_strength);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addtograph();
  public: void t_removefromgraph();
  public: void t_markinputs(CVarRef v_mark);
  public: void t_execute();
  public: void t_recalculate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_scaleconstraint_h__
