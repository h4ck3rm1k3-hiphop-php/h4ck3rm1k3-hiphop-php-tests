
#ifndef __GENERATED_cls_strength_h__
#define __GENERATED_cls_strength_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 99 */
class c_strength : virtual public ObjectData {
  BEGIN_CLASS_MAP(strength)
  END_CLASS_MAP(strength)
  DECLARE_CLASS(strength, Strength, ObjectData)
  void init();
  public: Variant m_strengthValue;
  public: Variant m_name;
  public: void t___construct(Variant v_strengthValue, Variant v_name);
  public: ObjectData *create(Variant v_strengthValue, Variant v_name);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static bool ti_stronger(const char* cls, CVarRef v_s1, CVarRef v_s2);
  public: static bool ti_weaker(const char* cls, CVarRef v_s1, CVarRef v_s2);
  public: static Variant ti_weakestof(const char* cls, CVarRef v_s1, CVarRef v_s2);
  public: static Variant ti_strongest(const char* cls, Variant v_s1, Variant v_s2);
  public: Variant t_nextweaker();
  public: static Variant t_weakestof(CVarRef v_s1, CVarRef v_s2) { return ti_weakestof("strength", v_s1, v_s2); }
  public: static bool t_stronger(CVarRef v_s1, CVarRef v_s2) { return ti_stronger("strength", v_s1, v_s2); }
  public: static bool t_weaker(CVarRef v_s1, CVarRef v_s2) { return ti_weaker("strength", v_s1, v_s2); }
  public: static Variant t_strongest(CVarRef v_s1, CVarRef v_s2) { return ti_strongest("strength", v_s1, v_s2); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_strength_h__
