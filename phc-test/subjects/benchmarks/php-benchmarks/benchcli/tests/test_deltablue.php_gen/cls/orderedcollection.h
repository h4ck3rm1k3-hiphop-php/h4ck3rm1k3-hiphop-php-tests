
#ifndef __GENERATED_cls_orderedcollection_h__
#define __GENERATED_cls_orderedcollection_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 57 */
class c_orderedcollection : virtual public ObjectData {
  BEGIN_CLASS_MAP(orderedcollection)
  END_CLASS_MAP(orderedcollection)
  DECLARE_CLASS(orderedcollection, OrderedCollection, ObjectData)
  void init();
  public: Variant m_elms;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add(CVarRef v_elm);
  public: Variant t_at(CVarRef v_index);
  public: int t_size();
  public: Variant t_removefirst();
  public: void t_remove(CVarRef v_elm);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_orderedcollection_h__
