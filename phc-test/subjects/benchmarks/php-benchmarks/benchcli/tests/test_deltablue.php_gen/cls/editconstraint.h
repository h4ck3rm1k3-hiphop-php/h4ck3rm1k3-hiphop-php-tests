
#ifndef __GENERATED_cls_editconstraint_h__
#define __GENERATED_cls_editconstraint_h__

#include <cls/unaryconstraint.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 313 */
class c_editconstraint : virtual public c_unaryconstraint {
  BEGIN_CLASS_MAP(editconstraint)
    PARENT_CLASS(constraint)
    PARENT_CLASS(unaryconstraint)
  END_CLASS_MAP(editconstraint)
  DECLARE_CLASS(editconstraint, EditConstraint, unaryconstraint)
  void init();
  public: void t___construct(Variant v_v, Variant v_str);
  public: ObjectData *create(Variant v_v, Variant v_str);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: bool t_isinput();
  public: void t_execute();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_editconstraint_h__
