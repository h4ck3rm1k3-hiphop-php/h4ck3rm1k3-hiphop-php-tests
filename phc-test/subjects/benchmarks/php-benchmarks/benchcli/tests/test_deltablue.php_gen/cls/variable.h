
#ifndef __GENERATED_cls_variable_h__
#define __GENERATED_cls_variable_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 17 */
class c_variable : virtual public ObjectData {
  BEGIN_CLASS_MAP(variable)
  END_CLASS_MAP(variable)
  DECLARE_CLASS(variable, Variable, ObjectData)
  void init();
  public: Variant m_value;
  public: Variant m_constraints;
  public: Variant m_determinedBy;
  public: Variant m_mark;
  public: Variant m_walkStrength;
  public: Variant m_stay;
  public: Variant m_name;
  public: void t___construct(Variant v_name, Variant v_initialValue);
  public: ObjectData *create(Variant v_name, Variant v_initialValue);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addconstraint(Variant v_c);
  public: void t_removeconstraint(Variant v_c);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_variable_h__
