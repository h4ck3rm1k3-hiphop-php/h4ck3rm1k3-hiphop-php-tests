
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 23:
      HASH_RETURN(0x5A45ECE042FCD917LL, g->GV(STRONG_PREFERRED),
                  STRONG_PREFERRED);
      break;
    case 24:
      HASH_RETURN(0x5A128D8774F579D8LL, g->GV(NORMAL),
                  NORMAL);
      break;
    case 25:
      HASH_RETURN(0x602BE2950CC83319LL, g->GV(STRONG_DEFAULT),
                  STRONG_DEFAULT);
      HASH_RETURN(0x29B46F718BC34D59LL, g->GV(WEAK_DEFAULT),
                  WEAK_DEFAULT);
      break;
    case 27:
      HASH_RETURN(0x2857FF83605BBB1BLL, g->GV(WEAKEST),
                  WEAKEST);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 36:
      HASH_RETURN(0x54E270D09130CC24LL, g->GV(REQUIRED),
                  REQUIRED);
      HASH_RETURN(0x482D8885C16D5DA4LL, g->GV(planner),
                  planner);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 55:
      HASH_RETURN(0x1668C861DE80CE37LL, g->GV(PREFERRED),
                  PREFERRED);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
