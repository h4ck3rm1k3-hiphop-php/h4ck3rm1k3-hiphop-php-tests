
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_stayconstraint(CArrRef params, bool init = true);
Variant cw_stayconstraint$os_get(const char *s);
Variant &cw_stayconstraint$os_lval(const char *s);
Variant cw_stayconstraint$os_constant(const char *s);
Variant cw_stayconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_equalityconstraint(CArrRef params, bool init = true);
Variant cw_equalityconstraint$os_get(const char *s);
Variant &cw_equalityconstraint$os_lval(const char *s);
Variant cw_equalityconstraint$os_constant(const char *s);
Variant cw_equalityconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_variable(CArrRef params, bool init = true);
Variant cw_variable$os_get(const char *s);
Variant &cw_variable$os_lval(const char *s);
Variant cw_variable$os_constant(const char *s);
Variant cw_variable$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_binaryconstraint(CArrRef params, bool init = true);
Variant cw_binaryconstraint$os_get(const char *s);
Variant &cw_binaryconstraint$os_lval(const char *s);
Variant cw_binaryconstraint$os_constant(const char *s);
Variant cw_binaryconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_scaleconstraint(CArrRef params, bool init = true);
Variant cw_scaleconstraint$os_get(const char *s);
Variant &cw_scaleconstraint$os_lval(const char *s);
Variant cw_scaleconstraint$os_constant(const char *s);
Variant cw_scaleconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_strength(CArrRef params, bool init = true);
Variant cw_strength$os_get(const char *s);
Variant &cw_strength$os_lval(const char *s);
Variant cw_strength$os_constant(const char *s);
Variant cw_strength$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_unaryconstraint(CArrRef params, bool init = true);
Variant cw_unaryconstraint$os_get(const char *s);
Variant &cw_unaryconstraint$os_lval(const char *s);
Variant cw_unaryconstraint$os_constant(const char *s);
Variant cw_unaryconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_editconstraint(CArrRef params, bool init = true);
Variant cw_editconstraint$os_get(const char *s);
Variant &cw_editconstraint$os_lval(const char *s);
Variant cw_editconstraint$os_constant(const char *s);
Variant cw_editconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_plan(CArrRef params, bool init = true);
Variant cw_plan$os_get(const char *s);
Variant &cw_plan$os_lval(const char *s);
Variant cw_plan$os_constant(const char *s);
Variant cw_plan$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_orderedcollection(CArrRef params, bool init = true);
Variant cw_orderedcollection$os_get(const char *s);
Variant &cw_orderedcollection$os_lval(const char *s);
Variant cw_orderedcollection$os_constant(const char *s);
Variant cw_orderedcollection$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_constraint(CArrRef params, bool init = true);
Variant cw_constraint$os_get(const char *s);
Variant &cw_constraint$os_lval(const char *s);
Variant cw_constraint$os_constant(const char *s);
Variant cw_constraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_planner(CArrRef params, bool init = true);
Variant cw_planner$os_get(const char *s);
Variant &cw_planner$os_lval(const char *s);
Variant cw_planner$os_constant(const char *s);
Variant cw_planner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_CREATE_OBJECT(0x7975F9D59AE1B381LL, constraint);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x2C984380FDBF8563LL, strength);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x3122888B73FAF465LL, plan);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x15A11018F0281FA6LL, binaryconstraint);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x161364166188F127LL, scaleconstraint);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x6DBF26C83AA0E7CFLL, stayconstraint);
      HASH_CREATE_OBJECT(0x79047BDBA1F64DEFLL, equalityconstraint);
      HASH_CREATE_OBJECT(0x33191D48AA4C1A6FLL, variable);
      HASH_CREATE_OBJECT(0x13D93870401C7B2FLL, planner);
      break;
    case 17:
      HASH_CREATE_OBJECT(0x5A0E6937D0C5DF51LL, orderedcollection);
      break;
    case 29:
      HASH_CREATE_OBJECT(0x074E43E44EC8E7DDLL, unaryconstraint);
      HASH_CREATE_OBJECT(0x589C8C35FB710A7DLL, editconstraint);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x7975F9D59AE1B381LL, constraint);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x2C984380FDBF8563LL, strength);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x3122888B73FAF465LL, plan);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x15A11018F0281FA6LL, binaryconstraint);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x161364166188F127LL, scaleconstraint);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x6DBF26C83AA0E7CFLL, stayconstraint);
      HASH_INVOKE_STATIC_METHOD(0x79047BDBA1F64DEFLL, equalityconstraint);
      HASH_INVOKE_STATIC_METHOD(0x33191D48AA4C1A6FLL, variable);
      HASH_INVOKE_STATIC_METHOD(0x13D93870401C7B2FLL, planner);
      break;
    case 17:
      HASH_INVOKE_STATIC_METHOD(0x5A0E6937D0C5DF51LL, orderedcollection);
      break;
    case 29:
      HASH_INVOKE_STATIC_METHOD(0x074E43E44EC8E7DDLL, unaryconstraint);
      HASH_INVOKE_STATIC_METHOD(0x589C8C35FB710A7DLL, editconstraint);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x7975F9D59AE1B381LL, constraint);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x2C984380FDBF8563LL, strength);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x3122888B73FAF465LL, plan);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x15A11018F0281FA6LL, binaryconstraint);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x161364166188F127LL, scaleconstraint);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x6DBF26C83AA0E7CFLL, stayconstraint);
      HASH_GET_STATIC_PROPERTY(0x79047BDBA1F64DEFLL, equalityconstraint);
      HASH_GET_STATIC_PROPERTY(0x33191D48AA4C1A6FLL, variable);
      HASH_GET_STATIC_PROPERTY(0x13D93870401C7B2FLL, planner);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY(0x5A0E6937D0C5DF51LL, orderedcollection);
      break;
    case 29:
      HASH_GET_STATIC_PROPERTY(0x074E43E44EC8E7DDLL, unaryconstraint);
      HASH_GET_STATIC_PROPERTY(0x589C8C35FB710A7DLL, editconstraint);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x7975F9D59AE1B381LL, constraint);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x2C984380FDBF8563LL, strength);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x3122888B73FAF465LL, plan);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x15A11018F0281FA6LL, binaryconstraint);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x161364166188F127LL, scaleconstraint);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x6DBF26C83AA0E7CFLL, stayconstraint);
      HASH_GET_STATIC_PROPERTY_LV(0x79047BDBA1F64DEFLL, equalityconstraint);
      HASH_GET_STATIC_PROPERTY_LV(0x33191D48AA4C1A6FLL, variable);
      HASH_GET_STATIC_PROPERTY_LV(0x13D93870401C7B2FLL, planner);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY_LV(0x5A0E6937D0C5DF51LL, orderedcollection);
      break;
    case 29:
      HASH_GET_STATIC_PROPERTY_LV(0x074E43E44EC8E7DDLL, unaryconstraint);
      HASH_GET_STATIC_PROPERTY_LV(0x589C8C35FB710A7DLL, editconstraint);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x7975F9D59AE1B381LL, constraint);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x2C984380FDBF8563LL, strength);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x3122888B73FAF465LL, plan);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x15A11018F0281FA6LL, binaryconstraint);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x161364166188F127LL, scaleconstraint);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x6DBF26C83AA0E7CFLL, stayconstraint);
      HASH_GET_CLASS_CONSTANT(0x79047BDBA1F64DEFLL, equalityconstraint);
      HASH_GET_CLASS_CONSTANT(0x33191D48AA4C1A6FLL, variable);
      HASH_GET_CLASS_CONSTANT(0x13D93870401C7B2FLL, planner);
      break;
    case 17:
      HASH_GET_CLASS_CONSTANT(0x5A0E6937D0C5DF51LL, orderedcollection);
      break;
    case 29:
      HASH_GET_CLASS_CONSTANT(0x074E43E44EC8E7DDLL, unaryconstraint);
      HASH_GET_CLASS_CONSTANT(0x589C8C35FB710A7DLL, editconstraint);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
