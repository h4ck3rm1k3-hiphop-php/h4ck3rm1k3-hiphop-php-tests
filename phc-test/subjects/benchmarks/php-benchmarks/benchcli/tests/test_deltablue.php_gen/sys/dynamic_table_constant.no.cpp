
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 15) {
    case 4:
      HASH_RETURN(0x54E270D09130CC24LL, g->k_REQUIRED, REQUIRED);
      break;
    case 7:
      HASH_RETURN(0x5A45ECE042FCD917LL, g->k_STRONG_PREFERRED, STRONG_PREFERRED);
      HASH_RETURN(0x1668C861DE80CE37LL, g->k_PREFERRED, PREFERRED);
      break;
    case 8:
      HASH_RETURN(0x5A128D8774F579D8LL, g->k_NORMAL, NORMAL);
      break;
    case 9:
      HASH_RETURN(0x602BE2950CC83319LL, g->k_STRONG_DEFAULT, STRONG_DEFAULT);
      HASH_RETURN(0x29B46F718BC34D59LL, g->k_WEAK_DEFAULT, WEAK_DEFAULT);
      break;
    case 11:
      HASH_RETURN(0x2857FF83605BBB1BLL, g->k_WEAKEST, WEAKEST);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
