
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "binaryconstraint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "constraint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "editconstraint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "equalityconstraint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "orderedcollection", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "plan", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "planner", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "scaleconstraint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "stayconstraint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "strength", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "unaryconstraint", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "variable", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "chaintest", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "change", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "deltablue", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "projectiontest", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  "run", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
