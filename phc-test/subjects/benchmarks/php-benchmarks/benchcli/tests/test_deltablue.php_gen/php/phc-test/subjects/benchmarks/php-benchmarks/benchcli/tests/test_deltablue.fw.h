
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_deltablue_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_deltablue_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(stayconstraint)
FORWARD_DECLARE_CLASS(equalityconstraint)
FORWARD_DECLARE_CLASS(variable)
FORWARD_DECLARE_CLASS(binaryconstraint)
FORWARD_DECLARE_CLASS(scaleconstraint)
FORWARD_DECLARE_CLASS(strength)
FORWARD_DECLARE_CLASS(unaryconstraint)
FORWARD_DECLARE_CLASS(editconstraint)
FORWARD_DECLARE_CLASS(plan)
FORWARD_DECLARE_CLASS(orderedcollection)
FORWARD_DECLARE_CLASS(constraint)
FORWARD_DECLARE_CLASS(planner)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_deltablue_fw_h__
