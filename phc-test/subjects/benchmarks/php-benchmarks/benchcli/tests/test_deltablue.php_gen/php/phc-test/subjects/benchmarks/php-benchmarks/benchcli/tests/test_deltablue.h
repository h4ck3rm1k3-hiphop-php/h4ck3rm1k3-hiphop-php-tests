
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_deltablue_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_deltablue_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.fw.h>

// Declarations
#include <cls/stayconstraint.h>
#include <cls/equalityconstraint.h>
#include <cls/variable.h>
#include <cls/binaryconstraint.h>
#include <cls/scaleconstraint.h>
#include <cls/strength.h>
#include <cls/unaryconstraint.h>
#include <cls/editconstraint.h>
#include <cls/plan.h>
#include <cls/orderedcollection.h>
#include <cls/constraint.h>
#include <cls/planner.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_change(Variant v_var, int64 v_newValue);
void f_projectiontest(int64 v_n);
void f_deltablue();
void f_run(int64 v_length);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_deltablue_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_chaintest(int64 v_n);
Object co_stayconstraint(CArrRef params, bool init = true);
Object co_equalityconstraint(CArrRef params, bool init = true);
Object co_variable(CArrRef params, bool init = true);
Object co_binaryconstraint(CArrRef params, bool init = true);
Object co_scaleconstraint(CArrRef params, bool init = true);
Object co_strength(CArrRef params, bool init = true);
Object co_unaryconstraint(CArrRef params, bool init = true);
Object co_editconstraint(CArrRef params, bool init = true);
Object co_plan(CArrRef params, bool init = true);
Object co_orderedcollection(CArrRef params, bool init = true);
Object co_constraint(CArrRef params, bool init = true);
Object co_planner(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_tests_test_deltablue_h__
