
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 336 */
Variant c_stayconstraint::os_get(const char *s, int64 hash) {
  return c_unaryconstraint::os_get(s, hash);
}
Variant &c_stayconstraint::os_lval(const char *s, int64 hash) {
  return c_unaryconstraint::os_lval(s, hash);
}
void c_stayconstraint::o_get(ArrayElementVec &props) const {
  c_unaryconstraint::o_get(props);
}
bool c_stayconstraint::o_exists(CStrRef s, int64 hash) const {
  return c_unaryconstraint::o_exists(s, hash);
}
Variant c_stayconstraint::o_get(CStrRef s, int64 hash) {
  return c_unaryconstraint::o_get(s, hash);
}
Variant c_stayconstraint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_unaryconstraint::o_set(s, hash, v, forInit);
}
Variant &c_stayconstraint::o_lval(CStrRef s, int64 hash) {
  return c_unaryconstraint::o_lval(s, hash);
}
Variant c_stayconstraint::os_constant(const char *s) {
  return c_unaryconstraint::os_constant(s);
}
IMPLEMENT_CLASS(stayconstraint)
ObjectData *c_stayconstraint::create(Variant v_v, Variant v_str) {
  init();
  t___construct(v_v, v_str);
  return this;
}
ObjectData *c_stayconstraint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_stayconstraint::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_stayconstraint::cloneImpl() {
  c_stayconstraint *obj = NEW(c_stayconstraint)();
  cloneSet(obj);
  return obj;
}
void c_stayconstraint::cloneSet(c_stayconstraint *clone) {
  c_unaryconstraint::cloneSet(clone);
}
Variant c_stayconstraint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(params.rvalAt(0)));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_unaryconstraint::o_invoke(s, params, hash, fatal);
}
Variant c_stayconstraint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(a0));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(a0), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(a0));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_unaryconstraint::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_stayconstraint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_unaryconstraint::os_invoke(c, s, params, hash, fatal);
}
Variant cw_stayconstraint$os_get(const char *s) {
  return c_stayconstraint::os_get(s, -1);
}
Variant &cw_stayconstraint$os_lval(const char *s) {
  return c_stayconstraint::os_lval(s, -1);
}
Variant cw_stayconstraint$os_constant(const char *s) {
  return c_stayconstraint::os_constant(s);
}
Variant cw_stayconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_stayconstraint::os_invoke(c, s, params, -1, fatal);
}
void c_stayconstraint::init() {
  c_unaryconstraint::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 339 */
void c_stayconstraint::t___construct(Variant v_v, Variant v_str) {
  INSTANCE_METHOD_INJECTION(StayConstraint, StayConstraint::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(341,c_unaryconstraint::t___construct(v_v, v_str));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 344 */
void c_stayconstraint::t_execute() {
  INSTANCE_METHOD_INJECTION(StayConstraint, StayConstraint::execute);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 458 */
Variant c_equalityconstraint::os_get(const char *s, int64 hash) {
  return c_binaryconstraint::os_get(s, hash);
}
Variant &c_equalityconstraint::os_lval(const char *s, int64 hash) {
  return c_binaryconstraint::os_lval(s, hash);
}
void c_equalityconstraint::o_get(ArrayElementVec &props) const {
  c_binaryconstraint::o_get(props);
}
bool c_equalityconstraint::o_exists(CStrRef s, int64 hash) const {
  return c_binaryconstraint::o_exists(s, hash);
}
Variant c_equalityconstraint::o_get(CStrRef s, int64 hash) {
  return c_binaryconstraint::o_get(s, hash);
}
Variant c_equalityconstraint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_binaryconstraint::o_set(s, hash, v, forInit);
}
Variant &c_equalityconstraint::o_lval(CStrRef s, int64 hash) {
  return c_binaryconstraint::o_lval(s, hash);
}
Variant c_equalityconstraint::os_constant(const char *s) {
  return c_binaryconstraint::os_constant(s);
}
IMPLEMENT_CLASS(equalityconstraint)
ObjectData *c_equalityconstraint::create(Variant v_var1, Variant v_var2, Variant v_strength) {
  init();
  t___construct(v_var1, v_var2, v_strength);
  return this;
}
ObjectData *c_equalityconstraint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_equalityconstraint::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_equalityconstraint::cloneImpl() {
  c_equalityconstraint *obj = NEW(c_equalityconstraint)();
  cloneSet(obj);
  return obj;
}
void c_equalityconstraint::cloneSet(c_equalityconstraint *clone) {
  c_binaryconstraint::cloneSet(clone);
}
Variant c_equalityconstraint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(params.rvalAt(0)));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_invoke(s, params, hash, fatal);
}
Variant c_equalityconstraint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(a0));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(a0), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(a0));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_equalityconstraint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_binaryconstraint::os_invoke(c, s, params, hash, fatal);
}
Variant cw_equalityconstraint$os_get(const char *s) {
  return c_equalityconstraint::os_get(s, -1);
}
Variant &cw_equalityconstraint$os_lval(const char *s) {
  return c_equalityconstraint::os_lval(s, -1);
}
Variant cw_equalityconstraint$os_constant(const char *s) {
  return c_equalityconstraint::os_constant(s);
}
Variant cw_equalityconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_equalityconstraint::os_invoke(c, s, params, -1, fatal);
}
void c_equalityconstraint::init() {
  c_binaryconstraint::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 461 */
void c_equalityconstraint::t___construct(Variant v_var1, Variant v_var2, Variant v_strength) {
  INSTANCE_METHOD_INJECTION(EqualityConstraint, EqualityConstraint::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant v_par;

  (v_par = LINE(463,x_get_parent_class(((Object)(this)))));
  LINE(464,c_binaryconstraint::t___construct(v_var1, v_var2, v_strength));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 468 */
void c_equalityconstraint::t_execute() {
  INSTANCE_METHOD_INJECTION(EqualityConstraint, EqualityConstraint::execute);
  (LINE(469,o_root_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)).o_lval("value", 0x69E7413AE0C88471LL) = t_input().o_get("value", 0x69E7413AE0C88471LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 17 */
Variant c_variable::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_variable::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_variable::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("value", m_value.isReferenced() ? ref(m_value) : m_value));
  props.push_back(NEW(ArrayElement)("constraints", m_constraints.isReferenced() ? ref(m_constraints) : m_constraints));
  props.push_back(NEW(ArrayElement)("determinedBy", m_determinedBy.isReferenced() ? ref(m_determinedBy) : m_determinedBy));
  props.push_back(NEW(ArrayElement)("mark", m_mark.isReferenced() ? ref(m_mark) : m_mark));
  props.push_back(NEW(ArrayElement)("walkStrength", m_walkStrength.isReferenced() ? ref(m_walkStrength) : m_walkStrength));
  props.push_back(NEW(ArrayElement)("stay", m_stay.isReferenced() ? ref(m_stay) : m_stay));
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  c_ObjectData::o_get(props);
}
bool c_variable::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_EXISTS_STRING(0x69E7413AE0C88471LL, value, 5);
      HASH_EXISTS_STRING(0x09C5DCFCA8935F61LL, determinedBy, 12);
      break;
    case 2:
      HASH_EXISTS_STRING(0x71AF3BF12FB19BA2LL, constraints, 11);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7814902B22458CC4LL, walkStrength, 12);
      break;
    case 12:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      break;
    case 14:
      HASH_EXISTS_STRING(0x514CD4C290174F1ELL, stay, 4);
      break;
    case 15:
      HASH_EXISTS_STRING(0x19D2840C0AB0CFCFLL, mark, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_variable::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      HASH_RETURN_STRING(0x09C5DCFCA8935F61LL, m_determinedBy,
                         determinedBy, 12);
      break;
    case 2:
      HASH_RETURN_STRING(0x71AF3BF12FB19BA2LL, m_constraints,
                         constraints, 11);
      break;
    case 4:
      HASH_RETURN_STRING(0x7814902B22458CC4LL, m_walkStrength,
                         walkStrength, 12);
      break;
    case 12:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    case 14:
      HASH_RETURN_STRING(0x514CD4C290174F1ELL, m_stay,
                         stay, 4);
      break;
    case 15:
      HASH_RETURN_STRING(0x19D2840C0AB0CFCFLL, m_mark,
                         mark, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_variable::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_SET_STRING(0x69E7413AE0C88471LL, m_value,
                      value, 5);
      HASH_SET_STRING(0x09C5DCFCA8935F61LL, m_determinedBy,
                      determinedBy, 12);
      break;
    case 2:
      HASH_SET_STRING(0x71AF3BF12FB19BA2LL, m_constraints,
                      constraints, 11);
      break;
    case 4:
      HASH_SET_STRING(0x7814902B22458CC4LL, m_walkStrength,
                      walkStrength, 12);
      break;
    case 12:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      break;
    case 14:
      HASH_SET_STRING(0x514CD4C290174F1ELL, m_stay,
                      stay, 4);
      break;
    case 15:
      HASH_SET_STRING(0x19D2840C0AB0CFCFLL, m_mark,
                      mark, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_variable::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      HASH_RETURN_STRING(0x09C5DCFCA8935F61LL, m_determinedBy,
                         determinedBy, 12);
      break;
    case 2:
      HASH_RETURN_STRING(0x71AF3BF12FB19BA2LL, m_constraints,
                         constraints, 11);
      break;
    case 4:
      HASH_RETURN_STRING(0x7814902B22458CC4LL, m_walkStrength,
                         walkStrength, 12);
      break;
    case 12:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    case 14:
      HASH_RETURN_STRING(0x514CD4C290174F1ELL, m_stay,
                         stay, 4);
      break;
    case 15:
      HASH_RETURN_STRING(0x19D2840C0AB0CFCFLL, m_mark,
                         mark, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_variable::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(variable)
ObjectData *c_variable::create(Variant v_name, Variant v_initialValue) {
  init();
  t___construct(v_name, v_initialValue);
  return this;
}
ObjectData *c_variable::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_variable::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_variable::cloneImpl() {
  c_variable *obj = NEW(c_variable)();
  cloneSet(obj);
  return obj;
}
void c_variable::cloneSet(c_variable *clone) {
  clone->m_value = m_value.isReferenced() ? ref(m_value) : m_value;
  clone->m_constraints = m_constraints.isReferenced() ? ref(m_constraints) : m_constraints;
  clone->m_determinedBy = m_determinedBy.isReferenced() ? ref(m_determinedBy) : m_determinedBy;
  clone->m_mark = m_mark.isReferenced() ? ref(m_mark) : m_mark;
  clone->m_walkStrength = m_walkStrength.isReferenced() ? ref(m_walkStrength) : m_walkStrength;
  clone->m_stay = m_stay.isReferenced() ? ref(m_stay) : m_stay;
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  ObjectData::cloneSet(clone);
}
Variant c_variable::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x1F886526FF5B6792LL, removeconstraint) {
        return (t_removeconstraint(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(params.rvalAt(0)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_variable::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x1F886526FF5B6792LL, removeconstraint) {
        return (t_removeconstraint(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(a0), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_variable::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_variable$os_get(const char *s) {
  return c_variable::os_get(s, -1);
}
Variant &cw_variable$os_lval(const char *s) {
  return c_variable::os_lval(s, -1);
}
Variant cw_variable$os_constant(const char *s) {
  return c_variable::os_constant(s);
}
Variant cw_variable$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_variable::os_invoke(c, s, params, -1, fatal);
}
void c_variable::init() {
  m_value = null;
  m_constraints = null;
  m_determinedBy = null;
  m_mark = null;
  m_walkStrength = null;
  m_stay = null;
  m_name = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 29 */
void c_variable::t___construct(Variant v_name, Variant v_initialValue) {
  INSTANCE_METHOD_INJECTION(Variable, Variable::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  (m_value = v_initialValue);
  (m_constraints = ((Object)(LINE(32,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))));
  (m_determinedBy = null);
  (m_mark = 0LL);
  (m_walkStrength = g->GV(WEAKEST));
  (m_stay = true);
  (m_name = v_name);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 41 */
void c_variable::t_addconstraint(Variant v_c) {
  INSTANCE_METHOD_INJECTION(Variable, Variable::addConstraint);
  LINE(43,m_constraints.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_c));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 48 */
void c_variable::t_removeconstraint(Variant v_c) {
  INSTANCE_METHOD_INJECTION(Variable, Variable::removeConstraint);
  LINE(50,m_constraints.o_invoke_few_args("remove", 0x508405C2FD14564ELL, 1, v_c));
  if (same(m_determinedBy, v_c)) (m_determinedBy = null);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 357 */
Variant c_binaryconstraint::os_get(const char *s, int64 hash) {
  return c_constraint::os_get(s, hash);
}
Variant &c_binaryconstraint::os_lval(const char *s, int64 hash) {
  return c_constraint::os_lval(s, hash);
}
void c_binaryconstraint::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("v1", m_v1.isReferenced() ? ref(m_v1) : m_v1));
  props.push_back(NEW(ArrayElement)("v2", m_v2.isReferenced() ? ref(m_v2) : m_v2));
  props.push_back(NEW(ArrayElement)("direction", m_direction.isReferenced() ? ref(m_direction) : m_direction));
  props.push_back(NEW(ArrayElement)("backward", m_backward));
  props.push_back(NEW(ArrayElement)("nodirection", m_nodirection));
  props.push_back(NEW(ArrayElement)("forward", m_forward));
  c_constraint::o_get(props);
}
bool c_binaryconstraint::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_EXISTS_STRING(0x079A01435AA36382LL, v1, 2);
      HASH_EXISTS_STRING(0x040A854A1A060102LL, forward, 7);
      break;
    case 7:
      HASH_EXISTS_STRING(0x0510BDBD02C38BA7LL, v2, 2);
      break;
    case 9:
      HASH_EXISTS_STRING(0x10014728FF258839LL, direction, 9);
      HASH_EXISTS_STRING(0x2328DAB8A0FD8519LL, backward, 8);
      break;
    case 12:
      HASH_EXISTS_STRING(0x53248383396117BCLL, nodirection, 11);
      break;
    default:
      break;
  }
  return c_constraint::o_exists(s, hash);
}
Variant c_binaryconstraint::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x079A01435AA36382LL, m_v1,
                         v1, 2);
      HASH_RETURN_STRING(0x040A854A1A060102LL, m_forward,
                         forward, 7);
      break;
    case 7:
      HASH_RETURN_STRING(0x0510BDBD02C38BA7LL, m_v2,
                         v2, 2);
      break;
    case 9:
      HASH_RETURN_STRING(0x10014728FF258839LL, m_direction,
                         direction, 9);
      HASH_RETURN_STRING(0x2328DAB8A0FD8519LL, m_backward,
                         backward, 8);
      break;
    case 12:
      HASH_RETURN_STRING(0x53248383396117BCLL, m_nodirection,
                         nodirection, 11);
      break;
    default:
      break;
  }
  return c_constraint::o_get(s, hash);
}
Variant c_binaryconstraint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_SET_STRING(0x079A01435AA36382LL, m_v1,
                      v1, 2);
      HASH_SET_STRING(0x040A854A1A060102LL, m_forward,
                      forward, 7);
      break;
    case 7:
      HASH_SET_STRING(0x0510BDBD02C38BA7LL, m_v2,
                      v2, 2);
      break;
    case 9:
      HASH_SET_STRING(0x10014728FF258839LL, m_direction,
                      direction, 9);
      HASH_SET_STRING(0x2328DAB8A0FD8519LL, m_backward,
                      backward, 8);
      break;
    case 12:
      HASH_SET_STRING(0x53248383396117BCLL, m_nodirection,
                      nodirection, 11);
      break;
    default:
      break;
  }
  return c_constraint::o_set(s, hash, v, forInit);
}
Variant &c_binaryconstraint::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x10014728FF258839LL, m_direction,
                         direction, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x079A01435AA36382LL, m_v1,
                         v1, 2);
      break;
    case 7:
      HASH_RETURN_STRING(0x0510BDBD02C38BA7LL, m_v2,
                         v2, 2);
      break;
    default:
      break;
  }
  return c_constraint::o_lval(s, hash);
}
Variant c_binaryconstraint::os_constant(const char *s) {
  return c_constraint::os_constant(s);
}
IMPLEMENT_CLASS(binaryconstraint)
ObjectData *c_binaryconstraint::create(Variant v_var1, Variant v_var2, Variant v_strength) {
  init();
  t___construct(v_var1, v_var2, v_strength);
  return this;
}
ObjectData *c_binaryconstraint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_binaryconstraint::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_binaryconstraint::cloneImpl() {
  c_binaryconstraint *obj = NEW(c_binaryconstraint)();
  cloneSet(obj);
  return obj;
}
void c_binaryconstraint::cloneSet(c_binaryconstraint *clone) {
  clone->m_v1 = m_v1.isReferenced() ? ref(m_v1) : m_v1;
  clone->m_v2 = m_v2.isReferenced() ? ref(m_v2) : m_v2;
  clone->m_direction = m_direction.isReferenced() ? ref(m_direction) : m_direction;
  clone->m_backward = m_backward;
  clone->m_nodirection = m_nodirection;
  clone->m_forward = m_forward;
  c_constraint::cloneSet(clone);
}
Variant c_binaryconstraint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_constraint::o_invoke(s, params, hash, fatal);
}
Variant c_binaryconstraint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(a0), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(a0));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_constraint::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_binaryconstraint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_constraint::os_invoke(c, s, params, hash, fatal);
}
Variant cw_binaryconstraint$os_get(const char *s) {
  return c_binaryconstraint::os_get(s, -1);
}
Variant &cw_binaryconstraint$os_lval(const char *s) {
  return c_binaryconstraint::os_lval(s, -1);
}
Variant cw_binaryconstraint$os_constant(const char *s) {
  return c_binaryconstraint::os_constant(s);
}
Variant cw_binaryconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_binaryconstraint::os_invoke(c, s, params, -1, fatal);
}
void c_binaryconstraint::init() {
  c_constraint::init();
  m_v1 = null;
  m_v2 = null;
  m_direction = null;
  m_backward = -1LL;
  m_nodirection = 0LL;
  m_forward = 1LL;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 367 */
void c_binaryconstraint::t___construct(Variant v_var1, Variant v_var2, Variant v_strength) {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(368,c_constraint::t___construct(v_strength));
  (m_v1 = v_var1);
  (m_v2 = v_var2);
  (m_direction = m_nodirection);
  LINE(372,t_addconstraint());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 376 */
bool c_binaryconstraint::t_issatisfied() {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::isSatisfied);
  return !equal(m_direction, m_nodirection);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 379 */
void c_binaryconstraint::t_addtograph() {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::addToGraph);
  LINE(381,m_v1.o_invoke_few_args("addConstraint", 0x42890456408CC8B6LL, 1, this));
  LINE(382,m_v2.o_invoke_few_args("addConstraint", 0x42890456408CC8B6LL, 1, this));
  (m_direction = m_nodirection);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 387 */
void c_binaryconstraint::t_removefromgraph() {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::removeFromGraph);
  if (!equal(m_v1, null)) LINE(389,m_v1.o_invoke_few_args("removeConstraint", 0x1F886526FF5B6792LL, 1, this));
  if (!equal(m_v2, null)) LINE(390,m_v2.o_invoke_few_args("removeConstraint", 0x1F886526FF5B6792LL, 1, this));
  (m_direction = m_nodirection);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 398 */
void c_binaryconstraint::t_choosemethod(CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::chooseMethod);
  if (equal(m_v1.o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark)) (m_direction = !equal(m_v2.o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark) && LINE(402,c_strength::t_stronger(m_strength, m_v2.o_get("walkStrength", 0x7814902B22458CC4LL))) ? ((m_forward)) : ((m_nodirection)));
  if (equal(m_v2.o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark)) (m_direction = !equal(m_v1.o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark) && LINE(407,c_strength::t_stronger(m_strength, m_v1.o_get("walkStrength", 0x7814902B22458CC4LL))) ? ((m_backward)) : ((m_nodirection)));
  if (LINE(411,c_strength::t_weaker(m_v1.o_get("walkStrength", 0x7814902B22458CC4LL), m_v2.o_get("walkStrength", 0x7814902B22458CC4LL)))) (m_direction = LINE(413,c_strength::t_stronger(m_strength, m_v1.o_get("walkStrength", 0x7814902B22458CC4LL))) ? ((m_backward)) : ((m_nodirection)));
  else (m_direction = LINE(416,c_strength::t_stronger(m_strength, m_v2.o_get("walkStrength", 0x7814902B22458CC4LL))) ? ((m_forward)) : ((m_nodirection)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 420 */
void c_binaryconstraint::t_markunsatisfied() {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::markUnsatisfied);
  (m_direction = m_nodirection);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 423 */
void c_binaryconstraint::t_markinputs(CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::markInputs);
  (LINE(425,t_input()).o_lval("mark", 0x19D2840C0AB0CFCFLL) = v_mark);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 428 */
bool c_binaryconstraint::t_inputsknown(CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::inputsKnown);
  Variant v_i;

  (v_i = LINE(430,t_input()));
  return equal(v_i.o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark) || toBoolean(v_i.o_get("stay", 0x514CD4C290174F1ELL)) || equal(v_i.o_get("determinedBy", 0x09C5DCFCA8935F61LL), null);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 435 */
Variant c_binaryconstraint::t_output() {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::output);
  return equal(m_direction, m_forward) ? ((Variant)(m_v2)) : ((Variant)(m_v1));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 438 */
Variant c_binaryconstraint::t_input() {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::input);
  return equal(m_direction, m_forward) ? ((Variant)(m_v1)) : ((Variant)(m_v2));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 444 */
void c_binaryconstraint::t_recalculate() {
  INSTANCE_METHOD_INJECTION(BinaryConstraint, BinaryConstraint::recalculate);
  Variant v_in;
  Variant v_out;

  (v_in = LINE(446,t_input()));
  (v_out = LINE(447,o_root_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)));
  (v_out.o_lval("walkStrength", 0x7814902B22458CC4LL) = LINE(448,c_strength::t_weakestof(m_strength, v_in.o_get("walkStrength", 0x7814902B22458CC4LL))));
  (v_out.o_lval("stay", 0x514CD4C290174F1ELL) = v_in.o_get("stay", 0x514CD4C290174F1ELL));
  if (toBoolean(v_out.o_get("stay", 0x514CD4C290174F1ELL))) LINE(450,o_root_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 480 */
Variant c_scaleconstraint::os_get(const char *s, int64 hash) {
  return c_binaryconstraint::os_get(s, hash);
}
Variant &c_scaleconstraint::os_lval(const char *s, int64 hash) {
  return c_binaryconstraint::os_lval(s, hash);
}
void c_scaleconstraint::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("scale", m_scale.isReferenced() ? ref(m_scale) : m_scale));
  props.push_back(NEW(ArrayElement)("offset", m_offset.isReferenced() ? ref(m_offset) : m_offset));
  props.push_back(NEW(ArrayElement)("planner", m_planner.isReferenced() ? ref(m_planner) : m_planner));
  c_binaryconstraint::o_get(props);
}
bool c_scaleconstraint::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_EXISTS_STRING(0x1D4612A55DAE3A1ALL, scale, 5);
      break;
    case 4:
      HASH_EXISTS_STRING(0x482D8885C16D5DA4LL, planner, 7);
      break;
    case 7:
      HASH_EXISTS_STRING(0x2381BD4159EBE8A7LL, offset, 6);
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_exists(s, hash);
}
Variant c_scaleconstraint::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x1D4612A55DAE3A1ALL, m_scale,
                         scale, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x482D8885C16D5DA4LL, m_planner,
                         planner, 7);
      break;
    case 7:
      HASH_RETURN_STRING(0x2381BD4159EBE8A7LL, m_offset,
                         offset, 6);
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_get(s, hash);
}
Variant c_scaleconstraint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_SET_STRING(0x1D4612A55DAE3A1ALL, m_scale,
                      scale, 5);
      break;
    case 4:
      HASH_SET_STRING(0x482D8885C16D5DA4LL, m_planner,
                      planner, 7);
      break;
    case 7:
      HASH_SET_STRING(0x2381BD4159EBE8A7LL, m_offset,
                      offset, 6);
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_set(s, hash, v, forInit);
}
Variant &c_scaleconstraint::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x1D4612A55DAE3A1ALL, m_scale,
                         scale, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x482D8885C16D5DA4LL, m_planner,
                         planner, 7);
      break;
    case 7:
      HASH_RETURN_STRING(0x2381BD4159EBE8A7LL, m_offset,
                         offset, 6);
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_lval(s, hash);
}
Variant c_scaleconstraint::os_constant(const char *s) {
  return c_binaryconstraint::os_constant(s);
}
IMPLEMENT_CLASS(scaleconstraint)
ObjectData *c_scaleconstraint::create(Variant v_src, Variant v_scale, Variant v_offset, Variant v_dest, Variant v_strength) {
  init();
  t___construct(v_src, v_scale, v_offset, v_dest, v_strength);
  return this;
}
ObjectData *c_scaleconstraint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
  } else return this;
}
void c_scaleconstraint::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
}
ObjectData *c_scaleconstraint::cloneImpl() {
  c_scaleconstraint *obj = NEW(c_scaleconstraint)();
  cloneSet(obj);
  return obj;
}
void c_scaleconstraint::cloneSet(c_scaleconstraint *clone) {
  clone->m_scale = m_scale.isReferenced() ? ref(m_scale) : m_scale;
  clone->m_offset = m_offset.isReferenced() ? ref(m_offset) : m_offset;
  clone->m_planner = m_planner.isReferenced() ? ref(m_planner) : m_planner;
  c_binaryconstraint::cloneSet(clone);
}
Variant c_scaleconstraint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(params.rvalAt(0)));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_invoke(s, params, hash, fatal);
}
Variant c_scaleconstraint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(a0));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(a0), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(a0));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2, a3, a4), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_binaryconstraint::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_scaleconstraint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_binaryconstraint::os_invoke(c, s, params, hash, fatal);
}
Variant cw_scaleconstraint$os_get(const char *s) {
  return c_scaleconstraint::os_get(s, -1);
}
Variant &cw_scaleconstraint$os_lval(const char *s) {
  return c_scaleconstraint::os_lval(s, -1);
}
Variant cw_scaleconstraint$os_constant(const char *s) {
  return c_scaleconstraint::os_constant(s);
}
Variant cw_scaleconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_scaleconstraint::os_invoke(c, s, params, -1, fatal);
}
void c_scaleconstraint::init() {
  c_binaryconstraint::init();
  m_scale = null;
  m_offset = null;
  m_planner = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 486 */
void c_scaleconstraint::t___construct(Variant v_src, Variant v_scale, Variant v_offset, Variant v_dest, Variant v_strength) {
  INSTANCE_METHOD_INJECTION(ScaleConstraint, ScaleConstraint::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_planner __attribute__((__unused__)) = g->GV(planner);
  (m_planner = gv_planner);
  (m_strength = v_strength);
  (m_v1 = v_src);
  (m_v2 = v_dest);
  (m_direction = m_nodirection);
  (m_scale = v_scale);
  (m_offset = v_offset);
  LINE(501,t_addconstraint());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 505 */
void c_scaleconstraint::t_addtograph() {
  INSTANCE_METHOD_INJECTION(ScaleConstraint, ScaleConstraint::addToGraph);
  LINE(507,c_binaryconstraint::t_addtograph());
  LINE(508,m_scale.o_invoke_few_args("addConstraint", 0x42890456408CC8B6LL, 1, this));
  LINE(509,m_offset.o_invoke_few_args("addConstraint", 0x42890456408CC8B6LL, 1, this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 513 */
void c_scaleconstraint::t_removefromgraph() {
  INSTANCE_METHOD_INJECTION(ScaleConstraint, ScaleConstraint::removeFromGraph);
  LINE(515,c_binaryconstraint::t_removefromgraph());
  if (!equal(m_scale, null)) LINE(516,m_scale.o_invoke_few_args("removeConstraint", 0x1F886526FF5B6792LL, 1, this));
  if (!equal(m_offset, null)) LINE(517,m_offset.o_invoke_few_args("removeConstraint", 0x1F886526FF5B6792LL, 1, this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 521 */
void c_scaleconstraint::t_markinputs(CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(ScaleConstraint, ScaleConstraint::markInputs);
  LINE(523,c_binaryconstraint::t_markinputs(v_mark));
  (m_scale.o_lval("mark", 0x19D2840C0AB0CFCFLL) = (m_offset.o_lval("mark", 0x19D2840C0AB0CFCFLL) = v_mark));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 528 */
void c_scaleconstraint::t_execute() {
  INSTANCE_METHOD_INJECTION(ScaleConstraint, ScaleConstraint::execute);
  if (equal(m_direction, m_forward)) (m_v2.o_lval("value", 0x69E7413AE0C88471LL) = m_v1.o_get("value", 0x69E7413AE0C88471LL) * m_scale.o_get("value", 0x69E7413AE0C88471LL) + m_offset.o_get("value", 0x69E7413AE0C88471LL));
  else (m_v1.o_lval("value", 0x69E7413AE0C88471LL) = divide((m_v2.o_get("value", 0x69E7413AE0C88471LL) - m_offset.o_get("value", 0x69E7413AE0C88471LL)), m_scale.o_get("value", 0x69E7413AE0C88471LL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 539 */
void c_scaleconstraint::t_recalculate() {
  INSTANCE_METHOD_INJECTION(ScaleConstraint, ScaleConstraint::recalculate);
  Variant v_in;
  Variant v_out;

  (v_in = LINE(541,t_input()));
  (v_out = LINE(542,o_root_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)));
  (v_out.o_lval("walkStrength", 0x7814902B22458CC4LL) = LINE(543,c_strength::t_weakestof(m_strength, v_in.o_get("walkStrength", 0x7814902B22458CC4LL))));
  (v_out.o_lval("stay", 0x514CD4C290174F1ELL) = toBoolean(v_in.o_get("stay", 0x514CD4C290174F1ELL)) && toBoolean(m_scale.o_get("stay", 0x514CD4C290174F1ELL)) && toBoolean(m_offset.o_get("stay", 0x514CD4C290174F1ELL)));
  if (toBoolean(v_out.o_get("stay", 0x514CD4C290174F1ELL))) LINE(545,o_root_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 99 */
Variant c_strength::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_strength::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_strength::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("strengthValue", m_strengthValue.isReferenced() ? ref(m_strengthValue) : m_strengthValue));
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  c_ObjectData::o_get(props);
}
bool c_strength::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      break;
    case 2:
      HASH_EXISTS_STRING(0x1E070BABBC7BA1BALL, strengthValue, 13);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_strength::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    case 2:
      HASH_RETURN_STRING(0x1E070BABBC7BA1BALL, m_strengthValue,
                         strengthValue, 13);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_strength::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      break;
    case 2:
      HASH_SET_STRING(0x1E070BABBC7BA1BALL, m_strengthValue,
                      strengthValue, 13);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_strength::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    case 2:
      HASH_RETURN_STRING(0x1E070BABBC7BA1BALL, m_strengthValue,
                         strengthValue, 13);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_strength::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(strength)
ObjectData *c_strength::create(Variant v_strengthValue, Variant v_name) {
  init();
  t___construct(v_strengthValue, v_name);
  return this;
}
ObjectData *c_strength::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_strength::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_strength::cloneImpl() {
  c_strength *obj = NEW(c_strength)();
  cloneSet(obj);
  return obj;
}
void c_strength::cloneSet(c_strength *clone) {
  clone->m_strengthValue = m_strengthValue.isReferenced() ? ref(m_strengthValue) : m_strengthValue;
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  ObjectData::cloneSet(clone);
}
Variant c_strength::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6310CA0179A45405LL, nextweaker) {
        return (t_nextweaker());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_strength::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x6310CA0179A45405LL, nextweaker) {
        return (t_nextweaker());
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_strength::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_strength$os_get(const char *s) {
  return c_strength::os_get(s, -1);
}
Variant &cw_strength$os_lval(const char *s) {
  return c_strength::os_lval(s, -1);
}
Variant cw_strength$os_constant(const char *s) {
  return c_strength::os_constant(s);
}
Variant cw_strength$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_strength::os_invoke(c, s, params, -1, fatal);
}
void c_strength::init() {
  m_strengthValue = null;
  m_name = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 103 */
void c_strength::t___construct(Variant v_strengthValue, Variant v_name) {
  INSTANCE_METHOD_INJECTION(Strength, Strength::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_strengthValue = v_strengthValue);
  (m_name = v_name);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 109 */
bool c_strength::ti_stronger(const char* cls, CVarRef v_s1, CVarRef v_s2) {
  STATIC_METHOD_INJECTION(Strength, Strength::stronger);
  return less(toObject(v_s1).o_get("strengthValue", 0x1E070BABBC7BA1BALL), toObject(v_s2).o_get("strengthValue", 0x1E070BABBC7BA1BALL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 114 */
bool c_strength::ti_weaker(const char* cls, CVarRef v_s1, CVarRef v_s2) {
  STATIC_METHOD_INJECTION(Strength, Strength::weaker);
  return more(toObject(v_s1).o_get("strengthValue", 0x1E070BABBC7BA1BALL), toObject(v_s2).o_get("strengthValue", 0x1E070BABBC7BA1BALL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 119 */
Variant c_strength::ti_weakestof(const char* cls, CVarRef v_s1, CVarRef v_s2) {
  STATIC_METHOD_INJECTION(Strength, Strength::weakestOf);
  return LINE(121,c_strength::t_weaker(v_s1, v_s2)) ? ((Variant)(v_s1)) : ((Variant)(v_s2));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 124 */
Variant c_strength::ti_strongest(const char* cls, Variant v_s1, Variant v_s2) {
  STATIC_METHOD_INJECTION(Strength, Strength::strongest);
  return toBoolean(LINE(126,throw_fatal("Using $this when not in object context"))) ? ((Variant)(v_s1)) : ((Variant)(v_s2));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 129 */
Variant c_strength::t_nextweaker() {
  INSTANCE_METHOD_INJECTION(Strength, Strength::nextWeaker);
  {
    switch (toInt64(m_strengthValue)) {
    case 0LL:
      {
        return ((Object)(LINE(133,p_strength(p_strength(NEWOBJ(c_strength)())->create(6LL, "weakest")))));
      }
    case 1LL:
      {
        return ((Object)(LINE(134,p_strength(p_strength(NEWOBJ(c_strength)())->create(5LL, "weakDefault")))));
      }
    case 2LL:
      {
        return ((Object)(LINE(135,p_strength(p_strength(NEWOBJ(c_strength)())->create(4LL, "normal")))));
      }
    case 3LL:
      {
        return ((Object)(LINE(136,p_strength(p_strength(NEWOBJ(c_strength)())->create(3LL, "strongDefault")))));
      }
    case 4LL:
      {
        return ((Object)(LINE(137,p_strength(p_strength(NEWOBJ(c_strength)())->create(2LL, "preferred")))));
      }
    case 5LL:
      {
        return ((Object)(LINE(138,p_strength(p_strength(NEWOBJ(c_strength)())->create(0LL, "required")))));
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 249 */
Variant c_unaryconstraint::os_get(const char *s, int64 hash) {
  return c_constraint::os_get(s, hash);
}
Variant &c_unaryconstraint::os_lval(const char *s, int64 hash) {
  return c_constraint::os_lval(s, hash);
}
void c_unaryconstraint::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("myOutput", m_myOutput.isReferenced() ? ref(m_myOutput) : m_myOutput));
  props.push_back(NEW(ArrayElement)("satisfied", m_satisfied.isReferenced() ? ref(m_satisfied) : m_satisfied));
  c_constraint::o_get(props);
}
bool c_unaryconstraint::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x34081D60CD6E8CADLL, myOutput, 8);
      HASH_EXISTS_STRING(0x5CF7DB718EBA0B61LL, satisfied, 9);
      break;
    default:
      break;
  }
  return c_constraint::o_exists(s, hash);
}
Variant c_unaryconstraint::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x34081D60CD6E8CADLL, m_myOutput,
                         myOutput, 8);
      HASH_RETURN_STRING(0x5CF7DB718EBA0B61LL, m_satisfied,
                         satisfied, 9);
      break;
    default:
      break;
  }
  return c_constraint::o_get(s, hash);
}
Variant c_unaryconstraint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x34081D60CD6E8CADLL, m_myOutput,
                      myOutput, 8);
      HASH_SET_STRING(0x5CF7DB718EBA0B61LL, m_satisfied,
                      satisfied, 9);
      break;
    default:
      break;
  }
  return c_constraint::o_set(s, hash, v, forInit);
}
Variant &c_unaryconstraint::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x34081D60CD6E8CADLL, m_myOutput,
                         myOutput, 8);
      HASH_RETURN_STRING(0x5CF7DB718EBA0B61LL, m_satisfied,
                         satisfied, 9);
      break;
    default:
      break;
  }
  return c_constraint::o_lval(s, hash);
}
Variant c_unaryconstraint::os_constant(const char *s) {
  return c_constraint::os_constant(s);
}
IMPLEMENT_CLASS(unaryconstraint)
ObjectData *c_unaryconstraint::create(Variant v_v, Variant v_strength) {
  init();
  t___construct(v_v, v_strength);
  return this;
}
ObjectData *c_unaryconstraint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_unaryconstraint::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_unaryconstraint::cloneImpl() {
  c_unaryconstraint *obj = NEW(c_unaryconstraint)();
  cloneSet(obj);
  return obj;
}
void c_unaryconstraint::cloneSet(c_unaryconstraint *clone) {
  clone->m_myOutput = m_myOutput.isReferenced() ? ref(m_myOutput) : m_myOutput;
  clone->m_satisfied = m_satisfied.isReferenced() ? ref(m_satisfied) : m_satisfied;
  c_constraint::cloneSet(clone);
}
Variant c_unaryconstraint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_constraint::o_invoke(s, params, hash, fatal);
}
Variant c_unaryconstraint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(a0), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(a0));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_constraint::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_unaryconstraint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_constraint::os_invoke(c, s, params, hash, fatal);
}
Variant cw_unaryconstraint$os_get(const char *s) {
  return c_unaryconstraint::os_get(s, -1);
}
Variant &cw_unaryconstraint$os_lval(const char *s) {
  return c_unaryconstraint::os_lval(s, -1);
}
Variant cw_unaryconstraint$os_constant(const char *s) {
  return c_unaryconstraint::os_constant(s);
}
Variant cw_unaryconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_unaryconstraint::os_invoke(c, s, params, -1, fatal);
}
void c_unaryconstraint::init() {
  c_constraint::init();
  m_myOutput = null;
  m_satisfied = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 254 */
void c_unaryconstraint::t___construct(Variant v_v, Variant v_strength) {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(256,c_constraint::t___construct(v_strength));
  (m_myOutput = v_v);
  (m_satisfied = false);
  LINE(259,t_addconstraint());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 263 */
Variant c_unaryconstraint::t_issatisfied() {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::isSatisfied);
  return m_satisfied;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 266 */
void c_unaryconstraint::t_markunsatisfied() {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::markUnsatisfied);
  (m_satisfied = false);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 269 */
Variant c_unaryconstraint::t_output() {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::output);
  return m_myOutput;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 272 */
void c_unaryconstraint::t_addtograph() {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::addToGraph);
  LINE(274,m_myOutput.o_invoke_few_args("addConstraint", 0x42890456408CC8B6LL, 1, this));
  (m_satisfied = false);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 279 */
void c_unaryconstraint::t_removefromgraph() {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::removeFromGraph);
  if (!equal(m_myOutput, null)) LINE(281,m_myOutput.o_invoke_few_args("removeConstraint", 0x1F886526FF5B6792LL, 1, this));
  ;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 286 */
void c_unaryconstraint::t_choosemethod(CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::chooseMethod);
  (m_satisfied = !equal(m_myOutput.o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark) && LINE(289,c_strength::t_stronger(m_strength, m_myOutput.o_get("walkStrength", 0x7814902B22458CC4LL))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 292 */
void c_unaryconstraint::t_markinputs(CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::markInputs);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 294 */
bool c_unaryconstraint::t_inputsknown(CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::inputsKnown);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 299 */
void c_unaryconstraint::t_recalculate() {
  INSTANCE_METHOD_INJECTION(UnaryConstraint, UnaryConstraint::recalculate);
  (m_myOutput.o_lval("walkStrength", 0x7814902B22458CC4LL) = m_strength);
  (m_myOutput.o_lval("stay", 0x514CD4C290174F1ELL) = !(toBoolean(LINE(302,o_root_invoke_few_args("isInput", 0x1EC631D7FF16CBFBLL, 0)))));
  if (toBoolean(m_myOutput.o_get("stay", 0x514CD4C290174F1ELL))) LINE(303,o_root_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 313 */
Variant c_editconstraint::os_get(const char *s, int64 hash) {
  return c_unaryconstraint::os_get(s, hash);
}
Variant &c_editconstraint::os_lval(const char *s, int64 hash) {
  return c_unaryconstraint::os_lval(s, hash);
}
void c_editconstraint::o_get(ArrayElementVec &props) const {
  c_unaryconstraint::o_get(props);
}
bool c_editconstraint::o_exists(CStrRef s, int64 hash) const {
  return c_unaryconstraint::o_exists(s, hash);
}
Variant c_editconstraint::o_get(CStrRef s, int64 hash) {
  return c_unaryconstraint::o_get(s, hash);
}
Variant c_editconstraint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_unaryconstraint::o_set(s, hash, v, forInit);
}
Variant &c_editconstraint::o_lval(CStrRef s, int64 hash) {
  return c_unaryconstraint::o_lval(s, hash);
}
Variant c_editconstraint::os_constant(const char *s) {
  return c_unaryconstraint::os_constant(s);
}
IMPLEMENT_CLASS(editconstraint)
ObjectData *c_editconstraint::create(Variant v_v, Variant v_str) {
  init();
  t___construct(v_v, v_str);
  return this;
}
ObjectData *c_editconstraint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_editconstraint::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_editconstraint::cloneImpl() {
  c_editconstraint *obj = NEW(c_editconstraint)();
  cloneSet(obj);
  return obj;
}
void c_editconstraint::cloneSet(c_editconstraint *clone) {
  c_unaryconstraint::cloneSet(clone);
}
Variant c_editconstraint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(params.rvalAt(0)));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_unaryconstraint::o_invoke(s, params, hash, fatal);
}
Variant c_editconstraint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 9:
      HASH_GUARD(0x7E6DAEA56B3685E9LL, addtograph) {
        return (t_addtograph(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x51CAF264B60E81AALL, issatisfied) {
        return (t_issatisfied());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(a0));
      }
      break;
    case 14:
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 15:
      HASH_GUARD(0x6A173DB082AB810FLL, recalculate) {
        return (t_recalculate(), null);
      }
      break;
    case 17:
      HASH_GUARD(0x6F5076C6CCDF4731LL, choosemethod) {
        return (t_choosemethod(a0), null);
      }
      HASH_GUARD(0x08819B6FD47F5191LL, markinputs) {
        return (t_markinputs(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 24:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2974B1382DDEA29ALL, markunsatisfied) {
        return (t_markunsatisfied(), null);
      }
      HASH_GUARD(0x6433725889B5435ALL, inputsknown) {
        return (t_inputsknown(a0));
      }
      break;
    case 27:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 28:
      HASH_GUARD(0x2DE850C1AE329AFCLL, removefromgraph) {
        return (t_removefromgraph(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      HASH_GUARD(0x07B89E83B77C677FLL, output) {
        return (t_output());
      }
      break;
    default:
      break;
  }
  return c_unaryconstraint::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_editconstraint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_unaryconstraint::os_invoke(c, s, params, hash, fatal);
}
Variant cw_editconstraint$os_get(const char *s) {
  return c_editconstraint::os_get(s, -1);
}
Variant &cw_editconstraint$os_lval(const char *s) {
  return c_editconstraint::os_lval(s, -1);
}
Variant cw_editconstraint$os_constant(const char *s) {
  return c_editconstraint::os_constant(s);
}
Variant cw_editconstraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_editconstraint::os_invoke(c, s, params, -1, fatal);
}
void c_editconstraint::init() {
  c_unaryconstraint::init();
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 315 */
void c_editconstraint::t___construct(Variant v_v, Variant v_str) {
  INSTANCE_METHOD_INJECTION(EditConstraint, EditConstraint::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(317,c_unaryconstraint::t___construct(v_v, v_str));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 321 */
bool c_editconstraint::t_isinput() {
  INSTANCE_METHOD_INJECTION(EditConstraint, EditConstraint::isInput);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 326 */
void c_editconstraint::t_execute() {
  INSTANCE_METHOD_INJECTION(EditConstraint, EditConstraint::execute);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 557 */
Variant c_plan::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_plan::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_plan::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("v", m_v.isReferenced() ? ref(m_v) : m_v));
  c_ObjectData::o_get(props);
}
bool c_plan::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x3C2F961831E4EF6BLL, v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_plan::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3C2F961831E4EF6BLL, m_v,
                         v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_plan::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x3C2F961831E4EF6BLL, m_v,
                      v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_plan::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3C2F961831E4EF6BLL, m_v,
                         v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_plan::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(plan)
ObjectData *c_plan::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_plan::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_plan::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_plan::cloneImpl() {
  c_plan *obj = NEW(c_plan)();
  cloneSet(obj);
  return obj;
}
void c_plan::cloneSet(c_plan *clone) {
  clone->m_v = m_v.isReferenced() ? ref(m_v) : m_v;
  ObjectData::cloneSet(clone);
}
Variant c_plan::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3A88D49C06AF8890LL, size) {
        return (t_size());
      }
      break;
    case 6:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_plan::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x3A88D49C06AF8890LL, size) {
        return (t_size());
      }
      break;
    case 6:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(a0), null);
      }
      HASH_GUARD(0x187C7F43EB57714ELL, execute) {
        return (t_execute(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_plan::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_plan$os_get(const char *s) {
  return c_plan::os_get(s, -1);
}
Variant &cw_plan$os_lval(const char *s) {
  return c_plan::os_lval(s, -1);
}
Variant cw_plan$os_constant(const char *s) {
  return c_plan::os_constant(s);
}
Variant cw_plan$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_plan::os_invoke(c, s, params, -1, fatal);
}
void c_plan::init() {
  m_v = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 561 */
void c_plan::t___construct() {
  INSTANCE_METHOD_INJECTION(Plan, Plan::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_v = ((Object)(LINE(561,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 563 */
void c_plan::t_addconstraint(Variant v_c) {
  INSTANCE_METHOD_INJECTION(Plan, Plan::addConstraint);
  LINE(563,m_v.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_c));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 565 */
Variant c_plan::t_size() {
  INSTANCE_METHOD_INJECTION(Plan, Plan::size);
  return LINE(565,m_v.o_invoke_few_args("size", 0x3A88D49C06AF8890LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 567 */
Variant c_plan::t_constraintat(Variant v_index) {
  INSTANCE_METHOD_INJECTION(Plan, Plan::constraintAt);
  return LINE(568,m_v.o_invoke_few_args("at", 0x6A7BC022A439E4B7LL, 1, v_index));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 570 */
void c_plan::t_execute() {
  INSTANCE_METHOD_INJECTION(Plan, Plan::execute);
  int64 v_i = 0;
  Variant v_c;

  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, LINE(572,t_size())); ++v_i) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_c = LINE(573,t_constraintat(v_i)));
        LINE(574,v_c.o_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 57 */
Variant c_orderedcollection::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_orderedcollection::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_orderedcollection::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("elms", m_elms.isReferenced() ? ref(m_elms) : m_elms));
  c_ObjectData::o_get(props);
}
bool c_orderedcollection::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x59697BCC2318EF78LL, elms, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_orderedcollection::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x59697BCC2318EF78LL, m_elms,
                         elms, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_orderedcollection::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x59697BCC2318EF78LL, m_elms,
                      elms, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_orderedcollection::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x59697BCC2318EF78LL, m_elms,
                         elms, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_orderedcollection::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(orderedcollection)
ObjectData *c_orderedcollection::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_orderedcollection::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_orderedcollection::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_orderedcollection::cloneImpl() {
  c_orderedcollection *obj = NEW(c_orderedcollection)();
  cloneSet(obj);
  return obj;
}
void c_orderedcollection::cloneSet(c_orderedcollection *clone) {
  clone->m_elms = m_elms.isReferenced() ? ref(m_elms) : m_elms;
  ObjectData::cloneSet(clone);
}
Variant c_orderedcollection::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3A88D49C06AF8890LL, size) {
        return (t_size());
      }
      break;
    case 7:
      HASH_GUARD(0x6A7BC022A439E4B7LL, at) {
        return (t_at(params.rvalAt(0)));
      }
      break;
    case 11:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(params.rvalAt(0)), null);
      }
      break;
    case 14:
      HASH_GUARD(0x508405C2FD14564ELL, remove) {
        return (t_remove(params.rvalAt(0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_orderedcollection::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x3A88D49C06AF8890LL, size) {
        return (t_size());
      }
      break;
    case 7:
      HASH_GUARD(0x6A7BC022A439E4B7LL, at) {
        return (t_at(a0));
      }
      break;
    case 11:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(a0), null);
      }
      break;
    case 14:
      HASH_GUARD(0x508405C2FD14564ELL, remove) {
        return (t_remove(a0), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_orderedcollection::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_orderedcollection$os_get(const char *s) {
  return c_orderedcollection::os_get(s, -1);
}
Variant &cw_orderedcollection$os_lval(const char *s) {
  return c_orderedcollection::os_lval(s, -1);
}
Variant cw_orderedcollection$os_constant(const char *s) {
  return c_orderedcollection::os_constant(s);
}
Variant cw_orderedcollection$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_orderedcollection::os_invoke(c, s, params, -1, fatal);
}
void c_orderedcollection::init() {
  m_elms = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 59 */
void c_orderedcollection::t___construct() {
  INSTANCE_METHOD_INJECTION(OrderedCollection, OrderedCollection::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_elms = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 62 */
void c_orderedcollection::t_add(CVarRef v_elm) {
  INSTANCE_METHOD_INJECTION(OrderedCollection, OrderedCollection::add);
  m_elms.append((v_elm));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 65 */
Variant c_orderedcollection::t_at(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(OrderedCollection, OrderedCollection::at);
  return m_elms.rvalAt(v_index);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 69 */
int c_orderedcollection::t_size() {
  INSTANCE_METHOD_INJECTION(OrderedCollection, OrderedCollection::size);
  return LINE(71,x_count(m_elms));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 73 */
Variant c_orderedcollection::t_removefirst() {
  INSTANCE_METHOD_INJECTION(OrderedCollection, OrderedCollection::removeFirst);
  return LINE(75,x_array_pop(ref(lval(m_elms))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 77 */
void c_orderedcollection::t_remove(CVarRef v_elm) {
  INSTANCE_METHOD_INJECTION(OrderedCollection, OrderedCollection::remove);
  int64 v_index = 0;
  int64 v_skipped = 0;
  int64 v_i = 0;
  Variant v_value;

  (v_index = 0LL);
  (v_skipped = 0LL);
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, LINE(81,x_count(m_elms))); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        (v_value = m_elms.rvalAt(v_i));
        if (!same(v_value, v_elm)) {
          m_elms.set(v_index, (v_value));
          v_index++;
        }
        else {
          v_skipped++;
        }
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_skipped); v_i++) {
      LOOP_COUNTER_CHECK(4);
      LINE(95,x_array_pop(ref(lval(m_elms))));
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 175 */
Variant c_constraint::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_constraint::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_constraint::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("strength", m_strength.isReferenced() ? ref(m_strength) : m_strength));
  c_ObjectData::o_get(props);
}
bool c_constraint::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x57A71E293C26A08ALL, strength, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_constraint::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x57A71E293C26A08ALL, m_strength,
                         strength, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_constraint::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x57A71E293C26A08ALL, m_strength,
                      strength, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_constraint::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x57A71E293C26A08ALL, m_strength,
                         strength, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_constraint::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(constraint)
ObjectData *c_constraint::create(Variant v_strength) {
  init();
  t___construct(v_strength);
  return this;
}
ObjectData *c_constraint::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_constraint::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_constraint::cloneImpl() {
  c_constraint *obj = NEW(c_constraint)();
  cloneSet(obj);
  return obj;
}
void c_constraint::cloneSet(c_constraint *clone) {
  clone->m_strength = m_strength.isReferenced() ? ref(m_strength) : m_strength;
  ObjectData::cloneSet(clone);
}
Variant c_constraint::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 6:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_constraint::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 6:
      HASH_GUARD(0x42890456408CC8B6LL, addconstraint) {
        return (t_addconstraint(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x32E33527029E44B8LL, destroyconstraint) {
        return (t_destroyconstraint(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x1EC631D7FF16CBFBLL, isinput) {
        return (t_isinput());
      }
      break;
    case 12:
      HASH_GUARD(0x0705C3734BDEDC0CLL, satisfy) {
        return (t_satisfy(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_constraint::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_constraint$os_get(const char *s) {
  return c_constraint::os_get(s, -1);
}
Variant &cw_constraint$os_lval(const char *s) {
  return c_constraint::os_lval(s, -1);
}
Variant cw_constraint$os_constant(const char *s) {
  return c_constraint::os_constant(s);
}
Variant cw_constraint$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_constraint::os_invoke(c, s, params, -1, fatal);
}
void c_constraint::init() {
  m_strength = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 179 */
void c_constraint::t___construct(Variant v_strength) {
  INSTANCE_METHOD_INJECTION(Constraint, Constraint::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_planner __attribute__((__unused__)) = g->GV(planner);
  (o_lval("planner", 0x482D8885C16D5DA4LL) = gv_planner);
  (m_strength = v_strength);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 191 */
bool c_constraint::t_isinput() {
  INSTANCE_METHOD_INJECTION(Constraint, Constraint::isInput);
  return false;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 194 */
void c_constraint::t_addconstraint() {
  INSTANCE_METHOD_INJECTION(Constraint, Constraint::addConstraint);
  LINE(196,o_root_invoke_few_args("addToGraph", 0x7E6DAEA56B3685E9LL, 0));
  LINE(197,o_get("planner", 0x482D8885C16D5DA4LL).o_invoke_few_args("incrementalAdd", 0x0B7E6FD5339F463BLL, 1, this));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 203 */
void c_constraint::t_destroyconstraint() {
  INSTANCE_METHOD_INJECTION(Constraint, Constraint::destroyConstraint);
  if (toBoolean(LINE(205,o_root_invoke_few_args("isSatisfied", 0x51CAF264B60E81AALL, 0)))) o_get("planner", 0x482D8885C16D5DA4LL).o_invoke_few_args("incrementalRemove", 0x795305B37A686484LL, 1, this);
  LINE(206,o_root_invoke_few_args("removeFromGraph", 0x2DE850C1AE329AFCLL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 216 */
Variant c_constraint::t_satisfy(Variant v_mark) {
  INSTANCE_METHOD_INJECTION(Constraint, Constraint::satisfy);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_out;
  Variant v_overridden;

  LINE(218,o_root_invoke_few_args("chooseMethod", 0x6F5076C6CCDF4731LL, 1, v_mark));
  if (!(toBoolean(LINE(219,o_root_invoke_few_args("isSatisfied", 0x51CAF264B60E81AALL, 0))))) {
    if (same(m_strength, g->GV(REQUIRED))) {
      echo("Could not satisfy a required constraint");
    }
    return null;
  }
  LINE(227,o_root_invoke_few_args("markInputs", 0x08819B6FD47F5191LL, 1, v_mark));
  (v_out = LINE(228,o_root_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)));
  (v_overridden = v_out.o_get("determinedBy", 0x09C5DCFCA8935F61LL));
  if (!equal(v_overridden, null)) LINE(230,v_overridden.o_invoke_few_args("markUnsatisfied", 0x2974B1382DDEA29ALL, 0));
  (v_out.o_lval("determinedBy", 0x09C5DCFCA8935F61LL) = ((Object)(this)));
  if (!(toBoolean(LINE(232,o_get("planner", 0x482D8885C16D5DA4LL).o_invoke_few_args("addPropagate", 0x71D6B0593136B3AFLL, 2, this, v_mark))))) {
    echo("Cycle encountered");
    return null;
  }
  (v_out.o_lval("mark", 0x19D2840C0AB0CFCFLL) = v_mark);
  return v_overridden;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 585 */
Variant c_planner::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_planner::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_planner::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("currentMark", m_currentMark));
  c_ObjectData::o_get(props);
}
bool c_planner::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x5ED11AC120F07EF5LL, currentMark, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_planner::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x5ED11AC120F07EF5LL, m_currentMark,
                         currentMark, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_planner::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x5ED11AC120F07EF5LL, m_currentMark,
                      currentMark, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_planner::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_planner::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(planner)
ObjectData *c_planner::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_planner::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_planner::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_planner::cloneImpl() {
  c_planner *obj = NEW(c_planner)();
  cloneSet(obj);
  return obj;
}
void c_planner::cloneSet(c_planner *clone) {
  clone->m_currentMark = m_currentMark;
  ObjectData::cloneSet(clone);
}
Variant c_planner::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x48BFF30A37DC7953LL, extractplanfromconstraints) {
        return (t_extractplanfromconstraints(params.rvalAt(0)));
      }
      break;
    case 4:
      HASH_GUARD(0x795305B37A686484LL, incrementalremove) {
        return (t_incrementalremove(params.rvalAt(0)), null);
      }
      break;
    case 11:
      HASH_GUARD(0x0B7E6FD5339F463BLL, incrementaladd) {
        return (t_incrementaladd(params.rvalAt(0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x71D6B0593136B3AFLL, addpropagate) {
        return (t_addpropagate(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_planner::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x48BFF30A37DC7953LL, extractplanfromconstraints) {
        return (t_extractplanfromconstraints(a0));
      }
      break;
    case 4:
      HASH_GUARD(0x795305B37A686484LL, incrementalremove) {
        return (t_incrementalremove(a0), null);
      }
      break;
    case 11:
      HASH_GUARD(0x0B7E6FD5339F463BLL, incrementaladd) {
        return (t_incrementaladd(a0), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      HASH_GUARD(0x71D6B0593136B3AFLL, addpropagate) {
        return (t_addpropagate(a0, a1));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_planner::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_planner$os_get(const char *s) {
  return c_planner::os_get(s, -1);
}
Variant &cw_planner$os_lval(const char *s) {
  return c_planner::os_lval(s, -1);
}
Variant cw_planner$os_constant(const char *s) {
  return c_planner::os_constant(s);
}
Variant cw_planner$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_planner::os_invoke(c, s, params, -1, fatal);
}
void c_planner::init() {
  m_currentMark = 0LL;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 590 */
int64 c_planner::t_newmark() {
  INSTANCE_METHOD_INJECTION(Planner, Planner::newMark);
  return ++m_currentMark;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 592 */
void c_planner::t___construct() {
  INSTANCE_METHOD_INJECTION(Planner, Planner::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_currentMark = 0LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 610 */
void c_planner::t_incrementaladd(CVarRef v_c) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::incrementalAdd);
  Variant v_mark;
  Variant v_overridden;

  (v_mark = LINE(612,t_newmark()));
  (v_overridden = LINE(613,toObject(v_c)->o_invoke_few_args("satisfy", 0x0705C3734BDEDC0CLL, 1, v_mark)));
  LOOP_COUNTER(5);
  {
    while (!equal(v_overridden, null)) {
      LOOP_COUNTER_CHECK(5);
      {
        (v_overridden = LINE(615,v_overridden.o_invoke_few_args("satisfy", 0x0705C3734BDEDC0CLL, 1, v_mark)));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 630 */
void c_planner::t_incrementalremove(CVarRef v_c) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::incrementalRemove);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_out;
  p_orderedcollection v_unsatisfied;
  Variant v_strength;
  Variant v_i;
  Variant v_u;

  (v_out = LINE(632,toObject(v_c)->o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)));
  LINE(633,toObject(v_c)->o_invoke_few_args("markUnsatisfied", 0x2974B1382DDEA29ALL, 0));
  LINE(634,toObject(v_c)->o_invoke_few_args("removeFromGraph", 0x2DE850C1AE329AFCLL, 0));
  ((Object)((v_unsatisfied = ((Object)(LINE(635,t_removepropagatefrom(v_out)))))));
  (v_strength = g->GV(REQUIRED));
  {
    LOOP_COUNTER(6);
    do {
      LOOP_COUNTER_CHECK(6);
      {
        {
          LOOP_COUNTER(7);
          for ((v_i = 0LL); less(v_i, LINE(638,v_unsatisfied->t_size())); ++v_i) {
            LOOP_COUNTER_CHECK(7);
            {
              (v_u = LINE(639,v_unsatisfied->t_at(v_i)));
              if (equal(v_u.o_get("strength", 0x57A71E293C26A08ALL), v_strength)) LINE(641,t_incrementaladd(v_u));
            }
          }
        }
        (v_strength = LINE(643,v_strength.o_invoke_few_args("nextWeaker", 0x6310CA0179A45405LL, 0)));
      }
    } while (!equal(v_strength, g->GV(WEAKEST)));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 659 */
bool c_planner::t_addpropagate(CVarRef v_c, CVarRef v_mark) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::addPropagate);
  Variant eo_0;
  Variant eo_1;
  p_orderedcollection v_todo;
  Variant v_d;

  ((Object)((v_todo = ((Object)(LINE(661,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))))));
  LINE(662,v_todo->t_add(v_c));
  LOOP_COUNTER(8);
  {
    while (more(LINE(663,v_todo->t_size()), 0LL)) {
      LOOP_COUNTER_CHECK(8);
      {
        (v_d = LINE(664,v_todo->t_removefirst()));
        if (equal(LINE(665,v_d.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)).o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark)) {
          LINE(666,t_incrementalremove(v_c));
          return false;
        }
        LINE(670,v_d.o_invoke_few_args("recalculate", 0x6A173DB082AB810FLL, 0));
        (assignCallTemp(eo_0, LINE(671,v_d.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0))),assignCallTemp(eo_1, ((Object)(v_todo))),t_addconstraintsconsumingto(eo_0, eo_1));
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 678 */
void c_planner::t_propagatefrom(CVarRef v_v) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::propagateFrom);
  Variant eo_0;
  Variant eo_1;
  p_orderedcollection v_todo;
  Variant v_c;

  ((Object)((v_todo = ((Object)(LINE(680,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))))));
  LINE(681,t_addconstraintsconsumingto(v_v, ((Object)(v_todo))));
  LOOP_COUNTER(9);
  {
    while (more(LINE(682,o_get("todo", 0x7123E99DAC74B316LL).o_invoke_few_args("size", 0x3A88D49C06AF8890LL, 0)), 0LL)) {
      LOOP_COUNTER_CHECK(9);
      {
        (v_c = LINE(683,v_todo->t_removefirst()));
        LINE(684,v_c.o_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
        (assignCallTemp(eo_0, LINE(685,v_c.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0))),assignCallTemp(eo_1, ((Object)(v_todo))),t_addconstraintsconsumingto(eo_0, eo_1));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 693 */
p_orderedcollection c_planner::t_removepropagatefrom(Variant v_out) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::removePropagateFrom);
  DECLARE_GLOBAL_VARIABLES(g);
  p_orderedcollection v_unsatisfied;
  p_orderedcollection v_todo;
  Variant v_v;
  Variant v_i;
  Variant v_c;
  Variant v_determiningC;
  Variant v_nextC;

  (v_out.o_lval("determinedBy", 0x09C5DCFCA8935F61LL) = null);
  (v_out.o_lval("walkStrength", 0x7814902B22458CC4LL) = g->GV(WEAKEST));
  (v_out.o_lval("stay", 0x514CD4C290174F1ELL) = true);
  ((Object)((v_unsatisfied = ((Object)(LINE(698,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))))));
  ((Object)((v_todo = ((Object)(LINE(699,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))))));
  LINE(700,v_todo->t_add(v_out));
  LOOP_COUNTER(10);
  {
    while (more(LINE(701,v_todo->t_size()), 0LL)) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_v = LINE(702,v_todo->t_removefirst()));
        {
          LOOP_COUNTER(11);
          for ((v_i = 0LL); less(v_i, LINE(703,v_v.o_get("constraints", 0x71AF3BF12FB19BA2LL).o_invoke_few_args("size", 0x3A88D49C06AF8890LL, 0))); ++v_i) {
            LOOP_COUNTER_CHECK(11);
            {
              (v_c = LINE(704,v_v.o_get("constraints", 0x71AF3BF12FB19BA2LL).o_invoke_few_args("at", 0x6A7BC022A439E4B7LL, 1, v_i)));
              if (!(toBoolean(LINE(705,v_c.o_invoke_few_args("isSatisfied", 0x51CAF264B60E81AALL, 0))))) LINE(706,v_unsatisfied->t_add(v_c));
            }
          }
        }
        (v_determiningC = v_v.o_get("determinedBy", 0x09C5DCFCA8935F61LL));
        {
          LOOP_COUNTER(12);
          for ((v_i = 0LL); less(v_i, LINE(709,v_v.o_get("constraints", 0x71AF3BF12FB19BA2LL).o_invoke_few_args("size", 0x3A88D49C06AF8890LL, 0))); ++v_i) {
            LOOP_COUNTER_CHECK(12);
            {
              (v_nextC = LINE(710,v_v.o_get("constraints", 0x71AF3BF12FB19BA2LL).o_invoke_few_args("at", 0x6A7BC022A439E4B7LL, 1, v_i)));
              if (!same(v_nextC, v_determiningC) && toBoolean(LINE(711,v_nextC.o_invoke_few_args("isSatisfied", 0x51CAF264B60E81AALL, 0)))) {
                LINE(712,v_nextC.o_invoke_few_args("recalculate", 0x6A173DB082AB810FLL, 0));
                LINE(713,v_todo->t_add(v_nextC.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)));
              }
            }
          }
        }
      }
    }
  }
  return ((Object)(v_unsatisfied));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 723 */
p_plan c_planner::t_extractplanfromconstraints(CVarRef v_constraints) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::extractPlanFromConstraints);
  p_orderedcollection v_sources;
  Variant v_i;
  Variant v_c;

  ((Object)((v_sources = ((Object)(LINE(725,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))))));
  {
    LOOP_COUNTER(13);
    for ((v_i = 0LL); less(v_i, LINE(726,toObject(v_constraints)->o_invoke_few_args("size", 0x3A88D49C06AF8890LL, 0))); ++v_i) {
      LOOP_COUNTER_CHECK(13);
      {
        (v_c = LINE(727,toObject(v_constraints)->o_invoke_few_args("at", 0x6A7BC022A439E4B7LL, 1, v_i)));
        if (toBoolean(LINE(728,v_c.o_invoke_few_args("isInput", 0x1EC631D7FF16CBFBLL, 0))) && toBoolean(v_c.o_invoke_few_args("isSatisfied", 0x51CAF264B60E81AALL, 0))) LINE(729,v_sources->t_add(v_c));
      }
    }
  }
  return ((Object)(LINE(731,t_makeplan(((Object)(v_sources))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 752 */
p_plan c_planner::t_makeplan(p_orderedcollection v_sources) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::makePlan);
  Variant eo_0;
  Variant eo_1;
  Variant v_mark;
  p_plan v_plan;
  p_orderedcollection v_todo;
  Variant v_c;

  (v_mark = LINE(754,t_newmark()));
  ((Object)((v_plan = ((Object)(LINE(755,p_plan(p_plan(NEWOBJ(c_plan)())->create())))))));
  ((Object)((v_todo = ((Object)(v_sources)))));
  LOOP_COUNTER(14);
  {
    while (more(LINE(757,v_todo->t_size()), 0LL)) {
      LOOP_COUNTER_CHECK(14);
      {
        (v_c = LINE(758,v_todo->t_removefirst()));
        if (!equal(LINE(759,v_c.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)).o_get("mark", 0x19D2840C0AB0CFCFLL), v_mark) && toBoolean(v_c.o_invoke_few_args("inputsKnown", 0x6433725889B5435ALL, 1, v_mark))) {
          LINE(761,v_plan->t_addconstraint(v_c));
          (LINE(762,v_c.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0)).o_lval("mark", 0x19D2840C0AB0CFCFLL) = v_mark);
          (assignCallTemp(eo_0, LINE(763,v_c.o_invoke_few_args("output", 0x07B89E83B77C677FLL, 0))),assignCallTemp(eo_1, ((Object)(v_todo))),t_addconstraintsconsumingto(eo_0, eo_1));
        }
      }
    }
  }
  return ((Object)(v_plan));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 769 */
void c_planner::t_addconstraintsconsumingto(CVarRef v_v, p_orderedcollection v_coll) {
  INSTANCE_METHOD_INJECTION(Planner, Planner::addConstraintsConsumingTo);
  Variant v_determiningC;
  Variant v_cc;
  Variant v_i;
  Variant v_c;

  (v_determiningC = toObject(v_v).o_get("determinedBy", 0x09C5DCFCA8935F61LL));
  (v_cc = toObject(v_v).o_get("constraints", 0x71AF3BF12FB19BA2LL));
  {
    LOOP_COUNTER(15);
    for ((v_i = 0LL); less(v_i, LINE(773,v_cc.o_invoke_few_args("size", 0x3A88D49C06AF8890LL, 0))); ++v_i) {
      LOOP_COUNTER_CHECK(15);
      {
        (v_c = LINE(774,v_cc.o_invoke_few_args("at", 0x6A7BC022A439E4B7LL, 1, v_i)));
        if (!same(v_c, v_determiningC) && toBoolean(LINE(775,v_c.o_invoke_few_args("isSatisfied", 0x51CAF264B60E81AALL, 0)))) LINE(776,v_coll->t_add(v_c));
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 865 */
void f_change(Variant v_var, int64 v_newValue) {
  FUNCTION_INJECTION(change);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_planner __attribute__((__unused__)) = g->GV(planner);
  Variant v_editC;
  Variant v_editV;
  Variant v_plan;
  int64 v_i = 0;

  (v_editC = ((Object)(LINE(868,p_editconstraint(p_editconstraint(NEWOBJ(c_editconstraint)())->create(v_var, g->GV(PREFERRED)))))));
  (v_editV = ((Object)(LINE(869,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))));
  LINE(870,v_editV.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_editC));
  (v_plan = LINE(871,gv_planner.o_invoke_few_args("extractPlanFromConstraints", 0x48BFF30A37DC7953LL, 1, v_editV)));
  {
    LOOP_COUNTER(16);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(16);
      {
        (v_var.o_lval("value", 0x69E7413AE0C88471LL) = v_newValue);
        LINE(874,v_plan.o_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
      }
    }
  }
  LINE(876,v_editC.o_invoke_few_args("destroyConstraint", 0x32E33527029E44B8LL, 0));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 826 */
void f_projectiontest(int64 v_n) {
  FUNCTION_INJECTION(projectionTest);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_planner __attribute__((__unused__)) = g->GV(planner);
  p_variable v_scale;
  p_variable v_offset;
  Variant v_src;
  Variant v_dst;
  p_orderedcollection v_dests;
  int64 v_i = 0;

  (gv_planner = ((Object)(LINE(829,p_planner(p_planner(NEWOBJ(c_planner)())->create())))));
  ((Object)((v_scale = ((Object)(LINE(831,p_variable(p_variable(NEWOBJ(c_variable)())->create("scale", 10LL))))))));
  ((Object)((v_offset = ((Object)(LINE(832,p_variable(p_variable(NEWOBJ(c_variable)())->create("offset", 1000LL))))))));
  setNull(v_src);
  setNull(v_dst);
  ((Object)((v_dests = ((Object)(LINE(836,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))))));
  {
    LOOP_COUNTER(17);
    for ((v_i = 0LL); less(v_i, v_n); ++v_i) {
      LOOP_COUNTER_CHECK(17);
      {
        (v_src = ((Object)(LINE(839,p_variable(p_variable(NEWOBJ(c_variable)())->create(concat("src", toString(v_i)), v_i))))));
        (v_dst = ((Object)(LINE(840,p_variable(p_variable(NEWOBJ(c_variable)())->create(concat("dst", toString(v_i)), v_i))))));
        LINE(841,v_dests->t_add(v_dst));
        ((Object)(LINE(842,p_stayconstraint(p_stayconstraint(NEWOBJ(c_stayconstraint)())->create(v_src, g->GV(NORMAL))))));
        ((Object)(LINE(843,p_scaleconstraint(p_scaleconstraint(NEWOBJ(c_scaleconstraint)())->create(v_src, ((Object)(v_scale)), ((Object)(v_offset)), v_dst, g->GV(REQUIRED))))));
      }
    }
  }
  LINE(846,f_change(v_src, 17LL));
  if (!equal(v_dst.o_get("value", 0x69E7413AE0C88471LL), 1170LL)) echo("Projection test 1 failed!");
  LINE(849,f_change(v_dst, 1050LL));
  if (!equal(v_src.o_get("value", 0x69E7413AE0C88471LL), 5LL)) echo("Projection test 2 failed!");
  LINE(852,f_change(((Object)(v_scale)), 5LL));
  {
    LOOP_COUNTER(18);
    for ((v_i = 0LL); less(v_i, v_n - 1LL); ++v_i) {
      LOOP_COUNTER_CHECK(18);
      {
        if (!equal(LINE(854,v_dests->t_at(v_i)).o_get("value", 0x69E7413AE0C88471LL), v_i * 5LL + 1000LL)) echo("Projection test 3 failed!");
      }
    }
  }
  LINE(858,f_change(((Object)(v_offset)), 2000LL));
  {
    LOOP_COUNTER(19);
    for ((v_i = 0LL); less(v_i, v_n - 1LL); ++v_i) {
      LOOP_COUNTER_CHECK(19);
      {
        if (!equal(LINE(860,v_dests->t_at(v_i)).o_get("value", 0x69E7413AE0C88471LL), v_i * 5LL + 2000LL)) echo("Projection test 4 failed!");
      }
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 879 */
void f_deltablue() {
  FUNCTION_INJECTION(deltaBlue);
  LINE(880,f_chaintest(100LL));
  LINE(881,f_projectiontest(100LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 884 */
void f_run(int64 v_length) {
  FUNCTION_INJECTION(run);
  int64 v_i = 0;

  {
    LOOP_COUNTER(20);
    for ((v_i = 0LL); less(v_i, v_length); ++v_i) {
      LOOP_COUNTER_CHECK(20);
      LINE(887,f_deltablue());
    }
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php line 786 */
void f_chaintest(int64 v_n) {
  FUNCTION_INJECTION(chainTest);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_planner __attribute__((__unused__)) = g->GV(planner);
  Variant v_prev;
  Variant v_first;
  Variant v_last;
  int64 v_i = 0;
  String v_name;
  p_variable v_v;
  Variant v_editC;
  Variant v_editV;
  Variant v_plan;

  setNull(v_prev);
  setNull(v_first);
  setNull(v_last);
  {
    LOOP_COUNTER(21);
    for ((v_i = 0LL); not_more(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(21);
      {
        (v_name = concat("v", toString(v_i)));
        ((Object)((v_v = ((Object)(LINE(797,p_variable(p_variable(NEWOBJ(c_variable)())->create(v_name, 0LL))))))));
        if (!equal(v_prev, null)) ((Object)(LINE(799,p_equalityconstraint(p_equalityconstraint(NEWOBJ(c_equalityconstraint)())->create(v_prev, ((Object)(v_v)), g->GV(REQUIRED))))));
        if (equal(v_i, 0LL)) (v_first = ((Object)(v_v)));
        if (equal(v_i, v_n)) (v_last = ((Object)(v_v)));
        (v_prev = ((Object)(v_v)));
      }
    }
  }
  ((Object)(LINE(805,p_stayconstraint(p_stayconstraint(NEWOBJ(c_stayconstraint)())->create(v_last, g->GV(STRONG_DEFAULT))))));
  (v_editC = ((Object)(LINE(806,p_editconstraint(p_editconstraint(NEWOBJ(c_editconstraint)())->create(v_first, g->GV(PREFERRED)))))));
  (v_editV = ((Object)(LINE(807,p_orderedcollection(p_orderedcollection(NEWOBJ(c_orderedcollection)())->create())))));
  LINE(808,v_editV.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_editC));
  (v_plan = LINE(809,gv_planner.o_invoke_few_args("extractPlanFromConstraints", 0x48BFF30A37DC7953LL, 1, v_editV)));
  {
    LOOP_COUNTER(22);
    for ((v_i = 0LL); less(v_i, 100LL); v_i++) {
      LOOP_COUNTER_CHECK(22);
      {
        (v_first.o_lval("value", 0x69E7413AE0C88471LL) = v_i);
        LINE(812,v_plan.o_invoke_few_args("execute", 0x187C7F43EB57714ELL, 0));
        if (!equal(v_last.o_get("value", 0x69E7413AE0C88471LL), v_i)) echo("Chain test failed!");
      }
    }
  }
  LINE(817,v_editC.o_invoke_few_args("destroyConstraint", 0x32E33527029E44B8LL, 0));
} /* function */
Object co_stayconstraint(CArrRef params, bool init /* = true */) {
  return Object(p_stayconstraint(NEW(c_stayconstraint)())->dynCreate(params, init));
}
Object co_equalityconstraint(CArrRef params, bool init /* = true */) {
  return Object(p_equalityconstraint(NEW(c_equalityconstraint)())->dynCreate(params, init));
}
Object co_variable(CArrRef params, bool init /* = true */) {
  return Object(p_variable(NEW(c_variable)())->dynCreate(params, init));
}
Object co_binaryconstraint(CArrRef params, bool init /* = true */) {
  return Object(p_binaryconstraint(NEW(c_binaryconstraint)())->dynCreate(params, init));
}
Object co_scaleconstraint(CArrRef params, bool init /* = true */) {
  return Object(p_scaleconstraint(NEW(c_scaleconstraint)())->dynCreate(params, init));
}
Object co_strength(CArrRef params, bool init /* = true */) {
  return Object(p_strength(NEW(c_strength)())->dynCreate(params, init));
}
Object co_unaryconstraint(CArrRef params, bool init /* = true */) {
  return Object(p_unaryconstraint(NEW(c_unaryconstraint)())->dynCreate(params, init));
}
Object co_editconstraint(CArrRef params, bool init /* = true */) {
  return Object(p_editconstraint(NEW(c_editconstraint)())->dynCreate(params, init));
}
Object co_plan(CArrRef params, bool init /* = true */) {
  return Object(p_plan(NEW(c_plan)())->dynCreate(params, init));
}
Object co_orderedcollection(CArrRef params, bool init /* = true */) {
  return Object(p_orderedcollection(NEW(c_orderedcollection)())->dynCreate(params, init));
}
Object co_constraint(CArrRef params, bool init /* = true */) {
  return Object(p_constraint(NEW(c_constraint)())->dynCreate(params, init));
}
Object co_planner(CArrRef params, bool init /* = true */) {
  return Object(p_planner(NEW(c_planner)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_deltablue_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$tests$test_deltablue_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_REQUIRED __attribute__((__unused__)) = (variables != gVariables) ? variables->get("REQUIRED") : g->GV(REQUIRED);
  Variant &v_STRONG_PREFERRED __attribute__((__unused__)) = (variables != gVariables) ? variables->get("STRONG_PREFERRED") : g->GV(STRONG_PREFERRED);
  Variant &v_PREFERRED __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PREFERRED") : g->GV(PREFERRED);
  Variant &v_STRONG_DEFAULT __attribute__((__unused__)) = (variables != gVariables) ? variables->get("STRONG_DEFAULT") : g->GV(STRONG_DEFAULT);
  Variant &v_NORMAL __attribute__((__unused__)) = (variables != gVariables) ? variables->get("NORMAL") : g->GV(NORMAL);
  Variant &v_WEAK_DEFAULT __attribute__((__unused__)) = (variables != gVariables) ? variables->get("WEAK_DEFAULT") : g->GV(WEAK_DEFAULT);
  Variant &v_WEAKEST __attribute__((__unused__)) = (variables != gVariables) ? variables->get("WEAKEST") : g->GV(WEAKEST);
  Variant &v_planner __attribute__((__unused__)) = (variables != gVariables) ? variables->get("planner") : g->GV(planner);

  LINE(15,x_error_reporting(toInt32(8191LL)));
  (v_REQUIRED = ((Object)(LINE(143,p_strength(p_strength(NEWOBJ(c_strength)())->create(0LL, "required"))))));
  LINE(144,g->declareConstant("REQUIRED", g->k_REQUIRED, v_REQUIRED.o_get("strengthValue", 0x1E070BABBC7BA1BALL)));
  (v_STRONG_PREFERRED = ((Object)(LINE(146,p_strength(p_strength(NEWOBJ(c_strength)())->create(1LL, "strongPreferred"))))));
  LINE(147,g->declareConstant("STRONG_PREFERRED", g->k_STRONG_PREFERRED, v_STRONG_PREFERRED.o_get("strengthValue", 0x1E070BABBC7BA1BALL)));
  (v_PREFERRED = ((Object)(LINE(149,p_strength(p_strength(NEWOBJ(c_strength)())->create(2LL, "preferred"))))));
  LINE(150,g->declareConstant("PREFERRED", g->k_PREFERRED, v_PREFERRED.o_get("strengthValue", 0x1E070BABBC7BA1BALL)));
  (v_STRONG_DEFAULT = ((Object)(LINE(152,p_strength(p_strength(NEWOBJ(c_strength)())->create(3LL, "strongDefault"))))));
  LINE(153,g->declareConstant("STRONG_DEFAULT", g->k_STRONG_DEFAULT, v_STRONG_DEFAULT.o_get("strengthValue", 0x1E070BABBC7BA1BALL)));
  (v_NORMAL = ((Object)(LINE(155,p_strength(p_strength(NEWOBJ(c_strength)())->create(4LL, "normal"))))));
  LINE(156,g->declareConstant("NORMAL", g->k_NORMAL, v_NORMAL.o_get("strengthValue", 0x1E070BABBC7BA1BALL)));
  (v_WEAK_DEFAULT = ((Object)(LINE(158,p_strength(p_strength(NEWOBJ(c_strength)())->create(5LL, "weakDefault"))))));
  LINE(159,g->declareConstant("WEAK_DEFAULT", g->k_WEAK_DEFAULT, v_WEAK_DEFAULT.o_get("strengthValue", 0x1E070BABBC7BA1BALL)));
  (v_WEAKEST = ((Object)(LINE(161,p_strength(p_strength(NEWOBJ(c_strength)())->create(6LL, "weakest"))))));
  LINE(162,g->declareConstant("WEAKEST", g->k_WEAKEST, v_WEAKEST.o_get("strengthValue", 0x1E070BABBC7BA1BALL)));
  (v_planner = ((Object)(LINE(785,p_planner(p_planner(NEWOBJ(c_planner)())->create())))));
  LINE(890,f_run(10LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
