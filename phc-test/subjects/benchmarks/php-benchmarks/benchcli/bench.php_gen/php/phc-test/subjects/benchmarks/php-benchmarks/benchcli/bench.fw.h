
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_bench_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_bench_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(benchmark)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.fw.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.fw.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.fw.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.fw.h>

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_bench_fw_h__
