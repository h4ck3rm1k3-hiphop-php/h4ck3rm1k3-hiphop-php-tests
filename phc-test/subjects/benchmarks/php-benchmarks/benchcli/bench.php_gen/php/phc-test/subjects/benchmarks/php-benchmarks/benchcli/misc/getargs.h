
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_getargs_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_getargs_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.fw.h>

// Declarations
#include <cls/console_getargs_options.h>
#include <cls/console_getargs.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$getargs_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_console_getargs_options(CArrRef params, bool init = true);
Object co_console_getargs(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_getargs_h__
