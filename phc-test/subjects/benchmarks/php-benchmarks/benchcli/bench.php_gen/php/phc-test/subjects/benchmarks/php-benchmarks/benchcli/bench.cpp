
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 33 */
Variant c_benchmark::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_benchmark::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_benchmark::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("memory_limit", m_memory_limit.isReferenced() ? ref(m_memory_limit) : m_memory_limit));
  props.push_back(NEW(ArrayElement)("debug", m_debug.isReferenced() ? ref(m_debug) : m_debug));
  props.push_back(NEW(ArrayElement)("include", m_include.isReferenced() ? ref(m_include) : m_include));
  props.push_back(NEW(ArrayElement)("php", m_php.isReferenced() ? ref(m_php) : m_php));
  props.push_back(NEW(ArrayElement)("config", m_config.isReferenced() ? ref(m_config) : m_config));
  props.push_back(NEW(ArrayElement)("log_file", m_log_file.isReferenced() ? ref(m_log_file) : m_log_file));
  props.push_back(NEW(ArrayElement)("quite", m_quite.isReferenced() ? ref(m_quite) : m_quite));
  props.push_back(NEW(ArrayElement)("test_files", m_test_files.isReferenced() ? ref(m_test_files) : m_test_files));
  props.push_back(NEW(ArrayElement)("cachegrind", m_cachegrind.isReferenced() ? ref(m_cachegrind) : m_cachegrind));
  props.push_back(NEW(ArrayElement)("smapsparser", m_smapsparser.isReferenced() ? ref(m_smapsparser) : m_smapsparser));
  props.push_back(NEW(ArrayElement)("mem_usage", m_mem_usage.isReferenced() ? ref(m_mem_usage) : m_mem_usage));
  c_ObjectData::o_get(props);
}
bool c_benchmark::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 0:
      HASH_EXISTS_STRING(0x3499AD466F23FB20LL, log_file, 8);
      break;
    case 1:
      HASH_EXISTS_STRING(0x52B182B0A94E9C41LL, php, 3);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7E355B0066B680A4LL, mem_usage, 9);
      break;
    case 9:
      HASH_EXISTS_STRING(0x6BBF99BA80368269LL, config, 6);
      break;
    case 15:
      HASH_EXISTS_STRING(0x6F7C140FDFB62A8FLL, smapsparser, 11);
      break;
    case 17:
      HASH_EXISTS_STRING(0x42466A24EC29D691LL, include, 7);
      break;
    case 21:
      HASH_EXISTS_STRING(0x06C4348493F1F1D5LL, cachegrind, 10);
      break;
    case 22:
      HASH_EXISTS_STRING(0x482CE589DAE9D076LL, debug, 5);
      break;
    case 27:
      HASH_EXISTS_STRING(0x5B142F960B4196BBLL, quite, 5);
      break;
    case 29:
      HASH_EXISTS_STRING(0x6DE3CFB71905593DLL, test_files, 10);
      break;
    case 31:
      HASH_EXISTS_STRING(0x4A506FB5C16F227FLL, memory_limit, 12);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_benchmark::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 0:
      HASH_RETURN_STRING(0x3499AD466F23FB20LL, m_log_file,
                         log_file, 8);
      break;
    case 1:
      HASH_RETURN_STRING(0x52B182B0A94E9C41LL, m_php,
                         php, 3);
      break;
    case 4:
      HASH_RETURN_STRING(0x7E355B0066B680A4LL, m_mem_usage,
                         mem_usage, 9);
      break;
    case 9:
      HASH_RETURN_STRING(0x6BBF99BA80368269LL, m_config,
                         config, 6);
      break;
    case 15:
      HASH_RETURN_STRING(0x6F7C140FDFB62A8FLL, m_smapsparser,
                         smapsparser, 11);
      break;
    case 17:
      HASH_RETURN_STRING(0x42466A24EC29D691LL, m_include,
                         include, 7);
      break;
    case 21:
      HASH_RETURN_STRING(0x06C4348493F1F1D5LL, m_cachegrind,
                         cachegrind, 10);
      break;
    case 22:
      HASH_RETURN_STRING(0x482CE589DAE9D076LL, m_debug,
                         debug, 5);
      break;
    case 27:
      HASH_RETURN_STRING(0x5B142F960B4196BBLL, m_quite,
                         quite, 5);
      break;
    case 29:
      HASH_RETURN_STRING(0x6DE3CFB71905593DLL, m_test_files,
                         test_files, 10);
      break;
    case 31:
      HASH_RETURN_STRING(0x4A506FB5C16F227FLL, m_memory_limit,
                         memory_limit, 12);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_benchmark::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 0:
      HASH_SET_STRING(0x3499AD466F23FB20LL, m_log_file,
                      log_file, 8);
      break;
    case 1:
      HASH_SET_STRING(0x52B182B0A94E9C41LL, m_php,
                      php, 3);
      break;
    case 4:
      HASH_SET_STRING(0x7E355B0066B680A4LL, m_mem_usage,
                      mem_usage, 9);
      break;
    case 9:
      HASH_SET_STRING(0x6BBF99BA80368269LL, m_config,
                      config, 6);
      break;
    case 15:
      HASH_SET_STRING(0x6F7C140FDFB62A8FLL, m_smapsparser,
                      smapsparser, 11);
      break;
    case 17:
      HASH_SET_STRING(0x42466A24EC29D691LL, m_include,
                      include, 7);
      break;
    case 21:
      HASH_SET_STRING(0x06C4348493F1F1D5LL, m_cachegrind,
                      cachegrind, 10);
      break;
    case 22:
      HASH_SET_STRING(0x482CE589DAE9D076LL, m_debug,
                      debug, 5);
      break;
    case 27:
      HASH_SET_STRING(0x5B142F960B4196BBLL, m_quite,
                      quite, 5);
      break;
    case 29:
      HASH_SET_STRING(0x6DE3CFB71905593DLL, m_test_files,
                      test_files, 10);
      break;
    case 31:
      HASH_SET_STRING(0x4A506FB5C16F227FLL, m_memory_limit,
                      memory_limit, 12);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_benchmark::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 0:
      HASH_RETURN_STRING(0x3499AD466F23FB20LL, m_log_file,
                         log_file, 8);
      break;
    case 1:
      HASH_RETURN_STRING(0x52B182B0A94E9C41LL, m_php,
                         php, 3);
      break;
    case 4:
      HASH_RETURN_STRING(0x7E355B0066B680A4LL, m_mem_usage,
                         mem_usage, 9);
      break;
    case 9:
      HASH_RETURN_STRING(0x6BBF99BA80368269LL, m_config,
                         config, 6);
      break;
    case 15:
      HASH_RETURN_STRING(0x6F7C140FDFB62A8FLL, m_smapsparser,
                         smapsparser, 11);
      break;
    case 17:
      HASH_RETURN_STRING(0x42466A24EC29D691LL, m_include,
                         include, 7);
      break;
    case 21:
      HASH_RETURN_STRING(0x06C4348493F1F1D5LL, m_cachegrind,
                         cachegrind, 10);
      break;
    case 22:
      HASH_RETURN_STRING(0x482CE589DAE9D076LL, m_debug,
                         debug, 5);
      break;
    case 27:
      HASH_RETURN_STRING(0x5B142F960B4196BBLL, m_quite,
                         quite, 5);
      break;
    case 29:
      HASH_RETURN_STRING(0x6DE3CFB71905593DLL, m_test_files,
                         test_files, 10);
      break;
    case 31:
      HASH_RETURN_STRING(0x4A506FB5C16F227FLL, m_memory_limit,
                         memory_limit, 12);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_benchmark::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(benchmark)
ObjectData *c_benchmark::create() {
  init();
  t_benchmark();
  return this;
}
ObjectData *c_benchmark::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_benchmark::cloneImpl() {
  c_benchmark *obj = NEW(c_benchmark)();
  cloneSet(obj);
  return obj;
}
void c_benchmark::cloneSet(c_benchmark *clone) {
  clone->m_memory_limit = m_memory_limit.isReferenced() ? ref(m_memory_limit) : m_memory_limit;
  clone->m_debug = m_debug.isReferenced() ? ref(m_debug) : m_debug;
  clone->m_include = m_include.isReferenced() ? ref(m_include) : m_include;
  clone->m_php = m_php.isReferenced() ? ref(m_php) : m_php;
  clone->m_config = m_config.isReferenced() ? ref(m_config) : m_config;
  clone->m_log_file = m_log_file.isReferenced() ? ref(m_log_file) : m_log_file;
  clone->m_quite = m_quite.isReferenced() ? ref(m_quite) : m_quite;
  clone->m_test_files = m_test_files.isReferenced() ? ref(m_test_files) : m_test_files;
  clone->m_cachegrind = m_cachegrind.isReferenced() ? ref(m_cachegrind) : m_cachegrind;
  clone->m_smapsparser = m_smapsparser.isReferenced() ? ref(m_smapsparser) : m_smapsparser;
  clone->m_mem_usage = m_mem_usage.isReferenced() ? ref(m_mem_usage) : m_mem_usage;
  ObjectData::cloneSet(clone);
}
Variant c_benchmark::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_benchmark::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_benchmark::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_benchmark$os_get(const char *s) {
  return c_benchmark::os_get(s, -1);
}
Variant &cw_benchmark$os_lval(const char *s) {
  return c_benchmark::os_lval(s, -1);
}
Variant cw_benchmark$os_constant(const char *s) {
  return c_benchmark::os_constant(s);
}
Variant cw_benchmark$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_benchmark::os_invoke(c, s, params, -1, fatal);
}
void c_benchmark::init() {
  m_memory_limit = null;
  m_debug = null;
  m_include = null;
  m_php = null;
  m_config = null;
  m_log_file = null;
  m_quite = null;
  m_test_files = null;
  m_cachegrind = null;
  m_smapsparser = null;
  m_mem_usage = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 107 */
void c_benchmark::t_benchmark() {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::Benchmark);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_args;
  Variant v_tmpdir;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_args; Variant &v_tmpdir;
    VariableTable(Variant &r_args, Variant &r_tmpdir) : v_args(r_args), v_tmpdir(r_tmpdir) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x4AF7CD17F976719ELL, v_args,
                      args);
          HASH_RETURN(0x4E60097CEE8FD6CELL, v_tmpdir,
                      tmpdir);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_args, v_tmpdir);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  LINE(109,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$getargs_php(true, variables));
  LINE(110,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$timer_php(true, variables));
  LINE(111,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$smapsparser_php(true, variables));
  LINE(112,pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$cachegrindparser_php(true, variables));
  LINE(113,t_setconfig());
  (v_args = ref(LINE(114,c_console_getargs::t_factory(m_config))));
  (m_smapsparser = ((Object)(LINE(115,p_smapsparser(p_smapsparser(NEWOBJ(c_smapsparser)())->create())))));
  if (toBoolean(LINE(116,throw_fatal("unknown class pear", ((void*)NULL)))) || toBoolean(v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "help"))) {
    LINE(117,t_printhelp(v_args));
    f_exit();
  }
  (m_debug = LINE(121,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "debug")));
  (m_memory_limit = LINE(122,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "memory-limit")));
  if (equal(m_memory_limit, "")) {
    (m_memory_limit = 128LL);
  }
  (m_include = LINE(126,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "include")));
  if (LINE(127,x_is_array(m_include))) {
    m_include.append(("tests"));
  }
  else {
    (v_tmpdir = m_include);
    (m_include = ScalarArrays::sa_[0]);
    m_include.append((v_tmpdir));
    m_include.append(("tests"));
  }
  (m_php = LINE(137,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "php")));
  if (equal(m_php, "")) {
    (m_php = "php");
  }
  (m_log_file = LINE(141,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "log")));
  if (equal(m_log_file, "")) {
    (m_log_file = false);
  }
  (m_quite = LINE(145,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "quite")));
  (m_mem_usage = LINE(146,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "memory-usage")));
  (m_cachegrind = LINE(147,v_args.o_invoke_few_args("getValue", 0x56879BCEB40997E3LL, 1, "cachegrind")));
  LINE(148,t_settestfiles());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 158 */
void c_benchmark::t_console(CVarRef v_msg, CVarRef v_print //  = true
) {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::console);
  if (!(toBoolean(m_quite))) {
    if (toBoolean(v_print)) {
      echo(toString(v_msg));
    }
  }
  if (toBoolean(m_log_file) && toBoolean(v_print)) {
    LINE(166,x_file_put_contents(toString(m_log_file), v_msg, toInt32(8LL) /* FILE_APPEND */));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 175 */
void c_benchmark::t_setconfig() {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::setConfig);
  (m_config = ScalarArrays::sa_[4]);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 218 */
void c_benchmark::t_printhelp(CVarRef v_args) {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::printHelp);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_header;

  (v_header = LINE(221,(assignCallTemp(eo_1, x_basename(toString(g->gv__SERVER.rvalAt("SCRIPT_NAME", 0x307C33F84E6BA43BLL)))),concat3("Php Benchmark Example\nUsage: ", eo_1, " [options]\n\n"))));
  if (same(LINE(222,toObject(v_args)->o_invoke_few_args("getCode", 0x5C108B351DC3D04FLL, 0)), -2LL /* CONSOLE_GETARGS_ERROR_USER */)) {
    echo(concat(LINE(223,(assignCallTemp(eo_0, m_config),assignCallTemp(eo_1, v_header),assignCallTemp(eo_2, toObject(v_args)->o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0)),c_console_getargs::t_gethelp(eo_0, eo_1, eo_2))), "\n"));
  }
  else if (same(LINE(224,toObject(v_args)->o_invoke_few_args("getCode", 0x5C108B351DC3D04FLL, 0)), -3LL /* CONSOLE_GETARGS_HELP */)) {
    echo(concat(LINE(225,c_console_getargs::t_gethelp(m_config, v_header)), "\n"));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 245 */
void c_benchmark::t_settestfiles() {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::setTestFiles);
  Array v_files;
  Variant v_dir;
  Variant v_filename;
  Sequence v_t;
  Variant v_matches;

  (v_files = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    Variant map2 = m_include;
    for (ArrayIterPtr iter3 = map2.begin("benchmark"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_dir = iter3->second();
      {
        {
          LOOP_COUNTER(4);
          Variant map5 = LINE(249,x_glob(toString(v_dir) + toString("/test_*.php")));
          for (ArrayIterPtr iter6 = map5.begin("benchmark"); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_filename = iter6->second();
            {
              v_t.set("filename", (v_filename), 0x4A5D0431E1A3CAEDLL);
              LINE(251,x_preg_match("/test_(.+)[.]php$/i", toString(v_filename), ref(v_matches)));
              v_t.set("name", (v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 0x0BCDB293DC3CBDDCLL);
              v_files.append((v_t));
            }
          }
        }
      }
    }
  }
  (m_test_files = v_files);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 264 */
void c_benchmark::t_run() {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::run);
  Variant eo_0;
  Variant eo_1;
  p_timer v_timer;
  Variant v_totaltime;
  String v_datetime;
  String v_startstring;
  Variant v_test;
  String v_cmd;
  Variant v_out;
  Variant v_err;
  Variant v_exit;
  p_cachegrindparser v_cacheparser;
  Variant v_result;

  ((Object)((v_timer = ((Object)(LINE(266,p_timer(p_timer(NEWOBJ(c_timer)())->create())))))));
  (v_totaltime = 0LL);
  (v_datetime = LINE(268,(assignCallTemp(eo_1, toInt64(x_time())),x_date("Y-m-d H:i:s", eo_1))));
  (v_startstring = "");
  LINE(270,t_console(concat3("--------------- Bench.php ", v_datetime, "--------------\n")));
  {
    LOOP_COUNTER(7);
    Variant map8 = m_test_files;
    for (ArrayIterPtr iter9 = map8.begin("benchmark"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_test = iter9->second();
      {
        ;
        if (toBoolean(m_cachegrind)) {
          (v_startstring = "valgrind --tool=cachegrind --branch-sim=yes");
        }
        (v_cmd = concat(v_startstring, LINE(277,concat6(" ", toString(m_php), " -d memory_limit=", toString(m_memory_limit), "M ", toString(v_test.rvalAt("filename", 0x4A5D0431E1A3CAEDLL))))));
        LINE(278,t_console(v_cmd + toString("\n"), m_debug));
        LINE(279,v_timer->t_start());
        df_lambda_1(LINE(280,t_completeexec(v_cmd, null, 0LL)), v_out, v_err, v_exit);
        if (toBoolean(m_mem_usage)) {
          if (!(toBoolean(m_quite))) {
            LINE(283,m_smapsparser.o_invoke_few_args("printMaxUsage", 0x64E62EC7666796E0LL, 1, 10LL));
          }
          if (toBoolean(m_log_file)) {
            LINE(286,m_smapsparser.o_invoke_few_args("printMaxUsage", 0x64E62EC7666796E0LL, 2, 10LL, m_log_file));
          }
          LINE(288,m_smapsparser.o_invoke_few_args("clear", 0x31DA235C5A226667LL, 0));
        }
        if (toBoolean(m_cachegrind)) {
          ((Object)((v_cacheparser = ((Object)(LINE(291,p_cachegrindparser(p_cachegrindparser(NEWOBJ(c_cachegrindparser)())->create())))))));
          (v_result = LINE(292,v_cacheparser->t_parse(v_err)));
          LINE(293,x_print_r(v_result));
        }
        LINE(295,v_timer->t_stop());
        LINE(296,t_console(v_out, m_debug));
        LINE(297,t_console(concat5("Results from ", toString(v_test.rvalAt("name", 0x0BCDB293DC3CBDDCLL)), ": ", toString(v_timer->m_elapsed), "\n")));
        v_totaltime += v_timer->m_elapsed;
      }
    }
  }
  LINE(301,t_console(concat3("Total time for the benchmark: ", toString(v_totaltime), " seconds\n")));
  (v_datetime = LINE(303,(assignCallTemp(eo_1, toInt64(x_time())),x_date("Y-m-d H:i:s", eo_1))));
  LINE(304,t_console(concat3("-------------- END ", v_datetime, "---------------------\n")));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 316 */
Array c_benchmark::t_completeexec(CStrRef v_command, CVarRef v_stdin //  = null_variant
, int64 v_timeout //  = 20LL
) {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::completeExec);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Array v_descriptorspec;
  Variant v_pipes;
  Variant v_handle;
  Variant v_out;
  Variant v_err;
  int v_start_time = 0;
  Array v_status;
  Variant v_new_out;
  Variant v_new_err;
  Variant v_pid;
  Variant v_data;
  Variant v_exit_code;

  (v_descriptorspec = ScalarArrays::sa_[5]);
  (v_pipes = ScalarArrays::sa_[0]);
  (v_handle = LINE(322,(assignCallTemp(eo_0, v_command),assignCallTemp(eo_1, v_descriptorspec),assignCallTemp(eo_2, ref(v_pipes)),assignCallTemp(eo_3, toString(x_getcwd())),x_proc_open(eo_0, eo_1, eo_2, eo_3))));
  if (!same(v_stdin, null)) {
    LINE(326,x_fwrite(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_stdin)));
  }
  LINE(328,x_fclose(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  v_pipes.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
  LINE(332,x_stream_set_blocking(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(0LL)));
  LINE(333,x_stream_set_blocking(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)), toInt32(0LL)));
  (v_out = "");
  (v_err = "");
  (v_start_time = LINE(338,x_time()));
  {
    LOOP_COUNTER(10);
    do {
      LOOP_COUNTER_CHECK(10);
      {
        (v_status = LINE(340,x_proc_get_status(toObject(v_handle))));
        (v_new_out = LINE(345,x_stream_get_contents(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
        (v_new_err = LINE(346,x_stream_get_contents(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
        concat_assign(v_out, toString(v_new_out));
        concat_assign(v_err, toString(v_new_err));
        (v_pid = LINE(349,t_getchildren(ref(v_handle), ref(v_pipes))));
        if (toBoolean(m_mem_usage)) {
          if (toBoolean((v_data = LINE(351,m_smapsparser.o_invoke_few_args("readSmapsData", 0x6300CB84E8E4F1E0LL, 1, v_pid.refvalAt(0LL, 0x77CFA1EEF01BCA90LL)))))) {
            LINE(352,m_smapsparser.o_invoke_few_args("parseSmapsData", 0x0FE6271E623C8A7BLL, 1, v_data));
          }
        }
        if (!equal(v_timeout, 0LL) && more(LINE(355,x_time()), v_start_time + v_timeout)) {
          (v_out = LINE(356,x_stream_get_contents(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
          (v_err = LINE(357,x_stream_get_contents(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
          LINE(359,t_killproperly(ref(v_handle), ref(v_pipes)));
          return Array(ArrayInit(3).set(0, "Timeout").set(1, v_out).set(2, v_err).create());
        }
        LINE(367,x_usleep(toInt32(7000LL)));
      }
    } while (toBoolean(v_status.rvalAt("running", 0x2947552314E5C43CLL)));
  }
  LINE(369,x_stream_set_blocking(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(1LL)));
  LINE(370,x_stream_set_blocking(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)), toInt32(1LL)));
  concat_assign(v_out, toString(LINE(371,x_stream_get_contents(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL))))));
  concat_assign(v_err, toString(LINE(372,x_stream_get_contents(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL))))));
  (v_exit_code = v_status.rvalAt("exitcode", 0x3FCBA2655EC7FA85LL));
  LINE(375,t_killproperly(ref(v_handle), ref(v_pipes)));
  return Array(ArrayInit(3).set(0, v_out).set(1, v_err).set(2, v_exit_code).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 388 */
Variant c_benchmark::t_getchildren(Variant v_handle, Variant v_pipes) {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::getChildren);
  Variant eo_0;
  Variant eo_1;
  Array v_status;
  Variant v_ppid;
  Variant v_pids;

  (v_status = LINE(390,x_proc_get_status(toObject(v_handle))));
  (v_ppid = v_status.rvalAt("pid", 0x133F26DF225E6273LL));
  (v_pids = LINE(392,(assignCallTemp(eo_1, x_trim(f_shell_exec(toString("ps -o pid --no-heading --ppid ") + toString(v_ppid)))),x_preg_split("/\\s+/", eo_1))));
  return v_pids;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 403 */
void c_benchmark::t_killproperly(Variant v_handle, Variant v_pipes) {
  INSTANCE_METHOD_INJECTION(Benchmark, Benchmark::killProperly);
  Variant v_pids;
  Variant v_pipe;
  Variant v_pid;

  (v_pids = LINE(408,t_getchildren(ref(v_handle), ref(v_pipes))));
  {
    LOOP_COUNTER(11);
    Variant map12 = ref(v_pipes);
    map12.escalate();
    for (MutableArrayIterPtr iter13 = map12.begin(NULL, v_pipe); iter13->advance();) {
      LOOP_COUNTER_CHECK(11);
      {
        LINE(412,x_fclose(toObject(v_pipe)));
      }
    }
  }
  LINE(414,x_proc_terminate(toObject(v_handle)));
  LINE(415,x_proc_close(toObject(v_handle)));
  {
    {
      LOOP_COUNTER(14);
      for (ArrayIterPtr iter16 = v_pids.begin("benchmark"); !iter16->end(); iter16->next()) {
        LOOP_COUNTER_CHECK(14);
        v_pid = iter16->second();
        {
          if (LINE(420,x_is_numeric(v_pid))) {
            LINE(421,x_posix_kill(toInt32(v_pid), toInt32(9LL)));
          }
        }
      }
    }
  }
} /* function */
Object co_benchmark(CArrRef params, bool init /* = true */) {
  return Object(p_benchmark(NEW(c_benchmark)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$bench_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$bench_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bench __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bench") : g->GV(bench);

  LINE(28,x_error_reporting(toInt32(6143LL)));
  LINE(29,x_set_time_limit(toInt32(0LL)));
  (v_bench = ((Object)(LINE(430,p_benchmark(p_benchmark(NEWOBJ(c_benchmark)())->create())))));
  LINE(431,v_bench.o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
