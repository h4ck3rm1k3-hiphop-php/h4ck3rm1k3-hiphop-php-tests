
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_bench_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_bench_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.fw.h>

// Declarations
#include <cls/benchmark.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$bench_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_benchmark(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_bench_h__
