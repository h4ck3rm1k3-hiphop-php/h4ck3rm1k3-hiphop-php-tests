
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_getargs_fw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_getargs_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_CONSOLE_GETARGS_ERROR_CONFIG;
extern const StaticString k_PEAR_ERROR_RETURN;
extern const StaticString k_PEAR_ERROR_TRIGGER;
extern const int64 k_CONSOLE_GETARGS_HELP;
extern const StaticString k_CONSOLE_GETARGS_PARAMS;
extern const int64 k_CONSOLE_GETARGS_ERROR_USER;


// 2. Classes
FORWARD_DECLARE_CLASS(console_getargs_options)
FORWARD_DECLARE_CLASS(console_getargs)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_getargs_fw_h__
