
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_timer_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_timer_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.fw.h>

// Declarations
#include <cls/timer.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$timer_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_timer(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_benchmarks_benchcli_misc_timer_h__
