
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 6 */
Variant c_smapsparser::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_smapsparser::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_smapsparser::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("results", m_results));
  props.push_back(NEW(ArrayElement)("usage", m_usage.isReferenced() ? ref(m_usage) : m_usage));
  c_ObjectData::o_get(props);
}
bool c_smapsparser::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x66E32C011EFD3838LL, results, 7);
      break;
    case 3:
      HASH_EXISTS_STRING(0x544977EA718F0A5FLL, usage, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_smapsparser::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x66E32C011EFD3838LL, m_results,
                         results, 7);
      break;
    case 3:
      HASH_RETURN_STRING(0x544977EA718F0A5FLL, m_usage,
                         usage, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_smapsparser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x66E32C011EFD3838LL, m_results,
                      results, 7);
      break;
    case 3:
      HASH_SET_STRING(0x544977EA718F0A5FLL, m_usage,
                      usage, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_smapsparser::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x544977EA718F0A5FLL, m_usage,
                         usage, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_smapsparser::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(smapsparser)
ObjectData *c_smapsparser::create() {
  init();
  t_smapsparser();
  return this;
}
ObjectData *c_smapsparser::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_smapsparser::cloneImpl() {
  c_smapsparser *obj = NEW(c_smapsparser)();
  cloneSet(obj);
  return obj;
}
void c_smapsparser::cloneSet(c_smapsparser *clone) {
  clone->m_results = m_results;
  clone->m_usage = m_usage.isReferenced() ? ref(m_usage) : m_usage;
  ObjectData::cloneSet(clone);
}
Variant c_smapsparser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x64E62EC7666796E0LL, printmaxusage) {
        int count = params.size();
        if (count <= 1) return (t_printmaxusage(params.rvalAt(0)), null);
        return (t_printmaxusage(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x6300CB84E8E4F1E0LL, readsmapsdata) {
        return (t_readsmapsdata(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x31DA235C5A226667LL, clear) {
        return (t_clear(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x0FE6271E623C8A7BLL, parsesmapsdata) {
        return (t_parsesmapsdata(params.rvalAt(0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x42C15CD9103AA93FLL, cmpkey) {
        return (ti_cmpkey(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_smapsparser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x64E62EC7666796E0LL, printmaxusage) {
        if (count <= 1) return (t_printmaxusage(a0), null);
        return (t_printmaxusage(a0, a1), null);
      }
      HASH_GUARD(0x6300CB84E8E4F1E0LL, readsmapsdata) {
        return (t_readsmapsdata(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x31DA235C5A226667LL, clear) {
        return (t_clear(), null);
      }
      break;
    case 11:
      HASH_GUARD(0x0FE6271E623C8A7BLL, parsesmapsdata) {
        return (t_parsesmapsdata(a0), null);
      }
      break;
    case 15:
      HASH_GUARD(0x42C15CD9103AA93FLL, cmpkey) {
        return (ti_cmpkey(o_getClassName(), a0, a1));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_smapsparser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x42C15CD9103AA93FLL, cmpkey) {
        return (ti_cmpkey(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_smapsparser$os_get(const char *s) {
  return c_smapsparser::os_get(s, -1);
}
Variant &cw_smapsparser$os_lval(const char *s) {
  return c_smapsparser::os_lval(s, -1);
}
Variant cw_smapsparser$os_constant(const char *s) {
  return c_smapsparser::os_constant(s);
}
Variant cw_smapsparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_smapsparser::os_invoke(c, s, params, -1, fatal);
}
void c_smapsparser::init() {
  m_results = null;
  m_usage = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 10 */
void c_smapsparser::t_smapsparser() {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::Smapsparser);
  bool oldInCtor = gasInCtor(true);
  ;
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 14 */
void c_smapsparser::t_parsesmapsdata(CVarRef v_data) {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::parseSmapsData);
  Array v_lines;
  Variant v_last;
  Variant v_result;
  Variant v_line;
  Variant v_matches;
  Variant v_match_type;
  Variant v_match_amount;

  (v_lines = LINE(16,x_explode("\n", toString(v_data))));
  ;
  (v_last = ScalarArrays::sa_[0]);
  (v_result = ScalarArrays::sa_[0]);
  v_result.set("objects", (ScalarArrays::sa_[0]), 0x644D4592B7D66F10LL);
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_lines.begin("smapsparser"); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_line = iter3.second();
      {
        (v_matches = ScalarArrays::sa_[0]);
        if (toBoolean(LINE(23,x_preg_match("/[a-z0-9]+[-][a-z0-9]+/", toString(v_line))))) {
          if (toBoolean(LINE(24,x_preg_match("/[\\d]{2}:[\\d]{2}[\\s][\\d]+[\\s]+(\?<name>.*)/", toString(v_line), ref(v_matches))))) {
            if (!(empty(v_last))) lval(v_result.lvalAt("objects", 0x644D4592B7D66F10LL)).append((v_last));
            v_last.set("name", (v_matches.rvalAt("name", 0x0BCDB293DC3CBDDCLL)), 0x0BCDB293DC3CBDDCLL);
          }
        }
        else if (toBoolean(LINE(29,x_preg_match("/(\?<name>[a-zA-Z_]+):/", toString(v_line), ref(v_match_type))))) {
          LINE(30,x_preg_match("/(\?<amount>\\d+)[\\s]kB/", toString(v_line), ref(v_match_amount)));
          v_last.set(v_match_type.rvalAt("name", 0x0BCDB293DC3CBDDCLL), (v_match_amount.rvalAt("amount", 0x0D165F1EF0931EDCLL)));
          if (!(isset(v_result, v_match_type.rvalAt("name", 0x0BCDB293DC3CBDDCLL)))) {
            v_result.set(v_match_type.rvalAt("name", 0x0BCDB293DC3CBDDCLL), (0LL));
          }
          lval(v_result.lvalAt(v_match_type.rvalAt("name", 0x0BCDB293DC3CBDDCLL))) += v_match_amount.rvalAt("amount", 0x0D165F1EF0931EDCLL);
        }
      }
    }
  }
  lval(v_result.lvalAt("objects", 0x644D4592B7D66F10LL)).append((v_last));
  m_usage.append((v_result));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 41 */
Variant c_smapsparser::t_getusage() {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::getUsage);
  return m_usage;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 49 */
int64 c_smapsparser::ti_cmpkey(const char* cls, CVarRef v_a, CVarRef v_b) {
  STATIC_METHOD_INJECTION(Smapsparser, Smapsparser::cmpKey);
  if (equal(v_a.rvalAt("Private_Dirty", 0x0805187A692409E7LL), v_b.rvalAt("Private_Dirty", 0x0805187A692409E7LL))) {
    return 0LL;
  }
  return (less(v_a.rvalAt("Private_Dirty", 0x0805187A692409E7LL), v_b.rvalAt("Private_Dirty", 0x0805187A692409E7LL))) ? ((1LL)) : ((-1LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 61 */
int64 c_smapsparser::t_getpeak() {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::getPeak);
  int64 v_maxi = 0;
  Variant v_maxval;
  int64 v_i = 0;
  Variant v_mem_array;

  (v_maxi = 0LL);
  (v_maxval = 0LL);
  (v_i = 0LL);
  {
    LOOP_COUNTER(4);
    Variant map5 = m_usage;
    for (ArrayIterPtr iter6 = map5.begin("smapsparser"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_mem_array = iter6->second();
      {
        if (less(v_maxval, v_mem_array.rvalAt("Size", 0x2F871A9A118ED87FLL))) {
          (v_maxi = v_i);
          (v_maxval = v_mem_array.rvalAt("Size", 0x2F871A9A118ED87FLL));
        }
        v_i++;
      }
    }
  }
  return v_maxi;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 80 */
void c_smapsparser::t_clear() {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::clear);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_results;
  Variant v_usage;

  unset(v_results);
  unset(v_usage);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 95 */
void c_smapsparser::t_printmaxusage(CVarRef v_n, CVarRef v_resource //  = "php://stdout"
) {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::printMaxUsage);
  int64 v_i = 0;
  Variant v_fh;
  Variant v_peak;
  Variant v_key;

  (v_i = 0LL);
  if (equal(v_resource, "php://stdout")) {
    (v_fh = LINE(100,x_fopen(toString(v_resource), "w")));
  }
  else {
    (v_fh = LINE(102,x_fopen(toString(v_resource), "a+")));
  }
  (v_peak = m_usage.rvalAt(LINE(104,t_getpeak())));
  LINE(105,t_sortmemusage(ref(lval(v_peak.lvalAt("objects", 0x644D4592B7D66F10LL)))));
  LINE(106,x_fprintf(3, toObject(v_fh), "VM Size total: %10skB\n", Array(ArrayInit(1).set(0, v_peak.rvalAt("Size", 0x2F871A9A118ED87FLL)).create())));
  LINE(107,x_fprintf(3, toObject(v_fh), "RSS          : %10skB Total\n", Array(ArrayInit(1).set(0, v_peak.rvalAt("Rss", 0x4D4A63371FC55FF0LL)).create())));
  LINE(108,x_fprintf(3, toObject(v_fh), "             : %10skB Shared total\n", Array(ArrayInit(1).set(0, v_peak.rvalAt("Shared_Dirty", 0x38778CFF32AC2045LL) + v_peak.rvalAt("Shared_Clean", 0x7A9B46F37648B0B9LL)).create())));
  LINE(109,x_fprintf(3, toObject(v_fh), "               %10skB Private Clean\n", Array(ArrayInit(1).set(0, v_peak.rvalAt("Private_Clean", 0x344A18693B90F8C1LL)).create())));
  LINE(110,x_fprintf(3, toObject(v_fh), "               %10skB Private Dirty\n", Array(ArrayInit(1).set(0, v_peak.rvalAt("Private_Dirty", 0x0805187A692409E7LL)).create())));
  LINE(111,x_fprintf(6, toObject(v_fh), "%10s %10s %10s %-15s\n", ScalarArrays::sa_[1]));
  LINE(112,x_fprintf(3, toObject(v_fh), "%'-59s\n", ScalarArrays::sa_[2]));
  {
    LOOP_COUNTER(7);
    Variant map8 = v_peak.rvalAt("objects", 0x644D4592B7D66F10LL);
    for (ArrayIterPtr iter9 = map8.begin("smapsparser"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_key = iter9->second();
      {
        v_i++;
        if (more(v_i, v_n)) {
          break;
        }
        if (!equal(v_key.rvalAt("name", 0x0BCDB293DC3CBDDCLL), "")) {
          LINE(120,x_fprintf(6, toObject(v_fh), "%8skB %8skB %8skB %-18s\n", Array(ArrayInit(4).set(0, v_key.rvalAt("Size", 0x2F871A9A118ED87FLL)).set(1, v_key.rvalAt("Private_Clean", 0x344A18693B90F8C1LL)).set(2, v_key.rvalAt("Private_Dirty", 0x0805187A692409E7LL)).set(3, v_key.rvalAt("name", 0x0BCDB293DC3CBDDCLL)).create())));
        }
        else {
          v_i--;
        }
      }
    }
  }
  LINE(125,x_fprintf(3, toObject(v_fh), "%s", ScalarArrays::sa_[3]));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 135 */
void c_smapsparser::t_sortmemusage(Variant v_array) {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::sortMemUsage);
  LINE(137,x_usort(ref(v_array), "Smapsparser::cmpKey"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 147 */
Variant c_smapsparser::t_readsmapsdata(CVarRef v_pid) {
  INSTANCE_METHOD_INJECTION(Smapsparser, Smapsparser::readSmapsData);
  if (LINE(149,x_file_exists(concat3("/proc/", toString(v_pid), "/smaps")))) {
    return LINE(150,x_file_get_contents(concat3("/proc/", toString(v_pid), "/smaps")));
  }
  return false;
} /* function */
Object co_smapsparser(CArrRef params, bool init /* = true */) {
  return Object(p_smapsparser(NEW(c_smapsparser)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$smapsparser_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$smapsparser_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
