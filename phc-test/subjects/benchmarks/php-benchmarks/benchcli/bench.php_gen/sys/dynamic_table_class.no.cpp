
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_cachegrindparser(CArrRef params, bool init = true);
Variant cw_cachegrindparser$os_get(const char *s);
Variant &cw_cachegrindparser$os_lval(const char *s);
Variant cw_cachegrindparser$os_constant(const char *s);
Variant cw_cachegrindparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_console_getargs_options(CArrRef params, bool init = true);
Variant cw_console_getargs_options$os_get(const char *s);
Variant &cw_console_getargs_options$os_lval(const char *s);
Variant cw_console_getargs_options$os_constant(const char *s);
Variant cw_console_getargs_options$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_console_getargs(CArrRef params, bool init = true);
Variant cw_console_getargs$os_get(const char *s);
Variant &cw_console_getargs$os_lval(const char *s);
Variant cw_console_getargs$os_constant(const char *s);
Variant cw_console_getargs$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_benchmark(CArrRef params, bool init = true);
Variant cw_benchmark$os_get(const char *s);
Variant &cw_benchmark$os_lval(const char *s);
Variant cw_benchmark$os_constant(const char *s);
Variant cw_benchmark$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_timer(CArrRef params, bool init = true);
Variant cw_timer$os_get(const char *s);
Variant &cw_timer$os_lval(const char *s);
Variant cw_timer$os_constant(const char *s);
Variant cw_timer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_smapsparser(CArrRef params, bool init = true);
Variant cw_smapsparser$os_get(const char *s);
Variant &cw_smapsparser$os_lval(const char *s);
Variant cw_smapsparser$os_constant(const char *s);
Variant cw_smapsparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_CREATE_OBJECT(0x1B51106C4FE6E860LL, cachegrindparser);
      break;
    case 1:
      HASH_CREATE_OBJECT(0x4897D64A07B24A81LL, console_getargs);
      HASH_CREATE_OBJECT(0x75CD85D76D78F381LL, smapsparser);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x05E9FB2EF0597C17LL, console_getargs_options);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x64A72202171F4BEALL, benchmark);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x6AB6B4B66BBED48FLL, timer);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x1B51106C4FE6E860LL, cachegrindparser);
      break;
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x4897D64A07B24A81LL, console_getargs);
      HASH_INVOKE_STATIC_METHOD(0x75CD85D76D78F381LL, smapsparser);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x05E9FB2EF0597C17LL, console_getargs_options);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x64A72202171F4BEALL, benchmark);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x6AB6B4B66BBED48FLL, timer);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x1B51106C4FE6E860LL, cachegrindparser);
      break;
    case 1:
      HASH_GET_STATIC_PROPERTY(0x4897D64A07B24A81LL, console_getargs);
      HASH_GET_STATIC_PROPERTY(0x75CD85D76D78F381LL, smapsparser);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x05E9FB2EF0597C17LL, console_getargs_options);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x64A72202171F4BEALL, benchmark);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x6AB6B4B66BBED48FLL, timer);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x1B51106C4FE6E860LL, cachegrindparser);
      break;
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x4897D64A07B24A81LL, console_getargs);
      HASH_GET_STATIC_PROPERTY_LV(0x75CD85D76D78F381LL, smapsparser);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x05E9FB2EF0597C17LL, console_getargs_options);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x64A72202171F4BEALL, benchmark);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x6AB6B4B66BBED48FLL, timer);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x1B51106C4FE6E860LL, cachegrindparser);
      break;
    case 1:
      HASH_GET_CLASS_CONSTANT(0x4897D64A07B24A81LL, console_getargs);
      HASH_GET_CLASS_CONSTANT(0x75CD85D76D78F381LL, smapsparser);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x05E9FB2EF0597C17LL, console_getargs_options);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x64A72202171F4BEALL, benchmark);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x6AB6B4B66BBED48FLL, timer);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
