
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$bench_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$getargs_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$timer_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$smapsparser_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$cachegrindparser_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_INCLUDE(0x7966DC88C41EB5C2LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$cachegrindparser_php);
      break;
    case 6:
      HASH_INCLUDE(0x1F9ABD7883D900B6LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$bench_php);
      break;
    case 8:
      HASH_INCLUDE(0x31B360D93165B758LL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$smapsparser_php);
      break;
    case 13:
      HASH_INCLUDE(0x70E1C9D663C5644DLL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$getargs_php);
      break;
    case 14:
      HASH_INCLUDE(0x49F54F3E375F2FDELL, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php", php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$timer_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
