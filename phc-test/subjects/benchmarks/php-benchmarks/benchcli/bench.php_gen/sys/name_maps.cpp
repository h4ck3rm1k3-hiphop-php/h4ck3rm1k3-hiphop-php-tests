
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "benchmark", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php",
  "cachegrindparser", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.php",
  "console_getargs", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php",
  "console_getargs_options", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php",
  "smapsparser", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php",
  "timer", "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
