
#ifndef __GENERATED_cls_cachegrindparser_h__
#define __GENERATED_cls_cachegrindparser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.php line 2 */
class c_cachegrindparser : virtual public ObjectData {
  BEGIN_CLASS_MAP(cachegrindparser)
  END_CLASS_MAP(cachegrindparser)
  DECLARE_CLASS(cachegrindparser, Cachegrindparser, ObjectData)
  void init();
  public: Variant t_parse(CVarRef v_text);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cachegrindparser_h__
