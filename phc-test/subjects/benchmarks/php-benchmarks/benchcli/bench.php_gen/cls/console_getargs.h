
#ifndef __GENERATED_cls_console_getargs_h__
#define __GENERATED_cls_console_getargs_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 110 */
class c_console_getargs : virtual public ObjectData {
  BEGIN_CLASS_MAP(console_getargs)
  END_CLASS_MAP(console_getargs)
  DECLARE_CLASS(console_getargs, Console_Getargs, ObjectData)
  void init();
  public: static Variant ti_factory(const char* cls, Variant v_config = ScalarArrays::sa_[0], Variant v_arguments = null);
  public: static String ti_gethelp(const char* cls, Variant v_config, Variant v_helpHeader = null, CVarRef v_helpFooter = "", int64 v_maxlength = 78LL, int64 v_indent = 0LL);
  public: static Array ti_getoptionalrequired(const char* cls, Variant v_config);
  public: static Variant t_factory(CVarRef v_config = ScalarArrays::sa_[0], CVarRef v_arguments = null_variant) { return ti_factory("console_getargs", v_config, v_arguments); }
  public: static Array t_getoptionalrequired(CVarRef v_config) { return ti_getoptionalrequired("console_getargs", v_config); }
  public: static String t_gethelp(CVarRef v_config, CVarRef v_helpHeader = null_variant, CVarRef v_helpFooter = "", int64 v_maxlength = 78LL, int64 v_indent = 0LL) { return ti_gethelp("console_getargs", v_config, v_helpHeader, v_helpFooter, v_maxlength, v_indent); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_console_getargs_h__
