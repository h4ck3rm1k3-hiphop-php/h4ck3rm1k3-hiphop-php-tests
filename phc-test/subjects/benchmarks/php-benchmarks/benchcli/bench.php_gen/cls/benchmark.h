
#ifndef __GENERATED_cls_benchmark_h__
#define __GENERATED_cls_benchmark_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/bench.php line 33 */
class c_benchmark : virtual public ObjectData {
  BEGIN_CLASS_MAP(benchmark)
  END_CLASS_MAP(benchmark)
  DECLARE_CLASS(benchmark, Benchmark, ObjectData)
  void init();
  public: Variant m_memory_limit;
  public: Variant m_debug;
  public: Variant m_include;
  public: Variant m_php;
  public: Variant m_config;
  public: Variant m_log_file;
  public: Variant m_quite;
  public: Variant m_test_files;
  public: Variant m_cachegrind;
  public: Variant m_smapsparser;
  public: Variant m_mem_usage;
  public: void t_benchmark();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_console(CVarRef v_msg, CVarRef v_print = true);
  public: void t_setconfig();
  public: void t_printhelp(CVarRef v_args);
  public: void t_settestfiles();
  public: void t_run();
  public: Array t_completeexec(CStrRef v_command, CVarRef v_stdin = null_variant, int64 v_timeout = 20LL);
  public: Variant t_getchildren(Variant v_handle, Variant v_pipes);
  public: void t_killproperly(Variant v_handle, Variant v_pipes);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_benchmark_h__
