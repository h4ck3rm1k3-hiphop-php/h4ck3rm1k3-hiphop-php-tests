
#ifndef __GENERATED_cls_console_getargs_options_h__
#define __GENERATED_cls_console_getargs_options_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 527 */
class c_console_getargs_options : virtual public ObjectData {
  BEGIN_CLASS_MAP(console_getargs_options)
  END_CLASS_MAP(console_getargs_options)
  DECLARE_CLASS(console_getargs_options, Console_Getargs_Options, ObjectData)
  void init();
  public: Array m__shortLong;
  public: Array m__aliasLong;
  public: Array m__longLong;
  public: Variant m__config;
  public: Variant m_args;
  public: Variant t_init(CVarRef v_config, CVarRef v_arguments = null_variant);
  public: Variant t_buildmaps();
  public: Variant t_parseargs();
  public: Variant t_parsearg(Variant v_arg, bool v_isLong, Variant v_pos);
  public: Variant t_setvalue(CVarRef v_optname, CVarRef v_value, Variant v_pos);
  public: bool t_isvalue(CVarRef v_arg);
  public: void t_updatevalue(CVarRef v_optname, CVarRef v_value);
  public: bool t_setdefaults();
  public: bool t_isdefined(CVarRef v_optname);
  public: Variant t_getlongname(CVarRef v_optname);
  public: Variant t_getvalue(CVarRef v_optname);
  public: Variant t_getvalues(CStrRef v_optionNames = "long");
  public: Variant t_checkrequired();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_console_getargs_options_h__
