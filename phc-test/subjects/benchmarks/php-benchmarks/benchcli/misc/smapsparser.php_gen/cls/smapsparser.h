
#ifndef __GENERATED_cls_smapsparser_h__
#define __GENERATED_cls_smapsparser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser.php line 6 */
class c_smapsparser : virtual public ObjectData {
  BEGIN_CLASS_MAP(smapsparser)
  END_CLASS_MAP(smapsparser)
  DECLARE_CLASS(smapsparser, Smapsparser, ObjectData)
  void init();
  public: String m_results;
  public: Variant m_usage;
  public: void t_smapsparser();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_parsesmapsdata(CVarRef v_data);
  public: Variant t_getusage();
  public: static int64 ti_cmpkey(const char* cls, CVarRef v_a, CVarRef v_b);
  public: int64 t_getpeak();
  public: void t_clear();
  public: void t_printmaxusage(CVarRef v_n, CStrRef v_resource = "php://stdout");
  public: void t_sortmemusage(Variant v_array);
  public: Variant t_readsmapsdata(CVarRef v_pid);
  public: static int64 t_cmpkey(CVarRef v_a, CVarRef v_b) { return ti_cmpkey("smapsparser", v_a, v_b); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_smapsparser_h__
