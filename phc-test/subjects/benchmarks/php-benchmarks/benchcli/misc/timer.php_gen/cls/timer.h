
#ifndef __GENERATED_cls_timer_h__
#define __GENERATED_cls_timer_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php line 5 */
class c_timer : virtual public ObjectData {
  BEGIN_CLASS_MAP(timer)
  END_CLASS_MAP(timer)
  DECLARE_CLASS(timer, Timer, ObjectData)
  void init();
  public: Variant m_starttime;
  public: Variant m_elapsed;
  public: void t_timer();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_start();
  public: void t_stop();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_timer_h__
