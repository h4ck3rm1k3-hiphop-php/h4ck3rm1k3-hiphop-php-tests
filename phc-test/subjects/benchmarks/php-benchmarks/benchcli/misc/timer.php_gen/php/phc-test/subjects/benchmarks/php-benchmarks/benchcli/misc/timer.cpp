
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php line 5 */
Variant c_timer::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_timer::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_timer::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("starttime", m_starttime.isReferenced() ? ref(m_starttime) : m_starttime));
  props.push_back(NEW(ArrayElement)("elapsed", m_elapsed.isReferenced() ? ref(m_elapsed) : m_elapsed));
  c_ObjectData::o_get(props);
}
bool c_timer::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x5308C39E159142E1LL, starttime, 9);
      HASH_EXISTS_STRING(0x6F219E8A99EA4EF1LL, elapsed, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_timer::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x5308C39E159142E1LL, m_starttime,
                         starttime, 9);
      HASH_RETURN_STRING(0x6F219E8A99EA4EF1LL, m_elapsed,
                         elapsed, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_timer::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x5308C39E159142E1LL, m_starttime,
                      starttime, 9);
      HASH_SET_STRING(0x6F219E8A99EA4EF1LL, m_elapsed,
                      elapsed, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_timer::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x5308C39E159142E1LL, m_starttime,
                         starttime, 9);
      HASH_RETURN_STRING(0x6F219E8A99EA4EF1LL, m_elapsed,
                         elapsed, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_timer::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(timer)
ObjectData *c_timer::create() {
  init();
  t_timer();
  return this;
}
ObjectData *c_timer::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_timer::cloneImpl() {
  c_timer *obj = NEW(c_timer)();
  cloneSet(obj);
  return obj;
}
void c_timer::cloneSet(c_timer *clone) {
  clone->m_starttime = m_starttime.isReferenced() ? ref(m_starttime) : m_starttime;
  clone->m_elapsed = m_elapsed.isReferenced() ? ref(m_elapsed) : m_elapsed;
  ObjectData::cloneSet(clone);
}
Variant c_timer::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_timer::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_timer::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_timer$os_get(const char *s) {
  return c_timer::os_get(s, -1);
}
Variant &cw_timer$os_lval(const char *s) {
  return c_timer::os_lval(s, -1);
}
Variant cw_timer$os_constant(const char *s) {
  return c_timer::os_constant(s);
}
Variant cw_timer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_timer::os_invoke(c, s, params, -1, fatal);
}
void c_timer::init() {
  m_starttime = null;
  m_elapsed = null;
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php line 13 */
void c_timer::t_timer() {
  INSTANCE_METHOD_INJECTION(Timer, Timer::Timer);
  bool oldInCtor = gasInCtor(true);
  (m_starttime = 0LL);
  (m_elapsed = 0LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php line 23 */
void c_timer::t_start() {
  INSTANCE_METHOD_INJECTION(Timer, Timer::start);
  (m_starttime = LINE(25,x_microtime(true)));
  (m_elapsed = 0LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php line 33 */
void c_timer::t_stop() {
  INSTANCE_METHOD_INJECTION(Timer, Timer::stop);
  (m_elapsed = LINE(35,x_microtime(true)) - m_starttime);
  (m_starttime = 0LL);
} /* function */
Object co_timer(CArrRef params, bool init /* = true */) {
  return Object(p_timer(NEW(c_timer)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$timer_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/timer.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$timer_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
