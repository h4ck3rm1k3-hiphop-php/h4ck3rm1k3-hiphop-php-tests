
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.php line 2 */
Variant c_cachegrindparser::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_cachegrindparser::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_cachegrindparser::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_cachegrindparser::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_cachegrindparser::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_cachegrindparser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_cachegrindparser::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_cachegrindparser::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(cachegrindparser)
ObjectData *c_cachegrindparser::cloneImpl() {
  c_cachegrindparser *obj = NEW(c_cachegrindparser)();
  cloneSet(obj);
  return obj;
}
void c_cachegrindparser::cloneSet(c_cachegrindparser *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_cachegrindparser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_cachegrindparser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_cachegrindparser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_cachegrindparser$os_get(const char *s) {
  return c_cachegrindparser::os_get(s, -1);
}
Variant &cw_cachegrindparser$os_lval(const char *s) {
  return c_cachegrindparser::os_lval(s, -1);
}
Variant cw_cachegrindparser$os_constant(const char *s) {
  return c_cachegrindparser::os_constant(s);
}
Variant cw_cachegrindparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_cachegrindparser::os_invoke(c, s, params, -1, fatal);
}
void c_cachegrindparser::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.php line 11 */
Variant c_cachegrindparser::t_parse(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Cachegrindparser, Cachegrindparser::parse);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  DECLARE_GLOBAL_VARIABLES(g);
  Array v_match_names;
  String v_start;
  String v_middle;
  String v_number;
  String v_rw;
  String v_branch_rw;
  String v_end;
  Variant v_matches;
  Variant v_match;
  Variant v_results;

  (v_match_names = ScalarArrays::sa_[0]);
  (v_start = "^==\\d+==");
  (v_middle = ":\\s+");
  (v_number = "([0-9,]+)");
  (v_rw = LINE(48,concat5("\\s*\\(\\s*", v_number, " rd\\s*\\+\\s*", v_number, " wr\\)")));
  (v_branch_rw = LINE(49,concat5("\\s*\\(\\s*", v_number, " cond\\s*\\+\\s*", v_number, " ind\\)")));
  (v_end = "$.");
  LINE(65,(assignCallTemp(eo_0, concat_rev((assignCallTemp(eo_3, LINE(60,concat6(v_start, " L2\\s+refs", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_4, LINE(61,concat6(v_start, " L2\\s+misses", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_6, LINE(63,concat6(v_start, " Branches", v_middle, v_number, v_branch_rw, v_end))),assignCallTemp(eo_7, LINE(64,concat6(v_start, " Mispredicts", v_middle, v_number, v_branch_rw, v_end))),concat6(eo_3, eo_4, ".*", eo_6, eo_7, "/ms")), concat_rev(LINE(65,(assignCallTemp(eo_3, LINE(54,concat5(v_start, " L2i\\s+misses", v_middle, v_number, v_end))),assignCallTemp(eo_5, LINE(56,concat6(v_start, " D\\s+refs", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_6, LINE(57,concat6(v_start, " D1\\s+misses", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_7, LINE(58,concat6(v_start, " L2d\\s+misses", v_middle, v_number, v_rw, v_end))),concat6(eo_3, ".*", eo_5, eo_6, eo_7, ".*"))), LINE(65,(assignCallTemp(eo_4, LINE(52,concat5(v_start, " I\\s+refs", v_middle, v_number, v_end))),assignCallTemp(eo_5, LINE(53,concat5(v_start, " I1\\s+misses", v_middle, v_number, v_end))),concat3("/", eo_4, eo_5)))))),assignCallTemp(eo_1, toString(v_text)),assignCallTemp(eo_2, ref(v_matches)),x_preg_match(eo_0, eo_1, eo_2)));
  LINE(67,x_array_shift(ref(v_matches)));
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(v_matches);
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(NULL, v_match); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_match = LINE(71,x_preg_replace("/,/", "", v_match)));
      }
    }
  }
  unset(v_match);
  (v_results = LINE(76,x_array_combine(v_match_names, v_matches)));
  return v_results;
} /* function */
Object co_cachegrindparser(CArrRef params, bool init /* = true */) {
  return Object(p_cachegrindparser(NEW(c_cachegrindparser)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$cachegrindparser_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$cachegrindparser_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
