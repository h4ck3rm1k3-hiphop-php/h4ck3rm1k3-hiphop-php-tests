
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_CONSOLE_GETARGS_ERROR_CONFIG;
extern const StaticString k_PEAR_ERROR_RETURN;
extern const StaticString k_PEAR_ERROR_TRIGGER;
extern const int64 k_CONSOLE_GETARGS_HELP;
extern const StaticString k_CONSOLE_GETARGS_PARAMS;
extern const int64 k_CONSOLE_GETARGS_ERROR_USER;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 15) {
    case 0:
      HASH_RETURN(0x012B1C5193589B90LL, k_PEAR_ERROR_TRIGGER, PEAR_ERROR_TRIGGER);
      break;
    case 3:
      HASH_RETURN(0x2D704F71EAF769A3LL, k_PEAR_ERROR_RETURN, PEAR_ERROR_RETURN);
      break;
    case 6:
      HASH_RETURN(0x1B17C1DEAE258166LL, k_CONSOLE_GETARGS_PARAMS, CONSOLE_GETARGS_PARAMS);
      break;
    case 9:
      HASH_RETURN(0x5F53821A0F8325A9LL, k_CONSOLE_GETARGS_ERROR_USER, CONSOLE_GETARGS_ERROR_USER);
      break;
    case 14:
      HASH_RETURN(0x5F9BA3CB7FF0CD6ELL, k_CONSOLE_GETARGS_ERROR_CONFIG, CONSOLE_GETARGS_ERROR_CONFIG);
      break;
    case 15:
      HASH_RETURN(0x5B7E8EE7947C18EFLL, k_CONSOLE_GETARGS_HELP, CONSOLE_GETARGS_HELP);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
