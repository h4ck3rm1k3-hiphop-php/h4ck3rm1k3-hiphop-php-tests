
#include <php/phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_CONSOLE_GETARGS_ERROR_CONFIG = -1LL;
const StaticString k_PEAR_ERROR_RETURN = "PEAR_ERROR_RETURN";
const StaticString k_PEAR_ERROR_TRIGGER = "PEAR_ERROR_TRIGGER";
const int64 k_CONSOLE_GETARGS_HELP = -3LL;
const StaticString k_CONSOLE_GETARGS_PARAMS = "parameters";
const int64 k_CONSOLE_GETARGS_ERROR_USER = -2LL;

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 527 */
Variant c_console_getargs_options::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_console_getargs_options::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_console_getargs_options::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_shortLong", m__shortLong));
  props.push_back(NEW(ArrayElement)("_aliasLong", m__aliasLong));
  props.push_back(NEW(ArrayElement)("_longLong", m__longLong));
  props.push_back(NEW(ArrayElement)("_config", m__config.isReferenced() ? ref(m__config) : m__config));
  props.push_back(NEW(ArrayElement)("args", m_args.isReferenced() ? ref(m_args) : m_args));
  c_ObjectData::o_get(props);
}
bool c_console_getargs_options::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_EXISTS_STRING(0x35E32838BA2711D1LL, _longLong, 9);
      break;
    case 2:
      HASH_EXISTS_STRING(0x76BE2C2715EF0432LL, _shortLong, 10);
      break;
    case 8:
      HASH_EXISTS_STRING(0x5BCE2D8D44324498LL, _aliasLong, 10);
      break;
    case 9:
      HASH_EXISTS_STRING(0x519E19D2E403F0A9LL, _config, 7);
      break;
    case 14:
      HASH_EXISTS_STRING(0x4AF7CD17F976719ELL, args, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_console_getargs_options::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x35E32838BA2711D1LL, m__longLong,
                         _longLong, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x76BE2C2715EF0432LL, m__shortLong,
                         _shortLong, 10);
      break;
    case 8:
      HASH_RETURN_STRING(0x5BCE2D8D44324498LL, m__aliasLong,
                         _aliasLong, 10);
      break;
    case 9:
      HASH_RETURN_STRING(0x519E19D2E403F0A9LL, m__config,
                         _config, 7);
      break;
    case 14:
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_console_getargs_options::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_SET_STRING(0x35E32838BA2711D1LL, m__longLong,
                      _longLong, 9);
      break;
    case 2:
      HASH_SET_STRING(0x76BE2C2715EF0432LL, m__shortLong,
                      _shortLong, 10);
      break;
    case 8:
      HASH_SET_STRING(0x5BCE2D8D44324498LL, m__aliasLong,
                      _aliasLong, 10);
      break;
    case 9:
      HASH_SET_STRING(0x519E19D2E403F0A9LL, m__config,
                      _config, 7);
      break;
    case 14:
      HASH_SET_STRING(0x4AF7CD17F976719ELL, m_args,
                      args, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_console_getargs_options::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x519E19D2E403F0A9LL, m__config,
                         _config, 7);
      break;
    case 2:
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_console_getargs_options::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(console_getargs_options)
ObjectData *c_console_getargs_options::cloneImpl() {
  c_console_getargs_options *obj = NEW(c_console_getargs_options)();
  cloneSet(obj);
  return obj;
}
void c_console_getargs_options::cloneSet(c_console_getargs_options *clone) {
  clone->m__shortLong = m__shortLong;
  clone->m__aliasLong = m__aliasLong;
  clone->m__longLong = m__longLong;
  clone->m__config = m__config.isReferenced() ? ref(m__config) : m__config;
  clone->m_args = m_args.isReferenced() ? ref(m_args) : m_args;
  ObjectData::cloneSet(clone);
}
Variant c_console_getargs_options::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x614F97E16B435A03LL, init) {
        int count = params.size();
        if (count <= 1) return (t_init(params.rvalAt(0)));
        return (t_init(params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x75125E0AC0B091B3LL, checkrequired) {
        return (t_checkrequired());
      }
      break;
    case 6:
      HASH_GUARD(0x40304451B3D144B6LL, setdefaults) {
        return (t_setdefaults());
      }
      break;
    case 7:
      HASH_GUARD(0x043AFDF53A44A287LL, buildmaps) {
        return (t_buildmaps());
      }
      break;
    case 14:
      HASH_GUARD(0x03F04809A4B0F5FELL, parseargs) {
        return (t_parseargs());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_console_getargs_options::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x614F97E16B435A03LL, init) {
        if (count <= 1) return (t_init(a0));
        return (t_init(a0, a1));
      }
      HASH_GUARD(0x75125E0AC0B091B3LL, checkrequired) {
        return (t_checkrequired());
      }
      break;
    case 6:
      HASH_GUARD(0x40304451B3D144B6LL, setdefaults) {
        return (t_setdefaults());
      }
      break;
    case 7:
      HASH_GUARD(0x043AFDF53A44A287LL, buildmaps) {
        return (t_buildmaps());
      }
      break;
    case 14:
      HASH_GUARD(0x03F04809A4B0F5FELL, parseargs) {
        return (t_parseargs());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_console_getargs_options::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_console_getargs_options$os_get(const char *s) {
  return c_console_getargs_options::os_get(s, -1);
}
Variant &cw_console_getargs_options$os_lval(const char *s) {
  return c_console_getargs_options::os_lval(s, -1);
}
Variant cw_console_getargs_options$os_constant(const char *s) {
  return c_console_getargs_options::os_constant(s);
}
Variant cw_console_getargs_options$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_console_getargs_options::os_invoke(c, s, params, -1, fatal);
}
void c_console_getargs_options::init() {
  m__shortLong = ScalarArrays::sa_[0];
  m__aliasLong = ScalarArrays::sa_[0];
  m__longLong = ScalarArrays::sa_[0];
  m__config = ScalarArrays::sa_[0];
  m_args = ScalarArrays::sa_[0];
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 572 */
Variant c_console_getargs_options::t_init(CVarRef v_config, CVarRef v_arguments //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::init);
  DECLARE_GLOBAL_VARIABLES(g);
  if (LINE(574,x_is_array(v_arguments))) {
    (m_args = v_arguments);
  }
  else {
    if (!(isset(g->gv__SERVER, "argv", 0x10EA7DC57768F8C6LL)) || !(LINE(579,x_is_array(g->gv__SERVER.rvalAt("argv", 0x10EA7DC57768F8C6LL))))) {
      return LINE(581,throw_fatal("unknown class pear", ((void*)NULL)));
    }
    (m_args = g->gv__SERVER.rvalAt("argv", 0x10EA7DC57768F8C6LL));
  }
  if (isset(m_args.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0LL, 0x77CFA1EEF01BCA90LL) && !equal(m_args.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-")) {
    LINE(588,x_array_shift(ref(lval(m_args))));
  }
  (m__config = v_config);
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 600 */
Variant c_console_getargs_options::t_buildmaps() {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::buildMaps);
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_long = 0;
  Variant v_def;
  Variant v_longArr;
  Variant v_longname;
  Variant v_alias;
  Variant v_shortArr;
  Variant v_short;

  {
    LOOP_COUNTER(1);
    Variant map2 = m__config;
    for (ArrayIterPtr iter3 = map2.begin("console_getargs_options"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_def = iter3->second();
      v_long = iter3->first();
      {
        (v_longArr = LINE(604,x_explode("|", toString(v_long))));
        (v_longname = v_longArr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
        if (more(LINE(607,x_count(v_longArr)), 1LL)) {
          LINE(610,x_array_shift(ref(v_longArr)));
          {
            LOOP_COUNTER(4);
            for (ArrayIterPtr iter6 = v_longArr.begin("console_getargs_options"); !iter6->end(); iter6->next()) {
              LOOP_COUNTER_CHECK(4);
              v_alias = iter6->second();
              {
                if (isset(m__aliasLong, v_alias)) {
                  return LINE(615,throw_fatal("unknown class pear", ((void*)NULL)));
                }
                m__aliasLong.set(v_alias, (v_longname));
              }
            }
          }
          m__config.set(v_longname, (v_def));
          m__config.weakRemove(v_long);
        }
        if (!(empty(v_def, "short", 0x377E75AD6C01626DLL))) {
          (v_shortArr = LINE(629,x_explode("|", toString(v_def.rvalAt("short", 0x377E75AD6C01626DLL)))));
          (v_short = v_shortArr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          if (more(LINE(631,x_count(v_shortArr)), 1LL)) {
            LINE(634,x_array_shift(ref(v_shortArr)));
            {
              LOOP_COUNTER(7);
              for (ArrayIterPtr iter9 = v_shortArr.begin("console_getargs_options"); !iter9->end(); iter9->next()) {
                LOOP_COUNTER_CHECK(7);
                v_alias = iter9->second();
                {
                  if (isset(m__shortLong, v_alias)) {
                    return LINE(639,throw_fatal("unknown class pear", ((void*)NULL)));
                  }
                  m__shortLong.set(v_alias, (v_longname));
                }
              }
            }
          }
          m__shortLong.set(v_short, (v_longname));
        }
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 658 */
Variant c_console_getargs_options::t_parseargs() {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::parseArgs);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_i;
  int v_count = 0;
  Variant v_arg;
  Variant v_err;
  Variant v_tempI;

  {
    LOOP_COUNTER(10);
    for ((v_i = 0LL), (v_count = LINE(661,x_count(m_args))); less(v_i, v_count); v_i++) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_arg = m_args.rvalAt(v_i));
        if (same(v_arg, "--help") || same(v_arg, "-h")) {
          return LINE(665,throw_fatal("unknown class pear", ((void*)NULL)));
        }
        if (same(v_arg, "--")) {
          (v_err = (assignCallTemp(eo_2, ++v_i),LINE(670,t_parsearg("parameters" /* CONSOLE_GETARGS_PARAMS */, true, eo_2))));
        }
        else if (more(LINE(671,x_strlen(toString(v_arg))), 1LL) && equal(v_arg.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-") && equal(v_arg.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "-")) {
          (v_err = (assignCallTemp(eo_0, LINE(673,x_substr(toString(v_arg), toInt32(2LL)))),assignCallTemp(eo_2, ref(v_i)),t_parsearg(eo_0, true, eo_2)));
        }
        else if (more(LINE(674,x_strlen(toString(v_arg))), 1LL) && equal(v_arg.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-")) {
          (v_err = (assignCallTemp(eo_0, LINE(676,x_substr(toString(v_arg), toInt32(1LL)))),assignCallTemp(eo_2, ref(v_i)),t_parsearg(eo_0, false, eo_2)));
          if (same(v_err, -1LL)) {
            break;
          }
        }
        else if (isset(m__config, "parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL)) {
          (v_tempI = ref(v_i)) - 1LL;
          (v_err = LINE(683,t_parsearg("parameters" /* CONSOLE_GETARGS_PARAMS */, true, ref(v_tempI))));
        }
        else {
          (v_err = LINE(687,throw_fatal("unknown class pear", ((void*)NULL))));
        }
        if (!same(v_err, true)) {
          return v_err;
        }
      }
    }
  }
  if (isset(v_err) && same(v_err, -1LL)) {
    return LINE(696,t_parseargs());
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 708 */
Variant c_console_getargs_options::t_parsearg(Variant v_arg, bool v_isLong, Variant v_pos) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::parseArg);
  Variant eo_0;
  Variant eo_1;
  Array v_newArgs;
  int64 v_i = 0;
  String v_opt;
  String v_cmp;
  Variant v_long;

  if (!(v_isLong) && !(isset(m__shortLong, v_arg)) && more(LINE(712,x_strlen(toString(v_arg))), 1LL)) {
    (v_newArgs = ScalarArrays::sa_[0]);
    {
      LOOP_COUNTER(11);
      for ((v_i = 0LL); less(v_i, LINE(714,x_strlen(toString(v_arg)))); v_i++) {
        LOOP_COUNTER_CHECK(11);
        {
          if (LINE(715,x_array_key_exists(v_arg.rvalAt(v_i), m__shortLong))) {
            v_newArgs.append((concat("-", toString(v_arg.rvalAt(v_i)))));
          }
          else {
            v_newArgs.append((v_arg.rvalAt(v_i)));
          }
        }
      }
    }
    LINE(722,x_array_splice(ref(lval(m_args)), toInt32(v_pos), 1LL, v_newArgs));
    (m__longLong = ScalarArrays::sa_[0]);
    return -1LL;
  }
  (v_opt = "");
  {
    LOOP_COUNTER(12);
    for ((v_i = 0LL); less(v_i, LINE(732,x_strlen(toString(v_arg)))); v_i++) {
      LOOP_COUNTER_CHECK(12);
      {
        concat_assign(v_opt, toString(v_arg.rvalAt(v_i)));
        if (same(v_isLong, false) && isset(m__shortLong, v_opt)) {
          (v_cmp = v_opt);
          (v_long = m__shortLong.rvalAt(v_opt));
        }
        else if (same(v_isLong, true) && isset(m__config, v_opt)) {
          (v_long = (v_cmp = v_opt));
        }
        else if (same(v_isLong, true) && isset(m__aliasLong, v_opt)) {
          (v_long = m__aliasLong.rvalAt(v_opt));
          (v_cmp = v_opt);
        }
        if (same(v_arg.rvalAt(v_i), "=")) {
          break;
        }
      }
    }
  }
  if (equal(v_opt, "")) {
    (v_long = "parameters" /* CONSOLE_GETARGS_PARAMS */);
  }
  if (isset(v_long)) {
    if (more_rev(LINE(760,x_strlen(v_cmp)), x_strlen(toString(v_arg)))) {
      (v_arg = LINE(765,(assignCallTemp(eo_0, toString(v_arg)),assignCallTemp(eo_1, x_strlen(v_cmp)),x_substr(eo_0, eo_1))));
      if (same(v_arg.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "=")) {
        (v_arg = LINE(768,x_substr(toString(v_arg), toInt32(1LL))));
      }
    }
    else {
      (v_arg = "");
    }
    return LINE(776,t_setvalue(v_long, v_arg, ref(v_pos)));
  }
  return LINE(780,throw_fatal("unknown class pear", ((void*)NULL)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 790 */
Variant c_console_getargs_options::t_setvalue(CVarRef v_optname, CVarRef v_value, Variant v_pos) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::setValue);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  int64 v_max = 0;
  int64 v_min = 0;
  int64 v_added = 0;
  Numeric v_i = 0;
  bool v_paramFull = false;

  if (!(isset(m__config.rvalAt(v_optname), "max", 0x022BD5C706BD9850LL))) {
    return LINE(796,throw_fatal("unknown class pear", ((void*)NULL)));
  }
  (v_max = toInt64(m__config.rvalAt(v_optname).rvalAt("max", 0x022BD5C706BD9850LL)));
  (v_min = isset(m__config.rvalAt(v_optname), "min", 0x66FFC7365E2D00CALL) ? ((toInt64(m__config.rvalAt(v_optname).rvalAt("min", 0x66FFC7365E2D00CALL)))) : ((v_max)));
  if (!same(v_value, "")) {
    if (equal(v_min, 1LL) && more(v_max, 0LL)) {
      LINE(807,t_updatevalue(v_optname, v_value));
      return true;
    }
    if (same(v_max, 0LL)) {
      return LINE(814,(assignCallTemp(eo_0, LINE(812,concat3("Argument ", toString(v_optname), " does not take any value"))),assignCallTemp(eo_2, k_PEAR_ERROR_RETURN),throw_fatal("unknown class pear", (concat3("Argument ", toString(v_optname), " does not take any value"), (void*)NULL))));
    }
    return LINE(819,(assignCallTemp(eo_0, LINE(817,concat3("Argument ", toString(v_optname), " expects more than one value"))),assignCallTemp(eo_2, k_PEAR_ERROR_RETURN),throw_fatal("unknown class pear", (concat3("Argument ", toString(v_optname), " expects more than one value"), (void*)NULL))));
  }
  if (same(v_min, 1LL) && same(v_max, 1LL)) {
    if (equal(v_optname, "parameters" /* CONSOLE_GETARGS_PARAMS */)) {
      v_pos--;
    }
    if (isset(m_args, v_pos + 1LL) && LINE(828,t_isvalue(m_args.rvalAt(v_pos + 1LL)))) {
      LINE(830,t_updatevalue(v_optname, m_args.rvalAt(v_pos + 1LL)));
      v_pos++;
      return true;
    }
    return LINE(837,(assignCallTemp(eo_0, LINE(835,concat3("Argument ", toString(v_optname), " expects one value"))),assignCallTemp(eo_2, k_PEAR_ERROR_RETURN),throw_fatal("unknown class pear", (concat3("Argument ", toString(v_optname), " expects one value"), (void*)NULL))));
  }
  else if (same(v_max, 0LL)) {
    if (isset(m_args, v_pos + 1LL) && LINE(841,t_isvalue(m_args.rvalAt(v_pos + 1LL)))) {
      LINE(844,t_updatevalue(v_optname, true));
      if (isset(m__config, "parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL)) {
        return (assignCallTemp(eo_2, ++v_pos),LINE(847,t_setvalue("parameters" /* CONSOLE_GETARGS_PARAMS */, "", eo_2)));
      }
      else {
        return LINE(851,(assignCallTemp(eo_0, LINE(849,concat3("Argument ", toString(v_optname), " does not take any value"))),assignCallTemp(eo_2, k_PEAR_ERROR_RETURN),throw_fatal("unknown class pear", (concat3("Argument ", toString(v_optname), " does not take any value"), (void*)NULL))));
      }
    }
    LINE(855,t_updatevalue(v_optname, true));
    return true;
  }
  else if (not_less(v_max, 1LL) && same(v_min, 0LL)) {
    if (!(isset(m__config.rvalAt(v_optname), "default", 0x6DE26F84570270CCLL))) {
      return LINE(864,throw_fatal("unknown class pear", ((void*)NULL)));
    }
    if (LINE(866,x_is_array(m__config.rvalAt(v_optname).rvalAt("default", 0x6DE26F84570270CCLL)))) {
      return LINE(870,(assignCallTemp(eo_0, LINE(868,concat3("Default value for ", toString(v_optname), " must be scalar"))),assignCallTemp(eo_2, k_PEAR_ERROR_TRIGGER),throw_fatal("unknown class pear", (concat3("Default value for ", toString(v_optname), " must be scalar"), (void*)NULL))));
    }
    if (equal(v_optname, "parameters" /* CONSOLE_GETARGS_PARAMS */)) {
      v_pos--;
    }
    if (isset(m_args, v_pos + 1LL) && LINE(878,t_isvalue(m_args.rvalAt(v_pos + 1LL)))) {
      LINE(880,t_updatevalue(v_optname, m_args.rvalAt(v_pos + 1LL)));
      v_pos++;
      return true;
    }
    LINE(885,t_updatevalue(v_optname, m__config.rvalAt(v_optname).rvalAt("default", 0x6DE26F84570270CCLL)));
    return true;
  }
  (v_added = 0LL);
  if (equal(v_optname, "parameters" /* CONSOLE_GETARGS_PARAMS */)) {
    (v_pos = LINE(893,x_max(2, v_pos - 1LL, ScalarArrays::sa_[1])));
  }
  {
    LOOP_COUNTER(13);
    for ((v_i = v_pos + 1LL); not_more(v_i, LINE(895,x_count(m_args))); v_i++) {
      LOOP_COUNTER_CHECK(13);
      {
        (v_paramFull = not_more(v_max, LINE(896,x_count(t_getvalue(v_optname)))) && !equal(v_max, -1LL));
        if (isset(m_args, v_i) && LINE(897,t_isvalue(m_args.rvalAt(v_i))) && !(v_paramFull)) {
          LINE(899,t_updatevalue(v_optname, m_args.rvalAt(v_i)));
          v_added++;
          v_pos++;
          if ((less(v_added, v_max) || less(v_max, 0LL)) && (less(v_max, 0LL) || !(v_paramFull))) {
            continue;
          }
        }
        if (more(v_min, v_added) && !(v_paramFull)) {
          return LINE(912,(assignCallTemp(eo_0, LINE(910,concat5("Argument ", toString(v_optname), " expects at least ", toString(v_min), (toString((more(v_min, 1LL)) ? ((" values")) : ((" value"))))))),assignCallTemp(eo_2, k_PEAR_ERROR_RETURN),throw_fatal("unknown class pear", (concat5("Argument ", toString(v_optname), " expects at least ", toString(v_min), (toString((more(v_min, 1LL)) ? ((" values")) : ((" value"))))), (void*)NULL))));
        }
        else if (!same(v_max, -1LL) && v_paramFull) {
          if (isset(m__config, "parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL) && !equal(v_optname, "parameters" /* CONSOLE_GETARGS_PARAMS */)) {
            return (assignCallTemp(eo_2, ++v_pos),LINE(917,t_setvalue("parameters" /* CONSOLE_GETARGS_PARAMS */, "", eo_2)));
          }
          else if (equal(v_optname, "parameters" /* CONSOLE_GETARGS_PARAMS */) && empty(m_args, v_i)) {
            v_pos += v_added;
            break;
          }
          else {
            return LINE(924,(assignCallTemp(eo_0, LINE(922,concat5("Argument ", toString(v_optname), " expects maximum ", toString(v_max), " values"))),assignCallTemp(eo_2, k_PEAR_ERROR_RETURN),throw_fatal("unknown class pear", (concat5("Argument ", toString(v_optname), " expects maximum ", toString(v_max), " values"), (void*)NULL))));
          }
        }
        break;
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 938 */
bool c_console_getargs_options::t_isvalue(CVarRef v_arg) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::isValue);
  if ((more(LINE(940,x_strlen(toString(v_arg))), 1LL) && equal(v_arg.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-") && equal(v_arg.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "-")) || (more(LINE(941,x_strlen(toString(v_arg))), 1LL) && equal(v_arg.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-"))) {
    return false;
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 956 */
void c_console_getargs_options::t_updatevalue(CVarRef v_optname, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::updateValue);
  Variant v_prevValue;

  if (isset(m__longLong, v_optname)) {
    if (LINE(959,x_is_array(m__longLong.rvalAt(v_optname)))) {
      lval(m__longLong.lvalAt(v_optname)).append((v_value));
    }
    else {
      (v_prevValue = m__longLong.rvalAt(v_optname));
      m__longLong.set(v_optname, (Array(ArrayInit(1).set(0, v_prevValue).create())));
      lval(m__longLong.lvalAt(v_optname)).append((v_value));
    }
  }
  else {
    m__longLong.set(v_optname, (v_value));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 979 */
bool c_console_getargs_options::t_setdefaults() {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::setDefaults);
  Primitive v_longname = 0;
  Variant v_def;

  {
    LOOP_COUNTER(14);
    Variant map15 = m__config;
    for (ArrayIterPtr iter16 = map15.begin("console_getargs_options"); !iter16->end(); iter16->next()) {
      LOOP_COUNTER_CHECK(14);
      v_def = iter16->second();
      v_longname = iter16->first();
      {
        if (isset(v_def, "default", 0x6DE26F84570270CCLL) && ((isset(v_def, "min", 0x66FFC7365E2D00CALL) && !same(v_def.rvalAt("min", 0x66FFC7365E2D00CALL), 0LL)) || (toBoolean(bitwise_and(toByte(!(isset(v_def, "min", 0x66FFC7365E2D00CALL))), toByte(isset(v_def, "max", 0x022BD5C706BD9850LL)))) && !same(v_def.rvalAt("max", 0x022BD5C706BD9850LL), 0LL))) && !(isset(m__longLong, v_longname))) {
          m__longLong.set(v_longname, (v_def.rvalAt("default", 0x6DE26F84570270CCLL)));
        }
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 1005 */
bool c_console_getargs_options::t_isdefined(CVarRef v_optname) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::isDefined);
  Variant v_longname;

  (v_longname = LINE(1007,t_getlongname(v_optname)));
  if (isset(m__longLong, v_longname)) {
    return true;
  }
  return false;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 1025 */
Variant c_console_getargs_options::t_getlongname(CVarRef v_optname) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::getLongName);
  Variant v_longname;

  if (isset(m__shortLong, v_optname)) {
    (v_longname = m__shortLong.rvalAt(v_optname));
  }
  else if (isset(m__aliasLong, v_optname)) {
    (v_longname = m__aliasLong.rvalAt(v_optname));
  }
  else {
    (v_longname = v_optname);
  }
  return v_longname;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 1052 */
Variant c_console_getargs_options::t_getvalue(CVarRef v_optname) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::getValue);
  Variant v_longname;

  if (LINE(1054,t_isdefined(v_optname))) {
    (v_longname = LINE(1056,t_getlongname(v_optname)));
    return m__longLong.rvalAt(v_longname);
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 1074 */
Variant c_console_getargs_options::t_getvalues(CStrRef v_optionNames //  = "long"
) {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::getValues);
  Variant v_values;
  Primitive v_short = 0;
  Variant v_long;

  {
    Variant tmp18 = (v_optionNames);
    int tmp19 = -1;
    if (equal(tmp18, ("short"))) {
      tmp19 = 0;
    } else if (equal(tmp18, ("long"))) {
      tmp19 = 1;
    } else if (true) {
      tmp19 = 2;
    }
    switch (tmp19) {
    case 0:
      {
        (v_values = ScalarArrays::sa_[0]);
        {
          LOOP_COUNTER(20);
          Variant map21 = m__shortLong;
          for (ArrayIterPtr iter22 = map21.begin("console_getargs_options"); !iter22->end(); iter22->next()) {
            LOOP_COUNTER_CHECK(20);
            v_long = iter22->second();
            v_short = iter22->first();
            {
              if (isset(m__longLong, v_long)) {
                v_values.set(v_short, (m__longLong.rvalAt(v_long)));
              }
            }
          }
        }
        if (isset(m__longLong, "parameters", 0x7EEE4F53F51536B8LL)) {
          v_values.set("parameters", (m__longLong.rvalAt("parameters", 0x7EEE4F53F51536B8LL)), 0x7EEE4F53F51536B8LL);
        }
        return v_values;
      }
    case 1:
      {
      }
    case 2:
      {
        return m__longLong;
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 1094 */
Variant c_console_getargs_options::t_checkrequired() {
  INSTANCE_METHOD_INJECTION(Console_Getargs_Options, Console_Getargs_Options::checkRequired);
  Primitive v_optName = 0;
  Variant v_opt;
  Variant v_err;

  {
    LOOP_COUNTER(23);
    Variant map24 = m__config;
    for (ArrayIterPtr iter25 = map24.begin("console_getargs_options"); !iter25->end(); iter25->next()) {
      LOOP_COUNTER_CHECK(23);
      v_opt = iter25->second();
      v_optName = iter25->first();
      {
        if (isset(v_opt, "min", 0x66FFC7365E2D00CALL) && equal(v_opt.rvalAt("min", 0x66FFC7365E2D00CALL), 1LL) && same(LINE(1098,t_getvalue(v_optName)), null)) {
          (v_err = LINE(1104,throw_fatal("unknown class pear", ((void*)NULL))));
          return v_err;
        }
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 110 */
Variant c_console_getargs::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_console_getargs::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_console_getargs::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_console_getargs::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_console_getargs::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_console_getargs::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_console_getargs::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_console_getargs::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(console_getargs)
ObjectData *c_console_getargs::cloneImpl() {
  c_console_getargs *obj = NEW(c_console_getargs)();
  cloneSet(obj);
  return obj;
}
void c_console_getargs::cloneSet(c_console_getargs *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_console_getargs::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_console_getargs::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_console_getargs::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_console_getargs$os_get(const char *s) {
  return c_console_getargs::os_get(s, -1);
}
Variant &cw_console_getargs$os_lval(const char *s) {
  return c_console_getargs::os_lval(s, -1);
}
Variant cw_console_getargs$os_constant(const char *s) {
  return c_console_getargs::os_constant(s);
}
Variant cw_console_getargs$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_console_getargs::os_invoke(c, s, params, -1, fatal);
}
void c_console_getargs::init() {
}
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 212 */
Variant c_console_getargs::t_factory(Variant v_config //  = ScalarArrays::sa_[0]
, Variant v_arguments //  = null
) {
  INSTANCE_METHOD_INJECTION(Console_Getargs, Console_Getargs::factory);
  Variant v_obj;
  Variant v_err;

  (v_obj = ((Object)(LINE(215,p_console_getargs_options(p_console_getargs_options(NEWOBJ(c_console_getargs_options)())->create())))));
  (v_err = LINE(218,v_obj.o_invoke_few_args("init", 0x614F97E16B435A03LL, 2, v_config, v_arguments)));
  if (!same(v_err, true)) {
    return ref(v_err);
  }
  (v_err = LINE(224,v_obj.o_invoke_few_args("buildMaps", 0x043AFDF53A44A287LL, 0)));
  if (!same(v_err, true)) {
    return ref(v_err);
  }
  (v_err = LINE(230,v_obj.o_invoke_few_args("parseArgs", 0x03F04809A4B0F5FELL, 0)));
  if (!same(v_err, true)) {
    return ref(v_err);
  }
  (v_err = LINE(236,v_obj.o_invoke_few_args("setDefaults", 0x40304451B3D144B6LL, 0)));
  if (!same(v_err, true)) {
    return ref(v_err);
  }
  (v_err = LINE(242,v_obj.o_invoke_few_args("checkRequired", 0x75125E0AC0B091B3LL, 0)));
  if (!same(v_err, true)) {
    return ref(v_err);
  }
  return ref(v_obj);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 285 */
String c_console_getargs::t_gethelp(Variant v_config, Variant v_helpHeader //  = null
, CStrRef v_helpFooter //  = ""
, int64 v_maxlength //  = 78LL
, int64 v_indent //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Console_Getargs, Console_Getargs::getHelp);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_help;
  Variant v_optional;
  Variant v_required;
  Variant v_params;
  Variant v_filename;
  Variant v_argv;
  String v_indentStr;
  Variant v_v;
  int64 v_shortlen = 0;
  Variant v_item;
  Array v_shortArr;
  int64 v_i = 0;
  Primitive v_long = 0;
  Variant v_def;
  Array v_longArr;
  Variant v_col1;
  int64 v_max = 0;
  int64 v_min = 0;
  Sequence v_col2;
  int64 v_arglen = 0;
  Variant v_txt;
  int v_length = 0;
  int64 v_desclen = 0;
  String v_padding;
  Primitive v_k = 0;
  Variant v_desc;

  (v_help = "");
  if (!(isset(v_helpHeader))) {
    df_lambda_1(LINE(293,c_console_getargs::t_getoptionalrequired(ref(v_config))), v_optional, v_required, v_params);
    if (isset(g->gv__SERVER, "SCRIPT_NAME", 0x307C33F84E6BA43BLL)) {
      (v_filename = LINE(296,x_basename(toString(g->gv__SERVER.rvalAt("SCRIPT_NAME", 0x307C33F84E6BA43BLL)))));
    }
    else {
      (v_filename = v_argv.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
    }
    (v_helpHeader = LINE(293,(assignCallTemp(eo_1, toString(v_filename)),assignCallTemp(eo_3, LINE(302,concat4(toString(v_optional), " ", toString(v_required), " "))),assignCallTemp(eo_4, concat(toString(v_params), "\n\n")),concat5("Usage: ", eo_1, " ", eo_3, eo_4))));
  }
  (v_indentStr = LINE(308,x_str_repeat(" ", toInt32(toInt64(v_indent)))));
  (v_v = LINE(311,x_array_values(v_config)));
  (v_shortlen = 0LL);
  {
    LOOP_COUNTER(26);
    for (ArrayIterPtr iter28 = v_v.begin("console_getargs"); !iter28->end(); iter28->next()) {
      LOOP_COUNTER_CHECK(26);
      v_item = iter28->second();
      {
        if (isset(v_item, "short", 0x377E75AD6C01626DLL)) {
          (v_shortArr = LINE(315,x_explode("|", toString(v_item.rvalAt("short", 0x377E75AD6C01626DLL)))));
          if (more(LINE(317,x_strlen(toString(v_shortArr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), v_shortlen)) {
            (v_shortlen = toInt64(LINE(318,x_strlen(toString(v_shortArr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))))));
          }
        }
      }
    }
  }
  v_shortlen += 2LL;
  (v_i = 0LL);
  {
    LOOP_COUNTER(29);
    for (ArrayIterPtr iter31 = v_config.begin("console_getargs"); !iter31->end(); iter31->next()) {
      LOOP_COUNTER_CHECK(29);
      v_def = iter31->second();
      v_long = iter31->first();
      {
        (v_shortArr = ScalarArrays::sa_[0]);
        if (isset(v_def, "short", 0x377E75AD6C01626DLL)) {
          (v_shortArr = LINE(333,x_explode("|", toString(v_def.rvalAt("short", 0x377E75AD6C01626DLL)))));
        }
        (v_longArr = LINE(335,x_explode("|", toString(v_long))));
        v_col1.set(v_i, (v_indentStr));
        concat_assign(lval(v_col1.lvalAt(v_i)), LINE(341,x_str_pad(toString(!(empty(v_shortArr)) ? ((Variant)(concat3("-", toString(v_shortArr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), " "))) : ((Variant)(""))), toInt32(v_shortlen))));
        concat_assign(lval(v_col1.lvalAt(v_i)), concat("--", toString(v_longArr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
        (v_max = toInt64(v_def.rvalAt("max", 0x022BD5C706BD9850LL)));
        (v_min = isset(v_def, "min", 0x66FFC7365E2D00CALL) ? ((toInt64(v_def.rvalAt("min", 0x66FFC7365E2D00CALL)))) : ((v_max)));
        if (same(v_max, 1LL) && same(v_min, 1LL)) {
          concat_assign(lval(v_col1.lvalAt(v_i)), "=<value>");
        }
        else if (more(v_max, 1LL)) {
          if (same(v_min, v_max)) {
            concat_assign(lval(v_col1.lvalAt(v_i)), LINE(356,concat3(" values(", toString(v_max), ")")));
          }
          else if (same(v_min, 0LL)) {
            concat_assign(lval(v_col1.lvalAt(v_i)), " values(optional)");
          }
          else {
            concat_assign(lval(v_col1.lvalAt(v_i)), LINE(362,concat5(" values(", toString(v_min), "-", toString(v_max), ")")));
          }
        }
        else if (same(v_max, 1LL) && same(v_min, 0LL)) {
          concat_assign(lval(v_col1.lvalAt(v_i)), " (optional)value");
        }
        else if (same(v_max, -1LL)) {
          if (more(v_min, 0LL)) {
            concat_assign(lval(v_col1.lvalAt(v_i)), LINE(370,concat3(" values(", toString(v_min), "-...)")));
          }
          else {
            concat_assign(lval(v_col1.lvalAt(v_i)), " (optional)values");
          }
        }
        if (isset(v_def, "desc", 0x2BA186627396B419LL)) {
          v_col2.set(v_i, (v_def.rvalAt("desc", 0x2BA186627396B419LL)));
        }
        else {
          v_col2.set(v_i, (""));
        }
        if (isset(v_def, "default", 0x6DE26F84570270CCLL)) {
          if (LINE(384,x_is_array(v_def.rvalAt("default", 0x6DE26F84570270CCLL)))) {
            concat_assign(lval(v_col2.lvalAt(v_i)), LINE(385,(assignCallTemp(eo_1, x_implode(", ", v_def.rvalAt("default", 0x6DE26F84570270CCLL))),concat3(" (", eo_1, ")"))));
          }
          else {
            concat_assign(lval(v_col2.lvalAt(v_i)), LINE(387,concat3(" (", toString(v_def.rvalAt("default", 0x6DE26F84570270CCLL)), ")")));
          }
        }
        v_i++;
      }
    }
  }
  (v_arglen = 0LL);
  {
    LOOP_COUNTER(32);
    for (ArrayIterPtr iter34 = v_col1.begin("console_getargs"); !iter34->end(); iter34->next()) {
      LOOP_COUNTER_CHECK(32);
      v_txt = iter34->second();
      {
        (v_length = LINE(396,x_strlen(toString(v_txt))));
        if (more(v_length, v_arglen)) {
          (v_arglen = v_length);
        }
      }
    }
  }
  (v_desclen = v_maxlength - v_arglen);
  (v_padding = LINE(404,x_str_repeat(" ", toInt32(v_arglen))));
  {
    LOOP_COUNTER(35);
    for (ArrayIterPtr iter37 = v_col1.begin("console_getargs"); !iter37->end(); iter37->next()) {
      LOOP_COUNTER_CHECK(35);
      v_txt = iter37->second();
      v_k = iter37->first();
      {
        if (more(LINE(407,x_strlen(toString(v_col2.rvalAt(v_k)))), v_desclen)) {
          (v_desc = LINE(408,x_wordwrap(toString(v_col2.rvalAt(v_k)), toInt32(v_desclen), concat("\n  ", v_padding))));
        }
        else {
          (v_desc = v_col2.rvalAt(v_k));
        }
        concat_assign(v_help, LINE(413,(assignCallTemp(eo_0, x_str_pad(toString(v_txt), toInt32(v_arglen))),assignCallTemp(eo_2, toString(v_desc)),concat4(eo_0, "  ", eo_2, "\n"))));
      }
    }
  }
  return LINE(417,concat3(toString(v_helpHeader), v_help, v_helpFooter));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 439 */
Array c_console_getargs::t_getoptionalrequired(Variant v_config) {
  INSTANCE_METHOD_INJECTION(Console_Getargs, Console_Getargs::getOptionalRequired);
  String v_optional;
  bool v_optionalHasShort = false;
  String v_required;
  bool v_requiredHasShort = false;
  Variant v_long;
  Variant v_def;
  String v_params;
  int64 v_i = 0;

  (v_optional = "");
  (v_optionalHasShort = false);
  (v_required = "");
  (v_requiredHasShort = false);
  LINE(448,x_ksort(ref(v_config)));
  {
    LOOP_COUNTER(38);
    for (ArrayIterPtr iter40 = v_config.begin("console_getargs"); !iter40->end(); iter40->next()) {
      LOOP_COUNTER_CHECK(38);
      v_def = iter40->second();
      v_long = iter40->first();
      {
        (v_long = LINE(452,x_explode("|", toString(v_long))));
        (v_long = LINE(453,x_reset(ref(v_long))));
        if (equal(v_long, "parameters" /* CONSOLE_GETARGS_PARAMS */)) {
          continue;
        }
        if (isset(v_def, "short", 0x377E75AD6C01626DLL)) {
          v_def.set("short", (LINE(461,x_explode("|", toString(v_def.rvalAt("short", 0x377E75AD6C01626DLL))))), 0x377E75AD6C01626DLL);
          v_def.set("short", (LINE(462,x_reset(ref(lval(v_def.lvalAt("short", 0x377E75AD6C01626DLL)))))), 0x377E75AD6C01626DLL);
        }
        if (!(isset(v_def, "min", 0x66FFC7365E2D00CALL)) || equal(v_def.rvalAt("min", 0x66FFC7365E2D00CALL), 0LL) || isset(v_def, "default", 0x6DE26F84570270CCLL)) {
          if (isset(v_def, "short", 0x377E75AD6C01626DLL) && equal(LINE(467,x_strlen(toString(v_def.rvalAt("short", 0x377E75AD6C01626DLL)))), 1LL)) {
            (v_optional = concat(toString(v_def.rvalAt("short", 0x377E75AD6C01626DLL)), v_optional));
            (v_optionalHasShort = true);
          }
          else {
            concat_assign(v_optional, concat(" --", toString(v_long)));
          }
        }
        else {
          if (isset(v_def, "short", 0x377E75AD6C01626DLL) && equal(LINE(475,x_strlen(toString(v_def.rvalAt("short", 0x377E75AD6C01626DLL)))), 1LL)) {
            (v_required = concat(toString(v_def.rvalAt("short", 0x377E75AD6C01626DLL)), v_required));
            (v_requiredHasShort = true);
          }
          else {
            concat_assign(v_required, concat(" --", toString(v_long)));
          }
        }
      }
    }
  }
  (v_params = "");
  if (isset(v_config, "parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL)) {
    {
      LOOP_COUNTER(41);
      for ((v_i = 1LL); not_more(v_i, LINE(487,x_max(2, v_config.rvalAt("parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL).rvalAt("max", 0x022BD5C706BD9850LL), Array(ArrayInit(1).set(0, v_config.rvalAt("parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL).rvalAt("min", 0x66FFC7365E2D00CALL)).create())))); ++v_i) {
        LOOP_COUNTER_CHECK(41);
        {
          if (equal(v_config.rvalAt("parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL).rvalAt("max", 0x022BD5C706BD9850LL), -1LL) || (more(v_i, v_config.rvalAt("parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL).rvalAt("min", 0x66FFC7365E2D00CALL)) && not_more(v_i, v_config.rvalAt("parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL).rvalAt("max", 0x022BD5C706BD9850LL))) || isset(v_config.rvalAt("parameters" /* CONSOLE_GETARGS_PARAMS */, 0x7EEE4F53F51536B8LL), "default", 0x6DE26F84570270CCLL)) {
            concat_assign(v_params, LINE(493,concat3("[param", toString(v_i), "] ")));
          }
          else {
            concat_assign(v_params, LINE(496,concat3("param", toString(v_i), " ")));
          }
        }
      }
    }
  }
  if (v_optionalHasShort) {
    (v_optional = concat("-", v_optional));
  }
  if (v_requiredHasShort) {
    (v_required = concat("-", v_required));
  }
  if (!(empty(v_optional))) {
    (v_optional = LINE(511,concat3("[", v_optional, "]")));
  }
  if (!(empty(v_required))) {
    (v_required = LINE(514,concat3("<", v_required, ">")));
  }
  return Array(ArrayInit(3).set(0, v_optional).set(1, v_required).set(2, v_params).create());
} /* function */
Object co_console_getargs_options(CArrRef params, bool init /* = true */) {
  return Object(p_console_getargs_options(NEW(c_console_getargs_options)())->dynCreate(params, init));
}
Object co_console_getargs(CArrRef params, bool init /* = true */) {
  return Object(p_console_getargs(NEW(c_console_getargs)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$getargs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_benchmarks$benchcli$misc$getargs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(21,require("PEAR.php", true, variables, "phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/"));
  ;
  ;
  ;
  ;
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
