
#ifndef __GENERATED_cls_console_getargs_h__
#define __GENERATED_cls_console_getargs_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs.php line 110 */
class c_console_getargs : virtual public ObjectData {
  BEGIN_CLASS_MAP(console_getargs)
  END_CLASS_MAP(console_getargs)
  DECLARE_CLASS(console_getargs, Console_Getargs, ObjectData)
  void init();
  public: Variant t_factory(Variant v_config = ScalarArrays::sa_[0], Variant v_arguments = null);
  public: String t_gethelp(Variant v_config, Variant v_helpHeader = null, CStrRef v_helpFooter = "", int64 v_maxlength = 78LL, int64 v_indent = 0LL);
  public: Array t_getoptionalrequired(Variant v_config);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_console_getargs_h__
