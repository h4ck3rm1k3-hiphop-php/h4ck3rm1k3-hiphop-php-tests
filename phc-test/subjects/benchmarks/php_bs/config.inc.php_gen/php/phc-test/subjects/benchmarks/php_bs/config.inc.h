
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_config_inc_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_config_inc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/config.inc.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_copyright();
Variant pm_php$phc_test$subjects$benchmarks$php_bs$config_inc_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_config_inc_h__
