
#include <php/phc-test/subjects/benchmarks/php_bs/license.txt.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$license_txt(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/license.txt);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$license_txt;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("John's PHP Benchmarking Suite (JPBS) is licensed using BSD.\r\n\r\n\r\nBSD Style-License\r\n=================\r\n\r\nCopyright (c) 2003,2004 John Lim\r\nAll rights reserved.\r\n\r\nRedistribution and use in source and binary forms, with or without modification, \r\nare permitted provided that the following conditions are met:\r\n\r\nRedistributions of source code must retain the above copyright notice, this list \r\nof con");
  echo("ditions and the following disclaimer. \r\n\r\nRedistributions in binary form must reproduce the above copyright notice, this list \r\nof conditions and the following disclaimer in the documentation and/or other materials \r\nprovided with the distribution. \r\n\r\nNeither the name of the John Lim nor the names of its contributors may be used to \r\nendorse or promote products derived from this software without ");
  echo("specific prior written \r\npermission. \r\n\r\nDISCLAIMER:\r\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND ANY \r\nEXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF \r\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL \r\nJOHN LIM OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,");
  echo(" \r\nEXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF \r\nSUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) \r\nHOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, \r\nOR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS \r\nSOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF");
  echo(" SUCH DAMAGE.\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
