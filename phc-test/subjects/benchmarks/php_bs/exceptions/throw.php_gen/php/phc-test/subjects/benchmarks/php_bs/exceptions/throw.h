
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_exceptions_throw_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_exceptions_throw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/exceptions/throw.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$exceptions$throw_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_b1();
void f_b2();
void f_bench1();
void f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_exceptions_throw_h__
