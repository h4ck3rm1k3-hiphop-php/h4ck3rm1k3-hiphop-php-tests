
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/exceptions/throw.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/exceptions/throw.php line 21 */
void f_b1() {
  FUNCTION_INJECTION(b1);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/exceptions/throw.php line 34 */
void f_b2() {
  FUNCTION_INJECTION(b2);
  throw_exception(((Object)((((Object)(LINE(36,p_exception(p_exception(NEWOBJ(c_exception)())->create("fail")))))))));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/exceptions/throw.php line 16 */
void f_bench1() {
  FUNCTION_INJECTION(bench1);
  LINE(18,f_b1());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/exceptions/throw.php line 25 */
void f_bench2() {
  FUNCTION_INJECTION(bench2);
  p_exception v_e;

  try {
    LINE(28,f_b2());
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
    } else {
      throw;
    }
  }
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$exceptions$throw_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/exceptions/throw.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$exceptions$throw_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  LINE(42,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
