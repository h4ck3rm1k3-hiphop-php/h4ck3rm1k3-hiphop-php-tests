
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_vs_xpath_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php5_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php4_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 4:
      HASH_INCLUDE(0x76F2CC10255E7924LL, "phc-test/subjects/benchmarks/php_bs/init.inc.php", php$phc_test$subjects$benchmarks$php_bs$init_inc_php);
      break;
    case 6:
      HASH_INCLUDE(0x7481DF3F2A9AEBD6LL, "phc-test/subjects/benchmarks/php_bs/xml/xpath_php5.inc.php", php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php5_inc_php);
      break;
    case 8:
      HASH_INCLUDE(0x15C70A839C2C3C28LL, "phc-test/subjects/benchmarks/php_bs/xml/explode_vs_xpath.php", php$phc_test$subjects$benchmarks$php_bs$xml$explode_vs_xpath_php);
      break;
    case 11:
      HASH_INCLUDE(0x7734FDB8E1EADD5BLL, "phc-test/subjects/benchmarks/php_bs/xml/xpath_php4.inc.php", php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php4_inc_php);
      break;
    case 13:
      HASH_INCLUDE(0x46AA2A0E3340214DLL, "phc-test/subjects/benchmarks/php_bs/xml/explode.inc.php", php$phc_test$subjects$benchmarks$php_bs$xml$explode_inc_php);
      HASH_INCLUDE(0x017D1864CA1AAE4DLL, "phc-test/subjects/benchmarks/php_bs/bench.inc.php", php$phc_test$subjects$benchmarks$php_bs$bench_inc_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
