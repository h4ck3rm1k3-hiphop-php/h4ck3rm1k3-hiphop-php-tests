
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/explode.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/explode_vs_xpath.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/xpath_php4.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/xpath_php5.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_vs_xpath_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/xml/explode_vs_xpath.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_vs_xpath_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_XML __attribute__((__unused__)) = (variables != gVariables) ? variables->get("XML") : g->GV(XML);

  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  (v_XML = LINE(12,x_file_get_contents("rss.xml")));
  LINE(16,pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_inc_php(true, variables));
  LINE(18,pm_php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php5_inc_php(true, variables));
  LINE(22,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
