
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_explode_vs_xpath_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_explode_vs_xpath_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/explode_vs_xpath.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_vs_xpath_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_explode_vs_xpath_h__
