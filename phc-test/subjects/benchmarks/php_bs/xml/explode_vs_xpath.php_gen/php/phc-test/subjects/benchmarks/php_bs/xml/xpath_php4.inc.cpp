
#include <php/phc-test/subjects/benchmarks/php_bs/xml/xpath_php4.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/xpath_php4.inc.php line 5 */
Array f_bench2_DupId1() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_XML __attribute__((__unused__)) = g->GV(XML);
  Variant v_dom;
  Variant v_ctx;
  Variant v_nodes;
  Variant v_v;
  Array v_arr;

  (v_dom = LINE(9,invoke_failed("domxml_open_mem", Array(ArrayInit(1).set(0, ref(gv_XML)).create()), 0x0000000032462932LL)));
  (v_ctx = LINE(10,v_dom.o_invoke_few_args("xpath_new_context", 0x1D86CC56EE8E0187LL, 0)));
  (v_nodes = LINE(11,v_ctx.o_invoke_few_args("xpath_eval", 0x61AF412A9EAD5408LL, 1, "//title/text()")));
  {
    LOOP_COUNTER(1);
    Variant map2 = v_nodes.o_get("nodeset", 0x6AF7EE586555EB55LL);
    for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        v_arr.append((LINE(14,v_v.o_invoke_few_args("node_value", 0x125FE070C47921A9LL, 0))));
      }
    }
  }
  LINE(16,v_dom.o_invoke_few_args("free", 0x6831682926A45450LL, 0));
  return v_arr;
} /* function */
Variant i_bench2_DupId1(CArrRef params) {
  return (f_bench2_DupId1());
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php4_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/xml/xpath_php4.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php4_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  g->i_bench2 = i_bench2_DupId1;
  g->declareFunction("bench2");
  f_exit("domxml not installed");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
