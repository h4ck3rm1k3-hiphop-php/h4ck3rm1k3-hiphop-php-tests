
#include <php/phc-test/subjects/benchmarks/php_bs/xml/xpath_php5.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/xpath_php5.inc.php line 3 */
Array f_bench2_DupId0() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_XML __attribute__((__unused__)) = g->GV(XML);
  p_domdocument v_dom;
  p_domxpath v_xp;
  Variant v_nodes;
  Variant v_n;
  Array v_arr;

  ((Object)((v_dom = ((Object)(LINE(8,create_object("domdocument", Array())))))));
  LINE(9,v_dom->t_loadxml(toString(gv_XML)));
  ((Object)((v_xp = ((Object)(LINE(10,p_domxpath(p_domxpath(NEWOBJ(c_domxpath)())->create(((Object)(v_dom))))))))));
  (v_nodes = LINE(11,v_xp->t_query("//title/text()")));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_nodes.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_n = iter3->second();
      {
        v_arr.append((v_n.o_get("nodeValue", 0x410C368CB95B4985LL)));
      }
    }
  }
  return v_arr;
} /* function */
Variant i_bench2_DupId0(CArrRef params) {
  return (f_bench2_DupId0());
}
Variant i_bench2(CArrRef params) {
  DECLARE_GLOBAL_VARIABLES(g);
  return g->i_bench2(params);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php5_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/xml/xpath_php5.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$xml$xpath_php5_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  g->i_bench2 = i_bench2_DupId0;
  g->declareFunction("bench2");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
