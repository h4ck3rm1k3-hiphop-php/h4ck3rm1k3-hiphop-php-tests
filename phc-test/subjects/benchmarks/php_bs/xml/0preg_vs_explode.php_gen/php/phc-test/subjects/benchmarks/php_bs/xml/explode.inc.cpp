
#include <php/phc-test/subjects/benchmarks/php_bs/xml/explode.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/explode.inc.php line 3 */
Array f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_XML __attribute__((__unused__)) = g->GV(XML);
  Array v_arr;
  Array v_x;
  Variant v_v;
  Variant v_pos;

  (v_arr = ScalarArrays::sa_[0]);
  (v_x = LINE(8,x_explode("<title>", toString(gv_XML))));
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_x.begin(); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3.second();
      {
        (v_pos = LINE(10,x_strpos(toString(v_v), "</title>")));
        (v_v = LINE(11,x_substr(toString(v_v), toInt32(0LL), toInt32(v_pos))));
        if (toBoolean(v_v)) v_arr.append((v_v));
      }
    }
  }
  return v_arr;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/xml/explode.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
