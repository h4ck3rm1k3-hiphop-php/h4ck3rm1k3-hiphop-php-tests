
#include <php/phc-test/subjects/benchmarks/php_bs/xml/domxml_php5.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/domxml_php5.inc.php line 3 */
Array f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_XML __attribute__((__unused__)) = g->GV(XML);
  p_domdocument v_dom;
  Variant v_children;
  Variant v_v;
  Array v_arr;

  ((Object)((v_dom = ((Object)(LINE(7,create_object("domdocument", Array())))))));
  LINE(8,v_dom->t_loadxml(toString(gv_XML)));
  (v_children = LINE(10,v_dom->t_getelementsbytagname("title")));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_children.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        v_arr.append((v_v.o_get("nodeValue", 0x410C368CB95B4985LL)));
      }
    }
  }
  return v_arr;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$domxml_php5_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/xml/domxml_php5.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$xml$domxml_php5_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
