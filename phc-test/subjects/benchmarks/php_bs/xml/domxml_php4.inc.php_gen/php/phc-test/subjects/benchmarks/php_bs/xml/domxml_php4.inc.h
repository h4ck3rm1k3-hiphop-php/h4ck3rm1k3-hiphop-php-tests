
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_domxml_php4_inc_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_domxml_php4_inc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/domxml_php4.inc.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$domxml_php4_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Array f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_domxml_php4_inc_h__
