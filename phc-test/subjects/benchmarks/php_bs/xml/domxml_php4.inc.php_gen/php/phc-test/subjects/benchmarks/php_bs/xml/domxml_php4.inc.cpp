
#include <php/phc-test/subjects/benchmarks/php_bs/xml/domxml_php4.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/domxml_php4.inc.php line 4 */
Array f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_XML __attribute__((__unused__)) = g->GV(XML);
  Variant v_dom;
  Variant v_children;
  Variant v_v;
  Array v_arr;

  (v_dom = LINE(8,invoke_failed("domxml_open_mem", Array(ArrayInit(1).set(0, ref(gv_XML)).create()), 0x0000000032462932LL)));
  (v_children = LINE(9,v_dom.o_invoke_few_args("get_elements_by_tagname", 0x0EC2102EB8D7955BLL, 1, "title")));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_children.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        v_arr.append((LINE(12,v_v.o_invoke_few_args("get_content", 0x438345E5A0F08C22LL, 0))));
      }
    }
  }
  LINE(14,v_dom.o_invoke_few_args("free", 0x6831682926A45450LL, 0));
  return v_arr;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$domxml_php4_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/xml/domxml_php4.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$xml$domxml_php4_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  f_exit("domxml not installed");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
