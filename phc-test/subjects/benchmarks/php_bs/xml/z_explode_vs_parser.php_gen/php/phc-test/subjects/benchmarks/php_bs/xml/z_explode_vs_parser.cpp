
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/explode.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php line 29 */
void f_char_handler(CVarRef v_inParser, CVarRef v_inData) {
  FUNCTION_INJECTION(char_handler);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  Variant &gv_ISTITLE __attribute__((__unused__)) = g->GV(ISTITLE);
  {
  }
  if (toBoolean(gv_ISTITLE)) {
    gv_ARR.append((v_inData));
    (gv_ISTITLE = false);
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php line 45 */
void f_ele_end_handler(CVarRef v_parser, CVarRef v_element) {
  FUNCTION_INJECTION(ele_end_handler);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php line 49 */
Variant f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_XML __attribute__((__unused__)) = g->GV(XML);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  Object v_parser;

  {
  }
  (gv_ARR = ScalarArrays::sa_[0]);
  (v_parser = LINE(54,x_xml_parser_create()));
  LINE(55,x_xml_parser_set_option(v_parser, toInt32(1LL) /* XML_OPTION_CASE_FOLDING */, false));
  LINE(56,x_xml_set_element_handler(v_parser, "ele_handler", "ele_end_handler"));
  LINE(57,x_xml_set_character_data_handler(v_parser, "char_handler"));
  LINE(58,x_xml_parse(v_parser, toString(gv_XML)));
  return gv_ARR;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php line 39 */
void f_ele_handler(CVarRef v_parser, CVarRef v_element, CVarRef v_attr) {
  FUNCTION_INJECTION(ele_handler);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ISTITLE __attribute__((__unused__)) = g->GV(ISTITLE);
  (gv_ISTITLE = (equal(v_element, "title")));
} /* function */
Variant i_char_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x74FD81B8023A7C7ELL, char_handler) {
    return (f_char_handler(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_ele_end_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x78F09505E7EC4234LL, ele_end_handler) {
    return (f_ele_end_handler(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_ele_handler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1C73E4F05D91CD38LL, ele_handler) {
    return (f_ele_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$z_explode_vs_parser_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$xml$z_explode_vs_parser_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_XML __attribute__((__unused__)) = (variables != gVariables) ? variables->get("XML") : g->GV(XML);
  Variant &v_XML2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("XML2") : g->GV(XML2);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  (v_XML = LINE(13,x_file_get_contents("rss.xml")));
  (v_XML2 = "<\?xml version=\"1.0\" \?>\n<library>\n\t<book><title>Programming PHP</title><pages>507</pages></book>\n\t<book><title>Programming Perl</title><author>Larry Wall</author></book>\n\t<book><title>Programming C#</title></book>\n\t<book><title>PHP Performance Tuning</title></book>\n</library>");
  LINE(26,pm_php$phc_test$subjects$benchmarks$php_bs$xml$explode_inc_php(true, variables));
  LINE(64,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
