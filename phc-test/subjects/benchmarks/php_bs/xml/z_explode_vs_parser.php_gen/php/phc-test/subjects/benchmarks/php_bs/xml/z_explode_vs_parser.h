
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_z_explode_vs_parser_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_z_explode_vs_parser_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_char_handler(CVarRef v_inParser, CVarRef v_inData);
void f_ele_end_handler(CVarRef v_parser, CVarRef v_element);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$xml$z_explode_vs_parser_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_bench2();
void f_ele_handler(CVarRef v_parser, CVarRef v_element, CVarRef v_attr);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_xml_z_explode_vs_parser_h__
