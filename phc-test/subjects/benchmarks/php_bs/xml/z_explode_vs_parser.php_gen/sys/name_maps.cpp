
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "_bench1", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "bench1", "phc-test/subjects/benchmarks/php_bs/xml/explode.inc.php",
  "bench2", "phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php",
  "char_handler", "phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php",
  "dobench1", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "dobench2", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "dobenchmark", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "ele_end_handler", "phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php",
  "ele_handler", "phc-test/subjects/benchmarks/php_bs/xml/z_explode_vs_parser.php",
  "file_get_contents", "phc-test/subjects/benchmarks/php_bs/init.inc.php",
  "getmicrotime", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
