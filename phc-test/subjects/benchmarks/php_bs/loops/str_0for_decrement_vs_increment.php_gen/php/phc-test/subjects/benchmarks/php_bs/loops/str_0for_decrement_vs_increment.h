
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_str_0for_decrement_vs_increment_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_str_0for_decrement_vs_increment_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/loops/str_0for_decrement_vs_increment.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Array f_bench1();
Array f_bench2();
Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$str_0for_decrement_vs_increment_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_str_0for_decrement_vs_increment_h__
