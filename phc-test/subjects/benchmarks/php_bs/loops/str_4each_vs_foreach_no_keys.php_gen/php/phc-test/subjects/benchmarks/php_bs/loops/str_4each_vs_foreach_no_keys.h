
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_str_4each_vs_foreach_no_keys_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_str_4each_vs_foreach_no_keys_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/loops/str_4each_vs_foreach_no_keys.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$str_4each_vs_foreach_no_keys_php(bool incOnce = false, LVariableTable* variables = NULL);
Array f_bench1();
Array f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_str_4each_vs_foreach_no_keys_h__
