
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_z_map_vs_foreach_crc32_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_z_map_vs_foreach_crc32_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/loops/z_map_vs_foreach_crc32.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$z_map_vs_foreach_crc32_php(bool incOnce = false, LVariableTable* variables = NULL);
Array f_bench1();
Variant f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_z_map_vs_foreach_crc32_h__
