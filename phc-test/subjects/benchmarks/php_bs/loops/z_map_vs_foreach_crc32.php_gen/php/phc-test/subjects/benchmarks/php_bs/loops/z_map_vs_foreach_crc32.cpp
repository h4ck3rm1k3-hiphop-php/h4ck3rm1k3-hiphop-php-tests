
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/loops/z_map_vs_foreach_crc32.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/loops/z_map_vs_foreach_crc32.php line 26 */
Array f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  Variant v_v;
  Array v_j;

  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = gv_ARR.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_j.append((LINE(30,x_crc32(toString(v_v)))));
    }
  }
  return v_j;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/loops/z_map_vs_foreach_crc32.php line 19 */
Variant f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  return LINE(22,x_array_map(2, "crc32", gv_ARR));
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$z_map_vs_foreach_crc32_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/loops/z_map_vs_foreach_crc32.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$loops$z_map_vs_foreach_crc32_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_ARR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ARR") : g->GV(ARR);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, 50LL); v_i++) {
      LOOP_COUNTER_CHECK(4);
      v_ARR.append(("ASDFGHJKL CVBNM ASDFGHJKL CVBNM ASDFGHJKL CVBNM ASDFGHJKL CVBNM 1234567890 ASDFGHJKL CVBNM"));
    }
  }
  LINE(36,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
