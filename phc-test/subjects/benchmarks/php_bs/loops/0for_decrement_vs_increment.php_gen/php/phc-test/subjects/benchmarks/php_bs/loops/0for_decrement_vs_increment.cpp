
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/loops/0for_decrement_vs_increment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/loops/0for_decrement_vs_increment.php line 17 */
Array f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  int v_i = 0;
  Array v_j;

  {
    LOOP_COUNTER(1);
    for ((v_i = LINE(21,x_sizeof(gv_ARR))); not_less(--v_i, 0LL); ) {
      LOOP_COUNTER_CHECK(1);
      v_j.append((gv_ARR.rvalAt(v_i)));
    }
  }
  return v_j;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/loops/0for_decrement_vs_increment.php line 27 */
Array f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  int64 v_i = 0;
  int v_max = 0;
  Array v_j;

  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL), (v_max = LINE(31,x_sizeof(gv_ARR))); less(v_i, v_max); v_i++) {
      LOOP_COUNTER_CHECK(2);
      v_j.append((gv_ARR.rvalAt(v_i)));
    }
  }
  return v_j;
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$0for_decrement_vs_increment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/loops/0for_decrement_vs_increment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$loops$0for_decrement_vs_increment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_ARR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ARR") : g->GV(ARR);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, 50LL); v_i++) {
      LOOP_COUNTER_CHECK(3);
      v_ARR.append((v_i));
    }
  }
  LINE(38,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
