
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/loops/4each_vs_foreach_no_keys.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/loops/4each_vs_foreach_no_keys.php line 17 */
Array f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  Variant v_v;
  Array v_j;

  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = gv_ARR.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        v_j.append((v_v));
      }
    }
  }
  return v_j;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/loops/4each_vs_foreach_no_keys.php line 27 */
Array f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ARR __attribute__((__unused__)) = g->GV(ARR);
  Variant v_v;
  Array v_j;

  LINE(31,x_reset(ref(gv_ARR)));
  LOOP_COUNTER(4);
  {
    while (toBoolean(df_lambda_1(LINE(32,x_each(ref(gv_ARR))), v_v))) {
      LOOP_COUNTER_CHECK(4);
      {
        v_j.append((v_v));
      }
    }
  }
  return v_j;
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$4each_vs_foreach_no_keys_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/loops/4each_vs_foreach_no_keys.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$loops$4each_vs_foreach_no_keys_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_ARR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ARR") : g->GV(ARR);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  {
    LOOP_COUNTER(5);
    for ((v_i = 0LL); less(v_i, 50LL); v_i++) {
      LOOP_COUNTER_CHECK(5);
      v_ARR.append((v_i));
    }
  }
  LINE(43,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
