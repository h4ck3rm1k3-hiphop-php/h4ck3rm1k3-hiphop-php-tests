
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$str_1for_decrement_vs_increment_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_INCLUDE(0x46D49DC78C211360LL, "phc-test/subjects/benchmarks/php_bs/loops/str_1for_decrement_vs_increment.php", php$phc_test$subjects$benchmarks$php_bs$loops$str_1for_decrement_vs_increment_php);
      break;
    case 4:
      HASH_INCLUDE(0x76F2CC10255E7924LL, "phc-test/subjects/benchmarks/php_bs/init.inc.php", php$phc_test$subjects$benchmarks$php_bs$init_inc_php);
      break;
    case 5:
      HASH_INCLUDE(0x017D1864CA1AAE4DLL, "phc-test/subjects/benchmarks/php_bs/bench.inc.php", php$phc_test$subjects$benchmarks$php_bs$bench_inc_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
