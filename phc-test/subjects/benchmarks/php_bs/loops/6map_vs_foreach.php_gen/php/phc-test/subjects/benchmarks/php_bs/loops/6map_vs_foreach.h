
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_6map_vs_foreach_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_6map_vs_foreach_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/loops/6map_vs_foreach.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$loops$6map_vs_foreach_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_v(CVarRef v_v);
Array f_bench1();
Variant f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_loops_6map_vs_foreach_h__
