
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_10ref_vs_value_array_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_10ref_vs_value_array_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/10ref_vs_value_array.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f__b1(Variant v_a);
void f__b2(Variant v_a);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$10ref_vs_value_array_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bench1();
void f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_10ref_vs_value_array_h__
