
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "x", "phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "_b1", "phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php",
  "_b2", "phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php",
  "_bench1", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "bench1", "phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php",
  "bench2", "phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php",
  "dobench1", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "dobench2", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "dobenchmark", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "file_get_contents", "phc-test/subjects/benchmarks/php_bs/init.inc.php",
  "getmicrotime", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
