
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php line 12 */
Variant c_x::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_x::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_x::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  props.push_back(NEW(ArrayElement)("b", m_b));
  props.push_back(NEW(ArrayElement)("c", m_c));
  props.push_back(NEW(ArrayElement)("d", m_d));
  props.push_back(NEW(ArrayElement)("e", m_e));
  props.push_back(NEW(ArrayElement)("f", m_f));
  props.push_back(NEW(ArrayElement)("str1", m_str1));
  props.push_back(NEW(ArrayElement)("str2", m_str2));
  props.push_back(NEW(ArrayElement)("str3", m_str3));
  props.push_back(NEW(ArrayElement)("str4", m_str4));
  props.push_back(NEW(ArrayElement)("str5", m_str5));
  props.push_back(NEW(ArrayElement)("str6", m_str6));
  props.push_back(NEW(ArrayElement)("str11", m_str11));
  props.push_back(NEW(ArrayElement)("str12", m_str12));
  props.push_back(NEW(ArrayElement)("str13", m_str13));
  props.push_back(NEW(ArrayElement)("str14", m_str14));
  props.push_back(NEW(ArrayElement)("str15", m_str15));
  props.push_back(NEW(ArrayElement)("str16", m_str16));
  c_ObjectData::o_get(props);
}
bool c_x::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_EXISTS_STRING(0x3A32ECE6CA52C540LL, str16, 5);
      break;
    case 4:
      HASH_EXISTS_STRING(0x7A452383AA9BF7C4LL, d, 1);
      break;
    case 10:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      HASH_EXISTS_STRING(0x722BA9EF9AAF148ALL, str6, 4);
      break;
    case 15:
      HASH_EXISTS_STRING(0x210BB96D0F12A60FLL, str4, 4);
      break;
    case 17:
      HASH_EXISTS_STRING(0x2DDEC88D71B4D151LL, str13, 5);
      break;
    case 21:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    case 24:
      HASH_EXISTS_STRING(0x02608B50F6372358LL, str15, 5);
      break;
    case 26:
      HASH_EXISTS_STRING(0x65B12A2345D2DF1ALL, str14, 5);
      break;
    case 34:
      HASH_EXISTS_STRING(0x5E0B0861166F4522LL, str11, 5);
      break;
    case 43:
      HASH_EXISTS_STRING(0x61B161496B7EA7EBLL, e, 1);
      HASH_EXISTS_STRING(0x0BA18693D00110EBLL, str2, 4);
      break;
    case 45:
      HASH_EXISTS_STRING(0x243374469C6F1DADLL, str1, 4);
      HASH_EXISTS_STRING(0x46B6B25317630FADLL, str12, 5);
      break;
    case 46:
      HASH_EXISTS_STRING(0x6BB4A0689FBAD42ELL, f, 1);
      HASH_EXISTS_STRING(0x190F0ED87570A72ELL, str3, 4);
      break;
    case 48:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    case 52:
      HASH_EXISTS_STRING(0x2460A89C3247BD34LL, str5, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_x::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_RETURN_STRING(0x3A32ECE6CA52C540LL, m_str16,
                         str16, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x7A452383AA9BF7C4LL, m_d,
                         d, 1);
      break;
    case 10:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      HASH_RETURN_STRING(0x722BA9EF9AAF148ALL, m_str6,
                         str6, 4);
      break;
    case 15:
      HASH_RETURN_STRING(0x210BB96D0F12A60FLL, m_str4,
                         str4, 4);
      break;
    case 17:
      HASH_RETURN_STRING(0x2DDEC88D71B4D151LL, m_str13,
                         str13, 5);
      break;
    case 21:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 24:
      HASH_RETURN_STRING(0x02608B50F6372358LL, m_str15,
                         str15, 5);
      break;
    case 26:
      HASH_RETURN_STRING(0x65B12A2345D2DF1ALL, m_str14,
                         str14, 5);
      break;
    case 34:
      HASH_RETURN_STRING(0x5E0B0861166F4522LL, m_str11,
                         str11, 5);
      break;
    case 43:
      HASH_RETURN_STRING(0x61B161496B7EA7EBLL, m_e,
                         e, 1);
      HASH_RETURN_STRING(0x0BA18693D00110EBLL, m_str2,
                         str2, 4);
      break;
    case 45:
      HASH_RETURN_STRING(0x243374469C6F1DADLL, m_str1,
                         str1, 4);
      HASH_RETURN_STRING(0x46B6B25317630FADLL, m_str12,
                         str12, 5);
      break;
    case 46:
      HASH_RETURN_STRING(0x6BB4A0689FBAD42ELL, m_f,
                         f, 1);
      HASH_RETURN_STRING(0x190F0ED87570A72ELL, m_str3,
                         str3, 4);
      break;
    case 48:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 52:
      HASH_RETURN_STRING(0x2460A89C3247BD34LL, m_str5,
                         str5, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_x::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_SET_STRING(0x3A32ECE6CA52C540LL, m_str16,
                      str16, 5);
      break;
    case 4:
      HASH_SET_STRING(0x7A452383AA9BF7C4LL, m_d,
                      d, 1);
      break;
    case 10:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      HASH_SET_STRING(0x722BA9EF9AAF148ALL, m_str6,
                      str6, 4);
      break;
    case 15:
      HASH_SET_STRING(0x210BB96D0F12A60FLL, m_str4,
                      str4, 4);
      break;
    case 17:
      HASH_SET_STRING(0x2DDEC88D71B4D151LL, m_str13,
                      str13, 5);
      break;
    case 21:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    case 24:
      HASH_SET_STRING(0x02608B50F6372358LL, m_str15,
                      str15, 5);
      break;
    case 26:
      HASH_SET_STRING(0x65B12A2345D2DF1ALL, m_str14,
                      str14, 5);
      break;
    case 34:
      HASH_SET_STRING(0x5E0B0861166F4522LL, m_str11,
                      str11, 5);
      break;
    case 43:
      HASH_SET_STRING(0x61B161496B7EA7EBLL, m_e,
                      e, 1);
      HASH_SET_STRING(0x0BA18693D00110EBLL, m_str2,
                      str2, 4);
      break;
    case 45:
      HASH_SET_STRING(0x243374469C6F1DADLL, m_str1,
                      str1, 4);
      HASH_SET_STRING(0x46B6B25317630FADLL, m_str12,
                      str12, 5);
      break;
    case 46:
      HASH_SET_STRING(0x6BB4A0689FBAD42ELL, m_f,
                      f, 1);
      HASH_SET_STRING(0x190F0ED87570A72ELL, m_str3,
                      str3, 4);
      break;
    case 48:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    case 52:
      HASH_SET_STRING(0x2460A89C3247BD34LL, m_str5,
                      str5, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_x::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_x::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(x)
ObjectData *c_x::cloneImpl() {
  c_x *obj = NEW(c_x)();
  cloneSet(obj);
  return obj;
}
void c_x::cloneSet(c_x *clone) {
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  clone->m_b = m_b;
  clone->m_c = m_c;
  clone->m_d = m_d;
  clone->m_e = m_e;
  clone->m_f = m_f;
  clone->m_str1 = m_str1;
  clone->m_str2 = m_str2;
  clone->m_str3 = m_str3;
  clone->m_str4 = m_str4;
  clone->m_str5 = m_str5;
  clone->m_str6 = m_str6;
  clone->m_str11 = m_str11;
  clone->m_str12 = m_str12;
  clone->m_str13 = m_str13;
  clone->m_str14 = m_str14;
  clone->m_str15 = m_str15;
  clone->m_str16 = m_str16;
  ObjectData::cloneSet(clone);
}
Variant c_x::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_x::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_x::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_x$os_get(const char *s) {
  return c_x::os_get(s, -1);
}
Variant &cw_x$os_lval(const char *s) {
  return c_x::os_lval(s, -1);
}
Variant cw_x$os_constant(const char *s) {
  return c_x::os_constant(s);
}
Variant cw_x$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_x::os_invoke(c, s, params, -1, fatal);
}
void c_x::init() {
  m_a = 1LL;
  m_b = 2LL;
  m_c = 3LL;
  m_d = 4LL;
  m_e = 5LL;
  m_f = 6LL;
  m_str1 = "The The The The The The The The";
  m_str2 = "The The The The The The The The";
  m_str3 = "The The The The The The The The";
  m_str4 = "The The The The The The The The";
  m_str5 = "The The The The The The The The";
  m_str6 = "The The The The The The The The";
  m_str11 = "The The The The The The The The";
  m_str12 = "The The The The The The The The";
  m_str13 = "The The The The The The The The";
  m_str14 = "The The The The The The The The";
  m_str15 = "The The The The The The The The";
  m_str16 = "The The The The The The The The";
}
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php line 38 */
void f__b1(Variant v_a) {
  FUNCTION_INJECTION(_b1);
  Object v_X;

  (v_X.o_lval("a", 0x4292CEE227B9150ALL) = "a");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php line 43 */
void f__b2(CVarRef v_a) {
  FUNCTION_INJECTION(_b2);
  Object v_X;

  (v_X.o_lval("a", 0x4292CEE227B9150ALL) = "a");
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php line 48 */
void f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_X __attribute__((__unused__)) = g->GV(X);
  LINE(51,f__b1(ref(gv_X)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php line 55 */
void f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_X __attribute__((__unused__)) = g->GV(X);
  LINE(59,f__b2(gv_X));
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_x(CArrRef params, bool init /* = true */) {
  return Object(p_x(NEW(c_x)())->dynCreate(params, init));
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$12ref_vs_value_obj_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$functions$12ref_vs_value_obj_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_X __attribute__((__unused__)) = (variables != gVariables) ? variables->get("X") : g->GV(X);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  (v_X = ((Object)(LINE(34,p_x(p_x(NEWOBJ(c_x)())->create())))));
  LINE(64,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
