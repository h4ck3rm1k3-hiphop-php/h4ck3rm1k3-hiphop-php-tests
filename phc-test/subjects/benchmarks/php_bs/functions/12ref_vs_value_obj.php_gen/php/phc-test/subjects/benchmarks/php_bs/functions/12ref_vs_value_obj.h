
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_12ref_vs_value_obj_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_12ref_vs_value_obj_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/12ref_vs_value_obj.fw.h>

// Declarations
#include <cls/x.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f__b1(Variant v_a);
void f__b2(CVarRef v_a);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$12ref_vs_value_obj_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bench1();
void f_bench2();
Object co_x(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_12ref_vs_value_obj_h__
