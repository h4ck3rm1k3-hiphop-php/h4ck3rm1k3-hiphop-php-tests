
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_31return_by_ref_vs_value_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_31return_by_ref_vs_value_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/31return_by_ref_vs_value.fw.h>

// Declarations
#include <cls/x.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$31return_by_ref_vs_value_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f__b1();
Variant f__b2();
void f_bench1();
void f_bench2();
Object co_x(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_31return_by_ref_vs_value_h__
