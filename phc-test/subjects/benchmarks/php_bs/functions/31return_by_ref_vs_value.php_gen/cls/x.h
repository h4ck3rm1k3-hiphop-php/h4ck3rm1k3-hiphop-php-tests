
#ifndef __GENERATED_cls_x_h__
#define __GENERATED_cls_x_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/subjects/benchmarks/php_bs/functions/31return_by_ref_vs_value.php line 12 */
class c_x : virtual public ObjectData {
  BEGIN_CLASS_MAP(x)
  END_CLASS_MAP(x)
  DECLARE_CLASS(x, X, ObjectData)
  void init();
  public: int64 m_a;
  public: int64 m_b;
  public: int64 m_c;
  public: int64 m_d;
  public: int64 m_e;
  public: int64 m_f;
  public: String m_str1;
  public: String m_str2;
  public: String m_str3;
  public: String m_str4;
  public: String m_str5;
  public: String m_str6;
  public: String m_str11;
  public: String m_str12;
  public: String m_str13;
  public: String m_str14;
  public: String m_str15;
  public: String m_str16;
  public: void t_notused();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_x_h__
