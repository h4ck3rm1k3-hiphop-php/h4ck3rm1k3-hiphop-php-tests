
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_30return_by_array_or_use_ref_params_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_30return_by_array_or_use_ref_params_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f__b1();
void f__b2(Variant v_a, Variant v_b, Variant v_c);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$30return_by_array_or_use_ref_params_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bench1();
void f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_30return_by_array_or_use_ref_params_h__
