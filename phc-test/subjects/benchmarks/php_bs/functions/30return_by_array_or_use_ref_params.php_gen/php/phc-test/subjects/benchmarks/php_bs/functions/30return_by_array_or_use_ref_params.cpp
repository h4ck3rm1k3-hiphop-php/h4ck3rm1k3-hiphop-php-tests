
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.php line 13 */
Variant f__b1() {
  FUNCTION_INJECTION(_b1);
  int64 v_a = 0;
  int64 v_b = 0;
  int64 v_c = 0;

  (v_a = 1LL);
  (v_b = 2LL);
  (v_c = 3LL);
  return Array(ArrayInit(3).set(0, v_a).set(1, v_b).set(2, v_c).create());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.php line 22 */
void f__b2(Variant v_a, Variant v_b, Variant v_c) {
  FUNCTION_INJECTION(_b2);
  (v_a = 1LL);
  (v_b = 2LL);
  (v_c = 3LL);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.php line 32 */
void f_bench1() {
  FUNCTION_INJECTION(bench1);
  Variant v_a;

  (v_a = LINE(35,f__b1()));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.php line 41 */
void f_bench2() {
  FUNCTION_INJECTION(bench2);
  Variant v_a;
  Variant v_b;
  Variant v_c;

  (v_a = "");
  (v_b = "");
  (v_c = "");
  LINE(45,f__b2(ref(v_a), ref(v_b), ref(v_c)));
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$30return_by_array_or_use_ref_params_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$functions$30return_by_array_or_use_ref_params_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  LINE(55,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
