
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$30return_by_array_or_use_ref_params_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_INCLUDE(0x5FC7B3A30E2CCF22LL, "phc-test/subjects/benchmarks/php_bs/functions/30return_by_array_or_use_ref_params.php", php$phc_test$subjects$benchmarks$php_bs$functions$30return_by_array_or_use_ref_params_php);
      break;
    case 4:
      HASH_INCLUDE(0x76F2CC10255E7924LL, "phc-test/subjects/benchmarks/php_bs/init.inc.php", php$phc_test$subjects$benchmarks$php_bs$init_inc_php);
      break;
    case 5:
      HASH_INCLUDE(0x017D1864CA1AAE4DLL, "phc-test/subjects/benchmarks/php_bs/bench.inc.php", php$phc_test$subjects$benchmarks$php_bs$bench_inc_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
