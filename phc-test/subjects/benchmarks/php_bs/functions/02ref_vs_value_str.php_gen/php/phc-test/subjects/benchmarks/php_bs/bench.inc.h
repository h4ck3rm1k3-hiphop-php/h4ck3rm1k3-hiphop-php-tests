
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_bench_inc_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_bench_inc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

double f_getmicrotime();
double f_dobench1(int64 v_ITER);
double f_dobench2(int64 v_ITER);
void f_dobenchmark();
Variant pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(bool incOnce = false, LVariableTable* variables = NULL);
void f__bench1();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_bench_inc_h__
