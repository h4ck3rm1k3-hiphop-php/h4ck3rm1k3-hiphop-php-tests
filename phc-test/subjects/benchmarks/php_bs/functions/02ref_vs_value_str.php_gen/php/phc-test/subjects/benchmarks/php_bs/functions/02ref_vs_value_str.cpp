
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/02ref_vs_value_str.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/02ref_vs_value_str.php line 17 */
void f__b1(Variant v_a) {
  FUNCTION_INJECTION(_b1);
  Variant v_z;

  (v_z = v_a);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/02ref_vs_value_str.php line 22 */
void f__b2(CVarRef v_a) {
  FUNCTION_INJECTION(_b2);
  Variant v_z;

  (v_z = v_a);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/02ref_vs_value_str.php line 28 */
void f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_STR __attribute__((__unused__)) = g->GV(STR);
  LINE(32,f__b1(ref(gv_STR)));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/02ref_vs_value_str.php line 36 */
void f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_STR __attribute__((__unused__)) = g->GV(STR);
  LINE(41,f__b2(gv_STR));
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$02ref_vs_value_str_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/functions/02ref_vs_value_str.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$functions$02ref_vs_value_str_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_STR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("STR") : g->GV(STR);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  (v_STR = LINE(13,x_str_pad("Z", toInt32(10240LL), "Z")));
  LINE(46,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
