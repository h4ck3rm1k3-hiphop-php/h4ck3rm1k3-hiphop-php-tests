
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_short_vs_long_fn_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_short_vs_long_fn_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_a();
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$short_vs_long_fn_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bench1();
void f_bench2();
void f_a_very_long_variable_name_to_be_tested_here_and_there_and_more_than_that();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_short_vs_long_fn_h__
