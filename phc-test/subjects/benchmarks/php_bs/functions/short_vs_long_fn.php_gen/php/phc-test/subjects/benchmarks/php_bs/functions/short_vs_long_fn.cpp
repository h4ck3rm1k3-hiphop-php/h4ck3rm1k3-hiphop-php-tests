
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php line 12 */
void f_a() {
  FUNCTION_INJECTION(a);
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php line 23 */
void f_bench1() {
  FUNCTION_INJECTION(bench1);
  LINE(25,f_a());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php line 29 */
void f_bench2() {
  FUNCTION_INJECTION(bench2);
  LINE(31,f_a_very_long_variable_name_to_be_tested_here_and_there_and_more_than_that());
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php line 16 */
void f_a_very_long_variable_name_to_be_tested_here_and_there_and_more_than_that() {
  FUNCTION_INJECTION(a_very_long_variable_name_to_be_tested_here_and_there_and_more_than_that);
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$short_vs_long_fn_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$functions$short_vs_long_fn_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  LINE(36,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
