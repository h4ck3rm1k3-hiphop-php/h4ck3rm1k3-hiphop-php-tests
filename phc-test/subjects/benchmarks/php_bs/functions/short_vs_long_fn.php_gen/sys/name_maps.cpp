
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "_bench1", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "a", "phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php",
  "a_very_long_variable_name_to_be_tested_here_and_there_and_more_than_that", "phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php",
  "bench1", "phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php",
  "bench2", "phc-test/subjects/benchmarks/php_bs/functions/short_vs_long_fn.php",
  "dobench1", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "dobench2", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "dobenchmark", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  "file_get_contents", "phc-test/subjects/benchmarks/php_bs/init.inc.php",
  "getmicrotime", "phc-test/subjects/benchmarks/php_bs/bench.inc.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
