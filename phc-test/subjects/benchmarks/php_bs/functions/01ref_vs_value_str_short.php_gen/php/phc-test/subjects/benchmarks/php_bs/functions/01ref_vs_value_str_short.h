
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_01ref_vs_value_str_short_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_01ref_vs_value_str_short_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/functions/01ref_vs_value_str_short.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f__b1(Variant v_a);
void f__b2(CVarRef v_a);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$functions$01ref_vs_value_str_short_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bench1();
void f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_functions_01ref_vs_value_str_short_h__
