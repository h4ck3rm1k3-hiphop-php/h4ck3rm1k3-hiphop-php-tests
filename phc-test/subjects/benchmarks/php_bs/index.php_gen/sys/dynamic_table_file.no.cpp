
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$subjects$benchmarks$php_bs$index_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$config_inc_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_INCLUDE(0x42977EF28196134ALL, "phc-test/subjects/benchmarks/php_bs/config.inc.php", php$phc_test$subjects$benchmarks$php_bs$config_inc_php);
      break;
    case 3:
      HASH_INCLUDE(0x5335B1877145F7FBLL, "phc-test/subjects/benchmarks/php_bs/index.php", php$phc_test$subjects$benchmarks$php_bs$index_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
