
#include <php/phc-test/subjects/benchmarks/php_bs/config.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/index.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$index_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/index.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$index_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_rootdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rootdir") : g->GV(rootdir);
  Variant &v_dirlist __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dirlist") : g->GV(dirlist);
  Variant &v_subdirname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("subdirname") : g->GV(subdirname);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_copyr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("copyr") : g->GV(copyr);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);

  LINE(5,x_set_time_limit(toInt32(1200LL)));
  LINE(7,pm_php$phc_test$subjects$benchmarks$php_bs$config_inc_php(true, variables));
  (v_s = "<html><title>PHP Benchmarking Suite</title><body><h3>Benchmarking PHP 5.2.5.hiphop</h3>Any pair of results that are within 10% of each other are probably not statistically significant, \nand the results can be considered approximately equal. All results are in seconds, mostly based on running the code 1000 times.\nFaster is better.</font></p>");
  echo(toString(v_s));
  concat_assign(v_s, "<\?php if (file_exists('../index.php')) echo '<h3><a href=../index.php>&lt;&lt; Back</a></h3>\n';\?>\n");
  (v_rootdir = LINE(18,x_dir(".")));
  (v_dirlist = ScalarArrays::sa_[0]);
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_subdirname = LINE(20,v_rootdir.o_invoke_few_args("read", 0x1F479267E49EF301LL, 0))))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (equal(LINE(21,(assignCallTemp(eo_0, x_strtolower(toString(v_subdirname))),x_strncmp(eo_0, "_php", toInt32(4LL)))), 0LL)) {
          v_dirlist.append((v_subdirname));
          continue;
        }
        if (!same(LINE(25,x_strpos(toString(v_subdirname), ".")), false)) continue;
        if (equal(LINE(26,x_strncmp(toString(v_subdirname), "_", toInt32(1LL))), 0LL)) continue;
        v_dirlist.append((v_subdirname));
      }
    }
  }
  LINE(29,x_asort(ref(v_dirlist)));
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_dirlist.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_d = iter4->second();
      {
        if (equal(LINE(32,(assignCallTemp(eo_0, x_strtolower(toString(v_d))),x_strncmp(eo_0, "_php", toInt32(4LL)))), 0LL)) {
          echo(concat(concat_rev(LINE(33,x_trim(toString(x_str_replace("_", " ", v_d)))), (assignCallTemp(eo_1, x_rawurlencode(toString(v_d))),concat3("<h3><a href=", eo_1, "/index.php>Cached "))), " Results</a></h3>"));
        }
        else {
          echo(concat(concat_rev(LINE(35,x_htmlspecialchars(toString(v_d))), (assignCallTemp(eo_1, x_rawurlencode(toString(v_d))),concat3("<h3><a href=bench.php\?d=", eo_1, ">"))), "</a></h3>\n"));
          concat_assign(v_s, concat(concat_rev(LINE(36,x_htmlspecialchars(toString(v_d))), (assignCallTemp(eo_1, x_rawurlencode(toString(v_d))),concat3("<h3><a href=", eo_1, ".html>"))), "</a></h3>\n"));
        }
      }
    }
  }
  echo(LINE(40,f_copyright()));
  concat_assign(v_s, LINE(42,f_copyright()));
  concat_assign(v_s, LINE(43,concat3("<hr>", toString(v_copyr), " <font size=1><b>Cached version</b>.</font>")));
  (v_f = LINE(45,x_fopen("_cache/index.php", "w")));
  LINE(46,x_fwrite(toObject(v_f), toString(v_s)));
  LINE(47,x_fclose(toObject(v_f)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
