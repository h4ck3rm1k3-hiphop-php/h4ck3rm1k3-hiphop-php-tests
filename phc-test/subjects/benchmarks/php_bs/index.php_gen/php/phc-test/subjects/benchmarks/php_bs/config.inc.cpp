
#include <php/phc-test/subjects/benchmarks/php_bs/config.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/config.inc.php line 22 */
String f_copyright() {
  FUNCTION_INJECTION(Copyright);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_VERSION __attribute__((__unused__)) = g->GV(VERSION);
  return LINE(29,concat3("<font size=1>(c) 2003-2004 John Lim \n\t<a href=http://phplens.com/benchmark_suite/>PHP Benchmark Suite (PHP BS ", toString(gv_VERSION), ")</a>. \n\tSponsored by <a href=http://natsoft.com/>Natsoft</a> and <a href=http://ormestech.com/>Ormes</a>.</font>"));
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$config_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/config.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$config_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_VERSION __attribute__((__unused__)) = (variables != gVariables) ? variables->get("VERSION") : g->GV(VERSION);
  Variant &v_CACHE __attribute__((__unused__)) = (variables != gVariables) ? variables->get("CACHE") : g->GV(CACHE);

  (v_VERSION = "1.01");
  (v_CACHE = 0LL);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
