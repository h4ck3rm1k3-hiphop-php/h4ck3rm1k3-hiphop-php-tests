
#include <php/phc-test/subjects/benchmarks/php_bs/readme.txt.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_CONSTANT = 1LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/readme.txt line 56 */
int64 f_bench1() {
  FUNCTION_INJECTION(bench1);
  return 3LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/readme.txt line 61 */
PlusOperand f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_CONSTANT __attribute__((__unused__)) = g->GV(CONSTANT);
  return gv_CONSTANT + gv_CONSTANT + gv_CONSTANT;
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$readme_txt(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/readme.txt);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$readme_txt;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_CONSTANT __attribute__((__unused__)) = (variables != gVariables) ? variables->get("CONSTANT") : g->GV(CONSTANT);

  echo("John's PHP Benchmarking Suite (PHP BS)\r\n======================================\r\n\r\nThis is a set of benchmark programs for PHP4. All scripts perform 2 benchmarks. A reference\r\nbenchmark, and a 2nd benchmark. All benchmarks are compared with the reference benchmark,\r\nso we can tell that benchmark X is perhaps 3.4 times faster than benchmark Y.\r\n\r\n\r\nInstallation\r\n============\r\nTo run, just unpack in ");
  echo("a web server directory, and open up index.php from your web browser.\r\n\r\nTo save a set of HTML results, turn caching on by going to config.inc.php and change \r\n$CACHE to 1. Then all results that you view are saved in _cache. An index.php is \r\nalso generated. Just rename _cache as appropriate, eg. _PHP420_RH9_P4-2.0GHz.\r\n\r\nResults\r\n=======\r\nAll cached results for PHP 4.0.6 and 4.3.3 have Zend Optimi");
  echo("zer installed.\r\n\r\nCreating Benchmarks\r\n===================\r\n\r\nBenchmarks are sorted by directory. Each benchmark directory is automatically read by \r\nindex.php, and all benchmarks in a directory are executed together.\r\n\r\nEvery benchmark script will perform 2 benchmarks and compare the results of the 2 benchmarks. \r\nThis makes it easier to do relative comparisons. The 1st benchmark is defined in be");
  echo("nch1() and \r\nthe 2nd in bench2().\r\n\r\nBenchmark functions are executed 1000 times by default, and the total time run is measured, \r\nless the time to execute an empty function 1000 times.\r\n\r\nSpecial metadata is stored in the benchmark .php file which is used to annotate the benchmark. \r\nThe metadata is of the form\r\n\r\n//~~ TITLE OF BENCHMARK, TITLE OF BENCH1, TITLE OF BENCH2\r\n\r\n\r\nHere is a simple ben");
  echo("chmark script:\r\n\r\n");
  LINE(44,include("../init.inc.php", true, variables, "phc-test/subjects/benchmarks/php_bs/"));
  ;
  (v_CONSTANT = 1LL);
  LINE(70,include("../bench.inc.php", true, variables, "phc-test/subjects/benchmarks/php_bs/"));
  echo("\r\n\r\n\r\n(c) 2003, 2004 John Lim jlim#natsoft.com.my\r\nLicensed under BSD style license.");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
