
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_readme_txt_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_readme_txt_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/readme.txt.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$readme_txt(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_bench1();
PlusOperand f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_readme_txt_nophp_h__
