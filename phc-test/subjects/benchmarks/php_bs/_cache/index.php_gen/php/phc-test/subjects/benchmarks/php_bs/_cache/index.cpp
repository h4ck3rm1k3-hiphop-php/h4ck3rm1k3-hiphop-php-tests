
#include <php/phc-test/subjects/benchmarks/php_bs/_cache/index.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$_cache$index_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/_cache/index.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$_cache$index_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("<html><title>PHP Benchmarking Suite</title><body><h3>Benchmarking PHP 5.2.3</h3>Any pair of results that are within 10% of each other are probably not statistically significant, \nand the results can be considered approximately equal. All results are in seconds, mostly based on running the code 1000 times.\nFaster is better.</font></p>");
  if (LINE(3,x_file_exists("../index.php"))) echo("<h3><a href=../index.php>&lt;&lt; Back</a></h3>\n");
  echo("<h3><a href=algorithms.html>algorithms</a></h3>\n<h3><a href=exceptions.html>exceptions</a></h3>\n<h3><a href=functions.html>functions</a></h3>\n<h3><a href=loops.html>loops</a></h3>\n<h3><a href=regex.html>regex</a></h3>\n<h3><a href=variables.html>variables</a></h3>\n<h3><a href=xml.html>xml</a></h3>\n<font size=1>(c) 2003-2004 John Lim \n\t<a href=http://phplens.com/benchmark_suite/>PHP Benchmark Suite (PHP BS 1.01)</a>. \n\tSponsored ");
  echo("by <a href=http://natsoft.com/>Natsoft</a> and <a href=http://ormestech.com/>Ormes</a>.</font><hr> <font size=1><b>Cached version</b>.</font>");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
