
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_bench_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_bench_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/bench.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_doscandir(CVarRef v_dir);
void f_testdir(CVarRef v_bencharr, CVarRef v_dest, CVarRef v_swap);
Variant pm_php$phc_test$subjects$benchmarks$php_bs$bench_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_dotestdir(CVarRef v_dir, CVarRef v_bencharr);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_bench_h__
