
#include <php/phc-test/subjects/benchmarks/php_bs/bench.h>
#include <php/phc-test/subjects/benchmarks/php_bs/config.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.php line 15 */
Variant f_doscandir(CVarRef v_dir) {
  FUNCTION_INJECTION(DoScanDir);
  Variant eo_0;
  Variant eo_1;
  Variant v_bencharr;
  Variant v_subdir;
  Variant v_fname;
  Variant v_f;
  Variant v_txt;
  Variant v_txtarr;

  (v_bencharr = ScalarArrays::sa_[0]);
  (v_subdir = LINE(18,x_dir(toString(v_dir))));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_fname = LINE(19,v_subdir.o_invoke_few_args("read", 0x1F479267E49EF301LL, 0))))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (!same(LINE(20,x_strpos(toString(v_fname), "---")), false)) v_bencharr.set(concat3(toString(v_dir), "/", toString(v_fname)), ("<br>"));
        if (same(LINE(21,x_strpos(toString(v_fname), ".php")), false)) continue;
        if (toBoolean(LINE(22,x_strpos(toString(v_fname), "~")))) continue;
        if (equal(LINE(23,x_strncmp(toString(v_fname), "_", toInt32(1LL))), 0LL)) continue;
        (v_fname = LINE(24,concat3(toString(v_dir), "/", toString(v_fname))));
        (v_f = LINE(25,x_fopen(toString(v_fname), "r")));
        if (toBoolean(v_f)) {
          (v_txt = LINE(27,(assignCallTemp(eo_0, toObject(v_f)),assignCallTemp(eo_1, toInt64(x_filesize(toString(v_fname)))),x_fread(eo_0, eo_1))));
          LINE(28,x_fclose(toObject(v_f)));
          if (toBoolean(LINE(29,x_preg_match("!//~~(.*)!", toString(v_txt), ref(v_txtarr))))) {
            v_bencharr.set(v_fname, (LINE(30,(assignCallTemp(eo_1, concat3(toString(v_fname), ",", toString(v_txtarr.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))),x_explode(",", eo_1)))));
          }
        }
      }
    }
  }
  LINE(34,x_ksort(ref(v_bencharr)));
  return v_bencharr;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.php line 38 */
void f_testdir(CVarRef v_bencharr, CVarRef v_dest, CVarRef v_swap) {
  FUNCTION_INJECTION(TestDir);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_CACHE __attribute__((__unused__)) = g->GV(CACHE);
  Variant v_host;
  Variant v_PHP_SELF;
  String v_dir;
  Numeric v_noswap = 0;
  Variant v_bfirst;
  Variant v_arr;
  Variant v_file;
  Variant v_desc;
  Variant v_descleft;
  Variant v_descright;
  int64 v_iter = 0;
  bool v_findhash = false;
  String v_urlbase;
  String v_url;
  Variant v_f;
  Variant v_data;
  Variant v_data1;
  Variant v_pos;
  Array v_r;
  Array v_results;
  String v_ss;

  (v_host = g->gv__SERVER.rvalAt("HTTP_HOST", 0x0635F2C35AA152F1LL));
  (v_PHP_SELF = g->gv__SERVER.rvalAt("PHP_SELF", 0x4C71E2E17E448EB2LL));
  (v_dir = LINE(48,x_dirname(toString(v_PHP_SELF))));
  echo("<a href=index.php>Back</a>");
  (v_noswap = 1LL - v_swap);
  (v_bfirst = toBoolean((toBoolean(v_noswap))) ? (("2")) : (("1")));
  if (empty(gv_CACHE)) {
    echo(concat(" &nbsp; <a href=bench.php\?d=", LINE(55,concat6(toString(v_dest), "&swap=", toString(v_noswap), ">Run bench", toString(v_bfirst), "() first.</a>"))));
  }
  LINE(57,x_flush());
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_bencharr.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_arr = iter4->second();
      {
        if (!(LINE(59,x_is_array(v_arr)))) {
          echo(toString(v_arr));
          continue;
        }
        (v_file = v_arr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
        (v_desc = v_arr.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
        (v_descleft = v_arr.rvalAt(2LL, 0x486AFCC090D5F98CLL));
        (v_descright = v_arr.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL));
        (v_iter = (!(empty(v_arr, 4LL, 0x6F2A25235E544A31LL))) ? (((v_iter = toInt64(LINE(69,x_trim(toString(v_arr.rvalAt(4LL, 0x6F2A25235E544A31LL)))))))) : ((1000LL)));
        (v_findhash = (!(empty(v_arr, 5LL, 0x350AEB726A15D700LL))));
        (v_urlbase = LINE(72,concat5("http://", toString(v_host), v_dir, "/", toString(v_file))));
        (v_url = LINE(73,concat3(v_urlbase, "\?iter=", toString(v_iter))));
        concat_assign(v_url, concat("&swap=", toString(v_swap)));
        (v_f = LINE(76,x_fopen(v_url, "r")));
        (v_data = "");
        if (toBoolean(v_f)) {
          LOOP_COUNTER(5);
          {
            while (toBoolean((v_data1 = LINE(79,x_fread(toObject(v_f), 100000LL))))) {
              LOOP_COUNTER_CHECK(5);
              concat_assign(v_data, toString(v_data1));
            }
          }
          LINE(80,x_fclose(toObject(v_f)));
        }
        if (v_findhash) {
          (v_pos = LINE(84,x_strpos(toString(v_data), "##")));
          (v_data = LINE(85,x_substr(toString(v_data), toInt32(v_pos))));
        }
        if (equal(LINE(88,x_substr(toString(v_data), toInt32(0LL), toInt32(2LL))), "##")) {
          (v_r = LINE(89,x_explode("##", toString(v_data))));
          v_results.append((Array(ArrayInit(2).set(0, v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_r.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
          if (LINE(91,x_is_numeric(v_r.rvalAt(2LL, 0x486AFCC090D5F98CLL)))) {
            if (less(v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL), v_r.rvalAt(2LL, 0x486AFCC090D5F98CLL))) {
              (v_descleft = LINE(93,concat3("<b>", toString(v_descleft), "</b>")));
              if (more(v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL), 0LL)) concat_assign(v_descleft, LINE(94,(assignCallTemp(eo_1, x_sprintf(2, "%2.2f", Array(ArrayInit(1).set(0, divide(toDouble(v_r.rvalAt(2LL, 0x486AFCC090D5F98CLL)), v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()))),concat3(" <font size=1>x", eo_1, " faster</font>"))));
            }
            else if (more(v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL), v_r.rvalAt(2LL, 0x486AFCC090D5F98CLL))) {
              (v_descright = LINE(96,concat3("<b>", toString(v_descright), "</b>")));
              if (more(v_r.rvalAt(2LL, 0x486AFCC090D5F98CLL), 0LL)) concat_assign(v_descright, LINE(97,(assignCallTemp(eo_1, x_sprintf(2, "%2.2f", Array(ArrayInit(1).set(0, divide(toDouble(v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), v_r.rvalAt(2LL, 0x486AFCC090D5F98CLL))).create()))),concat3(" <font size=1>x", eo_1, " faster</font>"))));
            }
          }
        }
        else v_results.append((Array(ArrayInit(3).set(0, false).set(1, false).set(2, v_data).create())));
        (v_ss = empty(gv_CACHE) ? ((LINE(104,concat6(" <a href=\"", v_url, "&view=1\">Source</a> &nbsp; <a href=\"", v_urlbase, "\?iter=1\">Test Once</a>  &nbsp; ", toString(v_desc))))) : ((concat(" &nbsp; ", toString(v_desc)))));
        {
          echo("<table border=1 width=90%><tr><td colspan=4 bgcolor=#EEEEEE>");
          echo(v_ss);
        }
        {
          LOOP_COUNTER(6);
          for (ArrayIter iter8 = v_results.begin(); !iter8.end(); ++iter8) {
            LOOP_COUNTER_CHECK(6);
            v_arr = iter8.second();
            {
              if (equal(LINE(111,x_sizeof(v_arr)), 3LL)) {
                {
                  echo("<tr><td>");
                  echo(concat(toString(v_descleft), "-"));
                  echo(toString(v_descright));
                  echo("<td colspan=3>");
                  echo(toString(v_arr.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
                }
              }
              else {
                {
                  echo("<tr><td width=35%>");
                  echo(toString(v_descleft));
                  echo("<td width=15%>");
                  echo(toString(v_arr.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
                  echo("<td width=35%>");
                  echo(toString(v_descright));
                  echo("<td width=15%>");
                  echo(toString(v_arr.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
                }
              }
            }
          }
        }
        echo("</table>\n\n");
        LINE(118,x_flush());
        (v_results = ScalarArrays::sa_[0]);
      }
    }
  }
  echo(LINE(122,(assignCallTemp(eo_1, x_date("d-M-Y H:i:s")),concat3("<p><font size=2>Testing PHP 5.2.5.hiphop Completed on Linux. Benchmark done on: ", eo_1, "</font></p></html>"))));
  echo(concat("<hr>", LINE(123,f_copyright())));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.php line 127 */
void f_dotestdir(CVarRef v_dir, CVarRef v_bencharr) {
  FUNCTION_INJECTION(DoTestDir);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_CACHE __attribute__((__unused__)) = g->GV(CACHE);
  Variant &gv_PHP_SELF __attribute__((__unused__)) = g->GV(PHP_SELF);
  Variant v_ok;
  String v_savef;
  String v_t;
  Variant v_f;

  {
  }
  (v_ok = (silenceInc(), silenceDec(LINE(131,x_mkdir("_cache")))));
  if (!(LINE(132,x_file_exists("_cache")))) f_exit("Cannot create _cache directory");
  (v_savef = LINE(133,concat3("_cache/", toString(v_dir), ".html")));
  if (LINE(134,x_file_exists(v_savef)) && !(empty(gv_CACHE)) && (less(gv_CACHE, 0LL) || less(x_abs(minus_rev(x_filemtime(v_savef), x_time())), gv_CACHE))) {
    LINE(135,x_readfile(v_savef));
    echo(LINE(136,concat5("<hr><font size=1>This is a cached result. Results cached for ", toString(gv_CACHE), " seconds. To disable caching, set $CACHE=0 in ", toString(gv_PHP_SELF), "</font>")));
  }
  else {
    LINE(138,x_ob_start());
    LINE(139,f_testdir(v_bencharr, v_dir, empty(g->gv__GET, "swap", 0x5B27177E198E8E01LL) ? (("0")) : (("1"))));
    (v_t = LINE(140,x_ob_get_contents()));
    LINE(141,x_ob_end_flush());
    (v_f = LINE(142,x_fopen(v_savef, "w")));
    LINE(143,x_fwrite(toObject(v_f), v_t));
    LINE(144,x_fclose(toObject(v_f)));
  }
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$bench_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/bench.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$bench_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dir") : g->GV(dir);
  Variant &v_dirpath __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dirpath") : g->GV(dirpath);
  Variant &v_bencharr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bencharr") : g->GV(bencharr);

  LINE(13,pm_php$phc_test$subjects$benchmarks$php_bs$config_inc_php(true, variables));
  (v_dir = isset(g->gv__GET, "d", 0x7A452383AA9BF7C4LL) ? ((Variant)(g->gv__GET.rvalAt("d", 0x7A452383AA9BF7C4LL))) : ((Variant)("")));
  if (empty(v_dir)) f_exit("No dir defined");
  if (!same(LINE(157,x_strpos(toString(v_dir), "..")), false) || !same(x_strpos(toString(v_dir), "/"), false) || !same(x_strpos(toString(v_dir), "\\"), false)) f_exit("Safety check violation 1");
  (v_dirpath = LINE(158,x_basename(toString(v_dir))));
  (v_bencharr = LINE(160,f_doscandir(v_dirpath)));
  LINE(162,f_dotestdir(v_dir, v_bencharr));
  f_exit();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
