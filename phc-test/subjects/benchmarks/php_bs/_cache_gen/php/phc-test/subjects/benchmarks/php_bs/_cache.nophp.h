
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs__cache_nophp_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs__cache_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/_cache.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$_cache(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs__cache_nophp_h__
