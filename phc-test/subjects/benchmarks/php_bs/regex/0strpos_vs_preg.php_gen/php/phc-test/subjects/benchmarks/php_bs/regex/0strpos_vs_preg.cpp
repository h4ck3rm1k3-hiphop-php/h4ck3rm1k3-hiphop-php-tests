
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/regex/0strpos_vs_preg.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/regex/0strpos_vs_preg.php line 15 */
void f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_STR __attribute__((__unused__)) = g->GV(STR);
  LINE(19,x_strpos(toString(gv_STR), "z"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/regex/0strpos_vs_preg.php line 23 */
void f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_STR __attribute__((__unused__)) = g->GV(STR);
  LINE(26,x_preg_match("/z/", toString(gv_STR)));
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$regex$0strpos_vs_preg_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/regex/0strpos_vs_preg.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$regex$0strpos_vs_preg_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_STR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("STR") : g->GV(STR);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  (v_STR = "The elemental items magic roses happy systems happy systemsz");
  LINE(31,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
