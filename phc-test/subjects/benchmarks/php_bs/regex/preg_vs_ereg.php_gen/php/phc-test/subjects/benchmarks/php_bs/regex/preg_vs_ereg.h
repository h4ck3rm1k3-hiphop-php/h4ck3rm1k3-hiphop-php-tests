
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_regex_preg_vs_ereg_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_regex_preg_vs_ereg_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/regex/preg_vs_ereg.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_bench1();
void f_bench2();
Variant pm_php$phc_test$subjects$benchmarks$php_bs$regex$preg_vs_ereg_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_regex_preg_vs_ereg_h__
