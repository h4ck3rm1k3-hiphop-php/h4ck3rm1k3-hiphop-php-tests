
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_short_long_var_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_short_long_var_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/variables/short_long_var.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$variables$short_long_var_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bench1();
void f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_short_long_var_h__
