
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_oper_str2_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_oper_str2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/variables/oper_str2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$variables$oper_str2_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_bench1();
void f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_oper_str2_h__
