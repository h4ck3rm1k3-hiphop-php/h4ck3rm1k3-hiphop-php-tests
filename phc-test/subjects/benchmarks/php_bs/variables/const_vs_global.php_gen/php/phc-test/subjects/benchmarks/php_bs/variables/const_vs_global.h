
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_const_vs_global_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_const_vs_global_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/variables/const_vs_global.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$variables$const_vs_global_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_bench1();
PlusOperand f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_variables_const_vs_global_h__
