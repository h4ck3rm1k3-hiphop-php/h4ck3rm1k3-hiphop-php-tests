
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/variables/const_vs_global.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_CONSTANT = 1LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/variables/const_vs_global.php line 19 */
int64 f_bench1() {
  FUNCTION_INJECTION(bench1);
  return 3LL;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/variables/const_vs_global.php line 24 */
PlusOperand f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_CONSTANT __attribute__((__unused__)) = g->GV(CONSTANT);
  return gv_CONSTANT + gv_CONSTANT + gv_CONSTANT;
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$variables$const_vs_global_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/variables/const_vs_global.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$variables$const_vs_global_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_CONSTANT __attribute__((__unused__)) = (variables != gVariables) ? variables->get("CONSTANT") : g->GV(CONSTANT);

  g->declareFunction("bench2");
  LINE(7,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  ;
  (v_CONSTANT = 1LL);
  LINE(33,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
