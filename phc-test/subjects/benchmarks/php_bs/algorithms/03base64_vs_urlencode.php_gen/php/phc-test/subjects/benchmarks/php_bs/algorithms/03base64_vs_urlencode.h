
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_03base64_vs_urlencode_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_03base64_vs_urlencode_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/03base64_vs_urlencode.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$03base64_vs_urlencode_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_bench1();
String f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_03base64_vs_urlencode_h__
