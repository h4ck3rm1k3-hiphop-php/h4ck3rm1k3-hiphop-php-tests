
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/01base64_vs_utf8.h>
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/01base64_vs_utf8.php line 22 */
String f_bench1() {
  FUNCTION_INJECTION(bench1);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DATA __attribute__((__unused__)) = g->GV(DATA);
  String v_d;

  (v_d = LINE(26,x_base64_encode(toString(gv_DATA))));
  return v_d;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/01base64_vs_utf8.php line 30 */
String f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DATA __attribute__((__unused__)) = g->GV(DATA);
  String v_d;

  (v_d = LINE(34,x_utf8_encode(toString(gv_DATA))));
  return v_d;
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$01base64_vs_utf8_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/algorithms/01base64_vs_utf8.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$01base64_vs_utf8_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_DATA __attribute__((__unused__)) = (variables != gVariables) ? variables->get("DATA") : g->GV(DATA);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  (v_f = LINE(15,x_fopen("pride.txt", "rb")));
  (v_DATA = LINE(16,x_fread(toObject(v_f), 1000000LL)));
  LINE(17,x_fclose(toObject(v_f)));
  LINE(41,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
