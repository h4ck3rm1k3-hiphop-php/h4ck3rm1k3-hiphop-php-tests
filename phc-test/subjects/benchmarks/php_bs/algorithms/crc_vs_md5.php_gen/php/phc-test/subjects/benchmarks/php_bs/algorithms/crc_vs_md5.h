
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_crc_vs_md5_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_crc_vs_md5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/crc_vs_md5.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_bench1();
void f_bench2();
Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$crc_vs_md5_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_crc_vs_md5_h__
