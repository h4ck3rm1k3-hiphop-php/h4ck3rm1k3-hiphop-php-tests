
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/crc_vs_md5.h>
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_CONSTANT = 1LL;

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/crc_vs_md5.php line 18 */
void f_bench1() {
  FUNCTION_INJECTION(bench1);
  LINE(20,x_crc32("The quality of mercy is not strained 1234567890"));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/crc_vs_md5.php line 23 */
void f_bench2() {
  FUNCTION_INJECTION(bench2);
  LINE(25,x_md5("The quality of mercy is not strained 1234567890"));
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$crc_vs_md5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/algorithms/crc_vs_md5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$crc_vs_md5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_CONSTANT __attribute__((__unused__)) = (variables != gVariables) ? variables->get("CONSTANT") : g->GV(CONSTANT);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  ;
  (v_CONSTANT = 1LL);
  LINE(30,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
