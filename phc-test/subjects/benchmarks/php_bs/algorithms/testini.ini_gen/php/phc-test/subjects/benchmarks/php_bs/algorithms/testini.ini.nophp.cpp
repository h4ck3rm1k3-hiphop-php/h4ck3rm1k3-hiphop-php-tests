
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/testini.ini.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$testini_ini(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/algorithms/testini.ini);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$testini_ini;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("abc=a123\nbbc=a123\ncbc=a123\ndbc=a123\nebc=a123\nfbc=a123\nadc=a123\nbdc=a123\ncdc=a123\nddc=a123\nedc=a123\nfdc=a123\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
