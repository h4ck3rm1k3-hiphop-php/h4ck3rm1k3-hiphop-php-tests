
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/fopen_vs_file_get_contents.h>
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/fopen_vs_file_get_contents.php line 19 */
Variant f_bench1() {
  FUNCTION_INJECTION(bench1);
  Variant eo_0;
  Variant eo_1;
  String v_fname;
  Variant v_f;
  Variant v_DATA;

  (v_fname = "03heart.jpg");
  (v_f = LINE(22,x_fopen(v_fname, "rb")));
  (v_DATA = LINE(23,(assignCallTemp(eo_0, toObject(v_f)),assignCallTemp(eo_1, toInt64(x_filesize(v_fname))),x_fread(eo_0, eo_1))));
  LINE(24,x_fclose(toObject(v_f)));
  return v_DATA;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/fopen_vs_file_get_contents.php line 29 */
Variant f_bench2() {
  FUNCTION_INJECTION(bench2);
  String v_fname;
  Variant v_DATA;

  (v_fname = "03heart.jpg");
  (v_DATA = LINE(32,x_file_get_contents(v_fname)));
  return v_DATA;
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$fopen_vs_file_get_contents_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/algorithms/fopen_vs_file_get_contents.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$fopen_vs_file_get_contents_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  LINE(38,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
