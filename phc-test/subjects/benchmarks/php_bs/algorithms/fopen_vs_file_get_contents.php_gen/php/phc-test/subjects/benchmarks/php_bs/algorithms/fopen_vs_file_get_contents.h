
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_fopen_vs_file_get_contents_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_fopen_vs_file_get_contents_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/fopen_vs_file_get_contents.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$fopen_vs_file_get_contents_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_bench1();
Variant f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_fopen_vs_file_get_contents_h__
