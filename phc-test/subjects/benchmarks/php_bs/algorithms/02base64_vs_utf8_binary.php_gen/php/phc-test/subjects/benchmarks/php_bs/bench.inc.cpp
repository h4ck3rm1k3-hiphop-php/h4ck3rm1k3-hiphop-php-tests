
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/02base64_vs_utf8_binary.h>
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.inc.php line 9 */
double f_getmicrotime() {
  FUNCTION_INJECTION(getmicrotime);
  Variant v_t;

  (v_t = LINE(11,x_microtime()));
  (v_t = LINE(12,x_explode(" ", toString(v_t))));
  return toDouble(v_t.rvalAt(1LL, 0x5BCA7C69B794F8CELL)) + toDouble(v_t.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.inc.php line 20 */
double f_dobench1(int64 v_ITER) {
  FUNCTION_INJECTION(dobench1);
  double v_t1 = 0.0;
  int64 v_i = 0;

  LINE(22,f_bench1());
  f_bench1();
  f_bench1();
  (v_t1 = LINE(23,f_getmicrotime()));
  {
    LOOP_COUNTER(1);
    for ((v_i = v_ITER); not_less(--v_i, 0LL); ) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(25,f_bench1());
      }
    }
  }
  return LINE(27,f_getmicrotime()) - v_t1;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.inc.php line 30 */
double f_dobench2(int64 v_ITER) {
  FUNCTION_INJECTION(dobench2);
  double v_t1 = 0.0;
  int64 v_i = 0;

  LINE(32,f_bench2());
  f_bench2();
  f_bench2();
  (v_t1 = LINE(33,f_getmicrotime()));
  {
    LOOP_COUNTER(2);
    for ((v_i = v_ITER); not_less(--v_i, 0LL); ) {
      LOOP_COUNTER_CHECK(2);
      {
        LINE(35,f_bench2());
      }
    }
  }
  return LINE(37,f_getmicrotime()) - v_t1;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.inc.php line 40 */
void f_dobenchmark() {
  FUNCTION_INJECTION(DoBenchmark);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  int64 v_ITER = 0;
  bool v_SWAP = false;
  double v_t1 = 0.0;
  int64 v_i = 0;
  double v_overhead = 0.0;
  double v_time2 = 0.0;
  double v_time1 = 0.0;

  (v_ITER = (isset(g->gv__GET, "iter", 0x4AD5071F49906E5FLL)) ? ((toInt64(g->gv__GET.rvalAt("iter", 0x4AD5071F49906E5FLL)))) : ((1000LL)));
  (v_SWAP = LINE(47,f_function_exists("bench2")) && !(empty(g->gv__GET, "swap", 0x5B27177E198E8E01LL)));
  if (more(v_ITER, 10000LL)) f_exit("DOS attack\?");
  LINE(50,x_flush());
  (v_t1 = LINE(52,f_getmicrotime()));
  {
    LOOP_COUNTER(3);
    for ((v_i = v_ITER); not_less(--v_i, 0LL); ) {
      LOOP_COUNTER_CHECK(3);
      {
        LINE(54,f__bench1());
      }
    }
  }
  (v_overhead = LINE(56,f_getmicrotime()) - v_t1);
  if (v_SWAP) (v_time2 = LINE(60,f_dobench2(v_ITER)) - v_overhead);
  else (v_time1 = LINE(61,f_dobench1(v_ITER)) - v_overhead);
  if (LINE(63,f_function_exists("bench2"))) {
    if (v_SWAP) (v_time1 = LINE(64,f_dobench1(v_ITER)) - v_overhead);
    else (v_time2 = LINE(65,f_dobench2(v_ITER)) - v_overhead);
    LINE(67,(assignCallTemp(eo_1, x_abs(v_time1) * (divide(1000.0, v_ITER))),assignCallTemp(eo_2, x_abs(v_time2) * (divide(1000.0, v_ITER))),x_printf(3, "##%8.4f##%8.4f##", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  }
  else {
    LINE(69,x_printf(2, "##%8.4f##", Array(ArrayInit(1).set(0, v_time1).create())));
  }
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/bench.inc.php line 16 */
void f__bench1() {
  FUNCTION_INJECTION(_bench1);
} /* function */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/bench.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_NOTEST __attribute__((__unused__)) = (variables != gVariables) ? variables->get("NOTEST") : g->GV(NOTEST);
  Variant &v__RET1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("_RET1") : g->GV(_RET1);

  LINE(6,x_error_reporting(toInt32(6143LL) /* E_ALL */));
  LINE(7,x_set_time_limit(toInt32(300LL)));
  if (empty(v_NOTEST)) {
    LINE(75,f_dobenchmark());
  }
  if (!(isset(g->gv__GET, "iter", 0x4AD5071F49906E5FLL)) || equal(g->gv__GET.rvalAt("iter", 0x4AD5071F49906E5FLL), 1LL)) {
    echo("<pre>");
    echo("<h3>Bench1</h3>");
    (v__RET1 = LINE(83,f_bench1()));
    LINE(84,x_print_r(v__RET1));
    if (LINE(85,x_is_string(v__RET1))) echo((assignCallTemp(eo_1, toString(x_strlen(toString(v__RET1)))),concat3("<br>strlen=", eo_1, "<br>")));
    echo("<h3>Bench2</h3>");
    (v__RET1 = LINE(87,f_bench2()));
    LINE(88,x_print_r(v__RET1));
    if (LINE(89,x_is_string(v__RET1))) echo((assignCallTemp(eo_1, toString(x_strlen(toString(v__RET1)))),concat3("<br>strlen=", eo_1, "<br>")));
    echo(LINE(91,(assignCallTemp(eo_1, toString(x_memory_get_usage())),concat3("<p>memory usage=", eo_1, "</p>"))));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
