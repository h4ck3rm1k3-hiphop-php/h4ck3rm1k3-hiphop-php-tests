
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/init.inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_txt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("txt") : g->GV(txt);

  if (isset(g->gv__GET, "view", 0x05661D6428543CDELL)) {
    (v_file = LINE(8,x_basename(toString(g->gv__SERVER.rvalAt("PHP_SELF", 0x4C71E2E17E448EB2LL)))));
    (v_f = LINE(9,x_fopen(toString(v_file), "r")));
    if (toBoolean(v_f)) {
      (v_txt = LINE(11,(assignCallTemp(eo_0, toObject(v_f)),assignCallTemp(eo_1, toInt64(x_filesize(toString(v_file)))),x_fread(eo_0, eo_1))));
      LINE(12,x_highlight_string(toString(v_txt)));
      LINE(13,x_fclose(toObject(v_f)));
    }
    f_exit();
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
