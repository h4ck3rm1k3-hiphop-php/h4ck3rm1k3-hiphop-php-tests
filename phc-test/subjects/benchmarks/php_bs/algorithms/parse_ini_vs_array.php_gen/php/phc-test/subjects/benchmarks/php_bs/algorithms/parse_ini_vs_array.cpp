
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/parse_ini_vs_array.h>
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/testini.h>
#include <php/phc-test/subjects/benchmarks/php_bs/bench.inc.h>
#include <php/phc-test/subjects/benchmarks/php_bs/init.inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/parse_ini_vs_array.php line 22 */
Variant f_bench1() {
  FUNCTION_INJECTION(bench1);
  Variant v_options;

  (v_options = LINE(24,x_parse_ini_file("testini.ini")));
  return v_options;
} /* function */
/* SRC: phc-test/subjects/benchmarks/php_bs/algorithms/parse_ini_vs_array.php line 28 */
Variant f_bench2() {
  FUNCTION_INJECTION(bench2);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_options;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_options;
    VariableTable(Variant &r_options) : v_options(r_options) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 0:
          HASH_RETURN(0x7C17922060DCA1EALL, v_options,
                      options);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_options);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  LINE(30,pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$testini_php(false, variables));
  return v_options;
} /* function */
Variant i_bench2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x40608FE1740EF5C7LL, bench2) {
    return (f_bench2());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$parse_ini_vs_array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/benchmarks/php_bs/algorithms/parse_ini_vs_array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$parse_ini_vs_array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_DATA __attribute__((__unused__)) = (variables != gVariables) ? variables->get("DATA") : g->GV(DATA);

  g->declareFunction("bench2");
  LINE(6,pm_php$phc_test$subjects$benchmarks$php_bs$init_inc_php(true, variables));
  (v_f = LINE(15,x_fopen("pride.txt", "rb")));
  (v_DATA = LINE(16,x_fread(toObject(v_f), 1000000LL)));
  LINE(17,x_fclose(toObject(v_f)));
  LINE(37,pm_php$phc_test$subjects$benchmarks$php_bs$bench_inc_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
