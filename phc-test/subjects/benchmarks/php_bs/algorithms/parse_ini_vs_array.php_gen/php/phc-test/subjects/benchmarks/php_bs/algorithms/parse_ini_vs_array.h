
#ifndef __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_parse_ini_vs_array_h__
#define __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_parse_ini_vs_array_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/benchmarks/php_bs/algorithms/parse_ini_vs_array.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$benchmarks$php_bs$algorithms$parse_ini_vs_array_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_bench1();
Variant f_bench2();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_benchmarks_php_bs_algorithms_parse_ini_vs_array_h__
