
#ifndef __GENERATED_php_phc_test_subjects_warnings_missing_include_h__
#define __GENERATED_php_phc_test_subjects_warnings_missing_include_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/warnings/missing_include.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$warnings$missing_include_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_warnings_missing_include_h__
