
#ifndef __GENERATED_php_phc_test_subjects_warnings_constant_folding2_h__
#define __GENERATED_php_phc_test_subjects_warnings_constant_folding2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/subjects/warnings/constant_folding2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$subjects$warnings$constant_folding2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_subjects_warnings_constant_folding2_h__
