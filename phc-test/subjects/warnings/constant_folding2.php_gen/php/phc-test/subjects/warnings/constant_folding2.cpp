
#include <php/phc-test/subjects/warnings/constant_folding2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$subjects$warnings$constant_folding2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/subjects/warnings/constant_folding2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$subjects$warnings$constant_folding2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_var_dump(1, divide(0LL, toByte(false))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
