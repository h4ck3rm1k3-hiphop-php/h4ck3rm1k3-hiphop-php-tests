
#include <php/phc-test/framework/generate_c.h>
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/generate_c.php line 14 */
Variant c_generate_c::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_generate_c::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_generate_c::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_generate_c::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_generate_c::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_generate_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_generate_c::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_generate_c::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(generate_c)
ObjectData *c_generate_c::cloneImpl() {
  c_generate_c *obj = NEW(c_generate_c)();
  cloneSet(obj);
  return obj;
}
void c_generate_c::cloneSet(c_generate_c *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_generate_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_generate_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_generate_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_generate_c$os_get(const char *s) {
  return c_generate_c::os_get(s, -1);
}
Variant &cw_generate_c$os_lval(const char *s) {
  return c_generate_c::os_lval(s, -1);
}
Variant cw_generate_c$os_constant(const char *s) {
  return c_generate_c::os_constant(s);
}
Variant cw_generate_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_generate_c::os_invoke(c, s, params, -1, fatal);
}
void c_generate_c::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/generate_c.php line 16 */
Array c_generate_c::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(Generate_C, Generate_C::get_dependent_test_names);
  return ScalarArrays::sa_[4];
} /* function */
/* SRC: phc-test/framework/generate_c.php line 21 */
Variant c_generate_c::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(Generate_C, Generate_C::get_test_subjects);
  return LINE(23,f_get_interpretable_scripts());
} /* function */
/* SRC: phc-test/framework/generate_c.php line 26 */
void c_generate_c::t_run_test(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(Generate_C, Generate_C::run_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  p_asyncbundle v_bundle;

  ((Object)((v_bundle = ((Object)(LINE(29,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (concat(toString(LINE(31,invoke_failed("get_phc_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000081EE2BC9LL))), " --generate-c")), 0x77CFA1EEF01BCA90LL);
  (v_bundle.o_lval("final", 0x5192930B2145036ELL) = "finish");
  LINE(34,v_bundle->t_start());
} /* function */
/* SRC: phc-test/framework/generate_c.php line 37 */
void c_generate_c::t_finish(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(Generate_C, Generate_C::finish);
  if (toBoolean(toObject(v_bundle).o_get("errs", 0x15E266045DABA002LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) LINE(40,t_async_failure("Stderr not clear", v_bundle));
  else if (toBoolean(toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) LINE(42,t_async_failure("exit code not clear", v_bundle));
  else if (less(LINE(43,x_strlen(toString(toObject(v_bundle).o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), 1428LL)) LINE(44,t_async_failure("output is too short", v_bundle));
  else LINE(46,t_async_success(v_bundle));
} /* function */
Object co_generate_c(CArrRef params, bool init /* = true */) {
  return Object(p_generate_c(NEW(c_generate_c)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$generate_c_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/generate_c.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$generate_c_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(11,pm_php$phc_test$framework$lib$async_test_php(true, variables));
  LINE(13,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_generate_c(p_generate_c(NEWOBJ(c_generate_c)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
