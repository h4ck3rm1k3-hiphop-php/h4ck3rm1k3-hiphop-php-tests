
#ifndef __GENERATED_cls_asynctest_h__
#define __GENERATED_cls_asynctest_h__

#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/async_test.php line 141 */
class c_asynctest : virtual public c_test {
  BEGIN_CLASS_MAP(asynctest)
    PARENT_CLASS(test)
  END_CLASS_MAP(asynctest)
  DECLARE_CLASS(asynctest, AsyncTest, test)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_reduce_run_function(Variant v_command, Variant v_stdin);
  public: Variant t_reduce_checking_function(CVarRef v_program);
  public: void t_reduce_debug_function(CVarRef v_level, CVarRef v_message);
  public: void t_reduce_failure(CStrRef v_reason, CVarRef v_bundle);
  public: void t_reduce_success(CVarRef v_bundle);
  public: void t_reduce_timeout(CVarRef v_reason, CVarRef v_bundle);
  public: void t_async_failure(CStrRef v_reason, CVarRef v_bundle);
  public: void t_async_timeout(CStrRef v_reason, CVarRef v_bundle);
  public: void t_async_success(CVarRef v_bundle);
  public: Variant t_fail_on_output(Variant v_stream, CVarRef v_bundle);
  public: void t_start_program(CVarRef v_bundle);
  public: void t_start_new_program(CVarRef v_bundle);
  public: void t_finish_test();
  public: void t_run_out_all_procs();
  public: void t_check_capacity();
  public: void t_check_running_procs();
  public: void t_run_waiting_procs();
  public: void t_run_program(Variant v_bundle);
  public: void t_two_command_finish(Object v_async);
  public: int64 t_get_phc_num_procs_divisor();
  public: Variant t_get_num_procs();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_asynctest_h__
