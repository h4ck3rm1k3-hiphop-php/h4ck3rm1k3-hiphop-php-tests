
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "annotated_test", "phc-test/framework/annotated_test.php",
  "annotation_translator", "phc-test/framework/annotated_test.php",
  "asyncbundle", "phc-test/framework/lib/async_test.php",
  "asynctest", "phc-test/framework/lib/async_test.php",
  "phc_exit_code_annotation", "phc-test/framework/annotated_test.php",
  "phc_option_annotation", "phc-test/framework/annotated_test.php",
  "phc_output_annotation", "phc-test/framework/annotated_test.php",
  "test", "phc-test/framework/lib/test.php",
  "test_annotation", "phc-test/framework/annotated_test.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "create_label_struct", "phc-test/framework/lib/labels.php",
  "get_all_plugins", "phc-test/framework/lib/labels.php",
  "get_all_scripts", "phc-test/framework/lib/labels.php",
  "get_all_scripts_in_dir", "phc-test/framework/lib/labels.php",
  "get_available_annotations", "phc-test/framework/annotated_test.php",
  "get_interpretable_scripts", "phc-test/framework/lib/labels.php",
  "get_non_interpretable_scripts", "phc-test/framework/lib/labels.php",
  "get_scripts_labelled", "phc-test/framework/lib/labels.php",
  "inst", "phc-test/framework/lib/async_test.php",
  "is_labelled", "phc-test/framework/lib/labels.php",
  "parse_options", "phc-test/framework/annotated_test.php",
  "process_label_file_line", "phc-test/framework/lib/labels.php",
  "skip_3rdparty", "phc-test/framework/lib/labels.php",
  "strip_long_files", "phc-test/framework/lib/labels.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
