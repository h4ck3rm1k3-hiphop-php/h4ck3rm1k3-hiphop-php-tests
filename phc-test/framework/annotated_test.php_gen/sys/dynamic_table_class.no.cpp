
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_asyncbundle(CArrRef params, bool init = true);
Variant cw_asyncbundle$os_get(const char *s);
Variant &cw_asyncbundle$os_lval(const char *s);
Variant cw_asyncbundle$os_constant(const char *s);
Variant cw_asyncbundle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_asynctest(CArrRef params, bool init = true);
Variant cw_asynctest$os_get(const char *s);
Variant &cw_asynctest$os_lval(const char *s);
Variant cw_asynctest$os_constant(const char *s);
Variant cw_asynctest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_phc_output_annotation(CArrRef params, bool init = true);
Variant cw_phc_output_annotation$os_get(const char *s);
Variant &cw_phc_output_annotation$os_lval(const char *s);
Variant cw_phc_output_annotation$os_constant(const char *s);
Variant cw_phc_output_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_annotated_test(CArrRef params, bool init = true);
Variant cw_annotated_test$os_get(const char *s);
Variant &cw_annotated_test$os_lval(const char *s);
Variant cw_annotated_test$os_constant(const char *s);
Variant cw_annotated_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_phc_exit_code_annotation(CArrRef params, bool init = true);
Variant cw_phc_exit_code_annotation$os_get(const char *s);
Variant &cw_phc_exit_code_annotation$os_lval(const char *s);
Variant cw_phc_exit_code_annotation$os_constant(const char *s);
Variant cw_phc_exit_code_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test_annotation(CArrRef params, bool init = true);
Variant cw_test_annotation$os_get(const char *s);
Variant &cw_test_annotation$os_lval(const char *s);
Variant cw_test_annotation$os_constant(const char *s);
Variant cw_test_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_annotation_translator(CArrRef params, bool init = true);
Variant cw_annotation_translator$os_get(const char *s);
Variant &cw_annotation_translator$os_lval(const char *s);
Variant cw_annotation_translator$os_constant(const char *s);
Variant cw_annotation_translator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_phc_option_annotation(CArrRef params, bool init = true);
Variant cw_phc_option_annotation$os_get(const char *s);
Variant &cw_phc_option_annotation$os_lval(const char *s);
Variant cw_phc_option_annotation$os_constant(const char *s);
Variant cw_phc_option_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test(CArrRef params, bool init = true);
Variant cw_test$os_get(const char *s);
Variant &cw_test$os_lval(const char *s);
Variant cw_test$os_constant(const char *s);
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 7:
      HASH_CREATE_OBJECT(0x09E7723E191A77E7LL, test_annotation);
      HASH_CREATE_OBJECT(0x37349B25A0ED29E7LL, test);
      break;
    case 8:
      HASH_CREATE_OBJECT(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      HASH_CREATE_OBJECT(0x7938B508681D0769LL, annotation_translator);
      break;
    case 14:
      HASH_CREATE_OBJECT(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 22:
      HASH_CREATE_OBJECT(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 24:
      HASH_CREATE_OBJECT(0x545A9E1B04FF1438LL, asyncbundle);
      break;
    case 30:
      HASH_CREATE_OBJECT(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x09E7723E191A77E7LL, test_annotation);
      HASH_INVOKE_STATIC_METHOD(0x37349B25A0ED29E7LL, test);
      break;
    case 8:
      HASH_INVOKE_STATIC_METHOD(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      HASH_INVOKE_STATIC_METHOD(0x7938B508681D0769LL, annotation_translator);
      break;
    case 14:
      HASH_INVOKE_STATIC_METHOD(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 22:
      HASH_INVOKE_STATIC_METHOD(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 24:
      HASH_INVOKE_STATIC_METHOD(0x545A9E1B04FF1438LL, asyncbundle);
      break;
    case 30:
      HASH_INVOKE_STATIC_METHOD(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 7:
      HASH_GET_STATIC_PROPERTY(0x09E7723E191A77E7LL, test_annotation);
      HASH_GET_STATIC_PROPERTY(0x37349B25A0ED29E7LL, test);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      HASH_GET_STATIC_PROPERTY(0x7938B508681D0769LL, annotation_translator);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 22:
      HASH_GET_STATIC_PROPERTY(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 24:
      HASH_GET_STATIC_PROPERTY(0x545A9E1B04FF1438LL, asyncbundle);
      break;
    case 30:
      HASH_GET_STATIC_PROPERTY(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x09E7723E191A77E7LL, test_annotation);
      HASH_GET_STATIC_PROPERTY_LV(0x37349B25A0ED29E7LL, test);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY_LV(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      HASH_GET_STATIC_PROPERTY_LV(0x7938B508681D0769LL, annotation_translator);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY_LV(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 22:
      HASH_GET_STATIC_PROPERTY_LV(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 24:
      HASH_GET_STATIC_PROPERTY_LV(0x545A9E1B04FF1438LL, asyncbundle);
      break;
    case 30:
      HASH_GET_STATIC_PROPERTY_LV(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 31) {
    case 7:
      HASH_GET_CLASS_CONSTANT(0x09E7723E191A77E7LL, test_annotation);
      HASH_GET_CLASS_CONSTANT(0x37349B25A0ED29E7LL, test);
      break;
    case 8:
      HASH_GET_CLASS_CONSTANT(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      HASH_GET_CLASS_CONSTANT(0x7938B508681D0769LL, annotation_translator);
      break;
    case 14:
      HASH_GET_CLASS_CONSTANT(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 22:
      HASH_GET_CLASS_CONSTANT(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 24:
      HASH_GET_CLASS_CONSTANT(0x545A9E1B04FF1438LL, asyncbundle);
      break;
    case 30:
      HASH_GET_CLASS_CONSTANT(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
