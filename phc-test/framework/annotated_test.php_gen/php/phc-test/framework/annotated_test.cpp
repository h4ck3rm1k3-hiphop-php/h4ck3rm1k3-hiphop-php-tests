
#include <php/phc-test/framework/annotated_test.h>
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/annotated_test.php line 102 */
Variant c_phc_output_annotation::os_get(const char *s, int64 hash) {
  return c_test_annotation::os_get(s, hash);
}
Variant &c_phc_output_annotation::os_lval(const char *s, int64 hash) {
  return c_test_annotation::os_lval(s, hash);
}
void c_phc_output_annotation::o_get(ArrayElementVec &props) const {
  c_test_annotation::o_get(props);
}
bool c_phc_output_annotation::o_exists(CStrRef s, int64 hash) const {
  return c_test_annotation::o_exists(s, hash);
}
Variant c_phc_output_annotation::o_get(CStrRef s, int64 hash) {
  return c_test_annotation::o_get(s, hash);
}
Variant c_phc_output_annotation::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test_annotation::o_set(s, hash, v, forInit);
}
Variant &c_phc_output_annotation::o_lval(CStrRef s, int64 hash) {
  return c_test_annotation::o_lval(s, hash);
}
Variant c_phc_output_annotation::os_constant(const char *s) {
  return c_test_annotation::os_constant(s);
}
IMPLEMENT_CLASS(phc_output_annotation)
ObjectData *c_phc_output_annotation::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_phc_output_annotation::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_phc_output_annotation::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_phc_output_annotation::cloneImpl() {
  c_phc_output_annotation *obj = NEW(c_phc_output_annotation)();
  cloneSet(obj);
  return obj;
}
void c_phc_output_annotation::cloneSet(c_phc_output_annotation *clone) {
  c_test_annotation::cloneSet(clone);
}
Variant c_phc_output_annotation::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(params.rvalAt(0)));
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(params.rvalAt(0)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke(s, params, hash, fatal);
}
Variant c_phc_output_annotation::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(a0, a1), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(a0));
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(a0), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_phc_output_annotation::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test_annotation::os_invoke(c, s, params, hash, fatal);
}
Variant cw_phc_output_annotation$os_get(const char *s) {
  return c_phc_output_annotation::os_get(s, -1);
}
Variant &cw_phc_output_annotation$os_lval(const char *s) {
  return c_phc_output_annotation::os_lval(s, -1);
}
Variant cw_phc_output_annotation$os_constant(const char *s) {
  return c_phc_output_annotation::os_constant(s);
}
Variant cw_phc_output_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_phc_output_annotation::os_invoke(c, s, params, -1, fatal);
}
void c_phc_output_annotation::init() {
  c_test_annotation::init();
}
/* SRC: phc-test/framework/annotated_test.php line 104 */
void c_phc_output_annotation::t___construct() {
  INSTANCE_METHOD_INJECTION(PHC_output_annotation, PHC_output_annotation::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(106,c_test_annotation::t___construct());
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = "phc-stdout");
  (o_lval("value_regex", 0x0AAD2AAB5871FA91LL) = "(!\?/.*/)|([^/])|([^/].*[^/])");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 112 */
Array c_phc_output_annotation::t_get_default_options() {
  INSTANCE_METHOD_INJECTION(PHC_output_annotation, PHC_output_annotation::get_default_options);
  return ScalarArrays::sa_[4];
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 122 */
Variant c_phc_output_annotation::t_post_process(Variant v_bundle) {
  INSTANCE_METHOD_INJECTION(PHC_output_annotation, PHC_output_annotation::post_process);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_value;
  Variant v_options;
  bool v_negate = false;
  Variant v_out;
  String v_pattern;
  Variant v_matches;
  Variant v_result;
  String v_suffix;

  {
    LOOP_COUNTER(1);
    Variant map2 = o_get("values", 0x76E3E991B86D1574LL);
    for (ArrayIterPtr iter3 = map2.begin("phc_output_annotation"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      {
        (v_options = LINE(126,x_array_shift(ref(lval(o_lval("options", 0x7C17922060DCA1EALL))))));
        (v_negate = false);
        if (!(toBoolean(v_options.rvalAt("regex", 0x2963E59544F2D0CFLL)))) (v_value = LINE(131,x_preg_quote(toString(v_value), "/")));
        else {
          if (equal(v_value.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "!")) {
            (v_negate = true);
            (v_value = LINE(137,x_substr(toString(v_value), toInt32(1LL))));
          }
          LINE(140,invoke_failed("phc_assert", Array(ArrayInit(2).set(0, equal(v_value.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "/")).set(1, "regexes must start with \"/\" or \"!/\"").create()), 0x000000002403FBF9LL));
          LINE(141,(assignCallTemp(eo_0, equal(v_value.rvalAt(x_strlen(toString(v_value)) - 1LL), "/")),invoke_failed("phc_assert", Array(ArrayInit(2).set(0, eo_0).set(1, "regexes must end with \"/\"").create()), 0x000000002403FBF9LL)));
          (v_value = LINE(144,(assignCallTemp(eo_0, toString(v_value)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_value)) - 2LL)),x_substr(eo_0, toInt32(1LL), eo_2))));
        }
        if (toBoolean(v_options.rvalAt("stderr", 0x5A17BD333513F000LL))) {
          (v_out = v_bundle.o_get("errs", 0x15E266045DABA002LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          (v_bundle.o_lval("expected_failure", 0x45CC9D552FFD4D63LL) = true);
        }
        else (v_out = v_bundle.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
        if (toBoolean(v_options.rvalAt("prefix", 0x1D3C3F3792B6F3B8LL))) (v_value = LINE(158,concat3(toString(v_options.rvalAt("prefix", 0x1D3C3F3792B6F3B8LL)), ": ", toString(v_value))));
        if (toBoolean(v_options.rvalAt("location", 0x2B6706CF4608DE2BLL))) {
          (v_pattern = LINE(164,concat3("!^[^:]+:\\d+: ", toString(v_value), "\nNote that line numbers are inaccurate, and will be fixed in a later release\n!ms")));
        }
        else (v_pattern = LINE(167,concat3("/", toString(v_value), "/ms")));
        (v_result = LINE(169,x_preg_match(v_pattern, toString(v_out), ref(v_matches))));
        (v_suffix = LINE(172,concat5("\"", toString(v_value), "\" using pattern \"", v_pattern, "\"")));
        if (v_negate) {
          if (same(v_result, 1LL)) return LINE(176,concat5(toString(v_options.rvalAt("prefix", 0x1D3C3F3792B6F3B8LL)), " found (", toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "), not expected: ", v_suffix));
        }
        else {
          if (same(v_result, 0LL)) return LINE(181,concat3(toString(v_options.rvalAt("prefix", 0x1D3C3F3792B6F3B8LL)), " not found, expected: ", v_suffix));
          if (same(v_result, false)) return toString("Test annotation error with: ") + v_suffix;
          if (!same(v_result, 1LL)) return toString("Unexpected error with: ") + v_suffix;
        }
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 312 */
Variant c_annotated_test::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_annotated_test::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_annotated_test::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_annotated_test::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_annotated_test::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_annotated_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_annotated_test::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_annotated_test::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(annotated_test)
ObjectData *c_annotated_test::cloneImpl() {
  c_annotated_test *obj = NEW(c_annotated_test)();
  cloneSet(obj);
  return obj;
}
void c_annotated_test::cloneSet(c_annotated_test *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_annotated_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_annotated_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_annotated_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_annotated_test$os_get(const char *s) {
  return c_annotated_test::os_get(s, -1);
}
Variant &cw_annotated_test$os_lval(const char *s) {
  return c_annotated_test::os_lval(s, -1);
}
Variant cw_annotated_test$os_constant(const char *s) {
  return c_annotated_test::os_constant(s);
}
Variant cw_annotated_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_annotated_test::os_invoke(c, s, params, -1, fatal);
}
void c_annotated_test::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/annotated_test.php line 314 */
Variant c_annotated_test::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(Annotated_test, Annotated_test::get_test_subjects);
  return LINE(316,f_get_all_scripts());
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 319 */
void c_annotated_test::t_run_test(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(Annotated_test, Annotated_test::run_test);
  Variant v_bundle;
  Variant v_annotation;

  (v_bundle = ((Object)(LINE(321,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))));
  (v_bundle.o_lval("annotations", 0x749B2BFC17CC198DLL) = LINE(323,t_get_annotations(v_subject)));
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(324,invoke_failed("get_phc_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000081EE2BC9LL))), 0x77CFA1EEF01BCA90LL);
  (v_bundle.o_lval("final", 0x5192930B2145036ELL) = "finish");
  {
    LOOP_COUNTER(4);
    Variant map5 = v_bundle.o_get("annotations", 0x749B2BFC17CC198DLL);
    for (ArrayIterPtr iter6 = map5.begin("annotated_test"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_annotation = iter6->second();
      LINE(328,v_annotation.o_invoke_few_args("pre_process", 0x2D978E66E8ACC726LL, 1, v_bundle));
    }
  }
  LINE(330,v_bundle.o_invoke_few_args("start", 0x3970634433FD3E52LL, 0));
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 333 */
void c_annotated_test::t_finish(Variant v_bundle) {
  INSTANCE_METHOD_INJECTION(Annotated_test, Annotated_test::finish);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_annotation;
  Variant v_failure;

  {
    LOOP_COUNTER(7);
    Variant map8 = v_bundle.o_get("annotations", 0x749B2BFC17CC198DLL);
    for (ArrayIterPtr iter9 = map8.begin("annotated_test"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_annotation = iter9->second();
      {
        (v_failure = LINE(338,v_annotation.o_invoke_few_args("post_process", 0x6A49FF703AB1D8D5LL, 1, v_bundle)));
        if (toBoolean(v_failure)) {
          LINE(341,t_async_failure(v_failure, v_bundle));
          return;
        }
      }
    }
  }
  LINE(346,t_async_success(v_bundle));
  if (toObject(v_bundle)->t___isset("expected_failure") && toBoolean(v_bundle.o_get("expected_failure", 0x45CC9D552FFD4D63LL))) {
    lval(o_lval("solo_tests", 0x1B0BE48B8213C382LL))++;
    LINE(352,(assignCallTemp(eo_0, ref(t_get_name())),assignCallTemp(eo_1, ref(v_bundle.o_lval("subject", 0x0EDD9FCC9525C3B2LL))),invoke_failed("write_dependencies", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, false).create()), 0x000000007DFD02A7LL)));
  }
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 356 */
Variant c_annotated_test::t_get_annotations(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(Annotated_test, Annotated_test::get_annotations);
  Variant eo_0;
  Variant eo_1;
  Variant v_out;
  Variant v_available;
  Variant v_triples;
  Variant v_lines;
  Array v_pregs;
  Variant v_line;
  Variant v_preg;
  Variant v_line_match;
  Variant v_comment_match;
  Variant v_result;
  Variant v_triple;
  Variant v_triple_match;
  Variant v_name;
  Variant v_options;
  Variant v_value;
  Variant v_annotation;

  (v_out = LINE(358,x_file_get_contents(toString(v_subject))));
  (v_available = LINE(360,f_get_available_annotations()));
  (v_triples = ScalarArrays::sa_[3]);
  (v_lines = LINE(364,x_split("\n", toString(v_out))));
  (v_pregs = ScalarArrays::sa_[5]);
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_lines.begin("annotated_test"); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_line = iter12->second();
      {
        {
          LOOP_COUNTER(13);
          for (ArrayIter iter15 = v_pregs.begin("annotated_test"); !iter15.end(); ++iter15) {
            LOOP_COUNTER_CHECK(13);
            v_preg = iter15.second();
            {
              if (toBoolean(LINE(370,x_preg_match(toString(v_preg), toString(v_line), ref(v_line_match))))) {
                if (toBoolean(LINE(385,x_preg_match("/\\s*\n\t\t\t\t\t\t\t\t{\t\t\t\t\t\t# start matching string\n\t\t\t\t\t\t\t\t\t\\s*\n\t\t\t\t\t\t\t\t\t(\\S+\t\t\t\t# start match with name\n\t\t\t\t\t\t\t\t\t\\s*\n\t\t\t\t\t\t\t\t\t(\\(.*\?\\))\?\t\t# match optional argument\n\t\t\t\t\t\t\t\t\t:\n\t\t\t\t\t\t\t\t\t\\s*\n\t\t\t\t\t\t\t\t\t\\S.*\\S)\t\t\t# end match with value\n\t\t\t\t\t\t\t\t\t\\s*\n\t\t\t\t\t\t\t\t}\t\t\t\t\t\t# finish match\n\t\t\t\t\t\t\t\\s*/x", toString(v_line_match.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), ref(v_comment_match))))) {
                  v_triples.append((v_comment_match.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
                }
              }
            }
          }
        }
      }
    }
  }
  (v_result = ScalarArrays::sa_[3]);
  LOOP_COUNTER(16);
  {
    while (more(LINE(395,x_count(v_triples)), 0LL)) {
      LOOP_COUNTER_CHECK(16);
      {
        (v_triple = LINE(397,x_array_shift(ref(v_triples))));
        LINE(414,x_preg_match("/^\n\t\t\t\t\t\t(\\S+)\t\t\t\t# match name\n\t\t\t\t\t\t\\s*\n\t\t\t\t\t\t(\?:\t\t\t\t# dont put this group into the result\n\t\t\t\t\t\t\t\\(\n\t\t\t\t\t\t\t\t(.*\?)\t\t# match options, but not the brackets\n\t\t\t\t\t\t\t\\)\t\n\t\t\t\t\t\t)\?\t\t\t\t\t# options is optional\n\t\t\t\t\t\t:\n\t\t\t\t\t\t\\s*\n\t\t\t\t\t\t(\\S.*\\S)\t\t\t# match value\n\t\t\t\t\t\t\\s*\n\t\t\t\t\t$/x", toString(v_triple), ref(v_triple_match)));
        (v_name = v_triple_match.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
        (v_options = LINE(417,f_parse_options(v_triple_match.rvalAt(2LL, 0x486AFCC090D5F98CLL))));
        (v_value = v_triple_match.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL));
        LINE(420,(assignCallTemp(eo_0, isset(v_available, v_name)),assignCallTemp(eo_1, concat4("Annotation ", toString(v_name), " not available, in ", toString(v_subject))),invoke_failed("phc_assert", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x000000002403FBF9LL)));
        LINE(422,v_available.rvalAt(v_name).o_invoke_few_args("add_value", 0x408D8E9A16765FE3LL, 2, v_options, v_value));
        v_result.set(v_name, (v_available.rvalAt(v_name)));
        (v_triples = LINE(425,(assignCallTemp(eo_0, v_available.rvalAt(v_name).o_invoke_few_args("get_dependencies", 0x230EE520FA1C4A53LL, 0)),assignCallTemp(eo_1, v_triples),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
      }
    }
  }
  {
    LOOP_COUNTER(17);
    for (ArrayIterPtr iter19 = v_available.begin("annotated_test"); !iter19->end(); iter19->next()) {
      LOOP_COUNTER_CHECK(17);
      v_annotation = iter19->second();
      v_name = iter19->first();
      {
        if (equal(LINE(431,x_count(v_annotation.o_get("values", 0x76E3E991B86D1574LL))), 0LL) && !same(v_annotation.o_invoke_few_args("get_default_value", 0x40C589AC5CCD7FDCLL, 0), null)) {
          (assignCallTemp(eo_0, ref(LINE(433,v_annotation.o_invoke_few_args("get_default_options", 0x17A3822F9EA40924LL, 0)))),assignCallTemp(eo_1, ref(v_annotation.o_invoke_few_args("get_default_value", 0x40C589AC5CCD7FDCLL, 0))),v_annotation.o_invoke("add_value", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x408D8E9A16765FE3LL));
          v_result.append((v_annotation));
        }
      }
    }
  }
  return v_result;
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 442 */
void c_annotated_test::t_run() {
  INSTANCE_METHOD_INJECTION(Annotated_test, Annotated_test::run);
  Variant v_num_skipped;

  LINE(444,c_asynctest::t_run());
  (v_num_skipped = o_get("solo_tests", 0x1B0BE48B8213C382LL));
  echo(LINE(446,concat3("(", toString(v_num_skipped), " solo tests)\n")));
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 64 */
Variant c_phc_exit_code_annotation::os_get(const char *s, int64 hash) {
  return c_test_annotation::os_get(s, hash);
}
Variant &c_phc_exit_code_annotation::os_lval(const char *s, int64 hash) {
  return c_test_annotation::os_lval(s, hash);
}
void c_phc_exit_code_annotation::o_get(ArrayElementVec &props) const {
  c_test_annotation::o_get(props);
}
bool c_phc_exit_code_annotation::o_exists(CStrRef s, int64 hash) const {
  return c_test_annotation::o_exists(s, hash);
}
Variant c_phc_exit_code_annotation::o_get(CStrRef s, int64 hash) {
  return c_test_annotation::o_get(s, hash);
}
Variant c_phc_exit_code_annotation::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test_annotation::o_set(s, hash, v, forInit);
}
Variant &c_phc_exit_code_annotation::o_lval(CStrRef s, int64 hash) {
  return c_test_annotation::o_lval(s, hash);
}
Variant c_phc_exit_code_annotation::os_constant(const char *s) {
  return c_test_annotation::os_constant(s);
}
IMPLEMENT_CLASS(phc_exit_code_annotation)
ObjectData *c_phc_exit_code_annotation::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_phc_exit_code_annotation::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_phc_exit_code_annotation::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_phc_exit_code_annotation::cloneImpl() {
  c_phc_exit_code_annotation *obj = NEW(c_phc_exit_code_annotation)();
  cloneSet(obj);
  return obj;
}
void c_phc_exit_code_annotation::cloneSet(c_phc_exit_code_annotation *clone) {
  c_test_annotation::cloneSet(clone);
}
Variant c_phc_exit_code_annotation::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(params.rvalAt(0)));
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(params.rvalAt(0)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke(s, params, hash, fatal);
}
Variant c_phc_exit_code_annotation::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(a0, a1), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(a0));
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(a0), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_phc_exit_code_annotation::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test_annotation::os_invoke(c, s, params, hash, fatal);
}
Variant cw_phc_exit_code_annotation$os_get(const char *s) {
  return c_phc_exit_code_annotation::os_get(s, -1);
}
Variant &cw_phc_exit_code_annotation$os_lval(const char *s) {
  return c_phc_exit_code_annotation::os_lval(s, -1);
}
Variant cw_phc_exit_code_annotation$os_constant(const char *s) {
  return c_phc_exit_code_annotation::os_constant(s);
}
Variant cw_phc_exit_code_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_phc_exit_code_annotation::os_invoke(c, s, params, -1, fatal);
}
void c_phc_exit_code_annotation::init() {
  c_test_annotation::init();
}
/* SRC: phc-test/framework/annotated_test.php line 66 */
void c_phc_exit_code_annotation::t___construct() {
  INSTANCE_METHOD_INJECTION(PHC_exit_code_annotation, PHC_exit_code_annotation::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(68,c_test_annotation::t___construct());
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = "phc-exit-code");
  (o_lval("value_description", 0x46B14C3579DD8026LL) = "phc exit-code");
  (o_lval("value_regex", 0x0AAD2AAB5871FA91LL) = "\\!\?\\d+");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 74 */
Variant c_phc_exit_code_annotation::t_post_process(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(PHC_exit_code_annotation, PHC_exit_code_annotation::post_process);
  Variant eo_0;
  Variant eo_1;
  Variant v_expected;
  bool v_complement = false;

  LINE(76,(assignCallTemp(eo_0, equal(x_count(o_get("values", 0x76E3E991B86D1574LL)), 1LL)),invoke_failed("phc_assert", Array(ArrayInit(2).set(0, eo_0).set(1, "Cant have more than 1 exit code").create()), 0x000000002403FBF9LL)));
  (v_expected = o_get("values", 0x76E3E991B86D1574LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  if (same(v_expected.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "!")) {
    (v_complement = true);
    v_expected.set(0LL, ("0"), 0x77CFA1EEF01BCA90LL);
  }
  else (v_complement = false);
  (v_expected = toInt64(v_expected));
  if ((v_complement && same(v_expected, toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) || (!(v_complement) && !same(v_expected, toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))) return LINE(92,concat5("Incorrect exit code: ", toString(toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), " (expected: ", toString(v_expected), ")"));
  return null;
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 95 */
String c_phc_exit_code_annotation::t_get_default_value() {
  INSTANCE_METHOD_INJECTION(PHC_exit_code_annotation, PHC_exit_code_annotation::get_default_value);
  return "0";
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 16 */
Variant c_test_annotation::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test_annotation::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test_annotation::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test_annotation::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test_annotation::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test_annotation::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test_annotation::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test_annotation::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test_annotation)
ObjectData *c_test_annotation::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_test_annotation::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_test_annotation::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_test_annotation::cloneImpl() {
  c_test_annotation *obj = NEW(c_test_annotation)();
  cloneSet(obj);
  return obj;
}
void c_test_annotation::cloneSet(c_test_annotation *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test_annotation::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(params.rvalAt(0)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test_annotation::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(a0, a1), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(a0), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test_annotation::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test_annotation$os_get(const char *s) {
  return c_test_annotation::os_get(s, -1);
}
Variant &cw_test_annotation$os_lval(const char *s) {
  return c_test_annotation::os_lval(s, -1);
}
Variant cw_test_annotation$os_constant(const char *s) {
  return c_test_annotation::os_constant(s);
}
Variant cw_test_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test_annotation::os_invoke(c, s, params, -1, fatal);
}
void c_test_annotation::init() {
}
/* SRC: phc-test/framework/annotated_test.php line 18 */
void c_test_annotation::t___construct() {
  INSTANCE_METHOD_INJECTION(Test_annotation, Test_annotation::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("values", 0x76E3E991B86D1574LL) = ScalarArrays::sa_[3]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 23 */
void c_test_annotation::t_add_value(CVarRef v_options, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(Test_annotation, Test_annotation::add_value);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  LINE(25,(assignCallTemp(eo_0, ref((assignCallTemp(eo_2, concat3("~", toString(o_get("value_regex", 0x0AAD2AAB5871FA91LL)), "~")),assignCallTemp(eo_3, toString(v_value)),x_preg_match(eo_2, eo_3)))),assignCallTemp(eo_1, concat5("Value (", toString(v_value), " )provided does not match regex (", toString(o_get("value_regex", 0x0AAD2AAB5871FA91LL)), ")")),invoke_failed("phc_assert", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x000000002403FBF9LL)));
  lval(o_lval("options", 0x7C17922060DCA1EALL)).append((LINE(26,(assignCallTemp(eo_0, o_root_invoke_few_args("get_default_options", 0x17A3822F9EA40924LL, 0)),assignCallTemp(eo_1, v_options),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))))));
  lval(o_lval("values", 0x76E3E991B86D1574LL)).append((v_value));
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 31 */
Variant c_test_annotation::t_get_default_value() {
  INSTANCE_METHOD_INJECTION(Test_annotation, Test_annotation::get_default_value);
  return null;
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 37 */
Array c_test_annotation::t_get_default_options() {
  INSTANCE_METHOD_INJECTION(Test_annotation, Test_annotation::get_default_options);
  return ScalarArrays::sa_[3];
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 42 */
Array c_test_annotation::t_get_dependencies() {
  INSTANCE_METHOD_INJECTION(Test_annotation, Test_annotation::get_dependencies);
  return ScalarArrays::sa_[3];
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 43 */
void c_test_annotation::t_pre_process(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(Test_annotation, Test_annotation::pre_process);
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 44 */
void c_test_annotation::t_post_process(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(Test_annotation, Test_annotation::post_process);
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 194 */
Variant c_annotation_translator::os_get(const char *s, int64 hash) {
  return c_test_annotation::os_get(s, hash);
}
Variant &c_annotation_translator::os_lval(const char *s, int64 hash) {
  return c_test_annotation::os_lval(s, hash);
}
void c_annotation_translator::o_get(ArrayElementVec &props) const {
  c_test_annotation::o_get(props);
}
bool c_annotation_translator::o_exists(CStrRef s, int64 hash) const {
  return c_test_annotation::o_exists(s, hash);
}
Variant c_annotation_translator::o_get(CStrRef s, int64 hash) {
  return c_test_annotation::o_get(s, hash);
}
Variant c_annotation_translator::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test_annotation::o_set(s, hash, v, forInit);
}
Variant &c_annotation_translator::o_lval(CStrRef s, int64 hash) {
  return c_test_annotation::o_lval(s, hash);
}
Variant c_annotation_translator::os_constant(const char *s) {
  return c_test_annotation::os_constant(s);
}
IMPLEMENT_CLASS(annotation_translator)
ObjectData *c_annotation_translator::create(Variant v_name, Variant v_description, Variant v_translations, Variant v_extras //  = ScalarArrays::sa_[3]
) {
  init();
  t___construct(v_name, v_description, v_translations, v_extras);
  return this;
}
ObjectData *c_annotation_translator::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
void c_annotation_translator::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 3) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
}
ObjectData *c_annotation_translator::cloneImpl() {
  c_annotation_translator *obj = NEW(c_annotation_translator)();
  cloneSet(obj);
  return obj;
}
void c_annotation_translator::cloneSet(c_annotation_translator *clone) {
  c_test_annotation::cloneSet(clone);
}
Variant c_annotation_translator::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(params.rvalAt(0)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 3) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke(s, params, hash, fatal);
}
Variant c_annotation_translator::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(a0, a1), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(a0), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 3) return (t___construct(a0, a1, a2), null);
        return (t___construct(a0, a1, a2, a3), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_annotation_translator::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test_annotation::os_invoke(c, s, params, hash, fatal);
}
Variant cw_annotation_translator$os_get(const char *s) {
  return c_annotation_translator::os_get(s, -1);
}
Variant &cw_annotation_translator$os_lval(const char *s) {
  return c_annotation_translator::os_lval(s, -1);
}
Variant cw_annotation_translator$os_constant(const char *s) {
  return c_annotation_translator::os_constant(s);
}
Variant cw_annotation_translator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_annotation_translator::os_invoke(c, s, params, -1, fatal);
}
void c_annotation_translator::init() {
  c_test_annotation::init();
}
/* SRC: phc-test/framework/annotated_test.php line 196 */
void c_annotation_translator::t___construct(Variant v_name, Variant v_description, Variant v_translations, Variant v_extras //  = ScalarArrays::sa_[3]
) {
  INSTANCE_METHOD_INJECTION(Annotation_translator, Annotation_translator::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(198,c_test_annotation::t___construct());
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = v_name);
  (o_lval("description", 0x3E0F7A9437DA26FELL) = v_description);
  (o_lval("translations", 0x769EA299FE592E1FLL) = v_translations);
  (o_lval("extras", 0x2CAACEBF850105F3LL) = v_extras);
  (o_lval("value_regex", 0x0AAD2AAB5871FA91LL) = ".*");
  if (!(LINE(206,x_is_array(v_translations)))) (o_lval("translations", 0x769EA299FE592E1FLL) = Array(ArrayInit(1).set(0, v_translations).create()));
  if (!(LINE(209,x_is_array(v_extras)))) (o_lval("extras", 0x2CAACEBF850105F3LL) = Array(ArrayInit(1).set(0, v_extras).create()));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 213 */
Array c_annotation_translator::t_get_dependencies() {
  INSTANCE_METHOD_INJECTION(Annotation_translator, Annotation_translator::get_dependencies);
  Array v_result;
  Variant v_value;
  Variant v_translation;
  Variant v_extra;

  (v_result = ScalarArrays::sa_[3]);
  {
    LOOP_COUNTER(20);
    Variant map21 = o_get("values", 0x76E3E991B86D1574LL);
    for (ArrayIterPtr iter22 = map21.begin("annotation_translator"); !iter22->end(); iter22->next()) {
      LOOP_COUNTER_CHECK(20);
      v_value = iter22->second();
      {
        LOOP_COUNTER(23);
        Variant map24 = o_get("translations", 0x769EA299FE592E1FLL);
        for (ArrayIterPtr iter25 = map24.begin("annotation_translator"); !iter25->end(); iter25->next()) {
          LOOP_COUNTER_CHECK(23);
          v_translation = iter25->second();
          v_result.append((LINE(218,concat3(toString(v_translation), ": ", toString(v_value)))));
        }
      }
    }
  }
  {
    LOOP_COUNTER(26);
    Variant map27 = o_get("extras", 0x2CAACEBF850105F3LL);
    for (ArrayIterPtr iter28 = map27.begin("annotation_translator"); !iter28->end(); iter28->next()) {
      LOOP_COUNTER_CHECK(26);
      v_extra = iter28->second();
      v_result.append((toString(v_extra)));
    }
  }
  (o_lval("values", 0x76E3E991B86D1574LL) = ScalarArrays::sa_[3]);
  (o_lval("extras", 0x2CAACEBF850105F3LL) = ScalarArrays::sa_[3]);
  return v_result;
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 47 */
Variant c_phc_option_annotation::os_get(const char *s, int64 hash) {
  return c_test_annotation::os_get(s, hash);
}
Variant &c_phc_option_annotation::os_lval(const char *s, int64 hash) {
  return c_test_annotation::os_lval(s, hash);
}
void c_phc_option_annotation::o_get(ArrayElementVec &props) const {
  c_test_annotation::o_get(props);
}
bool c_phc_option_annotation::o_exists(CStrRef s, int64 hash) const {
  return c_test_annotation::o_exists(s, hash);
}
Variant c_phc_option_annotation::o_get(CStrRef s, int64 hash) {
  return c_test_annotation::o_get(s, hash);
}
Variant c_phc_option_annotation::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test_annotation::o_set(s, hash, v, forInit);
}
Variant &c_phc_option_annotation::o_lval(CStrRef s, int64 hash) {
  return c_test_annotation::o_lval(s, hash);
}
Variant c_phc_option_annotation::os_constant(const char *s) {
  return c_test_annotation::os_constant(s);
}
IMPLEMENT_CLASS(phc_option_annotation)
ObjectData *c_phc_option_annotation::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_phc_option_annotation::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_phc_option_annotation::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_phc_option_annotation::cloneImpl() {
  c_phc_option_annotation *obj = NEW(c_phc_option_annotation)();
  cloneSet(obj);
  return obj;
}
void c_phc_option_annotation::cloneSet(c_phc_option_annotation *clone) {
  c_test_annotation::cloneSet(clone);
}
Variant c_phc_option_annotation::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(params.rvalAt(0)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke(s, params, hash, fatal);
}
Variant c_phc_option_annotation::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x230EE520FA1C4A53LL, get_dependencies) {
        return (t_get_dependencies());
      }
      HASH_GUARD(0x408D8E9A16765FE3LL, add_value) {
        return (t_add_value(a0, a1), null);
      }
      break;
    case 4:
      HASH_GUARD(0x17A3822F9EA40924LL, get_default_options) {
        return (t_get_default_options());
      }
      break;
    case 5:
      HASH_GUARD(0x6A49FF703AB1D8D5LL, post_process) {
        return (t_post_process(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x2D978E66E8ACC726LL, pre_process) {
        return (t_pre_process(a0), null);
      }
      break;
    case 12:
      HASH_GUARD(0x40C589AC5CCD7FDCLL, get_default_value) {
        return (t_get_default_value());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test_annotation::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_phc_option_annotation::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test_annotation::os_invoke(c, s, params, hash, fatal);
}
Variant cw_phc_option_annotation$os_get(const char *s) {
  return c_phc_option_annotation::os_get(s, -1);
}
Variant &cw_phc_option_annotation$os_lval(const char *s) {
  return c_phc_option_annotation::os_lval(s, -1);
}
Variant cw_phc_option_annotation$os_constant(const char *s) {
  return c_phc_option_annotation::os_constant(s);
}
Variant cw_phc_option_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_phc_option_annotation::os_invoke(c, s, params, -1, fatal);
}
void c_phc_option_annotation::init() {
  c_test_annotation::init();
}
/* SRC: phc-test/framework/annotated_test.php line 49 */
void c_phc_option_annotation::t___construct() {
  INSTANCE_METHOD_INJECTION(PHC_option_annotation, PHC_option_annotation::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(51,c_test_annotation::t___construct());
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = "phc-option");
  (o_lval("value_description", 0x46B14C3579DD8026LL) = "Command-line option to phc");
  (o_lval("value_regex", 0x0AAD2AAB5871FA91LL) = "\\S+");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 57 */
void c_phc_option_annotation::t_pre_process(Variant v_bundle) {
  INSTANCE_METHOD_INJECTION(PHC_option_annotation, PHC_option_annotation::pre_process);
  Variant v_value;

  {
    LOOP_COUNTER(29);
    Variant map30 = o_get("values", 0x76E3E991B86D1574LL);
    for (ArrayIterPtr iter31 = map30.begin("phc_option_annotation"); !iter31->end(); iter31->next()) {
      LOOP_COUNTER_CHECK(29);
      v_value = iter31->second();
      concat_assign(lval(lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(" ") + toString(v_value));
    }
  }
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 292 */
Variant f_parse_options(CVarRef v_options) {
  FUNCTION_INJECTION(parse_options);
  Array v_split;
  Variant v_key_pair;
  Sequence v_result;
  Variant v_key;
  Variant v_pair;

  if (equal(v_options, "")) return ScalarArrays::sa_[3];
  (v_split = LINE(297,x_explode(",", toString(v_options))));
  {
    LOOP_COUNTER(32);
    for (ArrayIter iter34 = v_split.begin(); !iter34.end(); ++iter34) {
      LOOP_COUNTER_CHECK(32);
      v_key_pair = iter34.second();
      {
        if (same(LINE(300,x_strpos(toString(v_key_pair), "=")), false)) v_result.set(v_key_pair, (true));
        else {
          df_lambda_1(LINE(304,x_split("=", toString(v_key_pair))), v_key, v_pair);
          v_result.set(v_key, (v_pair));
        }
      }
    }
  }
  return v_result;
} /* function */
/* SRC: phc-test/framework/annotated_test.php line 232 */
Variant f_get_available_annotations() {
  FUNCTION_INJECTION(get_available_annotations);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  Variant eo_9;
  Variant eo_10;
  Array v_annotations;
  Variant v_result;
  Variant v_annotation;

  (v_annotations = (assignCallTemp(eo_0, ((Object)(LINE(235,p_phc_option_annotation(p_phc_option_annotation(NEWOBJ(c_phc_option_annotation)())->create()))))),assignCallTemp(eo_1, ((Object)(LINE(236,p_phc_output_annotation(p_phc_output_annotation(NEWOBJ(c_phc_output_annotation)())->create()))))),assignCallTemp(eo_2, ((Object)(LINE(237,p_phc_exit_code_annotation(p_phc_exit_code_annotation(NEWOBJ(c_phc_exit_code_annotation)())->create()))))),assignCallTemp(eo_3, ((Object)(LINE(245,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-regex-error", "Error in PHP script, as a regex", "phc-stdout (stderr,regex,prefix=Error,location)", "phc-exit-code: !0")))))),assignCallTemp(eo_4, ((Object)(LINE(250,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-error", "Error in PHP script", "phc-stdout (stderr,prefix=Error,location)", "phc-exit-code: !0")))))),assignCallTemp(eo_5, ((Object)(LINE(255,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-usage-error", "Incorrect usage of phc command line", "phc-stdout (stderr,prefix=Error)", "phc-exit-code: !0")))))),assignCallTemp(eo_6, ((Object)(LINE(261,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-regex-warning", "Warning about PHP script, as a regex", "phc-stdout (stderr,regex,prefix=Warning,location)")))))),assignCallTemp(eo_7, ((Object)(LINE(265,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-warning", "Warning about PHP script", "phc-stdout (stderr,prefix=Warning,location)")))))),assignCallTemp(eo_8, ((Object)(LINE(271,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-regex-output", "Extract of phc output matches regex", "phc-stdout (stdout,regex)")))))),assignCallTemp(eo_9, ((Object)(LINE(275,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-output", "Extract of phc output", "phc-stdout (stdout)")))))),assignCallTemp(eo_10, ((Object)(LINE(279,p_annotation_translator(p_annotation_translator(NEWOBJ(c_annotation_translator)())->create("phc-debug", "Extract of phc debugging information", "phc-stdout (stderr,name=Debug)")))))),Array(ArrayInit(11).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, eo_4).set(5, eo_5).set(6, eo_6).set(7, eo_7).set(8, eo_8).set(9, eo_9).set(10, eo_10).create())));
  (v_result = ScalarArrays::sa_[3]);
  {
    LOOP_COUNTER(35);
    for (ArrayIter iter37 = v_annotations.begin(); !iter37.end(); ++iter37) {
      LOOP_COUNTER_CHECK(35);
      v_annotation = iter37.second();
      v_result.set(v_annotation.o_get("name", 0x0BCDB293DC3CBDDCLL), (v_annotation));
    }
  }
  return v_result;
} /* function */
Object co_phc_output_annotation(CArrRef params, bool init /* = true */) {
  return Object(p_phc_output_annotation(NEW(c_phc_output_annotation)())->dynCreate(params, init));
}
Object co_annotated_test(CArrRef params, bool init /* = true */) {
  return Object(p_annotated_test(NEW(c_annotated_test)())->dynCreate(params, init));
}
Object co_phc_exit_code_annotation(CArrRef params, bool init /* = true */) {
  return Object(p_phc_exit_code_annotation(NEW(c_phc_exit_code_annotation)())->dynCreate(params, init));
}
Object co_test_annotation(CArrRef params, bool init /* = true */) {
  return Object(p_test_annotation(NEW(c_test_annotation)())->dynCreate(params, init));
}
Object co_annotation_translator(CArrRef params, bool init /* = true */) {
  return Object(p_annotation_translator(NEW(c_annotation_translator)())->dynCreate(params, init));
}
Object co_phc_option_annotation(CArrRef params, bool init /* = true */) {
  return Object(p_phc_option_annotation(NEW(c_phc_option_annotation)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$annotated_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/annotated_test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$annotated_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(9,pm_php$phc_test$framework$lib$async_test_php(true, variables));
  LINE(449,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_annotated_test(p_annotated_test(NEWOBJ(c_annotated_test)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
