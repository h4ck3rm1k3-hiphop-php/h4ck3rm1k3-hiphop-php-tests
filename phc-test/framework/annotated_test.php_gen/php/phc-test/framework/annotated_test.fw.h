
#ifndef __GENERATED_php_phc_test_framework_annotated_test_fw_h__
#define __GENERATED_php_phc_test_framework_annotated_test_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(phc_output_annotation)
FORWARD_DECLARE_CLASS(annotated_test)
FORWARD_DECLARE_CLASS(phc_exit_code_annotation)
FORWARD_DECLARE_CLASS(test_annotation)
FORWARD_DECLARE_CLASS(annotation_translator)
FORWARD_DECLARE_CLASS(phc_option_annotation)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/framework/lib/async_test.fw.h>

#endif // __GENERATED_php_phc_test_framework_annotated_test_fw_h__
