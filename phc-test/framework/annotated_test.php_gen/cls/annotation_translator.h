
#ifndef __GENERATED_cls_annotation_translator_h__
#define __GENERATED_cls_annotation_translator_h__

#include <cls/test_annotation.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/annotated_test.php line 194 */
class c_annotation_translator : virtual public c_test_annotation {
  BEGIN_CLASS_MAP(annotation_translator)
    PARENT_CLASS(test_annotation)
  END_CLASS_MAP(annotation_translator)
  DECLARE_CLASS(annotation_translator, Annotation_translator, test_annotation)
  void init();
  public: void t___construct(Variant v_name, Variant v_description, Variant v_translations, Variant v_extras = ScalarArrays::sa_[3]);
  public: ObjectData *create(Variant v_name, Variant v_description, Variant v_translations, Variant v_extras = ScalarArrays::sa_[3]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Array t_get_dependencies();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_annotation_translator_h__
