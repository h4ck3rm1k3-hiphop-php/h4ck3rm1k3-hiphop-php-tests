
#ifndef __GENERATED_cls_test_annotation_h__
#define __GENERATED_cls_test_annotation_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/annotated_test.php line 16 */
class c_test_annotation : virtual public ObjectData {
  BEGIN_CLASS_MAP(test_annotation)
  END_CLASS_MAP(test_annotation)
  DECLARE_CLASS(test_annotation, Test_annotation, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add_value(CVarRef v_options, CVarRef v_value);
  public: Variant t_get_default_value();
  public: Array t_get_default_options();
  public: Array t_get_dependencies();
  public: void t_pre_process(CVarRef v_bundle);
  public: void t_post_process(CVarRef v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_annotation_h__
