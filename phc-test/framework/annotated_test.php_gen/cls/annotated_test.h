
#ifndef __GENERATED_cls_annotated_test_h__
#define __GENERATED_cls_annotated_test_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/annotated_test.php line 312 */
class c_annotated_test : virtual public c_asynctest {
  BEGIN_CLASS_MAP(annotated_test)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(annotated_test)
  DECLARE_CLASS(annotated_test, Annotated_test, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: void t_run_test(Variant v_subject);
  public: void t_finish(Variant v_bundle);
  public: Variant t_get_annotations(CVarRef v_subject);
  public: void t_run();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_annotated_test_h__
