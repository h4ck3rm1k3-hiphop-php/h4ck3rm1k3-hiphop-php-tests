
#ifndef __GENERATED_cls_phc_option_annotation_h__
#define __GENERATED_cls_phc_option_annotation_h__

#include <cls/test_annotation.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/annotated_test.php line 47 */
class c_phc_option_annotation : virtual public c_test_annotation {
  BEGIN_CLASS_MAP(phc_option_annotation)
    PARENT_CLASS(test_annotation)
  END_CLASS_MAP(phc_option_annotation)
  DECLARE_CLASS(phc_option_annotation, PHC_option_annotation, test_annotation)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_pre_process(Variant v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_phc_option_annotation_h__
