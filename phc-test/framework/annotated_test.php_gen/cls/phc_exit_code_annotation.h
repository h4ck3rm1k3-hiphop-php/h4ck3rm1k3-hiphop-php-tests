
#ifndef __GENERATED_cls_phc_exit_code_annotation_h__
#define __GENERATED_cls_phc_exit_code_annotation_h__

#include <cls/test_annotation.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/annotated_test.php line 64 */
class c_phc_exit_code_annotation : virtual public c_test_annotation {
  BEGIN_CLASS_MAP(phc_exit_code_annotation)
    PARENT_CLASS(test_annotation)
  END_CLASS_MAP(phc_exit_code_annotation)
  DECLARE_CLASS(phc_exit_code_annotation, PHC_exit_code_annotation, test_annotation)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_post_process(CVarRef v_bundle);
  public: String t_get_default_value();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_phc_exit_code_annotation_h__
