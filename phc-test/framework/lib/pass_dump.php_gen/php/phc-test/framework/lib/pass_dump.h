
#ifndef __GENERATED_php_phc_test_framework_lib_pass_dump_h__
#define __GENERATED_php_phc_test_framework_lib_pass_dump_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/pass_dump.fw.h>

// Declarations
#include <cls/pass_dump.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$lib$pass_dump_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_pass_dump(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_pass_dump_h__
