
#include <php/phc-test/framework/lib/pass_dump.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/pass_dump.php line 11 */
Variant c_pass_dump::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_pass_dump::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_pass_dump::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_pass_dump::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_pass_dump::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_pass_dump::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_pass_dump::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_pass_dump::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(pass_dump)
ObjectData *c_pass_dump::create(Variant v_pass, Variant v_dump //  = "dump"
, Variant v_dependency //  = "BasicParseTest"
) {
  init();
  t___construct(v_pass, v_dump, v_dependency);
  return this;
}
ObjectData *c_pass_dump::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_pass_dump::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_pass_dump::cloneImpl() {
  c_pass_dump *obj = NEW(c_pass_dump)();
  cloneSet(obj);
  return obj;
}
void c_pass_dump::cloneSet(c_pass_dump *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_pass_dump::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_pass_dump::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pass_dump::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pass_dump$os_get(const char *s) {
  return c_pass_dump::os_get(s, -1);
}
Variant &cw_pass_dump$os_lval(const char *s) {
  return c_pass_dump::os_lval(s, -1);
}
Variant cw_pass_dump$os_constant(const char *s) {
  return c_pass_dump::os_constant(s);
}
Variant cw_pass_dump$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pass_dump::os_invoke(c, s, params, -1, fatal);
}
void c_pass_dump::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/lib/pass_dump.php line 13 */
void c_pass_dump::t___construct(Variant v_pass, Variant v_dump //  = "dump"
, Variant v_dependency //  = "BasicParseTest"
) {
  INSTANCE_METHOD_INJECTION(Pass_dump, Pass_dump::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("pass", 0x70860AB53D1BA673LL) = v_pass);
  (o_lval("dump", 0x38B896FAF2928192LL) = v_dump);
  (o_lval("dependencies", 0x36DCDDF5BB1EBEBDLL) = Array(ArrayInit(1).set(0, v_dependency).create()));
  LINE(21,throw_fatal("unknown class asynctest", ((void*)NULL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/pass_dump.php line 24 */
String c_pass_dump::t_get_name() {
  INSTANCE_METHOD_INJECTION(Pass_dump, Pass_dump::get_name);
  return toString(o_get("pass", 0x70860AB53D1BA673LL)) + toString("_dump");
} /* function */
/* SRC: phc-test/framework/lib/pass_dump.php line 29 */
Variant c_pass_dump::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(Pass_dump, Pass_dump::get_test_subjects);
  return LINE(31,invoke_failed("get_all_scripts", Array(), 0x000000008819DA1ALL));
} /* function */
/* SRC: phc-test/framework/lib/pass_dump.php line 34 */
void c_pass_dump::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(Pass_dump, Pass_dump::run_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Object v_async;

  (v_async = LINE(37,create_object("asyncbundle", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, v_subject).create()))));
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (concat(toString(gv_phc), LINE(39,concat6(" ", toString(v_subject), " --", toString(o_get("dump", 0x38B896FAF2928192LL)), "=", toString(o_get("pass", 0x70860AB53D1BA673LL)))))), 0x77CFA1EEF01BCA90LL);
  lval(v_async.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(0LL, ("fail_on_output"), 0x77CFA1EEF01BCA90LL);
  (v_async.o_lval("final", 0x5192930B2145036ELL) = "async_success");
  LINE(43,v_async->o_invoke_few_args("start", 0x3970634433FD3E52LL, 0));
} /* function */
Object co_pass_dump(CArrRef params, bool init /* = true */) {
  return Object(p_pass_dump(NEW(c_pass_dump)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$pass_dump_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/pass_dump.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$pass_dump_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
