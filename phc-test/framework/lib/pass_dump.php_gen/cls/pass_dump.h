
#ifndef __GENERATED_cls_pass_dump_h__
#define __GENERATED_cls_pass_dump_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/pass_dump.php line 11 */
class c_pass_dump : virtual public ObjectData {
  BEGIN_CLASS_MAP(pass_dump)
  END_CLASS_MAP(pass_dump)
  DECLARE_CLASS(pass_dump, Pass_dump, asynctest)
  void init();
  public: void t___construct(Variant v_pass, Variant v_dump = "dump", Variant v_dependency = "BasicParseTest");
  public: ObjectData *create(Variant v_pass, Variant v_dump = "dump", Variant v_dependency = "BasicParseTest");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_get_name();
  public: Variant t_get_test_subjects();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pass_dump_h__
