
#ifndef __GENERATED_php_phc_test_framework_lib_labels_h__
#define __GENERATED_php_phc_test_framework_lib_labels_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/labels.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_is_labelled(CVarRef v_script, CVarRef v_label);
Variant f_get_all_scripts();
Variant f_strip_long_files(Variant v_label_struct);
Variant f_get_all_plugins();
Variant f_get_scripts_labelled(CStrRef v_label);
Variant f_get_interpretable_scripts();
bool f_skip_3rdparty(CVarRef v_filename);
Variant f_get_non_interpretable_scripts();
void f_process_label_file_line(CVarRef v_line, CVarRef v_files, Variant v_labelled_files);
Variant f_get_all_scripts_in_dir(CVarRef v_directory);
Variant pm_php$phc_test$framework$lib$labels_php(bool incOnce = false, LVariableTable* variables = NULL);
Sequence f_create_label_struct(CVarRef v_directory, CVarRef v_label_filename, CVarRef v_third_party_filename);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_labels_h__
