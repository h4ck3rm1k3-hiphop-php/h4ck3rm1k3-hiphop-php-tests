
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x52B182B0A94E9C41LL, php, 27);
      break;
    case 2:
      HASH_INDEX(0x47A8BEF0E404C882LL, opposite_label, 18);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x58FBD12F28C4BF49LL, label_file, 13);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      HASH_INDEX(0x2F2AA415EFAE460ELL, base_dir, 25);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 22:
      HASH_INDEX(0x73FDA0B4AF645216LL, non_default_labels, 16);
      break;
    case 26:
      HASH_INDEX(0x17B5D58607FC7F1ALL, default_labels, 15);
      HASH_INDEX(0x35A19C689312B1DALL, opt_no_progress_bar, 26);
      break;
    case 31:
      HASH_INDEX(0x7076E3AEF02E9CDFLL, opt_one, 22);
      HASH_INDEX(0x11FFDF37C0EB2C5FLL, tests, 28);
      break;
    case 32:
      HASH_INDEX(0x4B2063F3C49B8E60LL, exceptions, 19);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 38:
      HASH_INDEX(0x1DA88EB5034B9D26LL, plugin_dir, 24);
      break;
    case 42:
      HASH_INDEX(0x67EA86E1CF55026ALL, phc, 23);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      HASH_INDEX(0x7DDBEB38F40F832ELL, labels, 17);
      break;
    case 48:
      HASH_INDEX(0x5C390FD428B50E30LL, opt_long, 21);
      break;
    case 51:
      HASH_INDEX(0x6FB2F3D573DE52F3LL, subject_dir, 12);
      break;
    case 54:
      HASH_INDEX(0x19D6108580CBB436LL, label_struct, 20);
      break;
    case 55:
      HASH_INDEX(0x7D6AD2C5948BA3F7LL, third_party_label_file, 14);
      HASH_INDEX(0x4FFE4179D4CE15B7LL, opt_quick, 29);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 30) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
