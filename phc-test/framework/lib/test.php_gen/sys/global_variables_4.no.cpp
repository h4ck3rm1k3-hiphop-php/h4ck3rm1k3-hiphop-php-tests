
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "subject_dir",
    "label_file",
    "third_party_label_file",
    "default_labels",
    "non_default_labels",
    "labels",
    "opposite_label",
    "exceptions",
    "label_struct",
    "opt_long",
    "opt_one",
    "phc",
    "plugin_dir",
    "base_dir",
    "opt_no_progress_bar",
    "php",
    "tests",
    "opt_quick",
  };
  if (idx >= 0 && idx < 30) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(subject_dir);
    case 13: return GV(label_file);
    case 14: return GV(third_party_label_file);
    case 15: return GV(default_labels);
    case 16: return GV(non_default_labels);
    case 17: return GV(labels);
    case 18: return GV(opposite_label);
    case 19: return GV(exceptions);
    case 20: return GV(label_struct);
    case 21: return GV(opt_long);
    case 22: return GV(opt_one);
    case 23: return GV(phc);
    case 24: return GV(plugin_dir);
    case 25: return GV(base_dir);
    case 26: return GV(opt_no_progress_bar);
    case 27: return GV(php);
    case 28: return GV(tests);
    case 29: return GV(opt_quick);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
