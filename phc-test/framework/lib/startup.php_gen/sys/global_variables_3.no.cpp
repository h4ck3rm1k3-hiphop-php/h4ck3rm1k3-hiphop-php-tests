
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x4E80F79695D41A07LL, opt, 16);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 11:
      HASH_INDEX(0x1018D4B85360194BLL, opt_help, 23);
      break;
    case 12:
      HASH_INDEX(0x21DEE4EB204AE10CLL, cg, 12);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      HASH_INDEX(0x0510D59D8A97894ELL, opt_debug, 29);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 26:
      HASH_INDEX(0x35A19C689312B1DALL, opt_no_progress_bar, 24);
      break;
    case 27:
      HASH_INDEX(0x2FF01C2058B0B21BLL, opt_verbose, 18);
      HASH_INDEX(0x027BD19E0751121BLL, opt_numbered, 22);
      break;
    case 28:
      HASH_INDEX(0x0F60C6595ED9FADCLL, arguments, 15);
      break;
    case 29:
      HASH_INDEX(0x6890E1EA533ADF9DLL, getopt, 13);
      break;
    case 31:
      HASH_INDEX(0x7076E3AEF02E9CDFLL, opt_one, 28);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 36:
      HASH_INDEX(0x2F475D4DE7208D24LL, opt_valgrind, 20);
      break;
    case 39:
      HASH_INDEX(0x3F4289BA72D62D27LL, opt_installed, 25);
      break;
    case 42:
      HASH_INDEX(0x7C17922060DCA1EALL, options, 17);
      break;
    case 43:
      HASH_INDEX(0x2128D3CAEC57A82BLL, opt_support, 21);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 48:
      HASH_INDEX(0x5C390FD428B50E30LL, opt_long, 19);
      HASH_INDEX(0x300C2818152F9FF0LL, opt_reduce, 30);
      break;
    case 49:
      HASH_INDEX(0x070DFE5B93A7CAF1LL, opt_clean, 26);
      break;
    case 52:
      HASH_INDEX(0x421780730E09F634LL, opts, 14);
      break;
    case 55:
      HASH_INDEX(0x4FFE4179D4CE15B7LL, opt_quick, 27);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 31) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
