
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "cg",
    "getopt",
    "opts",
    "arguments",
    "opt",
    "options",
    "opt_verbose",
    "opt_long",
    "opt_valgrind",
    "opt_support",
    "opt_numbered",
    "opt_help",
    "opt_no_progress_bar",
    "opt_installed",
    "opt_clean",
    "opt_quick",
    "opt_one",
    "opt_debug",
    "opt_reduce",
  };
  if (idx >= 0 && idx < 31) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(cg);
    case 13: return GV(getopt);
    case 14: return GV(opts);
    case 15: return GV(arguments);
    case 16: return GV(opt);
    case 17: return GV(options);
    case 18: return GV(opt_verbose);
    case 19: return GV(opt_long);
    case 20: return GV(opt_valgrind);
    case 21: return GV(opt_support);
    case 22: return GV(opt_numbered);
    case 23: return GV(opt_help);
    case 24: return GV(opt_no_progress_bar);
    case 25: return GV(opt_installed);
    case 26: return GV(opt_clean);
    case 27: return GV(opt_quick);
    case 28: return GV(opt_one);
    case 29: return GV(opt_debug);
    case 30: return GV(opt_reduce);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
