
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 7:
      HASH_INITIALIZED(0x4E80F79695D41A07LL, g->GV(opt),
                       opt);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 11:
      HASH_INITIALIZED(0x1018D4B85360194BLL, g->GV(opt_help),
                       opt_help);
      break;
    case 12:
      HASH_INITIALIZED(0x21DEE4EB204AE10CLL, g->GV(cg),
                       cg);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      HASH_INITIALIZED(0x0510D59D8A97894ELL, g->GV(opt_debug),
                       opt_debug);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 26:
      HASH_INITIALIZED(0x35A19C689312B1DALL, g->GV(opt_no_progress_bar),
                       opt_no_progress_bar);
      break;
    case 27:
      HASH_INITIALIZED(0x2FF01C2058B0B21BLL, g->GV(opt_verbose),
                       opt_verbose);
      HASH_INITIALIZED(0x027BD19E0751121BLL, g->GV(opt_numbered),
                       opt_numbered);
      break;
    case 28:
      HASH_INITIALIZED(0x0F60C6595ED9FADCLL, g->GV(arguments),
                       arguments);
      break;
    case 29:
      HASH_INITIALIZED(0x6890E1EA533ADF9DLL, g->GV(getopt),
                       getopt);
      break;
    case 31:
      HASH_INITIALIZED(0x7076E3AEF02E9CDFLL, g->GV(opt_one),
                       opt_one);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 36:
      HASH_INITIALIZED(0x2F475D4DE7208D24LL, g->GV(opt_valgrind),
                       opt_valgrind);
      break;
    case 39:
      HASH_INITIALIZED(0x3F4289BA72D62D27LL, g->GV(opt_installed),
                       opt_installed);
      break;
    case 42:
      HASH_INITIALIZED(0x7C17922060DCA1EALL, g->GV(options),
                       options);
      break;
    case 43:
      HASH_INITIALIZED(0x2128D3CAEC57A82BLL, g->GV(opt_support),
                       opt_support);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 48:
      HASH_INITIALIZED(0x5C390FD428B50E30LL, g->GV(opt_long),
                       opt_long);
      HASH_INITIALIZED(0x300C2818152F9FF0LL, g->GV(opt_reduce),
                       opt_reduce);
      break;
    case 49:
      HASH_INITIALIZED(0x070DFE5B93A7CAF1LL, g->GV(opt_clean),
                       opt_clean);
      break;
    case 52:
      HASH_INITIALIZED(0x421780730E09F634LL, g->GV(opts),
                       opts);
      break;
    case 55:
      HASH_INITIALIZED(0x4FFE4179D4CE15B7LL, g->GV(opt_quick),
                       opt_quick);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
