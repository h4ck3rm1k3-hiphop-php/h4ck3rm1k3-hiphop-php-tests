
#include <php/phc-test/framework/lib/startup.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$phc_test$framework$lib$startup_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/startup.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$startup_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cg") : g->GV(cg);
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_getopt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getopt") : g->GV(getopt);
  Variant &v_opts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opts") : g->GV(opts);
  Variant &v_arguments __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arguments") : g->GV(arguments);
  Variant &v_opt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt") : g->GV(opt);
  Variant &v_options __attribute__((__unused__)) = (variables != gVariables) ? variables->get("options") : g->GV(options);
  Variant &v_opt_verbose __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_verbose") : g->GV(opt_verbose);
  Variant &v_opt_long __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_long") : g->GV(opt_long);
  Variant &v_opt_valgrind __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_valgrind") : g->GV(opt_valgrind);
  Variant &v_opt_support __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_support") : g->GV(opt_support);
  Variant &v_opt_numbered __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_numbered") : g->GV(opt_numbered);
  Variant &v_opt_help __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_help") : g->GV(opt_help);
  Variant &v_opt_no_progress_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_no_progress_bar") : g->GV(opt_no_progress_bar);
  Variant &v_opt_installed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_installed") : g->GV(opt_installed);
  Variant &v_opt_clean __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_clean") : g->GV(opt_clean);
  Variant &v_opt_quick __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_quick") : g->GV(opt_quick);
  Variant &v_opt_one __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_one") : g->GV(opt_one);
  Variant &v_opt_debug __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_debug") : g->GV(opt_debug);
  Variant &v_opt_reduce __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_reduce") : g->GV(opt_reduce);

  LINE(9,require("Console/Getopt.php", true, variables, "phc-test/framework/lib/"));
  LINE(10,require("Console/ProgressBar.php", true, variables, "phc-test/framework/lib/"));
  (v_cg = LINE(12,create_object("console_getopt", Array())));
  (v_argv = LINE(13,v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)));
  (v_getopt = (assignCallTemp(eo_0, ref(LINE(14,v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)))),v_cg.o_invoke("getOpt", Array(ArrayInit(3).set(0, eo_0).set(1, "lvVshnpicqO:Dr").set(2, Array(ArrayInit(13).set(0, "long").set(1, "verbose").set(2, "valgrind").set(3, "support").set(4, "help").set(5, "number").set(6, "no-progress").set(7, "installed").set(8, "clean").set(9, "quick").set(10, "one").set(11, "debug").set(12, "reduce").create())).create()), 0x2C6826999658AA5ELL)));
  if (instanceOf(v_getopt, "PEAR_Error")) f_exit(LINE(16,concat3("Command line error: ", toString(v_getopt.o_get("message", 0x3EAA4B97155366DFLL)), "\n")));
  df_lambda_1(v_getopt, v_opts, v_arguments);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_opts.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_opt = iter3->second();
      {
        if (equal(v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL), null)) v_options.set(v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), (""));
        else v_options.set(v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), ((v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
      }
    }
  }
  (v_opt_verbose = isset(v_options, "v", 0x3C2F961831E4EF6BLL));
  (v_opt_long = isset(v_options, "l", 0x2343A72E98024302LL));
  (v_opt_valgrind = isset(v_options, "V", 0x280F37D1F5DAB958LL));
  (v_opt_support = isset(v_options, "s", 0x0BC2CDE4BBFC9C10LL));
  (v_opt_numbered = isset(v_options, "n", 0x117B8667E4662809LL));
  (v_opt_help = isset(v_options, "h", 0x695875365480A8F0LL));
  (v_opt_no_progress_bar = isset(v_options, "p", 0x77F632A4E34F1526LL));
  (v_opt_installed = isset(v_options, "i", 0x0EB22EDA95766E98LL));
  (v_opt_clean = isset(v_options, "c", 0x32C769EE5C5509B0LL));
  (v_opt_quick = isset(v_options, "q", 0x05CAFC91A9B37763LL));
  (v_opt_one = isset(v_options, "O", 0x3769074A877921E6LL) ? ((Variant)(v_options.rvalAt("O", 0x3769074A877921E6LL))) : ((Variant)(false)));
  (v_opt_debug = isset(v_options, "D", 0x704CEFD058905A68LL));
  (v_opt_reduce = isset(v_options, "r", 0x6ECE84440D42ECF6LL));
  if (toBoolean(v_opt_help)) {
    f_exit(toString("driver.php - Front-end to the phc test framework - phpcompiler.org\n\nUsage: php driver.php [OPTIONS] [regex]\n    Note that [OPTIONS] cannot come after [regex]\n\nOptions:\n    -h     Print this help message\n    -V     Run phc through valgrind\n    -s     Generate support files (mostly for regression tests)\n    -v     Print verbose messages after test failure\n    -l     Run long tests (default: run only tests marked short in labels file)\n    -n     Print a numbered list of the files to be processed\n    -p     Don't use a progress bar (useful for nightly testing)\n    -i     Use installed program and plugins for tests\n    -c     Clean up (ie delete) all logs files and directories.\n    -q     Just use the first 10 test subjects, for a real quick test run\n    -O filename       Only test a single named file\n    -D     Run in debug mode.\n    -r     Automatically reduce failing test cases.\n\nRegex:\n    A list of regular expressions matching the tests to be run. Any test name\n    matching a regex in the list will be allowed run. By default, all tests\n    will be run.\n"));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
