
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_supportfiletest(CArrRef params, bool init = true);
Variant cw_supportfiletest$os_get(const char *s);
Variant &cw_supportfiletest$os_lval(const char *s);
Variant cw_supportfiletest$os_constant(const char *s);
Variant cw_supportfiletest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_regressiontest(CArrRef params, bool init = true);
Variant cw_regressiontest$os_get(const char *s);
Variant &cw_regressiontest$os_lval(const char *s);
Variant cw_regressiontest$os_constant(const char *s);
Variant cw_regressiontest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_CREATE_OBJECT(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
