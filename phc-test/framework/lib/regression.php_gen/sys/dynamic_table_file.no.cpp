
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$framework$lib$regression_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$support_file_test_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_INCLUDE(0x1B54C4EAD263D60ALL, "phc-test/framework/lib/regression.php", php$phc_test$framework$lib$regression_php);
      HASH_INCLUDE(0x4FEB3550FDA6EA42LL, "phc-test/framework/lib/support_file_test.php", php$phc_test$framework$lib$support_file_test_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
