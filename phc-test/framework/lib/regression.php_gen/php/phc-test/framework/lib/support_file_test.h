
#ifndef __GENERATED_php_phc_test_framework_lib_support_file_test_h__
#define __GENERATED_php_phc_test_framework_lib_support_file_test_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/support_file_test.fw.h>

// Declarations
#include <cls/supportfiletest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$lib$support_file_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_supportfiletest(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_support_file_test_h__
