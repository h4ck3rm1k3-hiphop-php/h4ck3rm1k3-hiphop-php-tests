
#ifndef __GENERATED_php_phc_test_framework_lib_header_h__
#define __GENERATED_php_phc_test_framework_lib_header_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/header.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_check_for_plugin(CVarRef v_plugin_name);
String f_reset_string();
String f_wd_name(CVarRef v_subject);
Variant f_homogenize_break_levels(Variant v_string);
Variant f_get_php();
Variant f_strip_console_codes(Variant v_string);
Variant f_homogenize_reference_count(Variant v_string);
Variant f_homogenize_all(Variant v_string, CVarRef v_filename);
Variant f_homogenize_xml(Variant v_string);
Variant f_date_string();
void f_kill_properly(Variant v_handle, Variant v_pipes);
bool f_check_for_program(Variant v_program_name);
void f_log_status(CVarRef v_status, CVarRef v_test_name, CVarRef v_subject, String v_reason);
String f_get_php_command_line(String v_subject, bool v_pipe = false);
void f_phc_error_handler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline, CVarRef v_errcontext);
String f_get_phc_command_line(CVarRef v_subject);
void f_phc_unreachable(CVarRef v_message = "This point should be unreachable");
void f_log_failure(Variant v_test_name, Variant v_subject, Array v_commands, Array v_outs, Variant v_errs, Array v_exits, Variant v_missing_dependency, Variant v_reason);
void f_open_status_files();
String f_red_string();
void f_close_status_files();
Variant f_homogenize_filenames_and_line_numbers(Variant v_string, CVarRef v_filename);
String f_copy_to_working_dir(CStrRef v_file);
Variant f_read_dependency(CVarRef v_test_name, CVarRef v_subject);
Variant f_diff(CVarRef v_string1, CVarRef v_string2);
String f_get_phc_compile_plugin();
void f_phc_assert(CVarRef v_boolean, CVarRef v_message);
void f_write_status(CVarRef v_status, CStrRef v_string);
Variant f_strip_colour(CVarRef v_string);
bool f_is_32_bit();
Variant f_adjusted_name(Variant v_script_name, int64 v_adjust_for_regression = 0LL);
String f_get_dependent_filename(CVarRef v_test_name, CVarRef v_subject);
void f_write_dependencies(CVarRef v_test_name, CVarRef v_subject, CVarRef v_value);
String f_green_string();
Variant pm_php$phc_test$framework$lib$header_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_blue_string();
Array f_complete_exec(CVarRef v_command, CVarRef v_stdin = null_variant, int64 v_timeout = 20LL, bool v_pass_through = false);
Variant f_homogenize_object_count(Variant v_string);
String f_get_phc();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_header_h__
