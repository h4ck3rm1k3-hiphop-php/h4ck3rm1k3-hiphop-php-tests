
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      HASH_INITIALIZED(0x52B182B0A94E9C41LL, g->GV(php),
                       php);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      HASH_INITIALIZED(0x2F2AA415EFAE460ELL, g->GV(base_dir),
                       base_dir);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      HASH_INITIALIZED(0x6C2E2CD6695D83D1LL, g->GV(log_directory),
                       log_directory);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      HASH_INITIALIZED(0x01F18996FC6E6193LL, g->GV(php_exe),
                       php_exe);
      break;
    case 27:
      HASH_INITIALIZED(0x2FF01C2058B0B21BLL, g->GV(opt_verbose),
                       opt_verbose);
      break;
    case 34:
      HASH_INITIALIZED(0x7359B522C6BE3A22LL, g->GV(phc_suffix),
                       phc_suffix);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      HASH_INITIALIZED(0x614447AB21C8FAE3LL, g->GV(valgrind),
                       valgrind);
      break;
    case 36:
      HASH_INITIALIZED(0x2F475D4DE7208D24LL, g->GV(opt_valgrind),
                       opt_valgrind);
      break;
    case 37:
      HASH_INITIALIZED(0x093CA964B9E038E5LL, g->GV(working_directory),
                       working_directory);
      break;
    case 38:
      HASH_INITIALIZED(0x1DA88EB5034B9D26LL, g->GV(plugin_dir),
                       plugin_dir);
      break;
    case 41:
      HASH_INITIALIZED(0x38BA10F6BA1F67E9LL, g->GV(working_dir),
                       working_dir);
      break;
    case 42:
      HASH_INITIALIZED(0x67EA86E1CF55026ALL, g->GV(phc),
                       phc);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 48:
      HASH_INITIALIZED(0x5C390FD428B50E30LL, g->GV(opt_long),
                       opt_long);
      HASH_INITIALIZED(0x7FD127A282D4A0F0LL, g->GV(strict),
                       strict);
      break;
    case 51:
      HASH_INITIALIZED(0x6FB2F3D573DE52F3LL, g->GV(subject_dir),
                       subject_dir);
      break;
    case 55:
      HASH_INITIALIZED(0x626A556D885E55F7LL, g->GV(status_files),
                       status_files);
      break;
    case 61:
      HASH_INITIALIZED(0x0B5BC0452CFBA5FDLL, g->GV(libphp),
                       libphp);
      break;
    case 62:
      HASH_INITIALIZED(0x18532A5EE8396FBELL, g->GV(date_string),
                       date_string);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
