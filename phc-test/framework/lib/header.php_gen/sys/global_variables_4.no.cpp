
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "opt_valgrind",
    "phc_suffix",
    "valgrind",
    "libphp",
    "working_dir",
    "php_exe",
    "php",
    "opt_long",
    "phc",
    "working_directory",
    "strict",
    "status_files",
    "log_directory",
    "subject_dir",
    "opt_verbose",
    "plugin_dir",
    "date_string",
    "base_dir",
  };
  if (idx >= 0 && idx < 30) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(opt_valgrind);
    case 13: return GV(phc_suffix);
    case 14: return GV(valgrind);
    case 15: return GV(libphp);
    case 16: return GV(working_dir);
    case 17: return GV(php_exe);
    case 18: return GV(php);
    case 19: return GV(opt_long);
    case 20: return GV(phc);
    case 21: return GV(working_directory);
    case 22: return GV(strict);
    case 23: return GV(status_files);
    case 24: return GV(log_directory);
    case 25: return GV(subject_dir);
    case 26: return GV(opt_verbose);
    case 27: return GV(plugin_dir);
    case 28: return GV(date_string);
    case 29: return GV(base_dir);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
