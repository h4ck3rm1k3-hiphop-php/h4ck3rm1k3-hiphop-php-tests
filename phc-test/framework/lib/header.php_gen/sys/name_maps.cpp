
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "adjusted_name", "phc-test/framework/lib/header.php",
  "blue_string", "phc-test/framework/lib/header.php",
  "check_for_plugin", "phc-test/framework/lib/header.php",
  "check_for_program", "phc-test/framework/lib/header.php",
  "close_status_files", "phc-test/framework/lib/header.php",
  "complete_exec", "phc-test/framework/lib/header.php",
  "copy_to_working_dir", "phc-test/framework/lib/header.php",
  "date_string", "phc-test/framework/lib/header.php",
  "diff", "phc-test/framework/lib/header.php",
  "get_dependent_filename", "phc-test/framework/lib/header.php",
  "get_phc", "phc-test/framework/lib/header.php",
  "get_phc_command_line", "phc-test/framework/lib/header.php",
  "get_phc_compile_plugin", "phc-test/framework/lib/header.php",
  "get_php", "phc-test/framework/lib/header.php",
  "get_php_command_line", "phc-test/framework/lib/header.php",
  "green_string", "phc-test/framework/lib/header.php",
  "homogenize_all", "phc-test/framework/lib/header.php",
  "homogenize_break_levels", "phc-test/framework/lib/header.php",
  "homogenize_filenames_and_line_numbers", "phc-test/framework/lib/header.php",
  "homogenize_object_count", "phc-test/framework/lib/header.php",
  "homogenize_reference_count", "phc-test/framework/lib/header.php",
  "homogenize_xml", "phc-test/framework/lib/header.php",
  "is_32_bit", "phc-test/framework/lib/header.php",
  "kill_properly", "phc-test/framework/lib/header.php",
  "log_failure", "phc-test/framework/lib/header.php",
  "log_status", "phc-test/framework/lib/header.php",
  "open_status_files", "phc-test/framework/lib/header.php",
  "phc_assert", "phc-test/framework/lib/header.php",
  "phc_error_handler", "phc-test/framework/lib/header.php",
  "phc_unreachable", "phc-test/framework/lib/header.php",
  "read_dependency", "phc-test/framework/lib/header.php",
  "red_string", "phc-test/framework/lib/header.php",
  "reset_string", "phc-test/framework/lib/header.php",
  "strip_colour", "phc-test/framework/lib/header.php",
  "strip_console_codes", "phc-test/framework/lib/header.php",
  "wd_name", "phc-test/framework/lib/header.php",
  "write_dependencies", "phc-test/framework/lib/header.php",
  "write_status", "phc-test/framework/lib/header.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
