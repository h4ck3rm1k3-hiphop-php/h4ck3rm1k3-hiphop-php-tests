
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x52B182B0A94E9C41LL, php, 18);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      HASH_INDEX(0x2F2AA415EFAE460ELL, base_dir, 29);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      HASH_INDEX(0x6C2E2CD6695D83D1LL, log_directory, 24);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x01F18996FC6E6193LL, php_exe, 17);
      break;
    case 27:
      HASH_INDEX(0x2FF01C2058B0B21BLL, opt_verbose, 26);
      break;
    case 34:
      HASH_INDEX(0x7359B522C6BE3A22LL, phc_suffix, 13);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      HASH_INDEX(0x614447AB21C8FAE3LL, valgrind, 14);
      break;
    case 36:
      HASH_INDEX(0x2F475D4DE7208D24LL, opt_valgrind, 12);
      break;
    case 37:
      HASH_INDEX(0x093CA964B9E038E5LL, working_directory, 21);
      break;
    case 38:
      HASH_INDEX(0x1DA88EB5034B9D26LL, plugin_dir, 27);
      break;
    case 41:
      HASH_INDEX(0x38BA10F6BA1F67E9LL, working_dir, 16);
      break;
    case 42:
      HASH_INDEX(0x67EA86E1CF55026ALL, phc, 20);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 48:
      HASH_INDEX(0x5C390FD428B50E30LL, opt_long, 19);
      HASH_INDEX(0x7FD127A282D4A0F0LL, strict, 22);
      break;
    case 51:
      HASH_INDEX(0x6FB2F3D573DE52F3LL, subject_dir, 25);
      break;
    case 55:
      HASH_INDEX(0x626A556D885E55F7LL, status_files, 23);
      break;
    case 61:
      HASH_INDEX(0x0B5BC0452CFBA5FDLL, libphp, 15);
      break;
    case 62:
      HASH_INDEX(0x18532A5EE8396FBELL, date_string, 28);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 30) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
