
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/labels.h>
#include <php/phc-test/framework/lib/plugin_test.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/plugin_test.php line 11 */
Variant c_plugintest::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_plugintest::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_plugintest::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_plugintest::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_plugintest::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_plugintest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_plugintest::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_plugintest::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(plugintest)
ObjectData *c_plugintest::create(Variant v_plugin_name, Variant v_other_commands //  = ""
) {
  init();
  t___construct(v_plugin_name, v_other_commands);
  return this;
}
ObjectData *c_plugintest::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_plugintest::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_plugintest::cloneImpl() {
  c_plugintest *obj = NEW(c_plugintest)();
  cloneSet(obj);
  return obj;
}
void c_plugintest::cloneSet(c_plugintest *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_plugintest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(params.rvalAt(0)), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_plugintest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(a0), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_plugintest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_plugintest$os_get(const char *s) {
  return c_plugintest::os_get(s, -1);
}
Variant &cw_plugintest$os_lval(const char *s) {
  return c_plugintest::os_lval(s, -1);
}
Variant cw_plugintest$os_constant(const char *s) {
  return c_plugintest::os_constant(s);
}
Variant cw_plugintest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_plugintest::os_invoke(c, s, params, -1, fatal);
}
void c_plugintest::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/lib/plugin_test.php line 13 */
void c_plugintest::t___construct(Variant v_plugin_name, Variant v_other_commands //  = ""
) {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("other_commands", 0x5C8BD712AEDB2CB8LL) = v_other_commands);
  (o_lval("commands_as_string", 0x12817F1DA4007A7CLL) = LINE(16,x_preg_replace("/\\s/", " ", v_other_commands)));
  (o_lval("plugin_name", 0x6C39659FB8B40C36LL) = v_plugin_name);
  LINE(18,c_asynctest::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/plugin_test.php line 21 */
Array c_plugintest::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::get_dependent_test_names);
  Array v_result;

  (v_result = ScalarArrays::sa_[4]);
  if (toBoolean(o_get("other_commands", 0x5C8BD712AEDB2CB8LL))) v_result.append((o_get("plugin_name", 0x6C39659FB8B40C36LL)));
  return v_result;
} /* function */
/* SRC: phc-test/framework/lib/plugin_test.php line 31 */
Variant c_plugintest::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::get_test_subjects);
  return LINE(33,f_get_all_scripts());
} /* function */
/* SRC: phc-test/framework/lib/plugin_test.php line 36 */
Variant c_plugintest::t_check_prerequisites() {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::check_prerequisites);
  return LINE(38,invoke_failed("check_for_plugin", Array(ArrayInit(1).set(0, concat("tests/", toString(o_get("plugin_name", 0x6C39659FB8B40C36LL)))).create()), 0x0000000072D1DD2DLL));
} /* function */
/* SRC: phc-test/framework/lib/plugin_test.php line 41 */
String c_plugintest::t_get_name() {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::get_name);
  String v_name;

  (v_name = toString("plugin_") + toString(o_get("plugin_name", 0x6C39659FB8B40C36LL)));
  if (toBoolean(o_get("other_commands", 0x5C8BD712AEDB2CB8LL))) concat_assign(v_name, concat("_with_", toString(o_get("commands_as_string", 0x12817F1DA4007A7CLL))));
  return v_name;
} /* function */
/* SRC: phc-test/framework/lib/plugin_test.php line 49 */
String c_plugintest::t_get_command_line(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::get_command_line);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  Variant v_plugin;
  Variant v_commands;

  {
  }
  (v_plugin = o_get("plugin_name", 0x6C39659FB8B40C36LL));
  (v_commands = o_get("other_commands", 0x5C8BD712AEDB2CB8LL));
  return concat_rev(LINE(54,concat6(" --run ", toString(gv_plugin_dir), "/tests/", toString(v_plugin), ".la ", toString(v_subject))), concat3(toString(gv_phc), " ", toString(v_commands)));
} /* function */
/* SRC: phc-test/framework/lib/plugin_test.php line 57 */
void c_plugintest::t_finish(CVarRef v_async) {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::finish);
  if (((same(toObject(v_async).o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "Success\n")) && (same(toObject(v_async).o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0LL))) && (same(toObject(v_async).o_get("errs", 0x15E266045DABA002LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), ""))) {
    LINE(63,t_async_success(v_async));
  }
  else {
    LINE(67,t_async_failure("Not success or skipped", v_async));
  }
} /* function */
/* SRC: phc-test/framework/lib/plugin_test.php line 71 */
void c_plugintest::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(PluginTest, PluginTest::run_test);
  p_asyncbundle v_async;

  ((Object)((v_async = ((Object)(LINE(73,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(75,t_get_command_line(v_subject))), 0x77CFA1EEF01BCA90LL);
  (v_async.o_lval("final", 0x5192930B2145036ELL) = "finish");
  LINE(79,v_async->t_start());
} /* function */
Object co_plugintest(CArrRef params, bool init /* = true */) {
  return Object(p_plugintest(NEW(c_plugintest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$plugin_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/plugin_test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$plugin_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(9,pm_php$phc_test$framework$lib$async_test_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
