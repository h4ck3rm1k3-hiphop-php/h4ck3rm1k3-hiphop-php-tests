
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "asyncbundle", "phc-test/framework/lib/async_test.php",
  "asynctest", "phc-test/framework/lib/async_test.php",
  "plugintest", "phc-test/framework/lib/plugin_test.php",
  "test", "phc-test/framework/lib/test.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "create_label_struct", "phc-test/framework/lib/labels.php",
  "get_all_plugins", "phc-test/framework/lib/labels.php",
  "get_all_scripts", "phc-test/framework/lib/labels.php",
  "get_all_scripts_in_dir", "phc-test/framework/lib/labels.php",
  "get_interpretable_scripts", "phc-test/framework/lib/labels.php",
  "get_non_interpretable_scripts", "phc-test/framework/lib/labels.php",
  "get_scripts_labelled", "phc-test/framework/lib/labels.php",
  "inst", "phc-test/framework/lib/async_test.php",
  "is_labelled", "phc-test/framework/lib/labels.php",
  "process_label_file_line", "phc-test/framework/lib/labels.php",
  "skip_3rdparty", "phc-test/framework/lib/labels.php",
  "strip_long_files", "phc-test/framework/lib/labels.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
