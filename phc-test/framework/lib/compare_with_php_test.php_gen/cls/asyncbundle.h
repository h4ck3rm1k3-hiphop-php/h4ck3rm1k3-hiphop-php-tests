
#ifndef __GENERATED_cls_asyncbundle_h__
#define __GENERATED_cls_asyncbundle_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/async_test.php line 23 */
class c_asyncbundle : virtual public ObjectData {
  BEGIN_CLASS_MAP(asyncbundle)
  END_CLASS_MAP(asyncbundle)
  DECLARE_CLASS(asyncbundle, AsyncBundle, ObjectData)
  void init();
  public: void t___construct(Variant v_object, Variant v_subject);
  public: ObjectData *create(Variant v_object, Variant v_subject);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_start();
  public: Variant t_get_command();
  public: Variant t_get_in();
  public: void t_continuation();
  public: void t_read_streams();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_asyncbundle_h__
