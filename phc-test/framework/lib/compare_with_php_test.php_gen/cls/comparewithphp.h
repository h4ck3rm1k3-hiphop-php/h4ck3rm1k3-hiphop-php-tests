
#ifndef __GENERATED_cls_comparewithphp_h__
#define __GENERATED_cls_comparewithphp_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/compare_with_php_test.php line 12 */
class c_comparewithphp : virtual public c_asynctest {
  BEGIN_CLASS_MAP(comparewithphp)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(comparewithphp)
  DECLARE_CLASS(comparewithphp, CompareWithPHP, asynctest)
  void init();
  public: void t___construct(Variant v_name, Variant v_command_line, Variant v_dependecy);
  public: ObjectData *create(Variant v_name, Variant v_command_line, Variant v_dependecy);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_get_test_subjects();
  public: Variant t_run_command(Variant v_command, Variant v_subject);
  public: Variant t_get_command_line1(Variant v_subject);
  public: String t_get_command_line2(Variant v_subject);
  public: Variant t_homogenize_output(Variant v_output, CVarRef v_bundle);
  public: Variant t_get_name();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_comparewithphp_h__
