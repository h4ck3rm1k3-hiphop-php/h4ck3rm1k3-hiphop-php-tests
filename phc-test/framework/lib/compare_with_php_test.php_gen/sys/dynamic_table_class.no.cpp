
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_asyncbundle(CArrRef params, bool init = true);
Variant cw_asyncbundle$os_get(const char *s);
Variant &cw_asyncbundle$os_lval(const char *s);
Variant cw_asyncbundle$os_constant(const char *s);
Variant cw_asyncbundle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_asynctest(CArrRef params, bool init = true);
Variant cw_asynctest$os_get(const char *s);
Variant &cw_asynctest$os_lval(const char *s);
Variant cw_asynctest$os_constant(const char *s);
Variant cw_asynctest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test(CArrRef params, bool init = true);
Variant cw_test$os_get(const char *s);
Variant &cw_test$os_lval(const char *s);
Variant cw_test$os_constant(const char *s);
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_comparewithphp(CArrRef params, bool init = true);
Variant cw_comparewithphp$os_get(const char *s);
Variant &cw_comparewithphp$os_lval(const char *s);
Variant cw_comparewithphp$os_constant(const char *s);
Variant cw_comparewithphp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_CREATE_OBJECT(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_CREATE_OBJECT(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x13C5FA969FBED37ELL, asynctest);
      break;
    case 7:
      HASH_CREATE_OBJECT(0x37349B25A0ED29E7LL, test);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_INVOKE_STATIC_METHOD(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x13C5FA969FBED37ELL, asynctest);
      break;
    case 7:
      HASH_INVOKE_STATIC_METHOD(0x37349B25A0ED29E7LL, test);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_GET_STATIC_PROPERTY(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x13C5FA969FBED37ELL, asynctest);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY(0x37349B25A0ED29E7LL, test);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_GET_STATIC_PROPERTY_LV(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x13C5FA969FBED37ELL, asynctest);
      break;
    case 7:
      HASH_GET_STATIC_PROPERTY_LV(0x37349B25A0ED29E7LL, test);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_GET_CLASS_CONSTANT(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x13C5FA969FBED37ELL, asynctest);
      break;
    case 7:
      HASH_GET_CLASS_CONSTANT(0x37349B25A0ED29E7LL, test);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
