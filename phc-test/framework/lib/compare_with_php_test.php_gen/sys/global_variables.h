
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(subject_dir)
    GVS(label_file)
    GVS(third_party_label_file)
    GVS(default_labels)
    GVS(non_default_labels)
    GVS(labels)
    GVS(opposite_label)
    GVS(exceptions)
    GVS(label_struct)
    GVS(opt_long)
    GVS(opt_one)
    GVS(phc)
    GVS(plugin_dir)
    GVS(base_dir)
    GVS(working_directory)
    GVS(opt_reduce)
    GVS(opt_verbose)
    GVS(opt_no_progress_bar)
    GVS(php)
    GVS(tests)
    GVS(opt_quick)
  END_GVS(21)

  // Dynamic Constants
  Variant k_PHC_NUM_PROCS;

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables
  Array s_comparewithphp_DupIdcache;

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$framework$lib$compare_with_php_test_php;
  bool run_pm_php$phc_test$framework$lib$async_test_php;
  bool run_pm_php$phc_test$framework$lib$test_php;
  bool run_pm_php$phc_test$framework$lib$labels_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 33;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[7];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
