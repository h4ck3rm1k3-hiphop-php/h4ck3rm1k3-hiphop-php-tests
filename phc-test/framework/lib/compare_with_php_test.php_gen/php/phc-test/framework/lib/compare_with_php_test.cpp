
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/compare_with_php_test.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 12 */
Variant c_comparewithphp::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x3E854EA2326AD070LL, g->s_comparewithphp_DupIdcache,
                  cache);
      break;
    default:
      break;
  }
  return c_asynctest::os_get(s, hash);
}
Variant &c_comparewithphp::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_comparewithphp::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_comparewithphp::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_comparewithphp::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_comparewithphp::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_comparewithphp::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_comparewithphp::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(comparewithphp)
ObjectData *c_comparewithphp::create(Variant v_name, Variant v_command_line, Variant v_dependecy) {
  init();
  t___construct(v_name, v_command_line, v_dependecy);
  return this;
}
ObjectData *c_comparewithphp::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_comparewithphp::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_comparewithphp::cloneImpl() {
  c_comparewithphp *obj = NEW(c_comparewithphp)();
  cloneSet(obj);
  return obj;
}
void c_comparewithphp::cloneSet(c_comparewithphp *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_comparewithphp::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_comparewithphp::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0, a1));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_comparewithphp::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_comparewithphp$os_get(const char *s) {
  return c_comparewithphp::os_get(s, -1);
}
Variant &cw_comparewithphp$os_lval(const char *s) {
  return c_comparewithphp::os_lval(s, -1);
}
Variant cw_comparewithphp$os_constant(const char *s) {
  return c_comparewithphp::os_constant(s);
}
Variant cw_comparewithphp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_comparewithphp::os_invoke(c, s, params, -1, fatal);
}
void c_comparewithphp::init() {
  c_asynctest::init();
}
void c_comparewithphp::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_comparewithphp_DupIdcache = ScalarArrays::sa_[3];
}
void csi_comparewithphp() {
  c_comparewithphp::os_static_initializer();
}
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 14 */
void c_comparewithphp::t___construct(Variant v_name, Variant v_command_line, Variant v_dependecy) {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = v_name);
  (o_lval("command_line", 0x3CDA59286E299AF9LL) = v_command_line);
  (o_lval("dependencies", 0x36DCDDF5BB1EBEBDLL) = Array(ArrayInit(1).set(0, v_dependecy).create()));
  LINE(19,c_asynctest::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 24 */
Variant c_comparewithphp::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::get_test_subjects);
  return LINE(26,f_get_interpretable_scripts());
} /* function */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 30 */
Variant c_comparewithphp::t_run_command(Variant v_command, Variant v_subject) {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::run_command);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_results;

  if (equal(v_command, LINE(32,t_get_command_line1(v_subject)))) {
    if (isset(g->s_comparewithphp_DupIdcache, v_subject)) {
      return g->s_comparewithphp_DupIdcache.rvalAt(v_subject);
    }
    else {
      (v_results = LINE(42,throw_fatal("unknown method asynctest::run_command", ((void*)NULL))));
      g->s_comparewithphp_DupIdcache.set(v_subject, (v_results));
      return v_results;
    }
  }
  else {
    return LINE(51,throw_fatal("unknown method asynctest::run_command", ((void*)NULL)));
  }
  return null;
} /* function */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 55 */
Variant c_comparewithphp::t_get_command_line1(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::get_command_line1);
  return LINE(57,invoke_failed("get_php_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000033B2020BLL));
} /* function */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 60 */
String c_comparewithphp::t_get_command_line2(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::get_command_line2);
  Variant v_command;
  Variant v_pipe_command;

  (v_command = o_get("command_line", 0x3CDA59286E299AF9LL));
  (v_pipe_command = LINE(63,invoke_failed("get_php_command_line", Array(ArrayInit(2).set(0, ref(v_subject)).set(1, "pipe").create()), 0x0000000033B2020BLL)));
  return concat_rev(LINE(64,concat4(" ", toString(v_command), " | ", toString(v_pipe_command))), toString(invoke_failed("get_phc_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000081EE2BC9LL)));
} /* function */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 67 */
Variant c_comparewithphp::t_homogenize_output(Variant v_output, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::homogenize_output);
  return LINE(69,invoke_failed("homogenize_all", Array(ArrayInit(2).set(0, ref(v_output)).set(1, ref(toObject(v_bundle).o_lval("subject", 0x0EDD9FCC9525C3B2LL))).create()), 0x00000000E9837FAELL));
} /* function */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 72 */
Variant c_comparewithphp::t_get_name() {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::get_name);
  return o_get("name", 0x0BCDB293DC3CBDDCLL);
} /* function */
/* SRC: phc-test/framework/lib/compare_with_php_test.php line 77 */
void c_comparewithphp::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(CompareWithPHP, CompareWithPHP::run_test);
  p_asyncbundle v_async;

  ((Object)((v_async = ((Object)(LINE(79,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(81,t_get_command_line1(v_subject))), 0x77CFA1EEF01BCA90LL);
  lval(v_async.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(0LL, ("homogenize_output"), 0x77CFA1EEF01BCA90LL);
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(1LL, (LINE(84,t_get_command_line2(v_subject))), 0x5BCA7C69B794F8CELL);
  lval(v_async.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(1LL, ("homogenize_output"), 0x5BCA7C69B794F8CELL);
  (v_async.o_lval("final", 0x5192930B2145036ELL) = "two_command_finish");
  LINE(89,v_async->t_start());
} /* function */
Object co_comparewithphp(CArrRef params, bool init /* = true */) {
  return Object(p_comparewithphp(NEW(c_comparewithphp)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$compare_with_php_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/compare_with_php_test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$compare_with_php_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(9,pm_php$phc_test$framework$lib$async_test_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
