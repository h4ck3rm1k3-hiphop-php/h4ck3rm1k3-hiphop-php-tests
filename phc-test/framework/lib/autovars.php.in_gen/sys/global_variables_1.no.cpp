
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 5:
      HASH_RETURN(0x59D14F33476C2705LL, g->GV(dot),
                  dot);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x01F18996FC6E6193LL, g->GV(php_exe),
                  php_exe);
      break;
    case 28:
      HASH_RETURN(0x3901F738F39EE99CLL, g->GV(graphviz_gc),
                  graphviz_gc);
      break;
    case 32:
      HASH_RETURN(0x5522393FED31B4E0LL, g->GV(pkglibdir),
                  pkglibdir);
      break;
    case 34:
      HASH_RETURN(0x7359B522C6BE3A22LL, g->GV(phc_suffix),
                  phc_suffix);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      HASH_RETURN(0x614447AB21C8FAE3LL, g->GV(valgrind),
                  valgrind);
      break;
    case 38:
      HASH_RETURN(0x25C6A69D9674BBA6LL, g->GV(php_path),
                  php_path);
      HASH_RETURN(0x0DC38D7049391A66LL, g->GV(trunk_CPPFLAGS),
                  trunk_CPPFLAGS);
      break;
    case 43:
      HASH_RETURN(0x210CB79045B0086BLL, g->GV(bindir),
                  bindir);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 54:
      HASH_RETURN(0x3725D0B1F30F14B6LL, g->GV(xerces_compiled_in),
                  xerces_compiled_in);
      break;
    case 59:
      HASH_RETURN(0x630DB2468196C37BLL, g->GV(gcc),
                  gcc);
      break;
    case 61:
      HASH_RETURN(0x0B5BC0452CFBA5FDLL, g->GV(libphp),
                  libphp);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
