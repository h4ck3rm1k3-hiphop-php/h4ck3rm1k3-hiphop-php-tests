
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "valgrind",
    "graphviz_gc",
    "dot",
    "php_exe",
    "gcc",
    "php_path",
    "libphp",
    "bindir",
    "pkglibdir",
    "phc_suffix",
    "trunk_CPPFLAGS",
    "xerces_compiled_in",
  };
  if (idx >= 0 && idx < 24) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(valgrind);
    case 13: return GV(graphviz_gc);
    case 14: return GV(dot);
    case 15: return GV(php_exe);
    case 16: return GV(gcc);
    case 17: return GV(php_path);
    case 18: return GV(libphp);
    case 19: return GV(bindir);
    case 20: return GV(pkglibdir);
    case 21: return GV(phc_suffix);
    case 22: return GV(trunk_CPPFLAGS);
    case 23: return GV(xerces_compiled_in);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
