
#include <php/phc-test/framework/lib/autovars.php.in.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$framework$lib$autovars_php_in(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/autovars.php.in);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$autovars_php_in;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_valgrind __attribute__((__unused__)) = (variables != gVariables) ? variables->get("valgrind") : g->GV(valgrind);
  Variant &v_graphviz_gc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("graphviz_gc") : g->GV(graphviz_gc);
  Variant &v_dot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dot") : g->GV(dot);
  Variant &v_php_exe __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_exe") : g->GV(php_exe);
  Variant &v_gcc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("gcc") : g->GV(gcc);
  Variant &v_php_path __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_path") : g->GV(php_path);
  Variant &v_libphp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("libphp") : g->GV(libphp);
  Variant &v_bindir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bindir") : g->GV(bindir);
  Variant &v_pkglibdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pkglibdir") : g->GV(pkglibdir);
  Variant &v_phc_suffix __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phc_suffix") : g->GV(phc_suffix);
  Variant &v_trunk_CPPFLAGS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("trunk_CPPFLAGS") : g->GV(trunk_CPPFLAGS);
  Variant &v_xerces_compiled_in __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xerces_compiled_in") : g->GV(xerces_compiled_in);

  (v_valgrind = "@valgrind@");
  (v_graphviz_gc = "@graphviz_gc@");
  (v_dot = "@dot@");
  (v_php_exe = "@php@");
  (v_gcc = "@CC@");
  (v_php_path = "@php_install_path@");
  if (toBoolean(v_php_path)) {
    (v_libphp = toString(v_php_path) + toString("/lib"));
  }
  (v_bindir = "@bindir@");
  (v_pkglibdir = "@pkglibdir@");
  (v_phc_suffix = "@EXEEXT@");
  (v_trunk_CPPFLAGS = "'@AM_CPPFLAGS@'");
  (v_xerces_compiled_in = "@xerces_compiled_in@");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
