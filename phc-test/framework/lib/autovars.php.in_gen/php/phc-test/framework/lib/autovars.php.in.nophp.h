
#ifndef __GENERATED_php_phc_test_framework_lib_autovars_php_in_nophp_h__
#define __GENERATED_php_phc_test_framework_lib_autovars_php_in_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/autovars.php.in.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$lib$autovars_php_in(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_autovars_php_in_nophp_h__
