
#ifndef __GENERATED_cls_supportfiletest_h__
#define __GENERATED_cls_supportfiletest_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/support_file_test.php line 7 */
class c_supportfiletest : virtual public ObjectData {
  BEGIN_CLASS_MAP(supportfiletest)
  END_CLASS_MAP(supportfiletest)
  DECLARE_CLASS(supportfiletest, SupportFileTest, test)
  void init();
  // public: void t_get_support_filename(CVarRef v_subject) = 0;
  // public: void t_generate_support_file(CVarRef v_subject) = 0;
  public: Variant t_generate_support_files();
  public: void t_write_support_file(CVarRef v_out, Variant v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_supportfiletest_h__
