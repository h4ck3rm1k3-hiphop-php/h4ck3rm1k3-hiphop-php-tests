
#ifndef __GENERATED_cls_comparebackwards_h__
#define __GENERATED_cls_comparebackwards_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/compare_backwards.php line 28 */
class c_comparebackwards : virtual public ObjectData {
  BEGIN_CLASS_MAP(comparebackwards)
  END_CLASS_MAP(comparebackwards)
  DECLARE_CLASS(comparebackwards, CompareBackwards, asynctest)
  void init();
  public: void t___construct(Variant v_last_pass, Variant v_dump = "dump", Variant v_dependency = "BasicParseTest");
  public: ObjectData *create(Variant v_last_pass, Variant v_dump = "dump", Variant v_dependency = "BasicParseTest");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_get_name();
  public: Variant t_get_test_subjects();
  public: void t_run_test(Variant v_subject);
  public: bool t_check_output(CVarRef v_out, CVarRef v_err, CVarRef v_exit, Variant v_bundle);
  public: String t_get_command_line(Variant v_subject, CVarRef v_dump, CVarRef v_pass_name);
  public: void t_finish(Variant v_bundle);
  public: Variant t_homogenize_output(Variant v_output, CVarRef v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_comparebackwards_h__
