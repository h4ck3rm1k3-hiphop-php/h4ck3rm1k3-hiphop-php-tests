
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 2:
      HASH_INITIALIZED(0x47A8BEF0E404C882LL, g->GV(opposite_label),
                       opposite_label);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x58FBD12F28C4BF49LL, g->GV(label_file),
                       label_file);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      HASH_INITIALIZED(0x2F2AA415EFAE460ELL, g->GV(base_dir),
                       base_dir);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 22:
      HASH_INITIALIZED(0x73FDA0B4AF645216LL, g->GV(non_default_labels),
                       non_default_labels);
      break;
    case 26:
      HASH_INITIALIZED(0x17B5D58607FC7F1ALL, g->GV(default_labels),
                       default_labels);
      break;
    case 31:
      HASH_INITIALIZED(0x7076E3AEF02E9CDFLL, g->GV(opt_one),
                       opt_one);
      break;
    case 32:
      HASH_INITIALIZED(0x4B2063F3C49B8E60LL, g->GV(exceptions),
                       exceptions);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 38:
      HASH_INITIALIZED(0x1DA88EB5034B9D26LL, g->GV(plugin_dir),
                       plugin_dir);
      break;
    case 42:
      HASH_INITIALIZED(0x67EA86E1CF55026ALL, g->GV(phc),
                       phc);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      HASH_INITIALIZED(0x7DDBEB38F40F832ELL, g->GV(labels),
                       labels);
      break;
    case 48:
      HASH_INITIALIZED(0x5C390FD428B50E30LL, g->GV(opt_long),
                       opt_long);
      break;
    case 51:
      HASH_INITIALIZED(0x6FB2F3D573DE52F3LL, g->GV(subject_dir),
                       subject_dir);
      break;
    case 54:
      HASH_INITIALIZED(0x19D6108580CBB436LL, g->GV(label_struct),
                       label_struct);
      break;
    case 55:
      HASH_INITIALIZED(0x7D6AD2C5948BA3F7LL, g->GV(third_party_label_file),
                       third_party_label_file);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
