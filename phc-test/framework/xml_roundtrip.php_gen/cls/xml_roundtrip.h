
#ifndef __GENERATED_cls_xml_roundtrip_h__
#define __GENERATED_cls_xml_roundtrip_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/xml_roundtrip.php line 10 */
class c_xml_roundtrip : virtual public ObjectData {
  BEGIN_CLASS_MAP(xml_roundtrip)
  END_CLASS_MAP(xml_roundtrip)
  DECLARE_CLASS(xml_roundtrip, XML_roundtrip, asynctest)
  void init();
  public: Array t_get_dependent_test_names();
  public: bool t_check_prerequisites();
  public: Variant t_get_test_subjects();
  public: int64 t_get_phc_num_procs_divisor();
  public: void t_run_test(CVarRef v_subject);
  public: Variant t_homogenize_output(Variant v_string);
  public: String t_get_long_command_line(Variant v_subject, CVarRef v_last_pass);
  public: String t_get_command_line(Variant v_pass, CVarRef v_subject);
  public: void t_finish(Variant v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_xml_roundtrip_h__
