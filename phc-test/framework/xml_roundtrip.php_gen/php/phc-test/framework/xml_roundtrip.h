
#ifndef __GENERATED_php_phc_test_framework_xml_roundtrip_h__
#define __GENERATED_php_phc_test_framework_xml_roundtrip_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/xml_roundtrip.fw.h>

// Declarations
#include <cls/xml_roundtrip.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$xml_roundtrip_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_xml_roundtrip(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_xml_roundtrip_h__
