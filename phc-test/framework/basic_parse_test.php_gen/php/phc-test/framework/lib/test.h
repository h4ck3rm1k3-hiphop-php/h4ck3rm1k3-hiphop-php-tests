
#ifndef __GENERATED_php_phc_test_framework_lib_test_h__
#define __GENERATED_php_phc_test_framework_lib_test_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/test.fw.h>

// Declarations
#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$lib$test_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_test(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_test_h__
