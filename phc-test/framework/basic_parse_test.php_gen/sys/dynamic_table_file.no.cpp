
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$framework$basic_parse_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$async_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$labels_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_INCLUDE(0x318E90A414672272LL, "phc-test/framework/lib/async_test.php", php$phc_test$framework$lib$async_test_php);
      HASH_INCLUDE(0x2EA53DA2FCFB60B2LL, "phc-test/framework/lib/test.php", php$phc_test$framework$lib$test_php);
      break;
    case 5:
      HASH_INCLUDE(0x7C3E3F44EB8F077DLL, "phc-test/framework/basic_parse_test.php", php$phc_test$framework$basic_parse_test_php);
      HASH_INCLUDE(0x3177FA7D451CB955LL, "phc-test/framework/lib/labels.php", php$phc_test$framework$lib$labels_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
