
#include <php/phc-test/framework/source_vs_semantic_values.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/source_vs_semantic_values.php line 10 */
Variant c_sourcevssemantictest::os_get(const char *s, int64 hash) {
  return c_plugintest::os_get(s, hash);
}
Variant &c_sourcevssemantictest::os_lval(const char *s, int64 hash) {
  return c_plugintest::os_lval(s, hash);
}
void c_sourcevssemantictest::o_get(ArrayElementVec &props) const {
  c_plugintest::o_get(props);
}
bool c_sourcevssemantictest::o_exists(CStrRef s, int64 hash) const {
  return c_plugintest::o_exists(s, hash);
}
Variant c_sourcevssemantictest::o_get(CStrRef s, int64 hash) {
  return c_plugintest::o_get(s, hash);
}
Variant c_sourcevssemantictest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_plugintest::o_set(s, hash, v, forInit);
}
Variant &c_sourcevssemantictest::o_lval(CStrRef s, int64 hash) {
  return c_plugintest::o_lval(s, hash);
}
Variant c_sourcevssemantictest::os_constant(const char *s) {
  return c_plugintest::os_constant(s);
}
IMPLEMENT_CLASS(sourcevssemantictest)
ObjectData *c_sourcevssemantictest::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_sourcevssemantictest::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_sourcevssemantictest::cloneImpl() {
  c_sourcevssemantictest *obj = NEW(c_sourcevssemantictest)();
  cloneSet(obj);
  return obj;
}
void c_sourcevssemantictest::cloneSet(c_sourcevssemantictest *clone) {
  c_plugintest::cloneSet(clone);
}
Variant c_sourcevssemantictest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_plugintest::o_invoke(s, params, hash, fatal);
}
Variant c_sourcevssemantictest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_plugintest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sourcevssemantictest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_plugintest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sourcevssemantictest$os_get(const char *s) {
  return c_sourcevssemantictest::os_get(s, -1);
}
Variant &cw_sourcevssemantictest$os_lval(const char *s) {
  return c_sourcevssemantictest::os_lval(s, -1);
}
Variant cw_sourcevssemantictest$os_constant(const char *s) {
  return c_sourcevssemantictest::os_constant(s);
}
Variant cw_sourcevssemantictest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sourcevssemantictest::os_invoke(c, s, params, -1, fatal);
}
void c_sourcevssemantictest::init() {
  c_plugintest::init();
}
/* SRC: phc-test/framework/source_vs_semantic_values.php line 12 */
void c_sourcevssemantictest::t___construct() {
  INSTANCE_METHOD_INJECTION(SourceVsSemanticTest, SourceVsSemanticTest::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(14,throw_fatal("unknown class plugintest", ((void*)NULL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/source_vs_semantic_values.php line 17 */
Array c_sourcevssemantictest::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(SourceVsSemanticTest, SourceVsSemanticTest::get_dependent_test_names);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: phc-test/framework/source_vs_semantic_values.php line 22 */
String c_sourcevssemantictest::t_get_command_line(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(SourceVsSemanticTest, SourceVsSemanticTest::get_command_line);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_php __attribute__((__unused__)) = g->GV(php);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  {
  }
  return LINE(25,(assignCallTemp(eo_0, toString(gv_phc)),assignCallTemp(eo_2, concat6(toString(gv_plugin_dir), "/tests/source_vs_semantic_values.la ", toString(v_subject), " 2>&1 | ", toString(gv_php), " -d memory_limit=128M 2>&1")),concat3(eo_0, " --run ", eo_2)));
} /* function */
Object co_sourcevssemantictest(CArrRef params, bool init /* = true */) {
  return Object(p_sourcevssemantictest(NEW(c_sourcevssemantictest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$source_vs_semantic_values_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/source_vs_semantic_values.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$source_vs_semantic_values_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(9,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_sourcevssemantictest(p_sourcevssemantictest(NEWOBJ(c_sourcevssemantictest)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
