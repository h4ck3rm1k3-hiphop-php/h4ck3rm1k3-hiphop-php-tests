
#ifndef __GENERATED_php_phc_test_framework_source_vs_semantic_values_h__
#define __GENERATED_php_phc_test_framework_source_vs_semantic_values_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/source_vs_semantic_values.fw.h>

// Declarations
#include <cls/sourcevssemantictest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$source_vs_semantic_values_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sourcevssemantictest(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_source_vs_semantic_values_h__
