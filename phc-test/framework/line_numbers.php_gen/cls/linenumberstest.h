
#ifndef __GENERATED_cls_linenumberstest_h__
#define __GENERATED_cls_linenumberstest_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/line_numbers.php line 10 */
class c_linenumberstest : virtual public ObjectData {
  BEGIN_CLASS_MAP(linenumberstest)
  END_CLASS_MAP(linenumberstest)
  DECLARE_CLASS(linenumberstest, LineNumbersTest, test)
  void init();
  public: Variant t_get_test_subjects();
  public: void t_run_test(Variant v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linenumberstest_h__
