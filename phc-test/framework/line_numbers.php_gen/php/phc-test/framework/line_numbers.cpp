
#include <php/phc-test/framework/line_numbers.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/line_numbers.php line 10 */
Variant c_linenumberstest::os_get(const char *s, int64 hash) {
  return c_test::os_get(s, hash);
}
Variant &c_linenumberstest::os_lval(const char *s, int64 hash) {
  return c_test::os_lval(s, hash);
}
void c_linenumberstest::o_get(ArrayElementVec &props) const {
  c_test::o_get(props);
}
bool c_linenumberstest::o_exists(CStrRef s, int64 hash) const {
  return c_test::o_exists(s, hash);
}
Variant c_linenumberstest::o_get(CStrRef s, int64 hash) {
  return c_test::o_get(s, hash);
}
Variant c_linenumberstest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test::o_set(s, hash, v, forInit);
}
Variant &c_linenumberstest::o_lval(CStrRef s, int64 hash) {
  return c_test::o_lval(s, hash);
}
Variant c_linenumberstest::os_constant(const char *s) {
  return c_test::os_constant(s);
}
IMPLEMENT_CLASS(linenumberstest)
ObjectData *c_linenumberstest::cloneImpl() {
  c_linenumberstest *obj = NEW(c_linenumberstest)();
  cloneSet(obj);
  return obj;
}
void c_linenumberstest::cloneSet(c_linenumberstest *clone) {
  c_test::cloneSet(clone);
}
Variant c_linenumberstest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test::o_invoke(s, params, hash, fatal);
}
Variant c_linenumberstest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_test::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_linenumberstest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test::os_invoke(c, s, params, hash, fatal);
}
Variant cw_linenumberstest$os_get(const char *s) {
  return c_linenumberstest::os_get(s, -1);
}
Variant &cw_linenumberstest$os_lval(const char *s) {
  return c_linenumberstest::os_lval(s, -1);
}
Variant cw_linenumberstest$os_constant(const char *s) {
  return c_linenumberstest::os_constant(s);
}
Variant cw_linenumberstest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_linenumberstest::os_invoke(c, s, params, -1, fatal);
}
void c_linenumberstest::init() {
  c_test::init();
}
/* SRC: phc-test/framework/line_numbers.php line 12 */
Variant c_linenumberstest::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(LineNumbersTest, LineNumbersTest::get_test_subjects);
  return LINE(14,invoke_failed("get_all_scripts", Array(), 0x000000008819DA1ALL));
} /* function */
/* SRC: phc-test/framework/line_numbers.php line 17 */
void c_linenumberstest::t_run_test(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(LineNumbersTest, LineNumbersTest::run_test);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  Variant v_command;
  Variant v_out;
  Variant v_err;
  Variant v_exit;
  Variant v_contents;
  bool v_ok = false;
  Variant v_test_output;
  Array v_lines;
  Variant v_line;
  Variant v_matches;
  Variant v_match;
  Variant v_token;
  Variant v_line_number;
  String v_error;
  int v_file_size = 0;
  Variant v_actual_line;

  {
  }
  (v_command = LINE(22,concat5(toString(gv_phc), " --run ", toString(gv_plugin_dir), "/tests/line_numbers.la ", toString(v_subject))));
  df_lambda_1(LINE(23,invoke_failed("complete_exec", Array(ArrayInit(1).set(0, ref(v_command)).create()), 0x00000000739F6DDCLL)), v_out, v_err, v_exit);
  if (equal(LINE(25,x_strlen(toString(v_out))), 0LL)) {
    LINE(27,o_root_invoke_few_args("mark_skipped", 0x67446827CB764B08LL, 0));
  }
  else {
    (v_contents = LINE(32,x_file(toString(v_subject))));
    (v_ok = true);
    (v_test_output = "");
    (v_lines = LINE(44,(assignCallTemp(eo_1, x_rtrim(toString(v_out))),x_explode("--!!--!!--", eo_1))));
    {
      LOOP_COUNTER(1);
      for (ArrayIter iter3 = v_lines.begin("linenumberstest"); !iter3.end(); ++iter3) {
        LOOP_COUNTER_CHECK(1);
        v_line = iter3.second();
        {
          if (equal(v_line, "")) continue;
          (v_match = LINE(50,x_preg_match("/^(.*):(\\d+)$/ms", toString(v_line), ref(v_matches))));
          (v_token = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          (v_line_number = v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL));
          if ((((equal(v_line_number, 0LL)) || (!(toBoolean(v_match)))) || (!(isset(v_token)))) || (!(isset(v_line_number)))) {
            (v_error = LINE(59,concat3("Got line 0 for '", toString(v_token), "'\n")));
            concat_assign(v_test_output, v_error);
            (v_ok = false);
            continue;
          }
          if (not_more(LINE(66,x_count(v_contents)), v_line_number - 1LL)) {
            (v_file_size = LINE(68,x_count(v_contents)));
            (v_error = concat("Expected (line ", LINE(69,concat6(toString(v_line_number), "): '", toString(v_token), "'\nOnly ", toString(v_file_size), " lines\n"))));
            concat_assign(v_test_output, v_error);
            (v_ok = false);
          }
          else {
            (v_actual_line = v_contents.rvalAt(v_line_number - 1LL));
            if (!same((silenceInc(), silenceDec(LINE(77,x_strpos(toString(v_actual_line), v_token)))), false)) {
            }
            else {
              (v_error = concat("Expected (line ", LINE(83,concat6(toString(v_line_number), "): '", toString(v_token), "'\nActual line:'", toString(v_actual_line), "'\n"))));
              concat_assign(v_test_output, v_error);
              (v_ok = false);
            }
          }
        }
      }
    }
    if (v_ok) LINE(91,o_root_invoke_few_args("mark_success", 0x5209BAB457104C4BLL, 1, v_subject));
    else LINE(92,o_root_invoke_few_args("mark_failure", 0x1DCBF33DE3962EF0LL, 4, v_subject, v_command, 0LL, v_test_output));
  }
} /* function */
Object co_linenumberstest(CArrRef params, bool init /* = true */) {
  return Object(p_linenumberstest(NEW(c_linenumberstest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$line_numbers_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/line_numbers.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$line_numbers_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
