
#include <php/phc-test/framework/refcounts.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/refcounts.php line 12 */
Variant c_refcounts::os_get(const char *s, int64 hash) {
  return c_compiledvsinterpreted::os_get(s, hash);
}
Variant &c_refcounts::os_lval(const char *s, int64 hash) {
  return c_compiledvsinterpreted::os_lval(s, hash);
}
void c_refcounts::o_get(ArrayElementVec &props) const {
  c_compiledvsinterpreted::o_get(props);
}
bool c_refcounts::o_exists(CStrRef s, int64 hash) const {
  return c_compiledvsinterpreted::o_exists(s, hash);
}
Variant c_refcounts::o_get(CStrRef s, int64 hash) {
  return c_compiledvsinterpreted::o_get(s, hash);
}
Variant c_refcounts::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_compiledvsinterpreted::o_set(s, hash, v, forInit);
}
Variant &c_refcounts::o_lval(CStrRef s, int64 hash) {
  return c_compiledvsinterpreted::o_lval(s, hash);
}
Variant c_refcounts::os_constant(const char *s) {
  return c_compiledvsinterpreted::os_constant(s);
}
IMPLEMENT_CLASS(refcounts)
ObjectData *c_refcounts::cloneImpl() {
  c_refcounts *obj = NEW(c_refcounts)();
  cloneSet(obj);
  return obj;
}
void c_refcounts::cloneSet(c_refcounts *clone) {
  c_compiledvsinterpreted::cloneSet(clone);
}
Variant c_refcounts::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_compiledvsinterpreted::o_invoke(s, params, hash, fatal);
}
Variant c_refcounts::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_compiledvsinterpreted::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_refcounts::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_compiledvsinterpreted::os_invoke(c, s, params, hash, fatal);
}
Variant cw_refcounts$os_get(const char *s) {
  return c_refcounts::os_get(s, -1);
}
Variant &cw_refcounts$os_lval(const char *s) {
  return c_refcounts::os_lval(s, -1);
}
Variant cw_refcounts$os_constant(const char *s) {
  return c_refcounts::os_constant(s);
}
Variant cw_refcounts$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_refcounts::os_invoke(c, s, params, -1, fatal);
}
void c_refcounts::init() {
  c_compiledvsinterpreted::init();
}
/* SRC: phc-test/framework/refcounts.php line 15 */
Array c_refcounts::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(Refcounts, Refcounts::get_dependent_test_names);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: phc-test/framework/refcounts.php line 20 */
String c_refcounts::t_get_phc_command(Variant v_subject, CVarRef v_exe_name) {
  INSTANCE_METHOD_INJECTION(Refcounts, Refcounts::get_phc_command);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  {
  }
  return concat_rev(LINE(23,concat4(" -c --run ", toString(gv_plugin_dir), "/tools/debug_zval.la -o ", toString(v_exe_name))), toString(invoke_failed("get_phc_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000081EE2BC9LL)));
} /* function */
/* SRC: phc-test/framework/refcounts.php line 26 */
String c_refcounts::t_get_php_command(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(Refcounts, Refcounts::get_php_command);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  {
  }
  return concat_rev(LINE(29,(assignCallTemp(eo_1, toString(gv_plugin_dir)),assignCallTemp(eo_3, toString(v_subject)),assignCallTemp(eo_5, toString(invoke_failed("get_php_command_line", Array(ArrayInit(2).set(0, ref(v_subject)).set(1, "pipe").create()), 0x0000000033B2020BLL))),concat6("/tools/debug_zval.la --convert-uppered --dump=", eo_1, "/tools/debug_zval.la ", eo_3, " | ", eo_5))), concat3(toString(gv_phc), " --run ", toString(gv_plugin_dir)));
} /* function */
Object co_refcounts(CArrRef params, bool init /* = true */) {
  return Object(p_refcounts(NEW(c_refcounts)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$refcounts_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/refcounts.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$refcounts_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(11,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_refcounts(p_refcounts(NEWOBJ(c_refcounts)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
