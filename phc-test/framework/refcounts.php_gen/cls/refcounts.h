
#ifndef __GENERATED_cls_refcounts_h__
#define __GENERATED_cls_refcounts_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/refcounts.php line 12 */
class c_refcounts : virtual public ObjectData {
  BEGIN_CLASS_MAP(refcounts)
  END_CLASS_MAP(refcounts)
  DECLARE_CLASS(refcounts, Refcounts, compiledvsinterpreted)
  void init();
  public: Array t_get_dependent_test_names();
  public: String t_get_phc_command(Variant v_subject, CVarRef v_exe_name);
  public: String t_get_php_command(Variant v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_refcounts_h__
