
#include <php/phc-test/framework/reparse_unparsed.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/reparse_unparsed.php line 10 */
Variant c_reparseunparsed::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_reparseunparsed::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_reparseunparsed::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_reparseunparsed::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_reparseunparsed::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_reparseunparsed::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_reparseunparsed::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_reparseunparsed::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(reparseunparsed)
ObjectData *c_reparseunparsed::cloneImpl() {
  c_reparseunparsed *obj = NEW(c_reparseunparsed)();
  cloneSet(obj);
  return obj;
}
void c_reparseunparsed::cloneSet(c_reparseunparsed *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_reparseunparsed::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_reparseunparsed::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_reparseunparsed::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_reparseunparsed$os_get(const char *s) {
  return c_reparseunparsed::os_get(s, -1);
}
Variant &cw_reparseunparsed$os_lval(const char *s) {
  return c_reparseunparsed::os_lval(s, -1);
}
Variant cw_reparseunparsed$os_constant(const char *s) {
  return c_reparseunparsed::os_constant(s);
}
Variant cw_reparseunparsed$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_reparseunparsed::os_invoke(c, s, params, -1, fatal);
}
void c_reparseunparsed::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/reparse_unparsed.php line 12 */
Variant c_reparseunparsed::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(ReparseUnparsed, ReparseUnparsed::get_test_subjects);
  return LINE(14,invoke_failed("get_all_scripts", Array(), 0x000000008819DA1ALL));
} /* function */
/* SRC: phc-test/framework/reparse_unparsed.php line 17 */
Array c_reparseunparsed::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(ReparseUnparsed, ReparseUnparsed::get_dependent_test_names);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: phc-test/framework/reparse_unparsed.php line 22 */
void c_reparseunparsed::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(ReparseUnparsed, ReparseUnparsed::run_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Object v_async;

  (v_async = LINE(26,create_object("asyncbundle", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, v_subject).create()))));
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(28,concat5("cat ", toString(v_subject), " | ", toString(gv_phc), " --pretty-print"))), 0x77CFA1EEF01BCA90LL);
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(1LL, (LINE(29,concat6(toString(gv_phc), " --pretty-print ", toString(v_subject), " 2>&1 | ", toString(gv_phc), " --pretty-print"))), 0x5BCA7C69B794F8CELL);
  (v_async.o_lval("final", 0x5192930B2145036ELL) = "two_command_finish");
  LINE(33,v_async->o_invoke_few_args("start", 0x3970634433FD3E52LL, 0));
} /* function */
Object co_reparseunparsed(CArrRef params, bool init /* = true */) {
  return Object(p_reparseunparsed(NEW(c_reparseunparsed)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$reparse_unparsed_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/reparse_unparsed.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$reparse_unparsed_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(9,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_reparseunparsed(p_reparseunparsed(NEWOBJ(c_reparseunparsed)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
