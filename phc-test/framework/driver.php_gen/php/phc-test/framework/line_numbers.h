
#ifndef __GENERATED_php_phc_test_framework_line_numbers_h__
#define __GENERATED_php_phc_test_framework_line_numbers_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/line_numbers.fw.h>

// Declarations
#include <cls/linenumberstest.h>
#include <php/phc-test/framework/lib/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$line_numbers_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_linenumberstest(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_line_numbers_h__
