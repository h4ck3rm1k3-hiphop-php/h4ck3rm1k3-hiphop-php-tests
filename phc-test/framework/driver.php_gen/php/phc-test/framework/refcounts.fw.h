
#ifndef __GENERATED_php_phc_test_framework_refcounts_fw_h__
#define __GENERATED_php_phc_test_framework_refcounts_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(refcounts)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/framework/compiled_vs_interpreted.fw.h>

#endif // __GENERATED_php_phc_test_framework_refcounts_fw_h__
