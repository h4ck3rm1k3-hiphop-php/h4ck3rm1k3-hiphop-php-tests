
#ifndef __GENERATED_php_phc_test_framework_compiled_vs_interpreted_h__
#define __GENERATED_php_phc_test_framework_compiled_vs_interpreted_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/compiled_vs_interpreted.fw.h>

// Declarations
#include <cls/compiledvsinterpreted.h>
#include <php/phc-test/framework/lib/async_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$compiled_vs_interpreted_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_compiledvsinterpreted(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_compiled_vs_interpreted_h__
