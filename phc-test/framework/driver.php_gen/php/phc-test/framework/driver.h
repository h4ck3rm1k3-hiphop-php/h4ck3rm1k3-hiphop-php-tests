
#ifndef __GENERATED_php_phc_test_framework_driver_h__
#define __GENERATED_php_phc_test_framework_driver_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/driver.fw.h>

// Declarations
#include <php/phc-test/framework/lib/compare_backwards.h>
#include <php/phc-test/framework/lib/compare_with_php_test.h>
#include <php/phc-test/framework/lib/pass_dump.h>
#include <php/phc-test/framework/lib/plugin_test.h>
#include <php/phc-test/framework/lib/regression.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$driver_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_driver_h__
