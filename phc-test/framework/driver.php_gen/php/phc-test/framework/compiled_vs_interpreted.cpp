
#include <php/phc-test/framework/compiled_vs_interpreted.h>
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 11 */
Variant c_compiledvsinterpreted::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_compiledvsinterpreted::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_compiledvsinterpreted::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_compiledvsinterpreted::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_compiledvsinterpreted::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_compiledvsinterpreted::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_compiledvsinterpreted::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_compiledvsinterpreted::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(compiledvsinterpreted)
ObjectData *c_compiledvsinterpreted::cloneImpl() {
  c_compiledvsinterpreted *obj = NEW(c_compiledvsinterpreted)();
  cloneSet(obj);
  return obj;
}
void c_compiledvsinterpreted::cloneSet(c_compiledvsinterpreted *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_compiledvsinterpreted::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 15:
      HASH_GUARD(0x72F2C7FCFE2FB14FLL, get_phc_command) {
        return (t_get_phc_command(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x42412A2BC4988AA2LL, get_php_command) {
        return (t_get_php_command(params.rvalAt(0)));
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(params.rvalAt(0)), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(params.rvalAt(0)), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_compiledvsinterpreted::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 15:
      HASH_GUARD(0x72F2C7FCFE2FB14FLL, get_phc_command) {
        return (t_get_phc_command(a0, a1));
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x42412A2BC4988AA2LL, get_php_command) {
        return (t_get_php_command(a0));
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(a0), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0, a1));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(a0), a1));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(a0), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_compiledvsinterpreted::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_compiledvsinterpreted$os_get(const char *s) {
  return c_compiledvsinterpreted::os_get(s, -1);
}
Variant &cw_compiledvsinterpreted$os_lval(const char *s) {
  return c_compiledvsinterpreted::os_lval(s, -1);
}
Variant cw_compiledvsinterpreted$os_constant(const char *s) {
  return c_compiledvsinterpreted::os_constant(s);
}
Variant cw_compiledvsinterpreted$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_compiledvsinterpreted::os_invoke(c, s, params, -1, fatal);
}
void c_compiledvsinterpreted::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 13 */
bool c_compiledvsinterpreted::t_check_prerequisites() {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::check_prerequisites);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_gcc __attribute__((__unused__)) = g->GV(gcc);
  Variant &gv_libphp __attribute__((__unused__)) = g->GV(libphp);
  {
  }
  if (!(LINE(17,f_check_for_program(toString(gv_libphp) + toString("/libphp5.so"))))) return false;
  return true;
} /* function */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 21 */
Array c_compiledvsinterpreted::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::get_dependent_test_names);
  return ScalarArrays::sa_[13];
} /* function */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 26 */
Variant c_compiledvsinterpreted::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::get_test_subjects);
  return LINE(28,f_get_interpretable_scripts());
} /* function */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 31 */
void c_compiledvsinterpreted::t_finish(Variant v_async) {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::finish);
  Variant v_output;

  if (((!same(v_async.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_async.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(2LL, 0x486AFCC090D5F98CLL))) || (!same(v_async.o_get("errs", 0x15E266045DABA002LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_async.o_get("errs", 0x15E266045DABA002LL).rvalAt(2LL, 0x486AFCC090D5F98CLL)))) || (!same(v_async.o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_async.o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(2LL, 0x486AFCC090D5F98CLL)))) {
    (v_output = LINE(37,f_diff(v_async.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_async.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(2LL, 0x486AFCC090D5F98CLL))));
    (v_async.o_lval("outs", 0x3D74198A5F0F7DDCLL) = v_output);
    LINE(39,t_async_failure("Outputs dont match PHP outputs", v_async));
  }
  else {
    LINE(43,t_async_success(v_async));
  }
} /* function */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 47 */
Variant c_compiledvsinterpreted::t_homogenize_output(Variant v_string, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::homogenize_output);
  (v_string = LINE(49,f_homogenize_filenames_and_line_numbers(v_string, toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL))));
  (v_string = LINE(50,f_homogenize_reference_count(v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 54 */
String c_compiledvsinterpreted::t_get_php_command(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::get_php_command);
  return LINE(56,f_get_php_command_line(v_subject));
} /* function */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 59 */
String c_compiledvsinterpreted::t_get_phc_command(CVarRef v_subject, CVarRef v_exe_name) {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::get_phc_command);
  return concat(LINE(61,f_get_phc_command_line(v_subject)), toString(" -c -o ") + toString(v_exe_name));
} /* function */
/* SRC: phc-test/framework/compiled_vs_interpreted.php line 64 */
void c_compiledvsinterpreted::t_run_test(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(CompiledVsInterpreted, CompiledVsInterpreted::run_test);
  p_asyncbundle v_async;
  Variant v_exe_name;

  ((Object)((v_async = ((Object)(LINE(66,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(68,o_root_invoke_few_args("get_php_command", 0x42412A2BC4988AA2LL, 1, v_subject))), 0x77CFA1EEF01BCA90LL);
  lval(v_async.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(0LL, ("homogenize_output"), 0x77CFA1EEF01BCA90LL);
  (v_exe_name = LINE(71,f_wd_name(concat(toString(v_subject) + toString(".out."), toString(o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0))))));
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(1LL, (LINE(72,o_root_invoke_few_args("get_phc_command", 0x72F2C7FCFE2FB14FLL, 2, v_subject, v_exe_name))), 0x5BCA7C69B794F8CELL);
  lval(v_async.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(1LL, ("fail_on_output"), 0x5BCA7C69B794F8CELL);
  lval(v_async.o_lval("exit_handlers", 0x4016CDEF485D2C08LL)).set(1LL, ("fail_on_output"), 0x5BCA7C69B794F8CELL);
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(2LL, (toString(v_exe_name)), 0x486AFCC090D5F98CLL);
  lval(v_async.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(2LL, ("homogenize_output"), 0x486AFCC090D5F98CLL);
  (v_async.o_lval("final", 0x5192930B2145036ELL) = "finish");
  LINE(81,v_async->t_start());
} /* function */
Object co_compiledvsinterpreted(CArrRef params, bool init /* = true */) {
  return Object(p_compiledvsinterpreted(NEW(c_compiledvsinterpreted)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$compiled_vs_interpreted_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/compiled_vs_interpreted.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$compiled_vs_interpreted_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(9,pm_php$phc_test$framework$lib$async_test_php(true, variables));
  LINE(84,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_compiledvsinterpreted(p_compiledvsinterpreted(NEWOBJ(c_compiledvsinterpreted)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
