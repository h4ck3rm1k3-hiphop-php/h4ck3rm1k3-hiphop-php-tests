
#ifndef __GENERATED_php_phc_test_framework_reparse_unparsed_h__
#define __GENERATED_php_phc_test_framework_reparse_unparsed_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/reparse_unparsed.fw.h>

// Declarations
#include <cls/reparseunparsed.h>
#include <php/phc-test/framework/lib/async_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$reparse_unparsed_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_reparseunparsed(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_reparse_unparsed_h__
