
#ifndef __GENERATED_php_phc_test_framework_compile_optimized_h__
#define __GENERATED_php_phc_test_framework_compile_optimized_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/compile_optimized.fw.h>

// Declarations
#include <cls/compileoptimized.h>
#include <php/phc-test/framework/compiled_vs_interpreted.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$compile_optimized_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_compileoptimized(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_compile_optimized_h__
