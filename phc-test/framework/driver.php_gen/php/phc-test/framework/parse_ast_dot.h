
#ifndef __GENERATED_php_phc_test_framework_parse_ast_dot_h__
#define __GENERATED_php_phc_test_framework_parse_ast_dot_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/parse_ast_dot.fw.h>

// Declarations
#include <cls/parseastdot.h>
#include <php/phc-test/framework/lib/async_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$parse_ast_dot_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_parseastdot(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_parse_ast_dot_h__
