
#ifndef __GENERATED_php_phc_test_framework_compile_plugin_test_h__
#define __GENERATED_php_phc_test_framework_compile_plugin_test_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/compile_plugin_test.fw.h>

// Declarations
#include <cls/compileplugintest.h>
#include <php/phc-test/framework/lib/async_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$compile_plugin_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_compileplugintest(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_compile_plugin_test_h__
