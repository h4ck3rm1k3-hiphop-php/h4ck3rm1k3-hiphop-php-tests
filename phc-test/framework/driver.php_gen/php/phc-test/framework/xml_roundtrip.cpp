
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/compare_backwards.h>
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <php/phc-test/framework/xml_roundtrip.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/xml_roundtrip.php line 10 */
Variant c_xml_roundtrip::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_xml_roundtrip::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_xml_roundtrip::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_xml_roundtrip::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_xml_roundtrip::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_xml_roundtrip::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_xml_roundtrip::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_xml_roundtrip::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(xml_roundtrip)
ObjectData *c_xml_roundtrip::cloneImpl() {
  c_xml_roundtrip *obj = NEW(c_xml_roundtrip)();
  cloneSet(obj);
  return obj;
}
void c_xml_roundtrip::cloneSet(c_xml_roundtrip *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_xml_roundtrip::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(params.rvalAt(0)), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(params.rvalAt(0)), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_xml_roundtrip::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(a0), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(a0), a1));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(a0), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_xml_roundtrip::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_xml_roundtrip$os_get(const char *s) {
  return c_xml_roundtrip::os_get(s, -1);
}
Variant &cw_xml_roundtrip$os_lval(const char *s) {
  return c_xml_roundtrip::os_lval(s, -1);
}
Variant cw_xml_roundtrip$os_constant(const char *s) {
  return c_xml_roundtrip::os_constant(s);
}
Variant cw_xml_roundtrip$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_xml_roundtrip::os_invoke(c, s, params, -1, fatal);
}
void c_xml_roundtrip::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/xml_roundtrip.php line 12 */
Array c_xml_roundtrip::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::get_dependent_test_names);
  return ScalarArrays::sa_[8];
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 17 */
bool c_xml_roundtrip::t_check_prerequisites() {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::check_prerequisites);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_xerces_compiled_in __attribute__((__unused__)) = g->GV(xerces_compiled_in);
  return (LINE(20,c_asynctest::t_check_prerequisites())) && (!equal(gv_xerces_compiled_in, "no"));
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 24 */
Variant c_xml_roundtrip::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::get_test_subjects);
  return LINE(26,f_get_all_scripts());
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 30 */
int64 c_xml_roundtrip::t_get_phc_num_procs_divisor() {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::get_phc_num_procs_divisor);
  return 2LL;
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 37 */
void c_xml_roundtrip::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::run_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  p_asyncbundle v_bundle;
  Variant v_passes;
  Variant v_last_pass;

  ((Object)((v_bundle = ((Object)(LINE(40,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  (v_passes = LINE(42,f_get_pass_list()));
  (v_last_pass = v_passes.rvalAt(LINE(43,x_count(v_passes)) - 1LL));
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(47,t_get_command_line(v_last_pass, v_subject))), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(0LL, ("homogenize_output"), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(1LL, (LINE(49,t_get_long_command_line(v_subject, v_last_pass))), 0x5BCA7C69B794F8CELL);
  lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(1LL, ("homogenize_output"), 0x5BCA7C69B794F8CELL);
  (v_bundle.o_lval("final", 0x5192930B2145036ELL) = "finish");
  LINE(54,v_bundle->t_start());
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 57 */
Variant c_xml_roundtrip::t_homogenize_output(CVarRef v_string) {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::homogenize_output);
  return LINE(59,f_homogenize_xml(v_string));
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 64 */
String c_xml_roundtrip::t_get_long_command_line(Variant v_subject, CVarRef v_last_pass) {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::get_long_command_line);
  String v_long_command;
  String v_concatenator;
  Variant v_pass;

  (v_long_command = "");
  ;
  (v_concatenator = "");
  {
    LOOP_COUNTER(1);
    Variant map2 = LINE(69,f_get_pass_list());
    for (ArrayIterPtr iter3 = map2.begin("xml_roundtrip"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_pass = iter3->second();
      {
        concat_assign(v_long_command, concat(v_concatenator, LINE(71,t_get_command_line(v_pass, v_subject))));
        setNull(v_subject);
        (v_concatenator = " | ");
        if (equal(v_pass, v_last_pass)) break;
      }
    }
  }
  return v_long_command;
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 84 */
String c_xml_roundtrip::t_get_command_line(Variant v_pass, CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::get_command_line);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  String v_command;

  (v_command = toString(gv_phc) + toString(" --no-hash-bang"));
  if (LINE(89,x_is_string(v_subject))) concat_assign(v_command, toString(" ") + toString(v_subject));
  if (equal(v_subject, null)) concat_assign(v_command, toString(" --read-xml=") + toString(v_pass));
  if (equal(v_pass, "AST-to-HIR")) (v_pass = "hir");
  if (equal(v_pass, "HIR-to-MIR")) (v_pass = "mir");
  concat_assign(v_command, toString(" --dump-xml=") + toString(v_pass));
  return v_command;
} /* function */
/* SRC: phc-test/framework/xml_roundtrip.php line 100 */
void c_xml_roundtrip::t_finish(Variant v_bundle) {
  INSTANCE_METHOD_INJECTION(XML_roundtrip, XML_roundtrip::finish);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_debug __attribute__((__unused__)) = g->GV(opt_debug);
  Variant v_expected_final_xml;
  Variant v_final_xml;
  String v_pass_string;
  Variant v_last_pass;
  Variant v_pass;
  String v_command1;
  String v_command2;
  Array v_result1;
  Array v_result2;

  (v_expected_final_xml = v_bundle.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (v_final_xml = v_bundle.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  if (same(v_final_xml, v_expected_final_xml)) LINE(106,t_async_success(v_bundle));
  else {
    (v_pass_string = "");
    if (toBoolean(gv_opt_debug)) {
      (v_last_pass = LINE(112,x_end(ref(f_get_pass_list()))));
      {
        LOOP_COUNTER(4);
        Variant map5 = LINE(114,f_get_pass_list());
        for (ArrayIterPtr iter6 = map5.begin("xml_roundtrip"); !iter6->end(); iter6->next()) {
          LOOP_COUNTER_CHECK(4);
          v_pass = iter6->second();
          {
            (v_command1 = LINE(116,t_get_command_line(v_pass, v_bundle.o_get("subject", 0x0EDD9FCC9525C3B2LL))));
            (v_command2 = LINE(117,t_get_long_command_line(v_bundle.o_get("subject", 0x0EDD9FCC9525C3B2LL), v_pass)));
            if (!same_rev(((v_result2 = LINE(118,f_complete_exec(v_command2)))), ((v_result1 = f_complete_exec(v_command1))))) {
              lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(2LL, (v_command1), 0x486AFCC090D5F98CLL);
              df_lambda_8(v_result1, lval(lval(v_bundle.o_lval("outs", 0x3D74198A5F0F7DDCLL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)), lval(lval(v_bundle.o_lval("errs", 0x15E266045DABA002LL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)), lval(lval(v_bundle.o_lval("exits", 0x1AE9F4DC5F326DFFLL)).lvalAt(2LL, 0x486AFCC090D5F98CLL)));
              lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(3LL, (v_command2), 0x135FDDF6A6BFBBDDLL);
              df_lambda_9(v_result2, lval(lval(v_bundle.o_lval("outs", 0x3D74198A5F0F7DDCLL)).lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), lval(lval(v_bundle.o_lval("errs", 0x15E266045DABA002LL)).lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), lval(lval(v_bundle.o_lval("exits", 0x1AE9F4DC5F326DFFLL)).lvalAt(3LL, 0x135FDDF6A6BFBBDDLL)));
              (v_pass_string = LINE(126,concat3(" in pass '", toString(v_pass), "'")));
              break;
            }
          }
        }
      }
    }
    LINE(132,t_async_failure(toString("Final output doesnt match expected") + v_pass_string, v_bundle));
  }
} /* function */
Object co_xml_roundtrip(CArrRef params, bool init /* = true */) {
  return Object(p_xml_roundtrip(NEW(c_xml_roundtrip)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$xml_roundtrip_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/xml_roundtrip.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$xml_roundtrip_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(9,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_xml_roundtrip(p_xml_roundtrip(NEWOBJ(c_xml_roundtrip)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
