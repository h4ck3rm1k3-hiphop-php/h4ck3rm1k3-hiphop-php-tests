
#ifndef __GENERATED_php_phc_test_framework_generate_c_h__
#define __GENERATED_php_phc_test_framework_generate_c_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/generate_c.fw.h>

// Declarations
#include <cls/generate_c.h>
#include <php/phc-test/framework/lib/async_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$generate_c_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_generate_c(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_generate_c_h__
