
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <php/phc-test/framework/no_whitespace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/no_whitespace.php line 13 */
Variant c_nowhitespace::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_nowhitespace::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_nowhitespace::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_nowhitespace::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_nowhitespace::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_nowhitespace::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_nowhitespace::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_nowhitespace::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(nowhitespace)
ObjectData *c_nowhitespace::cloneImpl() {
  c_nowhitespace *obj = NEW(c_nowhitespace)();
  cloneSet(obj);
  return obj;
}
void c_nowhitespace::cloneSet(c_nowhitespace *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_nowhitespace::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(params.rvalAt(0)), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      break;
    case 60:
      HASH_GUARD(0x1509B8A9B882D43CLL, strip_whitespace) {
        return (t_strip_whitespace(params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_nowhitespace::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(a0), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(a0), a1));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      break;
    case 60:
      HASH_GUARD(0x1509B8A9B882D43CLL, strip_whitespace) {
        return (t_strip_whitespace(a0, a1));
      }
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_nowhitespace::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_nowhitespace$os_get(const char *s) {
  return c_nowhitespace::os_get(s, -1);
}
Variant &cw_nowhitespace$os_lval(const char *s) {
  return c_nowhitespace::os_lval(s, -1);
}
Variant cw_nowhitespace$os_constant(const char *s) {
  return c_nowhitespace::os_constant(s);
}
Variant cw_nowhitespace$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_nowhitespace::os_invoke(c, s, params, -1, fatal);
}
void c_nowhitespace::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/no_whitespace.php line 15 */
Variant c_nowhitespace::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::get_test_subjects);
  return LINE(17,f_get_all_scripts());
} /* function */
/* SRC: phc-test/framework/no_whitespace.php line 20 */
Array c_nowhitespace::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::get_dependent_test_names);
  return ScalarArrays::sa_[8];
} /* function */
/* SRC: phc-test/framework/no_whitespace.php line 25 */
void c_nowhitespace::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::run_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  p_asyncbundle v_bundle;

  ((Object)((v_bundle = ((Object)(LINE(28,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (concat(LINE(30,f_get_phc_command_line(v_subject)), " --pretty-print")), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(0LL, ("strip_whitespace"), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(0LL, ("fail_on_output"), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(1LL, (toString("cat ") + toString(v_subject)), 0x5BCA7C69B794F8CELL);
  lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(1LL, ("strip_whitespace"), 0x5BCA7C69B794F8CELL);
  lval(v_bundle.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(1LL, ("fail_on_output"), 0x5BCA7C69B794F8CELL);
  (v_bundle.o_lval("final", 0x5192930B2145036ELL) = "two_command_finish");
  LINE(39,v_bundle->t_start());
} /* function */
/* SRC: phc-test/framework/no_whitespace.php line 44 */
Variant c_nowhitespace::t_strip_whitespace(Variant v_out, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::strip_whitespace);
  (v_out = LINE(46,x_preg_replace("/\\s+/", "", v_out)));
  (v_out = LINE(49,x_preg_replace("/<\\\?php/", "<\?", v_out)));
  return v_out;
} /* function */
Object co_nowhitespace(CArrRef params, bool init /* = true */) {
  return Object(p_nowhitespace(NEW(c_nowhitespace)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$no_whitespace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/no_whitespace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$no_whitespace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(12,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_nowhitespace(p_nowhitespace(NEWOBJ(c_nowhitespace)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
