
#ifndef __GENERATED_php_phc_test_framework_annotated_test_h__
#define __GENERATED_php_phc_test_framework_annotated_test_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/annotated_test.fw.h>

// Declarations
#include <cls/phc_output_annotation.h>
#include <cls/annotated_test.h>
#include <cls/phc_exit_code_annotation.h>
#include <cls/test_annotation.h>
#include <cls/annotation_translator.h>
#include <cls/phc_option_annotation.h>
#include <php/phc-test/framework/lib/async_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_parse_options(CVarRef v_options);
Variant pm_php$phc_test$framework$annotated_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_get_available_annotations();
Object co_phc_output_annotation(CArrRef params, bool init = true);
Object co_annotated_test(CArrRef params, bool init = true);
Object co_phc_exit_code_annotation(CArrRef params, bool init = true);
Object co_test_annotation(CArrRef params, bool init = true);
Object co_annotation_translator(CArrRef params, bool init = true);
Object co_phc_option_annotation(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_annotated_test_h__
