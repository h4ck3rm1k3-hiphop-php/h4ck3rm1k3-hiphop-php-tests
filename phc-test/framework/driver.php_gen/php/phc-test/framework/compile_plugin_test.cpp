
#include <php/phc-test/framework/compile_plugin_test.h>
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/compile_plugin_test.php line 9 */
Variant c_compileplugintest::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_compileplugintest::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_compileplugintest::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_compileplugintest::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_compileplugintest::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_compileplugintest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_compileplugintest::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_compileplugintest::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(compileplugintest)
ObjectData *c_compileplugintest::cloneImpl() {
  c_compileplugintest *obj = NEW(c_compileplugintest)();
  cloneSet(obj);
  return obj;
}
void c_compileplugintest::cloneSet(c_compileplugintest *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_compileplugintest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(params.rvalAt(0)), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_compileplugintest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(a0), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(a0), a1));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_compileplugintest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_compileplugintest$os_get(const char *s) {
  return c_compileplugintest::os_get(s, -1);
}
Variant &cw_compileplugintest$os_lval(const char *s) {
  return c_compileplugintest::os_lval(s, -1);
}
Variant cw_compileplugintest$os_constant(const char *s) {
  return c_compileplugintest::os_constant(s);
}
Variant cw_compileplugintest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_compileplugintest::os_invoke(c, s, params, -1, fatal);
}
void c_compileplugintest::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/compile_plugin_test.php line 11 */
bool c_compileplugintest::t_check_prerequisites() {
  INSTANCE_METHOD_INJECTION(CompilePluginTest, CompilePluginTest::check_prerequisites);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc_compile_plugin __attribute__((__unused__)) = g->GV(phc_compile_plugin);
  return LINE(14,f_check_for_program(gv_phc_compile_plugin));
} /* function */
/* SRC: phc-test/framework/compile_plugin_test.php line 18 */
Variant c_compileplugintest::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(CompilePluginTest, CompilePluginTest::get_test_subjects);
  return LINE(20,f_get_all_plugins());
} /* function */
/* SRC: phc-test/framework/compile_plugin_test.php line 23 */
Array c_compileplugintest::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(CompilePluginTest, CompilePluginTest::get_dependent_test_names);
  return ScalarArrays::sa_[3];
} /* function */
/* SRC: phc-test/framework/compile_plugin_test.php line 28 */
void c_compileplugintest::t_copy_headers() {
  INSTANCE_METHOD_INJECTION(CompilePluginTest, CompilePluginTest::copy_headers);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_working_directory __attribute__((__unused__)) = g->GV(working_directory);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  Variant &gv_base_dir __attribute__((__unused__)) = g->GV(base_dir);
  Variant v_headers;
  Variant v_header;
  String v_dest;
  Variant v_subject;

  {
  }
  (v_headers = LINE(34,(assignCallTemp(eo_1, x_chop(f_shell_exec(toString("find ") + toString(gv_base_dir) + toString("/plugins -name \"*.h\"")))),x_split("\n", eo_1))));
  ;
  if (!same(v_headers, ScalarArrays::sa_[5])) {
    {
      LOOP_COUNTER(1);
      for (ArrayIterPtr iter3 = v_headers.begin("compileplugintest"); !iter3->end(); iter3->next()) {
        LOOP_COUNTER_CHECK(1);
        v_header = iter3->second();
        {
          (v_dest = concat(toString(gv_working_directory) + toString("/"), LINE(40,x_basename(toString(v_header)))));
          if (!(LINE(41,x_copy(toString(v_header), v_dest)))) {
            (assignCallTemp(eo_0, v_subject),invoke_too_many_args("async_failure", (LINE(43,concat3(toString(v_header), " to ", v_dest)), 1), ((t_async_failure(eo_0, "Copying headers failed")), null)));
            return;
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/compile_plugin_test.php line 50 */
void c_compileplugintest::t_run() {
  INSTANCE_METHOD_INJECTION(CompilePluginTest, CompilePluginTest::run);
  LINE(52,t_copy_headers());
  LINE(53,c_asynctest::t_run());
} /* function */
/* SRC: phc-test/framework/compile_plugin_test.php line 56 */
void c_compileplugintest::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(CompilePluginTest, CompilePluginTest::run_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_trunk_CPPFLAGS __attribute__((__unused__)) = g->GV(trunk_CPPFLAGS);
  Variant &gv_phc_compile_plugin __attribute__((__unused__)) = g->GV(phc_compile_plugin);
  Variant &gv_working_directory __attribute__((__unused__)) = g->GV(working_directory);
  Variant &gv_base_dir __attribute__((__unused__)) = g->GV(base_dir);
  Variant v_plugin_name;
  p_asyncbundle v_async;
  String v_CPPFLAGS;
  Variant v_files;
  Variant v_filename;

  {
  }
  (v_plugin_name = LINE(61,x_tempnam(toString(gv_working_directory), "plugin")));
  LINE(62,x_unlink(toString(v_plugin_name)));
  if (!(LINE(63,x_copy(toString(v_subject), toString(v_plugin_name) + toString(".cpp"))))) {
    LINE(65,t_mark_failure(v_subject, "Copy failed"));
    return;
  }
  ((Object)((v_async = ((Object)(LINE(69,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  if (toBoolean(gv_trunk_CPPFLAGS)) (v_CPPFLAGS = toString("CFLAGS=") + toString(gv_trunk_CPPFLAGS));
  else (v_CPPFLAGS = "");
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(79,concat6(v_CPPFLAGS, " ", toString(gv_phc_compile_plugin), " ", toString(v_plugin_name), ".cpp"))), 0x77CFA1EEF01BCA90LL);
  lval(v_async.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(0LL, ("fail_on_output"), 0x77CFA1EEF01BCA90LL);
  lval(v_async.o_lval("exit_handlers", 0x4016CDEF485D2C08LL)).set(0LL, ("fail_on_output"), 0x77CFA1EEF01BCA90LL);
  (v_files = LINE(85,f_get_all_scripts()));
  (v_filename = v_files.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  lval(v_async.o_lval("commands", 0x189A3C89F0CA1103LL)).set(1LL, (LINE(87,concat5(toString(gv_phc), " --run ", toString(v_plugin_name), ".la ", toString(v_filename)))), 0x5BCA7C69B794F8CELL);
  lval(v_async.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(1LL, ("fail_on_output"), 0x5BCA7C69B794F8CELL);
  lval(v_async.o_lval("exit_handlers", 0x4016CDEF485D2C08LL)).set(1LL, ("fail_on_output"), 0x5BCA7C69B794F8CELL);
  (v_async.o_lval("final", 0x5192930B2145036ELL) = "async_success");
  LINE(93,v_async->t_start());
} /* function */
Object co_compileplugintest(CArrRef params, bool init /* = true */) {
  return Object(p_compileplugintest(NEW(c_compileplugintest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$compile_plugin_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/compile_plugin_test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$compile_plugin_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(98,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_compileplugintest(p_compileplugintest(NEWOBJ(c_compileplugintest)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
