
#include <php/phc-test/framework/annotated_test.h>
#include <php/phc-test/framework/basic_parse_test.h>
#include <php/phc-test/framework/compile_optimized.h>
#include <php/phc-test/framework/compile_plugin_test.h>
#include <php/phc-test/framework/compiled_vs_interpreted.h>
#include <php/phc-test/framework/demi_eval.h>
#include <php/phc-test/framework/driver.h>
#include <php/phc-test/framework/generate_c.h>
#include <php/phc-test/framework/lib/compare_backwards.h>
#include <php/phc-test/framework/lib/compare_with_php_test.h>
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/pass_dump.h>
#include <php/phc-test/framework/lib/plugin_test.h>
#include <php/phc-test/framework/lib/regression.h>
#include <php/phc-test/framework/lib/startup.h>
#include <php/phc-test/framework/line_numbers.h>
#include <php/phc-test/framework/no_whitespace.h>
#include <php/phc-test/framework/parse_ast_dot.h>
#include <php/phc-test/framework/parse_tree_dot.h>
#include <php/phc-test/framework/reduce/Reduce.h>
#include <php/phc-test/framework/refcounts.h>
#include <php/phc-test/framework/reparse_unparsed.h>
#include <php/phc-test/framework/source_vs_semantic_values.h>
#include <php/phc-test/framework/xml_roundtrip.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$framework$driver_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/driver.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$driver_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_support_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("support_dir") : g->GV(support_dir);
  Variant &v_plugin_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("plugin_dir") : g->GV(plugin_dir);
  Variant &v_log_directory __attribute__((__unused__)) = (variables != gVariables) ? variables->get("log_directory") : g->GV(log_directory);
  Variant &v_working_directory __attribute__((__unused__)) = (variables != gVariables) ? variables->get("working_directory") : g->GV(working_directory);
  Variant &v_opt_clean __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_clean") : g->GV(opt_clean);
  Variant &v_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php") : g->GV(php);
  Variant &v_base_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("base_dir") : g->GV(base_dir);
  Variant &v_opt_installed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_installed") : g->GV(opt_installed);
  Variant &v_bindir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bindir") : g->GV(bindir);
  Variant &v_phc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phc") : g->GV(phc);
  Variant &v_pkglibdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pkglibdir") : g->GV(pkglibdir);
  Variant &v_phc_compile_plugin __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phc_compile_plugin") : g->GV(phc_compile_plugin);
  Variant &v_trunk_CPPFLAGS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("trunk_CPPFLAGS") : g->GV(trunk_CPPFLAGS);
  Variant &v_pwd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pwd") : g->GV(pwd);
  Variant &v_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dir") : g->GV(dir);
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_test_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_name") : g->GV(test_name);
  Variant &v_arguments __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arguments") : g->GV(arguments);
  Variant &v_match __attribute__((__unused__)) = (variables != gVariables) ? variables->get("match") : g->GV(match);
  Variant &v_regex __attribute__((__unused__)) = (variables != gVariables) ? variables->get("regex") : g->GV(regex);
  Variant &v_opt_support __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_support") : g->GV(opt_support);
  Variant &v_opt_numbered __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_numbered") : g->GV(opt_numbered);

  LINE(10,x_ini_set("memory_limit", "768M"));
  LINE(11,(assignCallTemp(eo_1, concat("test/framework/external/:", x_get_include_path())),x_ini_set("include_path", eo_1)));
  if (less(LINE(14,(assignCallTemp(eo_0, x_phpversion()),x_substr(eo_0, toInt32(0LL), toInt32(1LL)))), 5LL)) {
    f_exit("Tests skipped (PHP version 5 required)\n");
  }
  (v_support_dir = "test/support_files");
  (v_plugin_dir = "plugins");
  LINE(22,pm_php$phc_test$framework$lib$header_php(true, variables));
  (v_log_directory = concat("test/logs/", toString(LINE(25,f_date_string()))));
  LINE(26,x_mkdir(toString(v_log_directory)));
  (silenceInc(), silenceDec(LINE(27,x_unlink("test/logs/latest"))));
  LINE(28,x_symlink(toString(v_log_directory), "test/logs/latest"));
  print((LINE(29,concat3("Logs in:      ", toString(v_log_directory), "\n"))));
  (v_working_directory = concat("test/working/", toString(LINE(32,f_date_string()))));
  LINE(33,x_mkdir(toString(v_working_directory)));
  (silenceInc(), silenceDec(LINE(34,x_unlink("test/working/latest"))));
  LINE(35,x_symlink(toString(v_working_directory), "test/working/latest"));
  LINE(36,x_mkdir(toString(v_working_directory) + toString("/.libs")));
  print((LINE(37,concat3("Working from: ", toString(v_working_directory), "\n"))));
  LINE(39,pm_php$phc_test$framework$lib$startup_php(true, variables));
  LINE(40,require("lib/autovars.php", true, variables, "phc-test/framework/"));
  LINE(41,pm_php$phc_test$framework$reduce$Reduce_php(true, variables));
  if (toBoolean(v_opt_clean)) {
    echo("rm -Rf ./test/logs/*\n");
    f_shell_exec(toString("rm -Rf ./test/logs/*"));
    echo("rm -Rf ./test/working/*\n");
    f_shell_exec(toString("rm -Rf ./test/working/*"));
    f_exit(0LL);
  }
  LINE(52,f_open_status_files());
  (v_php = LINE(54,f_get_php()));
  (v_base_dir = LINE(55,x_getcwd()));
  if (toBoolean(v_opt_installed)) {
    (v_phc = toString(v_bindir) + toString("/phc"));
    (v_plugin_dir = toString(v_pkglibdir) + toString("/plugins"));
    (v_phc_compile_plugin = toString(v_bindir) + toString("/phc_compile_plugin"));
    (v_trunk_CPPFLAGS = "");
    (v_pwd = LINE(63,x_getcwd()));
    (v_dir = concat_rev(toString(LINE(64,x_getmypid())), concat(x_sys_get_temp_dir(), "/phc-test-")));
    echo(f_shell_exec(toString("rm -Rf ") + toString(v_dir)));
    LINE(66,x_mkdir(toString(v_dir)));
    LINE(67,x_chdir(toString(v_dir)));
    echo(f_shell_exec(toString("ln -s ") + toString(v_pwd) + toString("/test test")));
    echo(LINE(69,(assignCallTemp(eo_1, toString(x_getcwd())),concat3("Running from: ", eo_1, "\n"))));
  }
  else {
    (v_phc = LINE(74,f_get_phc()));
    (v_phc_compile_plugin = LINE(75,f_get_phc_compile_plugin()));
  }
  LINE(78,pm_php$phc_test$framework$lib$compare_with_php_test_php(true, variables));
  LINE(79,pm_php$phc_test$framework$lib$plugin_test_php(true, variables));
  LINE(80,pm_php$phc_test$framework$lib$regression_php(true, variables));
  LINE(81,pm_php$phc_test$framework$lib$compare_backwards_php(true, variables));
  LINE(82,pm_php$phc_test$framework$lib$pass_dump_php(true, variables));
  (v_tests = ScalarArrays::sa_[3]);
  LINE(87,pm_php$phc_test$framework$annotated_test_php(true, variables));
  LINE(88,pm_php$phc_test$framework$basic_parse_test_php(true, variables));
  LINE(89,pm_php$phc_test$framework$no_whitespace_php(true, variables));
  v_tests.append((((Object)(LINE(90,p_comparebackwards(p_comparebackwards(NEWOBJ(c_comparebackwards)())->create("ast")))))));
  v_tests.append((((Object)(LINE(91,p_comparebackwards(p_comparebackwards(NEWOBJ(c_comparebackwards)())->create("sua", "dump", "cb_ast")))))));
  v_tests.append((((Object)(LINE(92,p_comparebackwards(p_comparebackwards(NEWOBJ(c_comparebackwards)())->create("AST-to-HIR", "dump", "cb_sua")))))));
  v_tests.append((((Object)(LINE(93,p_comparebackwards(p_comparebackwards(NEWOBJ(c_comparebackwards)())->create("hir", "dump", "cb_AST-to-HIR")))))));
  v_tests.append((((Object)(LINE(95,p_comparebackwards(p_comparebackwards(NEWOBJ(c_comparebackwards)())->create("mir", "convert-uppered --dump", "cb_hir")))))));
  v_tests.append((((Object)(LINE(96,p_pass_dump(p_pass_dump(NEWOBJ(c_pass_dump)())->create("HIR-to-MIR", "dump", "cb_hir")))))));
  v_tests.append((((Object)(LINE(97,p_pass_dump(p_pass_dump(NEWOBJ(c_pass_dump)())->create("mir", "dump", "cb_mir")))))));
  v_tests.append((((Object)(LINE(98,p_comparewithphp(p_comparewithphp(NEWOBJ(c_comparewithphp)())->create("InterpretOptimized", "-O1 --dump=generate-c --convert-uppered", "cb_mir")))))));
  v_tests.append((((Object)(LINE(99,p_comparewithphp(p_comparewithphp(NEWOBJ(c_comparewithphp)())->create("InterpretCanonicalUnparsed", "--run plugins/tests/canonical_unparser.la", "BasicParseTest")))))));
  v_tests.append((((Object)(LINE(100,p_comparewithphp(p_comparewithphp(NEWOBJ(c_comparewithphp)())->create("InterpretIncludes", "--include --dump=incl1 --no-warnings", "cb_sua")))))));
  v_tests.append((((Object)(LINE(101,p_comparewithphp(p_comparewithphp(NEWOBJ(c_comparewithphp)())->create("InterpretObfuscated", "--obfuscate", "cb_mir")))))));
  LINE(102,pm_php$phc_test$framework$generate_c_php(true, variables));
  LINE(103,pm_php$phc_test$framework$compiled_vs_interpreted_php(true, variables));
  LINE(104,pm_php$phc_test$framework$compile_optimized_php(true, variables));
  LINE(105,pm_php$phc_test$framework$refcounts_php(true, variables));
  LINE(106,pm_php$phc_test$framework$demi_eval_php(true, variables));
  v_tests.append((((Object)(LINE(107,p_plugintest(p_plugintest(NEWOBJ(c_plugintest)())->create("inconsistent_st_attr")))))));
  v_tests.append((((Object)(LINE(108,p_plugintest(p_plugintest(NEWOBJ(c_plugintest)())->create("linear")))))));
  v_tests.append((((Object)(LINE(109,p_plugintest(p_plugintest(NEWOBJ(c_plugintest)())->create("cloning")))))));
  v_tests.append((((Object)(LINE(110,p_plugintest(p_plugintest(NEWOBJ(c_plugintest)())->create("pre_vs_post_count")))))));
  LINE(111,pm_php$phc_test$framework$reparse_unparsed_php(true, variables));
  LINE(112,pm_php$phc_test$framework$source_vs_semantic_values_php(true, variables));
  LINE(113,pm_php$phc_test$framework$xml_roundtrip_php(true, variables));
  LINE(114,pm_php$phc_test$framework$compile_plugin_test_php(true, variables));
  LINE(115,pm_php$phc_test$framework$line_numbers_php(true, variables));
  LINE(116,pm_php$phc_test$framework$parse_ast_dot_php(true, variables));
  LINE(117,pm_php$phc_test$framework$parse_tree_dot_php(true, variables));
  v_tests.append((((Object)(LINE(118,p_regressiontest(p_regressiontest(NEWOBJ(c_regressiontest)())->create("regression_dump_ast", "--dump-dot=ast", "dot")))))));
  v_tests.append((((Object)(LINE(119,p_regressiontest(p_regressiontest(NEWOBJ(c_regressiontest)())->create("regression_dump_php", "--pretty-print --tab=\"   \"", "unparsed")))))));
  v_tests.append((((Object)(LINE(120,p_regressiontest(p_regressiontest(NEWOBJ(c_regressiontest)())->create("regression_dump_xml", "--dump-xml=ast --dump-xml=hir --dump-xml=mir", "unparsed")))))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_tests.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_test = iter3->second();
      {
        (v_test_name = LINE(127,v_test.o_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)));
        if (more(LINE(130,x_count(v_arguments)), 0LL)) {
          (v_match = false);
          {
            LOOP_COUNTER(4);
            for (ArrayIterPtr iter6 = v_arguments.begin(); !iter6->end(); iter6->next()) {
              LOOP_COUNTER_CHECK(4);
              v_regex = iter6->second();
              {
                if (toBoolean(LINE(135,(assignCallTemp(eo_0, concat3("/", toString(v_regex), "/")),assignCallTemp(eo_1, toString(v_test_name)),x_preg_match(eo_0, eo_1))))) (v_match = true);
              }
            }
          }
          if (!(toBoolean(v_match))) {
            continue;
          }
        }
        if (toBoolean(v_opt_support)) {
          if (LINE(147,x_method_exists(v_test, "generate_support_files"))) {
            LINE(149,v_test.o_invoke_few_args("generate_support_files", 0x261D586D2BEF01D1LL, 0));
          }
        }
        else if (toBoolean(v_opt_numbered)) {
          LINE(154,v_test.o_invoke_few_args("print_numbered", 0x065FDD3E5650A82BLL, 0));
        }
        else LINE(156,v_test.o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 0));
      }
    }
  }
  LINE(158,f_close_status_files());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
