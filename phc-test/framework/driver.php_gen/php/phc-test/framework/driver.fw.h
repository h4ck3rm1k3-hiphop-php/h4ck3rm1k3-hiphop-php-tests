
#ifndef __GENERATED_php_phc_test_framework_driver_fw_h__
#define __GENERATED_php_phc_test_framework_driver_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/framework/lib/compare_backwards.fw.h>
#include <php/phc-test/framework/lib/compare_with_php_test.fw.h>
#include <php/phc-test/framework/lib/pass_dump.fw.h>
#include <php/phc-test/framework/lib/plugin_test.fw.h>
#include <php/phc-test/framework/lib/regression.fw.h>

#endif // __GENERATED_php_phc_test_framework_driver_fw_h__
