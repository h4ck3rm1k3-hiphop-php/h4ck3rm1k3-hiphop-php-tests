
#include <php/phc-test/framework/lib/plugin_test.h>
#include <php/phc-test/framework/source_vs_semantic_values.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/source_vs_semantic_values.php line 10 */
Variant c_sourcevssemantictest::os_get(const char *s, int64 hash) {
  return c_plugintest::os_get(s, hash);
}
Variant &c_sourcevssemantictest::os_lval(const char *s, int64 hash) {
  return c_plugintest::os_lval(s, hash);
}
void c_sourcevssemantictest::o_get(ArrayElementVec &props) const {
  c_plugintest::o_get(props);
}
bool c_sourcevssemantictest::o_exists(CStrRef s, int64 hash) const {
  return c_plugintest::o_exists(s, hash);
}
Variant c_sourcevssemantictest::o_get(CStrRef s, int64 hash) {
  return c_plugintest::o_get(s, hash);
}
Variant c_sourcevssemantictest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_plugintest::o_set(s, hash, v, forInit);
}
Variant &c_sourcevssemantictest::o_lval(CStrRef s, int64 hash) {
  return c_plugintest::o_lval(s, hash);
}
Variant c_sourcevssemantictest::os_constant(const char *s) {
  return c_plugintest::os_constant(s);
}
IMPLEMENT_CLASS(sourcevssemantictest)
ObjectData *c_sourcevssemantictest::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_sourcevssemantictest::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_sourcevssemantictest::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_sourcevssemantictest::cloneImpl() {
  c_sourcevssemantictest *obj = NEW(c_sourcevssemantictest)();
  cloneSet(obj);
  return obj;
}
void c_sourcevssemantictest::cloneSet(c_sourcevssemantictest *clone) {
  c_plugintest::cloneSet(clone);
}
Variant c_sourcevssemantictest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      HASH_GUARD(0x45ECA01DB08EBB11LL, get_command_line) {
        return (t_get_command_line(params.rvalAt(0)));
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(params.rvalAt(0)), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(params.rvalAt(0)), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_plugintest::o_invoke(s, params, hash, fatal);
}
Variant c_sourcevssemantictest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      HASH_GUARD(0x45ECA01DB08EBB11LL, get_command_line) {
        return (t_get_command_line(a0));
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(a0), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(a0), a1));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(a0), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    default:
      break;
  }
  return c_plugintest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sourcevssemantictest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_plugintest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sourcevssemantictest$os_get(const char *s) {
  return c_sourcevssemantictest::os_get(s, -1);
}
Variant &cw_sourcevssemantictest$os_lval(const char *s) {
  return c_sourcevssemantictest::os_lval(s, -1);
}
Variant cw_sourcevssemantictest$os_constant(const char *s) {
  return c_sourcevssemantictest::os_constant(s);
}
Variant cw_sourcevssemantictest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sourcevssemantictest::os_invoke(c, s, params, -1, fatal);
}
void c_sourcevssemantictest::init() {
  c_plugintest::init();
}
/* SRC: phc-test/framework/source_vs_semantic_values.php line 12 */
void c_sourcevssemantictest::t___construct() {
  INSTANCE_METHOD_INJECTION(SourceVsSemanticTest, SourceVsSemanticTest::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(14,c_plugintest::t___construct("source_vs_semantic_values"));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/source_vs_semantic_values.php line 17 */
Array c_sourcevssemantictest::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(SourceVsSemanticTest, SourceVsSemanticTest::get_dependent_test_names);
  return ScalarArrays::sa_[8];
} /* function */
/* SRC: phc-test/framework/source_vs_semantic_values.php line 22 */
String c_sourcevssemantictest::t_get_command_line(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(SourceVsSemanticTest, SourceVsSemanticTest::get_command_line);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_php __attribute__((__unused__)) = g->GV(php);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  {
  }
  return LINE(25,(assignCallTemp(eo_0, toString(gv_phc)),assignCallTemp(eo_2, concat6(toString(gv_plugin_dir), "/tests/source_vs_semantic_values.la ", toString(v_subject), " 2>&1 | ", toString(gv_php), " -d memory_limit=128M 2>&1")),concat3(eo_0, " --run ", eo_2)));
} /* function */
Object co_sourcevssemantictest(CArrRef params, bool init /* = true */) {
  return Object(p_sourcevssemantictest(NEW(c_sourcevssemantictest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$source_vs_semantic_values_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/source_vs_semantic_values.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$source_vs_semantic_values_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(9,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_sourcevssemantictest(p_sourcevssemantictest(NEWOBJ(c_sourcevssemantictest)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
