
#ifndef __GENERATED_php_phc_test_framework_refcounts_h__
#define __GENERATED_php_phc_test_framework_refcounts_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/refcounts.fw.h>

// Declarations
#include <cls/refcounts.h>
#include <php/phc-test/framework/compiled_vs_interpreted.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$refcounts_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_refcounts(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_refcounts_h__
