
#ifndef __GENERATED_php_phc_test_framework_lib_regression_h__
#define __GENERATED_php_phc_test_framework_lib_regression_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/regression.fw.h>

// Declarations
#include <cls/regressiontest.h>
#include <php/phc-test/framework/lib/support_file_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$lib$regression_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_regressiontest(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_regression_h__
