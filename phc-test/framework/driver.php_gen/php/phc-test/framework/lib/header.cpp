
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/header.php line 573 */
bool f_check_for_plugin(CStrRef v_plugin_name) {
  FUNCTION_INJECTION(check_for_plugin);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  return LINE(576,x_file_exists(concat4(toString(gv_plugin_dir), "/", v_plugin_name, ".la")));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 268 */
String f_reset_string() {
  FUNCTION_INJECTION(reset_string);
  return LINE(270,x_sprintf(2, "%c[0m", ScalarArrays::sa_[11]));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 113 */
String f_wd_name(CStrRef v_subject) {
  FUNCTION_INJECTION(wd_name);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_working_directory __attribute__((__unused__)) = g->GV(working_directory);
  return concat(toString(gv_working_directory) + toString("/"), toString(LINE(116,x_preg_replace("/\\//", "_", v_subject))));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 711 */
Variant f_homogenize_break_levels(Variant v_string) {
  FUNCTION_INJECTION(homogenize_break_levels);
  (v_string = LINE(714,x_preg_replace("/(Fatal error: Cannot break\\/continue 1 level)s/", "$1", v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 54 */
Variant f_get_php() {
  FUNCTION_INJECTION(get_php);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_php_exe __attribute__((__unused__)) = g->GV(php_exe);
  String v_php;

  if (toBoolean(gv_php_exe)) {
    return toString(gv_php_exe) + toString(" -C");
  }
  else {
    (v_php = LINE(71,x_trim(f_shell_exec(toString("which php")))));
    return v_php + toString(" -C");
  }
  return null;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 780 */
Variant f_strip_console_codes(Variant v_string) {
  FUNCTION_INJECTION(strip_console_codes);
  (v_string = LINE(783,x_preg_replace("/\033\\[1;\\d\\dm/", "", v_string)));
  (v_string = LINE(784,x_preg_replace("/\033\\[0m/", "", v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 722 */
Variant f_homogenize_reference_count(Variant v_string) {
  FUNCTION_INJECTION(homogenize_reference_count);
  (v_string = LINE(736,x_preg_replace("/\n\t\t\t(\\s+\\[.*\?\\]=>\\s+)\t\t# key and newline\n\t\t\t&\t\t\t\t\t\t\t# we want to delete this\n\t\t\t(.*\?)\t\t\t\t\t\t# dont go too far\n\t\t/Ssmx", "$1$2", v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 761 */
Variant f_homogenize_all(Variant v_string, CVarRef v_filename) {
  FUNCTION_INJECTION(homogenize_all);
  (v_string = LINE(763,f_homogenize_reference_count(v_string)));
  (v_string = LINE(764,f_homogenize_object_count(v_string)));
  (v_string = LINE(765,f_homogenize_filenames_and_line_numbers(v_string, v_filename)));
  (v_string = LINE(766,f_homogenize_break_levels(v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 666 */
Variant f_homogenize_xml(Variant v_string) {
  FUNCTION_INJECTION(homogenize_xml);
  (v_string = LINE(668,x_preg_replace("/(<attr key=\"phc.line_number\">)\\d+(<\\/attr>)/", "$1$2", v_string)));
  (v_string = LINE(669,x_preg_replace("/(<attr key=\"phc.filename\">).*\?(<\\/attr>)/", "$1$2", v_string)));
  (v_string = LINE(672,x_preg_replace("/(<value>\\D+)\\d+(<\\/value>)/", "$1xx$2", v_string)));
  (v_string = LINE(673,x_preg_replace("/(<string>\\D+)\\d+(<\\/string>)/", "$1xx$2", v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 589 */
Variant f_date_string() {
  FUNCTION_INJECTION(date_string);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_date_string __attribute__((__unused__)) = g->GV(date_string);
  if (equal(gv_date_string, null)) {
    (gv_date_string = LINE(596,x_date("D_d_M_H_i_s")));
  }
  return gv_date_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 548 */
void f_kill_properly(Variant v_handle, Variant v_pipes) {
  FUNCTION_INJECTION(kill_properly);
  Variant eo_0;
  Variant eo_1;
  Array v_status;
  Variant v_ppid;
  Variant v_pids;
  Variant v_pipe;
  Variant v_pid;

  (v_status = LINE(550,x_proc_get_status(toObject(v_handle))));
  (v_ppid = v_status.rvalAt("pid", 0x133F26DF225E6273LL));
  (v_pids = LINE(555,(assignCallTemp(eo_1, x_trim(f_shell_exec(toString("ps -o pid --no-heading --ppid ") + toString(v_ppid)))),x_split("/\\s+/", eo_1))));
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(v_pipes);
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(NULL, v_pipe); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      LINE(558,x_fclose(toObject(v_pipe)));
    }
  }
  LINE(559,x_proc_terminate(toObject(v_handle)));
  LINE(560,x_proc_close(toObject(v_handle)));
  {
    {
      LOOP_COUNTER(4);
      for (ArrayIterPtr iter6 = v_pids.begin(); !iter6->end(); iter6->next()) {
        LOOP_COUNTER_CHECK(4);
        v_pid = iter6->second();
        {
          if (LINE(567,x_is_numeric(v_pid))) LINE(568,x_posix_kill(toInt32(v_pid), toInt32(9LL)));
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/header.php line 579 */
bool f_check_for_program(Variant v_program_name) {
  FUNCTION_INJECTION(check_for_program);
  Variant v_program_names;

  (v_program_names = LINE(582,x_split(" ", toString(v_program_name))));
  (v_program_name = v_program_names.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  return (!same(v_program_name, "") && ((LINE(586,x_file_exists(toString(v_program_name)))) || (equal(v_program_name, "gcc"))));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 230 */
void f_log_status(CStrRef v_status, CVarRef v_test_name, CVarRef v_subject, Variant v_reason) {
  FUNCTION_INJECTION(log_status);
  Variant eo_0;
  Variant eo_1;
  String v_status_name;

  (v_status_name = LINE(232,x_ucfirst(v_status)));
  if (!equal(v_reason, "")) (v_reason = toString(" - ") + toString(v_reason));
  LINE(234,(assignCallTemp(eo_0, v_status),assignCallTemp(eo_1, concat(toString(v_test_name), concat6(": ", v_status_name, " ", toString(v_subject), toString(v_reason), "\n"))),f_write_status(eo_0, eo_1)));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 77 */
String f_get_php_command_line(Variant v_subject, CVarRef v_pipe //  = false
) {
  FUNCTION_INJECTION(get_php_command_line);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_php __attribute__((__unused__)) = g->GV(php);
  Variant &gv_opt_long __attribute__((__unused__)) = g->GV(opt_long);
  int64 v_max_exe = 0;
  String v_dir_path;

  LINE(79,f_phc_assert(!equal(v_subject, ""), "dont pass empty subject, even for pipe"));
  if (toBoolean(gv_opt_long)) (v_max_exe = 12LL);
  else (v_max_exe = 5LL);
  (v_dir_path = LINE(89,x_dirname(toString(v_subject))));
  if (toBoolean(v_pipe)) (v_subject = "");
  return LINE(91,(assignCallTemp(eo_0, toString(gv_php)),assignCallTemp(eo_2, concat6(v_dir_path, " -d max_execution_time=", toString(v_max_exe), " ", toString(v_subject), " ")),concat3(eo_0, " -d include_path=./:test/subjects/:", eo_2)));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 121 */
void f_phc_error_handler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline, CVarRef v_errcontext) {
  FUNCTION_INJECTION(phc_error_handler);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_strict __attribute__((__unused__)) = g->GV(strict);
  String v_red;
  String v_reset;
  String v_blue;
  String v_long_dump;
  Primitive v_name = 0;
  Variant v_context;
  Variant v_dump;
  Array v_backtrace;
  Variant v_prev_frame;
  int64 v_j = 0;
  int64 v_i = 0;
  Variant v_frame;
  Variant v_function;
  Variant v_object;
  Variant v_class;
  Variant v_type;
  Variant v_args;
  Variant v_arg;
  Variant v_args_string;
  Variant v_file;
  Variant v_line;

  if (equal(LINE(124,x_error_reporting()), 0LL)) return;
  if (same(v_errno, 2048LL /* E_STRICT */) && equal(gv_strict, false)) return;
  if (same(v_errno, 2LL /* E_WARNING */) && equal(gv_strict, false)) return;
  if (same(v_errno, 512LL /* E_USER_WARNING */) && equal(gv_strict, false)) return;
  (v_red = LINE(129,f_red_string()));
  (v_reset = LINE(130,f_reset_string()));
  (v_blue = LINE(131,f_blue_string()));
  print(LINE(133,concat5("\n", v_red, "-----Error----------", v_reset, "\n")));
  print(LINE(135,concat5("\n", v_red, "----Context---------", v_reset, "\n")));
  (v_long_dump = "");
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_errcontext.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_context = iter9->second();
      v_name = iter9->first();
      {
        concat_assign(v_long_dump, LINE(139,(assignCallTemp(eo_0, concat4(v_blue, "$%-12s", v_reset, " => ")),assignCallTemp(eo_1, v_name),x_sprintf(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
        (v_dump = LINE(140,(assignCallTemp(eo_2, x_print_r(v_context, true)),x_preg_replace("/\n/", " ", eo_2))));
        concat_assign(v_long_dump, LINE(141,concat4(toString(v_dump), "; ", v_reset, "\n")));
      }
    }
  }
  if (more(LINE(143,x_strlen(v_long_dump)), 640LL)) {
    print(LINE(145,(assignCallTemp(eo_0, v_red + toString("Context skipped, too long (")),assignCallTemp(eo_1, toString(x_strlen(v_long_dump))),concat3(eo_0, eo_1, ")"))));
  }
  else {
    print(v_long_dump);
  }
  print("\n");
  print(LINE(153,concat5("\n", v_red, "----Backtrace-------", v_reset, "\n")));
  (v_backtrace = LINE(154,x_debug_backtrace()));
  (v_prev_frame = v_backtrace.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (v_j = 0LL);
  {
    LOOP_COUNTER(10);
    for ((v_i = 1LL); less(v_i, LINE(159,x_count(v_backtrace))); v_i++) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_frame = v_backtrace.rvalAt(v_i));
        (v_function = v_frame.rvalAt("function", 0x736D912A52403931LL));
        (v_object = v_frame.rvalAt("object", 0x7F30BC4E222B1861LL));
        (v_class = LINE(164,x_get_class(v_frame.rvalAt("object", 0x7F30BC4E222B1861LL))));
        (v_type = v_frame.rvalAt("type", 0x508FC7C8724A760ALL));
        if (((equal(v_function, "trigger_error"))) || (((equal(v_function, "phc_assert")) && (equal(v_backtrace.rvalAt(v_i + 1LL).rvalAt("function", 0x736D912A52403931LL), "phc_unreachable"))))) {
          (v_prev_frame = v_frame);
          continue;
        }
        (v_args = v_frame.rvalAt("args", 0x4AF7CD17F976719ELL));
        if (equal(v_args, null)) (v_args = ScalarArrays::sa_[3]);
        {
          LOOP_COUNTER(11);
          Variant map12 = ref(v_args);
          map12.escalate();
          for (MutableArrayIterPtr iter13 = map12.begin(NULL, v_arg); iter13->advance();) {
            LOOP_COUNTER_CHECK(11);
            {
              if (LINE(183,x_is_object(v_arg))) (v_arg = "OBJECT");
            }
          }
        }
        (v_args_string = LINE(186,x_join(", ", v_args)));
        (v_args_string = LINE(187,x_preg_replace("/\n/", "", v_args_string)));
        (v_args_string = LINE(188,x_substr(toString(v_args_string), toInt32(0LL), toInt32(30LL))));
        if (equal(LINE(189,x_strlen(toString(v_args_string))), 30LL)) concat_assign(v_args_string, "...");
        if (same(v_prev_frame, null)) {
          print(concat_rev(LINE(195,concat6(toString(v_class), toString(v_type), toString(v_function), " (", toString(v_args_string), ")\n")), concat3("#", toString(v_j), " ")));
        }
        else {
          (v_file = v_prev_frame.rvalAt("file", 0x612E37678CE7DB5BLL));
          (v_file = LINE(200,x_preg_replace("!^.*/framework/(.*$)!", "$1", v_file)));
          (v_line = v_prev_frame.rvalAt("line", 0x21093C71DDF8728CLL));
          print(concat_rev(LINE(202,concat6(toString(v_args_string), ") at ", toString(v_file), ":", toString(v_line), "\n")), concat("#", concat6(toString(v_j), " ", toString(v_class), toString(v_type), toString(v_function), " ("))));
        }
        (v_prev_frame = v_frame);
        v_j++;
      }
    }
  }
  (v_file = v_prev_frame.rvalAt("file", 0x612E37678CE7DB5BLL));
  (v_file = LINE(210,x_preg_replace("!^.*/framework/(.*$)!", "$1", v_file)));
  (v_line = v_prev_frame.rvalAt("line", 0x21093C71DDF8728CLL));
  print(concat("#", LINE(212,concat6(toString(v_j), " called from ", toString(v_file), ":", toString(v_line), "\n"))));
  print(LINE(215,concat5("\n", v_red, "----Message---------", v_reset, "\n")));
  f_exit(LINE(216,x_sprintf(1, concat_rev(concat6(toString(v_errstr), "' at ", toString(v_errfile), ":", toString(v_errline), "\n"), concat3("Error (", toString(v_errno), "): '")))));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 94 */
String f_get_phc_command_line(CVarRef v_subject) {
  FUNCTION_INJECTION(get_phc_command_line);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_opt_long __attribute__((__unused__)) = g->GV(opt_long);
  int64 v_max_exe = 0;
  String v_dir_path;

  LINE(96,f_phc_assert(!equal(v_subject, ""), "dont pass empty subject"));
  if (toBoolean(gv_opt_long)) (v_max_exe = 12LL);
  else (v_max_exe = 5LL);
  (v_dir_path = LINE(106,x_dirname(toString(v_subject))));
  return LINE(107,(assignCallTemp(eo_0, toString(gv_phc)),assignCallTemp(eo_2, concat6(v_dir_path, " -d max_execution_time=", toString(v_max_exe), " ", toString(v_subject), " ")),concat3(eo_0, " -d include_path=./:test/subjects/:", eo_2)));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 263 */
void f_phc_unreachable(CVarRef v_message //  = "This point should be unreachable"
) {
  FUNCTION_INJECTION(phc_unreachable);
  LINE(265,f_phc_assert(false, v_message));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 332 */
void f_log_failure(Variant v_test_name, Variant v_subject, Variant v_commands, Variant v_outs, Variant v_errs, Variant v_exits, Variant v_missing_dependency, Variant v_reason) {
  FUNCTION_INJECTION(log_failure);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_red;
  String v_reset;
  String v_err_string;
  String v_command_string;
  Variant v_command;
  Primitive v_i = 0;
  Variant v_exit;
  Variant v_err;
  String v_reason_string;
  String v_dependency_string;
  String v_header;
  Variant &gv_log_directory __attribute__((__unused__)) = g->GV(log_directory);
  Variant v_log_directory;
  Variant v_script_name;
  String v_filename;
  String v_dirname;
  String v_out_string;
  Variant v_out;
  String v_output_contents;


  class VariableTable : public RVariableTable {
  public:
    Variant &v_test_name; Variant &v_subject; Variant &v_commands; Variant &v_outs; Variant &v_errs; Variant &v_exits; Variant &v_missing_dependency; Variant &v_reason; String &v_red; String &v_reset; String &v_err_string; String &v_command_string; Variant &v_command; Primitive &v_i; Variant &v_exit; Variant &v_err; String &v_reason_string; String &v_dependency_string; String &v_header; Variant &v_log_directory; Variant &v_script_name; String &v_filename; String &v_dirname; String &v_out_string; Variant &v_out; String &v_output_contents;
    VariableTable(Variant &r_test_name, Variant &r_subject, Variant &r_commands, Variant &r_outs, Variant &r_errs, Variant &r_exits, Variant &r_missing_dependency, Variant &r_reason, String &r_red, String &r_reset, String &r_err_string, String &r_command_string, Variant &r_command, Primitive &r_i, Variant &r_exit, Variant &r_err, String &r_reason_string, String &r_dependency_string, String &r_header, Variant &r_log_directory, Variant &r_script_name, String &r_filename, String &r_dirname, String &r_out_string, Variant &r_out, String &r_output_contents) : v_test_name(r_test_name), v_subject(r_subject), v_commands(r_commands), v_outs(r_outs), v_errs(r_errs), v_exits(r_exits), v_missing_dependency(r_missing_dependency), v_reason(r_reason), v_red(r_red), v_reset(r_reset), v_err_string(r_err_string), v_command_string(r_command_string), v_command(r_command), v_i(r_i), v_exit(r_exit), v_err(r_err), v_reason_string(r_reason_string), v_dependency_string(r_dependency_string), v_header(r_header), v_log_directory(r_log_directory), v_script_name(r_script_name), v_filename(r_filename), v_dirname(r_dirname), v_out_string(r_out_string), v_out(r_out), v_output_contents(r_output_contents) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 63) {
        case 1:
          HASH_RETURN(0x6D72CEB44AC01301LL, v_test_name,
                      test_name);
          break;
        case 2:
          HASH_RETURN(0x15E266045DABA002LL, v_errs,
                      errs);
          break;
        case 3:
          HASH_RETURN(0x189A3C89F0CA1103LL, v_commands,
                      commands);
          break;
        case 9:
          HASH_RETURN(0x68AC28536BC41349LL, v_out_string,
                      out_string);
          break;
        case 11:
          HASH_RETURN(0x52BB0FA73DE2384BLL, v_err_string,
                      err_string);
          break;
        case 12:
          HASH_RETURN(0x2D82AF0F4EA2D20CLL, v_red,
                      red);
          break;
        case 17:
          HASH_RETURN(0x6C2E2CD6695D83D1LL, v_log_directory,
                      log_directory);
          break;
        case 21:
          HASH_RETURN(0x0A73C2ABB5E5CAD5LL, v_dependency_string,
                      dependency_string);
          break;
        case 23:
          HASH_RETURN(0x1061B1D6510FB917LL, v_reason_string,
                      reason_string);
          break;
        case 24:
          HASH_RETURN(0x0EB22EDA95766E98LL, v_i,
                      i);
          break;
        case 26:
          HASH_RETURN(0x461F139A4051755ALL, v_exit,
                      exit);
          break;
        case 28:
          HASH_RETURN(0x3D74198A5F0F7DDCLL, v_outs,
                      outs);
          break;
        case 30:
          HASH_RETURN(0x55844568CE9AE49ELL, v_script_name,
                      script_name);
          break;
        case 34:
          HASH_RETURN(0x7C801AC012E9F722LL, v_out,
                      out);
          break;
        case 37:
          HASH_RETURN(0x2B5C11D45C8999E5LL, v_reset,
                      reset);
          break;
        case 44:
          HASH_RETURN(0x097D5F1BF422236CLL, v_command_string,
                      command_string);
          break;
        case 45:
          HASH_RETURN(0x4A5D0431E1A3CAEDLL, v_filename,
                      filename);
          break;
        case 48:
          HASH_RETURN(0x7F6EA1943116D530LL, v_dirname,
                      dirname);
          break;
        case 49:
          HASH_RETURN(0x150ACBD97C259031LL, v_reason,
                      reason);
          break;
        case 50:
          HASH_RETURN(0x0EDD9FCC9525C3B2LL, v_subject,
                      subject);
          break;
        case 52:
          HASH_RETURN(0x296ACA8C8277C574LL, v_output_contents,
                      output_contents);
          break;
        case 53:
          HASH_RETURN(0x382AF294107C2C75LL, v_err,
                      err);
          break;
        case 59:
          HASH_RETURN(0x2A35E05780AA8E3BLL, v_command,
                      command);
          break;
        case 61:
          HASH_RETURN(0x070E77FC472C4C7DLL, v_header,
                      header);
          break;
        case 63:
          HASH_RETURN(0x1AE9F4DC5F326DFFLL, v_exits,
                      exits);
          HASH_RETURN(0x6D25C7F2292DC53FLL, v_missing_dependency,
                      missing_dependency);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_test_name, v_subject, v_commands, v_outs, v_errs, v_exits, v_missing_dependency, v_reason, v_red, v_reset, v_err_string, v_command_string, v_command, v_i, v_exit, v_err, v_reason_string, v_dependency_string, v_header, v_log_directory, v_script_name, v_filename, v_dirname, v_out_string, v_out, v_output_contents);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_red = LINE(334,f_red_string()));
  (v_reset = LINE(335,f_reset_string()));
  if (toBoolean(LINE(338,x_count(less(v_commands, x_count(v_exits)))))) v_commands.append(("Missing command"));
  LINE(341,x_assert(equal_rev(x_count(v_errs), x_count(v_outs))));
  LINE(342,x_assert(equal_rev(x_count(v_errs), x_count(v_exits))));
  LINE(343,x_assert(equal_rev(x_count(v_errs), x_count(v_commands))));
  (v_err_string = "");
  if (LINE(348,x_is_array(v_commands))) {
    if (equal(v_exits, "Not relevent")) {
      (v_command_string = "");
      {
        LOOP_COUNTER(14);
        for (ArrayIterPtr iter16 = v_commands.begin(); !iter16->end(); iter16->next()) {
          LOOP_COUNTER_CHECK(14);
          v_command = iter16->second();
          {
            concat_assign(v_command_string, LINE(355,concat6(v_red, "Command:", v_reset, " ", toString(v_command), "\n")));
          }
        }
      }
    }
    else {
      LINE(360,(assignCallTemp(eo_0, x_is_array(v_exits)),f_phc_assert(eo_0, "Expected array of return values")));
      LINE(362,(assignCallTemp(eo_0, equal_rev(LINE(361,x_count(v_errs)), x_count(v_exits))),f_phc_assert(eo_0, "Expected same number of exits as exit codes")));
      (v_command_string = "");
      (v_err_string = "");
      {
        LOOP_COUNTER(17);
        for (ArrayIterPtr iter19 = v_exits.begin(); !iter19->end(); iter19->next()) {
          LOOP_COUNTER_CHECK(17);
          v_exit = iter19->second();
          v_i = iter19->first();
          {
            if (!(isset(v_commands, v_i))) {
              LINE(370,x_var_dump(4, v_outs, Array(ArrayInit(3).set(0, v_exits).set(1, v_commands).set(2, v_errs).create())));
              f_exit();
            }
            (v_command = v_commands.rvalAt(v_i));
            (v_err = v_errs.rvalAt(v_i));
            concat_assign(v_command_string, concat_rev(LINE(377,concat6(v_reset, " (", toString(v_exit), "): ", toString(v_command), "\n")), concat3(v_red, "Command ", toString(v_i))));
            if (toBoolean(v_err)) concat_assign(v_err_string, concat(v_red, LINE(379,concat6("Error ", toString(v_i), v_reset, ": ", toString(v_err), "\n"))));
          }
        }
      }
    }
  }
  else {
    (v_command_string = LINE(386,(assignCallTemp(eo_0, v_red),assignCallTemp(eo_2, concat6(v_reset, " (", toString(v_exits), "): ", toString(v_commands), "\n")),concat3(eo_0, "Command", eo_2))));
    if (toBoolean(v_errs)) (v_err_string = LINE(388,concat6(v_red, "Error", v_reset, ": ", toString(v_errs), "\n")));
  }
  (v_reason_string = LINE(391,concat3("Reason: ", toString(v_reason), "\n")));
  (v_dependency_string = "");
  if (toBoolean(v_missing_dependency)) {
    (v_dependency_string = LINE(396,concat3("NOTE: dependency ", toString(v_missing_dependency), " is missing. This may be the cause of this failure\n")));
  }
  (v_header = LINE(399,concat4(v_reason_string, v_dependency_string, v_command_string, v_err_string)));
  v_log_directory = ref(g->GV(log_directory));
  (v_script_name = LINE(403,f_adjusted_name(v_subject)));
  (v_filename = LINE(404,concat6(toString(v_log_directory), "/", toString(v_test_name), "/", toString(v_script_name), ".log")));
  (v_dirname = LINE(405,x_dirname(v_filename)));
  if (!(LINE(406,x_is_dir(v_dirname)))) {
    (silenceInc(), silenceDec(LINE(408,x_mkdir(v_dirname, 493LL, true))));
    LINE(409,(assignCallTemp(eo_0, x_is_dir(v_dirname)),f_phc_assert(eo_0, "directory not created")));
  }
  if (!(LINE(413,x_is_array(v_outs)))) (v_outs = Array(ArrayInit(1).set(0, v_outs).create()));
  (v_out_string = "");
  if (more(LINE(418,x_count(v_outs)), 1LL) && toBoolean(x_count(x_array_filter(v_outs, "strlen")))) {
    {
      LOOP_COUNTER(20);
      for (ArrayIterPtr iter22 = v_outs.begin(); !iter22->end(); iter22->next()) {
        LOOP_COUNTER_CHECK(20);
        v_out = iter22->second();
        v_i = iter22->first();
        {
          (v_output_contents = LINE(422,concat4("Command: ", toString(variables->get(toString(v_commands.rvalAt(v_i)))), "\n", toString(v_out))));
          LINE(423,(assignCallTemp(eo_0, concat3(v_filename, ".out.", toString(v_i))),assignCallTemp(eo_1, v_output_contents),x_file_put_contents(eo_0, eo_1)));
        }
      }
    }
  }
  {
    LOOP_COUNTER(23);
    for (ArrayIterPtr iter25 = v_outs.begin(); !iter25->end(); iter25->next()) {
      LOOP_COUNTER_CHECK(23);
      v_out = iter25->second();
      v_i = iter25->first();
      {
        if (more(LINE(430,x_strlen(toString(v_out))), 1000LL)) (v_out = concat(toString(LINE(431,x_substr(toString(v_out), toInt32(0LL), toInt32(1000LL)))), "... [truncated]\n"));
        concat_assign(v_out_string, concat(v_red, LINE(433,concat6("Output ", toString(v_i), v_reset, ":\n", toString(v_out), "\n"))));
      }
    }
  }
  LINE(437,x_file_put_contents(v_filename, v_header));
  LINE(438,(assignCallTemp(eo_0, v_filename),assignCallTemp(eo_1, x_rtrim(v_out_string)),x_file_put_contents(eo_0, eo_1, toInt32(8LL) /* FILE_APPEND */)));
  LINE(439,x_file_put_contents(v_filename, "\n", toInt32(8LL) /* FILE_APPEND */));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 220 */
void f_open_status_files() {
  FUNCTION_INJECTION(open_status_files);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_status_files __attribute__((__unused__)) = g->GV(status_files);
  Variant &gv_log_directory __attribute__((__unused__)) = g->GV(log_directory);
  Variant v_status;

  {
    LOOP_COUNTER(26);
    Variant map27 = ScalarArrays::sa_[10];
    for (ArrayIterPtr iter28 = map27.begin(); !iter28->end(); iter28->next()) {
      LOOP_COUNTER_CHECK(26);
      v_status = iter28->second();
      {
        (toBoolean(gv_status_files.set(v_status, (LINE(226,(assignCallTemp(eo_0, concat3(toString(gv_log_directory), "/", toString(v_status))),x_fopen(eo_0, "w"))))))) || (toBoolean(f_exit(concat3("Cannot open ", toString(v_status), " file\n"))));
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/header.php line 286 */
String f_red_string() {
  FUNCTION_INJECTION(red_string);
  return LINE(288,x_sprintf(2, "%c[1;31m", ScalarArrays::sa_[11]));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 248 */
void f_close_status_files() {
  FUNCTION_INJECTION(close_status_files);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_status_files __attribute__((__unused__)) = g->GV(status_files);
  Variant v_file;

  {
    LOOP_COUNTER(29);
    for (ArrayIterPtr iter31 = gv_status_files.begin(); !iter31->end(); iter31->next()) {
      LOOP_COUNTER_CHECK(29);
      v_file = iter31->second();
      LINE(252,x_fclose(toObject(v_file)));
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/header.php line 677 */
Variant f_homogenize_filenames_and_line_numbers(Variant v_string, CVarRef v_filename) {
  FUNCTION_INJECTION(homogenize_filenames_and_line_numbers);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_base_dir __attribute__((__unused__)) = g->GV(base_dir);
  String v_stdin_filename;
  String v_full_filename;

  (v_stdin_filename = toString(gv_base_dir) + toString("/-"));
  (v_full_filename = LINE(683,concat3(toString(gv_base_dir), "/", toString(v_filename))));
  (v_string = LINE(687,x_preg_replace("/Warning: Unknown:/", "Warning:", v_string)));
  (v_string = LINE(688,x_preg_replace("/fatal error: Unknown:/", "Fatal error:", v_string)));
  (v_string = LINE(689,x_preg_replace("/Catchable fatal error: Unknown:/", "Catchable fatal error:", v_string)));
  (v_string = LINE(694,x_preg_replace("/(Warning: )(\\S*: )\?(.+\? in )\\S+ on line \\d+/", "$1$3", v_string)));
  (v_string = LINE(695,x_preg_replace("/(Fatal error: )(\\S*: )\?(.+\? in )\\S+ on line \\d+/", "$1$3", v_string)));
  (v_string = LINE(696,x_preg_replace("/(Catchable fatal error: .+\? in )\\S+ on line \\d+/", "$1", v_string)));
  (v_string = LINE(698,x_preg_replace("/on line \\d+/", "on line __LINE__", v_string)));
  (v_string = LINE(699,(assignCallTemp(eo_0, concat3("!", v_full_filename, "(:\\d+)\?!")),assignCallTemp(eo_2, v_string),x_preg_replace(eo_0, "__FILENAME__", eo_2))));
  (v_string = LINE(700,x_preg_replace("!\\[no active file\\](:\\d+)\?!", "__FILENAME__", v_string)));
  (v_string = LINE(701,(assignCallTemp(eo_0, concat3("!", toString(v_filename), "(:\\d+)\?!")),assignCallTemp(eo_2, v_string),x_preg_replace(eo_0, "__FILENAME__", eo_2))));
  (v_string = LINE(702,(assignCallTemp(eo_0, concat3("!", v_stdin_filename, "(:\\d+)\?!")),assignCallTemp(eo_2, v_string),x_preg_replace(eo_0, "__FILENAME__", eo_2))));
  (v_string = LINE(704,x_preg_replace("!__FILENAME__ on line __LINE__!", "", v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 770 */
String f_copy_to_working_dir(CStrRef v_file) {
  FUNCTION_INJECTION(copy_to_working_dir);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_working_directory __attribute__((__unused__)) = g->GV(working_directory);
  String v_filename;
  String v_new_file;

  (v_filename = LINE(773,x_basename(v_file)));
  (v_new_file = LINE(774,concat3(toString(gv_working_directory), "/", v_filename)));
  LINE(775,x_copy(v_file, v_new_file));
  LINE(776,(assignCallTemp(eo_0, v_new_file),assignCallTemp(eo_1, toInt64(x_fileperms(v_file))),x_chmod(eo_0, eo_1)));
  return v_new_file;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 655 */
Variant f_read_dependency(CVarRef v_test_name, CVarRef v_subject) {
  FUNCTION_INJECTION(read_dependency);
  String v_filename;

  (v_filename = LINE(657,f_get_dependent_filename(v_test_name, v_subject)));
  if (!(LINE(658,x_file_exists(v_filename)))) return "missing";
  else return LINE(661,x_file_get_contents(v_filename));
  return null;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 315 */
Variant f_diff(CVarRef v_string1, CVarRef v_string2) {
  FUNCTION_INJECTION(diff);
  Variant v_result;

  if (!(LINE(317,x_extension_loaded("xdiff")))) {
    (v_result = (silenceInc(), silenceDec(LINE(319,x_dl("xdiff.so")))));
    if (!(toBoolean(v_result))) {
      return LINE(322,concat4("Note: xdiff not available for diffing. Outputting both strings:\nString1:\n", toString(v_string1), "\nString2:\n", toString(v_string2)));
    }
  }
  if (more(LINE(326,x_strlen(toString(v_string1))), 5000000LL) || more(x_strlen(toString(v_string2)), 5000000LL)) return LINE(327,concat4("Too big to xdiff. Outputting both strings:\nString1:\n", toString(v_string1), "\nString2:\n", toString(v_string2)));
  return LINE(329,invoke_failed("xdiff_string_diff", Array(ArrayInit(2).set(0, toString(v_string1) + toString("\n")).set(1, toString(v_string2) + toString("\n")).create()), 0x00000000DC28AE27LL));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 38 */
String f_get_phc_compile_plugin() {
  FUNCTION_INJECTION(get_phc_compile_plugin);
  String v_phc_compile_plugin;
  Variant v_cwd;
  Variant v_phc;

  (v_phc_compile_plugin = "src/phc_compile_plugin");
  if (((!(LINE(42,x_file_exists(v_phc_compile_plugin)))) && (x_is_file(v_phc_compile_plugin))) && (x_is_executable(v_phc_compile_plugin))) {
    (v_cwd = LINE(44,x_getcwd()));
    f_exit(LINE(45,concat5("Error: The current directory, ", toString(v_cwd), ", does not contain the phc executable '", toString(v_phc), "'\n")));
  }
  (v_phc_compile_plugin = LINE(48,f_copy_to_working_dir(v_phc_compile_plugin)));
  return v_phc_compile_plugin;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 255 */
void f_phc_assert(CVarRef v_boolean, CVarRef v_message) {
  FUNCTION_INJECTION(phc_assert);
  if (!(toBoolean(v_boolean))) {
    LINE(259,x_trigger_error(toString(v_message)));
  }
} /* function */
/* SRC: phc-test/framework/lib/header.php line 237 */
void f_write_status(CStrRef v_status, CVarRef v_string) {
  FUNCTION_INJECTION(write_status);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_status_files __attribute__((__unused__)) = g->GV(status_files);
  Variant v_file;

  (v_file = gv_status_files.rvalAt(v_status));
  LINE(242,x_fwrite(toObject(v_file), toString(v_string)));
  LINE(245,x_fflush(toObject(v_file)));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 291 */
Variant f_strip_colour(CStrRef v_string) {
  FUNCTION_INJECTION(strip_colour);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Array v_codes;

  (v_codes = (assignCallTemp(eo_0, LINE(293,f_red_string())),assignCallTemp(eo_1, f_green_string()),assignCallTemp(eo_2, f_blue_string()),assignCallTemp(eo_3, f_reset_string()),Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create())));
  return LINE(294,x_str_replace(v_codes, "", v_string));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 299 */
bool f_is_32_bit() {
  FUNCTION_INJECTION(is_32_bit);
  if (LINE(302,x_is_integer(2147483658.0))) {
    return false;
  }
  return true;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 442 */
Variant f_adjusted_name(Variant v_script_name, int64 v_adjust_for_regression //  = 0LL
) {
  FUNCTION_INJECTION(adjusted_name);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_subject_dir __attribute__((__unused__)) = g->GV(subject_dir);
  Variant v_prefix;

  LINE(445,f_phc_assert(!same(gv_subject_dir, ""), "subject_dir must be defined before this function is called"));
  if (toBoolean(v_adjust_for_regression)) {
    if (toBoolean(LINE(451,f_is_labelled(v_script_name, "size-dependent")))) {
      if (LINE(453,f_is_32_bit())) {
        concat_assign(v_script_name, ".32bit");
      }
      else {
        concat_assign(v_script_name, ".64bit");
      }
    }
  }
  (v_prefix = gv_subject_dir);
  (v_prefix = LINE(466,x_preg_replace("/\\//", "\\/", v_prefix)));
  (v_script_name = LINE(467,(assignCallTemp(eo_0, concat3("/", toString(v_prefix), "/")),assignCallTemp(eo_2, v_script_name),x_preg_replace(eo_0, "", eo_2))));
  return v_script_name;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 629 */
String f_get_dependent_filename(CVarRef v_test_name, CVarRef v_subject) {
  FUNCTION_INJECTION(get_dependent_filename);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_result;
  bool v_is_made = false;

  (v_result = LINE(631,concat4("test/dependencies/", toString(v_test_name), "/", toString(v_subject))));
  if (!(LINE(634,x_file_exists(x_dirname(v_result))))) {
    (v_is_made = LINE(636,(assignCallTemp(eo_0, x_dirname(v_result)),x_mkdir(eo_0, 448LL, true))));
    LINE(637,x_assert(v_is_made));
  }
  return v_result;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 642 */
void f_write_dependencies(CVarRef v_test_name, CVarRef v_subject, bool v_value) {
  FUNCTION_INJECTION(write_dependencies);
  String v_filename;

  (v_filename = LINE(644,f_get_dependent_filename(v_test_name, v_subject)));
  if (equal(v_value, true)) {
    LINE(647,x_file_put_contents(v_filename, "Pass"));
  }
  else {
    LINE(651,x_file_put_contents(v_filename, "Fail"));
  }
} /* function */
/* SRC: phc-test/framework/lib/header.php line 280 */
String f_green_string() {
  FUNCTION_INJECTION(green_string);
  return LINE(282,x_sprintf(2, "%c[1;32m", ScalarArrays::sa_[11]));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 274 */
String f_blue_string() {
  FUNCTION_INJECTION(blue_string);
  return LINE(276,x_sprintf(2, "%c[1;34m", ScalarArrays::sa_[11]));
} /* function */
/* SRC: phc-test/framework/lib/header.php line 472 */
Array f_complete_exec(CVarRef v_command, CVarRef v_stdin //  = null_variant
, int64 v_timeout //  = 20LL
, bool v_pass_through //  = false
) {
  FUNCTION_INJECTION(complete_exec);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_verbose __attribute__((__unused__)) = g->GV(opt_verbose);
  Variant v_opt_verbose;
  Array v_descriptorspec;
  Variant v_pipes;
  Variant v_handle;
  Variant v_out;
  Variant v_err;
  int v_start_time = 0;
  Array v_status;
  Variant v_new_out;
  Variant v_new_err;
  Variant v_exit_code;

  v_opt_verbose = ref(g->GV(opt_verbose));
  if (toBoolean(v_opt_verbose)) print(LINE(476,concat3("Running command: ", toString(v_command), "\n")));
  (v_descriptorspec = ScalarArrays::sa_[12]);
  (v_pipes = ScalarArrays::sa_[3]);
  (v_handle = LINE(482,(assignCallTemp(eo_0, toString(v_command)),assignCallTemp(eo_1, v_descriptorspec),assignCallTemp(eo_2, ref(v_pipes)),assignCallTemp(eo_3, toString(x_getcwd())),x_proc_open(eo_0, eo_1, eo_2, eo_3))));
  if (!same(v_stdin, null)) LINE(486,x_fwrite(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_stdin)));
  LINE(488,x_fclose(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  v_pipes.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
  LINE(492,x_stream_set_blocking(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(0LL)));
  LINE(493,x_stream_set_blocking(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)), toInt32(0LL)));
  (v_out = "");
  (v_err = "");
  (v_start_time = LINE(498,x_time()));
  {
    LOOP_COUNTER(32);
    do {
      LOOP_COUNTER_CHECK(32);
      {
        (v_status = LINE(501,x_proc_get_status(toObject(v_handle))));
        (v_new_out = LINE(507,x_stream_get_contents(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
        (v_new_err = LINE(508,x_stream_get_contents(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
        concat_assign(v_out, toString(v_new_out));
        concat_assign(v_err, toString(v_new_err));
        if (v_pass_through) {
          print(toString(v_new_out));
          LINE(515,x_file_put_contents("php://stderr", v_new_err));
        }
        if (!equal(v_timeout, 0LL) && more(LINE(519,x_time()), v_start_time + v_timeout)) {
          (v_out = LINE(521,x_stream_get_contents(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
          (v_err = LINE(522,x_stream_get_contents(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
          LINE(524,f_kill_properly(ref(v_handle), ref(v_pipes)));
          return Array(ArrayInit(3).set(0, "Timeout").set(1, v_out).set(2, v_err).create());
        }
        LINE(532,x_usleep(toInt32(10000LL)));
      }
    } while (toBoolean(v_status.rvalAt("running", 0x2947552314E5C43CLL)));
  }
  LINE(535,x_stream_set_blocking(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(1LL)));
  LINE(536,x_stream_set_blocking(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)), toInt32(1LL)));
  concat_assign(v_out, toString(LINE(537,x_stream_get_contents(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL))))));
  concat_assign(v_err, toString(LINE(538,x_stream_get_contents(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL))))));
  (v_exit_code = v_status.rvalAt("exitcode", 0x3FCBA2655EC7FA85LL));
  LINE(542,f_kill_properly(ref(v_handle), ref(v_pipes)));
  return Array(ArrayInit(3).set(0, v_out).set(1, v_err).set(2, v_exit_code).create());
} /* function */
/* SRC: phc-test/framework/lib/header.php line 744 */
Variant f_homogenize_object_count(Variant v_string) {
  FUNCTION_INJECTION(homogenize_object_count);
  (v_string = LINE(757,x_preg_replace("/^(object\\(\\w+\\)#)\\d+/m", "$1x", v_string)));
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/header.php line 9 */
String f_get_phc() {
  FUNCTION_INJECTION(get_phc);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_valgrind __attribute__((__unused__)) = g->GV(opt_valgrind);
  Variant &gv_phc_suffix __attribute__((__unused__)) = g->GV(phc_suffix);
  Variant &gv_valgrind __attribute__((__unused__)) = g->GV(valgrind);
  Variant &gv_libphp __attribute__((__unused__)) = g->GV(libphp);
  Variant &gv_working_dir __attribute__((__unused__)) = g->GV(working_dir);
  String v_phc;
  Variant v_cwd;

  {
  }
  (v_phc = toString("src/phc") + toString(gv_phc_suffix));
  if (!((((LINE(17,x_file_exists(v_phc))) && (x_is_file(v_phc))) && (x_is_executable(v_phc))))) {
    (v_cwd = LINE(19,x_getcwd()));
    f_exit(LINE(20,concat5("Error: The current directory, ", toString(v_cwd), ", does not contain the phc executable '", v_phc, "'\n")));
  }
  (v_phc = LINE(26,f_copy_to_working_dir(v_phc)));
  if (toBoolean(gv_opt_valgrind)) {
    if (!(toBoolean(gv_valgrind))) f_exit("Error: Valgrind not available (see output from ./configure)\n");
    (v_phc = LINE(32,concat3(toString(gv_valgrind), " -q --suppressions=misc/valgrind_suppressions --leak-check=no ", v_phc)));
  }
  return v_phc;
} /* function */
Variant i_phc_error_handler(CArrRef params) {
  return (f_phc_error_handler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
}
Variant i_phc_unreachable(CArrRef params) {
  int count = params.size();
  if (count <= 0) return (f_phc_unreachable(), null);
  return (f_phc_unreachable(params.rvalAt(0)), null);
}
Variant i_phc_assert(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x13F7134C2403FBF9LL, phc_assert) {
    return (f_phc_assert(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$framework$lib$header_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/header.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$header_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_strict __attribute__((__unused__)) = (variables != gVariables) ? variables->get("strict") : g->GV(strict);

  (v_strict = false);
  LINE(218,x_set_error_handler("phc_error_handler"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
