
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <php/phc-test/framework/lib/regression.h>
#include <php/phc-test/framework/lib/support_file_test.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/lib/regression.php line 8 */
Variant c_regressiontest::os_get(const char *s, int64 hash) {
  return c_supportfiletest::os_get(s, hash);
}
Variant &c_regressiontest::os_lval(const char *s, int64 hash) {
  return c_supportfiletest::os_lval(s, hash);
}
void c_regressiontest::o_get(ArrayElementVec &props) const {
  c_supportfiletest::o_get(props);
}
bool c_regressiontest::o_exists(CStrRef s, int64 hash) const {
  return c_supportfiletest::o_exists(s, hash);
}
Variant c_regressiontest::o_get(CStrRef s, int64 hash) {
  return c_supportfiletest::o_get(s, hash);
}
Variant c_regressiontest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_supportfiletest::o_set(s, hash, v, forInit);
}
Variant &c_regressiontest::o_lval(CStrRef s, int64 hash) {
  return c_supportfiletest::o_lval(s, hash);
}
Variant c_regressiontest::os_constant(const char *s) {
  return c_supportfiletest::os_constant(s);
}
IMPLEMENT_CLASS(regressiontest)
ObjectData *c_regressiontest::create(CStrRef v_name, CStrRef v_command_line_options, CStrRef v_support_file_suffix, CArrRef v_dependencies //  = ScalarArrays::sa_[3]
) {
  init();
  t_regressiontest(v_name, v_command_line_options, v_support_file_suffix, v_dependencies);
  return this;
}
ObjectData *c_regressiontest::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  } else return this;
}
ObjectData *c_regressiontest::cloneImpl() {
  c_regressiontest *obj = NEW(c_regressiontest)();
  cloneSet(obj);
  return obj;
}
void c_regressiontest::cloneSet(c_regressiontest *clone) {
  c_supportfiletest::cloneSet(clone);
}
Variant c_regressiontest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      break;
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 9:
      HASH_GUARD(0x09F602C072269E49LL, get_support_filename) {
        return (t_get_support_filename(params.rvalAt(0)));
      }
      break;
    case 11:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      HASH_GUARD(0x261D586D2BEF01D1LL, generate_support_files) {
        return (t_generate_support_files());
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 26:
      HASH_GUARD(0x31C19FCBE91F96FALL, generate_support_file) {
        return (t_generate_support_file(params.rvalAt(0)), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_supportfiletest::o_invoke(s, params, hash, fatal);
}
Variant c_regressiontest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 2:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      break;
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 9:
      HASH_GUARD(0x09F602C072269E49LL, get_support_filename) {
        return (t_get_support_filename(a0));
      }
      break;
    case 11:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      HASH_GUARD(0x261D586D2BEF01D1LL, generate_support_files) {
        return (t_generate_support_files());
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 26:
      HASH_GUARD(0x31C19FCBE91F96FALL, generate_support_file) {
        return (t_generate_support_file(a0), null);
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_supportfiletest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_regressiontest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_supportfiletest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_regressiontest$os_get(const char *s) {
  return c_regressiontest::os_get(s, -1);
}
Variant &cw_regressiontest$os_lval(const char *s) {
  return c_regressiontest::os_lval(s, -1);
}
Variant cw_regressiontest$os_constant(const char *s) {
  return c_regressiontest::os_constant(s);
}
Variant cw_regressiontest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_regressiontest::os_invoke(c, s, params, -1, fatal);
}
void c_regressiontest::init() {
  c_supportfiletest::init();
}
/* SRC: phc-test/framework/lib/regression.php line 11 */
void c_regressiontest::t_regressiontest(CStrRef v_name, CStrRef v_command_line_options, CStrRef v_support_file_suffix, CArrRef v_dependencies //  = ScalarArrays::sa_[3]
) {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::RegressionTest);
  bool oldInCtor = gasInCtor(true);
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = v_name);
  (o_lval("options", 0x7C17922060DCA1EALL) = v_command_line_options);
  (o_lval("suffix", 0x3A99F3A12BB2F488LL) = v_support_file_suffix);
  (o_lval("dependencies", 0x36DCDDF5BB1EBEBDLL) = v_dependencies);
  LINE(17,c_supportfiletest::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 20 */
Array c_regressiontest::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::get_dependent_test_names);
  return ScalarArrays::sa_[8];
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 25 */
Variant c_regressiontest::t_get_name() {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::get_name);
  return o_get("name", 0x0BCDB293DC3CBDDCLL);
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 30 */
String c_regressiontest::t_get_support_filename(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::get_support_filename);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_support_dir __attribute__((__unused__)) = g->GV(support_dir);
  Variant v_script_name;

  (v_script_name = LINE(33,f_adjusted_name(v_subject, 1LL)));
  return concat(toString(gv_support_dir), LINE(34,concat6("/", toString(o_get("name", 0x0BCDB293DC3CBDDCLL)), "/", toString(v_script_name), ".", toString(o_get("suffix", 0x3A99F3A12BB2F488LL)))));
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 37 */
Variant c_regressiontest::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::get_test_subjects);
  return LINE(39,f_get_all_scripts());
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 42 */
String c_regressiontest::t_get_command_line(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::get_command_line);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  return LINE(45,concat5(toString(gv_phc), " ", toString(o_get("options", 0x7C17922060DCA1EALL)), " ", toString(v_subject)));
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 48 */
void c_regressiontest::t_run_test(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::run_test);
  Variant v_regression_file;
  String v_command;
  Variant v_diff;
  Variant v_err;
  Variant v_exit;

  (v_regression_file = LINE(50,o_root_invoke_few_args("get_support_filename", 0x09F602C072269E49LL, 1, v_subject)));
  if (LINE(53,x_file_exists(toString(v_regression_file)))) {
    (v_command = LINE(55,t_get_command_line(v_subject)));
    concat_assign(v_command, LINE(56,concat3(" | diff -u ", toString(v_regression_file), " -")));
    df_lambda_4(LINE(57,f_complete_exec(v_command)), v_diff, v_err, v_exit);
    if (toBoolean(v_diff)) {
      LINE(62,t_mark_failure(v_subject, v_command, v_diff, v_err, v_exit));
    }
    else {
      LINE(66,t_mark_success(v_subject));
    }
  }
  else {
    LINE(71,t_mark_skipped(v_subject, toString("No regression support file at ") + toString(v_regression_file)));
  }
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 76 */
Variant c_regressiontest::t_generate_support_files() {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::generate_support_files);
  Variant v_files;
  Variant v_subject;

  if (!(toBoolean(LINE(78,o_root_invoke_few_args("check_prerequisites", 0x547CFD01CE2F8190LL, 0))))) {
    LINE(80,t_mark_skipped("All", "Prerequisites failed"));
    return false;
  }
  (v_files = LINE(84,o_root_invoke_few_args("get_test_subjects", 0x268048E176687DE2LL, 0)));
  LINE(85,t_ready_progress_bar(x_count(v_files)));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_files.begin("regressiontest"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_subject = iter3->second();
      {
        if (!(LINE(89,t_check_test_prerequisites(v_subject)))) {
          LINE(91,t_mark_skipped(v_subject, "Test prerequsites failed"));
        }
        else {
          LINE(95,o_root_invoke_few_args("generate_support_file", 0x31C19FCBE91F96FALL, 1, v_subject));
        }
      }
    }
  }
  LINE(99,o_root_invoke_few_args("finish_test", 0x76D94AF9AA95C111LL, 0));
  return null;
} /* function */
/* SRC: phc-test/framework/lib/regression.php line 102 */
void c_regressiontest::t_generate_support_file(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(RegressionTest, RegressionTest::generate_support_file);
  Variant eo_0;
  Variant eo_1;
  String v_command;
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  (v_command = LINE(105,t_get_command_line(v_subject)));
  df_lambda_5(LINE(106,f_complete_exec(v_command)), v_out, v_err, v_exit);
  if (equal(v_exit, 0LL)) {
    LINE(110,t_write_support_file(v_out, v_subject));
    LINE(111,t_mark_success(v_subject));
  }
  else {
    (assignCallTemp(eo_0, v_subject),assignCallTemp(eo_1, LINE(115,concat3("Non zero exit code (", toString(v_exit), ")"))),t_mark_skipped(eo_0, eo_1));
  }
} /* function */
Object co_regressiontest(CArrRef params, bool init /* = true */) {
  return Object(p_regressiontest(NEW(c_regressiontest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$regression_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/regression.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$regression_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(6,pm_php$phc_test$framework$lib$support_file_test_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
