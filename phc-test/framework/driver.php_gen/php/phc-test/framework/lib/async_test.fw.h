
#ifndef __GENERATED_php_phc_test_framework_lib_async_test_fw_h__
#define __GENERATED_php_phc_test_framework_lib_async_test_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(asyncbundle)
FORWARD_DECLARE_CLASS(asynctest)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/framework/lib/test.fw.h>
#include <php/phc-test/framework/reduce/Reduce.fw.h>

#endif // __GENERATED_php_phc_test_framework_lib_async_test_fw_h__
