
#ifndef __GENERATED_php_phc_test_framework_lib_async_test_h__
#define __GENERATED_php_phc_test_framework_lib_async_test_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/async_test.fw.h>

// Declarations
#include <cls/asyncbundle.h>
#include <cls/asynctest.h>
#include <php/phc-test/framework/reduce/Reduce.h>
#include <php/phc-test/framework/lib/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$lib$async_test_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_inst(CStrRef v_string);
Object co_asyncbundle(CArrRef params, bool init = true);
Object co_asynctest(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_async_test_h__
