
#ifndef __GENERATED_php_phc_test_framework_lib_compare_backwards_h__
#define __GENERATED_php_phc_test_framework_lib_compare_backwards_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/lib/compare_backwards.fw.h>

// Declarations
#include <cls/comparebackwards.h>
#include <php/phc-test/framework/lib/async_test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$lib$compare_backwards_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_get_pass_list();
Object co_comparebackwards(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_lib_compare_backwards_h__
