
#include <php/phc-test/framework/lib/support_file_test.h>
#include <php/phc-test/framework/lib/test.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/support_file_test.php line 7 */
Variant c_supportfiletest::os_get(const char *s, int64 hash) {
  return c_test::os_get(s, hash);
}
Variant &c_supportfiletest::os_lval(const char *s, int64 hash) {
  return c_test::os_lval(s, hash);
}
void c_supportfiletest::o_get(ArrayElementVec &props) const {
  c_test::o_get(props);
}
bool c_supportfiletest::o_exists(CStrRef s, int64 hash) const {
  return c_test::o_exists(s, hash);
}
Variant c_supportfiletest::o_get(CStrRef s, int64 hash) {
  return c_test::o_get(s, hash);
}
Variant c_supportfiletest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test::o_set(s, hash, v, forInit);
}
Variant &c_supportfiletest::o_lval(CStrRef s, int64 hash) {
  return c_test::o_lval(s, hash);
}
Variant c_supportfiletest::os_constant(const char *s) {
  return c_test::os_constant(s);
}
IMPLEMENT_CLASS(supportfiletest)
ObjectData *c_supportfiletest::cloneImpl() {
  c_supportfiletest *obj = NEW(c_supportfiletest)();
  cloneSet(obj);
  return obj;
}
void c_supportfiletest::cloneSet(c_supportfiletest *clone) {
  c_test::cloneSet(clone);
}
Variant c_supportfiletest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 11:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      HASH_GUARD(0x261D586D2BEF01D1LL, generate_support_files) {
        return (t_generate_support_files());
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke(s, params, hash, fatal);
}
Variant c_supportfiletest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 11:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      HASH_GUARD(0x261D586D2BEF01D1LL, generate_support_files) {
        return (t_generate_support_files());
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_supportfiletest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test::os_invoke(c, s, params, hash, fatal);
}
Variant cw_supportfiletest$os_get(const char *s) {
  return c_supportfiletest::os_get(s, -1);
}
Variant &cw_supportfiletest$os_lval(const char *s) {
  return c_supportfiletest::os_lval(s, -1);
}
Variant cw_supportfiletest$os_constant(const char *s) {
  return c_supportfiletest::os_constant(s);
}
Variant cw_supportfiletest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_supportfiletest::os_invoke(c, s, params, -1, fatal);
}
void c_supportfiletest::init() {
  c_test::init();
}
/* SRC: phc-test/framework/lib/support_file_test.php line 9 */
/* SRC: phc-test/framework/lib/support_file_test.php line 10 */
/* SRC: phc-test/framework/lib/support_file_test.php line 12 */
Variant c_supportfiletest::t_generate_support_files() {
  INSTANCE_METHOD_INJECTION(SupportFileTest, SupportFileTest::generate_support_files);
  Variant v_files;
  Variant v_subject;

  if (!(toBoolean(LINE(14,o_root_invoke_few_args("check_prerequisites", 0x547CFD01CE2F8190LL, 0))))) {
    LINE(16,o_root_invoke_few_args("mark_skipped", 0x67446827CB764B08LL, 1, "All"));
    return false;
  }
  (v_files = LINE(20,o_root_invoke_few_args("get_test_subjects", 0x268048E176687DE2LL, 0)));
  LINE(21,t_ready_progress_bar(x_count(v_files)));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_files.begin("supportfiletest"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_subject = iter3->second();
      {
        if (!(LINE(25,t_check_test_prerequisites(v_subject)))) {
          LINE(27,t_mark_skipped(v_subject, "Test prerequisites failed"));
        }
        else {
          LINE(31,o_root_invoke_few_args("generate_support_file", 0x31C19FCBE91F96FALL, 1, v_subject));
        }
      }
    }
  }
  LINE(35,o_root_invoke_few_args("finish_test", 0x76D94AF9AA95C111LL, 0));
  return null;
} /* function */
/* SRC: phc-test/framework/lib/support_file_test.php line 38 */
void c_supportfiletest::t_write_support_file(CVarRef v_out, Variant v_subject) {
  INSTANCE_METHOD_INJECTION(SupportFileTest, SupportFileTest::write_support_file);
  Variant v_filename;
  String v_dirname;

  (v_filename = LINE(40,o_root_invoke_few_args("get_support_filename", 0x09F602C072269E49LL, 1, v_subject)));
  (v_dirname = LINE(43,x_dirname(toString(v_filename))));
  if (!(LINE(44,x_is_dir(v_dirname)))) {
    (silenceInc(), silenceDec(LINE(46,x_mkdir(v_dirname, 493LL, true))));
  }
  LINE(49,x_file_put_contents(toString(v_filename), v_out));
} /* function */
Object co_supportfiletest(CArrRef params, bool init /* = true */) {
  return Object(p_supportfiletest(NEW(c_supportfiletest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$support_file_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/support_file_test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$support_file_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
