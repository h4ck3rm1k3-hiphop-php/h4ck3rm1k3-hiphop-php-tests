
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/labels.php line 233 */
Variant f_is_labelled(CVarRef v_script, CStrRef v_label) {
  FUNCTION_INJECTION(is_labelled);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_label_struct __attribute__((__unused__)) = g->GV(label_struct);
  LINE(236,f_phc_assert(!same(gv_label_struct, null), "label structure not yet initialized"));
  if (same(LINE(238,x_array_search(v_script, gv_label_struct.rvalAt(v_label))), false)) {
    return false;
  }
  else {
    return true;
  }
  return null;
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 249 */
Variant f_get_all_scripts() {
  FUNCTION_INJECTION(get_all_scripts);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_label_struct __attribute__((__unused__)) = g->GV(label_struct);
  Variant v_all_files;
  Variant v_result;
  Variant v_array;

  LINE(252,f_phc_assert(!same(gv_label_struct, null), "label structure not yet initialized"));
  (v_all_files = LINE(255,x_array_values(gv_label_struct)));
  (v_result = ScalarArrays::sa_[3]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_all_files.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_array = iter3->second();
      {
        (v_result = LINE(261,x_array_merge(2, v_result, Array(ArrayInit(1).set(0, v_array).create()))));
      }
    }
  }
  (v_result = LINE(263,x_array_unique(v_result)));
  return v_result;
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 30 */
Variant f_strip_long_files(Variant v_label_struct) {
  FUNCTION_INJECTION(strip_long_files);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_labels __attribute__((__unused__)) = g->GV(labels);
  Variant v_long_files;
  Variant v_label;

  (v_long_files = v_label_struct.rvalAt("long", 0x196A928F7304A858LL));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = gv_labels.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_label = iter6->second();
      {
        v_label_struct.set(v_label, (LINE(37,x_array_values(x_array_diff(2, v_label_struct.rvalAt(v_label), v_long_files)))));
      }
    }
  }
  return v_label_struct;
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 281 */
Variant f_get_all_plugins() {
  FUNCTION_INJECTION(get_all_plugins);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_base_dir __attribute__((__unused__)) = g->GV(base_dir);
  String v_command;
  Variant v_result;

  (v_command = LINE(284,concat3("find ", toString(gv_base_dir), "/plugins -name \"*.cpp\"")));
  (v_result = LINE(285,(assignCallTemp(eo_1, x_trim(f_shell_exec(v_command))),x_split("\n", eo_1))));
  if (equal(LINE(286,x_count(v_result)), 1LL) && equal(v_result.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "")) return ScalarArrays::sa_[3];
  return v_result;
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 216 */
Variant f_get_scripts_labelled(CStrRef v_label) {
  FUNCTION_INJECTION(get_scripts_labelled);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_label_struct __attribute__((__unused__)) = g->GV(label_struct);
  LINE(219,f_phc_assert(!same(gv_label_struct, null), "label structure not yet initialized"));
  return gv_label_struct.rvalAt(v_label);
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 223 */
Variant f_get_interpretable_scripts() {
  FUNCTION_INJECTION(get_interpretable_scripts);
  return LINE(225,f_get_scripts_labelled("interpretable"));
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 43 */
bool f_skip_3rdparty(CVarRef v_filename) {
  FUNCTION_INJECTION(skip_3rdparty);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_long __attribute__((__unused__)) = g->GV(opt_long);
  return (!(toBoolean(gv_opt_long)) && toBoolean(LINE(46,x_preg_match("#/3rdparty/#", toString(v_filename)))));
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 228 */
Variant f_get_non_interpretable_scripts() {
  FUNCTION_INJECTION(get_non_interpretable_scripts);
  return LINE(230,f_get_scripts_labelled("non-interpretable"));
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 166 */
void f_process_label_file_line(CVarRef v_line, CVarRef v_files, Variant v_labelled_files) {
  FUNCTION_INJECTION(process_label_file_line);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_default_labels __attribute__((__unused__)) = g->GV(default_labels);
  Variant &gv_non_default_labels __attribute__((__unused__)) = g->GV(non_default_labels);
  Variant &gv_opposite_label __attribute__((__unused__)) = g->GV(opposite_label);
  Variant &gv_labels __attribute__((__unused__)) = g->GV(labels);
  Variant &gv_exceptions __attribute__((__unused__)) = g->GV(exceptions);
  Variant v_split;
  Variant v_pattern;
  Variant v_matches;
  Variant v_label;
  Variant v_filename;
  Variant v_exception_matches;
  String v_label_names;

  (v_split = LINE(175,x_preg_split("/\\s+/", v_line)));
  (v_pattern = LINE(177,x_array_shift(ref(v_split))));
  (v_pattern = toString("^test/subjects/") + toString(v_pattern));
  (v_matches = LINE(179,(assignCallTemp(eo_0, concat3("!", toString(v_pattern), "!")),assignCallTemp(eo_1, toArray(v_files)),x_preg_grep(eo_0, eo_1))));
  if (!same(v_pattern, "^test/subjects/3rdparty/.*")) {
    LINE(183,(assignCallTemp(eo_0, x_count(v_matches)),assignCallTemp(eo_1, concat3("pattern !", toString(v_pattern), "! matches no files")),f_phc_assert(eo_0, eo_1)));
  }
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_split.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_label = iter9->second();
      {
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = v_matches.begin(); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_filename = iter12->second();
            {
              if (LINE(191,x_in_array(v_label, gv_default_labels))) {
                lval(v_labelled_files.lvalAt(v_filename)).set(v_label, (1LL));
              }
              else if (LINE(195,x_in_array(v_label, gv_non_default_labels))) {
                lval(v_labelled_files.lvalAt(v_filename)).set(gv_opposite_label.rvalAt(v_label), (0LL));
              }
              else if (toBoolean(LINE(199,x_preg_match("/no-(.*)/", toString(v_label), ref(v_exception_matches))))) {
                lval(gv_exceptions.lvalAt(v_exception_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL))).append((v_filename));
              }
              else if (equal(v_label, "check-interpretable")) {
                lval(v_labelled_files.lvalAt(v_filename)).set("non-interpretable", ("check"), 0x09E2B875F978DCB9LL);
              }
              else {
                (v_label_names = LINE(209,x_join(", ", gv_labels)));
                LINE(210,f_phc_unreachable(concat5("Label file error: ", toString(v_label), " not a valid label (must be in ", v_label_names, ")")));
              }
            }
          }
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 269 */
Variant f_get_all_scripts_in_dir(CVarRef v_directory) {
  FUNCTION_INJECTION(get_all_scripts_in_dir);
  Variant eo_0;
  Variant eo_1;
  String v_command;
  Variant v_result;

  LINE(271,f_phc_assert(!equal(v_directory, ""), "Cant search blank directory"));
  LINE(272,(assignCallTemp(eo_0, x_preg_match("/\\/$/", toString(v_directory))),assignCallTemp(eo_1, concat3("directory '", toString(v_directory), "' must end in a '/'")),f_phc_assert(eo_0, eo_1)));
  (v_command = LINE(274,concat3("find -L ", toString(v_directory), " -name \"*.php\"")));
  (v_result = LINE(275,(assignCallTemp(eo_1, x_trim(f_shell_exec(v_command))),x_split("\n", eo_1))));
  if (equal(LINE(276,x_count(v_result)), 1LL) && equal(v_result.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "")) return ScalarArrays::sa_[3];
  return v_result;
} /* function */
/* SRC: phc-test/framework/lib/labels.php line 49 */
Sequence f_create_label_struct(CVarRef v_directory, CVarRef v_label_filename, CVarRef v_third_party_filename) {
  FUNCTION_INJECTION(create_label_struct);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_default_labels __attribute__((__unused__)) = g->GV(default_labels);
  Variant v_default_labels;
  Variant &gv_non_default_labels __attribute__((__unused__)) = g->GV(non_default_labels);
  Variant v_non_default_labels;
  Variant &gv_opposite_label __attribute__((__unused__)) = g->GV(opposite_label);
  Variant v_opposite_label;
  Variant &gv_labels __attribute__((__unused__)) = g->GV(labels);
  Variant v_labels;
  Variant &gv_exceptions __attribute__((__unused__)) = g->GV(exceptions);
  Variant v_exceptions;
  Variant &gv_opt_one __attribute__((__unused__)) = g->GV(opt_one);
  Variant v_opt_one;
  Variant v_files;
  Variant v_default;
  Variant v_filename;
  Variant v_labelled_files;
  Variant v_lines;
  Variant v_third_party_lines;
  Variant v_line;
  Primitive v_key = 0;
  Variant v_value;
  Variant v_label;
  Sequence v_label_struct;
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant v_phc;
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  Variant v_plugin_dir;

  v_default_labels = ref(g->GV(default_labels));
  v_non_default_labels = ref(g->GV(non_default_labels));
  v_opposite_label = ref(g->GV(opposite_label));
  v_labels = ref(g->GV(labels));
  v_exceptions = ref(g->GV(exceptions));
  v_opt_one = ref(g->GV(opt_one));
  (v_files = LINE(58,f_get_all_scripts_in_dir(v_directory)));
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_default_labels.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_default = iter15->second();
      {
        {
          LOOP_COUNTER(16);
          for (ArrayIterPtr iter18 = v_files.begin(); !iter18->end(); iter18->next()) {
            LOOP_COUNTER_CHECK(16);
            v_filename = iter18->second();
            {
              lval(v_labelled_files.lvalAt(v_filename)).set(v_default, (1LL));
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(19);
    for (ArrayIterPtr iter21 = v_files.begin(); !iter21->end(); iter21->next()) {
      LOOP_COUNTER_CHECK(19);
      v_filename = iter21->second();
      {
        lval(v_labelled_files.lvalAt(v_filename)).set("non-interpretable", ("check"), 0x09E2B875F978DCB9LL);
      }
    }
  }
  (v_lines = LINE(73,x_file(toString(v_label_filename))));
  if (LINE(74,x_file_exists(toString(v_third_party_filename)))) (v_third_party_lines = LINE(75,x_file(toString(v_third_party_filename))));
  else (v_third_party_lines = ScalarArrays::sa_[3]);
  {
    LOOP_COUNTER(22);
    for (ArrayIterPtr iter24 = v_lines.begin(); !iter24->end(); iter24->next()) {
      LOOP_COUNTER_CHECK(22);
      v_line = iter24->second();
      {
        (v_line = LINE(81,x_preg_replace("/#.*$/", "", v_line)));
        (v_line = LINE(82,x_trim(toString(v_line))));
        if (equal(v_line, "")) continue;
        LINE(84,f_process_label_file_line(v_line, v_files, ref(v_labelled_files)));
      }
    }
  }
  {
    LOOP_COUNTER(25);
    for (ArrayIterPtr iter27 = v_third_party_lines.begin(); !iter27->end(); iter27->next()) {
      LOOP_COUNTER_CHECK(25);
      v_line = iter27->second();
      {
        (v_line = LINE(89,x_preg_replace("/#.*$/", "", v_line)));
        (v_line = LINE(90,x_trim(toString(v_line))));
        if (equal(v_line, "")) continue;
        LINE(92,f_process_label_file_line(concat("3rdparty/", toString(v_line)), v_files, ref(v_labelled_files)));
      }
    }
  }
  if (toBoolean(v_opt_one)) {
    {
      LOOP_COUNTER(28);
      for (ArrayIterPtr iter30 = v_labelled_files.begin(); !iter30->end(); iter30->next()) {
        LOOP_COUNTER_CHECK(28);
        v_value = iter30->second();
        v_key = iter30->first();
        {
          if (!same(v_key, v_opt_one)) {
            v_labelled_files.weakRemove(v_key);
            (v_files = Array(ArrayInit(1).set(0, v_opt_one).create()));
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(31);
    for (ArrayIterPtr iter33 = v_labels.begin(); !iter33->end(); iter33->next()) {
      LOOP_COUNTER_CHECK(31);
      v_label = iter33->second();
      {
        v_label_struct.set(v_label, (ScalarArrays::sa_[3]));
      }
    }
  }
  {
    LOOP_COUNTER(34);
    for (ArrayIterPtr iter36 = v_files.begin(); !iter36->end(); iter36->next()) {
      LOOP_COUNTER_CHECK(34);
      v_filename = iter36->second();
      {
        if (LINE(118,f_skip_3rdparty(v_filename))) {
          lval(v_labelled_files.lvalAt(v_filename)).set("non-interpretable", (0LL), 0x09E2B875F978DCB9LL);
        }
        else {
          LINE(124,f_phc_assert(isset(v_labelled_files, v_filename), "file not found"));
          if (same(v_labelled_files.rvalAt(v_filename).rvalAt("non-interpretable", 0x09E2B875F978DCB9LL), "check")) {
            LINE(129,(assignCallTemp(eo_0, f_check_for_plugin("tools/purity_test")),f_phc_assert(eo_0, "purity not available")));
            {
              v_phc = ref(g->GV(phc));
              v_plugin_dir = ref(g->GV(plugin_dir));
            }
            if (equal(f_shell_exec(toString(v_phc) + toString(" --run ") + toString(v_plugin_dir) + toString("/tools/purity_test.la ") + toString(v_filename) + toString(" 2>&1")), "")) {
              LINE(133,f_log_status("pure", "", v_filename, ""));
              lval(v_labelled_files.lvalAt(v_filename)).set("non-interpretable", (0LL), 0x09E2B875F978DCB9LL);
            }
            else {
              LINE(138,f_log_status("impure", "", v_filename, ""));
              lval(v_labelled_files.lvalAt(v_filename)).set("non-interpretable", (1LL), 0x09E2B875F978DCB9LL);
            }
          }
        }
        {
          LOOP_COUNTER(37);
          for (ArrayIterPtr iter39 = v_default_labels.begin(); !iter39->end(); iter39->next()) {
            LOOP_COUNTER_CHECK(37);
            v_label = iter39->second();
            {
              if (toBoolean(v_labelled_files.rvalAt(v_filename).rvalAt(v_label))) {
                LINE(148,x_array_push(2, ref(lval(v_label_struct.lvalAt(v_label))), v_filename));
              }
              else {
                LINE(152,x_array_push(2, ref(lval(v_label_struct.lvalAt(v_opposite_label.rvalAt(v_label)))), v_filename));
              }
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(40);
    for (ArrayIterPtr iter42 = v_labels.begin(); !iter42->end(); iter42->next()) {
      LOOP_COUNTER_CHECK(40);
      v_label = iter42->second();
      {
        LINE(160,x_sort(ref(lval(v_label_struct.lvalAt(v_label)))));
      }
    }
  }
  return v_label_struct;
} /* function */
Variant pm_php$phc_test$framework$lib$labels_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/labels.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$labels_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_subject_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("subject_dir") : g->GV(subject_dir);
  Variant &v_label_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("label_file") : g->GV(label_file);
  Variant &v_third_party_label_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("third_party_label_file") : g->GV(third_party_label_file);
  Variant &v_default_labels __attribute__((__unused__)) = (variables != gVariables) ? variables->get("default_labels") : g->GV(default_labels);
  Variant &v_non_default_labels __attribute__((__unused__)) = (variables != gVariables) ? variables->get("non_default_labels") : g->GV(non_default_labels);
  Variant &v_labels __attribute__((__unused__)) = (variables != gVariables) ? variables->get("labels") : g->GV(labels);
  Variant &v_opposite_label __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opposite_label") : g->GV(opposite_label);
  Variant &v_exceptions __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exceptions") : g->GV(exceptions);
  Variant &v_label_struct __attribute__((__unused__)) = (variables != gVariables) ? variables->get("label_struct") : g->GV(label_struct);
  Variant &v_opt_long __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_long") : g->GV(opt_long);

  (v_subject_dir = "test/subjects/");
  (v_label_file = "test/subjects/labels");
  (v_third_party_label_file = "test/subjects/3rdparty/labels");
  (v_default_labels = ScalarArrays::sa_[0]);
  (v_non_default_labels = ScalarArrays::sa_[1]);
  (v_labels = LINE(15,x_array_merge(2, v_default_labels, Array(ArrayInit(1).set(0, v_non_default_labels).create()))));
  (v_opposite_label = ScalarArrays::sa_[2]);
  (v_exceptions = ScalarArrays::sa_[3]);
  (v_label_struct = LINE(23,f_create_label_struct(v_subject_dir, v_label_file, v_third_party_label_file)));
  if (!(toBoolean(v_opt_long))) {
    (v_label_struct = LINE(27,f_strip_long_files(v_label_struct)));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
