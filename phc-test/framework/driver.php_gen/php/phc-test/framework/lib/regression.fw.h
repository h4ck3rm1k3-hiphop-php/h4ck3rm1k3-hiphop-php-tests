
#ifndef __GENERATED_php_phc_test_framework_lib_regression_fw_h__
#define __GENERATED_php_phc_test_framework_lib_regression_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(regressiontest)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/phc-test/framework/lib/support_file_test.fw.h>

#endif // __GENERATED_php_phc_test_framework_lib_regression_fw_h__
