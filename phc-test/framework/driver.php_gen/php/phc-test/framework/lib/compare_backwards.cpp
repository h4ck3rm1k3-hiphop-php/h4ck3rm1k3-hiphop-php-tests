
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/compare_backwards.h>
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_6(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/lib/compare_backwards.php line 28 */
Variant c_comparebackwards::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_comparebackwards::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_comparebackwards::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_comparebackwards::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_comparebackwards::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_comparebackwards::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_comparebackwards::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_comparebackwards::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(comparebackwards)
ObjectData *c_comparebackwards::create(Variant v_last_pass, Variant v_dump //  = "dump"
, Variant v_dependency //  = "BasicParseTest"
) {
  init();
  t___construct(v_last_pass, v_dump, v_dependency);
  return this;
}
ObjectData *c_comparebackwards::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_comparebackwards::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_comparebackwards::cloneImpl() {
  c_comparebackwards *obj = NEW(c_comparebackwards)();
  cloneSet(obj);
  return obj;
}
void c_comparebackwards::cloneSet(c_comparebackwards *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_comparebackwards::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 21:
      HASH_GUARD(0x6D0F7DC0D691E815LL, check_output) {
        return (t_check_output(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(params.rvalAt(0)), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(params.rvalAt(0)), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(params.rvalAt(0)), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_comparebackwards::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      HASH_GUARD(0x2B7D56B0552C4507LL, get_phc_num_procs_divisor) {
        return (t_get_phc_num_procs_divisor());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 21:
      HASH_GUARD(0x6D0F7DC0D691E815LL, check_output) {
        return (t_check_output(a0, a1, a2, a3));
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 23:
      HASH_GUARD(0x6288F1F6D5D1E217LL, two_command_finish) {
        return (t_two_command_finish(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        return (t___construct(a0, a1, a2), null);
      }
      break;
    case 34:
      HASH_GUARD(0x268048E176687DE2LL, get_test_subjects) {
        return (t_get_test_subjects());
      }
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 36:
      HASH_GUARD(0x6BEDB7FD897AA464LL, async_success) {
        return (t_async_success(a0), null);
      }
      break;
    case 37:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0, a1));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 43:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 44:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(a0), a1));
      }
      break;
    case 46:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 49:
      HASH_GUARD(0x7BB317B3EC138471LL, run_test) {
        return (t_run_test(a0), null);
      }
      break;
    case 51:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(a0), null);
      }
      break;
    case 60:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_comparebackwards::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_comparebackwards$os_get(const char *s) {
  return c_comparebackwards::os_get(s, -1);
}
Variant &cw_comparebackwards$os_lval(const char *s) {
  return c_comparebackwards::os_lval(s, -1);
}
Variant cw_comparebackwards$os_constant(const char *s) {
  return c_comparebackwards::os_constant(s);
}
Variant cw_comparebackwards$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_comparebackwards::os_invoke(c, s, params, -1, fatal);
}
void c_comparebackwards::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/lib/compare_backwards.php line 30 */
void c_comparebackwards::t___construct(Variant v_last_pass, Variant v_dump //  = "dump"
, Variant v_dependency //  = "BasicParseTest"
) {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("last_pass", 0x0FA84CB5FD336BBDLL) = v_last_pass);
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = toString("cb_") + toString(v_last_pass));
  (o_lval("dump", 0x38B896FAF2928192LL) = v_dump);
  (o_lval("dependencies", 0x36DCDDF5BB1EBEBDLL) = Array(ArrayInit(1).set(0, v_dependency).create()));
  LINE(39,c_asynctest::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 43 */
Variant c_comparebackwards::t_get_name() {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::get_name);
  return o_get("name", 0x0BCDB293DC3CBDDCLL);
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 48 */
Variant c_comparebackwards::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::get_test_subjects);
  return LINE(50,f_get_interpretable_scripts());
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 53 */
void c_comparebackwards::t_run_test(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::run_test);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  p_asyncbundle v_bundle;
  Variant v_commands;
  Variant v_pass;
  Variant v_command;

  ((Object)((v_bundle = ((Object)(LINE(56,p_asyncbundle(p_asyncbundle(NEWOBJ(c_asyncbundle)())->create(((Object)(this)), v_subject))))))));
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (LINE(58,f_get_php_command_line(v_subject))), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(0LL, ("homogenize_output"), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("callbacks", 0x1F0B65B99DB7A31ELL)).set(0LL, (null), 0x77CFA1EEF01BCA90LL);
  (v_commands = ScalarArrays::sa_[3]);
  {
    LOOP_COUNTER(1);
    Variant map2 = LINE(63,f_get_pass_list());
    for (ArrayIterPtr iter3 = map2.begin("comparebackwards"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_pass = iter3->second();
      {
        LINE(65,(assignCallTemp(eo_0, ref(v_commands)),assignCallTemp(eo_1, t_get_command_line(v_subject, o_get("dump", 0x38B896FAF2928192LL), v_pass)),x_array_unshift(2, eo_0, eo_1)));
        if (equal(o_get("last_pass", 0x0FA84CB5FD336BBDLL), v_pass)) break;
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_commands.begin("comparebackwards"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_command = iter6->second();
      {
        lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).append((v_command));
        lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).append(("homogenize_output"));
        lval(v_bundle.o_lval("callbacks", 0x1F0B65B99DB7A31ELL)).append(("check_output"));
      }
    }
  }
  (v_bundle.o_lval("final", 0x5192930B2145036ELL) = "finish");
  LINE(82,v_bundle->t_start());
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 86 */
bool c_comparebackwards::t_check_output(CVarRef v_out, CVarRef v_err, CVarRef v_exit, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::check_output);
  if (same(toObject(v_bundle).o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_out) && same(toObject(v_bundle).o_get("errs", 0x15E266045DABA002LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_err) && same(toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_exit)) {
    if (equal(toObject(v_bundle).o_get("state", 0x5C84C80672BA5E22LL), 1LL)) {
      LINE(96,t_async_success(v_bundle));
    }
    else {
      LINE(101,t_async_failure("Latest pass fails", v_bundle));
    }
    return false;
  }
  return true;
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 109 */
String c_comparebackwards::t_get_command_line(CVarRef v_subject, CVarRef v_dump, CVarRef v_pass_name) {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::get_command_line);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  String v_pipe_command;

  (v_pipe_command = LINE(112,f_get_php_command_line(v_subject, "pipe")));
  return concat_rev(LINE(113,concat6("=", toString(v_pass_name), " ", toString(v_subject), " | ", v_pipe_command)), concat3(toString(gv_phc), " --no-hash-bang --", toString(v_dump)));
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 116 */
void c_comparebackwards::t_finish(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::finish);
  LINE(118,t_async_failure("Every pass fails", v_bundle));
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 121 */
Variant c_comparebackwards::t_homogenize_output(CVarRef v_output, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(CompareBackwards, CompareBackwards::homogenize_output);
  return LINE(123,f_homogenize_all(v_output, toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL)));
} /* function */
/* SRC: phc-test/framework/lib/compare_backwards.php line 9 */
Variant f_get_pass_list() {
  FUNCTION_INJECTION(get_pass_list);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &sv_cache __attribute__((__unused__)) = g->sv_get_pass_list_DupIdcache;
  bool &inited_sv_cache __attribute__((__unused__)) = g->inited_sv_get_pass_list_DupIdcache;
  Variant v_out;
  Variant v_err;
  Variant v_exit;
  Variant v_matches;

  if (!inited_sv_cache) {
    (sv_cache = false);
    inited_sv_cache = true;
  }
  if (same(sv_cache, false)) {
    df_lambda_6(LINE(16,f_complete_exec(toString(gv_phc) + toString(" --list-passes"))), v_out, v_err, v_exit);
    LINE(17,x_assert(((toBoolean(v_out)) && (same(v_err, ""))) && (same(v_exit, 0LL))));
    LINE(19,x_preg_match_all("/([a-zA-Z-_0-9]+)\\s+\\((enabled|disabled)\\s+- (AST|HIR|MIR|LIR)\\)\\s+.+/", toString(v_out), ref(v_matches)));
    (sv_cache = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  }
  return sv_cache;
} /* function */
Object co_comparebackwards(CArrRef params, bool init /* = true */) {
  return Object(p_comparebackwards(NEW(c_comparebackwards)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$compare_backwards_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/compare_backwards.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$compare_backwards_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
