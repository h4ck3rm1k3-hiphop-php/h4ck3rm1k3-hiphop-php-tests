
#include <php/phc-test/framework/lib/header.h>
#include <php/phc-test/framework/lib/labels.h>
#include <php/phc-test/framework/lib/test.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/test.php line 10 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_test::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_test::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 11:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 11:
      HASH_GUARD(0x065FDD3E5650A82BLL, print_numbered) {
        return (t_print_numbered(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 25:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
}
/* SRC: phc-test/framework/lib/test.php line 12 */
void c_test::t___construct() {
  INSTANCE_METHOD_INJECTION(Test, Test::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("successes", 0x3A59CDC7E0FC5178LL) = 0LL);
  (o_lval("failures", 0x508D5F5EBCCE2CEELL) = 0LL);
  (o_lval("skipped", 0x059F764B0FA56EC5LL) = 0LL);
  (o_lval("timeouts", 0x788BB3AF9C8DCEB1LL) = 0LL);
  (o_lval("max_time", 0x44D38A447EB55041LL) = 20LL);
  (o_lval("total", 0x563CDBA353C9C786LL) = 0LL);
  (o_lval("solo_tests", 0x1B0BE48B8213C382LL) = 0LL);
  (o_lval("missing_dependencies", 0x02E0D1CFD1BC951BLL) = ScalarArrays::sa_[3]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/test.php line 24 */
Variant c_test::t_get_name() {
  INSTANCE_METHOD_INJECTION(Test, Test::get_name);
  return LINE(26,x_get_class(((Object)(this))));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 29 */
void c_test::t_ready_progress_bar(int v_num_files) {
  INSTANCE_METHOD_INJECTION(Test, Test::ready_progress_bar);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_no_progress_bar __attribute__((__unused__)) = g->GV(opt_no_progress_bar);
  (o_lval("num_files", 0x40233513C43DFC4BLL) = v_num_files);
  if (!(toBoolean(gv_opt_no_progress_bar))) {
    (o_lval("progress_bar", 0x665070E8E2BD10D3LL) = LINE(38,(assignCallTemp(eo_0, concat(toString(LINE(37,o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0))), "%bar% Testing (%fraction%)")),assignCallTemp(eo_4, v_num_files),create_object("console_progressbar", Array(ArrayInit(5).set(0, eo_0).set(1, "#").set(2, "-").set(3, 80LL).set(4, eo_4).create())))));
  }
  invoke_too_many_args("update_count", (1), ((LINE(40,t_update_count()), null)));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 43 */
Variant c_test::t_homogenize_output(CVarRef v_string) {
  INSTANCE_METHOD_INJECTION(Test, Test::homogenize_output);
  return v_string;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 48 */
bool c_test::t_check_global_prerequisites() {
  INSTANCE_METHOD_INJECTION(Test, Test::check_global_prerequisites);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_php __attribute__((__unused__)) = g->GV(php);
  return LINE(51,f_check_for_program(gv_php));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 54 */
bool c_test::t_check_prerequisites() {
  INSTANCE_METHOD_INJECTION(Test, Test::check_prerequisites);
  return true;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 59 */
bool c_test::t_check_test_prerequisites(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(Test, Test::check_test_prerequisites);
  return true;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 64 */
Array c_test::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(Test, Test::get_dependent_test_names);
  return ScalarArrays::sa_[3];
} /* function */
/* SRC: phc-test/framework/lib/test.php line 70 */
Array c_test::t_get_builtin_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(Test, Test::get_builtin_dependent_test_names);
  if (!(t___isset("dependencies"))) return ScalarArrays::sa_[3];
  return toArray((o_get("dependencies", 0x36DCDDF5BB1EBEBDLL)));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 78 */
void c_test::t_calculate_all_dependencies() {
  INSTANCE_METHOD_INJECTION(Test, Test::calculate_all_dependencies);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_tests __attribute__((__unused__)) = g->GV(tests);
  Variant v_dependencies;
  Array v_all_dependencies;
  Variant v_dependency;
  Variant v_test;
  Variant v_new_dependency;

  (v_dependencies = plus_rev(LINE(82,o_root_invoke_few_args("get_dependent_test_names", 0x7D9172C389FDB2C7LL, 0)), (Variant)(LINE(81,t_get_builtin_dependent_test_names()))));
  (v_all_dependencies = ScalarArrays::sa_[3]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_dependencies.begin("test"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_dependency = iter3->second();
      {
        v_all_dependencies.append((v_dependency));
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = gv_tests.begin("test"); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_test = iter6->second();
            {
              if (equal(LINE(91,v_test.o_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)), v_dependency)) {
                {
                  LOOP_COUNTER(7);
                  Variant map8 = LINE(93,v_test.o_invoke_few_args("get_all_dependencies", 0x726772D0F7A3A2E5LL, 0));
                  for (ArrayIterPtr iter9 = map8.begin("test"); !iter9->end(); iter9->next()) {
                    LOOP_COUNTER_CHECK(7);
                    v_new_dependency = iter9->second();
                    v_all_dependencies.append((v_new_dependency));
                  }
                }
                break;
              }
            }
          }
        }
      }
    }
  }
  (o_lval("all_dependencies", 0x51EF2B760BC69FA1LL) = v_all_dependencies);
} /* function */
/* SRC: phc-test/framework/lib/test.php line 102 */
Variant c_test::t_get_all_dependencies() {
  INSTANCE_METHOD_INJECTION(Test, Test::get_all_dependencies);
  if (t___isset("all_dependencies")) return o_get("all_dependencies", 0x51EF2B760BC69FA1LL);
  LINE(107,t_calculate_all_dependencies());
  return o_get("all_dependencies", 0x51EF2B760BC69FA1LL);
} /* function */
/* SRC: phc-test/framework/lib/test.php line 111 */
bool c_test::t_passed_test_dependencies(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(Test, Test::passed_test_dependencies);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_shallow_dependencies;
  Array v_dependencies;
  Variant &gv_tests __attribute__((__unused__)) = g->GV(tests);
  Variant v_dependency;
  Variant v_test;
  Variant v_new_dependency;
  Variant v_depend;

  (v_shallow_dependencies = LINE(113,t_get_all_dependencies()));
  (v_dependencies = ScalarArrays::sa_[3]);
  LOOP_COUNTER(10);
  {
    while (toBoolean(v_shallow_dependencies)) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_dependency = LINE(120,x_array_pop(ref(v_shallow_dependencies))));
        if (LINE(121,x_in_array(v_dependency, v_dependencies))) continue;
        v_dependencies.append((v_dependency));
        {
          LOOP_COUNTER(11);
          for (ArrayIterPtr iter13 = gv_tests.begin("test"); !iter13->end(); iter13->next()) {
            LOOP_COUNTER_CHECK(11);
            v_test = iter13->second();
            {
              if (equal(LINE(127,v_test.o_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)), v_dependency)) {
                {
                  LOOP_COUNTER(14);
                  Variant map15 = LINE(129,v_test.o_invoke_few_args("get_all_dependencies", 0x726772D0F7A3A2E5LL, 0));
                  for (ArrayIterPtr iter16 = map15.begin("test"); !iter16->end(); iter16->next()) {
                    LOOP_COUNTER_CHECK(14);
                    v_new_dependency = iter16->second();
                    v_shallow_dependencies.append((v_new_dependency));
                  }
                }
                break;
              }
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(17);
    for (ArrayIter iter19 = v_dependencies.begin("test"); !iter19.end(); ++iter19) {
      LOOP_COUNTER_CHECK(17);
      v_dependency = iter19.second();
      {
        (v_depend = LINE(138,f_read_dependency(v_dependency, v_subject)));
        if (equal(v_depend, "Fail")) return false;
        else if (equal(v_depend, "missing")) lval(o_lval("missing_dependencies", 0x02E0D1CFD1BC951BLL)).set(v_subject, (v_dependency));
      }
    }
  }
  return true;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 147 */
/* SRC: phc-test/framework/lib/test.php line 148 */
/* SRC: phc-test/framework/lib/test.php line 150 */
void c_test::t_print_numbered() {
  INSTANCE_METHOD_INJECTION(Test, Test::print_numbered);
  Variant v_files;
  int64 v_i = 0;
  Variant v_file;

  (v_files = LINE(152,o_root_invoke_few_args("get_test_subjects", 0x268048E176687DE2LL, 0)));
  (v_i = 1LL);
  {
    LOOP_COUNTER(20);
    for (ArrayIterPtr iter22 = v_files.begin("test"); !iter22->end(); iter22->next()) {
      LOOP_COUNTER_CHECK(20);
      v_file = iter22->second();
      {
        print(LINE(156,concat4(toString(v_i), ". ", toString(v_file), "\n")));
        v_i++;
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/test.php line 161 */
Variant c_test::t_run() {
  INSTANCE_METHOD_INJECTION(Test, Test::run);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_files;
  Variant &gv_opt_quick __attribute__((__unused__)) = g->GV(opt_quick);
  int64 v_count = 0;
  Variant v_subject;

  (v_files = LINE(164,o_root_invoke_few_args("get_test_subjects", 0x268048E176687DE2LL, 0)));
  LINE(165,t_ready_progress_bar(x_count(v_files)));
  if (!(toBoolean(LINE(168,o_root_invoke_few_args("check_prerequisites", 0x547CFD01CE2F8190LL, 0)))) || !(t_check_global_prerequisites())) {
    LINE(170,t_mark_skipped("All", "Test prerequisties failed"));
    LINE(171,o_root_invoke_few_args("finish_test", 0x76D94AF9AA95C111LL, 0));
    return false;
  }
  (v_count = 0LL);
  {
    LOOP_COUNTER(23);
    for (ArrayIterPtr iter25 = v_files.begin("test"); !iter25->end(); iter25->next()) {
      LOOP_COUNTER_CHECK(23);
      v_subject = iter25->second();
      {
        if (!(LINE(181,t_check_test_prerequisites(v_subject)))) {
          LINE(183,t_mark_skipped(v_subject, "Individual prerequsite failed"));
        }
        else if (!(LINE(185,t_passed_test_dependencies(v_subject)))) {
          LINE(187,t_mark_skipped(v_subject, "Test failed dependency"));
        }
        else if (LINE(189,t_check_exception(v_subject))) {
          LINE(191,t_mark_skipped(v_subject, "Test excepted"));
        }
        else {
          LINE(195,o_root_invoke_few_args("run_test", 0x7BB317B3EC138471LL, 1, v_subject));
        }
        v_count++;
        if (toBoolean(gv_opt_quick) && not_less(v_count, 10LL)) break;
      }
    }
  }
  LINE(203,o_root_invoke_few_args("finish_test", 0x76D94AF9AA95C111LL, 0));
  return null;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 206 */
void c_test::t_mark_success(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(Test, Test::mark_success);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  LINE(208,(assignCallTemp(eo_0, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_1, v_subject),f_write_dependencies(eo_0, eo_1, true)));
  lval(o_lval("successes", 0x3A59CDC7E0FC5178LL))++;
  lval(o_lval("total", 0x563CDBA353C9C786LL))++;
  LINE(212,t_update_count());
  LINE(213,(assignCallTemp(eo_1, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_2, v_subject),f_log_status("success", eo_1, eo_2, "")));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 216 */
void c_test::t_mark_timeout(CVarRef v_subject, CVarRef v_commands, CVarRef v_outs, CVarRef v_errs, CVarRef v_exits) {
  INSTANCE_METHOD_INJECTION(Test, Test::mark_timeout);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  lval(o_lval("timeouts", 0x788BB3AF9C8DCEB1LL))++;
  lval(o_lval("total", 0x563CDBA353C9C786LL))++;
  LINE(220,t_update_count());
  LINE(221,(assignCallTemp(eo_1, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_2, v_subject),f_log_status("timeout", eo_1, eo_2, "")));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 224 */
void c_test::t_mark_skipped(CVarRef v_subject, CStrRef v_reason) {
  INSTANCE_METHOD_INJECTION(Test, Test::mark_skipped);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  if (equal(v_subject, "All")) {
    o_lval("skipped", 0x059F764B0FA56EC5LL) += LINE(228,x_count(o_root_invoke_few_args("get_test_subjects", 0x268048E176687DE2LL, 0)));
    (o_lval("total", 0x563CDBA353C9C786LL) = o_get("skipped", 0x059F764B0FA56EC5LL));
  }
  else {
    lval(o_lval("skipped", 0x059F764B0FA56EC5LL))++;
    lval(o_lval("total", 0x563CDBA353C9C786LL))++;
  }
  LINE(236,(assignCallTemp(eo_0, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_1, v_subject),f_write_dependencies(eo_0, eo_1, false)));
  LINE(237,(assignCallTemp(eo_1, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_2, v_subject),assignCallTemp(eo_3, v_reason),f_log_status("skipped", eo_1, eo_2, eo_3)));
  LINE(238,t_update_count());
} /* function */
/* SRC: phc-test/framework/lib/test.php line 241 */
void c_test::t_mark_failure(CVarRef v_subject, CVarRef v_commands, CVarRef v_outs //  = "Not relevent"
, CVarRef v_errs //  = "Not relevent"
, CVarRef v_exits //  = "Not relevent"
, CVarRef v_reason //  = "TODO - no reason given"
) {
  INSTANCE_METHOD_INJECTION(Test, Test::mark_failure);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  LINE(243,(assignCallTemp(eo_0, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_1, v_subject),f_write_dependencies(eo_0, eo_1, false)));
  LINE(245,(assignCallTemp(eo_0, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_1, v_subject),assignCallTemp(eo_2, v_commands),assignCallTemp(eo_3, v_outs),assignCallTemp(eo_4, v_errs),assignCallTemp(eo_5, v_exits),assignCallTemp(eo_6, isset(o_get("missing_dependencies", 0x02E0D1CFD1BC951BLL), v_subject) ? ((Variant)(o_get("missing_dependencies", 0x02E0D1CFD1BC951BLL).rvalAt(v_subject))) : ((Variant)(null))),assignCallTemp(eo_7, v_reason),f_log_failure(eo_0, eo_1, eo_2, eo_3, eo_4, eo_5, eo_6, eo_7)));
  LINE(246,(assignCallTemp(eo_1, o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)),assignCallTemp(eo_2, v_subject),assignCallTemp(eo_3, v_reason),f_log_status("failure", eo_1, eo_2, eo_3)));
  LINE(247,t_erase_progress_bar());
  LINE(249,t_display_progress_bar());
  lval(o_lval("failures", 0x508D5F5EBCCE2CEELL))++;
  lval(o_lval("total", 0x563CDBA353C9C786LL))++;
  LINE(253,t_update_count());
} /* function */
/* SRC: phc-test/framework/lib/test.php line 256 */
bool c_test::t_is_successful() {
  INSTANCE_METHOD_INJECTION(Test, Test::is_successful);
  return (equal(o_get("failures", 0x508D5F5EBCCE2CEELL), 0LL));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 261 */
bool c_test::t_is_completely_skipped() {
  INSTANCE_METHOD_INJECTION(Test, Test::is_completely_skipped);
  return ((((equal(o_get("failures", 0x508D5F5EBCCE2CEELL), 0LL)) && (equal(o_get("successes", 0x3A59CDC7E0FC5178LL), 0LL))) && (equal(o_get("timeouts", 0x788BB3AF9C8DCEB1LL), 0LL))) && (not_less(o_get("skipped", 0x059F764B0FA56EC5LL), 0LL)));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 266 */
Variant c_test::t_get_appropriate_colour() {
  INSTANCE_METHOD_INJECTION(Test, Test::get_appropriate_colour);
  if (LINE(268,t_is_completely_skipped())) return LINE(269,f_blue_string());
  else if (LINE(270,t_is_successful())) return LINE(271,f_green_string());
  else return LINE(273,f_red_string());
  return null;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 276 */
Variant c_test::t_get_appropriate_phrase() {
  INSTANCE_METHOD_INJECTION(Test, Test::get_appropriate_phrase);
  if (LINE(278,t_is_completely_skipped())) return "Skipped";
  else if (LINE(280,t_is_successful())) return "Success";
  else return "Failure";
  return null;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 286 */
Variant c_test::t_get_triple_string(bool v_finished //  = false
) {
  INSTANCE_METHOD_INJECTION(Test, Test::get_triple_string);
  String v_reset;
  String v_passed;
  String v_failed;
  String v_timedout;
  String v_skipped;
  Variant v_colour;
  String v_blue;
  String v_green;
  String v_red;

  (v_reset = LINE(288,f_reset_string()));
  (v_passed = LINE(289,x_sprintf(2, "%5s", Array(ArrayInit(1).set(0, concat(toString(o_get("successes", 0x3A59CDC7E0FC5178LL)), " P")).create()))));
  (v_failed = LINE(290,x_sprintf(2, "%5s", Array(ArrayInit(1).set(0, concat(toString(o_get("failures", 0x508D5F5EBCCE2CEELL)), " F")).create()))));
  (v_timedout = LINE(291,x_sprintf(2, "%5s", Array(ArrayInit(1).set(0, concat(toString(o_get("timeouts", 0x788BB3AF9C8DCEB1LL)), " T")).create()))));
  (v_skipped = LINE(292,x_sprintf(2, "%5s", Array(ArrayInit(1).set(0, concat(toString(o_get("skipped", 0x059F764B0FA56EC5LL)), " S")).create()))));
  if (v_finished) {
    (v_colour = LINE(295,t_get_appropriate_colour()));
    return concat_rev(LINE(296,concat6(v_failed, ", ", v_timedout, ", ", v_skipped, v_reset)), concat3(toString(v_colour), v_passed, ", "));
  }
  else {
    (v_blue = LINE(300,f_blue_string()));
    (v_green = LINE(301,f_green_string()));
    (v_red = LINE(302,f_red_string()));
    (v_reset = LINE(303,f_reset_string()));
    return concat_rev(LINE(304,concat6(v_timedout, v_reset, ", ", v_blue, v_skipped, v_reset)), concat_rev(concat6(", ", v_red, v_failed, v_reset, ", ", v_blue), concat3(v_green, v_passed, v_reset)));
  }
  return null;
} /* function */
/* SRC: phc-test/framework/lib/test.php line 308 */
void c_test::t_update_count() {
  INSTANCE_METHOD_INJECTION(Test, Test::update_count);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_no_progress_bar __attribute__((__unused__)) = g->GV(opt_no_progress_bar);
  Variant v_target_num;

  if (!(toBoolean(gv_opt_no_progress_bar))) {
    (v_target_num = o_get("num_files", 0x40233513C43DFC4BLL));
    if (equal(v_target_num, 0LL)) (v_target_num = 1LL);
    LINE(317,t_erase_progress_bar());
    (assignCallTemp(eo_0, toObject(o_get("progress_bar", 0x665070E8E2BD10D3LL))),(assignCallTemp(eo_1, concat_rev(toString(LINE(319,t_get_triple_string())), concat(toString(o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)), " %bar% %fraction%: "))),assignCallTemp(eo_5, ref(v_target_num)),LINE(320,eo_0.o_invoke("reset", Array(ArrayInit(5).set(0, eo_1).set(1, "#").set(2, "-").set(3, 112LL).set(4, eo_5).create()), 0x340A51AE22A924E0LL))));
    LINE(321,o_get("progress_bar", 0x665070E8E2BD10D3LL).o_invoke_few_args("update", 0x1402C6C4A8D472A0LL, 1, o_lval("total", 0x563CDBA353C9C786LL)));
  }
} /* function */
/* SRC: phc-test/framework/lib/test.php line 325 */
void c_test::t_erase_progress_bar() {
  INSTANCE_METHOD_INJECTION(Test, Test::erase_progress_bar);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_no_progress_bar __attribute__((__unused__)) = g->GV(opt_no_progress_bar);
  if (!(toBoolean(gv_opt_no_progress_bar))) {
    LINE(330,o_get("progress_bar", 0x665070E8E2BD10D3LL).o_invoke_few_args("erase", 0x00DFE2FA5EA20DEDLL, 0));
  }
} /* function */
/* SRC: phc-test/framework/lib/test.php line 334 */
void c_test::t_display_progress_bar() {
  INSTANCE_METHOD_INJECTION(Test, Test::display_progress_bar);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_no_progress_bar __attribute__((__unused__)) = g->GV(opt_no_progress_bar);
  if (!(toBoolean(gv_opt_no_progress_bar))) {
    LINE(339,o_get("progress_bar", 0x665070E8E2BD10D3LL).o_invoke_few_args("display", 0x797A6FC6339ADA9ELL, 1, o_lval("total", 0x563CDBA353C9C786LL)));
  }
} /* function */
/* SRC: phc-test/framework/lib/test.php line 343 */
void c_test::t_finish_test() {
  INSTANCE_METHOD_INJECTION(Test, Test::finish_test);
  Variant eo_0;
  Variant eo_1;
  String v_reset;
  Variant v_test;
  Variant v_triple;
  Variant v_phrase;
  Variant v_colour;
  String v_word;
  String v_string;

  LINE(345,t_erase_progress_bar());
  LINE(347,id(f_red_string()));
  LINE(348,id(f_blue_string()));
  LINE(349,id(f_green_string()));
  (v_reset = LINE(350,f_reset_string()));
  (v_test = LINE(352,o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0)));
  (v_triple = LINE(353,t_get_triple_string(true)));
  (v_phrase = LINE(355,t_get_appropriate_phrase()));
  (v_colour = LINE(356,t_get_appropriate_colour()));
  (v_word = LINE(357,concat4(toString(v_colour), toString(v_phrase), ":", v_reset)));
  (v_string = LINE(361,x_sprintf(4, "%-27s %20s %41s\n", Array(ArrayInit(3).set(0, v_test).set(1, v_word).set(2, v_triple).create()))));
  print(v_string);
  LINE(363,(assignCallTemp(eo_1, f_strip_colour(v_string)),f_write_status("results", eo_1)));
} /* function */
/* SRC: phc-test/framework/lib/test.php line 367 */
bool c_test::t_check_exception(CVarRef v_subject) {
  INSTANCE_METHOD_INJECTION(Test, Test::check_exception);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_exceptions __attribute__((__unused__)) = g->GV(exceptions);
  Variant v_array;

  if (!(isset(gv_exceptions, LINE(370,o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0))))) return false;
  (v_array = gv_exceptions.rvalAt(LINE(371,o_root_invoke_few_args("get_name", 0x6056A09A61AC106ELL, 0))));
  LINE(372,x_assert(x_is_array(v_array)));
  return (LINE(373,x_in_array(v_subject, v_array)));
} /* function */
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(9,pm_php$phc_test$framework$lib$labels_php(true, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
