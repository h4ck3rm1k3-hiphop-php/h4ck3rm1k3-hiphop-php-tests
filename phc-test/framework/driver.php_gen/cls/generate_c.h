
#ifndef __GENERATED_cls_generate_c_h__
#define __GENERATED_cls_generate_c_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/generate_c.php line 14 */
class c_generate_c : virtual public c_asynctest {
  BEGIN_CLASS_MAP(generate_c)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(generate_c)
  DECLARE_CLASS(generate_c, Generate_C, asynctest)
  void init();
  public: Array t_get_dependent_test_names();
  public: Variant t_get_test_subjects();
  public: void t_run_test(CVarRef v_subject);
  public: void t_finish(CVarRef v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_generate_c_h__
