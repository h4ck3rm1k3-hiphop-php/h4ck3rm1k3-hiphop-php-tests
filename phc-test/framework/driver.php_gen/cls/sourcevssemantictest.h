
#ifndef __GENERATED_cls_sourcevssemantictest_h__
#define __GENERATED_cls_sourcevssemantictest_h__

#include <cls/plugintest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/source_vs_semantic_values.php line 10 */
class c_sourcevssemantictest : virtual public c_plugintest {
  BEGIN_CLASS_MAP(sourcevssemantictest)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
    PARENT_CLASS(plugintest)
  END_CLASS_MAP(sourcevssemantictest)
  DECLARE_CLASS(sourcevssemantictest, SourceVsSemanticTest, plugintest)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Array t_get_dependent_test_names();
  public: String t_get_command_line(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sourcevssemantictest_h__
