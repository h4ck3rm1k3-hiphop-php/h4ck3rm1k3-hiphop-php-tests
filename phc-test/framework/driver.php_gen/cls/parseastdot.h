
#ifndef __GENERATED_cls_parseastdot_h__
#define __GENERATED_cls_parseastdot_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/parse_ast_dot.php line 10 */
class c_parseastdot : virtual public c_asynctest {
  BEGIN_CLASS_MAP(parseastdot)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(parseastdot)
  DECLARE_CLASS(parseastdot, ParseASTDot, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: Array t_get_dependent_test_names();
  public: bool t_check_prerequisites();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parseastdot_h__
