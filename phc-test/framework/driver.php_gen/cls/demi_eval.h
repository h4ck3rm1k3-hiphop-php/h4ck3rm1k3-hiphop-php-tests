
#ifndef __GENERATED_cls_demi_eval_h__
#define __GENERATED_cls_demi_eval_h__

#include <cls/compiledvsinterpreted.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/demi_eval.php line 13 */
class c_demi_eval : virtual public c_compiledvsinterpreted {
  BEGIN_CLASS_MAP(demi_eval)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
    PARENT_CLASS(compiledvsinterpreted)
  END_CLASS_MAP(demi_eval)
  DECLARE_CLASS(demi_eval, Demi_eval, compiledvsinterpreted)
  void init();
  public: void t___construct(Variant v_init);
  public: ObjectData *create(Variant v_init);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Array t_get_dependent_test_names();
  public: String t_get_name();
  public: String t_get_php_command(CVarRef v_subject);
  public: String t_get_phc_command(CVarRef v_subject, CVarRef v_exe_name);
  public: Variant t_homogenize_output(Variant v_string, CVarRef v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_demi_eval_h__
