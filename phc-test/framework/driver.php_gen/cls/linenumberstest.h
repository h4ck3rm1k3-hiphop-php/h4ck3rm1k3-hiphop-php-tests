
#ifndef __GENERATED_cls_linenumberstest_h__
#define __GENERATED_cls_linenumberstest_h__

#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/line_numbers.php line 10 */
class c_linenumberstest : virtual public c_test {
  BEGIN_CLASS_MAP(linenumberstest)
    PARENT_CLASS(test)
  END_CLASS_MAP(linenumberstest)
  DECLARE_CLASS(linenumberstest, LineNumbersTest, test)
  void init();
  public: Variant t_get_test_subjects();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linenumberstest_h__
