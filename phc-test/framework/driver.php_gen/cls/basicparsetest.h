
#ifndef __GENERATED_cls_basicparsetest_h__
#define __GENERATED_cls_basicparsetest_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/basic_parse_test.php line 11 */
class c_basicparsetest : virtual public c_asynctest {
  BEGIN_CLASS_MAP(basicparsetest)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(basicparsetest)
  DECLARE_CLASS(basicparsetest, BasicParseTest, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: void t_run_test(CVarRef v_subject);
  public: void t_finish(CVarRef v_bundle);
  public: Array t_get_dependent_test_names();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_basicparsetest_h__
