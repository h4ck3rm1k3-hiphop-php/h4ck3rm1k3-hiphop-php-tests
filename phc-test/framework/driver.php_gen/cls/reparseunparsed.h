
#ifndef __GENERATED_cls_reparseunparsed_h__
#define __GENERATED_cls_reparseunparsed_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/reparse_unparsed.php line 10 */
class c_reparseunparsed : virtual public c_asynctest {
  BEGIN_CLASS_MAP(reparseunparsed)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(reparseunparsed)
  DECLARE_CLASS(reparseunparsed, ReparseUnparsed, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: Array t_get_dependent_test_names();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_reparseunparsed_h__
