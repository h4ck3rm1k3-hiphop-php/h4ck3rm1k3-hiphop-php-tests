
#ifndef __GENERATED_cls_parsetreedot_h__
#define __GENERATED_cls_parsetreedot_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/parse_tree_dot.php line 10 */
class c_parsetreedot : virtual public c_asynctest {
  BEGIN_CLASS_MAP(parsetreedot)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(parsetreedot)
  DECLARE_CLASS(parsetreedot, ParseTreeDot, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: Array t_get_dependent_test_names();
  public: bool t_check_prerequisites();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parsetreedot_h__
