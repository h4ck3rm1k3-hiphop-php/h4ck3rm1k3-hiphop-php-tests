
#ifndef __GENERATED_cls_plugintest_h__
#define __GENERATED_cls_plugintest_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/plugin_test.php line 11 */
class c_plugintest : virtual public c_asynctest {
  BEGIN_CLASS_MAP(plugintest)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(plugintest)
  DECLARE_CLASS(plugintest, PluginTest, asynctest)
  void init();
  public: void t___construct(Variant v_plugin_name, Variant v_other_commands = "");
  public: ObjectData *create(Variant v_plugin_name, Variant v_other_commands = "");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Array t_get_dependent_test_names();
  public: Variant t_get_test_subjects();
  public: bool t_check_prerequisites();
  public: String t_get_name();
  public: String t_get_command_line(CVarRef v_subject);
  public: void t_finish(CVarRef v_async);
  public: void t_run_test(Variant v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_plugintest_h__
