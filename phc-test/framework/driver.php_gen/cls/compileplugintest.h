
#ifndef __GENERATED_cls_compileplugintest_h__
#define __GENERATED_cls_compileplugintest_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/compile_plugin_test.php line 9 */
class c_compileplugintest : virtual public c_asynctest {
  BEGIN_CLASS_MAP(compileplugintest)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(compileplugintest)
  DECLARE_CLASS(compileplugintest, CompilePluginTest, asynctest)
  void init();
  public: bool t_check_prerequisites();
  public: Variant t_get_test_subjects();
  public: Array t_get_dependent_test_names();
  public: void t_copy_headers();
  public: void t_run();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_compileplugintest_h__
