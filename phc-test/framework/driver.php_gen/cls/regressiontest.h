
#ifndef __GENERATED_cls_regressiontest_h__
#define __GENERATED_cls_regressiontest_h__

#include <cls/supportfiletest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/regression.php line 8 */
class c_regressiontest : virtual public c_supportfiletest {
  BEGIN_CLASS_MAP(regressiontest)
    PARENT_CLASS(test)
    PARENT_CLASS(supportfiletest)
  END_CLASS_MAP(regressiontest)
  DECLARE_CLASS(regressiontest, RegressionTest, supportfiletest)
  void init();
  public: void t_regressiontest(CStrRef v_name, CStrRef v_command_line_options, CStrRef v_support_file_suffix, CArrRef v_dependencies = ScalarArrays::sa_[3]);
  public: ObjectData *create(CStrRef v_name, CStrRef v_command_line_options, CStrRef v_support_file_suffix, CArrRef v_dependencies = ScalarArrays::sa_[3]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Array t_get_dependent_test_names();
  public: Variant t_get_name();
  public: String t_get_support_filename(CVarRef v_subject);
  public: Variant t_get_test_subjects();
  public: String t_get_command_line(CVarRef v_subject);
  public: void t_run_test(Variant v_subject);
  public: Variant t_generate_support_files();
  public: void t_generate_support_file(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_regressiontest_h__
