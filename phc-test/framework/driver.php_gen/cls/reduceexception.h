
#ifndef __GENERATED_cls_reduceexception_h__
#define __GENERATED_cls_reduceexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/reduce/Reduce.php line 135 */
class c_reduceexception : virtual public c_exception {
  BEGIN_CLASS_MAP(reduceexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(reduceexception)
  DECLARE_CLASS(reduceexception, ReduceException, exception)
  void init();
  public: void t___construct(Variant v_message);
  public: ObjectData *create(Variant v_message);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_reduceexception_h__
