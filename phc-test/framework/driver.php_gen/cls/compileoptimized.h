
#ifndef __GENERATED_cls_compileoptimized_h__
#define __GENERATED_cls_compileoptimized_h__

#include <cls/compiledvsinterpreted.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/compile_optimized.php line 11 */
class c_compileoptimized : virtual public c_compiledvsinterpreted {
  BEGIN_CLASS_MAP(compileoptimized)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
    PARENT_CLASS(compiledvsinterpreted)
  END_CLASS_MAP(compileoptimized)
  DECLARE_CLASS(compileoptimized, CompileOptimized, compiledvsinterpreted)
  void init();
  public: Array t_get_dependent_test_names();
  public: String t_get_phc_command(CVarRef v_subject, CVarRef v_exe_name);
  public: String t_get_php_command(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_compileoptimized_h__
