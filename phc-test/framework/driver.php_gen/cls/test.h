
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/lib/test.php line 10 */
class c_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(test)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, Test, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_get_name();
  public: void t_ready_progress_bar(int v_num_files);
  public: Variant t_homogenize_output(CVarRef v_string);
  public: bool t_check_global_prerequisites();
  public: bool t_check_prerequisites();
  public: bool t_check_test_prerequisites(CVarRef v_subject);
  public: Array t_get_dependent_test_names();
  public: Array t_get_builtin_dependent_test_names();
  public: void t_calculate_all_dependencies();
  public: Variant t_get_all_dependencies();
  public: bool t_passed_test_dependencies(CVarRef v_subject);
  // public: void t_run_test(CVarRef v_subject) = 0;
  // public: void t_get_test_subjects() = 0;
  public: void t_print_numbered();
  public: Variant t_run();
  public: void t_mark_success(CVarRef v_subject);
  public: void t_mark_timeout(CVarRef v_subject, CVarRef v_commands, CVarRef v_outs, CVarRef v_errs, CVarRef v_exits);
  public: void t_mark_skipped(CVarRef v_subject, CStrRef v_reason);
  public: void t_mark_failure(CVarRef v_subject, CVarRef v_commands, CVarRef v_outs = "Not relevent", CVarRef v_errs = "Not relevent", CVarRef v_exits = "Not relevent", CVarRef v_reason = "TODO - no reason given");
  public: bool t_is_successful();
  public: bool t_is_completely_skipped();
  public: Variant t_get_appropriate_colour();
  public: Variant t_get_appropriate_phrase();
  public: Variant t_get_triple_string(bool v_finished = false);
  public: void t_update_count();
  public: void t_erase_progress_bar();
  public: void t_display_progress_bar();
  public: void t_finish_test();
  public: bool t_check_exception(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
