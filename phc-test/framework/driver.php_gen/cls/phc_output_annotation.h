
#ifndef __GENERATED_cls_phc_output_annotation_h__
#define __GENERATED_cls_phc_output_annotation_h__

#include <cls/test_annotation.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/annotated_test.php line 102 */
class c_phc_output_annotation : virtual public c_test_annotation {
  BEGIN_CLASS_MAP(phc_output_annotation)
    PARENT_CLASS(test_annotation)
  END_CLASS_MAP(phc_output_annotation)
  DECLARE_CLASS(phc_output_annotation, PHC_output_annotation, test_annotation)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Array t_get_default_options();
  public: Variant t_post_process(Variant v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_phc_output_annotation_h__
