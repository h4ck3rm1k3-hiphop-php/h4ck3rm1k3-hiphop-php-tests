
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 255) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x6D72CEB44AC01301LL, test_name, 66);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 7:
      HASH_INDEX(0x4E80F79695D41A07LL, opt, 50);
      break;
    case 12:
      HASH_INDEX(0x21DEE4EB204AE10CLL, cg, 46);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      HASH_INDEX(0x2F2AA415EFAE460ELL, base_dir, 25);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x3A1DC49AA6CFEA13LL, test, 65);
      break;
    case 22:
      HASH_INDEX(0x73FDA0B4AF645216LL, non_default_labels, 16);
      break;
    case 26:
      HASH_INDEX(0x17B5D58607FC7F1ALL, default_labels, 15);
      break;
    case 27:
      HASH_INDEX(0x2FF01C2058B0B21BLL, opt_verbose, 43);
      HASH_INDEX(0x027BD19E0751121BLL, opt_numbered, 53);
      break;
    case 34:
      HASH_INDEX(0x7359B522C6BE3A22LL, phc_suffix, 34);
      break;
    case 36:
      HASH_INDEX(0x2F475D4DE7208D24LL, opt_valgrind, 33);
      break;
    case 38:
      HASH_INDEX(0x1DA88EB5034B9D26LL, plugin_dir, 24);
      break;
    case 39:
      HASH_INDEX(0x3F4289BA72D62D27LL, opt_installed, 56);
      break;
    case 43:
      HASH_INDEX(0x2128D3CAEC57A82BLL, opt_support, 52);
      break;
    case 46:
      HASH_INDEX(0x7DDBEB38F40F832ELL, labels, 17);
      break;
    case 48:
      HASH_INDEX(0x5C390FD428B50E30LL, opt_long, 21);
      break;
    case 52:
      HASH_INDEX(0x421780730E09F634LL, opts, 48);
      break;
    case 54:
      HASH_INDEX(0x19D6108580CBB436LL, label_struct, 20);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 64:
      HASH_INDEX(0x30704984F0078F40LL, match, 67);
      break;
    case 65:
      HASH_INDEX(0x52B182B0A94E9C41LL, php, 39);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x58FBD12F28C4BF49LL, label_file, 13);
      break;
    case 75:
      HASH_INDEX(0x1018D4B85360194BLL, opt_help, 54);
      break;
    case 78:
      HASH_INDEX(0x0510D59D8A97894ELL, opt_debug, 31);
      break;
    case 95:
      HASH_INDEX(0x11FFDF37C0EB2C5FLL, tests, 26);
      break;
    case 96:
      HASH_INDEX(0x4B2063F3C49B8E60LL, exceptions, 19);
      break;
    case 102:
      HASH_INDEX(0x0DC38D7049391A66LL, trunk_CPPFLAGS, 29);
      break;
    case 106:
      HASH_INDEX(0x67EA86E1CF55026ALL, phc, 23);
      break;
    case 107:
      HASH_INDEX(0x210CB79045B0086BLL, bindir, 61);
      break;
    case 123:
      HASH_INDEX(0x630DB2468196C37BLL, gcc, 45);
      break;
    case 126:
      HASH_INDEX(0x287DDAB321A4FF7ELL, support_dir, 60);
      break;
    case 130:
      HASH_INDEX(0x47A8BEF0E404C882LL, opposite_label, 18);
      break;
    case 144:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 147:
      HASH_INDEX(0x01F18996FC6E6193LL, php_exe, 38);
      break;
    case 151:
      HASH_INDEX(0x65CC93B55175C597LL, phc_compile_plugin, 27);
      break;
    case 156:
      HASH_INDEX(0x3901F738F39EE99CLL, graphviz_gc, 32);
      break;
    case 157:
      HASH_INDEX(0x6890E1EA533ADF9DLL, getopt, 47);
      break;
    case 163:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 182:
      HASH_INDEX(0x3725D0B1F30F14B6LL, xerces_compiled_in, 30);
      break;
    case 183:
      HASH_INDEX(0x4FFE4179D4CE15B7LL, opt_quick, 58);
      break;
    case 190:
      HASH_INDEX(0x18532A5EE8396FBELL, date_string, 44);
      break;
    case 198:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 206:
      HASH_INDEX(0x464FC5785F58C2CELL, pwd, 63);
      break;
    case 207:
      HASH_INDEX(0x522821F5063592CFLL, dir, 64);
      HASH_INDEX(0x2963E59544F2D0CFLL, regex, 68);
      break;
    case 209:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      HASH_INDEX(0x6C2E2CD6695D83D1LL, log_directory, 42);
      break;
    case 218:
      HASH_INDEX(0x35A19C689312B1DALL, opt_no_progress_bar, 55);
      break;
    case 220:
      HASH_INDEX(0x0F60C6595ED9FADCLL, arguments, 49);
      break;
    case 223:
      HASH_INDEX(0x7076E3AEF02E9CDFLL, opt_one, 22);
      break;
    case 224:
      HASH_INDEX(0x5522393FED31B4E0LL, pkglibdir, 62);
      break;
    case 227:
      HASH_INDEX(0x614447AB21C8FAE3LL, valgrind, 35);
      break;
    case 229:
      HASH_INDEX(0x093CA964B9E038E5LL, working_directory, 28);
      break;
    case 233:
      HASH_INDEX(0x38BA10F6BA1F67E9LL, working_dir, 37);
      break;
    case 234:
      HASH_INDEX(0x7C17922060DCA1EALL, options, 51);
      break;
    case 238:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 240:
      HASH_INDEX(0x7FD127A282D4A0F0LL, strict, 40);
      HASH_INDEX(0x300C2818152F9FF0LL, opt_reduce, 59);
      break;
    case 241:
      HASH_INDEX(0x070DFE5B93A7CAF1LL, opt_clean, 57);
      break;
    case 243:
      HASH_INDEX(0x6FB2F3D573DE52F3LL, subject_dir, 12);
      break;
    case 247:
      HASH_INDEX(0x7D6AD2C5948BA3F7LL, third_party_label_file, 14);
      HASH_INDEX(0x626A556D885E55F7LL, status_files, 41);
      break;
    case 253:
      HASH_INDEX(0x0B5BC0452CFBA5FDLL, libphp, 36);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 69) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
