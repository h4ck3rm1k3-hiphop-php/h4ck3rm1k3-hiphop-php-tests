
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_comparebackwards(CArrRef params, bool init = true);
Variant cw_comparebackwards$os_get(const char *s);
Variant &cw_comparebackwards$os_lval(const char *s);
Variant cw_comparebackwards$os_constant(const char *s);
Variant cw_comparebackwards$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_pass_dump(CArrRef params, bool init = true);
Variant cw_pass_dump$os_get(const char *s);
Variant &cw_pass_dump$os_lval(const char *s);
Variant cw_pass_dump$os_constant(const char *s);
Variant cw_pass_dump$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_asyncbundle(CArrRef params, bool init = true);
Variant cw_asyncbundle$os_get(const char *s);
Variant &cw_asyncbundle$os_lval(const char *s);
Variant cw_asyncbundle$os_constant(const char *s);
Variant cw_asyncbundle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_asynctest(CArrRef params, bool init = true);
Variant cw_asynctest$os_get(const char *s);
Variant &cw_asynctest$os_lval(const char *s);
Variant cw_asynctest$os_constant(const char *s);
Variant cw_asynctest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_basicparsetest(CArrRef params, bool init = true);
Variant cw_basicparsetest$os_get(const char *s);
Variant &cw_basicparsetest$os_lval(const char *s);
Variant cw_basicparsetest$os_constant(const char *s);
Variant cw_basicparsetest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_supportfiletest(CArrRef params, bool init = true);
Variant cw_supportfiletest$os_get(const char *s);
Variant &cw_supportfiletest$os_lval(const char *s);
Variant cw_supportfiletest$os_constant(const char *s);
Variant cw_supportfiletest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_xml_roundtrip(CArrRef params, bool init = true);
Variant cw_xml_roundtrip$os_get(const char *s);
Variant &cw_xml_roundtrip$os_lval(const char *s);
Variant cw_xml_roundtrip$os_constant(const char *s);
Variant cw_xml_roundtrip$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_nowhitespace(CArrRef params, bool init = true);
Variant cw_nowhitespace$os_get(const char *s);
Variant &cw_nowhitespace$os_lval(const char *s);
Variant cw_nowhitespace$os_constant(const char *s);
Variant cw_nowhitespace$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_phc_output_annotation(CArrRef params, bool init = true);
Variant cw_phc_output_annotation$os_get(const char *s);
Variant &cw_phc_output_annotation$os_lval(const char *s);
Variant cw_phc_output_annotation$os_constant(const char *s);
Variant cw_phc_output_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_annotated_test(CArrRef params, bool init = true);
Variant cw_annotated_test$os_get(const char *s);
Variant &cw_annotated_test$os_lval(const char *s);
Variant cw_annotated_test$os_constant(const char *s);
Variant cw_annotated_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_phc_exit_code_annotation(CArrRef params, bool init = true);
Variant cw_phc_exit_code_annotation$os_get(const char *s);
Variant &cw_phc_exit_code_annotation$os_lval(const char *s);
Variant cw_phc_exit_code_annotation$os_constant(const char *s);
Variant cw_phc_exit_code_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parseastdot(CArrRef params, bool init = true);
Variant cw_parseastdot$os_get(const char *s);
Variant &cw_parseastdot$os_lval(const char *s);
Variant cw_parseastdot$os_constant(const char *s);
Variant cw_parseastdot$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test_annotation(CArrRef params, bool init = true);
Variant cw_test_annotation$os_get(const char *s);
Variant &cw_test_annotation$os_lval(const char *s);
Variant cw_test_annotation$os_constant(const char *s);
Variant cw_test_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_compileplugintest(CArrRef params, bool init = true);
Variant cw_compileplugintest$os_get(const char *s);
Variant &cw_compileplugintest$os_lval(const char *s);
Variant cw_compileplugintest$os_constant(const char *s);
Variant cw_compileplugintest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_plugintest(CArrRef params, bool init = true);
Variant cw_plugintest$os_get(const char *s);
Variant &cw_plugintest$os_lval(const char *s);
Variant cw_plugintest$os_constant(const char *s);
Variant cw_plugintest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_annotation_translator(CArrRef params, bool init = true);
Variant cw_annotation_translator$os_get(const char *s);
Variant &cw_annotation_translator$os_lval(const char *s);
Variant cw_annotation_translator$os_constant(const char *s);
Variant cw_annotation_translator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_demi_eval(CArrRef params, bool init = true);
Variant cw_demi_eval$os_get(const char *s);
Variant &cw_demi_eval$os_lval(const char *s);
Variant cw_demi_eval$os_constant(const char *s);
Variant cw_demi_eval$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sourcevssemantictest(CArrRef params, bool init = true);
Variant cw_sourcevssemantictest$os_get(const char *s);
Variant &cw_sourcevssemantictest$os_lval(const char *s);
Variant cw_sourcevssemantictest$os_constant(const char *s);
Variant cw_sourcevssemantictest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_compiledvsinterpreted(CArrRef params, bool init = true);
Variant cw_compiledvsinterpreted$os_get(const char *s);
Variant &cw_compiledvsinterpreted$os_lval(const char *s);
Variant cw_compiledvsinterpreted$os_constant(const char *s);
Variant cw_compiledvsinterpreted$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_refcounts(CArrRef params, bool init = true);
Variant cw_refcounts$os_get(const char *s);
Variant &cw_refcounts$os_lval(const char *s);
Variant cw_refcounts$os_constant(const char *s);
Variant cw_refcounts$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linenumberstest(CArrRef params, bool init = true);
Variant cw_linenumberstest$os_get(const char *s);
Variant &cw_linenumberstest$os_lval(const char *s);
Variant cw_linenumberstest$os_constant(const char *s);
Variant cw_linenumberstest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_phc_option_annotation(CArrRef params, bool init = true);
Variant cw_phc_option_annotation$os_get(const char *s);
Variant &cw_phc_option_annotation$os_lval(const char *s);
Variant cw_phc_option_annotation$os_constant(const char *s);
Variant cw_phc_option_annotation$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_test(CArrRef params, bool init = true);
Variant cw_test$os_get(const char *s);
Variant &cw_test$os_lval(const char *s);
Variant cw_test$os_constant(const char *s);
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_compileoptimized(CArrRef params, bool init = true);
Variant cw_compileoptimized$os_get(const char *s);
Variant &cw_compileoptimized$os_lval(const char *s);
Variant cw_compileoptimized$os_constant(const char *s);
Variant cw_compileoptimized$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_generate_c(CArrRef params, bool init = true);
Variant cw_generate_c$os_get(const char *s);
Variant &cw_generate_c$os_lval(const char *s);
Variant cw_generate_c$os_constant(const char *s);
Variant cw_generate_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_reparseunparsed(CArrRef params, bool init = true);
Variant cw_reparseunparsed$os_get(const char *s);
Variant &cw_reparseunparsed$os_lval(const char *s);
Variant cw_reparseunparsed$os_constant(const char *s);
Variant cw_reparseunparsed$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parsetreedot(CArrRef params, bool init = true);
Variant cw_parsetreedot$os_get(const char *s);
Variant &cw_parsetreedot$os_lval(const char *s);
Variant cw_parsetreedot$os_constant(const char *s);
Variant cw_parsetreedot$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_reduce(CArrRef params, bool init = true);
Variant cw_reduce$os_get(const char *s);
Variant &cw_reduce$os_lval(const char *s);
Variant cw_reduce$os_constant(const char *s);
Variant cw_reduce$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_regressiontest(CArrRef params, bool init = true);
Variant cw_regressiontest$os_get(const char *s);
Variant &cw_regressiontest$os_lval(const char *s);
Variant cw_regressiontest$os_constant(const char *s);
Variant cw_regressiontest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_comparewithphp(CArrRef params, bool init = true);
Variant cw_comparewithphp$os_get(const char *s);
Variant &cw_comparewithphp$os_lval(const char *s);
Variant cw_comparewithphp$os_constant(const char *s);
Variant cw_comparewithphp$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_reduceexception(CArrRef params, bool init = true);
Variant cw_reduceexception$os_get(const char *s);
Variant &cw_reduceexception$os_lval(const char *s);
Variant cw_reduceexception$os_constant(const char *s);
Variant cw_reduceexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 0:
      HASH_CREATE_OBJECT(0x155BAEE6CD0C0280LL, generate_c);
      HASH_CREATE_OBJECT(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x00E5330C9F610102LL, demi_eval);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x074760E543088A83LL, reduceexception);
      break;
    case 8:
      HASH_CREATE_OBJECT(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_CREATE_OBJECT(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      break;
    case 14:
      HASH_CREATE_OBJECT(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 16:
      HASH_CREATE_OBJECT(0x31B54B6CC2EE3610LL, parsetreedot);
      break;
    case 17:
      HASH_CREATE_OBJECT(0x222CABA6331B7B91LL, sourcevssemantictest);
      break;
    case 20:
      HASH_CREATE_OBJECT(0x36DF3FFAE39AED54LL, pass_dump);
      break;
    case 21:
      HASH_CREATE_OBJECT(0x04D45152AC856A95LL, compiledvsinterpreted);
      break;
    case 28:
      HASH_CREATE_OBJECT(0x53FEB79F49DD14DCLL, basicparsetest);
      HASH_CREATE_OBJECT(0x61C8E0DF026F0E5CLL, plugintest);
      break;
    case 35:
      HASH_CREATE_OBJECT(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    case 38:
      HASH_CREATE_OBJECT(0x50748AEE280A69A6LL, comparebackwards);
      HASH_CREATE_OBJECT(0x1948F3EA228E8066LL, parseastdot);
      HASH_CREATE_OBJECT(0x3D9A6458C699DF66LL, refcounts);
      break;
    case 39:
      HASH_CREATE_OBJECT(0x09E7723E191A77E7LL, test_annotation);
      HASH_CREATE_OBJECT(0x24364CC4C27751E7LL, compileplugintest);
      HASH_CREATE_OBJECT(0x37349B25A0ED29E7LL, test);
      break;
    case 41:
      HASH_CREATE_OBJECT(0x3AE6736DDD5F9229LL, nowhitespace);
      HASH_CREATE_OBJECT(0x7938B508681D0769LL, annotation_translator);
      break;
    case 46:
      HASH_CREATE_OBJECT(0x1B7107CB8741112ELL, compileoptimized);
      break;
    case 51:
      HASH_CREATE_OBJECT(0x567C177C87E78EF3LL, xml_roundtrip);
      break;
    case 53:
      HASH_CREATE_OBJECT(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 54:
      HASH_CREATE_OBJECT(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 56:
      HASH_CREATE_OBJECT(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_CREATE_OBJECT(0x1B45B66F5DF8EE78LL, reduce);
      break;
    case 58:
      HASH_CREATE_OBJECT(0x1D1B02CC47D4C53ALL, reparseunparsed);
      break;
    case 60:
      HASH_CREATE_OBJECT(0x3B9526C2DDB746BCLL, linenumberstest);
      break;
    case 62:
      HASH_CREATE_OBJECT(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x155BAEE6CD0C0280LL, generate_c);
      HASH_INVOKE_STATIC_METHOD(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x00E5330C9F610102LL, demi_eval);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x074760E543088A83LL, reduceexception);
      break;
    case 8:
      HASH_INVOKE_STATIC_METHOD(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_INVOKE_STATIC_METHOD(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      break;
    case 14:
      HASH_INVOKE_STATIC_METHOD(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 16:
      HASH_INVOKE_STATIC_METHOD(0x31B54B6CC2EE3610LL, parsetreedot);
      break;
    case 17:
      HASH_INVOKE_STATIC_METHOD(0x222CABA6331B7B91LL, sourcevssemantictest);
      break;
    case 20:
      HASH_INVOKE_STATIC_METHOD(0x36DF3FFAE39AED54LL, pass_dump);
      break;
    case 21:
      HASH_INVOKE_STATIC_METHOD(0x04D45152AC856A95LL, compiledvsinterpreted);
      break;
    case 28:
      HASH_INVOKE_STATIC_METHOD(0x53FEB79F49DD14DCLL, basicparsetest);
      HASH_INVOKE_STATIC_METHOD(0x61C8E0DF026F0E5CLL, plugintest);
      break;
    case 35:
      HASH_INVOKE_STATIC_METHOD(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    case 38:
      HASH_INVOKE_STATIC_METHOD(0x50748AEE280A69A6LL, comparebackwards);
      HASH_INVOKE_STATIC_METHOD(0x1948F3EA228E8066LL, parseastdot);
      HASH_INVOKE_STATIC_METHOD(0x3D9A6458C699DF66LL, refcounts);
      break;
    case 39:
      HASH_INVOKE_STATIC_METHOD(0x09E7723E191A77E7LL, test_annotation);
      HASH_INVOKE_STATIC_METHOD(0x24364CC4C27751E7LL, compileplugintest);
      HASH_INVOKE_STATIC_METHOD(0x37349B25A0ED29E7LL, test);
      break;
    case 41:
      HASH_INVOKE_STATIC_METHOD(0x3AE6736DDD5F9229LL, nowhitespace);
      HASH_INVOKE_STATIC_METHOD(0x7938B508681D0769LL, annotation_translator);
      break;
    case 46:
      HASH_INVOKE_STATIC_METHOD(0x1B7107CB8741112ELL, compileoptimized);
      break;
    case 51:
      HASH_INVOKE_STATIC_METHOD(0x567C177C87E78EF3LL, xml_roundtrip);
      break;
    case 53:
      HASH_INVOKE_STATIC_METHOD(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 54:
      HASH_INVOKE_STATIC_METHOD(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 56:
      HASH_INVOKE_STATIC_METHOD(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_INVOKE_STATIC_METHOD(0x1B45B66F5DF8EE78LL, reduce);
      break;
    case 58:
      HASH_INVOKE_STATIC_METHOD(0x1D1B02CC47D4C53ALL, reparseunparsed);
      break;
    case 60:
      HASH_INVOKE_STATIC_METHOD(0x3B9526C2DDB746BCLL, linenumberstest);
      break;
    case 62:
      HASH_INVOKE_STATIC_METHOD(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x155BAEE6CD0C0280LL, generate_c);
      HASH_GET_STATIC_PROPERTY(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x00E5330C9F610102LL, demi_eval);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x074760E543088A83LL, reduceexception);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY(0x31B54B6CC2EE3610LL, parsetreedot);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY(0x222CABA6331B7B91LL, sourcevssemantictest);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY(0x36DF3FFAE39AED54LL, pass_dump);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY(0x04D45152AC856A95LL, compiledvsinterpreted);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY(0x53FEB79F49DD14DCLL, basicparsetest);
      HASH_GET_STATIC_PROPERTY(0x61C8E0DF026F0E5CLL, plugintest);
      break;
    case 35:
      HASH_GET_STATIC_PROPERTY(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    case 38:
      HASH_GET_STATIC_PROPERTY(0x50748AEE280A69A6LL, comparebackwards);
      HASH_GET_STATIC_PROPERTY(0x1948F3EA228E8066LL, parseastdot);
      HASH_GET_STATIC_PROPERTY(0x3D9A6458C699DF66LL, refcounts);
      break;
    case 39:
      HASH_GET_STATIC_PROPERTY(0x09E7723E191A77E7LL, test_annotation);
      HASH_GET_STATIC_PROPERTY(0x24364CC4C27751E7LL, compileplugintest);
      HASH_GET_STATIC_PROPERTY(0x37349B25A0ED29E7LL, test);
      break;
    case 41:
      HASH_GET_STATIC_PROPERTY(0x3AE6736DDD5F9229LL, nowhitespace);
      HASH_GET_STATIC_PROPERTY(0x7938B508681D0769LL, annotation_translator);
      break;
    case 46:
      HASH_GET_STATIC_PROPERTY(0x1B7107CB8741112ELL, compileoptimized);
      break;
    case 51:
      HASH_GET_STATIC_PROPERTY(0x567C177C87E78EF3LL, xml_roundtrip);
      break;
    case 53:
      HASH_GET_STATIC_PROPERTY(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 54:
      HASH_GET_STATIC_PROPERTY(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 56:
      HASH_GET_STATIC_PROPERTY(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_GET_STATIC_PROPERTY(0x1B45B66F5DF8EE78LL, reduce);
      break;
    case 58:
      HASH_GET_STATIC_PROPERTY(0x1D1B02CC47D4C53ALL, reparseunparsed);
      break;
    case 60:
      HASH_GET_STATIC_PROPERTY(0x3B9526C2DDB746BCLL, linenumberstest);
      break;
    case 62:
      HASH_GET_STATIC_PROPERTY(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x155BAEE6CD0C0280LL, generate_c);
      HASH_GET_STATIC_PROPERTY_LV(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x00E5330C9F610102LL, demi_eval);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x074760E543088A83LL, reduceexception);
      break;
    case 8:
      HASH_GET_STATIC_PROPERTY_LV(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_GET_STATIC_PROPERTY_LV(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY_LV(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY_LV(0x31B54B6CC2EE3610LL, parsetreedot);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY_LV(0x222CABA6331B7B91LL, sourcevssemantictest);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY_LV(0x36DF3FFAE39AED54LL, pass_dump);
      break;
    case 21:
      HASH_GET_STATIC_PROPERTY_LV(0x04D45152AC856A95LL, compiledvsinterpreted);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY_LV(0x53FEB79F49DD14DCLL, basicparsetest);
      HASH_GET_STATIC_PROPERTY_LV(0x61C8E0DF026F0E5CLL, plugintest);
      break;
    case 35:
      HASH_GET_STATIC_PROPERTY_LV(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    case 38:
      HASH_GET_STATIC_PROPERTY_LV(0x50748AEE280A69A6LL, comparebackwards);
      HASH_GET_STATIC_PROPERTY_LV(0x1948F3EA228E8066LL, parseastdot);
      HASH_GET_STATIC_PROPERTY_LV(0x3D9A6458C699DF66LL, refcounts);
      break;
    case 39:
      HASH_GET_STATIC_PROPERTY_LV(0x09E7723E191A77E7LL, test_annotation);
      HASH_GET_STATIC_PROPERTY_LV(0x24364CC4C27751E7LL, compileplugintest);
      HASH_GET_STATIC_PROPERTY_LV(0x37349B25A0ED29E7LL, test);
      break;
    case 41:
      HASH_GET_STATIC_PROPERTY_LV(0x3AE6736DDD5F9229LL, nowhitespace);
      HASH_GET_STATIC_PROPERTY_LV(0x7938B508681D0769LL, annotation_translator);
      break;
    case 46:
      HASH_GET_STATIC_PROPERTY_LV(0x1B7107CB8741112ELL, compileoptimized);
      break;
    case 51:
      HASH_GET_STATIC_PROPERTY_LV(0x567C177C87E78EF3LL, xml_roundtrip);
      break;
    case 53:
      HASH_GET_STATIC_PROPERTY_LV(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 54:
      HASH_GET_STATIC_PROPERTY_LV(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 56:
      HASH_GET_STATIC_PROPERTY_LV(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_GET_STATIC_PROPERTY_LV(0x1B45B66F5DF8EE78LL, reduce);
      break;
    case 58:
      HASH_GET_STATIC_PROPERTY_LV(0x1D1B02CC47D4C53ALL, reparseunparsed);
      break;
    case 60:
      HASH_GET_STATIC_PROPERTY_LV(0x3B9526C2DDB746BCLL, linenumberstest);
      break;
    case 62:
      HASH_GET_STATIC_PROPERTY_LV(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 63) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x155BAEE6CD0C0280LL, generate_c);
      HASH_GET_CLASS_CONSTANT(0x7C4B81C57D85C380LL, comparewithphp);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x00E5330C9F610102LL, demi_eval);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x074760E543088A83LL, reduceexception);
      break;
    case 8:
      HASH_GET_CLASS_CONSTANT(0x2E9CE9E80D7BAD48LL, annotated_test);
      break;
    case 9:
      HASH_GET_CLASS_CONSTANT(0x78853DA1DF6E5149LL, phc_exit_code_annotation);
      break;
    case 14:
      HASH_GET_CLASS_CONSTANT(0x63FD3F2EE80E2A4ELL, phc_option_annotation);
      break;
    case 16:
      HASH_GET_CLASS_CONSTANT(0x31B54B6CC2EE3610LL, parsetreedot);
      break;
    case 17:
      HASH_GET_CLASS_CONSTANT(0x222CABA6331B7B91LL, sourcevssemantictest);
      break;
    case 20:
      HASH_GET_CLASS_CONSTANT(0x36DF3FFAE39AED54LL, pass_dump);
      break;
    case 21:
      HASH_GET_CLASS_CONSTANT(0x04D45152AC856A95LL, compiledvsinterpreted);
      break;
    case 28:
      HASH_GET_CLASS_CONSTANT(0x53FEB79F49DD14DCLL, basicparsetest);
      HASH_GET_CLASS_CONSTANT(0x61C8E0DF026F0E5CLL, plugintest);
      break;
    case 35:
      HASH_GET_CLASS_CONSTANT(0x447400A9AEC2F5E3LL, supportfiletest);
      break;
    case 38:
      HASH_GET_CLASS_CONSTANT(0x50748AEE280A69A6LL, comparebackwards);
      HASH_GET_CLASS_CONSTANT(0x1948F3EA228E8066LL, parseastdot);
      HASH_GET_CLASS_CONSTANT(0x3D9A6458C699DF66LL, refcounts);
      break;
    case 39:
      HASH_GET_CLASS_CONSTANT(0x09E7723E191A77E7LL, test_annotation);
      HASH_GET_CLASS_CONSTANT(0x24364CC4C27751E7LL, compileplugintest);
      HASH_GET_CLASS_CONSTANT(0x37349B25A0ED29E7LL, test);
      break;
    case 41:
      HASH_GET_CLASS_CONSTANT(0x3AE6736DDD5F9229LL, nowhitespace);
      HASH_GET_CLASS_CONSTANT(0x7938B508681D0769LL, annotation_translator);
      break;
    case 46:
      HASH_GET_CLASS_CONSTANT(0x1B7107CB8741112ELL, compileoptimized);
      break;
    case 51:
      HASH_GET_CLASS_CONSTANT(0x567C177C87E78EF3LL, xml_roundtrip);
      break;
    case 53:
      HASH_GET_CLASS_CONSTANT(0x1B92C7F07651C1B5LL, regressiontest);
      break;
    case 54:
      HASH_GET_CLASS_CONSTANT(0x60035C245D321D76LL, phc_output_annotation);
      break;
    case 56:
      HASH_GET_CLASS_CONSTANT(0x545A9E1B04FF1438LL, asyncbundle);
      HASH_GET_CLASS_CONSTANT(0x1B45B66F5DF8EE78LL, reduce);
      break;
    case 58:
      HASH_GET_CLASS_CONSTANT(0x1D1B02CC47D4C53ALL, reparseunparsed);
      break;
    case 60:
      HASH_GET_CLASS_CONSTANT(0x3B9526C2DDB746BCLL, linenumberstest);
      break;
    case 62:
      HASH_GET_CLASS_CONSTANT(0x13C5FA969FBED37ELL, asynctest);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
