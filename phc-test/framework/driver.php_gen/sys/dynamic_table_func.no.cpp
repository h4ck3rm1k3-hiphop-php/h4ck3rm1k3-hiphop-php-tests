
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_phc_assert(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_phc_error_handler(CArrRef params);
Variant i_phc_unreachable(CArrRef params);
static Variant invoke_case_2(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x0959807FF78D6FC2LL, phc_error_handler);
  HASH_INVOKE(0x6D8E3CA83F680C7ALL, phc_unreachable);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[8])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 8; i++) funcTable[i] = &invoke_builtin;
    funcTable[1] = &i_phc_assert;
    funcTable[2] = &invoke_case_2;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 7](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
