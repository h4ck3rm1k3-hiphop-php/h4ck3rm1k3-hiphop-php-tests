
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "annotated_test", "phc-test/framework/annotated_test.php",
  "annotation_translator", "phc-test/framework/annotated_test.php",
  "asyncbundle", "phc-test/framework/lib/async_test.php",
  "asynctest", "phc-test/framework/lib/async_test.php",
  "basicparsetest", "phc-test/framework/basic_parse_test.php",
  "comparebackwards", "phc-test/framework/lib/compare_backwards.php",
  "comparewithphp", "phc-test/framework/lib/compare_with_php_test.php",
  "compiledvsinterpreted", "phc-test/framework/compiled_vs_interpreted.php",
  "compileoptimized", "phc-test/framework/compile_optimized.php",
  "compileplugintest", "phc-test/framework/compile_plugin_test.php",
  "demi_eval", "phc-test/framework/demi_eval.php",
  "generate_c", "phc-test/framework/generate_c.php",
  "linenumberstest", "phc-test/framework/line_numbers.php",
  "nowhitespace", "phc-test/framework/no_whitespace.php",
  "parseastdot", "phc-test/framework/parse_ast_dot.php",
  "parsetreedot", "phc-test/framework/parse_tree_dot.php",
  "pass_dump", "phc-test/framework/lib/pass_dump.php",
  "phc_exit_code_annotation", "phc-test/framework/annotated_test.php",
  "phc_option_annotation", "phc-test/framework/annotated_test.php",
  "phc_output_annotation", "phc-test/framework/annotated_test.php",
  "plugintest", "phc-test/framework/lib/plugin_test.php",
  "reduce", "phc-test/framework/reduce/Reduce.php",
  "reduceexception", "phc-test/framework/reduce/Reduce.php",
  "refcounts", "phc-test/framework/refcounts.php",
  "regressiontest", "phc-test/framework/lib/regression.php",
  "reparseunparsed", "phc-test/framework/reparse_unparsed.php",
  "sourcevssemantictest", "phc-test/framework/source_vs_semantic_values.php",
  "supportfiletest", "phc-test/framework/lib/support_file_test.php",
  "test", "phc-test/framework/lib/test.php",
  "test_annotation", "phc-test/framework/annotated_test.php",
  "xml_roundtrip", "phc-test/framework/xml_roundtrip.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "adjusted_name", "phc-test/framework/lib/header.php",
  "blue_string", "phc-test/framework/lib/header.php",
  "check_for_plugin", "phc-test/framework/lib/header.php",
  "check_for_program", "phc-test/framework/lib/header.php",
  "close_status_files", "phc-test/framework/lib/header.php",
  "complete_exec", "phc-test/framework/lib/header.php",
  "copy_to_working_dir", "phc-test/framework/lib/header.php",
  "create_label_struct", "phc-test/framework/lib/labels.php",
  "date_string", "phc-test/framework/lib/header.php",
  "diff", "phc-test/framework/lib/header.php",
  "get_all_plugins", "phc-test/framework/lib/labels.php",
  "get_all_scripts", "phc-test/framework/lib/labels.php",
  "get_all_scripts_in_dir", "phc-test/framework/lib/labels.php",
  "get_available_annotations", "phc-test/framework/annotated_test.php",
  "get_dependent_filename", "phc-test/framework/lib/header.php",
  "get_interpretable_scripts", "phc-test/framework/lib/labels.php",
  "get_non_interpretable_scripts", "phc-test/framework/lib/labels.php",
  "get_pass_list", "phc-test/framework/lib/compare_backwards.php",
  "get_phc", "phc-test/framework/lib/header.php",
  "get_phc_command_line", "phc-test/framework/lib/header.php",
  "get_phc_compile_plugin", "phc-test/framework/lib/header.php",
  "get_php", "phc-test/framework/lib/header.php",
  "get_php_command_line", "phc-test/framework/lib/header.php",
  "get_scripts_labelled", "phc-test/framework/lib/labels.php",
  "green_string", "phc-test/framework/lib/header.php",
  "homogenize_all", "phc-test/framework/lib/header.php",
  "homogenize_break_levels", "phc-test/framework/lib/header.php",
  "homogenize_filenames_and_line_numbers", "phc-test/framework/lib/header.php",
  "homogenize_object_count", "phc-test/framework/lib/header.php",
  "homogenize_reference_count", "phc-test/framework/lib/header.php",
  "homogenize_xml", "phc-test/framework/lib/header.php",
  "inst", "phc-test/framework/lib/async_test.php",
  "is_32_bit", "phc-test/framework/lib/header.php",
  "is_labelled", "phc-test/framework/lib/labels.php",
  "kill_properly", "phc-test/framework/lib/header.php",
  "log_failure", "phc-test/framework/lib/header.php",
  "log_status", "phc-test/framework/lib/header.php",
  "open_status_files", "phc-test/framework/lib/header.php",
  "parse_options", "phc-test/framework/annotated_test.php",
  "phc_assert", "phc-test/framework/lib/header.php",
  "phc_error_handler", "phc-test/framework/lib/header.php",
  "phc_unreachable", "phc-test/framework/lib/header.php",
  "process_label_file_line", "phc-test/framework/lib/labels.php",
  "read_dependency", "phc-test/framework/lib/header.php",
  "red_string", "phc-test/framework/lib/header.php",
  "reset_string", "phc-test/framework/lib/header.php",
  "skip_3rdparty", "phc-test/framework/lib/labels.php",
  "strip_colour", "phc-test/framework/lib/header.php",
  "strip_console_codes", "phc-test/framework/lib/header.php",
  "strip_long_files", "phc-test/framework/lib/labels.php",
  "wd_name", "phc-test/framework/lib/header.php",
  "write_dependencies", "phc-test/framework/lib/header.php",
  "write_status", "phc-test/framework/lib/header.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
