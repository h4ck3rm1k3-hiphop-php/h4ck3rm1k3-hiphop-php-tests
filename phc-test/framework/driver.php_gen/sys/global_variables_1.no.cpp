
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 255) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      HASH_RETURN(0x6D72CEB44AC01301LL, g->GV(test_name),
                  test_name);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 7:
      HASH_RETURN(0x4E80F79695D41A07LL, g->GV(opt),
                  opt);
      break;
    case 12:
      HASH_RETURN(0x21DEE4EB204AE10CLL, g->GV(cg),
                  cg);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      HASH_RETURN(0x2F2AA415EFAE460ELL, g->GV(base_dir),
                  base_dir);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x3A1DC49AA6CFEA13LL, g->GV(test),
                  test);
      break;
    case 22:
      HASH_RETURN(0x73FDA0B4AF645216LL, g->GV(non_default_labels),
                  non_default_labels);
      break;
    case 26:
      HASH_RETURN(0x17B5D58607FC7F1ALL, g->GV(default_labels),
                  default_labels);
      break;
    case 27:
      HASH_RETURN(0x2FF01C2058B0B21BLL, g->GV(opt_verbose),
                  opt_verbose);
      HASH_RETURN(0x027BD19E0751121BLL, g->GV(opt_numbered),
                  opt_numbered);
      break;
    case 34:
      HASH_RETURN(0x7359B522C6BE3A22LL, g->GV(phc_suffix),
                  phc_suffix);
      break;
    case 36:
      HASH_RETURN(0x2F475D4DE7208D24LL, g->GV(opt_valgrind),
                  opt_valgrind);
      break;
    case 38:
      HASH_RETURN(0x1DA88EB5034B9D26LL, g->GV(plugin_dir),
                  plugin_dir);
      break;
    case 39:
      HASH_RETURN(0x3F4289BA72D62D27LL, g->GV(opt_installed),
                  opt_installed);
      break;
    case 43:
      HASH_RETURN(0x2128D3CAEC57A82BLL, g->GV(opt_support),
                  opt_support);
      break;
    case 46:
      HASH_RETURN(0x7DDBEB38F40F832ELL, g->GV(labels),
                  labels);
      break;
    case 48:
      HASH_RETURN(0x5C390FD428B50E30LL, g->GV(opt_long),
                  opt_long);
      break;
    case 52:
      HASH_RETURN(0x421780730E09F634LL, g->GV(opts),
                  opts);
      break;
    case 54:
      HASH_RETURN(0x19D6108580CBB436LL, g->GV(label_struct),
                  label_struct);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 64:
      HASH_RETURN(0x30704984F0078F40LL, g->GV(match),
                  match);
      break;
    case 65:
      HASH_RETURN(0x52B182B0A94E9C41LL, g->GV(php),
                  php);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x58FBD12F28C4BF49LL, g->GV(label_file),
                  label_file);
      break;
    case 75:
      HASH_RETURN(0x1018D4B85360194BLL, g->GV(opt_help),
                  opt_help);
      break;
    case 78:
      HASH_RETURN(0x0510D59D8A97894ELL, g->GV(opt_debug),
                  opt_debug);
      break;
    case 95:
      HASH_RETURN(0x11FFDF37C0EB2C5FLL, g->GV(tests),
                  tests);
      break;
    case 96:
      HASH_RETURN(0x4B2063F3C49B8E60LL, g->GV(exceptions),
                  exceptions);
      break;
    case 102:
      HASH_RETURN(0x0DC38D7049391A66LL, g->GV(trunk_CPPFLAGS),
                  trunk_CPPFLAGS);
      break;
    case 106:
      HASH_RETURN(0x67EA86E1CF55026ALL, g->GV(phc),
                  phc);
      break;
    case 107:
      HASH_RETURN(0x210CB79045B0086BLL, g->GV(bindir),
                  bindir);
      break;
    case 123:
      HASH_RETURN(0x630DB2468196C37BLL, g->GV(gcc),
                  gcc);
      break;
    case 126:
      HASH_RETURN(0x287DDAB321A4FF7ELL, g->GV(support_dir),
                  support_dir);
      break;
    case 130:
      HASH_RETURN(0x47A8BEF0E404C882LL, g->GV(opposite_label),
                  opposite_label);
      break;
    case 144:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 147:
      HASH_RETURN(0x01F18996FC6E6193LL, g->GV(php_exe),
                  php_exe);
      break;
    case 151:
      HASH_RETURN(0x65CC93B55175C597LL, g->GV(phc_compile_plugin),
                  phc_compile_plugin);
      break;
    case 156:
      HASH_RETURN(0x3901F738F39EE99CLL, g->GV(graphviz_gc),
                  graphviz_gc);
      break;
    case 157:
      HASH_RETURN(0x6890E1EA533ADF9DLL, g->GV(getopt),
                  getopt);
      break;
    case 163:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 182:
      HASH_RETURN(0x3725D0B1F30F14B6LL, g->GV(xerces_compiled_in),
                  xerces_compiled_in);
      break;
    case 183:
      HASH_RETURN(0x4FFE4179D4CE15B7LL, g->GV(opt_quick),
                  opt_quick);
      break;
    case 190:
      HASH_RETURN(0x18532A5EE8396FBELL, g->GV(date_string),
                  date_string);
      break;
    case 198:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 206:
      HASH_RETURN(0x464FC5785F58C2CELL, g->GV(pwd),
                  pwd);
      break;
    case 207:
      HASH_RETURN(0x522821F5063592CFLL, g->GV(dir),
                  dir);
      HASH_RETURN(0x2963E59544F2D0CFLL, g->GV(regex),
                  regex);
      break;
    case 209:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      HASH_RETURN(0x6C2E2CD6695D83D1LL, g->GV(log_directory),
                  log_directory);
      break;
    case 218:
      HASH_RETURN(0x35A19C689312B1DALL, g->GV(opt_no_progress_bar),
                  opt_no_progress_bar);
      break;
    case 220:
      HASH_RETURN(0x0F60C6595ED9FADCLL, g->GV(arguments),
                  arguments);
      break;
    case 223:
      HASH_RETURN(0x7076E3AEF02E9CDFLL, g->GV(opt_one),
                  opt_one);
      break;
    case 224:
      HASH_RETURN(0x5522393FED31B4E0LL, g->GV(pkglibdir),
                  pkglibdir);
      break;
    case 227:
      HASH_RETURN(0x614447AB21C8FAE3LL, g->GV(valgrind),
                  valgrind);
      break;
    case 229:
      HASH_RETURN(0x093CA964B9E038E5LL, g->GV(working_directory),
                  working_directory);
      break;
    case 233:
      HASH_RETURN(0x38BA10F6BA1F67E9LL, g->GV(working_dir),
                  working_dir);
      break;
    case 234:
      HASH_RETURN(0x7C17922060DCA1EALL, g->GV(options),
                  options);
      break;
    case 238:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 240:
      HASH_RETURN(0x7FD127A282D4A0F0LL, g->GV(strict),
                  strict);
      HASH_RETURN(0x300C2818152F9FF0LL, g->GV(opt_reduce),
                  opt_reduce);
      break;
    case 241:
      HASH_RETURN(0x070DFE5B93A7CAF1LL, g->GV(opt_clean),
                  opt_clean);
      break;
    case 243:
      HASH_RETURN(0x6FB2F3D573DE52F3LL, g->GV(subject_dir),
                  subject_dir);
      break;
    case 247:
      HASH_RETURN(0x7D6AD2C5948BA3F7LL, g->GV(third_party_label_file),
                  third_party_label_file);
      HASH_RETURN(0x626A556D885E55F7LL, g->GV(status_files),
                  status_files);
      break;
    case 253:
      HASH_RETURN(0x0B5BC0452CFBA5FDLL, g->GV(libphp),
                  libphp);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
