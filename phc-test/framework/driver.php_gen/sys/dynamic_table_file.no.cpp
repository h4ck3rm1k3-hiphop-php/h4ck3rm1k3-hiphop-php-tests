
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$framework$driver_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$header_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$startup_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$reduce$Reduce_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$compare_with_php_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$plugin_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$regression_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$compare_backwards_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$pass_dump_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$annotated_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$basic_parse_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$no_whitespace_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$generate_c_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$compiled_vs_interpreted_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$compile_optimized_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$refcounts_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$demi_eval_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$reparse_unparsed_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$source_vs_semantic_values_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$xml_roundtrip_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$compile_plugin_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$line_numbers_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$parse_ast_dot_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$parse_tree_dot_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$async_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$support_file_test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$test_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$lib$labels_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_INCLUDE(0x17E6E84604C8FE80LL, "phc-test/framework/lib/startup.php", php$phc_test$framework$lib$startup_php);
      HASH_INCLUDE(0x036FB1E49E59A2C0LL, "phc-test/framework/annotated_test.php", php$phc_test$framework$annotated_test_php);
      break;
    case 2:
      HASH_INCLUDE(0x5B77137BDB402582LL, "phc-test/framework/no_whitespace.php", php$phc_test$framework$no_whitespace_php);
      HASH_INCLUDE(0x4FEB3550FDA6EA42LL, "phc-test/framework/lib/support_file_test.php", php$phc_test$framework$lib$support_file_test_php);
      break;
    case 3:
      HASH_INCLUDE(0x22137A63AB09E743LL, "phc-test/framework/compiled_vs_interpreted.php", php$phc_test$framework$compiled_vs_interpreted_php);
      HASH_INCLUDE(0x440E148F70AD0683LL, "phc-test/framework/parse_tree_dot.php", php$phc_test$framework$parse_tree_dot_php);
      break;
    case 10:
      HASH_INCLUDE(0x1B54C4EAD263D60ALL, "phc-test/framework/lib/regression.php", php$phc_test$framework$lib$regression_php);
      break;
    case 11:
      HASH_INCLUDE(0x00DAA42E68F909CBLL, "phc-test/framework/source_vs_semantic_values.php", php$phc_test$framework$source_vs_semantic_values_php);
      break;
    case 17:
      HASH_INCLUDE(0x01744221AB167FD1LL, "phc-test/framework/lib/pass_dump.php", php$phc_test$framework$lib$pass_dump_php);
      break;
    case 21:
      HASH_INCLUDE(0x78CC0178C8F93795LL, "phc-test/framework/compile_optimized.php", php$phc_test$framework$compile_optimized_php);
      HASH_INCLUDE(0x3177FA7D451CB955LL, "phc-test/framework/lib/labels.php", php$phc_test$framework$lib$labels_php);
      break;
    case 22:
      HASH_INCLUDE(0x78CA932A7740D696LL, "phc-test/framework/lib/compare_backwards.php", php$phc_test$framework$lib$compare_backwards_php);
      HASH_INCLUDE(0x04FF0449F156D8D6LL, "phc-test/framework/line_numbers.php", php$phc_test$framework$line_numbers_php);
      break;
    case 23:
      HASH_INCLUDE(0x2DE8FFAA7EBDA417LL, "phc-test/framework/parse_ast_dot.php", php$phc_test$framework$parse_ast_dot_php);
      break;
    case 27:
      HASH_INCLUDE(0x74E0891BC701915BLL, "phc-test/framework/lib/plugin_test.php", php$phc_test$framework$lib$plugin_test_php);
      HASH_INCLUDE(0x1D0EF2E7E81D201BLL, "phc-test/framework/generate_c.php", php$phc_test$framework$generate_c_php);
      break;
    case 30:
      HASH_INCLUDE(0x47263E09D7A7085ELL, "phc-test/framework/compile_plugin_test.php", php$phc_test$framework$compile_plugin_test_php);
      break;
    case 31:
      HASH_INCLUDE(0x1000193AD5AC499FLL, "phc-test/framework/driver.php", php$phc_test$framework$driver_php);
      break;
    case 37:
      HASH_INCLUDE(0x506F6EF899248325LL, "phc-test/framework/refcounts.php", php$phc_test$framework$refcounts_php);
      break;
    case 38:
      HASH_INCLUDE(0x47E75A9BCBC8A666LL, "phc-test/framework/lib/compare_with_php_test.php", php$phc_test$framework$lib$compare_with_php_test_php);
      break;
    case 42:
      HASH_INCLUDE(0x45F1410E13A3032ALL, "phc-test/framework/reparse_unparsed.php", php$phc_test$framework$reparse_unparsed_php);
      break;
    case 48:
      HASH_INCLUDE(0x1D0D94EDF20078B0LL, "phc-test/framework/reduce/Reduce.php", php$phc_test$framework$reduce$Reduce_php);
      break;
    case 50:
      HASH_INCLUDE(0x318E90A414672272LL, "phc-test/framework/lib/async_test.php", php$phc_test$framework$lib$async_test_php);
      HASH_INCLUDE(0x2EA53DA2FCFB60B2LL, "phc-test/framework/lib/test.php", php$phc_test$framework$lib$test_php);
      break;
    case 53:
      HASH_INCLUDE(0x6C205F1D832059F5LL, "phc-test/framework/xml_roundtrip.php", php$phc_test$framework$xml_roundtrip_php);
      break;
    case 56:
      HASH_INCLUDE(0x55732CDF042CEEF8LL, "phc-test/framework/demi_eval.php", php$phc_test$framework$demi_eval_php);
      break;
    case 60:
      HASH_INCLUDE(0x40422106B0FF71FCLL, "phc-test/framework/lib/header.php", php$phc_test$framework$lib$header_php);
      break;
    case 61:
      HASH_INCLUDE(0x7C3E3F44EB8F077DLL, "phc-test/framework/basic_parse_test.php", php$phc_test$framework$basic_parse_test_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
