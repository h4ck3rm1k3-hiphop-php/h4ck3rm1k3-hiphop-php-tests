
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(subject_dir)
    GVS(label_file)
    GVS(third_party_label_file)
    GVS(default_labels)
    GVS(non_default_labels)
    GVS(labels)
    GVS(opposite_label)
    GVS(exceptions)
    GVS(label_struct)
    GVS(opt_long)
    GVS(opt_one)
    GVS(phc)
    GVS(plugin_dir)
    GVS(base_dir)
    GVS(tests)
    GVS(phc_compile_plugin)
    GVS(working_directory)
    GVS(trunk_CPPFLAGS)
    GVS(xerces_compiled_in)
    GVS(opt_debug)
    GVS(graphviz_gc)
    GVS(opt_valgrind)
    GVS(phc_suffix)
    GVS(valgrind)
    GVS(libphp)
    GVS(working_dir)
    GVS(php_exe)
    GVS(php)
    GVS(strict)
    GVS(status_files)
    GVS(log_directory)
    GVS(opt_verbose)
    GVS(date_string)
    GVS(gcc)
    GVS(cg)
    GVS(getopt)
    GVS(opts)
    GVS(arguments)
    GVS(opt)
    GVS(options)
    GVS(opt_support)
    GVS(opt_numbered)
    GVS(opt_help)
    GVS(opt_no_progress_bar)
    GVS(opt_installed)
    GVS(opt_clean)
    GVS(opt_quick)
    GVS(opt_reduce)
    GVS(support_dir)
    GVS(bindir)
    GVS(pkglibdir)
    GVS(pwd)
    GVS(dir)
    GVS(test)
    GVS(test_name)
    GVS(match)
    GVS(regex)
  END_GVS(57)

  // Dynamic Constants
  Variant k_PHC_NUM_PROCS;

  // Function/Method Static Variables
  Variant sv_reduce_DupIdwarn_once_DupIdcache;
  Variant sv_get_pass_list_DupIdcache;

  // Function/Method Static Variable Initialization Booleans
  Variant inited_sv_reduce_DupIdwarn_once_DupIdcache;
  bool inited_sv_get_pass_list_DupIdcache;

  // Class Static Variables
  Array s_comparewithphp_DupIdcache;

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$framework$driver_php;
  bool run_pm_php$phc_test$framework$lib$header_php;
  bool run_pm_php$phc_test$framework$lib$startup_php;
  bool run_pm_php$phc_test$framework$reduce$Reduce_php;
  bool run_pm_php$phc_test$framework$lib$compare_with_php_test_php;
  bool run_pm_php$phc_test$framework$lib$plugin_test_php;
  bool run_pm_php$phc_test$framework$lib$regression_php;
  bool run_pm_php$phc_test$framework$lib$compare_backwards_php;
  bool run_pm_php$phc_test$framework$lib$pass_dump_php;
  bool run_pm_php$phc_test$framework$annotated_test_php;
  bool run_pm_php$phc_test$framework$basic_parse_test_php;
  bool run_pm_php$phc_test$framework$no_whitespace_php;
  bool run_pm_php$phc_test$framework$generate_c_php;
  bool run_pm_php$phc_test$framework$compiled_vs_interpreted_php;
  bool run_pm_php$phc_test$framework$compile_optimized_php;
  bool run_pm_php$phc_test$framework$refcounts_php;
  bool run_pm_php$phc_test$framework$demi_eval_php;
  bool run_pm_php$phc_test$framework$reparse_unparsed_php;
  bool run_pm_php$phc_test$framework$source_vs_semantic_values_php;
  bool run_pm_php$phc_test$framework$xml_roundtrip_php;
  bool run_pm_php$phc_test$framework$compile_plugin_test_php;
  bool run_pm_php$phc_test$framework$line_numbers_php;
  bool run_pm_php$phc_test$framework$parse_ast_dot_php;
  bool run_pm_php$phc_test$framework$parse_tree_dot_php;
  bool run_pm_php$phc_test$framework$lib$async_test_php;
  bool run_pm_php$phc_test$framework$lib$support_file_test_php;
  bool run_pm_php$phc_test$framework$lib$test_php;
  bool run_pm_php$phc_test$framework$lib$labels_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 69;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[18];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
