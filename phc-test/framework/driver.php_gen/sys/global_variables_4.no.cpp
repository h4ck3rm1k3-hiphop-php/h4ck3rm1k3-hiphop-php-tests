
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "subject_dir",
    "label_file",
    "third_party_label_file",
    "default_labels",
    "non_default_labels",
    "labels",
    "opposite_label",
    "exceptions",
    "label_struct",
    "opt_long",
    "opt_one",
    "phc",
    "plugin_dir",
    "base_dir",
    "tests",
    "phc_compile_plugin",
    "working_directory",
    "trunk_CPPFLAGS",
    "xerces_compiled_in",
    "opt_debug",
    "graphviz_gc",
    "opt_valgrind",
    "phc_suffix",
    "valgrind",
    "libphp",
    "working_dir",
    "php_exe",
    "php",
    "strict",
    "status_files",
    "log_directory",
    "opt_verbose",
    "date_string",
    "gcc",
    "cg",
    "getopt",
    "opts",
    "arguments",
    "opt",
    "options",
    "opt_support",
    "opt_numbered",
    "opt_help",
    "opt_no_progress_bar",
    "opt_installed",
    "opt_clean",
    "opt_quick",
    "opt_reduce",
    "support_dir",
    "bindir",
    "pkglibdir",
    "pwd",
    "dir",
    "test",
    "test_name",
    "match",
    "regex",
  };
  if (idx >= 0 && idx < 69) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(subject_dir);
    case 13: return GV(label_file);
    case 14: return GV(third_party_label_file);
    case 15: return GV(default_labels);
    case 16: return GV(non_default_labels);
    case 17: return GV(labels);
    case 18: return GV(opposite_label);
    case 19: return GV(exceptions);
    case 20: return GV(label_struct);
    case 21: return GV(opt_long);
    case 22: return GV(opt_one);
    case 23: return GV(phc);
    case 24: return GV(plugin_dir);
    case 25: return GV(base_dir);
    case 26: return GV(tests);
    case 27: return GV(phc_compile_plugin);
    case 28: return GV(working_directory);
    case 29: return GV(trunk_CPPFLAGS);
    case 30: return GV(xerces_compiled_in);
    case 31: return GV(opt_debug);
    case 32: return GV(graphviz_gc);
    case 33: return GV(opt_valgrind);
    case 34: return GV(phc_suffix);
    case 35: return GV(valgrind);
    case 36: return GV(libphp);
    case 37: return GV(working_dir);
    case 38: return GV(php_exe);
    case 39: return GV(php);
    case 40: return GV(strict);
    case 41: return GV(status_files);
    case 42: return GV(log_directory);
    case 43: return GV(opt_verbose);
    case 44: return GV(date_string);
    case 45: return GV(gcc);
    case 46: return GV(cg);
    case 47: return GV(getopt);
    case 48: return GV(opts);
    case 49: return GV(arguments);
    case 50: return GV(opt);
    case 51: return GV(options);
    case 52: return GV(opt_support);
    case 53: return GV(opt_numbered);
    case 54: return GV(opt_help);
    case 55: return GV(opt_no_progress_bar);
    case 56: return GV(opt_installed);
    case 57: return GV(opt_clean);
    case 58: return GV(opt_quick);
    case 59: return GV(opt_reduce);
    case 60: return GV(support_dir);
    case 61: return GV(bindir);
    case 62: return GV(pkglibdir);
    case 63: return GV(pwd);
    case 64: return GV(dir);
    case 65: return GV(test);
    case 66: return GV(test_name);
    case 67: return GV(match);
    case 68: return GV(regex);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
