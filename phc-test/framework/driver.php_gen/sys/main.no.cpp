
#include <cpp/base/hphp.h>
#include <cpp/base/array/zend_array.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void csi_comparewithphp();

IMPLEMENT_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
GlobalVariables::GlobalVariables() : dummy(false),
  inited_sv_reduce_DupIdwarn_once_DupIdcache(),
  inited_sv_get_pass_list_DupIdcache(false),
  run_pm_php$phc_test$framework$driver_php(false),
  run_pm_php$phc_test$framework$lib$header_php(false),
  run_pm_php$phc_test$framework$lib$startup_php(false),
  run_pm_php$phc_test$framework$reduce$Reduce_php(false),
  run_pm_php$phc_test$framework$lib$compare_with_php_test_php(false),
  run_pm_php$phc_test$framework$lib$plugin_test_php(false),
  run_pm_php$phc_test$framework$lib$regression_php(false),
  run_pm_php$phc_test$framework$lib$compare_backwards_php(false),
  run_pm_php$phc_test$framework$lib$pass_dump_php(false),
  run_pm_php$phc_test$framework$annotated_test_php(false),
  run_pm_php$phc_test$framework$basic_parse_test_php(false),
  run_pm_php$phc_test$framework$no_whitespace_php(false),
  run_pm_php$phc_test$framework$generate_c_php(false),
  run_pm_php$phc_test$framework$compiled_vs_interpreted_php(false),
  run_pm_php$phc_test$framework$compile_optimized_php(false),
  run_pm_php$phc_test$framework$refcounts_php(false),
  run_pm_php$phc_test$framework$demi_eval_php(false),
  run_pm_php$phc_test$framework$reparse_unparsed_php(false),
  run_pm_php$phc_test$framework$source_vs_semantic_values_php(false),
  run_pm_php$phc_test$framework$xml_roundtrip_php(false),
  run_pm_php$phc_test$framework$compile_plugin_test_php(false),
  run_pm_php$phc_test$framework$line_numbers_php(false),
  run_pm_php$phc_test$framework$parse_ast_dot_php(false),
  run_pm_php$phc_test$framework$parse_tree_dot_php(false),
  run_pm_php$phc_test$framework$lib$async_test_php(false),
  run_pm_php$phc_test$framework$lib$support_file_test_php(false),
  run_pm_php$phc_test$framework$lib$test_php(false),
  run_pm_php$phc_test$framework$lib$labels_php(false) {

  // Dynamic Constants
  k_PHC_NUM_PROCS = "PHC_NUM_PROCS";

  // Primitive Function/Method Static Variables

  // Primitive Class Static Variables

  // Redeclared Functions

  // Redeclared Classes
}

void GlobalVariables::initialize() {
  SystemGlobals::initialize();
  csi_comparewithphp();
}

void init_static_variables() { ScalarArrays::initialize();}
static ThreadLocalSingleton<GlobalVariables> g_variables;
static IMPLEMENT_THREAD_LOCAL(GlobalArrayWrapper, g_array_wrapper);
GlobalVariables *get_global_variables() {
  return g_variables.get();
}
void init_global_variables() { GlobalVariables::initialize();}
void free_global_variables() {
  g_variables.reset();
  g_array_wrapper.reset();
}
LVariableTable *get_variable_table() { return (LVariableTable*)get_global_variables();}
Globals *get_globals() { return (Globals*)get_global_variables();}
SystemGlobals *get_system_globals() { return (SystemGlobals*)get_global_variables();}
Array get_global_array_wrapper(){ return g_array_wrapper.get();}

///////////////////////////////////////////////////////////////////////////////
}

#ifndef HPHP_BUILD_LIBRARY
int main(int argc, char** argv) {
  return HPHP::execute_program(argc, argv);
}
#endif
