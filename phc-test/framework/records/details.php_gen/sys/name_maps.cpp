
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "add_difference", "phc-test/framework/records/common.php",
  "add_percentage_difference", "phc-test/framework/records/common.php",
  "date_from_timestamp", "phc-test/framework/records/common.php",
  "diff", "phc-test/framework/records/common.php",
  "get_bad_color", "phc-test/framework/records/common.php",
  "get_branch", "phc-test/framework/records/common.php",
  "get_good_color", "phc-test/framework/records/common.php",
  "get_prev_revision", "phc-test/framework/records/common.php",
  "get_running_color", "phc-test/framework/records/common.php",
  "maybe_link", "phc-test/framework/records/details.php",
  "minutes_from_seconds", "phc-test/framework/records/common.php",
  "order_by", "phc-test/framework/records/details.php",
  "print_benchmark_details", "phc-test/framework/records/details.php",
  "print_component_header", "phc-test/framework/records/details.php",
  "print_component_row", "phc-test/framework/records/details.php",
  "print_revision_details", "phc-test/framework/records/details.php",
  "print_test_details", "phc-test/framework/records/details.php",
  "run_main", "phc-test/framework/records/details.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
