
#ifndef __GENERATED_php_phc_test_framework_records_details_h__
#define __GENERATED_php_phc_test_framework_records_details_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/records/details.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_print_test_details(int64 v_rev, CVarRef v_old_rev);
void f_print_benchmark_details(int64 v_rev, CVarRef v_old_rev);
void f_print_revision_details(int64 v_rev, CVarRef v_old_rev, CVarRef v_old_bench_rev);
void f_print_component_header();
Variant f_maybe_link(int64 v_rev, CVarRef v_old_rev, CStrRef v_filename, CStrRef v_name, bool v_sort = false);
void f_run_main();
Variant pm_php$phc_test$framework$records$details_php(bool incOnce = false, LVariableTable* variables = NULL);
Array f_order_by(CVarRef v_data, CVarRef v_ordering, CStrRef v_row_index);
void f_print_component_row(CStrRef v_component, int64 v_rev, CVarRef v_old_rev);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_records_details_h__
