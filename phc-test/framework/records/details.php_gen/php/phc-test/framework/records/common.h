
#ifndef __GENERATED_php_phc_test_framework_records_common_h__
#define __GENERATED_php_phc_test_framework_records_common_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/records/common.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_get_running_color();
String f_get_bad_color();
Numeric f_add_percentage_difference(CStrRef v_name, Variant v_new, CVarRef v_old);
String f_get_good_color();
String f_date_from_timestamp(CVarRef v_timestamp);
Variant f_get_prev_revision(int64 v_rev, CVarRef v_has_benchmark_results = false);
Numeric f_add_difference(CStrRef v_name, Variant v_new, CVarRef v_old);
Variant f_diff(CVarRef v_string1, CVarRef v_string2);
String f_minutes_from_seconds(CVarRef v_seconds);
Variant pm_php$phc_test$framework$records$common_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_get_branch(int64 v_rev);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_records_common_h__
