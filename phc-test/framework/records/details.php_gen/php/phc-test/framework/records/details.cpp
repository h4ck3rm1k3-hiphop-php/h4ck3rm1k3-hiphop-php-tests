
#include <php/phc-test/framework/records/common.h>
#include <php/phc-test/framework/records/details.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/records/details.php line 188 */
void f_print_test_details(int64 v_rev, CVarRef v_old_rev) {
  FUNCTION_INJECTION(print_test_details);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_DB;
  Variant &gv_td __attribute__((__unused__)) = g->GV(td);
  Variant v_td;
  Variant &gv_th __attribute__((__unused__)) = g->GV(th);
  Variant v_th;
  Variant &gv_test_order __attribute__((__unused__)) = g->GV(test_order);
  Variant v_test_order;
  Variant v_test_data;
  Variant v_old_test_data;
  Variant v_row;
  Variant v_old_row;
  String v_color;
  Variant v_name;
  Primitive v_key = 0;
  Variant v_entry;

  {
    v_DB = ref(g->GV(DB));
    v_td = ref(g->GV(td));
    v_th = ref(g->GV(th));
    v_test_order = ref(g->GV(test_order));
  }
  (v_test_data = (assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, toObject(v_DB)),LINE(198,eo_1.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat3("\n\t\t\t\tSELECT\trevision, testname, pass, fail, timeout, skip\n\t\t\t\tFROM\t\ttests\n\t\t\t\tWHERE\t\trevision == ", toString(v_rev), "\n\t\t\t\t")))))),eo_0.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  print("<table class=info>");
  print(concat(concat_rev(toString(LINE(208,f_maybe_link(v_rev, v_old_rev, "test_logs/skipped", "Skips", true))), concat(concat_rev(toString(LINE(207,f_maybe_link(v_rev, v_old_rev, "test_logs/timeout", "Timeouts", true))), concat(concat_rev(toString(LINE(206,f_maybe_link(v_rev, v_old_rev, "test_logs/failure", "Fails", true))), (assignCallTemp(eo_3, LINE(205,concat5("<a href='details.php\?rev=", toString(v_old_rev), "'>", toString(v_old_rev), "</a>)</th><th>"))),assignCallTemp(eo_4, toString(f_maybe_link(v_rev, v_old_rev, "test_logs/success", "Passes", true))),concat4("<tr><th>Test Name (+/- vs ", eo_3, eo_4, "</th><th>"))), "</th><th>")), "</th><th>")), "</th></tr>\n"));
  (v_old_test_data = (assignCallTemp(eo_2, toObject((assignCallTemp(eo_3, toObject(v_DB)),LINE(216,eo_3.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat3("\n\t\t\t\tSELECT\trevision, testname, pass, fail, timeout, skip\n\t\t\t\tFROM\t\ttests\n\t\t\t\tWHERE\t\trevision == ", toString(v_old_rev), "\n\t\t\t\t")))))),eo_2.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(v_test_data);
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(NULL, v_row); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_old_row = LINE(221,x_array_shift(ref(v_old_test_data))));
        v_row.set("difference", (LINE(222,f_add_difference("pass", ref(v_row), v_old_row))), 0x6ADD28D83D2E1F6ELL);
        LINE(223,f_add_difference("fail", ref(v_row), v_old_row));
        LINE(224,f_add_difference("skip", ref(v_row), v_old_row));
        LINE(225,f_add_difference("timeout", ref(v_row), v_old_row));
      }
    }
  }
  unset(v_row);
  (v_test_data = LINE(229,f_order_by(v_test_data, v_test_order, "testname")));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_test_data.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_row = iter6->second();
      {
        (v_row = v_row.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
        v_row.weakRemove("revision", 0x62F087C6F59E7C3CLL);
        (v_color = "");
        if (more(v_row.rvalAt("difference", 0x6ADD28D83D2E1F6ELL), 0LL)) (v_color = LINE(240,f_get_good_color()));
        else if (less(v_row.rvalAt("difference", 0x6ADD28D83D2E1F6ELL), 0LL)) (v_color = LINE(242,f_get_bad_color()));
        v_row.weakRemove("difference", 0x6ADD28D83D2E1F6ELL);
        (v_name = v_row.rvalAt("testname", 0x7A7EB473C463EEA3LL));
        if (equal(v_name, "Total")) v_row.set("testname", (LINE(248,concat5("<a href=\"results/", toString(v_rev), "/test_logs/\">", toString(v_name), "</a>"))), 0x7A7EB473C463EEA3LL);
        else if (toBoolean(v_row.rvalAt("fail", 0x27B1F9AB2223F2ACLL))) v_row.set("testname", (concat("<a href=\"results/", LINE(250,concat6(toString(v_rev), "/test_logs/", toString(v_name), ".tar.gz\">", toString(v_name), "</a>")))), 0x7A7EB473C463EEA3LL);
        print("<tr>\n");
        {
          LOOP_COUNTER(7);
          for (ArrayIterPtr iter9 = v_row.begin(); !iter9->end(); iter9->next()) {
            LOOP_COUNTER_CHECK(7);
            v_entry = iter9->second();
            v_key = iter9->first();
            {
              print(LINE(255,concat5("<td ", v_color, ">", toString(v_entry), "</td>")));
            }
          }
        }
        print("</tr>\n");
      }
    }
  }
  print("</table>");
} /* function */
/* SRC: phc-test/framework/records/details.php line 262 */
void f_print_benchmark_details(int64 v_rev, CVarRef v_old_rev) {
  FUNCTION_INJECTION(print_benchmark_details);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_DB;
  Variant &gv_td __attribute__((__unused__)) = g->GV(td);
  Variant v_td;
  Variant &gv_th __attribute__((__unused__)) = g->GV(th);
  Variant v_th;
  Variant &gv_benchmark_order __attribute__((__unused__)) = g->GV(benchmark_order);
  Variant v_benchmark_order;
  Variant v_test_data;
  Variant v_old_test_data;
  Variant v_row;
  Variant v_old_row;
  Variant v_row_set;
  String v_color;
  Primitive v_key = 0;
  Variant v_entry;

  {
    v_DB = ref(g->GV(DB));
    v_td = ref(g->GV(td));
    v_th = ref(g->GV(th));
    v_benchmark_order = ref(g->GV(benchmark_order));
  }
  (v_test_data = (assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, toObject(v_DB)),LINE(272,eo_1.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat3("\n\t\t\t\tSELECT\trevision, name, metric, result\n\t\t\t\tFROM\t\tbenchmarks\n\t\t\t\tWHERE\t\trevision == ", toString(v_rev), "\n\t\t\t\t")))))),eo_0.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  print("<table class=info>");
  print(concat("<tr><th>Benchmark</th><th>Metric</th><th>Result (+/- vs ", LINE(280,concat5("<a href='details.php\?rev=", toString(v_old_rev), "'>", toString(v_old_rev), "</a>)</th>"))));
  (v_old_test_data = (assignCallTemp(eo_2, toObject((assignCallTemp(eo_3, toObject(v_DB)),LINE(288,eo_3.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat3("\n\t\t\t\tSELECT\trevision, name, metric, result\n\t\t\t\tFROM\t\tbenchmarks\n\t\t\t\tWHERE\t\trevision == ", toString(v_old_rev), "\n\t\t\t\t")))))),eo_2.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  {
    LOOP_COUNTER(10);
    Variant map11 = ref(v_test_data);
    map11.escalate();
    for (MutableArrayIterPtr iter12 = map11.begin(NULL, v_row); iter12->advance();) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_old_row = LINE(293,x_array_shift(ref(v_old_test_data))));
        v_row.set("difference", (LINE(294,f_add_percentage_difference("result", ref(v_row), v_old_row))), 0x6ADD28D83D2E1F6ELL);
      }
    }
  }
  unset(v_row);
  (v_test_data = LINE(298,f_order_by(v_test_data, v_benchmark_order, "metric")));
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_test_data.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_row_set = iter15->second();
      {
        {
          LOOP_COUNTER(16);
          for (ArrayIterPtr iter18 = v_row_set.begin(); !iter18->end(); iter18->next()) {
            LOOP_COUNTER_CHECK(16);
            v_row = iter18->second();
            {
              v_row.weakRemove("revision", 0x62F087C6F59E7C3CLL);
              (v_color = "");
              if (more(v_row.rvalAt("difference", 0x6ADD28D83D2E1F6ELL), 0LL)) (v_color = LINE(310,f_get_bad_color()));
              else if (less(v_row.rvalAt("difference", 0x6ADD28D83D2E1F6ELL), 0LL)) (v_color = LINE(312,f_get_good_color()));
              v_row.weakRemove("difference", 0x6ADD28D83D2E1F6ELL);
              print("<tr>\n");
              {
                LOOP_COUNTER(19);
                for (ArrayIterPtr iter21 = v_row.begin(); !iter21->end(); iter21->next()) {
                  LOOP_COUNTER_CHECK(19);
                  v_entry = iter21->second();
                  v_key = iter21->first();
                  {
                    print(LINE(318,concat5("<td ", v_color, ">", toString(v_entry), "</td>")));
                  }
                }
              }
              print("</tr>\n");
            }
          }
        }
      }
    }
  }
  print("</table>");
} /* function */
/* SRC: phc-test/framework/records/details.php line 96 */
void f_print_revision_details(int64 v_rev, CVarRef v_old_rev, CVarRef v_old_bench_rev) {
  FUNCTION_INJECTION(print_revision_details);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_complete_data;

  (v_complete_data = (assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, toObject(gv_DB)),LINE(104,eo_1.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat3("\n\t\t\t\tSELECT\t*\n\t\t\t\tFROM\t\tcomplete\n\t\t\t\tWHERE\t\trevision == ", toString(v_rev), " \n\t\t\t\t")))))),eo_0.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  if (!equal(LINE(106,x_count(v_complete_data)), 1LL)) f_exit(toString("Revision not available: ") + toString(v_rev));
  (v_complete_data = v_complete_data.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  print("<table class=info>");
  print("\t<tr>\n");
  print(LINE(113,(assignCallTemp(eo_3, toString(f_maybe_link(v_rev, v_old_rev, "log.log", "Log"))),concat3("\t\t<th>", eo_3, "</th>"))));
  print(LINE(114,concat5("\t\t<th>Revision:\t\t\t\t\t\t</th><td><a href=\"http://code.google.com/p/phc/source/detail\?r=", toString(v_rev), "\">", toString(v_complete_data.rvalAt("revision", 0x62F087C6F59E7C3CLL)), "</a></td>")));
  print(LINE(115,concat3("\t\t<th>Branch:\t\t\t\t\t\t\t</th><td>", toString(v_complete_data.rvalAt("branch", 0x5B071EF3F7DD559FLL)), "</td>")));
  print(LINE(116,concat3("\t\t<th>Author:\t\t\t\t\t\t\t</th><td>", toString(v_complete_data.rvalAt("author", 0x3EFB9E9D8850B7BCLL)), "</td>")));
  print(LINE(117,concat3("\t\t<th>Commit date:\t\t\t\t\t</th><td>", toString(v_complete_data.rvalAt("commit_date", 0x1FD3CDD8AD726525LL)), "\t</td>")));
  print("\t</tr>");
  print("</table>");
  print("<table class=info>");
  LINE(121,f_print_component_header());
  LINE(122,f_print_component_row("compile", v_rev, v_old_rev));
  LINE(123,f_print_component_row("test", v_rev, v_old_rev));
  LINE(124,f_print_component_row("benchmark", v_rev, v_old_rev));
  print("</table>");
} /* function */
/* SRC: phc-test/framework/records/details.php line 143 */
void f_print_component_header() {
  FUNCTION_INJECTION(print_component_header);
  print("<tr>\n");
  print("<th>Log</th>\n");
  print("<th>Time taken</th>\n");
  print("<th>Test date</th>\n");
  print("<th>Revision used</th>\n");
  print("<th>Failed</th>\n");
  print("<th>Redo</th>\n");
  print("</tr>\n");
} /* function */
/* SRC: phc-test/framework/records/details.php line 128 */
Variant f_maybe_link(int64 v_rev, CVarRef v_old_rev, CStrRef v_filename, CStrRef v_name, bool v_sort //  = false
) {
  FUNCTION_INJECTION(maybe_link);
  String v_file;
  String v_old_file;

  (v_file = LINE(130,concat4("results/", toString(v_rev), "/", v_filename)));
  if (LINE(131,x_file_exists(v_file))) {
    (v_old_file = LINE(133,concat4("results/", toString(v_old_rev), "/", v_filename)));
    if (LINE(134,x_file_exists(v_old_file))) return concat_rev(LINE(135,concat6(toString(v_old_rev), "&sort=", toString(v_sort), "&filename=", v_filename, "\">D</a>)")), concat("<a href=\"", concat6(v_file, "\">", v_name, "</a> (<a href=\"diff.php\?new_rev=", toString(v_rev), "&old_rev=")));
    else return LINE(137,concat5("<a href=\"", v_file, "\">", v_name, "</a>"));
  }
  else return v_name;
  return null;
} /* function */
/* SRC: phc-test/framework/records/details.php line 69 */
void f_run_main() {
  FUNCTION_INJECTION(run_main);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 v_rev = 0;
  Variant v_old_rev;
  Variant v_old_bench_rev;

  (v_rev = toInt64(g->gv__GET.rvalAt("rev", 0x22651263E5C98440LL)));
  if (not_more(v_rev, 0LL)) f_exit(LINE(74,concat3("Invalid revision: ", toString(g->gv__GET.rvalAt("rev", 0x22651263E5C98440LL)), ".")));
  (v_old_rev = LINE(76,f_get_prev_revision(v_rev)));
  (v_old_bench_rev = LINE(77,f_get_prev_revision(v_rev, "bench")));
  print("<table class=layout>");
  print("\t<tr><td colspan=2>\n");
  LINE(81,f_print_revision_details(v_rev, v_old_rev, v_old_bench_rev));
  print("\t</td></tr>\n");
  print("\t<tr><td>\n");
  print("\t\t<table class=layout>");
  print("\t\t<tr><td valign=top>\n");
  LINE(86,f_print_test_details(v_rev, v_old_rev));
  print("\t\t</td><td valign=top>\n");
  LINE(88,f_print_benchmark_details(v_rev, v_old_bench_rev));
  print("\t\t</td></tr>\n");
  print("\t\t</table>");
  print("\t</td></tr>\n");
  print("</table>");
} /* function */
/* SRC: phc-test/framework/records/details.php line 331 */
Array f_order_by(CVarRef v_data, CVarRef v_ordering, CStrRef v_row_index) {
  FUNCTION_INJECTION(order_by);
  DECLARE_GLOBAL_VARIABLES(g);
  Array v_result;
  Variant v_indexed;
  Variant v_row;
  Variant v_name;

  (v_result = ScalarArrays::sa_[2]);
  (v_indexed = ScalarArrays::sa_[2]);
  {
    LOOP_COUNTER(22);
    for (ArrayIterPtr iter24 = v_data.begin(); !iter24->end(); iter24->next()) {
      LOOP_COUNTER_CHECK(22);
      v_row = iter24->second();
      {
        lval(v_indexed.lvalAt(v_row.rvalAt(v_row_index))).append((v_row));
      }
    }
  }
  {
    LOOP_COUNTER(25);
    for (ArrayIterPtr iter27 = v_ordering.begin(); !iter27->end(); iter27->next()) {
      LOOP_COUNTER_CHECK(25);
      v_name = iter27->second();
      {
        if (isset(v_indexed, v_name)) {
          v_result.append((v_indexed.rvalAt(v_name)));
          v_indexed.weakRemove(v_name);
        }
      }
    }
  }
  {
    LOOP_COUNTER(28);
    for (ArrayIterPtr iter30 = v_indexed.begin(); !iter30->end(); iter30->next()) {
      LOOP_COUNTER_CHECK(28);
      v_row = iter30->second();
      v_result.append((v_row));
    }
  }
  return v_result;
} /* function */
/* SRC: phc-test/framework/records/details.php line 156 */
void f_print_component_row(CStrRef v_component, int64 v_rev, CVarRef v_old_rev) {
  FUNCTION_INJECTION(print_component_row);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  String v_uc_name;
  String v_log_title;
  String v_log_file;
  Variant v_data;
  Primitive v_key = 0;
  Variant v_value;

  (v_uc_name = LINE(159,x_ucfirst(v_component)));
  (v_log_title = v_uc_name);
  (v_log_file = v_component + toString(".log"));
  print("<tr>");
  print(LINE(164,(assignCallTemp(eo_1, toString(f_maybe_link(v_rev, v_old_rev, v_log_file, v_log_title))),concat3("<td>", eo_1, "</td>"))));
  (v_data = (assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, toObject(gv_DB)),LINE(171,eo_1.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat5("\n\t\t\t\tSELECT\ttime_taken, test_date, revision_used, failed, redo\n\t\t\t\tFROM\t\tcomponents\n\t\t\t\tWHERE\t\trevision == ", toString(v_rev), "\n\t\t\t\tAND\t\tcomponent == '", v_component, "'\n\t\t\t\t")))))),eo_0.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  if (isset(v_data, 0LL, 0x77CFA1EEF01BCA90LL)) {
    LOOP_COUNTER(31);
    Variant map32 = v_data.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
    for (ArrayIterPtr iter33 = map32.begin(); !iter33->end(); iter33->next()) {
      LOOP_COUNTER_CHECK(31);
      v_value = iter33->second();
      v_key = iter33->first();
      {
        if (equal(v_key, "test_date")) (v_value = LINE(177,f_date_from_timestamp(v_value)));
        else if (equal(v_key, "time_taken")) (v_value = LINE(179,f_minutes_from_seconds(v_value)));
        print(LINE(181,concat3("<td>", toString(v_value), "</td>")));
      }
    }
  }
  print("\n</tr>");
} /* function */
Variant pm_php$phc_test$framework$records$details_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/records/details.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$records$details_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test_order __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_order") : g->GV(test_order);
  Variant &v_benchmark_order __attribute__((__unused__)) = (variables != gVariables) ? variables->get("benchmark_order") : g->GV(benchmark_order);

  (v_test_order = ScalarArrays::sa_[0]);
  (v_benchmark_order = ScalarArrays::sa_[1]);
  LINE(67,pm_php$phc_test$framework$records$common_php(false, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
