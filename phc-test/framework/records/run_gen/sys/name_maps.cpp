
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "benchmark_component", "phc-test/framework/records/run",
  "compile_component", "phc-test/framework/records/run",
  "component", "phc-test/framework/records/run",
  "test_component", "phc-test/framework/records/run",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "c", "phc-test/framework/records/run",
  "cd", "phc-test/framework/records/run",
  "create_dir", "phc-test/framework/records/run",
  "del_dir", "phc-test/framework/records/run",
  "e", "phc-test/framework/records/run",
  "end_log", "phc-test/framework/records/run",
  "get_latest_revision", "phc-test/framework/records/run",
  "get_svn_branch", "phc-test/framework/records/run",
  "get_svn_info", "phc-test/framework/records/run",
  "get_test_revision", "phc-test/framework/records/run",
  "initialize_db", "phc-test/framework/records/run",
  "initialize_test", "phc-test/framework/records/run",
  "q", "phc-test/framework/records/run",
  "record_revision_data", "phc-test/framework/records/run",
  "save", "phc-test/framework/records/run",
  "save_log", "phc-test/framework/records/run",
  "script_problem", "phc-test/framework/records/run",
  "skip_revision", "phc-test/framework/records/run",
  "start_log", "phc-test/framework/records/run",
  "svn_problem", "phc-test/framework/records/run",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
