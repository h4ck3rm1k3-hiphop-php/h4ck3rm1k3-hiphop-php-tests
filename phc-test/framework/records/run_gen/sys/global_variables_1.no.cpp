
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x3F2B9FFED81C6A49LL, g->GV(WORKING_DIR),
                  WORKING_DIR);
      break;
    case 11:
      HASH_RETURN(0x7F9AFD13AA2B284BLL, g->GV(full_log),
                  full_log);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 15:
      HASH_RETURN(0x192B21B6D5578B4FLL, g->GV(DB),
                  DB);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 21:
      HASH_RETURN(0x3299B3C419300E15LL, g->GV(RESULTS_DIR),
                  RESULTS_DIR);
      HASH_RETURN(0x42C1B3083A082995LL, g->GV(START_TIME),
                  START_TIME);
      HASH_RETURN(0x5A4EF3F899897015LL, g->GV(REV),
                  REV);
      break;
    case 23:
      HASH_RETURN(0x7C0667E542FB3257LL, g->GV(REPO),
                  REPO);
      HASH_RETURN(0x2FF0FE0727D6A297LL, g->GV(DB_FILENAME),
                  DB_FILENAME);
      break;
    case 29:
      HASH_RETURN(0x069C028EFEF8C91DLL, g->GV(DATE),
                  DATE);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 37:
      HASH_RETURN(0x3CEB4AC2D7E1C665LL, g->GV(BRANCH),
                  BRANCH);
      break;
    case 38:
      HASH_RETURN(0x0657F839DD8CF4A6LL, g->GV(AUTHOR),
                  AUTHOR);
      break;
    case 39:
      HASH_RETURN(0x76A5CC5A15A4A4A7LL, g->GV(allowed_branches),
                  allowed_branches);
      break;
    case 42:
      HASH_RETURN(0x45857634D3A1466ALL, g->GV(comp),
                  comp);
      break;
    case 43:
      HASH_RETURN(0x61B161496B7EA7EBLL, g->GV(e),
                  e);
      break;
    case 44:
      HASH_RETURN(0x50EC965386450D6CLL, g->GV(CWD),
                  CWD);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 50:
      HASH_RETURN(0x52ED807A13699D72LL, g->GV(components),
                  components);
      break;
    case 58:
      HASH_RETURN(0x58590574DB53D17ALL, g->GV(overall),
                  overall);
      break;
    case 62:
      HASH_RETURN(0x41343BD8CE76873ELL, g->GV(LOG_DIR),
                  LOG_DIR);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      HASH_RETURN(0x5AB8EAD31709E8BFLL, g->GV(LATEST_REVISION),
                  LATEST_REVISION);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
