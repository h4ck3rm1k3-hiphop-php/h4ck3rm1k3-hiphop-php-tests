
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_test_component(CArrRef params, bool init = true);
Variant cw_test_component$os_get(const char *s);
Variant &cw_test_component$os_lval(const char *s);
Variant cw_test_component$os_constant(const char *s);
Variant cw_test_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_compile_component(CArrRef params, bool init = true);
Variant cw_compile_component$os_get(const char *s);
Variant &cw_compile_component$os_lval(const char *s);
Variant cw_compile_component$os_constant(const char *s);
Variant cw_compile_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_component(CArrRef params, bool init = true);
Variant cw_component$os_get(const char *s);
Variant &cw_component$os_lval(const char *s);
Variant cw_component$os_constant(const char *s);
Variant cw_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_benchmark_component(CArrRef params, bool init = true);
Variant cw_benchmark_component$os_get(const char *s);
Variant &cw_benchmark_component$os_lval(const char *s);
Variant cw_benchmark_component$os_constant(const char *s);
Variant cw_benchmark_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_CREATE_OBJECT(0x61C833CF1E72EAB8LL, test_component);
      break;
    case 1:
      HASH_CREATE_OBJECT(0x69C465D453F04471LL, benchmark_component);
      break;
    case 3:
      HASH_CREATE_OBJECT(0x1614A8DB01802A3BLL, compile_component);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x1D5B95CD4F94A3D6LL, component);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x61C833CF1E72EAB8LL, test_component);
      break;
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x69C465D453F04471LL, benchmark_component);
      break;
    case 3:
      HASH_INVOKE_STATIC_METHOD(0x1614A8DB01802A3BLL, compile_component);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x1D5B95CD4F94A3D6LL, component);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x61C833CF1E72EAB8LL, test_component);
      break;
    case 1:
      HASH_GET_STATIC_PROPERTY(0x69C465D453F04471LL, benchmark_component);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY(0x1614A8DB01802A3BLL, compile_component);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x1D5B95CD4F94A3D6LL, component);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x61C833CF1E72EAB8LL, test_component);
      break;
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x69C465D453F04471LL, benchmark_component);
      break;
    case 3:
      HASH_GET_STATIC_PROPERTY_LV(0x1614A8DB01802A3BLL, compile_component);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x1D5B95CD4F94A3D6LL, component);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x61C833CF1E72EAB8LL, test_component);
      break;
    case 1:
      HASH_GET_CLASS_CONSTANT(0x69C465D453F04471LL, benchmark_component);
      break;
    case 3:
      HASH_GET_CLASS_CONSTANT(0x1614A8DB01802A3BLL, compile_component);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x1D5B95CD4F94A3D6LL, component);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
