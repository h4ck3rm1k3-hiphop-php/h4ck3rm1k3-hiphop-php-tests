
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(full_log)
    GVS(REPO)
    GVS(CWD)
    GVS(WORKING_DIR)
    GVS(RESULTS_DIR)
    GVS(DB_FILENAME)
    GVS(allowed_branches)
    GVS(START_TIME)
    GVS(LATEST_REVISION)
    GVS(REV)
    GVS(AUTHOR)
    GVS(DATE)
    GVS(BRANCH)
    GVS(e)
    GVS(components)
    GVS(comp)
    GVS(LOG_DIR)
    GVS(DB)
    GVS(overall)
  END_GVS(19)

  // Dynamic Constants

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$framework$records$run;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 31;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[2];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
