
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x3F2B9FFED81C6A49LL, WORKING_DIR, 15);
      break;
    case 11:
      HASH_INDEX(0x7F9AFD13AA2B284BLL, full_log, 12);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x192B21B6D5578B4FLL, DB, 29);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x3299B3C419300E15LL, RESULTS_DIR, 16);
      HASH_INDEX(0x42C1B3083A082995LL, START_TIME, 19);
      HASH_INDEX(0x5A4EF3F899897015LL, REV, 21);
      break;
    case 23:
      HASH_INDEX(0x7C0667E542FB3257LL, REPO, 13);
      HASH_INDEX(0x2FF0FE0727D6A297LL, DB_FILENAME, 17);
      break;
    case 29:
      HASH_INDEX(0x069C028EFEF8C91DLL, DATE, 23);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 37:
      HASH_INDEX(0x3CEB4AC2D7E1C665LL, BRANCH, 24);
      break;
    case 38:
      HASH_INDEX(0x0657F839DD8CF4A6LL, AUTHOR, 22);
      break;
    case 39:
      HASH_INDEX(0x76A5CC5A15A4A4A7LL, allowed_branches, 18);
      break;
    case 42:
      HASH_INDEX(0x45857634D3A1466ALL, comp, 27);
      break;
    case 43:
      HASH_INDEX(0x61B161496B7EA7EBLL, e, 25);
      break;
    case 44:
      HASH_INDEX(0x50EC965386450D6CLL, CWD, 14);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 50:
      HASH_INDEX(0x52ED807A13699D72LL, components, 26);
      break;
    case 58:
      HASH_INDEX(0x58590574DB53D17ALL, overall, 30);
      break;
    case 62:
      HASH_INDEX(0x41343BD8CE76873ELL, LOG_DIR, 28);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      HASH_INDEX(0x5AB8EAD31709E8BFLL, LATEST_REVISION, 20);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 31) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
