
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "full_log",
    "REPO",
    "CWD",
    "WORKING_DIR",
    "RESULTS_DIR",
    "DB_FILENAME",
    "allowed_branches",
    "START_TIME",
    "LATEST_REVISION",
    "REV",
    "AUTHOR",
    "DATE",
    "BRANCH",
    "e",
    "components",
    "comp",
    "LOG_DIR",
    "DB",
    "overall",
  };
  if (idx >= 0 && idx < 31) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(full_log);
    case 13: return GV(REPO);
    case 14: return GV(CWD);
    case 15: return GV(WORKING_DIR);
    case 16: return GV(RESULTS_DIR);
    case 17: return GV(DB_FILENAME);
    case 18: return GV(allowed_branches);
    case 19: return GV(START_TIME);
    case 20: return GV(LATEST_REVISION);
    case 21: return GV(REV);
    case 22: return GV(AUTHOR);
    case 23: return GV(DATE);
    case 24: return GV(BRANCH);
    case 25: return GV(e);
    case 26: return GV(components);
    case 27: return GV(comp);
    case 28: return GV(LOG_DIR);
    case 29: return GV(DB);
    case 30: return GV(overall);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
