
#ifndef __GENERATED_php_phc_test_framework_records_run_nophp_h__
#define __GENERATED_php_phc_test_framework_records_run_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/records/run.nophp.fw.h>

// Declarations
#include <cls/test_component.h>
#include <cls/compile_component.h>
#include <cls/component.h>
#include <cls/benchmark_component.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_create_dir(CVarRef v_dir);
Variant f_cd(CStrRef v_command, CVarRef v_log_name = false);
Variant pm_php$phc_test$framework$records$run(bool incOnce = false, LVariableTable* variables = NULL);
void f_start_log();
Variant f_get_svn_branch(CVarRef v_rev);
Variant f_get_test_revision();
Variant f_c(CStrRef v_command, CVarRef v_log_name = false);
Variant f_get_latest_revision();
void f_e(Variant v_sql);
Variant f_q(Variant v_sql, Variant v_result_type);
void f_initialize_db();
void f_save(CVarRef v_string, CVarRef v_log_name);
void f_end_log(CVarRef v_log_name);
void f_script_problem();
void f_del_dir(CVarRef v_dir);
void f_save_log();
void f_initialize_test();
Array f_get_svn_info(CVarRef v_rev);
void f_record_revision_data();
void f_skip_revision();
void f_svn_problem();
Object co_test_component(CArrRef params, bool init = true);
Object co_compile_component(CArrRef params, bool init = true);
Object co_component(CArrRef params, bool init = true);
Object co_benchmark_component(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_records_run_nophp_h__
