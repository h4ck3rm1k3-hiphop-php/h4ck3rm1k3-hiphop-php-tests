
#ifndef __GENERATED_php_phc_test_framework_records_run_nophp_fw_h__
#define __GENERATED_php_phc_test_framework_records_run_nophp_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(test_component)
FORWARD_DECLARE_CLASS(compile_component)
FORWARD_DECLARE_CLASS(component)
FORWARD_DECLARE_CLASS(benchmark_component)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_records_run_nophp_fw_h__
