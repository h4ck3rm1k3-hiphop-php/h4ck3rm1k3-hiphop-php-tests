
#include <php/phc-test/framework/records/run.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3, Variant &p4) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  p4 = a0.rvalAt(4);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/records/run line 448 */
Variant c_test_component::os_get(const char *s, int64 hash) {
  return c_component::os_get(s, hash);
}
Variant &c_test_component::os_lval(const char *s, int64 hash) {
  return c_component::os_lval(s, hash);
}
void c_test_component::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("component_name", m_component_name.isReferenced() ? ref(m_component_name) : m_component_name));
  props.push_back(NEW(ArrayElement)("table_name", m_table_name.isReferenced() ? ref(m_table_name) : m_table_name));
  c_component::o_get(props);
}
bool c_test_component::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x2D0F86860E0DA568LL, component_name, 14);
      break;
    case 3:
      HASH_EXISTS_STRING(0x0B045E30D13889F7LL, table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_exists(s, hash);
}
Variant c_test_component::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2D0F86860E0DA568LL, m_component_name,
                         component_name, 14);
      break;
    case 3:
      HASH_RETURN_STRING(0x0B045E30D13889F7LL, m_table_name,
                         table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_get(s, hash);
}
Variant c_test_component::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x2D0F86860E0DA568LL, m_component_name,
                      component_name, 14);
      break;
    case 3:
      HASH_SET_STRING(0x0B045E30D13889F7LL, m_table_name,
                      table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_set(s, hash, v, forInit);
}
Variant &c_test_component::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2D0F86860E0DA568LL, m_component_name,
                         component_name, 14);
      break;
    case 3:
      HASH_RETURN_STRING(0x0B045E30D13889F7LL, m_table_name,
                         table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_lval(s, hash);
}
Variant c_test_component::os_constant(const char *s) {
  return c_component::os_constant(s);
}
IMPLEMENT_CLASS(test_component)
ObjectData *c_test_component::cloneImpl() {
  c_test_component *obj = NEW(c_test_component)();
  cloneSet(obj);
  return obj;
}
void c_test_component::cloneSet(c_test_component *clone) {
  clone->m_component_name = m_component_name.isReferenced() ? ref(m_component_name) : m_component_name;
  clone->m_table_name = m_table_name.isReferenced() ? ref(m_table_name) : m_table_name;
  c_component::cloneSet(clone);
}
Variant c_test_component::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x65E2E3336F4F7972LL, matcher_902) {
        return (t_matcher_902(params.rvalAt(0)));
      }
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    default:
      break;
  }
  return c_component::o_invoke(s, params, hash, fatal);
}
Variant c_test_component::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x65E2E3336F4F7972LL, matcher_902) {
        return (t_matcher_902(a0));
      }
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    default:
      break;
  }
  return c_component::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test_component::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_component::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test_component$os_get(const char *s) {
  return c_test_component::os_get(s, -1);
}
Variant &cw_test_component$os_lval(const char *s) {
  return c_test_component::os_lval(s, -1);
}
Variant cw_test_component$os_constant(const char *s) {
  return c_test_component::os_constant(s);
}
Variant cw_test_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test_component::os_invoke(c, s, params, -1, fatal);
}
void c_test_component::init() {
  c_component::init();
  m_component_name = "test";
  m_table_name = "tests";
}
/* SRC: phc-test/framework/records/run line 453 */
void c_test_component::t_run() {
  INSTANCE_METHOD_INJECTION(Test_component, Test_component::run);
  Variant v_results;

  (v_results = LINE(455,f_cd("php test/framework/driver.php -i -p", "test")));
  LINE(456,t_store_test_results(v_results));
  LINE(457,t_save_test_logs());
} /* function */
/* SRC: phc-test/framework/records/run line 460 */
void c_test_component::t_save_test_logs() {
  INSTANCE_METHOD_INJECTION(Test_component, Test_component::save_test_logs);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LOG_DIR __attribute__((__unused__)) = g->GV(LOG_DIR);
  Variant v_LOG_DIR;
  Variant &gv_WORKING_DIR __attribute__((__unused__)) = g->GV(WORKING_DIR);
  Variant v_WORKING_DIR;
  String v_test_dir;
  Variant v_dirs;
  Variant v_dir;

  v_LOG_DIR = ref(g->GV(LOG_DIR));
  v_WORKING_DIR = ref(g->GV(WORKING_DIR));
  (v_test_dir = toString(v_LOG_DIR) + toString("/test_logs"));
  LINE(465,f_c((assignCallTemp(eo_1, toString(x_readlink(toString(v_WORKING_DIR) + toString("/test/logs/latest")))),assignCallTemp(eo_2, toString(" ") + v_test_dir),concat3("mv ", eo_1, eo_2))));
  (v_dirs = LINE(468,x_scandir(v_test_dir)));
  v_dirs.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
  v_dirs.weakRemove(1LL, 0x5BCA7C69B794F8CELL);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_dirs.begin("test_component"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_dir = iter3->second();
      {
        (v_dir = LINE(473,concat3(v_test_dir, "/", toString(v_dir))));
        if (LINE(474,x_is_dir(toString(v_dir)))) {
          (v_dir = LINE(476,(assignCallTemp(eo_0, concat(toString(x_getcwd()), "/")),assignCallTemp(eo_2, v_dir),x_str_replace(eo_0, "", eo_2))));
          LINE(477,f_c(concat4("tar czf ", toString(v_dir), ".tar.gz ", toString(v_dir))));
          LINE(478,f_del_dir(v_dir));
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/records/run line 483 */
void c_test_component::t_store_test_results(Variant v_result_string) {
  INSTANCE_METHOD_INJECTION(Test_component, Test_component::store_test_results);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Array v_matchers;
  Variant v_matcher;
  Variant v_results;
  Variant v_total_pass;
  Variant v_total_fail;
  Variant v_total_timeout;
  Variant v_total_skip;
  Variant v_result;
  Variant v_name;
  Variant v_pass;
  Variant v_fail;
  Variant v_timeout;
  Variant v_skip;

  v_matchers.append(("matcher_902"));
  {
    LOOP_COUNTER(4);
    for (ArrayIter iter6 = v_matchers.begin("test_component"); !iter6.end(); ++iter6) {
      LOOP_COUNTER_CHECK(4);
      v_matcher = iter6.second();
      {
        (v_results = LINE(490,o_root_invoke_few_args(toString(v_matcher), -1LL, 1, v_result_string)));
        if (!same(v_results, false)) break;
      }
    }
  }
  if (same(v_results, false)) throw_exception(((Object)(LINE(496,p_exception(p_exception(NEWOBJ(c_exception)())->create())))));
  (v_total_pass = 0LL);
  (v_total_fail = 0LL);
  (v_total_timeout = 0LL);
  (v_total_skip = 0LL);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_results.begin("test_component"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_result = iter9->second();
      {
        df_lambda_3(v_result, v_name, v_pass, v_fail, v_timeout, v_skip);
        LINE(507,f_e(concat_rev(concat6(toString(v_fail), ", ", toString(v_timeout), ", ", toString(v_skip), ")"), concat("INSERT INTO tests VALUES (", concat6(toString(gv_REV), ", '", toString(v_name), "', ", toString(v_pass), ", ")))));
        v_total_pass += v_pass;
        v_total_fail += v_fail;
        v_total_timeout += v_timeout;
        v_total_skip += v_skip;
      }
    }
  }
  LINE(514,f_e(concat_rev(concat6(toString(v_total_fail), ", ", toString(v_total_timeout), ", ", toString(v_total_skip), ")"), concat5("INSERT INTO tests VALUES (", toString(gv_REV), ", 'Total', ", toString(v_total_pass), ", "))));
} /* function */
/* SRC: phc-test/framework/records/run line 517 */
bool c_test_component::t_matcher_858(Variant v_string) {
  INSTANCE_METHOD_INJECTION(Test_component, Test_component::matcher_858);
  (v_string = LINE(519,invoke_failed("strip_console_codes", Array(ArrayInit(1).set(0, ref(v_string)).create()), 0x000000006A9E45E0LL)));
  return false;
} /* function */
/* SRC: phc-test/framework/records/run line 524 */
Variant c_test_component::t_matcher_902(Variant v_string) {
  INSTANCE_METHOD_INJECTION(Test_component, Test_component::matcher_902);
  Variant v_matches;
  Variant v_results;

  (v_string = LINE(526,invoke_failed("strip_console_codes", Array(ArrayInit(1).set(0, ref(v_string)).create()), 0x000000006A9E45E0LL)));
  (v_results = LINE(528,x_preg_match_all("/(\\S+)\\s+\\S+:\\s+(\\d+) P,\\s*(\\d+) F,\\s*(\\d+) T,\\s*(\\d+) S/", toString(v_string), ref(v_matches), toInt32(2LL) /* PREG_SET_ORDER */)));
  if (toBoolean(v_results)) {
    LINE(532,x_array_map(2, "array_shift", ref(v_matches)));
    return v_matches;
  }
  return false;
} /* function */
/* SRC: phc-test/framework/records/run line 423 */
Variant c_compile_component::os_get(const char *s, int64 hash) {
  return c_component::os_get(s, hash);
}
Variant &c_compile_component::os_lval(const char *s, int64 hash) {
  return c_component::os_lval(s, hash);
}
void c_compile_component::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("component_name", m_component_name.isReferenced() ? ref(m_component_name) : m_component_name));
  c_component::o_get(props);
}
bool c_compile_component::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x2D0F86860E0DA568LL, component_name, 14);
      break;
    default:
      break;
  }
  return c_component::o_exists(s, hash);
}
Variant c_compile_component::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2D0F86860E0DA568LL, m_component_name,
                         component_name, 14);
      break;
    default:
      break;
  }
  return c_component::o_get(s, hash);
}
Variant c_compile_component::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x2D0F86860E0DA568LL, m_component_name,
                      component_name, 14);
      break;
    default:
      break;
  }
  return c_component::o_set(s, hash, v, forInit);
}
Variant &c_compile_component::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2D0F86860E0DA568LL, m_component_name,
                         component_name, 14);
      break;
    default:
      break;
  }
  return c_component::o_lval(s, hash);
}
Variant c_compile_component::os_constant(const char *s) {
  return c_component::os_constant(s);
}
IMPLEMENT_CLASS(compile_component)
ObjectData *c_compile_component::cloneImpl() {
  c_compile_component *obj = NEW(c_compile_component)();
  cloneSet(obj);
  return obj;
}
void c_compile_component::cloneSet(c_compile_component *clone) {
  clone->m_component_name = m_component_name.isReferenced() ? ref(m_component_name) : m_component_name;
  c_component::cloneSet(clone);
}
Variant c_compile_component::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    default:
      break;
  }
  return c_component::o_invoke(s, params, hash, fatal);
}
Variant c_compile_component::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    default:
      break;
  }
  return c_component::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_compile_component::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_component::os_invoke(c, s, params, hash, fatal);
}
Variant cw_compile_component$os_get(const char *s) {
  return c_compile_component::os_get(s, -1);
}
Variant &cw_compile_component$os_lval(const char *s) {
  return c_compile_component::os_lval(s, -1);
}
Variant cw_compile_component$os_constant(const char *s) {
  return c_compile_component::os_constant(s);
}
Variant cw_compile_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_compile_component::os_invoke(c, s, params, -1, fatal);
}
void c_compile_component::init() {
  c_component::init();
  m_component_name = "compile";
}
/* SRC: phc-test/framework/records/run line 426 */
bool c_compile_component::t_is_needed() {
  INSTANCE_METHOD_INJECTION(Compile_component, Compile_component::is_needed);
  return true;
} /* function */
/* SRC: phc-test/framework/records/run line 432 */
void c_compile_component::t_run() {
  INSTANCE_METHOD_INJECTION(Compile_component, Compile_component::run);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_WORKING_DIR __attribute__((__unused__)) = g->GV(WORKING_DIR);
  Variant &gv_BRANCH __attribute__((__unused__)) = g->GV(BRANCH);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  String v_configure;

  {
  }
  (v_configure = "");
  if (equal(gv_BRANCH, "branches/dataflow") && not_more(gv_REV, 2236LL)) (v_configure = "--disable-gc");
  LINE(442,f_cd("touch src/generated/*"));
  LINE(443,f_cd(concat4("./configure --prefix=", toString(gv_WORKING_DIR), "/installed ", v_configure)));
  LINE(444,f_cd("make install"));
} /* function */
/* SRC: phc-test/framework/records/run line 358 */
Variant c_component::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_component::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_component::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_component::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_component::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_component::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_component::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_component::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(component)
ObjectData *c_component::cloneImpl() {
  c_component *obj = NEW(c_component)();
  cloneSet(obj);
  return obj;
}
void c_component::cloneSet(c_component *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_component::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_component::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_component::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_component$os_get(const char *s) {
  return c_component::os_get(s, -1);
}
Variant &cw_component$os_lval(const char *s) {
  return c_component::os_lval(s, -1);
}
Variant cw_component$os_constant(const char *s) {
  return c_component::os_constant(s);
}
Variant cw_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_component::os_invoke(c, s, params, -1, fatal);
}
void c_component::init() {
}
/* SRC: phc-test/framework/records/run line 360 */
bool c_component::t_is_needed() {
  INSTANCE_METHOD_INJECTION(Component, Component::is_needed);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant v_components;

  (v_components = LINE(368,(assignCallTemp(eo_0, concat5("\n\t\t\t\t\tSELECT redo FROM components\n\t\t\t\t\tWHERE revision == ", toString(gv_REV), "\n\t\t\t\t\tAND component == '", toString(o_get("component_name", 0x2D0F86860E0DA568LL)), "'\n\t\t\t\t\t")),assignCallTemp(eo_1, throw_fatal("unknown class constant pdo::FETCH_COLUMN")),f_q(eo_0, eo_1))));
  return equal(LINE(369,x_count(v_components)), 0LL) || equal(v_components.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 1LL);
} /* function */
/* SRC: phc-test/framework/records/run line 372 */
void c_component::t_start() {
  INSTANCE_METHOD_INJECTION(Component, Component::start);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant &gv_overall __attribute__((__unused__)) = g->GV(overall);
  {
  }
  LINE(379,f_e(concat5("\tDELETE FROM components\n\t\t\t\t\tWHERE revision == ", toString(gv_REV), "\n\t\t\t\t\tAND component == '", toString(o_get("component_name", 0x2D0F86860E0DA568LL)), "'")));
  if (t___isset("table_name")) {
    LINE(384,f_e(concat4("\tDELETE FROM ", toString(o_get("table_name", 0x0B045E30D13889F7LL)), "\n\t\t\t\t\t\tWHERE revision == ", toString(gv_REV))));
  }
  LINE(387,f_start_log());
  (o_lval("start_time", 0x687BED2B0019D265LL) = LINE(389,x_time()));
} /* function */
/* SRC: phc-test/framework/records/run line 392 */
void c_component::t_finish() {
  INSTANCE_METHOD_INJECTION(Component, Component::finish);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant &gv_LATEST_REVISION __attribute__((__unused__)) = g->GV(LATEST_REVISION);
  int v_end_time = 0;
  Numeric v_time_taken = 0;

  {
  }
  (v_end_time = LINE(396,x_time()));
  (v_time_taken = v_end_time - o_get("start_time", 0x687BED2B0019D265LL));
  LINE(400,f_e(concat_rev(concat6(toString(v_time_taken), ", \n\t\t\t\t\t", toString(v_end_time), ", ", toString(gv_LATEST_REVISION), ", 0, 0)"), concat5("\tINSERT INTO components VALUES (\n\t\t\t\t\t", toString(gv_REV), ", '", toString(o_get("component_name", 0x2D0F86860E0DA568LL)), "', "))));
  LINE(402,f_end_log(o_get("component_name", 0x2D0F86860E0DA568LL)));
} /* function */
/* SRC: phc-test/framework/records/run line 405 */
void c_component::t_fail() {
  INSTANCE_METHOD_INJECTION(Component, Component::fail);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant &gv_LATEST_REVISION __attribute__((__unused__)) = g->GV(LATEST_REVISION);
  int v_end_time = 0;
  Numeric v_time_taken = 0;

  {
  }
  (v_end_time = LINE(409,x_time()));
  (v_time_taken = v_end_time - o_get("start_time", 0x687BED2B0019D265LL));
  LINE(413,f_e(concat_rev(concat6(toString(v_time_taken), ", \n\t\t\t\t\t", toString(v_end_time), ", ", toString(gv_LATEST_REVISION), ", 1, 0)"), concat5("\tINSERT INTO components VALUES (\n\t\t\t\t\t", toString(gv_REV), ", '", toString(o_get("component_name", 0x2D0F86860E0DA568LL)), "', "))));
  print(LINE(415,concat3("Problem with ", toString(o_get("component_name", 0x2D0F86860E0DA568LL)), " component\n")));
  LINE(417,f_end_log(o_get("component_name", 0x2D0F86860E0DA568LL)));
} /* function */
/* SRC: phc-test/framework/records/run line 420 */
/* SRC: phc-test/framework/records/run line 541 */
Variant c_benchmark_component::os_get(const char *s, int64 hash) {
  return c_component::os_get(s, hash);
}
Variant &c_benchmark_component::os_lval(const char *s, int64 hash) {
  return c_component::os_lval(s, hash);
}
void c_benchmark_component::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("component_name", m_component_name.isReferenced() ? ref(m_component_name) : m_component_name));
  props.push_back(NEW(ArrayElement)("table_name", m_table_name.isReferenced() ? ref(m_table_name) : m_table_name));
  c_component::o_get(props);
}
bool c_benchmark_component::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x2D0F86860E0DA568LL, component_name, 14);
      break;
    case 3:
      HASH_EXISTS_STRING(0x0B045E30D13889F7LL, table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_exists(s, hash);
}
Variant c_benchmark_component::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2D0F86860E0DA568LL, m_component_name,
                         component_name, 14);
      break;
    case 3:
      HASH_RETURN_STRING(0x0B045E30D13889F7LL, m_table_name,
                         table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_get(s, hash);
}
Variant c_benchmark_component::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x2D0F86860E0DA568LL, m_component_name,
                      component_name, 14);
      break;
    case 3:
      HASH_SET_STRING(0x0B045E30D13889F7LL, m_table_name,
                      table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_set(s, hash, v, forInit);
}
Variant &c_benchmark_component::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2D0F86860E0DA568LL, m_component_name,
                         component_name, 14);
      break;
    case 3:
      HASH_RETURN_STRING(0x0B045E30D13889F7LL, m_table_name,
                         table_name, 10);
      break;
    default:
      break;
  }
  return c_component::o_lval(s, hash);
}
Variant c_benchmark_component::os_constant(const char *s) {
  return c_component::os_constant(s);
}
IMPLEMENT_CLASS(benchmark_component)
ObjectData *c_benchmark_component::cloneImpl() {
  c_benchmark_component *obj = NEW(c_benchmark_component)();
  cloneSet(obj);
  return obj;
}
void c_benchmark_component::cloneSet(c_benchmark_component *clone) {
  clone->m_component_name = m_component_name.isReferenced() ? ref(m_component_name) : m_component_name;
  clone->m_table_name = m_table_name.isReferenced() ? ref(m_table_name) : m_table_name;
  c_component::cloneSet(clone);
}
Variant c_benchmark_component::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    default:
      break;
  }
  return c_component::o_invoke(s, params, hash, fatal);
}
Variant c_benchmark_component::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x20BF62C1A4617AB1LL, is_needed) {
        return (t_is_needed());
      }
      break;
    case 2:
      HASH_GUARD(0x3970634433FD3E52LL, start) {
        return (t_start(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D0EA11C27E80333LL, finish) {
        return (t_finish(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6F603BE3ACE9D919LL, run) {
        return (t_run(), null);
      }
      break;
    case 10:
      HASH_GUARD(0x731DBC30C2876EAALL, fail) {
        return (t_fail(), null);
      }
      break;
    default:
      break;
  }
  return c_component::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_benchmark_component::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_component::os_invoke(c, s, params, hash, fatal);
}
Variant cw_benchmark_component$os_get(const char *s) {
  return c_benchmark_component::os_get(s, -1);
}
Variant &cw_benchmark_component$os_lval(const char *s) {
  return c_benchmark_component::os_lval(s, -1);
}
Variant cw_benchmark_component$os_constant(const char *s) {
  return c_benchmark_component::os_constant(s);
}
Variant cw_benchmark_component$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_benchmark_component::os_invoke(c, s, params, -1, fatal);
}
void c_benchmark_component::init() {
  c_component::init();
  m_component_name = "benchmark";
  m_table_name = "benchmarks";
}
/* SRC: phc-test/framework/records/run line 546 */
void c_benchmark_component::t_run() {
  INSTANCE_METHOD_INJECTION(Benchmark_component, Benchmark_component::run);
  Variant v_benchmark;

  (v_benchmark = LINE(548,f_cd("../test/framework/bench/valbench -s", "benchmark")));
  LINE(549,t_store_benchmark_results(v_benchmark));
} /* function */
/* SRC: phc-test/framework/records/run line 552 */
Variant c_benchmark_component::t_store_benchmark_results(CVarRef v_result_string) {
  INSTANCE_METHOD_INJECTION(Benchmark_component, Benchmark_component::store_benchmark_results);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant v_results;
  Primitive v_bench = 0;
  Variant v_result_array;
  Primitive v_metric = 0;
  Variant v_result;

  if (toBoolean(LINE(556,x_preg_match("/^FAILURE:.*/", toString(v_result_string))))) return -1LL;
  (v_results = LINE(559,x_unserialize(toString(v_result_string))));
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_results.begin("benchmark_component"); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_result_array = iter12->second();
      v_bench = iter12->first();
      {
        LOOP_COUNTER(13);
        for (ArrayIterPtr iter15 = v_result_array.begin("benchmark_component"); !iter15->end(); iter15->next()) {
          LOOP_COUNTER_CHECK(13);
          v_result = iter15->second();
          v_metric = iter15->first();
          LINE(563,f_e(concat_rev(concat6(toString(v_bench), "', '", toString(v_metric), "', ", toString(v_result), ")"), concat3("INSERT INTO benchmarks VALUES (", toString(gv_REV), ", '"))));
        }
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/framework/records/run line 176 */
void f_create_dir(CVarRef v_dir) {
  FUNCTION_INJECTION(create_dir);
  LINE(176,f_c(toString("mkdir -p ") + toString(v_dir)));
} /* function */
/* SRC: phc-test/framework/records/run line 218 */
Variant f_cd(CStrRef v_command, CVarRef v_log_name //  = false
) {
  FUNCTION_INJECTION(cd);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_WORKING_DIR __attribute__((__unused__)) = g->GV(WORKING_DIR);
  Variant v_cwd;
  Variant v_result;

  (v_cwd = LINE(221,x_getcwd()));
  print(LINE(222,concat3("Entering ", toString(gv_WORKING_DIR), "\n")));
  (LINE(223,x_chdir(toString(gv_WORKING_DIR)))) || (toBoolean(invoke_failed("x", Array(ArrayInit(1).set(0, toString("Couldnt change dir to ") + toString(gv_WORKING_DIR)).create()), 0x000000000234EA81LL)));
  (v_result = LINE(224,f_c(v_command, v_log_name)));
  LINE(225,x_chdir(toString(v_cwd)));
  print(LINE(226,concat5("Leaving ", toString(gv_WORKING_DIR), " for ", toString(v_cwd), "\n")));
  return v_result;
} /* function */
/* SRC: phc-test/framework/records/run line 150 */
void f_start_log() {
  FUNCTION_INJECTION(start_log);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_full_log __attribute__((__unused__)) = g->GV(full_log);
  concat_assign(gv_full_log, LINE(153,x_ob_get_clean()));
  LINE(154,x_ob_start());
} /* function */
/* SRC: phc-test/framework/records/run line 313 */
Variant f_get_svn_branch(CVarRef v_rev) {
  FUNCTION_INJECTION(get_svn_branch);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REPO __attribute__((__unused__)) = g->GV(REPO);
  Variant v_diff;
  Variant v_matches;

  (v_diff = LINE(317,f_c(concat4("svn diff -c ", toString(v_rev), " ", toString(gv_REPO)))));
  if (toBoolean(LINE(320,x_preg_match("#^Index: (branches/[^\\/]+)/#", toString(v_diff), ref(v_matches))))) {
    return v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
  }
  else if (toBoolean(LINE(324,x_preg_match("#^Index: trunk#", toString(v_diff))))) return "trunk";
  else if (toBoolean(LINE(326,x_preg_match("#^Index: www#", toString(v_diff))))) return "www";
  else if (toBoolean(LINE(328,x_preg_match("#^Index: tags#", toString(v_diff))))) return "tags";
  else {
    return "unknown_branch";
  }
  return null;
} /* function */
/* SRC: phc-test/framework/records/run line 286 */
Variant f_get_test_revision() {
  FUNCTION_INJECTION(get_test_revision);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LATEST_REVISION __attribute__((__unused__)) = g->GV(LATEST_REVISION);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_revs;
  Variant v_redo_revs;
  Variant v_i;

  {
  }
  (v_revs = LINE(290,f_q("SELECT revision FROM complete", throw_fatal("unknown class constant pdo::FETCH_COLUMN"))));
  (v_redo_revs = LINE(291,f_q("SELECT revision FROM components WHERE redo == 1", throw_fatal("unknown class constant pdo::FETCH_COLUMN"))));
  (v_revs = LINE(292,x_array_flip(v_revs)));
  (v_redo_revs = LINE(293,x_array_flip(v_redo_revs)));
  {
    LOOP_COUNTER(16);
    for ((v_i = gv_LATEST_REVISION); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(16);
      {
        if (!(isset(v_revs, v_i)) || isset(v_redo_revs, v_i)) return LINE(299,f_get_svn_info(v_i));
      }
    }
  }
  return false;
} /* function */
/* SRC: phc-test/framework/records/run line 184 */
Variant f_c(CStrRef v_command, CVarRef v_log_name //  = false
) {
  FUNCTION_INJECTION(c);
  Variant eo_0;
  Variant eo_1;
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  print(LINE(186,concat3("Running command '", v_command, "'\n")));
  df_lambda_2(LINE(188,invoke_failed("complete_exec", Array(ArrayInit(4).set(0, toString("ulimit -v 307200; ") + v_command).set(1, null).set(2, 43200LL).set(3, true).create()), 0x00000000739F6DDCLL)), v_out, v_err, v_exit);
  if (toBoolean(v_log_name)) {
    LINE(192,(assignCallTemp(eo_0, concat6("Exit: ", toString(v_exit), "\n\nError:\n", toString(v_err), "\n\nOutput:\n", toString(v_out))),assignCallTemp(eo_1, v_log_name),f_save(eo_0, eo_1)));
  }
  if (same(v_out, "Timeout")) {
    (v_out = v_err);
    (v_err = v_exit);
    print(LINE(198,concat5("Timeout\nOutput:", toString(v_out), "\nError:", toString(v_err), "\n")));
    throw_exception(((Object)(LINE(199,p_exception(p_exception(NEWOBJ(c_exception)())->create())))));
  }
  print(LINE(203,concat3("Returning result '", toString(v_out), "'\n")));
  print(LINE(204,concat3("Returning error '", toString(v_err), "'\n")));
  print(LINE(205,concat3("Returning exit '", toString(v_exit), "'\n")));
  if (!same(v_exit, 0LL)) {
    print(LINE(209,concat3("Exit code is not zero: ", toString(v_exit), "\n")));
    throw_exception(((Object)(LINE(210,p_exception(p_exception(NEWOBJ(c_exception)())->create())))));
  }
  return v_out;
} /* function */
/* SRC: phc-test/framework/records/run line 304 */
Variant f_get_latest_revision() {
  FUNCTION_INJECTION(get_latest_revision);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REPO __attribute__((__unused__)) = g->GV(REPO);
  Variant v_result;
  Variant v_matches;

  (v_result = LINE(307,f_c(toString("svn info ") + toString(gv_REPO))));
  LINE(308,x_preg_match("/Revision: (\\d+)/", toString(v_result), ref(v_matches)));
  return v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/framework/records/run line 233 */
void f_e(Variant v_sql) {
  FUNCTION_INJECTION(e);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  print(LINE(235,concat3("Execing '", toString(v_sql), "'\n")));
  if (same(LINE(237,gv_DB.o_invoke_few_args("exec", 0x4F1D1ED7B087208CLL, 1, v_sql)), false)) {
    LINE(239,x_var_dump(1, gv_DB.o_invoke_few_args("errorInfo", 0x1DDD412E7F04605ELL, 0)));
    LINE(240,f_script_problem());
  }
} /* function */
/* SRC: phc-test/framework/records/run line 248 */
Variant f_q(Variant v_sql, Variant v_result_type) {
  FUNCTION_INJECTION(q);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_query;
  Variant v_result;

  print(LINE(250,concat3("Querying '", toString(v_sql), "'\n")));
  (v_query = LINE(252,gv_DB.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, v_sql)));
  if (same(v_query, false)) {
    LINE(256,x_var_dump(1, gv_DB.o_invoke_few_args("errorInfo", 0x1DDD412E7F04605ELL, 0)));
    LINE(257,f_script_problem());
  }
  (v_result = LINE(259,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, v_result_type)));
  LINE(260,x_var_dump(1, v_result));
  return v_result;
} /* function */
/* SRC: phc-test/framework/records/run line 116 */
void f_initialize_db() {
  FUNCTION_INJECTION(initialize_db);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_RESULTS_DIR __attribute__((__unused__)) = g->GV(RESULTS_DIR);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant &gv_DB_FILENAME __attribute__((__unused__)) = g->GV(DB_FILENAME);
  LINE(119,f_create_dir(gv_RESULTS_DIR));
  {
  }
  (gv_DB = LINE(122,create_object("pdo", Array(ArrayInit(1).set(0, toString("sqlite:") + toString(gv_DB_FILENAME)).create()))));
} /* function */
/* SRC: phc-test/framework/records/run line 140 */
void f_save(CVarRef v_string, CVarRef v_log_name) {
  FUNCTION_INJECTION(save);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LOG_DIR __attribute__((__unused__)) = g->GV(LOG_DIR);
  if (toBoolean(gv_LOG_DIR)) LINE(144,(assignCallTemp(eo_0, concat4(toString(gv_LOG_DIR), "/", toString(v_log_name), ".log")),assignCallTemp(eo_1, v_string),x_file_put_contents(eo_0, eo_1)));
  else print(toString("No log yet:\n ") + toString(v_string));
} /* function */
/* SRC: phc-test/framework/records/run line 157 */
void f_end_log(CVarRef v_log_name) {
  FUNCTION_INJECTION(end_log);
  Variant eo_0;
  Variant eo_1;
  LINE(159,(assignCallTemp(eo_0, x_ob_get_contents()),assignCallTemp(eo_1, v_log_name),f_save(eo_0, eo_1)));
} /* function */
/* SRC: phc-test/framework/records/run line 83 */
void f_script_problem() {
  FUNCTION_INJECTION(script_problem);
  String v_info;

  print("Script problem, saving and mailing error\n");
  (v_info = LINE(86,x_ob_get_contents()));
  LINE(87,f_save_log());
  LINE(89,x_mail("paul.biggar@gmail.com", "Script error", v_info));
  f_exit();
} /* function */
/* SRC: phc-test/framework/records/run line 175 */
void f_del_dir(CVarRef v_dir) {
  FUNCTION_INJECTION(del_dir);
  LINE(175,f_c(toString("rm -Rf ") + toString(v_dir)));
} /* function */
/* SRC: phc-test/framework/records/run line 162 */
void f_save_log() {
  FUNCTION_INJECTION(save_log);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_full_log __attribute__((__unused__)) = g->GV(full_log);
  concat_assign(gv_full_log, LINE(165,x_ob_get_contents()));
  LINE(166,f_save(gv_full_log, "log"));
} /* function */
/* SRC: phc-test/framework/records/run line 99 */
void f_initialize_test() {
  FUNCTION_INJECTION(initialize_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant &gv_LOG_DIR __attribute__((__unused__)) = g->GV(LOG_DIR);
  Variant &gv_WORKING_DIR __attribute__((__unused__)) = g->GV(WORKING_DIR);
  Variant &gv_RESULTS_DIR __attribute__((__unused__)) = g->GV(RESULTS_DIR);
  {
  }
  (gv_LOG_DIR = LINE(104,concat3(toString(gv_RESULTS_DIR), "/", toString(gv_REV))));
  LINE(105,f_del_dir(gv_WORKING_DIR));
  LINE(109,f_create_dir(gv_LOG_DIR));
  LINE(112,f_e("DELETE FROM running"));
  LINE(113,f_e(concat3("INSERT INTO running VALUES (", toString(gv_REV), ")")));
} /* function */
/* SRC: phc-test/framework/records/run line 268 */
Array f_get_svn_info(CVarRef v_rev) {
  FUNCTION_INJECTION(get_svn_info);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REPO __attribute__((__unused__)) = g->GV(REPO);
  Variant v_output;
  Variant v_matches;
  Variant v_author;
  Variant v_revision;
  Variant v_date;

  (v_output = LINE(271,f_c(concat4("svn info ", toString(gv_REPO), " -r ", toString(v_rev)))));
  LINE(273,x_preg_match("/^Last Changed Author: (.*)$/m", toString(v_output), ref(v_matches)));
  (v_author = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  LINE(276,x_preg_match("/^Revision: (.*\?)$/m", toString(v_output), ref(v_matches)));
  (v_revision = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  LINE(278,x_assert(equal(v_revision, v_rev)));
  LINE(280,x_preg_match("/^Last Changed Date: (.*)$/m", toString(v_output), ref(v_matches)));
  (v_date = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  return Array(ArrayInit(3).set(0, v_rev).set(1, v_author).set(2, v_date).create());
} /* function */
/* SRC: phc-test/framework/records/run line 125 */
void f_record_revision_data() {
  FUNCTION_INJECTION(record_revision_data);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant &gv_AUTHOR __attribute__((__unused__)) = g->GV(AUTHOR);
  Variant &gv_DATE __attribute__((__unused__)) = g->GV(DATE);
  Variant &gv_BRANCH __attribute__((__unused__)) = g->GV(BRANCH);
  {
  }
  LINE(131,f_e(toString("DELETE FROM complete WHERE revision == ") + toString(gv_REV)));
  LINE(132,f_e(concat_rev(concat6(toString(gv_BRANCH), "', '", toString(gv_AUTHOR), "', '", toString(gv_DATE), "')"), concat3("INSERT INTO complete VALUES (", toString(gv_REV), ", '"))));
} /* function */
/* SRC: phc-test/framework/records/run line 339 */
void f_skip_revision() {
  FUNCTION_INJECTION(skip_revision);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_REV __attribute__((__unused__)) = g->GV(REV);
  Variant &gv_LATEST_REVISION __attribute__((__unused__)) = g->GV(LATEST_REVISION);
  Variant v_component;

  {
  }
  LINE(342,f_e(toString("\tDELETE FROM components WHERE revision == ") + toString(gv_REV)));
  {
    LOOP_COUNTER(17);
    Variant map18 = ScalarArrays::sa_[1];
    for (ArrayIterPtr iter19 = map18.begin(); !iter19->end(); iter19->next()) {
      LOOP_COUNTER_CHECK(17);
      v_component = iter19->second();
      {
        LINE(347,f_e(concat("\tINSERT INTO components VALUES (\n\t\t\t\t", concat6(toString(gv_REV), ", '", toString(v_component), "', 0, \n\t\t\t\t0, ", toString(gv_LATEST_REVISION), ", 0, 0)"))));
      }
    }
  }
  LINE(350,f_e(concat3("DELETE FROM tests where revision == ", toString(gv_REV), ";")));
  LINE(351,f_e(toString("DELETE FROM benchmarks WHERE revision == ") + toString(gv_REV)));
  print(LINE(352,concat3("Skip revision ", toString(gv_REV), "\n")));
  LINE(353,f_save_log());
  f_exit();
} /* function */
/* SRC: phc-test/framework/records/run line 74 */
void f_svn_problem() {
  FUNCTION_INJECTION(svn_problem);
  print("SVN problem. Wait 1 min\n");
  LINE(77,f_save_log());
  print("SVN problem. Wait 1 min\n");
  LINE(79,x_sleep(toInt32(60LL)));
  f_exit();
} /* function */
Object co_test_component(CArrRef params, bool init /* = true */) {
  return Object(p_test_component(NEW(c_test_component)())->dynCreate(params, init));
}
Object co_compile_component(CArrRef params, bool init /* = true */) {
  return Object(p_compile_component(NEW(c_compile_component)())->dynCreate(params, init));
}
Object co_component(CArrRef params, bool init /* = true */) {
  return Object(p_component(NEW(c_component)())->dynCreate(params, init));
}
Object co_benchmark_component(CArrRef params, bool init /* = true */) {
  return Object(p_benchmark_component(NEW(c_benchmark_component)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$records$run(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/records/run);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$records$run;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_full_log __attribute__((__unused__)) = (variables != gVariables) ? variables->get("full_log") : g->GV(full_log);
  Variant &v_REPO __attribute__((__unused__)) = (variables != gVariables) ? variables->get("REPO") : g->GV(REPO);
  Variant &v_CWD __attribute__((__unused__)) = (variables != gVariables) ? variables->get("CWD") : g->GV(CWD);
  Variant &v_WORKING_DIR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("WORKING_DIR") : g->GV(WORKING_DIR);
  Variant &v_RESULTS_DIR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("RESULTS_DIR") : g->GV(RESULTS_DIR);
  Variant &v_DB_FILENAME __attribute__((__unused__)) = (variables != gVariables) ? variables->get("DB_FILENAME") : g->GV(DB_FILENAME);
  Variant &v_allowed_branches __attribute__((__unused__)) = (variables != gVariables) ? variables->get("allowed_branches") : g->GV(allowed_branches);
  Variant &v_START_TIME __attribute__((__unused__)) = (variables != gVariables) ? variables->get("START_TIME") : g->GV(START_TIME);
  Variant &v_LATEST_REVISION __attribute__((__unused__)) = (variables != gVariables) ? variables->get("LATEST_REVISION") : g->GV(LATEST_REVISION);
  Variant &v_REV __attribute__((__unused__)) = (variables != gVariables) ? variables->get("REV") : g->GV(REV);
  Variant &v_AUTHOR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("AUTHOR") : g->GV(AUTHOR);
  Variant &v_DATE __attribute__((__unused__)) = (variables != gVariables) ? variables->get("DATE") : g->GV(DATE);
  Variant &v_BRANCH __attribute__((__unused__)) = (variables != gVariables) ? variables->get("BRANCH") : g->GV(BRANCH);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_components __attribute__((__unused__)) = (variables != gVariables) ? variables->get("components") : g->GV(components);
  Variant &v_comp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("comp") : g->GV(comp);

  LINE(10,require("test/framework/lib/header.php", true, variables, "phc-test/framework/records/"));
  LINE(12,x_ob_start());
  (v_full_log = "");
  (v_REPO = "http://phc.googlecode.com/svn/");
  (v_CWD = LINE(16,x_getcwd()));
  (v_WORKING_DIR = toString(v_CWD) + toString("/testing"));
  (v_RESULTS_DIR = toString(v_CWD) + toString("/results"));
  (v_DB_FILENAME = toString(v_RESULTS_DIR) + toString("/results.db"));
  (v_allowed_branches = ScalarArrays::sa_[0]);
  LINE(24,f_initialize_db());
  (v_START_TIME = LINE(27,x_time()));
  try {
    (v_LATEST_REVISION = LINE(34,f_get_latest_revision()));
    (toBoolean(df_lambda_1(LINE(35,f_get_test_revision()), v_REV, v_AUTHOR, v_DATE))) || ((f_svn_problem(), null));
    (v_BRANCH = LINE(36,f_get_svn_branch(v_REV)));
    LINE(38,f_record_revision_data());
    if (!(LINE(40,x_in_array(v_BRANCH, v_allowed_branches)))) LINE(41,f_skip_revision());
    LINE(43,f_initialize_test());
    LINE(45,f_c(concat("svn export -q --revision=", concat6(toString(v_REV), " ", toString(v_REPO), toString(v_BRANCH), " ", toString(v_WORKING_DIR)))));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      LINE(47,f_svn_problem());
    } else {
      throw;
    }
  }
  (v_components = (assignCallTemp(eo_0, ((Object)(LINE(50,p_compile_component(p_compile_component(NEWOBJ(c_compile_component)())->create()))))),assignCallTemp(eo_1, ((Object)(p_test_component(p_test_component(NEWOBJ(c_test_component)())->create())))),assignCallTemp(eo_2, ((Object)(p_benchmark_component(p_benchmark_component(NEWOBJ(c_benchmark_component)())->create())))),Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, eo_2).create())));
  {
    LOOP_COUNTER(20);
    for (ArrayIterPtr iter22 = v_components.begin(); !iter22->end(); iter22->next()) {
      LOOP_COUNTER_CHECK(20);
      v_comp = iter22->second();
      {
        try {
          if (toBoolean(LINE(55,v_comp.o_invoke_few_args("is_needed", 0x20BF62C1A4617AB1LL, 0)))) {
            LINE(57,v_comp.o_invoke_few_args("start", 0x3970634433FD3E52LL, 0));
            LINE(58,v_comp.o_invoke_few_args("run", 0x6F603BE3ACE9D919LL, 0));
            LINE(59,v_comp.o_invoke_few_args("finish", 0x0D0EA11C27E80333LL, 0));
          }
        } catch (Object e) {
          if (e.instanceof("exception")) {
            v_e = e;
            LINE(64,v_comp.o_invoke_few_args("fail", 0x731DBC30C2876EAALL, 0));
          } else {
            throw;
          }
        }
      }
    }
  }
  LINE(67,f_save_log());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
