
#ifndef __GENERATED_cls_test_component_h__
#define __GENERATED_cls_test_component_h__

#include <cls/component.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/records/run line 448 */
class c_test_component : virtual public c_component {
  BEGIN_CLASS_MAP(test_component)
    PARENT_CLASS(component)
  END_CLASS_MAP(test_component)
  DECLARE_CLASS(test_component, Test_component, component)
  void init();
  public: Variant m_component_name;
  public: Variant m_table_name;
  public: void t_run();
  public: void t_save_test_logs();
  public: void t_store_test_results(Variant v_result_string);
  public: bool t_matcher_858(Variant v_string);
  public: Variant t_matcher_902(Variant v_string);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_component_h__
