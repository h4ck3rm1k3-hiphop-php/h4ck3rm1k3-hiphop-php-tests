
#ifndef __GENERATED_cls_component_h__
#define __GENERATED_cls_component_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/records/run line 358 */
class c_component : virtual public ObjectData {
  BEGIN_CLASS_MAP(component)
  END_CLASS_MAP(component)
  DECLARE_CLASS(component, Component, ObjectData)
  void init();
  public: bool t_is_needed();
  public: void t_start();
  public: void t_finish();
  public: void t_fail();
  // public: void t_run() = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_component_h__
