
#ifndef __GENERATED_cls_compile_component_h__
#define __GENERATED_cls_compile_component_h__

#include <cls/component.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/records/run line 423 */
class c_compile_component : virtual public c_component {
  BEGIN_CLASS_MAP(compile_component)
    PARENT_CLASS(component)
  END_CLASS_MAP(compile_component)
  DECLARE_CLASS(compile_component, Compile_component, component)
  void init();
  public: Variant m_component_name;
  public: bool t_is_needed();
  public: void t_run();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_compile_component_h__
