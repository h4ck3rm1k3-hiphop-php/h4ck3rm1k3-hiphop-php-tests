
#ifndef __GENERATED_cls_benchmark_component_h__
#define __GENERATED_cls_benchmark_component_h__

#include <cls/component.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/records/run line 541 */
class c_benchmark_component : virtual public c_component {
  BEGIN_CLASS_MAP(benchmark_component)
    PARENT_CLASS(component)
  END_CLASS_MAP(benchmark_component)
  DECLARE_CLASS(benchmark_component, Benchmark_component, component)
  void init();
  public: Variant m_component_name;
  public: Variant m_table_name;
  public: void t_run();
  public: Variant t_store_benchmark_results(CVarRef v_result_string);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_benchmark_component_h__
