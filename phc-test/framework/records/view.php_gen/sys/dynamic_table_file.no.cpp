
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$framework$records$view_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$records$common_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_INCLUDE(0x66A9E93B1ABAF2B2LL, "phc-test/framework/records/view.php", php$phc_test$framework$records$view_php);
      break;
    case 3:
      HASH_INCLUDE(0x613EDAB095FB8957LL, "phc-test/framework/records/common.php", php$phc_test$framework$records$common_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
