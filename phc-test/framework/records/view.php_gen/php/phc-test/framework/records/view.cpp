
#include <php/phc-test/framework/records/common.h>
#include <php/phc-test/framework/records/view.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/records/view.php line 156 */
Variant f_merge_results(Variant v_results, CVarRef v_new_results) {
  FUNCTION_INJECTION(merge_results);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ABBREVIATIONS __attribute__((__unused__)) = g->GV(ABBREVIATIONS);
  Variant v_new_result;
  int64 v_rev = 0;
  Primitive v_key = 0;
  Variant v_value;
  Variant v_old_value;

  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_new_results.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_new_result = iter3->second();
      {
        (v_rev = toInt64(v_new_result.rvalAt("revision", 0x62F087C6F59E7C3CLL)));
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_new_result.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_value = iter6->second();
            v_key = iter6->first();
            {
              if (isset(gv_ABBREVIATIONS, v_value)) (v_value = gv_ABBREVIATIONS.rvalAt(v_value));
              if (equal(v_key, "failed") || equal(v_key, "redo")) {
                if (equal(v_value, 1LL)) (v_value = LINE(173,x_strtoupper(toString(v_new_result.rvalAt("component", 0x325372E44A0C742FLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
                else (v_value = "");
              }
              if (isset(v_results.rvalAt(v_rev), v_key)) {
                (v_old_value = ref(lval(lval(v_results.lvalAt(v_rev)).lvalAt(v_key))));
                if (equal(v_key, "revision")) {}
                else if (equal(v_key, "time_taken")) (v_value = v_old_value + v_value);
                else if (equal(v_key, "revision_used")) (v_value = LINE(190,concat3(toString(v_old_value), ", ", toString(v_value))));
                else if (equal(v_key, "failed")) (v_value = toString(v_old_value) + toString(v_value));
                else if (equal(v_key, "test_date")) (v_value = LINE(194,x_max(2, v_old_value, Array(ArrayInit(1).set(0, v_value).create()))));
                else if (equal(v_key, "redo")) (v_value = toString(v_old_value) + toString(v_value));
                else if (equal(v_key, "component")) {}
                else f_exit(LINE(200,(assignCallTemp(eo_1, toString(v_rev)),assignCallTemp(eo_2, concat6(", ", toString(v_key), " (", toString(v_old_value), ") with ", toString(v_value))),concat3("Overwriting ", eo_1, eo_2))));
              }
              lval(v_results.lvalAt(v_rev)).set(v_key, (v_value));
            }
          }
        }
      }
    }
  }
  return v_results;
} /* function */
/* SRC: phc-test/framework/records/view.php line 12 */
void f_run_main() {
  FUNCTION_INJECTION(run_main);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_DB;
  Array v_order;
  Variant v_header;
  Sequence v_headers;
  Variant v_query;
  Variant v_completes;
  Variant v_tests;
  Variant v_benchmarks;
  Variant v_compile_meta;
  Variant v_test_meta;
  Variant v_bench_meta;
  Variant v_running;
  Variant v_revisions;
  Primitive v_rev = 0;
  Variant v_data;
  Variant v_branch;
  Variant v_prev;
  String v_color;
  Variant v_value;

  v_DB = ref(g->GV(DB));
  (v_order = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(7);
    for (ArrayIter iter9 = v_order.begin(); !iter9.end(); ++iter9) {
      LOOP_COUNTER_CHECK(7);
      v_header = iter9.second();
      {
        v_headers.set(v_header, (LINE(33,x_ucfirst(toString(x_str_replace("_", " ", v_header))))));
      }
    }
  }
  v_headers.set("revision", ("Rev"), 0x62F087C6F59E7C3CLL);
  v_headers.set("revision_used", ("Test rev"), 0x5F09E447C3573F3BLL);
  v_headers.set("time_taken", ("Time"), 0x175345F8C9006B11LL);
  v_headers.set("benchmark", ("Bench"), 0x4BB9B42DB883C0E1LL);
  v_headers.set("timeout", ("T/O"), 0x6DD5F78B7FC496F2LL);
  print("<table class=info>\n");
  print("<tr>\n");
  {
    LOOP_COUNTER(10);
    Variant map11 = LINE(44,x_array_values(v_headers));
    for (ArrayIterPtr iter12 = map11.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_header = iter12->second();
      print(LINE(45,concat3("<th>", toString(v_header), "</th>\n")));
    }
  }
  (v_query = (assignCallTemp(eo_0, toObject(v_DB)),LINE(51,eo_0.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, "\n\t\t\t\tSELECT\trevision, branch, author\n\t\t\t\tFROM\t\tcomplete\n\t\t\t\t"))));
  (v_completes = LINE(52,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  (v_query = (assignCallTemp(eo_1, toObject(v_DB)),LINE(59,eo_1.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, "\n\t\t\t\tSELECT\trevision, pass, fail, skip, timeout\n\t\t\t\tFROM\t\ttests\n\t\t\t\tWHERE\t\ttestname == 'Total'\n\t\t\t\t"))));
  (v_tests = LINE(60,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  (v_query = (assignCallTemp(eo_2, toObject(v_DB)),LINE(67,eo_2.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, "\n\t\t\t\tSELECT\trevision, result as benchmark\n\t\t\t\tFROM\t\tbenchmarks\n\t\t\t\tWHERE\t\tmetric = 'All'\n\t\t\t\t"))));
  (v_benchmarks = LINE(68,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  (v_query = (assignCallTemp(eo_3, toObject(v_DB)),LINE(75,eo_3.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, "\n\t\t\t\tSELECT\trevision, component, time_taken, test_date, revision_used, failed, redo\n\t\t\t\tFROM\t\tcomponents\n\t\t\t\tWHERE\t\tcomponent == 'compile'\n\t\t\t\t"))));
  (v_compile_meta = LINE(77,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  (v_query = (assignCallTemp(eo_4, toObject(v_DB)),LINE(84,eo_4.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, "\n\t\t\t\tSELECT\trevision, component, time_taken, test_date, revision_used, failed, redo\n\t\t\t\tFROM\t\tcomponents\n\t\t\t\tWHERE\t\tcomponent == 'test'\n\t\t\t\t"))));
  (v_test_meta = LINE(85,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  (v_query = (assignCallTemp(eo_5, toObject(v_DB)),LINE(92,eo_5.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, "\n\t\t\t\tSELECT\trevision, component, time_taken, test_date, revision_used, failed, redo\n\t\t\t\tFROM\t\tcomponents\n\t\t\t\tWHERE\t\tcomponent == 'benchmark'\n\t\t\t\t"))));
  (v_bench_meta = LINE(93,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  (v_query = LINE(95,v_DB.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, "SELECT revision FROM running")));
  (v_running = LINE(96,v_query.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  (v_running = v_running.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("revision", 0x62F087C6F59E7C3CLL));
  (v_revisions = LINE(102,x_array_reduce(Array(ArrayInit(6).set(0, v_completes).set(1, v_tests).set(2, v_benchmarks).set(3, v_compile_meta).set(4, v_test_meta).set(5, v_bench_meta).create()), "merge_results")));
  LINE(106,x_ksort(ref(v_revisions)));
  {
    LOOP_COUNTER(13);
    Variant map14 = ref(v_revisions);
    map14.escalate();
    for (MutableArrayIterPtr iter15 = map14.begin(&v_rev, v_data); iter15->advance();) {
      LOOP_COUNTER_CHECK(13);
      {
        (v_branch = v_data.rvalAt("branch", 0x5B071EF3F7DD559FLL));
        v_data.set("difference", (LINE(114,f_add_difference("pass", ref(v_data), v_revisions.rvalAt(v_prev.rvalAt(v_branch))))), 0x6ADD28D83D2E1F6ELL);
        LINE(115,f_add_difference("fail", ref(v_data), v_revisions.rvalAt(v_prev.rvalAt(v_branch))));
        v_prev.set(v_branch, (v_rev));
      }
    }
  }
  (v_revisions = LINE(120,x_array_reverse(v_revisions, true)));
  {
    LOOP_COUNTER(16);
    for (ArrayIterPtr iter18 = v_revisions.begin(); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_data = iter18->second();
      v_rev = iter18->first();
      {
        print("<tr>\n");
        (v_color = "");
        if (more(v_data.rvalAt("difference", 0x6ADD28D83D2E1F6ELL), 0LL)) (v_color = LINE(131,f_get_good_color()));
        else if (less(v_data.rvalAt("difference", 0x6ADD28D83D2E1F6ELL), 0LL)) (v_color = LINE(133,f_get_bad_color()));
        v_data.weakRemove("difference", 0x6ADD28D83D2E1F6ELL);
        if (equal(v_rev, v_running)) (v_color = LINE(137,f_get_running_color()));
        v_data.set("revision", (LINE(141,concat5("<a href=\"details.php\?rev=", toString(v_rev), "\">", toString(v_rev), "</a>"))), 0x62F087C6F59E7C3CLL);
        if (isset(v_data, "test_date", 0x6498890FF08072B3LL)) v_data.set("test_date", (LINE(143,f_date_from_timestamp(v_data.rvalAt("test_date", 0x6498890FF08072B3LL)))), 0x6498890FF08072B3LL);
        if (isset(v_data, "time_taken", 0x175345F8C9006B11LL)) v_data.set("time_taken", (LINE(145,f_minutes_from_seconds(v_data.rvalAt("time_taken", 0x175345F8C9006B11LL)))), 0x175345F8C9006B11LL);
        {
          LOOP_COUNTER(19);
          for (ArrayIter iter21 = v_order.begin(); !iter21.end(); ++iter21) {
            LOOP_COUNTER_CHECK(19);
            v_header = iter21.second();
            {
              (v_value = v_data.rvalAt(v_header));
              print(LINE(150,concat5("<td ", v_color, ">", toString(v_value), "</td>\n")));
            }
          }
        }
        print("<a/></tr>\n");
      }
    }
  }
} /* function */
Variant i_merge_results(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0E2AEB4C09F46EB8LL, merge_results) {
    return (f_merge_results(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$framework$records$view_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/records/view.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$records$view_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ABBREVIATIONS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ABBREVIATIONS") : g->GV(ABBREVIATIONS);

  (v_ABBREVIATIONS = ScalarArrays::sa_[0]);
  LINE(10,pm_php$phc_test$framework$records$common_php(false, variables));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
