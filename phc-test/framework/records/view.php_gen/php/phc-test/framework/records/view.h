
#ifndef __GENERATED_php_phc_test_framework_records_view_h__
#define __GENERATED_php_phc_test_framework_records_view_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/records/view.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_merge_results(Variant v_results, CVarRef v_new_results);
void f_run_main();
Variant pm_php$phc_test$framework$records$view_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_records_view_h__
