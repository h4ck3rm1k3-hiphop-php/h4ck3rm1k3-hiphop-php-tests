
#include <php/phc-test/framework/records/loop.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$framework$records$loop(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/records/loop);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$records$loop;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("#!/bin/bash\n\n# all subprocesses will have environmental variable set in their shell. That\n# way, we can identify and kill stray subprocesses after the main test has\n# completed. A number of other ways have been tried, including using $\? and $1\n# with pgrep, but none have actualy manages to consistently identify\n# subprocesses.\nexport PHC_SUB_PROCESS_ENVIRONMENTAL_VARIABLE_IDENTIFIER=\n\n\nwhile [ 1 ]");
  echo("\ndo\n\tsvn update\n\tphp test/framework/records/run\n\n\t# find all the subshells with identifier in their environment (1st column is the pid)\n\tfor kpid in `ps hef | grep [P]HC_SUB_PROCESS_ENVIRONMENTAL_VARIABLE_IDENTIFIER | awk '{print $1}'`; do\n\t\t# ignore errors, as it's not uncommon for the process to die between finding out about it and killing it.\n\t\tkill $kpid 2> /dev/null\t\t\t# kill it\n\t\tsleep 2\t\t\t\t\t");
  echo("\t\t\t# give it a chance to die\n\t\tkill -9 $kpid 2> /dev/null\t\t# make sure its dead\n\tdone\n\ndone\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
