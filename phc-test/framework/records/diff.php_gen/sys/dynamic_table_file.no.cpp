
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$framework$records$diff_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$records$common_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_INCLUDE(0x648F35239C5245B6LL, "phc-test/framework/records/diff.php", php$phc_test$framework$records$diff_php);
      break;
    case 3:
      HASH_INCLUDE(0x613EDAB095FB8957LL, "phc-test/framework/records/common.php", php$phc_test$framework$records$common_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
