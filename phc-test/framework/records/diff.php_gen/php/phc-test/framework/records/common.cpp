
#include <php/phc-test/framework/records/common.h>
#include <php/phc-test/framework/records/diff.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/records/common.php line 70 */
String f_get_running_color() {
  FUNCTION_INJECTION(get_running_color);
  return "style=\"color:white; background-color:black;\"";
} /* function */
/* SRC: phc-test/framework/records/common.php line 69 */
String f_get_bad_color() {
  FUNCTION_INJECTION(get_bad_color);
  return "style=\"color:red; font-weight: bold;\"";
} /* function */
/* SRC: phc-test/framework/records/common.php line 32 */
Numeric f_add_percentage_difference(CVarRef v_name, Variant v_new, CVarRef v_old) {
  FUNCTION_INJECTION(add_percentage_difference);
  Numeric v_difference = 0;
  Numeric v_difference_percentage = 0;

  if (toBoolean(v_old)) {
    (v_difference = (v_new.rvalAt(v_name) - v_old.rvalAt(v_name)));
    (v_difference_percentage = (divide(v_difference * 100LL, v_old.rvalAt(v_name))));
    if (less(LINE(38,x_abs(v_difference_percentage)), 0.10000000000000001)) (v_difference_percentage = 0LL);
    else (v_difference_percentage = LINE(41,x_round(v_difference_percentage, 1LL)));
    if (less(v_difference_percentage, 0LL)) concat_assign(lval(v_new.lvalAt(v_name)), LINE(44,concat5(" (", toString(v_difference), " [", toString(v_difference_percentage), "%])")));
    else if (more(v_difference_percentage, 0LL)) concat_assign(lval(v_new.lvalAt(v_name)), LINE(46,concat5(" (+", toString(v_difference), " [", toString(v_difference_percentage), "%])")));
  }
  return v_difference_percentage;
} /* function */
/* SRC: phc-test/framework/records/common.php line 68 */
String f_get_good_color() {
  FUNCTION_INJECTION(get_good_color);
  return "style=\"color:green; font-weight: bold\"";
} /* function */
/* SRC: phc-test/framework/records/common.php line 52 */
String f_date_from_timestamp(CVarRef v_timestamp) {
  FUNCTION_INJECTION(date_from_timestamp);
  if (equal(v_timestamp, 0LL)) return "";
  return LINE(57,x_date("d M Y H:i:s ", toInt64(v_timestamp)));
} /* function */
/* SRC: phc-test/framework/records/common.php line 92 */
Variant f_get_prev_revision(CVarRef v_rev, bool v_has_benchmark_results //  = false
) {
  FUNCTION_INJECTION(get_prev_revision);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_branch;
  Variant v_data;

  (v_branch = LINE(95,f_get_branch(v_rev)));
  if (v_has_benchmark_results) {
    (v_data = (assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, toObject(gv_DB)),LINE(111,eo_1.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat5("\n\t\t\t\t\tSELECT\tcomplete.revision as rev\n\t\t\t\t\tFROM\t\tcomplete, components\n\t\t\t\t\tWHERE\t\trev < ", toString(v_rev), "\n\t\t\t\t\t\t\t\tAND branch == '", toString(v_branch), "'\n\t\t\t\t\t\t\t\tAND components.component == 'benchmark'\n\t\t\t\t\t\t\t\tAND components.revision == rev\n\t\t\t\t\t")))))),eo_0.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  }
  else {
    (v_data = (assignCallTemp(eo_2, toObject((assignCallTemp(eo_3, toObject(gv_DB)),LINE(121,eo_3.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat5("\n\t\t\t\t\tSELECT\trevision as rev\n\t\t\t\t\tFROM\t\tcomplete\n\t\t\t\t\tWHERE\t\trev < ", toString(v_rev), "\n\t\t\t\t\tAND\t\tbranch == '", toString(v_branch), "'\n\t\t\t\t\t")))))),eo_2.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  }
  LINE(124,x_rsort(ref(v_data)));
  if (same(v_data, false)) return 0LL;
  return v_data.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("rev", 0x22651263E5C98440LL);
} /* function */
/* SRC: phc-test/framework/records/common.php line 19 */
Numeric f_add_difference(CVarRef v_name, Variant v_new, CVarRef v_old) {
  FUNCTION_INJECTION(add_difference);
  Numeric v_difference = 0;

  if (toBoolean(v_old)) {
    (v_difference = v_new.rvalAt(v_name) - v_old.rvalAt(v_name));
    if (less(v_difference, 0LL)) concat_assign(lval(v_new.lvalAt(v_name)), LINE(25,concat3(" (", toString(v_difference), ")")));
    else if (more(v_difference, 0LL)) concat_assign(lval(v_new.lvalAt(v_name)), LINE(27,concat3(" (+", toString(v_difference), ")")));
  }
  return v_difference;
} /* function */
/* SRC: phc-test/framework/records/common.php line 79 */
Variant f_diff(CVarRef v_string1, CVarRef v_string2) {
  FUNCTION_INJECTION(diff);
  Variant v_result;

  if (!(LINE(81,x_extension_loaded("xdiff")))) {
    (v_result = (silenceInc(), silenceDec(LINE(83,x_dl("xdiff.so")))));
    if (!(toBoolean(v_result))) {
      return LINE(86,concat4("Note: xdiff not available for diffing. Outputting both strings:\nString1:\n", toString(v_string1), "\nString2:\n", toString(v_string2)));
    }
  }
  return LINE(89,invoke_failed("xdiff_string_diff", Array(ArrayInit(2).set(0, toString(v_string1) + toString("\n")).set(1, toString(v_string2) + toString("\n")).create()), 0x00000000DC28AE27LL));
} /* function */
/* SRC: phc-test/framework/records/common.php line 60 */
String f_minutes_from_seconds(CVarRef v_seconds) {
  FUNCTION_INJECTION(minutes_from_seconds);
  if (equal(v_seconds, 0LL)) return "";
  return concat(toString(LINE(65,x_round(divide(v_seconds, 60.0), 1LL))), "m");
} /* function */
/* SRC: phc-test/framework/records/common.php line 134 */
Variant f_get_branch(CVarRef v_rev) {
  FUNCTION_INJECTION(get_branch);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DB __attribute__((__unused__)) = g->GV(DB);
  Variant v_data;

  (v_data = (assignCallTemp(eo_0, toObject((assignCallTemp(eo_1, toObject(gv_DB)),LINE(142,eo_1.o_invoke_few_args("query", 0x356758D4414DA377LL, 1, concat3("\n\t\t\t\tSELECT\tbranch\n\t\t\t\tFROM\t\tcomplete\n\t\t\t\tWHERE\t\trevision == ", toString(v_rev), " \n\t\t\t\t")))))),eo_0.o_invoke_few_args("fetchAll", 0x5740729B65EB8E41LL, 1, throw_fatal("unknown class constant pdo::FETCH_ASSOC"))));
  if (empty(v_data.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "branch", 0x5B071EF3F7DD559FLL)) f_exit("Unknown branch");
  return v_data.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt("branch", 0x5B071EF3F7DD559FLL);
} /* function */
Variant pm_php$phc_test$framework$records$common_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/records/common.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$records$common_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_DB __attribute__((__unused__)) = (variables != gVariables) ? variables->get("DB") : g->GV(DB);

  echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html>\n\t<head>\n\t\t<title>phc -- the open source PHP compiler</title>\n\t\t");
  echo("<s");
  echo("tyle type=\"text/css\">\n\t\t\ttable.layout { width: 100%; border-collapse: collapse  }\n\t\t\ttable.info { width: 100%; }\n\t\t\ttable.info td { color: gray; background-color: #efdba7 }\n\t\t\ttable.info th { color: black; background-color: #efdba7 }\n\t\t</style>\n\t</head>\n\t<body>\n");
  (v_DB = LINE(14,create_object("pdo", Array(ArrayInit(1).set(0, "sqlite:results/results.db").create()))));
  LINE(16,f_run_main());
  echo("\t</body>\n</html>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
