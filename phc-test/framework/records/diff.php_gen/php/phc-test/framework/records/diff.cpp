
#include <php/phc-test/framework/records/common.h>
#include <php/phc-test/framework/records/diff.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/records/diff.php line 5 */
void f_run_main() {
  FUNCTION_INJECTION(run_main);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  int64 v_old_rev = 0;
  int64 v_new_rev = 0;
  Variant v_unsafe_filename;
  Variant v_sort_first;
  String v_relative_filename;
  Variant v_real_filename;
  Variant v_real_scriptname;
  String v_scriptname;
  Variant v_script_dir;
  Variant v_old_filename;
  Variant v_new_filename;
  Variant v_old;
  Variant v_split;
  Variant v_new;

  (v_old_rev = toInt64(g->gv__GET.rvalAt("old_rev", 0x0C2010FDA677AFFDLL)));
  (v_new_rev = toInt64(g->gv__GET.rvalAt("new_rev", 0x036BC557475BABBALL)));
  (v_unsafe_filename = g->gv__GET.rvalAt("filename", 0x4A5D0431E1A3CAEDLL));
  toBoolean((v_sort_first = g->gv__GET.rvalAt("sort", 0x58835B3B7416F7F1LL)));
  if (!((less(0LL, v_old_rev)))) LINE(13,f_bad());
  if (!((less(v_old_rev, v_new_rev)))) LINE(14,f_bad());
  if (!((less(v_new_rev, 5000LL)))) LINE(15,f_bad());
  (v_relative_filename = LINE(18,concat4("results/", toString(v_new_rev), "/", toString(v_unsafe_filename))));
  (v_real_filename = LINE(19,x_realpath(v_relative_filename)));
  (v_real_scriptname = LINE(22,x_realpath(get_source_filename("phc-test/framework/records/diff.php"))));
  (v_scriptname = "test/framework/records/diff.php");
  (v_script_dir = LINE(24,x_str_replace(v_scriptname, "", v_real_scriptname)));
  if (!same(LINE(27,x_strpos(toString(v_real_filename), v_script_dir)), 0LL)) LINE(28,f_bad());
  (v_old_filename = LINE(30,x_realpath(concat4("results/", toString(v_old_rev), "/", toString(v_unsafe_filename)))));
  (v_new_filename = LINE(31,x_realpath(concat4("results/", toString(v_new_rev), "/", toString(v_unsafe_filename)))));
  if (!(LINE(33,x_file_exists(toString(v_old_filename))))) f_exit("No old file");
  if (!(LINE(36,x_file_exists(toString(v_new_filename))))) f_exit("No new file");
  (v_old = LINE(39,x_file_get_contents(toString(v_old_filename))));
  if (toBoolean(v_sort_first)) {
    (v_split = LINE(42,x_split("\n", toString(v_old))));
    LINE(43,x_sort(ref(v_split)));
    (v_old = LINE(44,x_join("\n", v_split)));
  }
  (v_new = LINE(47,x_file_get_contents(toString(v_new_filename))));
  if (toBoolean(v_sort_first)) {
    (v_split = LINE(50,x_split("\n", toString(v_new))));
    LINE(51,x_sort(ref(v_split)));
    (v_new = LINE(52,x_join("\n", v_split)));
  }
  echo(LINE(56,(assignCallTemp(eo_1, toString(f_diff(v_old, v_new))),concat3("<pre>", eo_1, "</pre>\n"))));
} /* function */
/* SRC: phc-test/framework/records/diff.php line 60 */
void f_bad() {
  FUNCTION_INJECTION(bad);
  f_exit("An error has been made with input variables");
} /* function */
Variant pm_php$phc_test$framework$records$diff_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/records/diff.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$records$diff_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,pm_php$phc_test$framework$records$common_php(false, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
