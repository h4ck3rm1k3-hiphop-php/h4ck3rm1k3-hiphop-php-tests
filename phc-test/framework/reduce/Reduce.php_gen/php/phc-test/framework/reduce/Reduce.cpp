
#include <php/phc-test/framework/reduce/Reduce.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/reduce/Reduce.php line 143 */
Variant c_reduce::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_reduce::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_reduce::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_reduce::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_reduce::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_reduce::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_reduce::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_reduce::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(reduce)
ObjectData *c_reduce::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_reduce::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_reduce::cloneImpl() {
  c_reduce *obj = NEW(c_reduce)();
  cloneSet(obj);
  return obj;
}
void c_reduce::cloneSet(c_reduce *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_reduce::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_reduce::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_reduce::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_reduce$os_get(const char *s) {
  return c_reduce::os_get(s, -1);
}
Variant &cw_reduce$os_lval(const char *s) {
  return c_reduce::os_lval(s, -1);
}
Variant cw_reduce$os_constant(const char *s) {
  return c_reduce::os_constant(s);
}
Variant cw_reduce$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_reduce::os_invoke(c, s, params, -1, fatal);
}
void c_reduce::init() {
}
/* SRC: phc-test/framework/reduce/Reduce.php line 145 */
void c_reduce::t___construct() {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(148,t_set_phc("phc"));
  LINE(149,t_set_pass("AST-to-HIR"));
  LINE(150,t_set_plugin_path("."));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 157 */
void c_reduce::t_set_comment(CVarRef v_comment) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_comment);
  (o_lval("comment", 0x044C0F716BE4D0ADLL) = v_comment);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 162 */
void c_reduce::t_set_plugin_path(CStrRef v_plugin_path) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_plugin_path);
  (o_lval("plugin_path", 0x03DE16FC01A1B2C6LL) = v_plugin_path);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 167 */
void c_reduce::t_set_run_command_function(CVarRef v_callback) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_run_command_function);
  (o_lval("run_command_function", 0x71D9722F309A0E1ELL) = v_callback);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 172 */
void c_reduce::t_set_debug_function(CVarRef v_callback) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_debug_function);
  (o_lval("debug_function", 0x409D1F51E455FCECLL) = v_callback);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 177 */
void c_reduce::t_set_dump_function(CVarRef v_callback) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_dump_function);
  (o_lval("dump_function", 0x44AED8EA9E123A07LL) = v_callback);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 182 */
void c_reduce::t_set_pass(CStrRef v_passname) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_pass);
  (o_lval("pass", 0x70860AB53D1BA673LL) = v_passname);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 187 */
void c_reduce::t_set_checking_function(CVarRef v_callback) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_checking_function);
  (o_lval("checking_function", 0x5E58CB74605AAE7CLL) = v_callback);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 192 */
void c_reduce::t_set_phc(CStrRef v_phc) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::set_phc);
  (o_lval("phc", 0x67EA86E1CF55026ALL) = v_phc);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 201 */
void c_reduce::t_debug(int64 v_level, CStrRef v_message) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::debug);
  if (t___isset("debug_function")) {
    LINE(206,x_call_user_func(3, o_get("debug_function", 0x409D1F51E455FCECLL), Array(ArrayInit(2).set(0, v_level).set(1, v_message).create())));
  }
  else {
    LINE(211,t_warn_once("Warning: no user-defined debug() function provided. Consider adding one via set_debug_function ()"));
    print(LINE(213,concat4(toString(v_level), ": ", v_message, "\n")));
  }
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 218 */
Variant c_reduce::t_run_command(CStrRef v_command, CVarRef v_stdin //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::run_command);
  Variant v_result;

  if (t___isset("run_command_function")) {
    (v_result = LINE(222,x_call_user_func(3, o_get("run_command_function", 0x71D9722F309A0E1ELL), Array(ArrayInit(2).set(0, v_command).set(1, v_stdin).create()))));
    if (LINE(225,x_is_array(v_result)) && equal(LINE(226,x_count(v_result)), 3LL) && LINE(227,x_is_string(v_result.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) && LINE(228,x_is_string(v_result.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) && LINE(229,x_is_numeric(v_result.rvalAt(2LL, 0x486AFCC090D5F98CLL)))) return v_result;
    else throw_exception(((Object)(LINE(232,p_reduceexception(p_reduceexception(NEWOBJ(c_reduceexception)())->create("Result of run_command function has the wrong structure"))))));
  }
  else {
    LINE(237,t_warn_once("Warning: no user-defined run_command () function provided. Consider adding one via set_run_command_function ()"));
    throw_exception(((Object)(LINE(239,p_reduceexception(p_reduceexception(NEWOBJ(c_reduceexception)())->create("No run_command function provided"))))));
  }
  return null;
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 243 */
void c_reduce::t_warn_once(CStrRef v_message) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::warn_once);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_cache __attribute__((__unused__)) = g->sv_reduce_DupIdwarn_once_DupIdcache.lvalAt(this->o_getClassName());
  Variant &inited_sv_cache __attribute__((__unused__)) = g->inited_sv_reduce_DupIdwarn_once_DupIdcache.lvalAt(this->o_getClassName());
  if (!inited_sv_cache) {
    (sv_cache = ScalarArrays::sa_[0]);
    inited_sv_cache = true;
  }
  if (isset(sv_cache, v_message)) return;
  sv_cache.set(v_message, (true));
  LINE(250,x_trigger_error(toString(2LL) /* E_WARNING */, toInt32(v_message)));
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 254 */
void c_reduce::t_dump(CStrRef v_suffix, CVarRef v_output) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::dump);
  if (t___isset("dump_function")) {
    LINE(259,x_call_user_func(3, o_get("dump_function", 0x44AED8EA9E123A07LL), Array(ArrayInit(2).set(0, v_suffix).set(1, v_output).create())));
  }
  else {
    LINE(264,t_warn_once("Warning: no user-defined dump() function provided. Consider adding one via set_dump_function ()"));
  }
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 269 */
Variant c_reduce::t_check(CVarRef v_program) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::check);
  if (t___isset("checking_function")) {
    return LINE(273,x_call_user_func(2, o_get("checking_function", 0x5E58CB74605AAE7CLL), Array(ArrayInit(1).set(0, v_program).create())));
  }
  else {
    throw_exception(((Object)(LINE(277,p_reduceexception(p_reduceexception(NEWOBJ(c_reduceexception)())->create("No checking function present - add one using set_checking_function ()"))))));
  }
  return null;
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 287 */
Variant c_reduce::t_add_comment(CVarRef v_xprogram) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::add_comment);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  String v_command;

  if (!(t___isset("comment"))) return v_xprogram;
  LINE(292,t_debug(2LL, "Adding comment"));
  (v_command = LINE(298,(assignCallTemp(eo_0, toString(o_get("phc", 0x67EA86E1CF55026ALL)) + toString(" ")),assignCallTemp(eo_1, LINE(295,concat3("--read-xml=", toString(o_get("pass", 0x70860AB53D1BA673LL)), " "))),assignCallTemp(eo_2, LINE(296,concat3("--run=", toString(o_get("plugin_path", 0x03DE16FC01A1B2C6LL)), "/plugins/tools/add_comment.la "))),assignCallTemp(eo_3, LINE(297,concat3("--r-option=\"Reduced by: ", toString(o_get("comment", 0x044C0F716BE4D0ADLL)), "\" "))),assignCallTemp(eo_4, LINE(298,concat3("--dump-xml=", toString(o_get("plugin_path", 0x03DE16FC01A1B2C6LL)), "/plugins/tools/add_comment.la "))),concat5(eo_0, eo_1, eo_2, eo_3, eo_4))));
  return LINE(300,t_run_safe(v_command, v_xprogram));
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 303 */
Variant c_reduce::t_reduce_step(CVarRef v_xprogram, CVarRef v_start, CVarRef v_num) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::reduce_step);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant v_out;

  LINE(305,t_debug(2LL, "Reducing"));
  (v_out = (assignCallTemp(eo_0, LINE(311,(assignCallTemp(eo_2, toString(o_get("phc", 0x67EA86E1CF55026ALL))),assignCallTemp(eo_3, toString(" --read-xml=") + toString(o_get("pass", 0x70860AB53D1BA673LL))),assignCallTemp(eo_4, LINE(309,concat3(" --run=", toString(o_get("plugin_path", 0x03DE16FC01A1B2C6LL)), "/plugins/tools/reduce_statements.la"))),assignCallTemp(eo_5, LINE(310,concat4(" --r-option=", toString(v_start), ":", toString(v_num)))),assignCallTemp(eo_6, LINE(311,concat3(" --dump-xml=", toString(o_get("plugin_path", 0x03DE16FC01A1B2C6LL)), "/plugins/tools/reduce_statements.la"))),concat5(eo_2, eo_3, eo_4, eo_5, eo_6)))),assignCallTemp(eo_1, v_xprogram),LINE(312,t_run_safe(eo_0, eo_1))));
  return v_out;
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 317 */
Variant c_reduce::t_convert(CVarRef v_xprogram, int64 v_upper) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::convert);
  String v_command;

  LINE(319,t_debug(2LL, "Converting to PHP from XML"));
  (v_command = LINE(322,concat3(toString(o_get("phc", 0x67EA86E1CF55026ALL)), toString(" --read-xml=") + toString(o_get("pass", 0x70860AB53D1BA673LL)), toString(" --dump=") + toString(o_get("pass", 0x70860AB53D1BA673LL)))));
  if (toBoolean(v_upper) && equal(o_get("pass", 0x70860AB53D1BA673LL), "mir")) {
    LINE(326,t_debug(2LL, "Uppering"));
    concat_assign(v_command, " --convert-uppered");
  }
  return LINE(330,t_run_safe(v_command, v_xprogram));
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 333 */
Variant c_reduce::t_count_statements(CVarRef v_xprogram) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::count_statements);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant v_out;
  Variant v_matched;

  LINE(335,t_debug(2LL, "Counting statements"));
  (v_out = (assignCallTemp(eo_0, LINE(341,(assignCallTemp(eo_2, toString(o_get("phc", 0x67EA86E1CF55026ALL))),assignCallTemp(eo_3, LINE(339,concat3(" --read-xml=", toString(o_get("plugin_path", 0x03DE16FC01A1B2C6LL)), "/plugins/tutorials/count_statements_easy.la"))),assignCallTemp(eo_4, LINE(340,concat3(" --run=", toString(o_get("plugin_path", 0x03DE16FC01A1B2C6LL)), "/plugins/tutorials/count_statements_easy.la"))),concat3(eo_2, eo_3, eo_4)))),assignCallTemp(eo_1, v_xprogram),LINE(341,t_run_safe(eo_0, eo_1))));
  LINE(343,t_debug(2LL, toString("Output is: ") + toString(v_out)));
  if (!(toBoolean(LINE(345,x_preg_match("/(\\d+) statements found/", toString(v_out), ref(v_matched)))))) throw_exception(toObject(LINE(346,invoke_failed("reduceexception", Array(ArrayInit(1).set(0, "No statement string found").create()), 0x0000000043088A83LL))));
  return v_matched.rvalAt(1LL, 0x5BCA7C69B794F8CELL);
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 352 */
bool c_reduce::t_has_syntax_errors(CVarRef v_program) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::has_syntax_errors);
  Variant eo_0;
  Variant eo_1;
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  LINE(354,t_debug(2LL, "Checking syntax errors"));
  df_lambda_1(LINE(355,t_run_command("php -l", v_program)), v_out, v_err, v_exit);
  if (toBoolean(v_exit) || toBoolean(v_err)) {
    (assignCallTemp(eo_1, concat("Syntax error detected: Skip. (out: ", LINE(359,concat6(toString(v_out), ", exit code: ", toString(v_exit), ", error: ", toString(v_err), ")")))),t_debug(1LL, eo_1));
    return true;
  }
  return false;
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 368 */
Variant c_reduce::t_run_safe(CStrRef v_command, CVarRef v_stdin //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::run_safe);
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  df_lambda_2(LINE(370,t_run_command(v_command, v_stdin)), v_out, v_err, v_exit);
  if (!same(v_exit, 0LL) || !same(v_err, "")) throw_exception(((Object)(LINE(373,p_reduceexception(p_reduceexception(NEWOBJ(c_reduceexception)())->create(concat4("Error (", toString(v_exit), "): ", toString(v_err))))))));
  return v_out;
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 383 */
Variant c_reduce::t_do_main_step(CVarRef v_xprogram, CVarRef v_start, CVarRef v_num) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::do_main_step);
  Variant eo_0;
  Variant eo_1;
  Variant v_xnew_program;
  Variant v_pnew_program;
  Variant v_unew_program;
  String v_id;

  lval(o_lval("num_steps", 0x4B4F58D8D51D6452LL))++;
  (assignCallTemp(eo_1, LINE(388,concat4("Attempting to reduce by ", toString(v_num), " statements, starting at statement ", toString(v_start)))),t_debug(1LL, eo_1));
  (v_xnew_program = LINE(389,t_reduce_step(v_xprogram, v_start, v_num)));
  (v_pnew_program = LINE(390,t_convert(v_xnew_program, 0LL)));
  (v_unew_program = LINE(391,t_convert(v_xnew_program, 1LL)));
  (v_id = LINE(393,concat3(toString(v_num), "_", toString(v_start))));
  LINE(394,t_dump(toString("xreduced_") + v_id, v_xnew_program));
  LINE(395,t_dump(toString("preduced_") + v_id, v_pnew_program));
  LINE(396,t_dump(toString("ureduced_") + v_id, v_unew_program));
  if (equal(v_xprogram, v_xnew_program) && !equal(v_num, 0LL)) {
    LINE(402,t_debug(1LL, "The two programs are identical. Skip."));
    return false;
  }
  if (LINE(406,t_has_syntax_errors(v_unew_program))) return false;
  if (toBoolean(LINE(411,t_check(v_unew_program)))) {
    LINE(413,t_debug(2LL, "Success, bug kept in"));
    LINE(414,t_dump(toString("xsuccess_") + v_id, v_xnew_program));
    LINE(415,t_dump(toString("psuccess_") + v_id, v_pnew_program));
    LINE(416,t_dump(toString("usuccess_") + v_id, v_unew_program));
    return v_xnew_program;
  }
  LINE(420,t_debug(2LL, "Bug removed. Skip."));
  return false;
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 427 */
Variant c_reduce::t_run_on_php(CVarRef v_program) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::run_on_php);
  String v_command;
  Variant v_out;

  LINE(429,t_debug(2LL, "Getting initial XML input"));
  (v_command = LINE(431,concat3(toString(o_get("phc", 0x67EA86E1CF55026ALL)), " --dump-xml=", toString(o_get("pass", 0x70860AB53D1BA673LL)))));
  (v_out = LINE(432,t_run_safe(v_command, v_program)));
  if (!equal(LINE(434,x_substr(toString(v_out), toInt32(0LL), toInt32(5LL))), "<\?xml")) throw_exception(((Object)(LINE(435,p_reduceexception(p_reduceexception(NEWOBJ(c_reduceexception)())->create(toString("Cannot convert input file into XML: ") + toString(v_out)))))));
  return LINE(437,t_run_on_xml(v_out));
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 440 */
Variant c_reduce::t_run_on_xml(Variant v_xprogram) {
  INSTANCE_METHOD_INJECTION(Reduce, Reduce::run_on_xml);
  Variant eo_0;
  Variant eo_1;
  Variant v_N;
  Variant v_original;
  Variant v_k;
  Variant v_i;
  Variant v_result;
  Variant v_pprogram;

  (o_lval("num_steps", 0x4B4F58D8D51D6452LL) = 0LL);
  (v_N = LINE(444,t_count_statements(v_xprogram)));
  (v_original = v_N);
  LINE(447,t_debug(1LL, toString(v_N) + toString(" statements")));
  if (equal(v_N, 0LL)) throw_exception(((Object)(LINE(450,p_reduceexception(p_reduceexception(NEWOBJ(c_reduceexception)())->create("No statements found"))))));
  if (!(toBoolean(LINE(454,t_do_main_step(v_xprogram, 0LL, 0LL))))) throw_exception(((Object)(LINE(455,p_reduceexception(p_reduceexception(NEWOBJ(c_reduceexception)())->create("Program does not appear to have a bug"))))));
  {
    LOOP_COUNTER(1);
    for ((v_k = toInt64((divide(v_N, 2LL)))); not_less(v_k, 1LL); (v_k = toInt64((divide(v_k, 2LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for ((v_i = 0LL); not_more(v_i, (v_N - v_k)); v_i += v_k) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_result = LINE(463,t_do_main_step(v_xprogram, v_i, v_k)));
              if (!same(v_result, false)) {
                (v_xprogram = v_result);
                (v_N = LINE(467,t_count_statements(v_xprogram)));
                (v_k = v_N);
                (assignCallTemp(eo_1, LINE(469,concat3("Success, program reduced to ", toString(v_N), " statements"))),t_debug(1LL, eo_1));
                LINE(470,t_debug(1LL, ""));
                goto continue1;
              }
              LINE(473,t_debug(1LL, ""));
            }
          }
        }
      }
      continue1:;
    }
  }
  (assignCallTemp(eo_1, concat("Reduced from ", LINE(478,concat6(toString(v_original), " to ", toString(v_N), " statements in ", toString(o_get("num_steps", 0x4B4F58D8D51D6452LL)), " steps.")))),t_debug(0LL, eo_1));
  (v_xprogram = LINE(480,t_add_comment(v_xprogram)));
  (v_pprogram = LINE(481,t_convert(v_xprogram, 0LL)));
  LINE(483,t_dump("reduced", v_pprogram));
  LINE(484,t_dump("xreduced", v_xprogram));
  return v_pprogram;
} /* function */
/* SRC: phc-test/framework/reduce/Reduce.php line 135 */
Variant c_reduceexception::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_reduceexception::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_reduceexception::o_get(ArrayElementVec &props) const {
  c_exception::o_get(props);
}
bool c_reduceexception::o_exists(CStrRef s, int64 hash) const {
  return c_exception::o_exists(s, hash);
}
Variant c_reduceexception::o_get(CStrRef s, int64 hash) {
  return c_exception::o_get(s, hash);
}
Variant c_reduceexception::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_reduceexception::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_reduceexception::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(reduceexception)
ObjectData *c_reduceexception::create(Variant v_message) {
  init();
  t___construct(v_message);
  return this;
}
ObjectData *c_reduceexception::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_reduceexception::cloneImpl() {
  c_reduceexception *obj = NEW(c_reduceexception)();
  cloneSet(obj);
  return obj;
}
void c_reduceexception::cloneSet(c_reduceexception *clone) {
  c_exception::cloneSet(clone);
}
Variant c_reduceexception::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_reduceexception::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_reduceexception::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_reduceexception$os_get(const char *s) {
  return c_reduceexception::os_get(s, -1);
}
Variant &cw_reduceexception$os_lval(const char *s) {
  return c_reduceexception::os_lval(s, -1);
}
Variant cw_reduceexception$os_constant(const char *s) {
  return c_reduceexception::os_constant(s);
}
Variant cw_reduceexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_reduceexception::os_invoke(c, s, params, -1, fatal);
}
void c_reduceexception::init() {
  c_exception::init();
}
/* SRC: phc-test/framework/reduce/Reduce.php line 137 */
void c_reduceexception::t___construct(Variant v_message) {
  INSTANCE_METHOD_INJECTION(ReduceException, ReduceException::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(139,c_exception::t___construct(v_message));
  gasInCtor(oldInCtor);
} /* function */
Object co_reduce(CArrRef params, bool init /* = true */) {
  return Object(p_reduce(NEW(c_reduce)())->dynCreate(params, init));
}
Object co_reduceexception(CArrRef params, bool init /* = true */) {
  return Object(p_reduceexception(NEW(c_reduceexception)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$reduce$Reduce_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/reduce/Reduce.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$reduce$Reduce_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
