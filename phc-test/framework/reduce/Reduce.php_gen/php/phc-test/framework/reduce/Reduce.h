
#ifndef __GENERATED_php_phc_test_framework_reduce_Reduce_h__
#define __GENERATED_php_phc_test_framework_reduce_Reduce_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/reduce/Reduce.fw.h>

// Declarations
#include <cls/reduce.h>
#include <cls/reduceexception.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$reduce$Reduce_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_reduce(CArrRef params, bool init = true);
Object co_reduceexception(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_reduce_Reduce_h__
