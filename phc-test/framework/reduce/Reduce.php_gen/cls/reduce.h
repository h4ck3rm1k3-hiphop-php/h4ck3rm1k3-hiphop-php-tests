
#ifndef __GENERATED_cls_reduce_h__
#define __GENERATED_cls_reduce_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/reduce/Reduce.php line 143 */
class c_reduce : virtual public ObjectData {
  BEGIN_CLASS_MAP(reduce)
  END_CLASS_MAP(reduce)
  DECLARE_CLASS(reduce, Reduce, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_set_comment(CVarRef v_comment);
  public: void t_set_plugin_path(CStrRef v_plugin_path);
  public: void t_set_run_command_function(CVarRef v_callback);
  public: void t_set_debug_function(CVarRef v_callback);
  public: void t_set_dump_function(CVarRef v_callback);
  public: void t_set_pass(CStrRef v_passname);
  public: void t_set_checking_function(CVarRef v_callback);
  public: void t_set_phc(CStrRef v_phc);
  public: void t_debug(int64 v_level, CStrRef v_message);
  public: Variant t_run_command(CStrRef v_command, CVarRef v_stdin = null_variant);
  public: void t_warn_once(CStrRef v_message);
  public: void t_dump(CStrRef v_suffix, CVarRef v_output);
  public: Variant t_check(CVarRef v_program);
  public: Variant t_add_comment(CVarRef v_xprogram);
  public: Variant t_reduce_step(CVarRef v_xprogram, CVarRef v_start, CVarRef v_num);
  public: Variant t_convert(CVarRef v_xprogram, int64 v_upper);
  public: Variant t_count_statements(CVarRef v_xprogram);
  public: bool t_has_syntax_errors(CVarRef v_program);
  public: Variant t_run_safe(CStrRef v_command, CVarRef v_stdin = null_variant);
  public: Variant t_do_main_step(CVarRef v_xprogram, CVarRef v_start, CVarRef v_num);
  public: Variant t_run_on_php(CVarRef v_program);
  public: Variant t_run_on_xml(Variant v_xprogram);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_reduce_h__
