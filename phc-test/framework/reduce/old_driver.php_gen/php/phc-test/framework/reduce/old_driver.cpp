
#include <php/phc-test/framework/reduce/Reduce.h>
#include <php/phc-test/framework/reduce/old_driver.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_6(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_7(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_10(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/reduce/old_driver.php line 188 */
void f_mydump(CVarRef v_suffix, CVarRef v_output) {
  FUNCTION_INJECTION(mydump);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_filename __attribute__((__unused__)) = g->GV(filename);
  if (LINE(191,f_above_debug_threshold(3LL))) {
    LINE(193,(assignCallTemp(eo_1, concat3("Dumping to ", toString(gv_filename), ".suffix")),f_mydebug(3LL, eo_1)));
    LINE(194,(assignCallTemp(eo_0, concat3(toString(gv_filename), ".", toString(v_suffix))),assignCallTemp(eo_1, v_output),x_file_put_contents(eo_0, eo_1)));
  }
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 180 */
void f_mydebug(CVarRef v_level, CVarRef v_message) {
  FUNCTION_INJECTION(mydebug);
  if (LINE(182,f_above_debug_threshold(v_level))) {
    print(LINE(184,concat4(toString(v_level), ": ", toString(v_message), "\n")));
  }
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 295 */
Variant f_run_with_phc(CVarRef v_program, Variant v_filename) {
  FUNCTION_INJECTION(run_with_phc);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_pass __attribute__((__unused__)) = g->GV(opt_pass);
  Variant &gv_opt_main_command __attribute__((__unused__)) = g->GV(opt_main_command);
  String v_command;
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  {
  }
  LINE(300,f_mydebug(2LL, "Getting compiled phc output"));
  (v_command = LINE(302,concat4("src/phc --read-xml=", toString(gv_opt_pass), " -e ", toString(gv_opt_main_command))));
  df_lambda_8(LINE(303,f_myrun(v_command, v_program)), v_out, v_err, v_exit);
  if (equal(v_out, "Timeout")) {
    LINE(307,f_mydebug(1LL, "Timed out"));
    return false;
  }
  (v_out = LINE(311,invoke_failed("homogenize_all", Array(ArrayInit(2).set(0, ref(v_out)).set(1, ref(v_filename)).create()), 0x00000000E9837FAELL)));
  return Array(ArrayInit(3).set(0, v_out).set(1, v_err).set(2, v_exit).create());
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 268 */
Variant f_run_with_php(CVarRef v_program) {
  FUNCTION_INJECTION(run_with_php);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_zero __attribute__((__unused__)) = g->GV(opt_zero);
  Variant &gv_filename __attribute__((__unused__)) = g->GV(filename);
  Variant v_command;
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  {
  }
  LINE(273,f_mydebug(2LL, "Getting PHP output"));
  (v_command = LINE(275,invoke_failed("get_php_command_line", Array(ArrayInit(2).set(0, ref(gv_filename)).set(1, true).create()), 0x0000000033B2020BLL)));
  df_lambda_7(LINE(276,f_myrun(v_command, v_program)), v_out, v_err, v_exit);
  (v_out = LINE(278,invoke_failed("homogenize_all", Array(ArrayInit(2).set(0, ref(v_out)).set(1, ref(gv_filename)).create()), 0x00000000E9837FAELL)));
  if (toBoolean(gv_opt_zero) && toBoolean(v_exit)) {
    LINE(282,(assignCallTemp(eo_1, concat("Exit code not zero, skip. (out: ", concat6(toString(v_out), ", exit code: ", toString(v_exit), ", error: ", toString(v_err), ")"))),f_mydebug(1LL, eo_1)));
    return false;
  }
  if (equal(v_out, "Timeout")) {
    LINE(288,f_mydebug(1LL, "Timed out"));
    return false;
  }
  return Array(ArrayInit(3).set(0, v_out).set(1, v_err).set(2, v_exit).create());
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 244 */
Variant f_interpret_with_phc(CVarRef v_program) {
  FUNCTION_INJECTION(interpret_with_phc);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_pass __attribute__((__unused__)) = g->GV(opt_pass);
  Variant &gv_opt_main_command __attribute__((__unused__)) = g->GV(opt_main_command);
  Variant &gv_opt_interpret __attribute__((__unused__)) = g->GV(opt_interpret);
  Variant &gv_filename __attribute__((__unused__)) = g->GV(filename);
  String v_command;
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  {
  }
  LINE(251,f_mydebug(2LL, "Getting compiled phc output"));
  (v_command = LINE(253,concat4("src/phc --dump=", toString(gv_opt_interpret), " ", toString(gv_opt_main_command))));
  df_lambda_5(LINE(254,f_myrun(v_command, v_program)), v_out, v_err, v_exit);
  if (!same(v_err, "") || !same(v_exit, 0LL)) {
    LINE(258,(assignCallTemp(eo_1, concat4("Error interpreting (", toString(v_exit), "), skip: ", toString(v_err))),f_mydebug(1LL, eo_1)));
    return false;
  }
  df_lambda_6(LINE(262,(assignCallTemp(eo_0, invoke_failed("get_php_command_line", Array(ArrayInit(2).set(0, ref(gv_filename)).set(1, true).create()), 0x0000000033B2020BLL)),assignCallTemp(eo_1, v_out),f_myrun(eo_0, eo_1))), v_out, v_err, v_exit);
  (v_out = LINE(263,invoke_failed("homogenize_all", Array(ArrayInit(2).set(0, ref(v_out)).set(1, ref(gv_filename)).create()), 0x00000000E9837FAELL)));
  return Array(ArrayInit(3).set(0, v_out).set(1, v_err).set(2, v_exit).create());
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 198 */
Variant f_failure_check(Variant v_program) {
  FUNCTION_INJECTION(failure_check);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_pass __attribute__((__unused__)) = g->GV(opt_pass);
  Variant &gv_opt_failure __attribute__((__unused__)) = g->GV(opt_failure);
  Variant v_out;
  Variant v_err;
  Variant v_exit;

  throw_exception(((Object)(LINE(200,p_exception(p_exception(NEWOBJ(c_exception)())->create("TODO- what does this do\?"))))));
  {
  }
  LINE(204,f_mydebug(2LL, "Getting output when run with failure plugin"));
  df_lambda_2(LINE(205,(assignCallTemp(eo_0, concat4("src/phc --read-xml=", toString(gv_opt_pass), " --run=", toString(gv_opt_failure))),assignCallTemp(eo_1, ref(v_program)),invoke_failed("run", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ACE9D919LL))), v_out, v_err, v_exit);
  if (toBoolean(LINE(206,x_preg_match("/Failure$/", toString(v_out))))) {
    LINE(208,f_mydebug(2LL, "Success, bug kept in"));
    return true;
  }
  else return false;
  return null;
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 316 */
bool f_check_against_compiled(CVarRef v_program) {
  FUNCTION_INJECTION(check_against_compiled);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_filename __attribute__((__unused__)) = g->GV(filename);
  Variant v_result;
  Variant v_php_out;
  Variant v_php_err;
  Variant v_php_exit;
  Variant v_phc_out;
  Variant v_phc_err;
  Variant v_phc_exit;

  (v_result = LINE(320,invoke_too_many_args("run_with_php", (1), ((f_run_with_php(v_program))))));
  if (equal(v_result, false)) return false;
  df_lambda_9(v_result, v_php_out, v_php_err, v_php_exit);
  (v_result = LINE(328,f_run_with_phc(v_program, gv_filename)));
  if (equal(v_result, false)) return false;
  df_lambda_10(v_result, v_phc_out, v_phc_err, v_phc_exit);
  if (!equal(v_phc_out, v_php_out) || !equal(v_phc_err, v_php_err) || !equal(v_phc_exit, v_php_exit)) {
    LINE(339,f_mydebug(1LL, "Outputs differ"));
    return true;
  }
  LINE(343,f_mydebug(1LL, "Outputs are the same"));
  return false;
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 153 */
Variant f_myrun(Variant v_command, Variant v_stdin //  = null
) {
  FUNCTION_INJECTION(myrun);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_verbose __attribute__((__unused__)) = g->GV(opt_verbose);
  Variant v_old;
  Variant v_result;

  if (same(v_stdin, null)) LINE(156,f_mydebug(2LL, toString("Running command: ") + toString(v_command)));
  else LINE(158,f_mydebug(2LL, toString("Running command with stdin: ") + toString(v_command)));
  (v_old = gv_opt_verbose);
  (gv_opt_verbose = false);
  (v_result = LINE(165,invoke_failed("complete_exec", Array(ArrayInit(2).set(0, ref(v_command)).set(1, ref(v_stdin)).create()), 0x00000000739F6DDCLL)));
  (gv_opt_verbose = v_old);
  if (equal(v_result.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "Timeout")) LINE(169,f_mydebug(2LL, "Command timed out"));
  return v_result;
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 174 */
bool f_above_debug_threshold(CVarRef v_level) {
  FUNCTION_INJECTION(above_debug_threshold);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_verbose __attribute__((__unused__)) = g->GV(opt_verbose);
  return (not_more(v_level, gv_opt_verbose));
} /* function */
/* SRC: phc-test/framework/reduce/old_driver.php line 217 */
bool f_check_against_interpreted(CVarRef v_program) {
  FUNCTION_INJECTION(check_against_interpreted);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_filename __attribute__((__unused__)) = g->GV(filename);
  Variant v_result;
  Variant v_php_out;
  Variant v_php_err;
  Variant v_php_exit;
  Variant v_phc_out;
  Variant v_phc_err;
  Variant v_phc_exit;

  (v_result = LINE(221,invoke_too_many_args("run_with_php", (1), ((f_run_with_php(v_program))))));
  if (equal(v_result, false)) return false;
  df_lambda_3(v_result, v_php_out, v_php_err, v_php_exit);
  (v_result = LINE(229,invoke_too_many_args("interpret_with_phc", (1), ((f_interpret_with_phc(v_program))))));
  df_lambda_4(v_result, v_phc_out, v_phc_err, v_phc_exit);
  if (!equal(v_phc_out, v_php_out) || !equal(v_phc_err, v_php_err) || !equal(v_phc_exit, v_php_exit)) {
    LINE(236,f_mydebug(1LL, "Outputs differ"));
    return true;
  }
  LINE(240,f_mydebug(1LL, "Outputs are the same"));
  return false;
} /* function */
Variant i_mydump(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3984347A2341A136LL, mydump) {
    return (f_mydump(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_mydebug(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5C6ED1495733BD60LL, mydebug) {
    return (f_mydebug(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_check_against_compiled(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x20226F8C22D1EC85LL, check_against_compiled) {
    return (f_check_against_compiled(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_myrun(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2C5590207B94C979LL, myrun) {
    int count = params.size();
    if (count <= 1) return (f_myrun(params.rvalAt(0)));
    return (f_myrun(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_check_against_interpreted(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6892B4AA317D50FELL, check_against_interpreted) {
    return (f_check_against_interpreted(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$framework$reduce$old_driver_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/reduce/old_driver.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$reduce$old_driver_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php") : g->GV(php);
  Variant &v_cg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cg") : g->GV(cg);
  Variant &v_command_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("command_line") : g->GV(command_line);
  Variant &v_opt_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_result") : g->GV(opt_result);
  Variant &v_opts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opts") : g->GV(opts);
  Variant &v_arguments __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arguments") : g->GV(arguments);
  Variant &v_opt_verbose __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_verbose") : g->GV(opt_verbose);
  Variant &v_opt_help __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_help") : g->GV(opt_help);
  Variant &v_opt_command __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_command") : g->GV(opt_command);
  Variant &v_opt_main_command __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_main_command") : g->GV(opt_main_command);
  Variant &v_opt_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_pass") : g->GV(opt_pass);
  Variant &v_opt_xml __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_xml") : g->GV(opt_xml);
  Variant &v_opt_interpret __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_interpret") : g->GV(opt_interpret);
  Variant &v_opt_dont_upper __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_dont_upper") : g->GV(opt_dont_upper);
  Variant &v_opt_upper __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_upper") : g->GV(opt_upper);
  Variant &v_opt_failure __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_failure") : g->GV(opt_failure);
  Variant &v_opt_zero __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_zero") : g->GV(opt_zero);
  Variant &v_opt_phc_prefix __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_phc_prefix") : g->GV(opt_phc_prefix);
  Variant &v_opt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt") : g->GV(opt);
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_reduce __attribute__((__unused__)) = (variables != gVariables) ? variables->get("reduce") : g->GV(reduce);
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);

  (v_php = "php");
  LINE(6,x_set_include_path(concat("test/framework/external/:test/framework/", x_get_include_path())));
  LINE(8,require("lib/header.php", true, variables, "phc-test/framework/reduce/"));
  LINE(9,require("Console/Getopt.php", true, variables, "phc-test/framework/reduce/"));
  LINE(11,pm_php$phc_test$framework$reduce$Reduce_php(true, variables));
  (v_cg = LINE(14,create_object("console_getopt", Array())));
  (v_command_line = LINE(15,(assignCallTemp(eo_1, v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)),x_join(" ", eo_1))));
  (v_opt_result = (assignCallTemp(eo_0, ref(LINE(16,v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)))),v_cg.o_invoke("getopt", Array(ArrayInit(2).set(0, eo_0).set(1, "vhc:rdDf:i:UuF:ZP:m:p:").create()), 0x2C6826999658AA5ELL)));
  if (!(LINE(17,x_is_array(v_opt_result)))) f_exit(concat(toString(v_opt_result.o_get("message", 0x3EAA4B97155366DFLL)), "\n"));
  df_lambda_1(v_opt_result, v_opts, v_arguments);
  (v_opt_verbose = 0LL);
  (v_opt_help = false);
  (v_opt_command = "");
  (v_opt_main_command = "");
  (v_opt_pass = "AST-to-HIR");
  setNull(v_opt_xml);
  (v_opt_interpret = false);
  (v_opt_dont_upper = false);
  (v_opt_upper = false);
  (v_opt_failure = false);
  (v_opt_zero = true);
  (v_opt_phc_prefix = ".");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_opts.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_opt = iter3->second();
      {
        {
          Variant tmp5 = (v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          int tmp6 = -1;
          if (equal(tmp5, ("h"))) {
            tmp6 = 0;
          } else if (equal(tmp5, ("v"))) {
            tmp6 = 1;
          } else if (equal(tmp5, ("U"))) {
            tmp6 = 2;
          } else if (equal(tmp5, ("u"))) {
            tmp6 = 3;
          } else if (equal(tmp5, ("c"))) {
            tmp6 = 4;
          } else if (equal(tmp5, ("m"))) {
            tmp6 = 5;
          } else if (equal(tmp5, ("x"))) {
            tmp6 = 6;
          } else if (equal(tmp5, ("i"))) {
            tmp6 = 7;
          } else if (equal(tmp5, ("F"))) {
            tmp6 = 8;
          } else if (equal(tmp5, ("Z"))) {
            tmp6 = 9;
          } else if (equal(tmp5, ("P"))) {
            tmp6 = 10;
          } else if (equal(tmp5, ("p"))) {
            tmp6 = 11;
          }
          switch (tmp6) {
          case 0:
            {
              (v_opt_help = true);
              goto break4;
            }
          case 1:
            {
              v_opt_verbose += 1LL;
              goto break4;
            }
          case 2:
            {
              (v_opt_dont_upper = true);
              goto break4;
            }
          case 3:
            {
              (v_opt_upper = true);
              goto break4;
            }
          case 4:
            {
              concat_assign(v_opt_command, toString(v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
              goto break4;
            }
          case 5:
            {
              concat_assign(v_opt_main_command, toString(v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
              goto break4;
            }
          case 6:
            {
              (v_opt_xml = v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
              goto break4;
            }
          case 7:
            {
              (v_opt_interpret = v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
              goto break4;
            }
          case 8:
            {
              (v_opt_failure = v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
              goto break4;
            }
          case 9:
            {
              (v_opt_zero = true);
              goto break4;
            }
          case 10:
            {
              (v_opt_phc_prefix = v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
              goto break4;
            }
          case 11:
            {
              (v_opt_pass = v_opt.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
              goto break4;
            }
          }
          break4:;
        }
      }
    }
  }
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_opts.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_opt = iter9->second();
      {
        {
          Variant tmp11 = (v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          int tmp12 = -1;
          if (equal(tmp11, ("r"))) {
            tmp12 = 0;
          } else if (equal(tmp11, ("d"))) {
            tmp12 = 1;
          } else if (equal(tmp11, ("D"))) {
            tmp12 = 2;
          }
          switch (tmp12) {
          case 0:
            {
              concat_assign(v_opt_command, LINE(55,concat5("--run=", toString(v_opt_phc_prefix), "/plugins/tools/debug_zval.la --dump-xml=", toString(v_opt_phc_prefix), "/plugins/tools/debug_zval.la")));
              goto break10;
            }
          case 1:
            {
              concat_assign(v_opt_command, LINE(56,concat5("--run=", toString(v_opt_phc_prefix), "/plugins/tools/demi_eval.la --r-option=true --dump-xml=", toString(v_opt_phc_prefix), "/plugins/tools/demi_eval.la")));
              goto break10;
            }
          case 2:
            {
              concat_assign(v_opt_command, LINE(57,concat5("--run=", toString(v_opt_phc_prefix), "/plugins/tools/demi_eval.la --r-option=false --dump-xml=", toString(v_opt_phc_prefix), "/plugins/tools/demi_eval.la")));
              goto break10;
            }
          }
          break10:;
        }
      }
    }
  }
  if (toBoolean(v_opt_help) || equal(LINE(61,x_count(v_arguments)), 0LL)) {
    f_exit(toString("reduce - A program to automatically reduce test cases for phc - phpcompiler.org\n\nUsage: reduce [OPTIONS] filename\n\nOptions:\n    -h     Print this help message\n    -v     Verbose. Can be set more than once. Set once, prints actions; set \n           twice, prints commands executed; set thrice, dumps results of \n           intermediate stages.\n    -p     Mnemoic: pass. Perform the reduction at the specified pass.\n    -P     Since src/phc is broken, another version can be specified to do the\n           conversion steps, with this prefix.\n    -c     Pass as arguments to the first phc. Be sure to include an --dump-xml\n           command, or nothing will happen.\n    -m     Pass as arguments to the main phc. Default \"\"\n    -r     Mnemonic: refcount. The equivalent of \"-c--run\n           plugins/tools/debug_zval.la -dump-xml=plugins/tools/debug_zval.la\".\n    -d     The equivalent of \"-c--run=plugins/tools/demi_eval.la\n           -dump-xml=plugins/tools/demi_eval.la --r-option=true\".\n    -D     The equivalent of \"-c--run=plugins/tools/demi_eval.la\n           -dump-xml=plugins/tools/demi_eval.la --r-option=false\".\n    -x     Use the passed XML file as the the first program, instead of calling\n           phc.\n    -i     Mnemonic: Interpret. Instead of compiling the script, dump it at the\n           specified pass and interpret it.\n    -U     Don't print 'upper' debug files (not related to -u)\n    -u     Used with -i, 'upper's before dumping (not related to -U)\n    -F     Mnemonic: Failure. Used for plugins which print \"Failure\" to\n           indicate error. Run phc with the plugin to determine failure.\n    -Z     Require script to return zero when run through interpreter\n\nSample commands:\n    reduce -v test/subjects/codegen/0001.php\n    reduce \"-c--run=plugins/tools/debug_zval.la -dump-xml=plugins/tools/debug_zval.la\" test/subjects/codegen/0001.php\n    reduce -v 0001.uhir.php\n    reduce -F plugins/tests/limit_assignments.la test/subjects/codegen/bench_simple.php\n"));
  }
  (v_filename = v_arguments.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (v_reduce = ((Object)(LINE(107,invoke_too_many_args("reduce", (1), ((p_reduce(p_reduce(NEWOBJ(c_reduce)())->create()))))))));
  LINE(109,v_reduce.o_invoke_few_args("set_phc", 0x7FBC945FCF50C511LL, 1, "src/phc"));
  LINE(110,v_reduce.o_invoke_few_args("set_comment", 0x0754B3C9F0032207LL, 1, v_command_line));
  LINE(111,v_reduce.o_invoke_few_args("set_plugin_path", 0x6F3B8F7AF4181F39LL, 1, v_opt_phc_prefix));
  LINE(112,v_reduce.o_invoke_few_args("set_run_command_function", 0x0D01C969891A2699LL, 1, "myrun"));
  LINE(113,v_reduce.o_invoke_few_args("set_debug_function", 0x066D22D85BB455E7LL, 1, "mydebug"));
  LINE(114,v_reduce.o_invoke_few_args("set_dump_function", 0x4C2A0F706A2223A8LL, 1, "mydump"));
  LINE(117,v_reduce.o_invoke_few_args("set_checking_function", 0x541CE80583348E0ALL, 1, "check_against_compiled"));
  if (toBoolean(v_opt_failure)) LINE(120,v_reduce.o_invoke_few_args("set_checking_function", 0x541CE80583348E0ALL, 1, "check_for_failure_string"));
  if (toBoolean(v_opt_interpret)) LINE(123,v_reduce.o_invoke_few_args("set_checking_function", 0x541CE80583348E0ALL, 1, "check_against_interpreted"));
  (v_input = LINE(126,x_file_get_contents(toString(v_filename))));
  if (!equal(v_opt_command, "")) {
    throw_exception(((Object)(LINE(133,p_exception(p_exception(NEWOBJ(c_exception)())->create("TODO"))))));
  }
  if (toBoolean(v_opt_xml)) {
    (v_result = LINE(138,v_reduce.o_invoke_few_args("run_on_xml", 0x5848B799E4635A0CLL, 1, v_input)));
  }
  else {
    (v_result = LINE(142,v_reduce.o_invoke_few_args("run_on_php", 0x6A40D7C11ADAF4FFLL, 1, v_input)));
  }
  print(LINE(145,concat3("Writing reduced file to ", toString(v_filename), ".reduced\n")));
  LINE(146,x_file_put_contents(toString(v_filename) + toString(".reduced"), v_result));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
