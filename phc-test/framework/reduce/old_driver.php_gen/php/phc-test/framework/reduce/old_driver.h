
#ifndef __GENERATED_php_phc_test_framework_reduce_old_driver_h__
#define __GENERATED_php_phc_test_framework_reduce_old_driver_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/reduce/old_driver.fw.h>

// Declarations
#include <php/phc-test/framework/reduce/Reduce.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_mydump(CVarRef v_suffix, CVarRef v_output);
void f_mydebug(CVarRef v_level, CVarRef v_message);
Variant f_run_with_phc(CVarRef v_program, Variant v_filename);
Variant pm_php$phc_test$framework$reduce$old_driver_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_run_with_php(CVarRef v_program);
Variant f_interpret_with_phc(CVarRef v_program);
Variant f_failure_check(Variant v_program);
bool f_check_against_compiled(CVarRef v_program);
Variant f_myrun(Variant v_command, Variant v_stdin = null);
bool f_above_debug_threshold(CVarRef v_level);
bool f_check_against_interpreted(CVarRef v_program);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_reduce_old_driver_h__
