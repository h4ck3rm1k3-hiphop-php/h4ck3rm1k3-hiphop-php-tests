
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_mydebug(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_check_against_compiled(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_mydump(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_myrun(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_check_against_interpreted(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[16])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 16; i++) funcTable[i] = &invoke_builtin;
    funcTable[0] = &i_mydebug;
    funcTable[5] = &i_check_against_compiled;
    funcTable[6] = &i_mydump;
    funcTable[9] = &i_myrun;
    funcTable[14] = &i_check_against_interpreted;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 15](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
