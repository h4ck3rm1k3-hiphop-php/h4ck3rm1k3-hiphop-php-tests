
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      HASH_INITIALIZED(0x4CB3E826DC85CA81LL, g->GV(opt_upper),
                       opt_upper);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 7:
      HASH_INITIALIZED(0x4E80F79695D41A07LL, g->GV(opt),
                       opt);
      break;
    case 12:
      HASH_INITIALIZED(0x21DEE4EB204AE10CLL, g->GV(cg),
                       cg);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 27:
      HASH_INITIALIZED(0x2FF01C2058B0B21BLL, g->GV(opt_verbose),
                       opt_verbose);
      break;
    case 29:
      HASH_INITIALIZED(0x37E66094A8A9409DLL, g->GV(reduce),
                       reduce);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 38:
      HASH_INITIALIZED(0x3CC816D24426A726LL, g->GV(opt_main_command),
                       opt_main_command);
      break;
    case 52:
      HASH_INITIALIZED(0x421780730E09F634LL, g->GV(opts),
                       opts);
      break;
    case 53:
      HASH_INITIALIZED(0x18FB4A2A9180C435LL, g->GV(opt_interpret),
                       opt_interpret);
      break;
    case 59:
      HASH_INITIALIZED(0x67DB402A3712D03BLL, g->GV(opt_result),
                       opt_result);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    case 65:
      HASH_INITIALIZED(0x52B182B0A94E9C41LL, g->GV(php),
                       php);
      break;
    case 70:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 72:
      HASH_INITIALIZED(0x599E3F67E2599248LL, g->GV(result),
                       result);
      break;
    case 73:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 75:
      HASH_INITIALIZED(0x1018D4B85360194BLL, g->GV(opt_help),
                       opt_help);
      HASH_INITIALIZED(0x1532499416EFFACBLL, g->GV(input),
                       input);
      break;
    case 76:
      HASH_INITIALIZED(0x07CCA5D7E3548D4CLL, g->GV(opt_failure),
                       opt_failure);
      break;
    case 81:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 92:
      HASH_INITIALIZED(0x0F60C6595ED9FADCLL, g->GV(arguments),
                       arguments);
      break;
    case 94:
      HASH_INITIALIZED(0x4836EF4A1D6F465ELL, g->GV(opt_command),
                       opt_command);
      break;
    case 97:
      HASH_INITIALIZED(0x39D6B3305B1780E1LL, g->GV(opt_xml),
                       opt_xml);
      break;
    case 103:
      HASH_INITIALIZED(0x42CE0CF22BD7E967LL, g->GV(opt_phc_prefix),
                       opt_phc_prefix);
      break;
    case 104:
      HASH_INITIALIZED(0x7B60EB65364D38E8LL, g->GV(opt_zero),
                       opt_zero);
      break;
    case 105:
      HASH_INITIALIZED(0x44BB01E55B028F69LL, g->GV(opt_pass),
                       opt_pass);
      break;
    case 109:
      HASH_INITIALIZED(0x4A5D0431E1A3CAEDLL, g->GV(filename),
                       filename);
      break;
    case 110:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 118:
      HASH_INITIALIZED(0x79DE41650173A4F6LL, g->GV(opt_dont_upper),
                       opt_dont_upper);
      break;
    case 121:
      HASH_INITIALIZED(0x3CDA59286E299AF9LL, g->GV(command_line),
                       command_line);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
