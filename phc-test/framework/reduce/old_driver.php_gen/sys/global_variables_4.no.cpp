
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "php",
    "cg",
    "command_line",
    "opt_result",
    "opts",
    "arguments",
    "opt_verbose",
    "opt_help",
    "opt_command",
    "opt_main_command",
    "opt_pass",
    "opt_xml",
    "opt_interpret",
    "opt_dont_upper",
    "opt_upper",
    "opt_failure",
    "opt_zero",
    "opt_phc_prefix",
    "opt",
    "filename",
    "reduce",
    "input",
    "result",
  };
  if (idx >= 0 && idx < 35) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(php);
    case 13: return GV(cg);
    case 14: return GV(command_line);
    case 15: return GV(opt_result);
    case 16: return GV(opts);
    case 17: return GV(arguments);
    case 18: return GV(opt_verbose);
    case 19: return GV(opt_help);
    case 20: return GV(opt_command);
    case 21: return GV(opt_main_command);
    case 22: return GV(opt_pass);
    case 23: return GV(opt_xml);
    case 24: return GV(opt_interpret);
    case 25: return GV(opt_dont_upper);
    case 26: return GV(opt_upper);
    case 27: return GV(opt_failure);
    case 28: return GV(opt_zero);
    case 29: return GV(opt_phc_prefix);
    case 30: return GV(opt);
    case 31: return GV(filename);
    case 32: return GV(reduce);
    case 33: return GV(input);
    case 34: return GV(result);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
