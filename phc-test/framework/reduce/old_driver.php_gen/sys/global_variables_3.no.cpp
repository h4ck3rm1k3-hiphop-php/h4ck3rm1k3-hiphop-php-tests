
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x4CB3E826DC85CA81LL, opt_upper, 26);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 7:
      HASH_INDEX(0x4E80F79695D41A07LL, opt, 30);
      break;
    case 12:
      HASH_INDEX(0x21DEE4EB204AE10CLL, cg, 13);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 27:
      HASH_INDEX(0x2FF01C2058B0B21BLL, opt_verbose, 18);
      break;
    case 29:
      HASH_INDEX(0x37E66094A8A9409DLL, reduce, 32);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 38:
      HASH_INDEX(0x3CC816D24426A726LL, opt_main_command, 21);
      break;
    case 52:
      HASH_INDEX(0x421780730E09F634LL, opts, 16);
      break;
    case 53:
      HASH_INDEX(0x18FB4A2A9180C435LL, opt_interpret, 24);
      break;
    case 59:
      HASH_INDEX(0x67DB402A3712D03BLL, opt_result, 15);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 65:
      HASH_INDEX(0x52B182B0A94E9C41LL, php, 12);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 72:
      HASH_INDEX(0x599E3F67E2599248LL, result, 34);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 75:
      HASH_INDEX(0x1018D4B85360194BLL, opt_help, 19);
      HASH_INDEX(0x1532499416EFFACBLL, input, 33);
      break;
    case 76:
      HASH_INDEX(0x07CCA5D7E3548D4CLL, opt_failure, 27);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 92:
      HASH_INDEX(0x0F60C6595ED9FADCLL, arguments, 17);
      break;
    case 94:
      HASH_INDEX(0x4836EF4A1D6F465ELL, opt_command, 20);
      break;
    case 97:
      HASH_INDEX(0x39D6B3305B1780E1LL, opt_xml, 23);
      break;
    case 103:
      HASH_INDEX(0x42CE0CF22BD7E967LL, opt_phc_prefix, 29);
      break;
    case 104:
      HASH_INDEX(0x7B60EB65364D38E8LL, opt_zero, 28);
      break;
    case 105:
      HASH_INDEX(0x44BB01E55B028F69LL, opt_pass, 22);
      break;
    case 109:
      HASH_INDEX(0x4A5D0431E1A3CAEDLL, filename, 31);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 118:
      HASH_INDEX(0x79DE41650173A4F6LL, opt_dont_upper, 25);
      break;
    case 121:
      HASH_INDEX(0x3CDA59286E299AF9LL, command_line, 14);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 35) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
