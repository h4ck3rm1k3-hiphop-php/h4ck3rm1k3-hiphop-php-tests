
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

static void print(FILE *fp, String s) {
  if (fp) {
    fwrite(s.c_str(), 1, s.size(), fp);
    return;
  }
  echo(s);
}
static void output_dynamic_constants(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_constants = json_decode('");
  Array dynamic_constants;
  String s = f_json_encode(dynamic_constants);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_static_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$static_global_variables = json_decode('");
  Array static_global_variables;
  static_global_variables.set("gv_argc", g->get("argc"));
  static_global_variables.set("gv_argv", g->get("argv"));
  static_global_variables.set("gv__SERVER", g->get("_SERVER"));
  static_global_variables.set("gv__GET", g->get("_GET"));
  static_global_variables.set("gv__POST", g->get("_POST"));
  static_global_variables.set("gv__COOKIE", g->get("_COOKIE"));
  static_global_variables.set("gv__FILES", g->get("_FILES"));
  static_global_variables.set("gv__ENV", g->get("_ENV"));
  static_global_variables.set("gv__REQUEST", g->get("_REQUEST"));
  static_global_variables.set("gv__SESSION", g->get("_SESSION"));
  static_global_variables.set("gv_HTTP_RAW_POST_DATA", g->get("HTTP_RAW_POST_DATA"));
  static_global_variables.set("gv_http_response_header", g->get("http_response_header"));
  static_global_variables.set("gv_php", g->get("php"));
  static_global_variables.set("gv_cg", g->get("cg"));
  static_global_variables.set("gv_command_line", g->get("command_line"));
  static_global_variables.set("gv_opt_result", g->get("opt_result"));
  static_global_variables.set("gv_opts", g->get("opts"));
  static_global_variables.set("gv_arguments", g->get("arguments"));
  static_global_variables.set("gv_opt_verbose", g->get("opt_verbose"));
  static_global_variables.set("gv_opt_help", g->get("opt_help"));
  static_global_variables.set("gv_opt_command", g->get("opt_command"));
  static_global_variables.set("gv_opt_main_command", g->get("opt_main_command"));
  static_global_variables.set("gv_opt_pass", g->get("opt_pass"));
  static_global_variables.set("gv_opt_xml", g->get("opt_xml"));
  static_global_variables.set("gv_opt_interpret", g->get("opt_interpret"));
  static_global_variables.set("gv_opt_dont_upper", g->get("opt_dont_upper"));
  static_global_variables.set("gv_opt_upper", g->get("opt_upper"));
  static_global_variables.set("gv_opt_failure", g->get("opt_failure"));
  static_global_variables.set("gv_opt_zero", g->get("opt_zero"));
  static_global_variables.set("gv_opt_phc_prefix", g->get("opt_phc_prefix"));
  static_global_variables.set("gv_opt", g->get("opt"));
  static_global_variables.set("gv_filename", g->get("filename"));
  static_global_variables.set("gv_reduce", g->get("reduce"));
  static_global_variables.set("gv_input", g->get("input"));
  static_global_variables.set("gv_result", g->get("result"));
  String s = f_json_encode(static_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_dynamic_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_global_variables = json_decode('");
  Array dynamic_global_variables;
  dynamic_global_variables = *get_variable_table();
  String s = f_json_encode(dynamic_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_variables = json_decode('");
  Array method_static_variables;
  method_static_variables.set("sv_reduce_DupIdwarn_once_DupIdcache", g->sv_reduce_DupIdwarn_once_DupIdcache);
  String s = f_json_encode(method_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_inited(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_inited = json_decode('");
  Array method_static_inited;
  method_static_inited.set("inited_sv_reduce_DupIdwarn_once_DupIdcache", g->inited_sv_reduce_DupIdwarn_once_DupIdcache);
  String s = f_json_encode(method_static_inited);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_class_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$class_static_variables = json_decode('");
  Array class_static_variables;
  String s = f_json_encode(class_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_pseudomain_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$pseudomain_variables = json_decode('");
  Array pseudomain_variables;
  pseudomain_variables.set("run_pm_php$phc_test$framework$reduce$old_driver_php", g->run_pm_php$phc_test$framework$reduce$old_driver_php);
  pseudomain_variables.set("run_pm_php$phc_test$framework$reduce$Reduce_php", g->run_pm_php$phc_test$framework$reduce$Reduce_php);
  String s = f_json_encode(pseudomain_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_functions(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_functions = json_decode('");
  Array redeclared_functions;
  String s = f_json_encode(redeclared_functions);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_classes(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_classes = json_decode('");
  Array redeclared_classes;
  String s = f_json_encode(redeclared_classes);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

void output_global_state(FILE *fp) {
  output_static_global_variables(fp);
  output_dynamic_global_variables(fp);
  output_dynamic_constants(fp);
  output_method_static_variables(fp);
  output_method_static_inited(fp);
  output_class_static_variables(fp);
  output_pseudomain_variables(fp);
  output_redeclared_functions(fp);
  output_redeclared_classes(fp);
}


///////////////////////////////////////////////////////////////////////////////
}
