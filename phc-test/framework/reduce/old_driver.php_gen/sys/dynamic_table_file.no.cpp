
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$phc_test$framework$reduce$old_driver_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$phc_test$framework$reduce$Reduce_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_INCLUDE(0x1D0D94EDF20078B0LL, "phc-test/framework/reduce/Reduce.php", php$phc_test$framework$reduce$Reduce_php);
      break;
    case 1:
      HASH_INCLUDE(0x1E9C5EB4443AC87DLL, "phc-test/framework/reduce/old_driver.php", php$phc_test$framework$reduce$old_driver_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
