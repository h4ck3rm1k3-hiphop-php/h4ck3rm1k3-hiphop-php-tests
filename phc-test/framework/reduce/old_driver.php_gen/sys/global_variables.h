
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(php)
    GVS(cg)
    GVS(command_line)
    GVS(opt_result)
    GVS(opts)
    GVS(arguments)
    GVS(opt_verbose)
    GVS(opt_help)
    GVS(opt_command)
    GVS(opt_main_command)
    GVS(opt_pass)
    GVS(opt_xml)
    GVS(opt_interpret)
    GVS(opt_dont_upper)
    GVS(opt_upper)
    GVS(opt_failure)
    GVS(opt_zero)
    GVS(opt_phc_prefix)
    GVS(opt)
    GVS(filename)
    GVS(reduce)
    GVS(input)
    GVS(result)
  END_GVS(23)

  // Dynamic Constants

  // Function/Method Static Variables
  Variant sv_reduce_DupIdwarn_once_DupIdcache;

  // Function/Method Static Variable Initialization Booleans
  Variant inited_sv_reduce_DupIdwarn_once_DupIdcache;

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$framework$reduce$old_driver_php;
  bool run_pm_php$phc_test$framework$reduce$Reduce_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 35;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[1];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
