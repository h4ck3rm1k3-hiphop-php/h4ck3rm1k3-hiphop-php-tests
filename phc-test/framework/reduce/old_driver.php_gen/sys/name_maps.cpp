
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "reduce", "phc-test/framework/reduce/Reduce.php",
  "reduceexception", "phc-test/framework/reduce/Reduce.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "above_debug_threshold", "phc-test/framework/reduce/old_driver.php",
  "check_against_compiled", "phc-test/framework/reduce/old_driver.php",
  "check_against_interpreted", "phc-test/framework/reduce/old_driver.php",
  "failure_check", "phc-test/framework/reduce/old_driver.php",
  "interpret_with_phc", "phc-test/framework/reduce/old_driver.php",
  "mydebug", "phc-test/framework/reduce/old_driver.php",
  "mydump", "phc-test/framework/reduce/old_driver.php",
  "myrun", "phc-test/framework/reduce/old_driver.php",
  "run_with_phc", "phc-test/framework/reduce/old_driver.php",
  "run_with_php", "phc-test/framework/reduce/old_driver.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
