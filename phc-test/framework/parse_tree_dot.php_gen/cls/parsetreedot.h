
#ifndef __GENERATED_cls_parsetreedot_h__
#define __GENERATED_cls_parsetreedot_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/parse_tree_dot.php line 10 */
class c_parsetreedot : virtual public ObjectData {
  BEGIN_CLASS_MAP(parsetreedot)
  END_CLASS_MAP(parsetreedot)
  DECLARE_CLASS(parsetreedot, ParseTreeDot, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: Array t_get_dependent_test_names();
  public: Variant t_check_prerequisites();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parsetreedot_h__
