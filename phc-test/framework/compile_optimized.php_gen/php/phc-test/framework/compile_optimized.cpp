
#include <php/phc-test/framework/compile_optimized.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/compile_optimized.php line 11 */
Variant c_compileoptimized::os_get(const char *s, int64 hash) {
  return c_compiledvsinterpreted::os_get(s, hash);
}
Variant &c_compileoptimized::os_lval(const char *s, int64 hash) {
  return c_compiledvsinterpreted::os_lval(s, hash);
}
void c_compileoptimized::o_get(ArrayElementVec &props) const {
  c_compiledvsinterpreted::o_get(props);
}
bool c_compileoptimized::o_exists(CStrRef s, int64 hash) const {
  return c_compiledvsinterpreted::o_exists(s, hash);
}
Variant c_compileoptimized::o_get(CStrRef s, int64 hash) {
  return c_compiledvsinterpreted::o_get(s, hash);
}
Variant c_compileoptimized::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_compiledvsinterpreted::o_set(s, hash, v, forInit);
}
Variant &c_compileoptimized::o_lval(CStrRef s, int64 hash) {
  return c_compiledvsinterpreted::o_lval(s, hash);
}
Variant c_compileoptimized::os_constant(const char *s) {
  return c_compiledvsinterpreted::os_constant(s);
}
IMPLEMENT_CLASS(compileoptimized)
ObjectData *c_compileoptimized::cloneImpl() {
  c_compileoptimized *obj = NEW(c_compileoptimized)();
  cloneSet(obj);
  return obj;
}
void c_compileoptimized::cloneSet(c_compileoptimized *clone) {
  c_compiledvsinterpreted::cloneSet(clone);
}
Variant c_compileoptimized::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_compiledvsinterpreted::o_invoke(s, params, hash, fatal);
}
Variant c_compileoptimized::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_compiledvsinterpreted::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_compileoptimized::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_compiledvsinterpreted::os_invoke(c, s, params, hash, fatal);
}
Variant cw_compileoptimized$os_get(const char *s) {
  return c_compileoptimized::os_get(s, -1);
}
Variant &cw_compileoptimized$os_lval(const char *s) {
  return c_compileoptimized::os_lval(s, -1);
}
Variant cw_compileoptimized$os_constant(const char *s) {
  return c_compileoptimized::os_constant(s);
}
Variant cw_compileoptimized$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_compileoptimized::os_invoke(c, s, params, -1, fatal);
}
void c_compileoptimized::init() {
  c_compiledvsinterpreted::init();
}
/* SRC: phc-test/framework/compile_optimized.php line 14 */
Array c_compileoptimized::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(CompileOptimized, CompileOptimized::get_dependent_test_names);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: phc-test/framework/compile_optimized.php line 19 */
String c_compileoptimized::t_get_phc_command(Variant v_subject, CVarRef v_exe_name) {
  INSTANCE_METHOD_INJECTION(CompileOptimized, CompileOptimized::get_phc_command);
  return concat(toString(LINE(21,invoke_failed("get_phc_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000081EE2BC9LL))), toString(" -O1 -c -o ") + toString(v_exe_name));
} /* function */
/* SRC: phc-test/framework/compile_optimized.php line 24 */
String c_compileoptimized::t_get_php_command(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(CompileOptimized, CompileOptimized::get_php_command);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  return LINE(27,(assignCallTemp(eo_0, toString(gv_phc)),assignCallTemp(eo_2, toString(v_subject)),assignCallTemp(eo_4, toString(invoke_failed("get_php_command_line", Array(ArrayInit(2).set(0, ref(v_subject)).set(1, "pipe").create()), 0x0000000033B2020BLL))),concat5(eo_0, " -O1 --dump=sua ", eo_2, " | ", eo_4)));
} /* function */
Object co_compileoptimized(CArrRef params, bool init /* = true */) {
  return Object(p_compileoptimized(NEW(c_compileoptimized)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$compile_optimized_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/compile_optimized.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$compile_optimized_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(10,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_compileoptimized(p_compileoptimized(NEWOBJ(c_compileoptimized)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
