
#ifndef __GENERATED_php_phc_test_framework_no_whitespace_h__
#define __GENERATED_php_phc_test_framework_no_whitespace_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/no_whitespace.fw.h>

// Declarations
#include <cls/nowhitespace.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$no_whitespace_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_nowhitespace(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_no_whitespace_h__
