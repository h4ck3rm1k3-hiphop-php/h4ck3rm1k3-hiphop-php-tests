
#include <php/phc-test/framework/no_whitespace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/no_whitespace.php line 13 */
Variant c_nowhitespace::os_get(const char *s, int64 hash) {
  return c_asynctest::os_get(s, hash);
}
Variant &c_nowhitespace::os_lval(const char *s, int64 hash) {
  return c_asynctest::os_lval(s, hash);
}
void c_nowhitespace::o_get(ArrayElementVec &props) const {
  c_asynctest::o_get(props);
}
bool c_nowhitespace::o_exists(CStrRef s, int64 hash) const {
  return c_asynctest::o_exists(s, hash);
}
Variant c_nowhitespace::o_get(CStrRef s, int64 hash) {
  return c_asynctest::o_get(s, hash);
}
Variant c_nowhitespace::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_asynctest::o_set(s, hash, v, forInit);
}
Variant &c_nowhitespace::o_lval(CStrRef s, int64 hash) {
  return c_asynctest::o_lval(s, hash);
}
Variant c_nowhitespace::os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
IMPLEMENT_CLASS(nowhitespace)
ObjectData *c_nowhitespace::cloneImpl() {
  c_nowhitespace *obj = NEW(c_nowhitespace)();
  cloneSet(obj);
  return obj;
}
void c_nowhitespace::cloneSet(c_nowhitespace *clone) {
  c_asynctest::cloneSet(clone);
}
Variant c_nowhitespace::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x1509B8A9B882D43CLL, strip_whitespace) {
        return (t_strip_whitespace(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke(s, params, hash, fatal);
}
Variant c_nowhitespace::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x1509B8A9B882D43CLL, strip_whitespace) {
        return (t_strip_whitespace(a0, a1));
      }
      break;
    default:
      break;
  }
  return c_asynctest::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_nowhitespace::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_asynctest::os_invoke(c, s, params, hash, fatal);
}
Variant cw_nowhitespace$os_get(const char *s) {
  return c_nowhitespace::os_get(s, -1);
}
Variant &cw_nowhitespace$os_lval(const char *s) {
  return c_nowhitespace::os_lval(s, -1);
}
Variant cw_nowhitespace$os_constant(const char *s) {
  return c_nowhitespace::os_constant(s);
}
Variant cw_nowhitespace$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_nowhitespace::os_invoke(c, s, params, -1, fatal);
}
void c_nowhitespace::init() {
  c_asynctest::init();
}
/* SRC: phc-test/framework/no_whitespace.php line 15 */
Variant c_nowhitespace::t_get_test_subjects() {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::get_test_subjects);
  return LINE(17,invoke_failed("get_all_scripts", Array(), 0x000000008819DA1ALL));
} /* function */
/* SRC: phc-test/framework/no_whitespace.php line 20 */
Array c_nowhitespace::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::get_dependent_test_names);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: phc-test/framework/no_whitespace.php line 25 */
void c_nowhitespace::t_run_test(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::run_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Object v_bundle;

  (v_bundle = LINE(28,create_object("asyncbundle", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, v_subject).create()))));
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(0LL, (concat(toString(LINE(30,invoke_failed("get_phc_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000081EE2BC9LL))), " --pretty-print")), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(0LL, ("strip_whitespace"), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(0LL, ("fail_on_output"), 0x77CFA1EEF01BCA90LL);
  lval(v_bundle.o_lval("commands", 0x189A3C89F0CA1103LL)).set(1LL, (toString("cat ") + toString(v_subject)), 0x5BCA7C69B794F8CELL);
  lval(v_bundle.o_lval("out_handlers", 0x5FE40B58F183D044LL)).set(1LL, ("strip_whitespace"), 0x5BCA7C69B794F8CELL);
  lval(v_bundle.o_lval("err_handlers", 0x0D4F3DB82D56CC90LL)).set(1LL, ("fail_on_output"), 0x5BCA7C69B794F8CELL);
  (v_bundle.o_lval("final", 0x5192930B2145036ELL) = "two_command_finish");
  LINE(39,v_bundle->o_invoke_few_args("start", 0x3970634433FD3E52LL, 0));
} /* function */
/* SRC: phc-test/framework/no_whitespace.php line 44 */
Variant c_nowhitespace::t_strip_whitespace(Variant v_out, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(NoWhitespace, NoWhitespace::strip_whitespace);
  (v_out = LINE(46,x_preg_replace("/\\s+/", "", v_out)));
  (v_out = LINE(49,x_preg_replace("/<\\\?php/", "<\?", v_out)));
  return v_out;
} /* function */
Object co_nowhitespace(CArrRef params, bool init /* = true */) {
  return Object(p_nowhitespace(NEW(c_nowhitespace)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$no_whitespace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/no_whitespace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$no_whitespace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(12,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_nowhitespace(p_nowhitespace(NEWOBJ(c_nowhitespace)())->create())))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
