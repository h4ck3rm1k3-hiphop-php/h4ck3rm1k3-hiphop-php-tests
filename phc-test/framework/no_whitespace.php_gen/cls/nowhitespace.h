
#ifndef __GENERATED_cls_nowhitespace_h__
#define __GENERATED_cls_nowhitespace_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/no_whitespace.php line 13 */
class c_nowhitespace : virtual public ObjectData {
  BEGIN_CLASS_MAP(nowhitespace)
  END_CLASS_MAP(nowhitespace)
  DECLARE_CLASS(nowhitespace, NoWhitespace, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: Array t_get_dependent_test_names();
  public: void t_run_test(Variant v_subject);
  public: Variant t_strip_whitespace(Variant v_out, CVarRef v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_nowhitespace_h__
