
#ifndef __GENERATED_cls_parseastdot_h__
#define __GENERATED_cls_parseastdot_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/parse_ast_dot.php line 10 */
class c_parseastdot : virtual public ObjectData {
  BEGIN_CLASS_MAP(parseastdot)
  END_CLASS_MAP(parseastdot)
  DECLARE_CLASS(parseastdot, ParseASTDot, asynctest)
  void init();
  public: Variant t_get_test_subjects();
  public: Array t_get_dependent_test_names();
  public: Variant t_check_prerequisites();
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parseastdot_h__
