
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x52B182B0A94E9C41LL, php, 13);
      break;
    case 2:
      HASH_INDEX(0x2027864469AD4382LL, string, 19);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 28:
      HASH_INDEX(0x0BCDB293DC3CBDDCLL, name, 16);
      break;
    case 32:
      HASH_INDEX(0x46C13C417DE7E0A0LL, time, 17);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 36:
      HASH_INDEX(0x38138B1B4F5F1764LL, phc_results, 14);
      break;
    case 37:
      HASH_INDEX(0x30F8F3347A4920A5LL, php_results, 15);
      break;
    case 42:
      HASH_INDEX(0x67EA86E1CF55026ALL, phc, 12);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 58:
      HASH_INDEX(0x1C8E6AC29485223ALL, speedups, 18);
      break;
    case 62:
      HASH_INDEX(0x66AD900A2301E2FELL, title, 20);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 21) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
