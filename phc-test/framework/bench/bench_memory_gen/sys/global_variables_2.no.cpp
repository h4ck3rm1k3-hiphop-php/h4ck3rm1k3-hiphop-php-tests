
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      HASH_INITIALIZED(0x52B182B0A94E9C41LL, g->GV(php),
                       php);
      break;
    case 2:
      HASH_INITIALIZED(0x2027864469AD4382LL, g->GV(string),
                       string);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 28:
      HASH_INITIALIZED(0x0BCDB293DC3CBDDCLL, g->GV(name),
                       name);
      break;
    case 32:
      HASH_INITIALIZED(0x46C13C417DE7E0A0LL, g->GV(time),
                       time);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 36:
      HASH_INITIALIZED(0x38138B1B4F5F1764LL, g->GV(phc_results),
                       phc_results);
      break;
    case 37:
      HASH_INITIALIZED(0x30F8F3347A4920A5LL, g->GV(php_results),
                       php_results);
      break;
    case 42:
      HASH_INITIALIZED(0x67EA86E1CF55026ALL, g->GV(phc),
                       phc);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 58:
      HASH_INITIALIZED(0x1C8E6AC29485223ALL, g->GV(speedups),
                       speedups);
      break;
    case 62:
      HASH_INITIALIZED(0x66AD900A2301E2FELL, g->GV(title),
                       title);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
