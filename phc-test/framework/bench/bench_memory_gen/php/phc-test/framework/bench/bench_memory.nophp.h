
#ifndef __GENERATED_php_phc_test_framework_bench_bench_memory_nophp_h__
#define __GENERATED_php_phc_test_framework_bench_bench_memory_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/bench/bench_memory.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$bench$bench_memory(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_process_data(CVarRef v_input);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_bench_bench_memory_nophp_h__
