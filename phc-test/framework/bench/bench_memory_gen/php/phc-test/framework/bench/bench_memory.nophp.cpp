
#include <php/phc-test/framework/bench/bench_memory.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/bench/bench_memory line 15 */
Variant f_process_data(CVarRef v_input) {
  FUNCTION_INJECTION(process_data);
  Variant v_result;
  Primitive v_index = 0;
  Variant v_line;
  String v_name;
  Variant v_matches;
  Variant v_msb;
  Variant v_num;

  (v_result = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    Variant map2 = LINE(19,x_split("\n", toString(v_input)));
    for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_line = iter3->second();
      v_index = iter3->first();
      {
        if (same(modulo(toInt64(v_index), 2LL), 0LL)) {
          (v_name = LINE(24,x_basename(toString(v_line), ".php")));
        }
        else {
          (v_msb = LINE(29,x_preg_match("/^==\\d+== Total spacetime:\\s+([0-9,]+) ms\\.B$/", toString(v_line), ref(v_matches))));
          (v_num = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          (v_num = LINE(31,x_preg_replace("/,/", "", v_num)));
          v_result.set(v_name, (v_num));
        }
      }
    }
  }
  return v_result;
} /* function */
Variant pm_php$phc_test$framework$bench$bench_memory(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/bench/bench_memory);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$bench$bench_memory;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_phc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phc") : g->GV(phc);
  Variant &v_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php") : g->GV(php);
  Variant &v_phc_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phc_results") : g->GV(phc_results);
  Variant &v_php_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_results") : g->GV(php_results);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);
  Variant &v_time __attribute__((__unused__)) = (variables != gVariables) ? variables->get("time") : g->GV(time);
  Variant &v_speedups __attribute__((__unused__)) = (variables != gVariables) ? variables->get("speedups") : g->GV(speedups);
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_title __attribute__((__unused__)) = (variables != gVariables) ? variables->get("title") : g->GV(title);

  if (!equal(v_argc, 2LL)) {
    f_exit("usage: misc/bench_memory NAME\n\n    creates results/NAME_memory.eps\n");
  }
  (v_phc = f_shell_exec(toString("for i in test/subjects/benchmarks/zend/individual/*; do echo $i; misc/comp -O -M $i 2>&1 | grep spacetime; done")));
  (v_php = f_shell_exec(toString("for i in test/subjects/benchmarks/zend/individual/*; do echo $i; valgrind --tool=massif /usr/local/php-opt/bin/php $i 2>&1 | grep spacetime; done")));
  (v_phc_results = LINE(12,f_process_data(v_phc)));
  (v_php_results = LINE(13,f_process_data(v_php)));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_php_results.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_php = iter6->second();
      v_name = iter6->first();
      {
        (v_phc = v_phc_results.rvalAt(v_name));
        (v_time = (divide(v_php, v_phc)));
        concat_assign(v_speedups, LINE(43,concat4(toString(v_name), " ", toString(v_time), "\n")));
      }
    }
  }
  (v_string = LINE(58,concat3("yformat=%g\nylabel=Relative memory reduction\n=arithmean\n=sort\n\n", toString(v_speedups), "\n\n ")));
  (v_title = v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  LINE(62,(assignCallTemp(eo_0, concat3("results/", toString(v_title), "_memory.plot")),assignCallTemp(eo_1, v_string),x_file_put_contents(eo_0, eo_1)));
  echo(f_shell_exec(toString("bargraph.pl results/") + toString(v_title) + toString("_memory.plot > results/") + toString(v_title) + toString("_memory.eps")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
