
#ifndef __GENERATED_php_phc_test_framework_bench_bench_compare_nophp_h__
#define __GENERATED_php_phc_test_framework_bench_bench_compare_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/bench/bench_compare.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$bench$bench_compare(bool incOnce = false, LVariableTable* variables = NULL);
Sequence f_parse(CStrRef v_filename);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_bench_bench_compare_nophp_h__
