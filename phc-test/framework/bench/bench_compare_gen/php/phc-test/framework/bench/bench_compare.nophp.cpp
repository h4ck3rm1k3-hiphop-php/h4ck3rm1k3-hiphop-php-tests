
#include <php/phc-test/framework/bench/bench_compare.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/bench/bench_compare line 23 */
Sequence f_parse(CStrRef v_filename) {
  FUNCTION_INJECTION(parse);
  Variant eo_0;
  Variant eo_1;
  Variant v_results_strings;
  Variant v_res;
  Variant v_name;
  Variant v_time;
  Sequence v_results;

  (v_results_strings = LINE(26,(assignCallTemp(eo_1, x_chop(toString(x_file_get_contents(v_filename)))),x_split("\n", eo_1))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_results_strings.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_res = iter3->second();
      {
        (v_res = LINE(30,x_preg_replace("/\\(\\d+\\)/", "", v_res)));
        if (same(v_res, "------------------------")) break;
        if (!(toBoolean(LINE(37,x_preg_match("/^\\w+\\s+[0-9\\.]+$/", toString(v_res)))))) f_exit(LINE(38,concat3("bad line: '", toString(v_res), "'")));
        df_lambda_1(LINE(39,x_split("[ ]+", toString(v_res))), v_name, v_time);
        if (equal(v_name, "Total")) continue;
        v_results.set(v_name, (v_time));
      }
    }
  }
  return v_results;
} /* function */
Variant pm_php$phc_test$framework$bench$bench_compare(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/bench/bench_compare);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$bench$bench_compare;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_title __attribute__((__unused__)) = (variables != gVariables) ? variables->get("title") : g->GV(title);
  Variant &v_dirname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dirname") : g->GV(dirname);
  Variant &v_php_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_results") : g->GV(php_results);
  Variant &v_phc_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phc_results") : g->GV(phc_results);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);
  Variant &v_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php") : g->GV(php);
  Variant &v_phc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phc") : g->GV(phc);
  Variant &v_time __attribute__((__unused__)) = (variables != gVariables) ? variables->get("time") : g->GV(time);
  Variant &v_speedups __attribute__((__unused__)) = (variables != gVariables) ? variables->get("speedups") : g->GV(speedups);
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);

  if (less(LINE(12,x_count(v_argv)), 1LL)) {
    print("usage: bench_compare php_NAME\n\n\t\t\t  or:    bench_compare phc_NAME\n \n\t\t\t  or:    bench_compare NAME\n\n\t\t\t  \n\t\t\t  to compare php_NAME.txt with phc_NAME.txt");
    f_exit();
  }
  (v_title = v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  (v_dirname = LINE(51,x_dirname(toString(v_title))));
  (v_title = LINE(54,x_basename(toString(v_title))));
  (v_title = LINE(57,x_preg_replace("/^ph[cp]_/", "", v_title)));
  (v_title = LINE(60,x_preg_replace("/.txt$/", "", v_title)));
  (v_php_results = LINE(63,f_parse(concat4(toString(v_dirname), "/php_", toString(v_title), ".txt"))));
  (v_phc_results = LINE(64,f_parse(concat4(toString(v_dirname), "/phc_", toString(v_title), ".txt"))));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_php_results.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_php = iter6->second();
      v_name = iter6->first();
      {
        (v_phc = v_phc_results.rvalAt(v_name));
        (v_time = (divide(v_php, v_phc)));
        concat_assign(v_speedups, LINE(71,concat4(toString(v_name), " ", toString(v_time), "\n")));
      }
    }
  }
  (v_string = LINE(86,concat3("yformat=%g\nylabel=Speedup of compiled benchmark\n=arithmean\n=sort\n\n", toString(v_speedups), "\n\n ")));
  LINE(89,(assignCallTemp(eo_0, concat3("results/", toString(v_title), ".plot")),assignCallTemp(eo_1, v_string),x_file_put_contents(eo_0, eo_1)));
  echo(f_shell_exec(toString("bargraph.pl results/") + toString(v_title) + toString(".plot > results/") + toString(v_title) + toString(".eps")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
