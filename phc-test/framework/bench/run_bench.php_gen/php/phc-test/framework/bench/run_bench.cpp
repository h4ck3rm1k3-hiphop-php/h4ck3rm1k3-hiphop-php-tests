
#include <php/phc-test/framework/bench/run_bench.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/bench/run_bench.php line 8 */
Numeric f_run_phc(CStrRef v_filename, CVarRef v_name, CVarRef v_input) {
  FUNCTION_INJECTION(run_phc);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_date __attribute__((__unused__)) = g->GV(date);
  String v_compile_cmd;
  Variant v_exit_code;
  Variant v_error_file;
  String v_run_cmd;
  Variant v_start;
  Variant v_stop;

  (v_compile_cmd = toString("misc/comp -O -i ") + v_filename);
  print(concat(v_compile_cmd, "\n"));
  LINE(16,x_system(v_compile_cmd, ref(v_exit_code)));
  print(toString((toString(v_exit_code))));
  if (toBoolean(v_exit_code)) {
    (v_error_file = LINE(20,(assignCallTemp(eo_0, concat3("results/", toString(gv_date), "_errors")),x_fopen(eo_0, "a"))));
    LINE(21,x_fwrite(toObject(v_error_file), v_filename + toString(" did not compile\n")));
    LINE(22,x_fclose(toObject(v_error_file)));
  }
  else {
    (v_run_cmd = concat("./", f_shell_exec(toString("basename \"") + toString(v_name) + toString(".out\""))));
    (v_start = LINE(28,x_microtime(toBoolean(1LL))));
    print(v_run_cmd + toString("\n"));
    LINE(30,x_system(v_run_cmd, ref(v_exit_code)));
    (v_stop = LINE(31,x_microtime(toBoolean(1LL))));
    print(toString((toString(v_exit_code))));
    if (toBoolean(v_exit_code)) {
      (v_error_file = LINE(35,(assignCallTemp(eo_0, concat3("results/", toString(gv_date), "_errors")),x_fopen(eo_0, "a"))));
      LINE(36,x_fwrite(toObject(v_error_file), v_run_cmd + toString(" failed\n")));
      LINE(37,x_fclose(toObject(v_error_file)));
      return 0LL;
    }
    return v_stop - v_start;
  }
  return 0LL;
} /* function */
/* SRC: phc-test/framework/bench/run_bench.php line 45 */
Numeric f_run_php(CStrRef v_filename, CVarRef v_input) {
  FUNCTION_INJECTION(run_php);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_php __attribute__((__unused__)) = g->GV(php);
  Variant v_start;
  String v_command;
  Variant v_stop;

  (v_start = LINE(50,x_microtime(toBoolean(1LL))));
  (v_command = LINE(52,concat5(toString(gv_php), " ", v_filename, " ", toString(v_input))));
  print(v_command + toString("\n"));
  print(f_shell_exec(v_command));
  (v_stop = LINE(57,x_microtime(toBoolean(1LL))));
  return v_stop - v_start;
} /* function */
/* SRC: phc-test/framework/bench/run_bench.php line 62 */
void f_run(CStrRef v_name, CStrRef v_cmdline_input) {
  FUNCTION_INJECTION(run);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_date __attribute__((__unused__)) = g->GV(date);
  String v_filename;
  Numeric v_time = 0;
  Variant v_php_res;
  Variant v_phc_res;

  (v_filename = v_name + toString(".php"));
  (v_time = LINE(69,f_run_php(v_filename, v_cmdline_input)));
  print(LINE(70,(assignCallTemp(eo_1, x_basename(v_name)),assignCallTemp(eo_2, toString(": ") + toString(v_time)),concat3("PHP: ", eo_1, eo_2))));
  print("\n");
  (v_php_res = LINE(73,(assignCallTemp(eo_0, concat3("results/php_", toString(gv_date), ".txt")),x_fopen(eo_0, "a"))));
  LINE(74,(assignCallTemp(eo_0, toObject(v_php_res)),assignCallTemp(eo_1, (assignCallTemp(eo_2, x_basename(v_name)),assignCallTemp(eo_4, toString(v_time)),concat4(eo_2, "     ", eo_4, "\n"))),x_fwrite(eo_0, eo_1)));
  LINE(75,x_fclose(toObject(v_php_res)));
  (v_time = LINE(78,f_run_phc(v_filename, v_name, v_cmdline_input)));
  print(LINE(79,(assignCallTemp(eo_1, x_basename(v_name)),assignCallTemp(eo_2, concat3(": ", toString(v_time), "\n")),concat3("phc: ", eo_1, eo_2))));
  (v_phc_res = LINE(81,(assignCallTemp(eo_0, concat3("results/phc_", toString(gv_date), ".txt")),x_fopen(eo_0, "a"))));
  LINE(82,(assignCallTemp(eo_0, toObject(v_phc_res)),assignCallTemp(eo_1, (assignCallTemp(eo_2, x_basename(v_name)),assignCallTemp(eo_4, toString(v_time)),concat4(eo_2, "     ", eo_4, "\n"))),x_fwrite(eo_0, eo_1)));
  LINE(83,x_fclose(toObject(v_phc_res)));
} /* function */
Variant pm_php$phc_test$framework$bench$run_bench_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/bench/run_bench.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$bench$run_bench_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php") : g->GV(php);
  Variant &v_date __attribute__((__unused__)) = (variables != gVariables) ? variables->get("date") : g->GV(date);
  Variant &v_bench_list __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bench_list") : g->GV(bench_list);
  Variant &v_bench __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bench") : g->GV(bench);

  (v_php = "/usr/local/php-opt/bin/php");
  (v_date = LINE(86,x_date("c")));
  (v_bench_list = LINE(88,x_fopen("test/framework/bench/list.txt", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_bench = LINE(89,x_fgets(toObject(v_bench_list)))))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(91,(assignCallTemp(eo_0, x_chop(toString(v_bench))),f_run(eo_0, "")));
      }
    }
  }
  LINE(94,x_fclose(toObject(v_bench_list)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
