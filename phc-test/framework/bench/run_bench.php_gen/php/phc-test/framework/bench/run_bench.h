
#ifndef __GENERATED_php_phc_test_framework_bench_run_bench_h__
#define __GENERATED_php_phc_test_framework_bench_run_bench_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/bench/run_bench.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Numeric f_run_phc(CStrRef v_filename, CVarRef v_name, CVarRef v_input);
Numeric f_run_php(CStrRef v_filename, CVarRef v_input);
void f_run(CStrRef v_name, CStrRef v_cmdline_input);
Variant pm_php$phc_test$framework$bench$run_bench_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_bench_run_bench_h__
