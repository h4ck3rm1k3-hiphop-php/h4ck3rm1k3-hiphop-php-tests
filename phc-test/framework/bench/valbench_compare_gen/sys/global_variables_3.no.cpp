
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 7:
      HASH_INDEX(0x4E80F79695D41A07LL, opt, 22);
      HASH_INDEX(0x612DD31212E90587LL, key, 31);
      break;
    case 8:
      HASH_INDEX(0x3AC8A99F82DA4288LL, opt_short, 17);
      break;
    case 12:
      HASH_INDEX(0x21DEE4EB204AE10CLL, cg, 12);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 29:
      HASH_INDEX(0x639A9489C189A59DLL, abbreviations, 25);
      break;
    case 34:
      HASH_INDEX(0x50690AA433880A22LL, short_keys, 24);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 50:
      HASH_INDEX(0x5B980D3E2D159A32LL, short_results, 34);
      break;
    case 52:
      HASH_INDEX(0x421780730E09F634LL, opts, 15);
      break;
    case 53:
      HASH_INDEX(0x74DCA4373A042AB5LL, baseline, 28);
      break;
    case 56:
      HASH_INDEX(0x66E32C011EFD3838LL, results, 33);
      break;
    case 59:
      HASH_INDEX(0x67DB402A3712D03BLL, opt_result, 14);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 69:
      HASH_INDEX(0x353F92C20A856FC5LL, other, 29);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 75:
      HASH_INDEX(0x1018D4B85360194BLL, opt_help, 18);
      break;
    case 78:
      HASH_INDEX(0x0510D59D8A97894ELL, opt_debug, 20);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 85:
      HASH_INDEX(0x54F0651B7317F455LL, opt_cost, 21);
      break;
    case 92:
      HASH_INDEX(0x0F60C6595ED9FADCLL, arguments, 16);
      break;
    case 93:
      HASH_INDEX(0x2183F11DA48E51DDLL, opt_png, 19);
      break;
    case 102:
      HASH_INDEX(0x2A3A32A302743166LL, descriptions, 26);
      break;
    case 106:
      HASH_INDEX(0x7E829BA8D21EE86ALL, useless_tests, 23);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 113:
      HASH_INDEX(0x69E7413AE0C88471LL, value, 32);
      break;
    case 114:
      HASH_INDEX(0x12CBDBCF4DD2FE72LL, included, 30);
      break;
    case 120:
      HASH_INDEX(0x7AE81F1CFBCA2BF8LL, individual, 35);
      break;
    case 121:
      HASH_INDEX(0x3CDA59286E299AF9LL, command_line, 13);
      break;
    case 127:
      HASH_INDEX(0x5F8ACA212093407FLL, costs, 27);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 36) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
