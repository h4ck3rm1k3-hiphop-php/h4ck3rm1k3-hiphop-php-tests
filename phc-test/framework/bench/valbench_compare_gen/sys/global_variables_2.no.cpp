
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 7:
      HASH_INITIALIZED(0x4E80F79695D41A07LL, g->GV(opt),
                       opt);
      HASH_INITIALIZED(0x612DD31212E90587LL, g->GV(key),
                       key);
      break;
    case 8:
      HASH_INITIALIZED(0x3AC8A99F82DA4288LL, g->GV(opt_short),
                       opt_short);
      break;
    case 12:
      HASH_INITIALIZED(0x21DEE4EB204AE10CLL, g->GV(cg),
                       cg);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 29:
      HASH_INITIALIZED(0x639A9489C189A59DLL, g->GV(abbreviations),
                       abbreviations);
      break;
    case 34:
      HASH_INITIALIZED(0x50690AA433880A22LL, g->GV(short_keys),
                       short_keys);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 50:
      HASH_INITIALIZED(0x5B980D3E2D159A32LL, g->GV(short_results),
                       short_results);
      break;
    case 52:
      HASH_INITIALIZED(0x421780730E09F634LL, g->GV(opts),
                       opts);
      break;
    case 53:
      HASH_INITIALIZED(0x74DCA4373A042AB5LL, g->GV(baseline),
                       baseline);
      break;
    case 56:
      HASH_INITIALIZED(0x66E32C011EFD3838LL, g->GV(results),
                       results);
      break;
    case 59:
      HASH_INITIALIZED(0x67DB402A3712D03BLL, g->GV(opt_result),
                       opt_result);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    case 69:
      HASH_INITIALIZED(0x353F92C20A856FC5LL, g->GV(other),
                       other);
      break;
    case 70:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 73:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 75:
      HASH_INITIALIZED(0x1018D4B85360194BLL, g->GV(opt_help),
                       opt_help);
      break;
    case 78:
      HASH_INITIALIZED(0x0510D59D8A97894ELL, g->GV(opt_debug),
                       opt_debug);
      break;
    case 81:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 85:
      HASH_INITIALIZED(0x54F0651B7317F455LL, g->GV(opt_cost),
                       opt_cost);
      break;
    case 92:
      HASH_INITIALIZED(0x0F60C6595ED9FADCLL, g->GV(arguments),
                       arguments);
      break;
    case 93:
      HASH_INITIALIZED(0x2183F11DA48E51DDLL, g->GV(opt_png),
                       opt_png);
      break;
    case 102:
      HASH_INITIALIZED(0x2A3A32A302743166LL, g->GV(descriptions),
                       descriptions);
      break;
    case 106:
      HASH_INITIALIZED(0x7E829BA8D21EE86ALL, g->GV(useless_tests),
                       useless_tests);
      break;
    case 110:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 113:
      HASH_INITIALIZED(0x69E7413AE0C88471LL, g->GV(value),
                       value);
      break;
    case 114:
      HASH_INITIALIZED(0x12CBDBCF4DD2FE72LL, g->GV(included),
                       included);
      break;
    case 120:
      HASH_INITIALIZED(0x7AE81F1CFBCA2BF8LL, g->GV(individual),
                       individual);
      break;
    case 121:
      HASH_INITIALIZED(0x3CDA59286E299AF9LL, g->GV(command_line),
                       command_line);
      break;
    case 127:
      HASH_INITIALIZED(0x5F8ACA212093407FLL, g->GV(costs),
                       costs);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
