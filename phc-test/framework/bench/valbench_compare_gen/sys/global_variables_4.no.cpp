
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "cg",
    "command_line",
    "opt_result",
    "opts",
    "arguments",
    "opt_short",
    "opt_help",
    "opt_png",
    "opt_debug",
    "opt_cost",
    "opt",
    "useless_tests",
    "short_keys",
    "abbreviations",
    "descriptions",
    "costs",
    "baseline",
    "other",
    "included",
    "key",
    "value",
    "results",
    "short_results",
    "individual",
  };
  if (idx >= 0 && idx < 36) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(cg);
    case 13: return GV(command_line);
    case 14: return GV(opt_result);
    case 15: return GV(opts);
    case 16: return GV(arguments);
    case 17: return GV(opt_short);
    case 18: return GV(opt_help);
    case 19: return GV(opt_png);
    case 20: return GV(opt_debug);
    case 21: return GV(opt_cost);
    case 22: return GV(opt);
    case 23: return GV(useless_tests);
    case 24: return GV(short_keys);
    case 25: return GV(abbreviations);
    case 26: return GV(descriptions);
    case 27: return GV(costs);
    case 28: return GV(baseline);
    case 29: return GV(other);
    case 30: return GV(included);
    case 31: return GV(key);
    case 32: return GV(value);
    case 33: return GV(results);
    case 34: return GV(short_results);
    case 35: return GV(individual);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
