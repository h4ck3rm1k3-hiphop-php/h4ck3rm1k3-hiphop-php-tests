
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "combine", "phc-test/framework/bench/valbench_compare",
  "combine_reducer", "phc-test/framework/bench/valbench_compare",
  "compare", "phc-test/framework/bench/valbench_compare",
  "create_barchart", "phc-test/framework/bench/valbench_compare",
  "print_barchart", "phc-test/framework/bench/valbench_compare",
  "print_costed_barchart", "phc-test/framework/bench/valbench_compare",
  "print_short_barchart", "phc-test/framework/bench/valbench_compare",
  "shorten", "phc-test/framework/bench/valbench_compare",
  "strip_cheap", "phc-test/framework/bench/valbench_compare",
  "strip_useless", "phc-test/framework/bench/valbench_compare",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
