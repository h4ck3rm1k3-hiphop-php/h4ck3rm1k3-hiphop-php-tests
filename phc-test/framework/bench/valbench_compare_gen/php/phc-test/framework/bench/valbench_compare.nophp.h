
#ifndef __GENERATED_php_phc_test_framework_bench_valbench_compare_nophp_h__
#define __GENERATED_php_phc_test_framework_bench_valbench_compare_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/bench/valbench_compare.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_print_barchart(CVarRef v_title, CVarRef v_results, Variant v_mean = false, Variant v_sort = false);
void f_print_costed_barchart(CVarRef v_old_results, CVarRef v_new_results);
Variant f_combine_reducer(Variant v_combined, CVarRef v_arr);
void f_create_barchart(CStrRef v_string, CVarRef v_title);
Variant f_strip_useless(Variant v_results);
Array f_strip_cheap(Variant v_baseline, Variant v_other);
Variant f_combine(CVarRef v_array);
void f_print_short_barchart(CVarRef v_results);
Variant f_shorten(Variant v_all_results);
Sequence f_compare(CVarRef v_r1, CVarRef v_r2);
Variant pm_php$phc_test$framework$bench$valbench_compare(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_bench_valbench_compare_nophp_h__
