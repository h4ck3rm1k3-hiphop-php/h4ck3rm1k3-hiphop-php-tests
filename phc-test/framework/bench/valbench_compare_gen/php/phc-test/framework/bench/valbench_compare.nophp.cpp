
#include <php/phc-test/framework/bench/valbench_compare.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/bench/valbench_compare line 257 */
void f_print_barchart(CVarRef v_title, CVarRef v_results, Variant v_mean //  = false
, Variant v_sort //  = false
) {
  FUNCTION_INJECTION(print_barchart);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_abbreviations __attribute__((__unused__)) = g->GV(abbreviations);
  Variant &gv_opt_png __attribute__((__unused__)) = g->GV(opt_png);
  String v_data;
  Primitive v_name = 0;
  Variant v_value;
  String v_string;

  {
  }
  (v_mean = toBoolean(v_mean) ? (("=arithmean")) : (("")));
  (v_sort = toBoolean(v_sort) ? (("=sort")) : (("")));
  (v_data = "");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_results.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_name = iter3->first();
      {
        if (isset(gv_abbreviations, v_name)) concat_assign(v_data, LINE(268,concat4(toString(gv_abbreviations.rvalAt(v_name)), " ", toString(v_value), "\n")));
        else concat_assign(v_data, LINE(270,concat4(toString(v_name), " ", toString(v_value), "\n")));
      }
    }
  }
  (v_string = concat_rev(LINE(282,concat6(toString(v_mean), "\n", toString(v_sort), "\n\n", v_data, "\n ")), concat3("yformat=%gx\nylabel=Improvement of ", toString(v_title), "\n")));
  LINE(284,f_create_barchart(v_string, v_title));
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 352 */
void f_print_costed_barchart(CVarRef v_old_results, CVarRef v_new_results) {
  FUNCTION_INJECTION(print_costed_barchart);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_descriptions __attribute__((__unused__)) = g->GV(descriptions);
  Variant &gv_short_keys __attribute__((__unused__)) = g->GV(short_keys);
  Variant &gv_costs __attribute__((__unused__)) = g->GV(costs);
  Variant &gv_opt_debug __attribute__((__unused__)) = g->GV(opt_debug);
  Variant v_results;
  String v_cluster;
  Variant v_key;
  String v_data;
  Primitive v_testname = 0;
  Variant v_array;
  Variant v_new;
  Variant v_old;
  Variant v_cost;
  Variant v_new_instructions;
  Numeric v_total = 0;
  String v_string;

  {
  }
  if (toBoolean(gv_opt_debug)) LINE(357,x_var_dump(1, v_results));
  (v_cluster = "=cluster");
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = gv_short_keys.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_key = iter6->second();
      {
        if (equal(v_key, "instruction")) continue;
        if (isset(gv_descriptions, v_key)) (v_key = gv_descriptions.rvalAt(v_key));
        concat_assign(v_cluster, toString(";") + toString(v_key));
      }
    }
  }
  (v_data = "");
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_new_results.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_array = iter9->second();
      v_testname = iter9->first();
      {
        concat_assign(v_data, toString(v_testname) + toString(" "));
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = gv_short_keys.begin(); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_key = iter12->second();
            {
              if (!equal(v_key, "instruction")) {
                (v_new = v_new_results.rvalAt(v_testname).rvalAt(v_key));
                (v_old = v_old_results.rvalAt(v_testname).rvalAt(v_key));
                (v_cost = gv_costs.rvalAt(v_key));
                (v_new_instructions = v_new_results.rvalAt(v_testname).rvalAt("instruction", 0x3965FA67120F25DDLL));
                (v_total = divide((v_old - v_new) * v_cost * 100LL, v_new_instructions));
                concat_assign(v_data, toString(v_total) + toString(" "));
              }
            }
          }
        }
        concat_assign(v_data, "\n");
      }
    }
  }
  (v_string = LINE(408,concat5("yformat=%g%%\nylabel=Reduction due to phc compiled code\n=sort\n=arithmean\n", v_cluster, "\n=table\nextraops=set yrange [-5:15]\nlegendy=1370\nlegendx=2350\n\n\n", v_data, "\n ")));
  if (toBoolean(gv_opt_debug)) LINE(410,x_var_dump(1, v_string));
  LINE(412,f_create_barchart(v_string, "cost"));
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 417 */
Variant f_combine_reducer(Variant v_combined, CVarRef v_arr) {
  FUNCTION_INJECTION(combine_reducer);
  Primitive v_key = 0;
  Variant v_val;

  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_arr.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_val = iter15->second();
      v_key = iter15->first();
      {
        lval(v_combined.lvalAt(v_key)) += v_val;
      }
    }
  }
  return v_combined;
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 287 */
void f_create_barchart(CStrRef v_string, CVarRef v_title) {
  FUNCTION_INJECTION(create_barchart);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_png __attribute__((__unused__)) = g->GV(opt_png);
  LINE(291,x_file_put_contents(toString(v_title) + toString(".plot"), v_string));
  if (toBoolean(gv_opt_png)) f_shell_exec(toString("bash -c \"bargraph.pl -png ") + toString(v_title) + toString(".plot > ") + toString(v_title) + toString(".png\""));
  else f_shell_exec(toString("bash -c \"bargraph.pl ") + toString(v_title) + toString(".plot > ") + toString(v_title) + toString(".eps\""));
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 461 */
Variant f_strip_useless(Variant v_results) {
  FUNCTION_INJECTION(strip_useless);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_useless_tests __attribute__((__unused__)) = g->GV(useless_tests);
  Variant v_useless_tests;
  Primitive v_test_name = 0;
  Variant v_test_results;

  v_useless_tests = ref(g->GV(useless_tests));
  {
    LOOP_COUNTER(16);
    for (ArrayIterPtr iter18 = v_results.begin(); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_test_results = iter18->second();
      v_test_name = iter18->first();
      {
        if (LINE(467,x_in_array(v_test_name, v_useless_tests))) v_results.weakRemove(v_test_name);
      }
    }
  }
  return v_results;
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 473 */
Array f_strip_cheap(Variant v_baseline, Variant v_other) {
  FUNCTION_INJECTION(strip_cheap);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_short_keys __attribute__((__unused__)) = g->GV(short_keys);
  Variant &gv_costs __attribute__((__unused__)) = g->GV(costs);
  Variant &gv_opt_debug __attribute__((__unused__)) = g->GV(opt_debug);
  Primitive v_test_name = 0;
  Variant v_test_result;
  Primitive v_key = 0;
  Variant v_value;
  Variant v_base_instructions;
  Variant v_other_instructions;
  int64 v_threshhold = 0;
  Numeric v_base_cost = 0;
  Numeric v_other_cost = 0;

  {
  }
  {
    LOOP_COUNTER(19);
    for (ArrayIterPtr iter21 = v_baseline.begin(); !iter21->end(); iter21->next()) {
      LOOP_COUNTER_CHECK(19);
      v_test_result = iter21->second();
      v_test_name = iter21->first();
      {
        {
          LOOP_COUNTER(22);
          for (ArrayIterPtr iter24 = v_test_result.begin(); !iter24->end(); iter24->next()) {
            LOOP_COUNTER_CHECK(22);
            v_value = iter24->second();
            v_key = iter24->first();
            {
              (v_base_instructions = v_test_result.rvalAt("instruction", 0x3965FA67120F25DDLL));
              (v_other_instructions = v_other.rvalAt(v_test_name).rvalAt("instruction", 0x3965FA67120F25DDLL));
              (v_threshhold = 10LL);
              (v_base_cost = v_threshhold * v_value * gv_costs.rvalAt(v_key));
              (v_other_cost = v_threshhold * v_other.rvalAt(v_test_name).rvalAt(v_key) * gv_costs.rvalAt(v_key));
              if (less(v_base_cost, v_base_instructions) && less(v_other_cost, v_other_instructions)) {
                lval(v_other.lvalAt(v_test_name)).set(v_key, (987654321LL));
                lval(v_baseline.lvalAt(v_test_name)).set(v_key, (1LL));
              }
            }
          }
        }
      }
    }
  }
  return Array(ArrayInit(2).set(0, v_baseline).set(1, v_other).create());
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 427 */
Variant f_combine(CVarRef v_array) {
  FUNCTION_INJECTION(combine);
  Variant eo_0;
  Variant eo_1;
  return LINE(430,(assignCallTemp(eo_0, x_array_values(v_array)),x_array_reduce(eo_0, "combine_reducer")));
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 303 */
void f_print_short_barchart(CVarRef v_results) {
  FUNCTION_INJECTION(print_short_barchart);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_descriptions __attribute__((__unused__)) = g->GV(descriptions);
  Variant &gv_short_keys __attribute__((__unused__)) = g->GV(short_keys);
  Variant &gv_costs __attribute__((__unused__)) = g->GV(costs);
  Variant &gv_opt_debug __attribute__((__unused__)) = g->GV(opt_debug);
  String v_cluster;
  Variant v_key;
  String v_data;
  Primitive v_name = 0;
  Variant v_array;
  String v_string;

  {
  }
  if (toBoolean(gv_opt_debug)) LINE(308,x_var_dump(1, v_results));
  (v_cluster = "=cluster");
  {
    LOOP_COUNTER(25);
    for (ArrayIterPtr iter27 = gv_short_keys.begin(); !iter27->end(); iter27->next()) {
      LOOP_COUNTER_CHECK(25);
      v_key = iter27->second();
      {
        if (isset(gv_descriptions, v_key)) (v_key = gv_descriptions.rvalAt(v_key));
        concat_assign(v_cluster, toString(";") + toString(v_key));
      }
    }
  }
  (v_data = "");
  {
    LOOP_COUNTER(28);
    for (ArrayIterPtr iter30 = v_results.begin(); !iter30->end(); iter30->next()) {
      LOOP_COUNTER_CHECK(28);
      v_array = iter30->second();
      v_name = iter30->first();
      {
        concat_assign(v_data, toString(v_name) + toString(" "));
        {
          LOOP_COUNTER(31);
          for (ArrayIterPtr iter33 = gv_short_keys.begin(); !iter33->end(); iter33->next()) {
            LOOP_COUNTER_CHECK(31);
            v_key = iter33->second();
            {
              concat_assign(v_data, toString(v_array.rvalAt(v_key)) + toString(" "));
            }
          }
        }
        concat_assign(v_data, "\n");
      }
    }
  }
  (v_string = LINE(342,concat5("yformat=%gx\nylabel=Reduction due to phc compiled code\n=sort\n=arithmean\n", v_cluster, "\n=table\n\n", v_data, "\n ")));
  if (toBoolean(gv_opt_debug)) LINE(346,x_var_dump(1, v_string));
  LINE(348,f_create_barchart(v_string, "short"));
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 434 */
Variant f_shorten(Variant v_all_results) {
  FUNCTION_INJECTION(shorten);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_short_keys __attribute__((__unused__)) = g->GV(short_keys);
  Variant v_short_keys;
  Variant &gv_included __attribute__((__unused__)) = g->GV(included);
  Variant v_included;
  Primitive v_test_name = 0;
  Variant v_test_result;
  Primitive v_key = 0;
  Variant v_value;

  {
    v_short_keys = ref(g->GV(short_keys));
    v_included = ref(g->GV(included));
  }
  {
    LOOP_COUNTER(34);
    for (ArrayIterPtr iter36 = v_all_results.begin(); !iter36->end(); iter36->next()) {
      LOOP_COUNTER_CHECK(34);
      v_test_result = iter36->second();
      v_test_name = iter36->first();
      {
        if (LINE(442,x_in_array(v_test_name, v_included)) || equal(v_test_name, "All") || equal(LINE(444,x_sizeof(v_included)), 0LL)) {
          {
            LOOP_COUNTER(37);
            for (ArrayIterPtr iter39 = v_test_result.begin(); !iter39->end(); iter39->next()) {
              LOOP_COUNTER_CHECK(37);
              v_value = iter39->second();
              v_key = iter39->first();
              {
                if (!(LINE(448,x_in_array(v_key, v_short_keys)))) lval(unsetLval(v_all_results, v_test_name)).weakRemove(v_key);
              }
            }
          }
        }
        else {
          v_all_results.weakRemove(v_test_name);
        }
      }
    }
  }
  return v_all_results;
} /* function */
/* SRC: phc-test/framework/bench/valbench_compare line 237 */
Sequence f_compare(CVarRef v_r1, CVarRef v_r2) {
  FUNCTION_INJECTION(compare);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_debug __attribute__((__unused__)) = g->GV(opt_debug);
  Primitive v_test_name = 0;
  Variant v_test_result;
  Primitive v_key = 0;
  Variant v_value;
  Variant v_base_val;
  Numeric v_answer = 0;
  Sequence v_result;

  {
    LOOP_COUNTER(40);
    for (ArrayIterPtr iter42 = v_r2.begin(); !iter42->end(); iter42->next()) {
      LOOP_COUNTER_CHECK(40);
      v_test_result = iter42->second();
      v_test_name = iter42->first();
      {
        {
          LOOP_COUNTER(43);
          for (ArrayIterPtr iter45 = v_test_result.begin(); !iter45->end(); iter45->next()) {
            LOOP_COUNTER_CHECK(43);
            v_value = iter45->second();
            v_key = iter45->first();
            {
              (v_base_val = v_r1.rvalAt(v_test_name).rvalAt(v_key));
              LINE(246,x_assert(!equal(v_base_val, 0LL)));
              (v_answer = divide(v_base_val, v_value));
              lval(v_result.lvalAt(v_test_name)).set(v_key, (divide(v_base_val, v_value)));
              if (toBoolean(gv_opt_debug)) echo(concat_rev(LINE(249,concat6(toString(v_answer), " = ", toString(v_base_val), " / ", toString(v_value), "\n")), concat4(toString(v_test_name), " => ", toString(v_key), ": ")));
            }
          }
        }
      }
    }
  }
  return v_result;
} /* function */
Variant i_combine_reducer(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5F3758B1C60D4001LL, combine_reducer) {
    return (f_combine_reducer(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$phc_test$framework$bench$valbench_compare(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/bench/valbench_compare);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$bench$valbench_compare;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cg") : g->GV(cg);
  Variant &v_command_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("command_line") : g->GV(command_line);
  Variant &v_opt_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_result") : g->GV(opt_result);
  Variant &v_opts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opts") : g->GV(opts);
  Variant &v_arguments __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arguments") : g->GV(arguments);
  Variant &v_opt_short __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_short") : g->GV(opt_short);
  Variant &v_opt_help __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_help") : g->GV(opt_help);
  Variant &v_opt_png __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_png") : g->GV(opt_png);
  Variant &v_opt_debug __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_debug") : g->GV(opt_debug);
  Variant &v_opt_cost __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_cost") : g->GV(opt_cost);
  Variant &v_opt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt") : g->GV(opt);
  Variant &v_useless_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("useless_tests") : g->GV(useless_tests);
  Variant &v_short_keys __attribute__((__unused__)) = (variables != gVariables) ? variables->get("short_keys") : g->GV(short_keys);
  Variant &v_abbreviations __attribute__((__unused__)) = (variables != gVariables) ? variables->get("abbreviations") : g->GV(abbreviations);
  Variant &v_descriptions __attribute__((__unused__)) = (variables != gVariables) ? variables->get("descriptions") : g->GV(descriptions);
  Variant &v_costs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("costs") : g->GV(costs);
  Variant &v_baseline __attribute__((__unused__)) = (variables != gVariables) ? variables->get("baseline") : g->GV(baseline);
  Variant &v_other __attribute__((__unused__)) = (variables != gVariables) ? variables->get("other") : g->GV(other);
  Variant &v_included __attribute__((__unused__)) = (variables != gVariables) ? variables->get("included") : g->GV(included);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);
  Variant &v_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("results") : g->GV(results);
  Variant &v_short_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("short_results") : g->GV(short_results);
  Variant &v_individual __attribute__((__unused__)) = (variables != gVariables) ? variables->get("individual") : g->GV(individual);

  LINE(6,require("Console/Getopt.php", true, variables, "phc-test/framework/bench/"));
  LINE(7,x_set_include_path(concat("test/framework/external/:", x_get_include_path())));
  (v_cg = LINE(8,create_object("console_getopt", Array())));
  (v_command_line = LINE(9,(assignCallTemp(eo_1, v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)),x_join(" ", eo_1))));
  (v_opt_result = (assignCallTemp(eo_0, ref(LINE(10,v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)))),v_cg.o_invoke("getopt", Array(ArrayInit(2).set(0, eo_0).set(1, "shpdc").create()), 0x2C6826999658AA5ELL)));
  if (!(LINE(11,x_is_array(v_opt_result)))) f_exit(concat(toString(v_opt_result.o_get("message", 0x3EAA4B97155366DFLL)), "\n"));
  df_lambda_1(v_opt_result, v_opts, v_arguments);
  (v_opt_short = false);
  (v_opt_help = false);
  (v_opt_png = false);
  (v_opt_debug = false);
  (v_opt_cost = false);
  {
    LOOP_COUNTER(46);
    for (ArrayIterPtr iter48 = v_opts.begin(); !iter48->end(); iter48->next()) {
      LOOP_COUNTER_CHECK(46);
      v_opt = iter48->second();
      {
        {
          Variant tmp50 = (v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          int tmp51 = -1;
          if (equal(tmp50, ("h"))) {
            tmp51 = 0;
          } else if (equal(tmp50, ("s"))) {
            tmp51 = 1;
          } else if (equal(tmp50, ("p"))) {
            tmp51 = 2;
          } else if (equal(tmp50, ("d"))) {
            tmp51 = 3;
          } else if (equal(tmp50, ("c"))) {
            tmp51 = 4;
          }
          switch (tmp51) {
          case 0:
            {
              (v_opt_help = true);
              goto break49;
            }
          case 1:
            {
              (v_opt_short = true);
              goto break49;
            }
          case 2:
            {
              (v_opt_png = true);
              goto break49;
            }
          case 3:
            {
              (v_opt_debug = true);
              goto break49;
            }
          case 4:
            {
              (v_opt_cost = true);
              goto break49;
            }
          }
          break49:;
        }
      }
    }
  }
  if (toBoolean(v_opt_help) || less(LINE(33,x_count(v_arguments)), 2LL)) {
    f_exit(toString("valbench_compare - Compares valbench results with a baseline. The baseline and the results are a serialized array, as produced by valbench -s.\n\nUsage: valbench_compare [OPTIONS] BASELINE RESULT [BENCHMARKS_NAMES]\n\nOptions:\n    -h     Print this help message\n    -s     Generate a short result (named benchmarks for important metrics, ignoring insignficant results). This expects extra arguments.\n    -c     Generate a costed result (named benchmarks for important metrics). This expects extra arguments.\n    -p     Create PNGs instead of EPSs.\n    -d     Print debug info"));
  }
  (v_useless_tests = ScalarArrays::sa_[0]);
  (v_short_keys = ScalarArrays::sa_[1]);
  (v_abbreviations = ScalarArrays::sa_[2]);
  (v_descriptions = ScalarArrays::sa_[3]);
  (v_costs = ScalarArrays::sa_[4]);
  (v_baseline = LINE(166,x_unserialize(toString(v_arguments.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  (v_other = LINE(167,x_unserialize(toString(v_arguments.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
  LINE(169,(assignCallTemp(eo_1, x_var_export(v_baseline, true)),x_file_put_contents("baseline.data", eo_1)));
  LINE(170,(assignCallTemp(eo_1, x_var_export(v_other, true)),x_file_put_contents("other.data", eo_1)));
  (v_included = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(52);
    for (ArrayIterPtr iter54 = v_arguments.begin(); !iter54->end(); iter54->next()) {
      LOOP_COUNTER_CHECK(52);
      v_value = iter54->second();
      v_key = iter54->first();
      {
        if (less(v_key, 2LL)) continue;
        v_included.append((v_value));
      }
    }
  }
  if (same(v_baseline, false)) f_exit(toString("Error in unserializing arg0: ") + toString(v_arguments.rvalAt(0, 0x77CFA1EEF01BCA90LL)));
  if (same(v_other, false)) f_exit(toString("Error in unserializing arg1: ") + toString(v_arguments.rvalAt(1, 0x5BCA7C69B794F8CELL)));
  (v_baseline = LINE(187,f_strip_useless(v_baseline)));
  (v_other = LINE(188,f_strip_useless(v_other)));
  if ((!(toBoolean(v_opt_short))) && (!(toBoolean(v_opt_cost)))) {
    v_baseline.set("All", (LINE(192,f_combine(v_baseline))), 0x2EEC5132A6A663E7LL);
    v_other.set("All", (LINE(193,f_combine(v_other))), 0x2EEC5132A6A663E7LL);
  }
  if (toBoolean(v_opt_short)) {
    df_lambda_2(LINE(198,f_strip_cheap(v_baseline, v_other)), v_baseline, v_other);
  }
  (v_results = LINE(201,f_compare(v_baseline, v_other)));
  if (toBoolean(v_opt_short)) {
    (v_short_results = LINE(212,f_shorten(v_results)));
    LINE(214,f_print_short_barchart(v_short_results));
  }
  else if (toBoolean(v_opt_cost)) {
    LINE(218,f_print_costed_barchart(v_baseline, v_other));
  }
  else {
    {
      LOOP_COUNTER(55);
      for (ArrayIterPtr iter57 = v_results.begin(); !iter57->end(); iter57->next()) {
        LOOP_COUNTER_CHECK(55);
        v_individual = iter57->second();
        v_key = iter57->first();
        {
          LINE(225,f_print_barchart(v_key, v_individual));
        }
      }
    }
    LINE(229,(assignCallTemp(eo_1, f_combine(v_results)),f_print_barchart("All", eo_1)));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
