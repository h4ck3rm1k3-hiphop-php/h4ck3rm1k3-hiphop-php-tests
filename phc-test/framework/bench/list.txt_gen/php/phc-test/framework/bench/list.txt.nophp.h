
#ifndef __GENERATED_php_phc_test_framework_bench_list_txt_nophp_h__
#define __GENERATED_php_phc_test_framework_bench_list_txt_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/bench/list.txt.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$bench$list_txt(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_bench_list_txt_nophp_h__
