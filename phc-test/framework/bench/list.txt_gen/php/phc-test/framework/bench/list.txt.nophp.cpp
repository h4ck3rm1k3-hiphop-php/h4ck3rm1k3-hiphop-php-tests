
#include <php/phc-test/framework/bench/list.txt.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$phc_test$framework$bench$list_txt(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/bench/list.txt);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$bench$list_txt;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("test/subjects/benchmarks/php-benchmarks/benchcli/tests/test_ackermann\ntest/subjects/benchmarks/php-benchmarks/benchcli/misc/cachegrindparser\ntest/subjects/benchmarks/php-benchmarks/benchcli/misc/getargs\ntest/subjects/benchmarks/php-benchmarks/benchcli/misc/smapsparser\ntest/subjects/benchmarks/php-benchmarks/benchcli/misc/timer\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/phpWhirl/whirl\nt");
  echo("est/subjects/benchmarks/php-benchmarks/benchcli/tests/raytracer/raytrace\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-aes\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto-md5\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_crypto\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_deltablue\ntest/subjects/benchmarks/php-benchmarks/benchcli");
  echo("/tests/test_gaussjordan\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_j4p5\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_pcre\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_raytrace\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_richards\ntest/subjects/benchmarks/php-benchmarks/benchcli/tests/test_whirl\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
