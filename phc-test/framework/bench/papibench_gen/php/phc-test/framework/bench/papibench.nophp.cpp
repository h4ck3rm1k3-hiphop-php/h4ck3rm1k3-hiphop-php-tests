
#include <php/phc-test/framework/bench/papibench.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/bench/papibench line 64 */
Variant f_run_benchmark(CVarRef v_filename) {
  FUNCTION_INJECTION(run_benchmark);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_php __attribute__((__unused__)) = g->GV(opt_php);
  Variant v_opt_php;
  Variant v_command;
  Variant v_out;
  Variant v_err;
  Variant v_exit;
  Array v_match_names;
  String v_start;
  String v_middle;
  String v_number;
  String v_rw;
  String v_branch_rw;
  String v_end;
  Variant v_matches;
  Variant v_match;
  Variant v_results;

  v_opt_php = ref(g->GV(opt_php));
  if (toBoolean(v_opt_php)) (v_command = toString("valgrind --tool=cachegrind --branch-sim=yes /usr/local/php-opt/bin/php ") + toString(v_filename));
  else (v_command = toString("misc/comp -B -O ") + toString(v_filename));
  df_lambda_2(LINE(76,invoke_failed("complete_exec", Array(ArrayInit(3).set(0, ref(v_command)).set(1, null).set(2, 0LL).create()), 0x00000000739F6DDCLL)), v_out, v_err, v_exit);
  if (!equal(v_exit, 0LL)) f_exit(toString("FAILURE:\n\n") + toString(v_err));
  (v_match_names = ScalarArrays::sa_[0]);
  (v_start = "^==\\d+==");
  (v_middle = ":\\s+");
  (v_number = "([0-9,]+)");
  (v_rw = LINE(116,concat5("\\s*\\(\\s*", v_number, " rd\\s*\\+\\s*", v_number, " wr\\)")));
  (v_branch_rw = LINE(117,concat5("\\s*\\(\\s*", v_number, " cond\\s*\\+\\s*", v_number, " ind\\)")));
  (v_end = "$.");
  LINE(133,(assignCallTemp(eo_0, concat_rev((assignCallTemp(eo_3, LINE(128,concat6(v_start, " L2\\s+refs", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_4, LINE(129,concat6(v_start, " L2\\s+misses", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_6, LINE(131,concat6(v_start, " Branches", v_middle, v_number, v_branch_rw, v_end))),assignCallTemp(eo_7, LINE(132,concat6(v_start, " Mispredicts", v_middle, v_number, v_branch_rw, v_end))),concat6(eo_3, eo_4, ".*", eo_6, eo_7, "/ms")), concat_rev(LINE(133,(assignCallTemp(eo_3, LINE(122,concat5(v_start, " L2i\\s+misses", v_middle, v_number, v_end))),assignCallTemp(eo_5, LINE(124,concat6(v_start, " D\\s+refs", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_6, LINE(125,concat6(v_start, " D1\\s+misses", v_middle, v_number, v_rw, v_end))),assignCallTemp(eo_7, LINE(126,concat6(v_start, " L2d\\s+misses", v_middle, v_number, v_rw, v_end))),concat6(eo_3, ".*", eo_5, eo_6, eo_7, ".*"))), LINE(133,(assignCallTemp(eo_4, LINE(120,concat5(v_start, " I\\s+refs", v_middle, v_number, v_end))),assignCallTemp(eo_5, LINE(121,concat5(v_start, " I1\\s+misses", v_middle, v_number, v_end))),concat3("/", eo_4, eo_5)))))),assignCallTemp(eo_1, toString(v_err)),assignCallTemp(eo_2, ref(v_matches)),x_preg_match(eo_0, eo_1, eo_2)));
  LINE(135,x_array_shift(ref(v_matches)));
  {
    LOOP_COUNTER(1);
    Variant map2 = ref(v_matches);
    map2.escalate();
    for (MutableArrayIterPtr iter3 = map2.begin(NULL, v_match); iter3->advance();) {
      LOOP_COUNTER_CHECK(1);
      (v_match = LINE(139,x_preg_replace("/,/", "", v_match)));
    }
  }
  unset(v_match);
  (v_results = LINE(144,x_array_combine(v_match_names, v_matches)));
  return v_results;
} /* function */
Variant pm_php$phc_test$framework$bench$papibench(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/bench/papibench);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$bench$papibench;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cg") : g->GV(cg);
  Variant &v_command_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("command_line") : g->GV(command_line);
  Variant &v_opt_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_result") : g->GV(opt_result);
  Variant &v_opts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opts") : g->GV(opts);
  Variant &v_arguments __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arguments") : g->GV(arguments);
  Variant &v_opt_help __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_help") : g->GV(opt_help);
  Variant &v_opt_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_php") : g->GV(opt_php);
  Variant &v_opt_serialize __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_serialize") : g->GV(opt_serialize);
  Variant &v_opt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt") : g->GV(opt);
  Variant &v_benchs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("benchs") : g->GV(benchs);
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_bench __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bench") : g->GV(bench);
  Variant &v_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("results") : g->GV(results);

  LINE(4,x_set_include_path(concat("test/framework/external/:test/framework/", x_get_include_path())));
  LINE(5,require("lib/header.php", true, variables, "phc-test/framework/bench/"));
  LINE(6,require("Console/Getopt.php", true, variables, "phc-test/framework/bench/"));
  (v_cg = LINE(9,create_object("console_getopt", Array())));
  (v_command_line = LINE(10,(assignCallTemp(eo_1, v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)),x_join(" ", eo_1))));
  (v_opt_result = (assignCallTemp(eo_0, ref(LINE(11,v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)))),v_cg.o_invoke("getopt", Array(ArrayInit(2).set(0, eo_0).set(1, "hps").create()), 0x2C6826999658AA5ELL)));
  if (!(LINE(12,x_is_array(v_opt_result)))) f_exit(concat(toString(v_opt_result.o_get("message", 0x3EAA4B97155366DFLL)), "\n"));
  df_lambda_1(v_opt_result, v_opts, v_arguments);
  (v_opt_help = false);
  (v_opt_php = false);
  (v_opt_serialize = false);
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_opts.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_opt = iter6->second();
      {
        {
          Variant tmp8 = (v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          int tmp9 = -1;
          if (equal(tmp8, ("h"))) {
            tmp9 = 0;
          } else if (equal(tmp8, ("p"))) {
            tmp9 = 1;
          } else if (equal(tmp8, ("s"))) {
            tmp9 = 2;
          }
          switch (tmp9) {
          case 0:
            {
              (v_opt_help = true);
              goto break7;
            }
          case 1:
            {
              (v_opt_php = true);
              goto break7;
            }
          case 2:
            {
              (v_opt_serialize = true);
              goto break7;
            }
          }
          break7:;
        }
      }
    }
  }
  if (toBoolean(v_opt_help) || more(LINE(30,x_count(v_arguments)), 0LL)) {
    f_exit(toString("bench - Compiles (with optimization) the Zend benchmark, and measures performance results using Cachegrind - phpcompiler.org\n\nUsage: benchmark [OPTIONS]\n\nOptions:\n\t -h     Print this help message\n\t -p     Run the benchmark using PHP, instead of compiling the code with phc\n"));
  }
  (v_benchs = LINE(47,x_glob(concat(x_dirname(get_source_filename("phc-test/framework/bench/papibench")), "/../../subjects/benchmarks/zend/individual/*.php"))));
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_benchs.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_filename = iter12->second();
      {
        (v_bench = LINE(50,x_basename(toString(v_filename), ".php")));
        v_results.set(v_bench, (LINE(51,f_run_benchmark(v_filename))));
      }
    }
  }
  if (toBoolean(v_opt_serialize)) echo((LINE(58,x_serialize(v_results))));
  else LINE(60,x_var_dump(1, v_results));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
