
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 7:
      HASH_RETURN(0x4E80F79695D41A07LL, g->GV(opt),
                  opt);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 11:
      HASH_RETURN(0x1018D4B85360194BLL, g->GV(opt_help),
                  opt_help);
      break;
    case 12:
      HASH_RETURN(0x21DEE4EB204AE10CLL, g->GV(cg),
                  cg);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 20:
      HASH_RETURN(0x4B9E7F4817450F14LL, g->GV(opt_fdo),
                  opt_fdo);
      break;
    case 22:
      HASH_RETURN(0x63BC5E080D162C56LL, g->GV(tally),
                  tally);
      break;
    case 24:
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 28:
      HASH_RETURN(0x0F60C6595ED9FADCLL, g->GV(arguments),
                  arguments);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 39:
      HASH_RETURN(0x14500986141654E7LL, g->GV(opt_php),
                  opt_php);
      break;
    case 45:
      HASH_RETURN(0x4A5D0431E1A3CAEDLL, g->GV(filename),
                  filename);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 48:
      HASH_RETURN(0x5C390FD428B50E30LL, g->GV(opt_long),
                  opt_long);
      break;
    case 52:
      HASH_RETURN(0x421780730E09F634LL, g->GV(opts),
                  opts);
      break;
    case 56:
      HASH_RETURN(0x66E32C011EFD3838LL, g->GV(results),
                  results);
      break;
    case 57:
      HASH_RETURN(0x3CDA59286E299AF9LL, g->GV(command_line),
                  command_line);
      break;
    case 59:
      HASH_RETURN(0x67DB402A3712D03BLL, g->GV(opt_result),
                  opt_result);
      HASH_RETURN(0x2A35E05780AA8E3BLL, g->GV(command),
                  command);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
