
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "cg",
    "command_line",
    "opt_result",
    "opts",
    "arguments",
    "opt_help",
    "opt_long",
    "opt_php",
    "opt_fdo",
    "opt",
    "filename",
    "command",
    "i",
    "results",
    "tally",
  };
  if (idx >= 0 && idx < 27) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(cg);
    case 13: return GV(command_line);
    case 14: return GV(opt_result);
    case 15: return GV(opts);
    case 16: return GV(arguments);
    case 17: return GV(opt_help);
    case 18: return GV(opt_long);
    case 19: return GV(opt_php);
    case 20: return GV(opt_fdo);
    case 21: return GV(opt);
    case 22: return GV(filename);
    case 23: return GV(command);
    case 24: return GV(i);
    case 25: return GV(results);
    case 26: return GV(tally);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
