
#include <php/phc-test/framework/bench/benchmark.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/bench/benchmark line 95 */
void f_process_results(CVarRef v_string_input) {
  FUNCTION_INJECTION(process_results);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_tally __attribute__((__unused__)) = g->GV(tally);
  Array v_strings;
  Variant v_line;
  Variant v_matches;

  (v_strings = LINE(98,x_explode("\n", toString(v_string_input))));
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_strings.begin(); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_line = iter3.second();
      {
        if (toBoolean(LINE(102,x_preg_match("/^(\\S+)\\s+([\\d\\.]+)$/", toString(v_line), ref(v_matches))))) {
          lval(gv_tally.lvalAt(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL))).append((v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
          if (same(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "Total")) echo(toString(v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
        }
        else {
          if (!((equal(v_line, "") || toBoolean(LINE(111,x_preg_match("/^-*$/", toString(v_line))))))) print(LINE(112,concat3("Skipping result line: ", toString(v_line), "\n")));
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/bench/benchmark line 118 */
void f_dump_results() {
  FUNCTION_INJECTION(dump_results);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_tally __attribute__((__unused__)) = g->GV(tally);
  Primitive v_key = 0;
  Variant v_result;
  int v_num = 0;
  Numeric v_average = 0;

  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = gv_tally.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_result = iter6->second();
      v_key = iter6->first();
      {
        (v_num = LINE(123,x_count(v_result)));
        (v_average = divide(LINE(124,x_array_sum(v_result)), v_num));
        LINE(125,(assignCallTemp(eo_0, concat5("%-20s (", toString(v_num), "):  ", toString(v_average), "\n")),assignCallTemp(eo_1, v_key),x_printf(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))));
      }
    }
  }
} /* function */
Variant pm_php$phc_test$framework$bench$benchmark(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/bench/benchmark);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$bench$benchmark;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cg") : g->GV(cg);
  Variant &v_command_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("command_line") : g->GV(command_line);
  Variant &v_opt_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_result") : g->GV(opt_result);
  Variant &v_opts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opts") : g->GV(opts);
  Variant &v_arguments __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arguments") : g->GV(arguments);
  Variant &v_opt_help __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_help") : g->GV(opt_help);
  Variant &v_opt_long __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_long") : g->GV(opt_long);
  Variant &v_opt_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_php") : g->GV(opt_php);
  Variant &v_opt_fdo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt_fdo") : g->GV(opt_fdo);
  Variant &v_opt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("opt") : g->GV(opt);
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_command __attribute__((__unused__)) = (variables != gVariables) ? variables->get("command") : g->GV(command);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("results") : g->GV(results);

  LINE(5,require("Console/Getopt.php", true, variables, "phc-test/framework/bench/"));
  LINE(6,x_set_include_path(concat("test/framework/external/:", x_get_include_path())));
  (v_cg = LINE(7,create_object("console_getopt", Array())));
  (v_command_line = LINE(8,(assignCallTemp(eo_1, v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)),x_join(" ", eo_1))));
  (v_opt_result = (assignCallTemp(eo_0, ref(LINE(9,v_cg.o_invoke_few_args("readPHPArgv", 0x7E99DEC75BDD6400LL, 0)))),v_cg.o_invoke("getopt", Array(ArrayInit(2).set(0, eo_0).set(1, "lhpf").create()), 0x2C6826999658AA5ELL)));
  if (!(LINE(10,x_is_array(v_opt_result)))) f_exit(concat(toString(v_opt_result.o_get("message", 0x3EAA4B97155366DFLL)), "\n"));
  df_lambda_1(v_opt_result, v_opts, v_arguments);
  (v_opt_help = false);
  (v_opt_long = false);
  (v_opt_php = false);
  (v_opt_fdo = false);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_opts.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_opt = iter9->second();
      {
        {
          Variant tmp11 = (v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          int tmp12 = -1;
          if (equal(tmp11, ("h"))) {
            tmp12 = 0;
          } else if (equal(tmp11, ("l"))) {
            tmp12 = 1;
          } else if (equal(tmp11, ("p"))) {
            tmp12 = 2;
          } else if (equal(tmp11, ("f"))) {
            tmp12 = 3;
          }
          switch (tmp12) {
          case 0:
            {
              (v_opt_help = true);
              goto break10;
            }
          case 1:
            {
              (v_opt_long = true);
              goto break10;
            }
          case 2:
            {
              (v_opt_php = true);
              goto break10;
            }
          case 3:
            {
              (v_opt_fdo = true);
              goto break10;
            }
          }
          break10:;
        }
      }
    }
  }
  if (toBoolean(v_opt_help) || more(LINE(30,x_count(v_arguments)), 0LL)) {
    f_exit(toString("bench - Compiles (with optimization) the Zend benchmark, producing averaged results for 10 runs - phpcompiler.org\n\nUsage: benchmark [OPTIONS]\n\nOptions:\n\t -h     Print this help message\n\t -l     Run the long benchmark, instead of the standard one.\n\t -p     Run the benchmark using PHP, instead of compiling the code with phc\n\t -f     Use gcc's feedback directed optimizations\n"));
  }
  (v_filename = "test/subjects/benchmarks/zend/bench.php");
  (v_command = "./bench.out");
  if (toBoolean(v_opt_long)) {
    (v_filename = "test/subjects/benchmarks/zend/long_bench.php");
    (v_command = "./long_bench.out");
  }
  if (toBoolean(v_opt_php)) (v_command = toString("/usr/local/php-opt/bin/php ") + toString(v_filename));
  else {
    print("Initializing test\n");
    if (toBoolean(v_opt_fdo)) {
      print("Generating FDO\n");
      f_shell_exec(toString("misc/comp -O -F ") + toString(v_filename));
      print("Using and generating FDO\n");
      f_shell_exec(toString("misc/comp -O -f -F ") + toString(v_filename));
      print("Using FDO\n");
      f_shell_exec(toString("misc/comp -O -f ") + toString(v_filename));
    }
    else {
      f_shell_exec(toString("misc/comp -O ") + toString(v_filename));
    }
  }
  {
    LOOP_COUNTER(13);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(13);
      {
        print(LINE(85,concat3("Starting benchmark ", toString(v_i), ": ")));
        (v_results = f_shell_exec(toString(v_command)));
        LINE(87,f_process_results(v_results));
        echo("\n");
      }
    }
  }
  LINE(91,f_dump_results());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
