
#ifndef __GENERATED_php_phc_test_framework_bench_benchmark_nophp_h__
#define __GENERATED_php_phc_test_framework_bench_benchmark_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/bench/benchmark.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_process_results(CVarRef v_string_input);
Variant pm_php$phc_test$framework$bench$benchmark(bool incOnce = false, LVariableTable* variables = NULL);
void f_dump_results();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_bench_benchmark_nophp_h__
