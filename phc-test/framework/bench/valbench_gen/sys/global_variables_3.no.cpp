
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x4E80F79695D41A07LL, opt, 20);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 11:
      HASH_INDEX(0x1018D4B85360194BLL, opt_help, 17);
      break;
    case 12:
      HASH_INDEX(0x21DEE4EB204AE10CLL, cg, 12);
      break;
    case 13:
      HASH_INDEX(0x3534433A7038564DLL, benchs, 21);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x7AE4C907049C28D0LL, bench, 23);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 28:
      HASH_INDEX(0x0F60C6595ED9FADCLL, arguments, 16);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 39:
      HASH_INDEX(0x14500986141654E7LL, opt_php, 18);
      break;
    case 45:
      HASH_INDEX(0x4A5D0431E1A3CAEDLL, filename, 22);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 52:
      HASH_INDEX(0x421780730E09F634LL, opts, 15);
      break;
    case 56:
      HASH_INDEX(0x66E32C011EFD3838LL, results, 24);
      break;
    case 57:
      HASH_INDEX(0x3CDA59286E299AF9LL, command_line, 13);
      HASH_INDEX(0x0B96D1D7D682C2B9LL, opt_serialize, 19);
      break;
    case 59:
      HASH_INDEX(0x67DB402A3712D03BLL, opt_result, 14);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 25) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
