
#ifndef __GENERATED_php_phc_test_framework_bench_valbench_nophp_h__
#define __GENERATED_php_phc_test_framework_bench_valbench_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/bench/valbench.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_run_benchmark(CVarRef v_filename);
Variant pm_php$phc_test$framework$bench$valbench(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_bench_valbench_nophp_h__
