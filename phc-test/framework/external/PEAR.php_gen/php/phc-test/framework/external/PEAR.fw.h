
#ifndef __GENERATED_php_phc_test_framework_external_PEAR_fw_h__
#define __GENERATED_php_phc_test_framework_external_PEAR_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_PEAR_ERROR_CALLBACK;
extern const int64 k_PEAR_ERROR_RETURN;
extern const int64 k_PEAR_ERROR_TRIGGER;
extern const int64 k_PEAR_ERROR_PRINT;
extern const int64 k_PEAR_ERROR_DIE;
extern const int64 k_PEAR_ERROR_EXCEPTION;


// 2. Classes
FORWARD_DECLARE_CLASS(pear)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_external_PEAR_fw_h__
