
#ifndef __GENERATED_php_phc_test_framework_external_PEAR_h__
#define __GENERATED_php_phc_test_framework_external_PEAR_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/external/PEAR.fw.h>

// Declarations
#include <cls/pear.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f__pear_call_destructors();
Variant pm_php$phc_test$framework$external$PEAR_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_pear(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_external_PEAR_h__
