
#include <php/phc-test/framework/external/PEAR.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_PEAR_ERROR_CALLBACK = 16LL;
const int64 k_PEAR_ERROR_RETURN = 1LL;
const int64 k_PEAR_ERROR_TRIGGER = 4LL;
const int64 k_PEAR_ERROR_PRINT = 2LL;
const int64 k_PEAR_ERROR_DIE = 8LL;
const int64 k_PEAR_ERROR_EXCEPTION = 32LL;

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/external/PEAR.php line 102 */
Variant c_pear::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_pear::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_pear::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_debug", m__debug));
  props.push_back(NEW(ArrayElement)("_default_error_mode", m__default_error_mode.isReferenced() ? ref(m__default_error_mode) : m__default_error_mode));
  props.push_back(NEW(ArrayElement)("_default_error_options", m__default_error_options.isReferenced() ? ref(m__default_error_options) : m__default_error_options));
  props.push_back(NEW(ArrayElement)("_default_error_handler", m__default_error_handler));
  props.push_back(NEW(ArrayElement)("_error_class", m__error_class.isReferenced() ? ref(m__error_class) : m__error_class));
  props.push_back(NEW(ArrayElement)("_expected_errors", m__expected_errors.isReferenced() ? ref(m__expected_errors) : m__expected_errors));
  c_ObjectData::o_get(props);
}
bool c_pear::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_EXISTS_STRING(0x459EB1800AD3C442LL, _debug, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x13B0A05A1C94D073LL, _default_error_handler, 22);
      break;
    case 5:
      HASH_EXISTS_STRING(0x2B4B9C58BBD8B635LL, _default_error_options, 22);
      break;
    case 8:
      HASH_EXISTS_STRING(0x164D6DA669F3DAD8LL, _error_class, 12);
      HASH_EXISTS_STRING(0x4E644514E972AF48LL, _expected_errors, 16);
      break;
    case 14:
      HASH_EXISTS_STRING(0x6813D11C0AADA02ELL, _default_error_mode, 19);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_pear::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x459EB1800AD3C442LL, m__debug,
                         _debug, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x13B0A05A1C94D073LL, m__default_error_handler,
                         _default_error_handler, 22);
      break;
    case 5:
      HASH_RETURN_STRING(0x2B4B9C58BBD8B635LL, m__default_error_options,
                         _default_error_options, 22);
      break;
    case 8:
      HASH_RETURN_STRING(0x164D6DA669F3DAD8LL, m__error_class,
                         _error_class, 12);
      HASH_RETURN_STRING(0x4E644514E972AF48LL, m__expected_errors,
                         _expected_errors, 16);
      break;
    case 14:
      HASH_RETURN_STRING(0x6813D11C0AADA02ELL, m__default_error_mode,
                         _default_error_mode, 19);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_pear::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_SET_STRING(0x459EB1800AD3C442LL, m__debug,
                      _debug, 6);
      break;
    case 3:
      HASH_SET_STRING(0x13B0A05A1C94D073LL, m__default_error_handler,
                      _default_error_handler, 22);
      break;
    case 5:
      HASH_SET_STRING(0x2B4B9C58BBD8B635LL, m__default_error_options,
                      _default_error_options, 22);
      break;
    case 8:
      HASH_SET_STRING(0x164D6DA669F3DAD8LL, m__error_class,
                      _error_class, 12);
      HASH_SET_STRING(0x4E644514E972AF48LL, m__expected_errors,
                      _expected_errors, 16);
      break;
    case 14:
      HASH_SET_STRING(0x6813D11C0AADA02ELL, m__default_error_mode,
                      _default_error_mode, 19);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_pear::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x164D6DA669F3DAD8LL, m__error_class,
                         _error_class, 12);
      HASH_RETURN_STRING(0x4E644514E972AF48LL, m__expected_errors,
                         _expected_errors, 16);
      break;
    case 5:
      HASH_RETURN_STRING(0x2B4B9C58BBD8B635LL, m__default_error_options,
                         _default_error_options, 22);
      break;
    case 6:
      HASH_RETURN_STRING(0x6813D11C0AADA02ELL, m__default_error_mode,
                         _default_error_mode, 19);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_pear::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(pear)
ObjectData *c_pear::create(CVarRef v_error_class //  = null_variant
) {
  init();
  t_pear(v_error_class);
  return this;
}
ObjectData *c_pear::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_pear::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t_pear());
  (t_pear(params.rvalAt(0)));
}
ObjectData *c_pear::cloneImpl() {
  c_pear *obj = NEW(c_pear)();
  cloneSet(obj);
  return obj;
}
void c_pear::cloneSet(c_pear *clone) {
  clone->m__debug = m__debug;
  clone->m__default_error_mode = m__default_error_mode.isReferenced() ? ref(m__default_error_mode) : m__default_error_mode;
  clone->m__default_error_options = m__default_error_options.isReferenced() ? ref(m__default_error_options) : m__default_error_options;
  clone->m__default_error_handler = m__default_error_handler;
  clone->m__error_class = m__error_class.isReferenced() ? ref(m__error_class) : m__error_class;
  clone->m__expected_errors = m__expected_errors.isReferenced() ? ref(m__expected_errors) : m__expected_errors;
  ObjectData::cloneSet(clone);
}
Variant c_pear::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x58F7533978A30DF7LL, pear) {
        int count = params.size();
        if (count <= 0) return (t_pear(), null);
        return (t_pear(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_pear::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x58F7533978A30DF7LL, pear) {
        if (count <= 0) return (t_pear(), null);
        return (t_pear(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pear::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pear$os_get(const char *s) {
  return c_pear::os_get(s, -1);
}
Variant &cw_pear$os_lval(const char *s) {
  return c_pear::os_lval(s, -1);
}
Variant cw_pear$os_constant(const char *s) {
  return c_pear::os_constant(s);
}
Variant cw_pear$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pear::os_invoke(c, s, params, -1, fatal);
}
void c_pear::init() {
  m__debug = false;
  m__default_error_mode = null;
  m__default_error_options = null;
  m__default_error_handler = "";
  m__error_class = "PEAR_Error";
  m__expected_errors = ScalarArrays::sa_[0];
}
/* SRC: phc-test/framework/external/PEAR.php line 170 */
void c_pear::t_pear(CVarRef v_error_class //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::PEAR);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_classname;
  String v_destructor;
  Variant &gv__PEAR_destructor_object_list __attribute__((__unused__)) = g->GV(_PEAR_destructor_object_list);
  Variant v__PEAR_destructor_object_list;

  (v_classname = LINE(172,x_strtolower(toString(x_get_class(((Object)(this)))))));
  if (m__debug) {
    print(LINE(174,concat3("PEAR constructor called, class=", toString(v_classname), "\n")));
  }
  if (!same(v_error_class, null)) {
    (m__error_class = v_error_class);
  }
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_classname) && toBoolean(LINE(179,x_strcasecmp(toString(v_classname), "pear")))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_destructor = toString("_") + toString(v_classname));
        if (LINE(181,x_method_exists(((Object)(this)), v_destructor))) {
          v__PEAR_destructor_object_list = ref(g->GV(_PEAR_destructor_object_list));
          v__PEAR_destructor_object_list.append((ref(this)));
          if (!(isset(g->GV(_PEAR_SHUTDOWN_REGISTERED)))) {
            LINE(185,x_register_shutdown_function(1, "_PEAR_call_destructors"));
            (g->GV(_PEAR_SHUTDOWN_REGISTERED) = true);
          }
          break;
        }
        else {
          (v_classname = LINE(190,x_get_parent_class(v_classname)));
        }
      }
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 209 */
void c_pear::t__pear() {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::_PEAR);
  Variant eo_0;
  Variant eo_1;
  if (m__debug) {
    LINE(211,(assignCallTemp(eo_1, x_strtolower(toString(x_get_class(((Object)(this)))))),x_printf(2, "PEAR destructor called, class=%s\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  }
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 230 */
Variant c_pear::ti_getstaticproperty(const char* cls, CStrRef v_class, CStrRef v_var) {
  STATIC_METHOD_INJECTION(PEAR, PEAR::getStaticProperty);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_properties __attribute__((__unused__)) = g->sv_pear_DupIdgetstaticproperty_DupIdproperties.lvalAt(cls);
  Variant &inited_sv_properties __attribute__((__unused__)) = g->inited_sv_pear_DupIdgetstaticproperty_DupIdproperties.lvalAt(cls);
  if (!inited_sv_properties) {
    (sv_properties = null);
    inited_sv_properties = true;
  }
  if (!(isset(sv_properties, v_class))) {
    sv_properties.set(v_class, (ScalarArrays::sa_[0]));
  }
  if (!(LINE(236,x_array_key_exists(v_var, sv_properties.rvalAt(v_class))))) {
    lval(sv_properties.lvalAt(v_class)).set(v_var, (null));
  }
  return ref(lval(lval(sv_properties.lvalAt(v_class)).lvalAt(v_var)));
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 254 */
void c_pear::t_registershutdownfunc(CVarRef v_func, CArrRef v_args //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::registerShutdownFunc);
  DECLARE_GLOBAL_VARIABLES(g);
  if (!(isset(g->GV(_PEAR_SHUTDOWN_REGISTERED)))) {
    LINE(259,x_register_shutdown_function(1, "_PEAR_call_destructors"));
    (g->GV(_PEAR_SHUTDOWN_REGISTERED) = true);
  }
  g->GV(_PEAR_shutdown_funcs).append((Array(ArrayInit(2).set(0, v_func).set(1, v_args).create())));
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 279 */
bool c_pear::t_iserror(Object v_data, CVarRef v_code //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::isError);
  if (LINE(281,x_is_a(v_data, "PEAR_Error"))) {
    if (LINE(282,x_is_null(v_code))) {
      return true;
    }
    else if (LINE(284,x_is_string(v_code))) {
      return equal(LINE(285,v_data->o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0)), v_code);
    }
    else {
      return equal(LINE(287,v_data->o_invoke_few_args("getCode", 0x5C108B351DC3D04FLL, 0)), v_code);
    }
  }
  return false;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 335 */
void c_pear::t_seterrorhandling(CVarRef v_mode //  = null_variant
, CVarRef v_options //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::setErrorHandling);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_setmode;
  Variant v_setoptions;

  if (isset(((Object)(this))) && LINE(337,x_is_a(((Object)(this)), "PEAR"))) {
    (v_setmode = ref(lval(m__default_error_mode)));
    (v_setoptions = ref(lval(m__default_error_options)));
  }
  else {
    (v_setmode = ref(lval(g->GV(_PEAR_default_error_mode))));
    (v_setoptions = ref(lval(g->GV(_PEAR_default_error_options))));
  }
  {
    Variant tmp3 = (v_mode);
    int tmp4 = -1;
    if (equal(tmp3, (32LL /* PEAR_ERROR_EXCEPTION */))) {
      tmp4 = 0;
    } else if (equal(tmp3, (1LL /* PEAR_ERROR_RETURN */))) {
      tmp4 = 1;
    } else if (equal(tmp3, (2LL /* PEAR_ERROR_PRINT */))) {
      tmp4 = 2;
    } else if (equal(tmp3, (4LL /* PEAR_ERROR_TRIGGER */))) {
      tmp4 = 3;
    } else if (equal(tmp3, (8LL /* PEAR_ERROR_DIE */))) {
      tmp4 = 4;
    } else if (equal(tmp3, (null))) {
      tmp4 = 5;
    } else if (equal(tmp3, (16LL /* PEAR_ERROR_CALLBACK */))) {
      tmp4 = 6;
    } else if (true) {
      tmp4 = 7;
    }
    switch (tmp4) {
    case 0:
      {
      }
    case 1:
      {
      }
    case 2:
      {
      }
    case 3:
      {
      }
    case 4:
      {
      }
    case 5:
      {
        (v_setmode = v_mode);
        (v_setoptions = v_options);
        goto break2;
      }
    case 6:
      {
        (v_setmode = v_mode);
        if (LINE(359,x_is_callable(v_options))) {
          (v_setoptions = v_options);
        }
        else {
          LINE(362,x_trigger_error("invalid error callback", toInt32(512LL) /* E_USER_WARNING */));
        }
        goto break2;
      }
    case 7:
      {
        LINE(367,x_trigger_error("invalid error mode", toInt32(512LL) /* E_USER_WARNING */));
        goto break2;
      }
    }
    break2:;
  }
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 390 */
int c_pear::t_expecterror(CStrRef v_code //  = "*"
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::expectError);
  if (LINE(392,x_is_array(v_code))) {
    LINE(393,x_array_push(2, ref(lval(m__expected_errors)), v_code));
  }
  else {
    LINE(395,x_array_push(2, ref(lval(m__expected_errors)), Array(ArrayInit(1).set(0, v_code).create())));
  }
  return LINE(397,x_sizeof(m__expected_errors));
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 409 */
Variant c_pear::t_popexpect() {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::popExpect);
  return LINE(411,x_array_pop(ref(lval(m__expected_errors))));
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 425 */
bool c_pear::t__checkdelexpect(CVarRef v_error_code) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::_checkDelExpect);
  DECLARE_GLOBAL_VARIABLES(g);
  bool v_deleted = false;
  Primitive v_key = 0;
  Variant v_error_array;

  (v_deleted = false);
  {
    LOOP_COUNTER(5);
    Variant map6 = m__expected_errors;
    for (ArrayIterPtr iter7 = map6.begin("pear"); !iter7->end(); iter7->next()) {
      LOOP_COUNTER_CHECK(5);
      v_error_array = iter7->second();
      v_key = iter7->first();
      {
        if (LINE(430,x_in_array(v_error_code, v_error_array))) {
          lval(unsetLval(m__expected_errors, v_key)).weakRemove(LINE(431,x_array_search(v_error_code, v_error_array)));
          (v_deleted = true);
        }
        if (equal(0LL, LINE(436,x_count(m__expected_errors.rvalAt(v_key))))) {
          m__expected_errors.weakRemove(v_key);
        }
      }
    }
  }
  return v_deleted;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 455 */
Variant c_pear::t_delexpect(CArrRef v_error_code) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::delExpect);
  bool v_deleted = false;
  Primitive v_key = 0;
  Variant v_error;

  (v_deleted = false);
  if ((LINE(459,x_is_array(v_error_code)) && (!equal(0LL, x_count(v_error_code))))) {
    {
      LOOP_COUNTER(8);
      for (ArrayIter iter10 = v_error_code.begin("pear"); !iter10.end(); ++iter10) {
        LOOP_COUNTER_CHECK(8);
        v_error = iter10.second();
        v_key = iter10.first();
        {
          if (LINE(464,t__checkdelexpect(v_error))) {
            (v_deleted = true);
          }
          else {
            (v_deleted = false);
          }
        }
      }
    }
    return v_deleted ? ((Variant)(true)) : ((Variant)(LINE(470,c_pear::t_raiseerror("The expected error you submitted does not exist"))));
  }
  else if (!(empty(v_error_code))) {
    if (LINE(473,t__checkdelexpect(v_error_code))) {
      return true;
    }
    else {
      return LINE(476,c_pear::t_raiseerror("The expected error you submitted does not exist"));
    }
  }
  else {
    return LINE(480,c_pear::t_raiseerror("The expected error you submitted is empty"));
  }
  return null;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 524 */
Variant c_pear::t_raiseerror(Variant v_message //  = null
, Variant v_code //  = null
, Variant v_mode //  = null
, Variant v_options //  = null
, Variant v_userinfo //  = null
, Variant v_error_class //  = null
, bool v_skipmsg //  = false
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::raiseError);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_exp;
  Variant v_ec;
  Variant v_a;

  if (LINE(533,x_is_object(v_message))) {
    (v_code = LINE(534,v_message.o_invoke_few_args("getCode", 0x5C108B351DC3D04FLL, 0)));
    (v_userinfo = LINE(535,v_message.o_invoke_few_args("getUserInfo", 0x05CF5B3C831C4053LL, 0)));
    (v_error_class = LINE(536,v_message.o_invoke_few_args("getType", 0x1D3B08AA0AF50F06LL, 0)));
    (v_message.o_lval("error_message_prefix", 0x4146A1C61F6C1AC2LL) = "");
    (v_message = LINE(538,v_message.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0)));
  }
  if (isset(((Object)(this))) && t___isset("_expected_errors") && more(LINE(541,x_sizeof(m__expected_errors)), 0LL) && toBoolean(x_sizeof((v_exp = x_end(ref(lval(m__expected_errors))))))) {
    if (equal(v_exp.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "*") || (LINE(543,x_is_int(x_reset(ref(v_exp)))) && x_in_array(v_code, v_exp)) || (LINE(544,x_is_string(x_reset(ref(v_exp)))) && x_in_array(v_message, v_exp))) {
      (v_mode = 1LL /* PEAR_ERROR_RETURN */);
    }
  }
  if (same(v_mode, null)) {
    if (isset(((Object)(this))) && t___isset("_default_error_mode")) {
      (v_mode = m__default_error_mode);
      (v_options = m__default_error_options);
    }
    else if (isset(g->GV(_PEAR_default_error_mode))) {
      (v_mode = g->GV(_PEAR_default_error_mode));
      (v_options = g->GV(_PEAR_default_error_options));
    }
  }
  if (!same(v_error_class, null)) {
    (v_ec = v_error_class);
  }
  else if (isset(((Object)(this))) && t___isset("_error_class")) {
    (v_ec = m__error_class);
  }
  else {
    (v_ec = "PEAR_Error");
  }
  if (v_skipmsg) {
    (v_a = LINE(569,create_object(toString(v_ec), Array(ArrayInit(4).set(0, ref(v_code)).set(1, ref(v_mode)).set(2, ref(v_options)).set(3, ref(v_userinfo)).create()))));
    return ref(v_a);
  }
  else {
    (v_a = LINE(572,create_object(toString(v_ec), Array(ArrayInit(5).set(0, ref(v_message)).set(1, ref(v_code)).set(2, ref(v_mode)).set(3, ref(v_options)).set(4, ref(v_userinfo)).create()))));
    return ref(v_a);
  }
  return null;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 587 */
Variant c_pear::t_throwerror(CVarRef v_message //  = null_variant
, CVarRef v_code //  = null_variant
, CVarRef v_userinfo //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::throwError);
  Variant v_a;

  if (isset(((Object)(this))) && LINE(591,x_is_a(((Object)(this)), "PEAR"))) {
    (v_a = ref(LINE(592,t_raiseerror(v_message, v_code, null, null, v_userinfo))));
    return ref(v_a);
  }
  else {
    (v_a = ref(LINE(595,c_pear::t_raiseerror(v_message, v_code, null, null, v_userinfo))));
    return ref(v_a);
  }
  return null;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 601 */
bool c_pear::t_staticpusherrorhandling(CVarRef v_mode, CVarRef v_options //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::staticPushErrorHandling);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_stack;
  Variant v_def_mode;
  Variant v_def_options;

  (v_stack = ref(lval(g->GV(_PEAR_error_handler_stack))));
  (v_def_mode = ref(lval(g->GV(_PEAR_default_error_mode))));
  (v_def_options = ref(lval(g->GV(_PEAR_default_error_options))));
  v_stack.append((Array(ArrayInit(2).set(0, v_def_mode).set(1, v_def_options).create())));
  {
    Variant tmp12 = (v_mode);
    int tmp13 = -1;
    if (equal(tmp12, (32LL /* PEAR_ERROR_EXCEPTION */))) {
      tmp13 = 0;
    } else if (equal(tmp12, (1LL /* PEAR_ERROR_RETURN */))) {
      tmp13 = 1;
    } else if (equal(tmp12, (2LL /* PEAR_ERROR_PRINT */))) {
      tmp13 = 2;
    } else if (equal(tmp12, (4LL /* PEAR_ERROR_TRIGGER */))) {
      tmp13 = 3;
    } else if (equal(tmp12, (8LL /* PEAR_ERROR_DIE */))) {
      tmp13 = 4;
    } else if (equal(tmp12, (null))) {
      tmp13 = 5;
    } else if (equal(tmp12, (16LL /* PEAR_ERROR_CALLBACK */))) {
      tmp13 = 6;
    } else if (true) {
      tmp13 = 7;
    }
    switch (tmp13) {
    case 0:
      {
      }
    case 1:
      {
      }
    case 2:
      {
      }
    case 3:
      {
      }
    case 4:
      {
      }
    case 5:
      {
        (v_def_mode = v_mode);
        (v_def_options = v_options);
        goto break11;
      }
    case 6:
      {
        (v_def_mode = v_mode);
        if (LINE(621,x_is_callable(v_options))) {
          (v_def_options = v_options);
        }
        else {
          LINE(624,x_trigger_error("invalid error callback", toInt32(512LL) /* E_USER_WARNING */));
        }
        goto break11;
      }
    case 7:
      {
        LINE(629,x_trigger_error("invalid error mode", toInt32(512LL) /* E_USER_WARNING */));
        goto break11;
      }
    }
    break11:;
  }
  v_stack.append((Array(ArrayInit(2).set(0, v_mode).set(1, v_options).create())));
  return true;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 636 */
bool c_pear::t_staticpoperrorhandling() {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::staticPopErrorHandling);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_stack;
  Variant v_setmode;
  Variant v_setoptions;
  Variant v_mode;
  Variant v_options;

  (v_stack = ref(lval(g->GV(_PEAR_error_handler_stack))));
  (v_setmode = ref(lval(g->GV(_PEAR_default_error_mode))));
  (v_setoptions = ref(lval(g->GV(_PEAR_default_error_options))));
  LINE(641,x_array_pop(ref(v_stack)));
  df_lambda_1(v_stack.rvalAt(LINE(642,x_sizeof(v_stack)) - 1LL), v_mode, v_options);
  LINE(643,x_array_pop(ref(v_stack)));
  {
    Variant tmp15 = (v_mode);
    int tmp16 = -1;
    if (equal(tmp15, (32LL /* PEAR_ERROR_EXCEPTION */))) {
      tmp16 = 0;
    } else if (equal(tmp15, (1LL /* PEAR_ERROR_RETURN */))) {
      tmp16 = 1;
    } else if (equal(tmp15, (2LL /* PEAR_ERROR_PRINT */))) {
      tmp16 = 2;
    } else if (equal(tmp15, (4LL /* PEAR_ERROR_TRIGGER */))) {
      tmp16 = 3;
    } else if (equal(tmp15, (8LL /* PEAR_ERROR_DIE */))) {
      tmp16 = 4;
    } else if (equal(tmp15, (null))) {
      tmp16 = 5;
    } else if (equal(tmp15, (16LL /* PEAR_ERROR_CALLBACK */))) {
      tmp16 = 6;
    } else if (true) {
      tmp16 = 7;
    }
    switch (tmp16) {
    case 0:
      {
      }
    case 1:
      {
      }
    case 2:
      {
      }
    case 3:
      {
      }
    case 4:
      {
      }
    case 5:
      {
        (v_setmode = v_mode);
        (v_setoptions = v_options);
        goto break14;
      }
    case 6:
      {
        (v_setmode = v_mode);
        if (LINE(658,x_is_callable(v_options))) {
          (v_setoptions = v_options);
        }
        else {
          LINE(661,x_trigger_error("invalid error callback", toInt32(512LL) /* E_USER_WARNING */));
        }
        goto break14;
      }
    case 7:
      {
        LINE(666,x_trigger_error("invalid error mode", toInt32(512LL) /* E_USER_WARNING */));
        goto break14;
      }
    }
    break14:;
  }
  return true;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 686 */
bool c_pear::t_pusherrorhandling(CVarRef v_mode, CVarRef v_options //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::pushErrorHandling);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_stack;
  Variant v_def_mode;
  Variant v_def_options;

  (v_stack = ref(lval(g->GV(_PEAR_error_handler_stack))));
  if (isset(((Object)(this))) && LINE(689,x_is_a(((Object)(this)), "PEAR"))) {
    (v_def_mode = ref(lval(m__default_error_mode)));
    (v_def_options = ref(lval(m__default_error_options)));
  }
  else {
    (v_def_mode = ref(lval(g->GV(_PEAR_default_error_mode))));
    (v_def_options = ref(lval(g->GV(_PEAR_default_error_options))));
  }
  v_stack.append((Array(ArrayInit(2).set(0, v_def_mode).set(1, v_def_options).create())));
  if (isset(((Object)(this))) && LINE(698,x_is_a(((Object)(this)), "PEAR"))) {
    LINE(699,t_seterrorhandling(v_mode, v_options));
  }
  else {
    LINE(701,c_pear::t_seterrorhandling(v_mode, v_options));
  }
  v_stack.append((Array(ArrayInit(2).set(0, v_mode).set(1, v_options).create())));
  return true;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 717 */
bool c_pear::t_poperrorhandling() {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::popErrorHandling);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_stack;
  Variant v_mode;
  Variant v_options;

  (v_stack = ref(lval(g->GV(_PEAR_error_handler_stack))));
  LINE(720,x_array_pop(ref(v_stack)));
  df_lambda_2(v_stack.rvalAt(LINE(721,x_sizeof(v_stack)) - 1LL), v_mode, v_options);
  LINE(722,x_array_pop(ref(v_stack)));
  if (isset(((Object)(this))) && LINE(723,x_is_a(((Object)(this)), "PEAR"))) {
    LINE(724,t_seterrorhandling(v_mode, v_options));
  }
  else {
    LINE(726,c_pear::t_seterrorhandling(v_mode, v_options));
  }
  return true;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 741 */
bool c_pear::t_loadextension(CVarRef v_ext) {
  INSTANCE_METHOD_INJECTION(PEAR, PEAR::loadExtension);
  String v_suffix;

  if (!(LINE(743,x_extension_loaded(toString(v_ext))))) {
    if ((!equal(LINE(745,x_ini_get("enable_dl")), 1LL)) || (equal(x_ini_get("safe_mode"), 1LL))) {
      return false;
    }
    if (toBoolean(get_global_variables()->k_OS_WINDOWS)) {
      (v_suffix = ".dll");
    }
    else {
      (v_suffix = ".so");
    }
    return toBoolean((silenceInc(), silenceDec(toBoolean(LINE(759,x_dl(concat3("php_", toString(v_ext), v_suffix))))))) || toBoolean((silenceInc(), silenceDec(toBoolean(x_dl(concat(toString(v_ext), v_suffix))))));
  }
  return true;
} /* function */
/* SRC: phc-test/framework/external/PEAR.php line 769 */
void f__pear_call_destructors() {
  FUNCTION_INJECTION(_PEAR_call_destructors);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv__PEAR_destructor_object_list __attribute__((__unused__)) = g->GV(_PEAR_destructor_object_list);
  Variant v_k;
  Variant v_objref;
  Variant v_classname;
  String v_destructor;
  Variant v_value;

  if (LINE(772,x_is_array(gv__PEAR_destructor_object_list)) && toBoolean(LINE(773,x_sizeof(gv__PEAR_destructor_object_list)))) {
    LINE(775,x_reset(ref(gv__PEAR_destructor_object_list)));
    if (toBoolean(LINE(776,c_pear::t_getstaticproperty("PEAR", "destructlifo")))) {
      (gv__PEAR_destructor_object_list = LINE(777,x_array_reverse(gv__PEAR_destructor_object_list)));
    }
    LOOP_COUNTER(17);
    {
      while (toBoolean(df_lambda_3(LINE(779,x_each(ref(gv__PEAR_destructor_object_list))), v_k, v_objref))) {
        LOOP_COUNTER_CHECK(17);
        {
          (v_classname = LINE(780,x_get_class(v_objref)));
          LOOP_COUNTER(18);
          {
            while (toBoolean(v_classname)) {
              LOOP_COUNTER_CHECK(18);
              {
                (v_destructor = toString("_") + toString(v_classname));
                if (LINE(783,x_method_exists(v_objref, v_destructor))) {
                  LINE(784,v_objref.o_invoke_few_args(v_destructor, -1LL, 0));
                  break;
                }
                else {
                  (v_classname = LINE(787,x_get_parent_class(v_classname)));
                }
              }
            }
          }
        }
      }
    }
    (gv__PEAR_destructor_object_list = ScalarArrays::sa_[0]);
  }
  if ((LINE(797,x_is_array(g->GV(_PEAR_shutdown_funcs)))) && (!(empty(g->GV(_PEAR_shutdown_funcs))))) {
    {
      LOOP_COUNTER(19);
      Variant map20 = g->GV(_PEAR_shutdown_funcs);
      for (ArrayIterPtr iter21 = map20.begin(); !iter21->end(); iter21->next()) {
        LOOP_COUNTER_CHECK(19);
        v_value = iter21->second();
        {
          LINE(799,x_call_user_func_array(v_value.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), toArray(v_value.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
        }
      }
    }
  }
} /* function */
Variant i__pear_call_destructors(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x19CAA008CBDE10D5LL, _pear_call_destructors) {
    return (f__pear_call_destructors(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_pear(CArrRef params, bool init /* = true */) {
  return Object(p_pear(NEW(c_pear)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$external$PEAR_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/external/PEAR.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$external$PEAR_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  ;
  ;
  ;
  ;
  ;
  ;
  LINE(43,g->declareConstant("PEAR_ZE2", g->k_PEAR_ZE2, (toBoolean((assignCallTemp(eo_2, x_zend_version()),x_version_compare(eo_2, "2-dev", "ge"))))));
  if (equal(LINE(45,x_substr("Linux" /* PHP_OS */, toInt32(0LL), toInt32(3LL))), "WIN")) {
    LINE(46,g->declareConstant("OS_WINDOWS", g->k_OS_WINDOWS, true));
    LINE(47,g->declareConstant("OS_UNIX", g->k_OS_UNIX, false));
    LINE(48,g->declareConstant("PEAR_OS", g->k_PEAR_OS, "Windows"));
  }
  else {
    LINE(50,g->declareConstant("OS_WINDOWS", g->k_OS_WINDOWS, false));
    LINE(51,g->declareConstant("OS_UNIX", g->k_OS_UNIX, true));
    LINE(52,g->declareConstant("PEAR_OS", g->k_PEAR_OS, "Unix"));
  }
  (g->GV(_PEAR_default_error_mode) = 1LL /* PEAR_ERROR_RETURN */);
  (g->GV(_PEAR_default_error_options) = 1024LL /* E_USER_NOTICE */);
  (g->GV(_PEAR_destructor_object_list) = ScalarArrays::sa_[0]);
  (g->GV(_PEAR_shutdown_funcs) = ScalarArrays::sa_[0]);
  (g->GV(_PEAR_error_handler_stack) = ScalarArrays::sa_[0]);
  (silenceInc(), silenceDec(LINE(70,x_ini_set("track_errors", toString(true)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
