
#ifndef __GENERATED_cls_pear_h__
#define __GENERATED_cls_pear_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/external/PEAR.php line 102 */
class c_pear : virtual public ObjectData {
  BEGIN_CLASS_MAP(pear)
  END_CLASS_MAP(pear)
  DECLARE_CLASS(pear, PEAR, ObjectData)
  void init();
  public: bool m__debug;
  public: Variant m__default_error_mode;
  public: Variant m__default_error_options;
  public: String m__default_error_handler;
  public: Variant m__error_class;
  public: Variant m__expected_errors;
  public: void t_pear(CVarRef v_error_class = null_variant);
  public: ObjectData *create(CVarRef v_error_class = null_variant);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t__pear();
  public: static Variant ti_getstaticproperty(const char* cls, CStrRef v_class, CStrRef v_var);
  public: void t_registershutdownfunc(CVarRef v_func, CArrRef v_args = ScalarArrays::sa_[0]);
  public: bool t_iserror(Object v_data, CVarRef v_code = null_variant);
  public: void t_seterrorhandling(CVarRef v_mode = null_variant, CVarRef v_options = null_variant);
  public: int t_expecterror(CStrRef v_code = "*");
  public: Variant t_popexpect();
  public: bool t__checkdelexpect(CVarRef v_error_code);
  public: Variant t_delexpect(CArrRef v_error_code);
  public: Variant t_raiseerror(Variant v_message = null, Variant v_code = null, Variant v_mode = null, Variant v_options = null, Variant v_userinfo = null, Variant v_error_class = null, bool v_skipmsg = false);
  public: Variant t_throwerror(CVarRef v_message = null_variant, CVarRef v_code = null_variant, CVarRef v_userinfo = null_variant);
  public: bool t_staticpusherrorhandling(CVarRef v_mode, CVarRef v_options = null_variant);
  public: bool t_staticpoperrorhandling();
  public: bool t_pusherrorhandling(CVarRef v_mode, CVarRef v_options = null_variant);
  public: bool t_poperrorhandling();
  public: bool t_loadextension(CVarRef v_ext);
  public: static Variant t_getstaticproperty(CStrRef v_class, CStrRef v_var) { return ti_getstaticproperty("pear", v_class, v_var); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pear_h__
