
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(_PEAR_default_error_mode)
    GVS(_PEAR_default_error_options)
    GVS(_PEAR_destructor_object_list)
    GVS(_PEAR_shutdown_funcs)
    GVS(_PEAR_error_handler_stack)
    GVS(_PEAR_SHUTDOWN_REGISTERED)
  END_GVS(6)

  // Dynamic Constants
  Variant k_PEAR_OS;
  Variant k_OS_UNIX;
  Variant k_PEAR_ZE2;
  Variant k_OS_WINDOWS;

  // Function/Method Static Variables
  Variant sv_pear_DupIdgetstaticproperty_DupIdproperties;

  // Function/Method Static Variable Initialization Booleans
  Variant inited_sv_pear_DupIdgetstaticproperty_DupIdproperties;

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$phc_test$framework$external$PEAR_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 18;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[2];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
