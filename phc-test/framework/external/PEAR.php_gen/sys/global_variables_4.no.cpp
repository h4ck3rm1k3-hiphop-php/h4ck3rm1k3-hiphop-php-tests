
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "_PEAR_default_error_mode",
    "_PEAR_default_error_options",
    "_PEAR_destructor_object_list",
    "_PEAR_shutdown_funcs",
    "_PEAR_error_handler_stack",
    "_PEAR_SHUTDOWN_REGISTERED",
  };
  if (idx >= 0 && idx < 18) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(_PEAR_default_error_mode);
    case 13: return GV(_PEAR_default_error_options);
    case 14: return GV(_PEAR_destructor_object_list);
    case 15: return GV(_PEAR_shutdown_funcs);
    case 16: return GV(_PEAR_error_handler_stack);
    case 17: return GV(_PEAR_SHUTDOWN_REGISTERED);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
