
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_PEAR_ERROR_CALLBACK;
extern const int64 k_PEAR_ERROR_RETURN;
extern const int64 k_PEAR_ERROR_TRIGGER;
extern const int64 k_PEAR_ERROR_PRINT;
extern const int64 k_PEAR_ERROR_DIE;
extern const int64 k_PEAR_ERROR_EXCEPTION;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 31) {
    case 0:
      HASH_RETURN(0x0BA73DFC326760E0LL, k_PEAR_ERROR_PRINT, PEAR_ERROR_PRINT);
      break;
    case 3:
      HASH_RETURN(0x2D704F71EAF769A3LL, k_PEAR_ERROR_RETURN, PEAR_ERROR_RETURN);
      HASH_RETURN(0x0E54CDF87041BFE3LL, k_PEAR_ERROR_DIE, PEAR_ERROR_DIE);
      break;
    case 8:
      HASH_RETURN(0x5E15BB1F1B8DB808LL, k_PEAR_ERROR_CALLBACK, PEAR_ERROR_CALLBACK);
      HASH_RETURN(0x00879F0E2A2CA4A8LL, g->k_OS_WINDOWS, OS_WINDOWS);
      break;
    case 13:
      HASH_RETURN(0x1F9E9427016A93ADLL, g->k_PEAR_ZE2, PEAR_ZE2);
      HASH_RETURN(0x6B5F89B32F2A2A2DLL, g->k_OS_UNIX, OS_UNIX);
      break;
    case 16:
      HASH_RETURN(0x012B1C5193589B90LL, k_PEAR_ERROR_TRIGGER, PEAR_ERROR_TRIGGER);
      break;
    case 18:
      HASH_RETURN(0x4338D48C3C4024D2LL, k_PEAR_ERROR_EXCEPTION, PEAR_ERROR_EXCEPTION);
      break;
    case 24:
      HASH_RETURN(0x31A82D7A9F831058LL, g->k_PEAR_OS, PEAR_OS);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
