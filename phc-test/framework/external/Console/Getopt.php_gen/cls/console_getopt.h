
#ifndef __GENERATED_cls_console_getopt_h__
#define __GENERATED_cls_console_getopt_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/external/Console/Getopt.php line 29 */
class c_console_getopt : virtual public ObjectData {
  BEGIN_CLASS_MAP(console_getopt)
  END_CLASS_MAP(console_getopt)
  DECLARE_CLASS(console_getopt, Console_Getopt, ObjectData)
  void init();
  public: Variant t_getopt2(CVarRef v_args, CVarRef v_short_options, CVarRef v_long_options = null_variant);
  public: Variant t_getopt(CVarRef v_args, CVarRef v_short_options, CVarRef v_long_options = null_variant);
  public: Variant t_dogetopt(int64 v_version, Variant v_args, CVarRef v_short_options, Variant v_long_options = null);
  public: Variant t__parseshortoption(CVarRef v_arg, CVarRef v_short_options, Variant v_opts, Variant v_args);
  public: Variant t__parselongoption(CVarRef v_arg, CVarRef v_long_options, Variant v_opts, Variant v_args);
  public: Variant t_readphpargv();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_console_getopt_h__
