
#ifndef __GENERATED_php_phc_test_framework_external_Console_Getopt_h__
#define __GENERATED_php_phc_test_framework_external_Console_Getopt_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/external/Console/Getopt.fw.h>

// Declarations
#include <cls/console_getopt.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$external$Console$Getopt_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_console_getopt(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_external_Console_Getopt_h__
