
#include <php/phc-test/framework/external/Console/Getopt.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: phc-test/framework/external/Console/Getopt.php line 29 */
Variant c_console_getopt::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_console_getopt::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_console_getopt::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_console_getopt::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_console_getopt::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_console_getopt::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_console_getopt::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_console_getopt::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(console_getopt)
ObjectData *c_console_getopt::cloneImpl() {
  c_console_getopt *obj = NEW(c_console_getopt)();
  cloneSet(obj);
  return obj;
}
void c_console_getopt::cloneSet(c_console_getopt *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_console_getopt::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_console_getopt::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_console_getopt::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_console_getopt$os_get(const char *s) {
  return c_console_getopt::os_get(s, -1);
}
Variant &cw_console_getopt$os_lval(const char *s) {
  return c_console_getopt::os_lval(s, -1);
}
Variant cw_console_getopt$os_constant(const char *s) {
  return c_console_getopt::os_constant(s);
}
Variant cw_console_getopt$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_console_getopt::os_invoke(c, s, params, -1, fatal);
}
void c_console_getopt::init() {
}
/* SRC: phc-test/framework/external/Console/Getopt.php line 66 */
Variant c_console_getopt::t_getopt2(CVarRef v_args, CVarRef v_short_options, CVarRef v_long_options //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(Console_Getopt, Console_Getopt::getopt2);
  return LINE(68,c_console_getopt::t_dogetopt(2LL, v_args, v_short_options, v_long_options));
} /* function */
/* SRC: phc-test/framework/external/Console/Getopt.php line 76 */
Variant c_console_getopt::t_getopt(CVarRef v_args, CVarRef v_short_options, CVarRef v_long_options //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(Console_Getopt, Console_Getopt::getopt);
  return LINE(78,c_console_getopt::t_dogetopt(1LL, v_args, v_short_options, v_long_options));
} /* function */
/* SRC: phc-test/framework/external/Console/Getopt.php line 84 */
Variant c_console_getopt::t_dogetopt(int64 v_version, Variant v_args, CVarRef v_short_options, Variant v_long_options //  = null
) {
  INSTANCE_METHOD_INJECTION(Console_Getopt, Console_Getopt::doGetopt);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_opts;
  Variant v_non_opts;
  Variant v_i;
  Variant v_arg;
  Variant v_error;

  if (toBoolean(LINE(87,throw_fatal("unknown class pear", ((void*)NULL))))) {
    return v_args;
  }
  if (empty(v_args)) {
    return ScalarArrays::sa_[0];
  }
  (v_opts = ScalarArrays::sa_[1]);
  (v_non_opts = ScalarArrays::sa_[1]);
  LINE(96,x_settype(ref(v_args), "array"));
  if (toBoolean(v_long_options)) {
    LINE(99,x_sort(ref(v_long_options)));
  }
  if (less(v_version, 2LL)) {
    if (isset(v_args.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0LL, 0x77CFA1EEF01BCA90LL) && !equal(v_args.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-")) {
      LINE(108,x_array_shift(ref(v_args)));
    }
  }
  LINE(112,x_reset(ref(v_args)));
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(113,x_each(ref(v_args))), v_i, v_arg))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (equal(v_arg, "--")) {
          (v_non_opts = LINE(119,(assignCallTemp(eo_0, v_non_opts),assignCallTemp(eo_1, x_array_slice(v_args, toInt32(v_i + 1LL))),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
          break;
        }
        if (!equal(v_arg.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-") || (more(LINE(123,x_strlen(toString(v_arg))), 1LL) && equal(v_arg.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "-") && !(toBoolean(v_long_options)))) {
          (v_non_opts = LINE(124,(assignCallTemp(eo_0, v_non_opts),assignCallTemp(eo_1, x_array_slice(v_args, toInt32(v_i))),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
          break;
        }
        else if (more(LINE(126,x_strlen(toString(v_arg))), 1LL) && equal(v_arg.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "-")) {
          (v_error = LINE(127,(assignCallTemp(eo_0, x_substr(toString(v_arg), toInt32(2LL))),assignCallTemp(eo_1, v_long_options),assignCallTemp(eo_2, ref(v_opts)),assignCallTemp(eo_3, ref(v_args)),c_console_getopt::t__parselongoption(eo_0, eo_1, eo_2, eo_3))));
          if (toBoolean(LINE(128,throw_fatal("unknown class pear", ((void*)NULL))))) return v_error;
        }
        else {
          (v_error = LINE(131,(assignCallTemp(eo_0, x_substr(toString(v_arg), toInt32(1LL))),assignCallTemp(eo_1, v_short_options),assignCallTemp(eo_2, ref(v_opts)),assignCallTemp(eo_3, ref(v_args)),c_console_getopt::t__parseshortoption(eo_0, eo_1, eo_2, eo_3))));
          if (toBoolean(LINE(132,throw_fatal("unknown class pear", ((void*)NULL))))) return v_error;
        }
      }
    }
  }
  return Array(ArrayInit(2).set(0, v_opts).set(1, v_non_opts).create());
} /* function */
/* SRC: phc-test/framework/external/Console/Getopt.php line 144 */
Variant c_console_getopt::t__parseshortoption(CVarRef v_arg, CVarRef v_short_options, Variant v_opts, Variant v_args) {
  INSTANCE_METHOD_INJECTION(Console_Getopt, Console_Getopt::_parseShortOption);
  Variant eo_0;
  Variant eo_1;
  int64 v_i = 0;
  Variant v_opt;
  Variant v_opt_arg;
  Variant v_spec;

  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, LINE(146,x_strlen(toString(v_arg)))); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_opt = v_arg.rvalAt(v_i));
        setNull(v_opt_arg);
        if (same(((v_spec = LINE(151,x_strstr(toString(v_short_options), v_opt)))), false) || equal(v_arg.rvalAt(v_i), ":")) {
          return LINE(153,throw_fatal("unknown class pear", ((void*)NULL)));
        }
        if (more(LINE(156,x_strlen(toString(v_spec))), 1LL) && equal(v_spec.rvalAt(1LL, 0x5BCA7C69B794F8CELL), ":")) {
          if (more(LINE(157,x_strlen(toString(v_spec))), 2LL) && equal(v_spec.rvalAt(2LL, 0x486AFCC090D5F98CLL), ":")) {
            if (less(v_i + 1LL, LINE(158,x_strlen(toString(v_arg))))) {
              v_opts.append(((assignCallTemp(eo_0, v_opt),assignCallTemp(eo_1, LINE(161,x_substr(toString(v_arg), toInt32(v_i + 1LL)))),Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()))));
              break;
            }
          }
          else {
            if (less(v_i + 1LL, LINE(167,x_strlen(toString(v_arg))))) {
              v_opts.append(((assignCallTemp(eo_0, v_opt),assignCallTemp(eo_1, LINE(168,x_substr(toString(v_arg), toInt32(v_i + 1LL)))),Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()))));
              break;
            }
            else if (toBoolean(df_lambda_2(LINE(170,x_each(ref(v_args))), v_opt_arg))) {}
            else return LINE(173,throw_fatal("unknown class pear", ((void*)NULL)));
          }
        }
        v_opts.append((Array(ArrayInit(2).set(0, v_opt).set(1, v_opt_arg).create())));
      }
    }
  }
  return null;
} /* function */
/* SRC: phc-test/framework/external/Console/Getopt.php line 185 */
Variant c_console_getopt::t__parselongoption(CVarRef v_arg, CVarRef v_long_options, Variant v_opts, Variant v_args) {
  INSTANCE_METHOD_INJECTION(Console_Getopt, Console_Getopt::_parseLongOption);
  Variant v_opt;
  Variant v_opt_arg;
  int v_opt_len = 0;
  int64 v_i = 0;
  Variant v_long_opt;
  Variant v_opt_start;
  Variant v_opt_rest;

  (silenceInc(), silenceDec(df_lambda_3(LINE(187,x_explode("=", toString(v_arg), toInt32(2LL))), v_opt, v_opt_arg)));
  (v_opt_len = LINE(188,x_strlen(toString(v_opt))));
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, LINE(190,x_count(v_long_options))); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        (v_long_opt = v_long_options.rvalAt(v_i));
        (v_opt_start = LINE(192,x_substr(toString(v_long_opt), toInt32(0LL), v_opt_len)));
        if (!equal(v_opt_start, v_opt)) continue;
        (v_opt_rest = LINE(198,x_substr(toString(v_long_opt), v_opt_len)));
        if (!equal(v_opt_rest, "") && !equal(v_opt.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "=") && less(v_i + 1LL, LINE(203,x_count(v_long_options))) && equal(v_opt, LINE(204,x_substr(toString(v_long_options.rvalAt(v_i + 1LL)), toInt32(0LL), v_opt_len)))) {
          return LINE(205,throw_fatal("unknown class pear", (concat3("Console_Getopt: option --", toString(v_opt), " is ambiguous"), (void*)NULL)));
        }
        if (equal(LINE(208,x_substr(toString(v_long_opt), toInt32(-1LL))), "=")) {
          if (!equal(LINE(209,x_substr(toString(v_long_opt), toInt32(-2LL))), "==")) {
            if (!(toBoolean(LINE(212,x_strlen(toString(v_opt_arg))))) && !((toBoolean(df_lambda_4(x_each(ref(v_args)), v_opt_arg))))) {
              return LINE(213,throw_fatal("unknown class pear", (concat3("Console_Getopt: option --", toString(v_opt), " requires an argument"), (void*)NULL)));
            }
          }
        }
        else if (toBoolean(v_opt_arg)) {
          return LINE(217,throw_fatal("unknown class pear", (concat3("Console_Getopt: option --", toString(v_opt), " doesn't allow an argument"), (void*)NULL)));
        }
        v_opts.append((Array(ArrayInit(2).set(0, concat("--", toString(v_opt))).set(1, v_opt_arg).create())));
        return null;
      }
    }
  }
  return LINE(224,throw_fatal("unknown class pear", ((void*)NULL)));
} /* function */
/* SRC: phc-test/framework/external/Console/Getopt.php line 234 */
Variant c_console_getopt::t_readphpargv() {
  INSTANCE_METHOD_INJECTION(Console_Getopt, Console_Getopt::readPHPArgv);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_argv __attribute__((__unused__)) = g->gv_argv;
  if (!(LINE(237,x_is_array(gv_argv)))) {
    if (!(toBoolean((silenceInc(), silenceDec(LINE(238,x_is_array(g->gv__SERVER.rvalAt("argv", 0x10EA7DC57768F8C6LL)))))))) {
      if (!(toBoolean((silenceInc(), silenceDec(LINE(239,x_is_array(g->GV(HTTP_SERVER_VARS).rvalAt("argv", 0x10EA7DC57768F8C6LL)))))))) {
        return LINE(240,throw_fatal("unknown class pear", ((void*)NULL)));
      }
      return g->GV(HTTP_SERVER_VARS).rvalAt("argv", 0x10EA7DC57768F8C6LL);
    }
    return g->gv__SERVER.rvalAt("argv", 0x10EA7DC57768F8C6LL);
  }
  return gv_argv;
} /* function */
Object co_console_getopt(CArrRef params, bool init /* = true */) {
  return Object(p_console_getopt(NEW(c_console_getopt)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$external$Console$Getopt_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/external/Console/Getopt.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$external$Console$Getopt_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(21,require("PEAR.php", true, variables, "phc-test/framework/external/Console/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
