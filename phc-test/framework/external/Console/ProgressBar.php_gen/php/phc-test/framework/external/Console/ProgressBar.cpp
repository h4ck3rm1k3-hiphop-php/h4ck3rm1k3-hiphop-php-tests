
#include <php/phc-test/framework/external/Console/ProgressBar.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 40 */
Variant c_console_progressbar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_console_progressbar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_console_progressbar::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("_skeleton", m__skeleton));
  props.push_back(NEW(ArrayElement)("_bar", m__bar));
  props.push_back(NEW(ArrayElement)("_blen", m__blen.isReferenced() ? ref(m__blen) : m__blen));
  props.push_back(NEW(ArrayElement)("_tlen", m__tlen.isReferenced() ? ref(m__tlen) : m__tlen));
  props.push_back(NEW(ArrayElement)("_target_num", m__target_num.isReferenced() ? ref(m__target_num) : m__target_num));
  props.push_back(NEW(ArrayElement)("_options", m__options.isReferenced() ? ref(m__options) : m__options));
  props.push_back(NEW(ArrayElement)("_rlen", m__rlen.isReferenced() ? ref(m__rlen) : m__rlen));
  props.push_back(NEW(ArrayElement)("_start_time", m__start_time.isReferenced() ? ref(m__start_time) : m__start_time));
  props.push_back(NEW(ArrayElement)("_rate_datapoints", m__rate_datapoints.isReferenced() ? ref(m__rate_datapoints) : m__rate_datapoints));
  c_ObjectData::o_get(props);
}
bool c_console_progressbar::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 6:
      HASH_EXISTS_STRING(0x725DD7B0D19989C6LL, _skeleton, 9);
      break;
    case 9:
      HASH_EXISTS_STRING(0x55514B307D889709LL, _start_time, 11);
      break;
    case 10:
      HASH_EXISTS_STRING(0x6B83B367BA63058ALL, _options, 8);
      break;
    case 18:
      HASH_EXISTS_STRING(0x194DDAE1FA8E4592LL, _target_num, 11);
      break;
    case 20:
      HASH_EXISTS_STRING(0x15687349249F1454LL, _tlen, 5);
      HASH_EXISTS_STRING(0x53F3733DA3E16FD4LL, _rlen, 5);
      break;
    case 23:
      HASH_EXISTS_STRING(0x6C296E0C0DA97797LL, _rate_datapoints, 16);
      break;
    case 26:
      HASH_EXISTS_STRING(0x33C1233723DC1F9ALL, _bar, 4);
      break;
    case 27:
      HASH_EXISTS_STRING(0x28A7835769C334FBLL, _blen, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_console_progressbar::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 6:
      HASH_RETURN_STRING(0x725DD7B0D19989C6LL, m__skeleton,
                         _skeleton, 9);
      break;
    case 9:
      HASH_RETURN_STRING(0x55514B307D889709LL, m__start_time,
                         _start_time, 11);
      break;
    case 10:
      HASH_RETURN_STRING(0x6B83B367BA63058ALL, m__options,
                         _options, 8);
      break;
    case 18:
      HASH_RETURN_STRING(0x194DDAE1FA8E4592LL, m__target_num,
                         _target_num, 11);
      break;
    case 20:
      HASH_RETURN_STRING(0x15687349249F1454LL, m__tlen,
                         _tlen, 5);
      HASH_RETURN_STRING(0x53F3733DA3E16FD4LL, m__rlen,
                         _rlen, 5);
      break;
    case 23:
      HASH_RETURN_STRING(0x6C296E0C0DA97797LL, m__rate_datapoints,
                         _rate_datapoints, 16);
      break;
    case 26:
      HASH_RETURN_STRING(0x33C1233723DC1F9ALL, m__bar,
                         _bar, 4);
      break;
    case 27:
      HASH_RETURN_STRING(0x28A7835769C334FBLL, m__blen,
                         _blen, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_console_progressbar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 6:
      HASH_SET_STRING(0x725DD7B0D19989C6LL, m__skeleton,
                      _skeleton, 9);
      break;
    case 9:
      HASH_SET_STRING(0x55514B307D889709LL, m__start_time,
                      _start_time, 11);
      break;
    case 10:
      HASH_SET_STRING(0x6B83B367BA63058ALL, m__options,
                      _options, 8);
      break;
    case 18:
      HASH_SET_STRING(0x194DDAE1FA8E4592LL, m__target_num,
                      _target_num, 11);
      break;
    case 20:
      HASH_SET_STRING(0x15687349249F1454LL, m__tlen,
                      _tlen, 5);
      HASH_SET_STRING(0x53F3733DA3E16FD4LL, m__rlen,
                      _rlen, 5);
      break;
    case 23:
      HASH_SET_STRING(0x6C296E0C0DA97797LL, m__rate_datapoints,
                      _rate_datapoints, 16);
      break;
    case 26:
      HASH_SET_STRING(0x33C1233723DC1F9ALL, m__bar,
                      _bar, 4);
      break;
    case 27:
      HASH_SET_STRING(0x28A7835769C334FBLL, m__blen,
                      _blen, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_console_progressbar::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x194DDAE1FA8E4592LL, m__target_num,
                         _target_num, 11);
      break;
    case 4:
      HASH_RETURN_STRING(0x15687349249F1454LL, m__tlen,
                         _tlen, 5);
      HASH_RETURN_STRING(0x53F3733DA3E16FD4LL, m__rlen,
                         _rlen, 5);
      break;
    case 7:
      HASH_RETURN_STRING(0x6C296E0C0DA97797LL, m__rate_datapoints,
                         _rate_datapoints, 16);
      break;
    case 9:
      HASH_RETURN_STRING(0x55514B307D889709LL, m__start_time,
                         _start_time, 11);
      break;
    case 10:
      HASH_RETURN_STRING(0x6B83B367BA63058ALL, m__options,
                         _options, 8);
      break;
    case 11:
      HASH_RETURN_STRING(0x28A7835769C334FBLL, m__blen,
                         _blen, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_console_progressbar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(console_progressbar)
ObjectData *c_console_progressbar::create(CVarRef v_formatstring, CVarRef v_bar, CVarRef v_prefill, CVarRef v_width, CVarRef v_target_num, CArrRef v_options //  = ScalarArrays::sa_[0]
) {
  init();
  t_console_progressbar(v_formatstring, v_bar, v_prefill, v_width, v_target_num, v_options);
  return this;
}
ObjectData *c_console_progressbar::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 5) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)));
  } else return this;
}
ObjectData *c_console_progressbar::cloneImpl() {
  c_console_progressbar *obj = NEW(c_console_progressbar)();
  cloneSet(obj);
  return obj;
}
void c_console_progressbar::cloneSet(c_console_progressbar *clone) {
  clone->m__skeleton = m__skeleton;
  clone->m__bar = m__bar;
  clone->m__blen = m__blen.isReferenced() ? ref(m__blen) : m__blen;
  clone->m__tlen = m__tlen.isReferenced() ? ref(m__tlen) : m__tlen;
  clone->m__target_num = m__target_num.isReferenced() ? ref(m__target_num) : m__target_num;
  clone->m__options = m__options.isReferenced() ? ref(m__options) : m__options;
  clone->m__rlen = m__rlen.isReferenced() ? ref(m__rlen) : m__rlen;
  clone->m__start_time = m__start_time.isReferenced() ? ref(m__start_time) : m__start_time;
  clone->m__rate_datapoints = m__rate_datapoints.isReferenced() ? ref(m__rate_datapoints) : m__rate_datapoints;
  ObjectData::cloneSet(clone);
}
Variant c_console_progressbar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_console_progressbar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_console_progressbar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_console_progressbar$os_get(const char *s) {
  return c_console_progressbar::os_get(s, -1);
}
Variant &cw_console_progressbar$os_lval(const char *s) {
  return c_console_progressbar::os_lval(s, -1);
}
Variant cw_console_progressbar$os_constant(const char *s) {
  return c_console_progressbar::os_constant(s);
}
Variant cw_console_progressbar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_console_progressbar::os_invoke(c, s, params, -1, fatal);
}
void c_console_progressbar::init() {
  m__skeleton = null;
  m__bar = null;
  m__blen = null;
  m__tlen = null;
  m__target_num = null;
  m__options = ScalarArrays::sa_[0];
  m__rlen = 0LL;
  m__start_time = null;
  m__rate_datapoints = ScalarArrays::sa_[0];
}
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 92 */
void c_console_progressbar::t_console_progressbar(CVarRef v_formatstring, CVarRef v_bar, CVarRef v_prefill, CVarRef v_width, CVarRef v_target_num, CArrRef v_options //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::Console_ProgressBar);
  bool oldInCtor = gasInCtor(true);
  LINE(96,t_reset(v_formatstring, v_bar, v_prefill, v_width, v_target_num, v_options));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 177 */
bool c_console_progressbar::t_reset(CVarRef v_formatstring, CVarRef v_bar, CVarRef v_prefill, CVarRef v_width, CVarRef v_target_num, Variant v_options //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::reset);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Array v_default_options;
  Variant v_intopts;
  Primitive v_key = 0;
  Variant v_value;
  String v_cur;
  Variant v_max;
  Numeric v_padding = 0;
  String v_perc;
  Array v_transitions;
  int v_slen = 0;
  Variant v_blen;
  Variant v_tlen;
  String v_lbar;
  String v_rbar;

  (m__target_num = v_target_num);
  (v_default_options = ScalarArrays::sa_[1]);
  (v_intopts = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_default_options.begin("console_progressbar"); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3.second();
      v_key = iter3.first();
      {
        if (!(isset(v_options, v_key))) {
          v_intopts.set(v_key, (v_value));
        }
        else {
          LINE(196,(assignCallTemp(eo_0, ref(lval(v_options.lvalAt(v_key)))),assignCallTemp(eo_1, x_gettype(v_value)),x_settype(eo_0, eo_1)));
          v_intopts.set(v_key, (v_options.rvalAt(v_key)));
        }
      }
    }
  }
  (m__options = (v_options = v_intopts));
  (v_cur = LINE(203,(assignCallTemp(eo_1, toString(v_options.rvalAt("fraction_pad", 0x1B51B486C9ADA77BLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))),assignCallTemp(eo_2, toString(LINE(202,x_strlen(toString(toInt64(v_target_num)))))),assignCallTemp(eo_4, toString(v_options.rvalAt("fraction_precision", 0x7F88476ED53EEAF6LL))),concat6("%2$'", eo_1, eo_2, ".", eo_4, "f"))));
  (v_max = v_cur);
  v_max.set(1LL, (3LL), 0x5BCA7C69B794F8CELL);
  if (toBoolean(LINE(207,x_version_compare("5.2.5.hiphop" /* PHP_VERSION */, "4.3.7", "ge")))) {
    (v_padding = 4LL + v_options.rvalAt("percent_precision", 0x1A7A826BF348CE9BLL));
  }
  else {
    (v_padding = 3LL);
  }
  (v_perc = LINE(213,concat6("%4$'", toString(v_options.rvalAt("percent_pad", 0x2188AEC424F52F92LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_padding), ".", toString(v_options.rvalAt("percent_precision", 0x1A7A826BF348CE9BLL)), "f")));
  (v_transitions = (assignCallTemp(eo_1, LINE(217,concat3(v_cur, "/", toString(v_max)))),assignCallTemp(eo_2, v_cur),assignCallTemp(eo_3, v_max),assignCallTemp(eo_4, concat(v_perc, "%%")),Array(ArrayInit(8).set(0, "%%", "%%", 0x3278EA6D6201A171LL).set(1, "%fraction%", eo_1, 0x63F6A706BF601E0FLL).set(2, "%current%", eo_2, 0x5ECD980BA1ED7EC3LL).set(3, "%max%", eo_3, 0x39DB7A1F1E841946LL).set(4, "%percent%", eo_4, 0x01717DE1E2B188A2LL).set(5, "%bar%", "%1$s", 0x1D5835AD17CB59CCLL).set(6, "%elapsed%", "%5$s", 0x04B0E9057A8FE910LL).set(7, "%estimate%", "%6$s", 0x00B4DB7AE2CF3E9BLL).create())));
  (m__skeleton = LINE(226,x_strtr(toString(v_formatstring), v_transitions)));
  (v_slen = LINE(228,x_strlen(x_sprintf(7, m__skeleton, ScalarArrays::sa_[2]))));
  if (toBoolean(v_options.rvalAt("width_absolute", 0x2AE342CE8FF40970LL))) {
    (v_blen = v_width - v_slen);
    (v_tlen = v_width);
  }
  else {
    (v_tlen = v_width + v_slen);
    (v_blen = v_width);
  }
  (v_lbar = LINE(238,x_str_pad(toString(v_bar), toInt32(v_blen), toString(v_bar.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toInt32(0LL) /* STR_PAD_LEFT */)));
  (v_rbar = LINE(239,(assignCallTemp(eo_0, toString(v_prefill)),assignCallTemp(eo_1, toInt32(v_blen)),assignCallTemp(eo_2, toString(x_substr(toString(v_prefill), toInt32(-1LL), toInt32(1LL)))),x_str_pad(eo_0, eo_1, eo_2))));
  (m__bar = concat_rev(toString(LINE(241,x_substr(v_rbar, toInt32(0LL), toInt32(v_blen)))), toString(x_substr(v_lbar, toInt32(negate(v_blen))))));
  (m__blen = v_blen);
  (m__tlen = v_tlen);
  (o_lval("_first", 0x2D3B453FE96C6105LL) = true);
  return true;
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 258 */
void c_console_progressbar::t_update(CVarRef v_current) {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::update);
  LINE(260,t__adddatapoint(v_current));
  if (toBoolean(o_get("_first", 0x2D3B453FE96C6105LL))) {
    if (toBoolean(m__options.rvalAt("ansi_terminal", 0x3CB74C1AEE902875LL))) {
      print("\033[s");
    }
    (o_lval("_first", 0x2D3B453FE96C6105LL) = false);
    (m__start_time = LINE(266,t__fetchtime()));
    LINE(267,t_display(v_current));
    return;
  }
  LINE(270,t_erase());
  LINE(271,t_display(v_current));
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 284 */
bool c_console_progressbar::t_display(CVarRef v_current) {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::display);
  Numeric v_percent = 0;
  double v_filled = 0.0;
  Variant v_visbar;
  String v_elapsed;
  String v_estimate;

  (v_percent = divide(v_current, m__target_num));
  (v_filled = LINE(287,x_round(v_percent * m__blen)));
  (v_visbar = LINE(288,x_substr(m__bar, toInt32(toDouble(m__blen) - v_filled), toInt32(m__blen))));
  (v_elapsed = LINE(291,t__formatseconds(LINE(290,t__fetchtime()) - m__start_time)));
  (v_estimate = LINE(292,t__formatseconds(t__generateestimate())));
  (m__rlen = LINE(296,x_printf(7, m__skeleton, Array(ArrayInit(6).set(0, v_visbar).set(1, v_current).set(2, m__target_num).set(3, v_percent * 100LL).set(4, v_elapsed).set(5, v_estimate).create()))));
  if (LINE(298,x_is_null(m__rlen))) {
    (m__rlen = m__tlen);
  }
  else if (less(m__rlen, m__tlen)) {
    print(LINE(302,x_str_repeat(" ", toInt32(m__tlen - m__rlen))));
    (m__rlen = m__tlen);
  }
  return true;
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 315 */
void c_console_progressbar::t_erase(bool v_clear //  = false
) {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::erase);
  Variant eo_0;
  Variant eo_1;
  if ((toBoolean(m__options.rvalAt("ansi_terminal", 0x3CB74C1AEE902875LL))) && (!(v_clear))) {
    if (toBoolean(m__options.rvalAt("ansi_clear", 0x3D76E9B3D401BF03LL))) {
      print("\033[2K\033[u");
    }
    else {
      print("\033[u");
    }
  }
  else {
    print(LINE(324,(assignCallTemp(eo_0, x_chr(8LL)),assignCallTemp(eo_1, toInt32(m__rlen)),x_str_repeat(eo_0, eo_1))));
  }
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 336 */
String c_console_progressbar::t__formatseconds(Numeric v_seconds) {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::_formatSeconds);
  double v_hou = 0.0;
  double v_min = 0.0;
  double v_sec = 0.0;
  String v_format;

  (v_hou = LINE(338,x_floor(toDouble(divide(v_seconds, 3600LL)))));
  (v_min = LINE(339,x_floor(toDouble(divide((toDouble(v_seconds) - v_hou * 3600LL), 60LL)))));
  (v_sec = toDouble(v_seconds) - v_hou * 3600LL - v_min * 60LL);
  if (equal(v_hou, 0LL)) {
    if (toBoolean(LINE(342,x_version_compare("5.2.5.hiphop" /* PHP_VERSION */, "4.3.7", "ge")))) {
      (v_format = "%2$02d:%3$05.2f");
    }
    else {
      (v_format = "%2$02d:%3$02.2f");
    }
  }
  else if (less(v_hou, 100LL)) {
    (v_format = "%02d:%02d:%02d");
  }
  else {
    (v_format = "%05d:%02d");
  }
  return LINE(352,x_sprintf(4, v_format, Array(ArrayInit(3).set(0, v_hou).set(1, v_min).set(2, v_sec).create())));
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 356 */
Variant c_console_progressbar::t__fetchtime() {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::_fetchTime);
  Variant eo_0;
  Variant eo_1;
  if (toBoolean(LINE(360,x_version_compare("5.2.5.hiphop" /* PHP_VERSION */, "5.0.0", "ge")))) {
    return LINE(361,x_microtime(true));
  }
  return LINE(363,x_array_sum((assignCallTemp(eo_1, toString(x_microtime())),x_explode(" ", eo_1))));
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 366 */
void c_console_progressbar::t__adddatapoint(CVarRef v_val) {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::_addDatapoint);
  Variant eo_0;
  Variant eo_1;
  if (equal(LINE(367,x_count(m__rate_datapoints)), m__options.rvalAt("num_datapoints", 0x1AEF00E5F4E2F22FLL))) {
    LINE(369,x_array_shift(ref(lval(m__rate_datapoints))));
  }
  m__rate_datapoints.append(((assignCallTemp(eo_0, LINE(372,t__fetchtime())),assignCallTemp(eo_1, v_val),Array(ArrayInit(2).set(0, "time", eo_0, 0x46C13C417DE7E0A0LL).set(1, "value", eo_1, 0x69E7413AE0C88471LL).create()))));
} /* function */
/* SRC: phc-test/framework/external/Console/ProgressBar.php line 377 */
Numeric c_console_progressbar::t__generateestimate() {
  INSTANCE_METHOD_INJECTION(Console_ProgressBar, Console_ProgressBar::_generateEstimate);
  Variant v_first;
  Variant v_last;

  if (less(LINE(378,x_count(m__rate_datapoints)), 2LL)) {
    return 0.0;
  }
  (v_first = m__rate_datapoints.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (v_last = LINE(382,x_end(ref(lval(m__rate_datapoints)))));
  return divide((m__target_num - v_last.rvalAt("value", 0x69E7413AE0C88471LL)), (v_last.rvalAt("value", 0x69E7413AE0C88471LL) - v_first.rvalAt("value", 0x69E7413AE0C88471LL))) * (v_last.rvalAt("time", 0x46C13C417DE7E0A0LL) - v_first.rvalAt("time", 0x46C13C417DE7E0A0LL));
} /* function */
Object co_console_progressbar(CArrRef params, bool init /* = true */) {
  return Object(p_console_progressbar(NEW(c_console_progressbar)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$external$Console$ProgressBar_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/external/Console/ProgressBar.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$external$Console$ProgressBar_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
