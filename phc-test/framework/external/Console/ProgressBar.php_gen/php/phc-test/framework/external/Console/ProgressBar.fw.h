
#ifndef __GENERATED_php_phc_test_framework_external_Console_ProgressBar_fw_h__
#define __GENERATED_php_phc_test_framework_external_Console_ProgressBar_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(console_progressbar)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_external_Console_ProgressBar_fw_h__
