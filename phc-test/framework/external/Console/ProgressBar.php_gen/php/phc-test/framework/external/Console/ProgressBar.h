
#ifndef __GENERATED_php_phc_test_framework_external_Console_ProgressBar_h__
#define __GENERATED_php_phc_test_framework_external_Console_ProgressBar_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/external/Console/ProgressBar.fw.h>

// Declarations
#include <cls/console_progressbar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$external$Console$ProgressBar_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_console_progressbar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_external_Console_ProgressBar_h__
