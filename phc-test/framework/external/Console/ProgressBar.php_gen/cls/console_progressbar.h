
#ifndef __GENERATED_cls_console_progressbar_h__
#define __GENERATED_cls_console_progressbar_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/external/Console/ProgressBar.php line 40 */
class c_console_progressbar : virtual public ObjectData {
  BEGIN_CLASS_MAP(console_progressbar)
  END_CLASS_MAP(console_progressbar)
  DECLARE_CLASS(console_progressbar, Console_ProgressBar, ObjectData)
  void init();
  public: String m__skeleton;
  public: String m__bar;
  public: Variant m__blen;
  public: Variant m__tlen;
  public: Variant m__target_num;
  public: Variant m__options;
  public: Variant m__rlen;
  public: Variant m__start_time;
  public: Variant m__rate_datapoints;
  public: void t_console_progressbar(CVarRef v_formatstring, CVarRef v_bar, CVarRef v_prefill, CVarRef v_width, CVarRef v_target_num, CArrRef v_options = ScalarArrays::sa_[0]);
  public: ObjectData *create(CVarRef v_formatstring, CVarRef v_bar, CVarRef v_prefill, CVarRef v_width, CVarRef v_target_num, CArrRef v_options = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: bool t_reset(CVarRef v_formatstring, CVarRef v_bar, CVarRef v_prefill, CVarRef v_width, CVarRef v_target_num, Variant v_options = ScalarArrays::sa_[0]);
  public: void t_update(CVarRef v_current);
  public: bool t_display(CVarRef v_current);
  public: void t_erase(bool v_clear = false);
  public: String t__formatseconds(Numeric v_seconds);
  public: Variant t__fetchtime();
  public: void t__adddatapoint(CVarRef v_val);
  public: Numeric t__generateestimate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_console_progressbar_h__
