
#include <php/phc-test/framework/lib/async_test.h>
#include <php/phc-test/framework/lib/test.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/lib/async_test.php line 23 */
Variant c_asyncbundle::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_asyncbundle::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_asyncbundle::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_asyncbundle::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_asyncbundle::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_asyncbundle::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_asyncbundle::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_asyncbundle::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(asyncbundle)
ObjectData *c_asyncbundle::create(Variant v_object, Variant v_subject) {
  init();
  t___construct(v_object, v_subject);
  return this;
}
ObjectData *c_asyncbundle::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_asyncbundle::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_asyncbundle::cloneImpl() {
  c_asyncbundle *obj = NEW(c_asyncbundle)();
  cloneSet(obj);
  return obj;
}
void c_asyncbundle::cloneSet(c_asyncbundle *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_asyncbundle::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x56AA34D1723D3920LL, read_streams) {
        return (t_read_streams(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x61DD21ABF790E9E2LL, continuation) {
        return (t_continuation(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x77F01963731361B5LL, get_in) {
        return (t_get_in());
      }
      break;
    case 11:
      HASH_GUARD(0x1241B2341F67E88BLL, get_command) {
        return (t_get_command());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_asyncbundle::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x56AA34D1723D3920LL, read_streams) {
        return (t_read_streams(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x61DD21ABF790E9E2LL, continuation) {
        return (t_continuation(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x77F01963731361B5LL, get_in) {
        return (t_get_in());
      }
      break;
    case 11:
      HASH_GUARD(0x1241B2341F67E88BLL, get_command) {
        return (t_get_command());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_asyncbundle::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_asyncbundle$os_get(const char *s) {
  return c_asyncbundle::os_get(s, -1);
}
Variant &cw_asyncbundle$os_lval(const char *s) {
  return c_asyncbundle::os_lval(s, -1);
}
Variant cw_asyncbundle$os_constant(const char *s) {
  return c_asyncbundle::os_constant(s);
}
Variant cw_asyncbundle$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_asyncbundle::os_invoke(c, s, params, -1, fatal);
}
void c_asyncbundle::init() {
}
/* SRC: phc-test/framework/lib/async_test.php line 25 */
void c_asyncbundle::t___construct(Variant v_object, Variant v_subject) {
  INSTANCE_METHOD_INJECTION(AsyncBundle, AsyncBundle::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("object", 0x7F30BC4E222B1861LL) = v_object);
  (o_lval("subject", 0x0EDD9FCC9525C3B2LL) = v_subject);
  (o_lval("state", 0x5C84C80672BA5E22LL) = 0LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 32 */
void c_asyncbundle::t_start() {
  INSTANCE_METHOD_INJECTION(AsyncBundle, AsyncBundle::start);
  LINE(34,o_get("object", 0x7F30BC4E222B1861LL).o_invoke_few_args("start_new_program", 0x202C70C55546CDD6LL, 1, this));
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 37 */
Variant c_asyncbundle::t_get_command() {
  INSTANCE_METHOD_INJECTION(AsyncBundle, AsyncBundle::get_command);
  return o_get("commands", 0x189A3C89F0CA1103LL).rvalAt(o_get("state", 0x5C84C80672BA5E22LL));
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 42 */
Variant c_asyncbundle::t_get_in() {
  INSTANCE_METHOD_INJECTION(AsyncBundle, AsyncBundle::get_in);
  if (t___isset("ins")) return o_get("ins", 0x192B6EDE1C5E129FLL).rvalAt(o_get("state", 0x5C84C80672BA5E22LL));
  return null;
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 50 */
void c_asyncbundle::t_continuation() {
  INSTANCE_METHOD_INJECTION(AsyncBundle, AsyncBundle::continuation);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_state;
  Variant v_object;
  Variant v_handler;
  Variant v_result;
  Variant v_callback;

  LINE(52,f_inst(concat5("continuing from state ", toString(o_get("state", 0x5C84C80672BA5E22LL)), " of ", toString(o_get("subject", 0x0EDD9FCC9525C3B2LL)), "\n")));
  (v_state = o_get("state", 0x5C84C80672BA5E22LL));
  lval(o_lval("outs", 0x3D74198A5F0F7DDCLL)).set(v_state, (o_get("out", 0x7C801AC012E9F722LL)));
  lval(o_lval("errs", 0x15E266045DABA002LL)).set(v_state, (o_get("err", 0x382AF294107C2C75LL)));
  lval(o_lval("exits", 0x1AE9F4DC5F326DFFLL)).set(v_state, (o_get("exit", 0x461F139A4051755ALL)));
  t___unset("out");
  t___unset("err");
  t___unset("exit");
  (v_object = o_get("object", 0x7F30BC4E222B1861LL));
  if (isset(o_get("out_handlers", 0x5FE40B58F183D044LL), v_state)) {
    (v_handler = o_get("out_handlers", 0x5FE40B58F183D044LL).rvalAt(v_state));
    (v_result = LINE(68,v_object.o_invoke_few_args(toString(v_handler), -1LL, 2, lval(o_lval("outs", 0x3D74198A5F0F7DDCLL)).refvalAt(v_state), this)));
    if (same(v_result, false)) return;
    lval(o_lval("outs", 0x3D74198A5F0F7DDCLL)).set(v_state, (v_result));
  }
  if (isset(o_get("err_handlers", 0x0D4F3DB82D56CC90LL), v_state)) {
    (v_handler = o_get("err_handlers", 0x0D4F3DB82D56CC90LL).rvalAt(v_state));
    (v_result = LINE(77,v_object.o_invoke_few_args(toString(v_handler), -1LL, 2, lval(o_lval("errs", 0x15E266045DABA002LL)).refvalAt(v_state), this)));
    if (same(v_result, false)) return;
    lval(o_lval("errs", 0x15E266045DABA002LL)).set(v_state, (v_result));
  }
  if (isset(o_get("exit_handlers", 0x4016CDEF485D2C08LL), v_state)) {
    (v_handler = o_get("exit_handlers", 0x4016CDEF485D2C08LL).rvalAt(v_state));
    (v_result = LINE(86,v_object.o_invoke_few_args(toString(v_handler), -1LL, 2, lval(o_lval("exits", 0x1AE9F4DC5F326DFFLL)).refvalAt(v_state), this)));
    if (same(v_result, false)) return;
    lval(o_lval("exits", 0x1AE9F4DC5F326DFFLL)).set(v_state, (v_result));
  }
  if (isset(o_get("callbacks", 0x1F0B65B99DB7A31ELL), v_state)) {
    (v_callback = o_get("callbacks", 0x1F0B65B99DB7A31ELL).rvalAt(v_state));
    (v_result = LINE(97,v_object.o_invoke_few_args(toString(v_callback), -1LL, 4, lval(o_lval("outs", 0x3D74198A5F0F7DDCLL)).refvalAt(v_state), lval(o_lval("errs", 0x15E266045DABA002LL)).refvalAt(v_state), lval(o_lval("exits", 0x1AE9F4DC5F326DFFLL)).refvalAt(v_state), this)));
    if (same(v_result, false)) return;
  }
  o_lval("state", 0x5C84C80672BA5E22LL) += 1LL;
  (v_state = o_get("state", 0x5C84C80672BA5E22LL));
  if (isset(o_get("commands", 0x189A3C89F0CA1103LL), v_state)) {
    LINE(108,f_inst(toString("Start next program: ") + toString(o_get("commands", 0x189A3C89F0CA1103LL).rvalAt(v_state))));
    LINE(109,v_object.o_invoke_few_args("start_program", 0x1EAF66BAF961A922LL, 1, this));
  }
  else if (t___isset("final")) {
    LINE(113,f_inst("Finalize"));
    LINE(114,v_object.o_invoke_few_args(toString(o_get("final", 0x5192930B2145036ELL)), -1LL, 1, this));
  }
  else {
    f_exit("uhoh\n");
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 122 */
void c_asyncbundle::t_read_streams() {
  INSTANCE_METHOD_INJECTION(AsyncBundle, AsyncBundle::read_streams);
  concat_assign(o_lval("out", 0x7C801AC012E9F722LL), toString(LINE(124,x_stream_get_contents(toObject(o_get("pipes", 0x22E86BF7D2042689LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL))))));
  concat_assign(o_lval("err", 0x382AF294107C2C75LL), toString(LINE(125,x_stream_get_contents(toObject(o_get("pipes", 0x22E86BF7D2042689LL).rvalAt(2LL, 0x486AFCC090D5F98CLL))))));
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 141 */
Variant c_asynctest::os_get(const char *s, int64 hash) {
  return c_test::os_get(s, hash);
}
Variant &c_asynctest::os_lval(const char *s, int64 hash) {
  return c_test::os_lval(s, hash);
}
void c_asynctest::o_get(ArrayElementVec &props) const {
  c_test::o_get(props);
}
bool c_asynctest::o_exists(CStrRef s, int64 hash) const {
  return c_test::o_exists(s, hash);
}
Variant c_asynctest::o_get(CStrRef s, int64 hash) {
  return c_test::o_get(s, hash);
}
Variant c_asynctest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_test::o_set(s, hash, v, forInit);
}
Variant &c_asynctest::o_lval(CStrRef s, int64 hash) {
  return c_test::o_lval(s, hash);
}
Variant c_asynctest::os_constant(const char *s) {
  return c_test::os_constant(s);
}
IMPLEMENT_CLASS(asynctest)
ObjectData *c_asynctest::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_asynctest::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_asynctest::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_asynctest::cloneImpl() {
  c_asynctest *obj = NEW(c_asynctest)();
  cloneSet(obj);
  return obj;
}
void c_asynctest::cloneSet(c_asynctest *clone) {
  c_test::cloneSet(clone);
}
Variant c_asynctest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(params.rvalAt(0)));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 12:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(params.rvalAt(0)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke(s, params, hash, fatal);
}
Variant c_asynctest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x0F86FC9BC0B5BD41LL, reduce_debug_function) {
        return (t_reduce_debug_function(a0, a1), null);
      }
      break;
    case 2:
      HASH_GUARD(0x1EAF66BAF961A922LL, start_program) {
        return (t_start_program(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x109136197F5FE1E5LL, homogenize_output) {
        return (t_homogenize_output(a0));
      }
      HASH_GUARD(0x726772D0F7A3A2E5LL, get_all_dependencies) {
        return (t_get_all_dependencies());
      }
      break;
    case 7:
      HASH_GUARD(0x7D9172C389FDB2C7LL, get_dependent_test_names) {
        return (t_get_dependent_test_names());
      }
      break;
    case 12:
      HASH_GUARD(0x22A6BFFC4C10296CLL, fail_on_output) {
        return (t_fail_on_output(ref(a0), a1));
      }
      break;
    case 14:
      HASH_GUARD(0x6056A09A61AC106ELL, get_name) {
        return (t_get_name());
      }
      break;
    case 16:
      HASH_GUARD(0x547CFD01CE2F8190LL, check_prerequisites) {
        return (t_check_prerequisites());
      }
      break;
    case 17:
      HASH_GUARD(0x76D94AF9AA95C111LL, finish_test) {
        return (t_finish_test(), null);
      }
      break;
    case 22:
      HASH_GUARD(0x202C70C55546CDD6LL, start_new_program) {
        return (t_start_new_program(a0), null);
      }
      break;
    case 24:
      HASH_GUARD(0x474C6B53E7316718LL, reduce_run_function) {
        return (t_reduce_run_function(a0, a1));
      }
      break;
    case 28:
      HASH_GUARD(0x4AA8CD1A99873EBCLL, reduce_checking_function) {
        return (t_reduce_checking_function(a0));
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_test::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_asynctest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_test::os_invoke(c, s, params, hash, fatal);
}
Variant cw_asynctest$os_get(const char *s) {
  return c_asynctest::os_get(s, -1);
}
Variant &cw_asynctest$os_lval(const char *s) {
  return c_asynctest::os_lval(s, -1);
}
Variant cw_asynctest$os_constant(const char *s) {
  return c_asynctest::os_constant(s);
}
Variant cw_asynctest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_asynctest::os_invoke(c, s, params, -1, fatal);
}
void c_asynctest::init() {
  c_test::init();
}
/* SRC: phc-test/framework/lib/async_test.php line 143 */
void c_asynctest::t___construct() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("waiting_procs", 0x36C6008865C6AFB8LL) = ScalarArrays::sa_[3]);
  (o_lval("running_procs", 0x609786D2623AA565LL) = ScalarArrays::sa_[3]);
  (o_lval("reductions", 0x6017611E2ACE004ELL) = ScalarArrays::sa_[3]);
  (o_lval("reduction_result", 0x1D60DA0B0B3279A5LL) = ScalarArrays::sa_[3]);
  (o_lval("is_special_reduction", 0x418AD6311B531DCFLL) = ScalarArrays::sa_[3]);
  LINE(152,c_test::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 159 */
Variant c_asynctest::t_reduce_run_function(Variant v_command, Variant v_stdin) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::reduce_run_function);
  Variant v_result;

  (v_result = LINE(161,invoke_failed("complete_exec", Array(ArrayInit(3).set(0, ref(v_command)).set(1, ref(v_stdin)).set(2, 3LL).create()), 0x00000000739F6DDCLL)));
  if (equal(v_result.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "Timeout")) return ScalarArrays::sa_[5];
  return v_result;
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 168 */
Variant c_asynctest::t_reduce_checking_function(CVarRef v_program) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::reduce_checking_function);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_working_directory __attribute__((__unused__)) = g->GV(working_directory);
  Variant v_filename;

  (v_filename = LINE(171,x_tempnam(toString(gv_working_directory), "reduce_")));
  LINE(172,x_file_put_contents(toString(v_filename), v_program));
  lval(o_lval("is_special_reduction", 0x418AD6311B531DCFLL)).set(v_filename, (true));
  LINE(176,o_root_invoke_few_args("run_test", 0x7BB317B3EC138471LL, 1, v_filename));
  LINE(178,t_run_out_all_procs());
  return o_get("reduction_result", 0x1D60DA0B0B3279A5LL).rvalAt(v_filename);
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 183 */
void c_asynctest::t_reduce_debug_function(CVarRef v_level, CVarRef v_message) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::reduce_debug_function);
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 188 */
void c_asynctest::t_reduce_failure(CStrRef v_reason, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::reduce_failure);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant v_subject;
  Object v_reduce;
  Variant v_contents;
  Variant v_final_contents;
  Object v_e;

  LINE(190,f_inst(concat4("reduce failure: ", v_reason, ", ", toString(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL)))));
  (v_subject = toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL));
  if (isset(o_get("is_special_reduction", 0x418AD6311B531DCFLL), v_subject)) {
    lval(o_lval("reduction_result", 0x1D60DA0B0B3279A5LL)).set(v_subject, (true));
  }
  else {
    try {
      (v_reduce = LINE(204,create_object("reduce", Array())));
      lval(o_lval("reductions", 0x6017611E2ACE004ELL)).set(v_subject, (v_reduce));
      LINE(206,v_reduce->o_invoke_few_args("set_checking_function", 0x541CE80583348E0ALL, 1, Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "reduce_checking_function").create())));
      LINE(207,v_reduce->o_invoke_few_args("set_run_command_function", 0x0D01C969891A2699LL, 1, Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "reduce_run_function").create())));
      LINE(208,v_reduce->o_invoke_few_args("set_phc", 0x7FBC945FCF50C511LL, 1, gv_phc));
      LINE(209,v_reduce->o_invoke_few_args("set_debug_function", 0x066D22D85BB455E7LL, 1, Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "reduce_debug_function").create())));
      (v_contents = LINE(212,x_file_get_contents(toString(v_subject))));
      if (!(toBoolean(LINE(214,v_reduce->o_invoke_few_args("has_syntax_errors", 0x535A9BDF831CAC10LL, 1, v_contents))))) {
        (v_final_contents = LINE(216,v_reduce->o_invoke_few_args("run_on_php", 0x6A40D7C11ADAF4FFLL, 1, v_contents)));
        LINE(217,(assignCallTemp(eo_0, (assignCallTemp(eo_2, toString(v_subject)),assignCallTemp(eo_4, toString(t_get_name())),concat4(eo_2, ".", eo_4, "_reduced"))),assignCallTemp(eo_1, v_final_contents),x_file_put_contents(eo_0, eo_1)));
      }
    } catch (Object e) {
      if (true) {
        throw_fatal("unknown class reduceexception");
      } else {
        throw;
      }
    }
    LINE(231,t_mark_failure(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL), toObject(v_bundle).o_get("commands", 0x189A3C89F0CA1103LL), toObject(v_bundle).o_get("outs", 0x3D74198A5F0F7DDCLL), toObject(v_bundle).o_get("errs", 0x15E266045DABA002LL), toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL), v_reason));
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 235 */
void c_asynctest::t_reduce_success(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::reduce_success);
  LINE(237,f_inst(toString("reduce success: ") + toString(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL))));
  if (isset(o_get("is_special_reduction", 0x418AD6311B531DCFLL), toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL))) {
    lval(o_lval("reduction_result", 0x1D60DA0B0B3279A5LL)).set(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL), (false));
  }
  else {
    invoke_too_many_args("mark_success", (4), ((LINE(250,t_mark_success(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL))), null)));
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 254 */
void c_asynctest::t_reduce_timeout(CVarRef v_reason, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::reduce_timeout);
  if (isset(o_get("is_special_reduction", 0x418AD6311B531DCFLL), toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL))) {
    lval(o_lval("reduction_result", 0x1D60DA0B0B3279A5LL)).set(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL), (false));
  }
  else {
    LINE(269,t_mark_timeout(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL), toObject(v_bundle).o_get("commands", 0x189A3C89F0CA1103LL), toObject(v_bundle).o_get("outs", 0x3D74198A5F0F7DDCLL), toObject(v_bundle).o_get("errs", 0x15E266045DABA002LL), toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL)));
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 276 */
void c_asynctest::t_async_failure(CStrRef v_reason, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::async_failure);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_reduce __attribute__((__unused__)) = g->GV(opt_reduce);
  if (toBoolean(gv_opt_reduce)) LINE(280,t_reduce_failure(v_reason, v_bundle));
  else {
    LINE(289,t_mark_failure(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL), toObject(v_bundle).o_get("commands", 0x189A3C89F0CA1103LL), toObject(v_bundle).o_get("outs", 0x3D74198A5F0F7DDCLL), toObject(v_bundle).o_get("errs", 0x15E266045DABA002LL), toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL), v_reason));
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 293 */
void c_asynctest::t_async_timeout(CStrRef v_reason, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::async_timeout);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_reduce __attribute__((__unused__)) = g->GV(opt_reduce);
  if (toBoolean(gv_opt_reduce)) LINE(297,t_reduce_timeout(v_reason, v_bundle));
  else {
    LINE(305,t_mark_timeout(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL), toObject(v_bundle).o_get("commands", 0x189A3C89F0CA1103LL), toObject(v_bundle).o_get("outs", 0x3D74198A5F0F7DDCLL), toObject(v_bundle).o_get("errs", 0x15E266045DABA002LL), toObject(v_bundle).o_get("exits", 0x1AE9F4DC5F326DFFLL)));
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 309 */
void c_asynctest::t_async_success(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::async_success);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_opt_reduce __attribute__((__unused__)) = g->GV(opt_reduce);
  if (toBoolean(gv_opt_reduce)) LINE(313,t_reduce_success(v_bundle));
  else {
    invoke_too_many_args("mark_success", (4), ((LINE(321,t_mark_success(toObject(v_bundle).o_get("subject", 0x0EDD9FCC9525C3B2LL))), null)));
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 325 */
Variant c_asynctest::t_fail_on_output(Variant v_stream, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::fail_on_output);
  if ((same(v_stream, 0LL)) || (same(v_stream, ""))) return v_stream;
  LINE(330,t_async_failure("exit or err not clear", v_bundle));
  return false;
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 335 */
void c_asynctest::t_start_program(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::start_program);
  LINE(337,x_array_unshift(2, ref(lval(o_lval("waiting_procs", 0x36C6008865C6AFB8LL))), v_bundle));
  LINE(339,t_check_capacity());
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 343 */
void c_asynctest::t_start_new_program(CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::start_new_program);
  lval(o_lval("waiting_procs", 0x36C6008865C6AFB8LL)).append((v_bundle));
  LINE(347,t_check_capacity());
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 353 */
void c_asynctest::t_finish_test() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::finish_test);
  LINE(355,t_run_out_all_procs());
  LINE(356,c_test::t_finish_test());
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 359 */
void c_asynctest::t_run_out_all_procs() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::run_out_all_procs);
  LOOP_COUNTER(1);
  {
    while ((toBoolean(LINE(361,x_count(o_get("running_procs", 0x609786D2623AA565LL))))) || (toBoolean(x_count(o_get("waiting_procs", 0x36C6008865C6AFB8LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(363,x_usleep(toInt32(30000LL)));
        LINE(364,t_run_waiting_procs());
        LINE(365,t_check_running_procs());
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 369 */
void c_asynctest::t_check_capacity() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::check_capacity);
  if (not_less_rev((divide_rev(LINE(372,t_get_phc_num_procs_divisor()), t_get_num_procs())), LINE(371,x_count(o_get("running_procs", 0x609786D2623AA565LL))))) {
    LINE(375,t_check_running_procs());
  }
  if (more(LINE(378,x_count(o_get("waiting_procs", 0x36C6008865C6AFB8LL))), 1LL)) {
    LINE(381,t_run_waiting_procs());
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 388 */
void c_asynctest::t_check_running_procs() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::check_running_procs);
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_index = 0;
  Variant v_bundle;
  Array v_status;

  {
    LOOP_COUNTER(2);
    Variant map3 = o_get("running_procs", 0x609786D2623AA565LL);
    for (ArrayIterPtr iter4 = map3.begin("asynctest"); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_bundle = iter4->second();
      v_index = iter4->first();
      {
        (v_status = LINE(395,x_proc_get_status(toObject(v_bundle.o_get("handle", 0x48E8F48146EEEF5CLL)))));
        LINE(398,v_bundle.o_invoke_few_args("read_streams", 0x56AA34D1723D3920LL, 0));
        if (!same(v_status.rvalAt("running", 0x2947552314E5C43CLL), true)) {
          (v_bundle.o_lval("exit", 0x461F139A4051755ALL) = v_status.rvalAt("exitcode", 0x3FCBA2655EC7FA85LL));
          lval(o_lval("running_procs", 0x609786D2623AA565LL)).weakRemove(v_index);
          LINE(404,v_bundle.o_invoke_few_args("continuation", 0x61DD21ABF790E9E2LL, 0));
        }
        else if (!equal(o_get("max_time", 0x44D38A447EB55041LL), 0LL) && more(LINE(407,x_time()), v_bundle.o_get("start_time", 0x687BED2B0019D265LL) + o_get("max_time", 0x44D38A447EB55041LL))) {
          LINE(409,invoke_failed("kill_properly", Array(ArrayInit(2).set(0, ref(v_bundle.o_lval("handle", 0x48E8F48146EEEF5CLL))).set(1, ref(v_bundle.o_lval("pipes", 0x22E86BF7D2042689LL))).create()), 0x0000000092C10C2ELL));
          lval(v_bundle.o_lval("exits", 0x1AE9F4DC5F326DFFLL)).append(("Timeout"));
          concat_assign(v_bundle.o_lval("out", 0x7C801AC012E9F722LL), "\n--- TIMEOUT ---");
          lval(v_bundle.o_lval("outs", 0x3D74198A5F0F7DDCLL)).append((v_bundle.o_get("out", 0x7C801AC012E9F722LL)));
          lval(v_bundle.o_lval("errs", 0x15E266045DABA002LL)).append((v_bundle.o_get("err", 0x382AF294107C2C75LL)));
          lval(o_lval("running_procs", 0x609786D2623AA565LL)).weakRemove(v_index);
          LINE(416,t_async_timeout("Timeout", v_bundle));
        }
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 425 */
void c_asynctest::t_run_waiting_procs() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::run_waiting_procs);
  Variant v_bundle;

  LOOP_COUNTER(5);
  {
    while (less_rev((divide_rev(LINE(428,t_get_phc_num_procs_divisor()), t_get_num_procs())), LINE(427,x_count(o_get("running_procs", 0x609786D2623AA565LL))))) {
      LOOP_COUNTER_CHECK(5);
      {
        LINE(430,f_inst("Poll waiting"));
        if (equal(LINE(431,x_count(o_get("waiting_procs", 0x36C6008865C6AFB8LL))), 0LL)) {
          LINE(433,f_inst("No procs waiting"));
          break;
        }
        (v_bundle = LINE(437,x_array_shift(ref(lval(o_lval("waiting_procs", 0x36C6008865C6AFB8LL))))));
        LINE(438,t_run_program(v_bundle));
      }
    }
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 442 */
void c_asynctest::t_run_program(Variant v_bundle) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::run_program);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_command;
  Variant &gv_opt_verbose __attribute__((__unused__)) = g->GV(opt_verbose);
  Variant v_opt_verbose;
  Variant v_descriptorspec;
  Variant v_in;
  Variant v_pipes;
  Variant v_handle;

  (v_command = LINE(444,v_bundle.o_invoke_few_args("get_command", 0x1241B2341F67E88BLL, 0)));
  (v_command = toString("ulimit -v 262144 && ") + toString(v_command));
  v_opt_verbose = ref(g->GV(opt_verbose));
  if (toBoolean(v_opt_verbose)) print(LINE(448,concat3("Running command: ", toString(v_command), "\n")));
  LINE(450,f_inst(toString("Running prog: ") + toString(v_command)));
  (v_descriptorspec = ScalarArrays::sa_[6]);
  (v_in = LINE(456,v_bundle.o_invoke_few_args("get_in", 0x77F01963731361B5LL, 0)));
  if (toBoolean(v_in)) v_descriptorspec.set(0LL, (ScalarArrays::sa_[7]), 0x77CFA1EEF01BCA90LL);
  (v_pipes = ScalarArrays::sa_[3]);
  (v_handle = LINE(460,x_proc_open(toString(v_command), toArray(v_descriptorspec), ref(v_pipes))));
  LINE(461,x_stream_set_blocking(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(0LL)));
  LINE(462,x_stream_set_blocking(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)), toInt32(0LL)));
  if (toBoolean(v_in)) {
    LINE(465,x_fwrite(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_in)));
    LINE(466,x_fclose(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  }
  (v_bundle.o_lval("handle", 0x48E8F48146EEEF5CLL) = v_handle);
  (v_bundle.o_lval("pipes", 0x22E86BF7D2042689LL) = v_pipes);
  (v_bundle.o_lval("start_time", 0x687BED2B0019D265LL) = LINE(472,x_time()));
  (v_bundle.o_lval("out", 0x7C801AC012E9F722LL) = "");
  (v_bundle.o_lval("err", 0x382AF294107C2C75LL) = "");
  toObject(v_bundle)->t___unset("exit");
  lval(o_lval("running_procs", 0x609786D2623AA565LL)).append((v_bundle));
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 481 */
void c_asynctest::t_two_command_finish(Object v_async) {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::two_command_finish);
  Variant v_output;

  if (((!same(v_async.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_async.o_get("outs", 0x3D74198A5F0F7DDCLL).rvalAt(1LL, 0x5BCA7C69B794F8CELL))) || (!same(v_async.o_get("errs", 0x15E266045DABA002LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_async.o_get("errs", 0x15E266045DABA002LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL)))) || (!same(v_async.o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_async.o_get("exits", 0x1AE9F4DC5F326DFFLL).rvalAt(1LL, 0x5BCA7C69B794F8CELL)))) {
    (v_output = LINE(487,invoke_failed("diff", Array(ArrayInit(2).set(0, ref(lval(v_async.o_lval("outs", 0x3D74198A5F0F7DDCLL)).refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).set(1, ref(lval(v_async.o_lval("outs", 0x3D74198A5F0F7DDCLL)).refvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()), 0x00000000168FB640LL)));
    (v_async.o_lval("outs", 0x3D74198A5F0F7DDCLL) = v_output);
    LINE(489,t_async_failure("Outputs dont match", v_async));
  }
  else {
    LINE(493,t_async_success(v_async));
  }
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 499 */
int64 c_asynctest::t_get_phc_num_procs_divisor() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::get_phc_num_procs_divisor);
  return 1LL;
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 504 */
Variant c_asynctest::t_get_num_procs() {
  INSTANCE_METHOD_INJECTION(AsyncTest, AsyncTest::get_num_procs);
  return get_global_variables()->k_PHC_NUM_PROCS;
} /* function */
/* SRC: phc-test/framework/lib/async_test.php line 18 */
void f_inst(CStrRef v_string) {
  FUNCTION_INJECTION(inst);
} /* function */
Object co_asyncbundle(CArrRef params, bool init /* = true */) {
  return Object(p_asyncbundle(NEW(c_asyncbundle)())->dynCreate(params, init));
}
Object co_asynctest(CArrRef params, bool init /* = true */) {
  return Object(p_asynctest(NEW(c_asynctest)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$lib$async_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/lib/async_test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$lib$async_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(11,pm_php$phc_test$framework$lib$test_php(true, variables));
  if (isset(g->gv__ENV, "PHC_NUM_PROCS", 0x745039C1687577D6LL)) LINE(14,g->declareConstant("PHC_NUM_PROCS", g->k_PHC_NUM_PROCS, toInt64((g->gv__ENV.rvalAt("PHC_NUM_PROCS", 0x745039C1687577D6LL)))));
  else LINE(16,g->declareConstant("PHC_NUM_PROCS", g->k_PHC_NUM_PROCS, 1LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
