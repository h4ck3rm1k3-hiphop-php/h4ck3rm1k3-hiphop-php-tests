
#ifndef __GENERATED_cls_compiledvsinterpreted_h__
#define __GENERATED_cls_compiledvsinterpreted_h__

#include <cls/asynctest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/compiled_vs_interpreted.php line 11 */
class c_compiledvsinterpreted : virtual public c_asynctest {
  BEGIN_CLASS_MAP(compiledvsinterpreted)
    PARENT_CLASS(test)
    PARENT_CLASS(asynctest)
  END_CLASS_MAP(compiledvsinterpreted)
  DECLARE_CLASS(compiledvsinterpreted, CompiledVsInterpreted, asynctest)
  void init();
  public: bool t_check_prerequisites();
  public: Array t_get_dependent_test_names();
  public: Variant t_get_test_subjects();
  public: void t_finish(Variant v_async);
  public: Variant t_homogenize_output(Variant v_string, CVarRef v_bundle);
  public: Variant t_get_php_command(Variant v_subject);
  public: String t_get_phc_command(Variant v_subject, CVarRef v_exe_name);
  public: void t_run_test(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_compiledvsinterpreted_h__
