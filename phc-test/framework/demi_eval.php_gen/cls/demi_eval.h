
#ifndef __GENERATED_cls_demi_eval_h__
#define __GENERATED_cls_demi_eval_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: phc-test/framework/demi_eval.php line 13 */
class c_demi_eval : virtual public ObjectData {
  BEGIN_CLASS_MAP(demi_eval)
  END_CLASS_MAP(demi_eval)
  DECLARE_CLASS(demi_eval, Demi_eval, compiledvsinterpreted)
  void init();
  public: void t___construct(String v_init);
  public: ObjectData *create(String v_init);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Array t_get_dependent_test_names();
  public: String t_get_name();
  public: String t_get_php_command(Variant v_subject);
  public: String t_get_phc_command(Variant v_subject, CVarRef v_exe_name);
  public: Variant t_homogenize_output(Variant v_string, CVarRef v_bundle);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_demi_eval_h__
