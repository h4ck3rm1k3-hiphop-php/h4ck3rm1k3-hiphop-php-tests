
#include <php/phc-test/framework/demi_eval.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: phc-test/framework/demi_eval.php line 13 */
Variant c_demi_eval::os_get(const char *s, int64 hash) {
  return c_compiledvsinterpreted::os_get(s, hash);
}
Variant &c_demi_eval::os_lval(const char *s, int64 hash) {
  return c_compiledvsinterpreted::os_lval(s, hash);
}
void c_demi_eval::o_get(ArrayElementVec &props) const {
  c_compiledvsinterpreted::o_get(props);
}
bool c_demi_eval::o_exists(CStrRef s, int64 hash) const {
  return c_compiledvsinterpreted::o_exists(s, hash);
}
Variant c_demi_eval::o_get(CStrRef s, int64 hash) {
  return c_compiledvsinterpreted::o_get(s, hash);
}
Variant c_demi_eval::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_compiledvsinterpreted::o_set(s, hash, v, forInit);
}
Variant &c_demi_eval::o_lval(CStrRef s, int64 hash) {
  return c_compiledvsinterpreted::o_lval(s, hash);
}
Variant c_demi_eval::os_constant(const char *s) {
  return c_compiledvsinterpreted::os_constant(s);
}
IMPLEMENT_CLASS(demi_eval)
ObjectData *c_demi_eval::create(String v_init) {
  init();
  t___construct(v_init);
  return this;
}
ObjectData *c_demi_eval::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_demi_eval::cloneImpl() {
  c_demi_eval *obj = NEW(c_demi_eval)();
  cloneSet(obj);
  return obj;
}
void c_demi_eval::cloneSet(c_demi_eval *clone) {
  c_compiledvsinterpreted::cloneSet(clone);
}
Variant c_demi_eval::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_compiledvsinterpreted::o_invoke(s, params, hash, fatal);
}
Variant c_demi_eval::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_compiledvsinterpreted::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_demi_eval::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_compiledvsinterpreted::os_invoke(c, s, params, hash, fatal);
}
Variant cw_demi_eval$os_get(const char *s) {
  return c_demi_eval::os_get(s, -1);
}
Variant &cw_demi_eval$os_lval(const char *s) {
  return c_demi_eval::os_lval(s, -1);
}
Variant cw_demi_eval$os_constant(const char *s) {
  return c_demi_eval::os_constant(s);
}
Variant cw_demi_eval$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_demi_eval::os_invoke(c, s, params, -1, fatal);
}
void c_demi_eval::init() {
  c_compiledvsinterpreted::init();
}
/* SRC: phc-test/framework/demi_eval.php line 15 */
void c_demi_eval::t___construct(String v_init) {
  INSTANCE_METHOD_INJECTION(Demi_eval, Demi_eval::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("init", 0x0C337193CA26E208LL) = v_init);
  LINE(18,throw_fatal("unknown class compiledvsinterpreted", ((void*)NULL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: phc-test/framework/demi_eval.php line 21 */
Array c_demi_eval::t_get_dependent_test_names() {
  INSTANCE_METHOD_INJECTION(Demi_eval, Demi_eval::get_dependent_test_names);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: phc-test/framework/demi_eval.php line 26 */
String c_demi_eval::t_get_name() {
  INSTANCE_METHOD_INJECTION(Demi_eval, Demi_eval::get_name);
  Variant v_init;

  (v_init = o_get("init", 0x0C337193CA26E208LL));
  return toString("Demi_eval_") + toString(v_init);
} /* function */
/* SRC: phc-test/framework/demi_eval.php line 32 */
String c_demi_eval::t_get_php_command(Variant v_subject) {
  INSTANCE_METHOD_INJECTION(Demi_eval, Demi_eval::get_php_command);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_phc __attribute__((__unused__)) = g->GV(phc);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  Variant v_init;

  {
  }
  (v_init = o_get("init", 0x0C337193CA26E208LL));
  return concat_rev(LINE(36,(assignCallTemp(eo_1, toString(gv_plugin_dir)),assignCallTemp(eo_3, toString(v_subject)),assignCallTemp(eo_5, toString(invoke_failed("get_php_command_line", Array(ArrayInit(2).set(0, ref(v_subject)).set(1, "pipe").create()), 0x0000000033B2020BLL))),concat6("\" --convert-uppered --dump=", eo_1, "/tools/demi_eval.la ", eo_3, " | ", eo_5))), concat5(toString(gv_phc), " --run ", toString(gv_plugin_dir), "/tools/demi_eval.la --r-option=\"", toString(v_init)));
} /* function */
/* SRC: phc-test/framework/demi_eval.php line 39 */
String c_demi_eval::t_get_phc_command(Variant v_subject, CVarRef v_exe_name) {
  INSTANCE_METHOD_INJECTION(Demi_eval, Demi_eval::get_phc_command);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_plugin_dir __attribute__((__unused__)) = g->GV(plugin_dir);
  Variant v_init;

  (v_init = o_get("init", 0x0C337193CA26E208LL));
  return concat_rev(LINE(44,concat6(" -c --run ", toString(gv_plugin_dir), "/tools/demi_eval.la --r-option=\"", toString(v_init), "\" -o ", toString(v_exe_name))), toString(invoke_failed("get_phc_command_line", Array(ArrayInit(1).set(0, ref(v_subject)).create()), 0x0000000081EE2BC9LL)));
} /* function */
/* SRC: phc-test/framework/demi_eval.php line 47 */
Variant c_demi_eval::t_homogenize_output(Variant v_string, CVarRef v_bundle) {
  INSTANCE_METHOD_INJECTION(Demi_eval, Demi_eval::homogenize_output);
  (v_string = LINE(49,throw_fatal("unknown class compiledvsinterpreted", ((void*)NULL))));
  (v_string = LINE(50,x_preg_replace("/in __FILENAME__\\(\\d+\\) : eval\\(\\)'d code/m", "in eval'd code", v_string)));
  return v_string;
} /* function */
Object co_demi_eval(CArrRef params, bool init /* = true */) {
  return Object(p_demi_eval(NEW(c_demi_eval)())->dynCreate(params, init));
}
Variant pm_php$phc_test$framework$demi_eval_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::phc-test/framework/demi_eval.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$phc_test$framework$demi_eval_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tests") : g->GV(tests);

  LINE(11,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_demi_eval(p_demi_eval(NEWOBJ(c_demi_eval)())->create("true"))))),x_array_push(2, eo_0, eo_1)));
  LINE(12,(assignCallTemp(eo_0, ref(v_tests)),assignCallTemp(eo_1, ((Object)(p_demi_eval(p_demi_eval(NEWOBJ(c_demi_eval)())->create("false"))))),x_array_push(2, eo_0, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
