
#ifndef __GENERATED_php_phc_test_framework_demi_eval_h__
#define __GENERATED_php_phc_test_framework_demi_eval_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/phc-test/framework/demi_eval.fw.h>

// Declarations
#include <cls/demi_eval.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$phc_test$framework$demi_eval_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_demi_eval(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_phc_test_framework_demi_eval_h__
