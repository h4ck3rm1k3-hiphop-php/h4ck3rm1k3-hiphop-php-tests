
#include <php/roadsend/tests/bug-id-0000797.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000797.php line 3 */
Variant c_testclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_testclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_testclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_testclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_testclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_testclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_testclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_testclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(testclass)
ObjectData *c_testclass::cloneImpl() {
  c_testclass *obj = NEW(c_testclass)();
  cloneSet(obj);
  return obj;
}
void c_testclass::cloneSet(c_testclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_testclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_testclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_testclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_testclass$os_get(const char *s) {
  return c_testclass::os_get(s, -1);
}
Variant &cw_testclass$os_lval(const char *s) {
  return c_testclass::os_lval(s, -1);
}
Variant cw_testclass$os_constant(const char *s) {
  return c_testclass::os_constant(s);
}
Variant cw_testclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_testclass::os_invoke(c, s, params, -1, fatal);
}
void c_testclass::init() {
}
Object co_testclass(CArrRef params, bool init /* = true */) {
  return Object(p_testclass(NEW(c_testclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000797_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000797.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000797_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
