
#ifndef __GENERATED_php_roadsend_tests_bug_id_0000797_h__
#define __GENERATED_php_roadsend_tests_bug_id_0000797_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0000797.fw.h>

// Declarations
#include <cls/testclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0000797_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_testclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0000797_h__
