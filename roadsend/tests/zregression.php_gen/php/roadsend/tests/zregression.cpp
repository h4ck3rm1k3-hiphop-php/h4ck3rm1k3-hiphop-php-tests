
#include <php/roadsend/tests/zregression.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$zregression_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zregression.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zregression_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_wedding_timestamp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wedding_timestamp") : g->GV(wedding_timestamp);
  Variant &v_time_left __attribute__((__unused__)) = (variables != gVariables) ? variables->get("time_left") : g->GV(time_left);
  Variant &v_days __attribute__((__unused__)) = (variables != gVariables) ? variables->get("days") : g->GV(days);
  Variant &v_hours __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hours") : g->GV(hours);
  Variant &v_minutes __attribute__((__unused__)) = (variables != gVariables) ? variables->get("minutes") : g->GV(minutes);
  Variant &v_wedding_date __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wedding_date") : g->GV(wedding_date);

  LINE(3,include("zregression-1.inc", false, variables, "roadsend/tests/"));
  (v_wedding_timestamp = LINE(5,x_mktime(toInt32(20LL), toInt32(0LL), toInt32(0LL), toInt32(8LL), toInt32(31LL), toInt32(1997LL))));
  (v_time_left = v_wedding_timestamp - LINE(6,x_time()));
  if (more(v_time_left, 0LL)) {
    (v_days = divide(v_time_left, 86400LL));
    v_time_left -= v_days * 86400LL;
    (v_hours = divide(v_time_left, 3600LL));
    v_time_left -= v_hours * 3600LL;
    (v_minutes = divide(v_time_left, 60LL));
    echo(LINE(14,(assignCallTemp(eo_1, ((v_wedding_date = x_date("l, F dS, Y", toInt64(v_wedding_timestamp))))),assignCallTemp(eo_2, concat(",\nwhich is ", concat6(toString(v_days), " days, ", toString(v_hours), " hours and ", toString(v_minutes), " minutes from now.\n"))),concat3("Limor Ullmann is getting married on ", eo_1, eo_2))));
    echo(LINE(15,concat3("Her hashed wedding date is ", toString(v_wedding_date), ".\n")));
  }
  else {
    echo("Limor Ullmann is now Limor Baruch :I\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
