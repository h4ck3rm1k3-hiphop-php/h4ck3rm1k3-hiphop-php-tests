
#ifndef __GENERATED_php_roadsend_tests_bug_id_0003090_h__
#define __GENERATED_php_roadsend_tests_bug_id_0003090_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0003090.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_write();
Variant pm_php$roadsend$tests$bug_id_0003090_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0003090_h__
