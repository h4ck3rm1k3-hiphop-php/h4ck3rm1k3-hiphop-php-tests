
#ifndef __GENERATED_php_roadsend_tests_unserialize_error_h__
#define __GENERATED_php_roadsend_tests_unserialize_error_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/unserialize-error.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_maybe_unserialize(CStrRef v_original);
Variant pm_php$roadsend$tests$unserialize_error_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_unserialize_error_h__
