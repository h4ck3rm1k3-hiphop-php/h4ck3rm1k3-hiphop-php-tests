
#include <php/roadsend/tests/bug-id-0001339.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001339.php line 3 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("avar", m_avar.isReferenced() ? ref(m_avar) : m_avar));
  props.push_back(NEW(ArrayElement)("bvar", m_bvar.isReferenced() ? ref(m_bvar) : m_bvar));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x2A6558C007B3B00ALL, avar, 4);
      HASH_EXISTS_STRING(0x22A5206FE17C894ELL, bvar, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x2A6558C007B3B00ALL, m_avar,
                         avar, 4);
      HASH_RETURN_STRING(0x22A5206FE17C894ELL, m_bvar,
                         bvar, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x2A6558C007B3B00ALL, m_avar,
                      avar, 4);
      HASH_SET_STRING(0x22A5206FE17C894ELL, m_bvar,
                      bvar, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x2A6558C007B3B00ALL, m_avar,
                         avar, 4);
      HASH_RETURN_STRING(0x22A5206FE17C894ELL, m_bvar,
                         bvar, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_avar = m_avar.isReferenced() ? ref(m_avar) : m_avar;
  clone->m_bvar = m_bvar.isReferenced() ? ref(m_bvar) : m_bvar;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return (t_afunc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return (t_afunc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_avar = null;
  m_bvar = null;
}
/* SRC: roadsend/tests/bug-id-0001339.php line 8 */
void c_aclass::t_afunc() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::afunc);
  Array v_a;
  Primitive v_k = 0;
  Variant v_v;

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_a.begin("aclass"); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      m_avar = iter3.second();
      echo(LINE(12,concat3("working with ", toString(m_avar), "\n")));
    }
  }
  {
    LOOP_COUNTER(4);
    for (ArrayIter iter6 = v_a.begin("aclass"); !iter6.end(); ++iter6) {
      LOOP_COUNTER_CHECK(4);
      m_avar = iter6.second();
      v_k = iter6.first();
      echo(LINE(15,concat5("working with ", toString(v_k), " and ", toString(m_avar), "\n")));
    }
  }
  {
    LOOP_COUNTER(7);
    for (ArrayIter iter9 = v_a.begin("aclass"); !iter9.end(); ++iter9) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9.second();
      m_avar = iter9.first();
      echo(LINE(18,concat5("working with ", toString(v_v), " and ", toString(m_avar), "\n")));
    }
  }
  {
    LOOP_COUNTER(10);
    for (ArrayIter iter12 = v_a.begin("aclass"); !iter12.end(); ++iter12) {
      LOOP_COUNTER_CHECK(10);
      m_bvar = iter12.second();
      m_avar = iter12.first();
      echo(LINE(21,concat5("working with ", toString(m_bvar), " and ", toString(m_avar), "\n")));
    }
  }
  LINE(23,x_var_dump(1, m_avar));
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0001339_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001339.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001339_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = ((Object)(LINE(29,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  LINE(30,v_a.o_invoke_few_args("afunc", 0x285048A47E9CBC29LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
