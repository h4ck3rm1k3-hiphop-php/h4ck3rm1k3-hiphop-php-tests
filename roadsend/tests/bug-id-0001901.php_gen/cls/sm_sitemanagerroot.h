
#ifndef __GENERATED_cls_sm_sitemanagerroot_h__
#define __GENERATED_cls_sm_sitemanagerroot_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0001901.php line 77 */
class c_sm_sitemanagerroot : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_sitemanagerroot)
  END_CLASS_MAP(sm_sitemanagerroot)
  DECLARE_CLASS(sm_sitemanagerroot, SM_siteManagerRoot, ObjectData)
  void init();
  public: Array m_templateCache;
  public: Variant t_loadtemplate(CVarRef v_fName, CVarRef v_fatalFNF = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_sitemanagerroot_h__
