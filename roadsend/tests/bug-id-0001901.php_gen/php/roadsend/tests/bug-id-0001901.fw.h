
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001901_fw_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001901_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_SM_TAG_CLASS;
extern const StaticString k_SM_AREAID_KEY;
extern const StaticString k_SM_TAG_PREGEXP;
extern const StaticString k_SM_TAG_ATTR_PREGEXP;
extern const StaticString k_SM_TAG_IDENTIFIER;


// 2. Classes
FORWARD_DECLARE_CLASS(sm_layouttemplate)
FORWARD_DECLARE_CLASS(sm_smtag_area)
FORWARD_DECLARE_CLASS(sm_sitemanagerroot)
FORWARD_DECLARE_CLASS(sm_smtag)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001901_fw_h__
