
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$tests$main_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$tests$inc1_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$tests$inc2_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_INCLUDE(0x1A4C43FEC6602090LL, "roadsend/tests/main.php", php$roadsend$tests$main_php);
      break;
    case 1:
      HASH_INCLUDE(0x1BDE506156E54821LL, "roadsend/tests/inc1.php", php$roadsend$tests$inc1_php);
      break;
    case 2:
      HASH_INCLUDE(0x73E80FF7B7BEDF2ALL, "roadsend/tests/inc2.php", php$roadsend$tests$inc2_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
