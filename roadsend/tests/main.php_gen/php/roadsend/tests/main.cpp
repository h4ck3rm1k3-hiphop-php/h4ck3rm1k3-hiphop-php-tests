
#include <php/roadsend/tests/inc1.h>
#include <php/roadsend/tests/inc2.h>
#include <php/roadsend/tests/main.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$main_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/main.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$main_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("starting main.php ...\n");
  LINE(12,pm_php$roadsend$tests$inc1_php(false, variables));
  LINE(13,pm_php$roadsend$tests$inc2_php(false, variables));
  LINE(15,f_inc1_function("foo", "bar"));
  LINE(16,f_inc2_function("baz", "bif"));
  echo("end of program\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
