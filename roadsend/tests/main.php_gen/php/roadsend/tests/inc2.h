
#ifndef __GENERATED_php_roadsend_tests_inc2_h__
#define __GENERATED_php_roadsend_tests_inc2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc2_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_inc2_function(CStrRef v_a, CStrRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc2_h__
