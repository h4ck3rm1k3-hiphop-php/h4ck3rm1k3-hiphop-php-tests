
#include <php/roadsend/tests/inc1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/inc1.php line 12 */
bool f_inc1_function(CStrRef v_a, CStrRef v_b) {
  FUNCTION_INJECTION(inc1_function);
  echo(LINE(14,concat5("calling inc1_function: ", v_a, ", ", v_b, "\n")));
  return true;
} /* function */
Variant pm_php$roadsend$tests$inc1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/inc1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$inc1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(":: including inc1.php\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
