
#include <php/roadsend/tests/commit-rollback.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$commit_rollback_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/commit-rollback.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$commit_rollback_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directdsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directdsn") : g->GV(directdsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_rr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rr") : g->GV(rr);

  LINE(3,include("connect.inc", false, variables, "roadsend/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_directdsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_rh = LINE(14,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "CREATE TABLE IF NOT EXISTS innotable ( idx INT UNSIGNED NOT NULL ) TYPE=InnoDB").create()), 0x00000000B7FC229FLL)));
  if (equal(v_rh, null)) {
    echo(toString(LINE(16,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_rh = LINE(20,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "INSERT INTO innotable SET idx=300").create()), 0x00000000B7FC229FLL)));
  if (equal(v_rh, null)) {
    echo(toString(LINE(22,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  LINE(26,invoke_failed("odbc_autocommit", Array(ArrayInit(2).set(0, ref(v_r)).set(1, false).create()), 0x00000000ED7EE5AALL));
  (v_rh = LINE(29,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "INSERT INTO innotable SET idx=500").create()), 0x00000000B7FC229FLL)));
  if (equal(v_rh, null)) {
    echo(toString(LINE(31,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  LINE(35,invoke_failed("odbc_rollback", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000B9279635LL));
  (v_rh = LINE(37,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "SELECT * FROM innotable").create()), 0x00000000B7FC229FLL)));
  if (equal(v_rh, null)) {
    echo(toString(LINE(39,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_rr = LINE(44,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(45,x_var_dump(1, v_rr));
      }
    }
  }
  (v_rh = LINE(48,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "INSERT INTO innotable SET idx=700").create()), 0x00000000B7FC229FLL)));
  if (equal(v_rh, null)) {
    echo(toString(LINE(50,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  LINE(54,invoke_failed("odbc_commit", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000001A7B6B1FLL));
  (v_rh = LINE(56,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "SELECT * FROM innotable").create()), 0x00000000B7FC229FLL)));
  if (equal(v_rh, null)) {
    echo(toString(LINE(58,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  LOOP_COUNTER(2);
  {
    while (toBoolean((v_rr = LINE(63,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
      LOOP_COUNTER_CHECK(2);
      {
        LINE(64,x_var_dump(1, v_rr));
      }
    }
  }
  (v_rh = LINE(67,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "DROP TABLE innotable").create()), 0x00000000B7FC229FLL)));
  if (equal(v_rh, null)) {
    echo(toString(LINE(69,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  echo(toString(LINE(74,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
