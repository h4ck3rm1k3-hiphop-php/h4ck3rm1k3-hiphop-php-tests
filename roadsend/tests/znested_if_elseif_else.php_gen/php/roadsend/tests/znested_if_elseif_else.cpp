
#include <php/roadsend/tests/znested_if_elseif_else.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$znested_if_elseif_else_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/znested_if_elseif_else.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$znested_if_elseif_else_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = 1LL);
  (v_b = 2LL);
  if (equal(v_a, 0LL)) {
    echo("bad");
  }
  else if (equal(v_a, 3LL)) {
    echo("bad");
  }
  else {
    if (equal(v_b, 1LL)) {
      echo("bad");
    }
    else if (equal(v_b, 2LL)) {
      echo("good");
    }
    else {
      echo("bad");
    }
  }
  echo("\t\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
