
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_INITIALIZED(0x35D32B9EF30B4340LL, g->GV(fruits),
                       fruits);
      break;
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 4:
      HASH_INITIALIZED(0x7A452383AA9BF7C4LL, g->GV(d),
                       d);
      break;
    case 5:
      HASH_INITIALIZED(0x6BFDB7C612C42345LL, g->GV(input_array),
                       input_array);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      HASH_INITIALIZED(0x7BEC6CE58E54F3C6LL, g->GV(trans),
                       trans);
      break;
    case 8:
      HASH_INITIALIZED(0x599E3F67E2599248LL, g->GV(result),
                       result);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 10:
      HASH_INITIALIZED(0x4292CEE227B9150ALL, g->GV(a),
                       a);
      break;
    case 11:
      HASH_INITIALIZED(0x1532499416EFFACBLL, g->GV(input),
                       input);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 21:
      HASH_INITIALIZED(0x08FBB133F8576BD5LL, g->GV(b),
                       b);
      break;
    case 22:
      HASH_INITIALIZED(0x04BFC205E59FA416LL, g->GV(x),
                       x);
      break;
    case 34:
      HASH_INITIALIZED(0x6E206FCF6CDA2862LL, g->GV(ar1),
                       ar1);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 43:
      HASH_INITIALIZED(0x5084E637B870262BLL, g->GV(stack),
                       stack);
      break;
    case 45:
      HASH_INITIALIZED(0x4DCAAC426B2BACADLL, g->GV(testarr),
                       testarr);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 47:
      HASH_INITIALIZED(0x037F1A734108016FLL, g->GV(cbobj),
                       cbobj);
      break;
    case 48:
      HASH_INITIALIZED(0x32C769EE5C5509B0LL, g->GV(c),
                       c);
      break;
    case 53:
      HASH_INITIALIZED(0x331353645577AFF5LL, g->GV(array1),
                       array1);
      break;
    case 56:
      HASH_INITIALIZED(0x021391DB4FD3F338LL, g->GV(ar2),
                       ar2);
      break;
    case 57:
      HASH_INITIALIZED(0x3D4B408A972F19F9LL, g->GV(array2),
                       array2);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
