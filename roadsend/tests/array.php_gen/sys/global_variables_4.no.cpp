
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "testarr",
    "input_array",
    "array1",
    "array2",
    "result",
    "cbobj",
    "trans",
    "a",
    "b",
    "c",
    "d",
    "ar1",
    "ar2",
    "fruits",
    "input",
    "stack",
    "x",
  };
  if (idx >= 0 && idx < 29) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(testarr);
    case 13: return GV(input_array);
    case 14: return GV(array1);
    case 15: return GV(array2);
    case 16: return GV(result);
    case 17: return GV(cbobj);
    case 18: return GV(trans);
    case 19: return GV(a);
    case 20: return GV(b);
    case 21: return GV(c);
    case 22: return GV(d);
    case 23: return GV(ar1);
    case 24: return GV(ar2);
    case 25: return GV(fruits);
    case 26: return GV(input);
    case 27: return GV(stack);
    case 28: return GV(x);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
