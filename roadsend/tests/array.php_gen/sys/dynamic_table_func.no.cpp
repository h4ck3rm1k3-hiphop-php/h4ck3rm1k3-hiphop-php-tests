
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_test_alter(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_even(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_test_print(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_rmul(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_show_spanish(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_cube(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_map_spanish(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_rsum(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_odd(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[32])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 32; i++) funcTable[i] = &invoke_builtin;
    funcTable[4] = &i_test_alter;
    funcTable[5] = &i_even;
    funcTable[6] = &i_test_print;
    funcTable[9] = &i_rmul;
    funcTable[12] = &i_show_spanish;
    funcTable[19] = &i_cube;
    funcTable[23] = &i_map_spanish;
    funcTable[24] = &i_rsum;
    funcTable[30] = &i_odd;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 31](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
