
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "cbclass", "roadsend/tests/array.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "cube", "roadsend/tests/array.php",
  "even", "roadsend/tests/array.php",
  "map_spanish", "roadsend/tests/array.php",
  "odd", "roadsend/tests/array.php",
  "rmul", "roadsend/tests/array.php",
  "rsum", "roadsend/tests/array.php",
  "show_spanish", "roadsend/tests/array.php",
  "test_alter", "roadsend/tests/array.php",
  "test_print", "roadsend/tests/array.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
