
#include <php/roadsend/tests/array.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/array.php line 85 */
Variant c_cbclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_cbclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_cbclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_cbclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_cbclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_cbclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_cbclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_cbclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(cbclass)
ObjectData *c_cbclass::cloneImpl() {
  c_cbclass *obj = NEW(c_cbclass)();
  cloneSet(obj);
  return obj;
}
void c_cbclass::cloneSet(c_cbclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_cbclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x292896FC8DC71247LL, cbmethod) {
        return (t_cbmethod(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_cbclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x292896FC8DC71247LL, cbmethod) {
        return (t_cbmethod(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_cbclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_cbclass$os_get(const char *s) {
  return c_cbclass::os_get(s, -1);
}
Variant &cw_cbclass$os_lval(const char *s) {
  return c_cbclass::os_lval(s, -1);
}
Variant cw_cbclass$os_constant(const char *s) {
  return c_cbclass::os_constant(s);
}
Variant cw_cbclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_cbclass::os_invoke(c, s, params, -1, fatal);
}
void c_cbclass::init() {
}
/* SRC: roadsend/tests/array.php line 86 */
Variant c_cbclass::t_cbmethod(CVarRef v_o) {
  INSTANCE_METHOD_INJECTION(cbclass, cbclass::cbmethod);
  if (more(v_o, 5LL)) {
    return false;
  }
  else {
    return true;
  }
  return null;
} /* function */
/* SRC: roadsend/tests/array.php line 18 */
Array f_map_spanish(CVarRef v_n, CVarRef v_m) {
  FUNCTION_INJECTION(map_Spanish);
  return Array(ArrayInit(1).set(0, v_n, v_m).create());
} /* function */
/* SRC: roadsend/tests/array.php line 22 */
void f_test_alter(Variant v_item1, CVarRef v_key, CVarRef v_prefix) {
  FUNCTION_INJECTION(test_alter);
  (v_item1 = LINE(23,concat3(toString(v_prefix), ": ", toString(v_item1))));
} /* function */
/* SRC: roadsend/tests/array.php line 242 */
Variant f_rsum(Variant v_v, CVarRef v_w) {
  FUNCTION_INJECTION(rsum);
  v_v += v_w;
  return v_v;
} /* function */
/* SRC: roadsend/tests/array.php line 10 */
Numeric f_cube(CVarRef v_n) {
  FUNCTION_INJECTION(cube);
  return v_n * v_n * v_n;
} /* function */
/* SRC: roadsend/tests/array.php line 26 */
void f_test_print(CVarRef v_item2, CVarRef v_key) {
  FUNCTION_INJECTION(test_print);
  echo(LINE(27,concat4(toString(v_key), ". ", toString(v_item2), "<br>\n")));
} /* function */
/* SRC: roadsend/tests/array.php line 2 */
bool f_odd(CVarRef v_var) {
  FUNCTION_INJECTION(odd);
  return (equal(modulo(toInt64(v_var), 2LL), 1LL));
} /* function */
/* SRC: roadsend/tests/array.php line 248 */
Variant f_rmul(Variant v_v, CVarRef v_w) {
  FUNCTION_INJECTION(rmul);
  v_v *= v_w;
  return v_v;
} /* function */
/* SRC: roadsend/tests/array.php line 14 */
String f_show_spanish(CVarRef v_n, CVarRef v_m) {
  FUNCTION_INJECTION(show_Spanish);
  return LINE(15,concat5("The number ", toString(v_n), " is called ", toString(v_m), " in Spanish"));
} /* function */
/* SRC: roadsend/tests/array.php line 6 */
bool f_even(CVarRef v_var) {
  FUNCTION_INJECTION(even);
  return (equal(modulo(toInt64(v_var), 2LL), 0LL));
} /* function */
Variant i_map_spanish(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x595DC8ACE1880237LL, map_spanish) {
    return (f_map_spanish(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_test_alter(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x076B6566C348D484LL, test_alter) {
    return (f_test_alter(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_rsum(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x13B3072E67963598LL, rsum) {
    return (f_rsum(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_cube(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x65610738EDAFBC53LL, cube) {
    return (f_cube(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_test_print(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2A3E74766FA74DA6LL, test_print) {
    return (f_test_print(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_odd(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7EC3DC2632C064BELL, odd) {
    return (f_odd(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_rmul(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4D2300718FD2DE49LL, rmul) {
    return (f_rmul(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_show_spanish(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0BA50364B4C08ECCLL, show_spanish) {
    return (f_show_spanish(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_even(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x464E1FDC15C86025LL, even) {
    return (f_even(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_cbclass(CArrRef params, bool init /* = true */) {
  return Object(p_cbclass(NEW(c_cbclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$array_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/array.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$array_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_testarr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testarr") : g->GV(testarr);
  Variant &v_input_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input_array") : g->GV(input_array);
  Variant &v_array1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array1") : g->GV(array1);
  Variant &v_array2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array2") : g->GV(array2);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_cbobj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cbobj") : g->GV(cbobj);
  Variant &v_trans __attribute__((__unused__)) = (variables != gVariables) ? variables->get("trans") : g->GV(trans);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_ar1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ar1") : g->GV(ar1);
  Variant &v_ar2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ar2") : g->GV(ar2);
  Variant &v_fruits __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fruits") : g->GV(fruits);
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_stack __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stack") : g->GV(stack);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);

  (v_testarr = ScalarArrays::sa_[0]);
  print("array_change_key_case:\n");
  LINE(38,x_print_r(v_testarr));
  LINE(39,x_print_r(x_array_change_key_case(v_testarr, toBoolean(1LL) /* CASE_UPPER */)));
  LINE(40,x_print_r(v_testarr));
  LINE(41,x_print_r(x_array_change_key_case(v_testarr)));
  LINE(42,x_print_r(v_testarr));
  print("array_chunk:\n");
  (v_input_array = ScalarArrays::sa_[1]);
  LINE(47,x_print_r(x_array_chunk(v_input_array, toInt32(2LL))));
  LINE(48,x_print_r(x_array_chunk(v_input_array, toInt32(3LL), true)));
  LINE(49,x_print_r(x_array_chunk(v_testarr, toInt32(2LL))));
  LINE(50,x_print_r(x_array_chunk(v_testarr, toInt32(2LL), true)));
  print("array_count_values:\n");
  LINE(55,x_print_r(x_array_count_values(v_testarr)));
  LINE(56,x_print_r(x_array_count_values(v_input_array)));
  print("array_diff:\n");
  (v_array1 = ScalarArrays::sa_[2]);
  (v_array2 = ScalarArrays::sa_[3]);
  LINE(62,x_print_r(x_array_diff(2, v_array1, v_array2)));
  LINE(63,x_print_r(x_array_diff(2, v_testarr, v_input_array)));
  print("array_diff_assoc:\n");
  (v_array1 = ScalarArrays::sa_[4]);
  (v_array2 = ScalarArrays::sa_[5]);
  (v_result = LINE(69,x_array_diff_assoc(2, v_array1, v_array2)));
  LINE(70,x_print_r(v_result));
  LINE(71,x_print_r(x_array_diff_assoc(2, v_testarr, v_input_array)));
  print("array_filter:\n");
  (v_array1 = ScalarArrays::sa_[6]);
  (v_array2 = ScalarArrays::sa_[7]);
  echo("Odd :\n");
  LINE(81,x_print_r(x_array_filter(v_array1, "odd")));
  echo("Even:\n");
  LINE(83,x_print_r(x_array_filter(v_array2, "even")));
  LINE(95,x_print_r(x_array_filter(v_array2, ScalarArrays::sa_[8])));
  (v_cbobj = ((Object)(LINE(96,p_cbclass(p_cbclass(NEWOBJ(c_cbclass)())->create())))));
  LINE(97,x_print_r(x_array_filter(v_array2, Array(ArrayInit(2).set(0, v_cbobj).set(1, "cbmethod").create()))));
  print("array_flip:\n");
  (v_trans = ScalarArrays::sa_[9]);
  LINE(101,x_print_r(x_array_flip(v_trans)));
  LINE(102,x_print_r(x_array_flip(v_testarr)));
  print("array_fill:\n");
  LINE(106,x_print_r(x_array_fill(toInt32(5LL), toInt32(6LL), "bicycle")));
  print("array_intersect:\n");
  (v_array1 = ScalarArrays::sa_[2]);
  (v_array2 = ScalarArrays::sa_[3]);
  LINE(112,x_print_r(x_array_intersect(2, v_array1, v_array2)));
  LINE(113,x_print_r(x_array_intersect(2, v_testarr, v_input_array)));
  LINE(115,x_print_r(v_testarr));
  print("array_key_exists:\n");
  if (LINE(117,x_array_key_exists("b", v_testarr))) {
    print("key \"b\" exists\n");
  }
  else {
    print("key \"b\" doesn't exist\n");
  }
  if (LINE(123,x_array_key_exists("grain", v_testarr))) {
    print("key \"grain\" exists\n");
  }
  else {
    print("key \"grain\" doesn't exist\n");
  }
  print("array_keys:\n");
  LINE(132,x_print_r(x_array_keys(v_testarr)));
  LINE(133,x_print_r(x_array_keys(v_input_array)));
  LINE(134,x_print_r(x_array_keys(v_array1, "red")));
  print("array_map:\n");
  (v_a = ScalarArrays::sa_[10]);
  (v_b = LINE(140,x_array_map(2, "cube", v_a)));
  LINE(141,x_print_r(v_b));
  (v_a = ScalarArrays::sa_[11]);
  (v_b = ScalarArrays::sa_[12]);
  (v_c = LINE(148,x_array_map(3, "show_Spanish", v_a, Array(ArrayInit(1).set(0, v_b).create()))));
  LINE(149,x_print_r(v_c));
  (v_d = LINE(151,x_array_map(3, "map_Spanish", v_a, Array(ArrayInit(1).set(0, v_b).create()))));
  LINE(152,x_print_r(v_d));
  print("array_merge:\n");
  (v_array1 = ScalarArrays::sa_[13]);
  (v_array2 = ScalarArrays::sa_[14]);
  LINE(159,x_print_r(x_array_merge(2, v_array1, Array(ArrayInit(1).set(0, v_array2).create()))));
  LINE(160,x_print_r(x_array_merge(3, v_testarr, Array(ArrayInit(2).set(0, v_array1).set(1, v_array2).create()))));
  (v_a = ScalarArrays::sa_[15]);
  (v_b = 4LL);
  (v_c = LINE(165,x_array_merge(2, v_a, Array(ArrayInit(1).set(0, v_b).create()))));
  LINE(166,x_var_dump(1, v_c));
  print("array_merge_recursive:\n");
  (v_ar1 = ScalarArrays::sa_[16]);
  (v_ar2 = ScalarArrays::sa_[17]);
  LINE(171,x_print_r(x_array_merge_recursive(2, v_ar1, Array(ArrayInit(1).set(0, v_ar2).create()))));
  LINE(172,x_print_r(x_array_merge_recursive(4, v_testarr, Array(ArrayInit(3).set(0, v_input_array).set(1, v_ar1).set(2, ScalarArrays::sa_[18]).create()))));
  print("array_walk:\n");
  (v_fruits = ScalarArrays::sa_[19]);
  echo("Before ...:\n");
  LINE(185,x_array_walk(ref(v_fruits), "test_print"));
  LINE(186,x_reset(ref(v_fruits)));
  LINE(187,x_array_walk(ref(v_fruits), "test_alter", "fruit"));
  echo("... and after:\n");
  LINE(189,x_reset(ref(v_fruits)));
  LINE(190,x_array_walk(ref(v_fruits), "test_print"));
  (v_input = ScalarArrays::sa_[20]);
  (v_result = LINE(196,x_array_pad(v_input, toInt32(5LL), 0LL)));
  LINE(198,x_var_dump(1, v_result));
  (v_result = LINE(200,x_array_pad(v_input, toInt32(-7LL), -1LL)));
  LINE(202,x_var_dump(1, v_result));
  (v_result = LINE(204,x_array_pad(v_input, toInt32(2LL), "noop")));
  LINE(205,x_var_dump(1, v_result));
  (v_stack = ScalarArrays::sa_[21]);
  LINE(209,x_array_push(3, ref(v_stack), "apple", ScalarArrays::sa_[22]));
  LINE(210,x_var_dump(1, v_stack));
  (v_a = ScalarArrays::sa_[23]);
  echo(LINE(214,(assignCallTemp(eo_1, toString(x_array_sum(v_a))),concat3("sum(a) = ", eo_1, "\n"))));
  (v_b = ScalarArrays::sa_[24]);
  echo(LINE(217,(assignCallTemp(eo_1, toString(x_array_sum(v_b))),concat3("sum(b) = ", eo_1, "\n"))));
  (v_b = ScalarArrays::sa_[25]);
  echo(LINE(220,(assignCallTemp(eo_1, toString(x_array_sum(v_b))),concat3("sum(c) = ", eo_1, "\n"))));
  (v_a = ScalarArrays::sa_[11]);
  (v_x = ScalarArrays::sa_[26]);
  echo(concat(toString(LINE(256,x_array_reduce(v_a, "rsum"))), "\n"));
  echo(concat(toString(LINE(257,x_array_reduce(v_a, "rmul", 10LL))), "\n"));
  echo(concat(toString(LINE(258,x_array_reduce(v_x, "rsum", 1LL))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
