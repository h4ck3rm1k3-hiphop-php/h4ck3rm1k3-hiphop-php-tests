
#ifndef __GENERATED_php_roadsend_tests_array_h__
#define __GENERATED_php_roadsend_tests_array_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/array.fw.h>

// Declarations
#include <cls/cbclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Array f_map_spanish(CVarRef v_n, CVarRef v_m);
void f_test_alter(Variant v_item1, CVarRef v_key, CVarRef v_prefix);
Variant f_rsum(Variant v_v, CVarRef v_w);
Numeric f_cube(CVarRef v_n);
Variant pm_php$roadsend$tests$array_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_test_print(CVarRef v_item2, CVarRef v_key);
bool f_odd(CVarRef v_var);
Variant f_rmul(Variant v_v, CVarRef v_w);
String f_show_spanish(CVarRef v_n, CVarRef v_m);
bool f_even(CVarRef v_var);
Object co_cbclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_array_h__
