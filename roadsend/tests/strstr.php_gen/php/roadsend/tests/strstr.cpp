
#include <php/roadsend/tests/strstr.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$strstr_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/strstr.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$strstr_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_email __attribute__((__unused__)) = (variables != gVariables) ? variables->get("email") : g->GV(email);

  (v_email = "user@example.com");
  print(toString(LINE(6,x_strstr(toString(v_email), "@"))));
  print(toString(LINE(7,x_strstr(concat(toString(v_email), "@example.com"), "@"))));
  print(toString(LINE(8,x_strstr(toString(v_email), "---"))));
  print(toString(LINE(9,x_strstr(toString(v_email), "USER"))));
  print(toString(LINE(10,x_strstr(toString(v_email), "cOm"))));
  print(toString(LINE(11,x_strstr(toString(v_email), "amp"))));
  print(toString(LINE(13,x_strchr(toString(v_email), "cOm"))));
  print(toString(LINE(14,x_strchr(toString(v_email), "amp"))));
  (v_email = "user@example.com");
  print(toString(LINE(18,x_stristr(toString(v_email), "@"))));
  print(toString(LINE(19,x_stristr(concat(toString(v_email), "@example.com"), "@"))));
  print(toString(LINE(20,x_stristr(toString(v_email), "---"))));
  print(toString(LINE(21,x_stristr(toString(v_email), "USER"))));
  print(toString(LINE(22,x_stristr(toString(v_email), "cOm"))));
  print(toString(LINE(23,x_stristr(toString(v_email), "amp"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
