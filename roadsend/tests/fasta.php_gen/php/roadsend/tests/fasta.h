
#ifndef __GENERATED_php_roadsend_tests_fasta_h__
#define __GENERATED_php_roadsend_tests_fasta_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/fasta.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_makecumulative(Variant v_genelist);
Variant f_selectrandom(Variant v_a);
Numeric f_gen_random(int64 v_max);
Variant pm_php$roadsend$tests$fasta_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_makerepeatfasta(CStrRef v_id, CStrRef v_desc, CVarRef v_s, Numeric v_n);
void f_makerandomfasta(CStrRef v_id, CStrRef v_desc, Variant v_genelist, Numeric v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_fasta_h__
