
#include <php/roadsend/tests/bug-id-0000727.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000727.php line 9 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("directive", m_directive));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x26998912FB5DF57ALL, directive, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x26998912FB5DF57ALL, m_directive,
                         directive, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x26998912FB5DF57ALL, m_directive,
                      directive, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_directive = m_directive;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x522D90ED0F9A03D1LL, makeone) {
        return (t_makeone(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x522D90ED0F9A03D1LL, makeone) {
        return (t_makeone(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_directive = ScalarArrays::sa_[0];
}
/* SRC: roadsend/tests/bug-id-0000727.php line 12 */
void c_foo::t_makeone() {
  INSTANCE_METHOD_INJECTION(foo, foo::makeOne);
  Variant v_columnDef;

  (v_columnDef = LINE(14,create_object((toString(m_directive.rvalAt("columnClass", 0x7682E26B671E6C89LL))), Array())));
  LINE(15,v_columnDef.o_invoke_few_args("zot", 0x05A26B348541B72DLL, 0));
} /* function */
/* SRC: roadsend/tests/bug-id-0000727.php line 19 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x05A26B348541B72DLL, zot) {
        return (t_zot(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x05A26B348541B72DLL, zot) {
        return (t_zot(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
}
/* SRC: roadsend/tests/bug-id-0000727.php line 20 */
void c_bar::t_zot() {
  INSTANCE_METHOD_INJECTION(bar, bar::zot);
  echo("they've spotted us\n");
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000727_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000727.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000727_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_afoo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("afoo") : g->GV(afoo);

  echo("\n0000727 \n\n\ninstantiate a class based on a classname from an array\n\n");
  (v_afoo = ((Object)(LINE(25,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(26,v_afoo.o_invoke_few_args("makeOne", 0x522D90ED0F9A03D1LL, 0));
  echo(" \t\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
