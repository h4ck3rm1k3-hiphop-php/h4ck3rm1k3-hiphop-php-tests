
#include <php/roadsend/tests/misc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$misc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/misc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$misc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_item __attribute__((__unused__)) = (variables != gVariables) ? variables->get("item") : g->GV(item);
  Variant &v_coded __attribute__((__unused__)) = (variables != gVariables) ? variables->get("coded") : g->GV(coded);
  Variant &v_decoded __attribute__((__unused__)) = (variables != gVariables) ? variables->get("decoded") : g->GV(decoded);

  (v_str = "foo ' '' \\' \\ \" \\\"");
  echo(concat(toString(LINE(5,invoke_failed("sqlite_escape_string", Array(ArrayInit(1).set(0, ref(v_str)).create()), 0x00000000CAE267E2LL))), "\n"));
  (v_str = LINE(7,(assignCallTemp(eo_1, x_chr(0LL)),concat3("this has a ", eo_1, " char in the middle"))));
  echo(concat(toString(LINE(8,invoke_failed("sqlite_escape_string", Array(ArrayInit(1).set(0, ref(v_str)).create()), 0x00000000CAE267E2LL))), "\n"));
  (v_data = (assignCallTemp(eo_1, LINE(13,(assignCallTemp(eo_6, x_chr(0LL)),concat3("this has a ", eo_6, " char in the middle")))),assignCallTemp(eo_2, concat(LINE(14,x_chr(1LL)), " this has an 0x01 at the start")),assignCallTemp(eo_3, LINE(15,(assignCallTemp(eo_6, x_chr(1LL)),concat3("this has ", eo_6, " in the middle")))),Array(ArrayInit(5).set(0, "hello there").set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, "").create())));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_data.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_item = iter3->second();
      {
        echo(LINE(20,concat3("raw: ", toString(v_item), "\n")));
        (v_coded = LINE(21,invoke_failed("sqlite_udf_encode_binary", Array(ArrayInit(1).set(0, ref(v_item)).create()), 0x00000000631236CDLL)));
        echo(LINE(22,concat3("coded: ", toString(v_coded), "\n")));
        (v_decoded = LINE(23,invoke_failed("sqlite_udf_decode_binary", Array(ArrayInit(1).set(0, ref(v_coded)).create()), 0x00000000E24B4D4FLL)));
        echo(LINE(24,concat3("decoded: ", toString(v_decoded), "\n")));
        if (!equal(v_item, v_decoded)) {
          echo("FAIL:\n");
          echo(LINE(27,concat3("[", toString(v_item), "]\n")));
          echo(LINE(28,concat3("[", toString(v_decoded), "]\n")));
        }
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
