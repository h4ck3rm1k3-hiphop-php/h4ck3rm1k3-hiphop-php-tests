
#ifndef __GENERATED_cls_aclass_h__
#define __GENERATED_cls_aclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0001534.php line 3 */
class c_aclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(aclass)
  END_CLASS_MAP(aclass)
  DECLARE_CLASS(aclass, aclass, ObjectData)
  void init();
  public: Variant m_blah;
  public: void t_unsetit();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_aclass_h__
