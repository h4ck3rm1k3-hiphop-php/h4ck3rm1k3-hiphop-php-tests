
#ifndef __GENERATED_php_roadsend_tests_resource_cleanup_h__
#define __GENERATED_php_roadsend_tests_resource_cleanup_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/resource-cleanup.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$resource_cleanup_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_resource_cleanup_h__
