
#include <php/roadsend/tests/files.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/files.php line 5 */
Variant f_first_digit(CVarRef v_number) {
  FUNCTION_INJECTION(first_digit);
  return LINE(6,x_substr(toString(v_number), toInt32(0LL), toInt32(1LL)));
} /* function */
Variant pm_php$roadsend$tests$files_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/files.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$files_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_testfile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testfile") : g->GV(testfile);
  Variant &v_testfile2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testfile2") : g->GV(testfile2);
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_handle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("handle") : g->GV(handle);
  Variant &v_buffer __attribute__((__unused__)) = (variables != gVariables) ? variables->get("buffer") : g->GV(buffer);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_contents __attribute__((__unused__)) = (variables != gVariables) ? variables->get("contents") : g->GV(contents);
  Variant &v_newc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newc") : g->GV(newc);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_here __attribute__((__unused__)) = (variables != gVariables) ? variables->get("here") : g->GV(here);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  LINE(3,x_error_reporting(toInt32(0LL)));
  (v_testfile = "file-test.txt");
  (v_testfile2 = "file-test2.txt");
  (v_string = "this is some text\n\nblah.. lazy blue fox jumps over satirical dog\nlorum crapsum dipsum jumpsum\n\n#!@$231hcjhco\nblah");
  (v_fp = LINE(20,x_fopen(toString(v_testfile), "wb")));
  echo(LINE(21,(assignCallTemp(eo_1, toString((assignCallTemp(eo_3, toObject(v_fp)),assignCallTemp(eo_4, toString(v_string)),assignCallTemp(eo_5, x_strlen(toString(v_string)) - 3LL),x_fwrite(eo_3, eo_4, eo_5)))),concat3("trunc fwrite: ", eo_1, "\n"))));
  echo(LINE(22,(assignCallTemp(eo_1, toString((assignCallTemp(eo_3, toObject(v_fp)),assignCallTemp(eo_4, toString(v_string)),assignCallTemp(eo_5, x_strlen(toString(v_string)) - 3LL),x_fputs(eo_3, eo_4, eo_5)))),concat3("trunc fputs: ", eo_1, "\n"))));
  echo(toString(LINE(23,x_fclose(toObject(v_fp)))));
  echo(LINE(24,(assignCallTemp(eo_1, toString(x_fclose(toObject(v_fp)))),concat3("[", eo_1, "]"))));
  (v_test = LINE(25,x_fclose(toObject(v_fp))));
  echo(LINE(26,concat3("<", toString(v_test), ">\n")));
  echo(LINE(28,(assignCallTemp(eo_1, toString(x_file_exists("file-test.txt"))),concat3("file_exists: ", eo_1, "\n"))));
  echo(LINE(30,(assignCallTemp(eo_1, toString(x_filesize(toString(v_testfile)))),concat3("filesize: ", eo_1, "\n"))));
  echo("fgets: ");
  (v_handle = LINE(33,x_fopen(toString(v_testfile), "r")));
  LOOP_COUNTER(1);
  {
    while (!(LINE(34,x_feof(toObject(v_handle))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_buffer = LINE(35,x_fgets(toObject(v_handle), 7LL)));
        echo(LINE(36,concat3("<", toString(v_buffer), ">")));
      }
    }
  }
  LINE(38,x_fclose(toObject(v_handle)));
  echo("fgets2: ");
  (v_handle = LINE(41,x_fopen(toString(v_testfile), "r")));
  LOOP_COUNTER(2);
  {
    while (!(LINE(42,x_feof(toObject(v_handle))))) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_buffer = LINE(43,x_fgets(toObject(v_handle))));
        echo(LINE(44,concat3("--<", toString(v_buffer), ">--")));
      }
    }
  }
  LINE(46,x_fclose(toObject(v_handle)));
  echo("fgetc1: ");
  (v_handle = LINE(49,x_fopen(toString(v_testfile), "r")));
  LOOP_COUNTER(3);
  {
    while (!(LINE(50,x_feof(toObject(v_handle))))) {
      LOOP_COUNTER_CHECK(3);
      {
        (v_buffer = LINE(51,x_fgetc(toObject(v_handle))));
        echo(LINE(52,concat3("<", toString(v_buffer), ">")));
      }
    }
  }
  LINE(54,x_fclose(toObject(v_handle)));
  echo("fgetc2: ");
  (v_handle = LINE(57,x_fopen(toString(v_testfile), "r")));
  LOOP_COUNTER(4);
  {
    while (!(LINE(58,x_feof(toObject(v_handle))))) {
      LOOP_COUNTER_CHECK(4);
      {
        (v_buffer = LINE(59,x_fgetc(toObject(v_handle))));
        echo(LINE(60,concat3("--<", toString(v_buffer), ">--")));
      }
    }
  }
  LINE(62,x_fclose(toObject(v_handle)));
  echo("fread + len: ");
  (v_handle = LINE(65,x_fopen(toString(v_testfile), "r")));
  LOOP_COUNTER(5);
  {
    while (toBoolean((v_data = LINE(66,x_fread(toObject(v_handle), 5LL))))) {
      LOOP_COUNTER_CHECK(5);
      {
        print(LINE(67,concat3("got [", toString(v_data), "]\n")));
      }
    }
  }
  LINE(69,x_fclose(toObject(v_handle)));
  echo("fread: ");
  (v_handle = LINE(73,x_fopen(toString(v_testfile), "r")));
  (v_contents = LINE(74,(assignCallTemp(eo_0, toObject(v_handle)),assignCallTemp(eo_1, toInt64(x_filesize(toString(v_testfile)))),x_fread(eo_0, eo_1))));
  LINE(75,x_fclose(toObject(v_handle)));
  echo(LINE(77,concat3("(", toString(v_contents), ")\n")));
  (v_fp = LINE(80,x_fopen(toString(v_testfile), "a")));
  echo(LINE(81,(assignCallTemp(eo_1, toString(x_fputs(toObject(v_fp), "appended-text-blahblah"))),concat3("append fputs: ", eo_1, "\n"))));
  LINE(82,x_fclose(toObject(v_fp)));
  echo(toString(LINE(85,x_file_put_contents(toString(v_testfile2), v_string))));
  (v_newc = LINE(88,x_file_get_contents(toString(v_testfile2))));
  echo(LINE(89,concat3("new contents: (", toString(v_newc), ")\n")));
  echo(toString(LINE(92,x_file_put_contents(toString(v_testfile2), ScalarArrays::sa_[0], toInt32(8LL) /* FILE_APPEND */))));
  (v_newc = LINE(95,x_file_get_contents(toString(v_testfile2))));
  echo(LINE(96,concat3("new contents: (", toString(v_newc), ")\n")));
  LINE(99,x_unlink(toString(v_testfile)));
  LINE(100,x_unlink(toString(v_testfile2)));
  (v_f = "/var/www/test/man/mack/the/knife/myfile.txt");
  echo(concat(LINE(104,x_basename(toString(v_f))), "\n"));
  echo("{");
  echo(toString(LINE(108,x_is_dir("/tmp/"))));
  echo(toString(LINE(109,x_is_dir("/doesntexist/"))));
  echo("}\n");
  echo("{");
  echo(toString(LINE(114,f_first_digit(x_filemtime("/etc/passwd")))));
  echo("}\n");
  (v_a = LINE(117,x_getcwd()));
  LINE(118,x_chdir("/usr/include"));
  (v_here = LINE(119,x_getcwd()));
  echo(LINE(120,concat3("in: ", toString(v_here), "\n")));
  LINE(121,x_chdir("/tmp"));
  echo(LINE(122,(assignCallTemp(eo_1, toString(x_getcwd())),concat3("now in ", eo_1, "\n"))));
  LINE(123,x_chdir(toString(v_here)));
  echo(LINE(124,(assignCallTemp(eo_1, toString(x_getcwd())),concat3("back in in: ", eo_1, "\n"))));
  LINE(125,x_chdir(toString(v_a)));
  echo("pathinfo1\n");
  (v_a = LINE(128,x_pathinfo("/home/php/some/directory/filename.ext")));
  LINE(129,x_var_dump(1, v_a));
  echo("pathinfo2\n");
  (v_c = LINE(131,x_pathinfo("/home/php/some/directory/filename.ext", toInt32(1LL) /* PATHINFO_DIRNAME */)));
  LINE(132,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
