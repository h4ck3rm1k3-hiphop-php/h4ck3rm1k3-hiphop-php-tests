
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "testfile",
    "testfile2",
    "string",
    "fp",
    "test",
    "handle",
    "buffer",
    "data",
    "contents",
    "newc",
    "f",
    "a",
    "here",
    "c",
  };
  if (idx >= 0 && idx < 26) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(testfile);
    case 13: return GV(testfile2);
    case 14: return GV(string);
    case 15: return GV(fp);
    case 16: return GV(test);
    case 17: return GV(handle);
    case 18: return GV(buffer);
    case 19: return GV(data);
    case 20: return GV(contents);
    case 21: return GV(newc);
    case 22: return GV(f);
    case 23: return GV(a);
    case 24: return GV(here);
    case 25: return GV(c);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
