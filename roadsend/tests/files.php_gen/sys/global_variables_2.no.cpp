
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 2:
      HASH_INITIALIZED(0x2027864469AD4382LL, g->GV(string),
                       string);
      HASH_INITIALIZED(0x640D12BF35788502LL, g->GV(newc),
                       newc);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 10:
      HASH_INITIALIZED(0x4292CEE227B9150ALL, g->GV(a),
                       a);
      break;
    case 13:
      HASH_INITIALIZED(0x41C3897C4E915A4DLL, g->GV(testfile2),
                       testfile2);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      HASH_INITIALIZED(0x60A103FBE1446E10LL, g->GV(buffer),
                       buffer);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      HASH_INITIALIZED(0x3A1DC49AA6CFEA13LL, g->GV(test),
                       test);
      break;
    case 28:
      HASH_INITIALIZED(0x48E8F48146EEEF5CLL, g->GV(handle),
                       handle);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 40:
      HASH_INITIALIZED(0x30164401A9853128LL, g->GV(data),
                       data);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      HASH_INITIALIZED(0x6BB4A0689FBAD42ELL, g->GV(f),
                       f);
      break;
    case 48:
      HASH_INITIALIZED(0x5B0C0C547374D530LL, g->GV(fp),
                       fp);
      HASH_INITIALIZED(0x32C769EE5C5509B0LL, g->GV(c),
                       c);
      break;
    case 54:
      HASH_INITIALIZED(0x31CF71EAC03B86B6LL, g->GV(testfile),
                       testfile);
      HASH_INITIALIZED(0x66D0F03A76191AF6LL, g->GV(here),
                       here);
      break;
    case 62:
      HASH_INITIALIZED(0x5FAC83E143BEACFELL, g->GV(contents),
                       contents);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
