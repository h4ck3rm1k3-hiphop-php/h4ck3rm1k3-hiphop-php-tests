
#include <php/roadsend/tests/str_pad.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$str_pad_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/str_pad.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$str_pad_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);

  (v_input = "Alien");
  print(concat(LINE(4,x_str_pad(toString(v_input), toInt32(10LL))), "\n"));
  print(concat(LINE(5,x_str_pad(toString(v_input), toInt32(10LL), "-=", toInt32(0LL) /* STR_PAD_LEFT */)), "\n"));
  print(concat(LINE(6,x_str_pad(toString(v_input), toInt32(10LL), "_", toInt32(2LL) /* STR_PAD_BOTH */)), "\n"));
  print(concat(LINE(7,x_str_pad(toString(v_input), toInt32(10LL), "-=", toInt32(2LL) /* STR_PAD_BOTH */)), "\n"));
  print(concat(LINE(8,x_str_pad(toString(v_input), toInt32(11LL), "-=", toInt32(2LL) /* STR_PAD_BOTH */)), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
