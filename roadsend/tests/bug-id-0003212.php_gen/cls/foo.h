
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0003212.php line 3 */
class c_foo : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, foo, ObjectData)
  void init();
  public: Variant m_roles;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
