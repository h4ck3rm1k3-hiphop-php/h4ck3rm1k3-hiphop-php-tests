
#ifndef __GENERATED_php_roadsend_tests_bug_id_0000906_h__
#define __GENERATED_php_roadsend_tests_bug_id_0000906_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0000906.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_cdr(CVarRef v_a);
Variant pm_php$roadsend$tests$bug_id_0000906_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_car(CVarRef v_a);
void f_cons(CStrRef v_a);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0000906_h__
