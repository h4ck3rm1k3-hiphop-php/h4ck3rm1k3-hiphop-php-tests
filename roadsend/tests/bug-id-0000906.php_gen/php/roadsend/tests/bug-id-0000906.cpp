
#include <php/roadsend/tests/bug-id-0000906.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000906.php line 9 */
void f_cdr(CVarRef v_a) {
  FUNCTION_INJECTION(cdr);
  echo(toString(v_a));
} /* function */
/* SRC: roadsend/tests/bug-id-0000906.php line 6 */
void f_car(CVarRef v_a) {
  FUNCTION_INJECTION(car);
  echo(toString(v_a));
} /* function */
/* SRC: roadsend/tests/bug-id-0000906.php line 12 */
void f_cons(CStrRef v_a) {
  FUNCTION_INJECTION(cons);
  echo(v_a);
} /* function */
Variant pm_php$roadsend$tests$bug_id_0000906_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000906.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000906_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo(" disallow overriding scheme functions from php\n\n right now you can define a php function (con or list for example) and it will override the scheme function\n\n");
  echo((LINE(15,f_cons("x")), null));
  v_a.append(("hi"));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_i = iter3->second();
      {
        echo(toString(v_i));
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
