
#ifndef __GENERATED_php_roadsend_tests_streams_h__
#define __GENERATED_php_roadsend_tests_streams_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/streams.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$streams_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_necho(int64 v_line_number, CVarRef v_string);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_streams_h__
