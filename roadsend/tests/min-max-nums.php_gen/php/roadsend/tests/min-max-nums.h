
#ifndef __GENERATED_php_roadsend_tests_min_max_nums_h__
#define __GENERATED_php_roadsend_tests_min_max_nums_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/min-max-nums.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$min_max_nums_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_min_max_nums_h__
