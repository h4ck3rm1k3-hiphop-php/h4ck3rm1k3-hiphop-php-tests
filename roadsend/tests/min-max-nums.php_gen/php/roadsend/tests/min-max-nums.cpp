
#include <php/roadsend/tests/min-max-nums.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_PHP_INT_MIN = "PHP_INT_MIN";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$min_max_nums_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/min-max-nums.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$min_max_nums_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 9223372036854775800.0 /* PHP_INT_MAX */);
  echo(concat("a is [", LINE(4,concat6(toString(v_a), "] :: as int [", toString(toInt64(v_a)), "] as float [", toString(toDouble(v_a)), "]\n"))));
  echo(LINE(5,concat3(toString(v_a) + toString(" + 1 = "), (toString(v_a + 1LL)), "\n")));
  echo(LINE(6,(assignCallTemp(eo_0, toString(v_a) + toString("++ = ")),assignCallTemp(eo_1, toString(++v_a)),concat3(eo_0, eo_1, "\n"))));
  LINE(7,throw_fatal("bad define"));
  (v_a = k_PHP_INT_MIN);
  echo(concat("a is [", LINE(9,concat6(toString(v_a), "] :: as int [", toString(toInt64(v_a)), "] as float [", toString(toDouble(v_a)), "]\n"))));
  echo(LINE(10,concat3(toString(v_a) + toString(" - 1 = "), (toString(v_a - 1LL)), "\n")));
  echo(LINE(11,(assignCallTemp(eo_0, toString(v_a) + toString("-- = ")),assignCallTemp(eo_1, toString(--v_a)),concat3(eo_0, eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
