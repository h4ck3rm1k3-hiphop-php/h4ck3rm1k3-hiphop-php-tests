
#ifndef __GENERATED_php_roadsend_tests_inc15_h__
#define __GENERATED_php_roadsend_tests_inc15_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc15.fw.h>

// Declarations
#include <cls/bfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc15_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_bfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc15_h__
