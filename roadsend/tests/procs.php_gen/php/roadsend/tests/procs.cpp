
#include <php/roadsend/tests/procs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$procs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/procs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$procs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_descriptorspec __attribute__((__unused__)) = (variables != gVariables) ? variables->get("descriptorspec") : g->GV(descriptorspec);
  Variant &v_cwd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cwd") : g->GV(cwd);
  Variant &v_env __attribute__((__unused__)) = (variables != gVariables) ? variables->get("env") : g->GV(env);
  Variant &v_pipes __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pipes") : g->GV(pipes);
  Variant &v_process __attribute__((__unused__)) = (variables != gVariables) ? variables->get("process") : g->GV(process);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);
  Variant &v_return_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("return_value") : g->GV(return_value);

  (v_descriptorspec = ScalarArrays::sa_[0]);
  (v_cwd = "/tmp");
  (v_env = ScalarArrays::sa_[1]);
  (v_process = LINE(12,x_proc_open("sh", toArray(v_descriptorspec), ref(v_pipes))));
  if (LINE(14,x_is_resource(v_process))) {
    echo(LINE(22,(assignCallTemp(eo_1, toString(x_fwrite(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "ls\n"))),concat3("fwrite: ", eo_1, "\n"))));
    echo(LINE(23,(assignCallTemp(eo_1, toString(x_fclose(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))))),concat3("fclose: ", eo_1, "\n"))));
    LOOP_COUNTER(1);
    {
      while (!(LINE(26,x_feof(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))))) {
        LOOP_COUNTER_CHECK(1);
        {
          (v_line = LINE(27,x_fgets(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
          echo(toString(v_line));
        }
      }
    }
    LINE(30,x_fclose(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
    (v_return_value = LINE(34,x_proc_close(toObject(v_process))));
    echo(LINE(36,concat3("command returned ", toString(v_return_value), "\n")));
  }
  (v_descriptorspec = ScalarArrays::sa_[2]);
  (v_process = LINE(45,x_proc_open("find /tmp", toArray(v_descriptorspec), ref(v_pipes))));
  if (LINE(46,x_is_resource(v_process))) {
    LOOP_COUNTER(2);
    {
      while (!(LINE(48,x_feof(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))))) {
        LOOP_COUNTER_CHECK(2);
        {
          (v_line = LINE(49,x_fgets(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
          echo(LINE(50,concat3("stdout: [", toString(v_line), "]\n")));
        }
      }
    }
    LINE(52,x_fclose(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
    LOOP_COUNTER(3);
    {
      while (!(LINE(54,x_feof(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)))))) {
        LOOP_COUNTER_CHECK(3);
        {
          (v_line = LINE(55,x_fgets(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
          echo(LINE(56,concat3("stderr: [", toString(v_line), "]\n")));
        }
      }
    }
    LINE(58,x_fclose(toObject(v_pipes.rvalAt(2LL, 0x486AFCC090D5F98CLL))));
    (v_return_value = LINE(62,x_proc_close(toObject(v_process))));
    echo(LINE(64,concat3("command returned ", toString(v_return_value), "\n")));
  }
  echo(" \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
