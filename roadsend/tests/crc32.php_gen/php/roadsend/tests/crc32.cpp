
#include <php/roadsend/tests/crc32.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$crc32_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/crc32.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$crc32_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo(concat(toString(LINE(3,x_crc32("the quick brown fox jumped over the lazy dog"))), "\n"));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 256LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo((LINE(6,(assignCallTemp(eo_0, toString(v_i)),assignCallTemp(eo_2, toString(x_crc32(x_chr(toInt64(v_i))))),concat4(eo_0, ": ", eo_2, "\n")))));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
