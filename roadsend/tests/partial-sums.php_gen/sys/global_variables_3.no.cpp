
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x68DF81F26D942FC7LL, a1, 13);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x117B8667E4662809LL, n, 12);
      break;
    case 13:
      HASH_INDEX(0x43EDA7BEE714570DLL, a3, 15);
      HASH_INDEX(0x437D9F169312FE4DLL, k2, 25);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x0D6857FDDD7F21CFLL, k, 24);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x6E47C085C7F241D3LL, sk, 27);
      break;
    case 24:
      HASH_INDEX(0x180CCF01FBC81F18LL, a7, 19);
      break;
    case 25:
      HASH_INDEX(0x1DC175F7E4343259LL, a4, 16);
      break;
    case 26:
      HASH_INDEX(0x7C60E6EC5B67C29ALL, k3, 26);
      break;
    case 27:
      HASH_INDEX(0x2AB21AF4F5B8471BLL, alt, 23);
      break;
    case 31:
      HASH_INDEX(0x0EFAF509EE3FE5DFLL, twothirds, 22);
      break;
    case 32:
      HASH_INDEX(0x4555E94E338480E0LL, a5, 17);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 41:
      HASH_INDEX(0x598AD62830EAF1E9LL, a6, 18);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 47:
      HASH_INDEX(0x36551E10CAD29DEFLL, ck, 28);
      break;
    case 52:
      HASH_INDEX(0x00FF5C07497953F4LL, a8, 20);
      break;
    case 54:
      HASH_INDEX(0x6C05C2858C72A876LL, a2, 14);
      break;
    case 55:
      HASH_INDEX(0x3A3C288F3DA61B77LL, a9, 21);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 29) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
