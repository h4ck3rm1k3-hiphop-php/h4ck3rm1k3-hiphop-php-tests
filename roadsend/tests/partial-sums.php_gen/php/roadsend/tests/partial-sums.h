
#ifndef __GENERATED_php_roadsend_tests_partial_sums_h__
#define __GENERATED_php_roadsend_tests_partial_sums_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/partial-sums.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$partial_sums_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_partial_sums_h__
