
#include <php/roadsend/tests/type_name_len_flags.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$type_name_len_flags_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/type_name_len_flags.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$type_name_len_flags_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_fields __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fields") : g->GV(fields);
  Variant &v_rows __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rows") : g->GV(rows);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_table __attribute__((__unused__)) = (variables != gVariables) ? variables->get("table") : g->GV(table);
  Variant &v_type __attribute__((__unused__)) = (variables != gVariables) ? variables->get("type") : g->GV(type);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);
  Variant &v_len __attribute__((__unused__)) = (variables != gVariables) ? variables->get("len") : g->GV(len);
  Variant &v_flags __attribute__((__unused__)) = (variables != gVariables) ? variables->get("flags") : g->GV(flags);

  (toBoolean(LINE(3,x_mysql_connect("localhost", "develUser", "d3v3lpa55")))) || (toBoolean(f_exit("Couldn't connect")));
  (toBoolean(LINE(6,x_mysql_select_db("test")))) || (toBoolean(f_exit("Couldn't select database")));
  (v_result = LINE(9,x_mysql_query("SELECT * FROM my_table")));
  (v_fields = LINE(11,x_mysql_num_fields(v_result)));
  (v_rows = LINE(12,x_mysql_num_rows(v_result)));
  (v_i = 0LL);
  (v_table = LINE(14,x_mysql_field_table(v_result, toInt32(v_i))));
  echo(concat("Your '", LINE(15,concat6(toString(v_table), "' table has ", toString(v_fields), " fields and ", toString(v_rows), " records\n"))));
  echo("The table has the following fields\n");
  LOOP_COUNTER(1);
  {
    while (less(v_i, v_fields)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_type = LINE(18,x_mysql_field_type(v_result, toInt32(v_i))));
        (v_name = LINE(19,x_mysql_field_name(v_result, toInt32(v_i))));
        (v_len = LINE(20,x_mysql_field_len(v_result, toInt32(v_i))));
        (v_flags = LINE(21,x_mysql_field_flags(v_result, toInt32(v_i))));
        echo(concat_rev(LINE(22,concat6(toString(v_name), " len: ", toString(v_len), " flags: ", toString(v_flags), "\n")), concat3("type: ", toString(v_type), " name: ")));
        v_i++;
      }
    }
  }
  LINE(25,x_mysql_close());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
