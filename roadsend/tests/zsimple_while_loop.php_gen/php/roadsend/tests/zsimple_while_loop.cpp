
#include <php/roadsend/tests/zsimple_while_loop.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$zsimple_while_loop_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zsimple_while_loop.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zsimple_while_loop_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 1LL);
  LOOP_COUNTER(1);
  {
    while (less(v_a, 10LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(v_a));
        v_a++;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
