
#include <php/roadsend/tests/zsimple_if_elsif_else.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$zsimple_if_elsif_else_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zsimple_if_elsif_else.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zsimple_if_elsif_else_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 1LL);
  if (equal(v_a, 0LL)) {
    echo("bad");
  }
  else if (equal(v_a, 3LL)) {
    echo("bad");
  }
  else {
    echo("good");
  }
  echo("\t\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
