
#ifndef __GENERATED_php_roadsend_tests_serialize_refs_h__
#define __GENERATED_php_roadsend_tests_serialize_refs_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/serialize-refs.fw.h>

// Declarations
#include <cls/zot.h>
#include <cls/myclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_test_ser(CVarRef v_val);
Variant pm_php$roadsend$tests$serialize_refs_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_zot(CArrRef params, bool init = true);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_serialize_refs_h__
