
#include <php/roadsend/tests/bug-id-0000728.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_TAB_4 = " ";
const StaticString k_TAB_8 = "  ";
const StaticString k_SM_TAG_PREGEXP = "/<\\s*12\\s(.+)\\s*>/Ui";
const int64 k_SM_TAG_IDENTIFIER = 12LL;

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000728_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000728.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000728_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("0000728 concat strings including other defines to build a define\n");
  ;
  ;
  ;
  ;
  echo("12, /<\\s*12\\s(.+)\\s*>/Ui,  ,   \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
