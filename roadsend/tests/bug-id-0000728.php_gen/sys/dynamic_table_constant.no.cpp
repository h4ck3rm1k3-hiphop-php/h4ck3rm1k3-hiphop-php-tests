
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_TAB_4;
extern const StaticString k_TAB_8;
extern const StaticString k_SM_TAG_PREGEXP;
extern const int64 k_SM_TAG_IDENTIFIER;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x0DE573DB5522CB78LL, k_TAB_4, TAB_4);
      break;
    case 1:
      HASH_RETURN(0x1329AB09C2AC33D1LL, k_TAB_8, TAB_8);
      break;
    case 3:
      HASH_RETURN(0x58F165E6668406CBLL, k_SM_TAG_IDENTIFIER, SM_TAG_IDENTIFIER);
      HASH_RETURN(0x4B8D47FD3B48CCD3LL, k_SM_TAG_PREGEXP, SM_TAG_PREGEXP);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
