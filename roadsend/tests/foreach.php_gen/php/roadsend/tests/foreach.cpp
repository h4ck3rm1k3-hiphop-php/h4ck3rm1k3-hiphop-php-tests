
#include <php/roadsend/tests/foreach.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$foreach_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/foreach.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$foreach_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_colors __attribute__((__unused__)) = (variables != gVariables) ? variables->get("colors") : g->GV(colors);
  Variant &v_color __attribute__((__unused__)) = (variables != gVariables) ? variables->get("color") : g->GV(color);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);

  (v_colors = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_colors.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_color = iter3->second();
      {
        echo(LINE(6,concat3("Do you like ", toString(v_color), "\?\n")));
      }
    }
  }
  print(concat(toString(v_color), "\n"));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_colors.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_color = iter6->second();
      v_key = iter6->first();
      {
        v_colors.set(v_key, (LINE(23,x_strtoupper(toString(v_color)))));
      }
    }
  }
  LINE(25,x_print_r(v_colors));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
