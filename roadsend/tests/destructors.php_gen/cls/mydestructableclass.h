
#ifndef __GENERATED_cls_mydestructableclass_h__
#define __GENERATED_cls_mydestructableclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/destructors.php line 2 */
class c_mydestructableclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(mydestructableclass)
  END_CLASS_MAP(mydestructableclass)
  DECLARE_CLASS(mydestructableclass, MyDestructableClass, ObjectData)
  void init();
  public: virtual void destruct();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t___destruct();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mydestructableclass_h__
