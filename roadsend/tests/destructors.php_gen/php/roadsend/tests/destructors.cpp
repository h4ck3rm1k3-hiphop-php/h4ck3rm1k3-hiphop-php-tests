
#include <php/roadsend/tests/destructors.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/destructors.php line 2 */
Variant c_mydestructableclass::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x2072FF3918A89F3DLL, g->s_mydestructableclass_DupIdcnt,
                  cnt);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_mydestructableclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_mydestructableclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_mydestructableclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_mydestructableclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_mydestructableclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_mydestructableclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_mydestructableclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(mydestructableclass)
ObjectData *c_mydestructableclass::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_mydestructableclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_mydestructableclass::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_mydestructableclass::cloneImpl() {
  c_mydestructableclass *obj = NEW(c_mydestructableclass)();
  cloneSet(obj);
  return obj;
}
void c_mydestructableclass::cloneSet(c_mydestructableclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_mydestructableclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_mydestructableclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_mydestructableclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_mydestructableclass$os_get(const char *s) {
  return c_mydestructableclass::os_get(s, -1);
}
Variant &cw_mydestructableclass$os_lval(const char *s) {
  return c_mydestructableclass::os_lval(s, -1);
}
Variant cw_mydestructableclass$os_constant(const char *s) {
  return c_mydestructableclass::os_constant(s);
}
Variant cw_mydestructableclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_mydestructableclass::os_invoke(c, s, params, -1, fatal);
}
void c_mydestructableclass::init() {
}
void c_mydestructableclass::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_mydestructableclass_DupIdcnt = 0LL;
}
void csi_mydestructableclass() {
  c_mydestructableclass::os_static_initializer();
}
/* SRC: roadsend/tests/destructors.php line 4 */
void c_mydestructableclass::t___construct() {
  INSTANCE_METHOD_INJECTION(MyDestructableClass, MyDestructableClass::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  print("In constructor\n");
  g->s_mydestructableclass_DupIdcnt++;
  (o_lval("name", 0x0BCDB293DC3CBDDCLL) = concat("MyDestructableClass: ", toString(g->s_mydestructableclass_DupIdcnt)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/destructors.php line 10 */
Variant c_mydestructableclass::t___destruct() {
  INSTANCE_METHOD_INJECTION(MyDestructableClass, MyDestructableClass::__destruct);
  setInDtor();
  print(LINE(11,concat3("Destroying ", toString(o_get("name", 0x0BCDB293DC3CBDDCLL)), "\n")));
  return null;
} /* function */
Object co_mydestructableclass(CArrRef params, bool init /* = true */) {
  return Object(p_mydestructableclass(NEW(c_mydestructableclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$destructors_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/destructors.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$destructors_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  (v_obj = ((Object)(LINE(15,p_mydestructableclass(p_mydestructableclass(NEWOBJ(c_mydestructableclass)())->create())))));
  setNull(v_obj);
  (v_obj = ((Object)(LINE(18,p_mydestructableclass(p_mydestructableclass(NEWOBJ(c_mydestructableclass)())->create())))));
  unset(v_obj);
  (v_obj = ((Object)(LINE(21,p_mydestructableclass(p_mydestructableclass(NEWOBJ(c_mydestructableclass)())->create())))));
  (v_obj = 50LL);
  (v_obj = ((Object)(LINE(24,p_mydestructableclass(p_mydestructableclass(NEWOBJ(c_mydestructableclass)())->create())))));
  echo(" \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
