
#ifndef __GENERATED_php_roadsend_tests_destructors_h__
#define __GENERATED_php_roadsend_tests_destructors_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/destructors.fw.h>

// Declarations
#include <cls/mydestructableclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$destructors_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_mydestructableclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_destructors_h__
