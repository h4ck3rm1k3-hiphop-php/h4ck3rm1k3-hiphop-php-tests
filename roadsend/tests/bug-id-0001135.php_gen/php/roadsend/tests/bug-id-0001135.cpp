
#include <php/roadsend/tests/bug-id-0001135.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001135_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001135.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001135_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_printf(2, "1 %10.5e\n", ScalarArrays::sa_[0]));
  LINE(3,x_var_dump(1, x_sprintf(2, "2 %10.5e\n", ScalarArrays::sa_[0])));
  LINE(4,x_printf(2, "3 %10.5f\n", ScalarArrays::sa_[0]));
  LINE(5,x_var_dump(1, x_sprintf(2, "4 %10.5f\n", ScalarArrays::sa_[0])));
  LINE(6,x_printf(2, "5 %10.5d\n", ScalarArrays::sa_[0]));
  LINE(7,x_var_dump(1, x_sprintf(2, "6 %10.5d\n", ScalarArrays::sa_[0])));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
