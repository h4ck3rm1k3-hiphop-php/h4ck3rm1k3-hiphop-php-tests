
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$tests$index_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$tests$apear_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_INCLUDE(0x41A6E6346A65DC42LL, "roadsend/tests/apear.php", php$roadsend$tests$apear_php);
      break;
    case 3:
      HASH_INCLUDE(0x383E4E842BA4297BLL, "roadsend/tests/index.php", php$roadsend$tests$index_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
