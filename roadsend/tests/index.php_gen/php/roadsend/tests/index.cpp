
#include <php/roadsend/tests/apear.h>
#include <php/roadsend/tests/index.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$index_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/index.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$index_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_prefix __attribute__((__unused__)) = (variables != gVariables) ? variables->get("prefix") : g->GV(prefix);

  echo("go for broke\n");
  (v_prefix = "../admin/");
  LINE(4,include((concat(toString(v_prefix), "common.inc")), false, variables, "roadsend/tests/"));
  LINE(6,pm_php$roadsend$tests$apear_php(false, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
