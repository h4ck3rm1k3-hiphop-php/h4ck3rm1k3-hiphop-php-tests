
#ifndef __GENERATED_php_roadsend_tests_inc_h__
#define __GENERATED_php_roadsend_tests_inc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc.fw.h>

// Declarations
#include <cls/nfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_nfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc_h__
