
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_m(CArrRef params, bool init = true);
Variant cw_m$os_get(const char *s);
Variant &cw_m$os_lval(const char *s);
Variant cw_m$os_constant(const char *s);
Variant cw_m$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_o(CArrRef params, bool init = true);
Variant cw_o$os_get(const char *s);
Variant &cw_o$os_lval(const char *s);
Variant cw_o$os_constant(const char *s);
Variant cw_o$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_CREATE_OBJECT(0x5E2DDF38169307FALL, m);
      HASH_CREATE_OBJECT(0x3769074A877921E6LL, o);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x5E2DDF38169307FALL, m);
      HASH_INVOKE_STATIC_METHOD(0x3769074A877921E6LL, o);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x5E2DDF38169307FALL, m);
      HASH_GET_STATIC_PROPERTY(0x3769074A877921E6LL, o);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x5E2DDF38169307FALL, m);
      HASH_GET_STATIC_PROPERTY_LV(0x3769074A877921E6LL, o);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x5E2DDF38169307FALL, m);
      HASH_GET_CLASS_CONSTANT(0x3769074A877921E6LL, o);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
