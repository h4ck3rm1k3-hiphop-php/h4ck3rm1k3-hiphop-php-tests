
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001217_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001217_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001217.fw.h>

// Declarations
#include <cls/m.h>
#include <cls/o.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001217_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_m(CArrRef params, bool init = true);
Object co_o(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001217_h__
