
#include <php/roadsend/tests/bug-id-0001217.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001217.php line 3 */
Variant c_m::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_m::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_m::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  c_ObjectData::o_get(props);
}
bool c_m::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_m::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_m::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_m::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_m::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(m)
ObjectData *c_m::cloneImpl() {
  c_m *obj = NEW(c_m)();
  cloneSet(obj);
  return obj;
}
void c_m::cloneSet(c_m *clone) {
  clone->m_a = m_a;
  ObjectData::cloneSet(clone);
}
Variant c_m::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_m::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_m::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_m$os_get(const char *s) {
  return c_m::os_get(s, -1);
}
Variant &cw_m$os_lval(const char *s) {
  return c_m::os_lval(s, -1);
}
Variant cw_m$os_constant(const char *s) {
  return c_m::os_constant(s);
}
Variant cw_m$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_m::os_invoke(c, s, params, -1, fatal);
}
void c_m::init() {
  m_a = 1LL;
}
/* SRC: roadsend/tests/bug-id-0001217.php line 5 */
String c_m::t___tostring() {
  INSTANCE_METHOD_INJECTION(m, m::__toString);
  return "[class m]";
} /* function */
/* SRC: roadsend/tests/bug-id-0001217.php line 10 */
Variant c_o::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_o::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_o::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("c", m_c));
  c_ObjectData::o_get(props);
}
bool c_o::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_o::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_o::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_o::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_o::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(o)
ObjectData *c_o::cloneImpl() {
  c_o *obj = NEW(c_o)();
  cloneSet(obj);
  return obj;
}
void c_o::cloneSet(c_o *clone) {
  clone->m_c = m_c;
  ObjectData::cloneSet(clone);
}
Variant c_o::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_o::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_o::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_o$os_get(const char *s) {
  return c_o::os_get(s, -1);
}
Variant &cw_o$os_lval(const char *s) {
  return c_o::os_lval(s, -1);
}
Variant cw_o$os_constant(const char *s) {
  return c_o::os_constant(s);
}
Variant cw_o$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_o::os_invoke(c, s, params, -1, fatal);
}
void c_o::init() {
  m_c = 1LL;
}
/* SRC: roadsend/tests/bug-id-0001217.php line 12 */
String c_o::t___tostring() {
  INSTANCE_METHOD_INJECTION(o, o::__toString);
  return "[class o]";
} /* function */
Object co_m(CArrRef params, bool init /* = true */) {
  return Object(p_m(NEW(c_m)())->dynCreate(params, init));
}
Object co_o(CArrRef params, bool init /* = true */) {
  return Object(p_o(NEW(c_o)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0001217_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001217.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001217_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_v1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v1") : g->GV(v1);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_v2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v2") : g->GV(v2);

  (v_c = LINE(17,p_m(p_m(NEWOBJ(c_m)())->create())));
  (v_d = ((Object)(LINE(18,p_o(p_o(NEWOBJ(c_o)())->create())))));
  echo(LINE(20,concat3("grimble: ", (toString(equal(v_c, v_c))), "\n")));
  echo(LINE(21,concat3("grimble: ", (toString(equal(v_d, v_d))), "\n")));
  echo(LINE(22,concat3("grumble: ", (toString(same(v_c, v_c))), "\n")));
  echo(LINE(23,concat3("grumble: ", (toString(same(v_d, v_d))), "\n")));
  (v_a = Array(ArrayInit(15).set(0, 1LL).set(1, "20").set(2, 100LL).set(3, "200").set(4, ScalarArrays::sa_[0]).set(5, true).set(6, "0f2fj").set(7, v_d).set(8, ScalarArrays::sa_[1]).set(9, "testing").set(10, null).set(11, ScalarArrays::sa_[2]).set(12, v_c).set(13, 1.23).set(14, "4.31").create()));
  (v_b = Array(ArrayInit(15).set(0, 89LL).set(1, "a string").set(2, "12").set(3, null).set(4, ScalarArrays::sa_[1]).set(5, 9321LL).set(6, "802").set(7, v_c).set(8, 1.23).set(9, "00v2nc8").set(10, ScalarArrays::sa_[3]).set(11, false).set(12, v_d).set(13, "this is a string").set(14, "4.31").create()));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v1 = iter3->second();
      {
        v_i++;
        (v_j = 0LL);
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_b.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_v2 = iter6->second();
            {
              v_j++;
              echo(LINE(63,(assignCallTemp(eo_0, toString(v_i)),assignCallTemp(eo_2, (assignCallTemp(eo_3, toString(v_j)),assignCallTemp(eo_5, toString(v_v1)),assignCallTemp(eo_7, x_gettype(v_v1)),concat6(eo_3, ": a is [", eo_5, "] and is ", eo_7, "\n"))),concat3(eo_0, ",", eo_2))));
              echo(LINE(64,(assignCallTemp(eo_0, toString(v_i)),assignCallTemp(eo_2, (assignCallTemp(eo_3, toString(v_j)),assignCallTemp(eo_5, toString(v_v2)),assignCallTemp(eo_7, x_gettype(v_v2)),concat6(eo_3, ": b is [", eo_5, "] and is ", eo_7, "\n"))),concat3(eo_0, ",", eo_2))));
              if (LINE(65,x_is_string(v_v1)) && x_is_numeric(v_v1)) echo(LINE(66,concat4(toString(v_i), ",", toString(v_j), ": a is a string and is numeric\n")));
              if (LINE(67,x_is_string(v_v2)) && x_is_numeric(v_v2)) echo(LINE(68,concat4(toString(v_i), ",", toString(v_j), ": b is a string and is numeric\n")));
              echo(LINE(69,concat6(toString(v_i), ",", toString(v_j), ": a == b: ", (toString(equal(v_v1, v_v2))), "\n")));
              LINE(70,x_print_r(v_v1));
              LINE(71,x_print_r(v_v2));
              echo(LINE(72,concat6(toString(v_i), ",", toString(v_j), ": a === b: ", (toString(same(v_v1, v_v2))), "\n")));
              echo(LINE(73,concat6(toString(v_i), ",", toString(v_j), ": a < b: ", (toString(less(v_v1, v_v2))), "\n")));
              echo(LINE(74,concat6(toString(v_i), ",", toString(v_j), ": a > b: ", (toString(more(v_v1, v_v2))), "\n")));
              echo(LINE(75,concat6(toString(v_i), ",", toString(v_j), ": a <= b: ", (toString(not_more(v_v1, v_v2))), "\n")));
              echo(LINE(76,concat6(toString(v_i), ",", toString(v_j), ": a >= b: ", (toString(not_less(v_v1, v_v2))), "\n")));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
