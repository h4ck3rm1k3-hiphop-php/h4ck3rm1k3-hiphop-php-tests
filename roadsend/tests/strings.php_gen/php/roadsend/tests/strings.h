
#ifndef __GENERATED_php_roadsend_tests_strings_h__
#define __GENERATED_php_roadsend_tests_strings_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/strings.fw.h>

// Declarations
#include <cls/myclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$strings_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_strings_h__
