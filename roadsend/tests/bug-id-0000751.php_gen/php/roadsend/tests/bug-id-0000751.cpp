
#include <php/roadsend/tests/bug-id-0000751.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000751.php line 3 */
String f_aphp_uname() {
  FUNCTION_INJECTION(aphp_uname);
  return "Linux simple 2.4.18 #1 Sat Mar 1 15:55:45 EST 2003 i686";
} /* function */
/* SRC: roadsend/tests/bug-id-0000751.php line 7 */
void f_parsesignature(Variant v_uname //  = null
) {
  FUNCTION_INJECTION(parseSignature);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Array &sv_sysmap __attribute__((__unused__)) = g->sv_parsesignature_DupIdsysmap;
  bool &inited_sv_sysmap __attribute__((__unused__)) = g->inited_sv_parsesignature_DupIdsysmap;
  Array &sv_cpumap __attribute__((__unused__)) = g->sv_parsesignature_DupIdcpumap;
  bool &inited_sv_cpumap __attribute__((__unused__)) = g->inited_sv_parsesignature_DupIdcpumap;
  Variant v_parts;

  if (!inited_sv_sysmap) {
    (sv_sysmap = ScalarArrays::sa_[0]);
    inited_sv_sysmap = true;
  }
  if (!inited_sv_cpumap) {
    (sv_cpumap = ScalarArrays::sa_[1]);
    inited_sv_cpumap = true;
  }
  if (same(v_uname, null)) {
    (v_uname = LINE(17,f_aphp_uname()));
  }
  (v_parts = LINE(19,(assignCallTemp(eo_1, x_trim(toString(v_uname))),x_split("[[:space:]]+", eo_1))));
  echo(toString(v_parts));
} /* function */
Variant pm_php$roadsend$tests$bug_id_0000751_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000751.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000751_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
