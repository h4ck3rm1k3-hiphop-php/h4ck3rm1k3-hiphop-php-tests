
#ifndef __GENERATED_php_roadsend_tests_bug_id_0000751_h__
#define __GENERATED_php_roadsend_tests_bug_id_0000751_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0000751.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_aphp_uname();
Variant pm_php$roadsend$tests$bug_id_0000751_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_parsesignature(Variant v_uname = null);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0000751_h__
