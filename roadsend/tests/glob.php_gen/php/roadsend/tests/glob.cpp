
#include <php/roadsend/tests/glob.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$glob_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/glob.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$glob_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  (v_e = LINE(3,x_glob("/etc/*.conf")));
  LINE(4,x_var_dump(1, v_e));
  (v_e = LINE(6,x_glob("/foo-bar/*")));
  LINE(7,x_var_dump(1, v_e));
  (v_e = LINE(9,x_glob("/etc/*", toInt32(6LL))));
  LINE(10,x_var_dump(1, v_e));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
