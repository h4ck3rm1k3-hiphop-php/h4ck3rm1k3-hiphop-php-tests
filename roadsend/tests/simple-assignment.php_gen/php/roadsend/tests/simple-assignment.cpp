
#include <php/roadsend/tests/simple-assignment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$simple_assignment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/simple-assignment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$simple_assignment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = 8LL);
  echo(LINE(4,concat3("A: ", toString(v_a), "\n")));
  (v_b = 32LL);
  echo(LINE(6,concat3("B: ", toString(v_b), "\n")));
  (v_a = v_b);
  echo(LINE(8,concat5("A: ", toString(v_a), ", B: ", toString(v_b), "\n")));
  (v_a = 1LL);
  echo(LINE(10,concat5("A: ", toString(v_a), ", B: ", toString(v_b), "\n")));
  v_b++;
  echo(LINE(12,concat5("A: ", toString(v_a), ", B: ", toString(v_b), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
