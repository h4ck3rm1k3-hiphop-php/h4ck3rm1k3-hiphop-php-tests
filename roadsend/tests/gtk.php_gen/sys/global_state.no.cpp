
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

static void print(FILE *fp, String s) {
  if (fp) {
    fwrite(s.c_str(), 1, s.size(), fp);
    return;
  }
  echo(s);
}
static void output_dynamic_constants(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_constants = json_decode('");
  Array dynamic_constants;
  String s = f_json_encode(dynamic_constants);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_static_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$static_global_variables = json_decode('");
  Array static_global_variables;
  static_global_variables.set("gv_argc", g->get("argc"));
  static_global_variables.set("gv_argv", g->get("argv"));
  static_global_variables.set("gv__SERVER", g->get("_SERVER"));
  static_global_variables.set("gv__GET", g->get("_GET"));
  static_global_variables.set("gv__POST", g->get("_POST"));
  static_global_variables.set("gv__COOKIE", g->get("_COOKIE"));
  static_global_variables.set("gv__FILES", g->get("_FILES"));
  static_global_variables.set("gv__ENV", g->get("_ENV"));
  static_global_variables.set("gv__REQUEST", g->get("_REQUEST"));
  static_global_variables.set("gv__SESSION", g->get("_SESSION"));
  static_global_variables.set("gv_HTTP_RAW_POST_DATA", g->get("HTTP_RAW_POST_DATA"));
  static_global_variables.set("gv_http_response_header", g->get("http_response_header"));
  static_global_variables.set("gv_windows", g->get("windows"));
  static_global_variables.set("gv_book_closed_xpm", g->get("book_closed_xpm"));
  static_global_variables.set("gv_book_open_xpm", g->get("book_open_xpm"));
  static_global_variables.set("gv_ctree_data", g->get("ctree_data"));
  static_global_variables.set("gv_book_open", g->get("book_open"));
  static_global_variables.set("gv_book_open_mask", g->get("book_open_mask"));
  static_global_variables.set("gv_book_closed", g->get("book_closed"));
  static_global_variables.set("gv_book_closed_mask", g->get("book_closed_mask"));
  static_global_variables.set("gv_sample_notebook", g->get("sample_notebook"));
  String s = f_json_encode(static_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_dynamic_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_global_variables = json_decode('");
  Array dynamic_global_variables;
  dynamic_global_variables = *get_variable_table();
  String s = f_json_encode(dynamic_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_variables = json_decode('");
  Array method_static_variables;
  method_static_variables.set("sv_insert_row_clist_DupIdstyle2", g->sv_insert_row_clist_DupIdstyle2);
  method_static_variables.set("sv_insert_row_clist_DupIdstyle3", g->sv_insert_row_clist_DupIdstyle3);
  method_static_variables.set("sv_change_style_DupIdstyle1", g->sv_change_style_DupIdstyle1);
  method_static_variables.set("sv_change_style_DupIdstyle2", g->sv_change_style_DupIdstyle2);
  method_static_variables.set("sv_create_event_watcher_DupIdevent_watcher_leave_id", g->sv_create_event_watcher_DupIdevent_watcher_leave_id);
  method_static_variables.set("sv_create_event_watcher_DupIdevent_watcher_enter_id", g->sv_create_event_watcher_DupIdevent_watcher_enter_id);
  method_static_variables.set("sv_create_dialog_DupIdlabel", g->sv_create_dialog_DupIdlabel);
  method_static_variables.set("sv_create_event_watcher_DupIddialog", g->sv_create_event_watcher_DupIddialog);
  method_static_variables.set("sv_insert_row_clist_DupIdstyle1", g->sv_insert_row_clist_DupIdstyle1);
  String s = f_json_encode(method_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_inited(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_inited = json_decode('");
  Array method_static_inited;
  method_static_inited.set("inited_sv_insert_row_clist_DupIdstyle2", g->inited_sv_insert_row_clist_DupIdstyle2);
  method_static_inited.set("inited_sv_insert_row_clist_DupIdstyle3", g->inited_sv_insert_row_clist_DupIdstyle3);
  method_static_inited.set("inited_sv_change_style_DupIdstyle1", g->inited_sv_change_style_DupIdstyle1);
  method_static_inited.set("inited_sv_change_style_DupIdstyle2", g->inited_sv_change_style_DupIdstyle2);
  method_static_inited.set("inited_sv_create_event_watcher_DupIdevent_watcher_leave_id", g->inited_sv_create_event_watcher_DupIdevent_watcher_leave_id);
  method_static_inited.set("inited_sv_create_event_watcher_DupIdevent_watcher_enter_id", g->inited_sv_create_event_watcher_DupIdevent_watcher_enter_id);
  method_static_inited.set("inited_sv_create_dialog_DupIdlabel", g->inited_sv_create_dialog_DupIdlabel);
  method_static_inited.set("inited_sv_create_event_watcher_DupIddialog", g->inited_sv_create_event_watcher_DupIddialog);
  method_static_inited.set("inited_sv_insert_row_clist_DupIdstyle1", g->inited_sv_insert_row_clist_DupIdstyle1);
  String s = f_json_encode(method_static_inited);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_class_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$class_static_variables = json_decode('");
  Array class_static_variables;
  String s = f_json_encode(class_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_pseudomain_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$pseudomain_variables = json_decode('");
  Array pseudomain_variables;
  pseudomain_variables.set("run_pm_php$roadsend$tests$gtk_php", g->run_pm_php$roadsend$tests$gtk_php);
  pseudomain_variables.set("run_pm_php$df_lambda", g->run_pm_php$df_lambda);
  String s = f_json_encode(pseudomain_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_functions(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_functions = json_decode('");
  Array redeclared_functions;
  String s = f_json_encode(redeclared_functions);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_classes(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_classes = json_decode('");
  Array redeclared_classes;
  String s = f_json_encode(redeclared_classes);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

void output_global_state(FILE *fp) {
  output_static_global_variables(fp);
  output_dynamic_global_variables(fp);
  output_dynamic_constants(fp);
  output_method_static_variables(fp);
  output_method_static_inited(fp);
  output_class_static_variables(fp);
  output_pseudomain_variables(fp);
  output_redeclared_functions(fp);
  output_redeclared_classes(fp);
}


///////////////////////////////////////////////////////////////////////////////
}
