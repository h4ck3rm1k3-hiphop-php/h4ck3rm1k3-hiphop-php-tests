
#include <cpp/base/hphp.h>
#include <cpp/base/array/zend_array.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


IMPLEMENT_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
GlobalVariables::GlobalVariables() : dummy(false),
  inited_sv_insert_row_clist_DupIdstyle2(false),
  inited_sv_insert_row_clist_DupIdstyle3(false),
  inited_sv_change_style_DupIdstyle1(false),
  inited_sv_change_style_DupIdstyle2(false),
  inited_sv_create_event_watcher_DupIdevent_watcher_leave_id(false),
  inited_sv_create_event_watcher_DupIdevent_watcher_enter_id(false),
  inited_sv_create_dialog_DupIdlabel(false),
  inited_sv_create_event_watcher_DupIddialog(false),
  inited_sv_insert_row_clist_DupIdstyle1(false),
  run_pm_php$roadsend$tests$gtk_php(false),
  run_pm_php$df_lambda(false) {

  // Dynamic Constants

  // Primitive Function/Method Static Variables

  // Primitive Class Static Variables

  // Redeclared Functions

  // Redeclared Classes
}

void GlobalVariables::initialize() {
  SystemGlobals::initialize();
}

void init_static_variables() { ScalarArrays::initialize();}
static ThreadLocalSingleton<GlobalVariables> g_variables;
static IMPLEMENT_THREAD_LOCAL(GlobalArrayWrapper, g_array_wrapper);
GlobalVariables *get_global_variables() {
  return g_variables.get();
}
void init_global_variables() { GlobalVariables::initialize();}
void free_global_variables() {
  g_variables.reset();
  g_array_wrapper.reset();
}
LVariableTable *get_variable_table() { return (LVariableTable*)get_global_variables();}
Globals *get_globals() { return (Globals*)get_global_variables();}
SystemGlobals *get_system_globals() { return (SystemGlobals*)get_global_variables();}
Array get_global_array_wrapper(){ return g_array_wrapper.get();}

///////////////////////////////////////////////////////////////////////////////
}

#ifndef HPHP_BUILD_LIBRARY
int main(int argc, char** argv) {
  return HPHP::execute_program(argc, argv);
}
#endif
