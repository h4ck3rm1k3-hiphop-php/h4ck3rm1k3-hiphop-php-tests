
#include <php/roadsend/tests/gtk.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_GTK_BUTTONBOX_START = "GTK_BUTTONBOX_START";
const StaticString k_GTK_WIN_POS_CENTER = "GTK_WIN_POS_CENTER";
const StaticString k_GTK_SPIN_STEP_FORWARD = "GTK_SPIN_STEP_FORWARD";
const StaticString k_GTK_DEST_DEFAULT_ALL = "GTK_DEST_DEFAULT_ALL";
const StaticString k_GTK_JUSTIFY_FILL = "GTK_JUSTIFY_FILL";
const StaticString k_GTK_BUTTONBOX_SPREAD = "GTK_BUTTONBOX_SPREAD";
const StaticString k_GTK_CTREE_EXPANDER_SQUARE = "GTK_CTREE_EXPANDER_SQUARE";
const StaticString k_GTK_SHADOW_OUT = "GTK_SHADOW_OUT";
const StaticString k_GDK_BUTTON_PRESS_MASK = "GDK_BUTTON_PRESS_MASK";
const StaticString k_GDK_BUTTON3_MASK = "GDK_BUTTON3_MASK";
const StaticString k_GTK_CTREE_LINES_TABBED = "GTK_CTREE_LINES_TABBED";
const StaticString k_GTK_UPDATE_CONTINUOUS = "GTK_UPDATE_CONTINUOUS";
const StaticString k_GDK_EXPOSURE_MASK = "GDK_EXPOSURE_MASK";
const Variant k_GTK_VISIBLE = "GTK_VISIBLE";
const StaticString k_GTK_STATE_NORMAL = "GTK_STATE_NORMAL";
const Variant k_GTK_JUSTIFY_LEFT = "GTK_JUSTIFY_LEFT";
const StaticString k_GTK_PACK_START = "GTK_PACK_START";
const Variant k_GTK_JUSTIFY_RIGHT = "GTK_JUSTIFY_RIGHT";
const StaticString k_GTK_POS_TOP = "GTK_POS_TOP";
const StaticString k_GTK_BUTTONBOX_EDGE = "GTK_BUTTONBOX_EDGE";
const StaticString k_GTK_CTREE_EXPANDER_TRIANGLE = "GTK_CTREE_EXPANDER_TRIANGLE";
const Variant k_GTK_SELECTION_EXTENDED = "GTK_SELECTION_EXTENDED";
const StaticString k_GTK_SELECTION_MULTIPLE = "GTK_SELECTION_MULTIPLE";
const Variant k_GTK_WIN_POS_MOUSE = "GTK_WIN_POS_MOUSE";
const StaticString k_GTK_POLICY_ALWAYS = "GTK_POLICY_ALWAYS";
const StaticString k_GTK_CTREE_EXPANDER_CIRCULAR = "GTK_CTREE_EXPANDER_CIRCULAR";
const StaticString k_GTK_STATE_SELECTED = "GTK_STATE_SELECTED";
const Variant k_GTK_SORT_ASCENDING = "GTK_SORT_ASCENDING";
const StaticString k_GTK_CTREE_EXPANDER_NONE = "GTK_CTREE_EXPANDER_NONE";
const StaticString k_GTK_CTREE_LINES_DOTTED = "GTK_CTREE_LINES_DOTTED";
const Variant k_GTK_SORT_DESCENDING = "GTK_SORT_DESCENDING";
const StaticString k_GTK_SELECTION_BROWSE = "GTK_SELECTION_BROWSE";
const StaticString k_GTK_SPIN_STEP_BACKWARD = "GTK_SPIN_STEP_BACKWARD";
const StaticString k_GTK_JUSTIFY_CENTER = "GTK_JUSTIFY_CENTER";
const Variant k_GDK_ACTION_COPY = "GDK_ACTION_COPY";
const StaticString k_GTK_SELECTION_SINGLE = "GTK_SELECTION_SINGLE";
const StaticString k_GTK_CTREE_LINES_NONE = "GTK_CTREE_LINES_NONE";
const Variant k_GTK_CAN_DEFAULT = "GTK_CAN_DEFAULT";
const StaticString k_GDK_BUTTON1_MASK = "GDK_BUTTON1_MASK";
const Variant k_GTK_SHADOW_IN = "GTK_SHADOW_IN";
const StaticString k_GDK_BUTTON_PRESS = "GDK_BUTTON_PRESS";
const StaticString k_GTK_BUTTONBOX_END = "GTK_BUTTONBOX_END";
const StaticString k_GTK_CTREE_LINES_SOLID = "GTK_CTREE_LINES_SOLID";
const Variant k_GTK_POLICY_AUTOMATIC = "GTK_POLICY_AUTOMATIC";

/* preface starts */
inline Variant df_lambda_7(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_10(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_11(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_12(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_13(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  p1 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_14(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_15(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_16(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: roadsend/tests/gtk.php line 467 */
void f_ctree_toggle_justify(CVarRef v_menu_item, CVarRef v_ctree, Variant v_justification) {
  FUNCTION_INJECTION(ctree_toggle_justify);
  LINE(469,toObject(v_ctree)->o_invoke_few_args("set_column_justification", 0x71ADAC81AA318ADELL, 2, toObject(v_ctree).o_lval("tree_column", 0x5B3170BC9ECDB49ELL), v_justification));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1167 */
void f_clist_click_column(CVarRef v_clist, Variant v_column) {
  FUNCTION_INJECTION(clist_click_column);
  if (equal(v_column, 4LL)) LINE(1170,toObject(v_clist)->o_invoke_few_args("set_column_visibility", 0x2868B570AD140A55LL, 2, v_column, false));
  else if (equal(v_column, toObject(v_clist).o_get("sort_column", 0x31E555E7D71EBDD2LL))) {
    if (equal(toObject(v_clist).o_get("sort_type", 0x4EEE890B8327DB1CLL), k_GTK_SORT_ASCENDING)) LINE(1173,toObject(v_clist)->o_invoke_few_args("set_sort_type", 0x13F19728C07E2FA7LL, 1, k_GTK_SORT_DESCENDING));
    else LINE(1175,toObject(v_clist)->o_invoke_few_args("set_sort_type", 0x13F19728C07E2FA7LL, 1, k_GTK_SORT_ASCENDING));
  }
  else LINE(1177,toObject(v_clist)->o_invoke_few_args("set_sort_column", 0x09E28CA07967193DLL, 1, v_column));
  LINE(1179,toObject(v_clist)->o_invoke_few_args("sort", 0x1F4984938E1DBB2ALL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 185 */
void f_create_ctree() {
  FUNCTION_INJECTION(create_ctree);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Variant &gv_ctree_data __attribute__((__unused__)) = g->GV(ctree_data);
  Variant &gv_book_closed_xpm __attribute__((__unused__)) = g->GV(book_closed_xpm);
  Variant &gv_book_open_xpm __attribute__((__unused__)) = g->GV(book_open_xpm);
  Object v_window;
  Object v_tooltips;
  Variant v_vbox;
  Variant v_hbox;
  Variant v_label;
  Object v_adj;
  Variant v_button;
  Variant v_scrolled_win;
  Variant v_ctree;
  String v_line_style;
  Variant v_bbox;
  Variant v_mbox;
  Variant v_spinner;
  Variant v_check;
  Array v_items1;
  Variant v_omenu1;
  Array v_items2;
  Variant v_omenu2;
  Array v_items3;
  Variant v_omenu3;
  Array v_items4;
  Variant v_omenu4;
  Array v_mini_page_xpm;
  Object v_transparent;
  Variant v_frame;
  Variant v_hbox2;

  {
  }
  if (!(isset(gv_windows, "ctree", 0x7E8EC21694E1D824LL))) {
    (v_window = LINE(478,create_object("gtkwindow", Array())));
    gv_windows.set("ctree", (v_window), 0x7E8EC21694E1D824LL);
    LINE(480,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(481,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "GtkCTree"));
    LINE(482,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_tooltips = LINE(484,create_object("gtktooltips", Array())));
    (v_vbox = LINE(486,create_object("gtkvbox", Array())));
    LINE(487,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_vbox));
    LINE(488,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(490,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(491,v_hbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(492,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_hbox, false));
    LINE(493,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(495,create_object("gtklabel", Array(ArrayInit(1).set(0, "Depth :").create()))));
    LINE(496,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(497,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_adj = LINE(499,create_object("gtkadjustment", Array(ArrayInit(6).set(0, 4LL).set(1, 1LL).set(2, 10LL).set(3, 1LL).set(4, 5LL).set(5, 0LL).create()))));
    gv_ctree_data.set("spin1", (LINE(500,create_object("gtkspinbutton", Array(ArrayInit(3).set(0, v_adj).set(1, 0LL).set(2, 0LL).create())))), 0x6669BDFCF901E624LL);
    LINE(501,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_ctree_data.refvalAt("spin1", 0x6669BDFCF901E624LL), false, true, 5LL));
    LINE(502,gv_ctree_data.rvalAt("spin1", 0x6669BDFCF901E624LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(504,create_object("gtklabel", Array(ArrayInit(1).set(0, "Books :").create()))));
    LINE(505,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(506,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_adj = LINE(508,create_object("gtkadjustment", Array(ArrayInit(6).set(0, 3LL).set(1, 1LL).set(2, 20LL).set(3, 1LL).set(4, 5LL).set(5, 0LL).create()))));
    gv_ctree_data.set("spin2", (LINE(509,create_object("gtkspinbutton", Array(ArrayInit(3).set(0, v_adj).set(1, 0LL).set(2, 0LL).create())))), 0x45C4EFC323C30F0ALL);
    LINE(510,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_ctree_data.refvalAt("spin2", 0x45C4EFC323C30F0ALL), false, true, 5LL));
    LINE(511,gv_ctree_data.rvalAt("spin2", 0x45C4EFC323C30F0ALL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(513,create_object("gtklabel", Array(ArrayInit(1).set(0, "Pages :").create()))));
    LINE(514,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(515,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_adj = LINE(517,create_object("gtkadjustment", Array(ArrayInit(6).set(0, 5LL).set(1, 1LL).set(2, 20LL).set(3, 1LL).set(4, 5LL).set(5, 0LL).create()))));
    gv_ctree_data.set("spin3", (LINE(518,create_object("gtkspinbutton", Array(ArrayInit(3).set(0, v_adj).set(1, 0LL).set(2, 0LL).create())))), 0x37C3B4C95FA52588LL);
    LINE(519,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_ctree_data.refvalAt("spin3", 0x37C3B4C95FA52588LL), false, true, 5LL));
    LINE(520,gv_ctree_data.rvalAt("spin3", 0x37C3B4C95FA52588LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(522,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Close").create()))));
    LINE(523,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(524,v_hbox.o_invoke_few_args("pack_end", 0x541A6F51B50E1593LL, 1, v_button));
    LINE(525,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(527,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Rebuild Tree").create()))));
    LINE(528,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(529,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_scrolled_win = LINE(531,create_object("gtkscrolledwindow", Array())));
    LINE(532,v_scrolled_win.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(533,v_scrolled_win.o_invoke_few_args("set_policy", 0x1AECF3B49AF6B58BLL, 2, k_GTK_POLICY_AUTOMATIC, k_GTK_POLICY_ALWAYS));
    LINE(534,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_scrolled_win));
    LINE(535,v_scrolled_win.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_ctree = LINE(537,create_object("gtkctree", Array(ArrayInit(3).set(0, 2LL).set(1, 0LL).set(2, ScalarArrays::sa_[5]).create()))));
    LINE(538,v_scrolled_win.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_ctree));
    LINE(539,v_ctree.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(541,v_ctree.o_invoke_few_args("set_column_auto_resize", 0x618A1CA716F606FALL, 2, 0LL, true));
    LINE(542,v_ctree.o_invoke_few_args("set_column_width", 0x1A3C26F06D5680B0LL, 2, 1LL, 200LL));
    LINE(543,v_ctree.o_invoke_few_args("set_selection_mode", 0x58B77CEABCFA5844LL, 1, k_GTK_SELECTION_EXTENDED));
    LINE(544,v_ctree.o_invoke_few_args("set_line_style", 0x669988BAEAB70E2ALL, 1, k_GTK_CTREE_LINES_DOTTED));
    (v_line_style = k_GTK_CTREE_LINES_DOTTED);
    LINE(547,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "rebuild_tree", v_ctree));
    LINE(548,v_ctree.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "click_column", "ctree_click_column"));
    LINE(550,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "button_press_event", "after_press"));
    LINE(551,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "button_release_event", "after_press"));
    LINE(552,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "tree_move", "after_press"));
    LINE(553,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "end_selection", "after_press"));
    LINE(554,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "toggle_focus_row", "after_press"));
    LINE(555,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "select_all", "after_press"));
    LINE(556,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "unselect_all", "after_press"));
    LINE(557,v_ctree.o_invoke_few_args("connect_after", 0x1D6CD94C7937A0ECLL, 2, "scroll_vertical", "after_press"));
    (v_bbox = LINE(559,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(560,v_bbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(561,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_bbox, false));
    LINE(562,v_bbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_mbox = LINE(564,create_object("gtkvbox", Array(ArrayInit(2).set(0, true).set(1, 5LL).create()))));
    LINE(565,v_bbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_mbox, false));
    LINE(566,v_mbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(568,create_object("gtklabel", Array(ArrayInit(1).set(0, "Row Height :").create()))));
    LINE(569,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_label, false, false));
    LINE(570,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(572,create_object("gtklabel", Array(ArrayInit(1).set(0, "Indent :").create()))));
    LINE(573,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_label, false, false));
    LINE(574,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(576,create_object("gtklabel", Array(ArrayInit(1).set(0, "Spacing :").create()))));
    LINE(577,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_label, false, false));
    LINE(578,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_mbox = LINE(580,create_object("gtkvbox", Array(ArrayInit(2).set(0, true).set(1, 5LL).create()))));
    LINE(581,v_bbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_mbox, false));
    LINE(582,v_mbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_adj = LINE(584,create_object("gtkadjustment", Array(ArrayInit(6).set(0, 20LL).set(1, 12LL).set(2, 100LL).set(3, 1LL).set(4, 10LL).set(5, 0LL).create()))));
    (v_spinner = LINE(585,create_object("gtkspinbutton", Array(ArrayInit(3).set(0, v_adj).set(1, 0LL).set(2, 0LL).create()))));
    LINE(586,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_spinner, "Row height of list items.", ""));
    LINE(587,v_adj->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "value_changed", "change_row_height", v_ctree));
    LINE(588,v_ctree.o_invoke_few_args("set_row_height", 0x707817F826E2CF49LL, 1, v_adj.o_lval("value", 0x69E7413AE0C88471LL)));
    LINE(589,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_spinner, false, false, 5LL));
    LINE(590,v_spinner.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_adj = LINE(592,create_object("gtkadjustment", Array(ArrayInit(6).set(0, 20LL).set(1, 0LL).set(2, 60LL).set(3, 1LL).set(4, 10LL).set(5, 0LL).create()))));
    (v_spinner = LINE(593,create_object("gtkspinbutton", Array(ArrayInit(3).set(0, v_adj).set(1, 0LL).set(2, 0LL).create()))));
    LINE(594,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_spinner, "Tree indentation.", ""));
    LINE(595,v_adj->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "value_changed", "change_indent", v_ctree));
    LINE(596,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_spinner, false, false, 5LL));
    LINE(597,v_spinner.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_adj = LINE(599,create_object("gtkadjustment", Array(ArrayInit(6).set(0, 5LL).set(1, 0LL).set(2, 60LL).set(3, 1LL).set(4, 10LL).set(5, 0LL).create()))));
    (v_spinner = LINE(600,create_object("gtkspinbutton", Array(ArrayInit(3).set(0, v_adj).set(1, 0LL).set(2, 0LL).create()))));
    LINE(601,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_spinner, "Tree spacing.", ""));
    LINE(602,v_adj->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "value_changed", "change_spacing", v_ctree));
    LINE(603,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_spinner, false, false, 5LL));
    LINE(604,v_spinner.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_mbox = LINE(606,create_object("gtkvbox", Array(ArrayInit(2).set(0, true).set(1, 5LL).create()))));
    LINE(607,v_bbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_mbox, false));
    LINE(608,v_mbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(610,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(611,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_hbox, false, false));
    LINE(612,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(614,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Expand All").create()))));
    LINE(615,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "expand_all", v_ctree));
    LINE(616,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(617,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(619,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Collapse All").create()))));
    LINE(620,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "collapse_all", v_ctree));
    LINE(621,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(622,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(624,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Change Style").create()))));
    LINE(625,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "change_style", v_ctree));
    LINE(626,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(627,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(629,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Export Tree").create()))));
    LINE(630,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "export_ctree", v_ctree));
    LINE(631,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(632,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(634,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(635,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_hbox, false, false));
    LINE(636,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(638,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Select All").create()))));
    LINE(639,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "select_all", v_ctree));
    LINE(640,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(641,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(643,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Unselect All").create()))));
    LINE(644,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "unselect_all", v_ctree));
    LINE(645,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(646,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(648,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Remove Selection").create()))));
    LINE(649,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "remove_selection", v_ctree));
    LINE(650,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(651,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_check = LINE(653,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Reorderable").create()))));
    LINE(654,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_check, "Tree items can be reordered by dragging.", ""));
    LINE(655,v_check.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "toggle_reorderable", v_ctree));
    LINE(656,v_check.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
    LINE(657,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_check, false));
    LINE(658,v_check.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(660,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(661,v_mbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_hbox, false, false));
    LINE(662,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_items1 = Array(ArrayInit(4).set(0, "No lines", Array(ArrayInit(3).set(0, "ctree_toggle_line_style").set(1, v_ctree).set(2, k_GTK_CTREE_LINES_NONE).create()), 0x6A06FB184BE6938ELL).set(1, "Solid", Array(ArrayInit(3).set(0, "ctree_toggle_line_style").set(1, v_ctree).set(2, k_GTK_CTREE_LINES_SOLID).create()), 0x0B2091BE31CE252ELL).set(2, "Dotted", Array(ArrayInit(3).set(0, "ctree_toggle_line_style").set(1, v_ctree).set(2, k_GTK_CTREE_LINES_DOTTED).create()), 0x6E58CC67008E7FA7LL).set(3, "Tabbed", Array(ArrayInit(3).set(0, "ctree_toggle_line_style").set(1, v_ctree).set(2, k_GTK_CTREE_LINES_TABBED).create()), 0x5606133D7D9026BDLL).create()));
    (v_omenu1 = LINE(668,f_build_option_menu(v_items1, 2LL)));
    LINE(669,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_omenu1, "The tree's line style.", ""));
    LINE(670,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_omenu1, false));
    LINE(671,v_omenu1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_items2 = Array(ArrayInit(4).set(0, "None", Array(ArrayInit(3).set(0, "ctree_toggle_expander_style").set(1, v_ctree).set(2, k_GTK_CTREE_EXPANDER_NONE).create()), 0x6C0082FF579D15B9LL).set(1, "Square", Array(ArrayInit(3).set(0, "ctree_toggle_expander_style").set(1, v_ctree).set(2, k_GTK_CTREE_EXPANDER_SQUARE).create()), 0x15479282C695A7BFLL).set(2, "Triangle", Array(ArrayInit(3).set(0, "ctree_toggle_expander_style").set(1, v_ctree).set(2, k_GTK_CTREE_EXPANDER_TRIANGLE).create()), 0x31B16DCC1910E7CCLL).set(3, "Circular", Array(ArrayInit(3).set(0, "ctree_toggle_expander_style").set(1, v_ctree).set(2, k_GTK_CTREE_EXPANDER_CIRCULAR).create()), 0x0C698999AFD0BE36LL).create()));
    (v_omenu2 = LINE(677,f_build_option_menu(v_items2, 2LL)));
    LINE(678,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_omenu1, "The tree's expander style.", ""));
    LINE(679,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_omenu2, false));
    LINE(680,v_omenu2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_items3 = Array(ArrayInit(2).set(0, "Left", Array(ArrayInit(3).set(0, "ctree_toggle_justify").set(1, v_ctree).set(2, k_GTK_JUSTIFY_LEFT).create()), 0x23D65491476448CDLL).set(1, "Right", Array(ArrayInit(3).set(0, "ctree_toggle_justify").set(1, v_ctree).set(2, k_GTK_JUSTIFY_RIGHT).create()), 0x43C1C88D19A6325DLL).create()));
    (v_omenu3 = LINE(684,f_build_option_menu(v_items3, 0LL)));
    LINE(685,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_omenu1, "The tree's justification.", ""));
    LINE(686,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_omenu3, false));
    LINE(687,v_omenu3.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_items4 = Array(ArrayInit(4).set(0, "Single", Array(ArrayInit(3).set(0, "ctree_toggle_sel_mode").set(1, v_ctree).set(2, k_GTK_SELECTION_SINGLE).create()), 0x726BCBE85D2C77A2LL).set(1, "Browse", Array(ArrayInit(3).set(0, "ctree_toggle_sel_mode").set(1, v_ctree).set(2, k_GTK_SELECTION_BROWSE).create()), 0x710F8064EF78E0D2LL).set(2, "Multiple", Array(ArrayInit(3).set(0, "ctree_toggle_sel_mode").set(1, v_ctree).set(2, k_GTK_SELECTION_MULTIPLE).create()), 0x2C658B30C9A9AB09LL).set(3, "Extended", Array(ArrayInit(3).set(0, "ctree_toggle_sel_mode").set(1, v_ctree).set(2, k_GTK_SELECTION_EXTENDED).create()), 0x454511F05F5CFEE2LL).create()));
    (v_omenu4 = LINE(693,f_build_option_menu(v_items4, 1LL)));
    LINE(694,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_omenu1, "The list's selection mode.", ""));
    LINE(695,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_omenu4, false));
    LINE(696,v_omenu4.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(698,v_window->o_invoke_few_args("realize", 0x44BDBA31DBD5EDE9LL, 0));
    (v_mini_page_xpm = ScalarArrays::sa_[6]);
    (v_transparent = LINE(721,create_object("gdkcolor", Array(ArrayInit(3).set(0, 0LL).set(1, 0LL).set(2, 0LL).create()))));
    df_lambda_7(LINE(723,throw_fatal("unknown class gdk", ((void*)NULL))), lval(gv_ctree_data.lvalAt("pixmap1", 0x1449CA4F8DE6D108LL)), lval(gv_ctree_data.lvalAt("mask1", 0x71EDD24E049DCECFLL)));
    df_lambda_8(LINE(724,throw_fatal("unknown class gdk", ((void*)NULL))), lval(gv_ctree_data.lvalAt("pixmap2", 0x07E5100FC93F7667LL)), lval(gv_ctree_data.lvalAt("mask2", 0x6FC4A765C6C6C1C1LL)));
    df_lambda_9(LINE(725,throw_fatal("unknown class gdk", ((void*)NULL))), lval(gv_ctree_data.lvalAt("pixmap3", 0x1B3134F7F59D6D5DLL)), lval(gv_ctree_data.lvalAt("mask3", 0x6A18099C4F24514CLL)));
    LINE(727,v_ctree.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 0LL, 300LL));
    (v_frame = LINE(729,create_object("gtkframe", Array())));
    LINE(730,v_frame.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    LINE(731,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_OUT));
    LINE(732,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_frame, false));
    LINE(733,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(735,create_object("gtkhbox", Array(ArrayInit(2).set(0, true).set(1, 2LL).create()))));
    LINE(736,v_hbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 2LL));
    LINE(737,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_hbox));
    LINE(738,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(740,create_object("gtkframe", Array())));
    LINE(741,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_IN));
    LINE(742,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_frame, false));
    LINE(743,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox2 = LINE(745,create_object("gtkhbox", Array())));
    LINE(746,v_hbox2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 2LL));
    LINE(747,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_hbox2));
    LINE(748,v_hbox2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(750,create_object("gtklabel", Array(ArrayInit(1).set(0, "Books :").create()))));
    LINE(751,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(752,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    gv_ctree_data.set("book_label", (LINE(754,create_object("gtklabel", Array(ArrayInit(1).set(0, gv_ctree_data.rvalAt("books", 0x06D8B08F90033F8ALL)).create())))), 0x089E6E0E96C4AED3LL);
    LINE(755,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_ctree_data.refvalAt("book_label", 0x089E6E0E96C4AED3LL), false, true, 5LL));
    LINE(756,gv_ctree_data.rvalAt("book_label", 0x089E6E0E96C4AED3LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(758,create_object("gtkframe", Array())));
    LINE(759,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_IN));
    LINE(760,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_frame, false));
    LINE(761,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox2 = LINE(763,create_object("gtkhbox", Array())));
    LINE(764,v_hbox2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 2LL));
    LINE(765,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_hbox2));
    LINE(766,v_hbox2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(768,create_object("gtklabel", Array(ArrayInit(1).set(0, "Pages :").create()))));
    LINE(769,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(770,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    gv_ctree_data.set("page_label", (LINE(772,create_object("gtklabel", Array(ArrayInit(1).set(0, gv_ctree_data.rvalAt("pages", 0x3EA464DEB0A970C3LL)).create())))), 0x7F70E039D07BD186LL);
    LINE(773,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_ctree_data.refvalAt("page_label", 0x7F70E039D07BD186LL), false, true, 5LL));
    LINE(774,gv_ctree_data.rvalAt("page_label", 0x7F70E039D07BD186LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(776,create_object("gtkframe", Array())));
    LINE(777,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_IN));
    LINE(778,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_frame, false));
    LINE(779,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox2 = LINE(781,create_object("gtkhbox", Array())));
    LINE(782,v_hbox2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 2LL));
    LINE(783,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_hbox2));
    LINE(784,v_hbox2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(786,create_object("gtklabel", Array(ArrayInit(1).set(0, "Selected :").create()))));
    LINE(787,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(788,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    gv_ctree_data.set("sel_label", (LINE(790,create_object("gtklabel", Array(ArrayInit(1).set(0, x_count(v_ctree.o_get("selection", 0x2781B225767237F1LL))).create())))), 0x0DF3C45A7479FE27LL);
    LINE(791,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_ctree_data.refvalAt("sel_label", 0x0DF3C45A7479FE27LL), false, true, 5LL));
    LINE(792,gv_ctree_data.rvalAt("sel_label", 0x0DF3C45A7479FE27LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(794,create_object("gtkframe", Array())));
    LINE(795,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_IN));
    LINE(796,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_frame, false));
    LINE(797,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox2 = LINE(799,create_object("gtkhbox", Array())));
    LINE(800,v_hbox2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 2LL));
    LINE(801,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_hbox2));
    LINE(802,v_hbox2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(804,create_object("gtklabel", Array(ArrayInit(1).set(0, "Visible :").create()))));
    LINE(805,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(806,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    gv_ctree_data.set("vis_label", (LINE(808,create_object("gtklabel", Array(ArrayInit(1).set(0, v_ctree.o_get("clist", 0x2CC693377BF4FAB3LL).o_get("rows", 0x3107565F03C446C8LL)).create())))), 0x3036EDF1F4FBE6A8LL);
    LINE(809,v_hbox2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_ctree_data.refvalAt("vis_label", 0x3036EDF1F4FBE6A8LL), false, true, 5LL));
    LINE(810,gv_ctree_data.rvalAt("vis_label", 0x3036EDF1F4FBE6A8LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(812,f_rebuild_tree(null, v_ctree));
  }
  if (toBoolean(bitwise_and(LINE(814,gv_windows.rvalAt("ctree", 0x7E8EC21694E1D824LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(815,gv_windows.rvalAt("ctree", 0x7E8EC21694E1D824LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(817,gv_windows.rvalAt("ctree", 0x7E8EC21694E1D824LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1034 */
void f_create_radio_buttons() {
  FUNCTION_INJECTION(create_radio_buttons);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_box1;
  Variant v_box2;
  Variant v_button1;
  int64 v_i = 0;
  Variant v_button;
  Variant v_separator;

  if (!(isset(gv_windows, "radio_buttons", 0x30715914B56681CELL))) {
    (v_window = LINE(1039,create_object("gtkwindow", Array())));
    gv_windows.set("radio_buttons", (v_window), 0x30715914B56681CELL);
    LINE(1041,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1042,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "radio buttons"));
    LINE(1043,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_box1 = LINE(1045,create_object("gtkvbox", Array())));
    LINE(1046,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(1047,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1049,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1050,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1051,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_box2));
    LINE(1052,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button1 = LINE(1054,create_object("gtkradiobutton", Array(ArrayInit(2).set(0, null).set(1, "button1").create()))));
    LINE(1055,v_button1.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
    LINE(1056,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button1));
    LINE(1057,v_button1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    {
      LOOP_COUNTER(1);
      for ((v_i = 2LL); not_more(v_i, 4LL); v_i++) {
        LOOP_COUNTER_CHECK(1);
        {
          (v_button = LINE(1059,create_object("gtkradiobutton", Array(ArrayInit(2).set(0, v_button1).set(1, concat("button", toString(v_i))).create()))));
          LINE(1060,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
          LINE(1061,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
        }
      }
    }
    (v_separator = LINE(1064,create_object("gtkhseparator", Array())));
    LINE(1065,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1066,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1068,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1069,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1070,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(1071,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1073,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1074,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1075,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1076,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1077,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1078,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1080,gv_windows.rvalAt("radio_buttons", 0x30715914B56681CELL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1081,gv_windows.rvalAt("radio_buttons", 0x30715914B56681CELL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1083,gv_windows.rvalAt("radio_buttons", 0x30715914B56681CELL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 62 */
void f_close_window(CVarRef v_widget) {
  FUNCTION_INJECTION(close_window);
  Variant v_window;

  (v_window = LINE(64,toObject(v_widget)->o_invoke_few_args("get_toplevel", 0x32E7C31C44204A52LL, 0)));
  LINE(65,v_window.o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 300 */
void f_after_press(CVarRef v_ctree) {
  FUNCTION_INJECTION(after_press);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ctree_data __attribute__((__unused__)) = g->GV(ctree_data);
  LINE(304,gv_ctree_data.rvalAt("sel_label", 0x0DF3C45A7479FE27LL).o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, x_count(toObject(v_ctree).o_get("selection", 0x2781B225767237F1LL))));
  LINE(305,gv_ctree_data.rvalAt("vis_label", 0x3036EDF1F4FBE6A8LL).o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, toObject(v_ctree).o_get("clist", 0x2CC693377BF4FAB3LL).o_lval("rows", 0x3107565F03C446C8LL)));
  LINE(306,gv_ctree_data.rvalAt("book_label", 0x089E6E0E96C4AED3LL).o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, gv_ctree_data.refvalAt("books", 0x06D8B08F90033F8ALL)));
  LINE(307,gv_ctree_data.rvalAt("page_label", 0x7F70E039D07BD186LL).o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, gv_ctree_data.refvalAt("pages", 0x3EA464DEB0A970C3LL)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 111 */
void f_dnd_drag_data_received(CVarRef v_widget, CVarRef v_context, CVarRef v_x, CVarRef v_y, CVarRef v_data, CVarRef v_info, CVarRef v_time) {
  FUNCTION_INJECTION(dnd_drag_data_received);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  if (toBoolean(v_data) && equal(toObject(v_data).o_get("format", 0x4575D5E219281E76LL), 8LL)) print(LINE(114,(assignCallTemp(eo_1, toString(toObject(v_data).o_get("target", 0x22F784C030DE5CB0LL).o_get("string", 0x2027864469AD4382LL))),assignCallTemp(eo_2, concat3(" was:\n'", toString(toObject(v_data).o_get("data", 0x30164401A9853128LL)), "'.\n")),concat3("Drop data of type ", eo_1, eo_2))));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1135 */
void f_create_clist() {
  FUNCTION_INJECTION(create_clist);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_vbox;
  Variant v_scrolled_win;
  Array v_titles;
  Variant v_clist;
  Variant v_hbox;
  Variant v_button;
  Variant v_label;
  Array v_items;
  Variant v_clist_omenu;
  int64 v_clist_rows = 0;
  Variant v_i;
  Variant v_texts;
  Object v_col1;
  Object v_col2;
  Variant v_style;
  Variant v_separator;

  if (!(isset(gv_windows, "clist", 0x2CC693377BF4FAB3LL))) {
    (v_window = LINE(1140,create_object("gtkwindow", Array())));
    gv_windows.set("clist", (v_window), 0x2CC693377BF4FAB3LL);
    LINE(1142,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1143,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "CList"));
    LINE(1144,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_vbox = LINE(1146,create_object("gtkvbox", Array())));
    LINE(1147,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_vbox));
    LINE(1148,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_scrolled_win = LINE(1150,create_object("gtkscrolledwindow", Array())));
    LINE(1151,v_scrolled_win.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(1152,v_scrolled_win.o_invoke_few_args("set_policy", 0x1AECF3B49AF6B58BLL, 2, k_GTK_POLICY_AUTOMATIC, k_GTK_POLICY_AUTOMATIC));
    (v_titles = ScalarArrays::sa_[7]);
    (v_clist = LINE(1157,create_object("gtkclist", Array(ArrayInit(2).set(0, 12LL).set(1, v_titles).create()))));
    LINE(1158,v_clist.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "click_column", "clist_click_column"));
    LINE(1159,v_scrolled_win.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_clist));
    LINE(1160,v_clist.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(1162,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(1163,v_hbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(1164,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_hbox, false, false));
    LINE(1165,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1219,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Insert Row").create()))));
    LINE(1220,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "insert_row_clist", v_clist));
    LINE(1221,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1222,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1326,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Add 1,000 Rows With Pixmaps").create()))));
    LINE(1327,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "add1000_clist", v_clist));
    LINE(1328,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1329,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1331,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Add 10,000 Rows").create()))));
    LINE(1332,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "add10000_clist", v_clist));
    LINE(1333,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1334,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(1336,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(1337,v_hbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(1338,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_hbox, false, false));
    LINE(1339,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1341,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Clear List").create()))));
    LINE(1342,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_clist).set(1, "clear").create())));
    LINE(1343,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1344,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1346,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Remove Selection").create()))));
    LINE(1347,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "clist_remove_selection", v_clist));
    LINE(1348,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1349,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1351,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Undo Selection").create()))));
    LINE(1352,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_clist).set(1, "undo_selection").create())));
    LINE(1353,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1354,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(1356,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(1357,v_hbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(1358,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_hbox, false, false));
    LINE(1359,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1361,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Show Title Buttons").create()))));
    LINE(1362,v_button.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
    LINE(1363,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "toggle_title_buttons", v_clist));
    LINE(1364,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_button, false));
    LINE(1365,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1367,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Reorderable").create()))));
    LINE(1368,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "toggle_reorderable", v_clist));
    LINE(1369,v_button.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
    LINE(1370,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_button, false));
    LINE(1371,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(1373,create_object("gtklabel", Array(ArrayInit(1).set(0, "Selection Mode: ").create()))));
    LINE(1374,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(1375,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_items = Array(ArrayInit(4).set(0, "Single", Array(ArrayInit(3).set(0, "clist_toggle_sel_mode").set(1, v_clist).set(2, k_GTK_SELECTION_SINGLE).create()), 0x726BCBE85D2C77A2LL).set(1, "Browse", Array(ArrayInit(3).set(0, "clist_toggle_sel_mode").set(1, v_clist).set(2, k_GTK_SELECTION_BROWSE).create()), 0x710F8064EF78E0D2LL).set(2, "Multiple", Array(ArrayInit(3).set(0, "clist_toggle_sel_mode").set(1, v_clist).set(2, k_GTK_SELECTION_MULTIPLE).create()), 0x2C658B30C9A9AB09LL).set(3, "Extended", Array(ArrayInit(3).set(0, "clist_toggle_sel_mode").set(1, v_clist).set(2, k_GTK_SELECTION_EXTENDED).create()), 0x454511F05F5CFEE2LL).create()));
    (v_clist_omenu = LINE(1386,f_build_option_menu(v_items, 3LL)));
    LINE(1387,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_clist_omenu));
    LINE(1388,v_clist_omenu.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1390,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_scrolled_win));
    LINE(1391,v_scrolled_win.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1393,v_clist.o_invoke_few_args("set_row_height", 0x707817F826E2CF49LL, 1, 18LL));
    LINE(1394,v_clist.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, -1LL, 300LL));
    (v_clist_rows = 0LL);
    {
      LOOP_COUNTER(2);
      for ((v_i = 1LL); less(v_i, 12LL); v_i++) {
        LOOP_COUNTER_CHECK(2);
        LINE(1398,v_clist.o_invoke_few_args("set_column_width", 0x1A3C26F06D5680B0LL, 2, v_i, 80LL));
      }
    }
    LINE(1400,v_clist.o_invoke_few_args("set_column_auto_resize", 0x618A1CA716F606FALL, 2, 0LL, true));
    LINE(1401,v_clist.o_invoke_few_args("set_column_resizeable", 0x77B4F7A77FEB6813LL, 2, 1LL, false));
    LINE(1402,v_clist.o_invoke_few_args("set_column_max_width", 0x0B4A65EF3D465BC9LL, 2, 2LL, 100LL));
    LINE(1403,v_clist.o_invoke_few_args("set_column_min_width", 0x076EB91019EB97A5LL, 2, 3LL, 50LL));
    LINE(1404,v_clist.o_invoke_few_args("set_selection_mode", 0x58B77CEABCFA5844LL, 1, k_GTK_SELECTION_EXTENDED));
    LINE(1405,v_clist.o_invoke_few_args("set_column_justification", 0x71ADAC81AA318ADELL, 2, 1LL, k_GTK_JUSTIFY_RIGHT));
    LINE(1406,v_clist.o_invoke_few_args("set_column_justification", 0x71ADAC81AA318ADELL, 2, 2LL, k_GTK_JUSTIFY_CENTER));
    {
      LOOP_COUNTER(3);
      for ((v_i = 0LL); less(v_i, 12LL); v_i++) {
        LOOP_COUNTER_CHECK(3);
        v_texts.set(v_i, (toString("Column ") + toString(v_i)));
      }
    }
    v_texts.set(1LL, ("Right"), 0x5BCA7C69B794F8CELL);
    v_texts.set(2LL, ("Center"), 0x486AFCC090D5F98CLL);
    (v_col1 = LINE(1414,create_object("gdkcolor", Array(ArrayInit(3).set(0, 56000LL).set(1, 0LL).set(2, 0LL).create()))));
    (v_col2 = LINE(1415,create_object("gdkcolor", Array(ArrayInit(3).set(0, 0LL).set(1, 56000LL).set(2, 32000LL).create()))));
    (v_style = LINE(1416,create_object("gtkstyle", Array())));
    lval(v_style.o_lval("fg", 0x297040659785B670LL)).set(k_GTK_STATE_NORMAL, (v_col1));
    lval(v_style.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_col2));
    (v_style.o_lval("font", 0x7C274F8B4A4D4058LL) = LINE(1419,throw_fatal("unknown class gdk", ((void*)NULL))));
    {
      LOOP_COUNTER(4);
      for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
        LOOP_COUNTER_CHECK(4);
        {
          v_texts.set(0LL, (LINE(1422,(assignCallTemp(eo_1, v_clist_rows++),x_sprintf(2, "CListRow %d", Array(ArrayInit(1).set(0, eo_1).create()))))), 0x77CFA1EEF01BCA90LL);
          LINE(1423,v_clist.o_invoke_few_args("append", 0x4DEE4A472DC69EC2LL, 1, v_texts));
          {
            switch (toInt64(modulo(toInt64(v_i), 4LL))) {
            case 2LL:
              {
                LINE(1427,v_clist.o_invoke_few_args("set_row_style", 0x734655A8199F6996LL, 2, v_i, v_style));
                break;
              }
            default:
              {
                LINE(1431,v_clist.o_invoke_few_args("set_cell_style", 0x0CDF776A336AA828LL, 3, v_i, modulo(toInt64(v_i), 4LL), v_style));
                break;
              }
            }
          }
        }
      }
    }
    (v_separator = LINE(1436,create_object("gtkhseparator", Array())));
    LINE(1437,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1438,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(1440,create_object("gtkhbox", Array())));
    LINE(1441,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_hbox, false));
    LINE(1442,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1444,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1445,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1446,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1447,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1448,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1449,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1452,gv_windows.rvalAt("clist", 0x2CC693377BF4FAB3LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1453,gv_windows.rvalAt("clist", 0x2CC693377BF4FAB3LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1455,gv_windows.rvalAt("clist", 0x2CC693377BF4FAB3LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 98 */
void f_create_dnd() {
  FUNCTION_INJECTION(create_dnd);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Variant v_targets;
  Object v_window;
  Variant v_box1;
  Variant v_box2;
  Variant v_frame;
  Variant v_box3;
  Variant v_button;
  Variant v_separator;

  if (!(isset(gv_windows, "dnd", 0x7D657FB314E54A8FLL))) {
    (v_targets = ScalarArrays::sa_[3]);
    (v_window = LINE(117,create_object("gtkwindow", Array())));
    gv_windows.set("dnd", (v_window), 0x7D657FB314E54A8FLL);
    LINE(119,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(120,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Drag-n-Drop"));
    (v_box1 = LINE(122,create_object("gtkvbox", Array())));
    LINE(123,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(124,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(126,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(127,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(128,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_box2));
    LINE(129,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(131,create_object("gtkframe", Array(ArrayInit(1).set(0, "Drag").create()))));
    LINE(132,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_frame));
    LINE(133,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box3 = LINE(135,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(136,v_box3.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(137,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box3));
    LINE(138,v_box3.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(140,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Drag me!").create()))));
    LINE(141,v_box3.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(142,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(143,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "drag_data_get", "dnd_drag_data_get"));
    (assignCallTemp(eo_0, toObject(v_button)),LINE(145,eo_0.o_invoke_few_args("drag_source_set", 0x5FC29FF088140794LL, 3, bitwise_or(k_GDK_BUTTON1_MASK, k_GDK_BUTTON3_MASK), v_targets, k_GDK_ACTION_COPY)));
    (v_frame = LINE(147,create_object("gtkframe", Array(ArrayInit(1).set(0, "Drop").create()))));
    LINE(148,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_frame));
    LINE(149,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box3 = LINE(151,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(152,v_box3.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(153,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box3));
    LINE(154,v_box3.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(156,create_object("gtkbutton", Array(ArrayInit(1).set(0, "To").create()))));
    LINE(157,v_box3.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(158,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(159,v_button.o_invoke_few_args("realize", 0x44BDBA31DBD5EDE9LL, 0));
    LINE(160,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "drag_data_received", "dnd_drag_data_received"));
    LINE(161,v_button.o_invoke_few_args("drag_dest_set", 0x2C1B1C985387B825LL, 3, k_GTK_DEST_DEFAULT_ALL, v_targets, k_GDK_ACTION_COPY));
    (v_separator = LINE(163,create_object("gtkhseparator", Array())));
    LINE(164,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(165,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(167,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(168,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(169,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(170,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(172,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(173,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(174,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(175,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(176,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(177,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(179,gv_windows.rvalAt("dnd", 0x7D657FB314E54A8FLL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(180,gv_windows.rvalAt("dnd", 0x7D657FB314E54A8FLL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(182,gv_windows.rvalAt("dnd", 0x7D657FB314E54A8FLL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 396 */
void f_count_items(CVarRef v_ctree, CVarRef v_node) {
  FUNCTION_INJECTION(count_items);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ctree_data __attribute__((__unused__)) = g->GV(ctree_data);
  if (toBoolean(toObject(v_node).o_get("is_leaf", 0x270D3D1D2AB727B3LL))) lval(gv_ctree_data.lvalAt("pages", 0x3EA464DEB0A970C3LL))--;
  else lval(gv_ctree_data.lvalAt("books", 0x06D8B08F90033F8ALL))--;
} /* function */
/* SRC: roadsend/tests/gtk.php line 880 */
void f_create_cursor_test() {
  FUNCTION_INJECTION(create_cursor_test);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_main_vbox;
  Variant v_vbox;
  Variant v_hbox;
  Variant v_label;
  Variant v_spinner;
  Variant v_frame;
  Variant v_darea;
  Variant v_cur_name;
  Variant v_separator;
  Variant v_button;

  if (!(isset(gv_windows, "cursor_test", 0x1C52EA1E6D508D31LL))) {
    (v_window = LINE(934,create_object("gtkwindow", Array())));
    gv_windows.set("cursor_test", (v_window), 0x1C52EA1E6D508D31LL);
    LINE(936,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(937,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Cursor Test"));
    LINE(938,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_main_vbox = LINE(940,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(941,v_main_vbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    LINE(942,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_main_vbox));
    LINE(943,v_main_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_vbox = LINE(945,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(946,v_vbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(947,v_main_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_vbox));
    LINE(948,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(950,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(951,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_hbox, false));
    LINE(952,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_label = LINE(954,create_object("gtklabel", Array(ArrayInit(1).set(0, "Cursor value: ").create()))));
    LINE(955,v_label.o_invoke_few_args("set_alignment", 0x6045548A953AA9B0LL, 2, 0LL, 0.5));
    LINE(956,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_label, false));
    LINE(957,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_spinner = LINE(959,(assignCallTemp(eo_0, create_object("gtkadjustment", Array(ArrayInit(6).set(0, 0LL).set(1, 0LL).set(2, 152LL).set(3, 2LL).set(4, 10LL).set(5, 0LL).create()))),create_object("gtkspinbutton", Array(ArrayInit(3).set(0, eo_0).set(1, 0LL).set(2, 0LL).create())))));
    LINE(960,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_spinner));
    LINE(961,v_spinner.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(963,create_object("gtkframe", Array(ArrayInit(1).set(0, "Cursor Area").create()))));
    LINE(964,v_frame.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(965,v_frame.o_invoke_few_args("set_label_align", 0x32D560903FADCC21LL, 2, 0.5, 0LL));
    LINE(966,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_frame));
    LINE(967,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_darea = LINE(969,create_object("gtkdrawingarea", Array())));
    LINE(970,v_darea.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 80LL, 80LL));
    LINE(971,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_darea));
    LINE(972,v_darea.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_cur_name = LINE(974,create_object("gtklabel", Array(ArrayInit(1).set(0, "").create()))));
    LINE(975,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_cur_name));
    LINE(976,v_cur_name.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(978,v_darea.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "expose_event", "expose_event"));
    LINE(979,v_darea.o_invoke_few_args("add_events", 0x4C2A95E65C3C5D37LL, 1, bitwise_or(k_GDK_EXPOSURE_MASK, k_GDK_BUTTON_PRESS_MASK)));
    LINE(980,v_darea.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "button_press_event", "button_press", v_spinner));
    LINE(981,v_spinner.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 4, "changed", "set_cursor", v_darea, v_cur_name));
    (v_separator = LINE(983,create_object("gtkhseparator", Array())));
    LINE(984,v_main_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(985,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_vbox = LINE(987,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(988,v_vbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(989,v_main_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_vbox, false));
    LINE(990,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(992,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(993,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(994,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(995,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(996,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(997,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(999,v_darea.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 5, "realize", "set_cursor", v_spinner, v_darea, v_cur_name));
  }
  if (toBoolean(bitwise_and(LINE(1001,gv_windows.rvalAt("cursor_test", 0x1C52EA1E6D508D31LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1002,gv_windows.rvalAt("cursor_test", 0x1C52EA1E6D508D31LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1004,gv_windows.rvalAt("cursor_test", 0x1C52EA1E6D508D31LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1224 */
void f_add1000_clist(CVarRef v_button, CVarRef v_clist) {
  FUNCTION_INJECTION(add1000_clist);
  Array v_gtk_mini_xpm;
  Variant v_pixmap;
  Variant v_mask;
  int64 v_i = 0;
  Variant v_texts;
  Variant v_row;

  (v_gtk_mini_xpm = ScalarArrays::sa_[9]);
  df_lambda_11(LINE(1269,throw_fatal("unknown class gdk", ((void*)NULL))), v_pixmap, v_mask);
  {
    LOOP_COUNTER(6);
    for ((v_i = 0LL); less(v_i, 12LL); v_i++) {
      LOOP_COUNTER_CHECK(6);
      v_texts.set(v_i, (toString("Column ") + toString(v_i)));
    }
  }
  v_texts.set(1LL, ("Right"), 0x5BCA7C69B794F8CELL);
  v_texts.set(2LL, ("Center"), 0x486AFCC090D5F98CLL);
  v_texts.set(3LL, (null), 0x135FDDF6A6BFBBDDLL);
  LINE(1277,toObject(v_clist)->o_invoke_few_args("freeze", 0x6A7D0E8BAA76B7E7LL, 0));
  {
    LOOP_COUNTER(7);
    for ((v_i = 0LL); less(v_i, 1000LL); v_i++) {
      LOOP_COUNTER_CHECK(7);
      {
        v_texts.set(0LL, (concat("CListRow ", (toString(modulo(LINE(1279,x_rand()), 10000LL))))), 0x77CFA1EEF01BCA90LL);
        (v_row = LINE(1280,toObject(v_clist)->o_invoke_few_args("append", 0x4DEE4A472DC69EC2LL, 1, v_texts)));
        LINE(1281,toObject(v_clist)->o_invoke_few_args("set_pixtext", 0x48B32FF57FA0AA64LL, 6, v_row, 3LL, "gtk+", 5LL, v_pixmap, v_mask));
      }
    }
  }
  LINE(1283,toObject(v_clist)->o_invoke_few_args("thaw", 0x64F2171F59D6F356LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2274 */
void f_event_watcher_down(Variant v_event_watcher_enter_id, Variant v_event_watcher_leave_id) {
  FUNCTION_INJECTION(event_watcher_down);
  Variant eo_0;
  Variant eo_1;
  Variant v_signal_id;

  if (toBoolean(v_event_watcher_enter_id)) {
    (v_signal_id = LINE(2278,(assignCallTemp(eo_1, throw_fatal("unknown class gtkwidget", ((void*)NULL))),throw_fatal("unknown class gtk", (throw_fatal("unknown class gtkwidget", ((void*)NULL)), (void*)NULL)))));
    LINE(2279,throw_fatal("unknown class gtk", ((void*)NULL)));
    (v_event_watcher_enter_id = 0LL);
    (v_signal_id = LINE(2281,(assignCallTemp(eo_1, throw_fatal("unknown class gtkwidget", ((void*)NULL))),throw_fatal("unknown class gtk", (throw_fatal("unknown class gtkwidget", ((void*)NULL)), (void*)NULL)))));
    LINE(2282,throw_fatal("unknown class gtk", ((void*)NULL)));
    (v_event_watcher_leave_id = 0LL);
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 2207 */
void f_label_toggle(CVarRef v_dialog, Variant v_label, CVarRef v_dialog0) {
  FUNCTION_INJECTION(label_toggle);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_vbox;

  if (!(toBoolean(v_label))) {
    (v_label = LINE(2211,create_object("gtklabel", Array(ArrayInit(1).set(0, "Dialog Test").create()))));
    LINE(2212,v_label.o_invoke_few_args("set_padding", 0x71D6A16B820D116ALL, 2, 10LL, 10LL));
    (assignCallTemp(eo_1, ref(LINE(2213,"df_lambda_5"))),assignCallTemp(eo_2, ref(v_label)),v_label.o_invoke("connect_object", Array(ArrayInit(3).set(0, "destroy").set(1, eo_1).set(2, eo_2).create()), 0x3E0DE7B55A894443LL));
    (v_vbox = toObject(v_dialog).o_get("vbox", 0x7DA40F8F1BC6EDBFLL));
    LINE(2215,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_label, true, true, 0LL));
    LINE(2216,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  else LINE(2218,v_label.o_invoke_few_args("destroy", 0x45CCFF10463AB662LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 472 */
void f_ctree_toggle_sel_mode(CVarRef v_menu_item, CVarRef v_ctree, Variant v_sel_mode) {
  FUNCTION_INJECTION(ctree_toggle_sel_mode);
  LINE(474,toObject(v_ctree)->o_invoke_few_args("set_selection_mode", 0x58B77CEABCFA5844LL, 1, v_sel_mode));
  LINE(475,f_after_press(v_ctree));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1087 */
void f_create_check_buttons() {
  FUNCTION_INJECTION(create_check_buttons);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_box1;
  Variant v_box2;
  int64 v_i = 0;
  Variant v_button;
  Variant v_separator;

  if (!(isset(gv_windows, "check_buttons", 0x1521821A2BDC9D5ELL))) {
    (v_window = LINE(1092,create_object("gtkwindow", Array())));
    gv_windows.set("check_buttons", (v_window), 0x1521821A2BDC9D5ELL);
    LINE(1094,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1095,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "check buttons"));
    LINE(1096,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_box1 = LINE(1098,create_object("gtkvbox", Array())));
    LINE(1099,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(1100,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1102,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1103,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1104,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_box2));
    LINE(1105,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    {
      LOOP_COUNTER(8);
      for ((v_i = 1LL); not_more(v_i, 4LL); v_i++) {
        LOOP_COUNTER_CHECK(8);
        {
          (v_button = LINE(1108,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, concat("button", toString(v_i))).create()))));
          LINE(1109,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
          LINE(1110,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
        }
      }
    }
    (v_separator = LINE(1113,create_object("gtkhseparator", Array())));
    LINE(1114,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1115,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1117,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1118,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1119,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(1120,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1122,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1123,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1124,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1125,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1126,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1127,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1129,gv_windows.rvalAt("check_buttons", 0x1521821A2BDC9D5ELL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1130,gv_windows.rvalAt("check_buttons", 0x1521821A2BDC9D5ELL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1132,gv_windows.rvalAt("check_buttons", 0x1521821A2BDC9D5ELL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2221 */
void f_create_dialog() {
  FUNCTION_INJECTION(create_dialog);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Variant &sv_label __attribute__((__unused__)) = g->sv_create_dialog_DupIdlabel;
  bool &inited_sv_label __attribute__((__unused__)) = g->inited_sv_create_dialog_DupIdlabel;
  Variant v_dialog;
  Variant v_button;
  Variant v_action_area;

  if (!inited_sv_label) {
    setNull(sv_label);
    inited_sv_label = true;
  }
  if (!(isset(gv_windows, "dialog", 0x526009C9BD054468LL))) {
    (v_dialog = LINE(2227,create_object("gtkdialog", Array())));
    gv_windows.set("dialog", (v_dialog), 0x526009C9BD054468LL);
    LINE(2229,v_dialog.o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "GtkDialog"));
    LINE(2230,v_dialog.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    LINE(2231,v_dialog.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 200LL, 110LL));
    LINE(2232,v_dialog.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete_event", "delete_event"));
    (v_button = LINE(2234,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Ok").create()))));
    LINE(2235,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(2236,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    (v_action_area = v_dialog.o_get("action_area", 0x37BC1B12C5D8C1FDLL));
    LINE(2239,v_action_area.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 0LL));
    LINE(2240,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(2241,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(2243,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Toggle").create()))));
    LINE(2244,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 4, "clicked", "label_toggle", ref(sv_label), v_dialog));
    LINE(2245,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(2246,v_action_area.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 0LL));
    LINE(2247,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(2249,gv_windows.rvalAt("dialog", 0x526009C9BD054468LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(2250,gv_windows.rvalAt("dialog", 0x526009C9BD054468LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(2252,gv_windows.rvalAt("dialog", 0x526009C9BD054468LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 390 */
void f_unselect_all(CVarRef v_button, CVarRef v_ctree) {
  FUNCTION_INJECTION(unselect_all);
  LINE(392,toObject(v_ctree)->o_invoke_few_args("unselect_recursive", 0x0C0D31F4974AA706LL, 0));
  LINE(393,f_after_press(v_ctree));
} /* function */
/* SRC: roadsend/tests/gtk.php line 821 */
void f_create_pixmap() {
  FUNCTION_INJECTION(create_pixmap);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_box1;
  Variant v_box2;
  Variant v_button;
  Variant v_pixmap;
  Variant v_mask;
  Variant v_pixmapwid;
  Variant v_label;
  Variant v_box3;
  Variant v_separator;

  if (!(isset(gv_windows, "pixmap", 0x4EBE6820955A77B5LL))) {
    (v_window = LINE(826,create_object("gtkwindow", Array())));
    gv_windows.set("pixmap", (v_window), 0x4EBE6820955A77B5LL);
    LINE(828,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(829,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "GtkPixmap"));
    LINE(830,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    LINE(831,v_window->o_invoke_few_args("realize", 0x44BDBA31DBD5EDE9LL, 0));
    (v_box1 = LINE(833,create_object("gtkvbox", Array())));
    LINE(834,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(835,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(837,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(838,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(839,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_box2));
    LINE(840,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(842,create_object("gtkbutton", Array())));
    LINE(843,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_button, false, false));
    LINE(844,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    df_lambda_10(LINE(846,throw_fatal("unknown class gdk", ((void*)NULL))), v_pixmap, v_mask);
    (v_pixmapwid = LINE(847,create_object("gtkpixmap", Array(ArrayInit(2).set(0, v_pixmap).set(1, v_mask).create()))));
    (v_label = LINE(848,create_object("gtklabel", Array(ArrayInit(1).set(0, "Pixmap\ntest").create()))));
    (v_box3 = LINE(849,create_object("gtkhbox", Array())));
    LINE(850,v_box3.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 2LL));
    LINE(851,v_box3.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_pixmapwid));
    LINE(852,v_box3.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_label));
    LINE(853,v_pixmapwid.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(854,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(855,v_button.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box3));
    LINE(856,v_box3.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_separator = LINE(858,create_object("gtkhseparator", Array())));
    LINE(859,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(860,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(862,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(863,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(864,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(865,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(867,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(868,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(869,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(870,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(871,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(872,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(874,gv_windows.rvalAt("pixmap", 0x4EBE6820955A77B5LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(875,gv_windows.rvalAt("pixmap", 0x4EBE6820955A77B5LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(877,gv_windows.rvalAt("pixmap", 0x4EBE6820955A77B5LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1795 */
bool f_tips_query_widget_selected(CVarRef v_tips_query, CVarRef v_widget, CVarRef v_tip_text, CVarRef v_tip_private, CVarRef v_event) {
  FUNCTION_INJECTION(tips_query_widget_selected);
  if (toBoolean(v_widget)) {
    print("Help \"");
    print(toString(toBoolean(v_tip_private) ? ((Variant)(v_tip_private)) : ((Variant)("None"))));
    print("\" requested for <");
    print(toString(LINE(1802,throw_fatal("unknown class gtk", (toObject(v_widget)->o_invoke_few_args("get_type", 0x06612A79C65DE888LL, 0), (void*)NULL)))));
    print(">\n");
  }
  return true;
} /* function */
/* SRC: roadsend/tests/gtk.php line 56 */
bool f_delete_event(CVarRef v_window, CVarRef v_event) {
  FUNCTION_INJECTION(delete_event);
  LINE(58,toObject(v_window)->o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  return true;
} /* function */
/* SRC: roadsend/tests/gtk.php line 1943 */
void f_entry_toggle_editable(CVarRef v_check_button, CVarRef v_entry) {
  FUNCTION_INJECTION(entry_toggle_editable);
  LINE(1945,toObject(v_entry)->o_invoke_few_args("set_editable", 0x6F2AD5C32FDEC973LL, 1, toObject(v_check_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 344 */
void f_collapse_all(CVarRef v_button, CVarRef v_ctree) {
  FUNCTION_INJECTION(collapse_all);
  LINE(346,toObject(v_ctree)->o_invoke_few_args("collapse_recursive", 0x4AC08C2E17C5964CLL, 0));
  LINE(347,f_after_press(v_ctree));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2255 */
bool f_event_watcher(CVarRef v_object, CVarRef v_signal_id) {
  FUNCTION_INJECTION(event_watcher);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  echo(concat(concat_rev(toString(LINE(2257,throw_fatal("unknown class gtk", (toObject(v_object)->o_invoke_few_args("get_type", 0x06612A79C65DE888LL, 0), (void*)NULL)))), (assignCallTemp(eo_1, toString(throw_fatal("unknown class gtk", ((void*)NULL)))),concat3("Event watch: ", eo_1, " emitted for "))), "\n"));
  return true;
} /* function */
/* SRC: roadsend/tests/gtk.php line 2172 */
void f_create_file_selection() {
  FUNCTION_INJECTION(create_file_selection);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Variant v_window;
  Variant v_button_ok;
  Variant v_button_cancel;
  Variant v_action_area;
  Variant v_button;

  if (!(isset(gv_windows, "file_selection", 0x356EF7337E4026FELL))) {
    (v_window = LINE(2177,create_object("gtkfileselection", Array(ArrayInit(1).set(0, "File selection dialog").create()))));
    gv_windows.set("file_selection", (v_window), 0x356EF7337E4026FELL);
    LINE(2179,v_window.o_invoke_few_args("hide_fileop_buttons", 0x71D33FCF10BC1557LL, 0));
    LINE(2180,v_window.o_invoke_few_args("set_position", 0x17D027C2B74A20CBLL, 1, k_GTK_WIN_POS_MOUSE));
    LINE(2181,v_window.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete_event", "delete_event"));
    (v_button_ok = v_window.o_get("ok_button", 0x67BDCAE85F751474LL));
    LINE(2184,v_button_ok.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "file_selection_ok", v_window));
    (v_button_cancel = v_window.o_get("cancel_button", 0x289F7B2EC5848D75LL));
    LINE(2187,v_button_cancel.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    (v_action_area = v_window.o_get("action_area", 0x37BC1B12C5D8C1FDLL));
    (v_button = LINE(2191,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Hide Fileops").create()))));
    LINE(2192,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (assignCallTemp(eo_1, ref(LINE(2193,"df_lambda_3"))),assignCallTemp(eo_2, ref(v_window)),v_button.o_invoke("connect_object", Array(ArrayInit(3).set(0, "clicked").set(1, eo_1).set(2, eo_2).create()), 0x3E0DE7B55A894443LL));
    LINE(2194,v_action_area.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, false, false, 0LL));
    (v_button = LINE(2196,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Show Fileops").create()))));
    LINE(2197,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (assignCallTemp(eo_1, ref(LINE(2198,"df_lambda_4"))),assignCallTemp(eo_2, ref(v_window)),v_button.o_invoke("connect_object", Array(ArrayInit(3).set(0, "clicked").set(1, eo_1).set(2, eo_2).create()), 0x3E0DE7B55A894443LL));
    LINE(2199,v_action_area.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, false, false, 0LL));
  }
  if (toBoolean(bitwise_and(LINE(2201,gv_windows.rvalAt("file_selection", 0x356EF7337E4026FELL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(2202,gv_windows.rvalAt("file_selection", 0x356EF7337E4026FELL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(2204,gv_windows.rvalAt("file_selection", 0x356EF7337E4026FELL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1897 */
void f_create_entry() {
  FUNCTION_INJECTION(create_entry);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_box1;
  Variant v_box2;
  Variant v_entry;
  Variant v_strings;
  Variant v_cb;
  Variant v_cb_entry;
  Variant v_editable_check;
  Variant v_visibility_check;
  Variant v_sensitive_check;
  Variant v_separator;
  Variant v_button;

  if (!(isset(gv_windows, "entry", 0x4EE7C2B9D91F2491LL))) {
    (v_window = LINE(1902,create_object("gtkwindow", Array())));
    gv_windows.set("entry", (v_window), 0x4EE7C2B9D91F2491LL);
    LINE(1904,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1905,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "entry"));
    LINE(1906,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_box1 = LINE(1908,create_object("gtkvbox", Array())));
    LINE(1909,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(1910,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1912,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1913,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1914,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_box2));
    LINE(1915,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_entry = LINE(1917,create_object("gtkentry", Array())));
    LINE(1918,v_entry.o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, "Hello World"));
    LINE(1919,v_entry.o_invoke_few_args("select_region", 0x27683AA18A602D0FLL, 2, 0LL, 5LL));
    LINE(1920,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_entry));
    LINE(1921,v_entry.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    v_strings.append(("item0"));
    v_strings.append(("item0 item1"));
    v_strings.append(("item0 item1 item2"));
    v_strings.append(("item0 item1 item2 item3"));
    v_strings.append(("item0 item1 item2 item3 item4"));
    v_strings.append(("item0 item1 item2 item3 item4 item5"));
    v_strings.append(("item0 item1 item2 item3 item4"));
    v_strings.append(("item0 item1 item2 item3"));
    v_strings.append(("item0 item1 item2"));
    v_strings.append(("item0 item1"));
    v_strings.append(("item0"));
    (v_cb = LINE(1935,create_object("gtkcombo", Array())));
    LINE(1936,v_cb.o_invoke_few_args("set_popdown_strings", 0x5C4E67BC54A71040LL, 1, v_strings));
    (v_cb_entry = v_cb.o_get("entry", 0x4EE7C2B9D91F2491LL));
    LINE(1938,v_cb_entry.o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, "Hello World"));
    LINE(1939,v_cb_entry.o_invoke_few_args("select_region", 0x27683AA18A602D0FLL, 2, 0LL, -1LL));
    LINE(1940,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_cb));
    LINE(1941,v_cb.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_editable_check = LINE(1958,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Editable").create()))));
    LINE(1959,v_editable_check.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "toggled", "entry_toggle_editable", v_entry));
    LINE(1960,v_editable_check.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
    LINE(1961,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_editable_check, false));
    LINE(1962,v_editable_check.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_visibility_check = LINE(1964,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Visible").create()))));
    LINE(1965,v_visibility_check.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "toggled", "entry_toggle_visibility", v_entry));
    LINE(1966,v_visibility_check.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
    LINE(1967,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_visibility_check, false));
    LINE(1968,v_visibility_check.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_sensitive_check = LINE(1970,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Sensitive").create()))));
    LINE(1971,v_sensitive_check.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "toggled", "entry_toggle_sensitive", v_entry));
    LINE(1972,v_sensitive_check.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
    LINE(1973,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_sensitive_check, false));
    LINE(1974,v_sensitive_check.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_separator = LINE(1976,create_object("gtkhseparator", Array())));
    LINE(1977,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1978,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1980,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1981,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1982,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(1983,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1985,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1986,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1987,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1988,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1989,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1990,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1992,gv_windows.rvalAt("entry", 0x4EE7C2B9D91F2491LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1993,gv_windows.rvalAt("entry", 0x4EE7C2B9D91F2491LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1995,gv_windows.rvalAt("entry", 0x4EE7C2B9D91F2491LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2394 */
void f_show_all_pages(CVarRef v_notebook) {
  FUNCTION_INJECTION(show_all_pages);
  Variant v_child;

  {
    LOOP_COUNTER(9);
    Variant map10 = LINE(2396,toObject(v_notebook)->o_invoke_few_args("children", 0x2B299E949E2E82ADLL, 0));
    for (ArrayIterPtr iter11 = map10.begin(); !iter11->end(); iter11->next()) {
      LOOP_COUNTER_CHECK(9);
      v_child = iter11->second();
      LINE(2397,v_child.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    }
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 2376 */
void f_notebook_homogeneous(CVarRef v_button, CVarRef v_notebook) {
  FUNCTION_INJECTION(notebook_homogeneous);
  LINE(2378,toObject(v_notebook)->o_invoke_few_args("set_homogeneous_tabs", 0x6797990D6697A287LL, 1, toObject(v_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2088 */
void f_create_panes() {
  FUNCTION_INJECTION(create_panes);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_vbox;
  Variant v_vpaned;
  Variant v_hpaned;
  Variant v_frame;
  Variant v_button;
  Variant v_separator;

  if (!(isset(gv_windows, "panes", 0x6C02B9C3464230A3LL))) {
    (v_window = LINE(2093,create_object("gtkwindow", Array())));
    gv_windows.set("panes", (v_window), 0x6C02B9C3464230A3LL);
    LINE(2095,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete_event", "delete_event"));
    LINE(2096,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "GtkPane"));
    (v_vbox = LINE(2098,create_object("gtkvbox", Array())));
    LINE(2099,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2100,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_vbox));
    (v_vpaned = LINE(2102,create_object("gtkvpaned", Array())));
    LINE(2103,v_vpaned.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2104,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_vpaned, true, true, 0LL));
    LINE(2105,v_vpaned.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    (v_hpaned = LINE(2107,create_object("gtkhpaned", Array())));
    LINE(2108,v_hpaned.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2109,v_vpaned.o_invoke_few_args("add1", 0x745CFE3DF990A09ELL, 1, v_hpaned));
    (v_frame = LINE(2111,create_object("gtkframe", Array())));
    LINE(2112,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2113,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_IN));
    LINE(2114,v_frame.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 60LL, 60LL));
    LINE(2115,v_hpaned.o_invoke_few_args("add1", 0x745CFE3DF990A09ELL, 1, v_frame));
    (v_button = LINE(2117,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Hi there").create()))));
    LINE(2118,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2119,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_button));
    (v_frame = LINE(2121,create_object("gtkframe", Array())));
    LINE(2122,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2123,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_IN));
    LINE(2124,v_frame.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 80LL, 60LL));
    LINE(2125,v_hpaned.o_invoke_few_args("add2", 0x18E49E043352A8C9LL, 1, v_frame));
    (v_frame = LINE(2127,create_object("gtkframe", Array())));
    LINE(2128,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2129,v_frame.o_invoke_few_args("set_shadow_type", 0x0FBA01C774C65780LL, 1, k_GTK_SHADOW_IN));
    LINE(2130,v_frame.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 60LL, 80LL));
    LINE(2131,v_vpaned.o_invoke_few_args("add2", 0x18E49E043352A8C9LL, 1, v_frame));
    (assignCallTemp(eo_0, toObject(v_vbox)),(assignCallTemp(eo_1, ref(LINE(2138,f_create_pane_options(v_hpaned, "Horizontal", "Left", "Right")))),LINE(2139,eo_0.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_1).set(1, false).set(2, false).set(3, 0LL).create()), 0x2D6BDE948B85D9F0LL))));
    (assignCallTemp(eo_1, toObject(v_vbox)),(assignCallTemp(eo_2, ref(LINE(2146,f_create_pane_options(v_vpaned, "Vertical", "Top", "Bottom")))),LINE(2147,eo_1.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_2).set(1, false).set(2, false).set(3, 0LL).create()), 0x2D6BDE948B85D9F0LL))));
    (v_separator = LINE(2149,create_object("gtkhseparator", Array())));
    LINE(2150,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2151,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    (v_button = LINE(2153,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Close").create()))));
    LINE(2154,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2155,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(2156,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(2157,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(2158,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(2160,gv_windows.rvalAt("panes", 0x6C02B9C3464230A3LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(2161,gv_windows.rvalAt("panes", 0x6C02B9C3464230A3LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(2163,gv_windows.rvalAt("panes", 0x6C02B9C3464230A3LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 68 */
Object f_build_option_menu(CArrRef v_items, Variant v_history //  = null
) {
  FUNCTION_INJECTION(build_option_menu);
  Object v_omenu;
  Variant v_menu;
  Primitive v_item_name = 0;
  Variant v_callback_data;
  Variant v_menu_item;

  (v_omenu = LINE(70,create_object("gtkoptionmenu", Array())));
  (v_menu = LINE(71,create_object("gtkmenu", Array())));
  {
    LOOP_COUNTER(12);
    for (ArrayIter iter14 = v_items.begin(); !iter14.end(); ++iter14) {
      LOOP_COUNTER_CHECK(12);
      v_callback_data = iter14.second();
      v_item_name = iter14.first();
      {
        (v_menu_item = LINE(74,create_object("gtkmenuitem", Array(ArrayInit(1).set(0, v_item_name).create()))));
        LINE(75,x_array_unshift(2, ref(v_callback_data), "activate"));
        LINE(76,x_call_user_func_array(Array(ArrayInit(2).setRef(0, ref(v_menu_item)).set(1, "connect").create()), toArray(v_callback_data)));
        LINE(77,v_menu.o_invoke_few_args("append", 0x4DEE4A472DC69EC2LL, 1, v_menu_item));
        LINE(78,v_menu_item.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
      }
    }
  }
  LINE(81,v_omenu->o_invoke_few_args("set_menu", 0x7C8BAE133B30D01ELL, 1, v_menu));
  if (!same(v_history, null)) LINE(83,v_omenu->o_invoke_few_args("set_history", 0x415B8B0BDC842F2ELL, 1, v_history));
  return v_omenu;
} /* function */
/* SRC: roadsend/tests/gtk.php line 2358 */
void f_tab_fill(CVarRef v_button, Variant v_child, CVarRef v_notebook) {
  FUNCTION_INJECTION(tab_fill);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_expand;
  Variant v_pack_type;

  df_lambda_12(LINE(2360,toObject(v_notebook)->o_invoke_few_args("query_tab_label_packing", 0x1FB3E3C22B33F661LL, 1, v_child)), v_expand, v_pack_type);
  (assignCallTemp(eo_0, ref(v_child)),assignCallTemp(eo_1, ref(v_expand)),assignCallTemp(eo_2, ref(LINE(2361,toObject(v_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)))),assignCallTemp(eo_3, ref(v_pack_type)),toObject(v_notebook)->o_invoke("set_tab_label_packing", Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()), 0x714C788285CCA5A2LL));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1732 */
void f_create_tooltips() {
  FUNCTION_INJECTION(create_tooltips);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Object v_tooltips;
  Variant v_box1;
  Variant v_box2;
  Variant v_button;
  Variant v_toggle;
  Variant v_box3;
  Variant v_tips_query;
  Variant v_frame;
  Variant v_separator;

  if (!(isset(gv_windows, "tooltips", 0x0CA76F8F56A42BB1LL))) {
    (v_window = LINE(1737,create_object("gtkwindow", Array())));
    gv_windows.set("tooltips", (v_window), 0x0CA76F8F56A42BB1LL);
    LINE(1739,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1740,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Tooltips"));
    LINE(1741,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    LINE(1742,v_window->o_invoke_few_args("set_policy", 0x1AECF3B49AF6B58BLL, 3, true, false, true));
    LINE(1743,v_window->o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 200LL, -2LL));
    (v_tooltips = LINE(1745,create_object("gtktooltips", Array())));
    (v_box1 = LINE(1747,create_object("gtkvbox", Array())));
    LINE(1748,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(1749,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1751,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1752,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1753,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_box2));
    LINE(1754,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1756,create_object("gtktogglebutton", Array(ArrayInit(1).set(0, "button1").create()))));
    LINE(1757,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1758,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1759,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_button, "This is button 1", "ContextHelp/buttons/1"));
    (v_button = LINE(1761,create_object("gtktogglebutton", Array(ArrayInit(1).set(0, "button2").create()))));
    LINE(1762,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1763,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1764,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_button, "This is button 2.  This is also a really long tooltip which probably won't fit on a single line and will therefore need to be wrapped.  Hopefully the wrapping will work correctly.", "ContextHelp/buttons/2_long"));
    (v_toggle = LINE(1766,create_object("gtktogglebutton", Array(ArrayInit(1).set(0, "Override TipsQuery Label").create()))));
    LINE(1767,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_toggle));
    LINE(1768,v_toggle.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1769,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_toggle, "Toggle TipsQuery view.", "Hi all!"));
    (v_box3 = LINE(1771,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(1772,v_box3.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(1773,v_box3.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_tips_query = LINE(1775,create_object("gtktipsquery", Array())));
    LINE(1776,v_tips_query.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1778,create_object("gtkbutton", Array(ArrayInit(1).set(0, "[\?]").create()))));
    LINE(1779,v_box3.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, false, false, 0LL));
    LINE(1780,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1781,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_tips_query).set(1, "start_query").create())));
    LINE(1782,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_button, "Start the Tooltips Inspector", "ContextHelp/buttons/\?"));
    LINE(1809,v_box3.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_tips_query));
    LINE(1810,v_tips_query.o_invoke_few_args("set_caller", 0x42156D982CE82F43LL, 1, v_button));
    LINE(1811,v_tips_query.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "widget_entered", "tips_query_widget_entered", v_toggle));
    LINE(1812,v_tips_query.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "widget_selected", "tips_query_widget_selected"));
    (v_frame = LINE(1814,create_object("gtkframe", Array(ArrayInit(1).set(0, "ToolTips Inspector").create()))));
    LINE(1815,v_frame.o_invoke_few_args("set_label_align", 0x32D560903FADCC21LL, 2, 0.5, 0LL));
    LINE(1816,v_frame.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    LINE(1817,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_frame, true, true, 10LL));
    LINE(1818,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box3));
    LINE(1819,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1820,v_box2.o_invoke_few_args("set_child_packing", 0x788080CD64258D2BLL, 5, v_frame, true, true, 10LL, k_GTK_PACK_START));
    (v_separator = LINE(1822,create_object("gtkhseparator", Array())));
    LINE(1823,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1824,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1826,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1827,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1828,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(1829,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1831,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1832,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1833,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1834,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1835,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1836,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1838,v_tooltips->o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_button, "Push this button to close window", "push"));
    LINE(1839,v_tooltips->o_invoke_few_args("enable", 0x36861E5E851AEE23LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1841,gv_windows.rvalAt("tooltips", 0x0CA76F8F56A42BB1LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1842,gv_windows.rvalAt("tooltips", 0x0CA76F8F56A42BB1LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1844,gv_windows.rvalAt("tooltips", 0x0CA76F8F56A42BB1LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2166 */
void f_file_selection_ok(CVarRef v_button, CVarRef v_fs) {
  FUNCTION_INJECTION(file_selection_ok);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  print(LINE(2168,(assignCallTemp(eo_1, toString(toObject(v_fs)->o_invoke_few_args("get_filename", 0x1406C1686B9ED075LL, 0))),concat3("selected '", eo_1, "'\n"))));
  LINE(2169,toObject(v_fs)->o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 333 */
void f_change_spacing(CVarRef v_adj, CVarRef v_ctree) {
  FUNCTION_INJECTION(change_spacing);
  LINE(335,toObject(v_ctree)->o_invoke_few_args("set_spacing", 0x049EECC16DACAABBLL, 1, toObject(v_adj).o_lval("value", 0x69E7413AE0C88471LL)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1301 */
void f_clist_remove_selection(CVarRef v_button, CVarRef v_clist) {
  FUNCTION_INJECTION(clist_remove_selection);
  Variant v_selection;
  Variant v_row;

  LINE(1303,toObject(v_clist)->o_invoke_few_args("freeze", 0x6A7D0E8BAA76B7E7LL, 0));
  (v_selection = toObject(v_clist).o_get("selection", 0x2781B225767237F1LL));
  LOOP_COUNTER(15);
  {
    while (!same(((v_row = toObject(v_clist).o_get("selection", 0x2781B225767237F1LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), null)) {
      LOOP_COUNTER_CHECK(15);
      {
        LINE(1307,toObject(v_clist)->o_invoke_few_args("remove", 0x508405C2FD14564ELL, 1, v_row));
        if (equal(toObject(v_clist).o_get("selection_mode", 0x0FE22FFCE6AB82F1LL), k_GTK_SELECTION_BROWSE)) break;
      }
    }
  }
  if (equal(toObject(v_clist).o_get("selection_mode", 0x0FE22FFCE6AB82F1LL), k_GTK_SELECTION_EXTENDED) && same(toObject(v_clist).o_get("selection", 0x2781B225767237F1LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), null) && not_less(toObject(v_clist).o_get("focus_row", 0x06A25AAAF397E6B3LL), 0LL)) LINE(1314,toObject(v_clist)->o_invoke_few_args("select_row", 0x760706D9E0DB1084LL, 2, toObject(v_clist).o_lval("focus_row", 0x06A25AAAF397E6B3LL), -1LL));
  LINE(1315,toObject(v_clist)->o_invoke_few_args("thaw", 0x64F2171F59D6F356LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2425 */
void f_create_pages(Variant v_notebook, int64 v_start, int64 v_end) {
  FUNCTION_INJECTION(create_pages);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_book_closed __attribute__((__unused__)) = g->GV(book_closed);
  Variant &gv_book_closed_mask __attribute__((__unused__)) = g->GV(book_closed_mask);
  Variant &gv_book_open __attribute__((__unused__)) = g->GV(book_open);
  Variant &gv_book_open_mask __attribute__((__unused__)) = g->GV(book_open_mask);
  int64 v_i = 0;
  Variant v_child;
  Variant v_vbox;
  Variant v_hbox;
  Variant v_button;
  Variant v_label_box;
  Variant v_pixwid;
  Variant v_label;
  Variant v_menu_box;

  {
  }
  {
    LOOP_COUNTER(16);
    for ((v_i = v_start); not_more(v_i, v_end); v_i++) {
      LOOP_COUNTER_CHECK(16);
      {
        (v_child = LINE(2432,create_object("gtkframe", Array(ArrayInit(1).set(0, toString("Page ") + toString(v_i)).create()))));
        LINE(2433,v_child.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
        (v_vbox = LINE(2435,create_object("gtkvbox", Array(ArrayInit(2).set(0, true).set(1, 0LL).create()))));
        LINE(2436,v_vbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
        LINE(2437,v_child.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_vbox));
        (v_hbox = LINE(2439,create_object("gtkhbox", Array(ArrayInit(2).set(0, true).set(1, 0LL).create()))));
        LINE(2440,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_hbox, false, true, 5LL));
        (v_button = LINE(2442,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Fill Tab").create()))));
        LINE(2443,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 5LL));
        LINE(2444,v_button.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
        LINE(2445,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 4, "toggled", "tab_fill", ref(v_child), ref(v_notebook)));
        (v_button = LINE(2447,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Expand Tab").create()))));
        LINE(2448,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 5LL));
        LINE(2449,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 4, "toggled", "tab_expand", ref(v_child), ref(v_notebook)));
        (v_button = LINE(2451,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Pack end").create()))));
        LINE(2452,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 5LL));
        LINE(2453,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 4, "toggled", "tab_pack", ref(v_child), ref(v_notebook)));
        (v_button = LINE(2455,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Hide page").create()))));
        LINE(2456,v_vbox.o_invoke_few_args("pack_end", 0x541A6F51B50E1593LL, 4, v_button, false, false, 5LL));
        LINE(2457,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).setRef(0, ref(v_child)).set(1, "hide").create())));
        LINE(2459,v_child.o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
        (v_label_box = LINE(2461,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 0LL).create()))));
        (v_pixwid = LINE(2462,create_object("gtkpixmap", Array(ArrayInit(2).set(0, gv_book_closed).set(1, gv_book_closed_mask).create()))));
        LINE(2463,v_label_box.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_pixwid, false, true, 0LL));
        LINE(2464,v_pixwid.o_invoke_few_args("set_padding", 0x71D6A16B820D116ALL, 2, 3LL, 1LL));
        (v_label = LINE(2466,create_object("gtklabel", Array(ArrayInit(1).set(0, toString("Page ") + toString(v_i)).create()))));
        LINE(2467,v_label_box.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_label, false, true, 0LL));
        LINE(2468,v_label_box.o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
        (v_menu_box = LINE(2470,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 0LL).create()))));
        (v_pixwid = LINE(2471,create_object("gtkpixmap", Array(ArrayInit(2).set(0, gv_book_closed).set(1, gv_book_closed_mask).create()))));
        LINE(2472,v_menu_box.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_pixwid, false, true, 0LL));
        LINE(2473,v_pixwid.o_invoke_few_args("set_padding", 0x71D6A16B820D116ALL, 2, 3LL, 1LL));
        (v_label = LINE(2475,create_object("gtklabel", Array(ArrayInit(1).set(0, toString("Page ") + toString(v_i)).create()))));
        LINE(2476,v_menu_box.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_label, false, true, 0LL));
        LINE(2477,v_menu_box.o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
        LINE(2479,v_notebook.o_invoke_few_args("append_page_menu", 0x1BA4048CC97D326FLL, 3, v_child, v_label_box, v_menu_box));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 338 */
void f_expand_all(CVarRef v_button, CVarRef v_ctree) {
  FUNCTION_INJECTION(expand_all);
  LINE(340,toObject(v_ctree)->o_invoke_few_args("expand_recursive", 0x49DB187DBA85FF0DLL, 0));
  LINE(341,f_after_press(v_ctree));
} /* function */
/* SRC: roadsend/tests/gtk.php line 462 */
void f_ctree_toggle_expander_style(CVarRef v_menu_item, CVarRef v_ctree, Variant v_expander_style) {
  FUNCTION_INJECTION(ctree_toggle_expander_style);
  LINE(464,toObject(v_ctree)->o_invoke_few_args("set_expander_style", 0x6DA96170F45E0277LL, 1, v_expander_style));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1459 */
void f_create_buttons() {
  FUNCTION_INJECTION(create_buttons);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_box1;
  Variant v_table;
  Variant v_buttons;
  int64 v_i = 0;
  Variant v_x;
  Variant v_y;
  Variant v_separator;
  Variant v_box2;
  Variant v_button;

  if (!(isset(gv_windows, "buttons", 0x425B4B1B002ABE31LL))) {
    (v_window = LINE(1464,create_object("gtkwindow", Array())));
    gv_windows.set("buttons", (v_window), 0x425B4B1B002ABE31LL);
    LINE(1466,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1467,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "GtkButton"));
    (v_box1 = LINE(1469,create_object("gtkvbox", Array())));
    LINE(1470,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(1471,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_table = LINE(1473,create_object("gtktable", Array(ArrayInit(2).set(0, 3LL).set(1, 3LL).create()))));
    LINE(1474,v_table.o_invoke_few_args("set_row_spacings", 0x064756090D636E93LL, 1, 5LL));
    LINE(1475,v_table.o_invoke_few_args("set_col_spacings", 0x5ABC83CA64290F28LL, 1, 5LL));
    LINE(1476,v_table.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1477,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_table));
    LINE(1478,v_table.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_buttons = LINE(1480,x_range(0LL, 8LL)));
    LINE(1481,(assignCallTemp(eo_0, ref(v_buttons)),assignCallTemp(eo_1, "df_lambda_1"),x_array_walk(eo_0, eo_1)));
    {
      LOOP_COUNTER(17);
      for ((v_i = 0LL); less(v_i, 9LL); v_i++) {
        LOOP_COUNTER_CHECK(17);
        {
          (assignCallTemp(eo_0, toObject(v_buttons.rvalAt(v_i))),(assignCallTemp(eo_2, ref(LINE(1493,"df_lambda_2"))),assignCallTemp(eo_3, ref(v_buttons.refvalAt(modulo((v_i + 1LL), 9LL)))),LINE(1494,eo_0.o_invoke("connect", Array(ArrayInit(3).set(0, "clicked").set(1, eo_2).set(2, eo_3).create()), 0x7032C660AD16D7FELL))));
          (v_x = toInt64((divide(v_i, 3LL))));
          (v_y = modulo(v_i, 3LL));
          LINE(1497,v_table.o_invoke_few_args("attach", 0x62DD82BFEB88A4ACLL, 5, v_buttons.refvalAt(v_i), v_x, v_x + 1LL, v_y, v_y + 1LL));
          LINE(1498,v_buttons.rvalAt(v_i).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
        }
      }
    }
    (v_separator = LINE(1501,create_object("gtkhseparator", Array())));
    LINE(1502,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1503,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1505,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1506,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1507,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(1508,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1510,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1511,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1512,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1513,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1514,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1515,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1517,gv_windows.rvalAt("buttons", 0x425B4B1B002ABE31LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1518,gv_windows.rvalAt("buttons", 0x425B4B1B002ABE31LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1520,gv_windows.rvalAt("buttons", 0x425B4B1B002ABE31LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1377 */
void f_clist_toggle_sel_mode(CVarRef v_menu_item, CVarRef v_clist, Variant v_sel_mode) {
  FUNCTION_INJECTION(clist_toggle_sel_mode);
  LINE(1379,toObject(v_clist)->o_invoke_few_args("set_selection_mode", 0x58B77CEABCFA5844LL, 1, v_sel_mode));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1784 */
void f_tips_query_widget_entered(CVarRef v_tips_query, CVarRef v_widget, CVarRef v_tip_text, CVarRef v_tip_private, CVarRef v_toggle) {
  FUNCTION_INJECTION(tips_query_widget_entered);
  if (toBoolean(LINE(1787,toObject(v_toggle)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)))) {
    LINE(1788,toObject(v_tips_query)->o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, toBoolean(v_tip_text) ? (("There is a Tip!")) : (("There is no Tip!"))));
    LINE(1790,toObject(v_tips_query)->o_invoke_few_args("emit_stop_by_name", 0x6F19306A20639FB2LL, 1, "widget_entered"));
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 328 */
void f_change_indent(CVarRef v_adj, CVarRef v_ctree) {
  FUNCTION_INJECTION(change_indent);
  LINE(330,toObject(v_ctree)->o_invoke_few_args("set_indent", 0x2BB1509264E6ECE9LL, 1, toObject(v_adj).o_lval("value", 0x69E7413AE0C88471LL)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2381 */
void f_notebook_popup(CVarRef v_button, CVarRef v_notebook) {
  FUNCTION_INJECTION(notebook_popup);
  if (toBoolean(LINE(2383,toObject(v_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)))) LINE(2384,toObject(v_notebook)->o_invoke_few_args("popup_enable", 0x63E7FFA1EC057473LL, 0));
  else LINE(2386,toObject(v_notebook)->o_invoke_few_args("popup_disable", 0x317588C7AE3C29D3LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2021 */
void f_toggle_shrink(Variant v_child) {
  FUNCTION_INJECTION(toggle_shrink);
  Variant v_paned;
  bool v_is_child1 = false;
  Variant v_resize;
  Variant v_shrink;
  Variant v_parent;

  (v_paned = v_child.o_get("parent", 0x16E2F26FFB10FD8CLL));
  (v_is_child1 = (equal(v_child, v_paned.o_get("child1", 0x218D32E28B3D34F8LL))));
  (v_resize = v_is_child1 ? ((Variant)(v_paned.o_get("child1_resize", 0x6A4C7E313C849CB8LL))) : ((Variant)(v_paned.o_get("child2_resize", 0x0E587CD2C4430C0BLL))));
  (v_shrink = v_is_child1 ? ((Variant)(v_paned.o_get("child1_shrink", 0x2273AC457C937E06LL))) : ((Variant)(v_paned.o_get("child2_shrink", 0x684FEF72B193CABDLL))));
  LINE(2030,v_child.o_invoke_few_args("ref", 0x05FF98D1208F545ELL, 0));
  (v_parent = v_child.o_get("parent", 0x16E2F26FFB10FD8CLL));
  LINE(2034,v_parent.o_invoke_few_args("remove", 0x508405C2FD14564ELL, 1, v_child));
  if (v_is_child1) LINE(2037,v_paned.o_invoke_few_args("pack1", 0x634A5EE8B557B741LL, 3, v_child, v_resize, !(toBoolean(v_shrink))));
  else LINE(2039,v_paned.o_invoke_few_args("pack2", 0x691C6CBB40EB5F71LL, 3, v_child, v_resize, !(toBoolean(v_shrink))));
  LINE(2041,v_child.o_invoke_few_args("unref", 0x3C841CEB64700B43LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 910 */
void f_set_cursor(CVarRef v_spinner, CVarRef v_darea, CVarRef v_cur_name) {
  FUNCTION_INJECTION(set_cursor);
  Variant v_c;
  Variant v_cursor;
  Variant v_window;

  (v_c = LINE(912,toObject(v_spinner)->o_invoke_few_args("get_value_as_int", 0x65CF3CF738983EDALL, 0)));
  (v_c = bitwise_and(v_c, 254LL));
  (v_cursor = LINE(914,throw_fatal("unknown class gdk", ((void*)NULL))));
  (v_window = toObject(v_darea).o_get("window", 0x6B178C4E4A2617E2LL));
  LINE(916,v_window.o_invoke_few_args("set_cursor", 0x112391F3498CA7D7LL, 1, v_cursor));
  echo("cur_name is:\n");
  LINE(918,x_var_dump(1, v_cur_name));
  echo("the cursor's name is:\n");
  LINE(920,x_var_dump(1, v_cursor.o_get("name", 0x0BCDB293DC3CBDDCLL)));
  LINE(921,toObject(v_cur_name)->o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, v_cursor.o_lval("name", 0x0BCDB293DC3CBDDCLL)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2409 */
void f_notabs_notebook(CVarRef v_menuitem, CVarRef v_notebook) {
  FUNCTION_INJECTION(notabs_notebook);
  int64 v_i = 0;

  LINE(2411,toObject(v_notebook)->o_invoke_few_args("set_show_tabs", 0x5F73C9EB66BC4DA5LL, 1, false));
  if (equal(LINE(2412,x_count(toObject(v_notebook)->o_invoke_few_args("children", 0x2B299E949E2E82ADLL, 0))), 15LL)) {
    LOOP_COUNTER(18);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(18);
      LINE(2414,toObject(v_notebook)->o_invoke_few_args("remove_page", 0x3E106A0FB9A3E2F0LL, 1, 5LL));
    }
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 1483 */
void f_toggle_show(CVarRef v_b) {
  FUNCTION_INJECTION(toggle_show);
  if (toBoolean(bitwise_and(LINE(1485,toObject(v_b)->o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1486,toObject(v_b)->o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1488,toObject(v_b)->o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1998 */
void f_toggle_resize(Variant v_child) {
  FUNCTION_INJECTION(toggle_resize);
  Variant v_paned;
  bool v_is_child1 = false;
  Variant v_resize;
  Variant v_shrink;
  Variant v_parent;

  (v_paned = v_child.o_get("parent", 0x16E2F26FFB10FD8CLL));
  (v_is_child1 = (equal(v_child, v_paned.o_get("child1", 0x218D32E28B3D34F8LL))));
  (v_resize = v_is_child1 ? ((Variant)(v_paned.o_get("child1_resize", 0x6A4C7E313C849CB8LL))) : ((Variant)(v_paned.o_get("child2_resize", 0x0E587CD2C4430C0BLL))));
  (v_shrink = v_is_child1 ? ((Variant)(v_paned.o_get("child1_shrink", 0x2273AC457C937E06LL))) : ((Variant)(v_paned.o_get("child2_shrink", 0x684FEF72B193CABDLL))));
  LINE(2007,v_child.o_invoke_few_args("ref", 0x05FF98D1208F545ELL, 0));
  (v_parent = v_child.o_get("parent", 0x16E2F26FFB10FD8CLL));
  LINE(2011,v_parent.o_invoke_few_args("remove", 0x508405C2FD14564ELL, 1, v_child));
  if (v_is_child1) LINE(2014,v_paned.o_invoke_few_args("pack1", 0x634A5EE8B557B741LL, 3, v_child, !(toBoolean(v_resize)), v_shrink));
  else LINE(2016,v_paned.o_invoke_few_args("pack2", 0x691C6CBB40EB5F71LL, 3, v_child, !(toBoolean(v_resize)), v_shrink));
  LINE(2018,v_child.o_invoke_few_args("unref", 0x3C841CEB64700B43LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2370 */
void f_tab_pack(CVarRef v_button, Variant v_child, CVarRef v_notebook) {
  FUNCTION_INJECTION(tab_pack);
  Variant v_expand;
  Variant v_fill;

  df_lambda_14(LINE(2372,toObject(v_notebook)->o_invoke_few_args("query_tab_label_packing", 0x1FB3E3C22B33F661LL, 1, v_child)), v_expand, v_fill);
  LINE(2373,toObject(v_notebook)->o_invoke_few_args("set_tab_label_packing", 0x714C788285CCA5A2LL, 4, v_child, v_expand, v_fill, toInt64(toObject(v_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0))));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1524 */
void f_create_labels() {
  FUNCTION_INJECTION(create_labels);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_box;
  Variant v_vbox;
  Variant v_hbox;
  Variant v_frame;
  Variant v_label;
  Variant v_separator;
  Variant v_button;

  if (!(isset(gv_windows, "labels", 0x7DDBEB38F40F832ELL))) {
    (v_window = LINE(1529,create_object("gtkwindow", Array())));
    gv_windows.set("labels", (v_window), 0x7DDBEB38F40F832ELL);
    LINE(1531,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1532,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "GtkLabel"));
    (v_box = LINE(1534,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1535,v_box.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1536,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box));
    LINE(1537,v_box.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_vbox = LINE(1539,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    (v_hbox = LINE(1540,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(1541,v_box.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_hbox, false));
    LINE(1542,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1543,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_vbox, false, false));
    LINE(1544,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1545,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    (v_frame = LINE(1547,create_object("gtkframe", Array(ArrayInit(1).set(0, "Normal Label").create()))));
    (v_label = LINE(1548,create_object("gtklabel", Array(ArrayInit(1).set(0, "This is a Normal label").create()))));
    LINE(1549,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
    LINE(1550,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1551,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_frame, false, false));
    LINE(1552,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(1554,create_object("gtkframe", Array(ArrayInit(1).set(0, "Multi-line Label").create()))));
    (v_label = LINE(1555,create_object("gtklabel", Array(ArrayInit(1).set(0, "This is a Multi-line label.\nSecond line\nThird line").create()))));
    LINE(1556,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
    LINE(1557,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1558,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_frame, false, false));
    LINE(1559,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(1561,create_object("gtkframe", Array(ArrayInit(1).set(0, "Left Justified Label").create()))));
    (v_label = LINE(1562,create_object("gtklabel", Array(ArrayInit(1).set(0, "This is a Left-Justified\nMulti-line label.\nThird line").create()))));
    LINE(1563,v_label.o_invoke_few_args("set_justify", 0x48751EB6B864B417LL, 1, k_GTK_JUSTIFY_LEFT));
    LINE(1564,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
    LINE(1565,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1566,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_frame, false, false));
    LINE(1567,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(1569,create_object("gtkframe", Array(ArrayInit(1).set(0, "Right Justified Label").create()))));
    (v_label = LINE(1570,create_object("gtklabel", Array(ArrayInit(1).set(0, "This is a Right-Justified\nMulti-line label.\nThird line").create()))));
    LINE(1571,v_label.o_invoke_few_args("set_justify", 0x48751EB6B864B417LL, 1, k_GTK_JUSTIFY_RIGHT));
    LINE(1572,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
    LINE(1573,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1574,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_frame, false, false));
    LINE(1575,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_vbox = LINE(1577,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(1578,v_hbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_vbox));
    LINE(1579,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(1581,create_object("gtkframe", Array(ArrayInit(1).set(0, "Line wrapped label").create()))));
    (v_label = LINE(1588,create_object("gtklabel", Array(ArrayInit(1).set(0, "This is an example of a line-wrapped label.  It should not be taking up the entire             width allocated to it, but automatically wraps the words to fit.  The time has come, for all good men, to come to the aid of their party.  The sixth sheik's six sheep's sick.\n     It supports multiple paragraphs correctly, and  correctly   adds many          extra  spaces. ").create()))));
    LINE(1589,v_label.o_invoke_few_args("set_line_wrap", 0x5DED6A14368C3561LL, 1, true));
    LINE(1590,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
    LINE(1591,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1592,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_frame, false, false));
    LINE(1593,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(1595,create_object("gtkframe", Array(ArrayInit(1).set(0, "Filled, wrapped label").create()))));
    (v_label = LINE(1602,create_object("gtklabel", Array(ArrayInit(1).set(0, "This is an example of a line-wrapped, filled label.  It should be taking up the entire              width allocated to it.  Here is a seneance to prove my point.  Here is another sentence. Here comes the sun, do de do de do.\n    This is a new paragraph.\n    This is another newer, longer, better paragraph.  It is coming to an end, unfortunately.").create()))));
    LINE(1603,v_label.o_invoke_few_args("set_line_wrap", 0x5DED6A14368C3561LL, 1, true));
    LINE(1604,v_label.o_invoke_few_args("set_justify", 0x48751EB6B864B417LL, 1, k_GTK_JUSTIFY_FILL));
    LINE(1605,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
    LINE(1606,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1607,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_frame, false, false));
    LINE(1608,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame = LINE(1610,create_object("gtkframe", Array(ArrayInit(1).set(0, "Underlined label").create()))));
    (v_label = LINE(1612,create_object("gtklabel", Array(ArrayInit(1).set(0, "This label is underlined!\nThis one is underlined in \306\374\313\334\270\354\244\316\306\376\315\321quite a funky fashion").create()))));
    LINE(1613,v_label.o_invoke_few_args("set_justify", 0x48751EB6B864B417LL, 1, k_GTK_JUSTIFY_LEFT));
    LINE(1614,v_label.o_invoke_few_args("set_pattern", 0x32524A03E4F3C3E8LL, 1, "_________________________ _ _________ _ _____ _ __ __  ___ ____ _____"));
    LINE(1615,v_frame.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
    LINE(1616,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(1617,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_frame, false, false));
    LINE(1618,v_frame.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_separator = LINE(1620,create_object("gtkhseparator", Array())));
    LINE(1621,v_box.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1622,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1624,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1625,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1626,v_box.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1627,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1628,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1629,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1631,gv_windows.rvalAt("labels", 0x7DDBEB38F40F832ELL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1632,gv_windows.rvalAt("labels", 0x7DDBEB38F40F832ELL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1634,gv_windows.rvalAt("labels", 0x7DDBEB38F40F832ELL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2400 */
void f_standard_notebook(CVarRef v_menuitem, CVarRef v_notebook) {
  FUNCTION_INJECTION(standard_notebook);
  int64 v_i = 0;

  LINE(2402,toObject(v_notebook)->o_invoke_few_args("set_show_tabs", 0x5F73C9EB66BC4DA5LL, 1, true));
  LINE(2403,toObject(v_notebook)->o_invoke_few_args("set_scrollable", 0x711D7937F2003374LL, 1, false));
  if (equal(LINE(2404,x_count(toObject(v_notebook)->o_invoke_few_args("children", 0x2B299E949E2E82ADLL, 0))), 15LL)) {
    LOOP_COUNTER(19);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(19);
      LINE(2406,toObject(v_notebook)->o_invoke_few_args("remove_page", 0x3E106A0FB9A3E2F0LL, 1, 5LL));
    }
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 2044 */
Object f_create_pane_options(CVarRef v_paned, CStrRef v_frame_label, CStrRef v_label1, CStrRef v_label2) {
  FUNCTION_INJECTION(create_pane_options);
  Object v_frame;
  Variant v_table;
  Variant v_label;
  Variant v_check_button;

  (v_frame = LINE(2046,create_object("gtkframe", Array(ArrayInit(1).set(0, v_frame_label).create()))));
  LINE(2047,v_frame->o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2048,v_frame->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
  (v_table = LINE(2050,create_object("gtktable", Array(ArrayInit(3).set(0, 3LL).set(1, 2LL).set(2, true).create()))));
  LINE(2051,v_table.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2052,v_frame->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_table));
  (v_label = LINE(2054,create_object("gtklabel", Array(ArrayInit(1).set(0, v_label1).create()))));
  LINE(2055,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2056,v_table.o_invoke_few_args("attach_defaults", 0x40C64ECB1E1E3566LL, 5, v_label, 0LL, 1LL, 0LL, 1LL));
  (v_check_button = LINE(2058,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Resize").create()))));
  LINE(2059,v_check_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2060,v_table.o_invoke_few_args("attach_defaults", 0x40C64ECB1E1E3566LL, 5, v_check_button, 0LL, 1LL, 1LL, 2LL));
  LINE(2061,v_check_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 3, "toggled", "toggle_resize", toObject(v_paned).o_lval("child1", 0x218D32E28B3D34F8LL)));
  (v_check_button = LINE(2063,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Shrink").create()))));
  LINE(2064,v_check_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2065,v_table.o_invoke_few_args("attach_defaults", 0x40C64ECB1E1E3566LL, 5, v_check_button, 0LL, 1LL, 2LL, 3LL));
  LINE(2066,v_check_button.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
  LINE(2067,v_check_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 3, "toggled", "toggle_shrink", toObject(v_paned).o_lval("child1", 0x218D32E28B3D34F8LL)));
  (v_label = LINE(2069,create_object("gtklabel", Array(ArrayInit(1).set(0, v_label2).create()))));
  LINE(2070,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2071,v_table.o_invoke_few_args("attach_defaults", 0x40C64ECB1E1E3566LL, 5, v_label, 1LL, 2LL, 0LL, 1LL));
  (v_check_button = LINE(2073,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Resize").create()))));
  LINE(2074,v_check_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2075,v_table.o_invoke_few_args("attach_defaults", 0x40C64ECB1E1E3566LL, 5, v_check_button, 1LL, 2LL, 1LL, 2LL));
  LINE(2076,v_check_button.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
  LINE(2077,v_check_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 3, "toggled", "toggle_resize", toObject(v_paned).o_lval("child2", 0x2BB2AB6BDD5BDFF6LL)));
  (v_check_button = LINE(2079,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "Shrink").create()))));
  LINE(2080,v_check_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(2081,v_table.o_invoke_few_args("attach_defaults", 0x40C64ECB1E1E3566LL, 5, v_check_button, 1LL, 2LL, 2LL, 3LL));
  LINE(2082,v_check_button.o_invoke_few_args("set_active", 0x20C88B213677C2C1LL, 1, true));
  LINE(2083,v_check_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 3, "toggled", "toggle_shrink", toObject(v_paned).o_lval("child2", 0x2BB2AB6BDD5BDFF6LL)));
  return v_frame;
} /* function */
/* SRC: roadsend/tests/gtk.php line 885 */
void f_expose_event(CVarRef v_darea, CVarRef v_event) {
  FUNCTION_INJECTION(expose_event);
  Variant v_drawable;
  Variant v_style;
  Variant v_white_gc;
  Variant v_grey_gc;
  Variant v_black_gc;
  Variant v_max_width;
  Variant v_max_height;

  (v_drawable = toObject(v_darea).o_get("window", 0x6B178C4E4A2617E2LL));
  (v_style = LINE(888,toObject(v_darea)->o_invoke_few_args("get_style", 0x33269B0A1AD7274FLL, 0)));
  (v_white_gc = v_style.o_get("white_gc", 0x06A1D59F7A0AE817LL));
  (v_grey_gc = v_style.o_get("bg_gc", 0x2680EBB3F2C187A7LL).rvalAt(k_GTK_STATE_NORMAL));
  (v_black_gc = v_style.o_get("black_gc", 0x7C0401670AEB6690LL));
  (v_max_width = v_drawable.o_get("width", 0x79CE5ED9259796ADLL));
  (v_max_height = v_drawable.o_get("height", 0x674F0FBB080B0779LL));
  LINE(902,throw_fatal("unknown class gdk", ((void*)NULL)));
  LINE(904,throw_fatal("unknown class gdk", ((void*)NULL)));
  LINE(907,throw_fatal("unknown class gdk", ((void*)NULL)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 193 */
void f_rebuild_tree(CVarRef v_button, CVarRef v_ctree) {
  FUNCTION_INJECTION(rebuild_tree);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ctree_data __attribute__((__unused__)) = g->GV(ctree_data);
  Variant v_d;
  Variant v_b;
  Variant v_p;
  Numeric v_n = 0;
  Variant v_text;
  Variant v_parent;
  Variant v_style;

  (v_d = LINE(197,gv_ctree_data.rvalAt("spin1", 0x6669BDFCF901E624LL).o_invoke_few_args("get_value_as_int", 0x65CF3CF738983EDALL, 0)));
  (v_b = LINE(198,gv_ctree_data.rvalAt("spin2", 0x45C4EFC323C30F0ALL).o_invoke_few_args("get_value_as_int", 0x65CF3CF738983EDALL, 0)));
  (v_p = LINE(199,gv_ctree_data.rvalAt("spin3", 0x37C3B4C95FA52588LL).o_invoke_few_args("get_value_as_int", 0x65CF3CF738983EDALL, 0)));
  (v_n = LINE(201,x_intval(divide((x_pow(v_b, v_d) - 1LL), (v_b - 1LL)))) * (v_p + 1LL));
  if (more(v_n, 100000LL)) {
    print(toString(v_n) + toString(" total items\? Try less\n"));
    return;
  }
  gv_ctree_data.set("books", (1LL), 0x06D8B08F90033F8ALL);
  gv_ctree_data.set("pages", (0LL), 0x3EA464DEB0A970C3LL);
  LINE(211,toObject(v_ctree)->o_invoke_few_args("freeze", 0x6A7D0E8BAA76B7E7LL, 0));
  LINE(212,toObject(v_ctree)->o_invoke_few_args("clear", 0x31DA235C5A226667LL, 0));
  (v_text = ScalarArrays::sa_[4]);
  (v_parent = (assignCallTemp(eo_0, toObject(v_ctree)),LINE(220,eo_0.toObject()->o_invoke("insert_node", Array(ArrayInit(10).set(0, null).set(1, null).set(2, ref(v_text)).set(3, 5LL).set(4, ref(gv_ctree_data.refvalAt("pixmap1", 0x1449CA4F8DE6D108LL))).set(5, ref(gv_ctree_data.refvalAt("mask1", 0x71EDD24E049DCECFLL))).set(6, ref(gv_ctree_data.refvalAt("pixmap2", 0x07E5100FC93F7667LL))).set(7, ref(gv_ctree_data.refvalAt("mask2", 0x6FC4A765C6C6C1C1LL))).set(8, false).set(9, true).create()), 0x3B1FEE291E349F94LL))));
  (v_style = LINE(222,create_object("gtkstyle", Array())));
  lval(v_style.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (LINE(223,create_object("gdkcolor", Array(ArrayInit(3).set(0, 0LL).set(1, 45000LL).set(2, 55000LL).create())))));
  LINE(224,toObject(v_ctree)->o_invoke_few_args("node_set_row_data", 0x2B4EA9BC2F733EE2LL, 2, v_parent, v_style));
  if (equal(toObject(v_ctree).o_get("line_style", 0x3D983ECCDE1F4BC4LL), k_GTK_CTREE_LINES_TABBED)) LINE(227,toObject(v_ctree)->o_invoke_few_args("node_set_row_style", 0x3B8087F7DDE997E0LL, 2, v_parent, v_style));
  LINE(229,f_build_recursive(v_ctree, 1LL, v_d, v_b, v_p, v_parent));
  LINE(230,toObject(v_ctree)->o_invoke_few_args("thaw", 0x64F2171F59D6F356LL, 0));
  LINE(231,f_after_press(v_ctree));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2364 */
void f_tab_expand(CVarRef v_button, Variant v_child, CVarRef v_notebook) {
  FUNCTION_INJECTION(tab_expand);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_fill;
  Variant v_pack_type;

  df_lambda_13(LINE(2366,toObject(v_notebook)->o_invoke_few_args("query_tab_label_packing", 0x1FB3E3C22B33F661LL, 1, v_child)), v_fill, v_pack_type);
  (assignCallTemp(eo_0, ref(v_child)),assignCallTemp(eo_1, ref(LINE(2367,toObject(v_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)))),assignCallTemp(eo_2, ref(v_fill)),assignCallTemp(eo_3, ref(v_pack_type)),toObject(v_notebook)->o_invoke("set_tab_label_packing", Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()), 0x714C788285CCA5A2LL));
} /* function */
/* SRC: roadsend/tests/gtk.php line 234 */
void f_build_recursive(CVarRef v_ctree, int64 v_cur_depth, CVarRef v_depth, CVarRef v_num_books, CVarRef v_num_pages, Variant v_parent) {
  FUNCTION_INJECTION(build_recursive);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ctree_data __attribute__((__unused__)) = g->GV(ctree_data);
  Variant v_sibling;
  Variant v_i;
  Variant v_text;
  Variant v_style;
  Object v_color;

  setNull(v_sibling);
  {
    LOOP_COUNTER(20);
    for ((v_i = v_num_pages + v_num_books); more(v_i, v_num_books); v_i--) {
      LOOP_COUNTER_CHECK(20);
      {
        lval(gv_ctree_data.lvalAt("pages", 0x3EA464DEB0A970C3LL))++;
        v_text.set(0LL, (LINE(242,(assignCallTemp(eo_1, modulo(x_rand(), 100LL)),x_sprintf(2, "Page %02d", Array(ArrayInit(1).set(0, eo_1).create()))))), 0x77CFA1EEF01BCA90LL);
        v_text.set(1LL, (LINE(243,x_sprintf(3, "Item %d-%d", Array(ArrayInit(2).set(0, v_cur_depth).set(1, v_i).create())))), 0x5BCA7C69B794F8CELL);
        (v_sibling = (assignCallTemp(eo_0, toObject(v_ctree)),LINE(247,eo_0.toObject()->o_invoke("insert_node", Array(ArrayInit(10).set(0, ref(v_parent)).set(1, ref(v_sibling)).set(2, ref(v_text)).set(3, 5LL).set(4, ref(gv_ctree_data.refvalAt("pixmap3", 0x1B3134F7F59D6D5DLL))).set(5, ref(gv_ctree_data.refvalAt("mask3", 0x6A18099C4F24514CLL))).set(6, null).set(7, null).set(8, true).set(9, false).create()), 0x3B1FEE291E349F94LL))));
        if (toBoolean(v_parent) && equal(toObject(v_ctree).o_get("line_style", 0x3D983ECCDE1F4BC4LL), k_GTK_CTREE_LINES_TABBED)) LINE(249,toObject(v_ctree)->o_invoke_few_args("node_set_row_style", 0x3B8087F7DDE997E0LL, 2, v_sibling, v_parent.o_get("row", 0x4307151CEB3C6312LL).o_lval("style", 0x040EF9119982696ALL)));
      }
    }
  }
  if (equal(v_cur_depth, v_depth)) return;
  {
    LOOP_COUNTER(21);
    for ((v_i = v_num_books); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(21);
      {
        lval(gv_ctree_data.lvalAt("books", 0x06D8B08F90033F8ALL))++;
        v_text.set(0LL, (LINE(258,(assignCallTemp(eo_2, modulo(x_rand(), 100LL)),x_sprintf(2, "Book %02d", Array(ArrayInit(1).set(0, eo_2).create()))))), 0x77CFA1EEF01BCA90LL);
        v_text.set(1LL, (LINE(259,x_sprintf(3, "Item %d-%d", Array(ArrayInit(2).set(0, v_cur_depth).set(1, v_i).create())))), 0x5BCA7C69B794F8CELL);
        (v_sibling = (assignCallTemp(eo_1, toObject(v_ctree)),LINE(265,eo_1.toObject()->o_invoke("insert_node", Array(ArrayInit(10).set(0, ref(v_parent)).set(1, ref(v_sibling)).set(2, ref(v_text)).set(3, 5LL).set(4, ref(gv_ctree_data.refvalAt("pixmap1", 0x1449CA4F8DE6D108LL))).set(5, ref(gv_ctree_data.refvalAt("mask1", 0x71EDD24E049DCECFLL))).set(6, ref(gv_ctree_data.refvalAt("pixmap2", 0x07E5100FC93F7667LL))).set(7, ref(gv_ctree_data.refvalAt("mask2", 0x6FC4A765C6C6C1C1LL))).set(8, false).set(9, false).create()), 0x3B1FEE291E349F94LL))));
        (v_style = LINE(267,create_object("gtkstyle", Array())));
        {
          switch (toInt64(modulo(v_cur_depth, 3LL))) {
          case 0LL:
            {
              (v_color = LINE(272,create_object("gdkcolor", Array(ArrayInit(3).set(0, 10000LL * (modulo(v_cur_depth, 6LL))).set(1, 0LL).set(2, 65535LL - (modulo((toInt64(v_i * 10000LL)), 65535LL))).create()))));
              lval(v_style.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_color));
              break;
            }
          case 1LL:
            {
              (v_color = LINE(279,create_object("gdkcolor", Array(ArrayInit(3).set(0, 10000LL * (modulo(v_cur_depth, 6LL))).set(1, 65535LL - (modulo((toInt64(v_i * 10000LL)), 65535LL))).set(2, 0LL).create()))));
              lval(v_style.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_color));
              break;
            }
          default:
            {
              (v_color = LINE(286,create_object("gdkcolor", Array(ArrayInit(3).set(0, 65535LL - (modulo((toInt64(v_i * 10000LL)), 65535LL))).set(1, 0LL).set(2, 10000LL * (modulo(v_cur_depth, 6LL))).create()))));
              lval(v_style.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_color));
              break;
            }
          }
        }
        LINE(291,toObject(v_ctree)->o_invoke_few_args("node_set_row_data", 0x2B4EA9BC2F733EE2LL, 2, v_sibling, v_style));
        if (equal(toObject(v_ctree).o_get("line_style", 0x3D983ECCDE1F4BC4LL), k_GTK_CTREE_LINES_TABBED)) LINE(293,toObject(v_ctree)->o_invoke_few_args("node_set_row_style", 0x3B8087F7DDE997E0LL, 2, v_sibling, v_style));
        LINE(296,f_build_recursive(v_ctree, v_cur_depth + 1LL, v_depth, v_num_books, v_num_pages, v_sibling));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 924 */
void f_button_press(CVarRef v_widget, CVarRef v_event, CVarRef v_spinner) {
  FUNCTION_INJECTION(button_press);
  if (equal(toObject(v_event).o_get("type", 0x508FC7C8724A760ALL), k_GDK_BUTTON_PRESS)) {
    if (equal(toObject(v_event).o_get("button", 0x614CB098CEBB2764LL), 1LL)) LINE(928,toObject(v_spinner)->o_invoke_few_args("spin", 0x142591EB6A06A9BBLL, 2, k_GTK_SPIN_STEP_FORWARD, 0LL));
    else if (equal(toObject(v_event).o_get("button", 0x614CB098CEBB2764LL), 3LL)) LINE(930,toObject(v_spinner)->o_invoke_few_args("spin", 0x142591EB6A06A9BBLL, 2, k_GTK_SPIN_STEP_BACKWARD, 0LL));
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 1953 */
void f_entry_toggle_visibility(CVarRef v_check_button, CVarRef v_entry) {
  FUNCTION_INJECTION(entry_toggle_visibility);
  LINE(1955,toObject(v_entry)->o_invoke_few_args("set_visibility", 0x687C91DB940F8E57LL, 1, toObject(v_check_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2287 */
void f_create_event_watcher() {
  FUNCTION_INJECTION(create_event_watcher);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_dialog __attribute__((__unused__)) = g->sv_create_event_watcher_DupIddialog;
  bool &inited_sv_dialog __attribute__((__unused__)) = g->inited_sv_create_event_watcher_DupIddialog;
  Variant &sv_event_watcher_enter_id __attribute__((__unused__)) = g->sv_create_event_watcher_DupIdevent_watcher_enter_id;
  bool &inited_sv_event_watcher_enter_id __attribute__((__unused__)) = g->inited_sv_create_event_watcher_DupIdevent_watcher_enter_id;
  Variant &sv_event_watcher_leave_id __attribute__((__unused__)) = g->sv_create_event_watcher_DupIdevent_watcher_leave_id;
  bool &inited_sv_event_watcher_leave_id __attribute__((__unused__)) = g->inited_sv_create_event_watcher_DupIdevent_watcher_leave_id;
  Variant v_vbox;
  Variant v_action_area;
  Variant v_button;

  if (!inited_sv_dialog) {
    setNull(sv_dialog);
    inited_sv_dialog = true;
  }
  if (!inited_sv_event_watcher_enter_id) {
    (sv_event_watcher_enter_id = 0LL);
    inited_sv_event_watcher_enter_id = true;
  }
  if (!inited_sv_event_watcher_leave_id) {
    (sv_event_watcher_leave_id = 0LL);
    inited_sv_event_watcher_leave_id = true;
  }
  if (!(toBoolean(sv_dialog))) {
    (sv_dialog = LINE(2294,create_object("gtkdialog", Array())));
    LINE(2295,sv_dialog.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 4, "destroy", "event_watcher_down", ref(sv_event_watcher_enter_id), ref(sv_event_watcher_leave_id)));
    (assignCallTemp(eo_1, ref(LINE(2296,"df_lambda_6"))),assignCallTemp(eo_2, ref(sv_dialog)),sv_dialog.o_invoke("connect_object", Array(ArrayInit(3).set(0, "destroy").set(1, eo_1).set(2, eo_2).create()), 0x3E0DE7B55A894443LL));
    LINE(2297,sv_dialog.o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Event Watcher"));
    LINE(2298,sv_dialog.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    LINE(2299,sv_dialog.o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 200LL, 110LL));
    (v_vbox = sv_dialog.o_get("vbox", 0x7DA40F8F1BC6EDBFLL));
    (v_action_area = sv_dialog.o_get("action_area", 0x37BC1B12C5D8C1FDLL));
    (v_button = LINE(2304,create_object("gtktogglebutton", Array(ArrayInit(1).set(0, "Activate Watch").create()))));
    LINE(2305,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 4, "clicked", "event_watcher_toggle", ref(sv_event_watcher_enter_id), ref(sv_event_watcher_leave_id)));
    LINE(2306,v_button.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(2307,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(2308,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(2310,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Close").create()))));
    LINE(2311,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 4, "clicked", "event_watcher_down", ref(sv_event_watcher_enter_id), ref(sv_event_watcher_leave_id)));
    LINE(2312,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, sv_dialog).set(1, "destroy").create())));
    LINE(2313,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(2314,v_action_area.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(2315,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(2316,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (!((toBoolean(bitwise_and(LINE(2318,sv_dialog.o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))))) LINE(2319,sv_dialog.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  else LINE(2321,sv_dialog.o_invoke_few_args("destroy", 0x45CCFF10463AB662LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1653 */
Object f_create_bbox(bool v_horizontal, CStrRef v_title, Variant v_spacing, Variant v_child_w, Variant v_child_h, Variant v_layout) {
  FUNCTION_INJECTION(create_bbox);
  Object v_frame;
  Variant v_bbox;
  Variant v_button;

  (v_frame = LINE(1656,create_object("gtkframe", Array(ArrayInit(1).set(0, v_title).create()))));
  if (v_horizontal) (v_bbox = LINE(1658,create_object("gtkhbuttonbox", Array())));
  else (v_bbox = LINE(1660,create_object("gtkvbuttonbox", Array())));
  LINE(1661,v_bbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
  LINE(1662,v_bbox.o_invoke_few_args("set_layout", 0x6E9D412620E694EDLL, 1, v_layout));
  LINE(1663,v_bbox.o_invoke_few_args("set_spacing", 0x049EECC16DACAABBLL, 1, v_spacing));
  LINE(1664,v_bbox.o_invoke_few_args("set_child_size", 0x6C391621C685A427LL, 2, v_child_w, v_child_h));
  LINE(1665,v_frame->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_bbox));
  LINE(1666,v_bbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  (v_button = LINE(1668,create_object("gtkbutton", Array(ArrayInit(1).set(0, "OK").create()))));
  LINE(1669,v_bbox.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_button));
  LINE(1670,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  (v_button = LINE(1672,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Cancel").create()))));
  LINE(1673,v_bbox.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_button));
  LINE(1674,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  (v_button = LINE(1676,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Help").create()))));
  LINE(1677,v_bbox.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_button));
  LINE(1678,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(1680,v_frame->o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  return v_frame;
} /* function */
/* SRC: roadsend/tests/gtk.php line 89 */
void f_toggle_reorderable(CVarRef v_button, CVarRef v_clist) {
  FUNCTION_INJECTION(toggle_reorderable);
  LINE(91,toObject(v_clist)->o_invoke_few_args("set_reorderable", 0x69A345EF30A46B0ELL, 1, toObject(v_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1008 */
void f_create_color_selection() {
  FUNCTION_INJECTION(create_color_selection);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_colorsel;
  Variant v_cancel_button;
  Variant v_ok_button;

  if (!(isset(gv_windows, "color_selection", 0x3AD99795DD8335FDLL))) {
    (v_window = LINE(1013,create_object("gtkcolorselectiondialog", Array(ArrayInit(1).set(0, "color selection dialog").create()))));
    gv_windows.set("color_selection", (v_window), 0x3AD99795DD8335FDLL);
    (v_colorsel = v_window.o_get("colorsel", 0x2DCA74CA33E5E6D5LL));
    LINE(1016,v_colorsel.o_invoke_few_args("set_opacity", 0x59824259A2E2F1C0LL, 1, true));
    LINE(1017,v_colorsel.o_invoke_few_args("set_update_policy", 0x69A61CC8F7D87D31LL, 1, k_GTK_UPDATE_CONTINUOUS));
    LINE(1018,v_colorsel.o_invoke_few_args("set_color", 0x31838CF336DA6E21LL, 4, 0.40000000000000002, 0.5, 0.69999999999999996, 0.75));
    LINE(1019,v_window->o_invoke_few_args("set_position", 0x17D027C2B74A20CBLL, 1, k_GTK_WIN_POS_MOUSE));
    LINE(1020,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    (v_cancel_button = v_window.o_get("cancel_button", 0x289F7B2EC5848D75LL));
    LINE(1023,v_cancel_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    (v_ok_button = v_window.o_get("ok_button", 0x67BDCAE85F751474LL));
    LINE(1025,v_ok_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
  }
  if (toBoolean(bitwise_and(LINE(1027,gv_windows.rvalAt("color_selection", 0x3AD99795DD8335FDLL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1028,gv_windows.rvalAt("color_selection", 0x3AD99795DD8335FDLL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1030,gv_windows.rvalAt("color_selection", 0x3AD99795DD8335FDLL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2417 */
void f_scrollable_notebook(CVarRef v_menuitem, Variant v_notebook) {
  FUNCTION_INJECTION(scrollable_notebook);
  LINE(2419,v_notebook.o_invoke_few_args("set_show_tabs", 0x5F73C9EB66BC4DA5LL, 1, true));
  LINE(2420,v_notebook.o_invoke_few_args("set_scrollable", 0x711D7937F2003374LL, 1, true));
  if (equal(LINE(2421,x_count(v_notebook.o_invoke_few_args("children", 0x2B299E949E2E82ADLL, 0))), 5LL)) LINE(2422,f_create_pages(ref(v_notebook), 6LL, 15LL));
} /* function */
/* SRC: roadsend/tests/gtk.php line 105 */
void f_dnd_drag_data_get(CVarRef v_widget, CVarRef v_context, CVarRef v_selection_data, CVarRef v_info, CVarRef v_time) {
  FUNCTION_INJECTION(dnd_drag_data_get);
  Variant v_dnd_string;

  (v_dnd_string = "Perl is the only language that looks\nthe same before and after RSA encryption");
  LINE(108,toObject(v_selection_data)->o_invoke_few_args("set", 0x399A6427C2185621LL, 3, toObject(v_selection_data).o_lval("target", 0x22F784C030DE5CB0LL), 8LL, v_dnd_string));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1286 */
void f_add10000_clist(CVarRef v_button, CVarRef v_clist) {
  FUNCTION_INJECTION(add10000_clist);
  int64 v_i = 0;
  Variant v_texts;
  Variant v_row;

  {
    LOOP_COUNTER(23);
    for ((v_i = 0LL); less(v_i, 12LL); v_i++) {
      LOOP_COUNTER_CHECK(23);
      v_texts.set(v_i, (toString("Column ") + toString(v_i)));
    }
  }
  v_texts.set(1LL, ("Right"), 0x5BCA7C69B794F8CELL);
  v_texts.set(2LL, ("Center"), 0x486AFCC090D5F98CLL);
  LINE(1293,toObject(v_clist)->o_invoke_few_args("freeze", 0x6A7D0E8BAA76B7E7LL, 0));
  {
    LOOP_COUNTER(24);
    for ((v_i = 0LL); less(v_i, 10000LL); v_i++) {
      LOOP_COUNTER_CHECK(24);
      {
        v_texts.set(0LL, (concat("CListRow ", (toString(modulo(LINE(1295,x_rand()), 10000LL))))), 0x77CFA1EEF01BCA90LL);
        (v_row = LINE(1296,toObject(v_clist)->o_invoke_few_args("append", 0x4DEE4A472DC69EC2LL, 1, v_texts)));
      }
    }
  }
  LINE(1298,toObject(v_clist)->o_invoke_few_args("thaw", 0x64F2171F59D6F356LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2334 */
void f_page_switch(CVarRef v_notebook, CVarRef v_page, Variant v_page_num) {
  FUNCTION_INJECTION(page_switch);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_book_open __attribute__((__unused__)) = g->GV(book_open);
  Variant &gv_book_open_mask __attribute__((__unused__)) = g->GV(book_open_mask);
  Variant &gv_book_closed __attribute__((__unused__)) = g->GV(book_closed);
  Variant &gv_book_closed_mask __attribute__((__unused__)) = g->GV(book_closed_mask);
  Variant v_child;
  Variant v_tab_label;
  Variant v_children;
  Variant v_pixwid;

  {
  }
  {
    LOOP_COUNTER(25);
    Variant map26 = LINE(2344,toObject(v_notebook)->o_invoke_few_args("children", 0x2B299E949E2E82ADLL, 0));
    for (ArrayIterPtr iter27 = map26.begin(); !iter27->end(); iter27->next()) {
      LOOP_COUNTER_CHECK(25);
      v_child = iter27->second();
      {
        (v_tab_label = LINE(2345,toObject(v_notebook)->o_invoke_few_args("get_tab_label", 0x467E3AFD302BE1FFLL, 1, v_child)));
        (v_children = LINE(2346,v_tab_label.o_invoke_few_args("children", 0x2B299E949E2E82ADLL, 0)));
        (v_pixwid = v_children.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
        LINE(2348,v_pixwid.o_invoke_few_args("set", 0x399A6427C2185621LL, 2, gv_book_closed, gv_book_closed_mask));
      }
    }
  }
  (v_tab_label = LINE(2352,toObject(v_notebook)->o_invoke_few_args("get_tab_label", 0x467E3AFD302BE1FFLL, 1, toObject(v_notebook)->o_invoke_few_args("get_nth_page", 0x1F453A085C51BCAFLL, 1, v_page_num))));
  (v_children = LINE(2353,v_tab_label.o_invoke_few_args("children", 0x2B299E949E2E82ADLL, 0)));
  (v_pixwid = v_children.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  LINE(2355,v_pixwid.o_invoke_few_args("set", 0x399A6427C2185621LL, 2, gv_book_open, gv_book_open_mask));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2389 */
void f_notebook_rotate(CVarRef v_notebook) {
  FUNCTION_INJECTION(notebook_rotate);
  LINE(2391,toObject(v_notebook)->o_invoke_few_args("set_tab_pos", 0x3259FA00C1B53E72LL, 1, modulo((toInt64(toObject(v_notebook).o_get("tab_pos", 0x77D5FE2E1DECB8D4LL) + 1LL)), 4LL)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1848 */
void f_create_toggle_buttons() {
  FUNCTION_INJECTION(create_toggle_buttons);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_box1;
  Variant v_box2;
  int64 v_i = 0;
  Variant v_button;
  Variant v_separator;

  if (!(isset(gv_windows, "toggle_buttons", 0x7CBA6C56AF55EC49LL))) {
    (v_window = LINE(1853,create_object("gtkwindow", Array())));
    gv_windows.set("toggle_buttons", (v_window), 0x7CBA6C56AF55EC49LL);
    LINE(1855,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1856,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Toggle buttons"));
    LINE(1857,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_box1 = LINE(1859,create_object("gtkvbox", Array())));
    LINE(1860,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    LINE(1861,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1863,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1864,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1865,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_box2));
    LINE(1866,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    {
      LOOP_COUNTER(28);
      for ((v_i = 1LL); not_more(v_i, 4LL); v_i++) {
        LOOP_COUNTER_CHECK(28);
        {
          (v_button = LINE(1869,create_object("gtktogglebutton", Array(ArrayInit(1).set(0, concat("button", toString(v_i))).create()))));
          LINE(1870,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
          LINE(1871,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
        }
      }
    }
    (v_separator = LINE(1874,create_object("gtkhseparator", Array())));
    LINE(1875,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1876,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_box2 = LINE(1878,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1879,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1880,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
    LINE(1881,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1883,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1884,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1885,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1886,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1887,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1888,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1890,gv_windows.rvalAt("toggle_buttons", 0x7CBA6C56AF55EC49LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1891,gv_windows.rvalAt("toggle_buttons", 0x7CBA6C56AF55EC49LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1893,gv_windows.rvalAt("toggle_buttons", 0x7CBA6C56AF55EC49LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 435 */
void f_set_background(CVarRef v_ctree, Variant v_node) {
  FUNCTION_INJECTION(set_background);
  Variant v_style;

  if (!(toBoolean(v_node))) return;
  if (!equal(toObject(v_ctree).o_get("line_style", 0x3D983ECCDE1F4BC4LL), k_GTK_CTREE_LINES_TABBED)) {
    if (!(toBoolean(v_node.o_get("is_leaf", 0x270D3D1D2AB727B3LL)))) (v_style = LINE(442,toObject(v_ctree)->o_invoke_few_args("node_get_row_data", 0x2BAA12D14CBCB1EDLL, 1, v_node)));
    else (v_style = LINE(444,toObject(v_ctree)->o_invoke_few_args("node_get_row_data", 0x2BAA12D14CBCB1EDLL, 1, v_node.o_lval("parent", 0x16E2F26FFB10FD8CLL))));
  }
  LINE(447,toObject(v_ctree)->o_invoke_few_args("node_set_row_style", 0x3B8087F7DDE997E0LL, 2, v_node, v_style));
} /* function */
/* SRC: roadsend/tests/gtk.php line 450 */
void f_ctree_toggle_line_style(CVarRef v_menu_item, CVarRef v_ctree, Variant v_line_style) {
  FUNCTION_INJECTION(ctree_toggle_line_style);
  if ((equal(toObject(v_ctree).o_get("line_style", 0x3D983ECCDE1F4BC4LL), k_GTK_CTREE_LINES_TABBED) && !equal(v_line_style, k_GTK_CTREE_LINES_TABBED)) || (!equal(toObject(v_ctree).o_get("line_style", 0x3D983ECCDE1F4BC4LL), k_GTK_CTREE_LINES_TABBED) && equal(v_line_style, k_GTK_CTREE_LINES_TABBED))) {
    LINE(456,toObject(v_ctree)->o_invoke_few_args("pre_recursive", 0x792C6897966D59A6LL, 2, null, "set_background"));
  }
  LINE(459,toObject(v_ctree)->o_invoke_few_args("set_line_style", 0x669988BAEAB70E2ALL, 1, v_line_style));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2261 */
void f_event_watcher_toggle(Variant v_event_watcher_enter_id, Variant v_event_watcher_leave_id) {
  FUNCTION_INJECTION(event_watcher_toggle);
  Variant eo_0;
  Variant eo_1;
  Variant v_signal_id;

  if (toBoolean(v_event_watcher_enter_id)) LINE(2264,f_event_watcher_down(ref(v_event_watcher_enter_id), ref(v_event_watcher_leave_id)));
  else {
    (v_signal_id = LINE(2267,(assignCallTemp(eo_1, throw_fatal("unknown class gtkwidget", ((void*)NULL))),throw_fatal("unknown class gtk", (throw_fatal("unknown class gtkwidget", ((void*)NULL)), (void*)NULL)))));
    (v_event_watcher_enter_id = LINE(2268,throw_fatal("unknown class gtk", ((void*)NULL))));
    (v_signal_id = LINE(2269,(assignCallTemp(eo_1, throw_fatal("unknown class gtkwidget", ((void*)NULL))),throw_fatal("unknown class gtk", (throw_fatal("unknown class gtkwidget", ((void*)NULL)), (void*)NULL)))));
    (v_event_watcher_leave_id = LINE(2270,throw_fatal("unknown class gtk", ((void*)NULL))));
  }
} /* function */
/* SRC: roadsend/tests/gtk.php line 310 */
void f_ctree_click_column(CVarRef v_ctree, Variant v_column) {
  FUNCTION_INJECTION(ctree_click_column);
  if (equal(v_column, toObject(v_ctree).o_get("sort_column", 0x31E555E7D71EBDD2LL))) {
    if (equal(toObject(v_ctree).o_get("sort_type", 0x4EEE890B8327DB1CLL), k_GTK_SORT_ASCENDING)) LINE(314,toObject(v_ctree)->o_invoke_few_args("set_sort_type", 0x13F19728C07E2FA7LL, 1, k_GTK_SORT_DESCENDING));
    else LINE(316,toObject(v_ctree)->o_invoke_few_args("set_sort_type", 0x13F19728C07E2FA7LL, 1, k_GTK_SORT_ASCENDING));
  }
  else LINE(318,toObject(v_ctree)->o_invoke_few_args("set_sort_column", 0x09E28CA07967193DLL, 1, v_column));
  LINE(320,toObject(v_ctree)->o_invoke_few_args("sort_recursive", 0x39D2F6D8B04F58A9LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1948 */
void f_entry_toggle_sensitive(CVarRef v_check_button, CVarRef v_entry) {
  FUNCTION_INJECTION(entry_toggle_sensitive);
  LINE(1950,toObject(v_entry)->o_invoke_few_args("set_sensitive", 0x571C73DB98E24A4ALL, 1, toObject(v_check_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 406 */
void f_remove_selection(CVarRef v_button, CVarRef v_ctree) {
  FUNCTION_INJECTION(remove_selection);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_ctree_data __attribute__((__unused__)) = g->GV(ctree_data);
  Variant v_node;

  LINE(410,toObject(v_ctree)->o_invoke_few_args("freeze", 0x6A7D0E8BAA76B7E7LL, 0));
  LOOP_COUNTER(29);
  {
    while (!same(((v_node = toObject(v_ctree).o_get("selection", 0x2781B225767237F1LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), null)) {
      LOOP_COUNTER_CHECK(29);
      {
        if (toBoolean(v_node.o_get("is_leaf", 0x270D3D1D2AB727B3LL))) lval(gv_ctree_data.lvalAt("pages", 0x3EA464DEB0A970C3LL))--;
        else LINE(416,toObject(v_ctree)->o_invoke_few_args("post_recursive", 0x0C317581F469929ELL, 2, v_node, "count_items"));
        LINE(418,toObject(v_ctree)->o_invoke_few_args("remove_node", 0x34B761903D34C7D2LL, 1, v_node));
        if (equal(toObject(v_ctree).o_get("selection_mode", 0x0FE22FFCE6AB82F1LL), k_GTK_SELECTION_BROWSE)) break;
      }
    }
  }
  if (equal(toObject(v_ctree).o_get("selection_mode", 0x0FE22FFCE6AB82F1LL), k_GTK_SELECTION_EXTENDED) && same(toObject(v_ctree).o_get("selection", 0x2781B225767237F1LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL), null) && not_less(toObject(v_ctree).o_get("focus_row", 0x06A25AAAF397E6B3LL), 0LL)) {
    (v_node = LINE(426,toObject(v_ctree)->o_invoke_few_args("node_nth", 0x1BB6F7054AFAF539LL, 1, toObject(v_ctree).o_lval("focus_row", 0x06A25AAAF397E6B3LL))));
    if (toBoolean(v_node)) LINE(428,toObject(v_ctree)->o_invoke_few_args("select", 0x5133F4B2CD4CBD41LL, 1, v_node));
  }
  LINE(431,toObject(v_ctree)->o_invoke_few_args("thaw", 0x64F2171F59D6F356LL, 0));
  LINE(432,f_after_press(v_ctree));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1182 */
void f_insert_row_clist(CVarRef v_button, CVarRef v_clist) {
  FUNCTION_INJECTION(insert_row_clist);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_style1 __attribute__((__unused__)) = g->sv_insert_row_clist_DupIdstyle1;
  bool &inited_sv_style1 __attribute__((__unused__)) = g->inited_sv_insert_row_clist_DupIdstyle1;
  Variant &sv_style2 __attribute__((__unused__)) = g->sv_insert_row_clist_DupIdstyle2;
  bool &inited_sv_style2 __attribute__((__unused__)) = g->inited_sv_insert_row_clist_DupIdstyle2;
  Variant &sv_style3 __attribute__((__unused__)) = g->sv_insert_row_clist_DupIdstyle3;
  bool &inited_sv_style3 __attribute__((__unused__)) = g->inited_sv_insert_row_clist_DupIdstyle3;
  Variant v_text;
  Variant v_row;
  Object v_col1;
  Object v_col2;
  Variant v_style;

  {
    if (!inited_sv_style1) {
      (sv_style1 = null);
      inited_sv_style1 = true;
    }
    if (!inited_sv_style2) {
      (sv_style2 = null);
      inited_sv_style2 = true;
    }
    if (!inited_sv_style3) {
      (sv_style3 = null);
      inited_sv_style3 = true;
    }
  }
  (v_text = ScalarArrays::sa_[8]);
  if (not_less(toObject(v_clist).o_get("focus_row", 0x06A25AAAF397E6B3LL), 0LL)) (v_row = LINE(1191,toObject(v_clist)->o_invoke_few_args("insert", 0x61F327248511D92ELL, 2, toObject(v_clist).o_lval("focus_row", 0x06A25AAAF397E6B3LL), v_text)));
  else (v_row = LINE(1193,toObject(v_clist)->o_invoke_few_args("prepend", 0x65C5EF328C77C917LL, 1, v_text)));
  if (!(isset(sv_style1))) {
    (v_col1 = LINE(1196,create_object("gdkcolor", Array(ArrayInit(3).set(0, 0LL).set(1, 56000LL).set(2, 0LL).create()))));
    (v_col2 = LINE(1197,create_object("gdkcolor", Array(ArrayInit(3).set(0, 32000LL).set(1, 0LL).set(2, 56000LL).create()))));
    (v_style = toObject(v_clist).o_get("style", 0x040EF9119982696ALL));
    (sv_style1 = LINE(1200,v_style.o_invoke_few_args("copy", 0x10D6AE9D688D1C68LL, 0)));
    lval(sv_style1.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_col1));
    lval(sv_style1.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_SELECTED, (v_col2));
    (sv_style2 = LINE(1204,v_style.o_invoke_few_args("copy", 0x10D6AE9D688D1C68LL, 0)));
    lval(sv_style2.o_lval("fg", 0x297040659785B670LL)).set(k_GTK_STATE_NORMAL, (v_col1));
    lval(sv_style2.o_lval("fg", 0x297040659785B670LL)).set(k_GTK_STATE_SELECTED, (v_col2));
    (sv_style3 = LINE(1208,v_style.o_invoke_few_args("copy", 0x10D6AE9D688D1C68LL, 0)));
    lval(sv_style3.o_lval("fg", 0x297040659785B670LL)).set(k_GTK_STATE_NORMAL, (v_col1));
    lval(sv_style3.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_col2));
    (sv_style3.o_lval("font", 0x7C274F8B4A4D4058LL) = LINE(1211,throw_fatal("unknown class gdk", ((void*)NULL))));
  }
  LINE(1214,toObject(v_clist)->o_invoke_few_args("set_cell_style", 0x0CDF776A336AA828LL, 3, v_row, 3LL, sv_style1));
  LINE(1215,toObject(v_clist)->o_invoke_few_args("set_cell_style", 0x0CDF776A336AA828LL, 3, v_row, 4LL, sv_style2));
  LINE(1216,toObject(v_clist)->o_invoke_few_args("set_cell_style", 0x0CDF776A336AA828LL, 3, v_row, 0LL, sv_style3));
} /* function */
/* SRC: roadsend/tests/gtk.php line 323 */
void f_change_row_height(CVarRef v_adj, CVarRef v_ctree) {
  FUNCTION_INJECTION(change_row_height);
  LINE(325,toObject(v_ctree)->o_invoke_few_args("set_row_height", 0x707817F826E2CF49LL, 1, toObject(v_adj).o_lval("value", 0x69E7413AE0C88471LL)));
} /* function */
/* SRC: roadsend/tests/gtk.php line 350 */
void f_change_style(CVarRef v_button, CVarRef v_ctree) {
  FUNCTION_INJECTION(change_style);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_style1 __attribute__((__unused__)) = g->sv_change_style_DupIdstyle1;
  bool &inited_sv_style1 __attribute__((__unused__)) = g->inited_sv_change_style_DupIdstyle1;
  Variant &sv_style2 __attribute__((__unused__)) = g->sv_change_style_DupIdstyle2;
  bool &inited_sv_style2 __attribute__((__unused__)) = g->inited_sv_change_style_DupIdstyle2;
  Variant v_node;
  Object v_col1;
  Object v_col2;

  {
    if (!inited_sv_style1) {
      (sv_style1 = null);
      inited_sv_style1 = true;
    }
    if (!inited_sv_style2) {
      (sv_style2 = null);
      inited_sv_style2 = true;
    }
  }
  if (not_less(toObject(v_ctree).o_get("focus_row", 0x06A25AAAF397E6B3LL), 0LL)) (v_node = toObject(v_ctree).o_get("row_list", 0x5355B9243688E9CELL).rvalAt(toObject(v_ctree).o_get("focus_row", 0x06A25AAAF397E6B3LL)));
  else (v_node = toObject(v_ctree).o_get("row_list", 0x5355B9243688E9CELL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  if (!(LINE(359,x_is_object(v_node)))) return;
  if (!(isset(sv_style1))) {
    (v_col1 = LINE(363,create_object("gdkcolor", Array(ArrayInit(3).set(0, 0LL).set(1, 56000LL).set(2, 0LL).create()))));
    (v_col2 = LINE(364,create_object("gdkcolor", Array(ArrayInit(3).set(0, 32000LL).set(1, 0LL).set(2, 56000LL).create()))));
    (sv_style1 = LINE(366,create_object("gtkstyle", Array())));
    lval(sv_style1.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_col1));
    lval(sv_style1.o_lval("fg", 0x297040659785B670LL)).set(k_GTK_STATE_SELECTED, (v_col2));
    (sv_style2 = LINE(370,create_object("gtkstyle", Array())));
    lval(sv_style2.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_SELECTED, (v_col2));
    lval(sv_style2.o_lval("fg", 0x297040659785B670LL)).set(k_GTK_STATE_NORMAL, (v_col1));
    lval(sv_style2.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_col2));
    (sv_style2.o_lval("font", 0x7C274F8B4A4D4058LL) = LINE(374,throw_fatal("unknown class gdk", ((void*)NULL))));
  }
  LINE(377,toObject(v_ctree)->o_invoke_few_args("node_set_cell_style", 0x1765CAE0BE2C08F8LL, 3, v_node, 1LL, sv_style1));
  LINE(378,toObject(v_ctree)->o_invoke_few_args("node_set_cell_style", 0x1765CAE0BE2C08F8LL, 3, v_node, 0LL, sv_style2));
  if (toBoolean(v_node.o_get("children", 0x1F588E8E717877F6LL))) LINE(381,toObject(v_ctree)->o_invoke_few_args("node_set_row_style", 0x3B8087F7DDE997E0LL, 2, lval(v_node.o_lval("children", 0x1F588E8E717877F6LL)).refvalAt(0LL, 0x77CFA1EEF01BCA90LL), sv_style2));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1638 */
void f_create_button_box() {
  FUNCTION_INJECTION(create_button_box);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Object v_window;
  Variant v_main_vbox;
  Variant v_frame_horz;
  Variant v_vbox;
  Variant v_frame_vert;
  Variant v_hbox;
  Variant v_separator;
  Variant v_button;

  if (!(isset(gv_windows, "button_box", 0x4B432EAFBBCE3F08LL))) {
    (v_window = LINE(1643,create_object("gtkwindow", Array())));
    gv_windows.set("button_box", (v_window), 0x4B432EAFBBCE3F08LL);
    LINE(1645,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
    LINE(1646,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Button Boxes"));
    LINE(1647,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    (v_main_vbox = LINE(1649,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
    LINE(1650,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_main_vbox));
    LINE(1651,v_main_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_frame_horz = LINE(1685,create_object("gtkframe", Array(ArrayInit(1).set(0, "Horizontal Button Boxes").create()))));
    LINE(1686,v_main_vbox.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_frame_horz));
    LINE(1687,v_frame_horz.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_vbox = LINE(1689,create_object("gtkvbox", Array())));
    LINE(1690,v_vbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1691,v_frame_horz.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_vbox));
    LINE(1692,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (assignCallTemp(eo_0, ref(LINE(1694,f_create_bbox(true, "Spread", 40LL, 85LL, 20LL, k_GTK_BUTTONBOX_SPREAD)))),v_vbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 10LL).create()), 0x2D6BDE948B85D9F0LL));
    (assignCallTemp(eo_0, ref(LINE(1695,f_create_bbox(true, "Edge", 40LL, 85LL, 20LL, k_GTK_BUTTONBOX_EDGE)))),v_vbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 10LL).create()), 0x2D6BDE948B85D9F0LL));
    (assignCallTemp(eo_0, ref(LINE(1696,f_create_bbox(true, "Start", 40LL, 85LL, 20LL, k_GTK_BUTTONBOX_START)))),v_vbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 10LL).create()), 0x2D6BDE948B85D9F0LL));
    (assignCallTemp(eo_0, ref(LINE(1697,f_create_bbox(true, "End", 40LL, 85LL, 20LL, k_GTK_BUTTONBOX_END)))),v_vbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 10LL).create()), 0x2D6BDE948B85D9F0LL));
    (v_frame_vert = LINE(1699,create_object("gtkframe", Array(ArrayInit(1).set(0, "Vertical Button Boxes").create()))));
    LINE(1700,v_main_vbox.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_frame_vert));
    LINE(1701,v_frame_vert.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_hbox = LINE(1703,create_object("gtkhbox", Array())));
    LINE(1704,v_hbox.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(1705,v_frame_vert.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_hbox));
    LINE(1706,v_hbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (assignCallTemp(eo_0, ref(LINE(1708,f_create_bbox(false, "Spread", 30LL, 85LL, 20LL, k_GTK_BUTTONBOX_SPREAD)))),v_hbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 5LL).create()), 0x2D6BDE948B85D9F0LL));
    (assignCallTemp(eo_0, ref(LINE(1709,f_create_bbox(false, "Edge", 30LL, 85LL, 20LL, k_GTK_BUTTONBOX_EDGE)))),v_hbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 5LL).create()), 0x2D6BDE948B85D9F0LL));
    (assignCallTemp(eo_0, ref(LINE(1710,f_create_bbox(false, "Start", 30LL, 85LL, 20LL, k_GTK_BUTTONBOX_START)))),v_hbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 5LL).create()), 0x2D6BDE948B85D9F0LL));
    (assignCallTemp(eo_0, ref(LINE(1711,f_create_bbox(false, "End", 30LL, 85LL, 20LL, k_GTK_BUTTONBOX_END)))),v_hbox.o_invoke("pack_start", Array(ArrayInit(4).set(0, eo_0).set(1, true).set(2, true).set(3, 5LL).create()), 0x2D6BDE948B85D9F0LL));
    (v_separator = LINE(1713,create_object("gtkhseparator", Array())));
    LINE(1714,v_main_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
    LINE(1715,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    (v_button = LINE(1717,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(1718,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(1719,v_main_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
    LINE(1720,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(1721,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
    LINE(1722,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(1725,gv_windows.rvalAt("button_box", 0x4B432EAFBBCE3F08LL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(1726,gv_windows.rvalAt("button_box", 0x4B432EAFBBCE3F08LL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(1728,gv_windows.rvalAt("button_box", 0x4B432EAFBBCE3F08LL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2595 */
void f_create_main_window() {
  FUNCTION_INJECTION(create_main_window);
  Variant v_buttons;
  Object v_window;
  Variant v_box1;
  Variant v_scrolled_window;
  Variant v_box2;
  Primitive v_label = 0;
  Variant v_function;
  Variant v_button;
  Variant v_separator;

  (v_buttons = ScalarArrays::sa_[10]);
  (v_window = LINE(2620,create_object("gtkwindow", Array())));
  LINE(2621,v_window->o_invoke_few_args("set_policy", 0x1AECF3B49AF6B58BLL, 3, false, false, false));
  LINE(2622,v_window->o_invoke_few_args("set_name", 0x20D03A6494326391LL, 1, "main window"));
  LINE(2623,v_window->o_invoke_few_args("set_usize", 0x49B5982D6042740ALL, 2, 200LL, 400LL));
  LINE(2624,v_window->o_invoke_few_args("set_position", 0x17D027C2B74A20CBLL, 1, k_GTK_WIN_POS_CENTER));
  LINE(2627,v_window->o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "destroy", Array(ArrayInit(2).set(0, "gtk").set(1, "main_quit").create())));
  LINE(2628,v_window->o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "delete-event", Array(ArrayInit(2).set(0, "gtk").set(1, "false").create())));
  (v_box1 = LINE(2630,create_object("gtkvbox", Array())));
  LINE(2631,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
  (v_scrolled_window = LINE(2633,create_object("gtkscrolledwindow", Array())));
  LINE(2634,v_scrolled_window.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
  LINE(2635,v_scrolled_window.o_invoke_few_args("set_policy", 0x1AECF3B49AF6B58BLL, 2, k_GTK_POLICY_AUTOMATIC, k_GTK_POLICY_AUTOMATIC));
  LINE(2637,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_scrolled_window));
  (v_box2 = LINE(2639,create_object("gtkvbox", Array())));
  LINE(2640,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
  LINE(2641,v_scrolled_window.o_invoke_few_args("add_with_viewport", 0x5B780EBB29AE5F68LL, 1, v_box2));
  LINE(2642,v_box2.o_invoke_few_args("set_focus_vadjustment", 0x3C597CED3819AA58LL, 1, v_scrolled_window.o_invoke_few_args("get_vadjustment", 0x7802ADA8482B7832LL, 0)));
  LINE(2644,x_ksort(ref(v_buttons)));
  {
    LOOP_COUNTER(30);
    for (ArrayIterPtr iter32 = v_buttons.begin(); !iter32->end(); iter32->next()) {
      LOOP_COUNTER_CHECK(30);
      v_function = iter32->second();
      v_label = iter32->first();
      {
        (v_button = LINE(2646,create_object("gtkbutton", Array(ArrayInit(1).set(0, v_label).create()))));
        if (toBoolean(v_function)) LINE(2648,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", v_function));
        else LINE(2650,v_button.o_invoke_few_args("set_sensitive", 0x571C73DB98E24A4ALL, 1, false));
        LINE(2651,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
      }
    }
  }
  (v_separator = LINE(2654,create_object("gtkhseparator", Array())));
  LINE(2655,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_separator, false));
  (v_box2 = LINE(2657,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 10LL).create()))));
  LINE(2658,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
  LINE(2659,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 2, v_box2, false));
  (v_button = LINE(2661,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
  LINE(2662,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, "gtk").set(1, "main_quit").create())));
  LINE(2663,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_button));
  LINE(2664,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
  LINE(2665,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
  LINE(2666,v_window->o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 1318 */
void f_toggle_title_buttons(CVarRef v_button, CVarRef v_clist) {
  FUNCTION_INJECTION(toggle_title_buttons);
  if (toBoolean(LINE(1320,toObject(v_button)->o_invoke_few_args("get_active", 0x636F6AD960604DC7LL, 0)))) LINE(1321,toObject(v_clist)->o_invoke_few_args("column_titles_show", 0x5CCBDCCFCCF0A41ALL, 0));
  else LINE(1323,toObject(v_clist)->o_invoke_few_args("column_titles_hide", 0x57EF377CE03B34A6LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 2483 */
void f_create_notebook() {
  FUNCTION_INJECTION(create_notebook);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_windows __attribute__((__unused__)) = g->GV(windows);
  Variant &gv_sample_notebook __attribute__((__unused__)) = g->GV(sample_notebook);
  Variant &gv_book_open __attribute__((__unused__)) = g->GV(book_open);
  Variant &gv_book_open_mask __attribute__((__unused__)) = g->GV(book_open_mask);
  Variant &gv_book_closed __attribute__((__unused__)) = g->GV(book_closed);
  Variant &gv_book_closed_mask __attribute__((__unused__)) = g->GV(book_closed_mask);
  Variant &gv_book_open_xpm __attribute__((__unused__)) = g->GV(book_open_xpm);
  Variant &gv_book_closed_xpm __attribute__((__unused__)) = g->GV(book_closed_xpm);
  Array v_items;
  Object v_window;
  Variant v_box1;
  Variant v_separator;
  Variant v_box2;
  Variant v_button;
  Variant v_label;
  Variant v_omenu;

  {
  }
  (v_items = Array(ArrayInit(3).set(0, "Standard", Array(ArrayInit(2).set(0, "standard_notebook").setRef(1, ref(gv_sample_notebook)).create()), 0x7E8436B08D23ED46LL).set(1, "No tabs", Array(ArrayInit(2).set(0, "notabs_notebook").setRef(1, ref(gv_sample_notebook)).create()), 0x2EA92E6AF3F91D70LL).set(2, "Scrollable", Array(ArrayInit(2).set(0, "scrollable_notebook").setRef(1, ref(gv_sample_notebook)).create()), 0x5521E5BC32FADE4ALL).create()));
  if (!(isset(gv_windows, "notebook", 0x77E5BB4884A6B41ALL))) {
    (v_window = LINE(2497,create_object("gtkwindow", Array())));
    gv_windows.set("notebook", (v_window), 0x77E5BB4884A6B41ALL);
    LINE(2499,v_window->o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete_event", "delete_event"));
    LINE(2500,v_window->o_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Notebook"));
    LINE(2501,v_window->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 0LL));
    (v_box1 = LINE(2503,create_object("gtkvbox", Array(ArrayInit(2).set(0, false).set(1, 0LL).create()))));
    LINE(2504,v_box1.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2505,v_window->o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_box1));
    (gv_sample_notebook = LINE(2507,create_object("gtknotebook", Array())));
    LINE(2508,gv_sample_notebook.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2509,gv_sample_notebook.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "switch_page", "page_switch"));
    LINE(2510,gv_sample_notebook.o_invoke_few_args("set_tab_pos", 0x3259FA00C1B53E72LL, 1, k_GTK_POS_TOP));
    LINE(2511,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, gv_sample_notebook, true, true, 0LL));
    LINE(2512,gv_sample_notebook.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(2513,gv_sample_notebook.o_invoke_few_args("realize", 0x44BDBA31DBD5EDE9LL, 0));
    df_lambda_15(LINE(2515,throw_fatal("unknown class gdk", ((void*)NULL))), gv_book_open, gv_book_open_mask);
    df_lambda_16(LINE(2516,throw_fatal("unknown class gdk", ((void*)NULL))), gv_book_closed, gv_book_closed_mask);
    LINE(2518,f_create_pages(ref(gv_sample_notebook), 1LL, 5LL));
    (v_separator = LINE(2520,create_object("gtkhseparator", Array())));
    LINE(2521,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2522,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_separator, false, true, 10LL));
    (v_box2 = LINE(2524,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(2525,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2526,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(2527,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_box2, false, true, 0LL));
    (v_button = LINE(2529,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "popup menu").create()))));
    LINE(2530,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2531,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, false, 0LL));
    LINE(2532,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "notebook_popup", ref(gv_sample_notebook)));
    (v_button = LINE(2534,create_object("gtkcheckbutton", Array(ArrayInit(1).set(0, "homogeneous tabs").create()))));
    LINE(2535,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2536,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, false, 0LL));
    LINE(2537,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 3, "clicked", "notebook_homogeneous", ref(gv_sample_notebook)));
    (v_box2 = LINE(2539,create_object("gtkhbox", Array(ArrayInit(2).set(0, false).set(1, 5LL).create()))));
    LINE(2540,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2541,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(2542,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_box2, false, true, 0LL));
    (v_label = LINE(2544,create_object("gtklabel", Array(ArrayInit(1).set(0, "Notebook Style:").create()))));
    LINE(2545,v_label.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2546,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_label, false, true, 0LL));
    (v_omenu = LINE(2548,invoke_too_many_args("build_option_menu", (2), ((f_build_option_menu(v_items, 3LL))))));
    LINE(2549,v_omenu.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2550,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_omenu, false, true, 0LL));
    (v_button = LINE(2552,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Show all Pages").create()))));
    LINE(2553,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2554,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, false, true, 0LL));
    LINE(2555,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 3, "clicked", "show_all_pages", ref(gv_sample_notebook)));
    (v_box2 = LINE(2557,create_object("gtkhbox", Array(ArrayInit(2).set(0, true).set(1, 10LL).create()))));
    LINE(2558,v_box2.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2559,v_box2.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
    LINE(2560,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_box2, false, true, 0LL));
    (v_button = LINE(2562,create_object("gtkbutton", Array(ArrayInit(1).set(0, "prev").create()))));
    LINE(2563,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2564,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).setRef(0, ref(gv_sample_notebook)).set(1, "prev_page").create())));
    LINE(2565,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 0LL));
    (v_button = LINE(2567,create_object("gtkbutton", Array(ArrayInit(1).set(0, "next").create()))));
    LINE(2568,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2569,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).setRef(0, ref(gv_sample_notebook)).set(1, "next_page").create())));
    LINE(2570,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 0LL));
    (v_button = LINE(2572,create_object("gtkbutton", Array(ArrayInit(1).set(0, "rotate").create()))));
    LINE(2573,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2574,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 3, "clicked", "notebook_rotate", ref(gv_sample_notebook)));
    LINE(2575,v_box2.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, true, true, 0LL));
    (v_separator = LINE(2577,create_object("gtkhseparator", Array())));
    LINE(2578,v_separator.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2579,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_separator, false, true, 5LL));
    (v_button = LINE(2581,create_object("gtkbutton", Array(ArrayInit(1).set(0, "close").create()))));
    LINE(2582,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
    LINE(2583,v_button.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 5LL));
    LINE(2584,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "close_window"));
    LINE(2585,v_box1.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 4, v_button, false, false, 0LL));
    LINE(2586,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
    LINE(2587,v_button.o_invoke_few_args("grab_default", 0x2E371A59875C1F03LL, 0));
  }
  if (toBoolean(bitwise_and(LINE(2589,gv_windows.rvalAt("notebook", 0x77E5BB4884A6B41ALL).o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)), k_GTK_VISIBLE))) LINE(2590,gv_windows.rvalAt("notebook", 0x77E5BB4884A6B41ALL).o_invoke_few_args("hide", 0x7CE78D6EF77A659FLL, 0));
  else LINE(2592,gv_windows.rvalAt("notebook", 0x77E5BB4884A6B41ALL).o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk.php line 384 */
void f_select_all(CVarRef v_button, CVarRef v_ctree) {
  FUNCTION_INJECTION(select_all);
  LINE(386,toObject(v_ctree)->o_invoke_few_args("select_recursive", 0x378ED563828F0C65LL, 0));
  LINE(387,f_after_press(v_ctree));
} /* function */
Variant i_ctree_toggle_justify(CArrRef params) {
  return (f_ctree_toggle_justify(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
}
Variant i_clist_click_column(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x072911BD830E6AE6LL, clist_click_column) {
    return (f_clist_click_column(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_ctree(CArrRef params) {
  return (f_create_ctree(), null);
}
Variant i_create_radio_buttons(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x44FF2C27BBF50469LL, create_radio_buttons) {
    return (f_create_radio_buttons(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_close_window(CArrRef params) {
  return (f_close_window(params.rvalAt(0)), null);
}
Variant i_after_press(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4C8DDCC23C140BC2LL, after_press) {
    return (f_after_press(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_dnd_drag_data_received(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x494317E33B80700ELL, dnd_drag_data_received) {
    return (f_dnd_drag_data_received(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5), params.rvalAt(6)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_clist(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x54090F8C17BCC146LL, create_clist) {
    return (f_create_clist(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_dnd(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x582C538ADC3F65C0LL, create_dnd) {
    return (f_create_dnd(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_count_items(CArrRef params) {
  return (f_count_items(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_create_cursor_test(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4D5BC9812D84F9E1LL, create_cursor_test) {
    return (f_create_cursor_test(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_add1000_clist(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7CFF5D0BD80934D6LL, add1000_clist) {
    return (f_add1000_clist(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_event_watcher_down(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1359FB03ACE6804FLL, event_watcher_down) {
    return (f_event_watcher_down(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_label_toggle(CArrRef params) {
  return (f_label_toggle(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
}
Variant i_ctree_toggle_sel_mode(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5E14EB99DB122CE7LL, ctree_toggle_sel_mode) {
    return (f_ctree_toggle_sel_mode(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_check_buttons(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6B7B4DF21226BC7DLL, create_check_buttons) {
    return (f_create_check_buttons(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_dialog(CArrRef params) {
  return (f_create_dialog(), null);
}
Variant i_unselect_all(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x34E7981D141AC5CALL, unselect_all) {
    return (f_unselect_all(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_pixmap(CArrRef params) {
  return (f_create_pixmap(), null);
}
Variant i_tips_query_widget_selected(CArrRef params) {
  return (f_tips_query_widget_selected(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
}
Variant i_delete_event(CArrRef params) {
  return (f_delete_event(params.rvalAt(0), params.rvalAt(1)));
}
Variant i_entry_toggle_editable(CArrRef params) {
  return (f_entry_toggle_editable(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_collapse_all(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x47C4EB88B94935B4LL, collapse_all) {
    return (f_collapse_all(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_event_watcher(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2FDFE8DE686F7743LL, event_watcher) {
    return (f_event_watcher(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_file_selection(CArrRef params) {
  return (f_create_file_selection(), null);
}
Variant i_create_entry(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1C2B38292372A717LL, create_entry) {
    return (f_create_entry(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_show_all_pages(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x651DF620D1EC0815LL, show_all_pages) {
    return (f_show_all_pages(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_notebook_homogeneous(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x72176EE8E0D5A800LL, notebook_homogeneous) {
    return (f_notebook_homogeneous(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_panes(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4C1B610A31833FCCLL, create_panes) {
    return (f_create_panes(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_tab_fill(CArrRef params) {
  return (f_tab_fill(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
}
Variant i_create_tooltips(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0931C7E9A7FF636BLL, create_tooltips) {
    return (f_create_tooltips(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_file_selection_ok(CArrRef params) {
  return (f_file_selection_ok(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_change_spacing(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4C520A71B1626D63LL, change_spacing) {
    return (f_change_spacing(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_clist_remove_selection(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x45AD7A0A1B894E08LL, clist_remove_selection) {
    return (f_clist_remove_selection(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_expand_all(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x06ADBF78F0E43DEELL, expand_all) {
    return (f_expand_all(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_ctree_toggle_expander_style(CArrRef params) {
  return (f_ctree_toggle_expander_style(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
}
Variant i_create_buttons(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5A096B242AD32027LL, create_buttons) {
    return (f_create_buttons(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_clist_toggle_sel_mode(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1E299FA201A333EDLL, clist_toggle_sel_mode) {
    return (f_clist_toggle_sel_mode(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_tips_query_widget_entered(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7BF121FAFB9549B7LL, tips_query_widget_entered) {
    return (f_tips_query_widget_entered(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_change_indent(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x32C31C3B24F61ACDLL, change_indent) {
    return (f_change_indent(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_notebook_popup(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2EF7906930E3A2A4LL, notebook_popup) {
    return (f_notebook_popup(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_toggle_shrink(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6B7907E518F248A1LL, toggle_shrink) {
    return (f_toggle_shrink(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_set_cursor(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x112391F3498CA7D7LL, set_cursor) {
    return (f_set_cursor(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_notabs_notebook(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6B57EA993EDBB509LL, notabs_notebook) {
    return (f_notabs_notebook(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_toggle_resize(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x169473F0268896DBLL, toggle_resize) {
    return (f_toggle_resize(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_tab_pack(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x78CCE67FD354603CLL, tab_pack) {
    return (f_tab_pack(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_labels(CArrRef params) {
  return (f_create_labels(), null);
}
Variant i_standard_notebook(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x333C34127AF900B9LL, standard_notebook) {
    return (f_standard_notebook(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_expose_event(CArrRef params) {
  return (f_expose_event(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_rebuild_tree(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x49BE483695ACE957LL, rebuild_tree) {
    return (f_rebuild_tree(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_tab_expand(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7D15F4146ED9EC68LL, tab_expand) {
    return (f_tab_expand(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_button_press(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6BE440A542BBF20DLL, button_press) {
    return (f_button_press(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_entry_toggle_visibility(CArrRef params) {
  return (f_entry_toggle_visibility(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_create_event_watcher(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5374350081DC30ACLL, create_event_watcher) {
    return (f_create_event_watcher(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_toggle_reorderable(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6236D2EA586E1A47LL, toggle_reorderable) {
    return (f_toggle_reorderable(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_color_selection(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x544D0CA039C3E94CLL, create_color_selection) {
    return (f_create_color_selection(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_scrollable_notebook(CArrRef params) {
  return (f_scrollable_notebook(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_dnd_drag_data_get(CArrRef params) {
  return (f_dnd_drag_data_get(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
}
Variant i_add10000_clist(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3C4405A5FCCA8C99LL, add10000_clist) {
    return (f_add10000_clist(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_page_switch(CArrRef params) {
  return (f_page_switch(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
}
Variant i_notebook_rotate(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x65D387FED182AA04LL, notebook_rotate) {
    return (f_notebook_rotate(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_toggle_buttons(CArrRef params) {
  return (f_create_toggle_buttons(), null);
}
Variant i_set_background(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x240DACC1D1BC429ELL, set_background) {
    return (f_set_background(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_ctree_toggle_line_style(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x780ED099926F4C37LL, ctree_toggle_line_style) {
    return (f_ctree_toggle_line_style(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_event_watcher_toggle(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x55DE268C64DA1233LL, event_watcher_toggle) {
    return (f_event_watcher_toggle(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_ctree_click_column(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0C265A0211C4C445LL, ctree_click_column) {
    return (f_ctree_click_column(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_entry_toggle_sensitive(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2FD64FF97F186F54LL, entry_toggle_sensitive) {
    return (f_entry_toggle_sensitive(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_remove_selection(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x30142EAFFB1909BALL, remove_selection) {
    return (f_remove_selection(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_insert_row_clist(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x761A43F66700081FLL, insert_row_clist) {
    return (f_insert_row_clist(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_change_row_height(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x71DDE7AD570C3886LL, change_row_height) {
    return (f_change_row_height(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_change_style(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3A90852AE60A3D72LL, change_style) {
    return (f_change_style(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_button_box(CArrRef params) {
  return (f_create_button_box(), null);
}
Variant i_toggle_title_buttons(CArrRef params) {
  return (f_toggle_title_buttons(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_create_notebook(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x28C8E95171FA5432LL, create_notebook) {
    return (f_create_notebook(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_select_all(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x733D80171129C3DCLL, select_all) {
    return (f_select_all(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$gtk_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/gtk.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$gtk_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_windows __attribute__((__unused__)) = (variables != gVariables) ? variables->get("windows") : g->GV(windows);
  Variant &v_book_closed_xpm __attribute__((__unused__)) = (variables != gVariables) ? variables->get("book_closed_xpm") : g->GV(book_closed_xpm);
  Variant &v_book_open_xpm __attribute__((__unused__)) = (variables != gVariables) ? variables->get("book_open_xpm") : g->GV(book_open_xpm);
  Variant &v_ctree_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ctree_data") : g->GV(ctree_data);
  Variant &v_book_open __attribute__((__unused__)) = (variables != gVariables) ? variables->get("book_open") : g->GV(book_open);
  Variant &v_book_open_mask __attribute__((__unused__)) = (variables != gVariables) ? variables->get("book_open_mask") : g->GV(book_open_mask);
  Variant &v_book_closed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("book_closed") : g->GV(book_closed);
  Variant &v_book_closed_mask __attribute__((__unused__)) = (variables != gVariables) ? variables->get("book_closed_mask") : g->GV(book_closed_mask);
  Variant &v_sample_notebook __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sample_notebook") : g->GV(sample_notebook);
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;

  if (!(LINE(4,x_extension_loaded("gtk")))) {
    LINE(5,x_dl("php_gtk.so"));
  }
  (v_windows = ScalarArrays::sa_[0]);
  (v_book_closed_xpm = ScalarArrays::sa_[1]);
  (v_book_open_xpm = ScalarArrays::sa_[2]);
  v_ctree_data.set("books", (1LL), 0x06D8B08F90033F8ALL);
  v_ctree_data.set("pages", (0LL), 0x3EA464DEB0A970C3LL);
  setNull(v_book_open);
  setNull(v_book_open_mask);
  setNull(v_book_closed);
  setNull(v_book_closed_mask);
  setNull(v_sample_notebook);
  LINE(2669,throw_fatal("unknown class gtk", (concat(x_dirname(toString(v_argv.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), "/testgtkrc"), (void*)NULL)));
  LINE(2670,f_create_main_window());
  LINE(2671,throw_fatal("unknown class gtk", ((void*)NULL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
