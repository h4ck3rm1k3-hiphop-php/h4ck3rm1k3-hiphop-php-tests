
#include <php/roadsend/tests/globals.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/globals.php line 28 */
void f_funoftwoglobals() {
  FUNCTION_INJECTION(funoftwoglobals);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_foo __attribute__((__unused__)) = g->GV(foo);
  Variant &gv_bar __attribute__((__unused__)) = g->GV(bar);
  {
  }
  echo(LINE(32,concat3("in funoftwoglobals, foo is ", toString(gv_foo), "\n")));
  echo(LINE(33,concat3("in funoftwoglobals, bar is ", toString(gv_bar), "\n")));
  (gv_foo = 8LL);
  (gv_bar = gv_foo + gv_bar);
  echo(LINE(38,concat3("in funoftwoglobals, foo becomes ", toString(gv_foo), "\n")));
  echo(LINE(39,concat3("in funoftwoglobals, bar becomes ", toString(gv_bar), "\n")));
} /* function */
/* SRC: roadsend/tests/globals.php line 21 */
void f_funofoneglobal() {
  FUNCTION_INJECTION(funofoneglobal);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_foo __attribute__((__unused__)) = g->GV(foo);
  echo(LINE(25,concat3("in funofoneglobal, foo is ", toString(gv_foo), "\n")));
} /* function */
Variant pm_php$roadsend$tests$globals_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/globals.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$globals_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_foo = "this is variable 'foo'");
  (v_bar = 12LL);
  echo(LINE(7,concat3("foo: ", toString(v_foo), "\n")));
  echo(LINE(8,concat3("bar: ", toString(v_bar), "\n")));
  LINE(10,f_funofoneglobal());
  echo(LINE(12,concat3("foo: ", toString(v_foo), "\n")));
  echo(LINE(13,concat3("bar: ", toString(v_bar), "\n")));
  LINE(15,f_funoftwoglobals());
  echo(LINE(17,concat3("foo: ", toString(v_foo), "\n")));
  echo(LINE(18,concat3("bar: ", toString(v_bar), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
