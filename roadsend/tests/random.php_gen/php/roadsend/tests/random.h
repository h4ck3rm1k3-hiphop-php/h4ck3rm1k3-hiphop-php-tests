
#ifndef __GENERATED_php_roadsend_tests_random_h__
#define __GENERATED_php_roadsend_tests_random_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/random.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Numeric f_gen_random(double v_max);
Variant pm_php$roadsend$tests$random_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_random_h__
