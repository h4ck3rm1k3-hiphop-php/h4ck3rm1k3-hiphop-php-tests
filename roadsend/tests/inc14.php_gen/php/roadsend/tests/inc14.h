
#ifndef __GENERATED_php_roadsend_tests_inc14_h__
#define __GENERATED_php_roadsend_tests_inc14_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc14.fw.h>

// Declarations
#include <cls/afunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc14_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_afunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc14_h__
