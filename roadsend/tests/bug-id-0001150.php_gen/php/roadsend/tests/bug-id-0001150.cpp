
#include <php/roadsend/tests/bug-id-0001150.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_6(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_7(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001150_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001150.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001150_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arrayOuter __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arrayOuter") : g->GV(arrayOuter);
  Variant &v_arrayInner __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arrayInner") : g->GV(arrayInner);
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_placeholder __attribute__((__unused__)) = (variables != gVariables) ? variables->get("placeholder") : g->GV(placeholder);

  echo(" ");
  (v_arrayOuter = ScalarArrays::sa_[0]);
  (v_arrayInner = ScalarArrays::sa_[1]);
  print("Correct - with inner loop reset.\n");
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(8,x_each(ref(v_arrayOuter))), v_o))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(9,x_reset(ref(v_arrayInner)));
        LOOP_COUNTER(2);
        {
          while (toBoolean(df_lambda_2(LINE(10,x_each(ref(v_arrayInner))), v_i))) {
            LOOP_COUNTER_CHECK(2);
            {
              print(LINE(11,concat5("inloop ", toString(v_i), " for ", toString(v_o), "\n")));
            }
          }
        }
      }
    }
  }
  LINE(14,x_reset(ref(v_arrayOuter)));
  LINE(15,x_reset(ref(v_arrayInner)));
  print("What happens without inner loop reset.\n");
  LOOP_COUNTER(3);
  {
    while (toBoolean(df_lambda_3(LINE(19,x_each(ref(v_arrayOuter))), v_o))) {
      LOOP_COUNTER_CHECK(3);
      {
        LOOP_COUNTER(4);
        {
          while (toBoolean(df_lambda_4(LINE(20,x_each(ref(v_arrayInner))), v_i))) {
            LOOP_COUNTER_CHECK(4);
            {
              print(LINE(21,concat5("inloop ", toString(v_i), " for ", toString(v_o), "\n")));
            }
          }
        }
      }
    }
  }
  LINE(24,x_reset(ref(v_arrayOuter)));
  LINE(25,x_reset(ref(v_arrayInner)));
  print("What happens without inner loop reset but copy.\n");
  LOOP_COUNTER(5);
  {
    while (toBoolean(df_lambda_5(LINE(29,x_each(ref(v_arrayOuter))), v_o))) {
      LOOP_COUNTER_CHECK(5);
      {
        (v_arrayInner = v_arrayInner);
        LOOP_COUNTER(6);
        {
          while (toBoolean(df_lambda_6(LINE(33,x_each(ref(v_arrayInner))), v_i))) {
            LOOP_COUNTER_CHECK(6);
            {
              print(LINE(34,concat5("inloop ", toString(v_i), " for ", toString(v_o), "\n")));
            }
          }
        }
      }
    }
  }
  LINE(37,x_reset(ref(v_arrayOuter)));
  LINE(38,x_reset(ref(v_arrayInner)));
  print("What happens with inner loop reset over copy.\n");
  LOOP_COUNTER(7);
  {
    while (toBoolean(df_lambda_7(LINE(42,x_each(ref(v_arrayOuter))), v_o))) {
      LOOP_COUNTER_CHECK(7);
      {
        (v_placeholder = v_arrayInner);
        LOOP_COUNTER(8);
        {
          while (toBoolean(df_lambda_8(LINE(44,x_each(ref(v_placeholder))), v_i))) {
            LOOP_COUNTER_CHECK(8);
            {
              print(LINE(45,concat5("inloop ", toString(v_i), " for ", toString(v_o), "\n")));
            }
          }
        }
      }
    }
  }
  LINE(48,x_reset(ref(v_arrayOuter)));
  LINE(49,x_reset(ref(v_arrayInner)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
