
#include <php/roadsend/tests/ip.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/ip.php line 2 */
Variant f_necho(int64 v_line_number, CVarRef v_string) {
  FUNCTION_INJECTION(necho);
  echo(LINE(3,concat4(toString(v_line_number), ": ", toString(v_string), "\n")));
  return v_string;
} /* function */
Variant pm_php$roadsend$tests$ip_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/ip.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$ip_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  print(concat(toString(minus_rev(LINE(7,x_ip2long("127.0.0.1")), x_ip2long("127.0.0.255"))), "\n"));
  LINE(10,(assignCallTemp(eo_1, x_ip2long("127.0.0.1")),f_necho(420LL, eo_1)));
  LINE(11,(assignCallTemp(eo_1, x_ip2long("192.168.20.40")),f_necho(430LL, eo_1)));
  LINE(12,(assignCallTemp(eo_1, x_ip2long("199.0.0.2")),f_necho(440LL, eo_1)));
  LINE(13,(assignCallTemp(eo_1, x_ip2long("255.255.255.255")),f_necho(450LL, eo_1)));
  LINE(14,(assignCallTemp(eo_1, x_ip2long("0.0.0.0")),f_necho(460LL, eo_1)));
  LINE(17,(assignCallTemp(eo_1, x_long2ip(toInt32(2130706433LL))),f_necho(470LL, eo_1)));
  LINE(18,(assignCallTemp(eo_1, x_long2ip(toInt32(-1062726616LL))),f_necho(480LL, eo_1)));
  LINE(19,(assignCallTemp(eo_1, x_long2ip(toInt32(-956301310LL))),f_necho(490LL, eo_1)));
  LINE(20,(assignCallTemp(eo_1, x_long2ip(toInt32(-1LL))),f_necho(500LL, eo_1)));
  LINE(21,(assignCallTemp(eo_1, x_long2ip(toInt32(0LL))),f_necho(510LL, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
