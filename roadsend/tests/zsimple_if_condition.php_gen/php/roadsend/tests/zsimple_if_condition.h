
#ifndef __GENERATED_php_roadsend_tests_zsimple_if_condition_h__
#define __GENERATED_php_roadsend_tests_zsimple_if_condition_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zsimple_if_condition.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$zsimple_if_condition_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zsimple_if_condition_h__
