
#include <php/roadsend/tests/mp3-manager.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/mp3-manager.php line 4 */
Variant c_songlist::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_songlist::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_songlist::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("view", m_view.isReferenced() ? ref(m_view) : m_view));
  props.push_back(NEW(ArrayElement)("model", m_model.isReferenced() ? ref(m_model) : m_model));
  c_ObjectData::o_get(props);
}
bool c_songlist::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x05661D6428543CDELL, view, 4);
      break;
    case 3:
      HASH_EXISTS_STRING(0x330D2777D3094C67LL, model, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_songlist::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x05661D6428543CDELL, m_view,
                         view, 4);
      break;
    case 3:
      HASH_RETURN_STRING(0x330D2777D3094C67LL, m_model,
                         model, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_songlist::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x05661D6428543CDELL, m_view,
                      view, 4);
      break;
    case 3:
      HASH_SET_STRING(0x330D2777D3094C67LL, m_model,
                      model, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_songlist::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x05661D6428543CDELL, m_view,
                         view, 4);
      break;
    case 3:
      HASH_RETURN_STRING(0x330D2777D3094C67LL, m_model,
                         model, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_songlist::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(songlist)
ObjectData *c_songlist::create(Variant v_clist) {
  init();
  t_songlist(ref(v_clist));
  return this;
}
ObjectData *c_songlist::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(ref(const_cast<Array&>(params).lvalAt(0))));
  } else return this;
}
void c_songlist::dynConstruct(CArrRef params) {
  (t_songlist(ref(const_cast<Array&>(params).lvalAt(0))));
}
ObjectData *c_songlist::cloneImpl() {
  c_songlist *obj = NEW(c_songlist)();
  cloneSet(obj);
  return obj;
}
void c_songlist::cloneSet(c_songlist *clone) {
  clone->m_view = m_view.isReferenced() ? ref(m_view) : m_view;
  clone->m_model = m_model.isReferenced() ? ref(m_model) : m_model;
  ObjectData::cloneSet(clone);
}
Variant c_songlist::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x0A9AD0A104ECA151LL, addsong) {
        return (t_addsong(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x4206E8F2319C2582LL, songlist) {
        return (t_songlist(ref(const_cast<Array&>(params).lvalAt(0))), null);
      }
      break;
    case 4:
      HASH_GUARD(0x04F8F7CEA46D0514LL, save) {
        return (t_save(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x176E20ED27F91146LL, deleteselected) {
        return (t_deleteselected(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_songlist::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x0A9AD0A104ECA151LL, addsong) {
        return (t_addsong(a0, a1, a2), null);
      }
      break;
    case 2:
      HASH_GUARD(0x4206E8F2319C2582LL, songlist) {
        return (t_songlist(ref(a0)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x04F8F7CEA46D0514LL, save) {
        return (t_save(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x176E20ED27F91146LL, deleteselected) {
        return (t_deleteselected(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_songlist::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_songlist$os_get(const char *s) {
  return c_songlist::os_get(s, -1);
}
Variant &cw_songlist$os_lval(const char *s) {
  return c_songlist::os_lval(s, -1);
}
Variant cw_songlist$os_constant(const char *s) {
  return c_songlist::os_constant(s);
}
Variant cw_songlist$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_songlist::os_invoke(c, s, params, -1, fatal);
}
void c_songlist::init() {
  m_view = null;
  m_model = ScalarArrays::sa_[0];
}
/* SRC: roadsend/tests/mp3-manager.php line 8 */
void c_songlist::t_songlist(Variant v_clist) {
  INSTANCE_METHOD_INJECTION(SongList, SongList::SongList);
  bool oldInCtor = gasInCtor(true);
  (m_view = ref(v_clist));
  LINE(10,t_load());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 13 */
void c_songlist::t_addsong(CVarRef v_title, CVarRef v_artist, CVarRef v_filename) {
  INSTANCE_METHOD_INJECTION(SongList, SongList::addSong);
  m_model.append((Array(ArrayInit(3).set(0, "title", v_title, 0x66AD900A2301E2FELL).set(1, "artist", v_artist, 0x30B66F3F6FEFBB27LL).set(2, "filename", v_filename, 0x4A5D0431E1A3CAEDLL).create())));
  LINE(17,t_redisplay());
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 20 */
void c_songlist::t_redisplay() {
  INSTANCE_METHOD_INJECTION(SongList, SongList::redisplay);
  Variant eo_0;
  Variant eo_1;
  int64 v_i = 0;
  Variant v_clTransparentTest;
  Variant v_song;

  LINE(21,m_view.o_invoke_few_args("clear", 0x31DA235C5A226667LL, 0));
  (v_i = 0LL);
  (v_clTransparentTest = LINE(23,create_object("gdkcolor", Array(ArrayInit(3).set(0, 0LL).set(1, 0LL).set(2, 0LL).create()))));
  LINE(24,throw_fatal("unknown class gdk", ((void*)NULL)));
  {
    LOOP_COUNTER(1);
    Variant map2 = m_model;
    for (ArrayIterPtr iter3 = map2.begin("songlist"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_song = iter3->second();
      {
        LINE(27,m_view.o_invoke_few_args("append", 0x4DEE4A472DC69EC2LL, 1, Array(ArrayInit(2).set(0, v_song.rvalAt("artist", 0x30B66F3F6FEFBB27LL)).set(1, v_song.rvalAt("title", 0x66AD900A2301E2FELL)).create())));
        (assignCallTemp(eo_0, v_i++),assignCallTemp(eo_1, ref(v_clTransparentTest)),LINE(28,m_view.o_invoke("set_foreground", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x36F18ED13681F90DLL)));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 32 */
void c_songlist::t_deleteselected() {
  INSTANCE_METHOD_INJECTION(SongList, SongList::deleteSelected);
  Array v_newModel;
  Primitive v_index = 0;
  Variant v_song;

  if (isset(m_view.o_get("selection", 0x2781B225767237F1LL), 0LL, 0x77CFA1EEF01BCA90LL)) {
    (v_newModel = ScalarArrays::sa_[0]);
    {
      LOOP_COUNTER(4);
      Variant map5 = m_model;
      for (ArrayIterPtr iter6 = map5.begin("songlist"); !iter6->end(); iter6->next()) {
        LOOP_COUNTER_CHECK(4);
        v_song = iter6->second();
        v_index = iter6->first();
        {
          if (!equal(v_index, m_view.o_get("selection", 0x2781B225767237F1LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) {
            v_newModel.append((v_song));
          }
        }
      }
    }
    (m_model = v_newModel);
    LINE(41,t_redisplay());
  }
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 45 */
void c_songlist::t_save() {
  INSTANCE_METHOD_INJECTION(SongList, SongList::save);
  Variant eo_0;
  Variant eo_1;
  Variant v_fp;

  (v_fp = LINE(46,x_fopen("mp3-manager-songs.dat", "w")));
  if (!(toBoolean(v_fp))) {
    LINE(48,f_alert("Error saving songs!"));
  }
  else {
    LINE(50,(assignCallTemp(eo_0, toObject(v_fp)),assignCallTemp(eo_1, x_serialize(m_model)),x_fwrite(eo_0, eo_1)));
    LINE(51,x_fclose(toObject(v_fp)));
  }
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 55 */
void c_songlist::t_load() {
  INSTANCE_METHOD_INJECTION(SongList, SongList::load);
  if (LINE(56,x_file_exists("mp3-manager-songs.dat"))) {
    (m_model = LINE(57,x_unserialize(toString(x_file_get_contents("mp3-manager-songs.dat")))));
  }
  LINE(59,t_redisplay());
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 63 */
Variant c_filedialog::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_filedialog::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_filedialog::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("fs", m_fs.isReferenced() ? ref(m_fs) : m_fs));
  props.push_back(NEW(ArrayElement)("field", m_field.isReferenced() ? ref(m_field) : m_field));
  c_ObjectData::o_get(props);
}
bool c_filedialog::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x779EB2E3F63E9F79LL, field, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x18C3CE9B4F235CE6LL, fs, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_filedialog::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x779EB2E3F63E9F79LL, m_field,
                         field, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x18C3CE9B4F235CE6LL, m_fs,
                         fs, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_filedialog::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x779EB2E3F63E9F79LL, m_field,
                      field, 5);
      break;
    case 2:
      HASH_SET_STRING(0x18C3CE9B4F235CE6LL, m_fs,
                      fs, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_filedialog::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x779EB2E3F63E9F79LL, m_field,
                         field, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x18C3CE9B4F235CE6LL, m_fs,
                         fs, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_filedialog::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(filedialog)
ObjectData *c_filedialog::create(Variant v_filenameField) {
  init();
  t_filedialog(ref(v_filenameField));
  return this;
}
ObjectData *c_filedialog::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(ref(const_cast<Array&>(params).lvalAt(0))));
  } else return this;
}
ObjectData *c_filedialog::cloneImpl() {
  c_filedialog *obj = NEW(c_filedialog)();
  cloneSet(obj);
  return obj;
}
void c_filedialog::cloneSet(c_filedialog *clone) {
  clone->m_fs = m_fs.isReferenced() ? ref(m_fs) : m_fs;
  clone->m_field = m_field.isReferenced() ? ref(m_field) : m_field;
  ObjectData::cloneSet(clone);
}
Variant c_filedialog::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x724D4477D330DEDELL, ok) {
        return (t_ok(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_filedialog::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x35121557DC0EAE15LL, show) {
        return (t_show(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x724D4477D330DEDELL, ok) {
        return (t_ok(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_filedialog::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_filedialog$os_get(const char *s) {
  return c_filedialog::os_get(s, -1);
}
Variant &cw_filedialog$os_lval(const char *s) {
  return c_filedialog::os_lval(s, -1);
}
Variant cw_filedialog$os_constant(const char *s) {
  return c_filedialog::os_constant(s);
}
Variant cw_filedialog$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_filedialog::os_invoke(c, s, params, -1, fatal);
}
void c_filedialog::init() {
  m_fs = null;
  m_field = null;
}
/* SRC: roadsend/tests/mp3-manager.php line 67 */
void c_filedialog::t_filedialog(Variant v_filenameField) {
  INSTANCE_METHOD_INJECTION(FileDialog, FileDialog::FileDialog);
  bool oldInCtor = gasInCtor(true);
  (m_field = ref(v_filenameField));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 71 */
void c_filedialog::t_show() {
  INSTANCE_METHOD_INJECTION(FileDialog, FileDialog::show);
  Variant v_fs;

  (v_fs = LINE(72,create_object("gtkfileselection", Array(ArrayInit(1).set(0, "Select MP3 File").create()))));
  LINE(73,v_fs.o_invoke_few_args("set_modal", 0x602D6639F3C5616ELL, 1, true));
  LINE(74,v_fs.o_get("ok_button", 0x67BDCAE85F751474LL).o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "ok").create())));
  LINE(75,v_fs.o_get("ok_button", 0x67BDCAE85F751474LL).o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_fs).set(1, "destroy").create())));
  LINE(76,v_fs.o_get("cancel_button", 0x289F7B2EC5848D75LL).o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_fs).set(1, "destroy").create())));
  LINE(77,v_fs.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  (m_fs = ref(v_fs));
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 81 */
void c_filedialog::t_ok() {
  INSTANCE_METHOD_INJECTION(FileDialog, FileDialog::ok);
  LINE(82,m_field.o_invoke_few_args("set_text", 0x49E46C5B47E6D224LL, 1, m_fs.o_invoke_few_args("get_filename", 0x1406C1686B9ED075LL, 0)));
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 135 */
void f_addsong(CVarRef v_widget, Variant v_form, Variant v_window) {
  FUNCTION_INJECTION(addSong);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_songList __attribute__((__unused__)) = g->GV(songList);
  Variant v_title;
  Variant v_artist;
  Variant v_filename;

  (v_title = LINE(138,v_form.rvalAt("title", 0x66AD900A2301E2FELL).o_invoke_few_args("get_text", 0x2B657E6110B24C09LL, 0)));
  (v_artist = LINE(139,v_form.rvalAt("artist", 0x30B66F3F6FEFBB27LL).o_invoke_few_args("get_text", 0x2B657E6110B24C09LL, 0)));
  (v_filename = LINE(140,v_form.rvalAt("filename", 0x4A5D0431E1A3CAEDLL).o_invoke_few_args("get_text", 0x2B657E6110B24C09LL, 0)));
  if (empty(v_filename)) {
    LINE(143,f_alert("Please enter a filename."));
  }
  else {
    LINE(145,gv_songList.o_invoke_few_args("addSong", 0x0A9AD0A104ECA151LL, 3, v_title, v_artist, v_filename));
    LINE(146,toObject(v_window)->o_invoke_few_args("destroy", 0x45CCFF10463AB662LL, 0));
  }
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 111 */
void f_handlenewsong(CVarRef v_widget) {
  FUNCTION_INJECTION(handleNewSong);
  Variant eo_0;
  Object v_gladexml;
  Variant v_window;
  Variant v_cancelButton;
  Variant v_title;
  Variant v_artist;
  Variant v_filename;
  p_filedialog v_fileSelect;
  Variant v_browseButton;
  Variant v_okButton;

  (v_gladexml = LINE(112,create_object("gladexml", Array(ArrayInit(1).set(0, "song-details.glade").create()))));
  (v_window = ref(LINE(114,v_gladexml->o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "details"))));
  (v_cancelButton = ref(LINE(115,v_gladexml->o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "cancel"))));
  LINE(116,v_cancelButton.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).setRef(0, ref(v_window)).set(1, "destroy").create())));
  (v_title = ref(LINE(118,v_gladexml->o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "title"))));
  (v_artist = ref(LINE(119,v_gladexml->o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "artist"))));
  (v_filename = ref(LINE(120,v_gladexml->o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "filename"))));
  ((Object)((v_fileSelect = LINE(122,p_filedialog(p_filedialog(NEWOBJ(c_filedialog)())->create(ref(v_filename)))))));
  (v_browseButton = ref(LINE(123,v_gladexml->o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "browse"))));
  LINE(124,v_browseButton.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, ((Object)(v_fileSelect))).set(1, "show").create())));
  (v_okButton = ref(LINE(127,v_gladexml->o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "ok"))));
  (assignCallTemp(eo_0, toObject(v_okButton)),LINE(131,eo_0.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 4, "clicked", "addSong", Array(ArrayInit(3).setRef(0, "title", ref(v_title), 0x66AD900A2301E2FELL).setRef(1, "artist", ref(v_artist), 0x30B66F3F6FEFBB27LL).setRef(2, "filename", ref(v_filename), 0x4A5D0431E1A3CAEDLL).create()), ref(v_window))));
} /* function */
/* SRC: roadsend/tests/mp3-manager.php line 151 */
void f_alert(CStrRef v_message) {
  FUNCTION_INJECTION(alert);
  Object v_dialog;
  Variant v_label;
  Variant v_okButton;

  (v_dialog = LINE(152,create_object("gtkdialog", Array())));
  LINE(153,v_dialog->o_invoke_few_args("set_modal", 0x602D6639F3C5616ELL, 1, true));
  LINE(154,v_dialog->o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
  (v_label = LINE(155,create_object("gtklabel", Array(ArrayInit(1).set(0, v_message).create()))));
  (v_okButton = LINE(156,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Okay").create()))));
  LINE(157,v_okButton.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_dialog).set(1, "destroy").create())));
  LINE(158,v_dialog.o_get("action_area", 0x37BC1B12C5D8C1FDLL).o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_okButton));
  LINE(159,v_dialog.o_get("vbox", 0x7DA40F8F1BC6EDBFLL).o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_label));
  LINE(160,v_dialog->o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
} /* function */
Variant i_addsong(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0A9AD0A104ECA151LL, addsong) {
    return (f_addsong(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1)), ref(const_cast<Array&>(params).lvalAt(2))), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_handlenewsong(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6780F3C05406C407LL, handlenewsong) {
    return (f_handlenewsong(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Object co_songlist(CArrRef params, bool init /* = true */) {
  return Object(p_songlist(NEW(c_songlist)())->dynCreate(params, init));
}
Object co_filedialog(CArrRef params, bool init /* = true */) {
  return Object(p_filedialog(NEW(c_filedialog)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$mp3_manager_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/mp3-manager.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$mp3_manager_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_gladexml __attribute__((__unused__)) = (variables != gVariables) ? variables->get("gladexml") : g->GV(gladexml);
  Variant &v_window __attribute__((__unused__)) = (variables != gVariables) ? variables->get("window") : g->GV(window);
  Variant &v_newSong __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newSong") : g->GV(newSong);
  Variant &v_songList __attribute__((__unused__)) = (variables != gVariables) ? variables->get("songList") : g->GV(songList);
  Variant &v_deleteSong __attribute__((__unused__)) = (variables != gVariables) ? variables->get("deleteSong") : g->GV(deleteSong);
  Variant &v_quitButton __attribute__((__unused__)) = (variables != gVariables) ? variables->get("quitButton") : g->GV(quitButton);

  if (!(LINE(87,x_extension_loaded("gtk")))) {
    LINE(88,x_dl("php_gtk.so"));
  }
  (v_gladexml = LINE(91,create_object("gladexml", Array(ArrayInit(1).set(0, "mp3-manager.glade").create()))));
  (v_window = ref(LINE(92,v_gladexml.o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "window"))));
  (v_newSong = ref(LINE(94,v_gladexml.o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "newSong"))));
  LINE(95,v_newSong.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "handleNewSong"));
  (v_songList = LINE(97,p_songlist(p_songlist(NEWOBJ(c_songlist)())->create(ref(v_gladexml.o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "songList"))))));
  (v_deleteSong = ref(LINE(99,v_gladexml.o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "deleteSong"))));
  LINE(100,v_deleteSong.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_songList).set(1, "deleteSelected").create())));
  (v_quitButton = ref(LINE(102,v_gladexml.o_invoke_few_args("get_widget", 0x36B3455506A5B656LL, 1, "quit"))));
  LINE(103,v_quitButton.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_window).set(1, "destroy").create())));
  LINE(105,v_window.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "destroy", Array(ArrayInit(2).set(0, "gtk").set(1, "main_quit").create())));
  LINE(106,v_window.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "destroy", Array(ArrayInit(2).set(0, v_songList).set(1, "save").create())));
  LINE(107,v_window.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "delete-event", Array(ArrayInit(2).set(0, "gtk").set(1, "false").create())));
  LINE(108,throw_fatal("unknown class gtk", ((void*)NULL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
