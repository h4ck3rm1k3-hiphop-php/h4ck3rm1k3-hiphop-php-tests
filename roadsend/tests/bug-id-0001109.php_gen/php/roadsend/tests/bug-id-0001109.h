
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001109_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001109_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001109.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001109_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_test(CVarRef v_val, CVarRef v_key);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001109_h__
