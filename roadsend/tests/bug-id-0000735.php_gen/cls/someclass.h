
#ifndef __GENERATED_cls_someclass_h__
#define __GENERATED_cls_someclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0000735.php line 5 */
class c_someclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(someclass)
  END_CLASS_MAP(someclass)
  DECLARE_CLASS(someclass, someClass, ObjectData)
  void init();
  public: int64 m_zot;
  public: void t_someclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: static Variant ti_test(const char* cls);
  public: static Variant t_test() { return ti_test("someclass"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_someclass_h__
