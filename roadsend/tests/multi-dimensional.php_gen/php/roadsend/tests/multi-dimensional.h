
#ifndef __GENERATED_php_roadsend_tests_multi_dimensional_h__
#define __GENERATED_php_roadsend_tests_multi_dimensional_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/multi-dimensional.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_test_global_x();
Variant pm_php$roadsend$tests$multi_dimensional_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_multi_dimensional_h__
