
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001709_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001709_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001709.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001709_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_test_foreach();
void f_test_while();
void f_test_do();
void f_test_for();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001709_h__
