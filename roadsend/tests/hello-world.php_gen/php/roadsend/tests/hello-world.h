
#ifndef __GENERATED_php_roadsend_tests_hello_world_h__
#define __GENERATED_php_roadsend_tests_hello_world_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/hello-world.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$hello_world_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_delete_event(CVarRef v_widget);
void f_hello(CVarRef v_widget);
void f_shutdown(CVarRef v_widget);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_hello_world_h__
