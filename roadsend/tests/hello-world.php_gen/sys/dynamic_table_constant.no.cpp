
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_GTK_WINDOW_POPUP;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x4BE37E80ED50593DLL, k_GTK_WINDOW_POPUP, GTK_WINDOW_POPUP);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
