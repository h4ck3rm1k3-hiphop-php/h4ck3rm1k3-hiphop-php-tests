
#ifndef __GENERATED_php_roadsend_tests_zclasses_test_h__
#define __GENERATED_php_roadsend_tests_zclasses_test_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zclasses-test.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_class_parentage(String v_obj, String v_class);
void f_print_vars(CVarRef v_obj);
void f_print_methods(CVarRef v_obj);
Variant pm_php$roadsend$tests$zclasses_test_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zclasses_test_h__
