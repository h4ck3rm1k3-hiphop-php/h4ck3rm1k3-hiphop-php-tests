
#include <php/roadsend/tests/zclasses-test.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: roadsend/tests/zclasses-test.php line 24 */
void f_class_parentage(String v_obj, String v_class) {
  FUNCTION_INJECTION(class_parentage);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);

  class VariableTable : public RVariableTable {
  public:
    String &v_obj; String &v_class;
    VariableTable(String &r_obj, String &r_class) : v_obj(r_obj), v_class(r_class) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 3) {
        case 2:
          HASH_RETURN(0x45397FE5C82DBD12LL, v_class,
                      class);
          break;
        case 3:
          HASH_RETURN(0x7FB577570F61BD03LL, v_obj,
                      obj);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_obj, v_class);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  throw_fatal("dynamic global");
  if (LINE(26,x_is_subclass_of(variables->get(v_obj), v_class))) {
    echo(LINE(27,(assignCallTemp(eo_1, v_obj),assignCallTemp(eo_3, toString(x_get_class(variables->get(v_obj)))),concat4("Object ", eo_1, " belongs to class ", eo_3))));
    echo(LINE(28,concat3(" a subclass of ", v_class, "\n")));
  }
  else {
    echo(LINE(30,concat5("Object ", v_obj, " does not belong to a subclass of ", v_class, "\n")));
  }
} /* function */
/* SRC: roadsend/tests/zclasses-test.php line 8 */
void f_print_vars(CVarRef v_obj) {
  FUNCTION_INJECTION(print_vars);
  Variant v_arr;
  Variant v_prop;
  Variant v_val;

  (v_arr = LINE(9,x_get_object_vars(toObject(v_obj))));
  print("print_r: \n");
  LINE(11,x_print_r(v_arr));
  print("while: \n");
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(13,x_each(ref(v_arr))), v_prop, v_val))) {
      LOOP_COUNTER_CHECK(1);
      echo(LINE(14,concat5("\t", toString(v_prop), " = ", toString(v_val), "\n")));
    }
  }
} /* function */
/* SRC: roadsend/tests/zclasses-test.php line 17 */
void f_print_methods(CVarRef v_obj) {
  FUNCTION_INJECTION(print_methods);
  Variant v_arr;
  Variant v_method;

  (v_arr = LINE(18,x_get_class_methods(x_get_class(v_obj))));
  LINE(19,x_sort(ref(v_arr)));
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_arr.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_method = iter4->second();
      echo(LINE(21,concat3("\tfunction ", toString(v_method), "()\n")));
    }
  }
} /* function */
Variant pm_php$roadsend$tests$zclasses_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zclasses-test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zclasses_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_veggie __attribute__((__unused__)) = (variables != gVariables) ? variables->get("veggie") : g->GV(veggie);
  Variant &v_leafy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("leafy") : g->GV(leafy);

  echo("<pre>\n");
  LINE(4,include("zclasses.inc", false, variables, "roadsend/tests/"));
  (v_veggie = LINE(36,create_object("vegetable", Array(ArrayInit(2).set(0, true).set(1, "blue").create()))));
  (v_leafy = LINE(37,create_object("spinach", Array())));
  echo(LINE(40,(assignCallTemp(eo_1, toString(x_get_class(v_veggie))),concat3("veggie: CLASS ", eo_1, "\n"))));
  echo(concat("leafy: CLASS ", toString(LINE(41,x_get_class(v_leafy)))));
  echo(LINE(42,(assignCallTemp(eo_1, toString(x_get_parent_class(v_leafy))),concat3(", PARENT ", eo_1, "\n"))));
  echo("\nveggie: Properties\n");
  LINE(46,f_print_vars(v_veggie));
  echo("\nleafy: Methods\n");
  LINE(50,f_print_methods(v_leafy));
  echo("\nParentage:\n");
  LINE(53,f_class_parentage("leafy", "Spinach"));
  LINE(54,f_class_parentage("leafy", "Vegetable"));
  LINE(57,throw_fatal("unknown class vegetable", ((void*)NULL)));
  print(concat("class vars for Vegetable: ", toString(LINE(60,x_get_class_vars(toString(x_get_class(v_veggie)))))));
  echo("</pre>\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
