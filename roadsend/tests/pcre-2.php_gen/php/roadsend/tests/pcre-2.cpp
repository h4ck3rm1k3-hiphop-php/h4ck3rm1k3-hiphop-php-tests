
#include <php/roadsend/tests/pcre-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$pcre_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/pcre-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$pcre_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("num") : g->GV(num);
  Variant &v_html __attribute__((__unused__)) = (variables != gVariables) ? variables->get("html") : g->GV(html);
  Variant &v_matches __attribute__((__unused__)) = (variables != gVariables) ? variables->get("matches") : g->GV(matches);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_phones __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phones") : g->GV(phones);

  (v_s = "laz/y brown cow jumps! over c0w the sly fox coww brown");
  (v_num = LINE(6,x_preg_match_all("!(c[0o]w{1,2})\\s(\\w+)!", toString(v_s), ref(v_m))));
  echo(toString(LINE(8,x_preg_match_all("//", toString(v_s), ref(v_m)))));
  echo(LINE(10,concat5("from ", toString(v_s), " there are ", toString(v_num), " matches:\n")));
  LINE(11,x_print_r(v_m));
  print("set order\n");
  (v_num = LINE(14,x_preg_match_all("!(c[0o]w{1,2})\\s(\\w+)!", toString(v_s), ref(v_m), toInt32(2LL) /* PREG_SET_ORDER */)));
  print(toString(v_num) + toString(":\n"));
  LINE(16,x_print_r(v_m));
  echo("pattern order + offset capture\n");
  (v_num = LINE(19,x_preg_match_all("!(c[0o]w{1,2})\\s(\\w+)!", toString(v_s), ref(v_m), toInt32(257LL))));
  print(toString(v_num) + toString(":\n"));
  LINE(21,x_print_r(v_m));
  echo("set order + offset capture\n");
  (v_num = LINE(24,x_preg_match_all("!(c[0o]w{1,2})\\s(\\w+)!", toString(v_s), ref(v_m), toInt32(258LL))));
  print(toString(v_num) + toString(":\n"));
  LINE(26,x_print_r(v_m));
  echo("pattern order + offset capture (again)\n");
  LINE(29,x_preg_match_all("/cow/", "cow cow cow monkey cow", ref(v_m), toInt32(257LL)));
  LINE(30,x_print_r(v_m));
  (v_html = "<b>bold text</b><a href=howdy.html>click me</a>");
  LINE(35,x_preg_match_all("/(<([\\w]+)[^>]*>)(.*)(<\\/\\2>)/", toString(v_html), ref(v_matches)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(37,x_count(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(38,concat3("matched: ", toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(v_i)), "\n")));
        echo(LINE(39,concat3("part 1: ", toString(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(v_i)), "\n")));
        echo(LINE(40,concat3("part 2: ", toString(v_matches.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL).rvalAt(v_i)), "\n")));
        echo(LINE(41,concat3("part 3: ", toString(v_matches.rvalAt(4LL, 0x6F2A25235E544A31LL).rvalAt(v_i)), "\n\n")));
      }
    }
  }
  LINE(45,x_preg_match_all("/\\(\?  (\\d{3})\?  \\)\?  (\?(1)  [\\-\\s] ) \\d{3}-\\d{4}/x", "Call 555-1212 or 1-800-555-1212", ref(v_phones)));
  LINE(47,x_print_r(v_phones));
  LINE(50,x_preg_match_all("/(dd)/", "", ref(v_m)));
  LINE(51,x_print_r(v_m));
  LINE(53,x_preg_match("/(dd)(ss)/", "", ref(v_m)));
  LINE(54,x_print_r(v_m));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
