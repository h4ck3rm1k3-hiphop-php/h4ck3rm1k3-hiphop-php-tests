
#ifndef __GENERATED_php_roadsend_tests_xml_4_h__
#define __GENERATED_php_roadsend_tests_xml_4_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/xml-4.fw.h>

// Declarations
#include <cls/aminoacid.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

p_aminoacid f_parsemol(CVarRef v_mvalues);
Array f_readdatabase(CVarRef v_filename);
Variant pm_php$roadsend$tests$xml_4_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_aminoacid(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_xml_4_h__
