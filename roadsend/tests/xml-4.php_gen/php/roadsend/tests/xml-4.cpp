
#include <php/roadsend/tests/xml-4.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/xml-4.php line 3 */
Variant c_aminoacid::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aminoacid::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aminoacid::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name));
  props.push_back(NEW(ArrayElement)("symbol", m_symbol));
  props.push_back(NEW(ArrayElement)("code", m_code));
  props.push_back(NEW(ArrayElement)("type", m_type));
  c_ObjectData::o_get(props);
}
bool c_aminoacid::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_EXISTS_STRING(0x508FC7C8724A760ALL, type, 4);
      break;
    case 3:
      HASH_EXISTS_STRING(0x4A4181CA6D57B493LL, symbol, 6);
      break;
    case 4:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      HASH_EXISTS_STRING(0x5B2CD7DDAB7A1DECLL, code, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aminoacid::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x508FC7C8724A760ALL, m_type,
                         type, 4);
      break;
    case 3:
      HASH_RETURN_STRING(0x4A4181CA6D57B493LL, m_symbol,
                         symbol, 6);
      break;
    case 4:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      HASH_RETURN_STRING(0x5B2CD7DDAB7A1DECLL, m_code,
                         code, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aminoacid::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_SET_STRING(0x508FC7C8724A760ALL, m_type,
                      type, 4);
      break;
    case 3:
      HASH_SET_STRING(0x4A4181CA6D57B493LL, m_symbol,
                      symbol, 6);
      break;
    case 4:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      HASH_SET_STRING(0x5B2CD7DDAB7A1DECLL, m_code,
                      code, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aminoacid::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aminoacid::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aminoacid)
ObjectData *c_aminoacid::create(CVarRef v_aa) {
  init();
  t_aminoacid(v_aa);
  return this;
}
ObjectData *c_aminoacid::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_aminoacid::cloneImpl() {
  c_aminoacid *obj = NEW(c_aminoacid)();
  cloneSet(obj);
  return obj;
}
void c_aminoacid::cloneSet(c_aminoacid *clone) {
  clone->m_name = m_name;
  clone->m_symbol = m_symbol;
  clone->m_code = m_code;
  clone->m_type = m_type;
  ObjectData::cloneSet(clone);
}
Variant c_aminoacid::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aminoacid::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aminoacid::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aminoacid$os_get(const char *s) {
  return c_aminoacid::os_get(s, -1);
}
Variant &cw_aminoacid$os_lval(const char *s) {
  return c_aminoacid::os_lval(s, -1);
}
Variant cw_aminoacid$os_constant(const char *s) {
  return c_aminoacid::os_constant(s);
}
Variant cw_aminoacid$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aminoacid::os_invoke(c, s, params, -1, fatal);
}
void c_aminoacid::init() {
  m_name = null;
  m_symbol = null;
  m_code = null;
  m_type = null;
}
/* SRC: roadsend/tests/xml-4.php line 9 */
void c_aminoacid::t_aminoacid(CVarRef v_aa) {
  INSTANCE_METHOD_INJECTION(AminoAcid, AminoAcid::AminoAcid);
  bool oldInCtor = gasInCtor(true);
  Primitive v_k = 0;
  Variant v_v;

  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_aa.begin("aminoacid"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      (o_lval("k", 0x0D6857FDDD7F21CFLL) = v_aa.rvalAt(v_k));
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/xml-4.php line 42 */
p_aminoacid f_parsemol(CVarRef v_mvalues) {
  FUNCTION_INJECTION(parseMol);
  int64 v_i = 0;
  Sequence v_mol;

  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, LINE(43,x_count(v_mvalues))); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        v_mol.set(v_mvalues.rvalAt(v_i).rvalAt("tag", 0x087D95440560592ALL), (v_mvalues.rvalAt(v_i).rvalAt("value", 0x69E7413AE0C88471LL)));
      }
    }
  }
  return ((Object)(LINE(46,p_aminoacid(p_aminoacid(NEWOBJ(c_aminoacid)())->create(v_mol)))));
} /* function */
/* SRC: roadsend/tests/xml-4.php line 15 */
Array f_readdatabase(CVarRef v_filename) {
  FUNCTION_INJECTION(readDatabase);
  Variant eo_0;
  Variant eo_1;
  String v_data;
  Object v_parser;
  Variant v_values;
  Variant v_tags;
  Primitive v_key = 0;
  Variant v_val;
  Variant v_molranges;
  int64 v_i = 0;
  Numeric v_offset = 0;
  Numeric v_len = 0;
  Array v_tdb;

  (v_data = LINE(17,(assignCallTemp(eo_1, x_file(toString(v_filename))),x_implode("", eo_1))));
  (v_parser = LINE(18,x_xml_parser_create()));
  LINE(19,x_xml_parser_set_option(v_parser, toInt32(1LL) /* XML_OPTION_CASE_FOLDING */, 0LL));
  LINE(20,x_xml_parser_set_option(v_parser, toInt32(4LL) /* XML_OPTION_SKIP_WHITE */, 1LL));
  LINE(21,x_xml_parse_into_struct(v_parser, v_data, ref(v_values), ref(v_tags)));
  LINE(22,x_xml_parser_free(v_parser));
  {
    LOOP_COUNTER(5);
    for (ArrayIterPtr iter7 = v_tags.begin(); !iter7->end(); iter7->next()) {
      LOOP_COUNTER_CHECK(5);
      v_val = iter7->second();
      v_key = iter7->first();
      {
        if (equal(v_key, "molecule")) {
          (v_molranges = v_val);
          {
            LOOP_COUNTER(8);
            for ((v_i = 0LL); less(v_i, LINE(30,x_count(v_molranges))); v_i += 2LL) {
              LOOP_COUNTER_CHECK(8);
              {
                (v_offset = v_molranges.rvalAt(v_i) + 1LL);
                (v_len = v_molranges.rvalAt(v_i + 1LL) - v_offset);
                v_tdb.append((((Object)(LINE(33,f_parsemol(x_array_slice(v_values, toInt32(v_offset), v_len)))))));
              }
            }
          }
        }
        else {
          continue;
        }
      }
    }
  }
  return v_tdb;
} /* function */
Object co_aminoacid(CArrRef params, bool init /* = true */) {
  return Object(p_aminoacid(NEW(c_aminoacid)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$xml_4_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/xml-4.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$xml_4_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);

  (v_file = concat(toString(LINE(49,x_getenv("PCC_HOME"))), "/runtime/ext/xml/tests/moldb.xml"));
  (v_db = LINE(50,f_readdatabase(v_file)));
  echo("** Database of AminoAcid objects:\n");
  LINE(52,x_var_dump(1, v_db));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
