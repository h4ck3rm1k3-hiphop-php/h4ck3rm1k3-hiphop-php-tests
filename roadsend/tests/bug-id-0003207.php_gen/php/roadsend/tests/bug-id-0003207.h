
#ifndef __GENERATED_php_roadsend_tests_bug_id_0003207_h__
#define __GENERATED_php_roadsend_tests_bug_id_0003207_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0003207.fw.h>

// Declarations
#include <cls/wibble.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_flup();
Variant pm_php$roadsend$tests$bug_id_0003207_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_wibble(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0003207_h__
