
#include <php/roadsend/tests/bug-id-0003207.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0003207.php line 9 */
Variant c_wibble::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_wibble::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_wibble::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("moo", m_moo.isReferenced() ? ref(m_moo) : m_moo));
  c_ObjectData::o_get(props);
}
bool c_wibble::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x0EF8B0BE4B7436AELL, moo, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_wibble::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0EF8B0BE4B7436AELL, m_moo,
                         moo, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_wibble::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x0EF8B0BE4B7436AELL, m_moo,
                      moo, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_wibble::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0EF8B0BE4B7436AELL, m_moo,
                         moo, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_wibble::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(wibble)
ObjectData *c_wibble::cloneImpl() {
  c_wibble *obj = NEW(c_wibble)();
  cloneSet(obj);
  return obj;
}
void c_wibble::cloneSet(c_wibble *clone) {
  clone->m_moo = m_moo.isReferenced() ? ref(m_moo) : m_moo;
  ObjectData::cloneSet(clone);
}
Variant c_wibble::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_wibble::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_wibble::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_wibble$os_get(const char *s) {
  return c_wibble::os_get(s, -1);
}
Variant &cw_wibble$os_lval(const char *s) {
  return c_wibble::os_lval(s, -1);
}
Variant cw_wibble$os_constant(const char *s) {
  return c_wibble::os_constant(s);
}
Variant cw_wibble$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_wibble::os_invoke(c, s, params, -1, fatal);
}
void c_wibble::init() {
  m_moo = "cow";
}
/* SRC: roadsend/tests/bug-id-0003207.php line 3 */
String f_flup() {
  FUNCTION_INJECTION(flup);
  return "asdfbar";
} /* function */
Object co_wibble(CArrRef params, bool init /* = true */) {
  return Object(p_wibble(NEW(c_wibble)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0003207_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003207.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003207_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_wibbler __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wibbler") : g->GV(wibbler);
  Variant &v_mo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mo") : g->GV(mo);
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  (variables->get(LINE(5,f_flup())) = "foo");
  echo(toString(variables->get("asdfbar")));
  (v_wibbler = ((Object)(LINE(14,p_wibble(p_wibble(NEWOBJ(c_wibble)())->create())))));
  (v_mo = "mo");
  if (toObject(v_wibbler)->t___isset(concat(toString(v_mo), "o"))) {
    echo(toString(v_wibbler.o_get("moo", 0x0EF8B0BE4B7436AELL)));
  }
  echo("\n");
  {
    LOOP_COUNTER(1);
    Variant map2 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_arr.lvalAt("d", 0x7A452383AA9BF7C4LL) = iter3->second();
      v_arr.lvalAt("c", 0x32C769EE5C5509B0LL) = iter3->first();
      {
        echo(toString(v_arr.rvalAt("c", 0x32C769EE5C5509B0LL)));
        echo(toString(v_arr.rvalAt("d", 0x7A452383AA9BF7C4LL)));
      }
    }
  }
  echo("\n");
  {
    LOOP_COUNTER(4);
    Variant map5 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter6 = map5.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_wibbler.o_lval(concat(toString(v_mo), "b"), -1LL) = iter6->second();
      v_wibbler.o_lval(concat(toString(v_mo), "o"), -1LL) = iter6->first();
      {
        echo(toString(v_wibbler.o_get("moo", 0x0EF8B0BE4B7436AELL)));
        echo(toString(v_wibbler.o_get("mob", 0x4719B11296904F34LL)));
      }
    }
  }
  echo("\n");
  {
    LOOP_COUNTER(7);
    Variant map8 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter9 = map8.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_arr.lvalAt("d", 0x7A452383AA9BF7C4LL) = iter9->second();
      v_arr.lvalAt("c", 0x32C769EE5C5509B0LL) = iter9->first();
      {
        echo(toString(v_arr.rvalAt("c", 0x32C769EE5C5509B0LL)));
        echo(toString(v_arr.rvalAt("d", 0x7A452383AA9BF7C4LL)));
      }
    }
  }
  echo("\n");
  {
    LOOP_COUNTER(10);
    Variant map11 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter12 = map11.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_wibbler.o_lval(concat(toString(v_mo), "b"), -1LL) = iter12->second();
      v_wibbler.o_lval(concat(toString(v_mo), "o"), -1LL) = iter12->first();
      {
        echo(toString(v_wibbler.o_get("moo", 0x0EF8B0BE4B7436AELL)));
        echo(toString(v_wibbler.o_get("mob", 0x4719B11296904F34LL)));
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
