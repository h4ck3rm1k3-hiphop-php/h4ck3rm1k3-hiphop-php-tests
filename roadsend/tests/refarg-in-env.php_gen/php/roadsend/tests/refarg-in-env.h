
#ifndef __GENERATED_php_roadsend_tests_refarg_in_env_h__
#define __GENERATED_php_roadsend_tests_refarg_in_env_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/refarg-in-env.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_four(Variant v_zot, Variant v_foo);
void f_two(Variant v_bar, Variant v_zot);
void f_one(Variant v_foo, Variant v_bar);
Variant pm_php$roadsend$tests$refarg_in_env_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_three(Variant v_zot, Variant v_foo);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_refarg_in_env_h__
