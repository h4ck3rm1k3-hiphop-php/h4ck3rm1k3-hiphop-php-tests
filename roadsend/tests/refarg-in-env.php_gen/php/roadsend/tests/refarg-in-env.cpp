
#include <php/roadsend/tests/refarg-in-env.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/refarg-in-env.php line 63 */
void f_four(Variant v_zot, Variant v_foo) {
  FUNCTION_INJECTION(four);
  v_foo++;
} /* function */
/* SRC: roadsend/tests/refarg-in-env.php line 46 */
void f_two(Variant v_bar, Variant v_zot) {
  FUNCTION_INJECTION(two);
  (v_zot = 12LL);
  (v_bar = ref(v_zot));
  (v_zot = 13LL);
} /* function */
/* SRC: roadsend/tests/refarg-in-env.php line 35 */
void f_one(Variant v_foo, Variant v_bar) {
  FUNCTION_INJECTION(one);
  (v_foo = 9LL);
  (v_bar = 10LL);
  (v_bar = ref(v_foo));
  (v_bar = 11LL);
} /* function */
/* SRC: roadsend/tests/refarg-in-env.php line 55 */
void f_three(Variant v_zot, Variant v_foo) {
  FUNCTION_INJECTION(three);
  Variant v_bar;

  (v_zot = ref(v_foo));
  (v_foo = ref(v_foo));
  (v_bar = ref(v_foo));
  (v_zot = ref(v_bar));
  v_bar++;
} /* function */
Variant pm_php$roadsend$tests$refarg_in_env_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/refarg-in-env.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$refarg_in_env_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);
  Variant &v_bing __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bing") : g->GV(bing);
  Variant &v_broof __attribute__((__unused__)) = (variables != gVariables) ? variables->get("broof") : g->GV(broof);
  Variant &v_barp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("barp") : g->GV(barp);

  (v_foo = 9LL);
  (v_bar = 10LL);
  (v_bar = ref(v_foo));
  (v_bar = 11LL);
  echo(LINE(8,concat5("0bar: ", toString(v_bar), ", foo: ", toString(v_foo), "\n")));
  (v_zot = 12LL);
  (v_bar = ref(v_zot));
  (v_zot = 13LL);
  echo(concat("1foo: ", LINE(14,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  (v_zot = ref(v_foo));
  (v_bar = ref(v_foo));
  echo(concat("2foo: ", LINE(24,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  (v_bing = "bar");
  (v_broof = "foo");
  (v_barp = "zot");
  echo(concat("3foo: ", LINE(32,concat6(toString(variables->get(toString(v_broof))), ", bar: ", toString(variables->get(toString(v_bing))), ", zot: ", toString(variables->get(toString(v_barp))), "\n"))));
  LINE(42,f_one(ref(v_bar), v_foo));
  echo(LINE(43,concat5("4bar: ", toString(v_bar), ", foo: ", toString(v_foo), "\n")));
  LINE(52,f_two(v_bar, ref(v_zot)));
  echo(concat("5foo: ", LINE(53,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  LINE(71,f_three(v_zot, v_foo));
  echo(concat("6foo: ", LINE(72,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  v_bar++;
  echo(concat("7foo: ", LINE(75,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  v_zot--;
  echo(concat("8foo: ", LINE(78,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  v_foo -= 3LL;
  echo(concat("9foo: ", LINE(81,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  LINE(84,f_four(ref(v_foo), ref(v_zot)));
  echo(concat("10foo: ", LINE(85,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  v_foo += 9LL;
  echo(concat("11foo: ", LINE(89,concat6(toString(v_foo), ", bar: ", toString(v_bar), ", zot: ", toString(v_zot), "\n"))));
  echo(concat("12foo: ", LINE(92,concat6(toString(variables->get(toString(v_broof))), ", bar: ", toString(variables->get(toString(v_bing))), ", zot: ", toString(variables->get(toString(v_barp))), "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
