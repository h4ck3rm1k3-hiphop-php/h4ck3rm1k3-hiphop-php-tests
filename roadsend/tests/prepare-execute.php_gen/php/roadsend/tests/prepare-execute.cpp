
#include <php/roadsend/tests/prepare-execute.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$prepare_execute_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/prepare-execute.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$prepare_execute_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directdsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directdsn") : g->GV(directdsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_rv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rv") : g->GV(rv);
  Variant &v_rows __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rows") : g->GV(rows);
  Variant &v_rr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rr") : g->GV(rr);

  LINE(3,include("connect.inc", false, variables, "roadsend/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_directdsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_rh = LINE(13,invoke_failed("odbc_prepare", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "SELECT * FROM my_table").create()), 0x00000000C0BF956BLL)));
  if (!(toBoolean(v_rh))) {
    echo("odbc_prepare failed!\n");
    echo(toString(LINE(16,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    echo(toString(LINE(17,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
    f_exit(1LL);
  }
  echo(LINE(21,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  (v_rv = LINE(24,invoke_failed("odbc_execute", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x000000005560188ALL)));
  echo(LINE(26,(assignCallTemp(eo_1, toString(x_is_resource(v_rv))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_rv))) {
    echo("odbc_execute failed!\n");
    echo(toString(LINE(29,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    echo(toString(LINE(30,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
    f_exit(1LL);
  }
  (v_rows = LINE(34,invoke_failed("odbc_num_rows", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000D8DBF1DDLL)));
  echo(LINE(35,concat3("num rows: ", toString(v_rows), "\n")));
  LINE(36,x_var_dump(1, v_rows));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_rr = LINE(39,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(40,x_var_dump(1, v_rr));
      }
    }
  }
  (v_rh = LINE(45,invoke_failed("odbc_prepare", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "SELECT ATAN(\?,\?)").create()), 0x00000000C0BF956BLL)));
  if (!(toBoolean(v_rh))) {
    echo("odbc_prepare failed!\n");
    echo(toString(LINE(48,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    echo(toString(LINE(49,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
    f_exit(1LL);
  }
  echo(LINE(53,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  (v_rv = LINE(55,invoke_failed("odbc_execute", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, Array(ArrayInit(2).set(0, "-2").set(1, "2").create())).create()), 0x000000005560188ALL)));
  echo(LINE(57,(assignCallTemp(eo_1, toString(x_is_resource(v_rv))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_rv))) {
    echo("odbc_execute failed!\n");
    echo(toString(LINE(60,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    echo(toString(LINE(61,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
    f_exit(1LL);
  }
  (v_rows = LINE(65,invoke_failed("odbc_num_rows", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000D8DBF1DDLL)));
  echo(LINE(66,concat3("num rows: ", toString(v_rows), "\n")));
  LINE(67,x_var_dump(1, v_rows));
  LOOP_COUNTER(2);
  {
    while (toBoolean((v_rr = LINE(70,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
      LOOP_COUNTER_CHECK(2);
      {
        LINE(71,x_var_dump(1, v_rr));
      }
    }
  }
  echo(toString(LINE(76,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
