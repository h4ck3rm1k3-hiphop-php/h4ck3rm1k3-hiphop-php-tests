
#ifndef __GENERATED_php_roadsend_tests_prepare_execute_h__
#define __GENERATED_php_roadsend_tests_prepare_execute_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/prepare-execute.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$prepare_execute_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_prepare_execute_h__
