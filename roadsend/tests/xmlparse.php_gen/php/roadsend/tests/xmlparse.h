
#ifndef __GENERATED_php_roadsend_tests_xmlparse_h__
#define __GENERATED_php_roadsend_tests_xmlparse_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/xmlparse.fw.h>

// Declarations
#include <cls/xmlparser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_xmlfile(CVarRef v_num);
Variant pm_php$roadsend$tests$xmlparse_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_xmlparser(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_xmlparse_h__
