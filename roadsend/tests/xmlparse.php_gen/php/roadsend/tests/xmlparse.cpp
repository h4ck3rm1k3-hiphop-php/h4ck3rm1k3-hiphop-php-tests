
#include <php/roadsend/tests/xmlparse.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/xmlparse.php line 9 */
Variant c_xmlparser::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_xmlparser::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_xmlparser::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_xmlparser::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_xmlparser::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_xmlparser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_xmlparser::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_xmlparser::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(xmlparser)
ObjectData *c_xmlparser::cloneImpl() {
  c_xmlparser *obj = NEW(c_xmlparser)();
  cloneSet(obj);
  return obj;
}
void c_xmlparser::cloneSet(c_xmlparser *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_xmlparser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 2:
      HASH_GUARD(0x55BFFBE5717EFD62LL, startelement) {
        return (t_startelement(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x733EA7621D800E65LL, cdatahandler) {
        return (t_cdatahandler(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 11:
      HASH_GUARD(0x3CE830263383382BLL, externalentityrefhandler) {
        return (t_externalentityrefhandler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
      }
      HASH_GUARD(0x099B9E77BBE7B1CBLL, defaulthandler) {
        return (t_defaulthandler(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x404D473172C5DE8BLL, runtest) {
        return (t_runtest(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x70F25BC06033842CLL, entitydeclhandler) {
        return (t_entitydeclhandler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x78A7A5D96EF2F4AFLL, endelement) {
        return (t_endelement(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 23:
      HASH_GUARD(0x5F104B5787299D77LL, pihandler) {
        return (t_pihandler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 24:
      HASH_GUARD(0x015B7FAE233BDE58LL, unparsedentitydeclhandler) {
        return (t_unparsedentitydeclhandler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_xmlparser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 2:
      HASH_GUARD(0x55BFFBE5717EFD62LL, startelement) {
        return (t_startelement(a0, a1, a2), null);
      }
      break;
    case 5:
      HASH_GUARD(0x733EA7621D800E65LL, cdatahandler) {
        return (t_cdatahandler(a0, a1), null);
      }
      break;
    case 11:
      HASH_GUARD(0x3CE830263383382BLL, externalentityrefhandler) {
        return (t_externalentityrefhandler(a0, a1, a2, a3, a4), null);
      }
      HASH_GUARD(0x099B9E77BBE7B1CBLL, defaulthandler) {
        return (t_defaulthandler(a0, a1), null);
      }
      HASH_GUARD(0x404D473172C5DE8BLL, runtest) {
        return (t_runtest(a0, a1), null);
      }
      break;
    case 12:
      HASH_GUARD(0x70F25BC06033842CLL, entitydeclhandler) {
        return (t_entitydeclhandler(a0, a1, a2, a3, a4), null);
      }
      break;
    case 15:
      HASH_GUARD(0x78A7A5D96EF2F4AFLL, endelement) {
        return (t_endelement(a0, a1), null);
      }
      break;
    case 23:
      HASH_GUARD(0x5F104B5787299D77LL, pihandler) {
        return (t_pihandler(a0, a1, a2), null);
      }
      break;
    case 24:
      HASH_GUARD(0x015B7FAE233BDE58LL, unparsedentitydeclhandler) {
        return (t_unparsedentitydeclhandler(a0, a1, a2, a3, a4, a5), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_xmlparser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_xmlparser$os_get(const char *s) {
  return c_xmlparser::os_get(s, -1);
}
Variant &cw_xmlparser$os_lval(const char *s) {
  return c_xmlparser::os_lval(s, -1);
}
Variant cw_xmlparser$os_constant(const char *s) {
  return c_xmlparser::os_constant(s);
}
Variant cw_xmlparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_xmlparser::os_invoke(c, s, params, -1, fatal);
}
void c_xmlparser::init() {
}
/* SRC: roadsend/tests/xmlparse.php line 11 */
void c_xmlparser::t_readfile(CVarRef v_num) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::readfile);
  Variant v_fp;
  Variant v_data;

  (o_lval("currentFile", 0x6F3EB7E7AB86E056LL) = LINE(12,f_xmlfile(v_num)));
  if (!((toBoolean((v_fp = LINE(14,x_fopen(toString(o_get("currentFile", 0x6F3EB7E7AB86E056LL)), "r"))))))) {
    f_exit("could not open XML input");
  }
  (o_lval("contentBuffer", 0x7547C6E08A9DEF7BLL) = null);
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_data = LINE(19,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(o_lval("contentBuffer", 0x7547C6E08A9DEF7BLL), toString(v_data));
      }
    }
  }
  LINE(23,x_fclose(toObject(v_fp)));
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 26 */
void c_xmlparser::t_runtest(CVarRef v_num, CVarRef v_i) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::runtest);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Object v_xml_parser;
  String v_inFile;

  LINE(28,t_readfile(v_num));
  echo(LINE(29,concat5("parsing xml data file ", toString(o_get("currentFile", 0x6F3EB7E7AB86E056LL)), " iteration ", toString(v_i), "\n")));
  (v_xml_parser = LINE(31,x_xml_parser_create()));
  LINE(32,x_xml_parser_set_option(v_xml_parser, toInt32(1LL) /* XML_OPTION_CASE_FOLDING */, 1LL));
  LINE(34,x_xml_set_object(v_xml_parser, ref(this)));
  LINE(35,x_xml_set_element_handler(v_xml_parser, "startElement", "endElement"));
  LINE(36,x_xml_set_character_data_handler(v_xml_parser, "cDataHandler"));
  LINE(38,x_xml_set_external_entity_ref_handler(v_xml_parser, "externalEntityRefHandler"));
  LINE(39,x_xml_set_processing_instruction_handler(v_xml_parser, "piHandler"));
  LINE(40,x_xml_set_unparsed_entity_decl_handler(v_xml_parser, "unparsedEntityDeclHandler"));
  LINE(41,x_xml_set_notation_decl_handler(v_xml_parser, "entityDeclHandler"));
  LINE(42,x_xml_set_default_handler(v_xml_parser, "defaultHandler"));
  if (!(toBoolean(LINE(44,x_xml_parse(v_xml_parser, toString(o_get("contentBuffer", 0x7547C6E08A9DEF7BLL)), true))))) {
    (!equal(o_get("currentFile", 0x6F3EB7E7AB86E056LL), "")) ? (((v_inFile = toString("in file ") + toString(o_get("currentFile", 0x6F3EB7E7AB86E056LL))))) : (((v_inFile = "")));
    LINE(48,o_root_invoke_few_args("fatalErrorPage", 0x714C18A07966ACD5LL, 1, (assignCallTemp(eo_0, concat(toString(LINE(46,x_get_class(((Object)(this))))), toString(": XML error: %s at line %d ") + v_inFile)),assignCallTemp(eo_1, LINE(47,x_xml_error_string(x_xml_get_error_code(v_xml_parser)))),assignCallTemp(eo_2, LINE(48,x_xml_get_current_line_number(v_xml_parser))),x_sprintf(3, eo_0, Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create())))));
  }
  LINE(51,x_xml_parser_free(v_xml_parser));
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 56 */
void c_xmlparser::t_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::startElement);
  print(LINE(57,concat3("start: ", toString(v_name), "\n")));
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 60 */
void c_xmlparser::t_endelement(CVarRef v_parser, CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::endElement);
  print(LINE(61,concat3("end: ", toString(v_name), "\n")));
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 65 */
void c_xmlparser::t_cdatahandler(CVarRef v_parser, CVarRef v_data) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::cDataHandler);
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 70 */
void c_xmlparser::t_externalentityrefhandler(CVarRef v_parser, CVarRef v_openEntityNames, CVarRef v_base, CVarRef v_systemID, CVarRef v_publicID) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::externalEntityRefHandler);
  echo("externalentity\n");
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 75 */
void c_xmlparser::t_unparsedentitydeclhandler(CVarRef v_parser, CVarRef v_entityName, CVarRef v_base, CVarRef v_systemID, CVarRef v_publicID, CVarRef v_notationName) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::unparsedEntityDeclHandler);
  echo("unparsedentity\n");
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 80 */
void c_xmlparser::t_entitydeclhandler(CVarRef v_parser, CVarRef v_notationName, CVarRef v_base, CVarRef v_systemID, CVarRef v_publicID) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::entityDeclHandler);
  echo("entitydecl\n");
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 85 */
void c_xmlparser::t_pihandler(CVarRef v_parser, CVarRef v_target, CVarRef v_data) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::piHandler);
  echo("pihandler\n");
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 90 */
void c_xmlparser::t_defaulthandler(CVarRef v_parser, CVarRef v_data) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::defaultHandler);
} /* function */
/* SRC: roadsend/tests/xmlparse.php line 4 */
String f_xmlfile(CVarRef v_num) {
  FUNCTION_INJECTION(xmlfile);
  DECLARE_GLOBAL_VARIABLES(g);
  return concat(toString(g->gv__ENV.rvalAt("PCC_HOME", 0x4F7B58EF1F3DF563LL)), LINE(5,concat3("/benchmarks/data/xml-data-", toString(v_num), ".xml")));
} /* function */
Object co_xmlparser(CArrRef params, bool init /* = true */) {
  return Object(p_xmlparser(NEW(c_xmlparser)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$xmlparse_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/xmlparse.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$xmlparse_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_pObj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pObj") : g->GV(pObj);
  Variant &v_numTests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("numTests") : g->GV(numTests);
  Variant &v_numIterations __attribute__((__unused__)) = (variables != gVariables) ? variables->get("numIterations") : g->GV(numIterations);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_pObj = LINE(98,p_xmlparser(p_xmlparser(NEWOBJ(c_xmlparser)())->create())));
  (v_numTests = 2LL);
  (v_numIterations = 20LL);
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, v_numTests); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        {
          LOOP_COUNTER(3);
          for ((v_t = 0LL); less(v_t, v_numIterations); v_t++) {
            LOOP_COUNTER_CHECK(3);
            {
              LINE(105,v_pObj.o_invoke_few_args("runtest", 0x404D473172C5DE8BLL, 2, v_i, v_t));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
