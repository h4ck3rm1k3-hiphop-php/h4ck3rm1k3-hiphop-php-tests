
#ifndef __GENERATED_cls_xmlparser_h__
#define __GENERATED_cls_xmlparser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/xmlparse.php line 9 */
class c_xmlparser : virtual public ObjectData {
  BEGIN_CLASS_MAP(xmlparser)
  END_CLASS_MAP(xmlparser)
  DECLARE_CLASS(xmlparser, xmlparser, ObjectData)
  void init();
  public: void t_readfile(CVarRef v_num);
  public: void t_runtest(CVarRef v_num, CVarRef v_i);
  public: void t_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs);
  public: void t_endelement(CVarRef v_parser, CVarRef v_name);
  public: void t_cdatahandler(CVarRef v_parser, CVarRef v_data);
  public: void t_externalentityrefhandler(CVarRef v_parser, CVarRef v_openEntityNames, CVarRef v_base, CVarRef v_systemID, CVarRef v_publicID);
  public: void t_unparsedentitydeclhandler(CVarRef v_parser, CVarRef v_entityName, CVarRef v_base, CVarRef v_systemID, CVarRef v_publicID, CVarRef v_notationName);
  public: void t_entitydeclhandler(CVarRef v_parser, CVarRef v_notationName, CVarRef v_base, CVarRef v_systemID, CVarRef v_publicID);
  public: void t_pihandler(CVarRef v_parser, CVarRef v_target, CVarRef v_data);
  public: void t_defaulthandler(CVarRef v_parser, CVarRef v_data);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_xmlparser_h__
