
#include <php/roadsend/tests/funorder.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/funorder.php line 5 */
void f_foo(int64 v_a, int64 v_b) {
  FUNCTION_INJECTION(foo);
  echo(LINE(6,concat4(toString(v_a), " ", toString(v_b), "\n")));
} /* function */
Variant pm_php$roadsend$tests$funorder_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/funorder.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$funorder_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,f_foo(1LL, 2LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
