
#include <php/roadsend/tests/error-handler.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/error-handler.php line 3 */
void f_soretoes(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d) {
  FUNCTION_INJECTION(soretoes);
  print(concat_rev(LINE(4,concat6(toString(v_b), ", ", toString(v_c), ", ", toString(v_d), "\n")), concat3("arguments were \t", toString(v_a), ", ")));
} /* function */
Variant i_soretoes(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x497B3877F867455ELL, soretoes) {
    return (f_soretoes(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$error_handler_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/error-handler.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$error_handler_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(7,x_set_error_handler("soretoes"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
