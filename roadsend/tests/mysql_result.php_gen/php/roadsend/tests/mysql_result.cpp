
#include <php/roadsend/tests/mysql_result.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$mysql_result_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/mysql_result.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$mysql_result_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_dbs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dbs") : g->GV(dbs);
  Variant &v_num_dbs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("num_dbs") : g->GV(num_dbs);
  Variant &v_real_num_dbs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("real_num_dbs") : g->GV(real_num_dbs);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_db_name_tmp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_name_tmp") : g->GV(db_name_tmp);
  Variant &v_dblink __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dblink") : g->GV(dblink);
  Variant &v_dblist __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dblist") : g->GV(dblist);

  (toBoolean((v_link = LINE(3,x_mysql_connect("localhost", "develUser", "d3v3lpa55"))))) || (toBoolean(f_exit(concat("Could not connect: ", toString(x_mysql_error())))));
  (v_result = LINE(5,x_mysql_unbuffered_query("USE bork")));
  LINE(6,x_var_dump(1, v_result));
  print((LINE(8,(assignCallTemp(eo_1, toString(x_is_resource(v_result))),concat3("is resource: ", eo_1, "\n")))));
  LINE(9,x_var_dump(1, (silenceInc(), silenceDec(x_mysql_fetch_array(v_result)))));
  (toBoolean((v_result = LINE(11,x_mysql_unbuffered_query("SELECT User FROM mysql.user"))))) || (toBoolean(f_exit(concat("Could not query: ", toString(x_mysql_error())))));
  print((LINE(13,(assignCallTemp(eo_1, toString(x_is_resource(v_result))),concat3("is resource: ", eo_1, "\n")))));
  echo(concat("\nresult is: ", toString((silenceInc(), silenceDec(toString(LINE(14,x_mysql_result(v_result, toInt32(3LL)))))))));
  echo("BOTH\n");
  LINE(17,x_var_dump(1, x_mysql_fetch_array(v_result)));
  echo("ASSOC\n");
  LINE(19,x_var_dump(1, x_mysql_fetch_array(v_result, toInt32(1LL) /* MYSQL_ASSOC */)));
  echo("NUM\n");
  LINE(21,x_var_dump(1, x_mysql_fetch_array(v_result, toInt32(2LL) /* MYSQL_NUM */)));
  (toBoolean((v_result = (silenceInc(), silenceDec(LINE(23,x_mysql_query("SELECT * FROM mysql.user"))))))) || (toBoolean(f_exit(concat("Could not query: ", toString(x_mysql_error())))));
  echo(concat("\n1result is: ", toString(LINE(24,x_mysql_result(v_result, toInt32(4LL), "password")))));
  echo(concat("\n2result is: ", toString(LINE(25,x_mysql_result(v_result, toInt32(4LL), "user.password")))));
  (toBoolean((v_result = LINE(27,x_mysql_query("SELECT * FROM mysql.user"))))) || (toBoolean(f_exit(concat("Could not query: ", toString(x_mysql_error())))));
  echo(concat("\nresult is: ", toString(LINE(28,x_mysql_result(v_result, toInt32(4LL), 1LL)))));
  LINE(29,x_var_dump(1, x_mysql_fetch_field(v_result)));
  (toBoolean((v_result = LINE(31,x_mysql_query("SELECT * FROM mysql.user"))))) || (toBoolean(f_exit(concat("Could not query: ", toString(x_mysql_error())))));
  echo(concat("\nresult is: ", toString(LINE(32,invoke_failed("mysql_dbname", Array(ArrayInit(3).set(0, ref(v_result)).set(1, 6LL).set(2, 0LL).create()), 0x00000000507117F2LL)))));
  (v_dbs = LINE(35,x_mysql_list_dbs()));
  echo("\ndbs:\n");
  (v_num_dbs = toBoolean((toBoolean(v_dbs))) ? ((Variant)(LINE(37,x_mysql_num_rows(v_dbs)))) : ((Variant)(0LL)));
  LINE(38,x_var_dump(1, v_num_dbs));
  (v_real_num_dbs = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_num_dbs); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_db_name_tmp = LINE(41,invoke_failed("mysql_dbname", Array(ArrayInit(2).set(0, ref(v_dbs)).set(1, ref(v_i)).create()), 0x00000000507117F2LL)));
        LINE(42,x_var_dump(1, v_db_name_tmp));
        (v_dblink = LINE(43,x_mysql_select_db(toString(v_db_name_tmp))));
        if (toBoolean(v_dblink)) {
          v_dblist.append((v_db_name_tmp));
          v_real_num_dbs++;
        }
      }
    }
  }
  LINE(49,x_mysql_free_result(v_dbs));
  LINE(50,x_var_dump(1, v_dblist));
  LINE(52,x_mysql_select_db("test"));
  (v_result = LINE(53,x_mysql_query("UPDATE my_table SET field1 = 'test one' WHERE id=1")));
  LINE(54,x_var_dump(1, v_result));
  LINE(57,x_mysql_close(v_link));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
