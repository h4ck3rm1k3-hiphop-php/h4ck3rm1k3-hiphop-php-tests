
#ifndef __GENERATED_php_roadsend_tests_mysql_result_h__
#define __GENERATED_php_roadsend_tests_mysql_result_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/mysql_result.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$mysql_result_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_mysql_result_h__
