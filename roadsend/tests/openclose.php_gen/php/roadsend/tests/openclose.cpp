
#include <php/roadsend/tests/openclose.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$openclose_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/openclose.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$openclose_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);

  (toBoolean((v_link = LINE(3,x_mysql_connect("localhost", "develUser", "d3v3lpa55"))))) || (toBoolean(f_exit("Could not connect\n")));
  print("Connected successfully\n");
  echo(toString(LINE(7,x_mysql_get_server_info(v_link))));
  echo(toString(LINE(8,x_mysql_get_host_info(v_link))));
  echo(toString(LINE(9,x_mysql_get_proto_info(v_link))));
  LINE(14,x_mysql_close(v_link));
  LINE(16,x_mysql_close(v_link));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
