
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "complex", "roadsend/tests/julia-ppm.php",
  "complex_abs", "roadsend/tests/julia-ppm.php",
  "complex_add", "roadsend/tests/julia-ppm.php",
  "complex_multiply", "roadsend/tests/julia-ppm.php",
  "imag_part", "roadsend/tests/julia-ppm.php",
  "julia_ppm", "roadsend/tests/julia-ppm.php",
  "ppm_write", "roadsend/tests/julia-ppm.php",
  "real_part", "roadsend/tests/julia-ppm.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
