
#ifndef __GENERATED_php_roadsend_tests_julia_ppm_h__
#define __GENERATED_php_roadsend_tests_julia_ppm_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/julia-ppm.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_julia_ppm(CStrRef v_filename, int64 v_size, int64 v_depth, double v_zreal, double v_zimag);
Array f_complex_multiply(CArrRef v_complex1, CArrRef v_complex2);
Array f_complex_add(CArrRef v_complex1, CArrRef v_complex2);
Variant f_imag_part(CVarRef v_complex_number);
double f_complex_abs(CArrRef v_complex);
void f_ppm_write(CVarRef v_filehandle, CStrRef v_data);
Variant pm_php$roadsend$tests$julia_ppm_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_real_part(CVarRef v_complex_number);
Array f_complex(CVarRef v_real_part, CVarRef v_imag_part);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_julia_ppm_h__
