
#ifndef __GENERATED_php_roadsend_tests_lib1_h__
#define __GENERATED_php_roadsend_tests_lib1_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/lib1.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_mylibfunc(CVarRef v_a);
Variant pm_php$roadsend$tests$lib1_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_lib1_h__
