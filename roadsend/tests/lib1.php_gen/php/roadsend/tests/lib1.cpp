
#include <php/roadsend/tests/lib1.h>
#include <php/roadsend/tests/lib2.h>
#include <php/roadsend/tests/lib3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/lib1.php line 24 */
void f_mylibfunc(CVarRef v_a) {
  FUNCTION_INJECTION(myLibFunc);
  echo(LINE(26,concat3("in myLibFunc: ", toString(v_a), "\n")));
} /* function */
Variant pm_php$roadsend$tests$lib1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/lib1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$lib1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Library was loaded\n");
  LINE(21,pm_php$roadsend$tests$lib2_php(false, variables));
  LINE(22,pm_php$roadsend$tests$lib3_php(false, variables));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
