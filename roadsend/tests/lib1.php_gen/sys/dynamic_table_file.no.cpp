
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$tests$lib1_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$tests$lib2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$tests$lib3_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_INCLUDE(0x75C0DF67D6C5A782LL, "roadsend/tests/lib1.php", php$roadsend$tests$lib1_php);
      break;
    case 3:
      HASH_INCLUDE(0x7FC7D9CB7AC7DEBBLL, "roadsend/tests/lib3.php", php$roadsend$tests$lib3_php);
      break;
    case 7:
      HASH_INCLUDE(0x14A81B82BF80F6AFLL, "roadsend/tests/lib2.php", php$roadsend$tests$lib2_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
