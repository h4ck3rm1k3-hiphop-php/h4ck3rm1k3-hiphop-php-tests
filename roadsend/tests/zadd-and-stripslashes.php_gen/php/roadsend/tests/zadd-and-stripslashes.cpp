
#include <php/roadsend/tests/zadd-and-stripslashes.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$zadd_and_stripslashes_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zadd-and-stripslashes.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zadd_and_stripslashes_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_input = "");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 512LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_input, LINE(5,x_chr(toInt64(modulo(toInt64(v_i), 256LL)))));
      }
    }
  }
  (v_foo = "\\&quote; \\foo 4: \\\\ 2: \\ 1: ---- \\\" \\'");
  LINE(10,x_var_dump(1, v_foo));
  (v_bar = LINE(12,x_stripslashes(toString(v_foo))));
  LINE(13,x_var_dump(1, v_bar));
  LINE(20,x_ini_set("magic_quotes_sybase", toString(0LL)));
  if (same(v_input, LINE(21,x_stripslashes(x_addslashes(toString(v_input)))))) {
    echo("OK\n");
  }
  else {
    echo("FAILED\n");
  }
  echo("Sybase: ");
  LINE(28,x_ini_set("magic_quotes_sybase", toString(1LL)));
  if (same(v_input, LINE(29,x_stripslashes(x_addslashes(toString(v_input)))))) {
    echo("OK\n");
  }
  else {
    echo("FAILED\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
