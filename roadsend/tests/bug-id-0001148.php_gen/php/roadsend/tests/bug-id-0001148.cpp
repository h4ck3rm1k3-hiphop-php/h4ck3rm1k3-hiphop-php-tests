
#include <php/roadsend/tests/bug-id-0001148.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001148.php line 34 */
void f_test(CStrRef v_v1, CStrRef v_v2) {
  FUNCTION_INJECTION(test);
  Variant v_compare;

  print(LINE(35,concat5("in php, checking ", v_v1, " against ", v_v2, "\n")));
  (v_compare = LINE(36,x_version_compare(v_v1, v_v2)));
  {
    switch (toInt64(v_compare)) {
    case -1LL:
      {
        print(LINE(39,concat4(v_v1, " < ", v_v2, "\n")));
        break;
      }
    case 1LL:
      {
        print(LINE(42,concat4(v_v1, " > ", v_v2, "\n")));
        break;
      }
    case 0LL:
      {
      }
    default:
      {
        print(LINE(46,concat4(v_v1, " = ", v_v2, "\n")));
        break;
      }
    }
  }
} /* function */
Variant pm_php$roadsend$tests$bug_id_0001148_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001148.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001148_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_special_forms __attribute__((__unused__)) = (variables != gVariables) ? variables->get("special_forms") : g->GV(special_forms);
  Variant &v_operators __attribute__((__unused__)) = (variables != gVariables) ? variables->get("operators") : g->GV(operators);
  Variant &v_f1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f1") : g->GV(f1);
  Variant &v_f2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f2") : g->GV(f2);
  Variant &v_op __attribute__((__unused__)) = (variables != gVariables) ? variables->get("op") : g->GV(op);
  Variant &v_v1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v1") : g->GV(v1);
  Variant &v_v2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v2") : g->GV(v2);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  echo(" ");
  print("TESTING COMPARE\n");
  (v_special_forms = ScalarArrays::sa_[0]);
  (v_operators = ScalarArrays::sa_[1]);
  LINE(13,f_test("1", "2"));
  LINE(14,f_test("10", "2"));
  LINE(15,f_test("1.0", "1.1"));
  LINE(16,f_test("1.2", "1.0.1"));
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_special_forms.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_f1 = iter4->second();
      {
        {
          LOOP_COUNTER(5);
          for (ArrayIterPtr iter7 = v_special_forms.begin(); !iter7->end(); iter7->next()) {
            LOOP_COUNTER_CHECK(5);
            v_f2 = iter7->second();
            {
              LINE(19,f_test(toString("1.0") + toString(v_f1), toString("1.0") + toString(v_f2)));
            }
          }
        }
      }
    }
  }
  print("TESTING OPERATORS\n");
  {
    LOOP_COUNTER(8);
    for (ArrayIterPtr iter10 = v_special_forms.begin(); !iter10->end(); iter10->next()) {
      LOOP_COUNTER_CHECK(8);
      v_f1 = iter10->second();
      {
        {
          LOOP_COUNTER(11);
          for (ArrayIterPtr iter13 = v_special_forms.begin(); !iter13->end(); iter13->next()) {
            LOOP_COUNTER_CHECK(11);
            v_f2 = iter13->second();
            {
              {
                LOOP_COUNTER(14);
                for (ArrayIterPtr iter16 = v_operators.begin(); !iter16->end(); iter16->next()) {
                  LOOP_COUNTER_CHECK(14);
                  v_op = iter16->second();
                  {
                    (v_v1 = toString("1.0") + toString(v_f1));
                    (v_v2 = toString("1.0") + toString(v_f2));
                    (v_test = toBoolean(LINE(28,x_version_compare(toString(v_v1), toString(v_v2), toString(v_op)))) ? (("true")) : (("false")));
                    LINE(29,x_printf(5, "%7s %2s %-7s : %s\n", Array(ArrayInit(4).set(0, v_v1).set(1, v_op).set(2, v_v2).set(3, v_test).create())));
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
