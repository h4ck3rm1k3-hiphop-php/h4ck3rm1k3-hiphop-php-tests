
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001148_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001148_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001148.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001148_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_test(CStrRef v_v1, CStrRef v_v2);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001148_h__
