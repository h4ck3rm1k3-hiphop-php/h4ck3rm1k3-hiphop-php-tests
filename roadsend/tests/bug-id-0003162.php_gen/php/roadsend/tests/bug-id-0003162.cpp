
#include <php/roadsend/tests/bug-id-0003162.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0003162_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003162.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003162_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_UNIXTIME __attribute__((__unused__)) = (variables != gVariables) ? variables->get("UNIXTIME") : g->GV(UNIXTIME);
  Variant &v_K __attribute__((__unused__)) = (variables != gVariables) ? variables->get("K") : g->GV(K);
  Variant &v_from __attribute__((__unused__)) = (variables != gVariables) ? variables->get("from") : g->GV(from);
  Variant &v_to __attribute__((__unused__)) = (variables != gVariables) ? variables->get("to") : g->GV(to);

  echo("0003162: windows: string-> int conversion broken\r\n");
  (v_UNIXTIME = "1104534831");
  (v_K = 5LL);
  (v_from = toInt64(v_UNIXTIME) - v_K);
  (v_to = toInt64(v_UNIXTIME) + v_K);
  echo(LINE(11,concat4(toString(v_from), " - ", toString(v_to), "\n")));
  (v_UNIXTIME = 1104534831LL);
  (v_from = v_UNIXTIME - v_K);
  (v_to = v_UNIXTIME + v_K);
  echo(LINE(18,concat4(toString(v_from), " - ", toString(v_to), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
