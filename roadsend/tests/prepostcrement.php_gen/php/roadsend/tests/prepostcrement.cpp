
#include <php/roadsend/tests/prepostcrement.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$prepostcrement_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/prepostcrement.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$prepostcrement_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("<h3&gt;Postincrement</h3&gt;");
  (v_a = 5LL);
  echo(LINE(4,(assignCallTemp(eo_1, toString(v_a++)),concat3("Should be 5: ", eo_1, "<br>\n"))));
  echo(LINE(5,concat3("Should be 6: ", toString(v_a), "<br>\n")));
  echo("<h3>Preincrement</h3>");
  (v_a = 5LL);
  echo(LINE(9,(assignCallTemp(eo_1, toString(++v_a)),concat3("Should be 6: ", eo_1, "<br>\n"))));
  echo(LINE(10,concat3("Should be 6: ", toString(v_a), "<br>\n")));
  echo("<h3>Postdecrement</h3>");
  (v_a = 5LL);
  echo(LINE(14,(assignCallTemp(eo_1, toString(v_a--)),concat3("Should be 5: ", eo_1, "<br>\n"))));
  echo(LINE(15,concat3("Should be 4: ", toString(v_a), "<br>\n")));
  echo("<h3>Predecrement</h3>");
  (v_a = 5LL);
  echo(LINE(19,(assignCallTemp(eo_1, toString(--v_a)),concat3("Should be 4: ", eo_1, "<br>\n"))));
  echo(LINE(20,concat3("Should be 4: ", toString(v_a), "<br>\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
