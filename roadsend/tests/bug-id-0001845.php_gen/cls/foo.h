
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0001845.php line 3 */
class c_foo : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, foo, ObjectData)
  void init();
  public: String m_include;
  public: String m_include_once;
  public: String m_require;
  public: String m_require_once;
  public: String m_continue;
  public: String m_define;
  public: String m_parent;
  public: String m_exit;
  public: String m_false;
  public: String m_true;
  public: String m_echo;
  public: String m_print;
  public: String m_if;
  public: String m_else;
  public: String m_elseif;
  public: String m_while;
  public: String m_do;
  public: String m_or;
  public: String m_xor;
  public: String m_and;
  public: String m_endwhile;
  public: String m_endif;
  public: String m_for;
  public: String m_foreach;
  public: String m_as;
  public: String m_unset;
  public: String m_function;
  public: String m_var;
  public: String m_class;
  public: String m_extends;
  public: String m_array;
  public: String m_list;
  public: String m_new;
  public: String m_return;
  public: String m_global;
  public: String m_static;
  public: String m_switch;
  public: String m_endswitch;
  public: String m_default;
  public: String m_break;
  public: String m_case;
  public: String m_null;
  public: String m_bool;
  public: String m_boolean;
  public: String m_int;
  public: String m_integer;
  public: String m_float;
  public: String m_real;
  public: String m_double;
  public: String m_string;
  public: String m_object;
  public: void t_define();
  public: void t_parent();
  public: void t_false();
  public: void t_true();
  public: void t_null();
  public: void t_bool();
  public: void t_boolean();
  public: void t_int();
  public: void t_integer();
  public: void t_float();
  public: void t_real();
  public: void t_double();
  public: void t_string();
  public: void t_object();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
