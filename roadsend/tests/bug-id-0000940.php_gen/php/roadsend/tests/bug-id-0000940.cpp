
#include <php/roadsend/tests/bug-id-0000940.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000940_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000940.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000940_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ts") : g->GV(ts);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("erroneous conversion of string to array\n");
  (v_ts = "test string");
  echo(toString(v_ts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  echo(toString(v_ts.rvalAt(LINE(7,x_strlen(toString(v_ts))) - 1LL)));
  (v_foo = v_ts.rvalAt(4LL, 0x6F2A25235E544A31LL));
  echo(toString(v_ts.rvalAt("sasf", 0x14BB425C54A76B52LL)));
  LINE(19,x_var_dump(1, v_ts));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
