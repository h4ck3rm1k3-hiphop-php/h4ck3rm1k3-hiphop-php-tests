
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 1:
      HASH_RETURN(0x4E6A8B2878C74F89LL, g->k_LONG_MAX, LONG_MAX);
      break;
    case 3:
      HASH_RETURN(0x0F3231F30C0BCD73LL, g->k_LONG_MIN, LONG_MIN);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
