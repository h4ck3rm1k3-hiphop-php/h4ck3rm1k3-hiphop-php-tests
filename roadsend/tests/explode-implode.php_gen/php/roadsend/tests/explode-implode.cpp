
#include <php/roadsend/tests/explode-implode.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$explode_implode_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/explode-implode.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$explode_implode_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_pizza __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pizza") : g->GV(pizza);
  Variant &v_pieces __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pieces") : g->GV(pieces);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  (v_pizza = "piece1 piece2 piece3 piece4 piece5 piece6");
  (v_pieces = LINE(5,x_explode(" ", toString(v_pizza))));
  LINE(6,x_var_dump(1, v_pieces));
  (v_data = "foo:*:1023:1000::/home/foo:/bin/sh");
  (v_val = LINE(9,x_explode(":", toString(v_data))));
  LINE(10,x_var_dump(1, v_val));
  (v_data = "foo:-:*:-:1023:-:1000:-::-:/home/foo:-:/bin/sh");
  (v_val = LINE(13,x_explode(":-:", toString(v_data))));
  LINE(14,x_var_dump(1, v_val));
  (v_data = "foo:-:*:-:1023:-:1000:-::-:/home/foo:-:/bin/sh");
  (v_val = LINE(17,x_explode(":-:", toString(v_data), toInt32(3LL))));
  LINE(18,x_var_dump(1, v_val));
  (v_v = LINE(21,x_join("*", v_pieces)));
  LINE(22,x_var_dump(1, v_v));
  (v_v = LINE(24,x_join(v_pieces, "*-*")));
  LINE(25,x_var_dump(1, v_v));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
