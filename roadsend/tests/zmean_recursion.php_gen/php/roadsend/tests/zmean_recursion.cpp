
#include <php/roadsend/tests/zmean_recursion.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/zmean_recursion.php line 2 */
void f_rektest(Numeric v_nr) {
  FUNCTION_INJECTION(RekTest);
  Numeric v_j = 0;

  echo(LINE(4,concat3(" ", toString(v_nr), " ")));
  (v_j = v_nr + 1LL);
  LOOP_COUNTER(1);
  {
    while (less(v_j, 10LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(" a ");
        LINE(11,f_rektest(v_j));
        v_j++;
        echo(LINE(13,concat3(" b ", toString(v_j), " ")));
      }
    }
  }
  echo("\n");
} /* function */
Variant pm_php$roadsend$tests$zmean_recursion_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zmean_recursion.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zmean_recursion_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(21,f_rektest(0LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
