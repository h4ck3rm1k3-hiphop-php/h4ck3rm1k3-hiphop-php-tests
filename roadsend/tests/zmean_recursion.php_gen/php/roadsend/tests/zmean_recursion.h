
#ifndef __GENERATED_php_roadsend_tests_zmean_recursion_h__
#define __GENERATED_php_roadsend_tests_zmean_recursion_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zmean_recursion.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$zmean_recursion_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_rektest(Numeric v_nr);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zmean_recursion_h__
