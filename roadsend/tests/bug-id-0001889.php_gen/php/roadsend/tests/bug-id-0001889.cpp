
#include <php/roadsend/tests/bug-id-0001889.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001889.php line 6 */
Variant c_tag::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_tag::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_tag::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("txt", m_txt));
  c_ObjectData::o_get(props);
}
bool c_tag::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x6D046C530069DEC5LL, txt, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_tag::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x6D046C530069DEC5LL, m_txt,
                         txt, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_tag::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x6D046C530069DEC5LL, m_txt,
                      txt, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_tag::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_tag::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(tag)
ObjectData *c_tag::create(CStrRef v_t) {
  init();
  t_tag(v_t);
  return this;
}
ObjectData *c_tag::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_tag::cloneImpl() {
  c_tag *obj = NEW(c_tag)();
  cloneSet(obj);
  return obj;
}
void c_tag::cloneSet(c_tag *clone) {
  clone->m_txt = m_txt;
  ObjectData::cloneSet(clone);
}
Variant c_tag::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_tag::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_tag::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_tag$os_get(const char *s) {
  return c_tag::os_get(s, -1);
}
Variant &cw_tag$os_lval(const char *s) {
  return c_tag::os_lval(s, -1);
}
Variant cw_tag$os_constant(const char *s) {
  return c_tag::os_constant(s);
}
Variant cw_tag$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_tag::os_invoke(c, s, params, -1, fatal);
}
void c_tag::init() {
  m_txt = null;
}
/* SRC: roadsend/tests/bug-id-0001889.php line 8 */
void c_tag::t_tag(CStrRef v_t) {
  INSTANCE_METHOD_INJECTION(tag, tag::tag);
  bool oldInCtor = gasInCtor(true);
  (m_txt = v_t);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/bug-id-0001889.php line 13 */
Variant c_tpt::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_tpt::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_tpt::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("tags", m_tags.isReferenced() ? ref(m_tags) : m_tags));
  c_ObjectData::o_get(props);
}
bool c_tpt::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x3F0E67191C3535A0LL, tags, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_tpt::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x3F0E67191C3535A0LL, m_tags,
                         tags, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_tpt::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x3F0E67191C3535A0LL, m_tags,
                      tags, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_tpt::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x3F0E67191C3535A0LL, m_tags,
                         tags, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_tpt::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(tpt)
ObjectData *c_tpt::create() {
  init();
  t_tpt();
  return this;
}
ObjectData *c_tpt::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_tpt::cloneImpl() {
  c_tpt *obj = NEW(c_tpt)();
  cloneSet(obj);
  return obj;
}
void c_tpt::cloneSet(c_tpt *clone) {
  clone->m_tags = m_tags.isReferenced() ? ref(m_tags) : m_tags;
  ObjectData::cloneSet(clone);
}
Variant c_tpt::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x27F1A3DB0B4EE38ELL, addmore) {
        return (t_addmore(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_tpt::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x27F1A3DB0B4EE38ELL, addmore) {
        return (t_addmore(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_tpt::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_tpt$os_get(const char *s) {
  return c_tpt::os_get(s, -1);
}
Variant &cw_tpt$os_lval(const char *s) {
  return c_tpt::os_lval(s, -1);
}
Variant cw_tpt$os_constant(const char *s) {
  return c_tpt::os_constant(s);
}
Variant cw_tpt$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_tpt::os_invoke(c, s, params, -1, fatal);
}
void c_tpt::init() {
  m_tags = null;
}
/* SRC: roadsend/tests/bug-id-0001889.php line 15 */
void c_tpt::t_tpt() {
  INSTANCE_METHOD_INJECTION(tpt, tpt::tpt);
  bool oldInCtor = gasInCtor(true);
  m_tags.append((LINE(16,p_tag(p_tag(NEWOBJ(c_tag)())->create("inited")))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/bug-id-0001889.php line 18 */
void c_tpt::t_addmore() {
  INSTANCE_METHOD_INJECTION(tpt, tpt::addmore);
  m_tags.append((LINE(19,p_tag(p_tag(NEWOBJ(c_tag)())->create("more here")))));
} /* function */
/* SRC: roadsend/tests/bug-id-0001889.php line 23 */
Variant c_root::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_root::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_root::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("tlist", m_tlist));
  c_ObjectData::o_get(props);
}
bool c_root::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x2439940CDF699E5CLL, tlist, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_root::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2439940CDF699E5CLL, m_tlist,
                         tlist, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_root::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x2439940CDF699E5CLL, m_tlist,
                      tlist, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_root::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_root::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(root)
ObjectData *c_root::cloneImpl() {
  c_root *obj = NEW(c_root)();
  cloneSet(obj);
  return obj;
}
void c_root::cloneSet(c_root *clone) {
  clone->m_tlist = m_tlist;
  ObjectData::cloneSet(clone);
}
Variant c_root::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x114E34441303895FLL, newtpt) {
        return (t_newtpt(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_root::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x114E34441303895FLL, newtpt) {
        return (t_newtpt(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_root::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_root$os_get(const char *s) {
  return c_root::os_get(s, -1);
}
Variant &cw_root$os_lval(const char *s) {
  return c_root::os_lval(s, -1);
}
Variant cw_root$os_constant(const char *s) {
  return c_root::os_constant(s);
}
Variant cw_root$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_root::os_invoke(c, s, params, -1, fatal);
}
void c_root::init() {
  m_tlist = ScalarArrays::sa_[0];
}
/* SRC: roadsend/tests/bug-id-0001889.php line 26 */
Variant c_root::t_newtpt(CVarRef v_key) {
  INSTANCE_METHOD_INJECTION(root, root::newtpt);
  Variant v_newTemplate;

  if (isset(m_tlist, v_key)) {
    echo(LINE(29,concat3("found cache for ", toString(v_key), "\n")));
    return m_tlist.rvalAt(v_key);
  }
  echo(LINE(33,concat3("no cache for ", toString(v_key), "\n")));
  (v_newTemplate = LINE(34,p_tpt(p_tpt(NEWOBJ(c_tpt)())->create())));
  m_tlist.set(v_key, (ref(v_newTemplate)));
  return v_newTemplate;
} /* function */
Object co_tag(CArrRef params, bool init /* = true */) {
  return Object(p_tag(NEW(c_tag)())->dynCreate(params, init));
}
Object co_tpt(CArrRef params, bool init /* = true */) {
  return Object(p_tpt(NEW(c_tpt)())->dynCreate(params, init));
}
Object co_root(CArrRef params, bool init /* = true */) {
  return Object(p_root(NEW(c_root)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0001889_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001889.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001889_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_t2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t2") : g->GV(t2);

  echo("this is similar to what sitemanager does.  it should help us ferret out \ncopying problems.\n\n");
  (v_r = LINE(46,p_root(p_root(NEWOBJ(c_root)())->create())));
  (v_t = ref(LINE(48,v_r.o_invoke_few_args("newtpt", 0x114E34441303895FLL, 1, "thiskey"))));
  LINE(49,x_var_dump(1, v_t));
  LINE(50,v_t.o_invoke_few_args("addmore", 0x27F1A3DB0B4EE38ELL, 0));
  LINE(51,x_var_dump(1, v_t));
  (v_t2 = ref(LINE(53,v_r.o_invoke_few_args("newtpt", 0x114E34441303895FLL, 1, "thiskey"))));
  LINE(54,x_var_dump(1, v_t2));
  LINE(55,v_t2.o_invoke_few_args("addmore", 0x27F1A3DB0B4EE38ELL, 0));
  LINE(56,x_var_dump(1, v_t2));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
