
#ifndef __GENERATED_php_roadsend_tests_ext_prop_order_h__
#define __GENERATED_php_roadsend_tests_ext_prop_order_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/ext-prop-order.fw.h>

// Declarations
#include <cls/a.h>
#include <cls/b.h>
#include <cls/c.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$ext_prop_order_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_a(CArrRef params, bool init = true);
Object co_b(CArrRef params, bool init = true);
Object co_c(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_ext_prop_order_h__
