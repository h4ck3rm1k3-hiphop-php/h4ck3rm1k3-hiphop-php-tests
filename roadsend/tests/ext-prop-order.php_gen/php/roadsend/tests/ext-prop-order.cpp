
#include <php/roadsend/tests/ext-prop-order.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/ext-prop-order.php line 3 */
Variant c_a::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 3) {
    case 1:
      HASH_RETURN(0x4827A10B8534590DLL, g->s_a_DupIdsa1,
                  sa1);
      break;
    case 3:
      HASH_RETURN(0x104C0D6FEB3F8E17LL, g->s_a_DupIdsa2,
                  sa2);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a1", m_a1));
  props.push_back(NEW(ArrayElement)("a2", m_a2));
  props.push_back(NEW(ArrayElement)("pa1", m_pa1));
  props.push_back(NEW(ArrayElement)("ppa1", m_ppa1));
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_EXISTS_STRING(0x26570500C08A1ED3LL, pa1, 3);
      break;
    case 6:
      HASH_EXISTS_STRING(0x6C05C2858C72A876LL, a2, 2);
      HASH_EXISTS_STRING(0x6BA7C5736DABB916LL, ppa1, 4);
      break;
    case 7:
      HASH_EXISTS_STRING(0x68DF81F26D942FC7LL, a1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_RETURN_STRING(0x26570500C08A1ED3LL, m_pa1,
                         pa1, 3);
      break;
    case 6:
      HASH_RETURN_STRING(0x6C05C2858C72A876LL, m_a2,
                         a2, 2);
      HASH_RETURN_STRING(0x6BA7C5736DABB916LL, m_ppa1,
                         ppa1, 4);
      break;
    case 7:
      HASH_RETURN_STRING(0x68DF81F26D942FC7LL, m_a1,
                         a1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_SET_STRING(0x26570500C08A1ED3LL, m_pa1,
                      pa1, 3);
      break;
    case 6:
      HASH_SET_STRING(0x6C05C2858C72A876LL, m_a2,
                      a2, 2);
      HASH_SET_STRING(0x6BA7C5736DABB916LL, m_ppa1,
                      ppa1, 4);
      break;
    case 7:
      HASH_SET_STRING(0x68DF81F26D942FC7LL, m_a1,
                      a1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  clone->m_a1 = m_a1;
  clone->m_a2 = m_a2;
  clone->m_pa1 = m_pa1;
  clone->m_ppa1 = m_ppa1;
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
  m_a1 = 1LL;
  m_a2 = 2LL;
  m_pa1 = 10LL;
  m_ppa1 = 20LL;
}
void c_a::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_a_DupIdsa1 = 50LL;
  g->s_a_DupIdsa2 = 51LL;
}
void csi_a() {
  c_a::os_static_initializer();
}
/* SRC: roadsend/tests/ext-prop-order.php line 12 */
Variant c_b::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 3) {
    case 1:
      HASH_RETURN(0x10E35875460A1A61LL, g->s_b_DupIdsb1,
                  sb1);
      break;
    case 3:
      HASH_RETURN(0x34F7DBE8FC48C31BLL, g->s_b_DupIdsb2,
                  sb2);
      break;
    default:
      break;
  }
  return c_a::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_a::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a2", m_a2));
  props.push_back(NEW(ArrayElement)("b1", m_b1));
  props.push_back(NEW(ArrayElement)("pb1", m_pb1));
  props.push_back(NEW(ArrayElement)("b2", m_b2));
  props.push_back(NEW(ArrayElement)("ppb1", m_ppb1));
  c_a::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x2D5185583EF85E98LL, b2, 2);
      break;
    case 2:
      HASH_EXISTS_STRING(0x4AC109778D405CDALL, pb1, 3);
      break;
    case 3:
      HASH_EXISTS_STRING(0x29C390281340176BLL, b1, 2);
      break;
    case 5:
      HASH_EXISTS_STRING(0x5A1C829E7D00F7F5LL, ppb1, 4);
      break;
    default:
      break;
  }
  return c_a::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x2D5185583EF85E98LL, m_b2,
                         b2, 2);
      break;
    case 2:
      HASH_RETURN_STRING(0x4AC109778D405CDALL, m_pb1,
                         pb1, 3);
      break;
    case 3:
      HASH_RETURN_STRING(0x29C390281340176BLL, m_b1,
                         b1, 2);
      break;
    case 5:
      HASH_RETURN_STRING(0x5A1C829E7D00F7F5LL, m_ppb1,
                         ppb1, 4);
      break;
    default:
      break;
  }
  return c_a::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x2D5185583EF85E98LL, m_b2,
                      b2, 2);
      break;
    case 2:
      HASH_SET_STRING(0x4AC109778D405CDALL, m_pb1,
                      pb1, 3);
      break;
    case 3:
      HASH_SET_STRING(0x29C390281340176BLL, m_b1,
                      b1, 2);
      break;
    case 5:
      HASH_SET_STRING(0x5A1C829E7D00F7F5LL, m_ppb1,
                      ppb1, 4);
      break;
    default:
      break;
  }
  return c_a::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_a::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  return c_a::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  clone->m_a2 = m_a2;
  clone->m_b1 = m_b1;
  clone->m_pb1 = m_pb1;
  clone->m_b2 = m_b2;
  clone->m_ppb1 = m_ppb1;
  c_a::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_a::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_a::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_a::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
  c_a::init();
  m_a2 = 199LL;
  m_b1 = 3LL;
  m_pb1 = 11LL;
  m_b2 = 4LL;
  m_ppb1 = 21LL;
}
void c_b::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_b_DupIdsb1 = 52LL;
  g->s_b_DupIdsb2 = 53LL;
}
void csi_b() {
  c_b::os_static_initializer();
}
/* SRC: roadsend/tests/ext-prop-order.php line 22 */
Variant c_c::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x7F54A7F95AF44C30LL, g->s_c_DupIdsc2,
                  sc2);
      break;
    case 1:
      HASH_RETURN(0x4AF0AF508925B6ADLL, g->s_c_DupIdsc1,
                  sc1);
      break;
    default:
      break;
  }
  return c_b::os_get(s, hash);
}
Variant &c_c::os_lval(const char *s, int64 hash) {
  return c_b::os_lval(s, hash);
}
void c_c::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("ppc1", m_ppc1));
  props.push_back(NEW(ArrayElement)("pc1", m_pc1));
  props.push_back(NEW(ArrayElement)("c1", m_c1));
  props.push_back(NEW(ArrayElement)("c2", m_c2));
  c_b::o_get(props);
}
bool c_c::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x062075DC196CACD0LL, c2, 2);
      break;
    case 3:
      HASH_EXISTS_STRING(0x23401BF3A8F8E58BLL, c1, 2);
      break;
    case 5:
      HASH_EXISTS_STRING(0x6ABBA6E6C6C7E6FDLL, ppc1, 4);
      break;
    case 6:
      HASH_EXISTS_STRING(0x62A2F816DB0BD0F6LL, pc1, 3);
      break;
    default:
      break;
  }
  return c_b::o_exists(s, hash);
}
Variant c_c::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x062075DC196CACD0LL, m_c2,
                         c2, 2);
      break;
    case 3:
      HASH_RETURN_STRING(0x23401BF3A8F8E58BLL, m_c1,
                         c1, 2);
      break;
    case 5:
      HASH_RETURN_STRING(0x6ABBA6E6C6C7E6FDLL, m_ppc1,
                         ppc1, 4);
      break;
    case 6:
      HASH_RETURN_STRING(0x62A2F816DB0BD0F6LL, m_pc1,
                         pc1, 3);
      break;
    default:
      break;
  }
  return c_b::o_get(s, hash);
}
Variant c_c::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x062075DC196CACD0LL, m_c2,
                      c2, 2);
      break;
    case 3:
      HASH_SET_STRING(0x23401BF3A8F8E58BLL, m_c1,
                      c1, 2);
      break;
    case 5:
      HASH_SET_STRING(0x6ABBA6E6C6C7E6FDLL, m_ppc1,
                      ppc1, 4);
      break;
    case 6:
      HASH_SET_STRING(0x62A2F816DB0BD0F6LL, m_pc1,
                      pc1, 3);
      break;
    default:
      break;
  }
  return c_b::o_set(s, hash, v, forInit);
}
Variant &c_c::o_lval(CStrRef s, int64 hash) {
  return c_b::o_lval(s, hash);
}
Variant c_c::os_constant(const char *s) {
  return c_b::os_constant(s);
}
IMPLEMENT_CLASS(c)
ObjectData *c_c::cloneImpl() {
  c_c *obj = NEW(c_c)();
  cloneSet(obj);
  return obj;
}
void c_c::cloneSet(c_c *clone) {
  clone->m_ppc1 = m_ppc1;
  clone->m_pc1 = m_pc1;
  clone->m_c1 = m_c1;
  clone->m_c2 = m_c2;
  c_b::cloneSet(clone);
}
Variant c_c::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_b::o_invoke(s, params, hash, fatal);
}
Variant c_c::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_b::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_c::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_b::os_invoke(c, s, params, hash, fatal);
}
Variant cw_c$os_get(const char *s) {
  return c_c::os_get(s, -1);
}
Variant &cw_c$os_lval(const char *s) {
  return c_c::os_lval(s, -1);
}
Variant cw_c$os_constant(const char *s) {
  return c_c::os_constant(s);
}
Variant cw_c$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_c::os_invoke(c, s, params, -1, fatal);
}
void c_c::init() {
  c_b::init();
  m_ppc1 = 22LL;
  m_pc1 = 12LL;
  m_c1 = 5LL;
  m_c2 = 6LL;
}
void c_c::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_c_DupIdsc1 = 54LL;
  g->s_c_DupIdsc2 = 55LL;
}
void csi_c() {
  c_c::os_static_initializer();
}
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Object co_c(CArrRef params, bool init /* = true */) {
  return Object(p_c(NEW(c_c)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$ext_prop_order_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/ext-prop-order.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$ext_prop_order_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_a = ((Object)(LINE(31,p_a(p_a(NEWOBJ(c_a)())->create())))));
  (v_a.o_lval("a3", 0x43EDA7BEE714570DLL) = 7LL);
  LINE(33,x_print_r(v_a));
  (v_b = ((Object)(LINE(35,p_b(p_b(NEWOBJ(c_b)())->create())))));
  (v_b.o_lval("b3", 0x5F4AE7AA1641D45ELL) = 8LL);
  LINE(37,x_print_r(v_b));
  (v_c = ((Object)(LINE(39,p_c(p_c(NEWOBJ(c_c)())->create())))));
  (v_c.o_lval("c3", 0x365DE53E91F259AELL) = 9LL);
  LINE(41,x_print_r(v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
