
#ifndef __GENERATED_php_roadsend_tests_ext_prop_order_fw_h__
#define __GENERATED_php_roadsend_tests_ext_prop_order_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(a)
FORWARD_DECLARE_CLASS(b)
FORWARD_DECLARE_CLASS(c)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_ext_prop_order_fw_h__
