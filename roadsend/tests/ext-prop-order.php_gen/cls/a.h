
#ifndef __GENERATED_cls_a_h__
#define __GENERATED_cls_a_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/ext-prop-order.php line 3 */
class c_a : virtual public ObjectData {
  BEGIN_CLASS_MAP(a)
  END_CLASS_MAP(a)
  DECLARE_CLASS(a, a, ObjectData)
  void init();
  public: int64 m_a1;
  public: int64 m_a2;
  public: int64 m_pa1;
  public: int64 m_ppa1;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_a_h__
