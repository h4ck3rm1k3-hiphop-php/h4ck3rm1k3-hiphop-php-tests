
#ifndef __GENERATED_php_roadsend_tests_ref_h__
#define __GENERATED_php_roadsend_tests_ref_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/ref.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$ref_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_foocrement(Variant v_foo);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_ref_h__
