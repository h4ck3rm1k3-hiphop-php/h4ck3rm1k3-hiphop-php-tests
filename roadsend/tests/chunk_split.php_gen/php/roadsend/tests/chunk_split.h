
#ifndef __GENERATED_php_roadsend_tests_chunk_split_h__
#define __GENERATED_php_roadsend_tests_chunk_split_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/chunk_split.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$chunk_split_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_chunk_split_h__
