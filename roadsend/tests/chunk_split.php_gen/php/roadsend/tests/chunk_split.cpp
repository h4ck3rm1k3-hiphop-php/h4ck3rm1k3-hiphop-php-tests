
#include <php/roadsend/tests/chunk_split.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$chunk_split_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/chunk_split.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$chunk_split_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);

  (v_data = "\n<MeMudd> doh\n<MeMudd> well get on it\n<Gollum> :)\n<Gollum> did you see the bot sign off\n<MeMudd> slacker\n<Gollum> it should be back in a min\n<MeMudd> did you see it sign back on\?\n<MeMudd> yeah whats up\n<Gollum> ;\n<Gollum> ;)\n<Gollum> it's the irc server \n<Gollum> it's slow\n--> Smeagol (~PircBot@u1037566.net) has joined #mudsville\n--- Gollum gives channel operator status to Smeagol\n<Smeagol> Thanks\n<MeMudd> sure sure, blame it on the irc server. Mr Developer, developers always blame it on Admins\n<Gollum> I'm ont a developer\n<MeMudd> then what is you\?\n<Gollum> I only play one on T.V.\n<Gollum> I'm not really a computer geek at all.\n<Gollum> I work at McDonalds.\n<Gollum> would you like fries with that B.S. burger\?\n<MeMudd> sure\n<no_l0gic> mmm... fries <drool>\n<no_l0gic> and buttered bacon\?\n<Gollum> bacon cheese burger with onion rings ;)\n<Gollum> and a beer\n");
  echo(LINE(33,x_chunk_split(toString(v_data), toInt32(10LL))));
  echo(LINE(35,x_chunk_split(toString(v_data), toInt32(5LL))));
  echo(LINE(37,x_chunk_split(toString(v_data))));
  echo(LINE(39,x_chunk_split(toString(v_data), toInt32(10LL), "-blah-\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
