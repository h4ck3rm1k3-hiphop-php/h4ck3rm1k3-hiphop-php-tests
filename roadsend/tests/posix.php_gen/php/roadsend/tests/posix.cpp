
#include <php/roadsend/tests/posix.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$posix_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/posix.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$posix_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_pid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pid") : g->GV(pid);
  Variant &v_ppid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ppid") : g->GV(ppid);
  Variant &v_uid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("uid") : g->GV(uid);
  Variant &v_euid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("euid") : g->GV(euid);
  Variant &v_gid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("gid") : g->GV(gid);
  Variant &v_egid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("egid") : g->GV(egid);
  Variant &v_groups __attribute__((__unused__)) = (variables != gVariables) ? variables->get("groups") : g->GV(groups);
  Variant &v_login __attribute__((__unused__)) = (variables != gVariables) ? variables->get("login") : g->GV(login);
  Variant &v_pgrp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pgrp") : g->GV(pgrp);
  Variant &v_setsid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("setsid") : g->GV(setsid);
  Variant &v_getpgid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getpgid") : g->GV(getpgid);
  Variant &v_getsid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getsid") : g->GV(getsid);
  Variant &v_setpgid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("setpgid") : g->GV(setpgid);
  Variant &v_uname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("uname") : g->GV(uname);
  Variant &v_times __attribute__((__unused__)) = (variables != gVariables) ? variables->get("times") : g->GV(times);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_ctermid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ctermid") : g->GV(ctermid);
  Variant &v_ttyname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ttyname") : g->GV(ttyname);
  Variant &v_isatty __attribute__((__unused__)) = (variables != gVariables) ? variables->get("isatty") : g->GV(isatty);
  Variant &v_cwd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cwd") : g->GV(cwd);
  Variant &v_testfile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testfile") : g->GV(testfile);
  Variant &v_mkfifo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mkfifo") : g->GV(mkfifo);
  Variant &v_getgrnam __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getgrnam") : g->GV(getgrnam);
  Variant &v_getgrgid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getgrgid") : g->GV(getgrgid);
  Variant &v_getpwnam __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getpwnam") : g->GV(getpwnam);
  Variant &v_getpwuid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getpwuid") : g->GV(getpwuid);
  Variant &v_getrlimit __attribute__((__unused__)) = (variables != gVariables) ? variables->get("getrlimit") : g->GV(getrlimit);
  Variant &v_retval __attribute__((__unused__)) = (variables != gVariables) ? variables->get("retval") : g->GV(retval);
  Variant &v_errno __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errno") : g->GV(errno);
  Variant &v_errstr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errstr") : g->GV(errstr);

  (v_pid = LINE(4,x_posix_getpid()));
  if (more(v_pid, 0LL)) echo("posix_getpid succeeded\n");
  else echo("posix_getpid failed\n");
  (v_ppid = LINE(10,x_posix_getppid()));
  if (more(v_ppid, 0LL)) echo("posix_getppid succeeded\n");
  else echo("posix_getppid failed\n");
  (v_uid = LINE(16,x_posix_getuid()));
  echo(LINE(17,concat3("uid=", toString(v_uid), "\n")));
  (v_euid = LINE(19,x_posix_geteuid()));
  echo(LINE(20,concat3("euid=", toString(v_euid), "\n")));
  (v_gid = LINE(22,x_posix_getgid()));
  echo(LINE(23,concat3("gid=", toString(v_gid), "\n")));
  (v_egid = LINE(25,x_posix_getegid()));
  echo(LINE(26,concat3("egid=", toString(v_egid), "\n")));
  LINE(28,x_posix_setuid(toInt32(1004LL)));
  (v_uid = LINE(29,x_posix_getuid()));
  echo(LINE(30,concat3("uid=", toString(v_uid), "\n")));
  LINE(32,x_posix_seteuid(toInt32(1004LL)));
  (v_euid = LINE(33,x_posix_geteuid()));
  echo(LINE(34,concat3("euid=", toString(v_euid), "\n")));
  LINE(36,x_posix_setgid(toInt32(1004LL)));
  (v_gid = LINE(37,x_posix_getgid()));
  echo(LINE(38,concat3("gid=", toString(v_gid), "\n")));
  LINE(40,x_posix_setegid(toInt32(1004LL)));
  (v_egid = LINE(41,x_posix_getegid()));
  echo(LINE(42,concat3("egid=", toString(v_egid), "\n")));
  (v_groups = LINE(44,x_posix_getgroups()));
  echo("groups=\n");
  LINE(46,x_print_r(v_groups));
  (v_login = LINE(48,x_posix_getlogin()));
  echo(LINE(49,concat3("login=", toString(v_login), "\n")));
  (v_pgrp = LINE(51,x_posix_getpgrp()));
  echo(LINE(52,concat3("pgrp=", toString(v_pgrp), "\n")));
  (v_setsid = LINE(54,x_posix_setsid()));
  if (more(v_setsid, 0LL)) echo("posix_setsid succeeded\n");
  else echo("posix_setsid failed\n");
  (v_getpgid = LINE(60,x_posix_getpgid(toInt32(v_pid))));
  if (more(v_getpgid, 0LL)) echo("posix_getpgid succeeded\n");
  else echo("posix_getpgid failed\n");
  (v_getsid = LINE(66,x_posix_getsid(toInt32(v_pid))));
  if (more(v_getsid, 0LL)) echo("posix_getsid succeeded\n");
  else echo("posix_getsid failed\n");
  (v_setpgid = LINE(72,x_posix_setpgid(toInt32(v_pid), toInt32(v_getpgid))));
  if (more(v_setpgid, 0LL)) echo("posix_setpgid succeeded\n");
  else echo("posix_setpgid failed\n");
  (v_uname = LINE(78,x_posix_uname()));
  echo("uname=\n");
  LINE(80,x_print_r(v_uname));
  (v_times = LINE(82,x_posix_times()));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_times.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      if (less(v_v, 0LL)) echo(LINE(85,concat3("times[", toString(v_k), "] is negative\n")));
      else echo(LINE(87,concat3("times[", toString(v_k), "] is greater than or equal to 0\n")));
    }
  }
  (v_ctermid = LINE(89,x_posix_ctermid()));
  echo(LINE(90,concat3("ctermid=", toString(v_ctermid), "\n")));
  (v_ttyname = LINE(92,x_posix_ttyname(1LL)));
  echo(LINE(93,concat3("ttyname for fd 1 = ", toString(v_ttyname), "\n")));
  (v_isatty = LINE(95,x_posix_isatty(1LL)));
  echo(LINE(96,concat3("isatty for fd 1 = ", toString(v_isatty), "\n")));
  (v_cwd = LINE(98,x_posix_getcwd()));
  if (LINE(99,x_file_exists(toString(v_cwd)))) echo("posix_getcwd succeeded\n");
  else echo("posix_getcwd failed\n");
  (v_testfile = "/tmp/phpoo_test_fifo204982");
  if (LINE(106,x_file_exists(toString(v_testfile)))) LINE(107,x_unlink(toString(v_testfile)));
  (v_mkfifo = LINE(109,x_posix_mkfifo(toString(v_testfile), toInt32(0LL))));
  echo(LINE(110,concat3("mkfifo=", toString(v_mkfifo), "\n")));
  if (LINE(113,x_file_exists(toString(v_testfile)))) LINE(114,x_unlink(toString(v_testfile)));
  (v_getgrnam = LINE(116,x_posix_getgrnam("floppy")));
  echo("getgrnam for floppy =\n");
  LINE(118,x_var_dump(1, v_getgrnam));
  (v_getgrgid = LINE(120,x_posix_getgrgid(toInt32(25LL))));
  echo("getgrgid for group 25 =\n");
  LINE(122,x_var_dump(1, v_getgrgid));
  (v_getpwnam = LINE(124,x_posix_getpwnam("ftp")));
  echo("getpwnam for user ftp =\n");
  LINE(126,x_var_dump(1, v_getpwnam));
  (v_getpwuid = LINE(128,x_posix_getpwuid(toInt32(106LL))));
  echo("getpwuid for userid 106 =\n");
  LINE(130,x_var_dump(1, v_getpwuid));
  (v_getrlimit = LINE(132,x_posix_getrlimit()));
  echo("getrlimit=\n");
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_getrlimit.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      if (!((same(v_k, "soft stack")))) echo(LINE(136,concat5("fstat[", toString(v_k), "] = ", toString(v_v), "\n")));
      else if (less(v_v, 0LL)) echo(LINE(139,concat3("fstat[", toString(v_k), "] is negative\n")));
      else echo(LINE(141,concat3("fstat[", toString(v_k), "] is greater than or equal to 0\n")));
    }
  }
  (v_retval = LINE(144,x_posix_kill(toInt32(-1LL), toInt32(-1LL))));
  (v_errno = LINE(145,x_posix_get_last_error()));
  (v_errstr = LINE(146,x_posix_strerror(toInt32(v_errno))));
  echo(LINE(147,concat3("posix_kill(-1, -1) returned ", toString(v_retval), "\n")));
  echo(LINE(148,concat5("posix_get_last_error(", toString(v_retval), ") returned ", toString(v_errno), "\n")));
  echo(LINE(149,concat5("posix_strerror(", toString(v_errno), ") returned ", toString(v_errstr), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
