
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "pid",
    "ppid",
    "uid",
    "euid",
    "gid",
    "egid",
    "groups",
    "login",
    "pgrp",
    "setsid",
    "getpgid",
    "getsid",
    "setpgid",
    "uname",
    "times",
    "k",
    "v",
    "ctermid",
    "ttyname",
    "isatty",
    "cwd",
    "testfile",
    "mkfifo",
    "getgrnam",
    "getgrgid",
    "getpwnam",
    "getpwuid",
    "getrlimit",
    "retval",
    "errno",
    "errstr",
  };
  if (idx >= 0 && idx < 43) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(pid);
    case 13: return GV(ppid);
    case 14: return GV(uid);
    case 15: return GV(euid);
    case 16: return GV(gid);
    case 17: return GV(egid);
    case 18: return GV(groups);
    case 19: return GV(login);
    case 20: return GV(pgrp);
    case 21: return GV(setsid);
    case 22: return GV(getpgid);
    case 23: return GV(getsid);
    case 24: return GV(setpgid);
    case 25: return GV(uname);
    case 26: return GV(times);
    case 27: return GV(k);
    case 28: return GV(v);
    case 29: return GV(ctermid);
    case 30: return GV(ttyname);
    case 31: return GV(isatty);
    case 32: return GV(cwd);
    case 33: return GV(testfile);
    case 34: return GV(mkfifo);
    case 35: return GV(getgrnam);
    case 36: return GV(getgrgid);
    case 37: return GV(getpwnam);
    case 38: return GV(getpwuid);
    case 39: return GV(getrlimit);
    case 40: return GV(retval);
    case 41: return GV(errno);
    case 42: return GV(errstr);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
