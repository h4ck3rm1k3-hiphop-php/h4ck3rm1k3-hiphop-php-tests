
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

static void print(FILE *fp, String s) {
  if (fp) {
    fwrite(s.c_str(), 1, s.size(), fp);
    return;
  }
  echo(s);
}
static void output_dynamic_constants(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_constants = json_decode('");
  Array dynamic_constants;
  String s = f_json_encode(dynamic_constants);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_static_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$static_global_variables = json_decode('");
  Array static_global_variables;
  static_global_variables.set("gv_argc", g->get("argc"));
  static_global_variables.set("gv_argv", g->get("argv"));
  static_global_variables.set("gv__SERVER", g->get("_SERVER"));
  static_global_variables.set("gv__GET", g->get("_GET"));
  static_global_variables.set("gv__POST", g->get("_POST"));
  static_global_variables.set("gv__COOKIE", g->get("_COOKIE"));
  static_global_variables.set("gv__FILES", g->get("_FILES"));
  static_global_variables.set("gv__ENV", g->get("_ENV"));
  static_global_variables.set("gv__REQUEST", g->get("_REQUEST"));
  static_global_variables.set("gv__SESSION", g->get("_SESSION"));
  static_global_variables.set("gv_HTTP_RAW_POST_DATA", g->get("HTTP_RAW_POST_DATA"));
  static_global_variables.set("gv_http_response_header", g->get("http_response_header"));
  static_global_variables.set("gv_pid", g->get("pid"));
  static_global_variables.set("gv_ppid", g->get("ppid"));
  static_global_variables.set("gv_uid", g->get("uid"));
  static_global_variables.set("gv_euid", g->get("euid"));
  static_global_variables.set("gv_gid", g->get("gid"));
  static_global_variables.set("gv_egid", g->get("egid"));
  static_global_variables.set("gv_groups", g->get("groups"));
  static_global_variables.set("gv_login", g->get("login"));
  static_global_variables.set("gv_pgrp", g->get("pgrp"));
  static_global_variables.set("gv_setsid", g->get("setsid"));
  static_global_variables.set("gv_getpgid", g->get("getpgid"));
  static_global_variables.set("gv_getsid", g->get("getsid"));
  static_global_variables.set("gv_setpgid", g->get("setpgid"));
  static_global_variables.set("gv_uname", g->get("uname"));
  static_global_variables.set("gv_times", g->get("times"));
  static_global_variables.set("gv_k", g->get("k"));
  static_global_variables.set("gv_v", g->get("v"));
  static_global_variables.set("gv_ctermid", g->get("ctermid"));
  static_global_variables.set("gv_ttyname", g->get("ttyname"));
  static_global_variables.set("gv_isatty", g->get("isatty"));
  static_global_variables.set("gv_cwd", g->get("cwd"));
  static_global_variables.set("gv_testfile", g->get("testfile"));
  static_global_variables.set("gv_mkfifo", g->get("mkfifo"));
  static_global_variables.set("gv_getgrnam", g->get("getgrnam"));
  static_global_variables.set("gv_getgrgid", g->get("getgrgid"));
  static_global_variables.set("gv_getpwnam", g->get("getpwnam"));
  static_global_variables.set("gv_getpwuid", g->get("getpwuid"));
  static_global_variables.set("gv_getrlimit", g->get("getrlimit"));
  static_global_variables.set("gv_retval", g->get("retval"));
  static_global_variables.set("gv_errno", g->get("errno"));
  static_global_variables.set("gv_errstr", g->get("errstr"));
  String s = f_json_encode(static_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_dynamic_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_global_variables = json_decode('");
  Array dynamic_global_variables;
  dynamic_global_variables = *get_variable_table();
  String s = f_json_encode(dynamic_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_variables = json_decode('");
  Array method_static_variables;
  String s = f_json_encode(method_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_inited(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_inited = json_decode('");
  Array method_static_inited;
  String s = f_json_encode(method_static_inited);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_class_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$class_static_variables = json_decode('");
  Array class_static_variables;
  String s = f_json_encode(class_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_pseudomain_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$pseudomain_variables = json_decode('");
  Array pseudomain_variables;
  pseudomain_variables.set("run_pm_php$roadsend$tests$posix_php", g->run_pm_php$roadsend$tests$posix_php);
  String s = f_json_encode(pseudomain_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_functions(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_functions = json_decode('");
  Array redeclared_functions;
  String s = f_json_encode(redeclared_functions);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_classes(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_classes = json_decode('");
  Array redeclared_classes;
  String s = f_json_encode(redeclared_classes);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

void output_global_state(FILE *fp) {
  output_static_global_variables(fp);
  output_dynamic_global_variables(fp);
  output_dynamic_constants(fp);
  output_method_static_variables(fp);
  output_method_static_inited(fp);
  output_class_static_variables(fp);
  output_pseudomain_variables(fp);
  output_redeclared_functions(fp);
  output_redeclared_classes(fp);
}


///////////////////////////////////////////////////////////////////////////////
}
