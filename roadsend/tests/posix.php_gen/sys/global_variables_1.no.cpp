
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 0:
      HASH_RETURN(0x2AC6042EF2A11F00LL, g->GV(ttyname),
                  ttyname);
      break;
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 7:
      HASH_RETURN(0x79F0EE2AD5236D07LL, g->GV(getgrgid),
                  getgrgid);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x51FE5505C0CE6491LL, g->GV(getpgid),
                  getpgid);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 22:
      HASH_RETURN(0x6B3093D1F2F27D16LL, g->GV(cwd),
                  cwd);
      break;
    case 24:
      HASH_RETURN(0x07D7BDC495A07A18LL, g->GV(uname),
                  uname);
      HASH_RETURN(0x3928BFE39F221018LL, g->GV(errstr),
                  errstr);
      break;
    case 25:
      HASH_RETURN(0x66FE4C97C9502A99LL, g->GV(isatty),
                  isatty);
      break;
    case 27:
      HASH_RETURN(0x5B22A7472448739BLL, g->GV(gid),
                  gid);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 44:
      HASH_RETURN(0x5E660E204990332CLL, g->GV(ppid),
                  ppid);
      break;
    case 54:
      HASH_RETURN(0x31CF71EAC03B86B6LL, g->GV(testfile),
                  testfile);
      break;
    case 61:
      HASH_RETURN(0x69DBBF74301F96BDLL, g->GV(retval),
                  retval);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 67:
      HASH_RETURN(0x225D2F8E6409A9C3LL, g->GV(login),
                  login);
      break;
    case 70:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x60B57838E4303D46LL, g->GV(getpwuid),
                  getpwuid);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 74:
      HASH_RETURN(0x2B7634BE3D12BDCALL, g->GV(uid),
                  uid);
      HASH_RETURN(0x23767E1D6F8CFCCALL, g->GV(groups),
                  groups);
      HASH_RETURN(0x57A84E31E9BBAFCALL, g->GV(times),
                  times);
      break;
    case 78:
      HASH_RETURN(0x3580763DDA0EFDCELL, g->GV(setpgid),
                  setpgid);
      break;
    case 79:
      HASH_RETURN(0x0D6857FDDD7F21CFLL, g->GV(k),
                  k);
      break;
    case 80:
      HASH_RETURN(0x6EAB859F44A4DED0LL, g->GV(egid),
                  egid);
      HASH_RETURN(0x409A9095CEE37350LL, g->GV(mkfifo),
                  mkfifo);
      break;
    case 81:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 85:
      HASH_RETURN(0x1DED64CF31236F55LL, g->GV(errno),
                  errno);
      break;
    case 98:
      HASH_RETURN(0x702CB6B3DEDF0FE2LL, g->GV(setsid),
                  setsid);
      break;
    case 102:
      HASH_RETURN(0x13C446138383FA66LL, g->GV(getgrnam),
                  getgrnam);
      break;
    case 103:
      HASH_RETURN(0x502920B03EBA16E7LL, g->GV(getrlimit),
                  getrlimit);
      break;
    case 107:
      HASH_RETURN(0x3C2F961831E4EF6BLL, g->GV(v),
                  v);
      break;
    case 108:
      HASH_RETURN(0x528E3AD621755D6CLL, g->GV(pgrp),
                  pgrp);
      break;
    case 110:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 115:
      HASH_RETURN(0x133F26DF225E6273LL, g->GV(pid),
                  pid);
      break;
    case 118:
      HASH_RETURN(0x5E5C63DE043CB3F6LL, g->GV(ctermid),
                  ctermid);
      break;
    case 120:
      HASH_RETURN(0x4DE3E5A930A830F8LL, g->GV(getsid),
                  getsid);
      break;
    case 124:
      HASH_RETURN(0x16F9AB1EABFCA77CLL, g->GV(getpwnam),
                  getpwnam);
      break;
    case 125:
      HASH_RETURN(0x2D4B2239845CC37DLL, g->GV(euid),
                  euid);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
