
#include <php/roadsend/tests/create-close.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$create_close_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/create-close.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$create_close_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_sock __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sock") : g->GV(sock);

  if (same(((v_sock = LINE(4,x_socket_create(toInt32(2LL) /* AF_INET */, toInt32(1LL) /* SOCK_STREAM */, toInt32(6LL) /* SOL_TCP */)))), false)) {
    echo(LINE(5,(assignCallTemp(eo_1, x_socket_strerror(x_socket_last_error())),concat3("socket_create() failed: reason: ", eo_1, "\n"))));
  }
  LINE(8,x_var_dump(1, v_sock));
  echo((LINE(10,x_socket_close(toObject(v_sock))), null));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
