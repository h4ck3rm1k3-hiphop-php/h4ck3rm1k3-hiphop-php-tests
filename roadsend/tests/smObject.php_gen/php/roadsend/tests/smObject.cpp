
#include <php/roadsend/tests/smObject.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$smObject_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/smObject.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$smObject_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  LINE(4,include("smErrors.inc", false, variables, "roadsend/tests/"));
  LINE(5,include("smObject.inc", false, variables, "roadsend/tests/"));
  (v_obj = LINE(7,create_object("sm_object", Array())));
  LINE(8,v_obj.o_invoke_few_args("debugLog", 0x603BE0E0675207ABLL, 1, "foo"));
  LINE(10,invoke_failed("sm_fatalerrorpage", Array(ArrayInit(1).set(0, "oops").create()), 0x000000009D452C39LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
