
#ifndef __GENERATED_php_roadsend_tests_bug_id_0000795_h__
#define __GENERATED_php_roadsend_tests_bug_id_0000795_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0000795.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0000795_php(bool incOnce = false, LVariableTable* variables = NULL);
int64 f_functest(CStrRef v_a, int64 v_b = -1LL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0000795_h__
