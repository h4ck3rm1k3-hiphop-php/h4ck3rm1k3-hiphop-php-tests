
#ifndef __GENERATED_php_roadsend_tests_zthis_h__
#define __GENERATED_php_roadsend_tests_zthis_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zthis.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$zthis_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_foo2(Variant v_foo);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zthis_h__
