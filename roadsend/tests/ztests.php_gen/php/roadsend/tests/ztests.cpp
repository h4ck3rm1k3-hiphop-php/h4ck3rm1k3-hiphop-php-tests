
#include <php/roadsend/tests/ztests.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_SQLITE_NUM = "SQLITE_NUM";
const StaticString k_SQLITE_BOTH = "SQLITE_BOTH";
const StaticString k_SQLITE_ASSOC = "SQLITE_ASSOC";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$ztests_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/ztests.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$ztests_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_strings __attribute__((__unused__)) = (variables != gVariables) ? variables->get("strings") : g->GV(strings);
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_errmsg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errmsg") : g->GV(errmsg);

  LINE(3,require("s_common.inc", false, variables, "roadsend/tests/"));
  (v_db = LINE(5,invoke_failed("makedb", Array(), 0x000000009D69F93DLL)));
  LINE(9,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE foo(c1 date, c2 time, c3 varchar(64))").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  LINE(10,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "INSERT INTO foo VALUES ('2002-01-02', '12:49:00', NULL)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  (v_r = LINE(11,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT * from foo").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LINE(12,x_var_dump(1, invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_BOTH).create()), 0x000000002867E087LL)));
  (v_r = LINE(13,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT * from foo").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LINE(14,x_var_dump(1, invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x000000002867E087LL)));
  (v_r = LINE(15,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT * from foo").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LINE(16,x_var_dump(1, invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_ASSOC).create()), 0x000000002867E087LL)));
  LINE(18,invoke_failed("sqlite_exec", Array(ArrayInit(2).set(0, "DROP TABLE foo").set(1, ref(v_db)).create()), 0x0000000046CD1877LL));
  (v_strings = (assignCallTemp(eo_1, LINE(24,(assignCallTemp(eo_6, x_chr(1LL)),concat3("hello", eo_6, "o")))),assignCallTemp(eo_2, concat(LINE(25,x_chr(1LL)), "hello there")),assignCallTemp(eo_3, LINE(26,(assignCallTemp(eo_6, x_chr(0LL)),concat3("hello", eo_6, "there")))),Array(ArrayInit(5).set(0, "hello").set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, "").create())));
  LINE(30,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE strings(a)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_strings.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_str = iter3->second();
      {
        LINE(33,(assignCallTemp(eo_0, (assignCallTemp(eo_3, toString(invoke_failed("sqlite_escape_string", Array(ArrayInit(1).set(0, ref(v_str)).create()), 0x00000000CAE267E2LL))),concat3("INSERT INTO strings VALUES('", eo_3, "')"))),assignCallTemp(eo_1, ref(v_db)),invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ADED2A6ALL)));
      }
    }
  }
  (v_i = 0LL);
  (v_r = LINE(37,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT * from strings").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LOOP_COUNTER(4);
  {
    while (toBoolean((v_row = LINE(38,invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x000000002867E087LL))))) {
      LOOP_COUNTER_CHECK(4);
      {
        if (!same(v_row.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_strings.rvalAt(v_i))) {
          echo("FAIL!\n");
          LINE(41,x_var_dump(1, v_row.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
          LINE(42,x_var_dump(1, v_strings.rvalAt(v_i)));
        }
        else {
          echo("OK!\n");
        }
        v_i++;
      }
    }
  }
  LINE(49,invoke_failed("sqlite_exec", Array(ArrayInit(2).set(0, "DROP TABLE strings").set(1, ref(v_db)).create()), 0x0000000046CD1877LL));
  LINE(54,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE foo(c1 date, c2 time, c3 varchar(64))").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  LINE(55,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "INSERT INTO foo VALUES ('2002-01-02', '12:49:00', NULL)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  (v_r = LINE(56,invoke_failed("sqlite_unbuffered_query", Array(ArrayInit(2).set(0, "SELECT * from foo").set(1, ref(v_db)).create()), 0x000000006FAA6ED4LL)));
  LINE(57,x_var_dump(1, invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_BOTH).create()), 0x000000002867E087LL)));
  (v_r = LINE(58,invoke_failed("sqlite_unbuffered_query", Array(ArrayInit(2).set(0, "SELECT * from foo").set(1, ref(v_db)).create()), 0x000000006FAA6ED4LL)));
  LINE(59,x_var_dump(1, invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x000000002867E087LL)));
  (v_r = LINE(60,invoke_failed("sqlite_unbuffered_query", Array(ArrayInit(2).set(0, "SELECT * from foo").set(1, ref(v_db)).create()), 0x000000006FAA6ED4LL)));
  LINE(61,x_var_dump(1, invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_ASSOC).create()), 0x000000002867E087LL)));
  LINE(63,invoke_failed("sqlite_exec", Array(ArrayInit(2).set(0, "DROP TABLE foo").set(1, ref(v_db)).create()), 0x0000000046CD1877LL));
  (v_data = ScalarArrays::sa_[0]);
  LINE(73,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE strings(a VARCHAR)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  {
    LOOP_COUNTER(5);
    for (ArrayIterPtr iter7 = v_data.begin(); !iter7->end(); iter7->next()) {
      LOOP_COUNTER_CHECK(5);
      v_str = iter7->second();
      {
        LINE(76,(assignCallTemp(eo_0, concat3("INSERT INTO strings VALUES('", toString(v_str), "')")),assignCallTemp(eo_1, ref(v_db)),invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ADED2A6ALL)));
      }
    }
  }
  (v_r = LINE(79,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT a from strings").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LOOP_COUNTER(8);
  {
    while (toBoolean((v_row = LINE(80,invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x000000002867E087LL))))) {
      LOOP_COUNTER_CHECK(8);
      {
        LINE(81,x_var_dump(1, v_row));
      }
    }
  }
  LINE(84,invoke_failed("sqlite_exec", Array(ArrayInit(2).set(0, "DROP TABLE strings").set(1, ref(v_db)).create()), 0x0000000046CD1877LL));
  LINE(89,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE strings(a VARCHAR)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  {
    LOOP_COUNTER(9);
    for (ArrayIterPtr iter11 = v_data.begin(); !iter11->end(); iter11->next()) {
      LOOP_COUNTER_CHECK(9);
      v_str = iter11->second();
      {
        LINE(92,(assignCallTemp(eo_0, concat3("INSERT INTO strings VALUES('", toString(v_str), "')")),assignCallTemp(eo_1, ref(v_db)),invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ADED2A6ALL)));
      }
    }
  }
  (v_r = LINE(95,invoke_failed("sqlite_unbuffered_query", Array(ArrayInit(2).set(0, "SELECT a from strings").set(1, ref(v_db)).create()), 0x000000006FAA6ED4LL)));
  LOOP_COUNTER(12);
  {
    while (toBoolean((v_row = LINE(96,invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x000000002867E087LL))))) {
      LOOP_COUNTER_CHECK(12);
      {
        LINE(97,x_var_dump(1, v_row));
      }
    }
  }
  LINE(100,invoke_failed("sqlite_exec", Array(ArrayInit(2).set(0, "DROP TABLE strings").set(1, ref(v_db)).create()), 0x0000000046CD1877LL));
  LINE(104,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE strings(a VARCHAR)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_data.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_str = iter15->second();
      {
        LINE(107,(assignCallTemp(eo_0, concat3("INSERT INTO strings VALUES('", toString(v_str), "')")),assignCallTemp(eo_1, ref(v_db)),invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ADED2A6ALL)));
      }
    }
  }
  (v_r = LINE(110,invoke_failed("sqlite_unbuffered_query", Array(ArrayInit(2).set(0, "SELECT a from strings").set(1, ref(v_db)).create()), 0x000000006FAA6ED4LL)));
  LOOP_COUNTER(16);
  {
    while (toBoolean(LINE(111,invoke_failed("sqlite_valid", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000021CBD4FLL)))) {
      LOOP_COUNTER_CHECK(16);
      {
        LINE(112,x_var_dump(1, invoke_failed("sqlite_current", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x00000000C82BD8C3LL)));
        LINE(113,invoke_failed("sqlite_next", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000009EB6E362LL));
      }
    }
  }
  (v_r = LINE(115,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT a from strings").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LOOP_COUNTER(17);
  {
    while (toBoolean(LINE(116,invoke_failed("sqlite_valid", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000021CBD4FLL)))) {
      LOOP_COUNTER_CHECK(17);
      {
        LINE(117,x_var_dump(1, invoke_failed("sqlite_current", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x00000000C82BD8C3LL)));
        LINE(118,invoke_failed("sqlite_next", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000009EB6E362LL));
      }
    }
  }
  LINE(120,invoke_failed("sqlite_rewind", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000576B4F50LL));
  LOOP_COUNTER(18);
  {
    while (toBoolean(LINE(121,invoke_failed("sqlite_valid", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000021CBD4FLL)))) {
      LOOP_COUNTER_CHECK(18);
      {
        LINE(122,x_var_dump(1, invoke_failed("sqlite_current", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x00000000C82BD8C3LL)));
        LINE(123,invoke_failed("sqlite_next", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000009EB6E362LL));
      }
    }
  }
  LINE(126,invoke_failed("sqlite_exec", Array(ArrayInit(2).set(0, "DROP TABLE strings").set(1, ref(v_db)).create()), 0x0000000046CD1877LL));
  LINE(130,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE strings(foo VARCHAR, bar VARCHAR, baz VARCHAR)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  echo("Buffered\n");
  (v_r = LINE(133,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT * from strings").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  echo(LINE(134,(assignCallTemp(eo_1, toString(invoke_failed("sqlite_num_fields", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000807B2163LL))),concat3("num fields: ", eo_1, "\n"))));
  {
    LOOP_COUNTER(19);
    for ((v_i = 0LL); less(v_i, LINE(135,invoke_failed("sqlite_num_fields", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000807B2163LL))); v_i++) {
      LOOP_COUNTER_CHECK(19);
      {
        LINE(136,x_var_dump(1, invoke_failed("sqlite_field_name", Array(ArrayInit(2).set(0, ref(v_r)).set(1, ref(v_i)).create()), 0x00000000B21B1902LL)));
      }
    }
  }
  echo("Unbuffered\n");
  (v_r = LINE(139,invoke_failed("sqlite_unbuffered_query", Array(ArrayInit(4).set(0, "SELECT * from strings").set(1, ref(v_db)).set(2, k_SQLITE_NUM).set(3, ref(v_errmsg)).create()), 0x000000006FAA6ED4LL)));
  if (!(toBoolean(v_r))) {
    LINE(141,x_var_dump(1, v_errmsg));
  }
  echo(LINE(143,(assignCallTemp(eo_1, toString(invoke_failed("sqlite_num_fields", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000807B2163LL))),concat3("num fields: ", eo_1, "\n"))));
  {
    LOOP_COUNTER(20);
    for ((v_i = 0LL); less(v_i, LINE(144,invoke_failed("sqlite_num_fields", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000807B2163LL))); v_i++) {
      LOOP_COUNTER_CHECK(20);
      {
        LINE(145,x_var_dump(1, invoke_failed("sqlite_field_name", Array(ArrayInit(2).set(0, ref(v_r)).set(1, ref(v_i)).create()), 0x00000000B21B1902LL)));
      }
    }
  }
  LINE(148,invoke_failed("sqlite_exec", Array(ArrayInit(2).set(0, "DROP TABLE strings").set(1, ref(v_db)).create()), 0x0000000046CD1877LL));
  (v_data = ScalarArrays::sa_[1]);
  LINE(157,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE strings(a VARCHAR, b VARCHAR)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  {
    LOOP_COUNTER(21);
    for (ArrayIterPtr iter23 = v_data.begin(); !iter23->end(); iter23->next()) {
      LOOP_COUNTER_CHECK(21);
      v_str = iter23->second();
      {
        LINE(160,(assignCallTemp(eo_0, concat5("INSERT INTO strings VALUES('", toString(variables->get(toString(v_str.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), "','", toString(variables->get(toString(v_str.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))), "')")),assignCallTemp(eo_1, ref(v_db)),invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ADED2A6ALL)));
      }
    }
  }
  echo("====BUFFERED====\n");
  (v_r = LINE(164,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT a, b from strings").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LOOP_COUNTER(24);
  {
    while (toBoolean(LINE(165,invoke_failed("sqlite_valid", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000021CBD4FLL)))) {
      LOOP_COUNTER_CHECK(24);
      {
        LINE(166,x_var_dump(1, invoke_failed("sqlite_current", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x00000000C82BD8C3LL)));
        LINE(167,x_var_dump(1, invoke_failed("sqlite_column", Array(ArrayInit(2).set(0, ref(v_r)).set(1, 0LL).create()), 0x00000000F001695DLL)));
        LINE(168,x_var_dump(1, invoke_failed("sqlite_column", Array(ArrayInit(2).set(0, ref(v_r)).set(1, 1LL).create()), 0x00000000F001695DLL)));
        LINE(169,x_var_dump(1, invoke_failed("sqlite_column", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "a").create()), 0x00000000F001695DLL)));
        LINE(170,x_var_dump(1, invoke_failed("sqlite_column", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "b").create()), 0x00000000F001695DLL)));
        LINE(171,invoke_failed("sqlite_next", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000009EB6E362LL));
      }
    }
  }
  (v_r = LINE(191,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
