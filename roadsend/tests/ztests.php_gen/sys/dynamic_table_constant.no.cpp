
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_SQLITE_NUM;
extern const StaticString k_SQLITE_BOTH;
extern const StaticString k_SQLITE_ASSOC;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x76A2DF9AE9162CC0LL, k_SQLITE_NUM, SQLITE_NUM);
      break;
    case 3:
      HASH_RETURN(0x5C67878392F8D0EBLL, k_SQLITE_BOTH, SQLITE_BOTH);
      break;
    case 6:
      HASH_RETURN(0x410E32C0F889A9FELL, k_SQLITE_ASSOC, SQLITE_ASSOC);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
