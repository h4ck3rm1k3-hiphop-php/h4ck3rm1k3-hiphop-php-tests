
#include <php/roadsend/tests/fsockopen.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/fsockopen.php line 5 */
Variant f_necho(int64 v_line_number, CVarRef v_string) {
  FUNCTION_INJECTION(necho);
  echo(LINE(6,concat4(toString(v_line_number), ": ", toString(v_string), "\n")));
  return v_string;
} /* function */
Variant pm_php$roadsend$tests$fsockopen_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/fsockopen.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$fsockopen_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_errno __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errno") : g->GV(errno);
  Variant &v_errstr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errstr") : g->GV(errstr);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);

  LINE(3,x_set_time_limit(toInt32(0LL)));
  (v_errno = 0LL);
  (v_errstr = "");
  (v_fp = LINE(12,x_fsockopen("www.google.com", toInt32(80LL), ref(v_errno), ref(v_errstr))));
  LINE(13,f_necho(10LL, v_errno));
  LINE(14,f_necho(20LL, v_errstr));
  LINE(15,(assignCallTemp(eo_1, x_fwrite(toObject(v_fp), "GET http://www.google.com/index.html\n")),f_necho(40LL, eo_1)));
  LINE(16,(assignCallTemp(eo_1, x_fflush(toObject(v_fp))),f_necho(50LL, eo_1)));
  LINE(17,(assignCallTemp(eo_1, x_fread(toObject(v_fp), 2048LL)),f_necho(60LL, eo_1)));
  (v_errno = 0LL);
  (v_errstr = "");
  (v_fp = LINE(21,x_fsockopen("smtp.roadsend.com", toInt32(25LL), ref(v_errno), ref(v_errstr))));
  LINE(22,f_necho(70LL, v_errno));
  LINE(23,f_necho(80LL, v_errstr));
  LINE(24,(assignCallTemp(eo_1, x_fwrite(toObject(v_fp), "HELO bob.com\n")),f_necho(100LL, eo_1)));
  LINE(25,(assignCallTemp(eo_1, x_fflush(toObject(v_fp))),f_necho(110LL, eo_1)));
  LINE(26,(assignCallTemp(eo_1, x_fread(toObject(v_fp), 80000LL)),f_necho(120LL, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
