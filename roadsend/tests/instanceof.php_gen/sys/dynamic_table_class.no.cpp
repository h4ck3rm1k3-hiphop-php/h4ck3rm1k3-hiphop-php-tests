
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_myclass2(CArrRef params, bool init = true);
Variant cw_myclass2$os_get(const char *s);
Variant &cw_myclass2$os_lval(const char *s);
Variant cw_myclass2$os_constant(const char *s);
Variant cw_myclass2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_myclass3(CArrRef params, bool init = true);
Variant cw_myclass3$os_get(const char *s);
Variant &cw_myclass3$os_lval(const char *s);
Variant cw_myclass3$os_constant(const char *s);
Variant cw_myclass3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_myclass4(CArrRef params, bool init = true);
Variant cw_myclass4$os_get(const char *s);
Variant &cw_myclass4$os_lval(const char *s);
Variant cw_myclass4$os_constant(const char *s);
Variant cw_myclass4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parentclass(CArrRef params, bool init = true);
Variant cw_parentclass$os_get(const char *s);
Variant &cw_parentclass$os_lval(const char *s);
Variant cw_parentclass$os_constant(const char *s);
Variant cw_parentclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_notmyclass(CArrRef params, bool init = true);
Variant cw_notmyclass$os_get(const char *s);
Variant &cw_notmyclass$os_lval(const char *s);
Variant cw_notmyclass$os_constant(const char *s);
Variant cw_notmyclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_myclass(CArrRef params, bool init = true);
Variant cw_myclass$os_get(const char *s);
Variant &cw_myclass$os_lval(const char *s);
Variant cw_myclass$os_constant(const char *s);
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_CREATE_OBJECT(0x63FAC858219AA7D0LL, myclass3);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x45C4D336A4FBEE92LL, parentclass);
      HASH_CREATE_OBJECT(0x124B454009539DD2LL, myclass);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x5309E620BA56E44ALL, notmyclass);
      break;
    case 11:
      HASH_CREATE_OBJECT(0x57D6A9DDA704A6FBLL, myclass2);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x4C7C3686EA47D2DCLL, myclass4);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x63FAC858219AA7D0LL, myclass3);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x45C4D336A4FBEE92LL, parentclass);
      HASH_INVOKE_STATIC_METHOD(0x124B454009539DD2LL, myclass);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x5309E620BA56E44ALL, notmyclass);
      break;
    case 11:
      HASH_INVOKE_STATIC_METHOD(0x57D6A9DDA704A6FBLL, myclass2);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x4C7C3686EA47D2DCLL, myclass4);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x63FAC858219AA7D0LL, myclass3);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x45C4D336A4FBEE92LL, parentclass);
      HASH_GET_STATIC_PROPERTY(0x124B454009539DD2LL, myclass);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x5309E620BA56E44ALL, notmyclass);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY(0x57D6A9DDA704A6FBLL, myclass2);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x4C7C3686EA47D2DCLL, myclass4);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x63FAC858219AA7D0LL, myclass3);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x45C4D336A4FBEE92LL, parentclass);
      HASH_GET_STATIC_PROPERTY_LV(0x124B454009539DD2LL, myclass);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x5309E620BA56E44ALL, notmyclass);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY_LV(0x57D6A9DDA704A6FBLL, myclass2);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x4C7C3686EA47D2DCLL, myclass4);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x63FAC858219AA7D0LL, myclass3);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x45C4D336A4FBEE92LL, parentclass);
      HASH_GET_CLASS_CONSTANT(0x124B454009539DD2LL, myclass);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x5309E620BA56E44ALL, notmyclass);
      break;
    case 11:
      HASH_GET_CLASS_CONSTANT(0x57D6A9DDA704A6FBLL, myclass2);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x4C7C3686EA47D2DCLL, myclass4);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
