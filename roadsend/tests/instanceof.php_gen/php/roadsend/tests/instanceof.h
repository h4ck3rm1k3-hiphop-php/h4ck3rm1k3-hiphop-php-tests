
#ifndef __GENERATED_php_roadsend_tests_instanceof_h__
#define __GENERATED_php_roadsend_tests_instanceof_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/instanceof.fw.h>

// Declarations
#include <cls/myinterface.h>
#include <cls/myclass2.h>
#include <cls/myclass3.h>
#include <cls/myclass4.h>
#include <cls/parentclass.h>
#include <cls/notmyclass.h>
#include <cls/myclass.h>
#include <cls/myinterface2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$instanceof_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_myclass2(CArrRef params, bool init = true);
Object co_myclass3(CArrRef params, bool init = true);
Object co_myclass4(CArrRef params, bool init = true);
Object co_parentclass(CArrRef params, bool init = true);
Object co_notmyclass(CArrRef params, bool init = true);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_instanceof_h__
