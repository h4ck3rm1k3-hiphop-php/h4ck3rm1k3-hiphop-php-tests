
#ifndef __GENERATED_php_roadsend_tests_instanceof_fw_h__
#define __GENERATED_php_roadsend_tests_instanceof_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(myinterface);
FORWARD_DECLARE_CLASS(myclass2)
FORWARD_DECLARE_CLASS(myclass3)
FORWARD_DECLARE_CLASS(myclass4)
FORWARD_DECLARE_CLASS(parentclass)
FORWARD_DECLARE_CLASS(notmyclass)
FORWARD_DECLARE_CLASS(myclass)
FORWARD_DECLARE_CLASS(myinterface2);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_instanceof_fw_h__
