
#ifndef __GENERATED_cls_myclass2_h__
#define __GENERATED_cls_myclass2_h__

#include <cls/parentclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/instanceof.php line 17 */
class c_myclass2 : virtual public c_parentclass {
  BEGIN_CLASS_MAP(myclass2)
    PARENT_CLASS(parentclass)
  END_CLASS_MAP(myclass2)
  DECLARE_CLASS(myclass2, MyClass2, parentclass)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass2_h__
