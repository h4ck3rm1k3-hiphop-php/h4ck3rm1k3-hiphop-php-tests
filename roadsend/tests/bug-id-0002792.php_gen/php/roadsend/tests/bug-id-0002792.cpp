
#include <php/roadsend/tests/bug-id-0002792.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0002792.php line 6 */
void f_loop() {
  FUNCTION_INJECTION(loop);
  DECLARE_GLOBAL_VARIABLES(g);
  lval(g->GV(foo))++;
  echo(concat(toString(g->GV(foo)), "\n"));
} /* function */
Variant pm_php$roadsend$tests$bug_id_0002792_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0002792.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0002792_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo(" ");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(17,f_loop());
      }
    }
  }
  echo("done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
