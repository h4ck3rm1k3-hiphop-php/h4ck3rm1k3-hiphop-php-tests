
#ifndef __GENERATED_php_roadsend_tests_ackermann_h__
#define __GENERATED_php_roadsend_tests_ackermann_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/ackermann.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$ackermann_php(bool incOnce = false, LVariableTable* variables = NULL);
Numeric f_ack(int64 v_m, Variant v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_ackermann_h__
