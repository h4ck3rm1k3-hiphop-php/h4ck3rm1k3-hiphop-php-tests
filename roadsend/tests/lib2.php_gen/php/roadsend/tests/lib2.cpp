
#include <php/roadsend/tests/lib2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/lib2.php line 8 */
void f_lib2func1(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(lib2func1);
  echo("this is a sample function\n");
} /* function */
/* SRC: roadsend/tests/lib2.php line 14 */
void f_lib2func2(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(lib2func2);
  echo("this is another sample function\n");
} /* function */
Variant pm_php$roadsend$tests$lib2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/lib2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$lib2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
