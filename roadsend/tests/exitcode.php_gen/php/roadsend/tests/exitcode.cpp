
#include <php/roadsend/tests/exitcode.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$exitcode_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/exitcode.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$exitcode_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_out __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out") : g->GV(out);

  LINE(2,x_system("sh -c 'exit 101'", ref(v_r)));
  echo(concat3("system: ", toString(v_r), "\n"));
  LINE(3,x_exec("sh -c 'exit 99'", ref(v_out), ref(v_r)));
  echo(concat3("exec: ", toString(v_r), "\n"));
  LINE(4,x_passthru("sh -c 'exit 76'", ref(v_r)));
  echo(concat3("passthru: ", toString(v_r), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
