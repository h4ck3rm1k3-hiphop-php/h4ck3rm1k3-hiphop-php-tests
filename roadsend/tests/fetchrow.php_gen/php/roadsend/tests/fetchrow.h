
#ifndef __GENERATED_php_roadsend_tests_fetchrow_h__
#define __GENERATED_php_roadsend_tests_fetchrow_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/fetchrow.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$fetchrow_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_fetchrow_h__
