
#include <php/roadsend/tests/bug-id-0001028.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_MY_CONST2 = "MY_CONST2";
const StaticString k_MY_CONST = "MY_CONST";
const StaticString k_SOME_CONSTANT = "SOME_CONSTANT";
const StaticString k_BLAH = "BLAH";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001028_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001028.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001028_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,g->declareConstant("CONST1", g->k_CONST1, 1LL));
  LINE(4,g->declareConstant("CONST2", g->k_CONST2, "test"));
  LINE(5,g->declareConstant("CONST2", g->k_CONST2, concat(toString(get_global_variables()->k_CONST2), "ing123")));
  LINE(6,g->declareConstant("CONST3", g->k_CONST3, concat(toString(get_global_variables()->k_CONST2), "ing123")));
  echo(k_SOME_CONSTANT);
  echo(toString(get_global_variables()->k_CONST1));
  echo(toString(get_global_variables()->k_CONST2));
  echo(toString(get_global_variables()->k_CONST3));
  echo(LINE(13,(assignCallTemp(eo_1, toString(x_defined("CONST1"))),concat3("defined1\? [", eo_1, "]\n"))));
  echo(LINE(14,(assignCallTemp(eo_1, toString(x_defined(toString(get_global_variables()->k_CONST1)))),concat3("defined1a\? [", eo_1, "]\n"))));
  echo(LINE(15,(assignCallTemp(eo_1, toString(x_defined(k_SOME_CONSTANT))),concat3("defined2\? [", eo_1, "]\n"))));
  echo("defined2a\? []\n");
  echo(LINE(17,(assignCallTemp(eo_1, toString(x_defined(toString(get_global_variables()->k_CONST3)))),concat3("defined3\? [", eo_1, "]\n"))));
  echo(LINE(18,(assignCallTemp(eo_1, toString(x_defined("CONST3"))),concat3("defined3a\? [", eo_1, "]\n"))));
  echo(LINE(19,(assignCallTemp(eo_1, toString(x_defined(k_BLAH))),concat3("defined4\? [", eo_1, "]\n"))));
  echo("test1\n");
  if (toBoolean(k_MY_CONST)) {
    echo("here meep\n");
  }
  echo("test2\n");
  if (equal(k_MY_CONST2, true)) {
    echo("there zang\n");
  }
  echo("test3\n");
  if (same(k_MY_CONST2, true)) {
    echo("there zang bazzle\n");
  }
  echo("test4\n");
  if (equal(k_MY_CONST2, 1LL)) {
    echo("there zang fizzle\n");
  }
  echo("test5\n");
  if (toBoolean(get_global_variables()->k_CONST1)) {
    echo("yay!\n");
  }
  echo("test6\n");
  {
    print("it was true\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
