
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_MY_CONST2;
extern const StaticString k_MY_CONST;
extern const StaticString k_SOME_CONSTANT;
extern const StaticString k_BLAH;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 15) {
    case 1:
      HASH_RETURN(0x44BA6995199752F1LL, k_MY_CONST2, MY_CONST2);
      break;
    case 4:
      HASH_RETURN(0x160AFB7952F4C584LL, g->k_CONST2, CONST2);
      break;
    case 10:
      HASH_RETURN(0x2A94B0C074C2CE3ALL, k_MY_CONST, MY_CONST);
      break;
    case 11:
      HASH_RETURN(0x5B4D8427D2208EFBLL, g->k_CONST1, CONST1);
      break;
    case 12:
      HASH_RETURN(0x64C1CD40FAFF19ACLL, k_SOME_CONSTANT, SOME_CONSTANT);
      break;
    case 13:
      HASH_RETURN(0x1A179982779CF0EDLL, g->k_CONST3, CONST3);
      HASH_RETURN(0x1477EA075A1A0ABDLL, k_BLAH, BLAH);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
