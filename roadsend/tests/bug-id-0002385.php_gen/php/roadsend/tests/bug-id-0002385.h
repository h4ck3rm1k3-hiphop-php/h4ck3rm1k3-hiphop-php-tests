
#ifndef __GENERATED_php_roadsend_tests_bug_id_0002385_h__
#define __GENERATED_php_roadsend_tests_bug_id_0002385_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0002385.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_m();
int64 f_n();
Variant pm_php$roadsend$tests$bug_id_0002385_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0002385_h__
