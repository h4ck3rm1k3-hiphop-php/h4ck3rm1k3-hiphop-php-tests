
#include <php/roadsend/tests/heapsort.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_IA = 3877LL;
const int64 k_IC = 29573LL;
const int64 k_IM = 139968LL;

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/heapsort.php line 11 */
Numeric f_gen_random(int64 v_n) {
  FUNCTION_INJECTION(gen_random);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_LAST __attribute__((__unused__)) = g->GV(LAST);
  return (divide((v_n * ((gv_LAST = modulo((toInt64(gv_LAST * 3877LL /* IA */ + 29573LL /* IC */)), 139968LL /* IM */)))), 139968LL /* IM */));
} /* function */
/* SRC: roadsend/tests/heapsort.php line 16 */
void f_heapsort(CVarRef v_n, Variant v_ra) {
  FUNCTION_INJECTION(heapsort);
  int64 v_l = 0;
  Variant v_ir;
  Variant v_rra;
  Numeric v_i = 0;
  Numeric v_j = 0;

  (v_l = (toInt64(toInt64(v_n)) >> 1LL) + 1LL);
  (v_ir = v_n);
  LOOP_COUNTER(1);
  {
    while (toBoolean(1LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        if (more(v_l, 1LL)) {
          (v_rra = v_ra.rvalAt(--v_l));
        }
        else {
          (v_rra = v_ra.rvalAt(v_ir));
          v_ra.set(v_ir, (v_ra.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
          if (equal(--v_ir, 1LL)) {
            v_ra.set(1LL, (v_rra), 0x5BCA7C69B794F8CELL);
            return;
          }
        }
        (v_i = v_l);
        (v_j = toInt64(v_l) << 1LL);
        LOOP_COUNTER(2);
        {
          while (not_more(v_j, v_ir)) {
            LOOP_COUNTER_CHECK(2);
            {
              if ((less(v_j, v_ir)) && (less(v_ra.rvalAt(v_j), v_ra.rvalAt(v_j + 1LL)))) {
                v_j++;
              }
              if (less(v_rra, v_ra.rvalAt(v_j))) {
                v_ra.set(v_i, (v_ra.rvalAt(v_j)));
                v_j += ((v_i = v_j));
              }
              else {
                (v_j = v_ir + 1LL);
              }
            }
          }
        }
        v_ra.set(v_i, (v_rra));
      }
    }
  }
} /* function */
Variant pm_php$roadsend$tests$heapsort_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/heapsort.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$heapsort_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_LAST __attribute__((__unused__)) = (variables != gVariables) ? variables->get("LAST") : g->GV(LAST);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_N __attribute__((__unused__)) = (variables != gVariables) ? variables->get("N") : g->GV(N);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_ary __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ary") : g->GV(ary);

  ;
  ;
  ;
  (v_LAST = 42LL);
  (v_N = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(10000LL)));
  {
    LOOP_COUNTER(3);
    for ((v_i = 1LL); not_more(v_i, v_N); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        v_ary.set(v_i, (LINE(52,f_gen_random(1LL))));
      }
    }
  }
  LINE(61,f_heapsort(v_N, ref(v_ary)));
  LINE(63,x_printf(2, "%.10f\n", Array(ArrayInit(1).set(0, v_ary.rvalAt(v_N)).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
