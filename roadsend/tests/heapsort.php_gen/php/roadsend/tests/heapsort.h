
#ifndef __GENERATED_php_roadsend_tests_heapsort_h__
#define __GENERATED_php_roadsend_tests_heapsort_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/heapsort.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$heapsort_php(bool incOnce = false, LVariableTable* variables = NULL);
Numeric f_gen_random(int64 v_n);
void f_heapsort(CVarRef v_n, Variant v_ra);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_heapsort_h__
