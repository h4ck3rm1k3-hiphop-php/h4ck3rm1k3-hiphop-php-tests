
#include <php/roadsend/tests/crypt-md5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/crypt-md5.php line 10 */
void f_p(CVarRef v_a) {
  FUNCTION_INJECTION(p);
  Variant eo_0;
  Variant eo_1;
  int64 v_i = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(11,x_strlen(toString(v_a)))); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(12,(assignCallTemp(eo_1, x_ord(toString(v_a.rvalAt(v_i)))),x_printf(2, "%x", Array(ArrayInit(1).set(0, eo_1).create()))));
      }
    }
  }
  echo("\n");
} /* function */
Variant pm_php$roadsend$tests$crypt_md5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/crypt-md5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$crypt_md5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo(LINE(3,(assignCallTemp(eo_1, x_md5("blork.nob-flanken-boop")),concat3("-[", eo_1, "]-"))));
  (v_c = "");
  (v_d = LINE(7,x_crypt("lady-reptile", "X4")));
  echo(LINE(8,concat4(toString(v_c), " - ", toString(v_d), "\n")));
  (v_a = LINE(17,x_sha1("foo", true)));
  LINE(18,f_p(v_a));
  (v_a = LINE(19,x_sha1("abc", true)));
  LINE(20,f_p(v_a));
  (v_a = LINE(21,x_sha1("this is a foobar", true)));
  LINE(22,f_p(v_a));
  {
    (v_a = LINE(24,x_sha1_file("/etc/passwd", true)));
    LINE(25,f_p(v_a));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
