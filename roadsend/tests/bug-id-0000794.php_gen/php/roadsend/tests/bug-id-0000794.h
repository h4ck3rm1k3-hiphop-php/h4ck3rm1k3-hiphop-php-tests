
#ifndef __GENERATED_php_roadsend_tests_bug_id_0000794_h__
#define __GENERATED_php_roadsend_tests_bug_id_0000794_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0000794.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0000794_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_functest(CVarRef v_a, CVarRef v_b, CVarRef v_c);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0000794_h__
