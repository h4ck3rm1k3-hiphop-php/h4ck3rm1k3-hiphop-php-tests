
#include <php/roadsend/tests/zsimple_switch.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$zsimple_switch_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zsimple_switch.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zsimple_switch_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 1LL);
  {
    switch (toInt64(v_a)) {
    case 0LL:
      {
        echo("bad");
        break;
      }
    case 1LL:
      {
        echo("good");
        break;
      }
    default:
      {
        echo("bad");
        break;
      }
    }
  }
  {
    switch (toInt64(v_a)) {
    case 0LL:
      {
        echo("bad");
        break;
      }
    case 1LL:
      {
        echo("good");
        break;
      }
    default:
      {
        echo("bad");
        break;
      }
    }
  }
  {
    switch (toInt64(v_a)) {
    case 0LL:
      {
        echo("bad");
        break;
      }
    case 1LL:
      {
        echo("good");
        break;
      }
    default:
      {
        echo("bad");
        break;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
