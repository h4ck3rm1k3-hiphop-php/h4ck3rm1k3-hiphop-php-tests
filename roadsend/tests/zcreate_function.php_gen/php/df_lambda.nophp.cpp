
#include <php/df_lambda.nophp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_b = "b";

/* preface starts */
/* preface finishes */
/* SRC: php$df_lambda line 2 */
String f_df_lambda_1(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(df_lambda_1);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  return LINE(2,(assignCallTemp(eo_1, toString(v_a)),assignCallTemp(eo_3, toString(v_b)),assignCallTemp(eo_5, toString(x_log(toDouble(v_a * v_b)))),concat6("ln(", eo_1, ") + ln(", eo_3, ") = ", eo_5)));
} /* function */
/* SRC: php$df_lambda line 3 */
String f_df_lambda_2(CVarRef v_x, CVarRef v_y) {
  FUNCTION_INJECTION(df_lambda_2);
  return concat("some trig: ", (toString(plus_rev(toDouble(v_x) * LINE(3,x_cos(toDouble(v_y))), x_sin(toDouble(v_x))))));
} /* function */
/* SRC: php$df_lambda line 4 */
String f_df_lambda_3(CVarRef v_x, CVarRef v_y) {
  FUNCTION_INJECTION(df_lambda_3);
  return concat("a hypotenuse: ", toString(LINE(4,x_sqrt(toDouble(v_x * v_x + v_y * v_y)))));
} /* function */
/* SRC: php$df_lambda line 5 */
Variant f_df_lambda_4(CVarRef v_b, CVarRef v_a) {
  FUNCTION_INJECTION(df_lambda_4);
  if (equal(LINE(5,x_strncmp(toString(v_a), toString(v_b), toInt32(3LL))), 0LL)) return concat5("** \"", toString(v_a), "\" and \"", toString(v_b), "\"\n** Look the same to me! (looking at the first 3 chars)");
  return null;
} /* function */
/* SRC: php$df_lambda line 6 */
String f_df_lambda_5(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(df_lambda_5);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return concat_rev(toString(LINE(6,x_crc32(k_b))), (assignCallTemp(eo_1, toString(x_crc32(toString(v_a)))),concat3("CRCs: ", eo_1, " , ")));
} /* function */
/* SRC: php$df_lambda line 7 */
String f_df_lambda_6(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(df_lambda_6);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_p;

  return LINE(7,(assignCallTemp(eo_1, toString(x_similar_text(toString(v_a), toString(v_b), ref(v_p)))),assignCallTemp(eo_2, concat3("(", toString(v_p), "%)")),concat3("similar(a,b) = ", eo_1, eo_2)));
} /* function */
/* SRC: php$df_lambda line 8 */
void f_df_lambda_7(Variant v_v, CVarRef v_k) {
  FUNCTION_INJECTION(df_lambda_7);
  (v_v = concat(toString(v_v), "mango"));
} /* function */
/* SRC: php$df_lambda line 9 */
Numeric f_df_lambda_8(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(df_lambda_8);
  return minus_rev(LINE(9,x_strlen(toString(v_a))), x_strlen(toString(v_b)));
} /* function */
Variant pm_php$df_lambda(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::df_lambda);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$df_lambda;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
