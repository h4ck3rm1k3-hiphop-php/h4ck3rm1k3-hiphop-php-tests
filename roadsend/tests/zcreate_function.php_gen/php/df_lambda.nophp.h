
#ifndef __GENERATED_php_df_lambda_nophp_h__
#define __GENERATED_php_df_lambda_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/df_lambda.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$df_lambda(bool incOnce = false, LVariableTable* variables = NULL);
String f_df_lambda_1(CVarRef v_a, CVarRef v_b);
String f_df_lambda_2(CVarRef v_x, CVarRef v_y);
String f_df_lambda_3(CVarRef v_x, CVarRef v_y);
Variant f_df_lambda_4(CVarRef v_b, CVarRef v_a);
String f_df_lambda_5(CVarRef v_a, CVarRef v_b);
String f_df_lambda_6(CVarRef v_a, CVarRef v_b);
void f_df_lambda_7(Variant v_v, CVarRef v_k);
Numeric f_df_lambda_8(CVarRef v_a, CVarRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_df_lambda_nophp_h__
