
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$tests$zcreate_function_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$df_lambda(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_INCLUDE(0x47AC8F7F7686FF0BLL, "roadsend/tests/zcreate_function.php", php$roadsend$tests$zcreate_function_php);
      HASH_INCLUDE(0x665D7AED97F307AFLL, "df_lambda", php$df_lambda);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
