
#ifndef __GENERATED_php_roadsend_tests_class_parent_h__
#define __GENERATED_php_roadsend_tests_class_parent_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/class-parent.fw.h>

// Declarations
#include <cls/foo.h>
#include <cls/bar.h>
#include <cls/baz.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$class_parent_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);
Object co_bar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_class_parent_h__
