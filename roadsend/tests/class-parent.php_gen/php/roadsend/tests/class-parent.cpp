
#include <php/roadsend/tests/class-parent.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/class-parent.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("zot", m_zot));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7A42596E78DEAC62LL, zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A42596E78DEAC62LL, m_zot,
                         zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7A42596E78DEAC62LL, m_zot,
                      zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::create() {
  init();
  t_foo();
  return this;
}
ObjectData *c_foo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_foo::dynConstruct(CArrRef params) {
  (t_foo());
}
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_zot = m_zot;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x496BBC806B3F6630LL, anothermethod) {
        int count = params.size();
        if (count <= 1) return (t_anothermethod(params.rvalAt(0)), null);
        return (t_anothermethod(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x496BBC806B3F6630LL, anothermethod) {
        if (count <= 1) return (t_anothermethod(a0), null);
        return (t_anothermethod(a0, a1), null);
      }
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_zot = 12LL;
}
/* SRC: roadsend/tests/class-parent.php line 7 */
void c_foo::t_foo() {
  INSTANCE_METHOD_INJECTION(foo, foo::foo);
  bool oldInCtor = gasInCtor(true);
  print("This is foo in foo.  Static method invocations can't count on properties, so no zot.\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/class-parent.php line 12 */
void c_foo::t_anothermethod(CVarRef v_a, CVarRef v_b //  = 32LL
) {
  INSTANCE_METHOD_INJECTION(foo, foo::anothermethod);
  print(LINE(13,concat5("foo->anothermethod: ", toString(v_a), " and ", toString(v_b), "\n")));
} /* function */
/* SRC: roadsend/tests/class-parent.php line 19 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_foo::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_foo::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("zot", m_zot));
  c_foo::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_foo::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_foo::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_foo::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_foo::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_foo::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::create() {
  init();
  t_bar();
  return this;
}
ObjectData *c_bar::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_bar::dynConstruct(CArrRef params) {
  (t_bar());
}
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  clone->m_zot = m_zot;
  c_foo::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x496BBC806B3F6630LL, anothermethod) {
        int count = params.size();
        if (count <= 1) return (t_anothermethod(params.rvalAt(0)), null);
        return (t_anothermethod(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 4:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      HASH_GUARD(0x6257FF850B48D36CLL, onemethod) {
        int count = params.size();
        if (count <= 1) return (t_onemethod(params.rvalAt(0)), null);
        return (t_onemethod(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x496BBC806B3F6630LL, anothermethod) {
        if (count <= 1) return (t_anothermethod(a0), null);
        return (t_anothermethod(a0, a1), null);
      }
      break;
    case 4:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      HASH_GUARD(0x6257FF850B48D36CLL, onemethod) {
        if (count <= 1) return (t_onemethod(a0), null);
        return (t_onemethod(a0, a1), null);
      }
      break;
    case 5:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_foo::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
  c_foo::init();
  m_zot = 14LL;
}
/* SRC: roadsend/tests/class-parent.php line 23 */
void c_bar::t_bar() {
  INSTANCE_METHOD_INJECTION(bar, bar::bar);
  bool oldInCtor = gasInCtor(true);
  LINE(24,c_foo::t_foo());
  LINE(25,c_foo::t_foo());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/class-parent.php line 28 */
void c_bar::t_onemethod(CVarRef v_a, CVarRef v_b //  = 32LL
) {
  INSTANCE_METHOD_INJECTION(bar, bar::onemethod);
  LINE(29,c_foo::t_anothermethod(1LL, 2LL));
  print(LINE(30,concat5("bar->onemethod: ", toString(v_a), " and ", toString(v_b), "\n")));
} /* function */
/* SRC: roadsend/tests/class-parent.php line 33 */
void c_bar::t_anothermethod(CVarRef v_a, CVarRef v_b //  = 32LL
) {
  INSTANCE_METHOD_INJECTION(bar, bar::anothermethod);
  LINE(34,c_foo::t_anothermethod(2LL, v_b));
  print(LINE(35,concat5("bar->anothermethod: ", toString(v_a), " and ", toString(v_b), "\n")));
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$class_parent_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/class-parent.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$class_parent_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  ((Object)(LINE(45,p_foo(p_foo(NEWOBJ(c_foo)())->create()))));
  (v_b = ((Object)(LINE(46,p_bar(p_bar(NEWOBJ(c_bar)())->create())))));
  LINE(49,v_b.o_invoke_few_args("onemethod", 0x6257FF850B48D36CLL, 1, 1LL));
  LINE(50,v_b.o_invoke_few_args("anothermethod", 0x496BBC806B3F6630LL, 1, 1LL));
  echo(concat(toString(LINE(52,x_get_class(v_b))), "\n"));
  echo(concat(toString(LINE(53,x_get_parent_class(v_b))), "\n"));
  echo(concat(toString(LINE(54,x_get_parent_class("bar"))), "\n"));
  echo(concat(toString(LINE(55,x_get_parent_class("stdClass"))), "\n"));
  echo(LINE(57,(assignCallTemp(eo_1, toString(x_is_subclass_of(v_b, "foo"))),concat3("subclass\? ", eo_1, "\n"))));
  echo(LINE(58,(assignCallTemp(eo_1, toString(x_is_subclass_of(v_a, "foo"))),concat3("subclass\? ", eo_1, "\n"))));
  echo(LINE(59,(assignCallTemp(eo_1, toString(x_is_subclass_of(v_a, "stdclass"))),concat3("subclass\? ", eo_1, "\n"))));
  echo(LINE(61,(assignCallTemp(eo_1, toString(x_is_subclass_of("bar", "foo"))),concat3("subclass\? ", eo_1, "\n"))));
  echo(LINE(63,(assignCallTemp(eo_1, toString(x_class_exists("bar", false))),concat3("class_exists\?", eo_1, "\n"))));
  echo(LINE(64,(assignCallTemp(eo_1, toString(x_class_exists("baz", false))),concat3("class_exists\?", eo_1, "\n"))));
  echo(LINE(65,(assignCallTemp(eo_1, toString(x_interface_exists("bar", false))),concat3("interface_exists\?", eo_1, "\n"))));
  echo(LINE(66,(assignCallTemp(eo_1, toString(x_interface_exists("baz", false))),concat3("interface_exists\?", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
