
#include <php/roadsend/tests/session3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/session3.php line 45 */
Variant f_destroy(CVarRef v_id) {
  FUNCTION_INJECTION(destroy);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_sess_save_path __attribute__((__unused__)) = g->GV(sess_save_path);
  Variant &gv_sess_session_name __attribute__((__unused__)) = g->GV(sess_session_name);
  String v_sess_file;

  {
  }
  echo("destroy\n");
  (v_sess_file = LINE(49,concat3(toString(gv_sess_save_path), "/sess_", toString(v_id))));
  return ((silenceInc(), silenceDec(LINE(50,x_unlink(v_sess_file)))));
} /* function */
/* SRC: roadsend/tests/session3.php line 57 */
bool f_gc(CVarRef v_maxlifetime) {
  FUNCTION_INJECTION(gc);
  echo("gc\n");
  return true;
} /* function */
/* SRC: roadsend/tests/session3.php line 32 */
Variant f_write(CVarRef v_id, CVarRef v_sess_data) {
  FUNCTION_INJECTION(write);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_sess_save_path __attribute__((__unused__)) = g->GV(sess_save_path);
  Variant &gv_sess_session_name __attribute__((__unused__)) = g->GV(sess_session_name);
  String v_sess_file;
  Variant v_fp;

  {
  }
  echo(LINE(35,concat3("write: ", toString(v_sess_data), "\n")));
  (v_sess_file = LINE(36,concat3(toString(gv_sess_save_path), "/sess_", toString(v_id))));
  if (toBoolean((v_fp = (silenceInc(), silenceDec(LINE(37,x_fopen(v_sess_file, "w"))))))) {
    return (LINE(38,x_fwrite(toObject(v_fp), toString(v_sess_data))));
  }
  else {
    return false;
  }
  return null;
} /* function */
/* SRC: roadsend/tests/session3.php line 12 */
bool f_close() {
  FUNCTION_INJECTION(close);
  echo("close\n");
  return true;
} /* function */
/* SRC: roadsend/tests/session3.php line 3 */
bool f_open(CVarRef v_save_path, CVarRef v_session_name) {
  FUNCTION_INJECTION(open);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_sess_save_path __attribute__((__unused__)) = g->GV(sess_save_path);
  Variant &gv_sess_session_name __attribute__((__unused__)) = g->GV(sess_session_name);
  {
  }
  echo(LINE(6,concat5("open: ", toString(v_save_path), ", ", toString(v_session_name), "\n")));
  (gv_sess_save_path = v_save_path);
  (gv_sess_session_name = v_session_name);
  return true;
} /* function */
/* SRC: roadsend/tests/session3.php line 18 */
Variant f_read(CVarRef v_id) {
  FUNCTION_INJECTION(read);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_sess_save_path __attribute__((__unused__)) = g->GV(sess_save_path);
  Variant &gv_sess_session_name __attribute__((__unused__)) = g->GV(sess_session_name);
  String v_sess_file;
  Variant v_fp;
  Variant v_sess_data;

  {
  }
  echo("read: \n");
  (v_sess_file = LINE(22,concat3(toString(gv_sess_save_path), "/sess_", toString(v_id))));
  if (toBoolean((v_fp = (silenceInc(), silenceDec(LINE(23,x_fopen(v_sess_file, "r"))))))) {
    (v_sess_data = LINE(24,(assignCallTemp(eo_0, toObject(v_fp)),assignCallTemp(eo_1, toInt64(x_filesize(v_sess_file))),x_fread(eo_0, eo_1))));
    return (v_sess_data);
  }
  else {
    return "";
  }
  return null;
} /* function */
Variant i_destroy(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x45CCFF10463AB662LL, destroy) {
    return (f_destroy(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_gc(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x577BBD06DD6388BCLL, gc) {
    return (f_gc(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_write(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x548D0802D1DB44ADLL, write) {
    return (f_write(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_close(CArrRef params) {
  return (f_close());
}
Variant i_open(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x21F68C010C124BC4LL, open) {
    return (f_open(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_read(CArrRef params) {
  return (f_read(params.rvalAt(0)));
}
Variant pm_php$roadsend$tests$session3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/session3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$session3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(63,invoke_failed("session_set_save_handler", Array(ArrayInit(6).set(0, "open").set(1, "close").set(2, "read").set(3, "write").set(4, "destroy").set(5, "gc").create()), 0x000000008A62506FLL));
  LINE(65,invoke_failed("session_start", Array(), 0x00000000FE2CE6F2LL));
  g->gv__SESSION.set("bork", ("zot"), 0x1E46B4C4E7D774CALL);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
