
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_close(CArrRef params);
Variant i_read(CArrRef params);
static Variant invoke_case_1(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x78AE97BFBEBF5341LL, close);
  HASH_INVOKE(0x1F479267E49EF301LL, read);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_destroy(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_open(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_gc(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_write(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[16])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 16; i++) funcTable[i] = &invoke_builtin;
    funcTable[1] = &invoke_case_1;
    funcTable[2] = &i_destroy;
    funcTable[4] = &i_open;
    funcTable[12] = &i_gc;
    funcTable[13] = &i_write;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 15](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
