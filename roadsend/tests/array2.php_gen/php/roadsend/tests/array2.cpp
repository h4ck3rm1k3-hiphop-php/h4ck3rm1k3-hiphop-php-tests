
#include <php/roadsend/tests/array2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$array2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/array2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$array2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_number __attribute__((__unused__)) = (variables != gVariables) ? variables->get("number") : g->GV(number);
  Variant &v_letter __attribute__((__unused__)) = (variables != gVariables) ? variables->get("letter") : g->GV(letter);
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_output __attribute__((__unused__)) = (variables != gVariables) ? variables->get("output") : g->GV(output);
  Variant &v_os __attribute__((__unused__)) = (variables != gVariables) ? variables->get("os") : g->GV(os);
  Variant &v_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array") : g->GV(array);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_result_keyed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result_keyed") : g->GV(result_keyed);
  Variant &v_stack __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stack") : g->GV(stack);
  Variant &v_fruit __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fruit") : g->GV(fruit);
  Variant &v_queue __attribute__((__unused__)) = (variables != gVariables) ? variables->get("queue") : g->GV(queue);
  Variant &v_fruits __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fruits") : g->GV(fruits);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  (v_a = LINE(4,x_range(0LL, 12LL)));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_number = iter3->second();
      {
        echo(toString(v_number));
      }
    }
  }
  (v_a = LINE(8,x_range("a", "i")));
  LINE(9,x_var_dump(1, v_a));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_letter = iter6->second();
      {
        echo(toString(v_letter));
      }
    }
  }
  (v_a = LINE(14,x_range("c", "a")));
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_a.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_letter = iter9->second();
      {
        echo(toString(v_letter));
      }
    }
  }
  echo("array_splice:\n");
  (v_input = ScalarArrays::sa_[0]);
  (v_output = LINE(22,x_array_slice(v_input, toInt32(2LL))));
  LINE(23,x_var_dump(1, v_output));
  (v_output = LINE(24,x_array_slice(v_input, toInt32(2LL), -1LL)));
  LINE(25,x_var_dump(1, v_output));
  (v_output = LINE(26,x_array_slice(v_input, toInt32(-2LL), 1LL)));
  LINE(27,x_var_dump(1, v_output));
  (v_output = LINE(28,x_array_slice(v_input, toInt32(0LL), 3LL)));
  LINE(29,x_var_dump(1, v_output));
  (v_output = LINE(30,x_array_slice(v_input, toInt32(4LL))));
  LINE(31,x_var_dump(1, v_output));
  (v_os = ScalarArrays::sa_[1]);
  if (LINE(36,x_in_array("Irix", v_os))) {
    print("Got Irix");
  }
  if (LINE(39,x_in_array("mac", v_os))) {
    print("Got mac");
  }
  if (LINE(42,x_in_array(55LL, v_os, true))) {
    print("strict works");
  }
  if (LINE(45,x_in_array("55", v_os, true))) {
    print("strict doesn't work");
  }
  (v_array = ScalarArrays::sa_[2]);
  LINE(51,x_print_r(x_array_values(v_array)));
  LINE(54,x_reset(ref(v_os)));
  print(toString(LINE(55,x_current(ref(v_os)))));
  print(toString(LINE(56,x_key(ref(v_os)))));
  LINE(57,x_next(ref(v_os)));
  print(toString(LINE(58,x_pos(ref(v_os)))));
  print(toString(LINE(59,x_key(ref(v_os)))));
  print(toString(LINE(60,x_prev(ref(v_os)))));
  print(toString(LINE(61,x_pos(ref(v_os)))));
  print(toString(LINE(62,x_key(ref(v_os)))));
  print(toString(LINE(63,x_end(ref(v_os)))));
  print(toString(LINE(64,x_current(ref(v_os)))));
  print(toString(LINE(65,x_key(ref(v_os)))));
  LOOP_COUNTER(10);
  {
    while (toBoolean((v_val = LINE(66,x_prev(ref(v_os)))))) {
      LOOP_COUNTER_CHECK(10);
      {
        echo(LINE(67,concat3("prev check: ", toString(v_val), "\n")));
      }
    }
  }
  (v_input = ScalarArrays::sa_[3]);
  (v_result = LINE(72,x_array_reverse(v_input)));
  (v_result_keyed = LINE(73,x_array_reverse(v_input, true)));
  LINE(74,x_var_dump(1, v_result));
  LINE(75,x_var_dump(1, v_result_keyed));
  (v_stack = ScalarArrays::sa_[4]);
  (v_fruit = LINE(79,x_array_pop(ref(v_stack))));
  LINE(80,x_print_r(v_stack));
  echo(toString(v_fruit));
  (v_stack = ScalarArrays::sa_[4]);
  (v_fruit = LINE(85,x_array_shift(ref(v_stack))));
  LINE(86,x_print_r(v_stack));
  echo(toString(v_fruit));
  (v_stack = ScalarArrays::sa_[5]);
  (v_fruit = LINE(90,x_array_shift(ref(v_stack))));
  LINE(91,x_print_r(v_stack));
  echo(toString(v_fruit));
  (v_queue = ScalarArrays::sa_[6]);
  LINE(96,x_array_unshift(3, ref(v_queue), "apple", ScalarArrays::sa_[7]));
  LINE(97,x_var_dump(1, v_queue));
  (v_queue = ScalarArrays::sa_[8]);
  LINE(99,x_array_unshift(3, ref(v_queue), "apple", ScalarArrays::sa_[7]));
  LINE(100,x_var_dump(1, v_queue));
  (v_queue = ScalarArrays::sa_[8]);
  LINE(102,x_array_unshift(4, ref(v_queue), "apple", ScalarArrays::sa_[10]));
  LINE(103,x_var_dump(1, v_queue));
  (v_fruits = ScalarArrays::sa_[11]);
  (v_v = LINE(107,x_array_search("a", v_fruits)));
  LINE(108,x_var_dump(1, v_v));
  (v_v = LINE(109,x_array_search("5", v_fruits)));
  LINE(110,x_var_dump(1, v_v));
  (v_v = LINE(111,x_array_search("5", v_fruits, true)));
  LINE(112,x_var_dump(1, v_v));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
