
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_RETURN(0x35D32B9EF30B4340LL, g->GV(fruits),
                  fruits);
      break;
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 2:
      HASH_RETURN(0x4056C2E766E0B782LL, g->GV(val),
                  val);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      HASH_RETURN(0x2E2E8BE57817A743LL, g->GV(number),
                  number);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 8:
      HASH_RETURN(0x599E3F67E2599248LL, g->GV(result),
                  result);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 10:
      HASH_RETURN(0x4292CEE227B9150ALL, g->GV(a),
                  a);
      break;
    case 11:
      HASH_RETURN(0x1532499416EFFACBLL, g->GV(input),
                  input);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 15:
      HASH_RETURN(0x46569A2485D3980FLL, g->GV(queue),
                  queue);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x41B0F2E9F44D4E53LL, g->GV(fruit),
                  fruit);
      break;
    case 23:
      HASH_RETURN(0x7177021A8C1BED17LL, g->GV(result_keyed),
                  result_keyed);
      break;
    case 32:
      HASH_RETURN(0x063D35168C04B960LL, g->GV(array),
                  array);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 36:
      HASH_RETURN(0x6F443E367A5DBB24LL, g->GV(output),
                  output);
      break;
    case 43:
      HASH_RETURN(0x5084E637B870262BLL, g->GV(stack),
                  stack);
      HASH_RETURN(0x3C2F961831E4EF6BLL, g->GV(v),
                  v);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 53:
      HASH_RETURN(0x2DEE9FD2A3725E35LL, g->GV(letter),
                  letter);
      HASH_RETURN(0x3AA4D0F56E8340F5LL, g->GV(os),
                  os);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
