
#include <php/roadsend/tests/float-output.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$float_output_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/float-output.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$float_output_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);

  v_a.append((1.2));
  v_a.append((1.23));
  v_a.append((1.234));
  v_a.append((1.2344999999999999));
  v_a.append((1.2345600000000001));
  v_a.append((1.234567));
  v_a.append((1.2345678));
  v_a.append((1.2345678899999999));
  v_a.append((1.2345678899999999));
  v_a.append((1.2345678900999999));
  v_a.append((1.2345678901199999));
  v_a.append((1.2345678901229999));
  v_a.append((1.2345678901234001));
  v_a.append((1.23456789012345));
  v_a.append((1.234567890123456));
  v_a.append((1.2345678901234567));
  v_a.append((1.2345678901234567));
  v_a.append((1.2345678901234567));
  v_a.append((1.2345678901234567));
  v_a.append((12.234567890123456));
  v_a.append((123.23456789012346));
  v_a.append((1234.2345678901233));
  v_a.append((12345.234567890124));
  v_a.append((123456.23456789012));
  v_a.append((1234567.2345678902));
  v_a.append((12345678.23456789));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((1.3));
  v_a.append((130.0));
  v_a.append((1300.0));
  v_a.append((13000.0));
  v_a.append((130000.0));
  v_a.append((1.3E+6));
  v_a.append((1.3E+7));
  v_a.append((1.3E+8));
  v_a.append((1.3E+9));
  v_a.append((1.3E+10));
  v_a.append((1.3E+11));
  v_a.append((1.3E+12));
  v_a.append((1.0));
  v_a.append((12.0));
  v_a.append((123.0));
  v_a.append((1234.0));
  v_a.append((12345.0));
  v_a.append((123456.0));
  v_a.append((1234567.0));
  v_a.append((12345678.0));
  v_a.append((123456789.0));
  v_a.append((1234567890.0));
  v_a.append((12345678901.0));
  v_a.append((123456789012.0));
  v_a.append((1235567890123.0));
  v_a.append((12345678901234.0));
  v_a.append((0.69999999999999996));
  v_a.append((0.070000000000000007));
  v_a.append((0.0070000000000000001));
  v_a.append((0.00069999999999999999));
  v_a.append((6.9999999999999994E-5));
  v_a.append((6.9999999999999999E-6));
  v_a.append((6.9999999999999997E-7));
  v_a.append((7.0000000000000005E-8));
  v_a.append((6.9999999999999998E-9));
  v_a.append((6.9999999999999996E-10));
  v_a.append((7.0000000000000004E-11));
  v_a.append((7.0000000000000001E-12));
  v_a.append((7.0000000000000005E-13));
  v_a.append((-0.69999999999999996));
  v_a.append((-0.070000000000000007));
  v_a.append((-0.0070000000000000001));
  v_a.append((-0.00069999999999999999));
  v_a.append((-6.9999999999999994E-5));
  v_a.append((-6.9999999999999999E-6));
  v_a.append((-6.9999999999999997E-7));
  v_a.append((-7.0000000000000005E-8));
  v_a.append((-6.9999999999999998E-9));
  v_a.append((-6.9999999999999996E-10));
  v_a.append((-7.0000000000000004E-11));
  v_a.append((-7.0000000000000001E-12));
  v_a.append((-7.0000000000000005E-13));
  v_a.append((-30.0));
  v_a.append((-300.0));
  v_a.append((-3000.0));
  v_a.append((-30000.0));
  v_a.append((-3.0E+5));
  v_a.append((-3.0E+6));
  v_a.append((-3.0E+7));
  v_a.append((-3.0E+8));
  v_a.append((-3.0E+9));
  v_a.append((-3.0E+10));
  v_a.append((-3.0E+11));
  v_a.append((-3.0E+12));
  v_a.append((1230.0));
  v_a.append((12300.0));
  v_a.append((123000.0));
  v_a.append((1230000.0));
  v_a.append((1.23E+7));
  v_a.append((1.23E+8));
  v_a.append((1.23E+9));
  v_a.append((1.23E+10));
  v_a.append((1.23E+11));
  v_a.append((1.23E+12));
  v_a.append((1.23E+13));
  v_a.append((1.23E+14));
  v_a.append((1.234E+14));
  v_a.append((1.23E+15));
  v_a.append((1.23E+16));
  v_a.append((1.23E+17));
  v_a.append((1000000000000LL));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_f = iter3->second();
      {
        LINE(154,x_var_dump(1, v_f));
        echo((LINE(155,concat3("echo: ", toString(v_f), "\n"))));
        LINE(156,x_printf(2, "printf(f): %f\n", Array(ArrayInit(1).set(0, v_f).create())));
        LINE(157,x_printf(2, "printf(e): %e\n", Array(ArrayInit(1).set(0, v_f).create())));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
