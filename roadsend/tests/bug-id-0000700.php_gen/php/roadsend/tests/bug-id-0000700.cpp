
#include <php/roadsend/tests/bug-id-0000700.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000700.php line 9 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("attributes", m_attributes));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x3E5975BA0FC37E03LL, attributes, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3E5975BA0FC37E03LL, m_attributes,
                         attributes, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x3E5975BA0FC37E03LL, m_attributes,
                      attributes, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_attributes = m_attributes;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x05A26B348541B72DLL, zot) {
        return (t_zot(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x05A26B348541B72DLL, zot) {
        return (t_zot(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_attributes = ScalarArrays::sa_[0];
}
/* SRC: roadsend/tests/bug-id-0000700.php line 13 */
void c_foo::t_zot() {
  INSTANCE_METHOD_INJECTION(foo, foo::zot);
  Primitive v_attrKey = 0;
  Variant v_attrVal;

  {
    LOOP_COUNTER(1);
    Variant map2 = m_attributes;
    for (ArrayIterPtr iter3 = map2.begin("foo"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_attrVal = iter3->second();
      v_attrKey = iter3->first();
      {
        print(LINE(16,concat4(toString(v_attrKey), ", ", toString(v_attrVal), "\n")));
      }
    }
  }
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000700_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000700.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000700_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_afoo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("afoo") : g->GV(afoo);

  echo("unable to foreach on a class variable\nparse error on this code in a class method:\n\nforeach ($this->attributes as $attrKey => $attrVal) {\n\n");
  (v_afoo = ((Object)(LINE(24,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(25,v_afoo.o_invoke_few_args("zot", 0x05A26B348541B72DLL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
