
#ifndef __GENERATED_php_roadsend_tests_varvararg_h__
#define __GENERATED_php_roadsend_tests_varvararg_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/varvararg.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_zot(String v_arg3);
Variant pm_php$roadsend$tests$varvararg_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_foo(Variant v_arg1);
void f_bar(Variant v_arg2);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_varvararg_h__
