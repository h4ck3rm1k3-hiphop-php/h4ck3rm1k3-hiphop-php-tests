
#ifndef __GENERATED_cls_yfunctionmenu_h__
#define __GENERATED_cls_yfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/inc12.php line 22 */
class c_yfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(yfunctionmenu)
  END_CLASS_MAP(yfunctionmenu)
  DECLARE_CLASS(yfunctionmenu, yFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_yfunctionmenu_h__
