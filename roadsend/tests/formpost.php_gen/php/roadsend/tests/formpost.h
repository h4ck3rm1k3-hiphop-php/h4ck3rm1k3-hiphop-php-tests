
#ifndef __GENERATED_php_roadsend_tests_formpost_h__
#define __GENERATED_php_roadsend_tests_formpost_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/formpost.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$formpost_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_encodepostarr(CVarRef v_arr);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_formpost_h__
