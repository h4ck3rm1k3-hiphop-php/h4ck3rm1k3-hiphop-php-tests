
#include <php/roadsend/tests/list_dbs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$list_dbs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/list_dbs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$list_dbs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_dbh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dbh") : g->GV(dbh);
  Variant &v_db_list __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_list") : g->GV(db_list);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_rr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rr") : g->GV(rr);

  (v_dbh = LINE(3,x_mysql_connect("localhost", "develUser", "d3v3lpa55")));
  (v_db_list = LINE(4,x_mysql_list_dbs(v_dbh)));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_row = LINE(6,x_mysql_fetch_object(v_db_list))))) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(concat(toString(v_row.o_get("Database", 0x4DEA76AC78F0A51FLL)), "\n"));
        if (toBoolean(LINE(8,x_mysql_select_db(toString(v_row.o_get("Database", 0x4DEA76AC78F0A51FLL)))))) {
          echo(LINE(9,concat3("I have access to ", toString(v_row.o_get("Database", 0x4DEA76AC78F0A51FLL)), "\n")));
        }
        (v_rh = LINE(11,x_mysql_list_tables(toString(v_row.o_get("Database", 0x4DEA76AC78F0A51FLL)))));
        (v_e = LINE(12,x_mysql_error()));
        LINE(13,x_var_dump(1, v_e));
        LOOP_COUNTER(2);
        {
          while (toBoolean((v_rr = LINE(14,x_mysql_fetch_array(v_rh))))) {
            LOOP_COUNTER_CHECK(2);
            {
              LINE(15,x_var_dump(1, v_rr));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
