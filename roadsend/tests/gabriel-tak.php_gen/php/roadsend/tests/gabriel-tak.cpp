
#include <php/roadsend/tests/gabriel-tak.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/gabriel-tak.php line 16 */
Variant f_tak(CVarRef v_x, CVarRef v_y, CVarRef v_z) {
  FUNCTION_INJECTION(tak);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  if (!((less(v_y, v_x)))) {
    return v_z;
  }
  else {
    return LINE(22,(assignCallTemp(eo_0, LINE(20,f_tak(v_x - 1LL, v_y, v_z))),assignCallTemp(eo_1, LINE(21,f_tak(v_y - 1LL, v_z, v_x))),assignCallTemp(eo_2, LINE(22,f_tak(v_z - 1LL, v_x, v_y))),f_tak(eo_0, eo_1, eo_2)));
  }
  return null;
} /* function */
Variant pm_php$roadsend$tests$gabriel_tak_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/gabriel-tak.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$gabriel_tak_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("From \"Performance and Evaluation of Lisp Systems\" by Richard Gabriel \npg. 82\n\n\n\"The Tak benchmark is a variant of the Takeuchi function that Ikuo\nTakeuchi of Japan used as a simple benchmark.  Because Tak is\nfunction-call-heavy, it is representative of many Lisp programs.  On\nthe other hand, because it does little else but function calls (fixnum\narithmetic is performed as well), it is not represen");
  echo("tative of the\nmajority of Lisp programs.  It is only a good test of function call\nand recursion, in particular.\"\n\n\n");
  print(LINE(26,(assignCallTemp(eo_1, toString(f_tak(18LL, 12LL, 6LL))),concat3("result: ", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
