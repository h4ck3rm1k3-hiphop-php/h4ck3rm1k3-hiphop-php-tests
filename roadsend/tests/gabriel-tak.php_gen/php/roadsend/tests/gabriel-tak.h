
#ifndef __GENERATED_php_roadsend_tests_gabriel_tak_h__
#define __GENERATED_php_roadsend_tests_gabriel_tak_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/gabriel-tak.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_tak(CVarRef v_x, CVarRef v_y, CVarRef v_z);
Variant pm_php$roadsend$tests$gabriel_tak_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_gabriel_tak_h__
