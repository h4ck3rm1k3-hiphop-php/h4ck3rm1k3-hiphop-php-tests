
#ifndef __GENERATED_php_roadsend_tests_singleton_h__
#define __GENERATED_php_roadsend_tests_singleton_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/singleton.fw.h>

// Declarations
#include <cls/example.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$singleton_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_example(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_singleton_h__
