
#ifndef __GENERATED_cls_example_h__
#define __GENERATED_cls_example_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/singleton.php line 3 */
class c_example : virtual public ObjectData {
  BEGIN_CLASS_MAP(example)
  END_CLASS_MAP(example)
  DECLARE_CLASS(example, Example, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static Variant ti_singleton(const char* cls);
  public: void t_bark();
  public: Variant t___clone();
  public: static Variant t_singleton() { return ti_singleton("example"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_example_h__
