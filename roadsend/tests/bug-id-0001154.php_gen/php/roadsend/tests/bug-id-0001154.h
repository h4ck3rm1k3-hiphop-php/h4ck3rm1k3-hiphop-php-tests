
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001154_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001154_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001154.fw.h>

// Declarations
#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001154_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_test(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001154_h__
