
#include <php/roadsend/tests/callbacks.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/callbacks.php line 40 */
int f_read_header(CVarRef v_ch, CVarRef v_string) {
  FUNCTION_INJECTION(read_header);
  int v_length = 0;

  (v_length = LINE(42,x_strlen(toString(v_string))));
  echo(LINE(43,concat3("Header: ", toString(v_string), "<br />\n")));
  return v_length;
} /* function */
/* SRC: roadsend/tests/callbacks.php line 51 */
int f_read_body(CVarRef v_ch, CVarRef v_string) {
  FUNCTION_INJECTION(read_body);
  int v_length = 0;

  (v_length = LINE(53,x_strlen(toString(v_string))));
  echo(LINE(54,concat3("Received ", toString(v_length), " bytes<br />\n")));
  return v_length;
} /* function */
Variant i_read_header(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0E619AD0080C9003LL, read_header) {
    return (f_read_header(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_read_body(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x5F13090AD7617CA5LL, read_body) {
    return (f_read_body(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$callbacks_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/callbacks.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$callbacks_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ch") : g->GV(ch);
  Variant &v_error __attribute__((__unused__)) = (variables != gVariables) ? variables->get("error") : g->GV(error);

  (v_ch = LINE(20,x_curl_init()));
  LINE(22,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "http://www.php.net/"));
  LINE(24,x_curl_setopt(toObject(v_ch), toInt32(20079LL) /* CURLOPT_HEADERFUNCTION */, "read_header"));
  LINE(26,x_curl_setopt(toObject(v_ch), toInt32(20011LL) /* CURLOPT_WRITEFUNCTION */, "read_body"));
  LINE(28,x_curl_exec(toObject(v_ch)));
  if (toBoolean((v_error = LINE(30,x_curl_error(toObject(v_ch)))))) {
    echo(LINE(31,concat3("Error: ", toString(v_error), "<br />\n")));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
