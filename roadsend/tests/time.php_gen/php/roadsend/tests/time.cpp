
#include <php/roadsend/tests/time.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$time_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/time.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$time_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_today __attribute__((__unused__)) = (variables != gVariables) ? variables->get("today") : g->GV(today);
  Variant &v_lt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lt") : g->GV(lt);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  echo(LINE(8,(assignCallTemp(eo_1, toString(x_date_default_timezone_set("America/New_York"))),concat3("timezone 1: ", eo_1, "\n"))));
  echo(LINE(9,(assignCallTemp(eo_1, x_date_default_timezone_get()),concat3("timezone 2: ", eo_1, "\n"))));
  echo(LINE(10,(assignCallTemp(eo_1, x_date_default_timezone_get()),concat3("timezone 3: ", eo_1, "\n"))));
  echo(LINE(13,(assignCallTemp(eo_1, toString(x_checkdate(toInt32(0LL), toInt32(0LL), toInt32(0LL)))),concat3("checkdate 1:", eo_1, "\n"))));
  echo(LINE(14,(assignCallTemp(eo_1, toString(x_checkdate(toInt32(1LL), toInt32(1LL), toInt32(1970LL)))),concat3("checkdate 2:", eo_1, "\n"))));
  echo(LINE(15,(assignCallTemp(eo_1, toString(x_checkdate(toInt32(2LL), toInt32(12LL), toInt32(1978LL)))),concat3("checkdate 3:", eo_1, "\n"))));
  echo(LINE(16,(assignCallTemp(eo_1, toString(x_checkdate(toInt32("2"), toInt32("12"), toInt32("1978")))),concat3("checkdate 4:", eo_1, "\n"))));
  echo(LINE(17,(assignCallTemp(eo_1, toString(x_checkdate(toInt32(2LL), toInt32(29LL), toInt32(2004LL)))),concat3("checkdate 5:", eo_1, "\n"))));
  echo(LINE(18,(assignCallTemp(eo_1, toString(x_checkdate(toInt32(2LL), toInt32(29LL), toInt32(2003LL)))),concat3("checkdate 6:", eo_1, "\n"))));
  echo(LINE(19,(assignCallTemp(eo_1, toString((silenceInc(), silenceDec(toString(x_checkdate(toInt32("hello"), toInt32("hi there"), toInt32("boing"))))))),concat3("checkdate 7:", eo_1, "\n"))));
  echo(LINE(22,(assignCallTemp(eo_1, toString((silenceInc(), silenceDec(toString(x_checkdate(toInt32("6 days"), toInt32("3 months"), toInt32("2003"))))))),concat3("checkdate 8:", eo_1, "\n"))));
  echo(LINE(23,(assignCallTemp(eo_1, toString((silenceInc(), silenceDec(toString(x_checkdate(toInt32("6 days"), toInt32("3 months"), toInt32(2003LL))))))),concat3("checkdate 9:", eo_1, "\n"))));
  echo(LINE(24,(assignCallTemp(eo_1, toString(x_checkdate(toInt32("6"), toInt32("3"), toInt32("2003")))),concat3("checkdate 10:", eo_1, "\n"))));
  echo(LINE(25,(assignCallTemp(eo_1, toString(x_checkdate(toInt32(6LL), toInt32(3LL), toInt32(2003LL)))),concat3("checkdate 11:", eo_1, "\n"))));
  echo(LINE(28,(assignCallTemp(eo_1, x_date("l \\t\\h\\e jS")),concat3("date 1: ", eo_1, "\n"))));
  echo(LINE(29,(assignCallTemp(eo_1, x_date("l \\t\\h\\e jS\\")),concat3("date 2: ", eo_1, "\n"))));
  echo(LINE(31,(assignCallTemp(eo_1, x_date("[  a  ]")),concat3("date 3: ", eo_1, "\n"))));
  echo(LINE(32,(assignCallTemp(eo_1, x_date("l dS of F Y h:i A e")),concat3("*date 4: ", eo_1, "\n"))));
  echo(LINE(33,(assignCallTemp(eo_1, x_date("l dS of F Y h:i A", 1066316052LL)),concat3("date 4a: ", eo_1, "\n"))));
  echo(LINE(34,(assignCallTemp(eo_1, x_date("l \\t\\h\\e jS")),concat3("date 5: ", eo_1, "\n"))));
  echo(LINE(35,(assignCallTemp(eo_1, x_date("F j, Y, g:i a")),concat3("date 6: ", eo_1, "\n"))));
  echo(LINE(36,(assignCallTemp(eo_1, x_date("m.d.y")),concat3("date 7: ", eo_1, "\n"))));
  echo(LINE(37,(assignCallTemp(eo_1, x_date("j, n, Y")),concat3("date 8: ", eo_1, "\n"))));
  echo(LINE(38,(assignCallTemp(eo_1, x_date("Ymd")),concat3("date 9: ", eo_1, "\n"))));
  echo(LINE(39,(assignCallTemp(eo_1, x_date("h-i-, j-m-y, it i w Day z ")),concat3("date 10: ", eo_1, "\n"))));
  echo(LINE(40,(assignCallTemp(eo_1, x_date("\\i\\t \\i\\s \\t\\h\\e jS \\d\\a\\y.")),concat3("date 11: ", eo_1, "\n"))));
  echo(LINE(41,(assignCallTemp(eo_1, x_date("D M j G:i: T Y")),concat3("*date 12: ", eo_1, "\n"))));
  echo(LINE(42,(assignCallTemp(eo_1, x_date("H:m: \\m \\i\\s\\ \\m\\o\\n\\t\\h")),concat3("*date 13: ", eo_1, "\n"))));
  echo(LINE(43,(assignCallTemp(eo_1, x_date("H:i:")),concat3("*date 14: ", eo_1, "\n"))));
  echo(LINE(45,(assignCallTemp(eo_1, x_date("Z I O")),concat3("date 16: ", eo_1, "\n"))));
  (v_r = LINE(46,x_date("r")));
  v_r.set(23LL, ("x"), 0x3F3294657E65284ELL);
  v_r.set(24LL, ("x"), 0x65029A5A4A053537LL);
  echo(LINE(49,concat3("date 17: ", toString(v_r), "\n")));
  echo(LINE(52,(assignCallTemp(eo_1, x_gmdate("F j, Y, g:i a")),concat3("gmdate 1: ", eo_1, "\n"))));
  echo(LINE(55,(assignCallTemp(eo_1, toString(x_mktime(toInt32(1LL), toInt32(1LL), toInt32(1LL), toInt32(12LL), toInt32(12LL), toInt32(1978LL)))),concat3("mktime 1: ", eo_1, "\n"))));
  echo(LINE(58,(assignCallTemp(eo_1, toString(x_mktime(toInt32(1LL), toInt32(1LL), toInt32(1LL)))),concat3("mktime 4: ", eo_1, "\n"))));
  echo(LINE(59,(assignCallTemp(eo_1, toString(x_mktime(toInt32(1LL), toInt32(1LL), toInt32(1LL), toInt32(12LL)))),concat3("mktime 5: ", eo_1, "\n"))));
  echo(LINE(60,(assignCallTemp(eo_1, toString(x_mktime(toInt32(1LL), toInt32(1LL), toInt32(1LL), toInt32(12LL), toInt32(12LL)))),concat3("mktime 6: ", eo_1, "\n"))));
  echo(LINE(61,(assignCallTemp(eo_1, toString(x_mktime(toInt32(1LL), toInt32(1LL), toInt32(1LL), toInt32(12LL), toInt32(12LL), toInt32(1978LL)))),concat3("mktime 7: ", eo_1, "\n"))));
  echo(LINE(63,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(12LL), toInt32(32LL), toInt32(1997LL)))),x_date("M-d-Y", eo_4))),concat3("mktime 8: ", eo_1, "\n"))));
  echo(LINE(64,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(13LL), toInt32(1LL), toInt32(1997LL)))),x_date("M-d-Y", eo_4))),concat3("mktime 9: ", eo_1, "\n"))));
  echo(LINE(65,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(1LL), toInt32(1998LL)))),x_date("M-d-Y", eo_4))),concat3("mktime 10: ", eo_1, "\n"))));
  echo(LINE(67,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(1LL), toInt32(98LL)))),x_date("M-d-Y", eo_4))),concat3("\?mktime 11: ", eo_1, "\n"))));
  echo(LINE(70,(assignCallTemp(eo_1, toString(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(1LL), toInt32(2002LL)))),concat3("mktime 12: ", eo_1, "\n"))));
  echo(LINE(72,(assignCallTemp(eo_1, toString(invoke_too_many_args("mktime", (1), ((x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(1LL), toInt32(2002LL))))))),concat3("mktime 13: ", eo_1, "\n"))));
  echo(LINE(74,(assignCallTemp(eo_1, toString(invoke_too_many_args("mktime", (1), ((x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(1LL), toInt32(2002LL))))))),concat3("mktime 14: ", eo_1, "\n"))));
  echo(LINE(76,(assignCallTemp(eo_1, toString(invoke_too_many_args("mktime", (1), ((x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(1LL), toInt32(2002LL))))))),concat3("\?mktime 15: ", eo_1, "\n"))));
  echo(LINE(79,(assignCallTemp(eo_1, toString(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(7LL), toInt32(1LL), toInt32(2002LL)))),concat3("mktime 16: ", eo_1, "\n"))));
  echo(LINE(81,(assignCallTemp(eo_1, toString(invoke_too_many_args("mktime", (1), ((x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(7LL), toInt32(1LL), toInt32(2002LL))))))),concat3("mktime 17: ", eo_1, "\n"))));
  echo(LINE(83,(assignCallTemp(eo_1, toString(invoke_too_many_args("mktime", (1), ((x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(7LL), toInt32(1LL), toInt32(2002LL))))))),concat3("\?mktime 18: ", eo_1, "\n"))));
  echo(LINE(85,(assignCallTemp(eo_1, toString(invoke_too_many_args("mktime", (1), ((x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(7LL), toInt32(1LL), toInt32(2002LL))))))),concat3("mktime 19: ", eo_1, "\n"))));
  echo(LINE(88,(assignCallTemp(eo_1, toString(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(3LL), toInt32(0LL), toInt32(2000LL)))),concat3("mktime 20: ", eo_1, "\n"))));
  echo(LINE(89,(assignCallTemp(eo_1, toString(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(4LL), toInt32(-31LL), toInt32(2000LL)))),concat3("mktime 21: ", eo_1, "\n"))));
  echo(LINE(91,(assignCallTemp(eo_1, toString(x_gmmktime(toInt32(1LL), toInt32(1LL), toInt32(1LL), toInt32(12LL), toInt32(12LL), toInt32(1978LL)))),concat3("gmmktime 1: ", eo_1, "\n"))));
  (v_today = LINE(95,x_getdate(toInt64(x_mktime(toInt32(1LL), toInt32(1LL), toInt32(1LL))))));
  LINE(96,x_var_dump(1, v_today));
  print(LINE(104,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(12LL), toInt32(28LL), toInt32(2002LL)))),x_strftime("%V,%G,%Y", eo_4))),concat3("12/28/2002 - %V,%G,%Y = ", eo_1, "\n"))));
  print(LINE(107,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(12LL), toInt32(30LL), toInt32(2002LL)))),x_strftime("%V,%G,%Y", eo_4))),concat3("12/30/2002 - %V,%G,%Y = ", eo_1, "\n"))));
  print(LINE(110,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(2LL), toInt32(2003LL)))),x_strftime("%V,%G,%Y", eo_4))),concat3("1/3/2003 - %V,%G,%Y = ", eo_1, "\n"))));
  print(LINE(113,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(10LL), toInt32(2003LL)))),x_strftime("%V,%G,%Y", eo_4))),concat3("1/10/2003 - %V,%G,%Y = ", eo_1, "\n"))));
  print(LINE(115,(assignCallTemp(eo_1, (assignCallTemp(eo_4, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(10LL), toInt32(2003LL)))),x_gmstrftime("%V,%G,%Y", eo_4))),concat3("gmstrftime: 1/10/2003 - %V,%G,%Y = ", eo_1, "\n"))));
  print(LINE(117,x_strftime("%V,%G,%Y")));
  (v_lt = LINE(121,x_localtime(toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(10LL), toInt32(2003LL))))));
  print("localtime: \n");
  LINE(123,x_var_dump(1, v_lt));
  echo(LINE(125,(assignCallTemp(eo_1, toString(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(10LL), toInt32(2003LL)))),concat3("nate mktime: ", eo_1, "\n"))));
  (v_lt = LINE(127,(assignCallTemp(eo_0, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(1LL), toInt32(10LL), toInt32(2003LL)))),x_localtime(eo_0, toBoolean(1LL)))));
  print("localtime 2: \n");
  LINE(129,x_var_dump(1, v_lt));
  (v_t = LINE(133,x_gettimeofday()));
  echo(concat("se", toString(LINE(134,x_substr(toString(v_t.rvalAt("sec", 0x5051D7D254724345LL)), toInt32(0LL), toInt32(-2LL))))));
  echo(concat("mw", toString(v_t.rvalAt("minuteswest", 0x3154FA2DE4E49265LL))));
  echo(concat("dt", toString(v_t.rvalAt("dsttime", 0x768BE8C6058E3490LL))));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
