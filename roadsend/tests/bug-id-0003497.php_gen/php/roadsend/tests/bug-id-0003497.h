
#ifndef __GENERATED_php_roadsend_tests_bug_id_0003497_h__
#define __GENERATED_php_roadsend_tests_bug_id_0003497_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0003497.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Array f_bwops(int64 v_a, int64 v_b);
Variant pm_php$roadsend$tests$bug_id_0003497_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0003497_h__
