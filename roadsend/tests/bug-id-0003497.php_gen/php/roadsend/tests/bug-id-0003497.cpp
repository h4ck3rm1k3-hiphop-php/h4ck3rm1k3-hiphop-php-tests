
#include <php/roadsend/tests/bug-id-0003497.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_b = "b";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0003497.php line 3 */
Array f_bwops(int64 v_a, int64 v_b) {
  FUNCTION_INJECTION(bwops);
  Array v_r;

  v_r.append((bitwise_xor(v_a, v_b)));
  logical_xor(toBoolean(v_r.append((v_a))), toBoolean(v_b));
  v_r.append((bitwise_and(v_a, k_b)));
  (toBoolean(v_r.append((v_a)))) && (toBoolean(v_b));
  v_r.append((bitwise_or(v_a, v_b)));
  (toBoolean(v_r.append((v_a)))) || (toBoolean(v_b));
  v_r.append((~v_a));
  v_r.append((toInt64(v_a) << v_b));
  v_r.append((toInt64(v_a) >> v_b));
  return v_r;
} /* function */
Variant pm_php$roadsend$tests$bug_id_0003497_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003497.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003497_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  (v_r = LINE(16,f_bwops(8LL, 16LL)));
  LINE(17,x_var_dump(1, v_r));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
