
#include <php/roadsend/tests/pcre-7.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/pcre-7.php line 9 */
String f_next_year(CVarRef v_matches) {
  FUNCTION_INJECTION(next_year);
  LINE(13,x_var_dump(1, v_matches));
  return concat(toString(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), (toString(v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL) + 1LL)));
} /* function */
Variant i_next_year(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x341B5F07DFF990F5LL, next_year) {
    return (f_next_year(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$pcre_7_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/pcre-7.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$pcre_7_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_text __attribute__((__unused__)) = (variables != gVariables) ? variables->get("text") : g->GV(text);

  (v_text = "April fools day is 04/01/2002\nLast christmas was 12/24/2001\n");
  echo(toString(LINE(20,x_preg_replace_callback("|(\\d{2}/\\d{2}/)(\\d{4})|", "next_year", v_text))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
