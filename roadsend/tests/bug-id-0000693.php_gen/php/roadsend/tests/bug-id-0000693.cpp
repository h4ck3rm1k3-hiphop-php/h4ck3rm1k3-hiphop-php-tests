
#include <php/roadsend/tests/bug-id-0000693.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000693.php line 17 */
int64 f_cnt() {
  FUNCTION_INJECTION(cnt);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_i __attribute__((__unused__)) = g->sv_cnt_DupIdi;
  bool &inited_sv_i __attribute__((__unused__)) = g->inited_sv_cnt_DupIdi;
  if (!inited_sv_i) {
    (sv_i = 0LL);
    inited_sv_i = true;
  }
  sv_i++;
  return sv_i;
} /* function */
Variant pm_php$roadsend$tests$bug_id_0000693_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000693.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000693_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo("0000693 foreach and others with no braces cause parse error\n\n");
  (v_arr = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_arr.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_val = iter3->second();
      v_key = iter3->first();
      echo(LINE(10,concat4(toString(v_key), ", ", toString(v_val), "\n")));
    }
  }
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(4);
      echo(toString(v_i) + toString("\n"));
    }
  }
  LOOP_COUNTER(5);
  {
    while (less(LINE(24,f_cnt()), 10LL)) {
      LOOP_COUNTER_CHECK(5);
      echo("counted\n");
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
