
#include <php/roadsend/tests/callback.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/callback.php line 3 */
String f_callback(CVarRef v_buffer) {
  FUNCTION_INJECTION(callback);
  return (LINE(6,x_ereg_replace("apples", "oranges", toString(v_buffer))));
} /* function */
Variant i_callback(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2DE38EBA0ED55F48LL, callback) {
    return (f_callback(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$callback_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/callback.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$callback_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(10,x_ob_start("callback"));
  echo("\n<html>\n<body>\n<p>It's like comparing apples to oranges.\n</body>\n</html>\n\n");
  LINE(22,x_ob_end_flush());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
