
#ifndef __GENERATED_php_roadsend_tests_callback_h__
#define __GENERATED_php_roadsend_tests_callback_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/callback.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_callback(CVarRef v_buffer);
Variant pm_php$roadsend$tests$callback_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_callback_h__
