
#include <php/roadsend/tests/bug-id-0000828.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000828_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000828.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000828_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dir") : g->GV(dir);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_dir = "/etc");
  (v_foo = f_shell_exec(toString("ls -l ") + toString(v_dir)));
  print(toString(v_foo));
  (v_bar = f_shell_exec(toString("echo \\`echo \"asdf\"\\`")));
  echo(toString(v_bar));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
