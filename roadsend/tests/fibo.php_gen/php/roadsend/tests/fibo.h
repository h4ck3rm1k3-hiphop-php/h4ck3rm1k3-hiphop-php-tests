
#ifndef __GENERATED_php_roadsend_tests_fibo_h__
#define __GENERATED_php_roadsend_tests_fibo_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/fibo.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$fibo_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_fibo(Variant v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_fibo_h__
