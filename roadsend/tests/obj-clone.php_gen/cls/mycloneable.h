
#ifndef __GENERATED_cls_mycloneable_h__
#define __GENERATED_cls_mycloneable_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/obj-clone.php line 16 */
class c_mycloneable : virtual public ObjectData {
  BEGIN_CLASS_MAP(mycloneable)
  END_CLASS_MAP(mycloneable)
  DECLARE_CLASS(mycloneable, MyCloneable, ObjectData)
  void init();
  public: Variant m_object1;
  public: Variant m_object2;
  public: Variant t___clone();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mycloneable_h__
