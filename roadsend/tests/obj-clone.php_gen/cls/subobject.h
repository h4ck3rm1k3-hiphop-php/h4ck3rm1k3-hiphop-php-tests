
#ifndef __GENERATED_cls_subobject_h__
#define __GENERATED_cls_subobject_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/obj-clone.php line 2 */
class c_subobject : virtual public ObjectData {
  BEGIN_CLASS_MAP(subobject)
  END_CLASS_MAP(subobject)
  DECLARE_CLASS(subobject, SubObject, ObjectData)
  void init();
  public: Variant m_instance;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t___clone();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_subobject_h__
