
#include <php/roadsend/tests/obj-clone.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/obj-clone.php line 2 */
Variant c_subobject::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x6DC3BF51CC8FC8E0LL, g->s_subobject_DupIdinstances,
                  instances);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_subobject::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_subobject::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("instance", m_instance.isReferenced() ? ref(m_instance) : m_instance));
  c_ObjectData::o_get(props);
}
bool c_subobject::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x24965F78CA389518LL, instance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_subobject::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x24965F78CA389518LL, m_instance,
                         instance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_subobject::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x24965F78CA389518LL, m_instance,
                      instance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_subobject::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x24965F78CA389518LL, m_instance,
                         instance, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_subobject::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(subobject)
ObjectData *c_subobject::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_subobject::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_subobject::cloneImpl() {
  c_subobject *obj = NEW(c_subobject)();
  cloneSet(obj);
  return obj;
}
void c_subobject::cloneSet(c_subobject *clone) {
  clone->m_instance = m_instance.isReferenced() ? ref(m_instance) : m_instance;
  ObjectData::cloneSet(clone);
}
Variant c_subobject::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_subobject::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_subobject::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_subobject$os_get(const char *s) {
  return c_subobject::os_get(s, -1);
}
Variant &cw_subobject$os_lval(const char *s) {
  return c_subobject::os_lval(s, -1);
}
Variant cw_subobject$os_constant(const char *s) {
  return c_subobject::os_constant(s);
}
Variant cw_subobject$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_subobject::os_invoke(c, s, params, -1, fatal);
}
void c_subobject::init() {
  m_instance = null;
}
void c_subobject::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_subobject_DupIdinstances = 0LL;
}
void csi_subobject() {
  c_subobject::os_static_initializer();
}
/* SRC: roadsend/tests/obj-clone.php line 7 */
void c_subobject::t___construct() {
  INSTANCE_METHOD_INJECTION(SubObject, SubObject::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  (m_instance = ++g->s_subobject_DupIdinstances);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/obj-clone.php line 11 */
Variant c_subobject::t___clone() {
  INSTANCE_METHOD_INJECTION(SubObject, SubObject::__clone);
  DECLARE_GLOBAL_VARIABLES(g);
  (m_instance = ++g->s_subobject_DupIdinstances);
  return null;
} /* function */
/* SRC: roadsend/tests/obj-clone.php line 16 */
Variant c_mycloneable::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_mycloneable::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_mycloneable::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("object1", m_object1.isReferenced() ? ref(m_object1) : m_object1));
  props.push_back(NEW(ArrayElement)("object2", m_object2.isReferenced() ? ref(m_object2) : m_object2));
  c_ObjectData::o_get(props);
}
bool c_mycloneable::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x57331FBF13122770LL, object1, 7);
      HASH_EXISTS_STRING(0x56925F2A3B3E8DC0LL, object2, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_mycloneable::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x57331FBF13122770LL, m_object1,
                         object1, 7);
      HASH_RETURN_STRING(0x56925F2A3B3E8DC0LL, m_object2,
                         object2, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_mycloneable::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x57331FBF13122770LL, m_object1,
                      object1, 7);
      HASH_SET_STRING(0x56925F2A3B3E8DC0LL, m_object2,
                      object2, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_mycloneable::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x57331FBF13122770LL, m_object1,
                         object1, 7);
      HASH_RETURN_STRING(0x56925F2A3B3E8DC0LL, m_object2,
                         object2, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_mycloneable::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(mycloneable)
ObjectData *c_mycloneable::cloneImpl() {
  c_mycloneable *obj = NEW(c_mycloneable)();
  cloneSet(obj);
  return obj;
}
void c_mycloneable::cloneSet(c_mycloneable *clone) {
  clone->m_object1 = m_object1.isReferenced() ? ref(m_object1) : m_object1;
  clone->m_object2 = m_object2.isReferenced() ? ref(m_object2) : m_object2;
  ObjectData::cloneSet(clone);
}
Variant c_mycloneable::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_mycloneable::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_mycloneable::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_mycloneable$os_get(const char *s) {
  return c_mycloneable::os_get(s, -1);
}
Variant &cw_mycloneable$os_lval(const char *s) {
  return c_mycloneable::os_lval(s, -1);
}
Variant cw_mycloneable$os_constant(const char *s) {
  return c_mycloneable::os_constant(s);
}
Variant cw_mycloneable$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_mycloneable::os_invoke(c, s, params, -1, fatal);
}
void c_mycloneable::init() {
  m_object1 = null;
  m_object2 = null;
}
/* SRC: roadsend/tests/obj-clone.php line 21 */
Variant c_mycloneable::t___clone() {
  INSTANCE_METHOD_INJECTION(MyCloneable, MyCloneable::__clone);
  (m_object1 = f_clone(toObject(m_object1)));
  return null;
} /* function */
Object co_subobject(CArrRef params, bool init /* = true */) {
  return Object(p_subobject(NEW(c_subobject)())->dynCreate(params, init));
}
Object co_mycloneable(CArrRef params, bool init /* = true */) {
  return Object(p_mycloneable(NEW(c_mycloneable)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$obj_clone_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/obj-clone.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$obj_clone_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_obj2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj2") : g->GV(obj2);

  (v_obj = ((Object)(LINE(29,p_mycloneable(p_mycloneable(NEWOBJ(c_mycloneable)())->create())))));
  (v_obj.o_lval("object1", 0x57331FBF13122770LL) = ((Object)(LINE(31,p_subobject(p_subobject(NEWOBJ(c_subobject)())->create())))));
  (v_obj.o_lval("object2", 0x56925F2A3B3E8DC0LL) = ((Object)(LINE(32,p_subobject(p_subobject(NEWOBJ(c_subobject)())->create())))));
  (v_obj2 = f_clone(toObject(v_obj)));
  print("Original Object:\n");
  LINE(38,x_print_r(v_obj));
  print("Cloned Object:\n");
  LINE(41,x_print_r(v_obj2));
  echo(" \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
