
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 4:
      HASH_INDEX(0x42DD5992F362B3C4LL, path, 13);
      HASH_INDEX(0x336176791BC03F04LL, num, 17);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x322D3D8EFCB96007LL, tmpfname, 27);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      HASH_INDEX(0x4E60097CEE8FD6CELL, tmpdir, 21);
      break;
    case 15:
      HASH_INDEX(0x0D6857FDDD7F21CFLL, k, 23);
      HASH_INDEX(0x3B32AB004231C30FLL, stat, 25);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 18:
      HASH_INDEX(0x4307151CEB3C6312LL, row, 14);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 28:
      HASH_INDEX(0x48E8F48146EEEF5CLL, handle, 15);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 40:
      HASH_INDEX(0x30164401A9853128LL, data, 16);
      break;
    case 43:
      HASH_INDEX(0x3C2F961831E4EF6BLL, v, 24);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 48:
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 18);
      break;
    case 50:
      HASH_INDEX(0x4DCB2F04C3F8C672LL, symlink_name, 26);
      break;
    case 51:
      HASH_INDEX(0x5497579F8DA0CBF3LL, newname, 20);
      break;
    case 53:
      HASH_INDEX(0x483374BF62404B35LL, link_name, 19);
      break;
    case 54:
      HASH_INDEX(0x31CF71EAC03B86B6LL, testfile, 12);
      break;
    case 57:
      HASH_INDEX(0x2ED65F776BB66679LL, fstat, 22);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 28) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
