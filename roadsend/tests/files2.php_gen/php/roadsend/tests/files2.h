
#ifndef __GENERATED_php_roadsend_tests_files2_h__
#define __GENERATED_php_roadsend_tests_files2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/files2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_first_digit(CVarRef v_number);
Variant f_necho(int64 v_line_number, CVarRef v_string);
Variant pm_php$roadsend$tests$files2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_files2_h__
