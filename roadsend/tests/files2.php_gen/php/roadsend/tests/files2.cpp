
#include <php/roadsend/tests/files2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/files2.php line 9 */
Variant f_first_digit(CVarRef v_number) {
  FUNCTION_INJECTION(first_digit);
  return LINE(10,x_substr(toString(v_number), toInt32(0LL), toInt32(1LL)));
} /* function */
/* SRC: roadsend/tests/files2.php line 4 */
Variant f_necho(int64 v_line_number, CVarRef v_string) {
  FUNCTION_INJECTION(necho);
  echo(LINE(5,concat4(toString(v_line_number), ": ", toString(v_string), "\n")));
  return v_string;
} /* function */
Variant pm_php$roadsend$tests$files2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/files2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$files2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_testfile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testfile") : g->GV(testfile);
  Variant &v_path __attribute__((__unused__)) = (variables != gVariables) ? variables->get("path") : g->GV(path);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_handle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("handle") : g->GV(handle);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("num") : g->GV(num);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_link_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link_name") : g->GV(link_name);
  Variant &v_newname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newname") : g->GV(newname);
  Variant &v_tmpdir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tmpdir") : g->GV(tmpdir);
  Variant &v_fstat __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fstat") : g->GV(fstat);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_stat __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stat") : g->GV(stat);
  Variant &v_symlink_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("symlink_name") : g->GV(symlink_name);
  Variant &v_tmpfname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tmpfname") : g->GV(tmpfname);

  LINE(2,x_error_reporting(toInt32(6143LL) /* E_ALL */));
  {
    (v_testfile = "./this-is-a-file-for-testing-the-compiler");
  }
  (v_path = "/home/httpd/html/index.php");
  LINE(22,(assignCallTemp(eo_1, x_basename(toString(v_path))),f_necho(10LL, eo_1)));
  LINE(23,(assignCallTemp(eo_1, x_basename(toString(v_path), ".php")),f_necho(20LL, eo_1)));
  LINE(24,(assignCallTemp(eo_1, x_basename(toString(v_path), "x.php")),f_necho(30LL, eo_1)));
  LINE(25,(assignCallTemp(eo_1, x_basename(toString(v_path), ".PHP")),f_necho(40LL, eo_1)));
  LINE(28,(assignCallTemp(eo_1, x_touch(toString(v_testfile))),f_necho(50LL, eo_1)));
  LINE(29,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(60LL, eo_1)));
  LINE(30,x_clearstatcache());
  LINE(31,(assignCallTemp(eo_1, x_fileperms(toString(v_testfile))),f_necho(90LL, eo_1)));
  LINE(32,(assignCallTemp(eo_1, x_filegroup(toString(v_testfile))),f_necho(100LL, eo_1)));
  {
    LINE(34,x_clearstatcache());
    LINE(35,(assignCallTemp(eo_1, x_fileperms(toString(v_testfile))),f_necho(70LL, eo_1)));
    LINE(36,(assignCallTemp(eo_1, x_chmod(toString(v_testfile), 274LL)),f_necho(80LL, eo_1)));
    LINE(37,x_clearstatcache());
    LINE(38,(assignCallTemp(eo_1, x_chgrp(toString(v_testfile), "cdrom")),f_necho(110LL, eo_1)));
    LINE(39,x_clearstatcache());
    LINE(40,(assignCallTemp(eo_1, x_filegroup(toString(v_testfile))),f_necho(120LL, eo_1)));
    LINE(41,(assignCallTemp(eo_1, x_fileowner(toString(v_testfile))),f_necho(130LL, eo_1)));
    LINE(42,(assignCallTemp(eo_1, x_chown(toString(v_testfile), "nobody")),f_necho(140LL, eo_1)));
    LINE(43,x_clearstatcache());
    LINE(44,(assignCallTemp(eo_1, x_fileowner(toString(v_testfile))),f_necho(150LL, eo_1)));
  }
  LINE(46,(assignCallTemp(eo_1, x_unlink(toString(v_testfile))),f_necho(160LL, eo_1)));
  LINE(47,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(170LL, eo_1)));
  LINE(51,x_clearstatcache());
  LINE(52,(assignCallTemp(eo_1, f_first_digit(x_fileatime("/usr/include/stdio.h"))),f_necho(180LL, eo_1)));
  LINE(53,(assignCallTemp(eo_1, f_first_digit(x_filectime("/usr/include/stdio.h"))),f_necho(190LL, eo_1)));
  LINE(54,(assignCallTemp(eo_1, x_filegroup("/usr/include/stdio.h")),f_necho(200LL, eo_1)));
  LINE(55,(assignCallTemp(eo_1, x_fileinode("/usr/include/stdio.h")),f_necho(210LL, eo_1)));
  LINE(56,(assignCallTemp(eo_1, f_first_digit(x_filemtime("/usr/include/stdio.h"))),f_necho(220LL, eo_1)));
  LINE(57,(assignCallTemp(eo_1, x_fileowner("/usr/include/stdio.h")),f_necho(230LL, eo_1)));
  LINE(58,(assignCallTemp(eo_1, x_fileperms("/usr/include/stdio.h")),f_necho(240LL, eo_1)));
  LINE(59,(assignCallTemp(eo_1, x_filesize("/usr/include/stdio.h")),f_necho(250LL, eo_1)));
  LINE(60,(assignCallTemp(eo_1, x_filetype("/usr/include/stdio.h")),f_necho(260LL, eo_1)));
  {
    LINE(64,f_necho(270LL, "file(\"/etc/passed\"):"));
    LINE(65,x_print_r(x_file("/etc/passwd")));
  }
  echo("\n");
  {
    LINE(75,(assignCallTemp(eo_1, x_dirname("/etc/passwd")),f_necho(280LL, eo_1)));
    LINE(76,(assignCallTemp(eo_1, x_dirname("/usr/local/lib/foo/local/lib/bar/foo.h")),f_necho(290LL, eo_1)));
  }
  {
    LINE(89,(assignCallTemp(eo_1, f_first_digit(x_disk_free_space("/etc"))),f_necho(300LL, eo_1)));
    LINE(90,(assignCallTemp(eo_1, f_first_digit(x_diskfreespace("/etc"))),f_necho(310LL, eo_1)));
    LINE(93,(assignCallTemp(eo_1, f_first_digit(x_disk_total_space("/tmp"))),f_necho(320LL, eo_1)));
  }
  (v_row = 1LL);
  (v_handle = LINE(98,x_fopen("/etc/passwd", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_data = LINE(99,x_fgetcsv(toObject(v_handle), 1000LL, ":"))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_num = LINE(100,x_count(v_data)));
        LINE(101,(assignCallTemp(eo_1, concat4(toString(v_num), " fields in line ", toString(v_row), ":")),f_necho(330LL, eo_1)));
        v_row++;
        {
          LOOP_COUNTER(2);
          for ((v_c = 0LL); less(v_c, v_num); v_c++) {
            LOOP_COUNTER_CHECK(2);
            {
              LINE(104,f_necho(340LL, v_data.rvalAt(v_c)));
            }
          }
        }
      }
    }
  }
  LINE(107,x_fclose(toObject(v_handle)));
  (v_handle = LINE(110,x_fopen("/etc/passwd", "r")));
  echo("350: ");
  LOOP_COUNTER(3);
  {
    while (toBoolean(v_handle) && !(LINE(112,x_feof(toObject(v_handle))))) {
      LOOP_COUNTER_CHECK(3);
      {
        LINE(113,x_fgets(toObject(v_handle), 4096LL));
      }
    }
  }
  LINE(115,x_fclose(toObject(v_handle)));
  LINE(125,(assignCallTemp(eo_1, x_is_dir("/proc")),f_necho(370LL, eo_1)));
  LINE(126,(assignCallTemp(eo_1, x_is_dir("./this_file_should_probably_not_exist")),f_necho(380LL, eo_1)));
  LINE(127,(assignCallTemp(eo_1, x_is_dir("/etc/group")),f_necho(390LL, eo_1)));
  LINE(134,(assignCallTemp(eo_1, x_is_file("/proc")),f_necho(420LL, eo_1)));
  LINE(135,(assignCallTemp(eo_1, x_is_file("./this_file_should_probably_not_exist")),f_necho(430LL, eo_1)));
  LINE(136,(assignCallTemp(eo_1, x_is_file("/etc/group")),f_necho(440LL, eo_1)));
  LINE(139,(assignCallTemp(eo_1, x_touch(toString(v_testfile))),f_necho(450LL, eo_1)));
  LINE(140,(assignCallTemp(eo_1, x_is_writeable(toString(v_testfile))),f_necho(460LL, eo_1)));
  LINE(141,(assignCallTemp(eo_1, x_is_writable(toString(v_testfile))),f_necho(470LL, eo_1)));
  LINE(142,(assignCallTemp(eo_1, x_is_readable(toString(v_testfile))),f_necho(480LL, eo_1)));
  LINE(143,(assignCallTemp(eo_1, x_is_readable("/etc/passwd")),f_necho(490LL, eo_1)));
  LINE(144,(assignCallTemp(eo_1, x_is_writeable("/etc/passwd")),f_necho(500LL, eo_1)));
  LINE(145,(assignCallTemp(eo_1, x_is_writable("/etc/passwd")),f_necho(510LL, eo_1)));
  LINE(146,(assignCallTemp(eo_1, x_unlink(toString(v_testfile))),f_necho(520LL, eo_1)));
  LINE(149,(assignCallTemp(eo_1, x_touch(toString(v_testfile))),f_necho(530LL, eo_1)));
  (v_link_name = LINE(150,x_tempnam("/tmp", "cowboy1")));
  LINE(151,(assignCallTemp(eo_1, x_file_exists(toString(v_link_name))),f_necho(540LL, eo_1)));
  LINE(152,(assignCallTemp(eo_1, x_unlink(toString(v_link_name))),f_necho(550LL, eo_1)));
  LINE(153,x_clearstatcache());
  {
    LINE(156,(assignCallTemp(eo_1, x_link(toString(v_testfile), toString(v_link_name))),f_necho(560LL, eo_1)));
    LINE(157,(assignCallTemp(eo_1, x_is_link(toString(v_link_name))),f_necho(570LL, eo_1)));
  }
  LINE(160,(assignCallTemp(eo_1, x_unlink(toString(v_link_name))),f_necho(580LL, eo_1)));
  (v_handle = LINE(163,x_popen("/bin/ls /etc", "r")));
  LINE(164,(assignCallTemp(eo_1, x_fread(toObject(v_handle), 2096LL)),f_necho(590LL, eo_1)));
  LINE(165,(assignCallTemp(eo_1, x_pclose(toObject(v_handle))),f_necho(600LL, eo_1)));
  (v_handle = LINE(167,x_popen("/path/to/spooge 2>&1", "r")));
  LINE(168,(assignCallTemp(eo_1, x_fread(toObject(v_handle), 2096LL)),f_necho(610LL, eo_1)));
  LINE(171,x_pclose(toObject(v_handle)));
  LINE(174,f_necho(620LL, "readfile(\"/etc/motd\"):"));
  LINE(175,x_readfile("/etc/motd"));
  echo("\n");
  LINE(177,f_necho(630LL, "readfile(\"./this_file_should_probably_not_exist\"):"));
  LINE(178,x_readfile("./this_file_should_probably_not_exist"));
  {
    LINE(182,(assignCallTemp(eo_1, x_realpath("/etc/alternatives/telnet")),f_necho(640LL, eo_1)));
    LINE(183,(assignCallTemp(eo_1, x_realpath("/etc/passwd")),f_necho(645LL, eo_1)));
    LINE(184,(assignCallTemp(eo_1, x_symlink("/proc", "./proc")),f_necho(650LL, eo_1)));
    LINE(185,(assignCallTemp(eo_1, x_realpath("/etc/../proc/.././proc")),f_necho(660LL, eo_1)));
  }
  LINE(193,(assignCallTemp(eo_1, x_unlink("./proc")),f_necho(670LL, eo_1)));
  (v_newname = concat(toString(v_testfile), "with-a-new-name"));
  LINE(197,(assignCallTemp(eo_1, x_touch(toString(v_testfile))),f_necho(680LL, eo_1)));
  LINE(198,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(690LL, eo_1)));
  LINE(199,(assignCallTemp(eo_1, x_rename(toString(v_testfile), toString(v_newname))),f_necho(700LL, eo_1)));
  LINE(200,x_clearstatcache());
  LINE(201,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(710LL, eo_1)));
  LINE(202,(assignCallTemp(eo_1, x_file_exists(toString(v_newname))),f_necho(720LL, eo_1)));
  LINE(203,(assignCallTemp(eo_1, x_copy(toString(v_newname), toString(v_testfile))),f_necho(730LL, eo_1)));
  LINE(204,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(740LL, eo_1)));
  LINE(205,(assignCallTemp(eo_1, x_file_exists(toString(v_newname))),f_necho(750LL, eo_1)));
  LINE(206,(assignCallTemp(eo_1, x_unlink(toString(v_testfile))),f_necho(760LL, eo_1)));
  LINE(207,(assignCallTemp(eo_1, x_unlink(toString(v_newname))),f_necho(770LL, eo_1)));
  LINE(208,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(780LL, eo_1)));
  LINE(209,(assignCallTemp(eo_1, x_file_exists(toString(v_newname))),f_necho(790LL, eo_1)));
  (v_tmpdir = "./a-temporary-directory-to-test-the-compiler");
  LINE(213,(assignCallTemp(eo_1, x_mkdir(toString(v_tmpdir))),f_necho(800LL, eo_1)));
  LINE(214,x_clearstatcache());
  LINE(215,(assignCallTemp(eo_1, x_fileperms(toString(v_tmpdir))),f_necho(810LL, eo_1)));
  LINE(216,(assignCallTemp(eo_1, x_is_dir(toString(v_tmpdir))),f_necho(820LL, eo_1)));
  LINE(217,(assignCallTemp(eo_1, x_rmdir(toString(v_tmpdir))),f_necho(830LL, eo_1)));
  LINE(218,x_clearstatcache());
  LINE(221,(assignCallTemp(eo_1, x_is_dir(toString(v_tmpdir))),f_necho(840LL, eo_1)));
  (v_handle = LINE(224,x_fopen("/usr/include/stdio.h", "r")));
  (v_fstat = LINE(225,(assignCallTemp(eo_1, x_fstat(toObject(v_handle))),f_necho(850LL, eo_1))));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_fstat.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      if ((!equal(v_k, 8LL)) && (!((same(v_k, "atime"))))) LINE(228,(assignCallTemp(eo_1, (assignCallTemp(eo_3, toString(v_k)),assignCallTemp(eo_5, toString(v_v)),assignCallTemp(eo_7, x_gettype(v_v)),concat6("fstat[", eo_3, "] = ", eo_5, ", type is: ", eo_7))),f_necho(860LL, eo_1)));
      else LINE(230,(assignCallTemp(eo_1, concat_rev(x_gettype(v_v), (assignCallTemp(eo_3, toString(v_k)),assignCallTemp(eo_5, toString(f_first_digit(v_v))),concat5("(first digit of) fstat[", eo_3, "] = ", eo_5, ", type is: ")))),f_necho(860LL, eo_1)));
    }
  }
  LINE(231,(assignCallTemp(eo_1, x_fclose(toObject(v_handle))),f_necho(870LL, eo_1)));
  (v_stat = LINE(234,x_stat("/usr/include/stdio.h")));
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_stat.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
      v_k = iter9->first();
      if ((!equal(v_k, 8LL)) && (!((same(v_k, "atime"))))) LINE(237,(assignCallTemp(eo_1, (assignCallTemp(eo_3, toString(v_k)),assignCallTemp(eo_5, toString(v_v)),assignCallTemp(eo_7, x_gettype(v_v)),concat6("stat[", eo_3, "] = ", eo_5, ", type is: ", eo_7))),f_necho(880LL, eo_1)));
      else LINE(239,(assignCallTemp(eo_1, concat_rev(x_gettype(v_v), (assignCallTemp(eo_3, toString(v_k)),assignCallTemp(eo_5, toString(f_first_digit(v_v))),concat5("(first digit of) stat[", eo_3, "] = ", eo_5, ", type is: ")))),f_necho(880LL, eo_1)));
    }
  }
  LINE(242,(assignCallTemp(eo_1, x_touch(toString(v_testfile))),f_necho(890LL, eo_1)));
  (v_symlink_name = LINE(243,x_tempnam("/tmp", "cowboy2")));
  LINE(244,(assignCallTemp(eo_1, x_file_exists(toString(v_symlink_name))),f_necho(900LL, eo_1)));
  LINE(245,(assignCallTemp(eo_1, x_unlink(toString(v_symlink_name))),f_necho(910LL, eo_1)));
  LINE(246,x_clearstatcache());
  {
    LINE(248,(assignCallTemp(eo_1, x_symlink(toString(v_testfile), toString(v_symlink_name))),f_necho(920LL, eo_1)));
    LINE(249,(assignCallTemp(eo_1, x_file_exists(toString(v_symlink_name))),f_necho(930LL, eo_1)));
    LINE(250,(assignCallTemp(eo_1, x_is_link(toString(v_symlink_name))),f_necho(940LL, eo_1)));
    LINE(251,(assignCallTemp(eo_1, x_unlink(toString(v_symlink_name))),f_necho(950LL, eo_1)));
  }
  (v_tmpfname = LINE(255,x_tempnam("/tmp", "cowboy3")));
  (v_handle = LINE(256,x_fopen(toString(v_tmpfname), "w")));
  LINE(257,(assignCallTemp(eo_1, x_fwrite(toObject(v_handle), "writing to tempfile")),f_necho(960LL, eo_1)));
  LINE(258,(assignCallTemp(eo_1, x_fclose(toObject(v_handle))),f_necho(970LL, eo_1)));
  LINE(259,(assignCallTemp(eo_1, x_unlink(toString(v_tmpfname))),f_necho(980LL, eo_1)));
  LINE(262,(assignCallTemp(eo_1, x_umask()),f_necho(990LL, eo_1)));
  LINE(263,(assignCallTemp(eo_1, x_umask(493LL)),f_necho(1000LL, eo_1)));
  LINE(264,(assignCallTemp(eo_1, x_umask("foo")),f_necho(1010LL, eo_1)));
  LINE(265,(assignCallTemp(eo_1, x_umask()),f_necho(1020LL, eo_1)));
  LINE(268,(assignCallTemp(eo_1, x_touch(toString(v_testfile))),f_necho(1030LL, eo_1)));
  LINE(269,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(1040LL, eo_1)));
  LINE(270,(assignCallTemp(eo_1, x_unlink(toString(v_testfile))),f_necho(1050LL, eo_1)));
  LINE(271,(assignCallTemp(eo_1, x_file_exists(toString(v_testfile))),f_necho(1060LL, eo_1)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
