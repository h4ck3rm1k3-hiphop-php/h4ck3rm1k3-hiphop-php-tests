
#include <php/roadsend/tests/bug-id-0000729.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000729.php line 4 */
Variant c_testc::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_testc::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_testc::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("test", m_test));
  c_ObjectData::o_get(props);
}
bool c_testc::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x3A1DC49AA6CFEA13LL, test, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_testc::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3A1DC49AA6CFEA13LL, m_test,
                         test, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_testc::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x3A1DC49AA6CFEA13LL, m_test,
                      test, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_testc::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_testc::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(testc)
ObjectData *c_testc::cloneImpl() {
  c_testc *obj = NEW(c_testc)();
  cloneSet(obj);
  return obj;
}
void c_testc::cloneSet(c_testc *clone) {
  clone->m_test = m_test;
  ObjectData::cloneSet(clone);
}
Variant c_testc::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_testc::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_testc::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_testc$os_get(const char *s) {
  return c_testc::os_get(s, -1);
}
Variant &cw_testc$os_lval(const char *s) {
  return c_testc::os_lval(s, -1);
}
Variant cw_testc$os_constant(const char *s) {
  return c_testc::os_constant(s);
}
Variant cw_testc$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_testc::os_invoke(c, s, params, -1, fatal);
}
void c_testc::init() {
  m_test = "5";
}
Object co_testc(CArrRef params, bool init /* = true */) {
  return Object(p_testc(NEW(c_testc)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000729_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000729.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000729_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_prop __attribute__((__unused__)) = (variables != gVariables) ? variables->get("prop") : g->GV(prop);

  echo("0000729 access to a class property through variable variable: \n\n");
  (v_c = ((Object)(LINE(5,p_testc(p_testc(NEWOBJ(c_testc)())->create())))));
  (v_prop = "test");
  echo(concat(toString(v_c.o_get(toString(v_prop), -1LL)), "\n"));
  echo(concat(toString(v_c.o_get(toString(v_prop), -1LL)), "\n"));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
