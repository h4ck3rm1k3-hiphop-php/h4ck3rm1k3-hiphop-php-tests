
#include <php/roadsend/tests/tags.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_expression = "expression";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$tags_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/tags.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$tags_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("this is the simplest, an SGML processing instruction\n");
  echo(k_expression);
  echo(" This is a shortcut for \"");
  echo(k_expression);
  echo("\"\n    \n");
  echo("if you want to serve XHTML or XML documents, do like this\n");
  echo("\n");
  echo("some editors (like FrontPage) don't like processing instructions");
  echo("\n");
  echo("<%");
  echo(" echo (\"You may optionally use ASP-style tags\"); %>\n\n");
  echo("<%=");
  echo(" $variable; # This is a shortcut for \"");
  echo("<%");
  echo(" echo . . .\" %>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
