
#include <php/roadsend/tests/bug-id-0000796.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000796_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000796.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000796_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_read __attribute__((__unused__)) = (variables != gVariables) ? variables->get("read") : g->GV(read);
  Variant &v_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("num") : g->GV(num);

  echo("0000796 parser error on empty braces\n");
  if (toBoolean(1LL)) {
  }
  else if (toBoolean(2LL)) {
  }
  echo("\nor\n\n");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
      }
    }
  }
  echo("\nwhich really comes from code like this: \n");
  {
    LOOP_COUNTER(2);
    for ((v_r = 0LL), (v_num = LINE(21,x_count(v_read))); less(v_r, v_num) && !equal(LINE(22,x_substr(toString(v_read.rvalAt(v_r)), toInt32(0LL), toInt32(8LL))), "* SEARCH"); v_r++) {
      LOOP_COUNTER_CHECK(2);
      {
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
