
#ifndef __GENERATED_cls_ufunctionmenu_h__
#define __GENERATED_cls_ufunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/inc8.php line 22 */
class c_ufunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(ufunctionmenu)
  END_CLASS_MAP(ufunctionmenu)
  DECLARE_CLASS(ufunctionmenu, uFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ufunctionmenu_h__
