
#ifndef __GENERATED_php_roadsend_tests_inc8_h__
#define __GENERATED_php_roadsend_tests_inc8_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc8.fw.h>

// Declarations
#include <cls/ufunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc8_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_ufunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc8_h__
