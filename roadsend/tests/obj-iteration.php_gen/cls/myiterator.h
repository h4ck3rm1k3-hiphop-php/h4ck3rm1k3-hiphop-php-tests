
#ifndef __GENERATED_cls_myiterator_h__
#define __GENERATED_cls_myiterator_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/obj-iteration.php line 3 */
class c_myiterator : virtual public c_iterator {
  BEGIN_CLASS_MAP(myiterator)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iterator)
  END_CLASS_MAP(myiterator)
  DECLARE_CLASS(myiterator, MyIterator, ObjectData)
  void init();
  public: Variant m_var;
  public: void t___construct(Variant v_array);
  public: ObjectData *create(Variant v_array);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_rewind();
  public: Variant t_current();
  public: Variant t_key();
  public: Variant t_next();
  public: bool t_valid();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myiterator_h__
