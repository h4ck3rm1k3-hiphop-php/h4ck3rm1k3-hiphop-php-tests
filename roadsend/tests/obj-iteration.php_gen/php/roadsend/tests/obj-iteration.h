
#ifndef __GENERATED_php_roadsend_tests_obj_iteration_h__
#define __GENERATED_php_roadsend_tests_obj_iteration_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/obj-iteration.fw.h>

// Declarations
#include <cls/myiterator.h>
#include <cls/mycollection.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$obj_iteration_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_myiterator(CArrRef params, bool init = true);
Object co_mycollection(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_obj_iteration_h__
