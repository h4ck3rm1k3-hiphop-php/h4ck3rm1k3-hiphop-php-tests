
#ifndef __GENERATED_php_roadsend_tests_obj_iteration_fw_h__
#define __GENERATED_php_roadsend_tests_obj_iteration_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(myiterator)
FORWARD_DECLARE_CLASS(mycollection)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_obj_iteration_fw_h__
