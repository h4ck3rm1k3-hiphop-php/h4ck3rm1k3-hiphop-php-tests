
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001263_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001263_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001263.fw.h>

// Declarations
#include <cls/template.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001263_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_template(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001263_h__
