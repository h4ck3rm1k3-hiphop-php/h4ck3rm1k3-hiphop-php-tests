
#include <php/roadsend/tests/bug-id-0001263.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001263.php line 6 */
Variant c_template::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_template::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_template::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("myVal", m_myVal));
  c_ObjectData::o_get(props);
}
bool c_template::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x3910DBD7337B3EEBLL, myVal, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_template::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3910DBD7337B3EEBLL, m_myVal,
                         myVal, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_template::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x3910DBD7337B3EEBLL, m_myVal,
                      myVal, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_template::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_template::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(template)
ObjectData *c_template::cloneImpl() {
  c_template *obj = NEW(c_template)();
  cloneSet(obj);
  return obj;
}
void c_template::cloneSet(c_template *clone) {
  clone->m_myVal = m_myVal;
  ObjectData::cloneSet(clone);
}
Variant c_template::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_template::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_template::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_template$os_get(const char *s) {
  return c_template::os_get(s, -1);
}
Variant &cw_template$os_lval(const char *s) {
  return c_template::os_lval(s, -1);
}
Variant cw_template$os_constant(const char *s) {
  return c_template::os_constant(s);
}
Variant cw_template$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_template::os_invoke(c, s, params, -1, fatal);
}
void c_template::init() {
  m_myVal = ScalarArrays::sa_[0];
}
/* SRC: roadsend/tests/bug-id-0001263.php line 9 */
void c_template::t_add(CVarRef v_k) {
  INSTANCE_METHOD_INJECTION(template, template::add);
  lval(m_myVal.lvalAt("foo", 0x4154FA2EF733DA8FLL)).set(v_k, ("foo"));
} /* function */
Object co_template(CArrRef params, bool init /* = true */) {
  return Object(p_template(NEW(c_template)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0001263_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001263.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001263_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_zip __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zip") : g->GV(zip);
  Variant &v_ping __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ping") : g->GV(ping);

  echo("When an object is copied, you need to copy the properties, \nnot just the hashtable that holds them.  Tarnation.\n\n");
  (v_foo = ((Object)(LINE(15,p_template(p_template(NEWOBJ(c_template)())->create())))));
  (v_bar = v_foo);
  LINE(19,v_bar.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, "asdf"));
  LINE(21,x_var_dump(1, v_foo));
  LINE(22,x_var_dump(1, v_bar));
  echo("hashes\n");
  v_zip.set("bar", (ScalarArrays::sa_[1]), 0x4A453601FBA86E5FLL);
  (v_ping = v_zip);
  lval(v_ping.lvalAt("ping", 0x7FE2F2329F74166ELL)).append((2LL));
  LINE(31,x_var_dump(1, v_zip));
  LINE(32,x_var_dump(1, v_ping));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
