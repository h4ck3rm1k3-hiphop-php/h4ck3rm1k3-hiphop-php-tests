
#include <php/roadsend/tests/getpost.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$getpost_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/getpost.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$getpost_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ch") : g->GV(ch);

  (v_ch = LINE(3,x_curl_init()));
  LINE(5,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "http://www.roadsend.com/_test/test.php"));
  LINE(6,x_curl_setopt(toObject(v_ch), toInt32(47LL) /* CURLOPT_POST */, 1LL));
  LINE(8,x_curl_setopt(toObject(v_ch), toInt32(10015LL) /* CURLOPT_POSTFIELDS */, "postvar1=value1&postvar2=value2&postvar3=value3"));
  LINE(10,x_curl_exec(toObject(v_ch)));
  echo(LINE(11,x_curl_error(toObject(v_ch))));
  LINE(13,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "http://www.roadsend.com/_test/test.php\?type=get&foo=bar"));
  LINE(14,x_curl_exec(toObject(v_ch)));
  LINE(16,x_curl_close(toObject(v_ch)));
  (v_ch = LINE(23,x_curl_init()));
  LINE(25,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "http://www.roadsend.com/_test/test.php"));
  LINE(26,x_curl_setopt(toObject(v_ch), toInt32(47LL) /* CURLOPT_POST */, 1LL));
  LINE(28,x_curl_setopt(toObject(v_ch), toInt32(10015LL) /* CURLOPT_POSTFIELDS */, "postvar1=value1&postvar2=value2&postvar3=value3"));
  LINE(31,x_curl_setopt(toObject(v_ch), toInt32(10036LL) /* CURLOPT_CUSTOMREQUEST */, "FOOBAR"));
  LINE(33,x_curl_exec(toObject(v_ch)));
  LINE(34,x_curl_close(toObject(v_ch)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
