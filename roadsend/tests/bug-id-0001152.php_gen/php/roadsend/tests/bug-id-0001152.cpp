
#include <php/roadsend/tests/bug-id-0001152.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001152.php line 3 */
void f_error_hdlr(CVarRef v_errno, CVarRef v_errstr) {
  FUNCTION_INJECTION(error_hdlr);
  echo(LINE(4,concat3("[", toString(v_errstr), "]\n")));
} /* function */
Variant i_error_hdlr(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x02D70B8F27073E45LL, error_hdlr) {
    return (f_error_hdlr(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$bug_id_0001152_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001152.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001152_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("0001152 string assignment problems\n");
  LINE(7,x_set_error_handler("error_hdlr"));
  (v_i = 4LL);
  (v_s = "string");
  echo("1: ");
  (v_result = "* *-*");
  LINE(14,x_var_dump(1, v_result));
  echo("2: ");
  v_result.set(6LL, ("*"), 0x26BF47194D7E8E12LL);
  LINE(17,x_var_dump(1, v_result));
  echo("3: ");
  v_result.set(1LL, (v_i), 0x5BCA7C69B794F8CELL);
  LINE(20,x_var_dump(1, v_result));
  echo("4: ");
  v_result.set(3LL, (v_s), 0x135FDDF6A6BFBBDDLL);
  LINE(23,x_var_dump(1, v_result));
  echo("5: ");
  v_result.set(7LL, (0LL), 0x7D75B33B7AEB669DLL);
  LINE(26,x_var_dump(1, v_result));
  echo("6: ");
  (v_a = v_result.set(1LL, (v_result.set(3LL, ("-"), 0x135FDDF6A6BFBBDDLL)), 0x5BCA7C69B794F8CELL));
  LINE(29,x_var_dump(1, v_result));
  echo("7: ");
  (v_b = v_result.set(3LL, (v_result.set(5LL, (v_s), 0x350AEB726A15D700LL)), 0x135FDDF6A6BFBBDDLL));
  LINE(32,x_var_dump(1, v_result));
  echo("8: ");
  (v_c = v_result.set(0LL, (v_result.set(2LL, (v_result.set(4LL, (v_i), 0x6F2A25235E544A31LL)), 0x486AFCC090D5F98CLL)), 0x77CFA1EEF01BCA90LL));
  LINE(35,x_var_dump(1, v_result));
  echo("9: ");
  (v_d = v_result.set(6LL, (v_result.set(8LL, (5LL), 0x21ABC084C3578135LL)), 0x26BF47194D7E8E12LL));
  LINE(38,x_var_dump(1, v_result));
  echo("10: ");
  (v_e = v_result.set(1LL, (v_result.rvalAt(6LL, 0x26BF47194D7E8E12LL)), 0x5BCA7C69B794F8CELL));
  LINE(41,x_var_dump(1, v_result));
  echo("11: ");
  LINE(43,x_var_dump(5, v_a, Array(ArrayInit(4).set(0, v_b).set(1, v_c).set(2, v_d).set(3, v_e).create())));
  echo("12: ");
  v_result.set(-1LL, ("a"), 0x1F89206E3F8EC794LL);
  LINE(46,x_var_dump(1, v_result));
  echo("\nNULLs suck\n");
  (v_b = 7LL);
  (v_a = (v_b = 12LL));
  LINE(56,x_var_dump(1, v_a));
  LINE(57,x_var_dump(1, v_b));
  (v_b = "foo");
  (v_a = v_b.set(2LL, ("p"), 0x486AFCC090D5F98CLL));
  LINE(62,x_var_dump(1, v_a));
  LINE(63,x_var_dump(1, v_b));
  (v_c = "asdf");
  v_c.set(2LL, (""), 0x486AFCC090D5F98CLL);
  LINE(67,x_var_dump(1, v_c));
  echo("\n\nthis test verifies that the string gets written back into an array\nin the case of a one-or-more dimensional array where the last dimension\nis actually a string-char.\n\n");
  (v_bar = 2LL);
  (v_foo = ScalarArrays::sa_[0]);
  lval(v_foo.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(3LL, ("b"), 0x135FDDF6A6BFBBDDLL);
  LINE(89,x_var_dump(1, v_foo));
  echo("and bar:\n");
  LINE(91,x_var_dump(1, v_bar));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
