
#include <php/roadsend/tests/zfunction_falling.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/zfunction_falling.php line 2 */
Variant f_test(CVarRef v_a) {
  FUNCTION_INJECTION(Test);
  if (less(v_a, 3LL)) {
    return 3LL;
  }
  return null;
} /* function */
Variant pm_php$roadsend$tests$zfunction_falling_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zfunction_falling.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zfunction_falling_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = 1LL);
  if (less(v_a, LINE(8,f_test(v_a)))) {
    echo(toString(v_a) + toString("\n"));
    v_a++;
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
