
#include <php/roadsend/tests/gdkcolor.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$gdkcolor_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/gdkcolor.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$gdkcolor_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_clTransparentTest __attribute__((__unused__)) = (variables != gVariables) ? variables->get("clTransparentTest") : g->GV(clTransparentTest);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  if (!(LINE(3,x_extension_loaded("gtk")))) {
    LINE(4,x_dl("php_gtk.so"));
  }
  (v_clTransparentTest = LINE(7,create_object("gdkcolor", Array(ArrayInit(1).set(0, "#FFAA33").create()))));
  (v_foo = LINE(8,create_object("gdkcolor", Array(ArrayInit(3).set(0, 255LL).set(1, 51LL).set(2, 170LL).create()))));
  (v_bar = LINE(10,create_object("gdkcolor", Array(ArrayInit(1).set(0, "blorp").create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
