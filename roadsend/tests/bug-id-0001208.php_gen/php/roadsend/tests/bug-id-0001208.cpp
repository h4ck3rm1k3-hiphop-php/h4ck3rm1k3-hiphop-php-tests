
#include <php/roadsend/tests/bug-id-0001208.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001208_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001208.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001208_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a1") : g->GV(a1);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_a3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a3") : g->GV(a3);
  Variant &v_a4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a4") : g->GV(a4);

  (v_a1 = ScalarArrays::sa_[0]);
  (v_a2 = ScalarArrays::sa_[1]);
  (v_a3 = LINE(5,x_array_merge(2, v_a1, Array(ArrayInit(1).set(0, v_a2).create()))));
  print("merged: ");
  LINE(7,x_print_r(v_a3));
  (v_a4 = LINE(9,x_array_unique(v_a3)));
  print("uniqued: ");
  LINE(11,x_print_r(v_a4));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
