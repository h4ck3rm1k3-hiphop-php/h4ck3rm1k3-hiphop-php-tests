
#include <php/roadsend/tests/remarkably_broken.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/remarkably_broken.php line 14 */
void f_zammo(CStrRef v_foo) {
  FUNCTION_INJECTION(zammo);
  echo((toString((Variant)(concat(v_foo, toString(2LL))) + 3LL)));
} /* function */
Variant pm_php$roadsend$tests$remarkably_broken_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/remarkably_broken.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$remarkably_broken_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_foo = "zippo");
  v_foo.set("zot", ("bar"), 0x7A42596E78DEAC62LL);
  echo((toString(v_foo) + toString("\n")));
  echo((toString(v_foo.rvalAt("zot", 0x7A42596E78DEAC62LL)) + toString("\n")));
  (v_foo = "foo");
  v_foo++;
  print(toString(v_foo) + toString("\n"));
  LINE(18,f_zammo("bar"));
  print("\n");
  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); not_more(v_i, 10LL); print(toString(v_i)), v_i++) {
      LOOP_COUNTER_CHECK(1);
    }
  }
  print("\n");
  print(toString(v_foo));
  print("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
