
#ifndef __GENERATED_cls_cclass_h__
#define __GENERATED_cls_cclass_h__

#include <cls/bclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/constructor.php line 22 */
class c_cclass : virtual public c_bclass {
  BEGIN_CLASS_MAP(cclass)
    PARENT_CLASS(aclass)
    PARENT_CLASS(bclass)
  END_CLASS_MAP(cclass)
  DECLARE_CLASS(cclass, cclass, bclass)
  void init();
  public: void t_cclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cclass_h__
