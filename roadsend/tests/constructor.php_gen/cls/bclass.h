
#ifndef __GENERATED_cls_bclass_h__
#define __GENERATED_cls_bclass_h__

#include <cls/aclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/constructor.php line 14 */
class c_bclass : virtual public c_aclass {
  BEGIN_CLASS_MAP(bclass)
    PARENT_CLASS(aclass)
  END_CLASS_MAP(bclass)
  DECLARE_CLASS(bclass, bclass, aclass)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bclass_h__
