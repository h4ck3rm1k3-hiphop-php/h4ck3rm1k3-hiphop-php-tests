
#include <php/roadsend/tests/bug-id-0000792.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000792.php line 5 */
Variant c_t::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_t::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_t::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("class", m_class));
  c_ObjectData::o_get(props);
}
bool c_t::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x45397FE5C82DBD12LL, class, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_t::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x45397FE5C82DBD12LL, m_class,
                         class, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_t::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x45397FE5C82DBD12LL, m_class,
                      class, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_t::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_t::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(t)
ObjectData *c_t::create() {
  init();
  t_t();
  return this;
}
ObjectData *c_t::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_t::cloneImpl() {
  c_t *obj = NEW(c_t)();
  cloneSet(obj);
  return obj;
}
void c_t::cloneSet(c_t *clone) {
  clone->m_class = m_class;
  ObjectData::cloneSet(clone);
}
Variant c_t::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_t::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_t::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_t$os_get(const char *s) {
  return c_t::os_get(s, -1);
}
Variant &cw_t$os_lval(const char *s) {
  return c_t::os_lval(s, -1);
}
Variant cw_t$os_constant(const char *s) {
  return c_t::os_constant(s);
}
Variant cw_t$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_t::os_invoke(c, s, params, -1, fatal);
}
void c_t::init() {
  m_class = "";
}
/* SRC: roadsend/tests/bug-id-0000792.php line 7 */
void c_t::t_t() {
  INSTANCE_METHOD_INJECTION(t, t::t);
  bool oldInCtor = gasInCtor(true);
  (m_class = "woops");
  gasInCtor(oldInCtor);
} /* function */
Object co_t(CArrRef params, bool init /* = true */) {
  return Object(p_t(NEW(c_t)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000792_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000792.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000792_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("0000792 parse error using class property name 'class'\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
