
#ifndef __GENERATED_php_roadsend_tests_inc11_h__
#define __GENERATED_php_roadsend_tests_inc11_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc11.fw.h>

// Declarations
#include <cls/xfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc11_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_xfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc11_h__
