
#include <php/roadsend/tests/bug-id-0002131.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0002131_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0002131.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0002131_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bount __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bount") : g->GV(bount);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);

  echo("When incrementing inside an element of an array, the var ($count in this case) seams to get incremented by the number of elements being addressed.\n\ni.e. $count is 2 at the end here\n");
  (v_count = 0LL);
  lval(v_foo.lvalAt(++v_count)).set(0LL, ("bar"), 0x77CFA1EEF01BCA90LL);
  print(toString(v_count) + toString("\n"));
  (v_bount = 0LL);
  (v_zot = v_foo.rvalAt(++v_bount).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  print(toString(v_bount) + toString("\n"));
  echo("\nand $count is 3 at the end here\n");
  (v_count = 0LL);
  lval(lval(v_foo.lvalAt(++v_count)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(1LL, ("bar"), 0x5BCA7C69B794F8CELL);
  print(toString(v_count) + toString("\n"));
  echo("\n\ncheck the reference case, too:\n\n");
  (v_count = 0LL);
  lval(lval(v_foo.lvalAt(++v_count)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(3LL, (ref(v_count)), 0x135FDDF6A6BFBBDDLL);
  print(toString(v_count) + toString("\n"));
  LINE(29,x_var_dump(1, v_foo));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
