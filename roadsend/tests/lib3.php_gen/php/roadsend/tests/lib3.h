
#ifndef __GENERATED_php_roadsend_tests_lib3_h__
#define __GENERATED_php_roadsend_tests_lib3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/lib3.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$lib3_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_lib3func1(CVarRef v_a, CVarRef v_b);
void f_lib3func2(CVarRef v_a, CVarRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_lib3_h__
