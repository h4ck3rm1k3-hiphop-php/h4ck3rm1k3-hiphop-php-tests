
#ifndef __GENERATED_php_roadsend_tests_inc3_h__
#define __GENERATED_php_roadsend_tests_inc3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc3.fw.h>

// Declarations
#include <cls/pfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc3_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_pfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc3_h__
