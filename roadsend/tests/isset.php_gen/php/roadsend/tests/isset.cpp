
#include <php/roadsend/tests/isset.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$isset_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/isset.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$isset_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  if (isset(v_a)) {
    echo("This thing isn't working.\n");
  }
  (v_a = "foo");
  if (isset(v_a)) {
    echo("This thing _does_ work!\n");
  }
  if ((isset(v_a) && isset(v_b))) {
    echo("Multiple cows at night.\n");
  }
  (v_b = 4LL);
  (v_c = 3LL);
  (v_d = 9LL);
  if ((isset(v_a) && isset(v_b) && isset(v_c) && isset(v_d))) {
    echo("My hero!\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
