
#ifndef __GENERATED_cls_notparent_h__
#define __GENERATED_cls_notparent_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/static-methods.php line 3 */
class c_notparent : virtual public ObjectData {
  BEGIN_CLASS_MAP(notparent)
  END_CLASS_MAP(notparent)
  DECLARE_CLASS(notparent, notparent, ObjectData)
  void init();
  public: String m_nprop;
  public: void t_nmethod();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_notparent_h__
