
#ifndef __GENERATED_cls_b_h__
#define __GENERATED_cls_b_h__

#include <cls/a.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/static-methods.php line 66 */
class c_b : virtual public c_a {
  BEGIN_CLASS_MAP(b)
    PARENT_CLASS(a)
  END_CLASS_MAP(b)
  DECLARE_CLASS(b, B, a)
  void init();
  public: static void ti_foo(const char* cls);
  public: static void t_foo() { ti_foo("b"); }
  public: static void t_callfoo() { ti_callfoo("b"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_b_h__
