
#ifndef __GENERATED_cls_aclass_h__
#define __GENERATED_cls_aclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/static-methods.php line 13 */
class c_aclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(aclass)
  END_CLASS_MAP(aclass)
  DECLARE_CLASS(aclass, aclass, ObjectData)
  void init();
  public: String m_aprop;
  public: void t_aclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_amethod();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_aclass_h__
