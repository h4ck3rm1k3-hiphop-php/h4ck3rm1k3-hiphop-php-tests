
#ifndef __GENERATED_php_roadsend_tests_static_methods_h__
#define __GENERATED_php_roadsend_tests_static_methods_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/static-methods.fw.h>

// Declarations
#include <cls/notparent.h>
#include <cls/aclass.h>
#include <cls/bclass.h>
#include <cls/a.h>
#include <cls/b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$static_methods_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_notparent(CArrRef params, bool init = true);
Object co_aclass(CArrRef params, bool init = true);
Object co_bclass(CArrRef params, bool init = true);
Object co_a(CArrRef params, bool init = true);
Object co_b(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_static_methods_h__
