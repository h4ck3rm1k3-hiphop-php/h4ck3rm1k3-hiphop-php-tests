
#ifndef __GENERATED_php_roadsend_tests_declare_order_h__
#define __GENERATED_php_roadsend_tests_declare_order_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/declare-order.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$declare_order_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_hey(CStrRef v_a = "hello" /* foo::a */);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_declare_order_h__
