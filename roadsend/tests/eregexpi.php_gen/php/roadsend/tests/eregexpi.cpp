
#include <php/roadsend/tests/eregexpi.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$eregexpi_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/eregexpi.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$eregexpi_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_regs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("regs") : g->GV(regs);
  Variant &v_date __attribute__((__unused__)) = (variables != gVariables) ? variables->get("date") : g->GV(date);

  (v_string = "adsf asdfljasldkfj als alskj Abc asdfl jaskdjf lsdjg ");
  if (toBoolean(LINE(5,x_eregi("abc", toString(v_string))))) {
    echo("works\n");
  }
  if (toBoolean(LINE(11,x_eregi("^abc", toString(v_string))))) {
    echo("broken\n");
  }
  if (toBoolean(LINE(15,x_eregi("abc$", toString(v_string))))) {
    echo("broken\n");
  }
  (v_string = "adsf asdfljasldkfj als\n alskj aBc asdfl\n jaskdjf lsdjg abc");
  if (toBoolean(LINE(23,x_eregi("abc$", toString(v_string))))) {
    echo("works\n");
  }
  LINE(29,x_eregi("([[:alnum:]]+) ([[:alnum:]]+) ([[:alnum:]]+)", toString(v_string), ref(v_regs)));
  LINE(31,x_print_r(v_regs));
  print(toString((toString(v_string))));
  print(toString((toString(v_string))));
  (v_string = "adsf Asdfljasldkfj als\n alskj abc asdfl\n jaskdjf lsdjg abc");
  (v_string = LINE(50,x_eregi_replace("a", "zoot", toString(v_string))));
  print(toString((toString(v_string))));
  LINE(54,x_print_r(x_spliti(" ", toString(v_string), toInt32(3LL))));
  (v_date = "2003-07-03");
  if (toBoolean(LINE(59,x_eregi("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", toString(v_date), ref(v_regs))))) {
    echo(LINE(60,concat5(toString(v_regs.rvalAt(3, 0x135FDDF6A6BFBBDDLL)), ".", toString(v_regs.rvalAt(2, 0x486AFCC090D5F98CLL)), ".", toString(v_regs.rvalAt(1, 0x5BCA7C69B794F8CELL)))));
  }
  else {
    echo(toString("Invalid date format: ") + toString(v_date));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
