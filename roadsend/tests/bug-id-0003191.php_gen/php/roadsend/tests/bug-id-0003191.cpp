
#include <php/roadsend/tests/bug-id-0003191.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0003191_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003191.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003191_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = "/^(3\\.23|4\\.|5\\.)/");
  echo(LINE(3,concat3("pattern: ", toString(v_foo), "\n")));
  (v_foo = "/^(3\\.23|4\\.|5\\.)/");
  echo(LINE(5,concat3("pattern: ", toString(v_foo), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
