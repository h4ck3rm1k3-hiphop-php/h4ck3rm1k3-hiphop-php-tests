
#ifndef __GENERATED_php_roadsend_tests_ftpupload_h__
#define __GENERATED_php_roadsend_tests_ftpupload_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/ftpupload.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$ftpupload_php(bool incOnce = false, LVariableTable* variables = NULL);
int f_curl_upload(CStrRef v_src);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_ftpupload_h__
