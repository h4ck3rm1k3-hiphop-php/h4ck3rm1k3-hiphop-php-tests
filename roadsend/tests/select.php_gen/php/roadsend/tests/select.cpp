
#include <php/roadsend/tests/select.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$select_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/select.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$select_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_query __attribute__((__unused__)) = (variables != gVariables) ? variables->get("query") : g->GV(query);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);
  Variant &v_col_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("col_name") : g->GV(col_name);
  Variant &v_col_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("col_value") : g->GV(col_value);

  (toBoolean((v_link = LINE(4,x_mysql_connect("localhost", "develUser", "d3v3lpa55"))))) || (toBoolean(f_exit("Could not connect")));
  print("Connected successfully\n");
  (toBoolean(LINE(9,x_mysql_select_db("test")))) || (toBoolean(f_exit("Could not select database")));
  (v_query = "SELECT * FROM my_table");
  (toBoolean((v_result = LINE(15,x_mysql_query(toString(v_query)))))) || (toBoolean(f_exit("Query failed")));
  print("<table>\n");
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_line = LINE(21,x_mysql_fetch_array(v_result, toInt32(3LL) /* MYSQL_BOTH */))))) {
      LOOP_COUNTER_CHECK(1);
      {
        print("\t<tr>\n");
        {
          LOOP_COUNTER(2);
          for (ArrayIterPtr iter4 = v_line.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_col_value = iter4->second();
            v_col_name = iter4->first();
            {
              print(LINE(24,concat5("\t\t<td>", toString(v_col_name), " => ", toString(v_col_value), "</td>\n")));
            }
          }
        }
        print("\t</tr>\n");
      }
    }
  }
  print("</table>\n");
  LINE(31,x_mysql_free_result(v_result));
  LINE(34,x_mysql_close(v_link));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
