
#include <php/roadsend/tests/bug-id-0001185.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001185_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001185.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001185_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  echo("0001185 here doc escaping\n\nphp escapes \\\\ to \\ within heredocs\n\n");
  (v_test = toString("this is a test\n\\\\ \\\\ \\\\\\ \\\n\\a\\b\\c\\d\\e\f\\g\nlast line"));
  echo(toString(v_test));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
