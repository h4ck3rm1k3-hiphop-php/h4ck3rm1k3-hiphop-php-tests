
#include <php/roadsend/tests/bug-id-0001861.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001861_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001861.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001861_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_char __attribute__((__unused__)) = (variables != gVariables) ? variables->get("char") : g->GV(char);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_testcases __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testcases") : g->GV(testcases);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  echo("PHP follows Perl's convention when dealing with arithmetic operations on character variables and not C's. For example, in Perl 'Z'+1 turns into 'AA', while in C 'Z'+1 turns into '[' ( ord('Z') == 90, ord('[') == 91 ). Note that character variables can be incremented but not decremented.\n\nAdditional Information \t\n");
  echo("1\n");
  echo("1\n");
  echo("1\n");
  (v_char = "A");
  v_char++;
  echo(concat(toString((toString(v_char))), "\n"));
  (v_char = "A");
  ++v_char;
  echo(concat(toString((toString(v_char))), "\n"));
  (v_char = "Z");
  v_char++;
  echo(concat(toString((toString(v_char))), "\n"));
  (v_char = "ZZ");
  v_char++;
  echo(concat(toString((toString(v_char))), "\n"));
  (v_char = "Z2Z");
  v_char++;
  echo(concat(toString((toString(v_char))), "\n"));
  (v_char = " Z");
  v_char++;
  echo(concat(toString((toString(v_char))), "\n"));
  {
    LOOP_COUNTER(1);
    for ((v_i = "A"), (v_t = 0LL); !equal(v_i, "AAA") && !equal(v_t, 100LL); v_i++, v_t++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(v_i) + toString("\n"));
      }
    }
  }
  echo("\nsome more tests\n\n");
  (v_testcases = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_testcases.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_test = iter4->second();
      {
        echo(LINE(46,concat3("testcase ", toString(v_test), "\n")));
        {
          LOOP_COUNTER(5);
          for ((v_i = 0LL); less(v_i, 5LL); v_i++) {
            LOOP_COUNTER_CHECK(5);
            {
              echo(concat(toString(++v_test), "\n"));
            }
          }
        }
        echo("--\n");
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
