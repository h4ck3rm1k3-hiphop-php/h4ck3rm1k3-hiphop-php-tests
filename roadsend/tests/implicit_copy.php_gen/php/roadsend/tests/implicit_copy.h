
#ifndef __GENERATED_php_roadsend_tests_implicit_copy_h__
#define __GENERATED_php_roadsend_tests_implicit_copy_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/implicit_copy.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$implicit_copy_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_incarray(Variant v_arr);
void f_incvar(Variant v_var);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_implicit_copy_h__
