
#include <php/roadsend/tests/znested_functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/znested_functions.php line 2 */
String f_f() {
  FUNCTION_INJECTION(F);
  String v_a;

  (v_a = "Hello ");
  return (v_a);
} /* function */
/* SRC: roadsend/tests/znested_functions.php line 8 */
void f_g() {
  FUNCTION_INJECTION(G);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_myvar __attribute__((__unused__)) = g->sv_g_DupIdmyvar;
  bool &inited_sv_myvar __attribute__((__unused__)) = g->inited_sv_g_DupIdmyvar;
  if (!inited_sv_myvar) {
    (sv_myvar = 4LL);
    inited_sv_myvar = true;
  }
  echo(toString(sv_myvar) + toString(" "));
  echo(LINE(13,f_f()));
  echo(toString(sv_myvar));
} /* function */
Variant pm_php$roadsend$tests$znested_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/znested_functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$znested_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(17,f_g());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
