
#include <php/roadsend/tests/stringtest.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$stringtest_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/stringtest.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$stringtest_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_zap __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zap") : g->GV(zap);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo(LINE(2,concat5("this $ is a ", toString(v_zap.rvalAt("zowie", 0x0BEBB58A65AF7ED4LL)), " dqstring ", toString(v_foo), "\n\"$")));
  echo("this is a [test] of brackets");
  echo("double \\\n      quoted \\\n      with \\\n      backslash-newlines");
  echo("single \\\n      quoted \\\n      with \\\n      backslash-newlines");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
