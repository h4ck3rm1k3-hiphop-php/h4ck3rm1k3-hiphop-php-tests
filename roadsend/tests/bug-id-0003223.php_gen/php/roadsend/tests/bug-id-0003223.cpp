
#include <php/roadsend/tests/bug-id-0003223.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0003223.php line 3 */
void f_afun() {
  FUNCTION_INJECTION(afun);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_str;
  Variant v_lastiteration;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_str; Variant &v_lastiteration;
    VariableTable(Variant &r_str, Variant &r_lastiteration) : v_str(r_str), v_lastiteration(r_lastiteration) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 3) {
        case 0:
          HASH_RETURN(0x362158638F83BD9CLL, v_str,
                      str);
          break;
        case 3:
          HASH_RETURN(0x30CD198F190CAE13LL, v_lastiteration,
                      lastiteration);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_str, v_lastiteration);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  concat_assign(v_str, "zoot");
  f_eval(LINE(5,concat3("$lastiteration = sizeof(", toString(v_str), ") - 1;")));
  echo(LINE(6,concat3("lastiteration is ", toString(v_lastiteration), "\n")));
} /* function */
Variant pm_php$roadsend$tests$bug_id_0003223_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003223.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003223_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_sizeof("Asdf"));
  LINE(8,f_afun());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
