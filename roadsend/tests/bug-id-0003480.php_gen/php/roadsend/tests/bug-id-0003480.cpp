
#include <php/roadsend/tests/bug-id-0003480.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0003480_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003480.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003480_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_bigkey __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bigkey") : g->GV(bigkey);
  Variant &v_bigkeyneg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bigkeyneg") : g->GV(bigkeyneg);
  Variant &v_bignumkey __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bignumkey") : g->GV(bignumkey);
  Variant &v_bignumkeyneg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bignumkeyneg") : g->GV(bignumkeyneg);
  Variant &v_leadzero __attribute__((__unused__)) = (variables != gVariables) ? variables->get("leadzero") : g->GV(leadzero);
  Variant &v_float __attribute__((__unused__)) = (variables != gVariables) ? variables->get("float") : g->GV(float);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  print("int max: 9.2233720368548E+18\n");
  print("int min: 8\n");
  (v_key = 9223372036854775800.0 /* PHP_INT_MAX */);
  v_a.set(v_key, (v_key));
  v_key++;
  print(LINE(11,concat3("overflow key: ", toString(v_key), "\n")));
  v_b.set(v_key, (v_key));
  (v_bigkey = "9999999999999999999999999999999999999");
  (v_bigkeyneg = "-9999999999999999999999999999999999999");
  (v_bignumkey = 9223372036854775807LL);
  (v_bignumkeyneg = -9223372036854775807LL);
  (v_leadzero = "00567");
  (v_float = 1.3217467000000001);
  LINE(19,x_var_dump(1, v_bignumkey));
  v_c.set(v_bigkey, ("foo"));
  v_c.set(v_bignumkey, ("bar"));
  v_c.set(v_bignumkeyneg, ("barzap"));
  v_c.set(v_bigkeyneg, ("bip"));
  v_c.set(v_leadzero, ("baz"));
  v_c.set(v_float, ("f"));
  LINE(26,x_print_r(v_a));
  LINE(27,x_print_r(v_b));
  LINE(28,x_print_r(v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
