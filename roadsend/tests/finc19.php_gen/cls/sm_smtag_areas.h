
#ifndef __GENERATED_cls_sm_smtag_areas_h__
#define __GENERATED_cls_sm_smtag_areas_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/finc19.php line 23 */
class c_sm_smtag_areas : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_areas)
  END_CLASS_MAP(sm_smtag_areas)
  DECLARE_CLASS(sm_smtag_areas, SM_smTag_AREAs, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_areas_h__
