
#ifndef __GENERATED_php_roadsend_tests_registry_fw_h__
#define __GENERATED_php_roadsend_tests_registry_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_PCC_VERSION_MINOR;
extern const StaticString k_HKEY_LOCAL_MACHINE;
extern const StaticString k_HKEY_CURRENT_USER;
extern const StaticString k_PCC_VERSION_MAJOR;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_registry_fw_h__
