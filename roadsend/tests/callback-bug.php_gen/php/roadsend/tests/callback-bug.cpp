
#include <php/roadsend/tests/callback-bug.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/callback-bug.php line 3 */
Variant f_foo(CVarRef v_matches) {
  FUNCTION_INJECTION(foo);
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_i = 0;

  (v_i = lval(g->GV(botMosImageCount))++);
  return (silenceInc(), silenceDec(g->GV(botMosImageArray).rvalAt(v_i)));
} /* function */
Variant i_foo(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6176C0B993BF5914LL, foo) {
    return (f_foo(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$callback_bug_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/callback-bug.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$callback_bug_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_text __attribute__((__unused__)) = (variables != gVariables) ? variables->get("text") : g->GV(text);

  (v_text = "April fools day is 04/01/2002\n");
  echo(toString(LINE(7,x_preg_replace_callback("/foo/", "foo", v_text))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
