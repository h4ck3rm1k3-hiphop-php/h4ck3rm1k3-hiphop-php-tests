
#ifndef __GENERATED_php_roadsend_tests_callback_bug_h__
#define __GENERATED_php_roadsend_tests_callback_bug_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/callback-bug.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$callback_bug_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_foo(CVarRef v_matches);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_callback_bug_h__
