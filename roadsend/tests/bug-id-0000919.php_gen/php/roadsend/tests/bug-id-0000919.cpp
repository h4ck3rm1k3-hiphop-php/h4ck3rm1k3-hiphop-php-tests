
#include <php/roadsend/tests/bug-id-0000919.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000919.php line 6 */
void f_useit() {
  FUNCTION_INJECTION(useit);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_aglobal __attribute__((__unused__)) = g->GV(aglobal);
  ;
} /* function */
Variant pm_php$roadsend$tests$bug_id_0000919_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000919.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000919_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_aglobal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("aglobal") : g->GV(aglobal);

  echo("unreferenced magic global stuff doesn't work.\n\n");
  (v_aglobal = "aglobal");
  print(concat(toString(v_aglobal), "\n"));
  LINE(19,f_useit());
  print(concat(toString(v_aglobal), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
