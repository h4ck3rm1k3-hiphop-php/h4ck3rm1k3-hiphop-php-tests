
#include <php/roadsend/tests/strtotime.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$strtotime_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/strtotime.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$strtotime_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_now __attribute__((__unused__)) = (variables != gVariables) ? variables->get("now") : g->GV(now);
  Variant &v_snow __attribute__((__unused__)) = (variables != gVariables) ? variables->get("snow") : g->GV(snow);
  Variant &v_dates __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dates") : g->GV(dates);
  Variant &v_date __attribute__((__unused__)) = (variables != gVariables) ? variables->get("date") : g->GV(date);

  (v_now = 1066316052LL);
  (v_snow = LINE(6,x_date("l dS of F Y h:i:s A", toInt64(v_now))));
  echo(LINE(7,concat5("now starts out as ", toString(v_now), " which is ", toString(v_snow), "\n")));
  {
    echo(toString(LINE(10,x_strtotime("now", toInt64(v_now)))));
    echo("\n");
  }
  {
    echo(toString(LINE(11,x_strtotime("10 September 2000", toInt64(v_now)))));
    echo("\n");
  }
  {
    echo(toString(LINE(12,x_strtotime("+1 day", toInt64(v_now)))));
    echo("\n");
  }
  {
    echo(toString(LINE(13,x_strtotime("+1 week", toInt64(v_now)))));
    echo("\n");
  }
  {
    echo(toString(LINE(18,x_strtotime("+1 week 2 days 4 hours 2 seconds", toInt64(v_now)))));
    echo("\n");
  }
  {
    echo(toString(LINE(22,x_strtotime("next Thursday", toInt64(v_now)))));
    echo("\n");
  }
  {
    echo(toString(LINE(23,x_strtotime("last Monday", toInt64(v_now)))));
    echo("\n");
  }
  (v_dates = ScalarArrays::sa_[0]);
  echo("gmt:\n");
  LINE(47,x_putenv("TZ=GMT"));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_dates.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_date = iter3->second();
      {
        echo(LINE(49,(assignCallTemp(eo_1, toInt64(x_strtotime(toString(v_date)))),x_date("Y-m-d H:i:s\n", eo_1))));
      }
    }
  }
  echo("us/eastern\n");
  LINE(54,x_putenv("TZ=US/Eastern"));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_dates.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_date = iter6->second();
      {
        echo(LINE(57,(assignCallTemp(eo_1, toInt64(x_strtotime(toString(v_date)))),x_date("Y-m-d H:i:s\n", eo_1))));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
