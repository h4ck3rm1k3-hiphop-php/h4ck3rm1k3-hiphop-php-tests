
#ifndef __GENERATED_php_roadsend_tests_bug_id_0002492_h__
#define __GENERATED_php_roadsend_tests_bug_id_0002492_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0002492.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_evenless(CVarRef v_target);
Variant pm_php$roadsend$tests$bug_id_0002492_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_pma_gpc_extract(CVarRef v_array, Variant v_target);
void f_nothing(Variant v_target);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0002492_h__
