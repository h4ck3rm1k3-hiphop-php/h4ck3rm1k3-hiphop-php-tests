
#ifndef __GENERATED_php_roadsend_tests_inc4_h__
#define __GENERATED_php_roadsend_tests_inc4_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc4.fw.h>

// Declarations
#include <cls/qfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc4_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_qfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc4_h__
