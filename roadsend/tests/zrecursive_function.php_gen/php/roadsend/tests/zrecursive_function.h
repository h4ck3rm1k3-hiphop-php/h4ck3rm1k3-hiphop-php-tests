
#ifndef __GENERATED_php_roadsend_tests_zrecursive_function_h__
#define __GENERATED_php_roadsend_tests_zrecursive_function_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zrecursive_function.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$zrecursive_function_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_test();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zrecursive_function_h__
