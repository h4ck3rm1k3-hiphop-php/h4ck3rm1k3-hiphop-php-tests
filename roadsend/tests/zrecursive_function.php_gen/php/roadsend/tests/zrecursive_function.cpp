
#include <php/roadsend/tests/zrecursive_function.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/zrecursive_function.php line 1 */
void f_test() {
  FUNCTION_INJECTION(Test);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_a __attribute__((__unused__)) = g->sv_test_DupIda;
  bool &inited_sv_a __attribute__((__unused__)) = g->inited_sv_test_DupIda;
  if (!inited_sv_a) {
    (sv_a = 1LL);
    inited_sv_a = true;
  }
  echo(toString(sv_a) + toString(" "));
  sv_a++;
  if (less(sv_a, 10LL)) {
    LINE(7,f_test());
  }
} /* function */
Variant pm_php$roadsend$tests$zrecursive_function_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zrecursive_function.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zrecursive_function_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(9,f_test());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
