
#include <php/roadsend/tests/enums.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$enums_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/enums.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$enums_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Just check if some random enum works\n");
  if (!(LINE(3,x_extension_loaded("php-gtk")))) {
    LINE(4,x_dl("php_gtk.so"));
  }
  print(concat(throw_fatal("unknown class constant gtk::BUTTONS_CLOSE"), "\n"));
  print(concat(throw_fatal("unknown class constant gdk::BUTTON1_MASK"), "\n"));
  print(concat(throw_fatal("unknown class constant gdk::WINDOW_TOPLEVEL"), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
