
#include <php/roadsend/tests/simple-string.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$simple_string_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/simple-string.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$simple_string_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);

  v_foo.set(1LL, (2LL), 0x5BCA7C69B794F8CELL);
  v_foo.set(2LL, (1LL), 0x486AFCC090D5F98CLL);
  (v_zot = 2LL);
  echo(LINE(8,concat3(toString(v_foo.rvalAt(1, 0x5BCA7C69B794F8CELL)), " ", toString(v_foo.rvalAt(v_zot)))));
  echo(LINE(10,concat3(toString(v_zot), " ", toString(v_zot))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
