
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_IDYES;
extern const StaticString k_MB_YESNOCANCEL;
extern const StaticString k_IDNO;
extern const StaticString k_IDCANCEL;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x14EC515EAAA03A19LL, k_IDYES, IDYES);
      break;
    case 2:
      HASH_RETURN(0x240D166FF8D9D9E2LL, k_IDNO, IDNO);
      break;
    case 3:
      HASH_RETURN(0x487654FABD9BDC03LL, k_MB_YESNOCANCEL, MB_YESNOCANCEL);
      HASH_RETURN(0x32C542381EF7B9E3LL, k_IDCANCEL, IDCANCEL);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
