
#ifndef __GENERATED_php_roadsend_tests_scribble_h__
#define __GENERATED_php_roadsend_tests_scribble_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/scribble.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_motion_notify_event(CVarRef v_widget, CVarRef v_event);
bool f_expose_event(CVarRef v_widget, CVarRef v_event);
void f_draw_brush(CVarRef v_widget, int64 v_x, int64 v_y);
Variant pm_php$roadsend$tests$scribble_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_button_press_event(CVarRef v_widget, CVarRef v_event);
bool f_configure_event(CVarRef v_widget, CVarRef v_event);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_scribble_h__
