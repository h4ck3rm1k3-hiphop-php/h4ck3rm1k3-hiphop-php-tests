
#ifndef __GENERATED_php_roadsend_tests_scribble_fw_h__
#define __GENERATED_php_roadsend_tests_scribble_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_GTK_WIN_POS_CENTER;
extern const StaticString k_GDK_LEAVE_NOTIFY_MASK;
extern const StaticString k_GDK_BUTTON_PRESS_MASK;
extern const StaticString k_GDK_EXPOSURE_MASK;
extern const StaticString k_GTK_STATE_NORMAL;
extern const StaticString k_GDK_POINTER_MOTION_MASK;
extern const StaticString k_GDK_BUTTON1_MASK;
extern const StaticString k_GDK_POINTER_MOTION_HINT_MASK;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_scribble_fw_h__
