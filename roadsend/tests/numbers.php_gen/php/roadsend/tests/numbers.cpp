
#include <php/roadsend/tests/numbers.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/numbers.php line 3 */
void f_test_num(CStrRef v_title, double v_a) {
  FUNCTION_INJECTION(test_num);
  print(LINE(4,concat4(v_title, " : ", toString(v_a), "\n")));
  LINE(5,x_var_dump(1, v_a));
} /* function */
Variant pm_php$roadsend$tests$numbers_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/numbers.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$numbers_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_large_number __attribute__((__unused__)) = (variables != gVariables) ? variables->get("large_number") : g->GV(large_number);
  Variant &v_million __attribute__((__unused__)) = (variables != gVariables) ? variables->get("million") : g->GV(million);
  Variant &v_ln __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ln") : g->GV(ln);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  (v_test = 25LL);
  LINE(9,x_var_dump(1, v_test));
  LINE(11,f_test_num("dec1", toDouble(536870912LL)));
  LINE(12,f_test_num("dec2", toDouble(536LL)));
  LINE(13,f_test_num("dec3", toDouble(11687091398LL)));
  LINE(14,f_test_num("dec4", toDouble(191687091398LL)));
  LINE(15,f_test_num("dec5", toDouble(1342177280LL)));
  (v_a = 7LL);
  (v_b = 474LL);
  LINE(19,x_var_dump(1, v_a));
  LINE(20,x_var_dump(1, v_b));
  LINE(22,f_test_num("hex1", toDouble(65535LL)));
  LINE(23,f_test_num("hex2 ", 0.0));
  LINE(24,f_test_num("hex2e", 0.0));
  LINE(25,f_test_num("hex2a", toDouble(268435455LL)));
  LINE(26,f_test_num("hex2b", toDouble(16777215LL)));
  LINE(27,f_test_num("hex2c", 0.0));
  LINE(28,f_test_num("hex2d", 0.0));
  LINE(29,f_test_num("hex3 ", 0.0));
  LINE(30,f_test_num("hex4a", 0.0));
  LINE(31,f_test_num("hex4b", toDouble(9223372036854775807LL)));
  LINE(33,f_test_num("oct1:", toDouble(33554430LL)));
  LINE(34,f_test_num("oct2:", 155555555554.0));
  LINE(36,f_test_num("fl1", 18642785878586.441));
  LINE(37,f_test_num("fl2:", 23303.058000000001));
  LINE(38,f_test_num("fl3:", 1.234));
  LINE(39,f_test_num("fl4:", 1200.0));
  LINE(40,f_test_num("fl5:", 6.9999999999999996E-10));
  LINE(41,f_test_num("fl5a:", 6.9999999999999996E-10));
  LINE(42,f_test_num("fl5b:", 6.9999999999999999E-6));
  LINE(43,f_test_num("fl6:", 7.243E+10));
  LINE(44,f_test_num("fl7:", 3.0E+5));
  LINE(45,f_test_num("fl8:", 3.0000000000000001E-5));
  (v_large_number = 2147483647LL);
  echo("p1:");
  LINE(49,x_var_dump(1, v_large_number));
  (v_large_number = 2147483648.0);
  echo("p2:");
  LINE(54,x_var_dump(1, v_large_number));
  (v_million = 1000000LL);
  (v_large_number = 50000LL * v_million);
  echo("p4:");
  LINE(68,x_var_dump(1, v_large_number));
  echo("p5:");
  (v_ln = -50000LL * v_million);
  LINE(72,x_var_dump(1, v_ln));
  echo("p6:");
  LINE(74,x_var_dump(1, 3.5714285714285716));
  echo("p7:");
  LINE(76,x_var_dump(1, 3LL));
  echo("p8:");
  echo(toString(7LL));
  echo("\n");
  LINE(84,f_test_num("of1:", toDouble(1000000000000LL)));
  LINE(85,f_test_num("of1a:", toDouble(4611686014132420609LL)));
  LINE(86,f_test_num("of1b:", 4611686042049708000.0));
  LINE(87,f_test_num("of1c:", -4611686042049708000.0));
  LINE(88,f_test_num("of2:", 4294967297.0));
  LINE(89,f_test_num("of3:", toDouble(4294967294LL)));
  LINE(90,f_test_num("of3a:", toDouble(-4294967294LL)));
  LINE(91,f_test_num("of4:", toDouble(-400000000LL)));
  LINE(92,f_test_num("of5:", toDouble(1000LL)));
  LINE(95,f_test_num("el1:", 139.15151515151516));
  LINE(96,f_test_num("el2:", toDouble(32375LL)));
  LINE(97,f_test_num("el3:", toDouble(7425264LL)));
  LINE(98,f_test_num("el4:", toDouble(31913LL)));
  v_c.append((5LL));
  v_c.append((-12.0));
  v_c.append((4.0));
  v_c.append((12.5));
  LINE(105,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
