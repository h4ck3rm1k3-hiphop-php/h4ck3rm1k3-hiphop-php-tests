
#ifndef __GENERATED_cls_bling_h__
#define __GENERATED_cls_bling_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0000959.php line 5 */
class c_bling : virtual public ObjectData {
  BEGIN_CLASS_MAP(bling)
  END_CLASS_MAP(bling)
  DECLARE_CLASS(bling, bling, ObjectData)
  void init();
  public: String t___tostring();
  public: void t_bling();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_wibble();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bling_h__
