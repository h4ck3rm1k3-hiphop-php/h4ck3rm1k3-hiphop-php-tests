
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__

#include <cls/bling.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0000959.php line 22 */
class c_foo : virtual public c_bling {
  BEGIN_CLASS_MAP(foo)
    PARENT_CLASS(bling)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, foo, bling)
  void init();
  public: int64 m_zot;
  public: String t___tostring();
  public: void t_foo();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_wibble();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
