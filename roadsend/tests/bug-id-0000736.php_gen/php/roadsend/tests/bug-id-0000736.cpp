
#include <php/roadsend/tests/bug-id-0000736.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000736.php line 37 */
Variant c_blah::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_blah::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_blah::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("style", m_style));
  c_ObjectData::o_get(props);
}
bool c_blah::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x040EF9119982696ALL, style, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_blah::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x040EF9119982696ALL, m_style,
                         style, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_blah::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x040EF9119982696ALL, m_style,
                      style, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_blah::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_blah::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(blah)
ObjectData *c_blah::cloneImpl() {
  c_blah *obj = NEW(c_blah)();
  cloneSet(obj);
  return obj;
}
void c_blah::cloneSet(c_blah *clone) {
  clone->m_style = m_style;
  ObjectData::cloneSet(clone);
}
Variant c_blah::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x759A97C37FAD53E5LL, style) {
        return (t_style());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_blah::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x759A97C37FAD53E5LL, style) {
        return (t_style());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_blah::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_blah$os_get(const char *s) {
  return c_blah::os_get(s, -1);
}
Variant &cw_blah$os_lval(const char *s) {
  return c_blah::os_lval(s, -1);
}
Variant cw_blah$os_constant(const char *s) {
  return c_blah::os_constant(s);
}
Variant cw_blah$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_blah::os_invoke(c, s, params, -1, fatal);
}
void c_blah::init() {
  m_style = null;
}
/* SRC: roadsend/tests/bug-id-0000736.php line 40 */
String c_blah::t_style() {
  INSTANCE_METHOD_INJECTION(blah, blah::style);
  String v_text;
  String v_sName;

  (v_text = "my text");
  (v_sName = "mystyle");
  m_style.set(v_sName, ("meep"));
  (v_text = LINE(44,concat5("<span class=\"", toString(m_style.rvalAt(v_sName)), "\">", v_text, "</span>\n")));
  return v_text;
} /* function */
Object co_blah(CArrRef params, bool init /* = true */) {
  return Object(p_blah(NEW(c_blah)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000736_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000736.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000736_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_columnConfig __attribute__((__unused__)) = (variables != gVariables) ? variables->get("columnConfig") : g->GV(columnConfig);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);
  Variant &v_postfix __attribute__((__unused__)) = (variables != gVariables) ? variables->get("postfix") : g->GV(postfix);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("0000736\n\nthis is the core of the issue:\n");
  echo(toString(v_columnConfig.rvalAt(v_name).rvalAt("postfix", 0x1ACF958312278C2ALL)));
  echo("\n parse error on ridiculous string assignment \n\n yes, this was pulled from actual code...\n\n");
  (v_name = "hi");
  lval(v_columnConfig.lvalAt(v_name)).set("postFix", ("test"), 0x1889C983CCC83DA0LL);
  if (!(empty(v_columnConfig.rvalAt(v_name), "postfix", 0x1ACF958312278C2ALL))) {
    (v_postfix = toString(v_columnConfig.rvalAt(v_name).rvalAt("postfix", 0x1ACF958312278C2ALL)));
  }
  else {
    (v_postfix = "nope");
  }
  echo(toString(v_postfix));
  echo("\n\n\ni believe this is the same parse problem, here is another example\n\n");
  (v_b = ((Object)(LINE(49,p_blah(p_blah(NEWOBJ(c_blah)())->create())))));
  echo(toString(LINE(50,v_b.o_invoke_few_args("style", 0x759A97C37FAD53E5LL, 0))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
