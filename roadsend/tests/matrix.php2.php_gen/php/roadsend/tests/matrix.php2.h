
#ifndef __GENERATED_php_roadsend_tests_matrix_php2_h__
#define __GENERATED_php_roadsend_tests_matrix_php2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/matrix.php2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_mmult(CVarRef v_rows, CVarRef v_cols, CVarRef v_m1, CVarRef v_m2);
Variant f_mkmatrix(CVarRef v_rows, CVarRef v_cols);
Variant pm_php$roadsend$tests$matrix_php2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_matrix_php2_h__
