
#include <php/roadsend/tests/bug-id-0001841.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001841.php line 3 */
void f_foo(int64 v_arg) {
  FUNCTION_INJECTION(foo);
  LINE(4,x_var_dump(1, v_arg));
} /* function */
Variant pm_php$roadsend$tests$bug_id_0001841_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001841.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001841_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(8,invoke_failed("foo", Array(), 0x0000000093BF5914LL));
  LINE(11,invoke_too_many_args("foo", (2), ((f_foo(1LL)), null)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
