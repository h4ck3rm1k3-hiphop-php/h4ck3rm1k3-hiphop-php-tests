
#include <php/roadsend/tests/bug-id-0001116.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001116_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001116.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001116_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);

  print("Testing NULL...\n");
  setNull(v_arr);
  print(LINE(4,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(0LL) /* COUNT_NORMAL */))),concat3("COUNT_NORMAL: should be 0, is ", eo_1, "\n"))));
  print(LINE(5,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(1LL) /* COUNT_RECURSIVE */))),concat3("COUNT_RECURSIVE: should be 0, is ", eo_1, "\n"))));
  print("Testing arrays...\n");
  (v_arr = ScalarArrays::sa_[0]);
  print(LINE(9,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(0LL) /* COUNT_NORMAL */))),concat3("COUNT_NORMAL: should be 2, is ", eo_1, "\n"))));
  print(LINE(10,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(1LL) /* COUNT_RECURSIVE */))),concat3("COUNT_RECURSIVE: should be 8, is ", eo_1, "\n"))));
  print("Testing hashes...\n");
  (v_arr = ScalarArrays::sa_[1]);
  print(LINE(14,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(0LL) /* COUNT_NORMAL */))),concat3("COUNT_NORMAL: should be 3, is ", eo_1, "\n"))));
  print(LINE(15,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(1LL) /* COUNT_RECURSIVE */))),concat3("COUNT_RECURSIVE: should be 6, is ", eo_1, "\n"))));
  print("Testing strings...\n");
  print(LINE(18,(assignCallTemp(eo_1, toString(x_count("string", toBoolean(0LL) /* COUNT_NORMAL */))),concat3("COUNT_NORMAL: should be 1, is ", eo_1, "\n"))));
  print(LINE(19,(assignCallTemp(eo_1, toString(x_count("string", toBoolean(1LL) /* COUNT_RECURSIVE */))),concat3("COUNT_RECURSIVE: should be 1, is ", eo_1, "\n"))));
  print("Testing various types with no second argument.\n");
  print(LINE(22,(assignCallTemp(eo_1, toString(x_count("string"))),concat3("COUNT_NORMAL: should be 1, is ", eo_1, "\n"))));
  print(LINE(23,(assignCallTemp(eo_1, toString(x_count(ScalarArrays::sa_[3]))),concat3("COUNT_NORMAL: should be 2, is ", eo_1, "\n"))));
  (v_arr = ScalarArrays::sa_[2]);
  print("Testing really cool arrays ;)\n");
  print(LINE(28,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(0LL) /* COUNT_NORMAL */))),concat3("COUNT_NORMAL: should be 3, is ", eo_1, "\n"))));
  print(LINE(29,(assignCallTemp(eo_1, toString(x_count(v_arr, toBoolean(1LL) /* COUNT_RECURSIVE */))),concat3("COUNT_RECURSIVE: should be 13, is ", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
