
#include <php/roadsend/tests/split.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3, Variant &p4) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  p4 = a0.rvalAt(4);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$roadsend$tests$split_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/split.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$split_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_passwd_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("passwd_line") : g->GV(passwd_line);
  Variant &v_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("user") : g->GV(user);
  Variant &v_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pass") : g->GV(pass);
  Variant &v_uid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("uid") : g->GV(uid);
  Variant &v_gid __attribute__((__unused__)) = (variables != gVariables) ? variables->get("gid") : g->GV(gid);
  Variant &v_extra __attribute__((__unused__)) = (variables != gVariables) ? variables->get("extra") : g->GV(extra);
  Variant &v_date __attribute__((__unused__)) = (variables != gVariables) ? variables->get("date") : g->GV(date);
  Variant &v_month __attribute__((__unused__)) = (variables != gVariables) ? variables->get("month") : g->GV(month);
  Variant &v_day __attribute__((__unused__)) = (variables != gVariables) ? variables->get("day") : g->GV(day);
  Variant &v_year __attribute__((__unused__)) = (variables != gVariables) ? variables->get("year") : g->GV(year);

  (v_passwd_line = "www-data:x:33:33:www-data:/var/www:/bin/sh");
  df_lambda_1(LINE(5,x_split(":", toString(v_passwd_line), toInt32(5LL))), v_user, v_pass, v_uid, v_gid, v_extra);
  echo(concat_rev(LINE(7,concat6(toString(v_uid), ",", toString(v_gid), ",", toString(v_extra), "\n")), concat4(toString(v_user), ",", toString(v_pass), ",")));
  (v_date = "04/30/1973");
  df_lambda_2(LINE(11,x_split("[/.-]", toString(v_date))), v_month, v_day, v_year);
  echo(concat("Month: ", LINE(12,concat6(toString(v_month), "; Day: ", toString(v_day), "; Year: ", toString(v_year), "<br>\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
