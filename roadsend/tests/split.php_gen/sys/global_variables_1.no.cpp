
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 2:
      HASH_RETURN(0x160449C754173842LL, g->GV(extra),
                  extra);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x58C7226464D57F09LL, g->GV(date),
                  date);
      break;
    case 10:
      HASH_RETURN(0x2B7634BE3D12BDCALL, g->GV(uid),
                  uid);
      break;
    case 12:
      HASH_RETURN(0x3E9D67504EE78ACCLL, g->GV(month),
                  month);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 15:
      HASH_RETURN(0x7B6498803C0AACCFLL, g->GV(passwd_line),
                  passwd_line);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 27:
      HASH_RETURN(0x5B22A7472448739BLL, g->GV(gid),
                  gid);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      HASH_RETURN(0x53052C743152AAE3LL, g->GV(user),
                  user);
      break;
    case 40:
      HASH_RETURN(0x0E12BE46FD64D268LL, g->GV(day),
                  day);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 51:
      HASH_RETURN(0x70860AB53D1BA673LL, g->GV(pass),
                  pass);
      break;
    case 54:
      HASH_RETURN(0x28D044C5EA490CB6LL, g->GV(year),
                  year);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
