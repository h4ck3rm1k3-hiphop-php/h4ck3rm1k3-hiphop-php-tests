
#ifndef __GENERATED_php_roadsend_tests_finc7_h__
#define __GENERATED_php_roadsend_tests_finc7_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/finc7.fw.h>

// Declarations
#include <cls/sm_smtag_areag.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$finc7_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_areag(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_finc7_h__
