
#include <php/roadsend/tests/zdirname.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/zdirname.php line 3 */
void f_check_dirname(CStrRef v_path) {
  FUNCTION_INJECTION(check_dirname);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  print(LINE(5,(assignCallTemp(eo_1, v_path),assignCallTemp(eo_3, x_dirname(v_path)),concat5("dirname(", eo_1, ") == ", eo_3, "\n"))));
} /* function */
Variant pm_php$roadsend$tests$zdirname_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zdirname.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zdirname_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(10,f_check_dirname("/foo"));
  LINE(11,f_check_dirname("/foo/bar"));
  LINE(12,f_check_dirname("d:\\foo\\bar.inc"));
  LINE(13,f_check_dirname("/"));
  LINE(14,f_check_dirname(".../foo"));
  LINE(15,f_check_dirname("./foo"));
  LINE(17,f_check_dirname("c:\foo"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
