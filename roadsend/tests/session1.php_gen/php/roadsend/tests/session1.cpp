
#include <php/roadsend/tests/session1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/session1.php line 68 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("v", m_v));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x3C2F961831E4EF6BLL, v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3C2F961831E4EF6BLL, m_v,
                         v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x3C2F961831E4EF6BLL, m_v,
                      v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_v = m_v;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_v = "hi";
}
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$session1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/session1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$session1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_s2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s2") : g->GV(s2);
  Variant &v_s1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s1") : g->GV(s1);
  Variant &v_vs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("vs") : g->GV(vs);

  LINE(6,invoke_failed("session_save_path", Array(ArrayInit(1).set(0, "/tmp/foo").create()), 0x000000009EE4D3B3LL));
  echo(LINE(8,(assignCallTemp(eo_1, toString(invoke_failed("session_save_path", Array(), 0x000000009EE4D3B3LL))),concat3("new save path is ", eo_1, "\n"))));
  echo(LINE(10,(assignCallTemp(eo_1, toString(invoke_failed("session_cache_expire", Array(), 0x000000008042E346LL))),concat3("cache expire is ", eo_1, "\n"))));
  LINE(12,invoke_failed("session_cache_expire", Array(ArrayInit(1).set(0, "260").create()), 0x000000008042E346LL));
  echo(LINE(14,(assignCallTemp(eo_1, toString(invoke_failed("session_cache_expire", Array(), 0x000000008042E346LL))),concat3("new cache expire is ", eo_1, "\n"))));
  echo(LINE(16,(assignCallTemp(eo_1, toString(invoke_failed("session_cache_limiter", Array(), 0x000000005F2B05FBLL))),concat3("cache limiter is ", eo_1, "\n"))));
  LINE(18,invoke_failed("session_cache_limiter", Array(ArrayInit(1).set(0, "private").create()), 0x000000005F2B05FBLL));
  echo(LINE(20,(assignCallTemp(eo_1, toString(invoke_failed("session_cache_limiter", Array(), 0x000000005F2B05FBLL))),concat3("new cache limiter is ", eo_1, "\n"))));
  echo(LINE(22,(assignCallTemp(eo_1, toString(invoke_failed("session_module_name", Array(), 0x000000000A955EC7LL))),concat3("module is ", eo_1, "\n"))));
  (v_a = LINE(25,invoke_failed("session_get_cookie_params", Array(), 0x0000000042E7E84FLL)));
  LINE(26,x_var_dump(1, v_a));
  LINE(28,invoke_failed("session_set_cookie_params", Array(ArrayInit(4).set(0, "newname").set(1, "path").set(2, "domain").set(3, true).create()), 0x0000000094864AF8LL));
  (v_a = LINE(30,invoke_failed("session_get_cookie_params", Array(), 0x0000000042E7E84FLL)));
  LINE(31,x_var_dump(1, v_a));
  LINE(36,invoke_failed("session_save_path", Array(ArrayInit(1).set(0, "/tmp").create()), 0x000000009EE4D3B3LL));
  echo(LINE(38,(assignCallTemp(eo_1, toString(invoke_failed("session_save_path", Array(), 0x000000009EE4D3B3LL))),concat3("the real save path is ", eo_1, "\n"))));
  LINE(40,invoke_failed("session_start", Array(), 0x00000000FE2CE6F2LL));
  (v_s = LINE(43,invoke_failed("session_id", Array(), 0x0000000042BAEA22LL)));
  echo(LINE(44,(assignCallTemp(eo_1, toString(x_preg_match("/^[a-f0-9]{32}$/", toString(v_s)))),concat3("session ID is ", eo_1, "\n"))));
  LINE(50,invoke_failed("session_regenerate_id", Array(), 0x000000002E11AA69LL));
  (v_s2 = LINE(52,invoke_failed("session_id", Array(), 0x0000000042BAEA22LL)));
  echo(LINE(54,(assignCallTemp(eo_1, toString(x_preg_match("/^[a-f0-9]{32}$/", toString(v_s2)))),concat3("new session ID is ", eo_1, "\n"))));
  if (equal(v_s1, v_s2)) {
    echo("ah crap they were the same!!\n");
  }
  LINE(64,invoke_failed("session_id", Array(ArrayInit(1).set(0, "newidhere").create()), 0x0000000042BAEA22LL));
  echo(LINE(66,(assignCallTemp(eo_1, toString(invoke_failed("session_id", Array(), 0x0000000042BAEA22LL))),concat3("new session ID is ", eo_1, "\n"))));
  g->gv__SESSION.set("myarray", (ScalarArrays::sa_[0]), 0x342AA93A325FEDAFLL);
  g->gv__SESSION.set("mystring", ("some | string"), 0x2BF615493707A80ELL);
  g->gv__SESSION.set("mybool", (true), 0x43603B98827ECB15LL);
  g->gv__SESSION.set("mynull", (null), 0x6A84B580195A1173LL);
  g->gv__SESSION.set("myint", (4241LL), 0x598A806C89346971LL);
  g->gv__SESSION.set("myobj", (((Object)(LINE(78,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create()))))), 0x23CB95B9927CBB15LL);
  g->gv__SESSION.set("undef", ("erase"), 0x2D6715E0DF8A895ALL);
  g->gv__SESSION.weakRemove("undef", 0x2D6715E0DF8A895ALL);
  LINE(82,x_print_r(g->gv__SESSION));
  (v_vs = LINE(84,invoke_failed("session_encode", Array(), 0x0000000001C9B079LL)));
  echo(toString(v_vs));
  LINE(87,invoke_failed("session_unset", Array(), 0x000000003932E39CLL));
  LINE(89,x_print_r(g->gv__SESSION));
  echo(toString(LINE(91,invoke_failed("session_encode", Array(), 0x0000000001C9B079LL))));
  LINE(93,invoke_failed("session_decode", Array(ArrayInit(1).set(0, ref(v_vs)).create()), 0x000000003590C9A0LL));
  LINE(94,x_print_r(g->gv__SESSION));
  echo(" \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
