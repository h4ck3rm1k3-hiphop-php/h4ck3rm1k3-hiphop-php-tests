
#ifndef __GENERATED_php_roadsend_tests_variables_h__
#define __GENERATED_php_roadsend_tests_variables_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/variables.fw.h>

// Declarations
#include <cls/uclass.h>
#include <cls/tclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$variables_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_uclass(CArrRef params, bool init = true);
Object co_tclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_variables_h__
