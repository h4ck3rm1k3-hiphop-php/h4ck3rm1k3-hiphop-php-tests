
#include <php/roadsend/tests/spellcheck.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/spellcheck.php line 10 */
void f_spellcheck() {
  FUNCTION_INJECTION(spellcheck);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_datafile;
  Variant v_dict;
  Variant v_fd;
  String v_word;

  (v_datafile = concat(toString(g->gv__ENV.rvalAt("PCC_HOME", 0x4F7B58EF1F3DF563LL)), "/benchmarks/data/spellcheck"));
  if (!(LINE(13,x_file_exists(v_datafile)))) {
    LINE(14,(assignCallTemp(eo_0, concat3("Couldn't find datafile ", v_datafile, ".")),x_trigger_error(eo_0, toInt32(256LL) /* E_USER_ERROR */)));
  }
  (v_dict = ScalarArrays::sa_[0]);
  (v_fd = LINE(18,x_fopen(v_datafile, "r")));
  LOOP_COUNTER(1);
  {
    while (!(LINE(19,x_feof(toObject(v_fd))))) {
      LOOP_COUNTER_CHECK(1);
      {
        v_dict.set(x_chop(toString(x_fgets(toObject(v_fd), 1024LL))), (1LL));
      }
    }
  }
  LINE(20,x_fclose(toObject(v_fd)));
  (v_fd = LINE(22,x_fopen(v_datafile, "r")));
  LOOP_COUNTER(2);
  {
    while (!(LINE(23,x_feof(toObject(v_fd))))) {
      LOOP_COUNTER_CHECK(2);
      {
        (v_word = LINE(24,x_chop(toString(x_fgets(toObject(v_fd), 1024LL)))));
        if (!(toBoolean(v_dict.rvalAt(v_word)))) {
          print(v_word + toString("\n"));
        }
      }
    }
  }
  LINE(29,x_fclose(toObject(v_fd)));
} /* function */
Variant pm_php$roadsend$tests$spellcheck_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/spellcheck.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$spellcheck_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(32,f_spellcheck());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
