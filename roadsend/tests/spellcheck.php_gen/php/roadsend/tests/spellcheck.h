
#ifndef __GENERATED_php_roadsend_tests_spellcheck_h__
#define __GENERATED_php_roadsend_tests_spellcheck_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/spellcheck.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$spellcheck_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_spellcheck();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_spellcheck_h__
