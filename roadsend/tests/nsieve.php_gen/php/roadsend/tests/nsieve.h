
#ifndef __GENERATED_php_roadsend_tests_nsieve_h__
#define __GENERATED_php_roadsend_tests_nsieve_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/nsieve.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_nsieve(int64 v_m);
Variant pm_php$roadsend$tests$nsieve_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_nsieve_h__
