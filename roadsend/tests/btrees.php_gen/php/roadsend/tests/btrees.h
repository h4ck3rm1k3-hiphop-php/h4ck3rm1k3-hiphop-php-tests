
#ifndef __GENERATED_php_roadsend_tests_btrees_h__
#define __GENERATED_php_roadsend_tests_btrees_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/btrees.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_itemcheck(Variant v_treeNode);
Variant pm_php$roadsend$tests$btrees_php(bool incOnce = false, LVariableTable* variables = NULL);
Array f_bottomuptree(CVarRef v_item, Variant v_depth);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_btrees_h__
