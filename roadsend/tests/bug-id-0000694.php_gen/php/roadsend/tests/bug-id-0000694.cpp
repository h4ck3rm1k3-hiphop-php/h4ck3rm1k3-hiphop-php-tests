
#include <php/roadsend/tests/bug-id-0000694.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000694.php line 9 */
Variant c_blah::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_blah::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_blah::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("fnord", m_fnord));
  props.push_back(NEW(ArrayElement)("melp", m_melp));
  c_ObjectData::o_get(props);
}
bool c_blah::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x26C2B819262A6288LL, fnord, 5);
      break;
    case 1:
      HASH_EXISTS_STRING(0x5A4F6AFD4C739F05LL, melp, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_blah::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x26C2B819262A6288LL, m_fnord,
                         fnord, 5);
      break;
    case 1:
      HASH_RETURN_STRING(0x5A4F6AFD4C739F05LL, m_melp,
                         melp, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_blah::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x26C2B819262A6288LL, m_fnord,
                      fnord, 5);
      break;
    case 1:
      HASH_SET_STRING(0x5A4F6AFD4C739F05LL, m_melp,
                      melp, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_blah::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_blah::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(blah)
ObjectData *c_blah::cloneImpl() {
  c_blah *obj = NEW(c_blah)();
  cloneSet(obj);
  return obj;
}
void c_blah::cloneSet(c_blah *clone) {
  clone->m_fnord = m_fnord;
  clone->m_melp = m_melp;
  ObjectData::cloneSet(clone);
}
Variant c_blah::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_blah::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_blah::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_blah$os_get(const char *s) {
  return c_blah::os_get(s, -1);
}
Variant &cw_blah$os_lval(const char *s) {
  return c_blah::os_lval(s, -1);
}
Variant cw_blah$os_constant(const char *s) {
  return c_blah::os_constant(s);
}
Variant cw_blah$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_blah::os_invoke(c, s, params, -1, fatal);
}
void c_blah::init() {
  m_fnord = "test";
  m_melp = "flag";
}
Object co_blah(CArrRef params, bool init /* = true */) {
  return Object(p_blah(NEW(c_blah)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000694_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000694.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000694_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_mah __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mah") : g->GV(mah);
  Variant &v_classVars __attribute__((__unused__)) = (variables != gVariables) ? variables->get("classVars") : g->GV(classVars);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("class property access with $ throws parse error\nthe following causes a parse error:\n(inside class method)\n\n$this->$var = 5;\n\n");
  (v_mah = ((Object)(LINE(15,p_blah(p_blah(NEWOBJ(c_blah)())->create())))));
  (v_classVars = LINE(17,x_get_class_vars(toString(x_get_class(v_mah)))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_classVars.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_n = iter3->first();
      {
        print(LINE(19,concat5("class prop is ", toString(v_n), " val is ", toString(v_v), "\n")));
        (v_v = v_mah.o_get(toString(v_n), -1LL));
        LINE(21,x_var_dump(1, v_v));
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
