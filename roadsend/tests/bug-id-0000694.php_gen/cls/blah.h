
#ifndef __GENERATED_cls_blah_h__
#define __GENERATED_cls_blah_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0000694.php line 9 */
class c_blah : virtual public ObjectData {
  BEGIN_CLASS_MAP(blah)
  END_CLASS_MAP(blah)
  DECLARE_CLASS(blah, blah, ObjectData)
  void init();
  public: String m_fnord;
  public: String m_melp;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_blah_h__
