
#include <php/roadsend/tests/strpos.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$strpos_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/strpos.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$strpos_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_mystring __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mystring") : g->GV(mystring);
  Variant &v_findme __attribute__((__unused__)) = (variables != gVariables) ? variables->get("findme") : g->GV(findme);
  Variant &v_pos __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pos") : g->GV(pos);

  (v_mystring = "abc");
  (v_findme = "a");
  (v_pos = concat("1: ", toString(LINE(6,x_strpos(toString(v_mystring), v_findme)))));
  echo(toString(v_pos) + toString("\n"));
  if (same(v_pos, false)) {
    echo(LINE(12,concat5("The string '", toString(v_findme), "' was not found in the string '", toString(v_mystring), "'")));
  }
  else {
    echo(LINE(14,concat5("The string '", toString(v_findme), "' was found in the string '", toString(v_mystring), "'")));
    echo(LINE(15,concat3(" and exists at position ", toString(v_pos), "\n")));
  }
  echo(concat("2: ", toString(LINE(18,x_strpos("blah-7-meep", "-7-")))));
  echo("\n");
  echo(concat("3: ", toString(LINE(20,x_strpos("blah-7-meep", "meep", toInt32(5LL))))));
  echo("\n");
  echo(concat("4: ", toString(LINE(22,x_strpos("blah-7-MEEP", "meep", toInt32(5LL))))));
  echo("\n");
  echo(concat("5: ", toString(LINE(24,x_strpos("blah-7-meep", "blah", toInt32(2LL))))));
  echo("\n");
  echo(concat("6: ", toString(LINE(26,x_strpos("BLAH-7-meep", "bLaH")))));
  echo("\n");
  echo("[");
  echo(concat("1: ", toString(LINE(32,x_stripos("blah-7-MEEP", "MEEP", toInt32(5LL))))));
  echo("\n");
  echo(concat("2: ", toString(LINE(34,x_stripos("bLaH-7-meep", "bLaH")))));
  echo("\n");
  echo("]");
  echo("done");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
