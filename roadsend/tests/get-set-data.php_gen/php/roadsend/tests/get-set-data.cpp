
#include <php/roadsend/tests/get-set-data.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$get_set_data_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/get-set-data.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$get_set_data_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  (v_obj = LINE(3,create_object("gtkbutton", Array())));
  LINE(5,v_obj.o_invoke_few_args("set_data", 0x07D6A34D6D7F7321LL, 2, "foo", "this is a string"));
  echo(LINE(6,(assignCallTemp(eo_1, toString(v_obj.o_invoke_few_args("get_data", 0x152546FA1A57E1EALL, 1, "foo"))),concat3("the data:  ", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
