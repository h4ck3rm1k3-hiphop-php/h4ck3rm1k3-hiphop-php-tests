
#ifndef __GENERATED_php_roadsend_tests_get_set_data_h__
#define __GENERATED_php_roadsend_tests_get_set_data_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/get-set-data.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$get_set_data_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_get_set_data_h__
