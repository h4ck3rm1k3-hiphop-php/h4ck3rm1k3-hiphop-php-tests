
#include <php/roadsend/tests/backtrace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/backtrace.php line 3 */
void f_dumpbt(CArrRef v_bt) {
  FUNCTION_INJECTION(dumpbt);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_v;

  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_bt.begin(); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3.second();
      {
        print(LINE(5,(assignCallTemp(eo_1, x_basename(toString(v_v.rvalAt("file", 0x612E37678CE7DB5BLL)))),concat3("file: ", eo_1, "\n"))));
        print(LINE(6,concat3("line: ", toString(v_v.rvalAt("line", 0x21093C71DDF8728CLL)), "\n")));
        print(LINE(7,concat3("function: ", toString(v_v.rvalAt("function", 0x736D912A52403931LL)), "\n")));
        print(LINE(8,concat3("args: ", toString(v_v.rvalAt("args", 0x4AF7CD17F976719ELL)), "\n")));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/backtrace.php line 21 */
void f_a(CStrRef v_b) {
  FUNCTION_INJECTION(a);
  Array v_a;

  LINE(22,f_b("1", "2"));
  (v_a = LINE(23,x_debug_backtrace()));
  LINE(24,f_dumpbt(v_a));
} /* function */
/* SRC: roadsend/tests/backtrace.php line 17 */
void f_b(CStrRef v_b, CStrRef v_c) {
  FUNCTION_INJECTION(b);
  LINE(18,f_c());
} /* function */
/* SRC: roadsend/tests/backtrace.php line 12 */
void f_c() {
  FUNCTION_INJECTION(c);
  Array v_a;

  (v_a = LINE(13,x_debug_backtrace()));
  LINE(14,f_dumpbt(v_a));
} /* function */
Variant pm_php$roadsend$tests$backtrace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/backtrace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$backtrace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(28,f_a("test"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
