
#ifndef __GENERATED_php_roadsend_tests_backtrace_h__
#define __GENERATED_php_roadsend_tests_backtrace_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/backtrace.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_dumpbt(CArrRef v_bt);
void f_a(CStrRef v_b);
void f_b(CStrRef v_b, CStrRef v_c);
void f_c();
Variant pm_php$roadsend$tests$backtrace_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_backtrace_h__
