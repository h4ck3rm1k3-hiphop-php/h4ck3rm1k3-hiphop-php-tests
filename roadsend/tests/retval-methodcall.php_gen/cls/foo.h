
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/retval-methodcall.php line 4 */
class c_foo : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, foo, ObjectData)
  void init();
  public: Variant m_prop;
  public: void t_foo(CVarRef v_arg);
  public: ObjectData *create(CVarRef v_arg);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: p_foo t_method();
  public: p_foo t_method2();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
