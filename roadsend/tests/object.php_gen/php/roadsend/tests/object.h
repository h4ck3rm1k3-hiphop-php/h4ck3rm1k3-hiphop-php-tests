
#ifndef __GENERATED_php_roadsend_tests_object_h__
#define __GENERATED_php_roadsend_tests_object_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/object.fw.h>

// Declarations
#include <cls/zot.h>
#include <cls/foo.h>
#include <cls/bar.h>
#include <cls/argconstructor.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_zot();
Variant pm_php$roadsend$tests$object_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_zot(CArrRef params, bool init = true);
Object co_foo(CArrRef params, bool init = true);
Object co_bar(CArrRef params, bool init = true);
Object co_argconstructor(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_object_h__
