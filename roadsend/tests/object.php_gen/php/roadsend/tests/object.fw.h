
#ifndef __GENERATED_php_roadsend_tests_object_fw_h__
#define __GENERATED_php_roadsend_tests_object_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(zot)
FORWARD_DECLARE_CLASS(foo)
FORWARD_DECLARE_CLASS(bar)
FORWARD_DECLARE_CLASS(argconstructor)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_object_fw_h__
