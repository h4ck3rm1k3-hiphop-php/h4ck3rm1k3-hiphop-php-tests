
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/object.php line 81 */
class c_bar : virtual public ObjectData {
  BEGIN_CLASS_MAP(bar)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, bar, ObjectData)
  void init();
  public: Variant m_baz;
  public: void t_bar(CVarRef v_a = "noarg");
  public: ObjectData *create(CVarRef v_a = "noarg");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
