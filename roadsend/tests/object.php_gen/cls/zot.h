
#ifndef __GENERATED_cls_zot_h__
#define __GENERATED_cls_zot_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/object.php line 3 */
class c_zot : virtual public ObjectData {
  BEGIN_CLASS_MAP(zot)
  END_CLASS_MAP(zot)
  DECLARE_CLASS(zot, zot, ObjectData)
  void init();
  public: int64 m_bing;
  public: String m_Bing;
  public: int64 m_otherplacement;
  public: void t_zot();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: String t___tostring();
  public: void t_afun(CVarRef v_anarg);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_zot_h__
