
#include <php/roadsend/tests/timeout.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/timeout.php line 11 */
Variant f_callback(CVarRef v_data) {
  FUNCTION_INJECTION(callback);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_i __attribute__((__unused__)) = g->sv_callback_DupIdi;
  bool &inited_sv_i __attribute__((__unused__)) = g->inited_sv_callback_DupIdi;
  if (!inited_sv_i) {
    (sv_i = 0LL);
    inited_sv_i = true;
  }
  if (less(sv_i, 1000LL)) {
    sv_i++;
    echo("callback called\n");
    return true;
  }
  else {
    echo(LINE(19,concat3("callback finished, data was: ", toString(v_data), "\n")));
    LINE(20,throw_fatal("unknown class gtk", ((void*)NULL)));
  }
  return null;
} /* function */
Variant i_callback(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2DE38EBA0ED55F48LL, callback) {
    return (f_callback(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$timeout_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/timeout.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$timeout_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  if (!(LINE(3,x_extension_loaded("gtk")))) {
    LINE(4,x_dl("php_gtk.so"));
  }
  LINE(7,throw_fatal("unknown class gtk", ((void*)NULL)));
  echo("callback added\n");
  LINE(9,throw_fatal("unknown class gtk", ((void*)NULL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
