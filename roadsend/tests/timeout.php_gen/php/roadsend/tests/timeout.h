
#ifndef __GENERATED_php_roadsend_tests_timeout_h__
#define __GENERATED_php_roadsend_tests_timeout_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/timeout.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_callback(CVarRef v_data);
Variant pm_php$roadsend$tests$timeout_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_timeout_h__
