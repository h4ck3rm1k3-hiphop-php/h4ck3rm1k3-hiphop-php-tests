
#include <php/roadsend/tests/style.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$style_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/style.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$style_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_drawing_area __attribute__((__unused__)) = (variables != gVariables) ? variables->get("drawing_area") : g->GV(drawing_area);

  if (!(LINE(3,x_extension_loaded("gtk")))) {
    LINE(4,x_dl("php_gtk.so"));
  }
  (v_drawing_area = LINE(7,create_object("gtkdrawingarea", Array())));
  (v_drawing_area = LINE(8,create_object("gtkdrawingarea", Array())));
  LINE(9,x_var_dump(1, v_drawing_area));
  LINE(12,x_var_dump(1, v_drawing_area.o_get("style", 0x040EF9119982696ALL)));
  LINE(13,x_var_dump(1, v_drawing_area.o_get("style", 0x040EF9119982696ALL)));
  (v_drawing_area = LINE(14,create_object("gtkdrawingarea", Array())));
  LINE(15,x_var_dump(1, v_drawing_area.o_get("style", 0x040EF9119982696ALL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
