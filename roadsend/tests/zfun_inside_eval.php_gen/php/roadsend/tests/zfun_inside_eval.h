
#ifndef __GENERATED_php_roadsend_tests_zfun_inside_eval_h__
#define __GENERATED_php_roadsend_tests_zfun_inside_eval_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zfun_inside_eval.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$zfun_inside_eval_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zfun_inside_eval_h__
