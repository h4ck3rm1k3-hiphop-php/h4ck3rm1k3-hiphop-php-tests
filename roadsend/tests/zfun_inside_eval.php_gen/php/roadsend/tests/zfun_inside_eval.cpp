
#include <php/roadsend/tests/zfun_inside_eval.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$zfun_inside_eval_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zfun_inside_eval.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zfun_inside_eval_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  LINE(3,x_error_reporting(toInt32(0LL)));
  f_eval("function test() { echo \"hey, this is a function inside an eval()!\\n\"; }");
  (v_i = 0LL);
  LOOP_COUNTER(1);
  {
    while (less(v_i, 10LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        f_eval("echo \"hey, this is a regular echo'd eval()\\n\";");
        LINE(10,invoke_failed("test", Array(), 0x00000000A0ED29E7LL));
        v_i++;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
