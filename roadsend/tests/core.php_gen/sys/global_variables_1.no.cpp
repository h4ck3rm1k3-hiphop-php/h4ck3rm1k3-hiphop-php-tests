
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      HASH_RETURN(0x5726DFA118184C01LL, g->GV(callable_name),
                  callable_name);
      break;
    case 2:
      HASH_RETURN(0x4056C2E766E0B782LL, g->GV(val),
                  val);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 41:
      HASH_RETURN(0x53F19D1B96F68229LL, g->GV(cl),
                  cl);
      break;
    case 43:
      HASH_RETURN(0x3C2F961831E4EF6BLL, g->GV(v),
                  v);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 48:
      HASH_RETURN(0x32C769EE5C5509B0LL, g->GV(c),
                  c);
      break;
    case 51:
      HASH_RETURN(0x0B9B87CD7C25BF73LL, g->GV(anObject),
                  anObject);
      break;
    case 62:
      HASH_RETURN(0x0605F9D6D1AD8D3ELL, g->GV(methodVariable),
                  methodVariable);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      HASH_RETURN(0x71490FB5524791BFLL, g->GV(functionVariable),
                  functionVariable);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
