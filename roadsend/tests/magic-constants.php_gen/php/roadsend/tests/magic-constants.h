
#ifndef __GENERATED_php_roadsend_tests_magic_constants_h__
#define __GENERATED_php_roadsend_tests_magic_constants_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/magic-constants.fw.h>

// Declarations
#include <cls/foo.h>
#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_testfunc();
Variant pm_php$roadsend$tests$magic_constants_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);
Object co_bar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_magic_constants_h__
