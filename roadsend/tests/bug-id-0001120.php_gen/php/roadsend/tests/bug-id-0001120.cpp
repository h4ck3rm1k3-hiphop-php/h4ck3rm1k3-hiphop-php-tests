
#include <php/roadsend/tests/bug-id-0001120.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001120_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001120.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001120_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);

  echo(" ");
  (v_filename = concat(LINE(3,x_dirname(get_source_filename("roadsend/tests/bug-id-0001120.php"))), "/test.csv"));
  if (LINE(6,x_file_exists(toString(v_filename)))) LINE(7,x_unlink(toString(v_filename)));
  (v_fp = LINE(9,x_fopen(toString(v_filename), "w")));
  LINE(10,x_fwrite(toObject(v_fp), "\"One\",\"\\\"Two\\\"\",\"Three\\\"\",\"Four\",\"\\\\\",foo,\"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"\\\\,\\\\\"\n"));
  LINE(11,x_fclose(toObject(v_fp)));
  (v_fp = LINE(13,x_fopen(toString(v_filename), "r")));
  LOOP_COUNTER(1);
  {
    while ((toBoolean((v_line = LINE(14,x_fgetcsv(toObject(v_fp), 1024LL)))))) {
      LOOP_COUNTER_CHECK(1);
      LINE(15,x_var_dump(1, v_line));
    }
  }
  LINE(16,x_fclose(toObject(v_fp)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
