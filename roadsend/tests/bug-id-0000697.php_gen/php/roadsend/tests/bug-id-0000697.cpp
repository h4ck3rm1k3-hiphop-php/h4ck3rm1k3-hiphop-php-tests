
#include <php/roadsend/tests/bug-id-0000697.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_undef = "undef";
const StaticString k_alsoundef = "alsoundef";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000697.php line 17 */
void f_myfun(CStrRef v_arg //  = k_undef
, CStrRef v_arg1 //  = k_alsoundef
) {
  FUNCTION_INJECTION(myfun);
  echo((LINE(18,concat4(v_arg, ", ", v_arg1, "\n"))));
} /* function */
Variant pm_php$roadsend$tests$bug_id_0000697_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000697.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000697_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("Bug# 0000697\n\ndefault function parameters should accept undefined tokens for default values\nbad practice, but php allows this:\n\n\nfunction myfun($arg=undef, $arg2=undef) {\n...\n}\n\nwhere undef isn't defined at all anywhere else. perhaps it turns it into a string\?\nAdditional Information \t\n\n");
  LINE(21,f_myfun());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
