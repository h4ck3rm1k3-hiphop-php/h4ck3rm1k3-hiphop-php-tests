
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_undef;
extern const StaticString k_alsoundef;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 1:
      HASH_RETURN(0x2F49224F7EE869B5LL, k_alsoundef, alsoundef);
      break;
    case 2:
      HASH_RETURN(0x2D6715E0DF8A895ALL, k_undef, undef);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
