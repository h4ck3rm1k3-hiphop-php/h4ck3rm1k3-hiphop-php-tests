
#ifndef __GENERATED_php_roadsend_tests_prog_exec_h__
#define __GENERATED_php_roadsend_tests_prog_exec_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/prog-exec.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$prog_exec_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_prog_exec_h__
