
#include <php/roadsend/tests/dirs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$dirs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/dirs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$dirs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dir") : g->GV(dir);
  Variant &v_handle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("handle") : g->GV(handle);
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);

  (v_dir = "/etc");
  if (toBoolean((v_handle = LINE(9,x_opendir(toString(v_dir)))))) {
    echo(LINE(10,concat3("Directory handle: ", toString(v_handle), "\n")));
    echo("Files:\n");
    LOOP_COUNTER(1);
    {
      while (!same(false, ((v_file = LINE(14,x_readdir(toObject(v_handle))))))) {
        LOOP_COUNTER_CHECK(1);
        {
          echo(toString(v_file) + toString("\n"));
        }
      }
    }
    LOOP_COUNTER(2);
    {
      while (toBoolean((v_file = LINE(19,x_readdir(toObject(v_handle)))))) {
        LOOP_COUNTER_CHECK(2);
        {
          echo(toString(v_file) + toString("\n"));
        }
      }
    }
    LINE(23,x_closedir(toObject(v_handle)));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
