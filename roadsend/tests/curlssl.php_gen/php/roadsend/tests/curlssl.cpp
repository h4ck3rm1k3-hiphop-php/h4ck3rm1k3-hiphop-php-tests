
#include <php/roadsend/tests/curlssl.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$curlssl_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/curlssl.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$curlssl_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ch") : g->GV(ch);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo(toString(LINE(3,x_curl_version())));
  echo("\niniting\n");
  (v_ch = LINE(6,x_curl_init()));
  echo("\nsetting options:");
  echo(toString(LINE(9,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "https://secure.roadsend.com/_test/test.php\?var1=test&var2=test"))));
  LINE(11,x_curl_setopt(toObject(v_ch), toInt32(19913LL) /* CURLOPT_RETURNTRANSFER */, 1LL));
  echo("\nexecing:");
  (v_foo = LINE(21,x_curl_exec(toObject(v_ch))));
  LINE(22,x_var_dump(1, v_foo));
  echo("\nclosing:");
  echo((LINE(26,x_curl_close(toObject(v_ch))), null));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
