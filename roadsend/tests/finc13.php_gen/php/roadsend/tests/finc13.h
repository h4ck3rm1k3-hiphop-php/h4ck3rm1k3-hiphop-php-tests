
#ifndef __GENERATED_php_roadsend_tests_finc13_h__
#define __GENERATED_php_roadsend_tests_finc13_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/finc13.fw.h>

// Declarations
#include <cls/sm_smtag_aream.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$finc13_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_aream(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_finc13_h__
