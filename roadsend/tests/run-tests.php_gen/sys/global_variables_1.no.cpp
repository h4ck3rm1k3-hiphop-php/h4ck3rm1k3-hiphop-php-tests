
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 255) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 5:
      HASH_RETURN(0x178B5DB860BB1F05LL, g->GV(n_total),
                  n_total);
      break;
    case 10:
      HASH_RETURN(0x508FC7C8724A760ALL, g->GV(type),
                  type);
      break;
    case 13:
      HASH_RETURN(0x262F35FB33EC540DLL, g->GV(ini_overwrites),
                  ini_overwrites);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 18:
      HASH_RETURN(0x152FE5CE75DA9C12LL, g->GV(failed_tests_file),
                  failed_tests_file);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x3A1DC49AA6CFEA13LL, g->GV(test),
                  test);
      break;
    case 22:
      HASH_RETURN(0x6B3093D1F2F27D16LL, g->GV(cwd),
                  cwd);
      break;
    case 28:
      HASH_RETURN(0x1F09B43053EBEB1CLL, g->GV(log_format),
                  log_format);
      break;
    case 29:
      HASH_RETURN(0x72C2CA3ECF7C191DLL, g->GV(test_dirs),
                  test_dirs);
      break;
    case 37:
      HASH_RETURN(0x09EF6630A291F025LL, g->GV(environment),
                  environment);
      break;
    case 44:
      HASH_RETURN(0x126C26F0F6A58E2CLL, g->GV(cfgtypes),
                  cfgtypes);
      break;
    case 45:
      HASH_RETURN(0x1B723EAC6938822DLL, g->GV(end_time),
                  end_time);
      break;
    case 54:
      HASH_RETURN(0x24CB2D71EFF85036LL, g->GV(is_switch),
                  is_switch);
      break;
    case 55:
      HASH_RETURN(0x310559E2D6E6EC37LL, g->GV(user_tests),
                  user_tests);
      break;
    case 59:
      HASH_RETURN(0x4A55D1778C725E3BLL, g->GV(compression),
                  compression);
      break;
    case 60:
      HASH_RETURN(0x43365B62E067203CLL, g->GV(temp_urlbase),
                  temp_urlbase);
      break;
    case 61:
      HASH_RETURN(0x6DE3CFB71905593DLL, g->GV(test_files),
                  test_files);
      break;
    case 62:
      HASH_RETURN(0x7D8D2E5AC1F9AC3ELL, g->GV(html_file),
                  html_file);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 65:
      HASH_RETURN(0x52B182B0A94E9C41LL, g->GV(php),
                  php);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 91:
      HASH_RETURN(0x612E37678CE7DB5BLL, g->GV(file),
                  file);
      break;
    case 101:
      HASH_RETURN(0x687BED2B0019D265LL, g->GV(start_time),
                  start_time);
      break;
    case 104:
      HASH_RETURN(0x77709D492BF62268LL, g->GV(temp_source),
                  temp_source);
      break;
    case 105:
      HASH_RETURN(0x51DA1403EA629E69LL, g->GV(info_params),
                  info_params);
      break;
    case 108:
      HASH_RETURN(0x45EC4BFB60499F6CLL, g->GV(conf_passed),
                  conf_passed);
      break;
    case 111:
      HASH_RETURN(0x6511131491A84B6FLL, g->GV(DETAILED),
                  DETAILED);
      break;
    case 118:
      HASH_RETURN(0x33885EEC4F612376LL, g->GV(html_output),
                  html_output);
      HASH_RETURN(0x1042147977F84376LL, g->GV(optionals),
                  optionals);
      break;
    case 119:
      HASH_RETURN(0x400AC3BA82543C77LL, g->GV(failed_test_summary),
                  failed_test_summary);
      break;
    case 121:
      HASH_RETURN(0x07627BF26E5ADC79LL, g->GV(leak_check),
                  leak_check);
      break;
    case 130:
      HASH_RETURN(0x4056C2E766E0B782LL, g->GV(val),
                  val);
      break;
    case 131:
      HASH_RETURN(0x4B9AE5064B0A5283LL, g->GV(exts_to_test),
                  exts_to_test);
      break;
    case 133:
      HASH_RETURN(0x124AB4DF0D760A85LL, g->GV(pass_options),
                  pass_options);
      break;
    case 135:
      HASH_RETURN(0x612DD31212E90587LL, g->GV(key),
                  key);
      break;
    case 137:
      HASH_RETURN(0x1E2E626C9AD36E89LL, g->GV(PHP_FAILED_TESTS),
                  PHP_FAILED_TESTS);
      break;
    case 138:
      HASH_RETURN(0x2D1866296FE4BE8ALL, g->GV(cfgfiles),
                  cfgfiles);
      break;
    case 144:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      HASH_RETURN(0x67FECFEAB01DEA90LL, g->GV(percent_results),
                  percent_results);
      break;
    case 151:
      HASH_RETURN(0x72CCC88F90317797LL, g->GV(exts_tested),
                  exts_tested);
      break;
    case 152:
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 163:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 179:
      HASH_RETURN(0x4B078B47A2EF86B3LL, g->GV(CUR_DIR),
                  CUR_DIR);
      break;
    case 180:
      HASH_RETURN(0x3AD6EDBE00AC3BB4LL, g->GV(php_info),
                  php_info);
      break;
    case 182:
      HASH_RETURN(0x31CF71EAC03B86B6LL, g->GV(testfile),
                  testfile);
      break;
    case 183:
      HASH_RETURN(0x15B870ABDE28C8B7LL, g->GV(php_cgi),
                  php_cgi);
      break;
    case 185:
      HASH_RETURN(0x61CFEEED54DD0AB9LL, g->GV(matches),
                  matches);
      break;
    case 186:
      HASH_RETURN(0x39D40BAF3B9E07BALL, g->GV(test_idx),
                  test_idx);
      break;
    case 195:
      HASH_RETURN(0x346B87C4FC1A7AC3LL, g->GV(no_clean),
                  no_clean);
      break;
    case 196:
      HASH_RETURN(0x7E7EB0DE39E966C4LL, g->GV(sum_results),
                  sum_results);
      break;
    case 198:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x2D2DC4D373BA0DC6LL, g->GV(cfg),
                  cfg);
      HASH_RETURN(0x77EE85A013BFFCC6LL, g->GV(ignored_by_ext),
                  ignored_by_ext);
      break;
    case 200:
      HASH_RETURN(0x5D2AE074B13A41C8LL, g->GV(pass_option_n),
                  pass_option_n);
      break;
    case 203:
      HASH_RETURN(0x7EDA455F36DBDCCBLL, g->GV(just_save_results),
                  just_save_results);
      break;
    case 206:
      HASH_RETURN(0x70BCE0E305461CCELL, g->GV(switch),
                  switch);
      break;
    case 207:
      HASH_RETURN(0x522821F5063592CFLL, g->GV(dir),
                  dir);
      HASH_RETURN(0x59B4E66CC0580FCFLL, g->GV(IN_REDIRECT),
                  IN_REDIRECT);
      break;
    case 209:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      HASH_RETURN(0x318FBA9639C18FD1LL, g->GV(temp_target),
                  temp_target);
      HASH_RETURN(0x2A28A6C4E9876ED1LL, g->GV(test_list),
                  test_list);
      break;
    case 219:
      HASH_RETURN(0x4CE40227CDA7D4DBLL, g->GV(redir_tests),
                  redir_tests);
      break;
    case 222:
      HASH_RETURN(0x7932B72E7B0466DELL, g->GV(exts_skipped),
                  exts_skipped);
      break;
    case 235:
      HASH_RETURN(0x519638790DED9BEBLL, g->GV(test_cnt),
                  test_cnt);
      break;
    case 236:
      HASH_RETURN(0x3A944C0BE05000ECLL, g->GV(output_file),
                  output_file);
      break;
    case 238:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 254:
      HASH_RETURN(0x5154C50FC7F55FFELL, g->GV(test_results),
                  test_results);
      HASH_RETURN(0x5A5A8C53402EACFELL, g->GV(repeat),
                  repeat);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
