
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "CUR_DIR",
    "cwd",
    "environment",
    "php",
    "php_cgi",
    "log_format",
    "DETAILED",
    "user_tests",
    "exts_to_test",
    "ini_overwrites",
    "php_info",
    "pass_options",
    "test_files",
    "redir_tests",
    "test_results",
    "PHP_FAILED_TESTS",
    "failed_tests_file",
    "pass_option_n",
    "compression",
    "output_file",
    "just_save_results",
    "leak_check",
    "html_output",
    "html_file",
    "temp_source",
    "temp_target",
    "temp_urlbase",
    "conf_passed",
    "no_clean",
    "cfgtypes",
    "cfgfiles",
    "cfg",
    "type",
    "file",
    "i",
    "is_switch",
    "switch",
    "repeat",
    "test_list",
    "test",
    "matches",
    "testfile",
    "test_cnt",
    "start_time",
    "test_idx",
    "end_time",
    "exts_tested",
    "exts_skipped",
    "ignored_by_ext",
    "test_dirs",
    "optionals",
    "dir",
    "key",
    "val",
    "sum_results",
    "info_params",
    "IN_REDIRECT",
    "n_total",
    "percent_results",
    "failed_test_summary",
  };
  if (idx >= 0 && idx < 72) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(CUR_DIR);
    case 13: return GV(cwd);
    case 14: return GV(environment);
    case 15: return GV(php);
    case 16: return GV(php_cgi);
    case 17: return GV(log_format);
    case 18: return GV(DETAILED);
    case 19: return GV(user_tests);
    case 20: return GV(exts_to_test);
    case 21: return GV(ini_overwrites);
    case 22: return GV(php_info);
    case 23: return GV(pass_options);
    case 24: return GV(test_files);
    case 25: return GV(redir_tests);
    case 26: return GV(test_results);
    case 27: return GV(PHP_FAILED_TESTS);
    case 28: return GV(failed_tests_file);
    case 29: return GV(pass_option_n);
    case 30: return GV(compression);
    case 31: return GV(output_file);
    case 32: return GV(just_save_results);
    case 33: return GV(leak_check);
    case 34: return GV(html_output);
    case 35: return GV(html_file);
    case 36: return GV(temp_source);
    case 37: return GV(temp_target);
    case 38: return GV(temp_urlbase);
    case 39: return GV(conf_passed);
    case 40: return GV(no_clean);
    case 41: return GV(cfgtypes);
    case 42: return GV(cfgfiles);
    case 43: return GV(cfg);
    case 44: return GV(type);
    case 45: return GV(file);
    case 46: return GV(i);
    case 47: return GV(is_switch);
    case 48: return GV(switch);
    case 49: return GV(repeat);
    case 50: return GV(test_list);
    case 51: return GV(test);
    case 52: return GV(matches);
    case 53: return GV(testfile);
    case 54: return GV(test_cnt);
    case 55: return GV(start_time);
    case 56: return GV(test_idx);
    case 57: return GV(end_time);
    case 58: return GV(exts_tested);
    case 59: return GV(exts_skipped);
    case 60: return GV(ignored_by_ext);
    case 61: return GV(test_dirs);
    case 62: return GV(optionals);
    case 63: return GV(dir);
    case 64: return GV(key);
    case 65: return GV(val);
    case 66: return GV(sum_results);
    case 67: return GV(info_params);
    case 68: return GV(IN_REDIRECT);
    case 69: return GV(n_total);
    case 70: return GV(percent_results);
    case 71: return GV(failed_test_summary);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
