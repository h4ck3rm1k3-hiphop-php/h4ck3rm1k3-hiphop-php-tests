
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "comp_line", "roadsend/tests/run-tests.php",
  "compute_summary", "roadsend/tests/run-tests.php",
  "count_array_diff", "roadsend/tests/run-tests.php",
  "error", "roadsend/tests/run-tests.php",
  "error_report", "roadsend/tests/run-tests.php",
  "find_files", "roadsend/tests/run-tests.php",
  "generate_array_diff", "roadsend/tests/run-tests.php",
  "generate_diff", "roadsend/tests/run-tests.php",
  "get_summary", "roadsend/tests/run-tests.php",
  "mail_qa_team", "roadsend/tests/run-tests.php",
  "run_all_tests", "roadsend/tests/run-tests.php",
  "run_test", "roadsend/tests/run-tests.php",
  "save_text", "roadsend/tests/run-tests.php",
  "settings2array", "roadsend/tests/run-tests.php",
  "settings2params", "roadsend/tests/run-tests.php",
  "show_end", "roadsend/tests/run-tests.php",
  "show_redirect_ends", "roadsend/tests/run-tests.php",
  "show_redirect_start", "roadsend/tests/run-tests.php",
  "show_result", "roadsend/tests/run-tests.php",
  "show_start", "roadsend/tests/run-tests.php",
  "show_summary", "roadsend/tests/run-tests.php",
  "show_test", "roadsend/tests/run-tests.php",
  "system_with_timeout", "roadsend/tests/run-tests.php",
  "test_name", "roadsend/tests/run-tests.php",
  "test_sort", "roadsend/tests/run-tests.php",
  "write_information", "roadsend/tests/run-tests.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
