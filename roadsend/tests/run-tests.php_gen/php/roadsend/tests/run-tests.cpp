
#include <php/roadsend/tests/run-tests.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: roadsend/tests/run-tests.php line 1852 */
void f_show_summary() {
  FUNCTION_INJECTION(show_summary);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_html_output __attribute__((__unused__)) = g->GV(html_output);
  Variant &gv_html_file __attribute__((__unused__)) = g->GV(html_file);
  {
  }
  if (toBoolean(gv_html_output)) {
    LINE(1858,(assignCallTemp(eo_0, toObject(gv_html_file)),assignCallTemp(eo_1, concat("<hr/>\n", toString(f_get_summary(true, true)))),x_fwrite(eo_0, eo_1)));
  }
  echo(toString(LINE(1860,f_get_summary(true, false))));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 936 */
String f_run_test(Variant v_php, Variant v_file, Variant v_env) {
  FUNCTION_INJECTION(run_test);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_log_format __attribute__((__unused__)) = g->GV(log_format);
  Variant v_log_format;
  Variant &gv_info_params __attribute__((__unused__)) = g->GV(info_params);
  Variant v_info_params;
  Variant &gv_ini_overwrites __attribute__((__unused__)) = g->GV(ini_overwrites);
  Variant v_ini_overwrites;
  Variant &gv_cwd __attribute__((__unused__)) = g->GV(cwd);
  Variant v_cwd;
  Variant &gv_PHP_FAILED_TESTS __attribute__((__unused__)) = g->GV(PHP_FAILED_TESTS);
  Variant v_PHP_FAILED_TESTS;
  Variant &gv_pass_options __attribute__((__unused__)) = g->GV(pass_options);
  Variant v_pass_options;
  Variant &gv_DETAILED __attribute__((__unused__)) = g->GV(DETAILED);
  Variant v_DETAILED;
  Variant &gv_IN_REDIRECT __attribute__((__unused__)) = g->GV(IN_REDIRECT);
  Variant v_IN_REDIRECT;
  Variant &gv_test_cnt __attribute__((__unused__)) = g->GV(test_cnt);
  Variant v_test_cnt;
  Variant &gv_test_idx __attribute__((__unused__)) = g->GV(test_idx);
  Variant v_test_idx;
  Variant &gv_leak_check __attribute__((__unused__)) = g->GV(leak_check);
  Variant v_leak_check;
  Variant &gv_temp_source __attribute__((__unused__)) = g->GV(temp_source);
  Variant v_temp_source;
  Variant &gv_temp_target __attribute__((__unused__)) = g->GV(temp_target);
  Variant v_temp_target;
  Variant &gv_cfg __attribute__((__unused__)) = g->GV(cfg);
  Variant v_cfg;
  Variant &gv_environment __attribute__((__unused__)) = g->GV(environment);
  Variant v_environment;
  Variant &gv_no_clean __attribute__((__unused__)) = g->GV(no_clean);
  Variant v_no_clean;
  Variant v_temp_filenames;
  Variant v_org_file;
  Variant v_php_cgi;
  Variant v_section_text;
  Variant v_fp;
  Variant v_borked;
  Variant v_bork_info;
  Variant v_line;
  Variant v_r;
  Variant v_section;
  Variant v_secfile;
  Variant v_secdone;
  Variant v_shortname;
  Variant v_tested_file;
  Variant v_tested;
  Variant v_old_php;
  Variant v_test_dir;
  Variant v_temp_dir;
  Variant v_main_file_name;
  Variant v_diff_filename;
  Variant v_log_filename;
  Variant v_exp_filename;
  Variant v_output_filename;
  Variant v_memcheck_filename;
  Variant v_temp_file;
  Variant v_test_file;
  Variant v_temp_skipif;
  Variant v_test_skipif;
  Variant v_temp_clean;
  Variant v_test_clean;
  Variant v_tmp_post;
  Variant v_tmp_relative_file;
  Variant v_copy_file;
  Variant v_e;
  Variant v_ini_settings;
  Variant v_info;
  Variant v_warn;
  Variant v_extra;
  Variant v_output;
  Variant v_reason;
  Variant v_test_files;
  Variant v_f;
  Variant v_redirenv;
  Variant v_unicode_semantics;
  Variant v_query_string;
  Variant v_args;
  Variant v_post;
  Variant v_raw_lines;
  Variant v_request;
  Variant v_res;
  Variant v_cmd;
  Variant v_content_length;
  Variant v_out;
  Variant v_clean_params;
  Variant v_leaked;
  Variant v_passed;
  Variant v_headers;
  Variant v_match;
  Variant v_rh;
  Variant v_failed_headers;
  Variant v_want;
  Variant v_wanted_headers;
  Variant v_lines;
  Variant v_org_headers;
  Variant v_output_headers;
  Variant v_k;
  Variant v_v;
  Variant v_wanted;
  Variant v_wanted_re;
  Variant v_restype;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_php; Variant &v_file; Variant &v_env; Variant &v_log_format; Variant &v_info_params; Variant &v_ini_overwrites; Variant &v_cwd; Variant &v_PHP_FAILED_TESTS; Variant &v_pass_options; Variant &v_DETAILED; Variant &v_IN_REDIRECT; Variant &v_test_cnt; Variant &v_test_idx; Variant &v_leak_check; Variant &v_temp_source; Variant &v_temp_target; Variant &v_cfg; Variant &v_environment; Variant &v_no_clean; Variant &v_temp_filenames; Variant &v_org_file; Variant &v_php_cgi; Variant &v_section_text; Variant &v_fp; Variant &v_borked; Variant &v_bork_info; Variant &v_line; Variant &v_r; Variant &v_section; Variant &v_secfile; Variant &v_secdone; Variant &v_shortname; Variant &v_tested_file; Variant &v_tested; Variant &v_old_php; Variant &v_test_dir; Variant &v_temp_dir; Variant &v_main_file_name; Variant &v_diff_filename; Variant &v_log_filename; Variant &v_exp_filename; Variant &v_output_filename; Variant &v_memcheck_filename; Variant &v_temp_file; Variant &v_test_file; Variant &v_temp_skipif; Variant &v_test_skipif; Variant &v_temp_clean; Variant &v_test_clean; Variant &v_tmp_post; Variant &v_tmp_relative_file; Variant &v_copy_file; Variant &v_e; Variant &v_ini_settings; Variant &v_info; Variant &v_warn; Variant &v_extra; Variant &v_output; Variant &v_reason; Variant &v_test_files; Variant &v_f; Variant &v_redirenv; Variant &v_unicode_semantics; Variant &v_query_string; Variant &v_args; Variant &v_post; Variant &v_raw_lines; Variant &v_request; Variant &v_res; Variant &v_cmd; Variant &v_content_length; Variant &v_out; Variant &v_clean_params; Variant &v_leaked; Variant &v_passed; Variant &v_headers; Variant &v_match; Variant &v_rh; Variant &v_failed_headers; Variant &v_want; Variant &v_wanted_headers; Variant &v_lines; Variant &v_org_headers; Variant &v_output_headers; Variant &v_k; Variant &v_v; Variant &v_wanted; Variant &v_wanted_re; Variant &v_restype;
    VariableTable(Variant &r_php, Variant &r_file, Variant &r_env, Variant &r_log_format, Variant &r_info_params, Variant &r_ini_overwrites, Variant &r_cwd, Variant &r_PHP_FAILED_TESTS, Variant &r_pass_options, Variant &r_DETAILED, Variant &r_IN_REDIRECT, Variant &r_test_cnt, Variant &r_test_idx, Variant &r_leak_check, Variant &r_temp_source, Variant &r_temp_target, Variant &r_cfg, Variant &r_environment, Variant &r_no_clean, Variant &r_temp_filenames, Variant &r_org_file, Variant &r_php_cgi, Variant &r_section_text, Variant &r_fp, Variant &r_borked, Variant &r_bork_info, Variant &r_line, Variant &r_r, Variant &r_section, Variant &r_secfile, Variant &r_secdone, Variant &r_shortname, Variant &r_tested_file, Variant &r_tested, Variant &r_old_php, Variant &r_test_dir, Variant &r_temp_dir, Variant &r_main_file_name, Variant &r_diff_filename, Variant &r_log_filename, Variant &r_exp_filename, Variant &r_output_filename, Variant &r_memcheck_filename, Variant &r_temp_file, Variant &r_test_file, Variant &r_temp_skipif, Variant &r_test_skipif, Variant &r_temp_clean, Variant &r_test_clean, Variant &r_tmp_post, Variant &r_tmp_relative_file, Variant &r_copy_file, Variant &r_e, Variant &r_ini_settings, Variant &r_info, Variant &r_warn, Variant &r_extra, Variant &r_output, Variant &r_reason, Variant &r_test_files, Variant &r_f, Variant &r_redirenv, Variant &r_unicode_semantics, Variant &r_query_string, Variant &r_args, Variant &r_post, Variant &r_raw_lines, Variant &r_request, Variant &r_res, Variant &r_cmd, Variant &r_content_length, Variant &r_out, Variant &r_clean_params, Variant &r_leaked, Variant &r_passed, Variant &r_headers, Variant &r_match, Variant &r_rh, Variant &r_failed_headers, Variant &r_want, Variant &r_wanted_headers, Variant &r_lines, Variant &r_org_headers, Variant &r_output_headers, Variant &r_k, Variant &r_v, Variant &r_wanted, Variant &r_wanted_re, Variant &r_restype) : v_php(r_php), v_file(r_file), v_env(r_env), v_log_format(r_log_format), v_info_params(r_info_params), v_ini_overwrites(r_ini_overwrites), v_cwd(r_cwd), v_PHP_FAILED_TESTS(r_PHP_FAILED_TESTS), v_pass_options(r_pass_options), v_DETAILED(r_DETAILED), v_IN_REDIRECT(r_IN_REDIRECT), v_test_cnt(r_test_cnt), v_test_idx(r_test_idx), v_leak_check(r_leak_check), v_temp_source(r_temp_source), v_temp_target(r_temp_target), v_cfg(r_cfg), v_environment(r_environment), v_no_clean(r_no_clean), v_temp_filenames(r_temp_filenames), v_org_file(r_org_file), v_php_cgi(r_php_cgi), v_section_text(r_section_text), v_fp(r_fp), v_borked(r_borked), v_bork_info(r_bork_info), v_line(r_line), v_r(r_r), v_section(r_section), v_secfile(r_secfile), v_secdone(r_secdone), v_shortname(r_shortname), v_tested_file(r_tested_file), v_tested(r_tested), v_old_php(r_old_php), v_test_dir(r_test_dir), v_temp_dir(r_temp_dir), v_main_file_name(r_main_file_name), v_diff_filename(r_diff_filename), v_log_filename(r_log_filename), v_exp_filename(r_exp_filename), v_output_filename(r_output_filename), v_memcheck_filename(r_memcheck_filename), v_temp_file(r_temp_file), v_test_file(r_test_file), v_temp_skipif(r_temp_skipif), v_test_skipif(r_test_skipif), v_temp_clean(r_temp_clean), v_test_clean(r_test_clean), v_tmp_post(r_tmp_post), v_tmp_relative_file(r_tmp_relative_file), v_copy_file(r_copy_file), v_e(r_e), v_ini_settings(r_ini_settings), v_info(r_info), v_warn(r_warn), v_extra(r_extra), v_output(r_output), v_reason(r_reason), v_test_files(r_test_files), v_f(r_f), v_redirenv(r_redirenv), v_unicode_semantics(r_unicode_semantics), v_query_string(r_query_string), v_args(r_args), v_post(r_post), v_raw_lines(r_raw_lines), v_request(r_request), v_res(r_res), v_cmd(r_cmd), v_content_length(r_content_length), v_out(r_out), v_clean_params(r_clean_params), v_leaked(r_leaked), v_passed(r_passed), v_headers(r_headers), v_match(r_match), v_rh(r_rh), v_failed_headers(r_failed_headers), v_want(r_want), v_wanted_headers(r_wanted_headers), v_lines(r_lines), v_org_headers(r_org_headers), v_output_headers(r_output_headers), v_k(r_k), v_v(r_v), v_wanted(r_wanted), v_wanted_re(r_wanted_re), v_restype(r_restype) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 255) {
        case 2:
          HASH_RETURN(0x3CD1BFEC90E90402LL, v_redirenv,
                      redirenv);
          break;
        case 13:
          HASH_RETURN(0x262F35FB33EC540DLL, v_ini_overwrites,
                      ini_overwrites);
          break;
        case 17:
          HASH_RETURN(0x7E2F6B6DDDE82111LL, v_wanted_headers,
                      wanted_headers);
          break;
        case 22:
          HASH_RETURN(0x6B3093D1F2F27D16LL, v_cwd,
                      cwd);
          break;
        case 27:
          HASH_RETURN(0x3004DC59E4DC261BLL, v_wanted,
                      wanted);
          break;
        case 28:
          HASH_RETURN(0x1F09B43053EBEB1CLL, v_log_format,
                      log_format);
          break;
        case 34:
          HASH_RETURN(0x7C801AC012E9F722LL, v_out,
                      out);
          break;
        case 36:
          HASH_RETURN(0x6F443E367A5DBB24LL, v_output,
                      output);
          break;
        case 37:
          HASH_RETURN(0x09EF6630A291F025LL, v_environment,
                      environment);
          break;
        case 40:
          HASH_RETURN(0x03F90A8A3B5A1F28LL, v_bork_info,
                      bork_info);
          break;
        case 41:
          HASH_RETURN(0x32FEF153B9ADA729LL, v_log_filename,
                      log_filename);
          break;
        case 46:
          HASH_RETURN(0x6BB4A0689FBAD42ELL, v_f,
                      f);
          break;
        case 48:
          HASH_RETURN(0x5B0C0C547374D530LL, v_fp,
                      fp);
          break;
        case 49:
          HASH_RETURN(0x150ACBD97C259031LL, v_reason,
                      reason);
          break;
        case 50:
          HASH_RETURN(0x343DD2024F85E432LL, v_section_text,
                      section_text);
          break;
        case 56:
          HASH_RETURN(0x30AB4C91151A5938LL, v_temp_file,
                      temp_file);
          break;
        case 58:
          HASH_RETURN(0x5AD7B0518B4A5D3ALL, v_wanted_re,
                      wanted_re);
          break;
        case 59:
          HASH_RETURN(0x770EC85BB611F13BLL, v_tmp_relative_file,
                      tmp_relative_file);
          break;
        case 61:
          HASH_RETURN(0x6DE3CFB71905593DLL, v_test_files,
                      test_files);
          break;
        case 62:
          HASH_RETURN(0x59E9384E33988B3ELL, v_info,
                      info);
          HASH_RETURN(0x68CB6481E55DC83ELL, v_content_length,
                      content_length);
          break;
        case 63:
          HASH_RETURN(0x3DAF3A848FD5F03FLL, v_org_file,
                      org_file);
          break;
        case 64:
          HASH_RETURN(0x30704984F0078F40LL, v_match,
                      match);
          break;
        case 65:
          HASH_RETURN(0x52B182B0A94E9C41LL, v_php,
                      php);
          break;
        case 66:
          HASH_RETURN(0x160449C754173842LL, v_extra,
                      extra);
          break;
        case 91:
          HASH_RETURN(0x612E37678CE7DB5BLL, v_file,
                      file);
          break;
        case 98:
          HASH_RETURN(0x21553EF098133B62LL, v_diff_filename,
                      diff_filename);
          break;
        case 103:
          HASH_RETURN(0x0995591F2721DE67LL, v_main_file_name,
                      main_file_name);
          break;
        case 104:
          HASH_RETURN(0x77709D492BF62268LL, v_temp_source,
                      temp_source);
          HASH_RETURN(0x3DA350D79D71B068LL, v_memcheck_filename,
                      memcheck_filename);
          break;
        case 105:
          HASH_RETURN(0x51DA1403EA629E69LL, v_info_params,
                      info_params);
          break;
        case 106:
          HASH_RETURN(0x3CA2C0872CBE2A6ALL, v_cmd,
                      cmd);
          break;
        case 107:
          HASH_RETURN(0x3C2F961831E4EF6BLL, v_v,
                      v);
          break;
        case 111:
          HASH_RETURN(0x6511131491A84B6FLL, v_DETAILED,
                      DETAILED);
          break;
        case 120:
          HASH_RETURN(0x3A2F333AD4259E78LL, v_env,
                      env);
          HASH_RETURN(0x602FD94428C31D78LL, v_section,
                      section);
          break;
        case 121:
          HASH_RETURN(0x07627BF26E5ADC79LL, v_leak_check,
                      leak_check);
          break;
        case 132:
          HASH_RETURN(0x226B42FE3C272E84LL, v_exp_filename,
                      exp_filename);
          HASH_RETURN(0x0A7FC646CCAE2184LL, v_post,
                      post);
          break;
        case 133:
          HASH_RETURN(0x124AB4DF0D760A85LL, v_pass_options,
                      pass_options);
          break;
        case 134:
          HASH_RETURN(0x777832301B48C986LL, v_res,
                      res);
          break;
        case 137:
          HASH_RETURN(0x1E2E626C9AD36E89LL, v_PHP_FAILED_TESTS,
                      PHP_FAILED_TESTS);
          break;
        case 140:
          HASH_RETURN(0x21093C71DDF8728CLL, v_line,
                      line);
          break;
        case 151:
          HASH_RETURN(0x542C5475DF312C97LL, v_output_headers,
                      output_headers);
          break;
        case 158:
          HASH_RETURN(0x4AF7CD17F976719ELL, v_args,
                      args);
          HASH_RETURN(0x3DFF302A59E58C9ELL, v_clean_params,
                      clean_params);
          break;
        case 162:
          HASH_RETURN(0x26952AF4353F73A2LL, v_org_headers,
                      org_headers);
          break;
        case 163:
          HASH_RETURN(0x5713DB0D48D38CA3LL, v_warn,
                      warn);
          break;
        case 165:
          HASH_RETURN(0x6103500A19D056A5LL, v_tmp_post,
                      tmp_post);
          HASH_RETURN(0x6C9B335AC6D99FA5LL, v_restype,
                      restype);
          break;
        case 168:
          HASH_RETURN(0x72A70BA9E70E21A8LL, v_lines,
                      lines);
          break;
        case 169:
          HASH_RETURN(0x3DB1C471C0521CA9LL, v_passed,
                      passed);
          break;
        case 171:
          HASH_RETURN(0x17E58CCFC1C647ABLL, v_copy_file,
                      copy_file);
          break;
        case 178:
          HASH_RETURN(0x713E1D1D3A5A26B2LL, v_temp_dir,
                      temp_dir);
          break;
        case 183:
          HASH_RETURN(0x15B870ABDE28C8B7LL, v_php_cgi,
                      php_cgi);
          HASH_RETURN(0x13F556150800FCB7LL, v_want,
                      want);
          break;
        case 184:
          HASH_RETURN(0x64A29C4B117892B8LL, v_raw_lines,
                      raw_lines);
          break;
        case 186:
          HASH_RETURN(0x39D40BAF3B9E07BALL, v_test_idx,
                      test_idx);
          break;
        case 188:
          HASH_RETURN(0x1693E929DBB4D8BCLL, v_leaked,
                      leaked);
          break;
        case 191:
          HASH_RETURN(0x4CA1517149CE9EBFLL, v_secfile,
                      secfile);
          break;
        case 195:
          HASH_RETURN(0x346B87C4FC1A7AC3LL, v_no_clean,
                      no_clean);
          break;
        case 197:
          HASH_RETURN(0x7ECBAE0E38A06AC5LL, v_borked,
                      borked);
          HASH_RETURN(0x2CC444E2746C9CC5LL, v_ini_settings,
                      ini_settings);
          break;
        case 198:
          HASH_RETURN(0x2D2DC4D373BA0DC6LL, v_cfg,
                      cfg);
          break;
        case 201:
          HASH_RETURN(0x008317845DB80EC9LL, v_old_php,
                      old_php);
          break;
        case 203:
          HASH_RETURN(0x492E5AD12670C1CBLL, v_test_skipif,
                      test_skipif);
          break;
        case 205:
          HASH_RETURN(0x2B418956DA46A5CDLL, v_rh,
                      rh);
          break;
        case 207:
          HASH_RETURN(0x59B4E66CC0580FCFLL, v_IN_REDIRECT,
                      IN_REDIRECT);
          HASH_RETURN(0x0D6857FDDD7F21CFLL, v_k,
                      k);
          break;
        case 208:
          HASH_RETURN(0x0164D41D0D133BD0LL, v_failed_headers,
                      failed_headers);
          break;
        case 209:
          HASH_RETURN(0x318FBA9639C18FD1LL, v_temp_target,
                      temp_target);
          break;
        case 217:
          HASH_RETURN(0x388170F9A3693CD9LL, v_tested,
                      tested);
          HASH_RETURN(0x4E2B1129D57B97D9LL, v_temp_clean,
                      temp_clean);
          HASH_RETURN(0x771773B4B0FA21D9LL, v_test_clean,
                      test_clean);
          break;
        case 223:
          HASH_RETURN(0x7724714A93A2AFDFLL, v_temp_skipif,
                      temp_skipif);
          break;
        case 226:
          HASH_RETURN(0x79B5CE2558619AE2LL, v_unicode_semantics,
                      unicode_semantics);
          break;
        case 228:
          HASH_RETURN(0x20B9D4C6774304E4LL, v_headers,
                      headers);
          break;
        case 231:
          HASH_RETURN(0x3987AB204679E9E7LL, v_temp_filenames,
                      temp_filenames);
          break;
        case 234:
          HASH_RETURN(0x282030DBDD879FEALL, v_tested_file,
                      tested_file);
          break;
        case 235:
          HASH_RETURN(0x519638790DED9BEBLL, v_test_cnt,
                      test_cnt);
          HASH_RETURN(0x61B161496B7EA7EBLL, v_e,
                      e);
          break;
        case 236:
          HASH_RETURN(0x4E51CE0477F36FECLL, v_query_string,
                      query_string);
          break;
        case 240:
          HASH_RETURN(0x599AF95F802F1DF0LL, v_test_dir,
                      test_dir);
          HASH_RETURN(0x6B1E2C4A562FA3F0LL, v_output_filename,
                      output_filename);
          break;
        case 246:
          HASH_RETURN(0x6ECE84440D42ECF6LL, v_r,
                      r);
          break;
        case 248:
          HASH_RETURN(0x3136EAB90A81D5F8LL, v_shortname,
                      shortname);
          break;
        case 249:
          HASH_RETURN(0x53477D2D02A51AF9LL, v_secdone,
                      secdone);
          HASH_RETURN(0x2184B6DF967AE2F9LL, v_request,
                      request);
          break;
        case 251:
          HASH_RETURN(0x4E39DEBC85F298FBLL, v_test_file,
                      test_file);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_php, v_file, v_env, v_log_format, v_info_params, v_ini_overwrites, v_cwd, v_PHP_FAILED_TESTS, v_pass_options, v_DETAILED, v_IN_REDIRECT, v_test_cnt, v_test_idx, v_leak_check, v_temp_source, v_temp_target, v_cfg, v_environment, v_no_clean, v_temp_filenames, v_org_file, v_php_cgi, v_section_text, v_fp, v_borked, v_bork_info, v_line, v_r, v_section, v_secfile, v_secdone, v_shortname, v_tested_file, v_tested, v_old_php, v_test_dir, v_temp_dir, v_main_file_name, v_diff_filename, v_log_filename, v_exp_filename, v_output_filename, v_memcheck_filename, v_temp_file, v_test_file, v_temp_skipif, v_test_skipif, v_temp_clean, v_test_clean, v_tmp_post, v_tmp_relative_file, v_copy_file, v_e, v_ini_settings, v_info, v_warn, v_extra, v_output, v_reason, v_test_files, v_f, v_redirenv, v_unicode_semantics, v_query_string, v_args, v_post, v_raw_lines, v_request, v_res, v_cmd, v_content_length, v_out, v_clean_params, v_leaked, v_passed, v_headers, v_match, v_rh, v_failed_headers, v_want, v_wanted_headers, v_lines, v_org_headers, v_output_headers, v_k, v_v, v_wanted, v_wanted_re, v_restype);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  {
    v_log_format = ref(g->GV(log_format));
    v_info_params = ref(g->GV(info_params));
    v_ini_overwrites = ref(g->GV(ini_overwrites));
    v_cwd = ref(g->GV(cwd));
    v_PHP_FAILED_TESTS = ref(g->GV(PHP_FAILED_TESTS));
  }
  {
    v_pass_options = ref(g->GV(pass_options));
    v_DETAILED = ref(g->GV(DETAILED));
    v_IN_REDIRECT = ref(g->GV(IN_REDIRECT));
    v_test_cnt = ref(g->GV(test_cnt));
    v_test_idx = ref(g->GV(test_idx));
  }
  {
    v_leak_check = ref(g->GV(leak_check));
    v_temp_source = ref(g->GV(temp_source));
    v_temp_target = ref(g->GV(temp_target));
    v_cfg = ref(g->GV(cfg));
    v_environment = ref(g->GV(environment));
  }
  v_no_clean = ref(g->GV(no_clean));
  setNull(v_temp_filenames);
  (v_org_file = v_file);
  if (isset(v_env, "TEST_PHP_CGI_EXECUTABLE", 0x302C6A3F87E667A3LL)) {
    (v_php_cgi = v_env.rvalAt("TEST_PHP_CGI_EXECUTABLE", 0x302C6A3F87E667A3LL));
  }
  if (LINE(950,x_is_array(v_file))) (v_file = v_file.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  if (toBoolean(v_DETAILED)) echo(LINE(956,concat3("\n=================\nTEST ", toString(v_file), "\n")));
  (v_section_text = ScalarArrays::sa_[10]);
  (toBoolean((v_fp = LINE(970,x_fopen(toString(v_file), "rt"))))) || ((f_error(toString("Cannot open test file: ") + toString(v_file)), null));
  (v_borked = false);
  (v_bork_info = "");
  if (!(LINE(974,x_feof(toObject(v_fp))))) {
    (v_line = LINE(975,x_fgets(toObject(v_fp))));
  }
  else {
    (v_bork_info = LINE(977,concat3("empty test [", toString(v_file), "]")));
    (v_borked = true);
  }
  if (!(toBoolean(LINE(980,x_ereg("^--TEST--", toString(v_line), ref(v_r)))))) {
    (v_bork_info = LINE(981,concat3("tests must start with --TEST-- [", toString(v_file), "]")));
    (v_borked = true);
  }
  (v_section = "TEST");
  (v_secfile = false);
  (v_secdone = false);
  LOOP_COUNTER(1);
  {
    while (!(LINE(987,x_feof(toObject(v_fp))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_line = LINE(988,x_fgets(toObject(v_fp))));
        if (toBoolean(LINE(991,x_preg_match("/^--([_A-Z]+)--/", toString(v_line), ref(v_r))))) {
          (v_section = v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          v_section_text.set(v_section, (""));
          (v_secfile = equal(v_section, "FILE") || equal(v_section, "FILEEOF"));
          (v_secdone = false);
          continue;
        }
        if (!(toBoolean(v_secdone))) {
          concat_assign(lval(v_section_text.lvalAt(v_section)), toString(v_line));
        }
        if (toBoolean(v_secfile) && toBoolean(LINE(1005,x_preg_match("/^===DONE===/", toString(v_line), ref(v_r))))) {
          (v_secdone = true);
        }
      }
    }
  }
  if (equal((silenceInc(), silenceDec(LINE(1012,x_count(v_section_text.rvalAt("REDIRECTTEST", 0x2B8C373988E3AC56LL))))), 1LL)) {
    if (toBoolean(v_IN_REDIRECT)) {
      (v_borked = true);
      (v_bork_info = "Can't redirect a test from within a redirected test");
    }
    else {
      (v_borked = false);
    }
  }
  else {
    if (!equal(plus_rev((silenceInc(), silenceDec(LINE(1020,x_count(v_section_text.rvalAt("FILEEOF", 0x0537CD7D4BB22E4CLL))))), (silenceInc(), silenceDec(x_count(v_section_text.rvalAt("FILE", 0x7863294A8F33D14FLL))))), 1LL)) {
      (v_bork_info = "missing section --FILE--");
      (v_borked = true);
    }
    if (equal((silenceInc(), silenceDec(LINE(1024,x_count(v_section_text.rvalAt("FILEEOF", 0x0537CD7D4BB22E4CLL))))), 1LL)) {
      v_section_text.set("FILE", (LINE(1025,x_preg_replace("/[\r\n]+$/", "", v_section_text.rvalAt("FILEEOF", 0x0537CD7D4BB22E4CLL)))), 0x7863294A8F33D14FLL);
      v_section_text.weakRemove("FILEEOF", 0x0537CD7D4BB22E4CLL);
    }
    if (!equal((plus_rev((silenceInc(), silenceDec(LINE(1028,x_count(v_section_text.rvalAt("EXPECTREGEX", 0x304B9E394FBFDA24LL))))), plus_rev((silenceInc(), silenceDec(x_count(v_section_text.rvalAt("EXPECTF", 0x78CEAF60AAE95596LL)))), (silenceInc(), silenceDec(x_count(v_section_text.rvalAt("EXPECT", 0x36CC00F3D4F35017LL))))))), 1LL)) {
      (v_bork_info = "missing section --EXPECT--, --EXPECTF-- or --EXPECTREGEX--");
      (v_borked = true);
    }
  }
  LINE(1033,x_fclose(toObject(v_fp)));
  (v_shortname = LINE(1035,x_str_replace(concat(toString(v_cwd), "/"), "", v_file)));
  (v_tested_file = v_shortname);
  if (toBoolean(v_borked)) {
    LINE(1039,f_show_result("BORK", v_bork_info, v_tested_file));
    lval(v_PHP_FAILED_TESTS.lvalAt("BORKED", 0x6D22374225C91058LL)).append(((assignCallTemp(eo_0, v_file),assignCallTemp(eo_4, LINE(1045,concat4(toString(v_bork_info), " [", toString(v_file), "]"))),Array(ArrayInit(5).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "test_name", "", 0x6D72CEB44AC01301LL).set(2, "output", "", 0x6F443E367A5DBB24LL).set(3, "diff", "", 0x77421C16EDA60C7ALL).set(4, "info", eo_4, 0x59E9384E33988B3ELL).create()))));
    return "BORKED";
  }
  (v_tested = LINE(1050,x_trim(toString(v_section_text.rvalAt("TEST", 0x37349B25A0ED29E7LL)))));
  if (!(empty(v_section_text, "GET", 0x25DCCC35D69AD828LL)) || !(empty(v_section_text, "POST", 0x73E3781F017E3A02LL)) || !(empty(v_section_text, "POST_RAW", 0x4846DDBC13931D22LL)) || !(empty(v_section_text, "COOKIE", 0x2183317DB64F7D2CLL))) {
    if (isset(v_php_cgi)) {
      (v_old_php = v_php);
      (v_php = concat(toString(v_php_cgi), " -C "));
    }
    else if (!(toBoolean(LINE(1057,x_strncasecmp("Linux" /* PHP_OS */, "win", toInt32(3LL))))) && x_file_exists(concat(x_dirname(toString(v_php)), "/php-cgi.exe"))) {
      (v_old_php = v_php);
      (v_php = concat(toString(LINE(1059,x_realpath(concat(x_dirname(toString(v_php)), "/php-cgi.exe")))), " -C "));
    }
    else if (LINE(1060,x_file_exists("./sapi/cgi/php"))) {
      (v_old_php = v_php);
      (v_php = concat(toString(LINE(1062,x_realpath("./sapi/cgi/php"))), " -C "));
    }
    else {
      LINE(1064,f_show_result("SKIP", v_tested, v_tested_file, "reason: CGI not available"));
      return "SKIPPED";
    }
  }
  LINE(1069,f_show_test(v_test_idx, v_shortname));
  if (LINE(1071,x_is_array(v_IN_REDIRECT))) {
    (v_temp_dir = (v_test_dir = v_IN_REDIRECT.rvalAt("dir", 0x522821F5063592CFLL)));
  }
  else {
    (v_temp_dir = (v_test_dir = LINE(1074,x_realpath(x_dirname(toString(v_file))))));
  }
  if (toBoolean(v_temp_source) && toBoolean(v_temp_target)) {
    (v_temp_dir = LINE(1077,x_str_replace(v_temp_source, v_temp_target, v_temp_dir)));
  }
  (v_main_file_name = LINE(1080,x_basename(toString(v_file), "phpt")));
  (v_diff_filename = LINE(1082,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "diff")));
  (v_log_filename = LINE(1083,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "log")));
  (v_exp_filename = LINE(1084,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "exp")));
  (v_output_filename = LINE(1085,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "out")));
  (v_memcheck_filename = LINE(1086,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "mem")));
  (v_temp_file = LINE(1087,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "php")));
  (v_test_file = LINE(1088,concat4(toString(v_test_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "php")));
  (v_temp_skipif = LINE(1089,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "skip.php")));
  (v_test_skipif = LINE(1090,concat4(toString(v_test_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "skip.php")));
  (v_temp_clean = LINE(1091,concat4(toString(v_temp_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "clean.php")));
  (v_test_clean = LINE(1092,concat4(toString(v_test_dir), "/" /* DIRECTORY_SEPARATOR */, toString(v_main_file_name), "clean.php")));
  (v_tmp_post = LINE(1093,(assignCallTemp(eo_0, toString(v_temp_dir)),assignCallTemp(eo_2, x_uniqid("/phpt.")),concat3(eo_0, "/" /* DIRECTORY_SEPARATOR */, eo_2))));
  (v_tmp_relative_file = concat(toString(LINE(1094,(assignCallTemp(eo_0, concat(x_dirname(get_source_filename("roadsend/tests/run-tests.php")), "/" /* DIRECTORY_SEPARATOR */)),assignCallTemp(eo_2, v_test_file),x_str_replace(eo_0, "", eo_2)))), "t"));
  if (toBoolean(v_temp_source) && toBoolean(v_temp_target)) {
    concat_assign(v_temp_skipif, "s");
    concat_assign(v_temp_file, "s");
    concat_assign(v_temp_clean, "s");
    (v_copy_file = LINE(1100,(assignCallTemp(eo_0, toString(v_temp_dir)),assignCallTemp(eo_2, x_basename(toString(x_is_array(v_file) ? ((Variant)(v_file.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(v_file))))),concat4(eo_0, "/" /* DIRECTORY_SEPARATOR */, eo_2, ".phps"))));
    if (!(LINE(1101,x_is_dir(x_dirname(toString(v_copy_file)))))) {
      (toBoolean((silenceInc(), silenceDec(LINE(1102,(assignCallTemp(eo_0, x_dirname(toString(v_copy_file))),x_mkdir(eo_0, 511LL, true))))))) || ((f_error(concat("Cannot create output directory - ", x_dirname(toString(v_copy_file)))), null));
    }
    if (isset(v_section_text, "FILE", 0x7863294A8F33D14FLL)) {
      LINE(1105,f_save_text(v_copy_file, v_section_text.rvalAt("FILE", 0x7863294A8F33D14FLL)));
    }
    (v_temp_filenames = Array(ArrayInit(9).set(0, "file", v_copy_file, 0x612E37678CE7DB5BLL).set(1, "diff", v_diff_filename, 0x77421C16EDA60C7ALL).set(2, "log", v_log_filename, 0x5F6DB6D334848260LL).set(3, "exp", v_exp_filename, 0x253BC5524B9E45DCLL).set(4, "out", v_output_filename, 0x7C801AC012E9F722LL).set(5, "mem", v_memcheck_filename, 0x08623EE45B5DC808LL).set(6, "php", v_temp_file, 0x52B182B0A94E9C41LL).set(7, "skip", v_temp_skipif, 0x0A22E6725DCF1C8ALL).set(8, "clean", v_temp_clean, 0x09B438FD7866941DLL).create()));
  }
  if (LINE(1119,x_is_array(v_IN_REDIRECT))) {
    (v_tested = LINE(1120,(assignCallTemp(eo_0, toString(v_IN_REDIRECT.rvalAt("prefix", 0x1D3C3F3792B6F3B8LL))),assignCallTemp(eo_2, x_trim(toString(v_section_text.rvalAt("TEST", 0x37349B25A0ED29E7LL)))),concat3(eo_0, " ", eo_2))));
    (v_tested_file = v_tmp_relative_file);
    v_section_text.set("FILE", (LINE(1122,concat4("# original source file: ", toString(v_shortname), "\n", toString(v_section_text.rvalAt("FILE", 0x7863294A8F33D14FLL))))), 0x7863294A8F33D14FLL);
  }
  (silenceInc(), silenceDec(LINE(1126,x_unlink(toString(v_diff_filename)))));
  (silenceInc(), silenceDec(LINE(1127,x_unlink(toString(v_log_filename)))));
  (silenceInc(), silenceDec(LINE(1128,x_unlink(toString(v_exp_filename)))));
  (silenceInc(), silenceDec(LINE(1129,x_unlink(toString(v_output_filename)))));
  (silenceInc(), silenceDec(LINE(1130,x_unlink(toString(v_memcheck_filename)))));
  (silenceInc(), silenceDec(LINE(1131,x_unlink(toString(v_temp_file)))));
  (silenceInc(), silenceDec(LINE(1132,x_unlink(toString(v_test_file)))));
  (silenceInc(), silenceDec(LINE(1133,x_unlink(toString(v_temp_skipif)))));
  (silenceInc(), silenceDec(LINE(1134,x_unlink(toString(v_test_skipif)))));
  (silenceInc(), silenceDec(LINE(1135,x_unlink(toString(v_tmp_post)))));
  (silenceInc(), silenceDec(LINE(1136,x_unlink(toString(v_temp_clean)))));
  (silenceInc(), silenceDec(LINE(1137,x_unlink(toString(v_test_clean)))));
  v_env.set("REDIRECT_STATUS", (""), 0x4555FCE147CC6367LL);
  v_env.set("QUERY_STRING", (""), 0x5F2195E8968970F5LL);
  v_env.set("PATH_TRANSLATED", (""), 0x4FBE39835E83F5F7LL);
  v_env.set("SCRIPT_FILENAME", (""), 0x33CA526B796DEF19LL);
  v_env.set("REQUEST_METHOD", (""), 0x1A817C69BF3C8784LL);
  v_env.set("CONTENT_TYPE", (""), 0x2ED4BDA292A1B80FLL);
  v_env.set("CONTENT_LENGTH", (""), 0x2B41A250C00F34B0LL);
  if (!(empty(v_section_text, "ENV", 0x7FE53713306C21A2LL))) {
    {
      LOOP_COUNTER(2);
      Variant map3 = LINE(1148,(assignCallTemp(eo_1, x_trim(toString(v_section_text.rvalAt("ENV", 0x7FE53713306C21A2LL)))),x_explode("\n", eo_1)));
      for (ArrayIterPtr iter4 = map3.begin(); !iter4->end(); iter4->next()) {
        LOOP_COUNTER_CHECK(2);
        v_e = iter4->second();
        {
          (v_e = LINE(1149,(assignCallTemp(eo_1, x_trim(toString(v_e))),x_explode("=", eo_1, toInt32(2LL)))));
          if (!(empty(v_e, 0LL, 0x77CFA1EEF01BCA90LL)) && isset(v_e, 1LL, 0x5BCA7C69B794F8CELL)) {
            v_env.set(v_e.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), (v_e.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
          }
        }
      }
    }
  }
  (v_ini_settings = ScalarArrays::sa_[0]);
  LINE(1160,f_settings2array(v_ini_overwrites, ref(v_ini_settings)));
  if (LINE(1164,x_array_key_exists("INI", v_section_text))) {
    if (!same(LINE(1165,x_strpos(toString(v_section_text.rvalAt("INI", 0x1B3ED6203A6B8527LL)), "{PWD}")), false)) {
      v_section_text.set("INI", (LINE(1166,(assignCallTemp(eo_1, x_dirname(toString(v_file))),assignCallTemp(eo_2, v_section_text.rvalAt("INI", 0x1B3ED6203A6B8527LL)),x_str_replace("{PWD}", eo_1, eo_2)))), 0x1B3ED6203A6B8527LL);
    }
    LINE(1168,(assignCallTemp(eo_0, x_preg_split("/[\n\r]+/", v_section_text.rvalAt("INI", 0x1B3ED6203A6B8527LL))),assignCallTemp(eo_1, ref(v_ini_settings)),f_settings2array(eo_0, eo_1)));
  }
  LINE(1170,f_settings2params(ref(v_ini_settings)));
  (v_info = "");
  (v_warn = false);
  if (LINE(1175,x_array_key_exists("SKIPIF", v_section_text))) {
    if (toBoolean(LINE(1176,x_trim(toString(v_section_text.rvalAt("SKIPIF", 0x027C39A55E059F80LL)))))) {
      if (toBoolean(v_cfg.rvalAt("show", 0x61A2B0ADCF60937FLL).rvalAt("skip", 0x0A22E6725DCF1C8ALL))) {
        echo("\n========SKIP========\n");
        echo(toString(v_section_text.rvalAt("SKIPIF", 0x027C39A55E059F80LL)));
        echo("========DONE========\n");
      }
      LINE(1182,f_save_text(v_test_skipif, v_section_text.rvalAt("SKIPIF", 0x027C39A55E059F80LL), v_temp_skipif));
      (v_extra = !same(LINE(1183,x_substr("Linux" /* PHP_OS */, toInt32(0LL), toInt32(3LL))), "WIN") ? (("unset REQUEST_METHOD; unset QUERY_STRING; unset PATH_TRANSLATED; unset SCRIPT_FILENAME; unset REQUEST_METHOD;")) : (("")));
      (v_output = LINE(1185,(assignCallTemp(eo_0, concat(toString(v_extra), concat6(" ", toString(v_php), " -q ", toString(v_ini_settings), " ", toString(v_test_skipif)))),assignCallTemp(eo_1, v_env),f_system_with_timeout(eo_0, eo_1))));
      if (!(toBoolean(v_cfg.rvalAt("keep", 0x24E892C8CBA97286LL).rvalAt("skip", 0x0A22E6725DCF1C8ALL)))) {
        (silenceInc(), silenceDec(LINE(1187,x_unlink(toString(v_test_skipif)))));
      }
      if (!(toBoolean(LINE(1189,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_strncasecmp("skip", eo_1, toInt32(4LL))))))) {
        (v_reason = (toBoolean(LINE(1190,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_eregi("^skip[[:space:]]*(.+)$", eo_1))))) ? ((Variant)((assignCallTemp(eo_2, x_trim(toString(v_output))),x_eregi_replace("^skip[[:space:]]*(.+)$", "\\1", eo_2)))) : ((Variant)(false)));
        if (toBoolean(v_reason)) {
          LINE(1192,f_show_result("SKIP", v_tested, v_tested_file, toString("reason: ") + toString(v_reason), v_temp_filenames));
        }
        else {
          LINE(1194,f_show_result("SKIP", v_tested, v_tested_file, "", v_temp_filenames));
        }
        if (isset(v_old_php)) {
          (v_php = v_old_php);
        }
        if (!(toBoolean(v_cfg.rvalAt("keep", 0x24E892C8CBA97286LL).rvalAt("skip", 0x0A22E6725DCF1C8ALL)))) {
          (silenceInc(), silenceDec(LINE(1200,x_unlink(toString(v_test_skipif)))));
        }
        return "SKIPPED";
      }
      if (!(toBoolean(LINE(1204,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_strncasecmp("info", eo_1, toInt32(4LL))))))) {
        (v_reason = (toBoolean(LINE(1205,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_ereg("^info[[:space:]]*(.+)$", eo_1))))) ? ((Variant)((assignCallTemp(eo_2, x_trim(toString(v_output))),x_ereg_replace("^info[[:space:]]*(.+)$", "\\1", eo_2)))) : ((Variant)(false)));
        if (toBoolean(v_reason)) {
          (v_info = LINE(1207,concat3(" (info: ", toString(v_reason), ")")));
        }
      }
      if (!(toBoolean(LINE(1210,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_strncasecmp("warn", eo_1, toInt32(4LL))))))) {
        (v_reason = (toBoolean(LINE(1211,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_ereg("^warn[[:space:]]*(.+)$", eo_1))))) ? ((Variant)((assignCallTemp(eo_2, x_trim(toString(v_output))),x_ereg_replace("^warn[[:space:]]*(.+)$", "\\1", eo_2)))) : ((Variant)(false)));
        if (toBoolean(v_reason)) {
          (v_warn = true);
          (v_info = LINE(1214,concat3(" (warn: ", toString(v_reason), ")")));
        }
      }
    }
  }
  if (equal((silenceInc(), silenceDec(LINE(1220,x_count(v_section_text.rvalAt("REDIRECTTEST", 0x2B8C373988E3AC56LL))))), 1LL)) {
    (v_test_files = ScalarArrays::sa_[0]);
    (v_IN_REDIRECT = f_eval(toString(v_section_text.rvalAt("REDIRECTTEST", 0x2B8C373988E3AC56LL))));
    v_IN_REDIRECT.set("via", (LINE(1224,concat3("via [", toString(v_shortname), "]\n\t"))), 0x4C893F5E3A9F4C78LL);
    v_IN_REDIRECT.set("dir", (LINE(1225,x_realpath(x_dirname(toString(v_file))))), 0x522821F5063592CFLL);
    v_IN_REDIRECT.set("prefix", (LINE(1226,x_trim(toString(v_section_text.rvalAt("TEST", 0x37349B25A0ED29E7LL))))), 0x1D3C3F3792B6F3B8LL);
    if (equal((silenceInc(), silenceDec(LINE(1228,x_count(v_IN_REDIRECT.rvalAt("TESTS", 0x28E83CE1C0C43214LL))))), 1LL)) {
      if (LINE(1229,x_is_array(v_org_file))) {
        v_test_files.append((v_org_file.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
      }
      else {
        (g->GV(test_files) = v_test_files);
        LINE(1233,f_find_files(v_IN_REDIRECT.rvalAt("TESTS", 0x28E83CE1C0C43214LL)));
        {
          LOOP_COUNTER(5);
          Variant map6 = g->GV(test_files);
          for (ArrayIterPtr iter7 = map6.begin(); !iter7->end(); iter7->next()) {
            LOOP_COUNTER_CHECK(5);
            v_f = iter7->second();
            {
              v_test_files.append((Array(ArrayInit(2).set(0, v_f).set(1, v_file).create())));
            }
          }
        }
      }
      v_test_cnt += LINE(1238,x_count(v_test_files)) - 1LL;
      v_test_idx--;
      LINE(1241,f_show_redirect_start(v_IN_REDIRECT.rvalAt("TESTS", 0x28E83CE1C0C43214LL), v_tested, v_tested_file));
      (v_redirenv = LINE(1244,x_array_merge(2, v_environment, Array(ArrayInit(1).set(0, v_IN_REDIRECT.rvalAt("ENV", 0x7FE53713306C21A2LL)).create()))));
      v_redirenv.set("REDIR_TEST_DIR", (concat(toString(LINE(1245,x_realpath(toString(v_IN_REDIRECT.rvalAt("TESTS", 0x28E83CE1C0C43214LL))))), "/" /* DIRECTORY_SEPARATOR */)), 0x0154CCB79CB10A34LL);
      LINE(1247,x_usort(ref(v_test_files), "test_sort"));
      LINE(1248,f_run_all_tests(v_test_files, v_redirenv, v_tested));
      LINE(1250,f_show_redirect_ends(v_IN_REDIRECT.rvalAt("TESTS", 0x28E83CE1C0C43214LL), v_tested, v_tested_file));
      (v_IN_REDIRECT = false);
      return "REDIR";
    }
    else {
      (v_bork_info = "Redirect info must contain exactly one TEST string to be used as redirect directory.");
      LINE(1257,f_show_result("BORK", v_bork_info, "", v_temp_filenames));
      lval(v_PHP_FAILED_TESTS.lvalAt("BORKED", 0x6D22374225C91058LL)).append(((assignCallTemp(eo_0, v_file),assignCallTemp(eo_4, LINE(1263,concat4(toString(v_bork_info), " [", toString(v_file), "]"))),assignCallTemp(eo_5, v_unicode_semantics),Array(ArrayInit(6).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "test_name", "", 0x6D72CEB44AC01301LL).set(2, "output", "", 0x6F443E367A5DBB24LL).set(3, "diff", "", 0x77421C16EDA60C7ALL).set(4, "info", eo_4, 0x59E9384E33988B3ELL).set(5, "unicode", eo_5, 0x46ED0775F42090D7LL).create()))));
    }
  }
  if (LINE(1268,x_is_array(v_org_file)) || equal((silenceInc(), silenceDec(x_count(v_section_text.rvalAt("REDIRECTTEST", 0x2B8C373988E3AC56LL)))), 1LL)) {
    if (LINE(1269,x_is_array(v_org_file))) (v_file = v_org_file.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
    (v_bork_info = "Redirected test did not contain redirection info");
    LINE(1271,f_show_result("BORK", v_bork_info, "", v_temp_filenames));
    lval(v_PHP_FAILED_TESTS.lvalAt("BORKED", 0x6D22374225C91058LL)).append(((assignCallTemp(eo_0, v_file),assignCallTemp(eo_4, LINE(1277,concat4(toString(v_bork_info), " [", toString(v_file), "]"))),Array(ArrayInit(5).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "test_name", "", 0x6D72CEB44AC01301LL).set(2, "output", "", 0x6F443E367A5DBB24LL).set(3, "diff", "", 0x77421C16EDA60C7ALL).set(4, "info", eo_4, 0x59E9384E33988B3ELL).create()))));
    return "BORKED";
  }
  if (toBoolean(v_cfg.rvalAt("show", 0x61A2B0ADCF60937FLL).rvalAt("php", 0x52B182B0A94E9C41LL))) {
    echo("\n========TEST========\n");
    echo(toString(v_section_text.rvalAt("FILE", 0x7863294A8F33D14FLL)));
    echo("========DONE========\n");
  }
  LINE(1290,f_save_text(v_test_file, v_section_text.rvalAt("FILE", 0x7863294A8F33D14FLL), v_temp_file));
  if (LINE(1291,x_array_key_exists("GET", v_section_text))) {
    (v_query_string = LINE(1292,x_trim(toString(v_section_text.rvalAt("GET", 0x25DCCC35D69AD828LL)))));
  }
  else {
    (v_query_string = "");
  }
  v_env.set("REDIRECT_STATUS", ("1"), 0x4555FCE147CC6367LL);
  v_env.set("QUERY_STRING", (v_query_string), 0x5F2195E8968970F5LL);
  v_env.set("PATH_TRANSLATED", (v_test_file), 0x4FBE39835E83F5F7LL);
  v_env.set("SCRIPT_FILENAME", (v_test_file), 0x33CA526B796DEF19LL);
  if (LINE(1302,x_array_key_exists("COOKIE", v_section_text))) {
    v_env.set("HTTP_COOKIE", (LINE(1303,x_trim(toString(v_section_text.rvalAt("COOKIE", 0x2183317DB64F7D2CLL))))), 0x24BE882F9BBA080FLL);
  }
  else {
    v_env.set("HTTP_COOKIE", (""), 0x24BE882F9BBA080FLL);
  }
  (v_args = toBoolean(v_section_text.rvalAt("ARGS", 0x7A24AFD8ADE43B8ELL)) ? ((Variant)(concat(" -- ", toString(v_section_text.rvalAt("ARGS", 0x7A24AFD8ADE43B8ELL))))) : ((Variant)("")));
  if (LINE(1310,x_array_key_exists("POST_RAW", v_section_text)) && !(empty(v_section_text, "POST_RAW", 0x4846DDBC13931D22LL))) {
    (v_post = LINE(1311,x_trim(toString(v_section_text.rvalAt("POST_RAW", 0x4846DDBC13931D22LL)))));
    (v_raw_lines = LINE(1312,x_explode("\n", toString(v_post))));
    (v_request = "");
    {
      LOOP_COUNTER(8);
      for (ArrayIterPtr iter10 = v_raw_lines.begin(); !iter10->end(); iter10->next()) {
        LOOP_COUNTER_CHECK(8);
        v_line = iter10->second();
        {
          if (empty(v_env, "CONTENT_TYPE", 0x2ED4BDA292A1B80FLL) && toBoolean(LINE(1316,x_eregi("^(Content-Type:)(.*)", toString(v_line), ref(v_res))))) {
            v_env.set("CONTENT_TYPE", (LINE(1317,x_trim(toString(x_str_replace("\r", "", v_res.rvalAt(2LL, 0x486AFCC090D5F98CLL)))))), 0x2ED4BDA292A1B80FLL);
            continue;
          }
          concat_assign(v_request, concat(toString(v_line), "\n"));
        }
      }
    }
    v_env.set("CONTENT_LENGTH", (LINE(1323,x_strlen(toString(v_request)))), 0x2B41A250C00F34B0LL);
    v_env.set("REQUEST_METHOD", ("POST"), 0x1A817C69BF3C8784LL);
    if (empty(v_request)) {
      return "BORKED";
    }
    LINE(1329,f_save_text(v_tmp_post, v_request));
    (v_cmd = concat(toString(v_php), LINE(1330,concat6(toString(v_pass_options), toString(v_ini_settings), " -f \"", toString(v_test_file), "\" 2>&1 < ", toString(v_tmp_post)))));
  }
  else if (LINE(1331,x_array_key_exists("POST", v_section_text)) && !(empty(v_section_text, "POST", 0x73E3781F017E3A02LL))) {
    (v_post = LINE(1333,x_trim(toString(v_section_text.rvalAt("POST", 0x73E3781F017E3A02LL)))));
    LINE(1334,f_save_text(v_tmp_post, v_post));
    (v_content_length = LINE(1335,x_strlen(toString(v_post))));
    v_env.set("REQUEST_METHOD", ("POST"), 0x1A817C69BF3C8784LL);
    v_env.set("CONTENT_TYPE", ("application/x-www-form-urlencoded"), 0x2ED4BDA292A1B80FLL);
    v_env.set("CONTENT_LENGTH", (v_content_length), 0x2B41A250C00F34B0LL);
    (v_cmd = concat(toString(v_php), LINE(1341,concat6(toString(v_pass_options), toString(v_ini_settings), " -f \"", toString(v_test_file), "\" 2>&1 < ", toString(v_tmp_post)))));
  }
  else {
    v_env.set("REQUEST_METHOD", ("GET"), 0x1A817C69BF3C8784LL);
    v_env.set("CONTENT_TYPE", (""), 0x2ED4BDA292A1B80FLL);
    v_env.set("CONTENT_LENGTH", (""), 0x2B41A250C00F34B0LL);
    (v_cmd = LINE(1349,(assignCallTemp(eo_0, toString(v_php)),assignCallTemp(eo_1, toString(v_pass_options)),assignCallTemp(eo_2, concat6(toString(v_ini_settings), " -f \"", toString(v_test_file), "\" ", toString(v_args), " 2>&1")),concat3(eo_0, eo_1, eo_2))));
  }
  if (toBoolean(v_leak_check)) {
    v_env.set("USE_ZEND_ALLOC", ("0"), 0x55AC0D6A619F8A61LL);
    (v_cmd = LINE(1354,concat4("valgrind -q --tool=memcheck --trace-children=yes --log-file-exactly=", toString(v_memcheck_filename), " ", toString(v_cmd))));
  }
  else {
    v_env.set("USE_ZEND_ALLOC", ("1"), 0x55AC0D6A619F8A61LL);
  }
  if (toBoolean(v_DETAILED)) echo(concat_rev(LINE(1370,(assignCallTemp(eo_0, toString(v_env.rvalAt("REQUEST_METHOD", 0x1A817C69BF3C8784LL))),assignCallTemp(eo_2, toString(v_env.rvalAt("SCRIPT_FILENAME", 0x33CA526B796DEF19LL))),assignCallTemp(eo_4, toString(v_env.rvalAt("HTTP_COOKIE", 0x24BE882F9BBA080FLL))),assignCallTemp(eo_5, concat3("\nCOMMAND ", toString(v_cmd), "\n")),concat6(eo_0, "\nSCRIPT_FILENAME = ", eo_2, "\nHTTP_COOKIE     = ", eo_4, eo_5))), concat_rev(concat6(toString(v_env.rvalAt("PATH_TRANSLATED", 0x4FBE39835E83F5F7LL)), "\nQUERY_STRING    = ", toString(v_env.rvalAt("QUERY_STRING", 0x5F2195E8968970F5LL)), "\nREDIRECT_STATUS = ", toString(v_env.rvalAt("REDIRECT_STATUS", 0x4555FCE147CC6367LL)), "\nREQUEST_METHOD  = "), concat5("\nCONTENT_LENGTH  = ", toString(v_env.rvalAt("CONTENT_LENGTH", 0x2B41A250C00F34B0LL)), "\nCONTENT_TYPE    = ", toString(v_env.rvalAt("CONTENT_TYPE", 0x2ED4BDA292A1B80FLL)), "\nPATH_TRANSLATED = "))));
  (v_out = LINE(1372,f_system_with_timeout(v_cmd, v_env, isset(v_section_text, "STDIN", 0x032395AA518739D2LL) ? ((Variant)(v_section_text.rvalAt("STDIN", 0x032395AA518739D2LL))) : ((Variant)(null)))));
  if (LINE(1374,x_array_key_exists("CLEAN", v_section_text)) && (!(toBoolean(v_no_clean)) || toBoolean(v_cfg.rvalAt("keep", 0x24E892C8CBA97286LL).rvalAt("clean", 0x09B438FD7866941DLL)))) {
    if (toBoolean(LINE(1375,x_trim(toString(v_section_text.rvalAt("CLEAN", 0x25966E74328D3D22LL)))))) {
      if (toBoolean(v_cfg.rvalAt("show", 0x61A2B0ADCF60937FLL).rvalAt("clean", 0x09B438FD7866941DLL))) {
        echo("\n========CLEAN=======\n");
        echo(toString(v_section_text.rvalAt("CLEAN", 0x25966E74328D3D22LL)));
        echo("========DONE========\n");
      }
      LINE(1381,(assignCallTemp(eo_0, v_test_clean),assignCallTemp(eo_1, x_trim(toString(v_section_text.rvalAt("CLEAN", 0x25966E74328D3D22LL)))),assignCallTemp(eo_2, v_temp_clean),f_save_text(eo_0, eo_1, eo_2)));
      if (!(toBoolean(v_no_clean))) {
        (v_clean_params = ScalarArrays::sa_[0]);
        LINE(1384,f_settings2array(v_ini_overwrites, ref(v_clean_params)));
        LINE(1385,f_settings2params(ref(v_clean_params)));
        (v_extra = !same(LINE(1386,x_substr("Linux" /* PHP_OS */, toInt32(0LL), toInt32(3LL))), "WIN") ? (("unset REQUEST_METHOD; unset QUERY_STRING; unset PATH_TRANSLATED; unset SCRIPT_FILENAME; unset REQUEST_METHOD;")) : (("")));
        LINE(1388,(assignCallTemp(eo_0, concat(toString(v_extra), concat6(" ", toString(v_php), " -q ", toString(v_clean_params), " ", toString(v_test_clean)))),assignCallTemp(eo_1, v_env),f_system_with_timeout(eo_0, eo_1)));
      }
      if (!(toBoolean(v_cfg.rvalAt("keep", 0x24E892C8CBA97286LL).rvalAt("clean", 0x09B438FD7866941DLL)))) {
        (silenceInc(), silenceDec(LINE(1391,x_unlink(toString(v_test_clean)))));
      }
    }
  }
  (silenceInc(), silenceDec(LINE(1396,x_unlink(toString(v_tmp_post)))));
  (v_leaked = false);
  (v_passed = false);
  if (toBoolean(v_leak_check)) {
    (v_leaked = more((silenceInc(), silenceDec(LINE(1402,x_filesize(toString(v_memcheck_filename))))), 0LL));
    if (!(toBoolean(v_leaked))) {
      (silenceInc(), silenceDec(LINE(1404,x_unlink(toString(v_memcheck_filename)))));
    }
  }
  (v_output = LINE(1409,(assignCallTemp(eo_2, x_trim(toString(v_out))),x_str_replace("\r\n", "\n", eo_2))));
  (v_headers = "");
  if (isset(v_old_php) && toBoolean(LINE(1413,x_preg_match("/^(.*\?)\r\?\n\r\?\n(.*)/s", toString(v_out), ref(v_match))))) {
    (v_output = LINE(1414,x_trim(toString(v_match.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
    (v_rh = LINE(1415,x_preg_split("/[\n\r]+/", v_match.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
    (v_headers = ScalarArrays::sa_[0]);
    {
      LOOP_COUNTER(11);
      for (ArrayIterPtr iter13 = v_rh.begin(); !iter13->end(); iter13->next()) {
        LOOP_COUNTER_CHECK(11);
        v_line = iter13->second();
        {
          if (!same(LINE(1418,x_strpos(toString(v_line), ":")), false)) {
            (v_line = LINE(1419,x_explode(":", toString(v_line), toInt32(2LL))));
            v_headers.set(LINE(1420,x_trim(toString(v_line.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), (x_trim(toString(v_line.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
          }
        }
      }
    }
  }
  (v_failed_headers = false);
  if (isset(v_section_text, "EXPECTHEADERS", 0x00D5151289D778CCLL)) {
    (v_want = ScalarArrays::sa_[0]);
    (v_wanted_headers = ScalarArrays::sa_[0]);
    (v_lines = LINE(1429,x_preg_split("/[\n\r]+/", v_section_text.rvalAt("EXPECTHEADERS", 0x00D5151289D778CCLL))));
    {
      LOOP_COUNTER(14);
      for (ArrayIterPtr iter16 = v_lines.begin(); !iter16->end(); iter16->next()) {
        LOOP_COUNTER_CHECK(14);
        v_line = iter16->second();
        {
          if (!same(LINE(1431,x_strpos(toString(v_line), ":")), false)) {
            (v_line = LINE(1432,x_explode(":", toString(v_line), toInt32(2LL))));
            v_want.set(LINE(1433,x_trim(toString(v_line.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), (x_trim(toString(v_line.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
            v_wanted_headers.append((concat_rev(LINE(1434,x_trim(toString(v_line.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))), concat(x_trim(toString(v_line.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), ": "))));
          }
        }
      }
    }
    (v_org_headers = v_headers);
    (v_headers = ScalarArrays::sa_[0]);
    (v_output_headers = ScalarArrays::sa_[0]);
    {
      LOOP_COUNTER(17);
      for (ArrayIterPtr iter19 = v_want.begin(); !iter19->end(); iter19->next()) {
        LOOP_COUNTER_CHECK(17);
        v_v = iter19->second();
        v_k = iter19->first();
        {
          if (isset(v_org_headers, v_k)) {
            (v_headers = v_org_headers.rvalAt(v_k));
            v_output_headers.append((LINE(1443,concat3(toString(v_k), ": ", toString(v_org_headers.rvalAt(v_k))))));
          }
          if (!(isset(v_org_headers, v_k)) || !equal(v_org_headers.rvalAt(v_k), v_v)) {
            (v_failed_headers = true);
          }
        }
      }
    }
    LINE(1449,x_ksort(ref(v_wanted_headers)));
    (v_wanted_headers = LINE(1450,x_join("\n", v_wanted_headers)));
    LINE(1451,x_ksort(ref(v_output_headers)));
    (v_output_headers = LINE(1452,x_join("\n", v_output_headers)));
  }
  if (isset(v_section_text, "EXPECTF", 0x78CEAF60AAE95596LL) || isset(v_section_text, "EXPECTREGEX", 0x304B9E394FBFDA24LL)) {
    if (isset(v_section_text, "EXPECTF", 0x78CEAF60AAE95596LL)) {
      (v_wanted = LINE(1457,x_trim(toString(v_section_text.rvalAt("EXPECTF", 0x78CEAF60AAE95596LL)))));
    }
    else {
      (v_wanted = LINE(1459,x_trim(toString(v_section_text.rvalAt("EXPECTREGEX", 0x304B9E394FBFDA24LL)))));
    }
    (v_wanted_re = LINE(1461,x_preg_replace("/\\r\\n/", "\n", v_wanted)));
    if (isset(v_section_text, "EXPECTF", 0x78CEAF60AAE95596LL)) {
      (v_wanted_re = LINE(1463,x_preg_quote(toString(v_wanted_re), "/")));
      (v_wanted_re = LINE(1465,x_str_replace("%e", "\\/", v_wanted_re)));
      (v_wanted_re = LINE(1466,x_str_replace("%s", ".+\?", v_wanted_re)));
      (v_wanted_re = LINE(1467,x_str_replace("%w", "\\s*", v_wanted_re)));
      (v_wanted_re = LINE(1468,x_str_replace("%i", "[+\\-]\?[0-9]+", v_wanted_re)));
      (v_wanted_re = LINE(1469,x_str_replace("%d", "[0-9]+", v_wanted_re)));
      (v_wanted_re = LINE(1470,x_str_replace("%x", "[0-9a-fA-F]+", v_wanted_re)));
      (v_wanted_re = LINE(1471,x_str_replace("%f", "[+\\-]\?\\.\?[0-9]+\\.\?[0-9]*(E-\?[0-9]+)\?", v_wanted_re)));
      (v_wanted_re = LINE(1472,x_str_replace("%c", ".", v_wanted_re)));
    }
    if (toBoolean(LINE(1480,(assignCallTemp(eo_0, concat3("/^", toString(v_wanted_re), "$/s")),assignCallTemp(eo_1, toString(v_output)),x_preg_match(eo_0, eo_1))))) {
      (v_passed = true);
      if (!(toBoolean(v_cfg.rvalAt("keep", 0x24E892C8CBA97286LL).rvalAt("php", 0x52B182B0A94E9C41LL)))) {
        (silenceInc(), silenceDec(LINE(1483,x_unlink(toString(v_test_file)))));
      }
      if (isset(v_old_php)) {
        (v_php = v_old_php);
      }
      if (!(toBoolean(v_leaked)) && !(toBoolean(v_failed_headers))) {
        LINE(1489,f_show_result("PASS", v_tested, v_tested_file, "", v_temp_filenames));
        return "PASSED";
      }
    }
  }
  else {
    (v_wanted = LINE(1494,x_trim(toString(v_section_text.rvalAt("EXPECT", 0x36CC00F3D4F35017LL)))));
    (v_wanted = LINE(1495,x_preg_replace("/\\r\\n/", "\n", v_wanted)));
    if (!(toBoolean(LINE(1497,x_strcmp(toString(v_output), toString(v_wanted)))))) {
      (v_passed = true);
      if (!(toBoolean(v_cfg.rvalAt("keep", 0x24E892C8CBA97286LL).rvalAt("php", 0x52B182B0A94E9C41LL)))) {
        (silenceInc(), silenceDec(LINE(1500,x_unlink(toString(v_test_file)))));
      }
      if (isset(v_old_php)) {
        (v_php = v_old_php);
      }
      if (!(toBoolean(v_leaked)) && !(toBoolean(v_failed_headers))) {
        LINE(1506,f_show_result("PASS", v_tested, v_tested_file, "", v_temp_filenames));
        return "PASSED";
      }
    }
    setNull(v_wanted_re);
  }
  if (toBoolean(v_failed_headers)) {
    (v_passed = false);
    (v_wanted = LINE(1516,concat3(toString(v_wanted_headers), "\n--HEADERS--\n", toString(v_wanted))));
    (v_output = LINE(1517,concat3(toString(v_output_headers), "\n--HEADERS--\n", toString(v_output))));
    if (isset(v_wanted_re)) {
      (v_wanted_re = LINE(1519,concat3(toString(v_wanted_headers), "\n--HEADERS--\n", toString(v_wanted_re))));
    }
  }
  if (toBoolean(v_leaked)) {
    (v_restype = "LEAK");
  }
  else if (toBoolean(v_warn)) {
    (v_restype = "WARN");
  }
  else {
    (v_restype = "FAIL");
  }
  if (!(toBoolean(v_passed))) {
    if (!same(LINE(1533,x_strpos(toString(v_log_format), "E")), false) && same(x_file_put_contents(toString(v_exp_filename), v_wanted), false)) {
      LINE(1534,f_error(toString("Cannot create expected test output - ") + toString(v_exp_filename)));
    }
    if (!same(LINE(1538,x_strpos(toString(v_log_format), "O")), false) && same(x_file_put_contents(toString(v_output_filename), v_output), false)) {
      LINE(1539,f_error(toString("Cannot create test output - ") + toString(v_output_filename)));
    }
    if (!same(LINE(1543,x_strpos(toString(v_log_format), "D")), false) && same((assignCallTemp(eo_0, toString(v_diff_filename)),assignCallTemp(eo_1, f_generate_diff(v_wanted, v_wanted_re, v_output)),x_file_put_contents(eo_0, eo_1)), false)) {
      LINE(1544,f_error(toString("Cannot create test diff - ") + toString(v_diff_filename)));
    }
    if (!same(LINE(1548,x_strpos(toString(v_log_format), "L")), false) && same(LINE(1556,(assignCallTemp(eo_0, toString(v_log_filename)),assignCallTemp(eo_1, concat5("\n---- EXPECTED OUTPUT\n", toString(v_wanted), "\n---- ACTUAL OUTPUT\n", toString(v_output), "\n---- FAILED\n")),x_file_put_contents(eo_0, eo_1))), false)) {
      LINE(1557,f_error(toString("Cannot create test log - ") + toString(v_log_filename)));
      LINE(1558,f_error_report(v_file, v_log_filename, v_tested));
    }
  }
  LINE(1562,f_show_result(v_restype, v_tested, v_tested_file, v_info, v_temp_filenames));
  lval(v_PHP_FAILED_TESTS.lvalAt(concat(toString(v_restype), "ED"))).append(((assignCallTemp(eo_0, v_file),assignCallTemp(eo_1, LINE(1566,(assignCallTemp(eo_5, (toString(x_is_array(v_IN_REDIRECT) ? ((Variant)(v_IN_REDIRECT.rvalAt("via", 0x4C893F5E3A9F4C78LL))) : ((Variant)(""))))),assignCallTemp(eo_6, toString(v_tested)),assignCallTemp(eo_7, concat3(" [", toString(v_tested_file), "]")),concat3(eo_5, eo_6, eo_7)))),assignCallTemp(eo_2, v_output_filename),assignCallTemp(eo_3, v_diff_filename),assignCallTemp(eo_4, v_info),Array(ArrayInit(5).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "test_name", eo_1, 0x6D72CEB44AC01301LL).set(2, "output", eo_2, 0x6F443E367A5DBB24LL).set(3, "diff", eo_3, 0x77421C16EDA60C7ALL).set(4, "info", eo_4, 0x59E9384E33988B3ELL).create()))));
  if (isset(v_old_php)) {
    (v_php = v_old_php);
  }
  return concat(toString(v_restype), "ED");
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1709 */
void f_settings2params(Variant v_ini_settings) {
  FUNCTION_INJECTION(settings2params);
  String v_settings;
  Primitive v_name = 0;
  Variant v_value;

  (v_settings = "");
  {
    LOOP_COUNTER(20);
    for (ArrayIterPtr iter22 = v_ini_settings.begin(); !iter22->end(); iter22->next()) {
      LOOP_COUNTER_CHECK(20);
      v_value = iter22->second();
      v_name = iter22->first();
      {
        (v_value = LINE(1713,x_addslashes(toString(v_value))));
        concat_assign(v_settings, LINE(1714,concat5(" -d \"", toString(v_name), "=", toString(v_value), "\"")));
      }
    }
  }
  (v_ini_settings = v_settings);
} /* function */
/* SRC: roadsend/tests/run-tests.php line 596 */
Variant f_test_name(CVarRef v_name) {
  FUNCTION_INJECTION(test_name);
  if (LINE(598,x_is_array(v_name))) {
    return LINE(599,concat3(toString(v_name.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), ":", toString(v_name.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
  }
  else {
    return v_name;
  }
  return null;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1691 */
void f_error(CStrRef v_message) {
  FUNCTION_INJECTION(error);
  echo(LINE(1693,concat3("ERROR: ", v_message, "\n")));
  f_exit(1LL);
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1893 */
void f_show_result(CVarRef v_result, Variant v_tested, CVarRef v_tested_file, Variant v_extra //  = ""
, CVarRef v_temp_filenames //  = null_variant
) {
  FUNCTION_INJECTION(show_result);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_html_output __attribute__((__unused__)) = g->GV(html_output);
  Variant &gv_html_file __attribute__((__unused__)) = g->GV(html_file);
  Variant &gv_temp_target __attribute__((__unused__)) = g->GV(temp_target);
  Variant &gv_temp_urlbase __attribute__((__unused__)) = g->GV(temp_urlbase);
  Variant v_url;
  String v_diff;
  String v_mem;

  {
  }
  echo(LINE(1897,(assignCallTemp(eo_0, toString(v_result)),assignCallTemp(eo_2, concat6(toString(v_tested), " [", toString(v_tested_file), "] ", toString(v_extra), "\n")),concat3(eo_0, " ", eo_2))));
  if (toBoolean(gv_html_output)) {
    if (isset(v_temp_filenames, "file", 0x612E37678CE7DB5BLL) && toBoolean((silenceInc(), silenceDec(LINE(1901,x_file_exists(toString(v_temp_filenames.rvalAt("file", 0x612E37678CE7DB5BLL)))))))) {
      (v_url = LINE(1902,x_str_replace(gv_temp_target, gv_temp_urlbase, v_temp_filenames.rvalAt("file", 0x612E37678CE7DB5BLL))));
      (v_tested = LINE(1903,concat5("<a href='", toString(v_url), "'>", toString(v_tested), "</a>")));
    }
    if (isset(v_temp_filenames, "skip", 0x0A22E6725DCF1C8ALL) && toBoolean((silenceInc(), silenceDec(LINE(1905,x_file_exists(toString(v_temp_filenames.rvalAt("skip", 0x0A22E6725DCF1C8ALL)))))))) {
      if (empty(v_extra)) {
        (v_extra = "skipif");
      }
      (v_url = LINE(1909,x_str_replace(gv_temp_target, gv_temp_urlbase, v_temp_filenames.rvalAt("skip", 0x0A22E6725DCF1C8ALL))));
      (v_extra = LINE(1910,concat5("<a href='", toString(v_url), "'>", toString(v_extra), "</a>")));
    }
    else if (empty(v_extra)) {
      (v_extra = "&nbsp;");
    }
    if (isset(v_temp_filenames, "diff", 0x77421C16EDA60C7ALL) && toBoolean((silenceInc(), silenceDec(LINE(1914,x_file_exists(toString(v_temp_filenames.rvalAt("diff", 0x77421C16EDA60C7ALL)))))))) {
      (v_url = LINE(1915,x_str_replace(gv_temp_target, gv_temp_urlbase, v_temp_filenames.rvalAt("diff", 0x77421C16EDA60C7ALL))));
      (v_diff = LINE(1916,concat3("<a href='", toString(v_url), "'>diff</a>")));
    }
    else {
      (v_diff = "&nbsp;");
    }
    if (isset(v_temp_filenames, "mem", 0x08623EE45B5DC808LL) && toBoolean((silenceInc(), silenceDec(LINE(1920,x_file_exists(toString(v_temp_filenames.rvalAt("mem", 0x08623EE45B5DC808LL)))))))) {
      (v_url = LINE(1921,x_str_replace(gv_temp_target, gv_temp_urlbase, v_temp_filenames.rvalAt("mem", 0x08623EE45B5DC808LL))));
      (v_mem = LINE(1922,concat3("<a href='", toString(v_url), "'>leaks</a>")));
    }
    else {
      (v_mem = "&nbsp;");
    }
    LINE(1933,(assignCallTemp(eo_0, toObject(gv_html_file)),assignCallTemp(eo_1, (assignCallTemp(eo_3, LINE(1928,concat3("<td>", toString(v_result), "</td>"))),assignCallTemp(eo_4, LINE(1929,concat3("<td>", toString(v_tested), "</td>"))),assignCallTemp(eo_5, LINE(1930,concat3("<td>", toString(v_extra), "</td>"))),assignCallTemp(eo_6, LINE(1931,concat3("<td>", v_diff, "</td>"))),assignCallTemp(eo_7, LINE(1933,concat3("<td>", v_mem, "</td></tr>\n"))),concat6("<tr>", eo_3, eo_4, eo_5, eo_6, eo_7))),x_fwrite(eo_0, eo_1)));
  }
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1874 */
void f_show_redirect_ends(CVarRef v_tests, CVarRef v_tested, CVarRef v_tested_file) {
  FUNCTION_INJECTION(show_redirect_ends);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_html_output __attribute__((__unused__)) = g->GV(html_output);
  Variant &gv_html_file __attribute__((__unused__)) = g->GV(html_file);
  {
  }
  if (toBoolean(gv_html_output)) {
    LINE(1880,(assignCallTemp(eo_0, toObject(gv_html_file)),assignCallTemp(eo_1, concat("<tr><td colspan='3'>---&gt; ", concat6(toString(v_tests), " (", toString(v_tested), " [", toString(v_tested_file), "]) done</td></tr>\n"))),x_fwrite(eo_0, eo_1)));
  }
  echo(concat("---> ", LINE(1882,concat6(toString(v_tests), " (", toString(v_tested), " [", toString(v_tested_file), "]) done\n"))));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1828 */
void f_show_start(CVarRef v_start_time) {
  FUNCTION_INJECTION(show_start);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_html_output __attribute__((__unused__)) = g->GV(html_output);
  Variant &gv_html_file __attribute__((__unused__)) = g->GV(html_file);
  {
  }
  if (toBoolean(gv_html_output)) {
    LINE(1834,(assignCallTemp(eo_0, toObject(gv_html_file)),assignCallTemp(eo_1, (assignCallTemp(eo_3, toString((silenceInc(), silenceDec(x_date("Y-m-d H:i:s", toInt64(v_start_time)))))),concat3("<h2>Time Start: ", eo_3, "</h2>\n"))),x_fwrite(eo_0, eo_1)));
    LINE(1835,x_fwrite(toObject(gv_html_file), "<table>\n"));
  }
  echo(LINE(1837,(assignCallTemp(eo_1, toString((silenceInc(), silenceDec(x_date("Y-m-d H:i:s", toInt64(v_start_time)))))),concat3("TIME START ", eo_1, "\n=====================================================================\n"))));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 833 */
void f_error_report(Variant v_testname, Variant v_logname, CVarRef v_tested) {
  FUNCTION_INJECTION(error_report);
  (v_testname = LINE(835,x_realpath(toString(v_testname))));
  (v_logname = LINE(836,x_realpath(toString(v_logname))));
  {
    String tmp24 = (LINE(837,x_strtoupper(toString(x_getenv("TEST_PHP_ERROR_STYLE")))));
    int tmp25 = -1;
    if (equal(tmp24, ("MSVC"))) {
      tmp25 = 0;
    } else if (equal(tmp24, ("EMACS"))) {
      tmp25 = 1;
    }
    switch (tmp25) {
    case 0:
      {
        echo(concat(toString(v_testname), LINE(839,concat3("(1) : ", toString(v_tested), "\n"))));
        echo(concat(toString(v_logname), LINE(840,concat3("(1) :  ", toString(v_tested), "\n"))));
        goto break23;
      }
    case 1:
      {
        echo(concat(toString(v_testname), LINE(843,concat3(":1: ", toString(v_tested), "\n"))));
        echo(concat(toString(v_logname), LINE(844,concat3(":1:  ", toString(v_tested), "\n"))));
        goto break23;
      }
    }
    break23:;
  }
} /* function */
/* SRC: roadsend/tests/run-tests.php line 898 */
void f_run_all_tests(CVarRef v_test_files, CVarRef v_env, CVarRef v_redir_tested //  = null_variant
) {
  FUNCTION_INJECTION(run_all_tests);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_test_results __attribute__((__unused__)) = g->GV(test_results);
  Variant &gv_failed_tests_file __attribute__((__unused__)) = g->GV(failed_tests_file);
  Variant &gv_php __attribute__((__unused__)) = g->GV(php);
  Variant &gv_test_cnt __attribute__((__unused__)) = g->GV(test_cnt);
  Variant &gv_test_idx __attribute__((__unused__)) = g->GV(test_idx);
  Variant v_name;
  Variant v_index;
  String v_result;

  {
  }
  {
    LOOP_COUNTER(26);
    for (ArrayIterPtr iter28 = v_test_files.begin(); !iter28->end(); iter28->next()) {
      LOOP_COUNTER_CHECK(26);
      v_name = iter28->second();
      {
        if (LINE(904,x_is_array(v_name))) {
          (v_index = LINE(906,concat4("# ", toString(v_name.rvalAt(1, 0x5BCA7C69B794F8CELL)), ": ", toString(v_name.rvalAt(0, 0x77CFA1EEF01BCA90LL)))));
          if (toBoolean(v_redir_tested)) {
            (v_name = v_name.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          }
        }
        else if (toBoolean(v_redir_tested)) {
          (v_index = LINE(914,concat4("# ", toString(v_redir_tested), ": ", toString(v_name))));
        }
        else {
          (v_index = v_name);
        }
        gv_test_idx++;
        (v_result = LINE(921,f_run_test(gv_php, v_name, v_env)));
        if (!(LINE(922,x_is_array(v_name))) && !equal(v_result, "REDIR")) {
          gv_test_results.set(v_index, (v_result));
          if (toBoolean(gv_failed_tests_file) && (equal(v_result, "FAILED") || equal(v_result, "WARNED") || equal(v_result, "LEAKED"))) {
            LINE(927,x_fwrite(toObject(gv_failed_tests_file), toString(v_index) + toString("\n")));
          }
        }
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1719 */
void f_compute_summary() {
  FUNCTION_INJECTION(compute_summary);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_n_total __attribute__((__unused__)) = g->GV(n_total);
  Variant &gv_test_results __attribute__((__unused__)) = g->GV(test_results);
  Variant &gv_ignored_by_ext __attribute__((__unused__)) = g->GV(ignored_by_ext);
  Variant &gv_sum_results __attribute__((__unused__)) = g->GV(sum_results);
  Variant &gv_percent_results __attribute__((__unused__)) = g->GV(percent_results);
  Variant v_v;
  Variant v_n;

  {
  }
  (gv_n_total = LINE(1723,x_count(gv_test_results)));
  gv_n_total += gv_ignored_by_ext;
  (gv_sum_results = ScalarArrays::sa_[11]);
  {
    LOOP_COUNTER(29);
    for (ArrayIterPtr iter31 = gv_test_results.begin(); !iter31->end(); iter31->next()) {
      LOOP_COUNTER_CHECK(29);
      v_v = iter31->second();
      {
        lval(gv_sum_results.lvalAt(v_v))++;
      }
    }
  }
  lval(gv_sum_results.lvalAt("SKIPPED", 0x663C301D44C750E5LL)) += gv_ignored_by_ext;
  (gv_percent_results = ScalarArrays::sa_[0]);
  LOOP_COUNTER(32);
  {
    while (toBoolean(df_lambda_1(LINE(1731,x_each(ref(gv_sum_results))), v_v, v_n))) {
      LOOP_COUNTER_CHECK(32);
      {
        gv_percent_results.set(v_v, (divide((100.0 * toDouble(v_n)), gv_n_total)));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1623 */
Array f_generate_array_diff(CArrRef v_ar1, CArrRef v_ar2, bool v_is_reg, CArrRef v_w) {
  FUNCTION_INJECTION(generate_array_diff);
  int64 v_idx1 = 0;
  int v_cnt1 = 0;
  int64 v_idx2 = 0;
  int v_cnt2 = 0;
  Array v_diff;
  Variant v_old1;
  Variant v_old2;
  int64 v_c1 = 0;
  int64 v_c2 = 0;
  Variant v_k1;
  Variant v_l1;
  Variant v_k2;
  Variant v_l2;

  (v_idx1 = 0LL);
  ;
  (v_cnt1 = LINE(1625,x_count(v_ar1)));
  (v_idx2 = 0LL);
  ;
  (v_cnt2 = LINE(1626,x_count(v_ar2)));
  (v_diff = ScalarArrays::sa_[0]);
  (v_old1 = ScalarArrays::sa_[0]);
  (v_old2 = ScalarArrays::sa_[0]);
  LOOP_COUNTER(33);
  {
    while (less(v_idx1, v_cnt1) && less(v_idx2, v_cnt2)) {
      LOOP_COUNTER_CHECK(33);
      {
        if (toBoolean(LINE(1632,f_comp_line(v_ar1.rvalAt(v_idx1), v_ar2.rvalAt(v_idx2), v_is_reg)))) {
          v_idx1++;
          v_idx2++;
          continue;
        }
        else {
          (v_c1 = LINE(1637,f_count_array_diff(v_ar1, v_ar2, v_is_reg, v_w, v_idx1 + 1LL, v_idx2, v_cnt1, v_cnt2, 10LL)));
          (v_c2 = LINE(1638,f_count_array_diff(v_ar1, v_ar2, v_is_reg, v_w, v_idx1, v_idx2 + 1LL, v_cnt1, v_cnt2, 10LL)));
          if (more(v_c1, v_c2)) {
            v_old1.set(v_idx1, (concat_rev(toString(v_w.rvalAt(v_idx1++)), LINE(1640,x_sprintf(2, "%03d- ", Array(ArrayInit(1).set(0, v_idx1 + 1LL).create()))))));
            ;
          }
          else if (more(v_c2, 0LL)) {
            v_old2.set(v_idx2, (concat_rev(toString(v_ar2.rvalAt(v_idx2++)), LINE(1643,x_sprintf(2, "%03d+ ", Array(ArrayInit(1).set(0, v_idx2 + 1LL).create()))))));
            ;
          }
          else {
            v_old1.set(v_idx1, (concat_rev(toString(v_w.rvalAt(v_idx1++)), LINE(1646,x_sprintf(2, "%03d- ", Array(ArrayInit(1).set(0, v_idx1 + 1LL).create()))))));
            v_old2.set(v_idx2, (concat_rev(toString(v_ar2.rvalAt(v_idx2++)), LINE(1647,x_sprintf(2, "%03d+ ", Array(ArrayInit(1).set(0, v_idx2 + 1LL).create()))))));
          }
        }
      }
    }
  }
  LINE(1652,x_reset(ref(v_old1)));
  (v_k1 = x_key(ref(v_old1)));
  (v_l1 = -2LL);
  LINE(1653,x_reset(ref(v_old2)));
  (v_k2 = x_key(ref(v_old2)));
  (v_l2 = -2LL);
  LOOP_COUNTER(34);
  {
    while (!same(v_k1, null) || !same(v_k2, null)) {
      LOOP_COUNTER_CHECK(34);
      {
        if (equal(v_k1, v_l1 + 1LL) || same(v_k2, null)) {
          (v_l1 = v_k1);
          v_diff.append((LINE(1657,x_current(ref(v_old1)))));
          (v_k1 = toBoolean(LINE(1658,x_next(ref(v_old1)))) ? ((Variant)(x_key(ref(v_old1)))) : ((Variant)(null)));
        }
        else if (equal(v_k2, v_l2 + 1LL) || same(v_k1, null)) {
          (v_l2 = v_k2);
          v_diff.append((LINE(1661,x_current(ref(v_old2)))));
          (v_k2 = toBoolean(LINE(1662,x_next(ref(v_old2)))) ? ((Variant)(x_key(ref(v_old2)))) : ((Variant)(null)));
        }
        else if (less(v_k1, v_k2)) {
          (v_l1 = v_k1);
          v_diff.append((LINE(1665,x_current(ref(v_old1)))));
          (v_k1 = toBoolean(LINE(1666,x_next(ref(v_old1)))) ? ((Variant)(x_key(ref(v_old1)))) : ((Variant)(null)));
        }
        else {
          (v_l2 = v_k2);
          v_diff.append((LINE(1669,x_current(ref(v_old2)))));
          (v_k2 = toBoolean(LINE(1670,x_next(ref(v_old2)))) ? ((Variant)(x_key(ref(v_old2)))) : ((Variant)(null)));
        }
      }
    }
  }
  LOOP_COUNTER(35);
  {
    while (less(v_idx1, v_cnt1)) {
      LOOP_COUNTER_CHECK(35);
      {
        v_diff.append((concat_rev(toString(v_w.rvalAt(v_idx1++)), LINE(1674,x_sprintf(2, "%03d- ", Array(ArrayInit(1).set(0, v_idx1 + 1LL).create()))))));
      }
    }
  }
  LOOP_COUNTER(36);
  {
    while (less(v_idx2, v_cnt2)) {
      LOOP_COUNTER_CHECK(36);
      {
        v_diff.append((concat_rev(toString(v_ar2.rvalAt(v_idx2++)), LINE(1677,x_sprintf(2, "%03d+ ", Array(ArrayInit(1).set(0, v_idx2 + 1LL).create()))))));
      }
    }
  }
  return v_diff;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 605 */
Variant f_test_sort(Variant v_a, Variant v_b) {
  FUNCTION_INJECTION(test_sort);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_cwd __attribute__((__unused__)) = g->GV(cwd);
  int64 v_ta = 0;
  int64 v_tb = 0;

  (v_a = LINE(609,f_test_name(v_a)));
  (v_b = LINE(610,f_test_name(v_b)));
  (v_ta = same(LINE(612,x_strpos(toString(v_a), toString(gv_cwd) + toString("/tests"))), 0LL) ? ((1LL + (same(x_strpos(toString(v_a), toString(gv_cwd) + toString("/tests/run-test")), 0LL) ? ((1LL)) : ((0LL))))) : ((0LL)));
  (v_tb = same(LINE(613,x_strpos(toString(v_b), toString(gv_cwd) + toString("/tests"))), 0LL) ? ((1LL + (same(x_strpos(toString(v_b), toString(gv_cwd) + toString("/tests/run-test")), 0LL) ? ((1LL)) : ((0LL))))) : ((0LL)));
  if (equal(v_ta, v_tb)) {
    return LINE(615,x_strcmp(toString(v_a), toString(v_b)));
  }
  else {
    return v_tb - v_ta;
  }
  return null;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 849 */
Variant f_system_with_timeout(CVarRef v_commandline, CVarRef v_env //  = null_variant
, CVarRef v_stdin //  = null_variant
) {
  FUNCTION_INJECTION(system_with_timeout);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_leak_check __attribute__((__unused__)) = g->GV(leak_check);
  Variant v_data;
  Variant v_pipes;
  Variant v_proc;
  Variant v_r;
  Variant v_w;
  Variant v_e;
  Variant v_n;
  Variant v_line;
  Array v_stat;

  (v_data = "");
  (v_proc = LINE(859,x_proc_open(toString(v_commandline), ScalarArrays::sa_[8], ref(v_pipes), toString(null), v_env, ScalarArrays::sa_[9])));
  if (!(toBoolean(v_proc))) return false;
  if (LINE(864,x_is_string(v_stdin))) {
    LINE(865,x_fwrite(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_stdin)));
  }
  LINE(867,x_fclose(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  LOOP_COUNTER(37);
  {
    while (true) {
      LOOP_COUNTER_CHECK(37);
      {
        (v_r = v_pipes);
        setNull(v_w);
        setNull(v_e);
        (v_n = (silenceInc(), silenceDec(LINE(874,x_stream_select(ref(v_r), ref(v_w), ref(v_e), toBoolean(gv_leak_check) ? ((300LL)) : ((60LL)))))));
        if (same(v_n, 0LL)) {
          concat_assign(v_data, "\n ** ERROR: process timed out **\n");
          LINE(879,x_proc_terminate(toObject(v_proc)));
          return v_data;
        }
        else if (more(v_n, 0LL)) {
          (v_line = LINE(882,x_fread(toObject(v_pipes.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), 8192LL)));
          if (equal(LINE(883,x_strlen(toString(v_line))), 0LL)) {
            break;
          }
          concat_assign(v_data, toString(v_line));
        }
      }
    }
  }
  (v_stat = LINE(890,x_proc_get_status(toObject(v_proc))));
  if (toBoolean(v_stat.rvalAt("signaled", 0x7B1A9633E5DFA07BLL))) {
    concat_assign(v_data, concat("\nTermsig=", toString(v_stat.rvalAt("stopsig", 0x2BF3DD9D046B1BEFLL))));
  }
  LINE(894,id(x_proc_close(toObject(v_proc))));
  return v_data;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1840 */
void f_show_end(CVarRef v_end_time) {
  FUNCTION_INJECTION(show_end);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_html_output __attribute__((__unused__)) = g->GV(html_output);
  Variant &gv_html_file __attribute__((__unused__)) = g->GV(html_file);
  {
  }
  if (toBoolean(gv_html_output)) {
    LINE(1846,x_fwrite(toObject(gv_html_file), "</table>\n"));
    LINE(1847,(assignCallTemp(eo_0, toObject(gv_html_file)),assignCallTemp(eo_1, (assignCallTemp(eo_3, toString((silenceInc(), silenceDec(x_date("Y-m-d H:i:s", toInt64(v_end_time)))))),concat3("<h2>Time End: ", eo_3, "</h2>\n"))),x_fwrite(eo_0, eo_1)));
  }
  echo(LINE(1849,(assignCallTemp(eo_1, toString((silenceInc(), silenceDec(x_date("Y-m-d H:i:s", toInt64(v_end_time)))))),concat3("=====================================================================\nTIME END ", eo_1, "\n"))));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 563 */
void f_find_files(CVarRef v_dir, bool v_is_ext_dir //  = false
, bool v_ignore //  = false
) {
  FUNCTION_INJECTION(find_files);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_test_files __attribute__((__unused__)) = g->GV(test_files);
  Variant &gv_exts_to_test __attribute__((__unused__)) = g->GV(exts_to_test);
  Variant &gv_ignored_by_ext __attribute__((__unused__)) = g->GV(ignored_by_ext);
  Variant &gv_exts_skipped __attribute__((__unused__)) = g->GV(exts_skipped);
  Variant &gv_exts_tested __attribute__((__unused__)) = g->GV(exts_tested);
  Variant v_o;
  Variant v_name;
  bool v_skip_ext = false;
  Variant v_testfile;

  {
  }
  (toBoolean((v_o = LINE(567,x_opendir(toString(v_dir)))))) || ((f_error(toString("cannot open directory: ") + toString(v_dir)), null));
  LOOP_COUNTER(38);
  {
    while (!same(((v_name = LINE(568,x_readdir(toObject(v_o))))), false)) {
      LOOP_COUNTER_CHECK(38);
      {
        if (LINE(569,x_is_dir(concat3(toString(v_dir), "/", toString(v_name)))) && !(x_in_array(v_name, ScalarArrays::sa_[7]))) {
          (v_skip_ext = (v_is_ext_dir && !(LINE(570,(assignCallTemp(eo_0, x_strtolower(toString(v_name))),assignCallTemp(eo_1, gv_exts_to_test),x_in_array(eo_0, eo_1))))));
          if (v_skip_ext) {
            gv_exts_skipped++;
          }
          LINE(574,(assignCallTemp(eo_0, concat3(toString(v_dir), "/", toString(v_name))),assignCallTemp(eo_2, v_ignore || v_skip_ext),f_find_files(eo_0, false, eo_2)));
        }
        if (equal(LINE(578,x_substr(toString(v_name), toInt32(-4LL))), ".tmp")) {
          (silenceInc(), silenceDec(LINE(579,x_unlink(concat3(toString(v_dir), "/", toString(v_name))))));
          continue;
        }
        if (equal(LINE(584,x_substr(toString(v_name), toInt32(-5LL))), ".phpt")) {
          if (v_ignore) {
            gv_ignored_by_ext++;
          }
          else {
            (v_testfile = LINE(588,x_realpath(concat3(toString(v_dir), "/", toString(v_name)))));
            gv_test_files.append((v_testfile));
          }
        }
      }
    }
  }
  LINE(593,x_closedir(toObject(v_o)));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1588 */
int64 f_count_array_diff(CVarRef v_ar1, CVarRef v_ar2, CVarRef v_is_reg, CVarRef v_w, Numeric v_idx1, Numeric v_idx2, int v_cnt1, int v_cnt2, Numeric v_steps) {
  FUNCTION_INJECTION(count_array_diff);
  int64 v_equal = 0;
  int64 v_eq1 = 0;
  Numeric v_st = 0;
  Numeric v_ofs1 = 0;
  int64 v_eq = 0;
  int64 v_eq2 = 0;
  Numeric v_ofs2 = 0;

  (v_equal = 0LL);
  LOOP_COUNTER(39);
  {
    while (less(v_idx1, v_cnt1) && less(v_idx2, v_cnt2) && toBoolean(LINE(1591,f_comp_line(v_ar1.rvalAt(v_idx1), v_ar2.rvalAt(v_idx2), v_is_reg)))) {
      LOOP_COUNTER_CHECK(39);
      {
        v_idx1++;
        v_idx2++;
        v_equal++;
        v_steps--;
      }
    }
  }
  if (more(--v_steps, 0LL)) {
    (v_eq1 = 0LL);
    (v_st = divide(v_steps, 2LL));
    {
      LOOP_COUNTER(40);
      for ((v_ofs1 = v_idx1 + 1LL); less(v_ofs1, v_cnt1) && more(v_st--, 0LL); v_ofs1++) {
        LOOP_COUNTER_CHECK(40);
        {
          (v_eq = LINE(1601,f_count_array_diff(v_ar1, v_ar2, v_is_reg, v_w, v_ofs1, v_idx2, v_cnt1, v_cnt2, v_st)));
          if (more(v_eq, v_eq1)) {
            (v_eq1 = v_eq);
          }
        }
      }
    }
    (v_eq2 = 0LL);
    (v_st = v_steps);
    {
      LOOP_COUNTER(41);
      for ((v_ofs2 = v_idx2 + 1LL); less(v_ofs2, v_cnt2) && more(v_st--, 0LL); v_ofs2++) {
        LOOP_COUNTER_CHECK(41);
        {
          (v_eq = LINE(1609,f_count_array_diff(v_ar1, v_ar2, v_is_reg, v_w, v_idx1, v_ofs2, v_cnt1, v_cnt2, v_st)));
          if (more(v_eq, v_eq2)) {
            (v_eq2 = v_eq);
          }
        }
      }
    }
    if (more(v_eq1, v_eq2)) {
      v_equal += v_eq1;
    }
    else if (more(v_eq2, 0LL)) {
      v_equal += v_eq2;
    }
  }
  return v_equal;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 775 */
int64 f_mail_qa_team(CVarRef v_data, CVarRef v_compression, bool v_status //  = false
) {
  FUNCTION_INJECTION(mail_qa_team);
  return 1LL;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 808 */
void f_save_text(CVarRef v_filename, CVarRef v_text, CVarRef v_filename_copy //  = null_variant
) {
  FUNCTION_INJECTION(save_text);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_DETAILED __attribute__((__unused__)) = g->GV(DETAILED);
  if (toBoolean(v_filename_copy) && !equal(v_filename_copy, v_filename)) {
    if (same((silenceInc(), silenceDec(LINE(813,x_file_put_contents(toString(v_filename_copy), v_text)))), false)) {
      LINE(814,f_error(concat3("Cannot open file '", toString(v_filename_copy), "' (save_text)")));
    }
  }
  if (same((silenceInc(), silenceDec(LINE(817,x_file_put_contents(toString(v_filename), v_text)))), false)) {
    LINE(818,f_error(concat3("Cannot open file '", toString(v_filename), "' (save_text)")));
  }
  if (less(1LL, gv_DETAILED)) echo(LINE(826,concat5("\nFILE ", toString(v_filename), " {{{\n", toString(v_text), "\n}}} \n")));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1736 */
Variant f_get_summary(bool v_show_ext_summary, bool v_show_html) {
  FUNCTION_INJECTION(get_summary);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_exts_skipped __attribute__((__unused__)) = g->GV(exts_skipped);
  Variant &gv_exts_tested __attribute__((__unused__)) = g->GV(exts_tested);
  Variant &gv_n_total __attribute__((__unused__)) = g->GV(n_total);
  Variant &gv_sum_results __attribute__((__unused__)) = g->GV(sum_results);
  Variant &gv_percent_results __attribute__((__unused__)) = g->GV(percent_results);
  Variant &gv_end_time __attribute__((__unused__)) = g->GV(end_time);
  Variant &gv_start_time __attribute__((__unused__)) = g->GV(start_time);
  Variant &gv_failed_test_summary __attribute__((__unused__)) = g->GV(failed_test_summary);
  Variant &gv_PHP_FAILED_TESTS __attribute__((__unused__)) = g->GV(PHP_FAILED_TESTS);
  Variant &gv_leak_check __attribute__((__unused__)) = g->GV(leak_check);
  Numeric v_x_total = 0;
  Numeric v_x_warned = 0;
  Numeric v_x_failed = 0;
  Numeric v_x_leaked = 0;
  Numeric v_x_passed = 0;
  Variant v_summary;
  Variant v_failed_test_data;

  {
  }
  (v_x_total = gv_n_total - gv_sum_results.rvalAt("SKIPPED", 0x663C301D44C750E5LL) - gv_sum_results.rvalAt("BORKED", 0x6D22374225C91058LL));
  if (toBoolean(v_x_total)) {
    (v_x_warned = divide((100.0 * toDouble(gv_sum_results.rvalAt("WARNED", 0x277E5E0800E36957LL))), v_x_total));
    (v_x_failed = divide((100.0 * toDouble(gv_sum_results.rvalAt("FAILED", 0x52B3F0742123E85BLL))), v_x_total));
    (v_x_leaked = divide((100.0 * toDouble(gv_sum_results.rvalAt("LEAKED", 0x4551966E3B21D70ALL))), v_x_total));
    (v_x_passed = divide((100.0 * toDouble(gv_sum_results.rvalAt("PASSED", 0x2F0347678F7BF36ELL))), v_x_total));
  }
  else {
    (v_x_warned = (v_x_failed = (v_x_passed = (v_x_leaked = 0LL))));
  }
  (v_summary = "");
  if (v_show_html) concat_assign(v_summary, "<pre>\n");
  if (v_show_ext_summary) {
    concat_assign(v_summary, concat(concat_rev(LINE(1758,x_sprintf(2, "%4d", Array(ArrayInit(1).set(0, gv_exts_tested).create()))), (assignCallTemp(eo_1, LINE(1757,x_sprintf(2, "%4d", Array(ArrayInit(1).set(0, gv_exts_skipped).create())))),concat3("\n=====================================================================\nTEST RESULT SUMMARY\n---------------------------------------------------------------------\nExts skipped    : ", eo_1, "\nExts tested     : "))), "\n---------------------------------------------------------------------\n"));
  }
  concat_assign(v_summary, concat_rev(LINE(1763,x_sprintf(2, "%8d", Array(ArrayInit(1).set(0, v_x_total).create()))), (assignCallTemp(eo_1, x_sprintf(2, "%4d", Array(ArrayInit(1).set(0, gv_n_total).create()))),concat3("\nNumber of tests : ", eo_1, "          "))));
  if (toBoolean(gv_sum_results.rvalAt("BORKED", 0x6D22374225C91058LL))) {
    concat_assign(v_summary, LINE(1766,(assignCallTemp(eo_1, x_sprintf(3, "%4d (%5.1f%%)", Array(ArrayInit(2).set(0, gv_sum_results.rvalAt("BORKED", 0x6D22374225C91058LL)).set(1, gv_percent_results.rvalAt("BORKED", 0x6D22374225C91058LL)).create()))),concat3("\nTests borked    : ", eo_1, " --------"))));
  }
  concat_assign(v_summary, concat_rev(LINE(1771,x_sprintf(2, "(%5.1f%%)", Array(ArrayInit(1).set(0, v_x_failed).create()))), concat(concat_rev(x_sprintf(3, "%4d (%5.1f%%)", Array(ArrayInit(2).set(0, gv_sum_results.rvalAt("FAILED", 0x52B3F0742123E85BLL)).set(1, gv_percent_results.rvalAt("FAILED", 0x52B3F0742123E85BLL)).create())), concat(concat_rev(LINE(1770,x_sprintf(2, "(%5.1f%%)", Array(ArrayInit(1).set(0, v_x_warned).create()))), concat(concat_rev(x_sprintf(3, "%4d (%5.1f%%)", Array(ArrayInit(2).set(0, gv_sum_results.rvalAt("WARNED", 0x277E5E0800E36957LL)).set(1, gv_percent_results.rvalAt("WARNED", 0x277E5E0800E36957LL)).create())), (assignCallTemp(eo_1, LINE(1769,x_sprintf(3, "%4d (%5.1f%%)", Array(ArrayInit(2).set(0, gv_sum_results.rvalAt("SKIPPED", 0x663C301D44C750E5LL)).set(1, gv_percent_results.rvalAt("SKIPPED", 0x663C301D44C750E5LL)).create())))),concat3("\nTests skipped   : ", eo_1, " --------\nTests warned    : "))), " ")), "\nTests failed    : ")), " ")));
  if (toBoolean(gv_leak_check)) {
    concat_assign(v_summary, concat_rev(LINE(1774,x_sprintf(2, "(%5.1f%%)", Array(ArrayInit(1).set(0, v_x_leaked).create()))), (assignCallTemp(eo_1, x_sprintf(3, "%4d (%5.1f%%)", Array(ArrayInit(2).set(0, gv_sum_results.rvalAt("LEAKED", 0x4551966E3B21D70ALL)).set(1, gv_percent_results.rvalAt("LEAKED", 0x4551966E3B21D70ALL)).create()))),concat3("\nTests leaked    : ", eo_1, " "))));
  }
  concat_assign(v_summary, concat(concat_rev(LINE(1779,x_sprintf(2, "%4d seconds", Array(ArrayInit(1).set(0, gv_end_time - gv_start_time).create()))), concat(concat_rev(LINE(1777,x_sprintf(2, "(%5.1f%%)", Array(ArrayInit(1).set(0, v_x_passed).create()))), (assignCallTemp(eo_1, x_sprintf(3, "%4d (%5.1f%%)", Array(ArrayInit(2).set(0, gv_sum_results.rvalAt("PASSED", 0x2F0347678F7BF36ELL)).set(1, gv_percent_results.rvalAt("PASSED", 0x2F0347678F7BF36ELL)).create()))),concat3("\nTests passed    : ", eo_1, " "))), "\n---------------------------------------------------------------------\nTime taken      : ")), "\n=====================================================================\n"));
  (gv_failed_test_summary = "");
  if (toBoolean(LINE(1783,x_count(gv_PHP_FAILED_TESTS.rvalAt("BORKED", 0x6D22374225C91058LL))))) {
    concat_assign(gv_failed_test_summary, "\n=====================================================================\nBORKED TEST SUMMARY\n---------------------------------------------------------------------\n");
    {
      LOOP_COUNTER(42);
      Variant map43 = gv_PHP_FAILED_TESTS.rvalAt("BORKED", 0x6D22374225C91058LL);
      for (ArrayIterPtr iter44 = map43.begin(); !iter44->end(); iter44->next()) {
        LOOP_COUNTER_CHECK(42);
        v_failed_test_data = iter44->second();
        {
          concat_assign(gv_failed_test_summary, concat(toString(v_failed_test_data.rvalAt("info", 0x59E9384E33988B3ELL)), "\n"));
        }
      }
    }
    concat_assign(gv_failed_test_summary, "=====================================================================\n");
  }
  if (toBoolean(LINE(1795,x_count(gv_PHP_FAILED_TESTS.rvalAt("FAILED", 0x52B3F0742123E85BLL))))) {
    concat_assign(gv_failed_test_summary, "\n=====================================================================\nFAILED TEST SUMMARY\n---------------------------------------------------------------------\n");
    {
      LOOP_COUNTER(45);
      Variant map46 = gv_PHP_FAILED_TESTS.rvalAt("FAILED", 0x52B3F0742123E85BLL);
      for (ArrayIterPtr iter47 = map46.begin(); !iter47->end(); iter47->next()) {
        LOOP_COUNTER_CHECK(45);
        v_failed_test_data = iter47->second();
        {
          concat_assign(gv_failed_test_summary, LINE(1802,concat3(toString(v_failed_test_data.rvalAt("test_name", 0x6D72CEB44AC01301LL)), toString(v_failed_test_data.rvalAt("info", 0x59E9384E33988B3ELL)), "\n")));
        }
      }
    }
    concat_assign(gv_failed_test_summary, "=====================================================================\n");
  }
  if (toBoolean(LINE(1807,x_count(gv_PHP_FAILED_TESTS.rvalAt("LEAKED", 0x4551966E3B21D70ALL))))) {
    concat_assign(gv_failed_test_summary, "\n=====================================================================\nLEAKED TEST SUMMARY\n---------------------------------------------------------------------\n");
    {
      LOOP_COUNTER(48);
      Variant map49 = gv_PHP_FAILED_TESTS.rvalAt("LEAKED", 0x4551966E3B21D70ALL);
      for (ArrayIterPtr iter50 = map49.begin(); !iter50->end(); iter50->next()) {
        LOOP_COUNTER_CHECK(48);
        v_failed_test_data = iter50->second();
        {
          concat_assign(gv_failed_test_summary, LINE(1814,concat3(toString(v_failed_test_data.rvalAt("test_name", 0x6D72CEB44AC01301LL)), toString(v_failed_test_data.rvalAt("info", 0x59E9384E33988B3ELL)), "\n")));
        }
      }
    }
    concat_assign(gv_failed_test_summary, "=====================================================================\n");
  }
  if (toBoolean(gv_failed_test_summary) && !(toBoolean(LINE(1819,x_getenv("NO_PHPTEST_SUMMARY"))))) {
    concat_assign(v_summary, toString(gv_failed_test_summary));
  }
  if (v_show_html) concat_assign(v_summary, "</pre>");
  return v_summary;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1682 */
String f_generate_diff(CVarRef v_wanted, CVarRef v_wanted_re, CVarRef v_output) {
  FUNCTION_INJECTION(generate_diff);
  Array v_w;
  Array v_o;
  Array v_r;
  Array v_diff;

  (v_w = LINE(1684,x_explode("\n", toString(v_wanted))));
  (v_o = LINE(1685,x_explode("\n", toString(v_output))));
  (v_r = LINE(1686,x_is_null(v_wanted_re)) ? ((v_w)) : ((x_explode("\n", toString(v_wanted_re)))));
  (v_diff = LINE(1687,f_generate_array_diff(v_r, v_o, !(x_is_null(v_wanted_re)), v_w)));
  return LINE(1688,x_implode("\r\n", v_diff));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1863 */
void f_show_redirect_start(CVarRef v_tests, CVarRef v_tested, CVarRef v_tested_file) {
  FUNCTION_INJECTION(show_redirect_start);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_html_output __attribute__((__unused__)) = g->GV(html_output);
  Variant &gv_html_file __attribute__((__unused__)) = g->GV(html_file);
  {
  }
  if (toBoolean(gv_html_output)) {
    LINE(1869,(assignCallTemp(eo_0, toObject(gv_html_file)),assignCallTemp(eo_1, concat("<tr><td colspan='3'>---&gt; ", concat6(toString(v_tests), " (", toString(v_tested), " [", toString(v_tested_file), "]) begin</td></tr>\n"))),x_fwrite(eo_0, eo_1)));
  }
  echo(concat("---> ", LINE(1871,concat6(toString(v_tests), " (", toString(v_tested), " [", toString(v_tested_file), "]) begin\n"))));
} /* function */
/* SRC: roadsend/tests/run-tests.php line 182 */
void f_write_information(CVarRef v_show_html) {
  FUNCTION_INJECTION(write_information);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_cwd __attribute__((__unused__)) = g->GV(cwd);
  Variant &gv_php __attribute__((__unused__)) = g->GV(php);
  Variant &gv_php_info __attribute__((__unused__)) = g->GV(php_info);
  Variant &gv_user_tests __attribute__((__unused__)) = g->GV(user_tests);
  Variant &gv_ini_overwrites __attribute__((__unused__)) = g->GV(ini_overwrites);
  Variant &gv_pass_options __attribute__((__unused__)) = g->GV(pass_options);
  Variant &gv_exts_to_test __attribute__((__unused__)) = g->GV(exts_to_test);
  String v_info_file;
  Variant v_info_params;
  Array v_info_params_ex;
  Primitive v_ext = 0;
  Variant v_ini_overwrites_ex;
  Variant v_test_dir;

  {
  }
  (v_info_file = concat(toString(LINE(187,x_realpath(x_dirname(get_source_filename("roadsend/tests/run-tests.php"))))), "/run-test-info.php"));
  (silenceInc(), silenceDec(LINE(188,x_unlink(v_info_file))));
  (gv_php_info = "<\?php echo \"\nPHP_SAPI    : \" . PHP_SAPI . \"\nPCC_VERSION : \" . PCC_VERSION . \"\nPHP_VERSION : \" . phpversion() . \"\nZEND_VERSION: \" . zend_version() . \"\nPHP_OS      : \" . PHP_OS . \" - \" . php_uname();\n\?>");
  LINE(196,f_save_text(v_info_file, gv_php_info));
  (v_info_params = ScalarArrays::sa_[0]);
  LINE(198,f_settings2array(gv_ini_overwrites, ref(v_info_params)));
  LINE(199,f_settings2params(ref(v_info_params)));
  (gv_php_info = f_shell_exec(toString(gv_php) + toString(" ") + toString(gv_pass_options) + toString(" ") + toString(v_info_params) + toString(" -f \"") + v_info_file + toString("\"")));
  (silenceInc(), silenceDec(LINE(201,x_unlink(v_info_file))));
  LINE(202,g->declareConstant("TESTED_PHP_VERSION", g->k_TESTED_PHP_VERSION, f_shell_exec(toString(gv_php) + toString(" -r \"echo PHP_VERSION;\""))));
  LINE(205,f_save_text(v_info_file, "<\?php echo join(\",\",get_loaded_extensions()); \?>"));
  (gv_exts_to_test = LINE(206,(assignCallTemp(eo_1, f_shell_exec(toString(gv_php) + toString(" ") + toString(gv_pass_options) + toString(" ") + toString(v_info_params) + toString(" \"") + v_info_file + toString("\""))),x_explode(",", eo_1))));
  (v_info_params_ex = ScalarArrays::sa_[2]);
  {
    LOOP_COUNTER(51);
    for (ArrayIter iter53 = v_info_params_ex.begin(); !iter53.end(); ++iter53) {
      LOOP_COUNTER_CHECK(51);
      v_ini_overwrites_ex = iter53.second();
      v_ext = iter53.first();
      {
        if (LINE(215,x_in_array(v_ext, gv_exts_to_test))) {
          (gv_ini_overwrites = LINE(216,x_array_merge(2, gv_ini_overwrites, Array(ArrayInit(1).set(0, v_ini_overwrites_ex).create()))));
        }
      }
    }
  }
  (silenceInc(), silenceDec(LINE(219,x_unlink(v_info_file))));
  echo(concat("\n=====================================================================\nCWD         : ", LINE(227,concat6(toString(gv_cwd), "\nPHP         : ", toString(gv_php), " ", toString(gv_php_info), "\nExtra dirs  : "))));
  {
    LOOP_COUNTER(54);
    for (ArrayIterPtr iter56 = gv_user_tests.begin(); !iter56->end(); iter56->next()) {
      LOOP_COUNTER_CHECK(54);
      v_test_dir = iter56->second();
      {
        echo(toString(v_test_dir) + toString("\n              "));
      }
    }
  }
  echo("\n=====================================================================\n");
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1885 */
void f_show_test(CVarRef v_test_idx, CVarRef v_shortname) {
  FUNCTION_INJECTION(show_test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_test_cnt __attribute__((__unused__)) = g->GV(test_cnt);
  echo(concat("TEST ", LINE(1889,concat6(toString(v_test_idx), "/", toString(gv_test_cnt), " [", toString(v_shortname), "]\r"))));
  LINE(1890,x_flush());
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1579 */
Variant f_comp_line(CVarRef v_l1, CVarRef v_l2, CVarRef v_is_reg) {
  FUNCTION_INJECTION(comp_line);
  Variant eo_0;
  Variant eo_1;
  if (toBoolean(v_is_reg)) {
    return LINE(1582,(assignCallTemp(eo_0, concat3("/^", toString(v_l1), "$/s")),assignCallTemp(eo_1, toString(v_l2)),x_preg_match(eo_0, eo_1)));
  }
  else {
    return !(toBoolean(LINE(1584,x_strcmp(toString(v_l1), toString(v_l2)))));
  }
  return null;
} /* function */
/* SRC: roadsend/tests/run-tests.php line 1697 */
void f_settings2array(CVarRef v_settings, Variant v_ini_settings) {
  FUNCTION_INJECTION(settings2array);
  Variant v_setting;
  String v_name;
  String v_value;

  {
    LOOP_COUNTER(57);
    for (ArrayIterPtr iter59 = v_settings.begin(); !iter59->end(); iter59->next()) {
      LOOP_COUNTER_CHECK(57);
      v_setting = iter59->second();
      {
        if (!same(LINE(1700,x_strpos(toString(v_setting), "=")), false)) {
          (v_setting = LINE(1701,x_explode("=", toString(v_setting), toInt32(2LL))));
          (v_name = LINE(1702,x_trim(x_strtolower(toString(v_setting.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))))));
          (v_value = LINE(1703,x_trim(toString(v_setting.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
          v_ini_settings.set(v_name, (v_value));
        }
      }
    }
  }
} /* function */
Variant i_test_name(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x139687FBB42DFFCALL, test_name) {
    return (f_test_name(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_test_sort(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x11FACE507E317D45LL, test_sort) {
    return (f_test_sort(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$run_tests_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/run-tests.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$run_tests_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_CUR_DIR __attribute__((__unused__)) = (variables != gVariables) ? variables->get("CUR_DIR") : g->GV(CUR_DIR);
  Variant &v_cwd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cwd") : g->GV(cwd);
  Variant &v_environment __attribute__((__unused__)) = (variables != gVariables) ? variables->get("environment") : g->GV(environment);
  Variant &v_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php") : g->GV(php);
  Variant &v_php_cgi __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_cgi") : g->GV(php_cgi);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_log_format __attribute__((__unused__)) = (variables != gVariables) ? variables->get("log_format") : g->GV(log_format);
  Variant &v_DETAILED __attribute__((__unused__)) = (variables != gVariables) ? variables->get("DETAILED") : g->GV(DETAILED);
  Variant &v_user_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("user_tests") : g->GV(user_tests);
  Variant &v_exts_to_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exts_to_test") : g->GV(exts_to_test);
  Variant &v_ini_overwrites __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ini_overwrites") : g->GV(ini_overwrites);
  Variant &v_test_files __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_files") : g->GV(test_files);
  Variant &v_redir_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("redir_tests") : g->GV(redir_tests);
  Variant &v_test_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_results") : g->GV(test_results);
  Variant &v_PHP_FAILED_TESTS __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PHP_FAILED_TESTS") : g->GV(PHP_FAILED_TESTS);
  Variant &v_failed_tests_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("failed_tests_file") : g->GV(failed_tests_file);
  Variant &v_pass_option_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pass_option_n") : g->GV(pass_option_n);
  Variant &v_pass_options __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pass_options") : g->GV(pass_options);
  Variant &v_compression __attribute__((__unused__)) = (variables != gVariables) ? variables->get("compression") : g->GV(compression);
  Variant &v_output_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("output_file") : g->GV(output_file);
  Variant &v_just_save_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("just_save_results") : g->GV(just_save_results);
  Variant &v_leak_check __attribute__((__unused__)) = (variables != gVariables) ? variables->get("leak_check") : g->GV(leak_check);
  Variant &v_html_output __attribute__((__unused__)) = (variables != gVariables) ? variables->get("html_output") : g->GV(html_output);
  Variant &v_html_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("html_file") : g->GV(html_file);
  Variant &v_temp_source __attribute__((__unused__)) = (variables != gVariables) ? variables->get("temp_source") : g->GV(temp_source);
  Variant &v_temp_target __attribute__((__unused__)) = (variables != gVariables) ? variables->get("temp_target") : g->GV(temp_target);
  Variant &v_temp_urlbase __attribute__((__unused__)) = (variables != gVariables) ? variables->get("temp_urlbase") : g->GV(temp_urlbase);
  Variant &v_conf_passed __attribute__((__unused__)) = (variables != gVariables) ? variables->get("conf_passed") : g->GV(conf_passed);
  Variant &v_no_clean __attribute__((__unused__)) = (variables != gVariables) ? variables->get("no_clean") : g->GV(no_clean);
  Variant &v_cfgtypes __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cfgtypes") : g->GV(cfgtypes);
  Variant &v_cfgfiles __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cfgfiles") : g->GV(cfgfiles);
  Variant &v_cfg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cfg") : g->GV(cfg);
  Variant &v_type __attribute__((__unused__)) = (variables != gVariables) ? variables->get("type") : g->GV(type);
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_is_switch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("is_switch") : g->GV(is_switch);
  Variant &v_switch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("switch") : g->GV(switch);
  Variant &v_repeat __attribute__((__unused__)) = (variables != gVariables) ? variables->get("repeat") : g->GV(repeat);
  Variant &v_test_list __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_list") : g->GV(test_list);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_matches __attribute__((__unused__)) = (variables != gVariables) ? variables->get("matches") : g->GV(matches);
  Variant &v_testfile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testfile") : g->GV(testfile);
  Variant &v_test_cnt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_cnt") : g->GV(test_cnt);
  Variant &v_start_time __attribute__((__unused__)) = (variables != gVariables) ? variables->get("start_time") : g->GV(start_time);
  Variant &v_test_idx __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_idx") : g->GV(test_idx);
  Variant &v_end_time __attribute__((__unused__)) = (variables != gVariables) ? variables->get("end_time") : g->GV(end_time);
  Variant &v_exts_tested __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exts_tested") : g->GV(exts_tested);
  Variant &v_exts_skipped __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exts_skipped") : g->GV(exts_skipped);
  Variant &v_ignored_by_ext __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ignored_by_ext") : g->GV(ignored_by_ext);
  Variant &v_test_dirs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_dirs") : g->GV(test_dirs);
  Variant &v_optionals __attribute__((__unused__)) = (variables != gVariables) ? variables->get("optionals") : g->GV(optionals);
  Variant &v_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dir") : g->GV(dir);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_sum_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sum_results") : g->GV(sum_results);

  if (!(LINE(37,x_extension_loaded("pcre")))) {
    echo(toString("\n+-----------------------------------------------------------+\n|                       ! ERROR !                           |\n| The test-suite requires that you have pcre extension      |\n| enabled. To enable this extension either compile your PHP |\n| with --with-pcre-regex or if you've compiled pcre as a    |\n| shared module load it via php.ini.                        |\n+-----------------------------------------------------------+\n"));
    f_exit();
  }
  (v_CUR_DIR = LINE(66,x_getcwd()));
  if (toBoolean(LINE(70,x_getenv("TEST_PHP_SRCDIR")))) {
    (silenceInc(), silenceDec(LINE(71,x_chdir(toString(x_getenv("TEST_PHP_SRCDIR"))))));
  }
  LINE(75,x_putenv("SSH_CLIENT=deleted"));
  LINE(76,x_putenv("SSH_AUTH_SOCK=deleted"));
  LINE(77,x_putenv("SSH_TTY=deleted"));
  LINE(78,x_putenv("SSH_CONNECTION=deleted"));
  (v_cwd = LINE(80,x_getcwd()));
  LINE(81,x_set_time_limit(toInt32(0LL)));
  LOOP_COUNTER(60);
  {
    while (toBoolean((silenceInc(), silenceDec(LINE(84,x_ob_end_clean()))))) {
      LOOP_COUNTER_CHECK(60);
    }
  }
  if (toBoolean(LINE(85,x_ob_get_level()))) echo("Not all buffers were deleted.\n");
  LINE(87,x_error_reporting(toInt32(6143LL) /* E_ALL */));
  LINE(88,x_ini_set("magic_quotes_runtime", toString(0LL)));
  if (toBoolean(LINE(90,x_ini_get("safe_mode")))) {
    echo(toString("\n+-----------------------------------------------------------+\n|                       ! WARNING !                         |\n| You are running the test-suite with \"safe_mode\" ENABLED ! |\n|                                                           |\n| Chances are high that no test will work at all,           |\n| depending on how you configured \"safe_mode\" !             |\n+-----------------------------------------------------------+\n\n"));
  }
  (v_environment = isset(g->gv__ENV) ? ((Variant)(g->gv__ENV)) : ((Variant)(ScalarArrays::sa_[0])));
  if (toBoolean(LINE(111,x_getenv("TEST_PHP_EXECUTABLE")))) {
    (v_php = LINE(112,x_getenv("TEST_PHP_EXECUTABLE")));
    if (equal(v_php, "auto")) {
      (v_php = concat(toString(v_cwd), "/sapi/cli/php"));
      LINE(115,x_putenv(toString("TEST_PHP_EXECUTABLE=") + toString(v_php)));
    }
    v_environment.set("TEST_PHP_EXECUTABLE", (v_php), 0x0D6C84BF04610564LL);
  }
  if (toBoolean(LINE(120,x_getenv("TEST_PHP_CGI_EXECUTABLE")))) {
    (v_php_cgi = LINE(121,x_getenv("TEST_PHP_CGI_EXECUTABLE")));
    if (equal(v_php_cgi, "auto")) {
      (v_php_cgi = concat(toString(v_cwd), "/sapi/cgi/php"));
      LINE(124,x_putenv(toString("TEST_PHP_CGI_EXECUTABLE=") + toString(v_php_cgi)));
    }
    v_environment.set("TEST_PHP_CGI_EXECUTABLE", (v_php_cgi), 0x302C6A3F87E667A3LL);
  }
  if (!equal(v_argc, 2LL) || (!equal(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "-h") && !equal(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "-help") && !equal(v_argv, "--help"))) {
    if (empty(v_php) || !(LINE(131,x_file_exists(toString(v_php))))) {
      LINE(132,f_error("environment variable TEST_PHP_EXECUTABLE must be set to specify PHP executable!"));
    }
    if (!(toBoolean((silenceInc(), silenceDec(LINE(134,x_is_executable(toString(v_php)))))))) {
      LINE(135,f_error(concat("invalid PHP executable specified by TEST_PHP_EXECUTABLE  = ", toString(v_php))));
    }
  }
  if (toBoolean(LINE(139,x_getenv("TEST_PHP_LOG_FORMAT")))) {
    (v_log_format = LINE(140,x_strtoupper(toString(x_getenv("TEST_PHP_LOG_FORMAT")))));
  }
  else {
    (v_log_format = "LEOD");
  }
  if (toBoolean(LINE(146,x_getenv("TEST_PHP_DETAILED")))) {
    (v_DETAILED = LINE(147,x_getenv("TEST_PHP_DETAILED")));
  }
  else {
    (v_DETAILED = 0LL);
  }
  if (toBoolean(LINE(153,x_getenv("TEST_PHP_USER")))) {
    (v_user_tests = LINE(154,(assignCallTemp(eo_1, toString(x_getenv("TEST_PHP_USER"))),x_explode(",", eo_1))));
  }
  else {
    (v_user_tests = ScalarArrays::sa_[0]);
  }
  (v_exts_to_test = ScalarArrays::sa_[0]);
  (v_ini_overwrites = ScalarArrays::sa_[1]);
  (v_test_files = ScalarArrays::sa_[0]);
  (v_redir_tests = ScalarArrays::sa_[0]);
  (v_test_results = ScalarArrays::sa_[0]);
  (v_PHP_FAILED_TESTS = ScalarArrays::sa_[3]);
  (v_failed_tests_file = false);
  (v_pass_option_n = false);
  (v_pass_options = "");
  (v_compression = 0LL);
  (v_output_file = LINE(249,(assignCallTemp(eo_0, toString(v_CUR_DIR)),assignCallTemp(eo_2, toString((silenceInc(), silenceDec(x_date("Ymd_Hi"))))),concat4(eo_0, "/php_test_results_", eo_2, ".txt"))));
  if (toBoolean(v_compression)) {
    (v_output_file = LINE(251,concat3("compress.zlib://", toString(v_output_file), ".gz")));
  }
  (v_just_save_results = false);
  (v_leak_check = false);
  (v_html_output = false);
  setNull(v_html_file);
  setNull(v_temp_source);
  setNull(v_temp_target);
  setNull(v_temp_urlbase);
  setNull(v_conf_passed);
  (v_no_clean = false);
  (v_cfgtypes = ScalarArrays::sa_[4]);
  (v_cfgfiles = ScalarArrays::sa_[5]);
  (v_cfg = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(61);
    for (ArrayIterPtr iter63 = v_cfgtypes.begin(); !iter63->end(); iter63->next()) {
      LOOP_COUNTER_CHECK(61);
      v_type = iter63->second();
      {
        v_cfg.set(v_type, (ScalarArrays::sa_[0]));
        {
          LOOP_COUNTER(64);
          for (ArrayIterPtr iter66 = v_cfgfiles.begin(); !iter66->end(); iter66->next()) {
            LOOP_COUNTER_CHECK(64);
            v_file = iter66->second();
            {
              lval(v_cfg.lvalAt(v_type)).set(v_file, (false));
            }
          }
        }
      }
    }
  }
  if (toBoolean(LINE(273,x_getenv("TEST_PHP_ARGS")))) {
    if (!(isset(v_argc)) || !(toBoolean(v_argc)) || !(isset(v_argv))) {
      (v_argv = Array(ArrayInit(1).set(0, get_source_filename("roadsend/tests/run-tests.php")).create()));
    }
    (v_argv = LINE(279,(assignCallTemp(eo_0, v_argv),assignCallTemp(eo_1, (assignCallTemp(eo_3, toString(x_getenv("TEST_PHP_ARGS"))),x_split(" ", eo_3))),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create())))));
    (v_argc = LINE(280,x_count(v_argv)));
  }
  if (isset(v_argc) && more(v_argc, 1LL)) {
    {
      LOOP_COUNTER(67);
      for ((v_i = 1LL); less(v_i, v_argc); v_i++) {
        LOOP_COUNTER_CHECK(67);
        {
          (v_is_switch = false);
          (v_switch = LINE(286,x_substr(toString(v_argv.rvalAt(v_i)), toInt32(1LL), toInt32(1LL))));
          (v_repeat = equal(LINE(287,x_substr(toString(v_argv.rvalAt(v_i)), toInt32(0LL), toInt32(1LL))), "-"));
          LOOP_COUNTER(68);
          {
            while (toBoolean(v_repeat)) {
              LOOP_COUNTER_CHECK(68);
              {
                (v_repeat = false);
                if (!(toBoolean(v_is_switch))) {
                  (v_switch = LINE(291,x_substr(toString(v_argv.rvalAt(v_i)), toInt32(1LL), toInt32(1LL))));
                }
                (v_is_switch = true);
                {
                  Variant tmp70 = (v_switch);
                  int tmp71 = -1;
                  if (equal(tmp70, ("r"))) {
                    tmp71 = 0;
                  } else if (equal(tmp70, ("l"))) {
                    tmp71 = 1;
                  } else if (equal(tmp70, ("w"))) {
                    tmp71 = 2;
                  } else if (equal(tmp70, ("a"))) {
                    tmp71 = 3;
                  } else if (equal(tmp70, ("c"))) {
                    tmp71 = 4;
                  } else if (equal(tmp70, ("d"))) {
                    tmp71 = 5;
                  } else if (equal(tmp70, ("--keep-all"))) {
                    tmp71 = 6;
                  } else if (equal(tmp70, ("--keep-skip"))) {
                    tmp71 = 7;
                  } else if (equal(tmp70, ("--keep-php"))) {
                    tmp71 = 8;
                  } else if (equal(tmp70, ("--keep-clean"))) {
                    tmp71 = 9;
                  } else if (equal(tmp70, ("m"))) {
                    tmp71 = 10;
                  } else if (equal(tmp70, ("n"))) {
                    tmp71 = 11;
                  } else if (equal(tmp70, ("N"))) {
                    tmp71 = 12;
                  } else if (equal(tmp70, ("--no-clean"))) {
                    tmp71 = 13;
                  } else if (equal(tmp70, ("q"))) {
                    tmp71 = 14;
                  } else if (equal(tmp70, ("s"))) {
                    tmp71 = 15;
                  } else if (equal(tmp70, ("--show-all"))) {
                    tmp71 = 16;
                  } else if (equal(tmp70, ("--show-skip"))) {
                    tmp71 = 17;
                  } else if (equal(tmp70, ("--show-php"))) {
                    tmp71 = 18;
                  } else if (equal(tmp70, ("--show-clean"))) {
                    tmp71 = 19;
                  } else if (equal(tmp70, ("--temp-source"))) {
                    tmp71 = 20;
                  } else if (equal(tmp70, ("--temp-target"))) {
                    tmp71 = 21;
                  } else if (equal(tmp70, ("--temp-urlbase"))) {
                    tmp71 = 22;
                  } else if (equal(tmp70, ("v"))) {
                    tmp71 = 23;
                  } else if (equal(tmp70, ("--verbose"))) {
                    tmp71 = 24;
                  } else if (equal(tmp70, ("-"))) {
                    tmp71 = 25;
                  } else if (equal(tmp70, ("--html"))) {
                    tmp71 = 26;
                  } else if (equal(tmp70, ("--version"))) {
                    tmp71 = 27;
                  } else if (equal(tmp70, ("h"))) {
                    tmp71 = 29;
                  } else if (equal(tmp70, ("-help"))) {
                    tmp71 = 30;
                  } else if (equal(tmp70, ("--help"))) {
                    tmp71 = 31;
                  } else if (true) {
                    tmp71 = 28;
                  }
                  switch (tmp71) {
                  case 0:
                    {
                    }
                  case 1:
                    {
                      (v_test_list = (silenceInc(), silenceDec(LINE(297,x_file(toString(v_argv.rvalAt(++v_i)))))));
                      if (toBoolean(v_test_list)) {
                        {
                          LOOP_COUNTER(72);
                          for (ArrayIterPtr iter74 = v_test_list.begin(); !iter74->end(); iter74->next()) {
                            LOOP_COUNTER_CHECK(72);
                            v_test = iter74->second();
                            {
                              (v_matches = ScalarArrays::sa_[0]);
                              if (toBoolean(LINE(301,x_preg_match("/^#.*\\[(.*)\\]\\:\\s+(.*)$/", toString(v_test), ref(v_matches))))) {
                                v_redir_tests.append((Array(ArrayInit(2).set(0, v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL)).create())));
                              }
                              else if (toBoolean(LINE(303,x_strlen(toString(v_test))))) {
                                v_test_files.append((LINE(304,x_trim(toString(v_test)))));
                              }
                            }
                          }
                        }
                      }
                      if (!equal(v_switch, "l")) {
                        goto break69;
                      }
                      v_i--;
                    }
                  case 2:
                    {
                      (v_failed_tests_file = LINE(314,(assignCallTemp(eo_0, toString(v_argv.rvalAt(++v_i))),x_fopen(eo_0, "w+t"))));
                      goto break69;
                    }
                  case 3:
                    {
                      (v_failed_tests_file = LINE(317,(assignCallTemp(eo_0, toString(v_argv.rvalAt(++v_i))),x_fopen(eo_0, "a+t"))));
                      goto break69;
                    }
                  case 4:
                    {
                      (v_conf_passed = v_argv.rvalAt(++v_i));
                      goto break69;
                    }
                  case 5:
                    {
                      v_ini_overwrites.append((v_argv.rvalAt(++v_i)));
                      goto break69;
                    }
                  case 6:
                    {
                      {
                        LOOP_COUNTER(75);
                        for (ArrayIterPtr iter77 = v_cfgfiles.begin(); !iter77->end(); iter77->next()) {
                          LOOP_COUNTER_CHECK(75);
                          v_file = iter77->second();
                          {
                            lval(v_cfg.lvalAt("keep", 0x24E892C8CBA97286LL)).set(v_file, (true));
                          }
                        }
                      }
                      goto break69;
                    }
                  case 7:
                    {
                      lval(v_cfg.lvalAt("keep", 0x24E892C8CBA97286LL)).set("skip", (true), 0x0A22E6725DCF1C8ALL);
                      goto break69;
                    }
                  case 8:
                    {
                      lval(v_cfg.lvalAt("keep", 0x24E892C8CBA97286LL)).set("php", (true), 0x52B182B0A94E9C41LL);
                      goto break69;
                    }
                  case 9:
                    {
                      lval(v_cfg.lvalAt("keep", 0x24E892C8CBA97286LL)).set("clean", (true), 0x09B438FD7866941DLL);
                      goto break69;
                    }
                  case 10:
                    {
                      (v_leak_check = true);
                      goto break69;
                    }
                  case 11:
                    {
                      if (!(toBoolean(v_pass_option_n))) {
                        concat_assign(v_pass_options, " -n");
                      }
                      (v_pass_option_n = true);
                      goto break69;
                    }
                  case 12:
                    {
                      goto break69;
                    }
                  case 13:
                    {
                      (v_no_clean = true);
                      goto break69;
                    }
                  case 14:
                    {
                      LINE(357,x_putenv("NO_INTERACTION=1"));
                      goto break69;
                    }
                  case 15:
                    {
                      (v_output_file = v_argv.rvalAt(++v_i));
                      (v_just_save_results = true);
                      goto break69;
                    }
                  case 16:
                    {
                      {
                        LOOP_COUNTER(78);
                        for (ArrayIterPtr iter80 = v_cfgfiles.begin(); !iter80->end(); iter80->next()) {
                          LOOP_COUNTER_CHECK(78);
                          v_file = iter80->second();
                          {
                            lval(v_cfg.lvalAt("show", 0x61A2B0ADCF60937FLL)).set(v_file, (true));
                          }
                        }
                      }
                      goto break69;
                    }
                  case 17:
                    {
                      lval(v_cfg.lvalAt("show", 0x61A2B0ADCF60937FLL)).set("skip", (true), 0x0A22E6725DCF1C8ALL);
                      goto break69;
                    }
                  case 18:
                    {
                      lval(v_cfg.lvalAt("show", 0x61A2B0ADCF60937FLL)).set("php", (true), 0x52B182B0A94E9C41LL);
                      goto break69;
                    }
                  case 19:
                    {
                      lval(v_cfg.lvalAt("show", 0x61A2B0ADCF60937FLL)).set("clean", (true), 0x09B438FD7866941DLL);
                      goto break69;
                    }
                  case 20:
                    {
                      (v_temp_source = v_argv.rvalAt(++v_i));
                      goto break69;
                    }
                  case 21:
                    {
                      (v_temp_target = v_argv.rvalAt(++v_i));
                      if (toBoolean(v_temp_urlbase)) {
                        (v_temp_urlbase = v_temp_target);
                      }
                      goto break69;
                    }
                  case 22:
                    {
                      (v_temp_urlbase = v_argv.rvalAt(++v_i));
                      goto break69;
                    }
                  case 23:
                    {
                    }
                  case 24:
                    {
                      (v_DETAILED = true);
                      goto break69;
                    }
                  case 25:
                    {
                      (v_switch = v_argv.rvalAt(v_i));
                      if (!equal(v_switch, "-")) {
                        (v_repeat = true);
                      }
                      goto break69;
                    }
                  case 26:
                    {
                      (v_html_file = (silenceInc(), silenceDec(LINE(403,(assignCallTemp(eo_0, toString(v_argv.rvalAt(++v_i))),x_fopen(eo_0, "wt"))))));
                      (v_html_output = LINE(404,x_is_resource(v_html_file)));
                      goto break69;
                    }
                  case 27:
                    {
                      echo("$Revision: 1.226.2.37.2.21 $\n");
                      f_exit(1LL);
                    }
                  case 28:
                    {
                      echo(LINE(410,concat3("Illegal switch '", toString(v_switch), "' specified!\n")));
                      if (equal(v_switch, "u") || equal(v_switch, "U")) {
                        goto break69;
                      }
                    }
                  case 29:
                    {
                    }
                  case 30:
                    {
                    }
                  case 31:
                    {
                      echo(toString("Synopsis:\n    php run-tests.php [options] [files] [directories]\n\nOptions:\n    -l <file>   Read the testfiles to be executed from <file>. After the test \n                has finished all failed tests are written to the same <file>. \n                If the list is empty and no further test is specified then\n                all tests are executed (same as: -r <file> -w <file>).\n\n    -r <file>   Read the testfiles to be executed from <file>.\n\n    -w <file>   Write a list of all failed tests to <file>.\n\n    -a <file>   Same as -w but append rather then truncating <file>.\n\n    -c <file>   Look for php.ini in directory <file> or use <file> as ini.\n\n    -n          Pass -n option to the php binary (Do not use a php.ini).\n\n    -d foo=bar  Pass -d option to the php binary (Define INI entry foo\n                with value 'bar').\n\n    -m          Test for memory leaks with Valgrind.\n    \n    -N          Always set (Test with unicode_semantics set off in PHP 6).\n    \n    -s <file>   Write output to <file>.\n\n    -q          Quiet, no user interaction (same as environment NO_INTERACTION).\n\n    --verbose\n    -v          Verbose mode.\n\n    --help\n    -h          This Help.\n\n    --html <file> Generate HTML output.\n\t\n    --temp-source <sdir>  --temp-target <tdir> [--temp-urlbase <url>]\n                Write temporary files to <tdir> by replacing <sdir> from the \n                filenames to generate with <tdir>. If --html is being used and \n                <url> given then the generated links are relative and prefixed\n                with the given url. In general you want to make <sdir> the path\n                to your source files and <tdir> some pach in your web page \n                hierarchy with <url> pointing to <tdir>.\n\n    --keep-[all|php|skip|clean]\n                Do not delete 'all' files, 'php' test file, 'skip' or 'clean' \n                file.\n\n    --show-[all|php|skip|clean]\n                Show 'all' files, 'php' test file, 'skip' or 'clean' file.\n\n    --no-clean  Do not execute clean section if any.\n"));
                      f_exit(1LL);
                    }
                  }
                  break69:;
                }
              }
            }
          }
          if (!(toBoolean(v_is_switch))) {
            (v_testfile = LINE(479,x_realpath(toString(v_argv.rvalAt(v_i)))));
            if (LINE(480,x_is_dir(toString(v_testfile)))) {
              LINE(481,f_find_files(v_testfile));
            }
            else if (toBoolean(LINE(482,x_preg_match("/\\.phpt$/", toString(v_testfile))))) {
              v_test_files.append((v_testfile));
            }
            else {
              f_exit(LINE(485,concat3("bogus test name ", toString(v_argv.rvalAt(v_i)), "\n")));
            }
          }
        }
      }
    }
    if (toBoolean(LINE(489,x_strlen(toString(v_conf_passed))))) {
      concat_assign(v_pass_options, LINE(491,concat3(" -c '", toString(v_conf_passed), "'")));
    }
    (v_test_files = LINE(493,x_array_unique(v_test_files)));
    (v_test_files = LINE(494,x_array_merge(2, v_test_files, Array(ArrayInit(1).set(0, v_redir_tests).create()))));
    (v_test_cnt = LINE(497,x_count(v_test_files)));
    if (toBoolean(v_test_cnt)) {
      LINE(499,f_write_information(v_html_output));
      LINE(500,x_usort(ref(v_test_files), "test_sort"));
      (v_start_time = LINE(501,x_time()));
      if (!(toBoolean(v_html_output))) {
        echo("Running selected tests.\n");
      }
      else {
        LINE(505,f_show_start(v_start_time));
      }
      (v_test_idx = 0LL);
      LINE(508,f_run_all_tests(v_test_files, v_environment));
      (v_end_time = LINE(509,x_time()));
      if (toBoolean(v_html_output)) {
        LINE(511,f_show_end(v_end_time));
      }
      if (toBoolean(v_failed_tests_file)) {
        LINE(514,x_fclose(toObject(v_failed_tests_file)));
      }
      if (toBoolean(LINE(516,x_count(v_test_files))) || toBoolean(x_count(v_test_results))) {
        LINE(517,f_compute_summary());
        if (toBoolean(v_html_output)) {
          LINE(519,(assignCallTemp(eo_0, toObject(v_html_file)),assignCallTemp(eo_1, concat("<hr/>\n", toString(f_get_summary(false, true)))),x_fwrite(eo_0, eo_1)));
        }
        echo("=====================================================================");
        echo(toString(LINE(522,f_get_summary(false, false))));
      }
      if (toBoolean(v_html_output)) {
        LINE(525,x_fclose(toObject(v_html_file)));
      }
      if ((equal(LINE(527,x_getenv("REPORT_EXIT_STATUS")), 1LL)) && (toBoolean((assignCallTemp(eo_1, x_implode(" ", v_test_results)),x_ereg("FAILED( |$)", eo_1))))) {
        f_exit(1LL);
      }
      f_exit(0LL);
    }
  }
  LINE(534,f_write_information(v_html_output));
  (v_test_files = ScalarArrays::sa_[0]);
  (v_exts_tested = LINE(538,x_count(v_exts_to_test)));
  (v_exts_skipped = 0LL);
  (v_ignored_by_ext = 0LL);
  LINE(541,x_sort(ref(v_exts_to_test)));
  (v_test_dirs = ScalarArrays::sa_[0]);
  (v_optionals = ScalarArrays::sa_[6]);
  {
    LOOP_COUNTER(81);
    for (ArrayIterPtr iter83 = v_optionals.begin(); !iter83->end(); iter83->next()) {
      LOOP_COUNTER_CHECK(81);
      v_dir = iter83->second();
      {
        if (equal((silenceInc(), silenceDec(LINE(545,x_filetype(toString(v_dir))))), "dir")) {
          v_test_dirs.append((v_dir));
        }
      }
    }
  }
  {
    LOOP_COUNTER(84);
    for (ArrayIterPtr iter86 = v_exts_to_test.begin(); !iter86->end(); iter86->next()) {
      LOOP_COUNTER_CHECK(84);
      v_val = iter86->second();
      v_key = iter86->first();
      {
        v_exts_to_test.set(v_key, (LINE(552,x_strtolower(toString(v_val)))));
      }
    }
  }
  {
    LOOP_COUNTER(87);
    for (ArrayIterPtr iter89 = v_test_dirs.begin(); !iter89->end(); iter89->next()) {
      LOOP_COUNTER_CHECK(87);
      v_dir = iter89->second();
      {
        LINE(556,(assignCallTemp(eo_0, concat3(toString(v_cwd), "/", toString(v_dir))),assignCallTemp(eo_1, (equal(v_dir, "ext"))),f_find_files(eo_0, eo_1)));
      }
    }
  }
  {
    LOOP_COUNTER(90);
    for (ArrayIterPtr iter92 = v_user_tests.begin(); !iter92->end(); iter92->next()) {
      LOOP_COUNTER_CHECK(90);
      v_dir = iter92->second();
      {
        LINE(560,f_find_files(v_dir, (equal(v_dir, "ext"))));
      }
    }
  }
  (v_test_files = LINE(621,x_array_unique(v_test_files)));
  LINE(622,x_usort(ref(v_test_files), "test_sort"));
  (v_start_time = LINE(624,x_time()));
  LINE(625,f_show_start(v_start_time));
  (v_test_cnt = LINE(627,x_count(v_test_files)));
  (v_test_idx = 0LL);
  LINE(629,f_run_all_tests(v_test_files, v_environment));
  (v_end_time = LINE(630,x_time()));
  if (toBoolean(v_failed_tests_file)) {
    LINE(632,x_fclose(toObject(v_failed_tests_file)));
  }
  if (equal(0LL, LINE(637,x_count(v_test_results)))) {
    echo("No tests were run.\n");
    return null;
  }
  LINE(642,f_compute_summary());
  LINE(644,f_show_end(v_end_time));
  LINE(645,f_show_summary());
  if (toBoolean(v_html_output)) {
    LINE(648,x_fclose(toObject(v_html_file)));
  }
  if ((equal(LINE(766,x_getenv("REPORT_EXIT_STATUS")), 1LL)) && (toBoolean(v_sum_results.rvalAt("FAILED", 0x52B3F0742123E85BLL)))) {
    f_exit(1LL);
  }
  f_exit(0LL);
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
