
#include <php/roadsend/tests/dataseek.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$dataseek_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/dataseek.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$dataseek_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_query __attribute__((__unused__)) = (variables != gVariables) ? variables->get("query") : g->GV(query);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);

  (toBoolean((v_link = LINE(2,x_mysql_pconnect("localhost", "develUser", "d3v3lpa55"))))) || (toBoolean(f_exit("Could not connect")));
  (toBoolean(LINE(5,x_mysql_select_db("test")))) || (toBoolean(f_exit("Could not select database")));
  (v_query = "SELECT last_name, first_name FROM friends");
  (toBoolean((v_result = LINE(9,x_mysql_query(toString(v_query)))))) || (toBoolean(f_exit("Query failed")));
  {
    LOOP_COUNTER(1);
    for ((v_i = LINE(14,x_mysql_num_rows(v_result)) - 1LL); not_less(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(1);
      {
        if (!(LINE(15,x_mysql_data_seek(v_result, toInt32(v_i))))) {
          echo(LINE(16,concat3("Cannot seek to row ", toString(v_i), "\n")));
          continue;
        }
        if (!((toBoolean((v_row = LINE(20,x_mysql_fetch_object(v_result))))))) {
          echo(LINE(21,concat3("Cannot fetch object ", toString(v_i), "\n")));
          continue;
        }
        echo(LINE(24,concat4(toString(v_row.o_get("last_name", 0x748E1D8307E6D301LL)), " ", toString(v_row.o_get("first_name", 0x012A5ADB562F426ALL)), "<br />\n")));
      }
    }
  }
  LINE(27,x_mysql_free_result(v_result));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
