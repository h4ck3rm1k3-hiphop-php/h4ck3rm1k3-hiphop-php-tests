
#include <php/roadsend/tests/math.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$math_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/math.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$math_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);

  echo("3.1415926535898\n");
  echo("2.718281828459\n");
  echo("1.442695040889\n");
  echo("0.43429448190325\n");
  echo("0.69314718055995\n");
  echo("2.302585092994\n");
  echo("1.5707963267949\n");
  echo("0.78539816339745\n");
  echo("0.31830988618379\n");
  echo("0.63661977236758\n");
  echo("1.1283791670955\n");
  echo("1.4142135623731\n");
  echo("0.70710678118655\n");
  echo("abs\n");
  echo(concat(concat_rev(toString(LINE(27,x_abs(1.2344999999999999))), concat_rev(toString(x_abs(5LL)), concat(concat_rev(toString(x_abs(0LL)), concat(concat_rev(toString(x_abs("foo")), concat(toString(x_abs(-2354LL)), " ")), " ")), " "))), "\n"));
  echo("acos\n");
  echo(concat(concat_rev(toString(LINE(30,x_acos(1.2344999999999999))), concat_rev(toString(x_acos(toDouble(5LL))), concat(concat_rev(toString(x_acos(toDouble(0LL))), concat(concat_rev(toString(x_acos(toDouble("foo"))), concat(toString(x_acos(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  {
    echo("acosh\n");
    echo(concat(concat_rev(toString(LINE(34,x_acosh(1.2344999999999999))), concat_rev(toString(x_acosh(toDouble(5LL))), concat(concat_rev(toString(x_acosh(toDouble(0LL))), concat(concat_rev(toString(x_acosh(toDouble("foo"))), concat(toString(x_acosh(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  }
  echo("asin\n");
  echo(concat(concat_rev(toString(LINE(38,x_asin(1.2344999999999999))), concat_rev(toString(x_asin(toDouble(5LL))), concat(concat_rev(toString(x_asin(toDouble(0LL))), concat(concat_rev(toString(x_asin(toDouble("foo"))), concat(toString(x_asin(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  {
    echo("asinh\n");
    echo(concat(concat_rev(toString(LINE(42,x_asinh(1.2344999999999999))), concat_rev(toString(x_asinh(toDouble(5LL))), concat(concat_rev(toString(x_asinh(toDouble(0LL))), concat(concat_rev(toString(x_asinh(toDouble("foo"))), concat(toString(x_asinh(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  }
  echo("atan\n");
  echo(concat(concat_rev(toString(LINE(46,x_atan(1.2344999999999999))), concat_rev(toString(x_atan(toDouble(5LL))), concat(concat_rev(toString(x_atan(toDouble(0LL))), concat(concat_rev(toString(x_atan(toDouble("foo"))), concat(toString(x_atan(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  {
    echo("atanh\n");
    echo(concat(concat_rev(toString(LINE(50,x_atanh(1.2344999999999999))), concat_rev(toString(x_atanh(toDouble(5LL))), concat(concat_rev(toString(x_atanh(toDouble(0LL))), concat(concat_rev(toString(x_atanh(toDouble("foo"))), concat(toString(x_atanh(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  }
  echo("atan2\n");
  echo(concat(concat_rev(toString(LINE(54,x_atan2(1.2344999999999999, 3.2200000000000002))), concat_rev(toString(x_atan2(toDouble(5LL), 2.2999999999999998)), concat(concat_rev(toString(x_atan2(toDouble(0LL), toDouble(3LL))), concat(concat_rev(toString(x_atan2(toDouble("foo"), toDouble(12LL))), concat(toString(x_atan2(toDouble(-2354LL), toDouble(3LL))), " ")), " ")), " "))), "\n"));
  echo("base_convert\n");
  echo(concat(concat_rev(LINE(58,x_base_convert(toString(1.2344999999999999), 8LL, 2LL)), concat(concat_rev(x_base_convert(toString(5LL), 4LL, 7LL), concat(concat_rev(x_base_convert(toString(5LL), 6LL, 7LL), concat(concat_rev(LINE(57,x_base_convert(toString(300LL), 4LL, 20LL)), concat(concat_rev(x_base_convert("foo", 10LL, 10LL), concat(x_base_convert("-2354", 10LL, 2LL), " ")), " ")), " ")), " ")), " ")), "\n"));
  echo("bindec\n");
  echo(concat(concat_rev(toString(LINE(61,x_bindec("1.2345"))), concat_rev(toString(x_bindec(toString(5LL))), concat(concat_rev(toString(x_bindec(toString(0LL))), concat(concat_rev(toString(x_bindec("foo")), concat(toString(x_bindec("-2354")), " ")), " ")), " "))), "\n"));
  echo("ceil\n");
  echo(concat(concat_rev(toString(LINE(64,x_ceil(1.2344999999999999))), concat_rev(toString(x_ceil(toDouble(5LL))), concat(concat_rev(toString(x_ceil(toDouble(0LL))), concat(concat_rev(toString(x_ceil(toDouble("foo"))), concat(toString(x_ceil(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("cos\n");
  echo(concat(concat_rev(toString(LINE(67,x_cos(1.2344999999999999))), concat_rev(toString(x_cos(toDouble(5LL))), concat(concat_rev(toString(x_cos(toDouble(0LL))), concat(concat_rev(toString(x_cos(toDouble("foo"))), concat(toString(x_cos(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("cosh\n");
  echo(concat(concat_rev(toString(LINE(70,x_cosh(1.2344999999999999))), concat_rev(toString(x_cosh(toDouble(5LL))), concat(concat_rev(toString(x_cosh(toDouble(0LL))), concat(concat_rev(toString(x_cosh(toDouble("foo"))), concat(toString(x_cosh(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("decbin\n");
  echo(concat(concat_rev(LINE(73,x_decbin(toInt64(1.2344999999999999))), concat_rev(x_decbin(toInt64("5")), concat(concat_rev(x_decbin(0LL), concat(concat_rev(x_decbin(toInt64("foo")), concat(x_decbin(2354LL), " ")), " ")), " "))), "\n"));
  echo("dechex\n");
  echo(concat(concat_rev(LINE(76,x_dechex(toInt64(1.2344999999999999))), concat_rev(x_dechex(5LL), concat(concat_rev(x_dechex(0LL), concat(concat_rev(x_dechex(toInt64("foo")), concat(x_dechex(toInt64("2354")), " ")), " ")), " "))), "\n"));
  echo("decoct\n");
  echo(concat(concat_rev(LINE(79,x_decoct(toInt64(1.2344999999999999))), concat_rev(x_decoct(5LL), concat(concat_rev(x_decoct(0LL), concat(concat_rev(x_decoct(toInt64("foo")), concat(x_decoct(2354LL), " ")), " ")), " "))), "\n"));
  echo("deg2rad\n");
  echo(concat(concat_rev(toString(LINE(82,x_deg2rad(1.2344999999999999))), concat_rev(toString(x_deg2rad(toDouble(5LL))), concat(concat_rev(toString(x_deg2rad(toDouble(0LL))), concat(concat_rev(toString(x_deg2rad(toDouble("foo"))), concat(toString(x_deg2rad(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("exp\n");
  echo(concat(concat_rev(toString(LINE(85,x_exp(1.2344999999999999))), concat_rev(toString(x_exp(toDouble(5LL))), concat(concat_rev(toString(x_exp(toDouble(0LL))), concat(concat_rev(toString(x_exp(toDouble("foo"))), concat(toString(x_exp(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  {
    echo("expm1\n");
    echo(concat(concat_rev(toString(LINE(89,x_expm1(1.2344999999999999))), concat_rev(toString(x_expm1(toDouble(5LL))), concat(concat_rev(toString(x_expm1(toDouble(0LL))), concat(concat_rev(toString(x_expm1(toDouble("foo"))), concat(toString(x_expm1(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  }
  echo("floor\n");
  echo(concat(concat_rev(toString(LINE(93,x_floor(1.2344999999999999))), concat_rev(toString(x_floor(toDouble(5LL))), concat(concat_rev(toString(x_floor(toDouble(0LL))), concat(concat_rev(toString(x_floor(toDouble("foo"))), concat(toString(x_floor(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  {
    echo(concat(toString(LINE(98,x_getrandmax())), "\n"));
    if (less(100000LL, LINE(99,x_getrandmax()))) {
      echo("getrandmax: Well, that's a start.\n");
    }
    else {
      echo(LINE(103,(assignCallTemp(eo_1, toString(x_getrandmax())),concat3("woops, getrandmax: ", eo_1, "\n"))));
    }
  }
  echo("hexdec\n");
  echo(concat(concat_rev(toString(LINE(109,x_hexdec("1.2345"))), concat_rev(toString(x_hexdec(toString(5LL))), concat(concat_rev(toString(x_hexdec(toString(0LL))), concat(concat_rev(toString(x_hexdec("foo")), concat(toString(x_hexdec("-2354")), " ")), " ")), " "))), "\n"));
  {
    echo("hypot\n");
    echo(concat(concat_rev(toString(LINE(113,x_hypot(1.2344999999999999, 2.3233999999999999))), concat_rev(toString(x_hypot(toDouble(5LL), toDouble(12LL))), concat(concat_rev(toString(x_hypot(toDouble(0LL), toDouble(0LL))), concat(concat_rev(toString(x_hypot(toDouble("foo"), toDouble(2LL))), concat(toString(x_hypot(toDouble(-2354LL), toDouble(34LL))), " ")), " ")), " "))), "\n"));
  }
  echo("log\n");
  echo(concat(concat_rev(toString(LINE(117,x_log(1.2344999999999999))), concat_rev(toString(x_log(toDouble(5LL))), concat(concat_rev(toString(x_log(toDouble(0LL))), concat(concat_rev(toString(x_log(toDouble("foo"))), concat(toString(x_log(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("log10\n");
  echo(concat(concat_rev(toString(LINE(120,x_log10(1.2344999999999999))), concat_rev(toString(x_log10(toDouble(5LL))), concat(concat_rev(toString(x_log10(toDouble(0LL))), concat(concat_rev(toString(x_log10(toDouble("foo"))), concat(toString(x_log10(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  {
    echo("log1p\n");
    echo(concat(concat_rev(toString(LINE(124,x_log1p(1.2344999999999999))), concat_rev(toString(x_log1p(toDouble(5LL))), concat(concat_rev(toString(x_log1p(toDouble(0LL))), concat(concat_rev(toString(x_log1p(toDouble("foo"))), concat(toString(x_log1p(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  }
  echo("octdec\n");
  echo(concat(concat_rev(toString(LINE(128,x_octdec("1.2345"))), concat_rev(toString(x_octdec(toString(5LL))), concat(concat_rev(toString(x_octdec(toString(0LL))), concat(concat_rev(toString(x_octdec("foo")), concat(toString(x_octdec("-2354")), " ")), " ")), " "))), "\n"));
  echo("pi\n");
  echo(concat(toString(LINE(131,x_pi())), "\n"));
  echo("pow\n");
  echo(concat(concat_rev(toString(LINE(134,x_pow(1.2344999999999999, 2.3233999999999999))), concat_rev(toString(x_pow(5LL, 12LL)), concat(concat_rev(toString(x_pow(0LL, 0LL)), concat(concat_rev(toString(x_pow("foo", 2LL)), concat(toString(x_pow(-2354LL, 4LL)), " ")), " ")), " "))), "\n"));
  LINE(135,x_var_dump(1, x_pow(2LL, 2LL)));
  echo("\n");
  LINE(136,x_var_dump(1, x_pow(2LL, 2.0)));
  LINE(137,x_var_dump(1, x_pow(2.0, 2.0)));
  LINE(138,x_var_dump(1, x_pow(2.0, 2LL)));
  echo(concat(toString(LINE(140,x_pow(2LL, 64LL))), "\n"));
  echo(concat(toString(LINE(141,x_pow(2LL, 64.0))), "\n"));
  echo("rad2deg\n");
  echo(concat(concat_rev(toString(LINE(144,x_rad2deg(1.2344999999999999))), concat_rev(toString(x_rad2deg(toDouble(5LL))), concat(concat_rev(toString(x_rad2deg(toDouble(0LL))), concat(concat_rev(toString(x_rad2deg(toDouble("foo"))), concat(toString(x_rad2deg(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("rand\n");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 1000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_zot.set(LINE(148,x_rand(0LL, 2LL)), ("foo!\n"));
      }
    }
  }
  LINE(151,x_sort(ref(v_zot)));
  LINE(152,x_print_r(v_zot));
  echo("round\n");
  echo(concat(concat_rev(toString(LINE(155,x_round(1.2344999999999999))), concat_rev(toString(x_round(5LL)), concat(concat_rev(toString(x_round(0LL)), concat(concat_rev(toString(x_round("foo")), concat(toString(x_round(-2354LL)), " ")), " ")), " "))), "\n"));
  echo("sin\n");
  echo(concat(concat_rev(toString(LINE(158,x_sin(1.2344999999999999))), concat_rev(toString(x_sin(toDouble(5LL))), concat(concat_rev(toString(x_sin(toDouble(0LL))), concat(concat_rev(toString(x_sin(toDouble("foo"))), concat(toString(x_sin(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("sinh\n");
  echo(concat(concat_rev(toString(LINE(161,x_sinh(1.2344999999999999))), concat_rev(toString(x_sinh(toDouble(5LL))), concat(concat_rev(toString(x_sinh(toDouble(0LL))), concat(concat_rev(toString(x_sinh(toDouble("foo"))), concat(toString(x_sinh(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("sqrt\n");
  echo(concat(concat_rev(toString(LINE(164,x_sqrt(1.2344999999999999))), concat_rev(toString(x_sqrt(toDouble(5LL))), concat(concat_rev(toString(x_sqrt(toDouble(0LL))), concat(concat_rev(toString(x_sqrt(toDouble("foo"))), concat(toString(x_sqrt(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("srand\n");
  LINE(167,x_srand(12LL));
  echo("(no output)\n");
  echo("tan\n");
  echo(concat(concat_rev(toString(LINE(171,x_tan(1.2344999999999999))), concat_rev(toString(x_tan(toDouble(5LL))), concat(concat_rev(toString(x_tan(toDouble(0LL))), concat(concat_rev(toString(x_tan(toDouble("foo"))), concat(toString(x_tan(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("tanh\n");
  echo(concat(concat_rev(toString(LINE(174,x_tanh(1.2344999999999999))), concat_rev(toString(x_tanh(toDouble(5LL))), concat(concat_rev(toString(x_tanh(toDouble(0LL))), concat(concat_rev(toString(x_tanh(toDouble("foo"))), concat(toString(x_tanh(toDouble(-2354LL))), " ")), " ")), " "))), "\n"));
  echo("is_finite\n");
  if (LINE(177,x_is_finite(toDouble(1LL)))) {
    echo("works.");
  }
  if (LINE(180,x_is_finite(x_log(toDouble(0LL))))) {
    echo(" broken");
  }
  if (LINE(183,x_is_finite(x_sqrt(toDouble(-1LL))))) {
    echo(" brokener");
  }
  echo("\n");
  echo("is_infinite\n");
  if (LINE(189,x_is_infinite(toDouble(1LL)))) {
    echo("broken.");
  }
  if (LINE(192,x_is_infinite(x_log(toDouble(0LL))))) {
    echo(" works");
  }
  if (LINE(195,x_is_infinite(x_sqrt(toDouble(-1LL))))) {
    echo(" brokener");
  }
  echo("\n");
  echo("is_nan\n");
  if (LINE(201,x_is_nan(toDouble(1LL)))) {
    echo("broken.");
  }
  if (LINE(204,x_is_nan(x_log(toDouble(0LL))))) {
    echo(" brokener");
  }
  if (LINE(207,x_is_nan(x_sqrt(toDouble(-1LL))))) {
    echo(" works");
  }
  echo("\n");
  LINE(213,x_mt_srand(101LL));
  echo(LINE(217,(assignCallTemp(eo_1, toString(x_mt_rand(0LL, 10LL))),concat3("[", eo_1, "]\n"))));
  echo(toString(LINE(221,x_mt_getrandmax())));
  echo("\n;XXX not implemented yet:\n;\n; lcg_value -- Combined linear congruential generator\n\n\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
