
#include <php/roadsend/tests/zswitch3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/zswitch3.php line 3 */
void f_switchtest(int64 v_i, CVarRef v_j) {
  FUNCTION_INJECTION(switchtest);
  {
    switch (v_i) {
    case 0LL:
      {
        {
          switch (toInt64(v_j)) {
          case 0LL:
            {
              echo("zero");
              break;
            }
          case 1LL:
            {
              echo("one");
              break;
            }
          default:
            {
              echo(toString(v_j));
              break;
            }
          }
        }
        echo("\n");
        break;
      }
    default:
      {
        echo("Default taken\n");
      }
    }
  }
} /* function */
Variant pm_php$roadsend$tests$zswitch3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zswitch3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zswitch3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, 3LL); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        {
          LOOP_COUNTER(4);
          for ((v_k = 0LL); less(v_k, 10LL); v_k++) {
            LOOP_COUNTER_CHECK(4);
            {
              LINE(26,f_switchtest(0LL, v_k));
            }
          }
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
