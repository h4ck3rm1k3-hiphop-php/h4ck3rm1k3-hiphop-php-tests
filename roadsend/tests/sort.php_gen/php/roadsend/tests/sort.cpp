
#include <php/roadsend/tests/sort.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$roadsend$tests$sort_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/sort.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$sort_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fruits __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fruits") : g->GV(fruits);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);

  (v_fruits = ScalarArrays::sa_[0]);
  LINE(4,x_print_r(v_fruits));
  LINE(5,x_sort(ref(v_fruits)));
  LINE(6,x_reset(ref(v_fruits)));
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(7,x_each(ref(v_fruits))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(8,concat5("fruits[", toString(v_key), "] = ", toString(v_val), "\n")));
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
