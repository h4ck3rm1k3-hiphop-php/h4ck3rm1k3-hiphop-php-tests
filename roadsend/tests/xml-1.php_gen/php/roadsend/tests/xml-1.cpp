
#include <php/roadsend/tests/xml-1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/xml-1.php line 7 */
Variant c_xmlparser::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_xmlparser::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_xmlparser::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_xmlparser::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_xmlparser::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_xmlparser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_xmlparser::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_xmlparser::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(xmlparser)
ObjectData *c_xmlparser::cloneImpl() {
  c_xmlparser *obj = NEW(c_xmlparser)();
  cloneSet(obj);
  return obj;
}
void c_xmlparser::cloneSet(c_xmlparser *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_xmlparser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x55BFFBE5717EFD62LL, startelement) {
        return (t_startelement(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x78A7A5D96EF2F4AFLL, endelement) {
        return (t_endelement(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_xmlparser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x55BFFBE5717EFD62LL, startelement) {
        return (t_startelement(a0, a1, a2), null);
      }
      break;
    case 3:
      HASH_GUARD(0x78A7A5D96EF2F4AFLL, endelement) {
        return (t_endelement(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_xmlparser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_xmlparser$os_get(const char *s) {
  return c_xmlparser::os_get(s, -1);
}
Variant &cw_xmlparser$os_lval(const char *s) {
  return c_xmlparser::os_lval(s, -1);
}
Variant cw_xmlparser$os_constant(const char *s) {
  return c_xmlparser::os_constant(s);
}
Variant cw_xmlparser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_xmlparser::os_invoke(c, s, params, -1, fatal);
}
void c_xmlparser::init() {
}
/* SRC: roadsend/tests/xml-1.php line 9 */
void c_xmlparser::t_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::startElement);
  print(LINE(10,concat3("start: ", toString(v_name), "\n")));
  LINE(11,x_print_r(v_attrs));
} /* function */
/* SRC: roadsend/tests/xml-1.php line 14 */
void c_xmlparser::t_endelement(CVarRef v_parser, CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(xmlparser, xmlparser::endElement);
  print(LINE(15,concat3("end: ", toString(v_name), "\n")));
} /* function */
Object co_xmlparser(CArrRef params, bool init /* = true */) {
  return Object(p_xmlparser(NEW(c_xmlparser)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$xml_1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/xml-1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$xml_1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);
  Variant &v_pObj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pObj") : g->GV(pObj);
  Variant &v_xml_parser __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xml_parser") : g->GV(xml_parser);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_xml_parser2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xml_parser2") : g->GV(xml_parser2);

  (v_file = concat(toString(LINE(4,x_getenv("PCC_HOME"))), "/runtime/ext/xml/tests/data.xml"));
  echo(LINE(5,concat3("working with ", toString(v_file), "\n")));
  (v_pObj = LINE(20,p_xmlparser(p_xmlparser(NEWOBJ(c_xmlparser)())->create())));
  (v_xml_parser = LINE(22,x_xml_parser_create()));
  LINE(24,x_xml_set_element_handler(toObject(v_xml_parser), "startElement", "endElement"));
  LINE(25,x_xml_set_object(toObject(v_xml_parser), ref(v_pObj)));
  if (!((toBoolean((v_fp = LINE(27,x_fopen(toString(v_file), "r"))))))) {
    f_exit("could not open XML input");
  }
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_data = LINE(31,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (!(toBoolean(LINE(32,(assignCallTemp(eo_0, toObject(v_xml_parser)),assignCallTemp(eo_1, toString(v_data)),assignCallTemp(eo_2, x_feof(toObject(v_fp))),x_xml_parse(eo_0, eo_1, eo_2)))))) {
          f_exit(LINE(35,(assignCallTemp(eo_1, LINE(34,x_xml_error_string(x_xml_get_error_code(toObject(v_xml_parser))))),assignCallTemp(eo_2, LINE(35,x_xml_get_current_line_number(toObject(v_xml_parser)))),x_sprintf(3, "XML error: %s at line %d", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create())))));
        }
      }
    }
  }
  LINE(38,x_xml_parser_free(toObject(v_xml_parser)));
  (v_xml_parser2 = LINE(42,x_xml_parser_create()));
  LINE(43,x_xml_set_element_handler(toObject(v_xml_parser2), toString(Array(ArrayInit(2).set(0, v_pObj).set(1, "startElement").create())), toString(Array(ArrayInit(2).set(0, v_pObj).set(1, "endElement").create()))));
  if (!((toBoolean((v_fp = LINE(45,x_fopen(toString(v_file), "r"))))))) {
    f_exit("could not open XML input");
  }
  LOOP_COUNTER(2);
  {
    while (toBoolean((v_data = LINE(49,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(2);
      {
        if (!(toBoolean(LINE(50,(assignCallTemp(eo_0, toObject(v_xml_parser2)),assignCallTemp(eo_1, toString(v_data)),assignCallTemp(eo_2, x_feof(toObject(v_fp))),x_xml_parse(eo_0, eo_1, eo_2)))))) {
          f_exit(LINE(53,(assignCallTemp(eo_1, LINE(52,x_xml_error_string(x_xml_get_error_code(toObject(v_xml_parser2))))),assignCallTemp(eo_2, LINE(53,x_xml_get_current_line_number(toObject(v_xml_parser2)))),x_sprintf(3, "XML error: %s at line %d", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create())))));
        }
      }
    }
  }
  LINE(56,x_xml_parser_free(toObject(v_xml_parser2)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
