
#ifndef __GENERATED_php_roadsend_tests_xml_1_h__
#define __GENERATED_php_roadsend_tests_xml_1_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/xml-1.fw.h>

// Declarations
#include <cls/xmlparser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$xml_1_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_xmlparser(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_xml_1_h__
