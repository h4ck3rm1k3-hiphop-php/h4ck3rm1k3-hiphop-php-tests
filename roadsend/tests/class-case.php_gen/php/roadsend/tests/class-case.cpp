
#include <php/roadsend/tests/class-case.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/class-case.php line 3 */
Variant c_test::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_test::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_test::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("BaZBiP", m_BaZBiP));
  c_ObjectData::o_get(props);
}
bool c_test::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x26183BC81EB3AA41LL, BaZBiP, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_test::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x26183BC81EB3AA41LL, m_BaZBiP,
                         BaZBiP, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_test::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x26183BC81EB3AA41LL, m_BaZBiP,
                      BaZBiP, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_test::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_test::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(test)
ObjectData *c_test::cloneImpl() {
  c_test *obj = NEW(c_test)();
  cloneSet(obj);
  return obj;
}
void c_test::cloneSet(c_test *clone) {
  clone->m_BaZBiP = m_BaZBiP;
  ObjectData::cloneSet(clone);
}
Variant c_test::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x2ACE2D47D81F2549LL, foobar) {
        return (t_foobar(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_test::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x2ACE2D47D81F2549LL, foobar) {
        return (t_foobar(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_test::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_test$os_get(const char *s) {
  return c_test::os_get(s, -1);
}
Variant &cw_test$os_lval(const char *s) {
  return c_test::os_lval(s, -1);
}
Variant cw_test$os_constant(const char *s) {
  return c_test::os_constant(s);
}
Variant cw_test$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_test::os_invoke(c, s, params, -1, fatal);
}
void c_test::init() {
  m_BaZBiP = "yes";
}
/* SRC: roadsend/tests/class-case.php line 5 */
void c_test::t_foobar() {
  INSTANCE_METHOD_INJECTION(TeSt, TeSt::FooBar);
  echo(LINE(6,concat3("-> ", toString(o_get("bazbip", 0x5F052BBE8EC15C82LL)), "\n")));
  echo(LINE(7,concat3("-> ", m_BaZBiP, "\n")));
} /* function */
Object co_test(CArrRef params, bool init /* = true */) {
  return Object(p_test(NEW(c_test)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$class_case_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/class-case.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$class_case_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = ((Object)(LINE(11,p_test(p_test(NEWOBJ(c_test)())->create())))));
  LINE(12,x_var_dump(1, v_a));
  LINE(13,v_a.o_invoke_few_args("foobar", 0x2ACE2D47D81F2549LL, 0));
  (v_b = LINE(14,x_get_class_methods(v_a)));
  LINE(15,x_var_dump(1, v_b));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
