
#ifndef __GENERATED_php_roadsend_tests_class_case_h__
#define __GENERATED_php_roadsend_tests_class_case_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/class-case.fw.h>

// Declarations
#include <cls/test.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$class_case_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_test(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_class_case_h__
