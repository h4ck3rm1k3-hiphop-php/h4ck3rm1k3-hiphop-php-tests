
#include <php/roadsend/tests/extract.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$extract_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/extract.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$extract_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_var_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("var_array") : g->GV(var_array);
  Variant &v_color __attribute__((__unused__)) = (variables != gVariables) ? variables->get("color") : g->GV(color);
  Variant &v_size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("size") : g->GV(size);
  Variant &v_shape __attribute__((__unused__)) = (variables != gVariables) ? variables->get("shape") : g->GV(shape);
  Variant &v_pref_color __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pref_color") : g->GV(pref_color);
  Variant &v_pref_size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pref_size") : g->GV(pref_size);
  Variant &v_pref_shape __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pref_shape") : g->GV(pref_shape);
  Variant &v_newpref_color __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newpref_color") : g->GV(newpref_color);
  Variant &v_newpref_size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newpref_size") : g->GV(newpref_size);
  Variant &v_blahshape __attribute__((__unused__)) = (variables != gVariables) ? variables->get("blahshape") : g->GV(blahshape);
  Variant &v_inpref_22 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("inpref_22") : g->GV(inpref_22);
  Variant &v_noshape __attribute__((__unused__)) = (variables != gVariables) ? variables->get("noshape") : g->GV(noshape);
  Variant &v_ifpref_color __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ifpref_color") : g->GV(ifpref_color);
  Variant &v_ifpref_size __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ifpref_size") : g->GV(ifpref_size);
  Variant &v_meepshape __attribute__((__unused__)) = (variables != gVariables) ? variables->get("meepshape") : g->GV(meepshape);
  Variant &v_fnord __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fnord") : g->GV(fnord);

  (v_var_array = ScalarArrays::sa_[0]);
  LINE(6,extract(variables, toArray(v_var_array)));
  echo(concat("EXTR_OVERWRITE (def): ", LINE(8,concat6(toString(v_color), ", ", toString(v_size), ", ", toString(v_shape), "\n"))));
  (v_var_array = ScalarArrays::sa_[0]);
  LINE(13,extract(variables, toArray(v_var_array), toInt32(3LL) /* EXTR_PREFIX_ALL */, "pref"));
  echo(concat("PREFIX_ALL: ", LINE(18,concat6(toString(v_pref_color), ", ", toString(v_pref_size), ", ", toString(v_pref_shape), "\n"))));
  (v_var_array = ScalarArrays::sa_[1]);
  LINE(24,extract(variables, toArray(v_var_array), toInt32(2LL) /* EXTR_PREFIX_SAME */, "newpref"));
  echo(concat("PREFIX_SAME: ", LINE(26,concat6(toString(v_newpref_color), ", ", toString(v_newpref_size), ", ", toString(v_blahshape), "\n"))));
  (v_var_array = ScalarArrays::sa_[2]);
  LINE(32,extract(variables, toArray(v_var_array), toInt32(4LL) /* EXTR_PREFIX_INVALID */, "inpref"));
  echo(concat("PREFIX_INVALID: ", LINE(34,concat6(toString(v_color), ", ", toString(v_size), ", ", toString(v_inpref_22), "\n"))));
  (v_var_array = ScalarArrays::sa_[3]);
  LINE(40,extract(variables, toArray(v_var_array), toInt32(1LL) /* EXTR_SKIP */));
  echo(concat("EXTR_SKIP: ", LINE(42,concat6(toString(v_color), ", ", toString(v_size), ", ", toString(v_noshape), "\n"))));
  (v_var_array = ScalarArrays::sa_[4]);
  LINE(48,extract(variables, toArray(v_var_array), toInt32(5LL) /* EXTR_PREFIX_IF_EXISTS */, "ifpref"));
  echo(concat("EXTR_PREFIX_IF_EXISTS: ", LINE(50,concat6(toString(v_ifpref_color), ", ", toString(v_ifpref_size), ", ", toString(v_meepshape), "\n"))));
  (v_var_array = ScalarArrays::sa_[5]);
  LINE(56,extract(variables, toArray(v_var_array), toInt32(6LL) /* EXTR_IF_EXISTS */));
  echo(concat("EXTR_IF_EXISTS: ", LINE(59,concat6(toString(v_color), ", ", toString(v_size), ", ", toString(v_fnord), "\n"))));
  (v_var_array = ScalarArrays::sa_[0]);
  LINE(65,extract(variables, toArray(v_var_array), toInt32(256LL) /* EXTR_REFS */));
  echo(concat("EXTR_IF_EXISTS: ", LINE(67,concat6(toString(v_color), ", ", toString(v_size), ", ", toString(v_shape), "\n"))));
  v_var_array.set("color", ("newvalue"), 0x1B20384A65BB9927LL);
  echo(concat("EXTR_IF_EXISTS: ", LINE(69,concat6(toString(v_color), ", ", toString(v_size), ", ", toString(v_shape), "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
