
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 8:
      HASH_INDEX(0x26C2B819262A6288LL, fnord, 27);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x51D6F99D8658AB49LL, shape, 15);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x3FE8023BDF261C0FLL, size, 14);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      HASH_INDEX(0x1544FA255CB78BD1LL, pref_size, 17);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 36:
      HASH_INDEX(0x0F42E71C38B0F2E4LL, inpref_22, 22);
      break;
    case 39:
      HASH_INDEX(0x1B20384A65BB9927LL, color, 13);
      break;
    case 40:
      HASH_INDEX(0x0A1086E917EBD728LL, var_array, 12);
      break;
    case 42:
      HASH_INDEX(0x25A3A0DE9EE15C6ALL, blahshape, 21);
      break;
    case 44:
      HASH_INDEX(0x18D42EF81920F8ACLL, ifpref_color, 24);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 47:
      HASH_INDEX(0x52E599B6E15024AFLL, ifpref_size, 25);
      break;
    case 54:
      HASH_INDEX(0x2E89746B84732936LL, pref_color, 16);
      break;
    case 56:
      HASH_INDEX(0x7DA75907F80DBF78LL, noshape, 23);
      break;
    case 57:
      HASH_INDEX(0x54F073F89EC16379LL, newpref_size, 20);
      HASH_INDEX(0x0791F0C3BF432D79LL, meepshape, 26);
      break;
    case 59:
      HASH_INDEX(0x58DA22BF109628BBLL, pref_shape, 18);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      HASH_INDEX(0x3FF1298F7C22B9BFLL, newpref_color, 19);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 28) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
