
#ifndef __GENERATED_php_roadsend_tests_overloading_h__
#define __GENERATED_php_roadsend_tests_overloading_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/overloading.fw.h>

// Declarations
#include <cls/tester.h>
#include <cls/setter.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$overloading_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_tester(CArrRef params, bool init = true);
Object co_setter(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_overloading_h__
