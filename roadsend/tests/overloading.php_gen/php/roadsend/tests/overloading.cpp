
#include <php/roadsend/tests/overloading.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/overloading.php line 3 */
Variant c_tester::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_tester::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_tester::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  props.push_back(NEW(ArrayElement)("b", m_b.isReferenced() ? ref(m_b) : m_b));
  props.push_back(NEW(ArrayElement)("c", m_c.isReferenced() ? ref(m_c) : m_c));
  c_ObjectData::o_get(props);
}
bool c_tester::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x32C769EE5C5509B0LL, c, 1);
      break;
    case 2:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    case 5:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_tester::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_tester::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x32C769EE5C5509B0LL, m_c,
                      c, 1);
      break;
    case 2:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    case 5:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_tester::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x32C769EE5C5509B0LL, m_c,
                         c, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    case 5:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_tester::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(tester)
ObjectData *c_tester::cloneImpl() {
  c_tester *obj = NEW(c_tester)();
  cloneSet(obj);
  return obj;
}
void c_tester::cloneSet(c_tester *clone) {
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  clone->m_b = m_b.isReferenced() ? ref(m_b) : m_b;
  clone->m_c = m_c.isReferenced() ? ref(m_c) : m_c;
  ObjectData::cloneSet(clone);
}
Variant c_tester::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x564D4529AE55BB92LL, testfromclass) {
        return (t_testfromclass(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_tester::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x564D4529AE55BB92LL, testfromclass) {
        return (t_testfromclass(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_tester::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_tester$os_get(const char *s) {
  return c_tester::os_get(s, -1);
}
Variant &cw_tester$os_lval(const char *s) {
  return c_tester::os_lval(s, -1);
}
Variant cw_tester$os_constant(const char *s) {
  return c_tester::os_constant(s);
}
Variant cw_tester$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_tester::os_invoke(c, s, params, -1, fatal);
}
void c_tester::init() {
  m_a = 1LL;
  m_b = 2LL;
  m_c = 3LL;
}
/* SRC: roadsend/tests/overloading.php line 7 */
bool c_tester::t___isset(Variant v_a) {
  INSTANCE_METHOD_INJECTION(tester, tester::__isset);
  echo(LINE(8,concat3("__isset called for [", toString(v_a), "]\n")));
  return toBoolean(null);
} /* function */
/* SRC: roadsend/tests/overloading.php line 10 */
Variant c_tester::t___unset(Variant v_a) {
  INSTANCE_METHOD_INJECTION(tester, tester::__unset);
  echo(LINE(11,concat3("__unset called for [", toString(v_a), "]\n")));
  return null;
} /* function */
/* SRC: roadsend/tests/overloading.php line 13 */
void c_tester::t_testfromclass() {
  INSTANCE_METHOD_INJECTION(tester, tester::testFromClass);
  echo(LINE(14,concat3("ic1: ", toString(t___isset("a")), "\n")));
  echo(LINE(15,concat3("ic2: ", toString(t___isset("b")), "\n")));
  echo(LINE(16,concat3("ic3: ", toString(t___isset("c")), "\n")));
  echo(LINE(17,concat3("ic4: ", toString(t___isset("notHere")), "\n")));
} /* function */
/* SRC: roadsend/tests/overloading.php line 38 */
Variant c_setter::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_setter::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_setter::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("n", m_n.isReferenced() ? ref(m_n) : m_n));
  props.push_back(NEW(ArrayElement)("x", m_x));
  c_ObjectData::o_get(props);
}
bool c_setter::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x117B8667E4662809LL, n, 1);
      break;
    case 2:
      HASH_EXISTS_STRING(0x04BFC205E59FA416LL, x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_setter::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x04BFC205E59FA416LL, m_x,
                         x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_setter::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x117B8667E4662809LL, m_n,
                      n, 1);
      break;
    case 2:
      HASH_SET_STRING(0x04BFC205E59FA416LL, m_x,
                      x, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_setter::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x117B8667E4662809LL, m_n,
                         n, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_setter::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(setter)
ObjectData *c_setter::cloneImpl() {
  c_setter *obj = NEW(c_setter)();
  cloneSet(obj);
  return obj;
}
void c_setter::cloneSet(c_setter *clone) {
  clone->m_n = m_n.isReferenced() ? ref(m_n) : m_n;
  clone->m_x = m_x;
  ObjectData::cloneSet(clone);
}
Variant c_setter::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_setter::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_setter::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_setter$os_get(const char *s) {
  return c_setter::os_get(s, -1);
}
Variant &cw_setter$os_lval(const char *s) {
  return c_setter::os_lval(s, -1);
}
Variant cw_setter$os_constant(const char *s) {
  return c_setter::os_constant(s);
}
Variant cw_setter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_setter::os_invoke(c, s, params, -1, fatal);
}
void c_setter::init() {
  m_n = null;
  m_x = ScalarArrays::sa_[0];
}
/* SRC: roadsend/tests/overloading.php line 43 */
Variant c_setter::t___get(Variant v_nm) {
  INSTANCE_METHOD_INJECTION(Setter, Setter::__get);
  Variant v_r;

  echo(LINE(45,concat3("Getting [", toString(v_nm), "]\n")));
  if (isset(m_x, v_nm)) {
    (v_r = m_x.rvalAt(v_nm));
    print(LINE(49,concat3("Returning: ", toString(v_r), "\n")));
    return v_r;
  }
  else {
    echo("Nothing!\n");
  }
  return null;
} /* function */
/* SRC: roadsend/tests/overloading.php line 43 */
Variant &c_setter::___lval(Variant v_nm) {
  INSTANCE_METHOD_INJECTION(Setter, Setter::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_nm);
  return v;
} /* function */
/* SRC: roadsend/tests/overloading.php line 56 */
Variant c_setter::t___set(Variant v_nm, Variant v_val) {
  INSTANCE_METHOD_INJECTION(Setter, Setter::__set);
  echo(LINE(58,concat5("Setting [", toString(v_nm), "] to ", toString(v_val), "\n")));
  if (isset(m_x, v_nm)) {
    m_x.set(v_nm, (v_val));
    echo("OK!\n");
  }
  else {
    echo("Not OK!\n");
  }
  return null;
} /* function */
/* SRC: roadsend/tests/overloading.php line 68 */
bool c_setter::t___isset(Variant v_nm) {
  INSTANCE_METHOD_INJECTION(Setter, Setter::__isset);
  echo(LINE(70,concat3("Checking if ", toString(v_nm), " is set\n")));
  return isset(m_x, v_nm);
} /* function */
/* SRC: roadsend/tests/overloading.php line 75 */
Variant c_setter::t___unset(Variant v_nm) {
  INSTANCE_METHOD_INJECTION(Setter, Setter::__unset);
  DECLARE_GLOBAL_VARIABLES(g);
  echo(LINE(77,concat3("Unsetting ", toString(v_nm), "\n")));
  m_x.weakRemove(v_nm);
  return null;
} /* function */
Object co_tester(CArrRef params, bool init /* = true */) {
  return Object(p_tester(NEW(c_tester)())->dynCreate(params, init));
}
Object co_setter(CArrRef params, bool init /* = true */) {
  return Object(p_setter(NEW(c_setter)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$overloading_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/overloading.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$overloading_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_a = ((Object)(LINE(21,p_tester(p_tester(NEWOBJ(c_tester)())->create())))));
  echo(LINE(22,concat3("i1: ", toString(isset(v_a)), "\n")));
  echo(LINE(23,concat3("i2: ", toString(toObject(v_a)->t___isset("a")), "\n")));
  echo(LINE(24,concat3("i3: ", toString(toObject(v_a)->t___isset("b")), "\n")));
  echo(LINE(25,concat3("i4: ", toString(toObject(v_a)->t___isset("c")), "\n")));
  echo(LINE(26,concat3("i5: ", toString(toObject(v_a)->t___isset("notThere")), "\n")));
  LINE(27,v_a.o_invoke_few_args("testFromClass", 0x564D4529AE55BB92LL, 0));
  echo("u1: ");
  toObject(v_a)->t___unset("a");
  echo("\n");
  echo("u2: ");
  toObject(v_a)->t___unset("b");
  echo("\n");
  echo("u3: ");
  toObject(v_a)->t___unset("c");
  echo("\n");
  (v_foo = ((Object)(LINE(83,p_setter(p_setter(NEWOBJ(c_setter)())->create())))));
  (v_foo.o_lval("n", 0x117B8667E4662809LL) = 1LL);
  (v_foo.o_lval("a", 0x4292CEE227B9150ALL) = 100LL);
  lval(v_foo.o_lval("a", 0x4292CEE227B9150ALL))++;
  lval(v_foo.o_lval("z", 0x62A103F6518DE2B3LL))++;
  LINE(89,x_var_dump(1, toObject(v_foo)->t___isset("a")));
  toObject(v_foo)->t___unset("a");
  LINE(91,x_var_dump(1, toObject(v_foo)->t___isset("a")));
  LINE(95,x_var_dump(1, toObject(v_foo)->t___isset("n")));
  LINE(97,x_var_dump(1, v_foo));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
