
#ifndef __GENERATED_cls_tester_h__
#define __GENERATED_cls_tester_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/overloading.php line 3 */
class c_tester : virtual public ObjectData {
  BEGIN_CLASS_MAP(tester)
  END_CLASS_MAP(tester)
  DECLARE_CLASS(tester, tester, ObjectData)
  void init();
  public: Variant m_a;
  public: Variant m_b;
  public: Variant m_c;
  public: bool t___isset(Variant v_a);
  public: Variant t___unset(Variant v_a);
  public: void t_testfromclass();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_tester_h__
