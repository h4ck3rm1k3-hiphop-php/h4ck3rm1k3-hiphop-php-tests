
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_startelement(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_unparsed(CArrRef params);
Variant i_default_h(CArrRef params);
static Variant invoke_case_4(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x33E8B48847232754LL, unparsed);
  HASH_INVOKE(0x3EA773BF0BE5B274LL, default_h);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_cdata(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_ext_ent(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_notation(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_pi_h(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_endelement(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[16])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 16; i++) funcTable[i] = &invoke_builtin;
    funcTable[2] = &i_startelement;
    funcTable[4] = &invoke_case_4;
    funcTable[6] = &i_cdata;
    funcTable[8] = &i_ext_ent;
    funcTable[9] = &i_notation;
    funcTable[10] = &i_pi_h;
    funcTable[15] = &i_endelement;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 15](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
