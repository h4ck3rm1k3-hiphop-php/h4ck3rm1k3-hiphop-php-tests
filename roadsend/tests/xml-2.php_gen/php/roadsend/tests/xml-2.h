
#ifndef __GENERATED_php_roadsend_tests_xml_2_h__
#define __GENERATED_php_roadsend_tests_xml_2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/xml-2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_notation(CVarRef v_parser, CVarRef v_name, CVarRef v_base, CVarRef v_sysid, CVarRef v_pubid);
void f_pi_h(CVarRef v_parser, CVarRef v_target, CVarRef v_data);
void f_cdata(CVarRef v_parser, CVarRef v_data);
Variant pm_php$roadsend$tests$xml_2_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_ext_ent(CVarRef v_parser, CVarRef v_names, CVarRef v_base, CVarRef v_sysid, CVarRef v_pubid);
void f_unparsed(CVarRef v_parser, CVarRef v_name, CVarRef v_base, CVarRef v_sysid, CVarRef v_pubid, CVarRef v_notation);
void f_default_h(CVarRef v_parser, CVarRef v_data);
void f_endelement(CVarRef v_parser, CVarRef v_name);
void f_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_xml_2_h__
