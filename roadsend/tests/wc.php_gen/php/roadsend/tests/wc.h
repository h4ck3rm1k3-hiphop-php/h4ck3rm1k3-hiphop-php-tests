
#ifndef __GENERATED_php_roadsend_tests_wc_h__
#define __GENERATED_php_roadsend_tests_wc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/wc.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_wc();
Variant pm_php$roadsend$tests$wc_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_wc_h__
