
#include <php/roadsend/tests/wc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/wc.php line 11 */
void f_wc() {
  FUNCTION_INJECTION(wc);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  String v_datafile;
  Variant v_fd;
  int64 v_nc = 0;
  int64 v_nw = 0;
  int64 v_nl = 0;
  Variant v_line;

  (v_datafile = concat(toString(g->gv__ENV.rvalAt("PCC_HOME", 0x4F7B58EF1F3DF563LL)), "/benchmarks/data/spellcheck"));
  if (!(LINE(14,x_file_exists(v_datafile)))) {
    LINE(15,(assignCallTemp(eo_0, concat3("Couldn't find datafile ", v_datafile, ".")),x_trigger_error(eo_0, toInt32(256LL) /* E_USER_ERROR */)));
  }
  (v_fd = LINE(18,x_fopen(v_datafile, "r")));
  (v_nl = (v_nw = (v_nc = 0LL)));
  LOOP_COUNTER(1);
  {
    while (!(LINE(20,x_feof(toObject(v_fd))))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (toBoolean((v_line = LINE(21,x_fgets(toObject(v_fd), 10000LL))))) {
          ++v_nl;
          v_nc += toInt64(LINE(23,x_strlen(toString(v_line))));
          v_nw += toInt64(LINE(24,x_count(x_preg_split("/\\s+/", v_line, toInt32(-1LL), toInt32(1LL) /* PREG_SPLIT_NO_EMPTY */))));
        }
      }
    }
  }
  LINE(27,x_fclose(toObject(v_fd)));
  print(LINE(28,concat6(toString(v_nl), " ", toString(v_nw), " ", toString(v_nc), "\n")));
} /* function */
Variant pm_php$roadsend$tests$wc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/wc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$wc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(9,f_wc());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
