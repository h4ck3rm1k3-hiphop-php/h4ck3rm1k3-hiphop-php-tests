
#ifndef __GENERATED_php_roadsend_tests_shared_environment_h__
#define __GENERATED_php_roadsend_tests_shared_environment_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/shared-environment.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foo();
Variant pm_php$roadsend$tests$shared_environment_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_shared_environment_h__
