
#include <php/roadsend/tests/zerrors2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/zerrors2.php line 7 */
void f_usererrorhandler(CVarRef v_errno, CVarRef v_errmsg, CVarRef v_filename, CVarRef v_linenum, CVarRef v_vars) {
  FUNCTION_INJECTION(userErrorHandler);
  String v_dt;
  Array v_errortype;
  String v_err;

  (v_dt = LINE(9,x_date("Y-m-d H (T)")));
  (v_errortype = ScalarArrays::sa_[0]);
  ;
  (v_err = concat("<errorentry>\n", LINE(31,concat3("\t<datetime>", v_dt, "</datetime>\n"))));
  concat_assign(v_err, LINE(32,concat3("\t<errornum>", toString(v_errno), "</errornum>\n")));
  concat_assign(v_err, LINE(33,concat3("\t<errortype>", toString(v_errortype.rvalAt(v_errno)), "</errortype>\n")));
  concat_assign(v_err, LINE(34,concat3("\t<errormsg>", toString(v_errmsg), "</errormsg>\n")));
  concat_assign(v_err, "</errorentry>\n\n");
  echo(v_err);
} /* function */
/* SRC: roadsend/tests/zerrors2.php line 54 */
Variant f_distance(CVarRef v_vect1, CVarRef v_vect2) {
  FUNCTION_INJECTION(distance);
  Variant eo_0;
  Variant eo_1;
  int64 v_i = 0;
  Variant v_c1;
  Variant v_c2;
  Numeric v_d = 0;

  if (!(LINE(55,x_is_array(v_vect1))) || !(x_is_array(v_vect2))) {
    LINE(56,x_trigger_error("Incorrect parameters, arrays expected", toInt32(256LL) /* E_USER_ERROR */));
    return null;
  }
  if (!equal_rev(LINE(60,x_count(v_vect2)), x_count(v_vect1))) {
    LINE(61,x_trigger_error("Vectors need to be of the same size", toInt32(256LL) /* E_USER_ERROR */));
    return null;
  }
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(65,x_count(v_vect1))); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_c1 = v_vect1.rvalAt(v_i));
        (v_c2 = v_vect2.rvalAt(v_i));
        (v_d = 0.0);
        if (!(LINE(68,x_is_numeric(v_c1)))) {
          LINE(70,(assignCallTemp(eo_0, LINE(69,concat3("Coordinate ", toString(v_i), " in vector 1 is not a number, using zero"))),x_trigger_error(eo_0, toInt32(512LL) /* E_USER_WARNING */)));
          (v_c1 = 0.0);
        }
        if (!(LINE(73,x_is_numeric(v_c2)))) {
          LINE(75,(assignCallTemp(eo_0, LINE(74,concat3("Coordinate ", toString(v_i), " in vector 2 is not a number, using zero"))),x_trigger_error(eo_0, toInt32(512LL) /* E_USER_WARNING */)));
          (v_c2 = 0.0);
        }
        v_d += v_c2 * v_c2 - v_c1 * v_c1;
      }
    }
  }
  return LINE(80,x_sqrt(toDouble(v_d)));
} /* function */
Variant i_usererrorhandler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2A27F5AE80B53C48LL, usererrorhandler) {
    return (f_usererrorhandler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$zerrors2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zerrors2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zerrors2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_old_error_handler __attribute__((__unused__)) = (variables != gVariables) ? variables->get("old_error_handler") : g->GV(old_error_handler);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_t1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t1") : g->GV(t1);
  Variant &v_t2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t2") : g->GV(t2);
  Variant &v_t3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t3") : g->GV(t3);

  LINE(4,x_error_reporting(toInt32(0LL)));
  (v_old_error_handler = LINE(83,x_set_error_handler("userErrorHandler")));
  (v_a = ScalarArrays::sa_[2]);
  (v_b = ScalarArrays::sa_[3]);
  (v_c = ScalarArrays::sa_[4]);
  (v_t1 = concat(toString(LINE(94,f_distance(v_c, v_b))), "\n"));
  (v_t2 = concat(toString(LINE(97,f_distance(v_b, "i am not an array"))), "\n"));
  (v_t3 = concat(toString(LINE(100,f_distance(v_a, v_b))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
