
#ifndef __GENERATED_php_roadsend_tests_zerrors2_h__
#define __GENERATED_php_roadsend_tests_zerrors2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zerrors2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_usererrorhandler(CVarRef v_errno, CVarRef v_errmsg, CVarRef v_filename, CVarRef v_linenum, CVarRef v_vars);
Variant f_distance(CVarRef v_vect1, CVarRef v_vect2);
Variant pm_php$roadsend$tests$zerrors2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zerrors2_h__
