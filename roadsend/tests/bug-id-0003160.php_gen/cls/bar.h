
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__

#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0003160.php line 12 */
class c_bar : virtual public c_foo {
  BEGIN_CLASS_MAP(bar)
    PARENT_CLASS(foo)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, bar, foo)
  void init();
  public: void t_raiseerror();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
