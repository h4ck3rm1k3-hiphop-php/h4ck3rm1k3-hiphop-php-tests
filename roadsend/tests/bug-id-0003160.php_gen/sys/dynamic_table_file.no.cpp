
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$tests$bug_id_0003160_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_INCLUDE(0x762B3E893E5ABD94LL, "roadsend/tests/bug-id-0003160.php", php$roadsend$tests$bug_id_0003160_php);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$roadsend$tests$bug_id_0003160_php(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
