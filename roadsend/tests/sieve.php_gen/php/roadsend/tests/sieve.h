
#ifndef __GENERATED_php_roadsend_tests_sieve_h__
#define __GENERATED_php_roadsend_tests_sieve_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/sieve.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_sieve(Variant v_n);
Variant pm_php$roadsend$tests$sieve_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_sieve_h__
