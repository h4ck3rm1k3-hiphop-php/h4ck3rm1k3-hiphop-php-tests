
#ifndef __GENERATED_php_roadsend_tests_hash2_h__
#define __GENERATED_php_roadsend_tests_hash2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/hash2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$hash2_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_hash2(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_hash2_h__
