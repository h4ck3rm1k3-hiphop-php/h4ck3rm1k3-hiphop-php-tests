
#include <php/roadsend/tests/unset.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$unset_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/unset.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$unset_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  (v_a = ScalarArrays::sa_[0]);
  v_a.weakRemove(2LL, 0x486AFCC090D5F98CLL);
  LINE(11,x_print_r(v_a));
  (v_a = 5LL);
  (v_b = ScalarArrays::sa_[1]);
  (v_c = "string");
  echo(LINE(14,concat3("i1: ", toString((isset(v_a) && isset(v_b) && isset(v_c))), "\n")));
  echo(LINE(15,concat3("i2: ", toString((isset(v_a) && isset(v_b) && isset(v_c) && isset(v_d))), "\n")));
  {
    unset(v_a);
    unset(v_b);
    unset(v_c);
  }
  echo(LINE(17,concat3("i3: ", toString((isset(v_a) && isset(v_b) && isset(v_c))), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
