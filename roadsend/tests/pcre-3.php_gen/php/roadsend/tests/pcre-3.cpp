
#include <php/roadsend/tests/pcre-3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$pcre_3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/pcre-3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$pcre_3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_subj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("subj") : g->GV(subj);
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_pattern __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pattern") : g->GV(pattern);
  Variant &v_replacement __attribute__((__unused__)) = (variables != gVariables) ? variables->get("replacement") : g->GV(replacement);
  Variant &v_patterns __attribute__((__unused__)) = (variables != gVariables) ? variables->get("patterns") : g->GV(patterns);
  Variant &v_replacements __attribute__((__unused__)) = (variables != gVariables) ? variables->get("replacements") : g->GV(replacements);
  Variant &v_replace __attribute__((__unused__)) = (variables != gVariables) ? variables->get("replace") : g->GV(replace);
  Variant &v_html_body __attribute__((__unused__)) = (variables != gVariables) ? variables->get("html_body") : g->GV(html_body);
  Variant &v_docText __attribute__((__unused__)) = (variables != gVariables) ? variables->get("docText") : g->GV(docText);
  Variant &v_SELF __attribute__((__unused__)) = (variables != gVariables) ? variables->get("SELF") : g->GV(SELF);
  Variant &v_sVars __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sVars") : g->GV(sVars);
  Variant &v_text __attribute__((__unused__)) = (variables != gVariables) ? variables->get("text") : g->GV(text);
  Variant &v_newtext __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newtext") : g->GV(newtext);

  echo(toString(LINE(4,x_preg_replace("/sandwhich/", "burger", "Would you like fries with your sandwhich, and if so, which sandwhich\?\n"))));
  (v_subj = "9 - x - 22 - dd9dd\n");
  echo(toString("orig: ") + toString(v_subj));
  echo(concat("new: ", toString(LINE(8,x_preg_replace("!\\d\\d!", "num", v_subj)))));
  echo(toString(LINE(11,x_preg_replace("/(foo)\\s(bar)\\s\\s(baz)/", "$3 $2 $1", "foo bar  baz"))));
  echo("\n1\n");
  echo(toString(LINE(13,x_preg_replace("/(foo)\\s(bar)\\s\\s(baz)/", "\\3 \\2 \\1", "foo bar  baz"))));
  echo("\n2\n");
  echo(toString(LINE(15,x_preg_replace("/(foo)\\s(bar)\\s\\s(baz)/", "$3$2$1", "foo bar  baz"))));
  echo("\n3\n");
  echo(toString(LINE(17,x_preg_replace("/(foo)\\s(bar)\\s\\s(baz)/", "$3 $2 $1", "foo bar  baz"))));
  echo("\n4\n");
  echo(toString(LINE(19,x_preg_replace("/(foo)\\s(bar)\\s\\s(baz)/", "\\3${2} ${1} \\\\3 \\\\12 hi", "foo bar  baz"))));
  echo("\n5\n");
  echo(toString(LINE(23,x_preg_replace("/boop/", "teet", "boop boop boop teet teet boop\n", toInt32(3LL)))));
  LINE(26,x_print_r(x_preg_replace("/boop/", "test", ScalarArrays::sa_[0])));
  print(toString(LINE(29,x_preg_replace(ScalarArrays::sa_[1], ScalarArrays::sa_[2], "baz baz meep moop"))));
  print(toString(LINE(32,x_preg_replace(ScalarArrays::sa_[3], "zip", "bar foo bar foo <-- all zips!"))));
  (v_string = "April 15, 2003");
  (v_pattern = "/(\\w+) (\\d+), (\\d+)/i");
  (v_replacement = "${1}1,$3");
  print(toString(LINE(39,x_preg_replace(v_pattern, v_replacement, v_string))));
  (v_string = "The quick brown fox jumped over the lazy dog.");
  v_patterns.set(0LL, ("/quick/"), 0x77CFA1EEF01BCA90LL);
  v_patterns.set(1LL, ("/brown/"), 0x5BCA7C69B794F8CELL);
  v_patterns.set(2LL, ("/fox/"), 0x486AFCC090D5F98CLL);
  v_replacements.set(2LL, ("bear"), 0x486AFCC090D5F98CLL);
  v_replacements.set(1LL, ("black"), 0x5BCA7C69B794F8CELL);
  v_replacements.set(0LL, ("slow"), 0x77CFA1EEF01BCA90LL);
  print(toString(LINE(51,x_preg_replace(v_patterns, v_replacements, v_string))));
  (v_patterns = ScalarArrays::sa_[4]);
  (v_replace = ScalarArrays::sa_[5]);
  print(toString(LINE(62,x_preg_replace(v_patterns, v_replace, "{startDate} = 1999-5-27"))));
  (v_html_body = "<html><title>this is a title, using eval</title>");
  echo(toString(LINE(69,x_preg_replace("/(<\\/\?)(\\w+)([^>]*>)/e", "'\\1'.strtoupper('\\2').'\\3'", v_html_body))));
  (v_docText = toString("<a href=\"introduction.html\">1. Introduction</a></span></dt><dd><dl><dt><span class=\"section\">\n<a href=\"introduction.html#id2524679\">What Is PHP\?</a></span></dt><dt><span class=\"section\">\n<a href=\"ch01s02.html\">What Is The Roadsend PHP Compiler\?</a></span></dt><dd><dl><dt>\n<span class=\"section\"><a href=\"ch01s02.html#id2539836\">What can The Roadsend PHP Compiler do\?</a></span></dt>\n\n</dl></dd><dt><span class=\"section\"><a href=\"ch01s03.html\">How It Works</a></span></dt><dt><span class=\"section\"><a href=\"ch01s04.html\">The Future of The Roadsend PHP Compiler</a></span></dt><dt><span class=\"section\"><a href=\"ch01s05.html\">Terms Used In This Manual</a></span></dt></dl></dd><dt><span class=\"chapter\"><a href=\"installation.html\">2. Installation and Configuration</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"installation.html#id2525662\">Installation and Development Environment</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"installation.html#id2534091\">Requirements</a></span></dt><dt><span class=\"section\"><a href=\"installation.html#id2560409\">Installing The Roadsend PHP Compiler</a></span></dt><dt><span class=\"section\"><a href=\"installation.html#id2523087\">Installing Apache Module</a></span></dt><dt><span class=\"section\"><a href=\"installation.html#id2523156\">Installing A License File</a></span></dt></dl></dd><dt><span class=\"section\"><a href=\"config-file.html\">Configuration</a></span></dt><dt><span class=\"section\"><a href=\"ch02s03.html\">Uninstalling</a></span></dt></dl></dd><dt><span class=\"chapter\"><a href=\"use.html\">3. Use</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"use.html#id2520221\">Overview\n  </a></span></dt><dt><span class=\"section\"><a href=\"ch03s02.html\">Interpreting vs. Compiling</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"ch03s02.html#id2511896\">Mixed Interpreting/Compiling</a></span></dt></dl></dd><dt><span class=\"section\"><a href=\"ch03s03.html\">Compiling Stand Alone Applications</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"ch03s03.html#id2522092\">Multiple Source Files</a></span></dt><dt><span class=\"section\"><a href=\"ch03s03.html#compile-with-libs\">Compiling With a Library</a></span></dt><dt><span class=\"section\"><a href=\"ch03s03.html#extensions\">Precompiled Extensions</a></span></dt></dl></dd><dt><span class=\"section\"><a href=\"ch03s04.html\">Compiling Libraries</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"ch03s04.html#installing-libs\">Installing Libraries</a></span></dt><dt><span class=\"section\"><a href=\"ch03s04.html#lib-cline\">Library Command Line Options</a></span></dt></dl></dd><dt><span class=\"section\"><a href=\"webapps.html\">Web Applications</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"webapps.html#id2510099\">Compiled Web Applications</a></span></dt><dt><span class=\"section\"><a href=\"webapps.html#id2510243\">Interpreted Web Applications</a></span></dt></dl></dd><dt><span class=\"section\"><a href=\"ch03s06.html\">Differences From Zend PHP</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"ch03s06.html#handling-includes\">Handling Include Files</a></span></dt><dt><span class=\"section\"><a href=\"ch03s06.html#id2523421\">Semantic Differences</a></span></dt><dt><span class=\"section\"><a href=\"ch03s06.html#id2523466\">Available Extensions</a></span></dt></dl></dd><dt><span class=\"section\"><a href=\"ch03s07.html\">Writing Portable PHP Code</a></span></dt><dt><span class=\"section\"><a href=\"ch03s08.html\">Optimization</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"ch03s08.html#id2523574\">Shared vs Static</a></span></dt></dl></dd></dl></dd><dt><span class=\"chapter\"><a href=\"reference.html\">4. Reference</a></span></dt><dd><dl><dt><span class=\"section\"><a href=\"reference.html#id2561585\">Command Line Options</a></span></dt><dt><span class=\"section\"><a href=\"ch04s02.html\">\n    Environment Variables\n  </a></span></dt><dt><span class=\"section\"><a href=\"ch04s03.html\">Known Issues</a></span></dt></dl></dd><dt><span class=\"chapter\"><a href=\"faq.html\">5. Frequently Asked Questions</a></span></dt></dl></div><div class=\"list-of-figures\"><p><b>List of Figures</b></p><dl><dt>3.1. <a href=\"ch03s02.html#id2511877\">Interpreting vs. Compiling</a></dt><dt>3.2. <a href=\"ch03s02.html#id2511909\">Interpreting Include Files at Runtime</a></dt><dt>3.3. <a href=\"ch03s02.html#id2511939\">Running Compiled Code From An Interpreted Web page</a></dt><dt>3.4. <a href=\"ch03s03.html#id2522115\">Multiple Source Files</a></dt><dt>3.5. <a href=\"ch03s04.html#id2546704\">Using Compiled Libraries</a></dt></dl></div><div class=\"list-of-tables\"><p><b>List of Tables</b></p><dl><dt>2.1. <a href=\"installation.html#id2516418\">Supported Platforms</a></dt><dt>2.2. <a href=\"installation.html#id2560425\">Packages</a></dt><dt>2.3. <a href=\"installation.html#id2445512\">Selecting which packages to install</a></dt><dt>2.4. <a href=\"installation.html#id2445577\">Self Installer Packages</a></dt><dt>2.5. <a href=\"installation.html#id2445643\">Debian Packages</a></dt><dt>2.6. <a href=\"installation.html#id2523031\">RPM Packages</a></dt><dt>2.7. <a href=\"config-file.html#id2544986\">Configuration Directives</a></dt><dt>2.8. <a href=\"ch02s03.html#id2514426\">Uninstallation Scripts</a></dt><dt>4.1. <a href=\"ch04s02.html#id2516676\">Environment Variables</a></dt></dl></div><div class=\"list-of-examples\"><p><b>List of Examples</b></p><dl><dt>3.1. <a href=\"ch03s03.html#id2522058\">Single Source File (first.php)</a></dt><dt>3.2. <a href=\"ch03s03.html#id2522137\">Multiple Source Files</a></dt><dt>3.3. <a href=\"ch03s06.html#id2523436\">Declaring a Pass-By-Reference Parameter</a></dt></dl></div></div><div class=\"navfooter\"><hr /><table width=\"100%\" summary=\"Navigation footer\"><tr><td width=\"40%\" align=\"left\">\\x{FFFF}</td><td width=\"20%\" align=\"center\">\\x{FFFF}</td><td width=\"40%\" align=\"right\">\\x{FFFF}<a accesskey=\"n\" href=\"introduction.html\">Next</a></td></tr><tr><td width=\"40%\" align=\"left\" valign=\"top\">\\x{FFFF}</td><td width=\"20%\" align=\"center\">\\x{FFFF}</td><td width=\"40%\" align=\"right\" valign=\"top\">\\x{FFFF}Chapter\\x{FFFF}1.\\x{FFFF}Introduction</td></tr></table></div></body></html>"));
  (v_SELF = "somefile.php");
  (v_sVars = "sID=12345");
  (v_docText = LINE(89,(assignCallTemp(eo_1, concat5("HREF=\"", toString(v_SELF), "\?", toString(v_sVars), "&doc=\\1\"")),assignCallTemp(eo_2, v_docText),x_preg_replace("/HREF=\"([^http].+)\"/miU", eo_1, eo_2))));
  LINE(90,x_var_dump(1, v_docText));
  (v_text = "---123445567890---");
  (v_newtext = LINE(94,x_preg_replace("/([45])/", "\\1-", v_text)));
  echo(LINE(95,concat3("\n", toString(v_newtext), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
