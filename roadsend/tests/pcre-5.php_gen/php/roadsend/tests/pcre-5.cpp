
#include <php/roadsend/tests/pcre-5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$pcre_5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/pcre-5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$pcre_5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_keywords __attribute__((__unused__)) = (variables != gVariables) ? variables->get("keywords") : g->GV(keywords);
  Variant &v_textbody __attribute__((__unused__)) = (variables != gVariables) ? variables->get("textbody") : g->GV(textbody);
  Variant &v_word __attribute__((__unused__)) = (variables != gVariables) ? variables->get("word") : g->GV(word);

  (v_keywords = "$40 for a g3/400");
  (v_keywords = LINE(7,x_preg_quote(toString(v_keywords), "/")));
  echo(toString(v_keywords));
  (v_textbody = "This book is *very* difficult to find.");
  (v_word = "*very*");
  (v_textbody = LINE(18,(assignCallTemp(eo_0, LINE(16,(assignCallTemp(eo_4, x_preg_quote(toString(v_word))),concat3("/", eo_4, "/")))),assignCallTemp(eo_1, LINE(17,concat3("<i>", toString(v_word), "</i>"))),assignCallTemp(eo_2, v_textbody),x_preg_replace(eo_0, eo_1, eo_2))));
  echo(toString(v_textbody));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
