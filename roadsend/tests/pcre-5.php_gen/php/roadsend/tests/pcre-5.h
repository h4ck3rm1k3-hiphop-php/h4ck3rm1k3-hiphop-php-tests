
#ifndef __GENERATED_php_roadsend_tests_pcre_5_h__
#define __GENERATED_php_roadsend_tests_pcre_5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/pcre-5.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$pcre_5_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_pcre_5_h__
