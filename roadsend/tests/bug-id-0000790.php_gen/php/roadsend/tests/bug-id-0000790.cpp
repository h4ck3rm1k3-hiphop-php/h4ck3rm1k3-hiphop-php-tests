
#include <php/roadsend/tests/bug-id-0000790.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000790_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000790.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000790_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_noise __attribute__((__unused__)) = (variables != gVariables) ? variables->get("noise") : g->GV(noise);
  Variant &v_pageOutput __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pageOutput") : g->GV(pageOutput);

  echo("0000790 support for here docs\n\n");
  (v_pageOutput = LINE(12,concat3("\nfoohi this is a 'here doc'\ncan you put all kinds of \n ", toString(v_noise), " in \" it\?\n")));
  echo(toString(v_pageOutput));
  echo("\n\n\n\tparse error:\n");
  echo(toString("<style type=\"text/css\">\n<!--\n/* avoid stupid IE6 bug with frames and scrollbars */\nbody {\nvoice-family: \"\\\"}\\\"\";\nvoice-family: inherit;\nwidth: expression(document.documentElement.clientWidth - 30);\n}\n-->\n</style>\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
