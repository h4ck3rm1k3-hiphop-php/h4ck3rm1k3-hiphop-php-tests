
#ifndef __GENERATED_cls_otherflag_h__
#define __GENERATED_cls_otherflag_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/equality.php line 29 */
class c_otherflag : virtual public ObjectData {
  BEGIN_CLASS_MAP(otherflag)
  END_CLASS_MAP(otherflag)
  DECLARE_CLASS(otherflag, OtherFlag, ObjectData)
  void init();
  public: Variant m_flag;
  public: void t_otherflag(bool v_flag = true);
  public: ObjectData *create(bool v_flag = true);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_otherflag_h__
