
#ifndef __GENERATED_php_roadsend_tests_equality_h__
#define __GENERATED_php_roadsend_tests_equality_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/equality.fw.h>

// Declarations
#include <cls/flag.h>
#include <cls/otherflag.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_compareobjects(Variant v_o1, Variant v_o2);
Variant pm_php$roadsend$tests$equality_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_bool2str(bool v_bool);
Object co_flag(CArrRef params, bool init = true);
Object co_otherflag(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_equality_h__
