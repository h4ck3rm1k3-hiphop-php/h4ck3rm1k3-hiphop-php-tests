
#ifndef __GENERATED_php_roadsend_tests_finc15_h__
#define __GENERATED_php_roadsend_tests_finc15_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/finc15.fw.h>

// Declarations
#include <cls/sm_smtag_areao.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$finc15_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_areao(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_finc15_h__
