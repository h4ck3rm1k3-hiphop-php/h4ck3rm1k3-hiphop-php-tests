
#ifndef __GENERATED_cls_sm_smtag_areao_h__
#define __GENERATED_cls_sm_smtag_areao_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/finc15.php line 23 */
class c_sm_smtag_areao : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_areao)
  END_CLASS_MAP(sm_smtag_areao)
  DECLARE_CLASS(sm_smtag_areao, SM_smTag_AREAo, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_areao_h__
