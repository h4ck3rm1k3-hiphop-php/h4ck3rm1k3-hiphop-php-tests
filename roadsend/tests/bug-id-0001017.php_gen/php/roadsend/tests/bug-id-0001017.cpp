
#include <php/roadsend/tests/bug-id-0001017.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001017.php line 5 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x032F080E7D9E11FDLL, amethod) {
        return (ti_amethod(o_getClassName()));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x032F080E7D9E11FDLL, amethod) {
        return (ti_amethod(o_getClassName()));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x032F080E7D9E11FDLL, amethod) {
        return (ti_amethod(c));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
}
/* SRC: roadsend/tests/bug-id-0001017.php line 6 */
Variant c_aclass::ti_amethod(const char* cls) {
  STATIC_METHOD_INJECTION(aclass, aclass::amethod);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_foo __attribute__((__unused__)) = g->sv_aclass_DupIdamethod_DupIdfoo.lvalAt(cls);
  Variant &inited_sv_foo __attribute__((__unused__)) = g->inited_sv_aclass_DupIdamethod_DupIdfoo.lvalAt(cls);
  if (!inited_sv_foo) {
    (sv_foo = null);
    inited_sv_foo = true;
  }
  return sv_foo++;
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0001017_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001017.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001017_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_anobj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("anobj") : g->GV(anobj);

  echo("0001017\tneed static variables in class methods in interpreter\n\n");
  print(concat(toString(LINE(13,c_aclass::t_amethod())), "\n"));
  print(concat(toString(LINE(14,c_aclass::t_amethod())), "\n"));
  print(concat(toString(LINE(15,c_aclass::t_amethod())), "\n"));
  (v_anobj = ((Object)(LINE(17,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  print(concat(toString(LINE(19,v_anobj.o_invoke_few_args("amethod", 0x032F080E7D9E11FDLL, 0))), "\n"));
  print(concat(toString(LINE(20,v_anobj.o_invoke_few_args("amethod", 0x032F080E7D9E11FDLL, 0))), "\n"));
  print(concat(toString(LINE(21,v_anobj.o_invoke_few_args("amethod", 0x032F080E7D9E11FDLL, 0))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
