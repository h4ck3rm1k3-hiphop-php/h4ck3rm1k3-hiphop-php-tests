
#include <php/roadsend/tests/query.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$query_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/query.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$query_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);

  LINE(3,require("s_common.inc", false, variables, "roadsend/tests/"));
  (v_db = LINE(5,invoke_failed("makedb", Array(), 0x000000009D69F93DLL)));
  (v_r = LINE(7,invoke_failed("sqlite_fetch_column_types", Array(ArrayInit(2).set(0, "mytable").set(1, ref(v_db)).create()), 0x00000000EDE186B6LL)));
  LINE(8,x_var_dump(1, v_r));
  (v_rh = LINE(10,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT * FROM mytable").create()), 0x00000000ADED2A6ALL)));
  echo(LINE(11,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("result resource\? ", eo_1, "\n"))));
  if (toBoolean(v_rh)) {
    echo(LINE(15,(assignCallTemp(eo_1, toString(invoke_failed("sqlite_num_rows", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000F022E9C8LL))),concat3("num rows: ", eo_1, "\n"))));
    echo("fetching results\n");
    LOOP_COUNTER(1);
    {
      while (toBoolean((v_result = LINE(19,invoke_failed("sqlite_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x000000002867E087LL))))) {
        LOOP_COUNTER_CHECK(1);
        {
          LINE(20,x_var_dump(1, v_result));
        }
      }
    }
    LINE(34,invoke_failed("sqlite_rewind", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000576B4F50LL));
    LOOP_COUNTER(2);
    {
      while (toBoolean(LINE(35,invoke_failed("sqlite_next", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x000000009EB6E362LL)))) {
        LOOP_COUNTER_CHECK(2);
        {
          (v_result = LINE(36,invoke_failed("sqlite_current", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000C82BD8C3LL)));
          LINE(37,x_var_dump(1, v_result));
          (v_b = LINE(38,invoke_failed("sqlite_has_more", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000BECBAE1ELL)));
          echo("has_more\?\n");
          (v_b = LINE(40,invoke_failed("sqlite_valid", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000021CBD4FLL)));
          echo("valid\?\n");
          LINE(42,x_var_dump(1, v_b));
        }
      }
    }
    LINE(49,invoke_failed("sqlite_seek", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 3LL).create()), 0x000000001255E5E6LL));
    (v_result = LINE(50,invoke_failed("sqlite_current", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000C82BD8C3LL)));
    LINE(51,x_var_dump(1, v_result));
    LINE(54,invoke_failed("sqlite_seek", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 100LL).create()), 0x000000001255E5E6LL));
    (v_result = LINE(55,invoke_failed("sqlite_current", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000C82BD8C3LL)));
    LINE(56,x_var_dump(1, v_result));
    LOOP_COUNTER(3);
    {
      while (toBoolean(LINE(59,invoke_failed("sqlite_prev", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000F4C3D33DLL)))) {
        LOOP_COUNTER_CHECK(3);
        {
          (v_result = LINE(60,invoke_failed("sqlite_current", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000C82BD8C3LL)));
          LINE(61,x_var_dump(1, v_result));
          (v_b = LINE(62,invoke_failed("sqlite_has_prev", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000405235C0LL)));
          echo("has_prev\?\n");
          LINE(64,x_var_dump(1, v_b));
        }
      }
    }
    LINE(72,invoke_failed("sqlite_seek", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 2LL).create()), 0x000000001255E5E6LL));
    {
      LOOP_COUNTER(4);
      for ((v_i = 0LL); less(v_i, LINE(73,invoke_failed("sqlite_num_fields", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000807B2163LL))); v_i++) {
        LOOP_COUNTER_CHECK(4);
        {
          (v_n = LINE(75,invoke_failed("sqlite_field_name", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, ref(v_i)).create()), 0x00000000B21B1902LL)));
          (v_c = LINE(77,invoke_failed("sqlite_column", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, ref(v_i)).create()), 0x00000000F001695DLL)));
          LINE(78,x_var_dump(1, v_c));
          (v_c = LINE(80,invoke_failed("sqlite_column", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, ref(v_n)).create()), 0x00000000F001695DLL)));
          LINE(81,x_var_dump(1, v_c));
        }
      }
    }
    (v_r = LINE(87,invoke_failed("sqlite_fetch_all", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000003737E3DLL)));
    LINE(88,x_var_dump(1, v_r));
    LINE(91,invoke_failed("sqlite_rewind", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000576B4F50LL));
    (v_r = LINE(92,invoke_failed("sqlite_fetch_string", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x000000003598FD36LL)));
    LINE(93,x_var_dump(1, v_r));
  }
  else {
    echo(concat("bad query: ", toString(LINE(97,invoke_failed("sqlite_error_string", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000B0060456LL)))));
  }
  (v_val = LINE(102,invoke_failed("sqlite_array_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT * FROM mytable").create()), 0x00000000013709F9LL)));
  LINE(103,x_var_dump(1, v_val));
  (v_val = LINE(106,invoke_failed("sqlite_single_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT * FROM mytable").create()), 0x000000006AD69FE3LL)));
  LINE(107,x_var_dump(1, v_val));
  (v_val = LINE(108,invoke_failed("sqlite_single_query", Array(ArrayInit(3).set(0, ref(v_db)).set(1, "SELECT * FROM mytable LIMIT 1").set(2, true).create()), 0x000000006AD69FE3LL)));
  LINE(109,x_var_dump(1, v_val));
  (v_val = LINE(110,invoke_failed("sqlite_single_query", Array(ArrayInit(3).set(0, ref(v_db)).set(1, "SELECT * FROM mytable LIMIT 1").set(2, false).create()), 0x000000006AD69FE3LL)));
  LINE(111,x_var_dump(1, v_val));
  (v_r = LINE(113,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
