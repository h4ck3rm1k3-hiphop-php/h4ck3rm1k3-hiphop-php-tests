
#include <php/roadsend/tests/bug-id-0001495.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001495_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001495.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001495_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("this inserts the 12 at key 1001, should be at key 2:\n\ncheck if looking up a big integer key bumps the maximum integer key\n\n");
  (v_foo = ScalarArrays::sa_[0]);
  print(toString(v_foo.rvalAt(1000LL, 0x46C0B8FC0D818733LL)));
  v_foo.append((12LL));
  LINE(13,x_print_r(v_foo));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
