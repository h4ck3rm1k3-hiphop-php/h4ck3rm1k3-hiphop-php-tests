
#ifndef __GENERATED_php_roadsend_tests_inc19_h__
#define __GENERATED_php_roadsend_tests_inc19_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc19.fw.h>

// Declarations
#include <cls/ffunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc19_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_ffunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc19_h__
