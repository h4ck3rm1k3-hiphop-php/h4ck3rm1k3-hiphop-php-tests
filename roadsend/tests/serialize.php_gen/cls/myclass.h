
#ifndef __GENERATED_cls_myclass_h__
#define __GENERATED_cls_myclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/serialize.php line 24 */
class c_myclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass)
  END_CLASS_MAP(myclass)
  DECLARE_CLASS(myclass, myClass, ObjectData)
  void init();
  public: Variant m_p1;
  public: Variant m_p2;
  public: Variant m_p3;
  public: Variant m_p4;
  public: Variant m_p5;
  public: Variant m_p6;
  public: String t___tostring();
  public: void t_hello();
  public: Variant t___wakeup();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass_h__
