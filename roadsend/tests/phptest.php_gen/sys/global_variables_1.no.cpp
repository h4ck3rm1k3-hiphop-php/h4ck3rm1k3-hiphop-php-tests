
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      HASH_RETURN(0x41140526F1E1D5C1LL, g->GV(somenewvarfirst),
                  somenewvarfirst);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 10:
      HASH_RETURN(0x2A6558C007B3B00ALL, g->GV(avar),
                  avar);
      HASH_RETURN(0x4292CEE227B9150ALL, g->GV(a),
                  a);
      HASH_RETURN(0x1E46B4C4E7D774CALL, g->GV(bork),
                  bork);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 15:
      HASH_RETURN(0x4154FA2EF733DA8FLL, g->GV(foo),
                  foo);
      HASH_RETURN(0x13DEB4809EAAC8CFLL, g->GV(somenewvarsecond),
                  somenewvarsecond);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      HASH_RETURN(0x04A52CA825E8EDD0LL, g->GV(zops),
                  zops);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 21:
      HASH_RETURN(0x08FBB133F8576BD5LL, g->GV(b),
                  b);
      break;
    case 24:
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 41:
      HASH_RETURN(0x44512AF0D29AB3A9LL, g->GV(zippy),
                  zippy);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 50:
      HASH_RETURN(0x045A631FC0FF2A32LL, g->GV(zing),
                  zing);
      break;
    case 61:
      HASH_RETURN(0x2072FF3918A89F3DLL, g->GV(cnt),
                  cnt);
      HASH_RETURN(0x26E70030364D72FDLL, g->GV(zonk),
                  zonk);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
