
#include <php/roadsend/tests/bug-id-0001155.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001155_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001155.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001155_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);

  (v_i = 10000000LL);
  LINE(4,x_var_dump(1, v_i));
  v_i *= 1001LL;
  LINE(8,x_var_dump(1, v_i));
  (v_j = 10000000LL);
  LINE(11,x_var_dump(1, v_j));
  (v_j = v_j * 1001LL);
  LINE(15,x_var_dump(2, v_i, Array(ArrayInit(1).set(0, v_j).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
