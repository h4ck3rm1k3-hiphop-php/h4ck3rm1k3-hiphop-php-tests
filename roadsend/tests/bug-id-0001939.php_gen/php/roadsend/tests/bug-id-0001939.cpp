
#include <php/roadsend/tests/bug-id-0001939.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001939.php line 37 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a;
  Variant v_foo;
  Variant v_bar;
  Variant v_b;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_a; Variant &v_foo; Variant &v_bar; Variant &v_b;
    VariableTable(Variant &r_a, Variant &r_foo, Variant &r_bar, Variant &r_b) : v_a(r_a), v_foo(r_foo), v_bar(r_bar), v_b(r_b) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 7) {
        case 2:
          HASH_RETURN(0x4292CEE227B9150ALL, v_a,
                      a);
          break;
        case 5:
          HASH_RETURN(0x08FBB133F8576BD5LL, v_b,
                      b);
          break;
        case 7:
          HASH_RETURN(0x4154FA2EF733DA8FLL, v_foo,
                      foo);
          HASH_RETURN(0x4A453601FBA86E5FLL, v_bar,
                      bar);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_a, v_foo, v_bar, v_b);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (v_a = 3LL);
  (v_foo = "bar");
  (variables->get(toString(v_foo)) = ref(v_a));
  (v_bar = 4LL);
  echo(toString(v_a) + toString("\n"));
  echo(toString(v_bar) + toString("\n"));
  (v_b = 3LL);
  (v_foo = "zot");
  (variables->get(toString(v_foo)) = ref(v_b));
  (variables->get(toString(v_foo)) = 4LL);
  echo(toString(v_b) + toString("\n"));
  echo(concat(toString(variables->get(toString(v_foo))), "\n"));
} /* function */
Variant pm_php$roadsend$tests$bug_id_0001939_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001939.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001939_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_blah __attribute__((__unused__)) = (variables != gVariables) ? variables->get("blah") : g->GV(blah);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo(" When trying to compile:\n\n");
  (v_blah = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_blah.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_key = iter3->first();
      {
        (variables->get(toString(v_key)) = ref(lval(v_blah.lvalAt(v_key))));
      }
    }
  }
  echo(toString(v_a) + toString("\n"));
  echo("\nI get an error\n\n\n\n");
  (v_a = 3LL);
  (v_foo = "bar");
  (variables->get(toString(v_foo)) = ref(v_a));
  (v_bar = 4LL);
  echo(toString(v_a) + toString("\n"));
  echo(toString(v_bar) + toString("\n"));
  (v_b = 3LL);
  (v_foo = "zot");
  (variables->get(toString(v_foo)) = ref(v_b));
  (variables->get(toString(v_foo)) = 4LL);
  echo(toString(v_b) + toString("\n"));
  echo(concat(toString(variables->get(toString(v_foo))), "\n"));
  echo("-----\n");
  LINE(53,f_foo());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
