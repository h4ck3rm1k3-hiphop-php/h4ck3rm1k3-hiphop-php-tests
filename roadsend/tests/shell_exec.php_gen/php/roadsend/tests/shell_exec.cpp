
#include <php/roadsend/tests/shell_exec.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$shell_exec_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/shell_exec.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$shell_exec_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_SysVer __attribute__((__unused__)) = (variables != gVariables) ? variables->get("SysVer") : g->GV(SysVer);

  if (equal(LINE(3,x_substr("Linux" /* PHP_OS */, toInt32(0LL), toInt32(3LL))), "WIN")) {
    print(LINE(4,x_shell_exec("dir c:\\")));
    echo(concat(f_shell_exec(toString("dir \"c:\\program files\\\"")), "\n"));
  }
  else {
    (v_SysVer = f_shell_exec(toString("ver")));
    print(toString(v_SysVer));
    print(LINE(11,x_shell_exec("echo wibble wibble wibble")));
    print(LINE(12,x_shell_exec("ls /")));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
