
#ifndef __GENERATED_php_roadsend_tests_string_ref_h__
#define __GENERATED_php_roadsend_tests_string_ref_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/string-ref.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$string_ref_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_foo();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_string_ref_h__
