
#include <php/roadsend/tests/bug-id-0002765.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0002765_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0002765.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0002765_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_hash __attribute__((__unused__)) = (variables != gVariables) ? variables->get("hash") : g->GV(hash);

  echo("php-hash's doubly linked list is being walked past the sentinel\n\nLooks like this will do it:\n\n");
  (v_hash = ScalarArrays::sa_[0]);
  print((toString(LINE(9,x_array_pop(ref(v_hash))))));
  print((toString(LINE(10,x_array_pop(ref(v_hash))))));
  print((toString(LINE(11,x_array_pop(ref(v_hash))))));
  print("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
