
#ifndef __GENERATED_cls_sm_smtag_areah_h__
#define __GENERATED_cls_sm_smtag_areah_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/finc8.php line 23 */
class c_sm_smtag_areah : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_areah)
  END_CLASS_MAP(sm_smtag_areah)
  DECLARE_CLASS(sm_smtag_areah, SM_smTag_AREAh, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_areah_h__
