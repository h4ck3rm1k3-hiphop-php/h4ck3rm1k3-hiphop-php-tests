
#ifndef __GENERATED_php_roadsend_tests_class_static_members_h__
#define __GENERATED_php_roadsend_tests_class_static_members_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/class-static-members.fw.h>

// Declarations
#include <cls/bar2.h>
#include <cls/foo2.h>
#include <cls/foo.h>
#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$class_static_members_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_bar2(CArrRef params, bool init = true);
Object co_foo2(CArrRef params, bool init = true);
Object co_foo(CArrRef params, bool init = true);
Object co_bar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_class_static_members_h__
