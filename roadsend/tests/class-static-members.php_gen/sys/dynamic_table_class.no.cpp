
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_bar2(CArrRef params, bool init = true);
Variant cw_bar2$os_get(const char *s);
Variant &cw_bar2$os_lval(const char *s);
Variant cw_bar2$os_constant(const char *s);
Variant cw_bar2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_foo2(CArrRef params, bool init = true);
Variant cw_foo2$os_get(const char *s);
Variant &cw_foo2$os_lval(const char *s);
Variant cw_foo2$os_constant(const char *s);
Variant cw_foo2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_foo(CArrRef params, bool init = true);
Variant cw_foo$os_get(const char *s);
Variant &cw_foo$os_lval(const char *s);
Variant cw_foo$os_constant(const char *s);
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_bar(CArrRef params, bool init = true);
Variant cw_bar$os_get(const char *s);
Variant &cw_bar$os_lval(const char *s);
Variant cw_bar$os_constant(const char *s);
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_CREATE_OBJECT(0x6BDD38463F55CF88LL, foo2);
      break;
    case 2:
      HASH_CREATE_OBJECT(0x1A3D88D28DA226F2LL, bar2);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x6176C0B993BF5914LL, foo);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_INVOKE_STATIC_METHOD(0x6BDD38463F55CF88LL, foo2);
      break;
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x1A3D88D28DA226F2LL, bar2);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x6176C0B993BF5914LL, foo);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY(0x6BDD38463F55CF88LL, foo2);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY(0x1A3D88D28DA226F2LL, bar2);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x6176C0B993BF5914LL, foo);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_STATIC_PROPERTY_LV(0x6BDD38463F55CF88LL, foo2);
      break;
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x1A3D88D28DA226F2LL, bar2);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x6176C0B993BF5914LL, foo);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GET_CLASS_CONSTANT(0x6BDD38463F55CF88LL, foo2);
      break;
    case 2:
      HASH_GET_CLASS_CONSTANT(0x1A3D88D28DA226F2LL, bar2);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x6176C0B993BF5914LL, foo);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x5E008C5BA4C3B3FDLL, bar);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
