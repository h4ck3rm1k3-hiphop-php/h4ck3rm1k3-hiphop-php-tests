
#ifndef __GENERATED_cls_bar2_h__
#define __GENERATED_cls_bar2_h__

#include <cls/foo2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/class-static-members.php line 48 */
class c_bar2 : virtual public c_foo2 {
  BEGIN_CLASS_MAP(bar2)
    PARENT_CLASS(foo2)
  END_CLASS_MAP(bar2)
  DECLARE_CLASS(bar2, Bar2, foo2)
  void init();
  public: String t_foostatic();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar2_h__
