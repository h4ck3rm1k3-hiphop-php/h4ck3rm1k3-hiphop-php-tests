
#ifndef __GENERATED_cls_foo2_h__
#define __GENERATED_cls_foo2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/class-static-members.php line 39 */
class c_foo2 : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo2)
  END_CLASS_MAP(foo2)
  DECLARE_CLASS(foo2, Foo2, ObjectData)
  void init();
  public: String t_staticvalue();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo2_h__
