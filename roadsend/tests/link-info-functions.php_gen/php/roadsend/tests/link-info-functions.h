
#ifndef __GENERATED_php_roadsend_tests_link_info_functions_h__
#define __GENERATED_php_roadsend_tests_link_info_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/link-info-functions.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$link_info_functions_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_link_info_functions_h__
