
#ifndef __GENERATED_php_roadsend_tests_finc_h__
#define __GENERATED_php_roadsend_tests_finc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/finc.fw.h>

// Declarations
#include <cls/sm_smtag_area.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$finc_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_area(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_finc_h__
