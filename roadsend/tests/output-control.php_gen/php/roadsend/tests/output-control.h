
#ifndef __GENERATED_php_roadsend_tests_output_control_h__
#define __GENERATED_php_roadsend_tests_output_control_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/output-control.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_callback(CVarRef v_buffer);
Variant pm_php$roadsend$tests$output_control_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_output_control_h__
