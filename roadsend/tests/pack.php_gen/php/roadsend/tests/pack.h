
#ifndef __GENERATED_php_roadsend_tests_pack_h__
#define __GENERATED_php_roadsend_tests_pack_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/pack.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$pack_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_tp(CStrRef v_fmt, double v_val);
String f_hexdump(CVarRef v_a);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_pack_h__
