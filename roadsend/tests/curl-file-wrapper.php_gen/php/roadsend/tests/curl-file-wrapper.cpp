
#include <php/roadsend/tests/curl-file-wrapper.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$curl_file_wrapper_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/curl-file-wrapper.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$curl_file_wrapper_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_handle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("handle") : g->GV(handle);
  Variant &v_contents __attribute__((__unused__)) = (variables != gVariables) ? variables->get("contents") : g->GV(contents);
  Variant &v_lines __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lines") : g->GV(lines);
  Variant &v_line_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line_num") : g->GV(line_num);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);

  (v_handle = LINE(3,x_fopen("http://www.google.com/", "rb")));
  (v_contents = "");
  LOOP_COUNTER(1);
  {
    while (!(LINE(5,x_feof(toObject(v_handle))))) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_contents, toString(LINE(6,x_fread(toObject(v_handle), 8192LL))));
      }
    }
  }
  LINE(8,x_fclose(toObject(v_handle)));
  LINE(10,x_var_dump(1, v_contents));
  (v_handle = LINE(12,x_fopen("https://secure.roadsend.com/_test/test.php\?arg=1", "rb")));
  (v_contents = "");
  if (toBoolean(v_handle)) {
    LOOP_COUNTER(2);
    {
      while (!(LINE(15,x_feof(toObject(v_handle))))) {
        LOOP_COUNTER_CHECK(2);
        {
          concat_assign(v_contents, toString(LINE(16,x_fread(toObject(v_handle), 8192LL))));
        }
      }
    }
    LINE(18,x_fclose(toObject(v_handle)));
    LINE(20,x_var_dump(1, v_contents));
  }
  else {
    echo("---- failed to connect ssl through wrapper\n");
  }
  (v_lines = LINE(28,x_file("http://www.google.com/")));
  {
    LOOP_COUNTER(3);
    for (ArrayIterPtr iter5 = v_lines.begin(); !iter5->end(); iter5->next()) {
      LOOP_COUNTER_CHECK(3);
      v_line = iter5->second();
      v_line_num = iter5->first();
      {
        echo(LINE(32,(assignCallTemp(eo_1, toString(v_line_num)),assignCallTemp(eo_3, x_htmlspecialchars(toString(v_line))),concat5("Line #<b>", eo_1, "</b> : ", eo_3, "<br />\n"))));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
