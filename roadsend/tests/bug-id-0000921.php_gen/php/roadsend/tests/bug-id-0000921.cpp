
#include <php/roadsend/tests/bug-id-0000921.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000921.php line 3 */
Variant c_hi::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_hi::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_hi::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("string", m_string.isReferenced() ? ref(m_string) : m_string));
  c_ObjectData::o_get(props);
}
bool c_hi::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x2027864469AD4382LL, string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_hi::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2027864469AD4382LL, m_string,
                         string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_hi::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x2027864469AD4382LL, m_string,
                      string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_hi::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2027864469AD4382LL, m_string,
                         string, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_hi::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(hi)
ObjectData *c_hi::create(Variant v_str) {
  init();
  t_hi(ref(v_str));
  return this;
}
ObjectData *c_hi::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(ref(const_cast<Array&>(params).lvalAt(0))));
  } else return this;
}
ObjectData *c_hi::cloneImpl() {
  c_hi *obj = NEW(c_hi)();
  cloneSet(obj);
  return obj;
}
void c_hi::cloneSet(c_hi *clone) {
  clone->m_string = m_string.isReferenced() ? ref(m_string) : m_string;
  ObjectData::cloneSet(clone);
}
Variant c_hi::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_hi::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_hi::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_hi$os_get(const char *s) {
  return c_hi::os_get(s, -1);
}
Variant &cw_hi$os_lval(const char *s) {
  return c_hi::os_lval(s, -1);
}
Variant cw_hi$os_constant(const char *s) {
  return c_hi::os_constant(s);
}
Variant cw_hi$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_hi::os_invoke(c, s, params, -1, fatal);
}
void c_hi::init() {
  m_string = null;
}
/* SRC: roadsend/tests/bug-id-0000921.php line 6 */
void c_hi::t_hi(Variant v_str) {
  INSTANCE_METHOD_INJECTION(hi, hi::hi);
  bool oldInCtor = gasInCtor(true);
  (m_string = v_str);
  echo(toString(m_string));
  gasInCtor(oldInCtor);
} /* function */
Object co_hi(CArrRef params, bool init /* = true */) {
  return Object(p_hi(NEW(c_hi)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000921_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000921.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000921_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_a = "blah");
  (v_n = ((Object)(LINE(16,p_hi(p_hi(NEWOBJ(c_hi)())->create(ref(v_a)))))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
