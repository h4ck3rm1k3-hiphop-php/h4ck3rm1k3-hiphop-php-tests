
#ifndef __GENERATED_cls_grandchild_h__
#define __GENERATED_cls_grandchild_h__

#include <cls/child.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/extends.php line 43 */
class c_grandchild : virtual public c_child {
  BEGIN_CLASS_MAP(grandchild)
    PARENT_CLASS(base)
    PARENT_CLASS(child)
  END_CLASS_MAP(grandchild)
  DECLARE_CLASS(grandchild, grandchild, child)
  void init();
  public: void t_grandchild(CVarRef v_foo);
  public: ObjectData *create(CVarRef v_foo);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_grandchild_h__
