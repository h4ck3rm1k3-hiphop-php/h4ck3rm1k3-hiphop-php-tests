
#ifndef __GENERATED_cls_base_h__
#define __GENERATED_cls_base_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/extends.php line 4 */
class c_base : virtual public ObjectData {
  BEGIN_CLASS_MAP(base)
  END_CLASS_MAP(base)
  DECLARE_CLASS(base, base, ObjectData)
  void init();
  public: String m_base_var;
  public: void t_base();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_base_method();
  public: void t_arith_op(CVarRef v_a, CVarRef v_b);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_base_h__
