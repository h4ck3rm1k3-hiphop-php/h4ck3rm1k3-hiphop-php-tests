
#ifndef __GENERATED_cls_child_h__
#define __GENERATED_cls_child_h__

#include <cls/base.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/extends.php line 25 */
class c_child : virtual public c_base {
  BEGIN_CLASS_MAP(child)
    PARENT_CLASS(base)
  END_CLASS_MAP(child)
  DECLARE_CLASS(child, child, base)
  void init();
  public: void t_arith_op(CVarRef v_a, CVarRef v_b);
  public: void t_child_method();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_child_h__
