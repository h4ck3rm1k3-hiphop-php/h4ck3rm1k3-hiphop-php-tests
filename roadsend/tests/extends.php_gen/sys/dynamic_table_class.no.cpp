
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_base(CArrRef params, bool init = true);
Variant cw_base$os_get(const char *s);
Variant &cw_base$os_lval(const char *s);
Variant cw_base$os_constant(const char *s);
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_grandchild(CArrRef params, bool init = true);
Variant cw_grandchild$os_get(const char *s);
Variant &cw_grandchild$os_lval(const char *s);
Variant cw_grandchild$os_constant(const char *s);
Variant cw_grandchild$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_child(CArrRef params, bool init = true);
Variant cw_child$os_get(const char *s);
Variant &cw_child$os_lval(const char *s);
Variant cw_child$os_constant(const char *s);
Variant cw_child$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_CREATE_OBJECT(0x62D2775E17EE82D1LL, base);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x21FC1EB8E42F870CLL, grandchild);
      break;
    case 6:
      HASH_CREATE_OBJECT(0x12C237AADF7B9B86LL, child);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_INVOKE_STATIC_METHOD(0x62D2775E17EE82D1LL, base);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x21FC1EB8E42F870CLL, grandchild);
      break;
    case 6:
      HASH_INVOKE_STATIC_METHOD(0x12C237AADF7B9B86LL, child);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GET_STATIC_PROPERTY(0x62D2775E17EE82D1LL, base);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x21FC1EB8E42F870CLL, grandchild);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY(0x12C237AADF7B9B86LL, child);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GET_STATIC_PROPERTY_LV(0x62D2775E17EE82D1LL, base);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x21FC1EB8E42F870CLL, grandchild);
      break;
    case 6:
      HASH_GET_STATIC_PROPERTY_LV(0x12C237AADF7B9B86LL, child);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GET_CLASS_CONSTANT(0x62D2775E17EE82D1LL, base);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x21FC1EB8E42F870CLL, grandchild);
      break;
    case 6:
      HASH_GET_CLASS_CONSTANT(0x12C237AADF7B9B86LL, child);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
