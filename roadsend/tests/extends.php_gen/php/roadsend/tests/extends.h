
#ifndef __GENERATED_php_roadsend_tests_extends_h__
#define __GENERATED_php_roadsend_tests_extends_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/extends.fw.h>

// Declarations
#include <cls/base.h>
#include <cls/grandchild.h>
#include <cls/child.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$extends_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_base(CArrRef params, bool init = true);
Object co_grandchild(CArrRef params, bool init = true);
Object co_child(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_extends_h__
