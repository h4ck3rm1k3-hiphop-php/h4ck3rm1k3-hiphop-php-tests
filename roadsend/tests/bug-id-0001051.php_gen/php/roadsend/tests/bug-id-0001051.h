
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001051_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001051_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001051.fw.h>

// Declarations
#include <cls/aclass.h>
#include <cls/bclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001051_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_aclass(CArrRef params, bool init = true);
Object co_bclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001051_h__
