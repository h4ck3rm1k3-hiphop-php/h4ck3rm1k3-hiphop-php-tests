
#include <php/roadsend/tests/bug-id-0000894.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000894.php line 5 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("default", m_default));
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x6DE26F84570270CCLL, default, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x6DE26F84570270CCLL, m_default,
                         default, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x6DE26F84570270CCLL, m_default,
                      default, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  clone->m_default = m_default;
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x1477EA075A1A0ABDLL, blah) {
        return (t_blah(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x1477EA075A1A0ABDLL, blah) {
        return (t_blah(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
  m_default = "test";
}
/* SRC: roadsend/tests/bug-id-0000894.php line 7 */
void c_myclass::t_blah() {
  INSTANCE_METHOD_INJECTION(myclass, myclass::blah);
  (m_default = "meep");
} /* function */
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000894_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000894.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000894_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("parse error on class variable named 'default'\n\n");
  (v_c = ((Object)(LINE(12,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  LINE(13,v_c.o_invoke_few_args("blah", 0x1477EA075A1A0ABDLL, 0));
  echo(toString(v_c.o_get("default", 0x6DE26F84570270CCLL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
