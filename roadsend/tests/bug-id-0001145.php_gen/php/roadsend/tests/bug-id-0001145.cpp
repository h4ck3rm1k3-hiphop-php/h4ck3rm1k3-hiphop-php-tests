
#include <php/roadsend/tests/bug-id-0001145.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001145_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001145.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001145_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_var_dump(1, x_strstr("test string", "test")));
  LINE(4,x_var_dump(1, x_strstr("test string", "string")));
  LINE(5,x_var_dump(1, x_strstr("test string", "strin")));
  LINE(6,x_var_dump(1, x_strstr("test string", "t s")));
  LINE(7,x_var_dump(1, x_strstr("test string", "g")));
  LINE(8,x_var_dump(1, x_md5(toString((assignCallTemp(eo_0, (assignCallTemp(eo_3, x_chr(0LL)),concat3("te", eo_3, "st"))),assignCallTemp(eo_1, x_chr(0LL)),x_strstr(eo_0, eo_1))))));
  LINE(9,x_var_dump(1, x_strstr("tEst", "test")));
  LINE(10,x_var_dump(1, x_strstr("teSt", "test")));
  LINE(11,x_var_dump(1, (silenceInc(), silenceDec(x_strstr("", "")))));
  LINE(12,x_var_dump(1, (silenceInc(), silenceDec(x_strstr("a", "")))));
  LINE(13,x_var_dump(1, (silenceInc(), silenceDec(x_strstr("", "a")))));
  LINE(14,x_var_dump(1, x_md5(toString((silenceInc(), silenceDec(toString(x_strstr("\\\\a\\", "\\a"))))))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
