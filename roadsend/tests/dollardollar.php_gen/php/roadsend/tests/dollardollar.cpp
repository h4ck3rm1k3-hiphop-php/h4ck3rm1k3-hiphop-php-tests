
#include <php/roadsend/tests/dollardollar.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$dollardollar_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/dollardollar.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$dollardollar_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);

  (v_bar = 7LL);
  (v_foo = "bar");
  echo(LINE(6,concat3("bar is ", toString(v_bar), "\n")));
  echo(LINE(7,concat3("bar is $", toString(v_foo), "\n")));
  echo(LINE(8,concat3("bar is ", toString(variables->get(toString(v_foo))), "\n")));
  echo(LINE(9,concat3("bar is ", toString(variables->get(toString(v_foo))), "\n")));
  (v_zot = "foo");
  echo(LINE(13,concat3("bar is still ", toString(variables->get(toString(variables->get(toString(v_zot))))), "\n")));
  (variables->get(toString(variables->get(toString(v_zot)))) = "ping");
  echo(LINE(17,concat3("bar is now ", toString(v_bar), " (want ping)\n")));
  (v_foo = "bar\n");
  echo(LINE(20,concat3("bar is not ", toString(variables->get(toString(v_foo))), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
