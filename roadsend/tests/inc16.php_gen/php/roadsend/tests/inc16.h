
#ifndef __GENERATED_php_roadsend_tests_inc16_h__
#define __GENERATED_php_roadsend_tests_inc16_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc16.fw.h>

// Declarations
#include <cls/cfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc16_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_cfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc16_h__
