
#include <php/roadsend/tests/bug-id-0001104.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001104_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001104.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001104_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  (v_a = ScalarArrays::sa_[0]);
  (v_b = ScalarArrays::sa_[1]);
  (v_c = ScalarArrays::sa_[2]);
  (v_d = ScalarArrays::sa_[3]);
  {
    echo(toString(LINE(11,x_array_pop(ref(v_a)))));
    echo("\n");
  }
  LINE(12,x_array_push(2, ref(v_a), "foobar"));
  LINE(13,x_var_dump(1, v_a));
  {
    echo(toString(LINE(15,x_array_pop(ref(v_d)))));
    echo("\n");
  }
  LINE(16,x_array_push(2, ref(v_d), "mope"));
  LINE(17,x_var_dump(1, v_d));
  {
    echo(toString(LINE(20,x_array_pop(ref(v_b)))));
    echo("\n");
  }
  (v_k = LINE(21,x_key(ref(v_b))));
  LINE(22,x_var_dump(1, v_k));
  LINE(23,x_var_dump(1, v_b));
  {
    echo(toString(LINE(26,x_array_pop(ref(v_c)))));
    echo("\n");
  }
  LINE(27,x_var_dump(1, v_c));
  LINE(29,x_array_pop(ref(get_global_array_wrapper())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
