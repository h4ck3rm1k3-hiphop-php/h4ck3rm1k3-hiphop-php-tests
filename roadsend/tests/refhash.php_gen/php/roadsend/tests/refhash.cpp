
#include <php/roadsend/tests/refhash.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$refhash_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/refhash.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$refhash_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_tree __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tree") : g->GV(tree);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_bar = 1LL);
  (v_tree = 2LL);
  v_foo.set("asdf", (v_bar), 0x06528546A3758F5CLL);
  v_foo.set("bsdf", (ref(v_bar)), 0x491C75C38A5D0F27LL);
  v_foo.set("csdf", (ref(v_tree)), 0x2DBE308821A31660LL);
  echo(LINE(10,concat5("1: bar: ", toString(v_bar), ", tree: ", toString(v_tree), "\n")));
  echo(concat("2: ", LINE(11,concat6(toString(v_foo.rvalAt("asdf", 0x06528546A3758F5CLL)), ", ", toString(v_foo.rvalAt("bsdf", 0x491C75C38A5D0F27LL)), ", ", toString(v_foo.rvalAt("csdf", 0x2DBE308821A31660LL)), "\n"))));
  v_foo.set("asdf", (8LL), 0x06528546A3758F5CLL);
  v_foo.set("bsdf", (9LL), 0x491C75C38A5D0F27LL);
  v_foo.set("csdf", (10LL), 0x2DBE308821A31660LL);
  echo(LINE(17,concat5("3: bar: ", toString(v_bar), ", tree: ", toString(v_tree), "\n")));
  echo(concat("4: ", LINE(18,concat6(toString(v_foo.rvalAt("asdf", 0x06528546A3758F5CLL)), ", ", toString(v_foo.rvalAt("bsdf", 0x491C75C38A5D0F27LL)), ", ", toString(v_foo.rvalAt("csdf", 0x2DBE308821A31660LL)), "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
