
#include <php/roadsend/tests/str_replace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$str_replace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/str_replace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$str_replace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_vowels __attribute__((__unused__)) = (variables != gVariables) ? variables->get("vowels") : g->GV(vowels);
  Variant &v_phrase __attribute__((__unused__)) = (variables != gVariables) ? variables->get("phrase") : g->GV(phrase);
  Variant &v_healthy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("healthy") : g->GV(healthy);
  Variant &v_yummy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("yummy") : g->GV(yummy);

  echo(toString(LINE(4,x_str_replace("%body%", "black", "<body text='%body%'>"))));
  (v_vowels = ScalarArrays::sa_[0]);
  echo(toString(LINE(9,x_str_replace(v_vowels, "", "Hello World of PHP"))));
  (v_phrase = "You should eat fruits, vegetables, and fiber every day.");
  (v_healthy = ScalarArrays::sa_[1]);
  (v_yummy = ScalarArrays::sa_[2]);
  echo(toString(LINE(16,x_str_replace(v_healthy, v_yummy, v_phrase))));
  (v_healthy = ScalarArrays::sa_[1]);
  (v_yummy = ScalarArrays::sa_[3]);
  echo(toString(LINE(20,x_str_replace(v_healthy, v_yummy, v_phrase))));
  (v_healthy = ScalarArrays::sa_[1]);
  (v_yummy = "beer");
  echo(toString(LINE(24,x_str_replace(v_healthy, v_yummy, v_phrase))));
  LINE(30,x_var_dump(1, x_str_replace(ScalarArrays::sa_[4], ScalarArrays::sa_[5], "1")));
  LINE(32,x_var_dump(1, x_str_replace(ScalarArrays::sa_[6], "", "foo")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
