
#include <php/roadsend/tests/gabriel-takf.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/gabriel-takf.php line 14 */
Variant f_takfsub(Variant v_f, Variant v_x, Variant v_y, Variant v_z) {
  FUNCTION_INJECTION(takfsub);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  if (!((less(v_y, v_x)))) {
    return v_z;
  }
  else {
    return LINE(20,(assignCallTemp(eo_0, ref(v_f)),assignCallTemp(eo_1, ref(LINE(18,invoke(toString(v_f), Array(ArrayInit(4).set(0, ref(v_f)).set(1, v_x - 1LL).set(2, ref(v_y)).set(3, ref(v_z)).create()), -1)))),assignCallTemp(eo_2, ref(LINE(19,invoke(toString(v_f), Array(ArrayInit(4).set(0, ref(v_f)).set(1, v_y - 1LL).set(2, ref(v_z)).set(3, ref(v_x)).create()), -1)))),assignCallTemp(eo_3, ref(LINE(20,invoke(toString(v_f), Array(ArrayInit(4).set(0, ref(v_f)).set(1, v_z - 1LL).set(2, ref(v_x)).set(3, ref(v_y)).create()), -1)))),invoke(toString(v_f), Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()), -1)));
  }
  return null;
} /* function */
/* SRC: roadsend/tests/gabriel-takf.php line 10 */
Variant f_takf(int64 v_x, int64 v_y, int64 v_z) {
  FUNCTION_INJECTION(takf);
  return LINE(11,f_takfsub("takfsub", v_x, v_y, v_z));
} /* function */
Variant i_takfsub(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x29FA48BEEF98A047LL, takfsub) {
    return (f_takfsub(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$gabriel_takf_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/gabriel-takf.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$gabriel_takf_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("From \"Performance and Evaluation of Lisp Systems\" by Richard Gabriel \npg. 100\n\n\"TAKF is an alternative TAK formulation proposed by George Carrette,\nwho was working on the Vax NIL project at MIT when the benchmark study\nwas started.\"\n\n");
  print(LINE(24,(assignCallTemp(eo_1, toString(f_takf(18LL, 12LL, 6LL))),concat3("result: ", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
