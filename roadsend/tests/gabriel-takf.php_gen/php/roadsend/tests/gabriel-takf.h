
#ifndef __GENERATED_php_roadsend_tests_gabriel_takf_h__
#define __GENERATED_php_roadsend_tests_gabriel_takf_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/gabriel-takf.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_takfsub(Variant v_f, Variant v_x, Variant v_y, Variant v_z);
Variant pm_php$roadsend$tests$gabriel_takf_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_takf(int64 v_x, int64 v_y, int64 v_z);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_gabriel_takf_h__
