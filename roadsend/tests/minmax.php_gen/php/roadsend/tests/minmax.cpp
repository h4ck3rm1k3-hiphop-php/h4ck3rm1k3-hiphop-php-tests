
#include <php/roadsend/tests/minmax.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$minmax_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/minmax.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$minmax_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("max\n");
  echo(concat(concat_rev(toString(LINE(4,x_max(2, 1.2344999999999999, ScalarArrays::sa_[4]))), concat_rev(toString(x_max(4, 5LL, ScalarArrays::sa_[3])), concat(concat_rev(toString(x_max(3, 0LL, ScalarArrays::sa_[2])), concat(concat_rev(toString(x_max(2, "foo", ScalarArrays::sa_[1])), concat(toString(x_max(5, -2354LL, ScalarArrays::sa_[0])), " ")), " ")), " "))), "\n"));
  echo(LINE(5,(assignCallTemp(eo_1, toString(x_max(1, ScalarArrays::sa_[15]))),concat3("max array: ", eo_1, "\n"))));
  (v_v = LINE(6,x_max(2, ScalarArrays::sa_[5], ScalarArrays::sa_[7])));
  echo(LINE(7,(assignCallTemp(eo_1, (x_var_dump(1, v_v), null)),concat3("max array 2: ", eo_1, "\n"))));
  (v_v = LINE(8,x_max(3, ScalarArrays::sa_[8], ScalarArrays::sa_[10])));
  echo(LINE(9,(assignCallTemp(eo_1, (x_var_dump(1, v_v), null)),concat3("max array 2a: ", eo_1, "\n"))));
  echo(LINE(10,(assignCallTemp(eo_1, toString(x_max(3, "string", ScalarArrays::sa_[17]))),concat3("max array 3: ", eo_1, "\n"))));
  echo(LINE(11,(assignCallTemp(eo_1, toString(x_max(2, 0LL, ScalarArrays::sa_[12]))),concat3("max 2: ", eo_1, "\n"))));
  echo(LINE(12,(assignCallTemp(eo_1, toString(x_max(2, "hello", ScalarArrays::sa_[11]))),concat3("max 3: ", eo_1, "\n"))));
  echo(LINE(13,(assignCallTemp(eo_1, toString(x_max(2, "hello", ScalarArrays::sa_[18]))),concat3("max 4: ", eo_1, "\n"))));
  echo("min\n");
  echo(concat(concat_rev(toString(LINE(16,x_min(2, 1.2344999999999999, ScalarArrays::sa_[4]))), concat_rev(toString(x_min(4, 5LL, ScalarArrays::sa_[3])), concat(concat_rev(toString(x_min(3, 0LL, ScalarArrays::sa_[2])), concat(concat_rev(toString(x_min(2, 0LL, ScalarArrays::sa_[1])), concat(toString(x_min(5, -2354LL, ScalarArrays::sa_[0])), " ")), " ")), " "))), "\n"));
  echo(LINE(17,(assignCallTemp(eo_1, toString(x_min(1, ScalarArrays::sa_[15]))),concat3("min array: ", eo_1, "\n"))));
  echo(LINE(18,(assignCallTemp(eo_1, toString(x_min(2, ScalarArrays::sa_[5], ScalarArrays::sa_[7]))),concat3("min array 2: ", eo_1, "\n"))));
  echo(LINE(19,(assignCallTemp(eo_1, toString(x_min(3, "string", ScalarArrays::sa_[17]))),concat3("min array 3: ", eo_1, "\n"))));
  echo(LINE(20,(assignCallTemp(eo_1, toString(x_min(2, 0LL, ScalarArrays::sa_[12]))),concat3("min 2: ", eo_1, "\n"))));
  echo(LINE(21,(assignCallTemp(eo_1, toString(x_min(2, "hello", ScalarArrays::sa_[11]))),concat3("min 3: ", eo_1, "\n"))));
  echo(LINE(22,(assignCallTemp(eo_1, toString(x_min(2, "hello", ScalarArrays::sa_[18]))),concat3("min 4: ", eo_1, "\n"))));
  echo((toString(LINE(27,x_max(2, "hello", ScalarArrays::sa_[11])))));
  echo("\n");
  echo((toString(LINE(29,x_max(2, 0LL, ScalarArrays::sa_[12])))));
  echo("\n");
  echo((toString(LINE(31,x_min(2, "hello", ScalarArrays::sa_[11])))));
  echo("\n");
  echo((toString(LINE(33,x_min(2, 0LL, ScalarArrays::sa_[12])))));
  echo("\n");
  echo((toString(LINE(36,x_max(1, ScalarArrays::sa_[13])))));
  echo("\n");
  echo((toString(LINE(38,x_min(1, ScalarArrays::sa_[14])))));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
