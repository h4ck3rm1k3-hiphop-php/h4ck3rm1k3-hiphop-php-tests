
#ifndef __GENERATED_php_roadsend_tests_finc9_h__
#define __GENERATED_php_roadsend_tests_finc9_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/finc9.fw.h>

// Declarations
#include <cls/sm_smtag_areai.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$finc9_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_areai(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_finc9_h__
