
#ifndef __GENERATED_php_roadsend_tests_hello_h__
#define __GENERATED_php_roadsend_tests_hello_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/hello.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_destroy();
bool f_delete_event();
void f_hello();
Variant pm_php$roadsend$tests$hello_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_hello_h__
