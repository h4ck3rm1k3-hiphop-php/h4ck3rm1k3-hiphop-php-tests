
#include <php/roadsend/tests/bug-id-0003250.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0003250_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003250.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003250_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo(" This\n ");
  if (less(v_i++, 1LL)) {
  }
  echo(" gives me\n Runtime error in file bar.php on line 0: Type `onum' expected, `bnil' provided\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
