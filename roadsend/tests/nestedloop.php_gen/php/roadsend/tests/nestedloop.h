
#ifndef __GENERATED_php_roadsend_tests_nestedloop_h__
#define __GENERATED_php_roadsend_tests_nestedloop_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/nestedloop.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_nestedloop(CVarRef v_n);
Variant pm_php$roadsend$tests$nestedloop_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_nestedloop_h__
