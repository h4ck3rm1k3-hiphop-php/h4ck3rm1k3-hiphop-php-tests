
#include <php/roadsend/tests/bug-id-0001107.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001107_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001107.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001107_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arrays __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arrays") : g->GV(arrays);
  Variant &v_item __attribute__((__unused__)) = (variables != gVariables) ? variables->get("item") : g->GV(item);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_arrays = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_arrays.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_item = iter3->second();
      {
        echo("===========================================\n");
        LINE(38,x_var_dump(1, v_item));
        echo("-------------------------------------------\n");
        {
          LOOP_COUNTER(4);
          for ((v_i = 0LL); less(v_i, (LINE(40,x_sizeof(v_item)) + 1LL)); v_i++) {
            LOOP_COUNTER_CHECK(4);
            {
              echo(LINE(41,concat3("[", toString(v_i), "]\n")));
              LINE(42,x_var_dump(1, (silenceInc(), silenceDec(x_array_chunk(v_item, toInt32(v_i))))));
              LINE(43,x_var_dump(1, (silenceInc(), silenceDec(x_array_chunk(v_item, toInt32(v_i), true)))));
              LINE(44,x_var_dump(1, (silenceInc(), silenceDec(x_array_chunk(v_item, toInt32(v_i), false)))));
              echo("\n");
            }
          }
        }
        echo("\n");
      }
    }
  }
  echo("end\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
