
#ifndef __GENERATED_php_roadsend_tests_autoload_member_h__
#define __GENERATED_php_roadsend_tests_autoload_member_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/autoload-member.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f___autoload(CVarRef v_class);
Variant pm_php$roadsend$tests$autoload_member_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_autoload_member_h__
