
#include <php/roadsend/tests/images.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$roadsend$tests$images_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/images.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$images_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_width __attribute__((__unused__)) = (variables != gVariables) ? variables->get("width") : g->GV(width);
  Variant &v_height __attribute__((__unused__)) = (variables != gVariables) ? variables->get("height") : g->GV(height);
  Variant &v_type __attribute__((__unused__)) = (variables != gVariables) ? variables->get("type") : g->GV(type);
  Variant &v_attr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("attr") : g->GV(attr);

  df_lambda_1(LINE(3,x_getimagesize("../../data/bottom.gif")), v_width, v_height, v_type, v_attr);
  echo(concat_rev(LINE(4,concat6(toString(v_height), ", ", toString(v_type), ", ", toString(v_attr), "\n")), concat3("1: ", toString(v_width), ", ")));
  df_lambda_2(LINE(5,x_getimagesize("../../data/bottom.png")), v_width, v_height, v_type, v_attr);
  echo(concat_rev(LINE(6,concat6(toString(v_height), ", ", toString(v_type), ", ", toString(v_attr), "\n")), concat3("2: ", toString(v_width), ", ")));
  df_lambda_3(LINE(7,x_getimagesize("../../data/bottom.jpg")), v_width, v_height, v_type, v_attr);
  echo(concat_rev(LINE(8,concat6(toString(v_height), ", ", toString(v_type), ", ", toString(v_attr), "\n")), concat3("3: ", toString(v_width), ", ")));
  echo(LINE(28,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(1LL) /* IMAGETYPE_GIF */)),concat3("gif: ", eo_1, "\n"))));
  echo(LINE(29,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(2LL) /* IMAGETYPE_JPEG */)),concat3("jpeg: ", eo_1, "\n"))));
  echo(LINE(30,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(3LL) /* IMAGETYPE_PNG */)),concat3("png: ", eo_1, "\n"))));
  echo(LINE(34,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(1LL) /* IMAGETYPE_GIF */)),concat3(" IMAGETYPE_GIF: 1 ", eo_1, "\n"))));
  echo(LINE(36,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(2LL) /* IMAGETYPE_JPEG */)),concat3(" IMAGETYPE_JPEG: 2 ", eo_1, "\n"))));
  echo(LINE(38,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(3LL) /* IMAGETYPE_PNG */)),concat3(" IMAGETYPE_PNG: 3 ", eo_1, "\n"))));
  echo(LINE(40,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(4LL) /* IMAGETYPE_SWF */)),concat3(" IMAGETYPE_SWF: 4 ", eo_1, "\n"))));
  echo(LINE(42,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(5LL) /* IMAGETYPE_PSD */)),concat3(" IMAGETYPE_PSD: 5 ", eo_1, "\n"))));
  echo(LINE(44,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(6LL) /* IMAGETYPE_BMP */)),concat3(" IMAGETYPE_BMP: 6 ", eo_1, "\n"))));
  echo(LINE(46,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(7LL) /* IMAGETYPE_TIFF_II */)),concat3(" IMAGETYPE_TIFF_II: 7 ", eo_1, "\n"))));
  echo(LINE(48,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(8LL) /* IMAGETYPE_TIFF_MM */)),concat3(" IMAGETYPE_TIFF_MM: 8 ", eo_1, "\n"))));
  echo(LINE(50,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(9LL) /* IMAGETYPE_JPC */)),concat3(" IMAGETYPE_JPC: 9 ", eo_1, "\n"))));
  echo(LINE(52,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(9LL) /* IMAGETYPE_JPEG2000 */)),concat3(" IMAGETYPE_JPEG2000: 9 ", eo_1, "\n"))));
  echo(LINE(54,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(10LL) /* IMAGETYPE_JP2 */)),concat3(" IMAGETYPE_JP2: 10 ", eo_1, "\n"))));
  echo(LINE(56,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(11LL) /* IMAGETYPE_JPX */)),concat3(" IMAGETYPE_JPX: 11 ", eo_1, "\n"))));
  echo(LINE(58,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(12LL) /* IMAGETYPE_JB2 */)),concat3(" IMAGETYPE_JB2: 12 ", eo_1, "\n"))));
  echo(LINE(62,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(14LL) /* IMAGETYPE_IFF */)),concat3(" IMAGETYPE_IFF: 14 ", eo_1, "\n"))));
  echo(LINE(64,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(15LL) /* IMAGETYPE_WBMP */)),concat3(" IMAGETYPE_WBMP: 15 ", eo_1, "\n"))));
  echo(LINE(66,(assignCallTemp(eo_1, x_image_type_to_mime_type(toInt32(16LL) /* IMAGETYPE_XBM */)),concat3(" IMAGETYPE_XBM: 16 ", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
