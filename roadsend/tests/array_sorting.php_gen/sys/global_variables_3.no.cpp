
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_INDEX(0x35D32B9EF30B4340LL, fruits, 16);
      break;
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x4056C2E766E0B782LL, val, 18);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 7:
      HASH_INDEX(0x612DD31212E90587LL, key, 17);
      break;
    case 8:
      HASH_INDEX(0x22012958B0EDE8C8LL, input3, 14);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x301931AB31B0F289LL, input4, 15);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 19);
      break;
    case 11:
      HASH_INDEX(0x1532499416EFFACBLL, input, 12);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 22);
      break;
    case 24:
      HASH_INDEX(0x2D5185583EF85E98LL, b2, 28);
      break;
    case 29:
      HASH_INDEX(0x01EC08C71128761DLL, input2, 13);
      break;
    case 34:
      HASH_INDEX(0x1D05EC09E2BD4C22LL, img, 26);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 49:
      HASH_INDEX(0x69E7413AE0C88471LL, value, 20);
      break;
    case 53:
      HASH_INDEX(0x331353645577AFF5LL, array1, 23);
      break;
    case 54:
      HASH_INDEX(0x6C05C2858C72A876LL, a2, 21);
      HASH_INDEX(0x1AF5392A9D424176LL, array3, 25);
      break;
    case 57:
      HASH_INDEX(0x3D4B408A972F19F9LL, array2, 24);
      break;
    case 60:
      HASH_INDEX(0x2F9A5EEE8A58063CLL, wtfarray, 27);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 29) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
