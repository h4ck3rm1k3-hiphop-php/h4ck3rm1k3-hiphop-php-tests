
#ifndef __GENERATED_php_roadsend_tests_array_sorting_h__
#define __GENERATED_php_roadsend_tests_array_sorting_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/array_sorting.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_cmp(CVarRef v_a, CVarRef v_b);
Variant pm_php$roadsend$tests$array_sorting_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_array_sorting_h__
