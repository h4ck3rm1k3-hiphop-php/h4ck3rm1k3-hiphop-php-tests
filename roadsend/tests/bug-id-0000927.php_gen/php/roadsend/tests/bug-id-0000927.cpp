
#include <php/roadsend/tests/bug-id-0000927.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000927_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000927.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000927_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_thisisaglobal __attribute__((__unused__)) = (variables != gVariables) ? variables->get("thisisaglobal") : g->GV(thisisaglobal);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo("0000927 unable to foreach() on $GLOBALS\n\n");
  (v_thisisaglobal = "foo");
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = get_global_array_wrapper().begin(); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3.second();
      v_key = iter3.first();
      {
        v_i++;
      }
    }
  }
  echo("it worked.\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
