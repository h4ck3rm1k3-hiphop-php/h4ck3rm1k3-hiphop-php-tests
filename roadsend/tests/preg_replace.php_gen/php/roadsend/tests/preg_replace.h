
#ifndef __GENERATED_php_roadsend_tests_preg_replace_h__
#define __GENERATED_php_roadsend_tests_preg_replace_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/preg_replace.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$preg_replace_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_preg_replace_h__
