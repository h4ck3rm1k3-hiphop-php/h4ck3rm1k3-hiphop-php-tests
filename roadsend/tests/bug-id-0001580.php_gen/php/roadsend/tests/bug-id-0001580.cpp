
#include <php/roadsend/tests/bug-id-0001580.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001580_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001580.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001580_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("failure to compile on return in global context\n");
  echo("up here\n");
  if (toBoolean(0LL)) {
    return null;
  }
  else {
    print(concat((toString(LINE(9,include("1580.inc", false, variables, "roadsend/tests/")))), "\n"));
    return null;
  }
  echo("hi\n");
  echo("down here\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
