
#include <php/roadsend/tests/zerrors.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_ERROR = 512LL /* E_USER_WARNING */;
const int64 k_FATAL = 256LL /* E_USER_ERROR */;
const int64 k_WARNING = 1024LL /* E_USER_NOTICE */;

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/zerrors.php line 35 */
Variant f_scale_by_log(CVarRef v_vect, double v_scale) {
  FUNCTION_INJECTION(scale_by_log);
  Variant eo_0;
  Variant eo_1;
  int64 v_i = 0;
  Sequence v_temp;

  if (!(LINE(36,x_is_numeric(v_scale))) || not_more(v_scale, 0LL)) LINE(38,x_trigger_error(toString("log(x) for x <= 0 is undefined, you used: scale = ") + toString(v_scale), toInt32(256LL) /* FATAL */));
  if (!(LINE(39,x_is_array(v_vect)))) {
    LINE(40,x_trigger_error("Incorrect input vector, array of values expected", toInt32(512LL) /* ERROR */));
    return null;
  }
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(43,x_count(v_vect))); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        if (!(LINE(44,x_is_numeric(v_vect.rvalAt(v_i))))) LINE(46,(assignCallTemp(eo_0, LINE(45,concat3("Value at position ", toString(v_i), " is not a number, using 0 (zero)"))),x_trigger_error(eo_0, toInt32(1024LL) /* WARNING */)));
        v_temp.set(v_i, (LINE(47,x_log(v_scale)) * toDouble(v_vect.rvalAt(v_i))));
      }
    }
  }
  return v_temp;
} /* function */
/* SRC: roadsend/tests/zerrors.php line 12 */
void f_myerrorhandler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline, CVarRef v_vars) {
  FUNCTION_INJECTION(myErrorHandler);
  {
    switch (toInt64(v_errno)) {
    case 256LL:
      {
        echo(LINE(15,concat5("<b>FATAL</b> [", toString(v_errno), "] ", toString(v_errstr), "<br>\n")));
        echo("Aborting...<br>\n");
        f_exit();
        break;
      }
    case 512LL:
      {
        echo(LINE(23,concat5("<b>ERROR</b> [", toString(v_errno), "] ", toString(v_errstr), "<br>\n")));
        break;
      }
    case 1024LL:
      {
        echo(LINE(26,concat5("<b>WARNING</b> [", toString(v_errno), "] ", toString(v_errstr), "<br>\n")));
        break;
      }
    default:
      {
        echo(LINE(29,concat5("Unkown error type: [", toString(v_errno), "] ", toString(v_errstr), "<br>\n")));
        break;
      }
    }
  }
} /* function */
Variant i_myerrorhandler(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x32D66CB21D0498C5LL, myerrorhandler) {
    return (f_myerrorhandler(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$zerrors_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/zerrors.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$zerrors_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_old_error_handler __attribute__((__unused__)) = (variables != gVariables) ? variables->get("old_error_handler") : g->GV(old_error_handler);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  ;
  ;
  ;
  LINE(9,x_error_reporting(toInt32(1792LL)));
  (v_old_error_handler = LINE(53,x_set_error_handler("myErrorHandler")));
  echo("vector a\n");
  (v_a = ScalarArrays::sa_[0]);
  LINE(58,x_print_r(v_a));
  echo("----\nvector b - a warning (b = log(PI) * a)\n");
  (v_b = LINE(62,f_scale_by_log(v_a, 3.1415926535898002 /* M_PI */)));
  LINE(63,x_print_r(v_b));
  echo("----\nvector c - an error\n");
  (v_c = LINE(67,f_scale_by_log("not array", 2.2999999999999998)));
  LINE(68,x_var_dump(1, v_c));
  echo("----\nvector d - fatal error\n");
  (v_d = LINE(72,f_scale_by_log(v_a, -2.5)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
