
#ifndef __GENERATED_php_roadsend_tests_bug_id_0000912_h__
#define __GENERATED_php_roadsend_tests_bug_id_0000912_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0000912.fw.h>

// Declarations
#include <cls/oneclass.h>
#include <cls/twoclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0000912_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_oneclass(CArrRef params, bool init = true);
Object co_twoclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0000912_h__
