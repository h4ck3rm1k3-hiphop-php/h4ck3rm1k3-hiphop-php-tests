
#include <php/roadsend/tests/bug-id-0000912.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000912.php line 4 */
Variant c_oneclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_oneclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_oneclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_oneclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_oneclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_oneclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_oneclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_oneclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(oneclass)
ObjectData *c_oneclass::cloneImpl() {
  c_oneclass *obj = NEW(c_oneclass)();
  cloneSet(obj);
  return obj;
}
void c_oneclass::cloneSet(c_oneclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_oneclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x4F38A775A0938899LL, a) {
        return (t_a(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_oneclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x4F38A775A0938899LL, a) {
        return (t_a(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_oneclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_oneclass$os_get(const char *s) {
  return c_oneclass::os_get(s, -1);
}
Variant &cw_oneclass$os_lval(const char *s) {
  return c_oneclass::os_lval(s, -1);
}
Variant cw_oneclass$os_constant(const char *s) {
  return c_oneclass::os_constant(s);
}
Variant cw_oneclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_oneclass::os_invoke(c, s, params, -1, fatal);
}
void c_oneclass::init() {
}
/* SRC: roadsend/tests/bug-id-0000912.php line 5 */
void c_oneclass::t_a(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(oneclass, oneclass::a);
  echo(LINE(6,concat3("two a ", toString(v_b), "\n")));
} /* function */
/* SRC: roadsend/tests/bug-id-0000912.php line 10 */
Variant c_twoclass::os_get(const char *s, int64 hash) {
  return c_oneclass::os_get(s, hash);
}
Variant &c_twoclass::os_lval(const char *s, int64 hash) {
  return c_oneclass::os_lval(s, hash);
}
void c_twoclass::o_get(ArrayElementVec &props) const {
  c_oneclass::o_get(props);
}
bool c_twoclass::o_exists(CStrRef s, int64 hash) const {
  return c_oneclass::o_exists(s, hash);
}
Variant c_twoclass::o_get(CStrRef s, int64 hash) {
  return c_oneclass::o_get(s, hash);
}
Variant c_twoclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_oneclass::o_set(s, hash, v, forInit);
}
Variant &c_twoclass::o_lval(CStrRef s, int64 hash) {
  return c_oneclass::o_lval(s, hash);
}
Variant c_twoclass::os_constant(const char *s) {
  return c_oneclass::os_constant(s);
}
IMPLEMENT_CLASS(twoclass)
ObjectData *c_twoclass::cloneImpl() {
  c_twoclass *obj = NEW(c_twoclass)();
  cloneSet(obj);
  return obj;
}
void c_twoclass::cloneSet(c_twoclass *clone) {
  c_oneclass::cloneSet(clone);
}
Variant c_twoclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x4F38A775A0938899LL, a) {
        return (t_a(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_oneclass::o_invoke(s, params, hash, fatal);
}
Variant c_twoclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x4F38A775A0938899LL, a) {
        return (t_a(a0), null);
      }
      break;
    default:
      break;
  }
  return c_oneclass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_twoclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_oneclass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_twoclass$os_get(const char *s) {
  return c_twoclass::os_get(s, -1);
}
Variant &cw_twoclass$os_lval(const char *s) {
  return c_twoclass::os_lval(s, -1);
}
Variant cw_twoclass$os_constant(const char *s) {
  return c_twoclass::os_constant(s);
}
Variant cw_twoclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_twoclass::os_invoke(c, s, params, -1, fatal);
}
void c_twoclass::init() {
  c_oneclass::init();
}
/* SRC: roadsend/tests/bug-id-0000912.php line 11 */
void c_twoclass::t_a(CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(twoclass, twoclass::a);
  echo(LINE(12,concat3("one a ", toString(v_b), "\n")));
  {
    Variant tmp2 = (v_b);
    int tmp3 = -1;
    if (equal(tmp2, ("meek"))) {
      tmp3 = 0;
    } else if (equal(tmp2, ("blah"))) {
      tmp3 = 1;
    }
    switch (tmp3) {
    case 0:
      {
        LINE(15,c_oneclass::t_a(v_b));
        goto break1;
      }
    case 1:
      {
        echo("hi");
        goto break1;
      }
    }
    break1:;
  }
} /* function */
Object co_oneclass(CArrRef params, bool init /* = true */) {
  return Object(p_oneclass(NEW(c_oneclass)())->dynCreate(params, init));
}
Object co_twoclass(CArrRef params, bool init /* = true */) {
  return Object(p_twoclass(NEW(c_twoclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000912_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000912.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000912_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("parent:: doesn't work inside a switch\n");
  (v_a = ((Object)(LINE(24,p_twoclass(p_twoclass(NEWOBJ(c_twoclass)())->create())))));
  print("that line\n");
  LINE(28,v_a.o_invoke_few_args("a", 0x4F38A775A0938899LL, 1, "meek"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
