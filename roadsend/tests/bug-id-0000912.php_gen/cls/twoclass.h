
#ifndef __GENERATED_cls_twoclass_h__
#define __GENERATED_cls_twoclass_h__

#include <cls/oneclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0000912.php line 10 */
class c_twoclass : virtual public c_oneclass {
  BEGIN_CLASS_MAP(twoclass)
    PARENT_CLASS(oneclass)
  END_CLASS_MAP(twoclass)
  DECLARE_CLASS(twoclass, twoclass, oneclass)
  void init();
  public: void t_a(CVarRef v_b);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_twoclass_h__
