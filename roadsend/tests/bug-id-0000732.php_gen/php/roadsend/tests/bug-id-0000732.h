
#ifndef __GENERATED_php_roadsend_tests_bug_id_0000732_h__
#define __GENERATED_php_roadsend_tests_bug_id_0000732_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0000732.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

int64 f_azend_version();
Variant pm_php$roadsend$tests$bug_id_0000732_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_afunction_exists(CStrRef v_a);
String f_aversion_compare(int64 v_a, CStrRef v_b, CStrRef v_c);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0000732_h__
