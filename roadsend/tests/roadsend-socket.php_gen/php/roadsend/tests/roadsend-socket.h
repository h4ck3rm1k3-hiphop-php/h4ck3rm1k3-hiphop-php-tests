
#ifndef __GENERATED_php_roadsend_tests_roadsend_socket_h__
#define __GENERATED_php_roadsend_tests_roadsend_socket_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/roadsend-socket.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$roadsend_socket_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_roadsend_socket_h__
