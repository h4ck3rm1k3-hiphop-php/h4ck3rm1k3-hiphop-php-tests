
#ifndef __GENERATED_cls_sm_smtag_areak_h__
#define __GENERATED_cls_sm_smtag_areak_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/finc11.php line 23 */
class c_sm_smtag_areak : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_areak)
  END_CLASS_MAP(sm_smtag_areak)
  DECLARE_CLASS(sm_smtag_areak, SM_smTag_AREAk, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_areak_h__
