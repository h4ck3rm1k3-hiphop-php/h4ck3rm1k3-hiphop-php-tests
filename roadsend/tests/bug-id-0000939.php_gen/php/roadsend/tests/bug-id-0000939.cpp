
#include <php/roadsend/tests/bug-id-0000939.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000939_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000939.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000939_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("unset() needs to take variable number of arguments\n\n");
  (v_a = "hi");
  (v_b = "yo");
  (v_c = "man!");
  echo(LINE(9,concat6(toString(v_a), ", ", toString(v_b), ", ", toString(v_c), "\n")));
  {
    unset(v_a);
    unset(v_b);
    unset(v_c);
  }
  echo(LINE(11,concat6(toString(v_a), ", ", toString(v_b), ", ", toString(v_c), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
