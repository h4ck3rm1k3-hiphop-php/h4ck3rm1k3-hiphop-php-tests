
#include <php/roadsend/tests/fannkuch.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/fannkuch.php line 10 */
Variant f_fannkuch(CVarRef v_n) {
  FUNCTION_INJECTION(Fannkuch);
  int64 v_check = 0;
  Variant v_perm;
  Variant v_perm1;
  Variant v_count;
  Variant v_maxPerm;
  int64 v_maxFlipsCount = 0;
  Numeric v_m = 0;
  int64 v_i = 0;
  Variant v_r;
  int64 v_flipsCount = 0;
  Variant v_k;
  int64 v_k2 = 0;
  Variant v_temp;
  Variant v_perm0;
  int64 v_j = 0;

  (v_check = 0LL);
  (v_perm = ScalarArrays::sa_[0]);
  (v_perm1 = ScalarArrays::sa_[0]);
  (v_count = ScalarArrays::sa_[0]);
  (v_maxPerm = ScalarArrays::sa_[0]);
  (v_maxFlipsCount = 0LL);
  (v_m = v_n - 1LL);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      v_perm1.set(v_i, (v_i));
    }
  }
  (v_r = v_n);
  LOOP_COUNTER(2);
  {
    while (true) {
      LOOP_COUNTER_CHECK(2);
      {
        if (less(v_check, 30LL)) {
          {
            LOOP_COUNTER(3);
            for ((v_i = 0LL); less(v_i, v_n); v_i++) {
              LOOP_COUNTER_CHECK(3);
              echo(toString(v_perm1.rvalAt(v_i) + 1LL));
            }
          }
          echo("\n");
          v_check++;
        }
        LOOP_COUNTER(4);
        {
          while (!equal(v_r, 1LL)) {
            LOOP_COUNTER_CHECK(4);
            {
              v_count.set(v_r - 1LL, (v_r));
              v_r--;
            }
          }
        }
        if (!((equal(v_perm1.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), 0LL) || equal(v_perm1.rvalAt(v_m), v_m)))) {
          {
            LOOP_COUNTER(5);
            for ((v_i = 0LL); less(v_i, v_n); v_i++) {
              LOOP_COUNTER_CHECK(5);
              v_perm.set(v_i, (v_perm1.rvalAt(v_i)));
            }
          }
          (v_flipsCount = 0LL);
          LOOP_COUNTER(6);
          {
            while (!((equal(((v_k = v_perm.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))), 0LL)))) {
              LOOP_COUNTER_CHECK(6);
              {
                (v_k2 = toInt64((toInt64(v_k + 1LL))) >> 1LL);
                {
                  LOOP_COUNTER(7);
                  for ((v_i = 0LL); less(v_i, v_k2); v_i++) {
                    LOOP_COUNTER_CHECK(7);
                    {
                      (v_temp = v_perm.rvalAt(v_i));
                      v_perm.set(v_i, (v_perm.rvalAt(v_k - v_i)));
                      v_perm.set(v_k - v_i, (v_temp));
                    }
                  }
                }
                v_flipsCount++;
              }
            }
          }
          if (more(v_flipsCount, v_maxFlipsCount)) {
            (v_maxFlipsCount = v_flipsCount);
            {
              LOOP_COUNTER(8);
              for ((v_i = 0LL); less(v_i, v_n); v_i++) {
                LOOP_COUNTER_CHECK(8);
                v_maxPerm.set(v_i, (v_perm1.rvalAt(v_i)));
              }
            }
          }
        }
        LOOP_COUNTER(9);
        {
          while (true) {
            LOOP_COUNTER_CHECK(9);
            {
              if (equal(v_r, v_n)) return v_maxFlipsCount;
              (v_perm0 = v_perm1.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
              (v_i = 0LL);
              LOOP_COUNTER(10);
              {
                while (less(v_i, v_r)) {
                  LOOP_COUNTER_CHECK(10);
                  {
                    (v_j = v_i + 1LL);
                    v_perm1.set(v_i, (v_perm1.rvalAt(v_j)));
                    (v_i = v_j);
                  }
                }
              }
              v_perm1.set(v_r, (v_perm0));
              v_count.set(v_r, (v_count.rvalAt(v_r) - 1LL));
              if (more(v_count.rvalAt(v_r), 0LL)) break;
              v_r++;
            }
          }
        }
      }
    }
  }
  return null;
} /* function */
Variant pm_php$roadsend$tests$fannkuch_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/fannkuch.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$fannkuch_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = 9LL);
  LINE(68,(assignCallTemp(eo_1, v_n),assignCallTemp(eo_2, f_fannkuch(v_n)),x_printf(3, "Pfannkuchen(%d) = %d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
