
#ifndef __GENERATED_php_roadsend_tests_fannkuch_h__
#define __GENERATED_php_roadsend_tests_fannkuch_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/fannkuch.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$fannkuch_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_fannkuch(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_fannkuch_h__
