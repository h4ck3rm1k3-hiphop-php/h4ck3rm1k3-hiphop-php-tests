
#include <php/roadsend/tests/parseini.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$parseini_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/parseini.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$parseini_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cfile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cfile") : g->GV(cfile);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_cfile = "/etc/php/cli-php5/php.ini");
  if (LINE(5,x_file_exists(toString(v_cfile)))) {
    (v_a = LINE(6,x_parse_ini_file(toString(v_cfile))));
    LINE(7,x_var_dump(1, v_a));
  }
  else {
    echo("where is php.ini\?\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
