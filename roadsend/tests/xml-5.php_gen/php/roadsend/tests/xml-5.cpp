
#include <php/roadsend/tests/xml-5.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/xml-5.php line 20 */
void f_endelement(CVarRef v_parser, CVarRef v_name) {
  FUNCTION_INJECTION(endElement);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_depth __attribute__((__unused__)) = g->GV(depth);
  gv_depth--;
} /* function */
/* SRC: roadsend/tests/xml-5.php line 10 */
void f_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs) {
  FUNCTION_INJECTION(startElement);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_depth __attribute__((__unused__)) = g->GV(depth);
  int64 v_i = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, gv_depth); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        print("  ");
      }
    }
  }
  print(toString(v_name) + toString("\n"));
  gv_depth++;
  LINE(17,x_print_r(v_attrs));
} /* function */
Variant i_endelement(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x78A7A5D96EF2F4AFLL, endelement) {
    return (f_endelement(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_startelement(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x55BFFBE5717EFD62LL, startelement) {
    return (f_startelement(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$xml_5_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/xml-5.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$xml_5_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);
  Variant &v_depth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("depth") : g->GV(depth);
  Variant &v_xml_parser __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xml_parser") : g->GV(xml_parser);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);

  (v_file = concat(toString(LINE(6,x_getenv("PCC_HOME"))), "/runtime/ext/xml/tests/data.xml"));
  echo(LINE(7,concat3("working with ", toString(v_file), "\n")));
  (v_depth = 0LL);
  (v_xml_parser = LINE(25,x_xml_parser_create()));
  LINE(27,x_xml_set_element_handler(toObject(v_xml_parser), "startElement", "endElement"));
  if (!((toBoolean((v_fp = LINE(28,x_fopen(toString(v_file), "r"))))))) {
    f_exit("could not open XML input");
  }
  LOOP_COUNTER(2);
  {
    while (toBoolean((v_data = LINE(32,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(2);
      {
        if (!(toBoolean(LINE(33,(assignCallTemp(eo_0, toObject(v_xml_parser)),assignCallTemp(eo_1, toString(v_data)),assignCallTemp(eo_2, x_feof(toObject(v_fp))),x_xml_parse(eo_0, eo_1, eo_2)))))) {
          f_exit(LINE(36,(assignCallTemp(eo_1, LINE(35,x_xml_error_string(x_xml_get_error_code(toObject(v_xml_parser))))),assignCallTemp(eo_2, LINE(36,x_xml_get_current_line_number(toObject(v_xml_parser)))),x_sprintf(3, "XML error: %s at line %d", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create())))));
        }
        print(LINE(38,(assignCallTemp(eo_1, toString(x_xml_get_current_line_number(toObject(v_xml_parser)))),concat3("on line: ", eo_1, "\n"))));
        print(LINE(39,(assignCallTemp(eo_1, toString(x_xml_get_current_column_number(toObject(v_xml_parser)))),concat3("on col: ", eo_1, "\n"))));
        print(LINE(40,(assignCallTemp(eo_1, toString(x_xml_get_current_byte_index(toObject(v_xml_parser)))),concat3("on byte: ", eo_1, "\n"))));
      }
    }
  }
  LINE(43,x_xml_parser_free(toObject(v_xml_parser)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
