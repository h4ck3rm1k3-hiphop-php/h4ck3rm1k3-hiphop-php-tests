
#ifndef __GENERATED_php_roadsend_tests_xml_5_h__
#define __GENERATED_php_roadsend_tests_xml_5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/xml-5.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$xml_5_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_endelement(CVarRef v_parser, CVarRef v_name);
void f_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_xml_5_h__
