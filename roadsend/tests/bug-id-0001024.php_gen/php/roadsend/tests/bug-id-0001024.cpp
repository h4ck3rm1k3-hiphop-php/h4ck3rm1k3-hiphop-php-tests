
#include <php/roadsend/tests/bug-id-0001024.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001024.php line 9 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("directive", m_directive));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x26998912FB5DF57ALL, directive, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x26998912FB5DF57ALL, m_directive,
                         directive, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x26998912FB5DF57ALL, m_directive,
                      directive, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_directive = m_directive;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x2A3D897C160973DFLL, runit) {
        return (t_runit(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x2A3D897C160973DFLL, runit) {
        return (t_runit(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_directive = ScalarArrays::sa_[0];
}
/* SRC: roadsend/tests/bug-id-0001024.php line 11 */
void c_aclass::t_runit(CVarRef v_a) {
  INSTANCE_METHOD_INJECTION(aclass, aclass::runit);
  Variant v_b;

  (v_b = LINE(12,p_bclass(p_bclass(NEWOBJ(c_bclass)())->create())));
  lval(m_directive.lvalAt("obj", 0x7FB577570F61BD03LL)).set(v_a, (ref(v_b)));
} /* function */
/* SRC: roadsend/tests/bug-id-0001024.php line 5 */
Variant c_bclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("str", m_str));
  c_ObjectData::o_get(props);
}
bool c_bclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x362158638F83BD9CLL, str, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x362158638F83BD9CLL, m_str,
                         str, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_bclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x362158638F83BD9CLL, m_str,
                      str, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bclass)
ObjectData *c_bclass::cloneImpl() {
  c_bclass *obj = NEW(c_bclass)();
  cloneSet(obj);
  return obj;
}
void c_bclass::cloneSet(c_bclass *clone) {
  clone->m_str = m_str;
  ObjectData::cloneSet(clone);
}
Variant c_bclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bclass$os_get(const char *s) {
  return c_bclass::os_get(s, -1);
}
Variant &cw_bclass$os_lval(const char *s) {
  return c_bclass::os_lval(s, -1);
}
Variant cw_bclass$os_constant(const char *s) {
  return c_bclass::os_constant(s);
}
Variant cw_bclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bclass::os_invoke(c, s, params, -1, fatal);
}
void c_bclass::init() {
  m_str = "hi there";
}
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Object co_bclass(CArrRef params, bool init /* = true */) {
  return Object(p_bclass(NEW(c_bclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0001024_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001024.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001024_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("0001024 unable to assign a reference to class property hash if the key doesn't already exist\n\n");
  (v_a = ((Object)(LINE(19,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  LINE(20,v_a.o_invoke_few_args("runit", 0x2A3D897C160973DFLL, 1, "akey"));
  echo(toString(v_a.o_get("directive", 0x26998912FB5DF57ALL).rvalAt("obj", 0x7FB577570F61BD03LL).rvalAt("akey", 0x3D81666F9D5A61A0LL).o_get("str", 0x362158638F83BD9CLL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
