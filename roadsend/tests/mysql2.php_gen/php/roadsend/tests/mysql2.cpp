
#include <php/roadsend/tests/mysql2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/mysql2.php line 10 */
void f_domysql() {
  FUNCTION_INJECTION(domysql);
  String v_query;
  Variant v_result;
  Variant v_line;
  Primitive v_col_name = 0;
  Variant v_col_value;

  (v_query = "SELECT * FROM my_table");
  (toBoolean((v_result = LINE(15,x_mysql_query(v_query))))) || (toBoolean(f_exit("Query failed")));
  print("<table>\n");
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_line = LINE(20,x_mysql_fetch_array(v_result, toInt32(3LL) /* MYSQL_BOTH */))))) {
      LOOP_COUNTER_CHECK(1);
      {
        print("\t<tr>\n");
        {
          LOOP_COUNTER(2);
          for (ArrayIterPtr iter4 = v_line.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_col_value = iter4->second();
            v_col_name = iter4->first();
            {
              print(LINE(23,concat5("\t\t<td>", toString(v_col_name), " => ", toString(v_col_value), "</td>\n")));
            }
          }
        }
        print("\t</tr>\n");
      }
    }
  }
  print("</table>\n");
  LINE(30,x_mysql_free_result(v_result));
} /* function */
Variant pm_php$roadsend$tests$mysql2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/mysql2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$mysql2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (toBoolean((v_link = LINE(3,x_mysql_connect("dbserver", "develUser", "d3v3lpa55"))))) || (toBoolean(f_exit("Could not connect")));
  print("Connected successfully\n");
  (toBoolean(LINE(7,x_mysql_select_db("test")))) || (toBoolean(f_exit("Could not select database")));
  {
    LOOP_COUNTER(5);
    for ((v_i = 0LL); less(v_i, 1000LL); v_i++) {
      LOOP_COUNTER_CHECK(5);
      {
        LINE(35,f_domysql());
      }
    }
  }
  LINE(39,x_mysql_close(v_link));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
