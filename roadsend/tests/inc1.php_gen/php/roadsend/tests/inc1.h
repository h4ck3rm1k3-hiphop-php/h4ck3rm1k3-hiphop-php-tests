
#ifndef __GENERATED_php_roadsend_tests_inc1_h__
#define __GENERATED_php_roadsend_tests_inc1_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc1.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc1_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_inc1_function(CVarRef v_a, CVarRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc1_h__
