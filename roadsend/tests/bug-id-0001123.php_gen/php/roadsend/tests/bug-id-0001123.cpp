
#include <php/roadsend/tests/bug-id-0001123.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0001123.php line 2 */
String f_test(CStrRef v_str) {
  FUNCTION_INJECTION(test);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_a;
  Variant v_b;
  String v_res;

  (v_a = LINE(3,x_base64_encode(v_str)));
  echo(LINE(4,concat3("a ", v_a, "\n")));
  (v_b = LINE(5,x_base64_decode(v_a)));
  echo(LINE(6,concat3("b ", toString(v_b), "\n")));
  echo(LINE(7,(assignCallTemp(eo_1, x_md5(toString(v_b))),concat3("c ", eo_1, "\n"))));
  (v_res = concat(LINE(8,x_md5(toString(x_base64_decode(x_base64_encode(v_str))))), "\n"));
  return v_res;
} /* function */
Variant pm_php$roadsend$tests$bug_id_0001123_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001123.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001123_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 256LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(concat(LINE(13,x_base64_encode(concat("messafe diges", x_chr(toInt64(v_i))))), "\n"));
      }
    }
  }
  echo(LINE(17,f_test("")));
  echo(LINE(18,f_test("a")));
  echo(LINE(19,f_test("abc")));
  echo(LINE(20,f_test("message digest")));
  echo(LINE(21,f_test("abcdefghijklmnopqrstuvwxyz")));
  echo(LINE(22,f_test("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")));
  echo(LINE(23,f_test("12345678901234567890123456789012345678901234567890123456789012345678901234567890")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
