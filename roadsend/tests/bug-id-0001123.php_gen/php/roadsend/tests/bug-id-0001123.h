
#ifndef __GENERATED_php_roadsend_tests_bug_id_0001123_h__
#define __GENERATED_php_roadsend_tests_bug_id_0001123_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/bug-id-0001123.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$bug_id_0001123_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_test(CStrRef v_str);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_bug_id_0001123_h__
