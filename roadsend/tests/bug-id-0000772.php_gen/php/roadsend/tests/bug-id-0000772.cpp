
#include <php/roadsend/tests/bug-id-0000772.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000772_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000772.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000772_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);

  (v_s = "hello");
  echo(LINE(4,concat3("thus should be 'hello' --> '", toString(v_s), "'\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
