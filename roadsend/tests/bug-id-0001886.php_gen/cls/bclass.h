
#ifndef __GENERATED_cls_bclass_h__
#define __GENERATED_cls_bclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0001886.php line 15 */
class c_bclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(bclass)
  END_CLASS_MAP(bclass)
  DECLARE_CLASS(bclass, bclass, ObjectData)
  void init();
  public: Variant m_id;
  public: Variant m_parent;
  public: void t_bclass(CVarRef v_id, Variant v_parent);
  public: ObjectData *create(CVarRef v_id, Variant v_parent);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bclass_h__
