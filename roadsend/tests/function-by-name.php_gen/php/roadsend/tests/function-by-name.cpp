
#include <php/roadsend/tests/function-by-name.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/function-by-name.php line 17 */
void f_fun_with_ref_arg(CVarRef v_foo, Variant v_bar) {
  FUNCTION_INJECTION(fun_with_ref_arg);
  echo(LINE(18,concat5("called with ", toString(v_foo), ", ", toString(v_bar), ".\n")));
  v_bar++;
} /* function */
/* SRC: roadsend/tests/function-by-name.php line 5 */
void f_mofun(CVarRef v_moarg) {
  FUNCTION_INJECTION(mofun);
  echo(LINE(6,concat3("called with ", toString(v_moarg), ".\n")));
} /* function */
/* SRC: roadsend/tests/function-by-name.php line 28 */
void f_fun_with_opt_arg(CVarRef v_foo, CVarRef v_bar, CVarRef v_baz //  = 2LL
) {
  FUNCTION_INJECTION(fun_with_opt_arg);
  echo(concat("called with ", LINE(29,concat6(toString(v_foo), ", ", toString(v_bar), ", ", toString(v_baz), ".\n"))));
} /* function */
Variant i_fun_with_ref_arg(CArrRef params) {
  return (f_fun_with_ref_arg(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))), null);
}
Variant i_mofun(CArrRef params) {
  return (f_mofun(params.rvalAt(0)), null);
}
Variant i_fun_with_opt_arg(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x046B99C7EBA4509BLL, fun_with_opt_arg) {
    int count = params.size();
    if (count <= 2) return (f_fun_with_opt_arg(params.rvalAt(0), params.rvalAt(1)), null);
    return (f_fun_with_opt_arg(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$function_by_name_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/function-by-name.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$function_by_name_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fun __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fun") : g->GV(fun);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_fun = "mofun");
  LINE(11,invoke(toString(v_fun), Array(ArrayInit(1).set(0, "does this work\?").create()), -1));
  (v_bar = 3LL);
  (v_fun = "fun_with_ref_arg");
  LINE(25,invoke(toString(v_fun), Array(ArrayInit(2).set(0, "trash").set(1, ref(v_bar)).create()), -1));
  echo(LINE(26,concat3("bar is now ", toString(v_bar), "\n")));
  (v_fun = "fun_with_opt_arg");
  LINE(35,invoke(toString(v_fun), Array(ArrayInit(2).set(0, "trash").set(1, "bork").create()), -1));
  LINE(36,invoke(toString(v_fun), Array(ArrayInit(3).set(0, "trash").set(1, "bork").set(2, ref(v_bar)).create()), -1));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
