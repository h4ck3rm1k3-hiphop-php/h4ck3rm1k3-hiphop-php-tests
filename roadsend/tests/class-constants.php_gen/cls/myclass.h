
#ifndef __GENERATED_cls_myclass_h__
#define __GENERATED_cls_myclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/class-constants.php line 2 */
class c_myclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass)
  END_CLASS_MAP(myclass)
  DECLARE_CLASS(myclass, MyClass, ObjectData)
  void init();
  public: void t_showconstant();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass_h__
