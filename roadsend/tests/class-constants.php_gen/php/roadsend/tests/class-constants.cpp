
#include <php/roadsend/tests/class-constants.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/class-constants.php line 2 */
const StaticString q_myclass_constant = "constant value";
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x639D3D392D1AA971LL, q_myclass_constant, constant);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x408225742B2601EELL, showconstant) {
        return (t_showconstant(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x408225742B2601EELL, showconstant) {
        return (t_showconstant(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
}
/* SRC: roadsend/tests/class-constants.php line 6 */
void c_myclass::t_showconstant() {
  INSTANCE_METHOD_INJECTION(MyClass, MyClass::showConstant);
  echo("constant value\n");
} /* function */
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$class_constants_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/class-constants.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$class_constants_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_class __attribute__((__unused__)) = (variables != gVariables) ? variables->get("class") : g->GV(class);

  echo("constant value\n");
  (v_class = ((Object)(LINE(13,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  LINE(14,v_class.o_invoke_few_args("showConstant", 0x408225742B2601EELL, 0));
  echo(" ");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
