
#include <php/roadsend/tests/bug-id-0001110.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001110_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001110.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001110_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("==Mixed==\n");
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        LINE(7,x_var_dump(1, v_k));
        LINE(8,x_var_dump(1, v_v));
      }
    }
  }
  echo("==Normal==\n");
  (v_b = ScalarArrays::sa_[1]);
  v_b.append(("a"));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_b.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_k = iter6->first();
      {
        LINE(16,x_var_dump(1, v_k));
        LINE(17,x_var_dump(1, v_v));
      }
    }
  }
  echo("==Negative==\n");
  (v_c = ScalarArrays::sa_[2]);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_c.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_v = iter9->second();
      v_k = iter9->first();
      {
        LINE(24,x_var_dump(1, v_k));
        LINE(25,x_var_dump(1, v_v));
      }
    }
  }
  echo("==Done==\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
