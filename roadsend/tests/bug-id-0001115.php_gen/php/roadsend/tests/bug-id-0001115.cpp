
#include <php/roadsend/tests/bug-id-0001115.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001115_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001115.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001115_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_var_dump(1, x_range("2003", "2004")));
  LINE(3,x_var_dump(1, x_range("a", "z")));
  LINE(4,x_var_dump(1, x_range("1", "10")));
  LINE(5,x_var_dump(1, x_range("", "1")));
  LINE(6,x_var_dump(1, x_range(1LL, "")));
  LINE(7,x_var_dump(1, x_range(1LL, ScalarArrays::sa_[0])));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
