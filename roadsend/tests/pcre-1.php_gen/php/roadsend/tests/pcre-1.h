
#ifndef __GENERATED_php_roadsend_tests_pcre_1_h__
#define __GENERATED_php_roadsend_tests_pcre_1_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/pcre-1.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$pcre_1_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_pcre_1_h__
