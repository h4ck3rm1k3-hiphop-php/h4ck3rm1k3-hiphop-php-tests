
#include <php/roadsend/tests/ref3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/ref3.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$ref3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/ref3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$ref3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  v_a.append((((Object)(LINE(6,p_foo(p_foo(NEWOBJ(c_foo)())->create()))))));
  v_a.append((LINE(7,p_foo(p_foo(NEWOBJ(c_foo)())->create()))));
  (v_b = ((Object)(LINE(9,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  (v_c = LINE(10,p_foo(p_foo(NEWOBJ(c_foo)())->create())));
  (v_d = ((Object)(LINE(11,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  (v_d.o_lval("a", 0x4292CEE227B9150ALL) = v_b);
  (v_d.o_lval("b", 0x08FBB133F8576BD5LL) = v_c);
  (v_d.o_lval("a1", 0x68DF81F26D942FC7LL) = ref(v_b));
  (v_d.o_lval("a2", 0x6C05C2858C72A876LL) = ref(v_c));
  lval(v_d.o_lval("a3", 0x43EDA7BEE714570DLL)).append((v_a));
  lval(v_d.o_lval("a4", 0x1DC175F7E4343259LL)).append((v_b));
  lval(v_d.o_lval("a5", 0x4555E94E338480E0LL)).append((v_c));
  lval(v_d.o_lval("a6", 0x598AD62830EAF1E9LL)).append((ref(v_a)));
  lval(v_d.o_lval("a7", 0x180CCF01FBC81F18LL)).append((ref(v_b)));
  lval(v_d.o_lval("a8", 0x00FF5C07497953F4LL)).append((ref(v_c)));
  LINE(24,x_var_dump(1, v_a));
  LINE(25,x_var_dump(1, v_b));
  LINE(26,x_var_dump(1, v_c));
  LINE(27,x_var_dump(1, v_d));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
