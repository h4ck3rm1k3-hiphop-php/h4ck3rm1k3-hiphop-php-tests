
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__

#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/bug-id-0001916.php line 14 */
class c_bar : virtual public c_foo {
  BEGIN_CLASS_MAP(bar)
    PARENT_CLASS(foo)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, bar, foo)
  void init();
  public: void t_bar();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
