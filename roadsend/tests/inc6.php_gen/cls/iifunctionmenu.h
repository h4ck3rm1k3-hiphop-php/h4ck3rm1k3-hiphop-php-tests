
#ifndef __GENERATED_cls_iifunctionmenu_h__
#define __GENERATED_cls_iifunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/inc6.php line 22 */
class c_iifunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(iifunctionmenu)
  END_CLASS_MAP(iifunctionmenu)
  DECLARE_CLASS(iifunctionmenu, iiFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_iifunctionmenu_h__
