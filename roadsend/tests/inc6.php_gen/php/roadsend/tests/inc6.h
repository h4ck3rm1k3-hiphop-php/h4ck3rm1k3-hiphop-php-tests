
#ifndef __GENERATED_php_roadsend_tests_inc6_h__
#define __GENERATED_php_roadsend_tests_inc6_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc6.fw.h>

// Declarations
#include <cls/iifunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc6_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_iifunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc6_h__
