
#include <php/roadsend/tests/ary2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/ary2.php line 10 */
void f_ary2(CVarRef v_n) {
  FUNCTION_INJECTION(ary2);
  Numeric v_i = 0;
  Sequence v_X;
  Sequence v_Y;
  Numeric v_last = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); ) {
      LOOP_COUNTER_CHECK(1);
      {
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
        v_X.set(v_i, (v_i));
        ++v_i;
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = v_n - 1LL); not_less(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(2);
      {
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
        v_Y.set(v_i, (v_X.rvalAt(v_i)));
        --v_i;
      }
    }
  }
  (v_last = v_n - 1LL);
  print(toString(v_Y.rvalAt(v_last)) + toString("\n"));
} /* function */
Variant pm_php$roadsend$tests$ary2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/ary2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$ary2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(100000LL)));
  LINE(8,f_ary2(v_n));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
