
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_upper;
extern const StaticString k_MID;
extern const StaticString k_DYNAMIC_CONSTANT;
extern const StaticString k_ACONSTANT;
extern const StaticString k_LOWER;
extern const StaticString k_SENSIBLE;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 31) {
    case 10:
      HASH_RETURN(0x5F335E6DCFA569EALL, g->k_UPPER, UPPER);
      HASH_RETURN(0x35DE1F19273116CALL, g->k_sensible, sensible);
      break;
    case 11:
      HASH_RETURN(0x733A634F298D78EBLL, k_upper, upper);
      break;
    case 12:
      HASH_RETURN(0x710BF2861AF1808CLL, g->k_MId, MId);
      break;
    case 13:
      HASH_RETURN(0x25C1E3FDCA9D34EDLL, k_MID, MID);
      break;
    case 26:
      HASH_RETURN(0x730C21608765381ALL, g->k_lower, lower);
      HASH_RETURN(0x7ED796E1D2F7945ALL, k_ACONSTANT, ACONSTANT);
      break;
    case 30:
      HASH_RETURN(0x2F97CD67688AF95ELL, k_LOWER, LOWER);
      break;
    case 31:
      HASH_RETURN(0x7C1C8A56700DF17FLL, k_SENSIBLE, SENSIBLE);
      HASH_RETURN(0x1875F6178B5CC13FLL, k_DYNAMIC_CONSTANT, DYNAMIC_CONSTANT);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
