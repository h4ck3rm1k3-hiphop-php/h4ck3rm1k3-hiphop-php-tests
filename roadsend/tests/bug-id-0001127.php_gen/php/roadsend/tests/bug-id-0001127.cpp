
#include <php/roadsend/tests/bug-id-0001127.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001127_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001127.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001127_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);
  Variant &v_x2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x2") : g->GV(x2);
  Variant &v_base __attribute__((__unused__)) = (variables != gVariables) ? variables->get("base") : g->GV(base);

  {
    LOOP_COUNTER(1);
    for ((v_x = 0LL), (v_count = 0LL); less(v_x, 200LL); v_x++) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_x2 = toInt64(LINE(4,x_exp(x_log(toDouble(v_x))))));
        if ((less(v_x2, (v_x + 2LL))) && (more(v_x2, (v_x - 2LL)))) {
          v_count++;
        }
        else {
          print(LINE(9,concat4(toString(v_x), " : ", toString(v_x2), "\n")));
        }
      }
    }
  }
  print(concat(toString(v_count), "\n"));
  {
    LOOP_COUNTER(2);
    for ((v_base = 2LL); less(v_base, 11LL); v_base++) {
      LOOP_COUNTER_CHECK(2);
      {
        {
          LOOP_COUNTER(3);
          for ((v_x = 0LL), (v_count = 0LL); less(v_x, 50LL); v_x++) {
            LOOP_COUNTER_CHECK(3);
            {
              (v_x2 = toInt64(LINE(18,(assignCallTemp(eo_0, v_base),assignCallTemp(eo_1, x_log(toDouble(v_x), toDouble(v_base))),x_pow(eo_0, eo_1)))));
              if ((less(v_x2, (v_x + 2LL))) && (more(v_x2, (v_x - 2LL)))) {
                v_count++;
              }
              else {
                print(concat("base ", LINE(23,concat6(toString(v_base), ": ", toString(v_x), " : ", toString(v_x2), "\n"))));
              }
            }
          }
        }
        print(concat(toString(v_count), "\n"));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
