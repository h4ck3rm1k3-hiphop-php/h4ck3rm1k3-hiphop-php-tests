
#include <php/roadsend/tests/version_compare.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$version_compare_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/version_compare.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$version_compare_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(toString(LINE(4,x_version_compare("4.0.4", "4.0.6"))));
  echo(toString(LINE(6,x_version_compare("4.3.2RC1", "4.0.6RC2"))));
  echo(toString(LINE(8,x_version_compare("4.0.6alpha", "4.0.6beta"))));
  echo(toString(LINE(10,x_version_compare("4.0.6-alpha", "4.0.6+beta"))));
  echo(toString(LINE(11,x_version_compare("4.1.0_test", "4.0.6.9_test2"))));
  echo(toString(LINE(13,x_version_compare("4.0.4", "4.0.6", "<"))));
  echo(toString(LINE(14,x_version_compare("4.0.6", "4.0.6", "eq"))));
  echo(toString(LINE(16,x_version_compare("5.2.1.4", "5.2.1.4.3"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
