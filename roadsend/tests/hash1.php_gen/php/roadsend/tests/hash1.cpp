
#include <php/roadsend/tests/hash1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$hash1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/hash1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$hash1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ahash __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ahash") : g->GV(ahash);

  v_ahash.set("1", ("foo"), 0x5BCA7C69B794F8CELL);
  echo(concat(toString(v_ahash.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), "\n"));
  echo(concat(toString(v_ahash.rvalAt("1", 0x5BCA7C69B794F8CELL)), "\n"));
  echo(toString(v_ahash.rvalAt(1, 0x5BCA7C69B794F8CELL)) + toString("\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
