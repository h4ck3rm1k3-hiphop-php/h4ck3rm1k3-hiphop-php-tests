
#include <php/roadsend/tests/num-test.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/num-test.php line 3 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  int64 v_i = 0;
  int64 v_j = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 1000000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_j += v_i * 2LL;
      }
    }
  }
  print(LINE(7,concat4(toString(v_j), ", ", toString(v_i), "\n")));
} /* function */
Variant pm_php$roadsend$tests$num_test_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/num-test.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$num_test_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(10,f_foo());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
