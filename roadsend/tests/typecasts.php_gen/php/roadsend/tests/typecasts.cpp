
#include <php/roadsend/tests/typecasts.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/typecasts.php line 112 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("aprop", m_aprop));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x572D6D4C827191C9LL, aprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x572D6D4C827191C9LL, m_aprop,
                         aprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x572D6D4C827191C9LL, m_aprop,
                      aprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_aprop = m_aprop;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_aprop = 12LL;
}
/* SRC: roadsend/tests/typecasts.php line 116 */
void c_aclass::t_foo() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::foo);
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$typecasts_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/typecasts.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$typecasts_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  print("Four scalar types:\n");
  print("***   boolean   ***\n");
  LINE(9,x_var_dump(1, false));
  LINE(10,x_var_dump(1, false));
  LINE(11,x_var_dump(1, false));
  LINE(12,x_var_dump(1, false));
  LINE(13,x_var_dump(1, false));
  LINE(14,x_var_dump(1, false));
  LINE(15,x_var_dump(1, false));
  LINE(16,x_var_dump(1, toBoolean(toArray(v_foo))));
  LINE(17,x_var_dump(1, toBoolean(toObject(v_foo))));
  LINE(18,x_var_dump(1, false));
  v_foo.set(1LL, (2LL), 0x5BCA7C69B794F8CELL);
  LINE(23,x_var_dump(1, true));
  LINE(24,x_var_dump(1, true));
  LINE(25,x_var_dump(1, true));
  LINE(26,x_var_dump(3, true, ScalarArrays::sa_[0]));
  LINE(27,x_var_dump(1, true));
  LINE(28,x_var_dump(1, toBoolean(v_foo)));
  LINE(29,x_var_dump(1, toBoolean(toObject(v_foo))));
  print("***   integer   ***\n");
  LINE(35,x_var_dump(1, 12LL));
  LINE(36,x_var_dump(1, 0LL));
  LINE(37,x_var_dump(1, 1LL));
  LINE(38,x_var_dump(1, -23LL));
  LINE(39,x_var_dump(1, 3LL));
  LINE(40,x_var_dump(1, 14336LL));
  LINE(41,x_var_dump(1, 0LL));
  LINE(42,x_var_dump(1, 0LL));
  LINE(43,x_var_dump(1, 1LL));
  LINE(44,x_var_dump(1, -1LL));
  LINE(45,x_var_dump(1, 0LL));
  LINE(46,x_var_dump(1, 0LL));
  LINE(47,x_var_dump(1, toInt64(v_foo)));
  LINE(48,x_var_dump(1, toInt64(toObject(v_foo))));
  LINE(49,x_var_dump(1, 0LL));
  print("***   float   ***\n");
  LINE(54,x_var_dump(1, 12.1234));
  LINE(55,x_var_dump(1, 0.0));
  LINE(56,x_var_dump(1, 1.0));
  LINE(57,x_var_dump(1, -23.0));
  LINE(58,x_var_dump(1, 3.0));
  LINE(59,x_var_dump(1, 14336.0));
  LINE(60,x_var_dump(1, 0.0));
  LINE(61,x_var_dump(1, 0.0));
  LINE(62,x_var_dump(1, 1.2323999999999999));
  LINE(63,x_var_dump(1, -1.2323999999999999));
  LINE(64,x_var_dump(1, 1.2323999999999999));
  LINE(65,x_var_dump(1, -1.2323999999999999));
  LINE(66,x_var_dump(1, 0.0));
  LINE(67,x_var_dump(1, 0.0));
  LINE(68,x_var_dump(1, toDouble(v_foo)));
  LINE(69,x_var_dump(1, toDouble(toObject(v_foo))));
  LINE(70,x_var_dump(1, 0.0));
  LINE(71,x_var_dump(1, 1.234));
  LINE(72,x_var_dump(1, -320.0));
  LINE(74,x_var_dump(1, 1200.0));
  LINE(75,x_var_dump(1, 1.0000000007000001));
  print("***   string   ***\n");
  LINE(80,x_var_dump(1, "12.1234"));
  LINE(81,x_var_dump(1, ""));
  LINE(82,x_var_dump(1, "1"));
  LINE(83,x_var_dump(1, "-23"));
  LINE(84,x_var_dump(1, "3"));
  LINE(85,x_var_dump(1, "14336"));
  LINE(86,x_var_dump(1, "0"));
  LINE(87,x_var_dump(1, "0"));
  LINE(88,x_var_dump(1, "1.2324"));
  LINE(89,x_var_dump(1, "-1.2324"));
  LINE(90,x_var_dump(1, "1.2324"));
  LINE(91,x_var_dump(1, "-1.2324"));
  LINE(92,x_var_dump(1, ""));
  LINE(93,x_var_dump(1, "0"));
  LINE(94,x_var_dump(1, toString(v_foo)));
  LINE(96,x_var_dump(1, ""));
  LINE(99,x_var_dump(1, "1200"));
  LINE(100,x_var_dump(1, "1.0000000007"));
  print("\n\nTwo compound types:.\n");
  v_foo.set("asdf", ("valasdf"), 0x06528546A3758F5CLL);
  v_foo.set("hjlk", ("valhjlk"), 0x54E63E00D28711FDLL);
  (v_bar = ((Object)(LINE(120,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  (v_bar.o_lval("bprop", 0x7A17297E520DDBABLL) = "fdsa");
  print("***   array   ***\n");
  LINE(125,x_var_dump(1, ScalarArrays::sa_[1]));
  LINE(126,x_var_dump(1, ScalarArrays::sa_[2]));
  LINE(127,x_var_dump(1, ScalarArrays::sa_[3]));
  LINE(128,x_var_dump(1, v_foo));
  LINE(129,x_var_dump(1, toArray(false)));
  LINE(130,x_var_dump(1, toArray(true)));
  LINE(131,x_var_dump(1, toArray(-23LL)));
  LINE(132,x_var_dump(1, toArray("")));
  LINE(133,x_var_dump(1, toArray("0")));
  LINE(134,x_var_dump(1, toArray(v_foo)));
  LINE(135,x_var_dump(1, toArray(v_bar)));
  LINE(136,x_var_dump(1, toArray(null)));
  print("***   object   ***\n");
  LINE(142,x_print_r(v_bar));
  (v_bar.o_lval("cprop", 0x1E9CB485F607A18ELL) = v_bar);
  LINE(144,x_print_r(v_bar));
  LINE(145,x_print_r(toObject(ScalarArrays::sa_[4])));
  LINE(146,x_print_r(toObject(false)));
  LINE(147,x_print_r(toObject(true)));
  LINE(148,x_print_r(toObject(-23LL)));
  LINE(149,x_print_r(toObject("")));
  LINE(150,x_print_r(toObject("0")));
  LINE(151,x_print_r(toObject(v_foo)));
  LINE(152,x_print_r(toObject(v_bar)));
  LINE(153,x_print_r(toObject(null)));
  print("\n\nTwo special types:\n");
  print("***   resource   ***\n");
  print("***   NULL   ***\n");
  LINE(167,x_var_dump(1, null));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
