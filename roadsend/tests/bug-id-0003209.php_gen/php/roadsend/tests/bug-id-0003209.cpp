
#include <php/roadsend/tests/bug-id-0003209.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0003209.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("prop", m_prop.isReferenced() ? ref(m_prop) : m_prop));
  props.push_back(NEW(ArrayElement)("otherprop", m_otherprop.isReferenced() ? ref(m_otherprop) : m_otherprop));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x2ACC0749E12117E8LL, otherprop, 9);
      break;
    case 3:
      HASH_EXISTS_STRING(0x4EA429E0DCC990B7LL, prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2ACC0749E12117E8LL, m_otherprop,
                         otherprop, 9);
      break;
    case 3:
      HASH_RETURN_STRING(0x4EA429E0DCC990B7LL, m_prop,
                         prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x2ACC0749E12117E8LL, m_otherprop,
                      otherprop, 9);
      break;
    case 3:
      HASH_SET_STRING(0x4EA429E0DCC990B7LL, m_prop,
                      prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x2ACC0749E12117E8LL, m_otherprop,
                         otherprop, 9);
      break;
    case 3:
      HASH_RETURN_STRING(0x4EA429E0DCC990B7LL, m_prop,
                         prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::create() {
  init();
  t_foo();
  return this;
}
ObjectData *c_foo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_foo::dynConstruct(CArrRef params) {
  (t_foo());
}
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_prop = m_prop.isReferenced() ? ref(m_prop) : m_prop;
  clone->m_otherprop = m_otherprop.isReferenced() ? ref(m_otherprop) : m_otherprop;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (t_foo(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_prop = null;
  m_otherprop = null;
}
/* SRC: roadsend/tests/bug-id-0003209.php line 7 */
void c_foo::t_foo() {
  INSTANCE_METHOD_INJECTION(Foo, Foo::Foo);
  bool oldInCtor = gasInCtor(true);
  (m_prop = ref(lval(m_otherprop)));
  (m_prop = "foo");
  gasInCtor(oldInCtor);
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0003209_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0003209.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0003209_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = ((Object)(LINE(13,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(14,x_var_dump(1, v_foo));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
