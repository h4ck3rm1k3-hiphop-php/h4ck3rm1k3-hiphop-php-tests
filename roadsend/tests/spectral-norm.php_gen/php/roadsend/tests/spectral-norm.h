
#ifndef __GENERATED_php_roadsend_tests_spectral_norm_h__
#define __GENERATED_php_roadsend_tests_spectral_norm_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/spectral-norm.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_av(CVarRef v_n, CVarRef v_v);
Variant f_atv(CVarRef v_n, CVarRef v_v);
Numeric f_a(int64 v_i, int64 v_j);
Variant pm_php$roadsend$tests$spectral_norm_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_atav(CVarRef v_n, CVarRef v_v);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_spectral_norm_h__
