
#include <php/roadsend/tests/printf.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$printf_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/printf.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$printf_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("num") : g->GV(num);
  Variant &v_location __attribute__((__unused__)) = (variables != gVariables) ? variables->get("location") : g->GV(location);
  Variant &v_format __attribute__((__unused__)) = (variables != gVariables) ? variables->get("format") : g->GV(format);
  Variant &v_year __attribute__((__unused__)) = (variables != gVariables) ? variables->get("year") : g->GV(year);
  Variant &v_month __attribute__((__unused__)) = (variables != gVariables) ? variables->get("month") : g->GV(month);
  Variant &v_day __attribute__((__unused__)) = (variables != gVariables) ? variables->get("day") : g->GV(day);
  Variant &v_isodate __attribute__((__unused__)) = (variables != gVariables) ? variables->get("isodate") : g->GV(isodate);
  Variant &v_money1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("money1") : g->GV(money1);
  Variant &v_money2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("money2") : g->GV(money2);
  Variant &v_money __attribute__((__unused__)) = (variables != gVariables) ? variables->get("money") : g->GV(money);
  Variant &v_formatted __attribute__((__unused__)) = (variables != gVariables) ? variables->get("formatted") : g->GV(formatted);

  (v_num = 12LL);
  (v_location = "fragit");
  (v_format = "There are %d monkeys in the %s\n");
  LINE(9,x_printf(3, toString(v_format), Array(ArrayInit(2).set(0, v_num).set(1, v_location).create())));
  (v_format = "The %s contains %d monkeys\n");
  LINE(12,x_printf(3, toString(v_format), Array(ArrayInit(2).set(0, v_num).set(1, v_location).create())));
  (v_format = "The %2$s contains %1$d monkeys\n");
  LINE(16,x_printf(3, toString(v_format), Array(ArrayInit(2).set(0, v_num).set(1, v_location).create())));
  (v_format = "The %2$s contains %1$d monkeys.\n           That's a nice %2$s full of %1$d monkeys.\n");
  LINE(26,x_printf(3, toString(v_format), Array(ArrayInit(2).set(0, v_num).set(1, v_location).create())));
  (v_year = 3LL);
  (v_month = 4LL);
  (v_day = 5LL);
  (v_isodate = LINE(32,x_sprintf(4, "%04d-%02d-%02d", Array(ArrayInit(3).set(0, v_year).set(1, v_month).set(2, v_day).create()))));
  (v_money1 = 68.75);
  (v_money2 = 54.350000000000001);
  (v_money = v_money1 + v_money2);
  (v_formatted = LINE(39,x_sprintf(2, "%01.2f", Array(ArrayInit(1).set(0, v_money).create()))));
  echo(LINE(42,concat4(toString(v_money), " ", toString(v_formatted), "\n")));
  LINE(44,x_printf(2, "%'g10.10f\n", ScalarArrays::sa_[0]));
  LINE(46,x_printf(2, "%10.10f\n", ScalarArrays::sa_[0]));
  LINE(48,x_printf(2, "%010.10f\n", ScalarArrays::sa_[0]));
  LINE(50,x_printf(2, "%-'m10.10f\n", ScalarArrays::sa_[0]));
  LINE(52,x_printf(2, "%1$-20s, %1$20s \n", ScalarArrays::sa_[1]));
  LINE(56,x_vprintf("%d plus %d = %d\n", ScalarArrays::sa_[2]));
  echo(LINE(57,x_vsprintf("%d plus %d = %d\n", ScalarArrays::sa_[2])));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
