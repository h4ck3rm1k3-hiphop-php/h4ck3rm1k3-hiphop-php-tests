
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 4:
      HASH_RETURN(0x336176791BC03F04LL, g->GV(num),
                  num);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 8:
      HASH_RETURN(0x4F451C70DF501D48LL, g->GV(formatted),
                  formatted);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x3F444870EE149B09LL, g->GV(money2),
                  money2);
      break;
    case 12:
      HASH_RETURN(0x3E9D67504EE78ACCLL, g->GV(month),
                  month);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 18:
      HASH_RETURN(0x065327C8F5142892LL, g->GV(isodate),
                  isodate);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x4CB7156046B03E53LL, g->GV(money1),
                  money1);
      break;
    case 23:
      HASH_RETURN(0x2CDDD058B4D49597LL, g->GV(money),
                  money);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 40:
      HASH_RETURN(0x0E12BE46FD64D268LL, g->GV(day),
                  day);
      break;
    case 43:
      HASH_RETURN(0x2B6706CF4608DE2BLL, g->GV(location),
                  location);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 54:
      HASH_RETURN(0x4575D5E219281E76LL, g->GV(format),
                  format);
      HASH_RETURN(0x28D044C5EA490CB6LL, g->GV(year),
                  year);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
