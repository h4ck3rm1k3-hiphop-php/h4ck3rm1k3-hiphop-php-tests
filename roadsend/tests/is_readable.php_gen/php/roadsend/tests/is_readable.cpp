
#include <php/roadsend/tests/is_readable.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$is_readable_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/is_readable.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$is_readable_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_PHP_SELF __attribute__((__unused__)) = (variables != gVariables) ? variables->get("PHP_SELF") : g->GV(PHP_SELF);

  echo(concat("cwd: ", toString(LINE(3,x_getcwd()))));
  echo(concat("this should be readable: ", (toString(LINE(6,x_is_readable(toString(v_PHP_SELF))) ? (("and it is.")) : (("but it's not!"))))));
  echo("\n");
  echo(concat("this should not be readable: ", (toString(LINE(10,x_is_readable(concat(toString(v_PHP_SELF), "bork"))) ? (("but it is!")) : (("and it's not."))))));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
