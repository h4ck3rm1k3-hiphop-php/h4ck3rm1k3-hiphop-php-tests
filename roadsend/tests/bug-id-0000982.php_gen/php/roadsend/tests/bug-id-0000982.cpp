
#include <php/roadsend/tests/bug-id-0000982.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000982.php line 8 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::create() {
  init();
  t_aclass();
  return this;
}
ObjectData *c_aclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
}
/* SRC: roadsend/tests/bug-id-0000982.php line 9 */
void c_aclass::t_aclass() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::aclass);
  bool oldInCtor = gasInCtor(true);
  Sequence v_a;

  v_a.set("Default", ("nope"), 0x2F418B6392F6C49DLL);
  echo(toString(v_a.rvalAt("Default", 0x2F418B6392F6C49DLL)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/tests/bug-id-0000982.php line 26 */
Variant c_bclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bclass)
ObjectData *c_bclass::create() {
  init();
  t_bclass();
  return this;
}
ObjectData *c_bclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_bclass::cloneImpl() {
  c_bclass *obj = NEW(c_bclass)();
  cloneSet(obj);
  return obj;
}
void c_bclass::cloneSet(c_bclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bclass$os_get(const char *s) {
  return c_bclass::os_get(s, -1);
}
Variant &cw_bclass$os_lval(const char *s) {
  return c_bclass::os_lval(s, -1);
}
Variant cw_bclass$os_constant(const char *s) {
  return c_bclass::os_constant(s);
}
Variant cw_bclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bclass::os_invoke(c, s, params, -1, fatal);
}
void c_bclass::init() {
}
/* SRC: roadsend/tests/bug-id-0000982.php line 27 */
void c_bclass::t_bclass() {
  INSTANCE_METHOD_INJECTION(bclass, bclass::bclass);
  bool oldInCtor = gasInCtor(true);
  Sequence v_a;

  v_a.set("VAR", ("nope"), 0x4C2672AC07B9CF8ALL);
  echo(toString(v_a.rvalAt("VAR", 0x4C2672AC07B9CF8ALL)));
  echo(toString(v_a.rvalAt("VAR", 0x4C2672AC07B9CF8ALL)));
  gasInCtor(oldInCtor);
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Object co_bclass(CArrRef params, bool init /* = true */) {
  return Object(p_bclass(NEW(c_bclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000982_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000982.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000982_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  echo("0000982 hash key 'default','var' throws parse error\n\n");
  v_b.set("Default", ("hey"), 0x2F418B6392F6C49DLL);
  echo(toString(v_b.rvalAt("Default", 0x2F418B6392F6C49DLL)) + toString("\n"));
  (v_a = ((Object)(LINE(15,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  echo("\n\n also with the VAR key:\n");
  v_b.set("VAR", ("hey"), 0x4C2672AC07B9CF8ALL);
  echo(toString(v_b.rvalAt("VAR", 0x4C2672AC07B9CF8ALL)) + toString("\n"));
  (v_a = ((Object)(LINE(34,p_bclass(p_bclass(NEWOBJ(c_bclass)())->create())))));
  echo("\n ");
  v_test.set("include", ("yippie"), 0x42466A24EC29D691LL);
  echo(toString(v_test.rvalAt("include", 0x42466A24EC29D691LL)));
  echo(toString(v_test.rvalAt("include", 0x42466A24EC29D691LL)));
  echo(toString(v_test.rvalAt("include", 0x42466A24EC29D691LL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
