
#ifndef __GENERATED_php_roadsend_tests_shutdown_function_h__
#define __GENERATED_php_roadsend_tests_shutdown_function_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/shutdown_function.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_shut2();
Variant pm_php$roadsend$tests$shutdown_function_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_shut();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_shutdown_function_h__
