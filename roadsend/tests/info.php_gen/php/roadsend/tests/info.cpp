
#include <php/roadsend/tests/info.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$info_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/info.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$info_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("hello!!!<br>\n");
  LINE(5,x_phpinfo());
  echo("<pre>\r\n");
  echo("SERVER:\n");
  LINE(11,x_var_dump(1, g->gv__SERVER));
  echo("GET:\n");
  LINE(13,x_var_dump(1, g->gv__GET));
  echo("POST:\n");
  LINE(15,x_var_dump(1, g->gv__POST));
  echo("REQUEST:\n");
  LINE(17,x_var_dump(1, g->gv__REQUEST));
  echo("</pre>\r\n<form action=\"");
  echo(toString(g->gv__SERVER.rvalAt("PHP_SELF", 0x4C71E2E17E448EB2LL)));
  echo("\" method=\"post\">\r\n<input type=\"text\" name=\"sample\">\r\n<input type=\"submit\">\r\n</form>\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
