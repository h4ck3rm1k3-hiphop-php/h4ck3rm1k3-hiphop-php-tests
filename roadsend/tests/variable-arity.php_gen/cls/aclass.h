
#ifndef __GENERATED_cls_aclass_h__
#define __GENERATED_cls_aclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/variable-arity.php line 37 */
class c_aclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(aclass)
  END_CLASS_MAP(aclass)
  DECLARE_CLASS(aclass, aclass, ObjectData)
  void init();
  public: static void ti_afun(const char* cls, int num_args, Array args = Array());
  public: static void ti_afun_with_args(const char* cls, int num_args, CVarRef v_a, Array args = Array());
  public: static void t_afun_with_args(int num_args, CVarRef v_a, Array args = Array()) { ti_afun_with_args("aclass", num_args, v_a, args); }
  public: static void t_afun(int num_args, Array args = Array()) { ti_afun("aclass", num_args, args); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_aclass_h__
