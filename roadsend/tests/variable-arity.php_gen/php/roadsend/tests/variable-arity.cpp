
#include <php/roadsend/tests/variable-arity.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/variable-arity.php line 37 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        int count = params.size();
        if (count <= 0) return (ti_afun(o_getClassName(),count), null);
        return (ti_afun(o_getClassName(),count,params.slice(0, count - 0, false)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x6368516AB437AB82LL, afun_with_args) {
        int count = params.size();
        if (count <= 1) return (ti_afun_with_args(o_getClassName(),count, params.rvalAt(0)), null);
        return (ti_afun_with_args(o_getClassName(),count,params.rvalAt(0), params.slice(1, count - 1, false)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        if (count <= 0) return (ti_afun(o_getClassName(),count), null);
        Array params;
        if (count >= 1) params.append(a0);
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_afun(o_getClassName(),count,params), null);
      }
      break;
    case 2:
      HASH_GUARD(0x6368516AB437AB82LL, afun_with_args) {
        if (count <= 1) return (ti_afun_with_args(o_getClassName(),count, a0), null);
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_afun_with_args(o_getClassName(),count,a0, params), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        int count = params.size();
        if (count <= 0) return (ti_afun(c,count), null);
        return (ti_afun(c,count,params.slice(0, count - 0, false)), null);
      }
      break;
    case 2:
      HASH_GUARD(0x6368516AB437AB82LL, afun_with_args) {
        int count = params.size();
        if (count <= 1) return (ti_afun_with_args(c,count, params.rvalAt(0)), null);
        return (ti_afun_with_args(c,count,params.rvalAt(0), params.slice(1, count - 1, false)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
}
/* SRC: roadsend/tests/variable-arity.php line 38 */
void c_aclass::ti_afun(const char* cls, int num_args, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(aclass, aclass::afun);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int v_numargs = 0;
  Array v_arg_list;
  int64 v_i = 0;

  (v_numargs = LINE(39,num_args));
  echo(LINE(40,concat3("1Number of arguments: ", toString(v_numargs), "<br />\n")));
  if (not_less(v_numargs, 2LL)) {
    echo(LINE(42,(assignCallTemp(eo_1, toString(func_get_arg(num_args, Array(),args,toInt32(1LL)))),concat3("1Second argument is: ", eo_1, "<br />\n"))));
  }
  (v_arg_list = LINE(44,func_get_args(num_args, Array(),args)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_numargs); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(46,concat5("1Argument ", toString(v_i), " is: ", toString(v_arg_list.rvalAt(v_i)), "<br />\n")));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/variable-arity.php line 50 */
void c_aclass::ti_afun_with_args(const char* cls, int num_args, CVarRef v_a, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(aclass, aclass::afun_with_args);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int v_numargs = 0;
  Array v_arg_list;
  int64 v_i = 0;

  (v_numargs = LINE(51,num_args));
  echo(LINE(52,concat3("2Number of arguments: ", toString(v_numargs), "<br />\n")));
  if (not_less(v_numargs, 2LL)) {
    echo(LINE(54,(assignCallTemp(eo_1, toString(func_get_arg(num_args, Array(ArrayInit(1).set(0, v_a).create()),args,toInt32(1LL)))),concat3("2Second argument is: ", eo_1, "<br />\n"))));
  }
  (v_arg_list = LINE(56,func_get_args(num_args, Array(ArrayInit(1).set(0, v_a).create()),args)));
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, v_numargs); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(LINE(58,concat5("2Argument ", toString(v_i), " is: ", toString(v_arg_list.rvalAt(v_i)), "<br />\n")));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/variable-arity.php line 20 */
void f_foo_with_args(int num_args, int64 v_a, int64 v_b, Array args /* = Array() */) {
  FUNCTION_INJECTION(foo_with_args);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int v_numargs = 0;
  Array v_arg_list;
  int64 v_i = 0;

  (v_numargs = LINE(22,num_args));
  echo(LINE(23,concat3("0Number of arguments: ", toString(v_numargs), "<br />\n")));
  if (not_less(v_numargs, 2LL)) {
    echo(LINE(25,(assignCallTemp(eo_1, toString(func_get_arg(num_args, Array(ArrayInit(2).set(0, v_a).set(1, v_b).create()),args,toInt32(1LL)))),concat3("0Second argument is: ", eo_1, "<br />\n"))));
  }
  (v_arg_list = LINE(27,func_get_args(num_args, Array(ArrayInit(2).set(0, v_a).set(1, v_b).create()),args)));
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, v_numargs); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        echo(LINE(29,concat5("0Argument ", toString(v_i), " is: ", toString(v_arg_list.rvalAt(v_i)), "<br />\n")));
      }
    }
  }
} /* function */
/* SRC: roadsend/tests/variable-arity.php line 4 */
void f_foo(int num_args, Array args /* = Array() */) {
  FUNCTION_INJECTION(foo);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int v_numargs = 0;
  Array v_arg_list;
  int64 v_i = 0;

  (v_numargs = LINE(6,num_args));
  echo(LINE(7,concat3("Number of arguments: ", toString(v_numargs), "<br />\n")));
  if (not_less(v_numargs, 2LL)) {
    echo(LINE(9,(assignCallTemp(eo_1, toString(func_get_arg(num_args, Array(),args,toInt32(1LL)))),concat3("Second argument is: ", eo_1, "<br />\n"))));
  }
  (v_arg_list = LINE(11,func_get_args(num_args, Array(),args)));
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, v_numargs); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        echo(LINE(13,concat5("Argument ", toString(v_i), " is: ", toString(v_arg_list.rvalAt(v_i)), "<br />\n")));
      }
    }
  }
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$variable_arity_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/variable-arity.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$variable_arity_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("test variable arity user functions\n\n");
  LINE(17,f_foo(0));
  LINE(18,f_foo(3, ScalarArrays::sa_[0]));
  LINE(34,f_foo_with_args(3, 1LL, 2LL, ScalarArrays::sa_[1]));
  LINE(63,c_aclass::t_afun(2, ScalarArrays::sa_[2]));
  (v_a = ((Object)(LINE(64,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  LINE(65,v_a.o_invoke_few_args("afun", 0x64AA3995684BE7FDLL, 4, 88LL, 89LL, 90LL, 91LL));
  LINE(68,c_aclass::t_afun_with_args(2, 1LL, ScalarArrays::sa_[3]));
  (v_a = ((Object)(LINE(69,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  LINE(70,v_a.o_invoke_few_args("afun_with_args", 0x6368516AB437AB82LL, 4, 88LL, 89LL, 90LL, 91LL));
  echo(" \n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
