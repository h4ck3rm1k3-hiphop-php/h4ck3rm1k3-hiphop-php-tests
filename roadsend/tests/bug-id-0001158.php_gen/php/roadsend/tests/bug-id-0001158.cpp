
#include <php/roadsend/tests/bug-id-0001158.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001158_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001158.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001158_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_ui1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ui1") : g->GV(ui1);
  Variant &v_ui2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ui2") : g->GV(ui2);

  echo("Testing uniqid: ");
  (v_str = "prefix");
  (v_ui1 = LINE(5,x_uniqid(toString(v_str))));
  (v_ui2 = LINE(6,x_uniqid(toString(v_str))));
  if (equal_rev(LINE(7,x_strlen(toString(v_ui2))), x_strlen(toString(v_ui1))) && equal(x_strlen(toString(v_ui1)), 19LL) && !equal(v_ui1, v_ui2)) {
    echo("passed\n");
  }
  else {
    echo((LINE(10,concat5("failed: <", toString(v_ui1), "> <", toString(v_ui2), ">\n"))));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
