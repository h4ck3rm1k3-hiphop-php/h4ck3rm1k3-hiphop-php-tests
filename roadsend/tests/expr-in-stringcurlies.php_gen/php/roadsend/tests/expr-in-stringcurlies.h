
#ifndef __GENERATED_php_roadsend_tests_expr_in_stringcurlies_h__
#define __GENERATED_php_roadsend_tests_expr_in_stringcurlies_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/expr-in-stringcurlies.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$expr_in_stringcurlies_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_expr_in_stringcurlies_h__
