
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
