
#include <php/roadsend/tests/ereg-replace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$ereg_replace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/ereg-replace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$ereg_replace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("num") : g->GV(num);
  Variant &v_text __attribute__((__unused__)) = (variables != gVariables) ? variables->get("text") : g->GV(text);

  (v_string = "This is a test");
  echo(concat(LINE(3,x_ereg_replace(" is", " was", toString(v_string))), "\n"));
  echo(concat(LINE(4,x_ereg_replace("( )is", "\\1was", toString(v_string))), "\n"));
  echo(concat(LINE(5,x_ereg_replace("(( )is)", "\\2was", toString(v_string))), "\n"));
  (v_num = 4LL);
  (v_string = "This string has four words.");
  (v_string = LINE(12,x_ereg_replace("four", toString(v_num), toString(v_string))));
  echo(concat(toString(v_string), "\n"));
  (v_num = "4");
  (v_string = "This string has four words.");
  (v_string = LINE(18,x_ereg_replace("four", toString(v_num), toString(v_string))));
  echo(concat(toString(v_string), "\n"));
  (v_text = "file:///usr/local/doc/bigloo-2.5c/bigloo-5.10.html#container1673");
  (v_text = LINE(25,x_ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]", "<a href=\"\\0\">\\0</a>", toString(v_text))));
  echo(LINE(27,concat3("\n", toString(v_text), "\n")));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
