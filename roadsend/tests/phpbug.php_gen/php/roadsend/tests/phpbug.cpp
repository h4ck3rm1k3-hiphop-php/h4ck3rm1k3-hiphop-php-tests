
#include <php/roadsend/tests/phpbug.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/phpbug.php line 2 */
void f_zammo(CStrRef v_foo) {
  FUNCTION_INJECTION(zammo);
  echo((toString((Variant)((concat(v_foo, toString(2LL)))) + 3LL)));
} /* function */
Variant pm_php$roadsend$tests$phpbug_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/phpbug.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$phpbug_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(9,f_zammo("bar"));
  echo("\n\n\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
