
#ifndef __GENERATED_php_roadsend_tests_gtk_main_h__
#define __GENERATED_php_roadsend_tests_gtk_main_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/gtk-main.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_destroy(CVarRef v_arg);
void f_hello_world(CVarRef v_arg);
bool f_delete_event();
Variant pm_php$roadsend$tests$gtk_main_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_gtk_main_h__
