
#include <php/roadsend/tests/gtk-main.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/gtk-main.php line 23 */
void f_destroy(CVarRef v_arg) {
  FUNCTION_INJECTION(destroy);
  LINE(25,throw_fatal("unknown class gtk", ((void*)NULL)));
} /* function */
/* SRC: roadsend/tests/gtk-main.php line 41 */
void f_hello_world(CVarRef v_arg) {
  FUNCTION_INJECTION(hello_world);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_window __attribute__((__unused__)) = g->GV(window);
  print("Hello World!\n");
  LINE(46,gv_window.o_invoke_few_args("destroy", 0x45CCFF10463AB662LL, 0));
} /* function */
/* SRC: roadsend/tests/gtk-main.php line 32 */
bool f_delete_event() {
  FUNCTION_INJECTION(delete_event);
  return false;
} /* function */
Variant i_destroy(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x45CCFF10463AB662LL, destroy) {
    return (f_destroy(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_hello_world(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3394D087BD075A8CLL, hello_world) {
    return (f_hello_world(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_delete_event(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0549FAE24D08964BLL, delete_event) {
    return (f_delete_event());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$tests$gtk_main_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/gtk-main.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$gtk_main_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_window __attribute__((__unused__)) = (variables != gVariables) ? variables->get("window") : g->GV(window);
  Variant &v_button __attribute__((__unused__)) = (variables != gVariables) ? variables->get("button") : g->GV(button);
  Variant &v_tt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tt") : g->GV(tt);

  (v_window = LINE(53,create_object("gtkwindow", Array())));
  LINE(54,v_window.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "destroy", "destroy"));
  LINE(55,v_window.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
  LINE(56,v_window.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
  (v_button = LINE(62,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Hello World!").create()))));
  LINE(63,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "hello_world"));
  LINE(64,v_window.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_button));
  (v_tt = LINE(69,create_object("gtktooltips", Array())));
  LINE(70,v_tt.o_invoke_few_args("set_delay", 0x78C815B577A3085ELL, 1, 200LL));
  LINE(71,v_tt.o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_button, "Prints \"Hello World!\"", ""));
  LINE(72,v_tt.o_invoke_few_args("enable", 0x36861E5E851AEE23LL, 0));
  LINE(77,v_window.o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
  LINE(80,throw_fatal("unknown class gtk", ((void*)NULL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
