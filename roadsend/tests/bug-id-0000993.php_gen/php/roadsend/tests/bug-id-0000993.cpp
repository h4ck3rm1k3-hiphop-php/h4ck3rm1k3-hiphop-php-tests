
#include <php/roadsend/tests/bug-id-0000993.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000993.php line 4 */
void f_zot_DupId0(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(zot);
  echo(LINE(5,concat5("$a ", toString(v_a), ", $b ", toString(v_b), "\n")));
} /* function */
/* SRC: roadsend/tests/bug-id-0000993.php line 10 */
void f_zot_DupId1(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(zot);
  echo("woops!");
} /* function */
Variant i_zot_DupId0(CArrRef params) {
  return (f_zot_DupId0(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_zot(CArrRef params) {
  DECLARE_GLOBAL_VARIABLES(g);
  return g->i_zot(params);
}
Variant i_zot_DupId1(CArrRef params) {
  return (f_zot_DupId1(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant pm_php$roadsend$tests$bug_id_0000993_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000993.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000993_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  if (!(LINE(3,(get_global_variables()->i_zot != invoke_failed_zot)))) {
    g->i_zot = i_zot_DupId0;
    g->declareFunction("zot");
  }
  if (!(LINE(9,(get_global_variables()->i_zot != invoke_failed_zot)))) {
    g->i_zot = i_zot_DupId1;
    g->declareFunction("zot");
  }
  LINE(21,get_global_variables()->i_zot(Array(ArrayInit(2).set(0, "one").set(1, 2LL).create())));
  LINE(22,x_print_r(ScalarArrays::sa_[0]));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
