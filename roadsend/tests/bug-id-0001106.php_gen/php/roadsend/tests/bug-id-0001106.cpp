
#include <php/roadsend/tests/bug-id-0001106.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001106_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001106.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001106_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arrays __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arrays") : g->GV(arrays);
  Variant &v_item __attribute__((__unused__)) = (variables != gVariables) ? variables->get("item") : g->GV(item);

  (v_arrays = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_arrays.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_item = iter3->second();
      {
        LINE(43,x_var_dump(1, x_array_change_key_case(v_item)));
        LINE(44,x_var_dump(1, x_array_change_key_case(v_item, toBoolean(1LL) /* CASE_UPPER */)));
        LINE(45,x_var_dump(1, x_array_change_key_case(v_item, toBoolean(0LL) /* CASE_LOWER */)));
        echo("\n");
      }
    }
  }
  echo("end\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
