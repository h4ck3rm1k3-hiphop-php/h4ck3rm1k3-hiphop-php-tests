
#include <php/roadsend/tests/deployment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$deployment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/deployment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$deployment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_num __attribute__((__unused__)) = (variables != gVariables) ? variables->get("num") : g->GV(num);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);

  echo("<HTML>\n  <HEAD>\n    <TITLE>\"This is a \n      ");
  echo("successfully");
  echo(" \n      deployed web page.\n    </TITLE>\n  </HEAD>\n  <BODY>\n    Two plus two is \n    ");
  (v_foo = ScalarArrays::sa_[0]);
  echo(toString(v_foo.rvalAt(4LL, 0x6F2A25235E544A31LL)));
  echo(".<BR>    \n    It's clearly not\n    ");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_foo.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_name = iter3->second();
      v_num = iter3->first();
      {
        if (!((equal(v_num, 4LL) || equal(v_num, LINE(22,x_count(v_foo)) - 1LL)))) echo(toString(v_name) + toString(", "));
      }
    }
  }
  echo(LINE(25,concat3("or ", toString(v_foo.rvalAt(v_num)), ".")));
  echo("<BR>\n  </BODY>\n</HTML>\n \n    \n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
