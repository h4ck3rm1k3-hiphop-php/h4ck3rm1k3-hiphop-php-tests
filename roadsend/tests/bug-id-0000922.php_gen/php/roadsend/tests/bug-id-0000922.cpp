
#include <php/roadsend/tests/bug-id-0000922.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000922.php line 4 */
Variant c_numberiterator::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_numberiterator::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_numberiterator::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_numberiterator::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_numberiterator::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_numberiterator::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_numberiterator::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_numberiterator::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(numberiterator)
ObjectData *c_numberiterator::cloneImpl() {
  c_numberiterator *obj = NEW(c_numberiterator)();
  cloneSet(obj);
  return obj;
}
void c_numberiterator::cloneSet(c_numberiterator *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_numberiterator::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_numberiterator::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_numberiterator::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_numberiterator$os_get(const char *s) {
  return c_numberiterator::os_get(s, -1);
}
Variant &cw_numberiterator$os_lval(const char *s) {
  return c_numberiterator::os_lval(s, -1);
}
Variant cw_numberiterator$os_constant(const char *s) {
  return c_numberiterator::os_constant(s);
}
Variant cw_numberiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_numberiterator::os_invoke(c, s, params, -1, fatal);
}
void c_numberiterator::init() {
}
/* SRC: roadsend/tests/bug-id-0000922.php line 6 */
void c_numberiterator::ti_next(const char* cls) {
  STATIC_METHOD_INJECTION(NumberIterator, NumberIterator::next);
  print("I'm your wurst nightmare\n");
} /* function */
Object co_numberiterator(CArrRef params, bool init /* = true */) {
  return Object(p_numberiterator(NEW(c_numberiterator)())->dynCreate(params, init));
}
Variant pm_php$roadsend$tests$bug_id_0000922_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000922.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000922_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(13,c_numberiterator::t_next());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
