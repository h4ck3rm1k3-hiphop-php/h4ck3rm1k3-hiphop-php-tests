
#ifndef __GENERATED_php_roadsend_tests_inc5_h__
#define __GENERATED_php_roadsend_tests_inc5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/inc5.fw.h>

// Declarations
#include <cls/fifunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$tests$inc5_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_fifunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_inc5_h__
