
#ifndef __GENERATED_cls_fifunctionmenu_h__
#define __GENERATED_cls_fifunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/tests/inc5.php line 22 */
class c_fifunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(fifunctionmenu)
  END_CLASS_MAP(fifunctionmenu)
  DECLARE_CLASS(fifunctionmenu, fiFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fifunctionmenu_h__
