
#ifndef __GENERATED_php_roadsend_tests_zcallbacks_h__
#define __GENERATED_php_roadsend_tests_zcallbacks_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/tests/zcallbacks.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_implode_args(int num_args, Array args = Array());
Variant pm_php$roadsend$tests$zcallbacks_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_tests_zcallbacks_h__
