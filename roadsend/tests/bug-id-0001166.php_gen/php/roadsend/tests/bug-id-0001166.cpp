
#include <php/roadsend/tests/bug-id-0001166.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001166_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001166.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001166_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);

  echo("0001166 nested foreach problem\n\nAdditional Information i'm sure this has to do with the internal array\nindex. not sure how php handles it but it works ok in php, ie it\niterates through all items for each nest level.\n\n");
  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_k = iter6->second();
            {
              print(LINE(14,concat5("a1 is ", toString(v_v), " while a2 is ", toString(v_k), "\n")));
            }
          }
        }
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
