
#include <php/roadsend/tests/skeleton.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$skeleton_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/skeleton.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$skeleton_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);

  LINE(3,invoke_failed("skel_hello_world", Array(ArrayInit(1).set(0, "yippie!\n").create()), 0x0000000015B27CB0LL));
  LINE(5,x_var_dump(1, invoke_failed("skel_hash", Array(ArrayInit(2).set(0, 123LL).set(1, "456").create()), 0x00000000881D8BA8LL)));
  (v_s = LINE(7,create_object("skeleton", Array(ArrayInit(1).set(0, "my message").create()))));
  LINE(8,x_var_dump(1, v_s));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
