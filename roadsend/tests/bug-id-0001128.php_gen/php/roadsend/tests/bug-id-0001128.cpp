
#include <php/roadsend/tests/bug-id-0001128.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0001128_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0001128.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0001128_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_registers __attribute__((__unused__)) = (variables != gVariables) ? variables->get("registers") : g->GV(registers);

  (v_a = "This is a nice and simple string");
  echo(toString(LINE(4,x_ereg(".*(is).*(is).*", toString(v_a), ref(v_registers)))));
  echo("\n");
  echo(toString(v_registers.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  echo("\n");
  echo(toString(v_registers.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  echo("\n");
  echo(toString(v_registers.rvalAt(2LL, 0x486AFCC090D5F98CLL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
