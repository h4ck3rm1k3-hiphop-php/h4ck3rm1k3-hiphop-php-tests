
#include <php/roadsend/tests/shellexec.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_SW_SHOWMAXIMIZED = "SW_SHOWMAXIMIZED";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$shellexec_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/shellexec.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$shellexec_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ret __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ret") : g->GV(ret);

  (v_ret = LINE(3,invoke_failed("win_shellexecute", Array(ArrayInit(5).set(0, "open").set(1, "http://www.google.com").set(2, "").set(3, "").set(4, k_SW_SHOWMAXIMIZED).create()), 0x00000000847832C2LL)));
  echo(LINE(4,concat3("return: ", toString(v_ret), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
