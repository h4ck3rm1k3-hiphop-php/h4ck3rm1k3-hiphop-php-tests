
#include <php/roadsend/tests/bug-id-0000882.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$tests$bug_id_0000882_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000882.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000882_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_my_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("my_file") : g->GV(my_file);
  Variant &v_php_errormsg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_errormsg") : g->GV(php_errormsg);
  Variant &v_cache __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cache") : g->GV(cache);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);

  echo(" problem with using @ to disable errors\n");
  (toBoolean((v_my_file = (silenceInc(), silenceDec(LINE(4,x_file("non_existent_file"))))))) || (toBoolean(f_exit(LINE(5,concat3("Failed opening file: error was '", toString(v_php_errormsg), "'")))));
  (v_value = (silenceInc(), silenceDec(v_cache.rvalAt(v_key))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
