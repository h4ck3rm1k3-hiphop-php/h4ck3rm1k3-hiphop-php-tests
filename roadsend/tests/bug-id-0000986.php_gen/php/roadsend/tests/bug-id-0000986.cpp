
#include <php/roadsend/tests/bug-id-0000986.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/tests/bug-id-0000986.php line 6 */
void f_blah() {
  FUNCTION_INJECTION(blah);
  DECLARE_GLOBAL_VARIABLES(g);
  Sequence v_a;


  class VariableTable : public RVariableTable {
  public:
    Sequence &v_a;
    VariableTable(Sequence &r_a) : v_a(r_a) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 1) {
        case 0:
          HASH_RETURN(0x4292CEE227B9150ALL, v_a,
                      a);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_a);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  v_a.set("test", ("aval"), 0x3A1DC49AA6CFEA13LL);
  throw_fatal("dynamic global");
  echo(LINE(9,concat3("this should say hi--> ", toString(variables->get(toString(v_a.rvalAt("test", 0x3A1DC49AA6CFEA13LL)))), "\n")));
} /* function */
Variant pm_php$roadsend$tests$bug_id_0000986_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/tests/bug-id-0000986.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$tests$bug_id_0000986_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_aval __attribute__((__unused__)) = (variables != gVariables) ? variables->get("aval") : g->GV(aval);

  echo("0000986 parse error globalizing a variable variable from a hash\n");
  (v_aval = "hi");
  LINE(12,f_blah());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
