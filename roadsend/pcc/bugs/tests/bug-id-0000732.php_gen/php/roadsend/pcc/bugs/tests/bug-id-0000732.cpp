
#include <php/roadsend/pcc/bugs/tests/bug-id-0000732.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000732.php line 13 */
int64 f_azend_version() {
  FUNCTION_INJECTION(azend_version);
  return 1LL;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000732.php line 5 */
bool f_afunction_exists(CStrRef v_a) {
  FUNCTION_INJECTION(afunction_exists);
  return true;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000732.php line 9 */
String f_aversion_compare(int64 v_a, CStrRef v_b, CStrRef v_c) {
  FUNCTION_INJECTION(aversion_compare);
  return "mofunctioncall";
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000732_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000732.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000732_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("0000732 parse error on complicated constant define()\n\n");
  LINE(17,g->declareConstant("PEAR_ZE2", g->k_PEAR_ZE2, (f_afunction_exists("version_compare") && toBoolean((assignCallTemp(eo_2, f_azend_version()),f_aversion_compare(eo_2, "2-dev", "ge"))))));
  echo(concat(toString(get_global_variables()->k_PEAR_ZE2), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
