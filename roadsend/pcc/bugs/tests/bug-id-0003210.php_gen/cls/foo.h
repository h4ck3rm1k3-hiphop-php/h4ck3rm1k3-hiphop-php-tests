
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0003210.php line 5 */
class c_foo : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, Foo, ObjectData)
  void init();
  public: bool m_prop;
  public: void t_foo();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
