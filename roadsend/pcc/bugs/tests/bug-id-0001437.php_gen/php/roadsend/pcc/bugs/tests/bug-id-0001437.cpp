
#include <php/roadsend/pcc/bugs/tests/bug-id-0001437.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001437_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001437.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001437_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("0001437 php-hashes don't behave quite the same as php's hashes after deleting entries\n");
  (v_foo = ScalarArrays::sa_[0]);
  v_foo.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
  LINE(6,x_print_r(v_foo));
  echo("\nThis is due to the way elements are deleted from the ordered-rep vector. It's probably better to maintain two pointers in each entry in the hashtable -- prev and next -- like a doubly linked list, than to dick around with the vector. That would fix this.");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
