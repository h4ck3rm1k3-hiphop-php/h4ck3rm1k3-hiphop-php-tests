
#include <php/roadsend/pcc/bugs/tests/bug-id-0001125.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001125_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001125.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001125_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_za __attribute__((__unused__)) = (variables != gVariables) ? variables->get("za") : g->GV(za);
  Variant &v_zb __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zb") : g->GV(zb);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  LINE(3,g->declareConstant("LONG_MAX", g->k_LONG_MAX, x_is_int(5000000000LL) ? ((Variant)(9223372036854775800.0)) : ((Variant)(2147483647LL))));
  LINE(4,g->declareConstant("LONG_MIN", g->k_LONG_MIN, negate(get_global_variables()->k_LONG_MAX) - 1LL));
  LINE(6,(assignCallTemp(eo_1, LINE(5,x_is_int(get_global_variables()->k_LONG_MIN))),assignCallTemp(eo_2, x_is_int(get_global_variables()->k_LONG_MAX)),assignCallTemp(eo_3, LINE(6,x_is_int(get_global_variables()->k_LONG_MIN - 1LL))),assignCallTemp(eo_4, x_is_int(get_global_variables()->k_LONG_MAX + 1LL)),x_printf(5, "%d,%d,%d,%d\n", Array(ArrayInit(4).set(0, eo_1).set(1, eo_2).set(2, eo_3).set(3, eo_4).create()))));
  (v_z = 5000000000LL);
  LINE(9,x_var_dump(1, v_z));
  (v_z = 9223372036854775800.0);
  LINE(11,x_var_dump(1, v_z));
  echo("this is z: ");
  (v_z = 2147483647LL);
  LINE(15,x_var_dump(1, v_z));
  echo(LINE(17,concat3("this is -z: ", toString(negate(v_z)), "\n")));
  echo("1: ");
  (v_z = (negate(v_z) - 1LL));
  LINE(21,x_var_dump(1, v_z));
  echo("1a: ");
  (v_z = 2147483647LL);
  (v_za = negate(v_z));
  (v_zb = (v_za - 1LL));
  echo(LINE(27,concat5("za is ", toString(v_za), ", zb is ", toString(v_zb), "\n")));
  LINE(28,x_var_dump(1, v_zb));
  echo("2: ");
  (v_z = ((negate(v_z)) + 1LL));
  LINE(32,x_var_dump(1, v_z));
  (v_a = get_global_variables()->k_LONG_MIN);
  (v_b = get_global_variables()->k_LONG_MAX);
  (v_c = (get_global_variables()->k_LONG_MIN - 1LL));
  (v_d = (get_global_variables()->k_LONG_MAX + 1LL));
  echo("3:");
  LINE(40,x_var_dump(1, v_a));
  echo("4:");
  LINE(42,x_var_dump(1, v_b));
  echo("5:");
  LINE(44,x_var_dump(1, v_c));
  echo("6:");
  LINE(46,x_var_dump(1, v_d));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
