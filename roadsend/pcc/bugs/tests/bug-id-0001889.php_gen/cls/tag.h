
#ifndef __GENERATED_cls_tag_h__
#define __GENERATED_cls_tag_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001889.php line 6 */
class c_tag : virtual public ObjectData {
  BEGIN_CLASS_MAP(tag)
  END_CLASS_MAP(tag)
  DECLARE_CLASS(tag, tag, ObjectData)
  void init();
  public: String m_txt;
  public: void t_tag(CStrRef v_t);
  public: ObjectData *create(CStrRef v_t);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_tag_h__
