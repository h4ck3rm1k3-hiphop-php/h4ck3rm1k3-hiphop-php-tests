
#ifndef __GENERATED_cls_tpt_h__
#define __GENERATED_cls_tpt_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001889.php line 13 */
class c_tpt : virtual public ObjectData {
  BEGIN_CLASS_MAP(tpt)
  END_CLASS_MAP(tpt)
  DECLARE_CLASS(tpt, tpt, ObjectData)
  void init();
  public: Variant m_tags;
  public: void t_tpt();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_addmore();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_tpt_h__
