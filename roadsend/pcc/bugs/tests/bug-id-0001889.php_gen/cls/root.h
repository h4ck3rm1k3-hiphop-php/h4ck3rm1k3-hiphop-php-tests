
#ifndef __GENERATED_cls_root_h__
#define __GENERATED_cls_root_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001889.php line 23 */
class c_root : virtual public ObjectData {
  BEGIN_CLASS_MAP(root)
  END_CLASS_MAP(root)
  DECLARE_CLASS(root, root, ObjectData)
  void init();
  public: Array m_tlist;
  public: Variant t_newtpt(CVarRef v_key);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_root_h__
