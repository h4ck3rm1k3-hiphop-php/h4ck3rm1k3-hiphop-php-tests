
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001889_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001889_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001889.fw.h>

// Declarations
#include <cls/tag.h>
#include <cls/tpt.h>
#include <cls/root.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001889_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_tag(CArrRef params, bool init = true);
Object co_tpt(CArrRef params, bool init = true);
Object co_root(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001889_h__
