
#include <php/roadsend/pcc/bugs/tests/bug-id-0000735.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000735.php line 5 */
Variant c_someclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_someclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_someclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("zot", m_zot));
  c_ObjectData::o_get(props);
}
bool c_someclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7A42596E78DEAC62LL, zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_someclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A42596E78DEAC62LL, m_zot,
                         zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_someclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7A42596E78DEAC62LL, m_zot,
                      zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_someclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_someclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(someclass)
ObjectData *c_someclass::create() {
  init();
  t_someclass();
  return this;
}
ObjectData *c_someclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_someclass::cloneImpl() {
  c_someclass *obj = NEW(c_someclass)();
  cloneSet(obj);
  return obj;
}
void c_someclass::cloneSet(c_someclass *clone) {
  clone->m_zot = m_zot;
  ObjectData::cloneSet(clone);
}
Variant c_someclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_someclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_someclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_someclass$os_get(const char *s) {
  return c_someclass::os_get(s, -1);
}
Variant &cw_someclass$os_lval(const char *s) {
  return c_someclass::os_lval(s, -1);
}
Variant cw_someclass$os_constant(const char *s) {
  return c_someclass::os_constant(s);
}
Variant cw_someclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_someclass::os_invoke(c, s, params, -1, fatal);
}
void c_someclass::init() {
  m_zot = 6LL;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000735.php line 8 */
void c_someclass::t_someclass() {
  INSTANCE_METHOD_INJECTION(someClass, someClass::someClass);
  bool oldInCtor = gasInCtor(true);
  (m_zot = 2LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000735.php line 12 */
Variant c_someclass::ti_test(const char* cls) {
  STATIC_METHOD_INJECTION(someClass, someClass::test);
  Variant v_a;

  (v_a = LINE(13,p_someclass(p_someclass(NEWOBJ(c_someclass)())->create())));
  return ref(v_a);
} /* function */
Object co_someclass(CArrRef params, bool init /* = true */) {
  return Object(p_someclass(NEW(c_someclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000735_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000735.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000735_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("0000735 need support for returning a value from a function by reference\n\n");
  (v_b = LINE(21,c_someclass::t_test()));
  echo(toString(v_b.o_get("zot", 0x7A42596E78DEAC62LL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
