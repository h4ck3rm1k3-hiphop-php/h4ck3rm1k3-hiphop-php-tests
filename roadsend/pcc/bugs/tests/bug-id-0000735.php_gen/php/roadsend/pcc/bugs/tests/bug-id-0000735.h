
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000735_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000735_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0000735.fw.h>

// Declarations
#include <cls/someclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000735_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_someclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000735_h__
