
#include <php/roadsend/pcc/bugs/tests/bug-id-0001140.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001140_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001140.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001140_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_var_dump(1, x_nl2br("test")));
  LINE(3,x_var_dump(1, x_nl2br("")));
  LINE(4,x_var_dump(1, x_nl2br(toString(null))));
  LINE(5,x_var_dump(1, x_nl2br("\r\n")));
  LINE(6,x_var_dump(1, x_nl2br("\n")));
  LINE(7,x_var_dump(1, x_nl2br("\r")));
  LINE(8,x_var_dump(1, x_nl2br("\n\r")));
  LINE(10,x_var_dump(1, x_nl2br("\n\r\r\n\r\r\r\r")));
  LINE(11,x_var_dump(1, x_nl2br("\n\r\n\n\r\n\r\r\n\r\n")));
  LINE(12,x_var_dump(1, x_nl2br("\n\r\n\n\n\n\r\r\r\r\n\r")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
