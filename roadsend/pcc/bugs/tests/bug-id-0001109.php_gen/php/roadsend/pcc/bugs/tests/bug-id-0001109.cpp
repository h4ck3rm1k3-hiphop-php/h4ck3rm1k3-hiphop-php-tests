
#include <php/roadsend/pcc/bugs/tests/bug-id-0001109.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001109.php line 3 */
void f_test(CVarRef v_val, CVarRef v_key) {
  FUNCTION_INJECTION(test);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_globalArray __attribute__((__unused__)) = g->GV(globalArray);
  gv_globalArray.append((v_key));
  gv_globalArray.append((toString(v_key)));
  print(LINE(8,concat5("val: ", toString(v_val), "; key: ", toString(v_key), "\n")));
  x_flush();
} /* function */
Variant i_test(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x37349B25A0ED29E7LL, test) {
    return (f_test(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001109_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001109.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001109_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_arr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("arr") : g->GV(arr);
  Variant &v_globalArray __attribute__((__unused__)) = (variables != gVariables) ? variables->get("globalArray") : g->GV(globalArray);

  (v_arr = ScalarArrays::sa_[0]);
  LINE(12,x_array_walk(ref(v_arr), "test"));
  print(concat("First value: ", toString(v_globalArray.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
  print("\nDone\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
