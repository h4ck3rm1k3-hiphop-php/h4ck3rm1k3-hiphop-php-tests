
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001152_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001152_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001152.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001152_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_error_hdlr(CVarRef v_errno, CVarRef v_errstr);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001152_h__
