
#include <php/roadsend/pcc/bugs/tests/bug-id-0000962.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000962.php line 5 */
void f_baddefault(CVarRef v_a //  = false
) {
  FUNCTION_INJECTION(badDefault);
  echo(LINE(6,concat3("made it, a is ", toString(v_a), "\n")));
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000962_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000962.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000962_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("0000962 user defined functions with default values of false can fail\n \tapparently for the same reason we can't have a default of #f for defbuiltins, user defined functions can't either:\n");
  LINE(9,f_baddefault("hi"));
  LINE(10,f_baddefault());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
