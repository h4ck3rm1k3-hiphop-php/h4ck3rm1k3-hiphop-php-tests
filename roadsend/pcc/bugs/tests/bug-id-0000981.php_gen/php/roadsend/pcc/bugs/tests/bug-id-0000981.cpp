
#include <php/roadsend/pcc/bugs/tests/bug-id-0000981.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000981_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000981.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000981_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_looper __attribute__((__unused__)) = (variables != gVariables) ? variables->get("looper") : g->GV(looper);

  echo("continue accepts an optional integer argument\n\n");
  echo("\n****** while loop\n");
  (v_i = 0LL);
  LOOP_COUNTER(1);
  {
    while (less(v_i++, 5LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo("Outer\n");
        LOOP_COUNTER(2);
        {
          while (toBoolean(1LL)) {
            LOOP_COUNTER_CHECK(2);
            {
              echo("\tMiddle\n");
              LOOP_COUNTER(3);
              {
                while (toBoolean(1LL)) {
                  LOOP_COUNTER_CHECK(3);
                  {
                    echo("\tInner\n");
                    goto continue1;
                  }
                }
              }
              echo("This never gets output.\n");
            }
          }
        }
        echo("Neither does this.\n");
      }
      continue1:;
    }
  }
  echo("\n****** for loop\n");
  {
    LOOP_COUNTER(4);
    for ((v_i = 0LL); less(v_i, 5LL); v_i++) {
      LOOP_COUNTER_CHECK(4);
      {
        echo("Outer\n");
        {
          LOOP_COUNTER(5);
          for (; ; ) {
            LOOP_COUNTER_CHECK(5);
            {
              echo("\tMiddle\n");
              {
                LOOP_COUNTER(6);
                for (; ; ) {
                  LOOP_COUNTER_CHECK(6);
                  {
                    echo("\tInner\n");
                    goto continue4;
                  }
                }
              }
              echo("This never gets output.\n");
            }
          }
        }
        echo("Neither does this.\n");
      }
      continue4:;
    }
  }
  echo("\n****** foreach loop\n");
  (v_looper = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_looper.begin(); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_i = iter9->second();
      {
        echo("Outer\n");
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = v_looper.begin(); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_i = iter12->second();
            {
              echo("\tMiddle\n");
              {
                LOOP_COUNTER(13);
                for (ArrayIterPtr iter15 = v_looper.begin(); !iter15->end(); iter15->next()) {
                  LOOP_COUNTER_CHECK(13);
                  v_i = iter15->second();
                  {
                    echo("\tInner\n");
                    goto continue7;
                  }
                }
              }
              echo("This never gets output.\n");
            }
          }
        }
        echo("Neither does this.\n");
      }
      continue7:;
    }
  }
  echo("\n****** do loop\n");
  (v_i = 0LL);
  {
    LOOP_COUNTER(16);
    do {
      LOOP_COUNTER_CHECK(16);
      {
        echo("Outer\n");
        {
          LOOP_COUNTER(17);
          do {
            LOOP_COUNTER_CHECK(17);
            {
              echo("\tMiddle\n");
              {
                LOOP_COUNTER(18);
                do {
                  LOOP_COUNTER_CHECK(18);
                  {
                    echo("\tInner\n");
                    goto continue16;
                  }
                } while (toBoolean(1LL));
              }
              echo("This never gets output.\n");
            }
          } while (toBoolean(1LL));
        }
        echo("Neither does this.\n");
      }
      continue16:;
    } while (less(v_i++, 4LL));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
