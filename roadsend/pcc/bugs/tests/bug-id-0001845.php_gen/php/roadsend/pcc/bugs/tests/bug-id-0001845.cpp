
#include <php/roadsend/pcc/bugs/tests/bug-id-0001845.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("include", m_include));
  props.push_back(NEW(ArrayElement)("include_once", m_include_once));
  props.push_back(NEW(ArrayElement)("require", m_require));
  props.push_back(NEW(ArrayElement)("require_once", m_require_once));
  props.push_back(NEW(ArrayElement)("continue", m_continue));
  props.push_back(NEW(ArrayElement)("define", m_define));
  props.push_back(NEW(ArrayElement)("parent", m_parent));
  props.push_back(NEW(ArrayElement)("exit", m_exit));
  props.push_back(NEW(ArrayElement)("false", m_false));
  props.push_back(NEW(ArrayElement)("true", m_true));
  props.push_back(NEW(ArrayElement)("echo", m_echo));
  props.push_back(NEW(ArrayElement)("print", m_print));
  props.push_back(NEW(ArrayElement)("if", m_if));
  props.push_back(NEW(ArrayElement)("else", m_else));
  props.push_back(NEW(ArrayElement)("elseif", m_elseif));
  props.push_back(NEW(ArrayElement)("while", m_while));
  props.push_back(NEW(ArrayElement)("do", m_do));
  props.push_back(NEW(ArrayElement)("or", m_or));
  props.push_back(NEW(ArrayElement)("xor", m_xor));
  props.push_back(NEW(ArrayElement)("and", m_and));
  props.push_back(NEW(ArrayElement)("endwhile", m_endwhile));
  props.push_back(NEW(ArrayElement)("endif", m_endif));
  props.push_back(NEW(ArrayElement)("for", m_for));
  props.push_back(NEW(ArrayElement)("foreach", m_foreach));
  props.push_back(NEW(ArrayElement)("as", m_as));
  props.push_back(NEW(ArrayElement)("unset", m_unset));
  props.push_back(NEW(ArrayElement)("function", m_function));
  props.push_back(NEW(ArrayElement)("var", m_var));
  props.push_back(NEW(ArrayElement)("class", m_class));
  props.push_back(NEW(ArrayElement)("extends", m_extends));
  props.push_back(NEW(ArrayElement)("array", m_array));
  props.push_back(NEW(ArrayElement)("list", m_list));
  props.push_back(NEW(ArrayElement)("new", m_new));
  props.push_back(NEW(ArrayElement)("return", m_return));
  props.push_back(NEW(ArrayElement)("global", m_global));
  props.push_back(NEW(ArrayElement)("static", m_static));
  props.push_back(NEW(ArrayElement)("switch", m_switch));
  props.push_back(NEW(ArrayElement)("endswitch", m_endswitch));
  props.push_back(NEW(ArrayElement)("default", m_default));
  props.push_back(NEW(ArrayElement)("break", m_break));
  props.push_back(NEW(ArrayElement)("case", m_case));
  props.push_back(NEW(ArrayElement)("null", m_null));
  props.push_back(NEW(ArrayElement)("bool", m_bool));
  props.push_back(NEW(ArrayElement)("boolean", m_boolean));
  props.push_back(NEW(ArrayElement)("int", m_int));
  props.push_back(NEW(ArrayElement)("integer", m_integer));
  props.push_back(NEW(ArrayElement)("float", m_float));
  props.push_back(NEW(ArrayElement)("real", m_real));
  props.push_back(NEW(ArrayElement)("double", m_double));
  props.push_back(NEW(ArrayElement)("string", m_string));
  props.push_back(NEW(ArrayElement)("object", m_object));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 2:
      HASH_EXISTS_STRING(0x2027864469AD4382LL, string, 6);
      break;
    case 7:
      HASH_EXISTS_STRING(0x47E3F1B76CF54007LL, do, 2);
      break;
    case 10:
      HASH_EXISTS_STRING(0x45D0A0098E6C668ALL, new, 3);
      break;
    case 11:
      HASH_EXISTS_STRING(0x6E9E419AB157050BLL, for, 3);
      break;
    case 12:
      HASH_EXISTS_STRING(0x16E2F26FFB10FD8CLL, parent, 6);
      break;
    case 14:
      HASH_EXISTS_STRING(0x78E45F7B558DB28ELL, bool, 4);
      break;
    case 17:
      HASH_EXISTS_STRING(0x42466A24EC29D691LL, include, 7);
      break;
    case 18:
      HASH_EXISTS_STRING(0x45397FE5C82DBD12LL, class, 5);
      break;
    case 22:
      HASH_EXISTS_STRING(0x4B19ECD5B61B5296LL, global, 6);
      break;
    case 24:
      HASH_EXISTS_STRING(0x459CEA8F1CA01918LL, require_once, 12);
      break;
    case 31:
      HASH_EXISTS_STRING(0x75CC2E090DE8C99FLL, echo, 4);
      break;
    case 32:
      HASH_EXISTS_STRING(0x2EB2436AE4D58920LL, xor, 3);
      HASH_EXISTS_STRING(0x0953EE1CC9042120LL, var, 3);
      break;
    case 36:
      HASH_EXISTS_STRING(0x77959A48F3CD72A4LL, or, 2);
      break;
    case 38:
      HASH_EXISTS_STRING(0x3ECA0BAAEC2B7CA6LL, require, 7);
      HASH_EXISTS_STRING(0x042A876FC8CE68A6LL, print, 5);
      HASH_EXISTS_STRING(0x5470793341E95EA6LL, break, 5);
      break;
    case 40:
      HASH_EXISTS_STRING(0x782EAD2F77AC7AA8LL, else, 4);
      break;
    case 42:
      HASH_EXISTS_STRING(0x40E6EE81B7A92CAALL, endwhile, 8);
      break;
    case 44:
      HASH_EXISTS_STRING(0x2FF6F3DFF927FF2CLL, null, 4);
      break;
    case 46:
      HASH_EXISTS_STRING(0x0AF5110A234CE62ELL, elseif, 6);
      break;
    case 49:
      HASH_EXISTS_STRING(0x736D912A52403931LL, function, 8);
      break;
    case 50:
      HASH_EXISTS_STRING(0x507C12C34EF2AF32LL, list, 4);
      break;
    case 51:
      HASH_EXISTS_STRING(0x4303E20C1191F133LL, define, 6);
      break;
    case 56:
      HASH_EXISTS_STRING(0x02D4DFB036A5B738LL, extends, 7);
      break;
    case 57:
      HASH_EXISTS_STRING(0x3E1768D82FC7EBB9LL, while, 5);
      break;
    case 68:
      HASH_EXISTS_STRING(0x3D1E3BCD102BFBC4LL, true, 4);
      break;
    case 71:
      HASH_EXISTS_STRING(0x0AF3F92B41D50147LL, continue, 8);
      break;
    case 76:
      HASH_EXISTS_STRING(0x6DE26F84570270CCLL, default, 7);
      break;
    case 78:
      HASH_EXISTS_STRING(0x70BCE0E305461CCELL, switch, 6);
      break;
    case 79:
      HASH_EXISTS_STRING(0x0337E0CD99E343CFLL, as, 2);
      break;
    case 82:
      HASH_EXISTS_STRING(0x2C3A4E68B7D92AD2LL, unset, 5);
      break;
    case 86:
      HASH_EXISTS_STRING(0x03CADA938C98C156LL, false, 5);
      HASH_EXISTS_STRING(0x17A2E16B41CAAED6LL, if, 2);
      break;
    case 90:
      HASH_EXISTS_STRING(0x461F139A4051755ALL, exit, 4);
      break;
    case 93:
      HASH_EXISTS_STRING(0x1F5751E5F08D205DLL, static, 6);
      HASH_EXISTS_STRING(0x255E323BB62FF05DLL, integer, 7);
      break;
    case 95:
      HASH_EXISTS_STRING(0x65D6DECE84DDBADFLL, include_once, 12);
      break;
    case 96:
      HASH_EXISTS_STRING(0x063D35168C04B960LL, array, 5);
      break;
    case 97:
      HASH_EXISTS_STRING(0x7F30BC4E222B1861LL, object, 6);
      break;
    case 100:
      HASH_EXISTS_STRING(0x19AB2CA7919BFFE4LL, float, 5);
      break;
    case 101:
      HASH_EXISTS_STRING(0x4B4A863A179152E5LL, case, 4);
      break;
    case 102:
      HASH_EXISTS_STRING(0x414221625E0A0BE6LL, foreach, 7);
      break;
    case 106:
      HASH_EXISTS_STRING(0x0BFE5B874F6CA0EALL, endswitch, 9);
      break;
    case 110:
      HASH_EXISTS_STRING(0x77EC1C5DADA7D86ELL, real, 4);
      break;
    case 112:
      HASH_EXISTS_STRING(0x37AD4EBE928A1EF0LL, int, 3);
      break;
    case 114:
      HASH_EXISTS_STRING(0x2A644C3D30DF4872LL, return, 6);
      break;
    case 116:
      HASH_EXISTS_STRING(0x2A3C5D9AB36538F4LL, boolean, 7);
      break;
    case 122:
      HASH_EXISTS_STRING(0x104F6C1CAEA2D57ALL, double, 6);
      break;
    case 123:
      HASH_EXISTS_STRING(0x359BD4DD0B78B1FBLL, and, 3);
      HASH_EXISTS_STRING(0x42707D58BBF5BB7BLL, endif, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 2:
      HASH_RETURN_STRING(0x2027864469AD4382LL, m_string,
                         string, 6);
      break;
    case 7:
      HASH_RETURN_STRING(0x47E3F1B76CF54007LL, m_do,
                         do, 2);
      break;
    case 10:
      HASH_RETURN_STRING(0x45D0A0098E6C668ALL, m_new,
                         new, 3);
      break;
    case 11:
      HASH_RETURN_STRING(0x6E9E419AB157050BLL, m_for,
                         for, 3);
      break;
    case 12:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    case 14:
      HASH_RETURN_STRING(0x78E45F7B558DB28ELL, m_bool,
                         bool, 4);
      break;
    case 17:
      HASH_RETURN_STRING(0x42466A24EC29D691LL, m_include,
                         include, 7);
      break;
    case 18:
      HASH_RETURN_STRING(0x45397FE5C82DBD12LL, m_class,
                         class, 5);
      break;
    case 22:
      HASH_RETURN_STRING(0x4B19ECD5B61B5296LL, m_global,
                         global, 6);
      break;
    case 24:
      HASH_RETURN_STRING(0x459CEA8F1CA01918LL, m_require_once,
                         require_once, 12);
      break;
    case 31:
      HASH_RETURN_STRING(0x75CC2E090DE8C99FLL, m_echo,
                         echo, 4);
      break;
    case 32:
      HASH_RETURN_STRING(0x2EB2436AE4D58920LL, m_xor,
                         xor, 3);
      HASH_RETURN_STRING(0x0953EE1CC9042120LL, m_var,
                         var, 3);
      break;
    case 36:
      HASH_RETURN_STRING(0x77959A48F3CD72A4LL, m_or,
                         or, 2);
      break;
    case 38:
      HASH_RETURN_STRING(0x3ECA0BAAEC2B7CA6LL, m_require,
                         require, 7);
      HASH_RETURN_STRING(0x042A876FC8CE68A6LL, m_print,
                         print, 5);
      HASH_RETURN_STRING(0x5470793341E95EA6LL, m_break,
                         break, 5);
      break;
    case 40:
      HASH_RETURN_STRING(0x782EAD2F77AC7AA8LL, m_else,
                         else, 4);
      break;
    case 42:
      HASH_RETURN_STRING(0x40E6EE81B7A92CAALL, m_endwhile,
                         endwhile, 8);
      break;
    case 44:
      HASH_RETURN_STRING(0x2FF6F3DFF927FF2CLL, m_null,
                         null, 4);
      break;
    case 46:
      HASH_RETURN_STRING(0x0AF5110A234CE62ELL, m_elseif,
                         elseif, 6);
      break;
    case 49:
      HASH_RETURN_STRING(0x736D912A52403931LL, m_function,
                         function, 8);
      break;
    case 50:
      HASH_RETURN_STRING(0x507C12C34EF2AF32LL, m_list,
                         list, 4);
      break;
    case 51:
      HASH_RETURN_STRING(0x4303E20C1191F133LL, m_define,
                         define, 6);
      break;
    case 56:
      HASH_RETURN_STRING(0x02D4DFB036A5B738LL, m_extends,
                         extends, 7);
      break;
    case 57:
      HASH_RETURN_STRING(0x3E1768D82FC7EBB9LL, m_while,
                         while, 5);
      break;
    case 68:
      HASH_RETURN_STRING(0x3D1E3BCD102BFBC4LL, m_true,
                         true, 4);
      break;
    case 71:
      HASH_RETURN_STRING(0x0AF3F92B41D50147LL, m_continue,
                         continue, 8);
      break;
    case 76:
      HASH_RETURN_STRING(0x6DE26F84570270CCLL, m_default,
                         default, 7);
      break;
    case 78:
      HASH_RETURN_STRING(0x70BCE0E305461CCELL, m_switch,
                         switch, 6);
      break;
    case 79:
      HASH_RETURN_STRING(0x0337E0CD99E343CFLL, m_as,
                         as, 2);
      break;
    case 82:
      HASH_RETURN_STRING(0x2C3A4E68B7D92AD2LL, m_unset,
                         unset, 5);
      break;
    case 86:
      HASH_RETURN_STRING(0x03CADA938C98C156LL, m_false,
                         false, 5);
      HASH_RETURN_STRING(0x17A2E16B41CAAED6LL, m_if,
                         if, 2);
      break;
    case 90:
      HASH_RETURN_STRING(0x461F139A4051755ALL, m_exit,
                         exit, 4);
      break;
    case 93:
      HASH_RETURN_STRING(0x1F5751E5F08D205DLL, m_static,
                         static, 6);
      HASH_RETURN_STRING(0x255E323BB62FF05DLL, m_integer,
                         integer, 7);
      break;
    case 95:
      HASH_RETURN_STRING(0x65D6DECE84DDBADFLL, m_include_once,
                         include_once, 12);
      break;
    case 96:
      HASH_RETURN_STRING(0x063D35168C04B960LL, m_array,
                         array, 5);
      break;
    case 97:
      HASH_RETURN_STRING(0x7F30BC4E222B1861LL, m_object,
                         object, 6);
      break;
    case 100:
      HASH_RETURN_STRING(0x19AB2CA7919BFFE4LL, m_float,
                         float, 5);
      break;
    case 101:
      HASH_RETURN_STRING(0x4B4A863A179152E5LL, m_case,
                         case, 4);
      break;
    case 102:
      HASH_RETURN_STRING(0x414221625E0A0BE6LL, m_foreach,
                         foreach, 7);
      break;
    case 106:
      HASH_RETURN_STRING(0x0BFE5B874F6CA0EALL, m_endswitch,
                         endswitch, 9);
      break;
    case 110:
      HASH_RETURN_STRING(0x77EC1C5DADA7D86ELL, m_real,
                         real, 4);
      break;
    case 112:
      HASH_RETURN_STRING(0x37AD4EBE928A1EF0LL, m_int,
                         int, 3);
      break;
    case 114:
      HASH_RETURN_STRING(0x2A644C3D30DF4872LL, m_return,
                         return, 6);
      break;
    case 116:
      HASH_RETURN_STRING(0x2A3C5D9AB36538F4LL, m_boolean,
                         boolean, 7);
      break;
    case 122:
      HASH_RETURN_STRING(0x104F6C1CAEA2D57ALL, m_double,
                         double, 6);
      break;
    case 123:
      HASH_RETURN_STRING(0x359BD4DD0B78B1FBLL, m_and,
                         and, 3);
      HASH_RETURN_STRING(0x42707D58BBF5BB7BLL, m_endif,
                         endif, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 2:
      HASH_SET_STRING(0x2027864469AD4382LL, m_string,
                      string, 6);
      break;
    case 7:
      HASH_SET_STRING(0x47E3F1B76CF54007LL, m_do,
                      do, 2);
      break;
    case 10:
      HASH_SET_STRING(0x45D0A0098E6C668ALL, m_new,
                      new, 3);
      break;
    case 11:
      HASH_SET_STRING(0x6E9E419AB157050BLL, m_for,
                      for, 3);
      break;
    case 12:
      HASH_SET_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                      parent, 6);
      break;
    case 14:
      HASH_SET_STRING(0x78E45F7B558DB28ELL, m_bool,
                      bool, 4);
      break;
    case 17:
      HASH_SET_STRING(0x42466A24EC29D691LL, m_include,
                      include, 7);
      break;
    case 18:
      HASH_SET_STRING(0x45397FE5C82DBD12LL, m_class,
                      class, 5);
      break;
    case 22:
      HASH_SET_STRING(0x4B19ECD5B61B5296LL, m_global,
                      global, 6);
      break;
    case 24:
      HASH_SET_STRING(0x459CEA8F1CA01918LL, m_require_once,
                      require_once, 12);
      break;
    case 31:
      HASH_SET_STRING(0x75CC2E090DE8C99FLL, m_echo,
                      echo, 4);
      break;
    case 32:
      HASH_SET_STRING(0x2EB2436AE4D58920LL, m_xor,
                      xor, 3);
      HASH_SET_STRING(0x0953EE1CC9042120LL, m_var,
                      var, 3);
      break;
    case 36:
      HASH_SET_STRING(0x77959A48F3CD72A4LL, m_or,
                      or, 2);
      break;
    case 38:
      HASH_SET_STRING(0x3ECA0BAAEC2B7CA6LL, m_require,
                      require, 7);
      HASH_SET_STRING(0x042A876FC8CE68A6LL, m_print,
                      print, 5);
      HASH_SET_STRING(0x5470793341E95EA6LL, m_break,
                      break, 5);
      break;
    case 40:
      HASH_SET_STRING(0x782EAD2F77AC7AA8LL, m_else,
                      else, 4);
      break;
    case 42:
      HASH_SET_STRING(0x40E6EE81B7A92CAALL, m_endwhile,
                      endwhile, 8);
      break;
    case 44:
      HASH_SET_STRING(0x2FF6F3DFF927FF2CLL, m_null,
                      null, 4);
      break;
    case 46:
      HASH_SET_STRING(0x0AF5110A234CE62ELL, m_elseif,
                      elseif, 6);
      break;
    case 49:
      HASH_SET_STRING(0x736D912A52403931LL, m_function,
                      function, 8);
      break;
    case 50:
      HASH_SET_STRING(0x507C12C34EF2AF32LL, m_list,
                      list, 4);
      break;
    case 51:
      HASH_SET_STRING(0x4303E20C1191F133LL, m_define,
                      define, 6);
      break;
    case 56:
      HASH_SET_STRING(0x02D4DFB036A5B738LL, m_extends,
                      extends, 7);
      break;
    case 57:
      HASH_SET_STRING(0x3E1768D82FC7EBB9LL, m_while,
                      while, 5);
      break;
    case 68:
      HASH_SET_STRING(0x3D1E3BCD102BFBC4LL, m_true,
                      true, 4);
      break;
    case 71:
      HASH_SET_STRING(0x0AF3F92B41D50147LL, m_continue,
                      continue, 8);
      break;
    case 76:
      HASH_SET_STRING(0x6DE26F84570270CCLL, m_default,
                      default, 7);
      break;
    case 78:
      HASH_SET_STRING(0x70BCE0E305461CCELL, m_switch,
                      switch, 6);
      break;
    case 79:
      HASH_SET_STRING(0x0337E0CD99E343CFLL, m_as,
                      as, 2);
      break;
    case 82:
      HASH_SET_STRING(0x2C3A4E68B7D92AD2LL, m_unset,
                      unset, 5);
      break;
    case 86:
      HASH_SET_STRING(0x03CADA938C98C156LL, m_false,
                      false, 5);
      HASH_SET_STRING(0x17A2E16B41CAAED6LL, m_if,
                      if, 2);
      break;
    case 90:
      HASH_SET_STRING(0x461F139A4051755ALL, m_exit,
                      exit, 4);
      break;
    case 93:
      HASH_SET_STRING(0x1F5751E5F08D205DLL, m_static,
                      static, 6);
      HASH_SET_STRING(0x255E323BB62FF05DLL, m_integer,
                      integer, 7);
      break;
    case 95:
      HASH_SET_STRING(0x65D6DECE84DDBADFLL, m_include_once,
                      include_once, 12);
      break;
    case 96:
      HASH_SET_STRING(0x063D35168C04B960LL, m_array,
                      array, 5);
      break;
    case 97:
      HASH_SET_STRING(0x7F30BC4E222B1861LL, m_object,
                      object, 6);
      break;
    case 100:
      HASH_SET_STRING(0x19AB2CA7919BFFE4LL, m_float,
                      float, 5);
      break;
    case 101:
      HASH_SET_STRING(0x4B4A863A179152E5LL, m_case,
                      case, 4);
      break;
    case 102:
      HASH_SET_STRING(0x414221625E0A0BE6LL, m_foreach,
                      foreach, 7);
      break;
    case 106:
      HASH_SET_STRING(0x0BFE5B874F6CA0EALL, m_endswitch,
                      endswitch, 9);
      break;
    case 110:
      HASH_SET_STRING(0x77EC1C5DADA7D86ELL, m_real,
                      real, 4);
      break;
    case 112:
      HASH_SET_STRING(0x37AD4EBE928A1EF0LL, m_int,
                      int, 3);
      break;
    case 114:
      HASH_SET_STRING(0x2A644C3D30DF4872LL, m_return,
                      return, 6);
      break;
    case 116:
      HASH_SET_STRING(0x2A3C5D9AB36538F4LL, m_boolean,
                      boolean, 7);
      break;
    case 122:
      HASH_SET_STRING(0x104F6C1CAEA2D57ALL, m_double,
                      double, 6);
      break;
    case 123:
      HASH_SET_STRING(0x359BD4DD0B78B1FBLL, m_and,
                      and, 3);
      HASH_SET_STRING(0x42707D58BBF5BB7BLL, m_endif,
                      endif, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_include = m_include;
  clone->m_include_once = m_include_once;
  clone->m_require = m_require;
  clone->m_require_once = m_require_once;
  clone->m_continue = m_continue;
  clone->m_define = m_define;
  clone->m_parent = m_parent;
  clone->m_exit = m_exit;
  clone->m_false = m_false;
  clone->m_true = m_true;
  clone->m_echo = m_echo;
  clone->m_print = m_print;
  clone->m_if = m_if;
  clone->m_else = m_else;
  clone->m_elseif = m_elseif;
  clone->m_while = m_while;
  clone->m_do = m_do;
  clone->m_or = m_or;
  clone->m_xor = m_xor;
  clone->m_and = m_and;
  clone->m_endwhile = m_endwhile;
  clone->m_endif = m_endif;
  clone->m_for = m_for;
  clone->m_foreach = m_foreach;
  clone->m_as = m_as;
  clone->m_unset = m_unset;
  clone->m_function = m_function;
  clone->m_var = m_var;
  clone->m_class = m_class;
  clone->m_extends = m_extends;
  clone->m_array = m_array;
  clone->m_list = m_list;
  clone->m_new = m_new;
  clone->m_return = m_return;
  clone->m_global = m_global;
  clone->m_static = m_static;
  clone->m_switch = m_switch;
  clone->m_endswitch = m_endswitch;
  clone->m_default = m_default;
  clone->m_break = m_break;
  clone->m_case = m_case;
  clone->m_null = m_null;
  clone->m_bool = m_bool;
  clone->m_boolean = m_boolean;
  clone->m_int = m_int;
  clone->m_integer = m_integer;
  clone->m_float = m_float;
  clone->m_real = m_real;
  clone->m_double = m_double;
  clone->m_string = m_string;
  clone->m_object = m_object;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x01F7BC290731BD81LL, boolean) {
        return (t_boolean(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x2FEB3ED9AB82BCC2LL, int) {
        return (t_int(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x4BBFB807B38E2EE8LL, integer) {
        return (t_integer(), null);
      }
      HASH_GUARD(0x2F5C0893D1128668LL, real) {
        return (t_real(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x7364AD4E26CEF7E9LL, bool) {
        return (t_bool(), null);
      }
      HASH_GUARD(0x15B369BE0D0C8149LL, string) {
        return (t_string(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x3078202CA3F8DCEELL, true) {
        return (t_true(), null);
      }
      break;
    case 16:
      HASH_GUARD(0x297533C26B28BDF0LL, double) {
        return (t_double(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x6F900C618583E4F3LL, null) {
        return (t_null(), null);
      }
      break;
    case 21:
      HASH_GUARD(0x25D578B4772C1715LL, define) {
        return (t_define(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (t_object(), null);
      }
      break;
    case 28:
      HASH_GUARD(0x25CDD385D0DF085CLL, false) {
        return (t_false(), null);
      }
      break;
    case 30:
      HASH_GUARD(0x5985905F1404E87ELL, float) {
        return (t_float(), null);
      }
      HASH_GUARD(0x53DCFFF92D13045ELL, parent) {
        return (t_parent(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 1:
      HASH_GUARD(0x01F7BC290731BD81LL, boolean) {
        return (t_boolean(), null);
      }
      break;
    case 2:
      HASH_GUARD(0x2FEB3ED9AB82BCC2LL, int) {
        return (t_int(), null);
      }
      break;
    case 8:
      HASH_GUARD(0x4BBFB807B38E2EE8LL, integer) {
        return (t_integer(), null);
      }
      HASH_GUARD(0x2F5C0893D1128668LL, real) {
        return (t_real(), null);
      }
      break;
    case 9:
      HASH_GUARD(0x7364AD4E26CEF7E9LL, bool) {
        return (t_bool(), null);
      }
      HASH_GUARD(0x15B369BE0D0C8149LL, string) {
        return (t_string(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x3078202CA3F8DCEELL, true) {
        return (t_true(), null);
      }
      break;
    case 16:
      HASH_GUARD(0x297533C26B28BDF0LL, double) {
        return (t_double(), null);
      }
      break;
    case 19:
      HASH_GUARD(0x6F900C618583E4F3LL, null) {
        return (t_null(), null);
      }
      break;
    case 21:
      HASH_GUARD(0x25D578B4772C1715LL, define) {
        return (t_define(), null);
      }
      break;
    case 26:
      HASH_GUARD(0x2F0C4AEA911ADA3ALL, object) {
        return (t_object(), null);
      }
      break;
    case 28:
      HASH_GUARD(0x25CDD385D0DF085CLL, false) {
        return (t_false(), null);
      }
      break;
    case 30:
      HASH_GUARD(0x5985905F1404E87ELL, float) {
        return (t_float(), null);
      }
      HASH_GUARD(0x53DCFFF92D13045ELL, parent) {
        return (t_parent(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_include = "include";
  m_include_once = "include_once";
  m_require = "require";
  m_require_once = "require_once";
  m_continue = "continue";
  m_define = "define";
  m_parent = "parent";
  m_exit = "exit";
  m_false = "false";
  m_true = "true";
  m_echo = "echo";
  m_print = "print";
  m_if = "if";
  m_else = "else";
  m_elseif = "elseif";
  m_while = "while";
  m_do = "do";
  m_or = "or";
  m_xor = "xor";
  m_and = "and";
  m_endwhile = "endwhile";
  m_endif = "endif";
  m_for = "for";
  m_foreach = "foreach";
  m_as = "as";
  m_unset = "unset";
  m_function = "function";
  m_var = "var";
  m_class = "class";
  m_extends = "extends";
  m_array = "array";
  m_list = "list";
  m_new = "new";
  m_return = "return";
  m_global = "global";
  m_static = "static";
  m_switch = "switch";
  m_endswitch = "endswitch";
  m_default = "default";
  m_break = "break";
  m_case = "case";
  m_null = "null";
  m_bool = "bool";
  m_boolean = "boolean";
  m_int = "int";
  m_integer = "integer";
  m_float = "float";
  m_real = "real";
  m_double = "double";
  m_string = "string";
  m_object = "object";
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 61 */
void c_foo::t_define() {
  INSTANCE_METHOD_INJECTION(foo, foo::define);
  echo("called define\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 62 */
void c_foo::t_parent() {
  INSTANCE_METHOD_INJECTION(foo, foo::parent);
  echo("called parent\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 64 */
void c_foo::t_false() {
  INSTANCE_METHOD_INJECTION(foo, foo::false);
  echo("called false\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 65 */
void c_foo::t_true() {
  INSTANCE_METHOD_INJECTION(foo, foo::true);
  echo("called true\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 98 */
void c_foo::t_null() {
  INSTANCE_METHOD_INJECTION(foo, foo::null);
  echo("called null\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 99 */
void c_foo::t_bool() {
  INSTANCE_METHOD_INJECTION(foo, foo::bool);
  echo("called bool\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 100 */
void c_foo::t_boolean() {
  INSTANCE_METHOD_INJECTION(foo, foo::boolean);
  echo("called boolean\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 101 */
void c_foo::t_int() {
  INSTANCE_METHOD_INJECTION(foo, foo::int);
  echo("called int\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 102 */
void c_foo::t_integer() {
  INSTANCE_METHOD_INJECTION(foo, foo::integer);
  echo("called integer\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 103 */
void c_foo::t_float() {
  INSTANCE_METHOD_INJECTION(foo, foo::float);
  echo("called float\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 104 */
void c_foo::t_real() {
  INSTANCE_METHOD_INJECTION(foo, foo::real);
  echo("called real\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 105 */
void c_foo::t_double() {
  INSTANCE_METHOD_INJECTION(foo, foo::double);
  echo("called double\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 106 */
void c_foo::t_string() {
  INSTANCE_METHOD_INJECTION(foo, foo::string);
  echo("called string\n");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001845.php line 107 */
void c_foo::t_object() {
  INSTANCE_METHOD_INJECTION(foo, foo::object);
  echo("called object\n");
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001845_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001845.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001845_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_afoo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("afoo") : g->GV(afoo);

  (v_afoo = ((Object)(LINE(110,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  echo(LINE(112,concat3("property 'include': ", toString(v_afoo.o_get("include", 0x42466A24EC29D691LL)), "\n")));
  echo(LINE(113,concat3("property 'include_once': ", toString(v_afoo.o_get("include_once", 0x65D6DECE84DDBADFLL)), "\n")));
  echo(LINE(114,concat3("property 'require': ", toString(v_afoo.o_get("require", 0x3ECA0BAAEC2B7CA6LL)), "\n")));
  echo(LINE(115,concat3("property 'require_once': ", toString(v_afoo.o_get("require_once", 0x459CEA8F1CA01918LL)), "\n")));
  echo(LINE(116,concat3("property 'continue': ", toString(v_afoo.o_get("continue", 0x0AF3F92B41D50147LL)), "\n")));
  echo(LINE(117,concat3("property 'define': ", toString(v_afoo.o_get("define", 0x4303E20C1191F133LL)), "\n")));
  echo(LINE(118,concat3("property 'parent': ", toString(v_afoo.o_get("parent", 0x16E2F26FFB10FD8CLL)), "\n")));
  echo(LINE(119,concat3("property 'exit': ", toString(v_afoo.o_get("exit", 0x461F139A4051755ALL)), "\n")));
  echo(LINE(120,concat3("property 'false': ", toString(v_afoo.o_get("false", 0x03CADA938C98C156LL)), "\n")));
  echo(LINE(121,concat3("property 'true': ", toString(v_afoo.o_get("true", 0x3D1E3BCD102BFBC4LL)), "\n")));
  echo(LINE(122,concat3("property 'echo': ", toString(v_afoo.o_get("echo", 0x75CC2E090DE8C99FLL)), "\n")));
  echo(LINE(123,concat3("property 'print': ", toString(v_afoo.o_get("print", 0x042A876FC8CE68A6LL)), "\n")));
  echo(LINE(124,concat3("property 'if': ", toString(v_afoo.o_get("if", 0x17A2E16B41CAAED6LL)), "\n")));
  echo(LINE(125,concat3("property 'else': ", toString(v_afoo.o_get("else", 0x782EAD2F77AC7AA8LL)), "\n")));
  echo(LINE(126,concat3("property 'elseif': ", toString(v_afoo.o_get("elseif", 0x0AF5110A234CE62ELL)), "\n")));
  echo(LINE(127,concat3("property 'while': ", toString(v_afoo.o_get("while", 0x3E1768D82FC7EBB9LL)), "\n")));
  echo(LINE(128,concat3("property 'do': ", toString(v_afoo.o_get("do", 0x47E3F1B76CF54007LL)), "\n")));
  echo(LINE(129,concat3("property 'or': ", toString(v_afoo.o_get("or", 0x77959A48F3CD72A4LL)), "\n")));
  echo(LINE(130,concat3("property 'xor': ", toString(v_afoo.o_get("xor", 0x2EB2436AE4D58920LL)), "\n")));
  echo(LINE(131,concat3("property 'and': ", toString(v_afoo.o_get("and", 0x359BD4DD0B78B1FBLL)), "\n")));
  echo(LINE(132,concat3("property 'endwhile': ", toString(v_afoo.o_get("endwhile", 0x40E6EE81B7A92CAALL)), "\n")));
  echo(LINE(133,concat3("property 'endif': ", toString(v_afoo.o_get("endif", 0x42707D58BBF5BB7BLL)), "\n")));
  echo(LINE(134,concat3("property 'for': ", toString(v_afoo.o_get("for", 0x6E9E419AB157050BLL)), "\n")));
  echo(LINE(135,concat3("property 'foreach': ", toString(v_afoo.o_get("foreach", 0x414221625E0A0BE6LL)), "\n")));
  echo(LINE(136,concat3("property 'as': ", toString(v_afoo.o_get("as", 0x0337E0CD99E343CFLL)), "\n")));
  echo(LINE(137,concat3("property 'unset': ", toString(v_afoo.o_get("unset", 0x2C3A4E68B7D92AD2LL)), "\n")));
  echo(LINE(138,concat3("property 'function': ", toString(v_afoo.o_get("function", 0x736D912A52403931LL)), "\n")));
  echo(LINE(139,concat3("property 'var': ", toString(v_afoo.o_get("var", 0x0953EE1CC9042120LL)), "\n")));
  echo(LINE(140,concat3("property 'class': ", toString(v_afoo.o_get("class", 0x45397FE5C82DBD12LL)), "\n")));
  echo(LINE(141,concat3("property 'extends': ", toString(v_afoo.o_get("extends", 0x02D4DFB036A5B738LL)), "\n")));
  echo(LINE(142,concat3("property 'array': ", toString(v_afoo.o_get("array", 0x063D35168C04B960LL)), "\n")));
  echo(LINE(143,concat3("property 'list': ", toString(v_afoo.o_get("list", 0x507C12C34EF2AF32LL)), "\n")));
  echo(LINE(144,concat3("property 'new': ", toString(v_afoo.o_get("new", 0x45D0A0098E6C668ALL)), "\n")));
  echo(LINE(145,concat3("property 'return': ", toString(v_afoo.o_get("return", 0x2A644C3D30DF4872LL)), "\n")));
  echo(LINE(146,concat3("property 'break': ", toString(v_afoo.o_get("break", 0x5470793341E95EA6LL)), "\n")));
  echo(LINE(147,concat3("property 'global': ", toString(v_afoo.o_get("global", 0x4B19ECD5B61B5296LL)), "\n")));
  echo(LINE(148,concat3("property 'static': ", toString(v_afoo.o_get("static", 0x1F5751E5F08D205DLL)), "\n")));
  echo(LINE(149,concat3("property 'switch': ", toString(v_afoo.o_get("switch", 0x70BCE0E305461CCELL)), "\n")));
  echo(LINE(150,concat3("property 'endswitch': ", toString(v_afoo.o_get("endswitch", 0x0BFE5B874F6CA0EALL)), "\n")));
  echo(LINE(151,concat3("property 'default': ", toString(v_afoo.o_get("default", 0x6DE26F84570270CCLL)), "\n")));
  echo(LINE(152,concat3("property 'break': ", toString(v_afoo.o_get("break", 0x5470793341E95EA6LL)), "\n")));
  echo(LINE(153,concat3("property 'case': ", toString(v_afoo.o_get("case", 0x4B4A863A179152E5LL)), "\n")));
  echo(LINE(154,concat3("property 'null': ", toString(v_afoo.o_get("null", 0x2FF6F3DFF927FF2CLL)), "\n")));
  echo(LINE(155,concat3("property 'bool': ", toString(v_afoo.o_get("bool", 0x78E45F7B558DB28ELL)), "\n")));
  echo(LINE(156,concat3("property 'boolean': ", toString(v_afoo.o_get("boolean", 0x2A3C5D9AB36538F4LL)), "\n")));
  echo(LINE(157,concat3("property 'int': ", toString(v_afoo.o_get("int", 0x37AD4EBE928A1EF0LL)), "\n")));
  echo(LINE(158,concat3("property 'integer': ", toString(v_afoo.o_get("integer", 0x255E323BB62FF05DLL)), "\n")));
  echo(LINE(159,concat3("property 'float': ", toString(v_afoo.o_get("float", 0x19AB2CA7919BFFE4LL)), "\n")));
  echo(LINE(160,concat3("property 'real': ", toString(v_afoo.o_get("real", 0x77EC1C5DADA7D86ELL)), "\n")));
  echo(LINE(161,concat3("property 'double': ", toString(v_afoo.o_get("double", 0x104F6C1CAEA2D57ALL)), "\n")));
  echo(LINE(162,concat3("property 'string': ", toString(v_afoo.o_get("string", 0x2027864469AD4382LL)), "\n")));
  echo(LINE(163,concat3("property 'object': ", toString(v_afoo.o_get("object", 0x7F30BC4E222B1861LL)), "\n")));
  LINE(172,v_afoo.o_invoke_few_args("define", 0x25D578B4772C1715LL, 0));
  LINE(173,v_afoo.o_invoke_few_args("parent", 0x53DCFFF92D13045ELL, 0));
  LINE(175,v_afoo.o_invoke_few_args("false", 0x25CDD385D0DF085CLL, 0));
  LINE(176,v_afoo.o_invoke_few_args("true", 0x3078202CA3F8DCEELL, 0));
  LINE(209,v_afoo.o_invoke_few_args("null", 0x6F900C618583E4F3LL, 0));
  LINE(210,v_afoo.o_invoke_few_args("bool", 0x7364AD4E26CEF7E9LL, 0));
  LINE(211,v_afoo.o_invoke_few_args("boolean", 0x01F7BC290731BD81LL, 0));
  LINE(212,v_afoo.o_invoke_few_args("int", 0x2FEB3ED9AB82BCC2LL, 0));
  LINE(213,v_afoo.o_invoke_few_args("integer", 0x4BBFB807B38E2EE8LL, 0));
  LINE(214,v_afoo.o_invoke_few_args("float", 0x5985905F1404E87ELL, 0));
  LINE(215,v_afoo.o_invoke_few_args("real", 0x2F5C0893D1128668LL, 0));
  LINE(216,v_afoo.o_invoke_few_args("double", 0x297533C26B28BDF0LL, 0));
  LINE(217,v_afoo.o_invoke_few_args("string", 0x15B369BE0D0C8149LL, 0));
  LINE(218,v_afoo.o_invoke_few_args("object", 0x2F0C4AEA911ADA3ALL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
