
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001845_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001845_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001845.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001845_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001845_h__
