
#include <php/roadsend/pcc/bugs/tests/bug-id-0000923.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(2);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000923_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000923.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000923_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  echo("key or value is optional in list()\n\n");
  (v_a = ScalarArrays::sa_[0]);
  LINE(7,x_reset(ref(v_a)));
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(8,x_each(ref(v_a))), v_v))) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(v_v) + toString("\n"));
      }
    }
  }
  echo("-- next --\n");
  LINE(14,x_reset(ref(v_a)));
  LOOP_COUNTER(2);
  {
    while (toBoolean(df_lambda_2(LINE(15,x_each(ref(v_a))), v_v))) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(toString(v_v) + toString("\n"));
      }
    }
  }
  echo("-- next --\n");
  LINE(21,x_reset(ref(v_a)));
  LOOP_COUNTER(3);
  {
    while (toBoolean(df_lambda_3(LINE(22,x_each(ref(v_a))), v_v))) {
      LOOP_COUNTER_CHECK(3);
      {
        echo(toString(v_v) + toString("\n"));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
