
#ifndef __GENERATED_cls_blah_h__
#define __GENERATED_cls_blah_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0000736.php line 37 */
class c_blah : virtual public ObjectData {
  BEGIN_CLASS_MAP(blah)
  END_CLASS_MAP(blah)
  DECLARE_CLASS(blah, blah, ObjectData)
  void init();
  public: String m_style;
  public: String t_style();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_blah_h__
