
#include <php/roadsend/pcc/bugs/tests/bug-id-0001824.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001824_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001824.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001824_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  echo("Alternative syntax for control structures\n\n");
  if (equal(v_foo, "no")) {
    (v_bar = v_foo);
  }
  else if (toBoolean((v_foo = "yes"))) {}
  echo("\n-- or --\n\n");
  if (equal(v_foo, "no")) {}
  else if (toBoolean((v_foo = "yes"))) {
    (v_bar = v_foo);
  }
  echo(" ");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
