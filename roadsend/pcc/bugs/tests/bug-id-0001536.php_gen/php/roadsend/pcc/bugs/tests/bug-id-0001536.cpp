
#include <php/roadsend/pcc/bugs/tests/bug-id-0001536.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_ZOT = "ZOT";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001536.php line 18 */
void f_foo(CStrRef v_a //  = k_ZOT
) {
  FUNCTION_INJECTION(foo);
  print((LINE(19,concat3("$a is ", v_a, "\n"))));
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001536_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001536.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001536_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("define() needs to happen like a normal statement, and still work as a\ndefault value.\n\n");
  echo(LINE(6,(assignCallTemp(eo_1, toString(x_defined("MEME"))),concat3("hey:", eo_1, ":yeh\n"))));
  LINE(7,g->declareConstant("MEME", g->k_MEME, 1LL));
  echo(LINE(8,(assignCallTemp(eo_1, toString(x_defined("MEME"))),concat3("hey:", eo_1, ":yeh\n"))));
  echo("\nmore\n\n");
  LINE(16,f_foo());
  LINE(22,throw_fatal("bad define"));
  LINE(24,f_foo());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
