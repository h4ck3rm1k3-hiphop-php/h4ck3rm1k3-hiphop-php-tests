
#include <php/roadsend/pcc/bugs/tests/bug-id-0001149.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001149_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001149.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001149_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  if (!(toBoolean(LINE(3,x_ini_get("register_globals"))))) {
    (v_argc = g->gv__SERVER.rvalAt("argc", 0x596A642EB89EED13LL));
    (v_argv = g->gv__SERVER.rvalAt("argv", 0x10EA7DC57768F8C6LL));
  }
  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); less(v_i, v_argc); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(9,concat4((toString(v_i - 1LL)), ": ", toString(v_argv.rvalAt(v_i)), "\n")));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
