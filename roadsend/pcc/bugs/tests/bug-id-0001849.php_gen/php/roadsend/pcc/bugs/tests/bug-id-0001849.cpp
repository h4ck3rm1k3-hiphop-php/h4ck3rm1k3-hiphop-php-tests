
#include <php/roadsend/pcc/bugs/tests/bug-id-0001849.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001849.php line 5 */
void f_lang_load(Variant v_p_lang) {
  FUNCTION_INJECTION(lang_load);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_g_lang_strings __attribute__((__unused__)) = g->GV(g_lang_strings);
  Variant v_g_lang_strings;
  Variant &gv_g_lang_current __attribute__((__unused__)) = g->GV(g_lang_current);
  Variant v_g_lang_current;
  Variant v_t_lang_dir;
  Variant v_t_vars;
  Variant v_t_var;
  Variant v_t_lang_var;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_p_lang; Variant &v_g_lang_strings; Variant &v_g_lang_current; Variant &v_t_lang_dir; Variant &v_t_vars; Variant &v_t_var; Variant &v_t_lang_var;
    VariableTable(Variant &r_p_lang, Variant &r_g_lang_strings, Variant &r_g_lang_current, Variant &r_t_lang_dir, Variant &r_t_vars, Variant &r_t_var, Variant &r_t_lang_var) : v_p_lang(r_p_lang), v_g_lang_strings(r_g_lang_strings), v_g_lang_current(r_g_lang_current), v_t_lang_dir(r_t_lang_dir), v_t_vars(r_t_vars), v_t_var(r_t_var), v_t_lang_var(r_t_lang_var) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 15) {
        case 1:
          HASH_RETURN(0x37AB0D2EFA286271LL, v_g_lang_current,
                      g_lang_current);
          break;
        case 2:
          HASH_RETURN(0x2A37174B043E5942LL, v_t_var,
                      t_var);
          break;
        case 3:
          HASH_RETURN(0x25704EBA419895D3LL, v_t_lang_var,
                      t_lang_var);
          break;
        case 4:
          HASH_RETURN(0x6AE5D830157DAAE4LL, v_t_lang_dir,
                      t_lang_dir);
          break;
        case 9:
          HASH_RETURN(0x18E3746906DAAE59LL, v_g_lang_strings,
                      g_lang_strings);
          HASH_RETURN(0x286CE37BDEDB0F49LL, v_t_vars,
                      t_vars);
          break;
        case 14:
          HASH_RETURN(0x22D697E12F158A3ELL, v_p_lang,
                      p_lang);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
    virtual Array getDefinedVars() {
      Array ret = LVariableTable::getDefinedVars();
      ret.set("p_lang", v_p_lang);
      ret.set("g_lang_strings", v_g_lang_strings);
      ret.set("g_lang_current", v_g_lang_current);
      ret.set("t_lang_dir", v_t_lang_dir);
      ret.set("t_vars", v_t_vars);
      ret.set("t_var", v_t_var);
      ret.set("t_lang_var", v_t_lang_var);
      return ret;
    }
  } variableTable(v_p_lang, v_g_lang_strings, v_g_lang_current, v_t_lang_dir, v_t_vars, v_t_var, v_t_lang_var);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  {
    v_g_lang_strings = ref(g->GV(g_lang_strings));
    v_g_lang_current = ref(g->GV(g_lang_current));
  }
  (v_g_lang_current = v_p_lang);
  (v_t_lang_dir = "./");
  LINE(18,require((concat4(toString(v_t_lang_dir), "strings_", toString(v_p_lang), ".inc")), true, variables, "roadsend/pcc/bugs/tests/"));
  (v_t_vars = LINE(20,get_defined_vars(variables)));
  {
    LOOP_COUNTER(1);
    Variant map2 = LINE(23,x_array_keys(v_t_vars));
    for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_t_var = iter3->second();
      {
        (v_t_lang_var = LINE(24,x_ereg_replace("^s_", "", toString(v_t_var))));
        if (!equal(v_t_lang_var, v_t_var) || equal("MANTIS_ERROR", v_t_var)) {
          v_g_lang_strings.set(v_t_lang_var, (variables->get(toString(v_t_var))));
        }
      }
    }
  }
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001849_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001849.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001849_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_g_lang_strings __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g_lang_strings") : g->GV(g_lang_strings);

  LINE(32,f_lang_load("english"));
  LINE(34,x_print_r(v_g_lang_strings));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
