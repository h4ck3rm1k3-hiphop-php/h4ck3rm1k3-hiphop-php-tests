
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001849_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001849_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001849.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_lang_load(Variant v_p_lang);
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001849_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001849_h__
