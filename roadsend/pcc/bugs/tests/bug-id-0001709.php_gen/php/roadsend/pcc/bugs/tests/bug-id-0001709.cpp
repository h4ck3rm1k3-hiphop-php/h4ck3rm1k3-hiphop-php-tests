
#include <php/roadsend/pcc/bugs/tests/bug-id-0001709.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001709.php line 5 */
void f_test_foreach() {
  FUNCTION_INJECTION(test_foreach);
  Array v_strings;
  Variant v_val;
  String v_op;

  print("\ntesting foreach:\n");
  (v_strings = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIter iter3 = v_strings.begin(); !iter3.end(); ++iter3) {
      LOOP_COUNTER_CHECK(1);
      v_val = iter3.second();
      {
        concat_assign(v_op, LINE(11,concat3("outer ", toString(v_val), ", ")));
        {
          LOOP_COUNTER(4);
          for (ArrayIter iter6 = v_strings.begin(); !iter6.end(); ++iter6) {
            LOOP_COUNTER_CHECK(4);
            v_val = iter6.second();
            {
              if (equal(v_val, 3LL)) {
                break;
              }
              concat_assign(v_op, LINE(16,concat3("inner ", toString(v_val), ", ")));
            }
          }
        }
        concat_assign(v_op, "\n");
      }
    }
  }
  print((v_op + toString("\n")));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001709.php line 39 */
void f_test_while() {
  FUNCTION_INJECTION(test_while);
  int64 v_i = 0;
  String v_op;
  int64 v_j = 0;

  print("\ntesting while:\n");
  (v_i = 0LL);
  LOOP_COUNTER(7);
  {
    while (less(v_i, 4LL)) {
      LOOP_COUNTER_CHECK(7);
      {
        concat_assign(v_op, LINE(44,concat3("outer ", toString(v_i), ", ")));
        (v_j = 0LL);
        LOOP_COUNTER(8);
        {
          while (less(v_j, 4LL)) {
            LOOP_COUNTER_CHECK(8);
            {
              if (equal(v_j, 3LL)) {
                break;
              }
              concat_assign(v_op, LINE(50,concat3("inner ", toString(v_j), ", ")));
              v_j++;
            }
          }
        }
        concat_assign(v_op, "\n");
        v_i++;
      }
    }
  }
  print((v_op + toString("\n")));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001709.php line 59 */
void f_test_do() {
  FUNCTION_INJECTION(test_do);
  int64 v_i = 0;
  String v_op;
  int64 v_j = 0;

  print("\ntesting do:\n");
  (v_i = 0LL);
  {
    LOOP_COUNTER(9);
    do {
      LOOP_COUNTER_CHECK(9);
      {
        concat_assign(v_op, LINE(64,concat3("outer ", toString(v_i), ", ")));
        (v_j = 0LL);
        {
          LOOP_COUNTER(10);
          do {
            LOOP_COUNTER_CHECK(10);
            {
              if (equal(v_j, 3LL)) {
                break;
              }
              concat_assign(v_op, LINE(70,concat3("inner ", toString(v_j), ", ")));
              v_j++;
            }
          } while (less(v_j, 4LL));
        }
        concat_assign(v_op, "\n");
        v_i++;
      }
    } while (less(v_i, 4LL));
  }
  print((v_op + toString("\n")));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001709.php line 23 */
void f_test_for() {
  FUNCTION_INJECTION(test_for);
  int64 v_i = 0;
  String v_op;
  int64 v_j = 0;

  print("\ntesting for:\n");
  {
    LOOP_COUNTER(11);
    for ((v_i = 0LL); less(v_i, 4LL); v_i++) {
      LOOP_COUNTER_CHECK(11);
      {
        concat_assign(v_op, LINE(27,concat3("outer ", toString(v_i), ", ")));
        {
          LOOP_COUNTER(12);
          for ((v_j = 0LL); less(v_j, 4LL); v_j++) {
            LOOP_COUNTER_CHECK(12);
            {
              if (equal(v_j, 3LL)) {
                break;
              }
              concat_assign(v_op, LINE(32,concat3("inner ", toString(v_j), ", ")));
            }
          }
        }
        concat_assign(v_op, "\n");
      }
    }
  }
  print((v_op + toString("\n")));
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001709_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001709.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001709_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("string-port replacements aren't nesting properly\n'\n");
  LINE(80,f_test_foreach());
  LINE(81,f_test_while());
  LINE(82,f_test_for());
  LINE(83,f_test_do());
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
