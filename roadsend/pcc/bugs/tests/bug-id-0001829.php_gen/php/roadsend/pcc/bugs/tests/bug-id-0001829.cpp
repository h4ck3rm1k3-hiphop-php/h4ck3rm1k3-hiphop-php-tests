
#include <php/roadsend/pcc/bugs/tests/bug-id-0001829.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001829_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001829.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001829_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("\r\r\n/** \r\r\n * admin/reports/reports.php - Administrator's Sales Performance Data \r\r\n * \r\r\n * Displays Sales Performance Data.\r\r\n * \r\r\n * $Id: bug-id-0001829.php,v 1.1 2004/06/04 10:42:58 tim Exp $ \r\r\n */ \r\r\n\r\r\n\?>\nasdf\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
