
#include <php/roadsend/pcc/bugs/tests/bug-id-0001144.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001144_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001144.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001144_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_a = "22222222aaaa bbb1111 cccc");
  (v_b = "1234");
  LINE(5,x_var_dump(1, v_a));
  LINE(6,x_var_dump(1, v_b));
  LINE(7,x_var_dump(1, x_strspn(toString(v_a), toString(v_b))));
  LINE(8,x_var_dump(1, x_strspn(toString(v_a), toString(v_b), toInt32(2LL))));
  LINE(9,x_var_dump(1, x_strspn(toString(v_a), toString(v_b), toInt32(2LL), toInt32(3LL))));
  LINE(11,x_var_dump(1, x_strcspn(toString(v_a), toString(v_b))));
  LINE(12,x_var_dump(1, x_strcspn(toString(v_a), toString(v_b), toInt32(9LL))));
  LINE(13,x_var_dump(1, x_strcspn(toString(v_a), toString(v_b), toInt32(9LL), toInt32(6LL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
