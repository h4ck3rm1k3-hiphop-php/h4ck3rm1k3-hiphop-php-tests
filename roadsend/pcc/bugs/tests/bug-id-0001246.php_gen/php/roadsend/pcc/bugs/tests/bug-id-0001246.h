
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001246_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001246_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001246.fw.h>

// Declarations
#include <cls/aclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_modit(Variant v_var);
Variant f_copying();
Variant f_byref();
void f_modarray(Variant v_a);
Variant f_returncopy(Variant v_var);
Variant f_returnref(Variant v_var);
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001246_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_aclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001246_h__
