
#include <php/roadsend/pcc/bugs/tests/bug-id-0001246.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001246.php line 57 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("v", m_v.isReferenced() ? ref(m_v) : m_v));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x3C2F961831E4EF6BLL, v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3C2F961831E4EF6BLL, m_v,
                         v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x3C2F961831E4EF6BLL, m_v,
                      v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x3C2F961831E4EF6BLL, m_v,
                         v, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_v = m_v.isReferenced() ? ref(m_v) : m_v;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_v = "initial";
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001246.php line 6 */
void f_modit(Variant v_var) {
  FUNCTION_INJECTION(modit);
  (v_var = "i have changed the variable");
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001246.php line 94 */
Variant f_copying() {
  FUNCTION_INJECTION(copying);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_foo __attribute__((__unused__)) = g->sv_copying_DupIdfoo;
  bool &inited_sv_foo __attribute__((__unused__)) = g->inited_sv_copying_DupIdfoo;
  if (!inited_sv_foo) {
    (sv_foo = null);
    inited_sv_foo = true;
  }
  sv_foo.append(("asdf"));
  LINE(98,x_var_dump(1, sv_foo));
  return sv_foo;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001246.php line 75 */
Variant f_byref() {
  FUNCTION_INJECTION(byref);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_foo __attribute__((__unused__)) = g->sv_byref_DupIdfoo;
  bool &inited_sv_foo __attribute__((__unused__)) = g->inited_sv_byref_DupIdfoo;
  if (!inited_sv_foo) {
    (sv_foo = null);
    inited_sv_foo = true;
  }
  sv_foo++;
  echo(toString(sv_foo) + toString("\n"));
  return ref(sv_foo);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001246.php line 10 */
void f_modarray(Variant v_a) {
  FUNCTION_INJECTION(modarray);
  LINE(11,x_asort(ref(v_a)));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001246.php line 14 */
Variant f_returncopy(Variant v_var) {
  FUNCTION_INJECTION(returncopy);
  (v_var = "meep");
  return v_var;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001246.php line 19 */
Variant f_returnref(Variant v_var) {
  FUNCTION_INJECTION(returnref);
  return ref(v_var);
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001246_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001246.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001246_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_k __attribute__((__unused__)) = (variables != gVariables) ? variables->get("k") : g->GV(k);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  echo("0001246 warning on mutation of non reference function parameter\n\n");
  (v_a = "test");
  LINE(24,f_modit(v_a));
  LINE(25,x_var_dump(1, v_a));
  (v_a = ScalarArrays::sa_[0]);
  LINE(28,f_modarray(v_a));
  LINE(29,x_var_dump(1, v_a));
  (v_b = ref(v_a));
  (v_a = "hello");
  (v_c = LINE(33,f_returncopy(v_b)));
  LINE(34,x_var_dump(1, v_c));
  (v_x = ref(v_a));
  (v_a = "hi ho");
  (v_w = LINE(38,f_returnref(v_x)));
  LINE(39,x_var_dump(1, v_w));
  (v_d = (v_f = ScalarArrays::sa_[1]));
  LINE(42,x_asort(ref(v_f)));
  LINE(43,x_var_dump(1, v_d));
  LINE(44,x_var_dump(1, v_f));
  (v_a = ScalarArrays::sa_[2]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        echo(LINE(51,concat4(toString(v_k), " => ", toString(v_v), "\n")));
        (v_v = "changeit");
      }
    }
  }
  LINE(55,x_var_dump(1, v_a));
  v_z.append((LINE(61,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create()))));
  v_z.append((((Object)(LINE(62,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create()))))));
  LINE(64,x_var_dump(1, v_z));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_z.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_c = iter6->second();
      {
        (v_c.o_lval("v", 0x3C2F961831E4EF6BLL) = "changed");
      }
    }
  }
  LINE(70,x_var_dump(1, v_z));
  echo("Test return values by reference\n");
  (v_bar = LINE(83,f_byref()));
  v_bar++;
  LINE(85,f_byref());
  (v_bar = ref(LINE(88,f_byref())));
  v_bar++;
  LINE(90,f_byref());
  echo("Test return values getting copied\n");
  (v_bar = LINE(102,f_copying()));
  v_bar.append((22LL));
  LINE(104,f_copying());
  (v_bar = ref(LINE(107,f_copying())));
  v_bar.append((23LL));
  LINE(109,f_copying());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
