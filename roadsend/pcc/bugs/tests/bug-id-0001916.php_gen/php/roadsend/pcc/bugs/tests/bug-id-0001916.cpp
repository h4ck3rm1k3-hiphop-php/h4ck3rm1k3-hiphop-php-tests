
#include <php/roadsend/pcc/bugs/tests/bug-id-0001916.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001916.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("member", m_member));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7D0BEDECC8D925AELL, member, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7D0BEDECC8D925AELL, m_member,
                         member, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7D0BEDECC8D925AELL, m_member,
                      member, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_member = m_member;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_member = "foo";
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001916.php line 5 */
void c_foo::t_func() {
  INSTANCE_METHOD_INJECTION(foo, foo::func);
  if (isset(((Object)(this)))) {
    print(LINE(7,concat3("this is set, mem: ", m_member, "\n")));
  }
  else {
    print(LINE(9,concat3("this not set, mem: ", m_member, "\n")));
  }
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001916.php line 14 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_foo::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_foo::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("member", m_member));
  c_foo::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_foo::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_foo::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_foo::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_foo::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_foo::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::create() {
  init();
  t_bar();
  return this;
}
ObjectData *c_bar::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_bar::dynConstruct(CArrRef params) {
  (t_bar());
}
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  clone->m_member = m_member;
  c_foo::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        return (t_bar(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_foo::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
  c_foo::init();
  m_member = "bar";
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001916.php line 16 */
void c_bar::t_bar() {
  INSTANCE_METHOD_INJECTION(bar, bar::bar);
  bool oldInCtor = gasInCtor(true);
  p_foo v_foo;

  print("calling foo statically:\n");
  LINE(18,c_foo::t_func());
  print("calling foo dynamically:\n");
  ((Object)((v_foo = ((Object)(LINE(20,p_foo(p_foo(NEWOBJ(c_foo)())->create())))))));
  LINE(21,v_foo->t_func());
  print("done\n");
  gasInCtor(oldInCtor);
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001916_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001916.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001916_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_bar = ((Object)(LINE(26,p_bar(p_bar(NEWOBJ(c_bar)())->create())))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
