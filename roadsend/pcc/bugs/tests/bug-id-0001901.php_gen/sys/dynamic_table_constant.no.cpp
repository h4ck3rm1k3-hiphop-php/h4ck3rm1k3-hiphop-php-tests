
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_SM_TAG_CLASS;
extern const StaticString k_SM_AREAID_KEY;
extern const StaticString k_SM_TAG_PREGEXP;
extern const StaticString k_SM_TAG_ATTR_PREGEXP;
extern const StaticString k_SM_TAG_IDENTIFIER;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 15) {
    case 0:
      HASH_RETURN(0x66FCA4F00EA5DE00LL, k_SM_AREAID_KEY, SM_AREAID_KEY);
      break;
    case 3:
      HASH_RETURN(0x4B8D47FD3B48CCD3LL, k_SM_TAG_PREGEXP, SM_TAG_PREGEXP);
      break;
    case 5:
      HASH_RETURN(0x33C34EC066068705LL, k_SM_TAG_CLASS, SM_TAG_CLASS);
      break;
    case 9:
      HASH_RETURN(0x57DCC41CE859E629LL, k_SM_TAG_ATTR_PREGEXP, SM_TAG_ATTR_PREGEXP);
      break;
    case 11:
      HASH_RETURN(0x58F165E6668406CBLL, k_SM_TAG_IDENTIFIER, SM_TAG_IDENTIFIER);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
