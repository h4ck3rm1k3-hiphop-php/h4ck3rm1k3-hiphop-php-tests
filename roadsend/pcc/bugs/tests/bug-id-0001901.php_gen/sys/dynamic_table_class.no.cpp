
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_sm_layouttemplate(CArrRef params, bool init = true);
Variant cw_sm_layouttemplate$os_get(const char *s);
Variant &cw_sm_layouttemplate$os_lval(const char *s);
Variant cw_sm_layouttemplate$os_constant(const char *s);
Variant cw_sm_layouttemplate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sm_smtag_area(CArrRef params, bool init = true);
Variant cw_sm_smtag_area$os_get(const char *s);
Variant &cw_sm_smtag_area$os_lval(const char *s);
Variant cw_sm_smtag_area$os_constant(const char *s);
Variant cw_sm_smtag_area$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sm_sitemanagerroot(CArrRef params, bool init = true);
Variant cw_sm_sitemanagerroot$os_get(const char *s);
Variant &cw_sm_sitemanagerroot$os_lval(const char *s);
Variant cw_sm_sitemanagerroot$os_constant(const char *s);
Variant cw_sm_sitemanagerroot$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sm_smtag(CArrRef params, bool init = true);
Variant cw_sm_smtag$os_get(const char *s);
Variant &cw_sm_smtag$os_lval(const char *s);
Variant cw_sm_smtag$os_constant(const char *s);
Variant cw_sm_smtag$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_CREATE_OBJECT(0x7A2C3CF12843F17ALL, sm_layouttemplate);
      break;
    case 4:
      HASH_CREATE_OBJECT(0x50C52A2D252F8F94LL, sm_smtag_area);
      break;
    case 5:
      HASH_CREATE_OBJECT(0x15D046C8548DE645LL, sm_sitemanagerroot);
      HASH_CREATE_OBJECT(0x0F1741D4FFDB8F15LL, sm_smtag);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_INVOKE_STATIC_METHOD(0x7A2C3CF12843F17ALL, sm_layouttemplate);
      break;
    case 4:
      HASH_INVOKE_STATIC_METHOD(0x50C52A2D252F8F94LL, sm_smtag_area);
      break;
    case 5:
      HASH_INVOKE_STATIC_METHOD(0x15D046C8548DE645LL, sm_sitemanagerroot);
      HASH_INVOKE_STATIC_METHOD(0x0F1741D4FFDB8F15LL, sm_smtag);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GET_STATIC_PROPERTY(0x7A2C3CF12843F17ALL, sm_layouttemplate);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY(0x50C52A2D252F8F94LL, sm_smtag_area);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY(0x15D046C8548DE645LL, sm_sitemanagerroot);
      HASH_GET_STATIC_PROPERTY(0x0F1741D4FFDB8F15LL, sm_smtag);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GET_STATIC_PROPERTY_LV(0x7A2C3CF12843F17ALL, sm_layouttemplate);
      break;
    case 4:
      HASH_GET_STATIC_PROPERTY_LV(0x50C52A2D252F8F94LL, sm_smtag_area);
      break;
    case 5:
      HASH_GET_STATIC_PROPERTY_LV(0x15D046C8548DE645LL, sm_sitemanagerroot);
      HASH_GET_STATIC_PROPERTY_LV(0x0F1741D4FFDB8F15LL, sm_smtag);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GET_CLASS_CONSTANT(0x7A2C3CF12843F17ALL, sm_layouttemplate);
      break;
    case 4:
      HASH_GET_CLASS_CONSTANT(0x50C52A2D252F8F94LL, sm_smtag_area);
      break;
    case 5:
      HASH_GET_CLASS_CONSTANT(0x15D046C8548DE645LL, sm_sitemanagerroot);
      HASH_GET_CLASS_CONSTANT(0x0F1741D4FFDB8F15LL, sm_smtag);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
