
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001901_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001901_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001901.fw.h>

// Declarations
#include <cls/sm_layouttemplate.h>
#include <cls/sm_smtag_area.h>
#include <cls/sm_sitemanagerroot.h>
#include <cls/sm_smtag.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_sm_loadtag(CVarRef v_tag);
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001901_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_layouttemplate(CArrRef params, bool init = true);
Object co_sm_smtag_area(CArrRef params, bool init = true);
Object co_sm_sitemanagerroot(CArrRef params, bool init = true);
Object co_sm_smtag(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001901_h__
