
#include <php/roadsend/pcc/bugs/tests/bug-id-0001901.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_SM_TAG_CLASS = "SM_smTag_";
const StaticString k_SM_AREAID_KEY = "_SMarea-";
const StaticString k_SM_TAG_PREGEXP = "/<\\s*SM\\s(.+)\\s*>/Ui";
const StaticString k_SM_TAG_ATTR_PREGEXP = "/(\\w+)\\s*=\\s*[\\\"'](.+)[\\\"']/SUi";
const StaticString k_SM_TAG_IDENTIFIER = "SM";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 19 */
Variant c_sm_layouttemplate::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_layouttemplate::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_layouttemplate::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("smTagList", m_smTagList));
  props.push_back(NEW(ArrayElement)("htmlTemplate", m_htmlTemplate.isReferenced() ? ref(m_htmlTemplate) : m_htmlTemplate));
  props.push_back(NEW(ArrayElement)("htmlParsedOutput", m_htmlParsedOutput));
  props.push_back(NEW(ArrayElement)("htmlOutput", m_htmlOutput));
  props.push_back(NEW(ArrayElement)("tagPriorityList", m_tagPriorityList));
  props.push_back(NEW(ArrayElement)("parsed", m_parsed));
  c_ObjectData::o_get(props);
}
bool c_sm_layouttemplate::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_EXISTS_STRING(0x6F2881E7CC9CE553LL, tagPriorityList, 15);
      break;
    case 7:
      HASH_EXISTS_STRING(0x6678DFEE0B590A77LL, htmlParsedOutput, 16);
      HASH_EXISTS_STRING(0x1F704D0107B79A97LL, parsed, 6);
      break;
    case 8:
      HASH_EXISTS_STRING(0x218513B1A20BE688LL, smTagList, 9);
      HASH_EXISTS_STRING(0x4DEFFE6ACDCC1138LL, htmlTemplate, 12);
      break;
    case 10:
      HASH_EXISTS_STRING(0x3AD1C1685717846ALL, htmlOutput, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_layouttemplate::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_RETURN_STRING(0x6F2881E7CC9CE553LL, m_tagPriorityList,
                         tagPriorityList, 15);
      break;
    case 7:
      HASH_RETURN_STRING(0x6678DFEE0B590A77LL, m_htmlParsedOutput,
                         htmlParsedOutput, 16);
      HASH_RETURN_STRING(0x1F704D0107B79A97LL, m_parsed,
                         parsed, 6);
      break;
    case 8:
      HASH_RETURN_STRING(0x218513B1A20BE688LL, m_smTagList,
                         smTagList, 9);
      HASH_RETURN_STRING(0x4DEFFE6ACDCC1138LL, m_htmlTemplate,
                         htmlTemplate, 12);
      break;
    case 10:
      HASH_RETURN_STRING(0x3AD1C1685717846ALL, m_htmlOutput,
                         htmlOutput, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_layouttemplate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 3:
      HASH_SET_STRING(0x6F2881E7CC9CE553LL, m_tagPriorityList,
                      tagPriorityList, 15);
      break;
    case 7:
      HASH_SET_STRING(0x6678DFEE0B590A77LL, m_htmlParsedOutput,
                      htmlParsedOutput, 16);
      HASH_SET_STRING(0x1F704D0107B79A97LL, m_parsed,
                      parsed, 6);
      break;
    case 8:
      HASH_SET_STRING(0x218513B1A20BE688LL, m_smTagList,
                      smTagList, 9);
      HASH_SET_STRING(0x4DEFFE6ACDCC1138LL, m_htmlTemplate,
                      htmlTemplate, 12);
      break;
    case 10:
      HASH_SET_STRING(0x3AD1C1685717846ALL, m_htmlOutput,
                      htmlOutput, 10);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_layouttemplate::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4DEFFE6ACDCC1138LL, m_htmlTemplate,
                         htmlTemplate, 12);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_layouttemplate::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_layouttemplate)
ObjectData *c_sm_layouttemplate::cloneImpl() {
  c_sm_layouttemplate *obj = NEW(c_sm_layouttemplate)();
  cloneSet(obj);
  return obj;
}
void c_sm_layouttemplate::cloneSet(c_sm_layouttemplate *clone) {
  clone->m_smTagList = m_smTagList;
  clone->m_htmlTemplate = m_htmlTemplate.isReferenced() ? ref(m_htmlTemplate) : m_htmlTemplate;
  clone->m_htmlParsedOutput = m_htmlParsedOutput;
  clone->m_htmlOutput = m_htmlOutput;
  clone->m_tagPriorityList = m_tagPriorityList;
  clone->m_parsed = m_parsed;
  ObjectData::cloneSet(clone);
}
Variant c_sm_layouttemplate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x59666246664E2C44LL, settemplatedata) {
        int count = params.size();
        if (count <= 1) return (t_settemplatedata(params.rvalAt(0)));
        return (t_settemplatedata(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 3:
      HASH_GUARD(0x07C56137CBB068FFLL, addtext) {
        return (t_addtext(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_layouttemplate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x59666246664E2C44LL, settemplatedata) {
        if (count <= 1) return (t_settemplatedata(a0));
        return (t_settemplatedata(a0, a1));
      }
      break;
    case 3:
      HASH_GUARD(0x07C56137CBB068FFLL, addtext) {
        return (t_addtext(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_layouttemplate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_layouttemplate$os_get(const char *s) {
  return c_sm_layouttemplate::os_get(s, -1);
}
Variant &cw_sm_layouttemplate$os_lval(const char *s) {
  return c_sm_layouttemplate::os_lval(s, -1);
}
Variant cw_sm_layouttemplate$os_constant(const char *s) {
  return c_sm_layouttemplate::os_constant(s);
}
Variant cw_sm_layouttemplate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_layouttemplate::os_invoke(c, s, params, -1, fatal);
}
void c_sm_layouttemplate::init() {
  m_smTagList = ScalarArrays::sa_[0];
  m_htmlTemplate = "";
  m_htmlParsedOutput = "";
  m_htmlOutput = "";
  m_tagPriorityList = ScalarArrays::sa_[0];
  m_parsed = false;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 34 */
bool c_sm_layouttemplate::t_settemplatedata(CVarRef v_templateData, CVarRef v_parse //  = true
) {
  INSTANCE_METHOD_INJECTION(SM_layoutTemplate, SM_layoutTemplate::setTemplateData);
  (m_htmlTemplate = LINE(35,x_split("\n", toString(v_templateData))));
  return LINE(36,t__php_parsetemplate());
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 40 */
bool c_sm_layouttemplate::t__php_parsetemplate() {
  INSTANCE_METHOD_INJECTION(SM_layoutTemplate, SM_layoutTemplate::_php_parseTemplate);
  p_sm_smtag_area v_tagClass;

  if (equal(LINE(42,x_sizeof(m_htmlTemplate)), 0LL)) {
    LINE(43,o_root_invoke_few_args("debugLog", 0x603BE0E0675207ABLL, 1, "template data was blank, ignoring"));
    return false;
  }
  (m_htmlParsedOutput = "");
  ((Object)((v_tagClass = LINE(51,p_sm_smtag_area(p_sm_smtag_area(NEWOBJ(c_sm_smtag_area)())->create())))));
  LINE(52,v_tagClass->t_setparent(ref(this)));
  LINE(53,v_tagClass->t_settagdata("<sm type=\"area\" name=\"areaOne\">", ScalarArrays::sa_[1]));
  m_smTagList.set("areaOne", (((Object)(v_tagClass))), 0x12E01BAFEA5761A8LL);
  lval(m_tagPriorityList.lvalAt(v_tagClass->m_thinkPriority)).append(("areaOne"));
  (m_parsed = true);
  return true;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 64 */
void c_sm_layouttemplate::t_addtext(Variant v_output, CVarRef v_areaName) {
  INSTANCE_METHOD_INJECTION(SM_layoutTemplate, SM_layoutTemplate::addText);
  LINE(66,m_smTagList.rvalAt(v_areaName).o_invoke_few_args("addItem", 0x70854AAB688D1D60LL, 1, v_output));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 146 */
Variant c_sm_smtag_area::os_get(const char *s, int64 hash) {
  return c_sm_smtag::os_get(s, hash);
}
Variant &c_sm_smtag_area::os_lval(const char *s, int64 hash) {
  return c_sm_smtag::os_lval(s, hash);
}
void c_sm_smtag_area::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("areaName", m_areaName));
  props.push_back(NEW(ArrayElement)("itemList", m_itemList));
  c_sm_smtag::o_get(props);
}
bool c_sm_smtag_area::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x7DDB95372031CCF8LL, areaName, 8);
      HASH_EXISTS_STRING(0x0B544F21590D2A74LL, itemList, 8);
      break;
    default:
      break;
  }
  return c_sm_smtag::o_exists(s, hash);
}
Variant c_sm_smtag_area::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x7DDB95372031CCF8LL, m_areaName,
                         areaName, 8);
      HASH_RETURN_STRING(0x0B544F21590D2A74LL, m_itemList,
                         itemList, 8);
      break;
    default:
      break;
  }
  return c_sm_smtag::o_get(s, hash);
}
Variant c_sm_smtag_area::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x7DDB95372031CCF8LL, m_areaName,
                      areaName, 8);
      HASH_SET_STRING(0x0B544F21590D2A74LL, m_itemList,
                      itemList, 8);
      break;
    default:
      break;
  }
  return c_sm_smtag::o_set(s, hash, v, forInit);
}
Variant &c_sm_smtag_area::o_lval(CStrRef s, int64 hash) {
  return c_sm_smtag::o_lval(s, hash);
}
Variant c_sm_smtag_area::os_constant(const char *s) {
  return c_sm_smtag::os_constant(s);
}
IMPLEMENT_CLASS(sm_smtag_area)
ObjectData *c_sm_smtag_area::cloneImpl() {
  c_sm_smtag_area *obj = NEW(c_sm_smtag_area)();
  cloneSet(obj);
  return obj;
}
void c_sm_smtag_area::cloneSet(c_sm_smtag_area *clone) {
  clone->m_areaName = m_areaName;
  clone->m_itemList = m_itemList;
  c_sm_smtag::cloneSet(clone);
}
Variant c_sm_smtag_area::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x70854AAB688D1D60LL, additem) {
        return (t_additem(ref(const_cast<Array&>(params).lvalAt(0))), null);
      }
      break;
    default:
      break;
  }
  return c_sm_smtag::o_invoke(s, params, hash, fatal);
}
Variant c_sm_smtag_area::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x70854AAB688D1D60LL, additem) {
        return (t_additem(ref(a0)), null);
      }
      break;
    default:
      break;
  }
  return c_sm_smtag::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_smtag_area::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_sm_smtag::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_smtag_area$os_get(const char *s) {
  return c_sm_smtag_area::os_get(s, -1);
}
Variant &cw_sm_smtag_area$os_lval(const char *s) {
  return c_sm_smtag_area::os_lval(s, -1);
}
Variant cw_sm_smtag_area$os_constant(const char *s) {
  return c_sm_smtag_area::os_constant(s);
}
Variant cw_sm_smtag_area$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_smtag_area::os_invoke(c, s, params, -1, fatal);
}
void c_sm_smtag_area::init() {
  c_sm_smtag::init();
  m_areaName = "";
  m_itemList = ScalarArrays::sa_[0];
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 152 */
void c_sm_smtag_area::t_additem(Variant v_data) {
  INSTANCE_METHOD_INJECTION(SM_smTag_AREA, SM_smTag_AREA::addItem);
  m_itemList.append((ref(v_data)));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 77 */
Variant c_sm_sitemanagerroot::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_sitemanagerroot::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_sitemanagerroot::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("templateCache", m_templateCache));
  c_ObjectData::o_get(props);
}
bool c_sm_sitemanagerroot::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x73A56FC71C27A2BDLL, templateCache, 13);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_sitemanagerroot::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x73A56FC71C27A2BDLL, m_templateCache,
                         templateCache, 13);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_sitemanagerroot::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x73A56FC71C27A2BDLL, m_templateCache,
                      templateCache, 13);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_sitemanagerroot::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_sitemanagerroot::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_sitemanagerroot)
ObjectData *c_sm_sitemanagerroot::cloneImpl() {
  c_sm_sitemanagerroot *obj = NEW(c_sm_sitemanagerroot)();
  cloneSet(obj);
  return obj;
}
void c_sm_sitemanagerroot::cloneSet(c_sm_sitemanagerroot *clone) {
  clone->m_templateCache = m_templateCache;
  ObjectData::cloneSet(clone);
}
Variant c_sm_sitemanagerroot::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x50698758D3B070F6LL, loadtemplate) {
        int count = params.size();
        if (count <= 1) return (t_loadtemplate(params.rvalAt(0)));
        return (t_loadtemplate(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_sitemanagerroot::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x50698758D3B070F6LL, loadtemplate) {
        if (count <= 1) return (t_loadtemplate(a0));
        return (t_loadtemplate(a0, a1));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_sitemanagerroot::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_sitemanagerroot$os_get(const char *s) {
  return c_sm_sitemanagerroot::os_get(s, -1);
}
Variant &cw_sm_sitemanagerroot$os_lval(const char *s) {
  return c_sm_sitemanagerroot::os_lval(s, -1);
}
Variant cw_sm_sitemanagerroot$os_constant(const char *s) {
  return c_sm_sitemanagerroot::os_constant(s);
}
Variant cw_sm_sitemanagerroot$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_sitemanagerroot::os_invoke(c, s, params, -1, fatal);
}
void c_sm_sitemanagerroot::init() {
  m_templateCache = ScalarArrays::sa_[0];
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 81 */
Variant c_sm_sitemanagerroot::t_loadtemplate(CVarRef v_fName, CVarRef v_fatalFNF //  = true
) {
  INSTANCE_METHOD_INJECTION(SM_siteManagerRoot, SM_siteManagerRoot::loadTemplate);
  Variant v_tptData;
  Variant v_newTemplate;

  (v_tptData = "<b>Sample Template</b><br>\n<sm type=\"area\" name=\"areaOne\">\n<br>\nTemplate Test Complete\n");
  if (isset(m_templateCache, v_fName)) {
    echo("CACHED:\n");
    return m_templateCache.rvalAt(v_fName);
  }
  echo("NOT CACHED\n");
  (v_newTemplate = LINE(97,p_sm_layouttemplate(p_sm_layouttemplate(NEWOBJ(c_sm_layouttemplate)())->create())));
  LINE(98,v_newTemplate.o_invoke_few_args("setTemplateData", 0x59666246664E2C44LL, 1, v_tptData));
  m_templateCache.set(v_fName, (ref(v_newTemplate)));
  return v_newTemplate;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 110 */
Variant c_sm_smtag::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_smtag::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_smtag::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("tagLine", m_tagLine));
  props.push_back(NEW(ArrayElement)("attributes", m_attributes));
  props.push_back(NEW(ArrayElement)("tagOutput", m_tagOutput.isReferenced() ? ref(m_tagOutput) : m_tagOutput));
  props.push_back(NEW(ArrayElement)("thinkPriority", m_thinkPriority));
  props.push_back(NEW(ArrayElement)("parentTemplate", m_parentTemplate.isReferenced() ? ref(m_parentTemplate) : m_parentTemplate));
  c_ObjectData::o_get(props);
}
bool c_sm_smtag::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_EXISTS_STRING(0x2B5EFCBB0DE2F071LL, tagOutput, 9);
      break;
    case 3:
      HASH_EXISTS_STRING(0x3E5975BA0FC37E03LL, attributes, 10);
      break;
    case 5:
      HASH_EXISTS_STRING(0x0B0C0BD217937265LL, tagLine, 7);
      break;
    case 10:
      HASH_EXISTS_STRING(0x68F6F7210064A48ALL, thinkPriority, 13);
      break;
    case 14:
      HASH_EXISTS_STRING(0x065DC742EDD268CELL, parentTemplate, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_smtag::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x2B5EFCBB0DE2F071LL, m_tagOutput,
                         tagOutput, 9);
      break;
    case 3:
      HASH_RETURN_STRING(0x3E5975BA0FC37E03LL, m_attributes,
                         attributes, 10);
      break;
    case 5:
      HASH_RETURN_STRING(0x0B0C0BD217937265LL, m_tagLine,
                         tagLine, 7);
      break;
    case 10:
      HASH_RETURN_STRING(0x68F6F7210064A48ALL, m_thinkPriority,
                         thinkPriority, 13);
      break;
    case 14:
      HASH_RETURN_STRING(0x065DC742EDD268CELL, m_parentTemplate,
                         parentTemplate, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_smtag::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_SET_STRING(0x2B5EFCBB0DE2F071LL, m_tagOutput,
                      tagOutput, 9);
      break;
    case 3:
      HASH_SET_STRING(0x3E5975BA0FC37E03LL, m_attributes,
                      attributes, 10);
      break;
    case 5:
      HASH_SET_STRING(0x0B0C0BD217937265LL, m_tagLine,
                      tagLine, 7);
      break;
    case 10:
      HASH_SET_STRING(0x68F6F7210064A48ALL, m_thinkPriority,
                      thinkPriority, 13);
      break;
    case 14:
      HASH_SET_STRING(0x065DC742EDD268CELL, m_parentTemplate,
                      parentTemplate, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_smtag::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x2B5EFCBB0DE2F071LL, m_tagOutput,
                         tagOutput, 9);
      break;
    case 2:
      HASH_RETURN_STRING(0x065DC742EDD268CELL, m_parentTemplate,
                         parentTemplate, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_smtag::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_smtag)
ObjectData *c_sm_smtag::cloneImpl() {
  c_sm_smtag *obj = NEW(c_sm_smtag)();
  cloneSet(obj);
  return obj;
}
void c_sm_smtag::cloneSet(c_sm_smtag *clone) {
  clone->m_tagLine = m_tagLine;
  clone->m_attributes = m_attributes;
  clone->m_tagOutput = m_tagOutput.isReferenced() ? ref(m_tagOutput) : m_tagOutput;
  clone->m_thinkPriority = m_thinkPriority;
  clone->m_parentTemplate = m_parentTemplate.isReferenced() ? ref(m_parentTemplate) : m_parentTemplate;
  ObjectData::cloneSet(clone);
}
Variant c_sm_smtag::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_smtag::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_smtag::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_smtag$os_get(const char *s) {
  return c_sm_smtag::os_get(s, -1);
}
Variant &cw_sm_smtag$os_lval(const char *s) {
  return c_sm_smtag::os_lval(s, -1);
}
Variant cw_sm_smtag$os_constant(const char *s) {
  return c_sm_smtag::os_constant(s);
}
Variant cw_sm_smtag$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_smtag::os_invoke(c, s, params, -1, fatal);
}
void c_sm_smtag::init() {
  m_tagLine = "";
  m_attributes = ScalarArrays::sa_[0];
  m_tagOutput = "";
  m_thinkPriority = 0LL;
  m_parentTemplate = null;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 122 */
void c_sm_smtag::t_setparent(Variant v_template) {
  INSTANCE_METHOD_INJECTION(SM_smTag, SM_smTag::setParent);
  (m_parentTemplate = ref(v_template));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 128 */
void c_sm_smtag::t_settagdata(CStrRef v_tagLine, CArrRef v_attrs) {
  INSTANCE_METHOD_INJECTION(SM_smTag, SM_smTag::setTagData);
  (m_tagLine = v_tagLine);
  (m_attributes = v_attrs);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 137 */
void c_sm_smtag::t_say(CVarRef v_output) {
  INSTANCE_METHOD_INJECTION(SM_smTag, SM_smTag::say);
  concat_assign(m_tagOutput, toString(v_output));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 160 */
bool f_sm_loadtag(CVarRef v_tag) {
  FUNCTION_INJECTION(SM_loadTag);
  return true;
} /* function */
Object co_sm_layouttemplate(CArrRef params, bool init /* = true */) {
  return Object(p_sm_layouttemplate(NEW(c_sm_layouttemplate)())->dynCreate(params, init));
}
Object co_sm_smtag_area(CArrRef params, bool init /* = true */) {
  return Object(p_sm_smtag_area(NEW(c_sm_smtag_area)())->dynCreate(params, init));
}
Object co_sm_sitemanagerroot(CArrRef params, bool init /* = true */) {
  return Object(p_sm_sitemanagerroot(NEW(c_sm_sitemanagerroot)())->dynCreate(params, init));
}
Object co_sm_smtag(CArrRef params, bool init /* = true */) {
  return Object(p_sm_smtag(NEW(c_sm_smtag)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001901_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001901.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001901_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_root __attribute__((__unused__)) = (variables != gVariables) ? variables->get("root") : g->GV(root);
  Variant &v_tpt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tpt") : g->GV(tpt);

  echo("methods can modify their $this, so it must have any pending copies \nforced on it for the method call.  This tests for that, in an \nadmittedly baroque way. the line         \n\n  $this->smTagList[$areaName]->addItem($output);\n\nis where the copy should be forced\n\n\n");
  ;
  ;
  ;
  ;
  ;
  (v_root = LINE(164,p_sm_sitemanagerroot(p_sm_sitemanagerroot(NEWOBJ(c_sm_sitemanagerroot)())->create())));
  (v_tpt = ref(LINE(165,v_root.o_invoke_few_args("loadTemplate", 0x50698758D3B070F6LL, 1, "sample.tpt"))));
  LINE(166,x_var_dump(1, v_tpt));
  LINE(167,v_tpt.o_invoke_few_args("addText", 0x07C56137CBB068FFLL, 2, "this is from first call", "areaOne"));
  (v_tpt = ref(LINE(170,v_root.o_invoke_few_args("loadTemplate", 0x50698758D3B070F6LL, 1, "sample.tpt"))));
  LINE(171,x_var_dump(1, v_tpt));
  LINE(172,v_tpt.o_invoke_few_args("addText", 0x07C56137CBB068FFLL, 2, "this is from second call", "areaOne"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
