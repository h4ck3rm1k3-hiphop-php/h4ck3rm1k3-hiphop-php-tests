
#ifndef __GENERATED_cls_sm_smtag_area_h__
#define __GENERATED_cls_sm_smtag_area_h__

#include <cls/sm_smtag.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 146 */
class c_sm_smtag_area : virtual public c_sm_smtag {
  BEGIN_CLASS_MAP(sm_smtag_area)
    PARENT_CLASS(sm_smtag)
  END_CLASS_MAP(sm_smtag_area)
  DECLARE_CLASS(sm_smtag_area, SM_smTag_AREA, sm_smtag)
  void init();
  public: String m_areaName;
  public: Array m_itemList;
  public: void t_additem(Variant v_data);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_area_h__
