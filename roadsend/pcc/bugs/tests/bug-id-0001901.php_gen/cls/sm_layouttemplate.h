
#ifndef __GENERATED_cls_sm_layouttemplate_h__
#define __GENERATED_cls_sm_layouttemplate_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 19 */
class c_sm_layouttemplate : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_layouttemplate)
  END_CLASS_MAP(sm_layouttemplate)
  DECLARE_CLASS(sm_layouttemplate, SM_layoutTemplate, ObjectData)
  void init();
  public: Array m_smTagList;
  public: Variant m_htmlTemplate;
  public: String m_htmlParsedOutput;
  public: String m_htmlOutput;
  public: Array m_tagPriorityList;
  public: bool m_parsed;
  public: bool t_settemplatedata(CVarRef v_templateData, CVarRef v_parse = true);
  public: bool t__php_parsetemplate();
  public: void t_addtext(Variant v_output, CVarRef v_areaName);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_layouttemplate_h__
