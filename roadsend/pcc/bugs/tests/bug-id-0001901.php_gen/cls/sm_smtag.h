
#ifndef __GENERATED_cls_sm_smtag_h__
#define __GENERATED_cls_sm_smtag_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001901.php line 110 */
class c_sm_smtag : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag)
  END_CLASS_MAP(sm_smtag)
  DECLARE_CLASS(sm_smtag, SM_smTag, ObjectData)
  void init();
  public: String m_tagLine;
  public: Array m_attributes;
  public: Variant m_tagOutput;
  public: int64 m_thinkPriority;
  public: Variant m_parentTemplate;
  public: void t_setparent(Variant v_template);
  public: void t_settagdata(CStrRef v_tagLine, CArrRef v_attrs);
  public: void t_say(CVarRef v_output);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_h__
