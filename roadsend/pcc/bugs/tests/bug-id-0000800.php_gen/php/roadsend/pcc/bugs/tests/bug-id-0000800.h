
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000800_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000800_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0000800.fw.h>

// Declarations
#include <cls/testclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000800_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_testclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000800_h__
