
#ifndef __GENERATED_cls_testclass_h__
#define __GENERATED_cls_testclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0000800.php line 5 */
class c_testclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(testclass)
  END_CLASS_MAP(testclass)
  DECLARE_CLASS(testclass, testClass, ObjectData)
  void init();
  public: String m_var;
  public: void t_testclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testclass_h__
