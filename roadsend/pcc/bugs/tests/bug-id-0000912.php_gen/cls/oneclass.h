
#ifndef __GENERATED_cls_oneclass_h__
#define __GENERATED_cls_oneclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0000912.php line 4 */
class c_oneclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(oneclass)
  END_CLASS_MAP(oneclass)
  DECLARE_CLASS(oneclass, oneclass, ObjectData)
  void init();
  public: void t_a(CVarRef v_b);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_oneclass_h__
