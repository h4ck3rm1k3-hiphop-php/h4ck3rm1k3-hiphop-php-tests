
#ifndef __GENERATED_cls_bclass_h__
#define __GENERATED_cls_bclass_h__

#include <cls/aclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001057.php line 8 */
class c_bclass : virtual public c_aclass {
  BEGIN_CLASS_MAP(bclass)
    PARENT_CLASS(aclass)
  END_CLASS_MAP(bclass)
  DECLARE_CLASS(bclass, bclass, aclass)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bclass_h__
