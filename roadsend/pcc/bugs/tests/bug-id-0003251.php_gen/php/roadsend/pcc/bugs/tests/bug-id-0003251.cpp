
#include <php/roadsend/pcc/bugs/tests/bug-id-0003251.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003251_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0003251.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0003251_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = "some val");
  LINE(3,g->declareConstant("DEFFOO", g->k_DEFFOO, v_foo));
  echo(LINE(4,concat5("$foo is ", toString(v_foo), " and DEFFOO is ", toString(get_global_variables()->k_DEFFOO), "\n")));
  unset(v_foo);
  echo(LINE(6,concat5("$foo is ", toString(v_foo), " and DEFFOO is ", toString(get_global_variables()->k_DEFFOO), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
