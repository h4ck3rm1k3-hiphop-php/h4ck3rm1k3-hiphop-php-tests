
#include <php/roadsend/pcc/bugs/tests/bug-id-0003213.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003213_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0003213.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0003213_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_day_index __attribute__((__unused__)) = (variables != gVariables) ? variables->get("day_index") : g->GV(day_index);

  {
    LOOP_COUNTER(1);
    for ((v_day_index = 0LL); not_more(v_day_index, 6LL); v_day_index++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(v_day_index));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
