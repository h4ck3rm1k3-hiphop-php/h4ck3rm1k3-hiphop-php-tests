
#include <php/roadsend/pcc/bugs/tests/bug-id-0000692.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_SF_LAYOUT_UNDEFINED2 = 200LL;
const int64 k_SF_LAYOUT_UNDEFINED = 100LL;

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000692.php line 29 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("sample", m_sample));
  props.push_back(NEW(ArrayElement)("sample2", m_sample2));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x1366B2D64AB116F6LL, sample2, 7);
      break;
    case 3:
      HASH_EXISTS_STRING(0x094C6D01C7E6AAE7LL, sample, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x1366B2D64AB116F6LL, m_sample2,
                         sample2, 7);
      break;
    case 3:
      HASH_RETURN_STRING(0x094C6D01C7E6AAE7LL, m_sample,
                         sample, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x1366B2D64AB116F6LL, m_sample2,
                      sample2, 7);
      break;
    case 3:
      HASH_SET_STRING(0x094C6D01C7E6AAE7LL, m_sample,
                      sample, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_sample = m_sample;
  clone->m_sample2 = m_sample2;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_sample = 100LL /* SF_LAYOUT_UNDEFINED */;
  m_sample2 = 200LL /* SF_LAYOUT_UNDEFINED2 */;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000692.php line 12 */
void f_addtext(CStrRef v_text, int64 v_lay //  = 100LL /* SF_LAYOUT_UNDEFINED */
) {
  FUNCTION_INJECTION(addText);
  echo((LINE(14,concat4(v_text, ", ", toString(v_lay), "\n"))));
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000692_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000692.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000692_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_afoo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("afoo") : g->GV(afoo);

  echo("constant as default argument for function parameter\n\nthe following breaks in the parser:\n\n\n");
  echo("100\n");
  ;
  echo("100\n");
  LINE(17,f_addtext("foo"));
  echo("\nthis is also a problem for assigning a constant to a class variable in a class definition, ie:\n\n");
  (v_afoo = ((Object)(LINE(35,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  echo(concat(toString(v_afoo.o_get("sample", 0x094C6D01C7E6AAE7LL)), "\n"));
  echo(concat(toString(v_afoo.o_get("sample2", 0x1366B2D64AB116F6LL)), "\n"));
  echo("200\n");
  ;
  echo("200\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
