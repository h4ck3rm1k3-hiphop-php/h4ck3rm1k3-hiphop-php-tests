
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_SF_LAYOUT_UNDEFINED2;
extern const int64 k_SF_LAYOUT_UNDEFINED;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x552BEBB52614BBF4LL, k_SF_LAYOUT_UNDEFINED, SF_LAYOUT_UNDEFINED);
      break;
    case 2:
      HASH_RETURN(0x29544B2AB86997AELL, k_SF_LAYOUT_UNDEFINED2, SF_LAYOUT_UNDEFINED2);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
