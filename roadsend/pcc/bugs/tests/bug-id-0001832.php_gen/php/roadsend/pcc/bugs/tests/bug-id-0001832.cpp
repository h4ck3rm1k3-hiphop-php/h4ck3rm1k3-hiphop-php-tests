
#include <php/roadsend/pcc/bugs/tests/bug-id-0001832.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001832_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001832.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001832_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  echo("wierd problem with unset\n\nWhen unset is called as the only instruction in a foreach loop \nand curly brackets aren't used, and this foreach loop is the only \nargument inside an if/else block where curly brackets aren't used, \npcc complains about the else.\n\nexample:\n\n\n");
  {
    LOOP_COUNTER(1);
    Variant map2 = ScalarArrays::sa_[0];
    for (ArrayIterPtr iter3 = map2.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_bar = iter3->second();
      unset(v_bar);
    }
  }
  echo("\n\nit doesn't seam to be a problem under any other combination of curly brackets or functions.");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
