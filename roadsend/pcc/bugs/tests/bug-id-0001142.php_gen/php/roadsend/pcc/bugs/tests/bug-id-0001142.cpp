
#include <php/roadsend/pcc/bugs/tests/bug-id-0001142.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001142_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001142.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001142_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);

  (v_s = LINE(3,(assignCallTemp(eo_1, x_chr(0LL)),concat3("alabala nica", eo_1, "turska panica"))));
  LINE(4,x_var_dump(1, x_strstr(toString(v_s), "nic")));
  LINE(5,x_var_dump(1, x_strrchr(toString(v_s), " ")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
