
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001142_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_INCLUDE(0x0EC10EBE68A9F080LL, "roadsend/pcc/bugs/tests/bug-id-0001142.php", php$roadsend$pcc$bugs$tests$bug_id_0001142_php);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$roadsend$pcc$bugs$tests$bug_id_0001142_php(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
