
#include <php/roadsend/pcc/bugs/tests/bug-id-0001052.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001052_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001052.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001052_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo(LINE(4,concat3("1 file is ", get_source_filename("roadsend/pcc/bugs/tests/bug-id-0001052.php"), "\n")));
  LINE(5,include("1052-2.inc", false, variables, "roadsend/pcc/bugs/tests/"));
  echo(LINE(6,concat3("1 file is ", get_source_filename("roadsend/pcc/bugs/tests/bug-id-0001052.php"), "\n")));
  LINE(8,invoke_failed("sayhi3", Array(), 0x0000000074EB754CLL));
  (v_a = LINE(10,create_object("aclass", Array())));
  LINE(11,v_a.o_invoke_few_args("afunc", 0x285048A47E9CBC29LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
