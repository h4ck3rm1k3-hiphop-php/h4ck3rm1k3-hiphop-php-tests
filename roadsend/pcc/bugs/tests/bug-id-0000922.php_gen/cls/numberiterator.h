
#ifndef __GENERATED_cls_numberiterator_h__
#define __GENERATED_cls_numberiterator_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0000922.php line 4 */
class c_numberiterator : virtual public ObjectData {
  BEGIN_CLASS_MAP(numberiterator)
  END_CLASS_MAP(numberiterator)
  DECLARE_CLASS(numberiterator, NumberIterator, ObjectData)
  void init();
  public: static void ti_next(const char* cls);
  public: static void t_next() { ti_next("numberiterator"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_numberiterator_h__
