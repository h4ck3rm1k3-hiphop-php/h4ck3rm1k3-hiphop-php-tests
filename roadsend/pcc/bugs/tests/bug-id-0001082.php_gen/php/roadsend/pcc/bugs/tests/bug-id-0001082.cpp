
#include <php/roadsend/pcc/bugs/tests/bug-id-0001082.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001082_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001082.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001082_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  (v_test = "some string");
  v_test.set("user", ("bleh"), 0x53052C743152AAE3LL);
  v_test.set("db", ("blah"), 0x722F599AEA279E29LL);
  LINE(8,x_var_dump(1, v_test));
  (v_test = "");
  v_test.set("user", ("bleh"), 0x53052C743152AAE3LL);
  v_test.set("db", ("blah"), 0x722F599AEA279E29LL);
  LINE(15,x_var_dump(1, v_test));
  (v_test = "some string");
  v_test.set(0LL, ("blah"), 0x77CFA1EEF01BCA90LL);
  v_test.set(1LL, ("c"), 0x5BCA7C69B794F8CELL);
  LINE(22,x_var_dump(1, v_test));
  (v_test = "some string");
  v_test.set(0LL, ("blah"), 0x77CFA1EEF01BCA90LL);
  v_test.set(1LL, ("c"), 0x5BCA7C69B794F8CELL);
  LINE(29,x_var_dump(1, v_test));
  (v_test = "");
  v_test.set(0LL, ("blah"), 0x77CFA1EEF01BCA90LL);
  v_test.set(1LL, ("c"), 0x5BCA7C69B794F8CELL);
  LINE(36,x_var_dump(1, v_test));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
