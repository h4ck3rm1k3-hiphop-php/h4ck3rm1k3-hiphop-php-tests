
#ifndef __GENERATED_cls_template_h__
#define __GENERATED_cls_template_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001263.php line 6 */
class c_template : virtual public ObjectData {
  BEGIN_CLASS_MAP(template)
  END_CLASS_MAP(template)
  DECLARE_CLASS(template, template, ObjectData)
  void init();
  public: Array m_myVal;
  public: void t_add(CVarRef v_k);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_template_h__
