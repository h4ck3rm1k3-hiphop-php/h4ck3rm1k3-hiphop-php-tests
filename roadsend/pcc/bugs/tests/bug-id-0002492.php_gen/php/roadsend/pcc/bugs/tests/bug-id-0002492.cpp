
#include <php/roadsend/pcc/bugs/tests/bug-id-0002492.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002492.php line 57 */
void f_evenless(CVarRef v_target) {
  FUNCTION_INJECTION(evenless);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002492.php line 7 */
bool f_pma_gpc_extract(CVarRef v_array, Variant v_target) {
  FUNCTION_INJECTION(PMA_gpc_extract);
  DECLARE_GLOBAL_VARIABLES(g);
  int v_is_magic_quotes = 0;
  Primitive v_key = 0;
  Variant v_value;

  if (!(LINE(8,x_is_array(v_array)))) {
    return false;
  }
  (v_is_magic_quotes = LINE(11,x_get_magic_quotes_gpc()));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_array.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_key = iter3->first();
      {
        if (LINE(13,x_is_array(v_value))) {
          v_target.weakRemove(v_key);
          LINE(18,f_pma_gpc_extract(v_value, ref(lval(v_target.lvalAt(v_key)))));
        }
        else if (toBoolean(v_is_magic_quotes)) {
          v_target.set(v_key, (LINE(20,x_stripslashes(toString(v_value)))));
        }
        else {
          v_target.set(v_key, (v_value));
        }
      }
    }
  }
  return true;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002492.php line 41 */
void f_nothing(Variant v_target) {
  FUNCTION_INJECTION(nothing);
  DECLARE_GLOBAL_VARIABLES(g);
  (g->GV(save) = ref(v_target));
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002492_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002492.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002492_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_choad __attribute__((__unused__)) = (variables != gVariables) ? variables->get("choad") : g->GV(choad);
  Variant &v_baz __attribute__((__unused__)) = (variables != gVariables) ? variables->get("baz") : g->GV(baz);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);

  echo(" 0002492: more array/reference blues\n\nthis code is used in phpmyadmin to extract variables from GET/POST and make them global. the problem occurs when there is an array in one of them (because the script passed something like \"foo[]=12&foo[]=13\") and it recurses in the function.\n\n");
  g->gv__GET.set("blah", (ScalarArrays::sa_[0]), 0x49B1816976A1D113LL);
  LINE(29,f_pma_gpc_extract(g->gv__GET, ref(get_global_array_wrapper())));
  LINE(31,x_var_dump(1, g->GV(blah)));
  echo("\nThe gist of the problem is that the reference lookup of $target[$key] \nactually has to create the entry, i.e. call php-hash-lookup-ref with #t\n  for create\?. Here is a more specific test:\n\n");
  LINE(52,f_nothing(ref(lval(v_foo.lvalAt("blah", 0x49B1816976A1D113LL)))));
  print("foo:\n");
  LINE(54,x_var_dump(1, v_foo));
  LINE(61,f_evenless(v_choad.rvalAt("blah", 0x49B1816976A1D113LL)));
  print("choad:\n");
  LINE(63,x_var_dump(1, v_choad));
  (v_bar = ref(lval(v_baz.lvalAt("blah", 0x49B1816976A1D113LL))));
  LINE(67,x_var_dump(1, v_baz));
  (v_bar = v_zot.rvalAt("blah", 0x49B1816976A1D113LL));
  LINE(71,x_var_dump(1, v_zot));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
