
#include <php/roadsend/pcc/bugs/tests/bug-id-0002036.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002036_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002036.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002036_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("the reference property belongs to values, not hash slots\n\nin this case, both entries in the array are references, because there is only \none value and it's a reference. pcc does not implement this correctly:\n\n\n\n");
  (v_foo = ScalarArrays::sa_[0]);
  v_foo.set(1LL, (ref(lval(v_foo.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)))), 0x5BCA7C69B794F8CELL);
  LINE(12,x_var_dump(1, v_foo));
  echo("\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
