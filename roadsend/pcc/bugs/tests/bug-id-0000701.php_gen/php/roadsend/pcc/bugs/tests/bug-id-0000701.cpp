
#include <php/roadsend/pcc/bugs/tests/bug-id-0000701.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000701.php line 6 */
Variant c_sm_configreader_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_configreader_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_configreader_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("zot", m_zot));
  c_ObjectData::o_get(props);
}
bool c_sm_configreader_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7A42596E78DEAC62LL, zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_configreader_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A42596E78DEAC62LL, m_zot,
                         zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_configreader_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7A42596E78DEAC62LL, m_zot,
                      zot, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_configreader_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_configreader_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_configreader_foo)
ObjectData *c_sm_configreader_foo::cloneImpl() {
  c_sm_configreader_foo *obj = NEW(c_sm_configreader_foo)();
  cloneSet(obj);
  return obj;
}
void c_sm_configreader_foo::cloneSet(c_sm_configreader_foo *clone) {
  clone->m_zot = m_zot;
  ObjectData::cloneSet(clone);
}
Variant c_sm_configreader_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_configreader_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_configreader_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_configreader_foo$os_get(const char *s) {
  return c_sm_configreader_foo::os_get(s, -1);
}
Variant &cw_sm_configreader_foo$os_lval(const char *s) {
  return c_sm_configreader_foo::os_lval(s, -1);
}
Variant cw_sm_configreader_foo$os_constant(const char *s) {
  return c_sm_configreader_foo::os_constant(s);
}
Variant cw_sm_configreader_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_configreader_foo::os_invoke(c, s, params, -1, fatal);
}
void c_sm_configreader_foo::init() {
  m_zot = 23LL;
}
Object co_sm_configreader_foo(CArrRef params, bool init /* = true */) {
  return Object(p_sm_configreader_foo(NEW(c_sm_configreader_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000701_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000701.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000701_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_rName __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rName") : g->GV(rName);
  Variant &v_className __attribute__((__unused__)) = (variables != gVariables) ? variables->get("className") : g->GV(className);
  Variant &v_reader __attribute__((__unused__)) = (variables != gVariables) ? variables->get("reader") : g->GV(reader);

  echo("instantiating a class with a variable class name\n\n\n");
  (v_rName = "foo");
  (v_className = concat("SM_configReader_", toString(v_rName)));
  (v_reader = LINE(13,create_object(toString(v_className), Array())));
  echo(concat(toString(v_reader.o_get("zot", 0x7A42596E78DEAC62LL)), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
