
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000697_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000697_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0000697.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_myfun(CStrRef v_arg = k_undef, CStrRef v_arg1 = k_alsoundef);
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000697_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000697_h__
