
#include <php/roadsend/pcc/bugs/tests/bug-id-0001825.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001825_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001825.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001825_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("If the last semicolon before the '\?>' is skipped, there is a comment on the same \nline as the last line of code and the end tag is also on the line, pcc pukes.\n\n");
  echo("goodbye, cruel world\n");
  echo("\nfoo\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
