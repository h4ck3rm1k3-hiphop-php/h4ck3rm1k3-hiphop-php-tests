
#include <php/roadsend/pcc/bugs/tests/bug-id-0001723.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001723_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001723.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001723_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("compiled code is mutating arrays on n-dimensional access:\n\n");
  if (isset(v_foo.rvalAt("asdf", 0x06528546A3758F5CLL), "zipl", 0x0451D669D34FBE99LL)) {
    echo("wasset\n");
  }
  LINE(10,x_print_r(v_foo));
  print((toString(v_foo.rvalAt("anotherkey", 0x791011E7890E0EB6LL).rvalAt("bork", 0x1E46B4C4E7D774CALL))));
  LINE(15,x_print_r(v_foo));
  if (isset(v_foo)) {
    print("the foo variable was set\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
