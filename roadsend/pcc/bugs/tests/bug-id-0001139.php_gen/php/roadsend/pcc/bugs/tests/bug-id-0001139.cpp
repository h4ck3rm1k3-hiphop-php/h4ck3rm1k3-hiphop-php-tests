
#include <php/roadsend/pcc/bugs/tests/bug-id-0001139.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001139_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001139.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001139_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_php_errormsg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_errormsg") : g->GV(php_errormsg);

  echo(concat(LINE(2,x_implode(ScalarArrays::sa_[0])), "\n"));
  echo(concat(LINE(3,x_implode("nothing", ScalarArrays::sa_[0])), "\n"));
  echo(concat(LINE(4,x_implode(ScalarArrays::sa_[1])), "\n"));
  echo(concat(LINE(5,x_implode(":", ScalarArrays::sa_[1])), "\n"));
  echo(concat(LINE(6,x_implode(":", ScalarArrays::sa_[2])), "\n"));
  echo(concat(toString(v_php_errormsg), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
