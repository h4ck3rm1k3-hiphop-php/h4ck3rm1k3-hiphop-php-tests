
#include <php/roadsend/pcc/bugs/tests/bug-id-0001153.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001153.php line 2 */
void f_foo(CArrRef v_bar //  = ScalarArrays::sa_[0]
) {
  FUNCTION_INJECTION(foo);
  LINE(4,x_var_dump(1, v_bar));
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001153_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001153.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001153_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(6,f_foo());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
