
#include <php/roadsend/pcc/bugs/tests/bug-id-0001090.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001090_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001090.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001090_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_g __attribute__((__unused__)) = (variables != gVariables) ? variables->get("g") : g->GV(g);

  echo("precedence problem with ugly if operator\n\n");
  (v_m = true);
  (v_t = "");
  (v_g = "something");
  echo(LINE(8,concat3("answer is ", (toString(!equal(v_t, "") && toBoolean(v_m) ? (("yes")) : (("no")))), " <-- yeah")));
  echo("\n/*\nthis line:\n($t != '' && $m \? 'yes' : 'no')\nis currently parsed like this:\n(($t != '') && ($m \? 'yes' : 'no'))\nneeds to be like this:\n((($t != '') && $m) \? 'yes' : 'no')\n*/");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
