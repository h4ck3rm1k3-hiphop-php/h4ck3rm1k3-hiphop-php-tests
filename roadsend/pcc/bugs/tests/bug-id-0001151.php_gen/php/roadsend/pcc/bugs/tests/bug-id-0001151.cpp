
#include <php/roadsend/pcc/bugs/tests/bug-id-0001151.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001151_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001151.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001151_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ar") : g->GV(ar);
  Variant &v_count __attribute__((__unused__)) = (variables != gVariables) ? variables->get("count") : g->GV(count);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("0001151 core error\n");
  (v_ar = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for ((v_count = 0LL); less(v_count, 10LL); v_count++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_ar.set(v_count, (toString(v_count)));
        lval(v_ar.lvalAt(v_count)).set("idx", (toString(v_count)), 0x7FC46D158D6CEA23LL);
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_count = 0LL); less(v_count, 10LL); v_count++) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(LINE(10,concat4(toString(v_ar.rvalAt(v_count)), " -- ", toString(v_ar.rvalAt(v_count).rvalAt("idx", 0x7FC46D158D6CEA23LL)), "\n")));
      }
    }
  }
  (v_a = "0123456789");
  print(concat(toString(v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "\n"));
  v_a.set(9LL, (v_a.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), 0x3A1F6363F43EC697LL);
  LINE(15,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
