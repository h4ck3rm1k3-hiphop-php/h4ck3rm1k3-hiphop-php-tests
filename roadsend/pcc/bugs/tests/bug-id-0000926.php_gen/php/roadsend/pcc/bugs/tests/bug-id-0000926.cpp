
#include <php/roadsend/pcc/bugs/tests/bug-id-0000926.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000926_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000926.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000926_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("0000926: we need more operators\n\n");
  (v_a = 10LL);
  echo("*=  ");
  v_a *= 42LL;
  LINE(11,x_var_dump(1, v_a));
  echo("\n/=  ");
  v_a /= 42LL;
  LINE(15,x_var_dump(1, v_a));
  echo("\n.=  ");
  concat_assign(v_a, toString(42LL));
  LINE(19,x_var_dump(1, v_a));
  echo("\n%=  ");
  v_a %= 42LL;
  LINE(23,x_var_dump(1, v_a));
  echo("\n&=  ");
  v_a &= 42LL;
  LINE(27,x_var_dump(1, v_a));
  print("\n");
  print("152 << 2: ");
  print(toString(608LL));
  print("\n");
  print("152 >> 2: ");
  print(toString(38LL));
  echo("\n|=  ");
  v_a |= 42LL;
  LINE(39,x_var_dump(1, v_a));
  echo("\n^=  ");
  v_a ^= 42LL;
  LINE(43,x_var_dump(1, v_a));
  echo("\n<<=  ");
  v_a <<= 42LL;
  LINE(47,x_var_dump(1, v_a));
  echo("\n>>=  ");
  v_a >>= 4LL;
  LINE(51,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
