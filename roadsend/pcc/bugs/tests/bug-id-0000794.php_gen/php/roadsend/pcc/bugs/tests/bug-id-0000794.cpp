
#include <php/roadsend/pcc/bugs/tests/bug-id-0000794.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000794.php line 3 */
String f_functest(CVarRef v_a, CVarRef v_b, CVarRef v_c) {
  FUNCTION_INJECTION(functest);
  return "this is craptacular";
} /* function */
Variant i_functest(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x32B7BDB70DFEDE9ELL, functest) {
    return (f_functest(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000794_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000794.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000794_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_languages __attribute__((__unused__)) = (variables != gVariables) ? variables->get("languages") : g->GV(languages);
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);

  lval(v_languages.lvalAt("one", 0x37301431385F1EDDLL)).set("XTRA_CODE", ("functest"), 0x3CCB80F279AAC5A8LL);
  (v_filename = LINE(9,invoke((toString(v_languages.rvalAt("one", 0x37301431385F1EDDLL).rvalAt("XTRA_CODE", 0x3CCB80F279AAC5A8LL))), Array(ArrayInit(3).set(0, "downloadfilename").set(1, "").set(2, "").create()), -1)));
  echo(toString(v_filename));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
