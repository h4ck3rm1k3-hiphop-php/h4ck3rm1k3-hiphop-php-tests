
#include <php/roadsend/pcc/bugs/tests/bug-id-0001886.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001886.php line 4 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("hash", m_hash.isReferenced() ? ref(m_hash) : m_hash));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x4436B8A58BF97C51LL, hash, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4436B8A58BF97C51LL, m_hash,
                         hash, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x4436B8A58BF97C51LL, m_hash,
                      hash, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4436B8A58BF97C51LL, m_hash,
                         hash, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_hash = m_hash.isReferenced() ? ref(m_hash) : m_hash;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return ref(t_afunc(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return ref(t_afunc(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_hash = null;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001886.php line 7 */
Variant c_aclass::t_afunc(CVarRef v_i) {
  INSTANCE_METHOD_INJECTION(aclass, aclass::afunc);
  Variant v_var;

  (v_var = LINE(8,p_bclass(p_bclass(NEWOBJ(c_bclass)())->create(v_i, ref(this)))));
  m_hash.set(v_i, (ref(v_var)));
  return ref(v_var);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001886.php line 15 */
Variant c_bclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("id", m_id.isReferenced() ? ref(m_id) : m_id));
  props.push_back(NEW(ArrayElement)("parent", m_parent.isReferenced() ? ref(m_parent) : m_parent));
  c_ObjectData::o_get(props);
}
bool c_bclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x16E2F26FFB10FD8CLL, parent, 6);
      break;
    case 2:
      HASH_EXISTS_STRING(0x028B9FE0C4522BE2LL, id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    case 2:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_bclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                      parent, 6);
      break;
    case 2:
      HASH_SET_STRING(0x028B9FE0C4522BE2LL, m_id,
                      id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    case 2:
      HASH_RETURN_STRING(0x028B9FE0C4522BE2LL, m_id,
                         id, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bclass)
ObjectData *c_bclass::create(CVarRef v_id, Variant v_parent) {
  init();
  t_bclass(v_id, ref(v_parent));
  return this;
}
ObjectData *c_bclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), ref(const_cast<Array&>(params).lvalAt(1))));
  } else return this;
}
ObjectData *c_bclass::cloneImpl() {
  c_bclass *obj = NEW(c_bclass)();
  cloneSet(obj);
  return obj;
}
void c_bclass::cloneSet(c_bclass *clone) {
  clone->m_id = m_id.isReferenced() ? ref(m_id) : m_id;
  clone->m_parent = m_parent.isReferenced() ? ref(m_parent) : m_parent;
  ObjectData::cloneSet(clone);
}
Variant c_bclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bclass$os_get(const char *s) {
  return c_bclass::os_get(s, -1);
}
Variant &cw_bclass$os_lval(const char *s) {
  return c_bclass::os_lval(s, -1);
}
Variant cw_bclass$os_constant(const char *s) {
  return c_bclass::os_constant(s);
}
Variant cw_bclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bclass::os_invoke(c, s, params, -1, fatal);
}
void c_bclass::init() {
  m_id = null;
  m_parent = null;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001886.php line 18 */
void c_bclass::t_bclass(CVarRef v_id, Variant v_parent) {
  INSTANCE_METHOD_INJECTION(bclass, bclass::bclass);
  bool oldInCtor = gasInCtor(true);
  (m_id = v_id);
  (m_parent = ref(v_parent));
  gasInCtor(oldInCtor);
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Object co_bclass(CArrRef params, bool init /* = true */) {
  return Object(p_bclass(NEW(c_bclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001886_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001886.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001886_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  echo("recursive references\n");
  (v_m = ((Object)(LINE(25,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  (v_a = ref(LINE(26,v_m.o_invoke_few_args("afunc", 0x285048A47E9CBC29LL, 1, 1LL))));
  (v_b = ref(LINE(27,v_m.o_invoke_few_args("afunc", 0x285048A47E9CBC29LL, 1, 2LL))));
  (v_c = ref(LINE(28,v_m.o_invoke_few_args("afunc", 0x285048A47E9CBC29LL, 1, 3LL))));
  (v_b.o_lval("id", 0x028B9FE0C4522BE2LL) = 10LL);
  lval(lval(v_b.o_lval("parent", 0x16E2F26FFB10FD8CLL)).o_lval("hash", 0x4436B8A58BF97C51LL)).append(("hello"));
  LINE(33,x_print_r(v_m));
  LINE(34,x_print_r(v_a));
  LINE(35,x_print_r(v_b));
  LINE(36,x_print_r(v_c));
  echo("\n\n and now, var_dump \n\n");
  LINE(40,x_var_dump(1, v_m));
  LINE(41,x_var_dump(1, v_a));
  LINE(42,x_var_dump(1, v_b));
  LINE(43,x_var_dump(1, v_c));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
