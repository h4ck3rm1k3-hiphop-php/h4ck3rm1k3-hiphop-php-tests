
#include <php/roadsend/pcc/bugs/tests/bug-id-0001854.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001854_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001854.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001854_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("Summary  \t continue\n\n problem inside a switch statement\n\nDescription \t\n\n when you try to continue out of a nested switch statment, pcc complains but php lets you do it.\n\n");
  {
    LOOP_COUNTER(1);
    for ((v_a = 1LL); less(v_a, 10LL); v_a++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          switch (toInt64(v_a)) {
          case 1LL:
            {
              echo(toString(v_a) + toString("\n"));
              break;
            }
          case 2LL:
            {
              echo(toString(v_a) + toString("\n"));
              break;
            }
          case 3LL:
            {
              echo(toString(v_a) + toString("\n"));
              break;
            }
          case 4LL:
            {
              goto continue1;
            }
          case 5LL:
            {
              echo(toString(v_a) + toString("\n"));
              break;
            }
          default:
            {
            }
          }
        }
      }
      continue1:;
    }
  }
  echo("\nfoo\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
