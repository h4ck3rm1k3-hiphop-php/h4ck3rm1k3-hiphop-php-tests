
#include <php/roadsend/pcc/bugs/tests/bug-id-0002385.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002385.php line 50 */
int64 f_m() {
  FUNCTION_INJECTION(m);
  return 2LL;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002385.php line 67 */
int64 f_n() {
  FUNCTION_INJECTION(n);
  return 2LL;
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002385_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002385.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002385_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);

  echo("continue with parens\n\n");
  {
    LOOP_COUNTER(1);
    for ((v_a = 0LL); less(v_a, 5LL); v_a++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(6,concat3("a: ", toString(v_a), "\n")));
        {
          LOOP_COUNTER(2);
          for ((v_b = 0LL); less(v_b, 5LL); v_b++) {
            LOOP_COUNTER_CHECK(2);
            {
              echo(LINE(8,concat3("b: ", toString(v_b), "\n")));
              if (equal(v_b, 3LL)) {
                goto continue1;
              }
            }
          }
        }
      }
      continue1:;
    }
  }
  echo("\nand break with parens:\n\n");
  {
    LOOP_COUNTER(3);
    for ((v_a = 0LL); less(v_a, 5LL); v_a++) {
      LOOP_COUNTER_CHECK(3);
      {
        echo(LINE(20,concat3("a: ", toString(v_a), "\n")));
        {
          LOOP_COUNTER(4);
          for ((v_b = 0LL); less(v_b, 5LL); v_b++) {
            LOOP_COUNTER_CHECK(4);
            {
              echo(LINE(22,concat3("b: ", toString(v_b), "\n")));
              if (equal(v_b, 3LL)) {
                goto break3;
              }
            }
          }
        }
      }
    }
    break3:;
  }
  echo("\n\ncontinue with a variable:\n\n");
  (v_m = 2LL);
  {
    LOOP_COUNTER(5);
    for ((v_a = 0LL); less(v_a, 5LL); v_a++) {
      LOOP_COUNTER_CHECK(5);
      {
        echo(LINE(36,concat3("a: ", toString(v_a), "\n")));
        {
          LOOP_COUNTER(6);
          for ((v_b = 0LL); less(v_b, 5LL); v_b++) {
            LOOP_COUNTER_CHECK(6);
            {
              echo(LINE(38,concat3("b: ", toString(v_b), "\n")));
              if (equal(v_b, 3LL)) {
                switch (toInt64((toInt64(v_m)))) {
                case 2: goto continue5;
                case 1: goto continue6;
                default:
                if ((toInt64((toInt64(v_m))))<2) {
                goto continue6;
                } else {
                throw_fatal("bad continue");
                }
                }
              }
            }
            continue6:;
          }
        }
      }
      continue5:;
    }
  }
  echo("\n\ncontinue with a function call:\n\n");
  {
    LOOP_COUNTER(7);
    for ((v_a = 0LL); less(v_a, 5LL); v_a++) {
      LOOP_COUNTER_CHECK(7);
      {
        echo(LINE(53,concat3("a: ", toString(v_a), "\n")));
        {
          LOOP_COUNTER(8);
          for ((v_b = 0LL); less(v_b, 5LL); v_b++) {
            LOOP_COUNTER_CHECK(8);
            {
              echo(LINE(55,concat3("b: ", toString(v_b), "\n")));
              if (equal(v_b, 3LL)) {
                switch (LINE(57,f_m())) {
                case 2: goto continue7;
                case 1: goto continue8;
                default:
                if ((f_m())<2) {
                goto continue8;
                } else {
                throw_fatal("bad continue");
                }
                }
              }
            }
            continue8:;
          }
        }
      }
      continue7:;
    }
  }
  echo("\n\nbreak with a function call:\n\n");
  {
    LOOP_COUNTER(9);
    for ((v_a = 0LL); less(v_a, 5LL); v_a++) {
      LOOP_COUNTER_CHECK(9);
      {
        echo(LINE(70,concat3("a: ", toString(v_a), "\n")));
        {
          LOOP_COUNTER(10);
          for ((v_b = 0LL); less(v_b, 5LL); v_b++) {
            LOOP_COUNTER_CHECK(10);
            {
              echo(LINE(72,concat3("b: ", toString(v_b), "\n")));
              if (equal(v_b, 3LL)) {
                switch (LINE(74,f_n())) {
                case 2: goto break9;
                case 1: goto break10;
                default:
                if ((f_n())<2) {
                goto break10;
                } else {
                throw_fatal("bad break");
                }
                }
              }
            }
          }
          break10:;
        }
      }
    }
    break9:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
