
#include <php/roadsend/pcc/bugs/tests/bug-id-0003091.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003091_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0003091.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0003091_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);

  echo("test comparison of resources\n");
  (v_fp = LINE(5,x_fopen(get_source_filename("roadsend/pcc/bugs/tests/bug-id-0003091.php"), "r")));
  if (equal(v_fp, "")) {
    echo("unable to open file\n");
  }
  else {
    echo("opened file\n");
  }
  echo(toString(toInt64(v_fp)));
  echo("\n");
  echo(LINE(16,concat3("1: ", (toString(equal(v_fp, 0LL))), "\n")));
  echo(LINE(17,concat3("2: ", (toString(equal(v_fp, 1LL))), "\n")));
  echo(LINE(18,concat3("3: ", (toString(more(v_fp, 0LL))), "\n")));
  echo(LINE(19,concat3("4: ", (toString(less(v_fp, 0LL))), "\n")));
  LINE(21,x_fclose(toObject(v_fp)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
