
#include <php/roadsend/pcc/bugs/tests/bug-id-0000734.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000734.php line 6 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("SfFilter", m_SfFilter.isReferenced() ? ref(m_SfFilter) : m_SfFilter));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x2FED378C3D7C8758LL, SfFilter, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2FED378C3D7C8758LL, m_SfFilter,
                         SfFilter, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x2FED378C3D7C8758LL, m_SfFilter,
                      SfFilter, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x2FED378C3D7C8758LL, m_SfFilter,
                         SfFilter, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_SfFilter = m_SfFilter.isReferenced() ? ref(m_SfFilter) : m_SfFilter;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        return (t_afun(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        return (t_afun(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_SfFilter = null;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000734.php line 9 */
void c_aclass::t_afun(CVarRef v_autoFilter) {
  INSTANCE_METHOD_INJECTION(aclass, aclass::afun);
  if (toBoolean(v_autoFilter)) {
  }
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000734.php line 19 */
void c_aclass::t_setsffilter(CVarRef v_filter) {
  INSTANCE_METHOD_INJECTION(aclass, aclass::setSfFilter);
  (m_SfFilter = v_filter);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000734.php line 23 */
String c_aclass::t_getname() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::getName);
  return "foo";
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000734_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000734.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000734_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_aninstance __attribute__((__unused__)) = (variables != gVariables) ? variables->get("aninstance") : g->GV(aninstance);

  echo("0000734 parse error on empty conditional block\n\n\n");
  (v_aninstance = ((Object)(LINE(28,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  LINE(29,v_aninstance.o_invoke_few_args("afun", 0x64AA3995684BE7FDLL, 1, false));
  echo(toString(v_aninstance.o_get("SfFilter", 0x2FED378C3D7C8758LL)) + toString("\n"));
  LINE(31,v_aninstance.o_invoke_few_args("afun", 0x64AA3995684BE7FDLL, 1, true));
  echo(toString(v_aninstance.o_get("SfFilter", 0x2FED378C3D7C8758LL)) + toString("\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
