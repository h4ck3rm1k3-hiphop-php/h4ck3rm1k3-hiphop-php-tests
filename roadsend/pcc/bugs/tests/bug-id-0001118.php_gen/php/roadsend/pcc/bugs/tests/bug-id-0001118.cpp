
#include <php/roadsend/pcc/bugs/tests/bug-id-0001118.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001118_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001118.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001118_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_filename __attribute__((__unused__)) = (variables != gVariables) ? variables->get("filename") : g->GV(filename);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);

  (v_filename = LINE(2,x_tempnam("/tmp", "phpt")));
  (toBoolean((v_fp = LINE(4,x_fopen(toString(v_filename), "w+"))))) || (toBoolean(f_exit(concat3("can't open ", toString(v_filename), " for append"))));
  echo((LINE(5,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 1:  ", eo_1, "\n")))));
  echo((LINE(6,(assignCallTemp(eo_1, toString(x_fwrite(toObject(v_fp), "quxbar"))),concat3("fwrite 1: ", eo_1, "\n")))));
  echo((LINE(7,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 2:  ", eo_1, "\n")))));
  echo((LINE(8,(assignCallTemp(eo_1, toString(x_fseek(toObject(v_fp), 3LL, 0LL /* SEEK_SET */))),concat3("fseek 1:  ", eo_1, "\n")))));
  echo((LINE(9,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 3:  ", eo_1, "\n")))));
  LINE(10,x_var_dump(1, x_fread(toObject(v_fp), 1LL)));
  echo((LINE(11,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 4:  ", eo_1, "\n")))));
  echo((LINE(12,(assignCallTemp(eo_1, toString(x_fseek(toObject(v_fp), 4LL, 0LL /* SEEK_SET */))),concat3("fseek 2:  ", eo_1, "\n")))));
  echo((LINE(13,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 5:  ", eo_1, "\n")))));
  echo((LINE(14,(assignCallTemp(eo_1, toString(x_fwrite(toObject(v_fp), "!"))),concat3("fwrite 2: ", eo_1, "\n")))));
  echo((LINE(15,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 6:  ", eo_1, "\n")))));
  echo((LINE(16,(assignCallTemp(eo_1, toString(x_fseek(toObject(v_fp), 0LL, 0LL /* SEEK_SET */))),concat3("fseek 3:  ", eo_1, "\n")))));
  echo((LINE(17,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 7:  ", eo_1, "\n")))));
  LINE(18,x_var_dump(1, x_fread(toObject(v_fp), 4095LL)));
  echo((LINE(19,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 8:  ", eo_1, "\n")))));
  echo((LINE(20,(assignCallTemp(eo_1, toString(x_ftruncate(toObject(v_fp), 0LL))),concat3("ftruncate:", eo_1, "\n")))));
  echo((LINE(21,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 9:  ", eo_1, "\n")))));
  echo((LINE(22,(assignCallTemp(eo_1, toString(x_rewind(toObject(v_fp)))),concat3("rewind:   ", eo_1, "\n")))));
  echo((LINE(23,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 10: ", eo_1, "\n")))));
  echo((LINE(24,(assignCallTemp(eo_1, toString(x_fwrite(toObject(v_fp), "barfoo"))),concat3("fwrite 3: ", eo_1, "\n")))));
  echo((LINE(25,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 11: ", eo_1, "\n")))));
  echo((LINE(26,(assignCallTemp(eo_1, toString(x_fseek(toObject(v_fp), 3LL, 0LL /* SEEK_SET */))),concat3("fseek 4:  ", eo_1, "\n")))));
  echo((LINE(27,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 12: ", eo_1, "\n")))));
  LINE(28,x_var_dump(1, x_fread(toObject(v_fp), 1LL)));
  echo((LINE(29,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 13: ", eo_1, "\n")))));
  echo((LINE(30,(assignCallTemp(eo_1, toString(x_fwrite(toObject(v_fp), "!"))),concat3("fwrite 4: ", eo_1, "\n")))));
  echo((LINE(31,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 14: ", eo_1, "\n")))));
  echo((LINE(32,(assignCallTemp(eo_1, toString(x_fseek(toObject(v_fp), 0LL, 0LL /* SEEK_SET */))),concat3("fseek 5:  ", eo_1, "\n")))));
  echo((LINE(33,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 15: ", eo_1, "\n")))));
  LINE(34,x_var_dump(1, x_fread(toObject(v_fp), 4095LL)));
  echo((LINE(35,(assignCallTemp(eo_1, toString(x_ftell(toObject(v_fp)))),concat3("ftell 16: ", eo_1, "\n")))));
  LINE(37,x_fclose(toObject(v_fp)));
  LINE(38,x_unlink(toString(v_filename)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
