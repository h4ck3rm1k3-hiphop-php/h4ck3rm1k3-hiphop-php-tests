
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000751_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000751_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0000751.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_aphp_uname();
void f_parsesignature(Variant v_uname = null);
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000751_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000751_h__
