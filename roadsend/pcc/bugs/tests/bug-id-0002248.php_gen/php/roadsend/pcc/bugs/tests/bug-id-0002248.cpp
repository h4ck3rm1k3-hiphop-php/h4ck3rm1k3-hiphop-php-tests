
#include <php/roadsend/pcc/bugs/tests/bug-id-0002248.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002248_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002248.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002248_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  if (LINE(3,x_function_exists(toString(null)))) print("foo\n");
  else print("bar\n");
  if (LINE(8,invoke_failed("function_exists", Array(), 0x000000008342A0C7LL))) print("foo\n");
  else print("bar\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
