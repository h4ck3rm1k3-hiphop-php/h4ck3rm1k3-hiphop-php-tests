
#include <php/roadsend/pcc/bugs/tests/bug-id-0003090.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0003090.php line 6 */
void f_write() {
  FUNCTION_INJECTION(Write);
  int64 v_i = 0;

  (v_i = 0LL);
  LOOP_COUNTER(1);
  {
    while (toBoolean(0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        if (toBoolean(1LL)) {
          if (toBoolean(2LL)) {
            if (toBoolean(3LL)) {
              v_i++;
            }
          }
          else {
            (v_i = 1LL);
          }
          ;
        }
        else {
          v_i++;
        }
      }
    }
  }
} /* function */
Variant i_write(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x548D0802D1DB44ADLL, write) {
    return (f_write(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003090_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0003090.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0003090_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("we want this to not cause CFA flapping!  (too much optimization)\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
