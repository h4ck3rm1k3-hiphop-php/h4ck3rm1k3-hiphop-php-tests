
#include <php/roadsend/pcc/bugs/tests/bug-id-0000798.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000798_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000798.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000798_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bndreg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bndreg") : g->GV(bndreg);

  (v_bndreg = LINE(3,x_str_replace("\\", "\\\\", v_bndreg)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
