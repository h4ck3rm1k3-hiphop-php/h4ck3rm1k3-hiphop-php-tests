
#include <php/roadsend/pcc/bugs/tests/bug-id-0001407.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001407_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001407.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001407_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_p __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p") : g->GV(p);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);

  (v_a = ScalarArrays::sa_[0]);
  (v_b = v_a);
  (v_a = ScalarArrays::sa_[1]);
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(11,x_each(ref(v_b))), v_p, v_s))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(12,x_var_dump(1, v_p));
        LINE(13,x_var_dump(1, v_s));
        v_a.set(v_p, (v_s));
      }
    }
  }
  LINE(17,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
