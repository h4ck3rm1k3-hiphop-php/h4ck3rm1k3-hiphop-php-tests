
#include <php/roadsend/pcc/bugs/tests/bug-id-0002128.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002128_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002128.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002128_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_out __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out") : g->GV(out);

  (v_out = LINE(2,x_exec("echo foobar;echo barfoo")));
  LINE(3,x_var_dump(1, v_out));
  (v_out = LINE(4,x_exec("echo foobar")));
  LINE(5,x_var_dump(1, v_out));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
