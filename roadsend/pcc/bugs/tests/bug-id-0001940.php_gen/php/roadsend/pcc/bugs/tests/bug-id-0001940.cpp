
#include <php/roadsend/pcc/bugs/tests/bug-id-0001940.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001940_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001940.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001940_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("define() inside of an ugly-then produces a parse error\n\n\n");
  (!(LINE(6,x_defined("DEF")))) ? ((Variant)(g->declareConstant("DEF", g->k_DEF, "worked ok"))) : ((Variant)(null));
  echo(concat(toString(get_global_variables()->k_DEF), "\n"));
  echo("\nthis is because define() is actually an rval (I think it's even \na normal function in php):\n\n");
  echo(toString(LINE(18,g->declareConstant("FOO", g->k_FOO, 2LL))));
  echo(toString(LINE(19,g->declareConstant("FOO", g->k_FOO, 3LL))));
  echo(LINE(20,concat3(", ", toString(get_global_variables()->k_FOO), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
