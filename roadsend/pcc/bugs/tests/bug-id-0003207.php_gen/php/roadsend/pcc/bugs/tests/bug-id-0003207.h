
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0003207_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0003207_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0003207.fw.h>

// Declarations
#include <cls/wibble.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003207_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_flup();
Object co_wibble(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0003207_h__
