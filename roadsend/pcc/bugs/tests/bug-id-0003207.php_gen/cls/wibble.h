
#ifndef __GENERATED_cls_wibble_h__
#define __GENERATED_cls_wibble_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0003207.php line 9 */
class c_wibble : virtual public ObjectData {
  BEGIN_CLASS_MAP(wibble)
  END_CLASS_MAP(wibble)
  DECLARE_CLASS(wibble, wibble, ObjectData)
  void init();
  public: Variant m_moo;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_wibble_h__
