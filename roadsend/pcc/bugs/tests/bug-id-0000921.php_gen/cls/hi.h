
#ifndef __GENERATED_cls_hi_h__
#define __GENERATED_cls_hi_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0000921.php line 3 */
class c_hi : virtual public ObjectData {
  BEGIN_CLASS_MAP(hi)
  END_CLASS_MAP(hi)
  DECLARE_CLASS(hi, hi, ObjectData)
  void init();
  public: Variant m_string;
  public: void t_hi(Variant v_str);
  public: ObjectData *create(Variant v_str);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_hi_h__
