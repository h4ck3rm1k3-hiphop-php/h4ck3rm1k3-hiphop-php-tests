
#include <php/roadsend/pcc/bugs/tests/bug-id-0001092.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001092.php line 6 */
void f_error(CStrRef v_msg) {
  FUNCTION_INJECTION(error);
  echo("error, your keyboard is one firE!!!!!!!\n");
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001092_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001092.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001092_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("0001092 able to override builtin function 'error'\n\nNote: This test only produces a warning, and then only when compiled.\n");
  LINE(10,f_error("meep"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
