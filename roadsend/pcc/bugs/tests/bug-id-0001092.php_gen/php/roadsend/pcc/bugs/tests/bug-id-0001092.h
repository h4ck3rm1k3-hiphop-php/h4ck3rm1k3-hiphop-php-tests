
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001092_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001092_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001092.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001092_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_error(CStrRef v_msg);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001092_h__
