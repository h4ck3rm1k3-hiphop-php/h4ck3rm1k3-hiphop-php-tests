
#include <php/roadsend/pcc/bugs/tests/bug-id-0001834.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001834_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001834.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001834_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("doesn't escape curly bracket right in front of a var inside double quotes\n\nif a literal string contains a curly bracket followed by a variable and\nthe curly bracket is escaped (so it's not part of the variable) pcc pukes on the next character after the close curly bracket.\n\n");
  (v_foo = "problem");
  echo(LINE(8,concat3("this is a \\{", toString(v_foo), "}\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
