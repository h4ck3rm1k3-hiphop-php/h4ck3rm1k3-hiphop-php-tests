
#include <php/roadsend/pcc/bugs/tests/bug-id-0002793.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002793.php line 3 */
void f_bar() {
  FUNCTION_INJECTION(bar);
  Variant v_a;
  String v_b;
  Variant v_c;
  Variant v_d;

  (v_a = "foo");
  (v_b = "foo");
  (v_c = ref(v_a));
  (v_d = v_a);
  v_a.set(0LL, ("b"), 0x77CFA1EEF01BCA90LL);
  print(toString(v_a) + toString("\n"));
  print(v_b + toString("\n"));
  print(toString(v_c) + toString("\n"));
  print(toString(v_d) + toString("\n"));
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002793_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002793.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002793_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(15,f_bar());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
