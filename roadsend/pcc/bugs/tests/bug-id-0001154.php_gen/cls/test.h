
#ifndef __GENERATED_cls_test_h__
#define __GENERATED_cls_test_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0001154.php line 2 */
class c_test : virtual public ObjectData {
  BEGIN_CLASS_MAP(test)
  END_CLASS_MAP(test)
  DECLARE_CLASS(test, Test, ObjectData)
  void init();
  public: void t_test();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: String t_transform(CVarRef v_buffer);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_test_h__
