
#include <php/roadsend/pcc/bugs/tests/bug-id-0003165.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0003165.php line 16 */
void f_foo_with_args(int num_args, int64 v_a, int64 v_b, Array args /* = Array() */) {
  FUNCTION_INJECTION(foo_with_args);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int v_numargs = 0;
  Array v_arg_list;
  int64 v_i = 0;

  (v_numargs = LINE(18,num_args));
  echo(LINE(19,concat3("correct -- Number of arguments: ", toString(v_numargs), "<br />\n")));
  if (not_less(v_numargs, 2LL)) {
    echo(LINE(21,(assignCallTemp(eo_1, toString(func_get_arg(num_args, Array(ArrayInit(2).set(0, v_a).set(1, v_b).create()),args,toInt32(1LL)))),concat3("0Second argument is: ", eo_1, "<br />\n"))));
  }
  LINE(23,f_foo(3, ScalarArrays::sa_[0]));
  (v_numargs = LINE(24,num_args));
  echo(LINE(25,concat3("wrong -- Number of arguments: ", toString(v_numargs), "<br />\n")));
  if (not_less(v_numargs, 2LL)) {
    echo(LINE(27,(assignCallTemp(eo_1, toString(func_get_arg(num_args, Array(ArrayInit(2).set(0, v_a).set(1, v_b).create()),args,toInt32(1LL)))),concat3("1Second argument is: ", eo_1, "<br />\n"))));
  }
  LINE(29,f_foo(3, ScalarArrays::sa_[1]));
  (v_arg_list = LINE(30,func_get_args(num_args, Array(ArrayInit(2).set(0, v_a).set(1, v_b).create()),args)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_numargs); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(32,concat5("2Argument ", toString(v_i), " is: ", toString(v_arg_list.rvalAt(v_i)), "<br />\n")));
      }
    }
  }
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0003165.php line 3 */
void f_foo(int num_args, Array args /* = Array() */) {
  FUNCTION_INJECTION(foo);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int v_numargs = 0;

  (v_numargs = LINE(5,num_args));
  echo(LINE(6,concat3("Number of arguments: ", toString(v_numargs), "<br />\n")));
  if (not_less(v_numargs, 2LL)) {
    echo(LINE(8,(assignCallTemp(eo_1, toString(func_get_arg(num_args, Array(),args,toInt32(1LL)))),concat3("Second argument is: ", eo_1, "<br />\n"))));
  }
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003165_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0003165.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0003165_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(36,f_foo_with_args(2, 1LL, 2LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
