
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0003165_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0003165_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0003165.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foo_with_args(int num_args, int64 v_a, int64 v_b, Array args = Array());
void f_foo(int num_args, Array args = Array());
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003165_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0003165_h__
