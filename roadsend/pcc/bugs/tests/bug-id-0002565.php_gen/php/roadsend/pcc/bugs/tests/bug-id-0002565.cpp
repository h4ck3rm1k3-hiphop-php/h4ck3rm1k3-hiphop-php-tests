
#include <php/roadsend/pcc/bugs/tests/bug-id-0002565.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002565_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002565.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002565_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_baz __attribute__((__unused__)) = (variables != gVariables) ? variables->get("baz") : g->GV(baz);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  echo("0002565: fwrite() is not binary safe\n\nit's using stdio's fputs(), so it stops at the first 0 in the string:\n\n");
  (v_baz = LINE(7,x_fopen("/dev/zero", "r")));
  (v_foo = LINE(8,x_fread(toObject(v_baz), 8192LL)));
  (v_bar = LINE(9,x_fopen("woot", "wb")));
  LINE(10,x_fwrite(toObject(v_bar), toString(v_foo)));
  LINE(11,x_fclose(toObject(v_bar)));
  echo(concat(toString(LINE(12,x_filesize("woot"))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
