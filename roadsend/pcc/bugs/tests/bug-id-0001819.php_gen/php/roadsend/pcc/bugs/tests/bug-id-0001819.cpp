
#include <php/roadsend/pcc/bugs/tests/bug-id-0001819.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001819_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001819.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001819_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);

  echo("It appears to be undocumented, but you can use {} for \naccessing arrays as well as []. i.e. foo{1}{2}\n\n");
  (v_test = ScalarArrays::sa_[0]);
  echo(concat(toString(v_test.rvalAt("foo", 0x4154FA2EF733DA8FLL).rvalAt("foo2", 0x2D5E1CDE7FC9C98CLL)), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
