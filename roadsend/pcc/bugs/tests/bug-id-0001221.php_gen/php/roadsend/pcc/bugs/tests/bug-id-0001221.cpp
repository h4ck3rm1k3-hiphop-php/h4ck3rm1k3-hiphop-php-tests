
#include <php/roadsend/pcc/bugs/tests/bug-id-0001221.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001221_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001221.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001221_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("0001221 badness in the interpreter with multidimensional arrays\n\n$foo[0][0] works, but $a=0; $foo[0][$a] doesn't.\n\n");
  lval(v_foo.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(0LL, ("asdf"), 0x77CFA1EEF01BCA90LL);
  (v_a = 0LL);
  print((LINE(10,concat3("with a variable it's ", toString(v_foo.rvalAt(v_a).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "\n"))));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
