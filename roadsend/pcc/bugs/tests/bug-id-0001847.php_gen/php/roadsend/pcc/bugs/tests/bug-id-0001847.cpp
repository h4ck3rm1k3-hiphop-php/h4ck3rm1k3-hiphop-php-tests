
#include <php/roadsend/pcc/bugs/tests/bug-id-0001847.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001847.php line 10 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  Variant v_bar;

  ;
} /* function */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001847_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001847.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001847_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("bigloo type error from pcc\n\nDescription \tpcc gives the following warning:\n\nType error -- `struct' expected, `nil' provided\n\nwhen trying to compile the following snipet of code:\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
