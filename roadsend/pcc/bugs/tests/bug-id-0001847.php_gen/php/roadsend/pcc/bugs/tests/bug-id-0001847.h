
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001847_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001847_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001847.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foo();
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001847_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001847_h__
