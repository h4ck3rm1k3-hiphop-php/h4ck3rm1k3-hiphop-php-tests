
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001123_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001123_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0001123.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_test(CStrRef v_str);
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001123_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0001123_h__
