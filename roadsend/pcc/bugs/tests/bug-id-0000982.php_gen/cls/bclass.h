
#ifndef __GENERATED_cls_bclass_h__
#define __GENERATED_cls_bclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/bugs/tests/bug-id-0000982.php line 26 */
class c_bclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(bclass)
  END_CLASS_MAP(bclass)
  DECLARE_CLASS(bclass, bclass, ObjectData)
  void init();
  public: void t_bclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bclass_h__
