
#include <php/roadsend/pcc/bugs/tests/bug-id-0003230.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0003230_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0003230.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0003230_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("0003230: line numbers incorrect with multiline strings in double quotes\n");
  echo("4\n");
  echo("foo\n");
  echo("8\n");
  echo("string line 1\ntwo\nthree\nfour\nfive");
  echo("\n16\n");
  echo("string line 1\ntwo\nthree\n\n\nfour\nfive\n");
  echo("24\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
