
#include <php/roadsend/pcc/bugs/tests/bug-id-0001051.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001051.php line 5 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return (t_afunc());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return (t_afunc());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001051.php line 6 */
String c_aclass::t_afunc() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::afunc);
  return "super!!!!!!!!!";
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001051.php line 11 */
Variant c_bclass::os_get(const char *s, int64 hash) {
  return c_aclass::os_get(s, hash);
}
Variant &c_bclass::os_lval(const char *s, int64 hash) {
  return c_aclass::os_lval(s, hash);
}
void c_bclass::o_get(ArrayElementVec &props) const {
  c_aclass::o_get(props);
}
bool c_bclass::o_exists(CStrRef s, int64 hash) const {
  return c_aclass::o_exists(s, hash);
}
Variant c_bclass::o_get(CStrRef s, int64 hash) {
  return c_aclass::o_get(s, hash);
}
Variant c_bclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_aclass::o_set(s, hash, v, forInit);
}
Variant &c_bclass::o_lval(CStrRef s, int64 hash) {
  return c_aclass::o_lval(s, hash);
}
Variant c_bclass::os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
IMPLEMENT_CLASS(bclass)
ObjectData *c_bclass::cloneImpl() {
  c_bclass *obj = NEW(c_bclass)();
  cloneSet(obj);
  return obj;
}
void c_bclass::cloneSet(c_bclass *clone) {
  c_aclass::cloneSet(clone);
}
Variant c_bclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return (t_afunc());
      }
      break;
    default:
      break;
  }
  return c_aclass::o_invoke(s, params, hash, fatal);
}
Variant c_bclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x285048A47E9CBC29LL, afunc) {
        return (t_afunc());
      }
      break;
    default:
      break;
  }
  return c_aclass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_aclass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bclass$os_get(const char *s) {
  return c_bclass::os_get(s, -1);
}
Variant &cw_bclass$os_lval(const char *s) {
  return c_bclass::os_lval(s, -1);
}
Variant cw_bclass$os_constant(const char *s) {
  return c_bclass::os_constant(s);
}
Variant cw_bclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bclass::os_invoke(c, s, params, -1, fatal);
}
void c_bclass::init() {
  c_aclass::init();
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0001051.php line 12 */
String c_bclass::t_afunc() {
  INSTANCE_METHOD_INJECTION(bclass, bclass::afunc);
  String v_a;

  (v_a = LINE(14,c_aclass::t_afunc()));
  return v_a;
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Object co_bclass(CArrRef params, bool init /* = true */) {
  return Object(p_bclass(NEW(c_bclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001051_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001051.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001051_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  echo("0001051:  parse error on parent:: as rval\n");
  (v_a = ((Object)(LINE(19,p_bclass(p_bclass(NEWOBJ(c_bclass)())->create())))));
  echo(toString(LINE(20,v_a.o_invoke_few_args("afunc", 0x285048A47E9CBC29LL, 0))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
