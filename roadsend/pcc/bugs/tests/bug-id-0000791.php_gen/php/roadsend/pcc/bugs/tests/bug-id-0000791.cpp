
#include <php/roadsend/pcc/bugs/tests/bug-id-0000791.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000791_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000791.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000791_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo("0000791 switch statement parse error having to do with where the default: is\n\n");
  (v_i = 5LL);
  {
    int64 tmp2 = (toInt64(v_i));
    int tmp3 = -1;
    if (equal(tmp2, (1LL))) {
      tmp3 = 1;
    } else if (equal(tmp2, (2LL))) {
      tmp3 = 2;
    } else if (true) {
      tmp3 = 0;
    }
    switch (tmp3) {
    case 0:
      {
      }
    case 1:
      {
        echo("1\n");
      }
    case 2:
      {
        echo("2\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
