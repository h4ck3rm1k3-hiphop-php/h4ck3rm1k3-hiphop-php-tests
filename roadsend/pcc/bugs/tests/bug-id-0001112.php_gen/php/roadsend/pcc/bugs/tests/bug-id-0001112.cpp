
#include <php/roadsend/pcc/bugs/tests/bug-id-0001112.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001112_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001112.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001112_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_a = ScalarArrays::sa_[0]);
  LINE(5,x_var_dump(1, x_key(ref(v_a))));
  LINE(6,x_var_dump(1, x_array_pop(ref(v_a))));
  LINE(7,x_var_dump(1, x_key(ref(v_a))));
  LINE(8,x_var_dump(1, x_array_pop(ref(v_a))));
  LINE(9,x_var_dump(1, x_key(ref(v_a))));
  LINE(10,x_var_dump(1, x_array_pop(ref(v_a))));
  LINE(11,x_var_dump(1, x_key(ref(v_a))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
