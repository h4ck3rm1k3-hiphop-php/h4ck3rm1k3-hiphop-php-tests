
#include <php/roadsend/pcc/bugs/tests/bug-id-0001143.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001143_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001143.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001143_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(2,x_var_dump(1, x_strpos("test string", "test")));
  LINE(3,x_var_dump(1, x_strpos("test string", "string")));
  LINE(4,x_var_dump(1, x_strpos("test string", "strin")));
  LINE(5,x_var_dump(1, x_strpos("test string", "t s")));
  LINE(6,x_var_dump(1, x_strpos("test string", "g")));
  LINE(7,x_var_dump(1, (assignCallTemp(eo_0, (assignCallTemp(eo_3, x_chr(0LL)),concat3("te", eo_3, "st"))),assignCallTemp(eo_1, x_chr(0LL)),x_strpos(eo_0, eo_1))));
  LINE(8,x_var_dump(1, x_strpos("tEst", "test")));
  LINE(9,x_var_dump(1, x_strpos("teSt", "test")));
  LINE(10,x_var_dump(1, (silenceInc(), silenceDec(x_strpos("", "")))));
  LINE(11,x_var_dump(1, (silenceInc(), silenceDec(x_strpos("a", "")))));
  LINE(12,x_var_dump(1, (silenceInc(), silenceDec(x_strpos("", "a")))));
  LINE(13,x_var_dump(1, (silenceInc(), silenceDec(x_strpos("\\\\a", "\\a")))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
