
#include <php/roadsend/pcc/bugs/tests/bug-id-0000959.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 5 */
Variant c_bling::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bling::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bling::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_bling::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bling::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_bling::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bling::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bling::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bling)
ObjectData *c_bling::create() {
  init();
  t_bling();
  return this;
}
ObjectData *c_bling::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_bling::cloneImpl() {
  c_bling *obj = NEW(c_bling)();
  cloneSet(obj);
  return obj;
}
void c_bling::cloneSet(c_bling *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_bling::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0B84280E6ED02363LL, wibble) {
        return (t_wibble(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bling::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0B84280E6ED02363LL, wibble) {
        return (t_wibble(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bling::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bling$os_get(const char *s) {
  return c_bling::os_get(s, -1);
}
Variant &cw_bling$os_lval(const char *s) {
  return c_bling::os_lval(s, -1);
}
Variant cw_bling$os_constant(const char *s) {
  return c_bling::os_constant(s);
}
Variant cw_bling$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bling::os_invoke(c, s, params, -1, fatal);
}
void c_bling::init() {
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 7 */
String c_bling::t___tostring() {
  INSTANCE_METHOD_INJECTION(bling, bling::__toString);
  return "[this is class bling]";
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 10 */
void c_bling::t_bling() {
  INSTANCE_METHOD_INJECTION(bling, bling::bling);
  bool oldInCtor = gasInCtor(true);
  echo(LINE(11,concat3("bling this is ", toString(((Object)(this))), "\n")));
  echo(LINE(12,concat3("bling this->zot is ", toString(o_get("zot", 0x7A42596E78DEAC62LL)), "\n")));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 15 */
void c_bling::t_wibble() {
  INSTANCE_METHOD_INJECTION(bling, bling::wibble);
  print("bling's wibble\n");
  echo(LINE(17,concat3("bling's wibble's this: ", toString(((Object)(this))), "\n")));
  echo(LINE(18,concat3("bling's wibbles' this->zot is ", toString(o_get("zot", 0x7A42596E78DEAC62LL)), "\n")));
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 22 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_bling::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_bling::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("zot", m_zot));
  c_bling::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7A42596E78DEAC62LL, zot, 3);
      break;
    default:
      break;
  }
  return c_bling::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7A42596E78DEAC62LL, m_zot,
                         zot, 3);
      break;
    default:
      break;
  }
  return c_bling::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7A42596E78DEAC62LL, m_zot,
                      zot, 3);
      break;
    default:
      break;
  }
  return c_bling::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_bling::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_bling::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::create() {
  init();
  t_foo();
  return this;
}
ObjectData *c_foo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_zot = m_zot;
  c_bling::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0B84280E6ED02363LL, wibble) {
        return (t_wibble(), null);
      }
      break;
    default:
      break;
  }
  return c_bling::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      HASH_GUARD(0x0B84280E6ED02363LL, wibble) {
        return (t_wibble(), null);
      }
      break;
    default:
      break;
  }
  return c_bling::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_bling::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  c_bling::init();
  m_zot = 3LL;
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 25 */
String c_foo::t___tostring() {
  INSTANCE_METHOD_INJECTION(foo, foo::__toString);
  return "[this is class foo]";
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 28 */
void c_foo::t_foo() {
  INSTANCE_METHOD_INJECTION(foo, foo::foo);
  bool oldInCtor = gasInCtor(true);
  LINE(29,c_bling::t_bling());
  echo(LINE(30,concat3("this is ", toString(((Object)(this))), "\n")));
  echo(LINE(31,concat3("this->zot is ", toString(m_zot), "\n")));
  LINE(32,o_root_invoke_few_args("wibble", 0x0B84280E6ED02363LL, 0));
  LINE(33,c_bling::t_wibble());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0000959.php line 36 */
void c_foo::t_wibble() {
  INSTANCE_METHOD_INJECTION(foo, foo::wibble);
  print("foo's wibble\n");
  echo(LINE(38,concat3("foo's wibble's this: ", toString(((Object)(this))), "\n")));
  echo(LINE(39,concat3("foos's wibbles' this->zot is ", toString(m_zot), "\n")));
} /* function */
Object co_bling(CArrRef params, bool init /* = true */) {
  return Object(p_bling(NEW(c_bling)())->dynCreate(params, init));
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000959_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0000959.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0000959_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  echo("0000959 $this is null in constructor\n\n");
  (v_bar = ((Object)(LINE(44,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
