
#include <php/roadsend/pcc/bugs/tests/bug-id-0001159.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001159_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001159.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001159_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(" ");
  LINE(3,x_printf(2, "printf test 1:%s\n", ScalarArrays::sa_[0]));
  LINE(4,x_printf(2, "printf test 2:%d\n", ScalarArrays::sa_[1]));
  LINE(5,x_printf(2, "printf test 3:%f\n", ScalarArrays::sa_[2]));
  LINE(6,x_printf(2, "printf test 4:%.10f\n", ScalarArrays::sa_[2]));
  LINE(7,x_printf(2, "printf test 5:%-10.2f\n", ScalarArrays::sa_[3]));
  LINE(8,x_printf(2, "printf test 6:%-010.2f\n", ScalarArrays::sa_[3]));
  LINE(9,x_printf(2, "printf test 7:%010.2f\n", ScalarArrays::sa_[3]));
  LINE(10,x_printf(2, "printf test 8:<%20s>\n", ScalarArrays::sa_[4]));
  LINE(11,x_printf(2, "printf test 9:<%-20s>\n", ScalarArrays::sa_[5]));
  LINE(12,x_printf(1, "printf test 10: 123456789012345\n"));
  LINE(13,x_printf(2, "printf test 10:<%15s>\n", ScalarArrays::sa_[6]));
  LINE(14,x_printf(1, "printf test 11: 123456789012345678901234567890\n"));
  LINE(15,x_printf(2, "printf test 11:<%30s>\n", ScalarArrays::sa_[6]));
  LINE(16,x_printf(2, "printf test 12:%5.2f\n", ScalarArrays::sa_[7]));
  LINE(17,x_printf(2, "printf test 13:%5d\n", ScalarArrays::sa_[8]));
  LINE(18,x_printf(2, "printf test 14:%c\n", ScalarArrays::sa_[9]));
  LINE(19,x_printf(2, "printf test 15:%b\n", ScalarArrays::sa_[10]));
  LINE(20,x_printf(2, "printf test 16:%x\n", ScalarArrays::sa_[10]));
  LINE(21,x_printf(2, "printf test 17:%X\n", ScalarArrays::sa_[10]));
  LINE(22,x_printf(2, "printf test 18:%16b\n", ScalarArrays::sa_[10]));
  LINE(23,x_printf(2, "printf test 19:%16x\n", ScalarArrays::sa_[10]));
  LINE(24,x_printf(2, "printf test 20:%16X\n", ScalarArrays::sa_[10]));
  LINE(25,x_printf(2, "printf test 21:%016b\n", ScalarArrays::sa_[10]));
  LINE(26,x_printf(2, "printf test 22:%016x\n", ScalarArrays::sa_[10]));
  LINE(27,x_printf(2, "printf test 23:%016X\n", ScalarArrays::sa_[10]));
  LINE(28,x_printf(2, "printf test 24:%.5s\n", ScalarArrays::sa_[11]));
  LINE(29,x_printf(2, "printf test 25:%-2s\n", ScalarArrays::sa_[12]));
  LINE(30,x_printf(3, "printf test 26:%2$d %1$d\n", ScalarArrays::sa_[13]));
  LINE(31,x_printf(4, "printf test 27:%3$d %d %d\n", ScalarArrays::sa_[14]));
  LINE(32,x_printf(3, "printf test 28:%2$02d %1$2d\n", ScalarArrays::sa_[13]));
  LINE(33,x_printf(3, "printf test 29:%2$-2d %1$2d\n", ScalarArrays::sa_[13]));
  print("printf test 30:");
  LINE(34,x_printf(2, "%0$s", ScalarArrays::sa_[15]));
  print("x\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
