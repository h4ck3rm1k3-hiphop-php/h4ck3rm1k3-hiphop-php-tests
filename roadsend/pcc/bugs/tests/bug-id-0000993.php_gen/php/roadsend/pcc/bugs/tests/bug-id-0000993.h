
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000993_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000993_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0000993.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0000993_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_zot_DupId0(CVarRef v_a, CVarRef v_b);
void f_zot_DupId1(CVarRef v_a, CVarRef v_b);
Variant i_zot_DupId0(CArrRef params);
Variant i_zot_DupId1(CArrRef params);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0000993_h__
