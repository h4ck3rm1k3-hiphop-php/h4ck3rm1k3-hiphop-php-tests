
#ifndef __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0002040_h__
#define __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0002040_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/bugs/tests/bug-id-0002040.fw.h>

// Declarations
#include <cls/b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002040_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_a();
Variant f_b();
Variant f_c();
Variant f_d();
Object co_b(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_bugs_tests_bug_id_0002040_h__
