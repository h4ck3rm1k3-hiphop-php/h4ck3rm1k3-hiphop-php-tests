
#include <php/roadsend/pcc/bugs/tests/bug-id-0002040.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002040.php line 31 */
Variant c_b::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
}
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002040.php line 13 */
Variant f_a() {
  FUNCTION_INJECTION(a);
  Variant v_a;

  if (equal(v_a, v_a)) {
    return 2LL;
  }
  return null;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002040.php line 21 */
Variant f_b() {
  FUNCTION_INJECTION(b);
  Variant v_a;

  if (same(v_a, v_a)) {
    return 2LL;
  }
  return null;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002040.php line 35 */
Variant f_c() {
  FUNCTION_INJECTION(c);
  Variant v_b;

  if (equal(v_b, v_b)) {
    return 2LL;
  }
  return null;
} /* function */
/* SRC: roadsend/pcc/bugs/tests/bug-id-0002040.php line 43 */
Variant f_d() {
  FUNCTION_INJECTION(d);
  Variant v_b;

  if (same(v_b, v_b)) {
    return 2LL;
  }
  return null;
} /* function */
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0002040_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0002040.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0002040_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("0002040: using == or === on recursive objects and arrays causes a segfault \n\nthe @'s are because in zend php these comparisons cause a fatal error.  we\njust want to make sure that pcc doesn't crash -- it should reach the \"done\".\nI don't see why a comparison of recursive data should be a \"fatal error\".\n--tim\n\n");
  v_a.set(2LL, (ref(v_a)), 0x486AFCC090D5F98CLL);
  (silenceInc(), silenceDec(LINE(19,f_a())));
  (silenceInc(), silenceDec(LINE(27,f_b())));
  (v_b = ((Object)(LINE(32,p_b(p_b(NEWOBJ(c_b)())->create())))));
  (v_b.o_lval("foo", 0x4154FA2EF733DA8FLL) = ref(v_b));
  (silenceInc(), silenceDec(LINE(41,f_c())));
  (silenceInc(), silenceDec(LINE(49,f_d())));
  echo("done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
