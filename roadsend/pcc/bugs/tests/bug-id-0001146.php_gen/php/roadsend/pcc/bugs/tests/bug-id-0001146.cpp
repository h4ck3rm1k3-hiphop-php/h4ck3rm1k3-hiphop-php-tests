
#include <php/roadsend/pcc/bugs/tests/bug-id-0001146.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$bugs$tests$bug_id_0001146_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/bugs/tests/bug-id-0001146.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$bugs$tests$bug_id_0001146_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_sample_urls __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sample_urls") : g->GV(sample_urls);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_url __attribute__((__unused__)) = (variables != gVariables) ? variables->get("url") : g->GV(url);

  (v_sample_urls = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_sample_urls.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_url = iter3->second();
      v_i = iter3->first();
      {
        echo(LINE(72,concat5("parsing ", toString(v_i), ": <", toString(v_url), ">\n")));
        LINE(73,x_var_dump(1, (silenceInc(), silenceDec(x_parse_url(toString(v_url))))));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
