
#ifndef __GENERATED_php_roadsend_pcc_webconnect_tests_auth_nophp_h__
#define __GENERATED_php_roadsend_pcc_webconnect_tests_auth_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/webconnect/tests/auth.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$webconnect$tests$auth(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_webconnect_tests_auth_nophp_h__
