
#include <php/roadsend/pcc/webconnect/tests/httpupload/upload.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$webconnect$tests$httpupload$upload_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/webconnect/tests/httpupload/upload.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$webconnect$tests$httpupload$upload_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("test file uploads\n\n<!-- The data encoding type, enctype, MUST be specified as below -->\n<form enctype=\"multipart/form-data\" action=\"receive.php\" method=\"POST\">\n    <!-- MAX_FILE_SIZE must precede the file input field -->\n    <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"30000\" />\n    <!-- Name of input element determines name in $_FILES array -->\n    Send this file: <input name=\"userfile\" type=\"file\"");
  echo(" />\n    <input type=\"submit\" value=\"Send File\" />\n</form>\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
