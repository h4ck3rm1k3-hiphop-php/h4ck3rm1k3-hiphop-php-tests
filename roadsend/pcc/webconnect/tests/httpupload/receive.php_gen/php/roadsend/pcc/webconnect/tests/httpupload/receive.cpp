
#include <php/roadsend/pcc/webconnect/tests/httpupload/receive.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$webconnect$tests$httpupload$receive_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/webconnect/tests/httpupload/receive.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$webconnect$tests$httpupload$receive_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_uploaddir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("uploaddir") : g->GV(uploaddir);
  Variant &v_uploadfile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("uploadfile") : g->GV(uploadfile);

  (v_uploaddir = "/tmp/");
  (v_uploadfile = concat(toString(v_uploaddir), LINE(6,x_basename(toString(g->gv__FILES.rvalAt("userfile", 0x1CF0630024CFE902LL).rvalAt("name", 0x0BCDB293DC3CBDDCLL))))));
  echo("<pre>");
  if (LINE(9,x_move_uploaded_file(toString(g->gv__FILES.rvalAt("userfile", 0x1CF0630024CFE902LL).rvalAt("tmp_name", 0x2F7127AF7D0115EALL)), toString(v_uploadfile)))) {
    echo("File is valid, and was successfully uploaded.\n");
  }
  else {
    echo("Possible file upload attack!\n");
  }
  echo("Here is some more debugging info:");
  LINE(16,x_print_r(g->gv__FILES));
  print("</pre>");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
