
#ifndef __GENERATED_php_roadsend_pcc_doc_glade_tutorial_nophp_h__
#define __GENERATED_php_roadsend_pcc_doc_glade_tutorial_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/doc/glade-tutorial.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$doc$glade_tutorial(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_doc_glade_tutorial_nophp_h__
