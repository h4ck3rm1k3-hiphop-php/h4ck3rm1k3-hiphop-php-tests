
#ifndef __GENERATED_cls_filedialog_h__
#define __GENERATED_cls_filedialog_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/doc/glade-tutorial/mp3-manager.php line 63 */
class c_filedialog : virtual public ObjectData {
  BEGIN_CLASS_MAP(filedialog)
  END_CLASS_MAP(filedialog)
  DECLARE_CLASS(filedialog, FileDialog, ObjectData)
  void init();
  public: Variant m_fs;
  public: Variant m_field;
  public: void t_filedialog(Variant v_filenameField);
  public: ObjectData *create(Variant v_filenameField);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_show();
  public: void t_ok();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_filedialog_h__
