
#ifndef __GENERATED_php_roadsend_pcc_doc_glade_tutorial_mp3_manager_h__
#define __GENERATED_php_roadsend_pcc_doc_glade_tutorial_mp3_manager_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/doc/glade-tutorial/mp3-manager.fw.h>

// Declarations
#include <cls/songlist.h>
#include <cls/filedialog.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_addsong(CVarRef v_widget, Variant v_form, Variant v_window);
Variant pm_php$roadsend$pcc$doc$glade_tutorial$mp3_manager_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_handlenewsong(CVarRef v_widget);
void f_alert(CStrRef v_message);
Object co_songlist(CArrRef params, bool init = true);
Object co_filedialog(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_doc_glade_tutorial_mp3_manager_h__
