
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$pcc$doc$examples$cl_project$main_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$pcc$doc$examples$cl_project$inc1_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$pcc$doc$examples$cl_project$inc2_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_INCLUDE(0x7D559C5D72519519LL, "roadsend/pcc/doc/examples/cl-project/main.php", php$roadsend$pcc$doc$examples$cl_project$main_php);
      break;
    case 5:
      HASH_INCLUDE(0x758BE3DF09F57E6DLL, "roadsend/pcc/doc/examples/cl-project/inc1.php", php$roadsend$pcc$doc$examples$cl_project$inc1_php);
      break;
    case 6:
      HASH_INCLUDE(0x3EC5F8F851CAD74ELL, "roadsend/pcc/doc/examples/cl-project/inc2.php", php$roadsend$pcc$doc$examples$cl_project$inc2_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
