
#ifndef __GENERATED_php_roadsend_pcc_doc_examples_cl_project_inc1_h__
#define __GENERATED_php_roadsend_pcc_doc_examples_cl_project_inc1_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/doc/examples/cl-project/inc1.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$doc$examples$cl_project$inc1_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_inc1_function(CStrRef v_a, CStrRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_doc_examples_cl_project_inc1_h__
