
#ifndef __GENERATED_php_roadsend_pcc_doc_examples_cl_project_main_h__
#define __GENERATED_php_roadsend_pcc_doc_examples_cl_project_main_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/doc/examples/cl-project/main.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$doc$examples$cl_project$main_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_doc_examples_cl_project_main_h__
