
#ifndef __GENERATED_php_roadsend_pcc_doc_examples_cl_project_inc2_h__
#define __GENERATED_php_roadsend_pcc_doc_examples_cl_project_inc2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/doc/examples/cl-project/inc2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$doc$examples$cl_project$inc2_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_inc2_function(CVarRef v_a, CVarRef v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_doc_examples_cl_project_inc2_h__
