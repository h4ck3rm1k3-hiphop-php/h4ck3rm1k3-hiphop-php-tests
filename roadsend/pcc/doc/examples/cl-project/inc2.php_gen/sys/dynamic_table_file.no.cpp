
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$pcc$doc$examples$cl_project$inc2_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_INCLUDE(0x3EC5F8F851CAD74ELL, "roadsend/pcc/doc/examples/cl-project/inc2.php", php$roadsend$pcc$doc$examples$cl_project$inc2_php);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$roadsend$pcc$doc$examples$cl_project$inc2_php(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
