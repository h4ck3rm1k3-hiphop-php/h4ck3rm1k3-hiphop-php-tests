
#include <php/roadsend/pcc/benchmarks/tests/explode.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$benchmarks$tests$explode_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/explode.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$explode_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo("explode() is important for a lot of templating systems\n");
  (v_test = toString("root:x:0:0:root:/root:/bin/bash\ndaemon:x:1:1:daemon:/usr/sbin:/bin/sh\nbin:x:2:2:bin:/bin:/bin/sh\nsys:x:3:3:sys:/dev:/bin/sh\nsync:x:4:65534:sync:/bin:/bin/sync\ngames:x:5:60:games:/usr/games:/bin/sh\nman:x:6:12:man:/var/cache/man:/bin/sh\nlp:x:7:7:lp:/var/spool/lpd:/bin/sh\nmail:x:8:8:mail:/var/mail:/bin/sh\nnews:x:9:9:news:/var/spool/news:/bin/sh\nuucp:x:10:10:uucp:/var/spool/uucp:/bin/sh\nproxy:x:13:13:proxy:/bin:/bin/sh\nwww-data:x:33:33:www-data:/var/www:/bin/sh\nbackup:x:34:34:backup:/var/backups:/bin/sh\nlist:x:38:38:Mailing List Manager:/var/list:/bin/sh\nirc:x:39:39:ircd:/var/run/ircd:/bin/sh\ngnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/bin/sh\nnobody:x:65534:65534:nobody:/nonexistent:/bin/sh\nDebian-exim:x:102:102::/var/spool/exim4:/bin/false\ntim:x:1000:1000:tim,,,:/home/tim:/bin/bash\nidentd:x:100:65534::/var/run/identd:/bin/false\nsshd:x:101:65534::/var/run/sshd:/bin/false\nbind:x:103:104::/var/cache/bind:/bin/false\nmessagebus:x:104:105::/var/run/dbus:/bin/false\ngdm:x:105:106:Gnome Display Manager:/var/lib/gdm:/bin/false\nhal:x:108:108:Hardware abstraction layer,,,:/var/run/hal:/bin/false\nsaned:x:109:109::/home/saned:/bin/false\nmysql:x:106:112:MySQL Server,,,:/var/lib/mysql:/bin/false\ntestuser:x:1001:1001:,,,:/home/testuser:/bin/bash\ndistccd:x:107:65534::/:/bin/false\npostgres:x:110:113:PostgreSQL administrator,,,:/var/lib/postgres:/bin/bash"));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        if (!equal(LINE(38,x_count(x_explode(":", toString(v_test)))), 187LL)) {
          f_exit("error!");
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
