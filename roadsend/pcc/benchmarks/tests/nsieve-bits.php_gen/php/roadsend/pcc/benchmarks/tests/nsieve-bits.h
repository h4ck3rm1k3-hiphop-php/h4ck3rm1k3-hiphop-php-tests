
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_nsieve_bits_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_nsieve_bits_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/nsieve-bits.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$nsieve_bits_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_primes(CVarRef v_size);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_nsieve_bits_h__
