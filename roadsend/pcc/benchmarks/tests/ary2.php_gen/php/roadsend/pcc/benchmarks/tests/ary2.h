
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_ary2_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_ary2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/ary2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$ary2_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_ary2(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_ary2_h__
