
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_gabriel_stak_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_gabriel_stak_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/gabriel-stak.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_stak_aux();
Variant pm_php$roadsend$pcc$benchmarks$tests$gabriel_stak_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_stak(int64 v_x1, int64 v_y1, int64 v_z1);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_gabriel_stak_h__
