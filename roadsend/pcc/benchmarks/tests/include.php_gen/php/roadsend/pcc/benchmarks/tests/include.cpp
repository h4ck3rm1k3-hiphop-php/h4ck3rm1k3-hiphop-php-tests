
#include <php/roadsend/pcc/benchmarks/tests/include.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/include.php line 9 */
Variant c_sm_codeplate::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_codeplate::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_codeplate::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_sm_codeplate::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_codeplate::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_codeplate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_codeplate::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_codeplate::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_codeplate)
ObjectData *c_sm_codeplate::cloneImpl() {
  c_sm_codeplate *obj = NEW(c_sm_codeplate)();
  cloneSet(obj);
  return obj;
}
void c_sm_codeplate::cloneSet(c_sm_codeplate *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_sm_codeplate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_codeplate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_codeplate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_codeplate$os_get(const char *s) {
  return c_sm_codeplate::os_get(s, -1);
}
Variant &cw_sm_codeplate$os_lval(const char *s) {
  return c_sm_codeplate::os_lval(s, -1);
}
Variant cw_sm_codeplate$os_constant(const char *s) {
  return c_sm_codeplate::os_constant(s);
}
Variant cw_sm_codeplate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_codeplate::os_invoke(c, s, params, -1, fatal);
}
void c_sm_codeplate::init() {
}
/* SRC: roadsend/pcc/benchmarks/tests/include.php line 6 */
Variant c_sm_module::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_module::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_module::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_sm_module::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_module::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_module::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_module::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_module::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_module)
ObjectData *c_sm_module::cloneImpl() {
  c_sm_module *obj = NEW(c_sm_module)();
  cloneSet(obj);
  return obj;
}
void c_sm_module::cloneSet(c_sm_module *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_sm_module::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_module::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_module::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_module$os_get(const char *s) {
  return c_sm_module::os_get(s, -1);
}
Variant &cw_sm_module$os_lval(const char *s) {
  return c_sm_module::os_lval(s, -1);
}
Variant cw_sm_module$os_constant(const char *s) {
  return c_sm_module::os_constant(s);
}
Variant cw_sm_module$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_module::os_invoke(c, s, params, -1, fatal);
}
void c_sm_module::init() {
}
/* SRC: roadsend/pcc/benchmarks/tests/include.php line 3 */
Variant c_sm_smtag::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_sm_smtag::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_sm_smtag::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_sm_smtag::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_sm_smtag::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_sm_smtag::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_sm_smtag::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_sm_smtag::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(sm_smtag)
ObjectData *c_sm_smtag::cloneImpl() {
  c_sm_smtag *obj = NEW(c_sm_smtag)();
  cloneSet(obj);
  return obj;
}
void c_sm_smtag::cloneSet(c_sm_smtag *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_sm_smtag::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_sm_smtag::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_sm_smtag::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_sm_smtag$os_get(const char *s) {
  return c_sm_smtag::os_get(s, -1);
}
Variant &cw_sm_smtag$os_lval(const char *s) {
  return c_sm_smtag::os_lval(s, -1);
}
Variant cw_sm_smtag$os_constant(const char *s) {
  return c_sm_smtag::os_constant(s);
}
Variant cw_sm_smtag$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_sm_smtag::os_invoke(c, s, params, -1, fatal);
}
void c_sm_smtag::init() {
}
Object co_sm_codeplate(CArrRef params, bool init /* = true */) {
  return Object(p_sm_codeplate(NEW(c_sm_codeplate)())->dynCreate(params, init));
}
Object co_sm_module(CArrRef params, bool init /* = true */) {
  return Object(p_sm_module(NEW(c_sm_module)())->dynCreate(params, init));
}
Object co_sm_smtag(CArrRef params, bool init /* = true */) {
  return Object(p_sm_smtag(NEW(c_sm_smtag)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$benchmarks$tests$include_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/include.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$include_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_includedir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("includedir") : g->GV(includedir);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);

  (v_includedir = concat(toString(g->gv__ENV.rvalAt("PCC_HOME", 0x4F7B58EF1F3DF563LL)), "/benchmarks/data/include/"));
  LINE(14,include((concat(toString(v_includedir), "/inc.php")), true, variables, "roadsend/pcc/benchmarks/tests/"));
  LINE(15,include((concat(toString(v_includedir), "/finc.php")), true, variables, "roadsend/pcc/benchmarks/tests/"));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(18,include((concat(toString(v_includedir), concat3("/inc", toString(v_i), ".php"))), true, variables, "roadsend/pcc/benchmarks/tests/"));
        echo("\n");
        LINE(20,include((concat(toString(v_includedir), concat3("/finc", toString(v_i), ".php"))), true, variables, "roadsend/pcc/benchmarks/tests/"));
        echo("\n");
        v_i++;
      }
    } while (not_more(v_i, 20LL));
  }
  unset(v_i);
  {
    LOOP_COUNTER(2);
    do {
      LOOP_COUNTER_CHECK(2);
      {
        if (isset(v_j)) (v_j = 0LL);
        {
          LOOP_COUNTER(3);
          do {
            LOOP_COUNTER_CHECK(3);
            {
              LINE(29,include((concat(toString(v_includedir), concat4("/main", toString(v_i), toString(v_j), ".cpt"))), true, variables, "roadsend/pcc/benchmarks/tests/"));
              echo(LINE(30,concat4(toString(v_i), ", ", toString(v_j), "\n")));
              v_j++;
              if (!(isset(v_i))) {
                echo("isset");
                (v_i = 0LL);
              }
            }
          } while (less(v_j, 10LL));
        }
        v_i++;
      }
    } while (less(v_i, 10LL));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
