
#include <php/roadsend/pcc/benchmarks/tests/gabriel-takl.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$benchmarks$tests$gabriel_takl_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/gabriel-takl.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$gabriel_takl_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("From \"Performance and Evaluation of Lisp Systems\" by Richard Gabriel \npg. 114\n\n\"TAKL is very much like TAK, but it does not perform any explicit\narithmetic.\"\n\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
