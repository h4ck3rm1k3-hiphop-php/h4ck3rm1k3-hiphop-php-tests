
#include <php/roadsend/pcc/benchmarks/tests/btrees.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/btrees.php line 23 */
Variant f_itemcheck(Variant v_treeNode) {
  FUNCTION_INJECTION(itemCheck);
  Variant v_check;

  (v_check = 0LL);
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        v_check += v_treeNode.rvalAt(2LL, 0x486AFCC090D5F98CLL);
        if (equal(null, v_treeNode.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) {
          return v_check;
        }
        v_check -= LINE(33,f_itemcheck(v_treeNode.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
        (v_treeNode = v_treeNode.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
      }
    } while (true);
  }
  return null;
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/btrees.php line 8 */
Array f_bottomuptree(CVarRef v_item, Variant v_depth) {
  FUNCTION_INJECTION(bottomUpTree);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int64 v_newItem = 0;

  if (toBoolean(v_depth)) {
    --v_depth;
    (v_newItem = toInt64(toInt64(v_item)) << 1LL);
    return (assignCallTemp(eo_0, LINE(15,f_bottomuptree(v_newItem - 1LL, v_depth))),assignCallTemp(eo_1, LINE(16,f_bottomuptree(v_newItem, v_depth))),assignCallTemp(eo_2, v_item),Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, eo_2).create()));
  }
  return Array(ArrayInit(3).set(0, null).set(1, null).set(2, v_item).create());
} /* function */
Variant pm_php$roadsend$pcc$benchmarks$tests$btrees_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/btrees.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$btrees_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_minDepth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("minDepth") : g->GV(minDepth);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_maxDepth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("maxDepth") : g->GV(maxDepth);
  Variant &v_stretchDepth __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stretchDepth") : g->GV(stretchDepth);
  Variant &v_stretchTree __attribute__((__unused__)) = (variables != gVariables) ? variables->get("stretchTree") : g->GV(stretchTree);
  Variant &v_longLivedTree __attribute__((__unused__)) = (variables != gVariables) ? variables->get("longLivedTree") : g->GV(longLivedTree);
  Variant &v_iterations __attribute__((__unused__)) = (variables != gVariables) ? variables->get("iterations") : g->GV(iterations);
  Variant &v_check __attribute__((__unused__)) = (variables != gVariables) ? variables->get("check") : g->GV(check);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);

  (v_minDepth = 4LL);
  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(10LL)));
  (v_maxDepth = LINE(42,x_max(2, v_minDepth + 2LL, Array(ArrayInit(1).set(0, v_n).create()))));
  (v_stretchDepth = v_maxDepth + 1LL);
  (v_stretchTree = LINE(45,f_bottomuptree(0LL, v_stretchDepth)));
  LINE(47,(assignCallTemp(eo_1, v_stretchDepth),assignCallTemp(eo_2, f_itemcheck(v_stretchTree)),x_printf(3, "stretch tree of depth %d\t check: %d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  unset(v_stretchTree);
  (v_longLivedTree = LINE(50,f_bottomuptree(0LL, v_maxDepth)));
  (v_iterations = toInt64(1LL) << toInt64((toInt64(v_maxDepth))));
  {
    LOOP_COUNTER(2);
    do {
      LOOP_COUNTER_CHECK(2);
      {
        (v_check = 0LL);
        {
          LOOP_COUNTER(3);
          for ((v_i = 1LL); not_more(v_i, v_iterations); ++v_i) {
            LOOP_COUNTER_CHECK(3);
            {
              (v_t = LINE(58,f_bottomuptree(v_i, v_minDepth)));
              v_check += LINE(59,f_itemcheck(v_t));
              unset(v_t);
              (v_t = LINE(61,f_bottomuptree(negate(v_i), v_minDepth)));
              v_check += LINE(62,f_itemcheck(v_t));
              unset(v_t);
            }
          }
        }
        LINE(67,x_printf(4, "%d\t trees of depth %d\t check: %d\n", Array(ArrayInit(3).set(0, toInt64(toInt64(v_iterations)) << 1LL).set(1, v_minDepth).set(2, v_check).create())));
        v_minDepth += 2LL;
        v_iterations >>= 2LL;
      }
    } while (not_more(v_minDepth, v_maxDepth));
  }
  LINE(75,(assignCallTemp(eo_1, v_maxDepth),assignCallTemp(eo_2, f_itemcheck(v_longLivedTree)),x_printf(3, "long lived tree of depth %d\t check: %d\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
