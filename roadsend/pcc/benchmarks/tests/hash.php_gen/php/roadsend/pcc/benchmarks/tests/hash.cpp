
#include <php/roadsend/pcc/benchmarks/tests/hash.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/hash.php line 11 */
void f_myhash(CVarRef v_n) {
  FUNCTION_INJECTION(myhash);
  Variant v_i;
  Sequence v_X;
  Variant v_c;

  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); not_more(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_X.set(LINE(14,x_dechex(toInt64(v_i))), (v_i));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = v_n); more(v_i, 0LL); v_i--) {
      LOOP_COUNTER_CHECK(2);
      {
        if (toBoolean(v_X.rvalAt(v_i))) {
          v_c++;
        }
      }
    }
  }
  print(toString(v_c) + toString("\n"));
} /* function */
Variant pm_php$roadsend$pcc$benchmarks$tests$hash_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/hash.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$hash_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(100000LL)));
  LINE(9,f_myhash(v_n));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
