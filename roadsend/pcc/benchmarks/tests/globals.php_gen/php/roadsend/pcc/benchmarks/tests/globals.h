
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_globals_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_globals_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/globals.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$globals_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_increment_k2(Variant v_var);
void f_increment_global_k();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_globals_h__
