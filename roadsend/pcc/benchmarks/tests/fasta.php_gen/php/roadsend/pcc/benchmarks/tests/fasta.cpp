
#include <php/roadsend/pcc/benchmarks/tests/fasta.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_LINE_LENGTH = 60LL;
const int64 k_IA = 3877LL;
const int64 k_IC = 29573LL;
const int64 k_IM = 139968LL;

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/fasta.php line 22 */
void f_makecumulative(Variant v_genelist) {
  FUNCTION_INJECTION(makeCumulative);
  int v_count = 0;
  int64 v_i = 0;

  (v_count = LINE(23,x_count(v_genelist)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); less(v_i, v_count); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        lval(lval(v_genelist.lvalAt(v_i)).lvalAt(1LL, 0x5BCA7C69B794F8CELL)) += v_genelist.rvalAt(v_i - 1LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL);
      }
    }
  }
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/fasta.php line 30 */
Variant f_selectrandom(Variant v_a) {
  FUNCTION_INJECTION(selectRandom);
  Numeric v_r = 0;
  int v_hi = 0;
  int64 v_i = 0;

  (v_r = LINE(31,f_gen_random(1LL)));
  (v_hi = LINE(32,x_sizeof(v_a)));
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, v_hi); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        if (less(v_r, v_a.rvalAt(v_i).rvalAt(1LL, 0x5BCA7C69B794F8CELL))) return v_a.rvalAt(v_i).rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
      }
    }
  }
  return v_a.rvalAt(v_hi - 1LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/fasta.php line 15 */
Numeric f_gen_random(int64 v_max) {
  FUNCTION_INJECTION(gen_random);
  DECLARE_GLOBAL_VARIABLES(g);
  Numeric &sv_last __attribute__((__unused__)) = g->sv_gen_random_DupIdlast;
  bool &inited_sv_last __attribute__((__unused__)) = g->inited_sv_gen_random_DupIdlast;
  if (!inited_sv_last) {
    (sv_last = 42LL);
    inited_sv_last = true;
  }
  return divide(v_max * ((sv_last = modulo((toInt64(sv_last * 3877LL /* IA */ + 29573LL /* IC */)), 139968LL /* IM */))), 139968LL /* IM */);
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/fasta.php line 58 */
void f_makerepeatfasta(CStrRef v_id, CStrRef v_desc, CVarRef v_s, Numeric v_n) {
  FUNCTION_INJECTION(makeRepeatFasta);
  Numeric v_i = 0;
  int v_sLength = 0;
  Numeric v_lineLength = 0;

  echo(LINE(59,concat5(">", v_id, " ", v_desc, "\n")));
  (v_i = 0LL);
  (v_sLength = LINE(60,x_strlen(toString(v_s))));
  (v_lineLength = 60LL /* LINE_LENGTH */);
  LOOP_COUNTER(3);
  {
    while (more(v_n, 0LL)) {
      LOOP_COUNTER_CHECK(3);
      {
        if (less(v_n, v_lineLength)) (v_lineLength = v_n);
        if (less(v_i + v_lineLength, v_sLength)) {
          print((toString(LINE(64,x_substr(toString(v_s), toInt32(v_i), toInt32(v_lineLength))))));
          print("\n");
          v_i += v_lineLength;
        }
        else {
          print((toString(LINE(67,x_substr(toString(v_s), toInt32(v_i))))));
          (v_i = v_lineLength - (v_sLength - v_i));
          print((toString(LINE(69,x_substr(toString(v_s), toInt32(0LL), toInt32(v_i))))));
          print("\n");
        }
        v_n -= v_lineLength;
      }
    }
  }
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/fasta.php line 45 */
void f_makerandomfasta(CStrRef v_id, CStrRef v_desc, Variant v_genelist, Numeric v_n) {
  FUNCTION_INJECTION(makeRandomFasta);
  Numeric v_todo = 0;
  String v_pick;
  Variant v_m;
  int64 v_i = 0;

  print((LINE(46,concat5(">", v_id, " ", v_desc, "\n"))));
  {
    LOOP_COUNTER(4);
    for ((v_todo = v_n); more(v_todo, 0LL); v_todo -= 60LL /* LINE_LENGTH */) {
      LOOP_COUNTER_CHECK(4);
      {
        (v_pick = "");
        (v_m = less(v_todo, 60LL /* LINE_LENGTH */) ? ((Variant)(v_todo)) : ((Variant)(60LL /* LINE_LENGTH */)));
        {
          LOOP_COUNTER(5);
          for ((v_i = 0LL); less(v_i, v_m); v_i++) {
            LOOP_COUNTER_CHECK(5);
            concat_assign(v_pick, toString(LINE(51,f_selectrandom(ref(v_genelist)))));
          }
        }
        concat_assign(v_pick, "\n");
        print((v_pick));
      }
    }
  }
} /* function */
Variant pm_php$roadsend$pcc$benchmarks$tests$fasta_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/fasta.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$fasta_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_iub __attribute__((__unused__)) = (variables != gVariables) ? variables->get("iub") : g->GV(iub);
  Variant &v_homosapiens __attribute__((__unused__)) = (variables != gVariables) ? variables->get("homosapiens") : g->GV(homosapiens);
  Variant &v_alu __attribute__((__unused__)) = (variables != gVariables) ? variables->get("alu") : g->GV(alu);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  ;
  ;
  ;
  ;
  (v_iub = ScalarArrays::sa_[0]);
  (v_homosapiens = ScalarArrays::sa_[1]);
  (v_alu = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGGGAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGACCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAATACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCAGCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGGAGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCCAGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA");
  (v_n = 1000LL);
  if (more(g->gv__SERVER.rvalAt("argc", 0x596A642EB89EED13LL), 1LL)) (v_n = g->gv__SERVER.rvalAt("argv", 0x10EA7DC57768F8C6LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  LINE(117,f_makecumulative(ref(v_iub)));
  LINE(118,f_makecumulative(ref(v_homosapiens)));
  LINE(120,f_makerepeatfasta("ONE", "Homo sapiens alu", v_alu, v_n * 2LL));
  LINE(121,f_makerandomfasta("TWO", "IUB ambiguity codes", ref(v_iub), v_n * 3LL));
  LINE(122,f_makerandomfasta("THREE", "Homo sapiens frequency", ref(v_homosapiens), v_n * 5LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
