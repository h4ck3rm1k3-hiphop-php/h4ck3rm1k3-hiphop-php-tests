
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_num_test_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_num_test_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/num-test.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foo();
Variant pm_php$roadsend$pcc$benchmarks$tests$num_test_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_num_test_h__
