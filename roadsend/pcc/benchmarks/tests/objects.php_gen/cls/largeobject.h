
#ifndef __GENERATED_cls_largeobject_h__
#define __GENERATED_cls_largeobject_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/tests/objects.php line 18 */
class c_largeobject : virtual public ObjectData {
  BEGIN_CLASS_MAP(largeobject)
  END_CLASS_MAP(largeobject)
  DECLARE_CLASS(largeobject, largeobject, ObjectData)
  void init();
  public: String m_var1;
  public: String m_var2;
  public: String m_var3;
  public: String m_var4;
  public: String m_var5;
  public: String m_var6;
  public: String m_var7;
  public: String m_var8;
  public: String m_var9;
  public: String m_var10;
  public: String m_var11;
  public: String m_var12;
  public: String m_var13;
  public: String m_var14;
  public: String m_var15;
  public: String m_var16;
  public: String m_var17;
  public: String m_var18;
  public: String m_var19;
  public: String m_var20;
  public: String m_var21;
  public: String m_var22;
  public: String m_var23;
  public: String m_var24;
  public: String m_var25;
  public: String m_var26;
  public: String m_var27;
  public: String m_var28;
  public: String m_var29;
  public: String m_var30;
  public: String m_var31;
  public: String m_var32;
  public: String m_var33;
  public: String m_var34;
  public: String m_var35;
  public: String m_var36;
  public: String m_var37;
  public: String m_var38;
  public: String m_var39;
  public: String m_var40;
  public: void t_largeobject();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_afunc11(CVarRef v_a);
  public: void t_afunc12(CVarRef v_a);
  public: void t_afunc13(CVarRef v_a);
  public: void t_afunc14(CVarRef v_a);
  public: void t_afunc15(CVarRef v_a);
  public: void t_afunc16(CVarRef v_a);
  public: void t_afunc17(CVarRef v_a);
  public: void t_afunc18(CVarRef v_a);
  public: void t_afunc19(CVarRef v_a);
  public: void t_afunc20(CVarRef v_a);
  public: void t_afunc21(CVarRef v_a);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_largeobject_h__
