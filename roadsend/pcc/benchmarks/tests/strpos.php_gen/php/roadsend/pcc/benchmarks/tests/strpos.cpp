
#include <php/roadsend/pcc/benchmarks/tests/strpos.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$benchmarks$tests$strpos_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/strpos.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$strpos_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_small __attribute__((__unused__)) = (variables != gVariables) ? variables->get("small") : g->GV(small);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_big __attribute__((__unused__)) = (variables != gVariables) ? variables->get("big") : g->GV(big);

  (v_small = "ABCDEFGHIJKLMNOPQRSTUVQXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}");
  {
    LOOP_COUNTER(1);
    for ((v_i = 65LL); less(v_i, 126LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(LINE(5,(assignCallTemp(eo_0, toString(v_small)),assignCallTemp(eo_1, x_chr(toInt64(v_i))),x_strpos(eo_0, eo_1)))));
      }
    }
  }
  (v_big = toString("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam orci. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec nisl. Aenean vitae sapien. Maecenas consequat dui quis turpis. Sed convallis est ac tellus. Duis tristique. Nulla vitae neque sit amet mauris fringilla luctus. Aliquam nonummy consectetuer augue. Etiam dictum tristique diam. Fusce vel urna. Integer vitae tellus. Etiam semper dui sed nulla. Morbi pellentesque. Quisque pretium, nisi sit amet pretium dapibus, ante dolor sollicitudin tellus, a dapibus sapien orci eget lectus. Sed suscipit, ligula eget ultricies cursus, nisi libero consequat massa, vel tempus dui justo id diam. Proin suscipit sapien vel elit.\n\nQuisque pharetra, tellus eget ornare lobortis, mauris ligula aliquet neque, sit amet laoreet leo purus eu lacus. Cras rutrum sapien ac augue. Curabitur tempor. In vestibulum, pede nec rutrum viverra, augue purus ultricies mauris, eget euismod nulla odio ornare felis. Morbi sodales mollis eros. Vivamus arcu lacus, interdum nec, sollicitudin at, tempor nec, urna. In sit amet magna. Vivamus nisi. Nulla diam leo, vehicula nec, adipiscing id, interdum non, quam. Curabitur suscipit libero non dolor. Vestibulum id lacus. Maecenas porta. Nunc semper varius pede. Duis luctus risus a nisi. Nunc non justo ultrices elit gravida mollis. Suspendisse tincidunt condimentum ante. Aenean eget sapien.\n\nSuspendisse ut est at magna adipiscing commodo. Vestibulum fermentum commodo mauris. Vivamus pulvinar ligula quis nulla. Quisque adipiscing elit at sem auctor aliquet. Donec tellus erat, aliquam vel, gravida vel, dapibus vitae, quam. Mauris ornare. Praesent scelerisque dolor non diam. Vestibulum vel ante non metus ullamcorper dictum. In nisl neque, ultrices sodales, aliquam tempor, commodo at, massa. Aenean rutrum orci non est. Mauris hendrerit. In mollis, pede vel euismod interdum, velit metus sodales urna, sit amet elementum risus pede vel eros. In condimentum adipiscing pede. Sed odio. Curabitur adipiscing odio in dolor. Maecenas fermentum mi sed enim. Aenean ipsum. Cras semper molestie odio.\n\nNulla in felis. Duis malesuada libero vestibulum justo. Maecenas egestas lorem quis tellus. Phasellus turpis pede, posuere sit amet, sagittis sollicitudin, faucibus vitae, velit. Curabitur at libero ut diam placerat feugiat. Fusce sed lacus. Maecenas metus enim, luctus non, placerat ut, ornare nec, tellus. Pellentesque ac magna eget elit suscipit auctor. Duis mattis. Ut sapien velit, ullamcorper congue, molestie et, accumsan ut, quam. Curabitur hendrerit.\n\nAenean pellentesque, metus a condimentum feugiat, nibh arcu tristique risus, sed bibendum magna augue at justo. Proin laoreet vestibulum sapien. Phasellus congue, libero nec fringilla consequat, ligula lorem bibendum purus, a consectetuer ligula diam et justo. Morbi id urna eget ipsum mattis dignissim. In elit. Donec gravida quam et purus. Praesent rhoncus sem quis tortor varius ornare. Fusce quis felis non leo porta sollicitudin. Donec suscipit urna. Vestibulum ornare ante nec nisl. In consectetuer libero nec sem. Aenean felis justo, pretium ut, facilisis a, consectetuer in, eros. Nulla semper semper eros. Maecenas sed nunc dignissim enim consequat rutrum.\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam orci. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec nisl. Aenean vitae sapien. Maecenas consequat dui quis turpis. Sed convallis est ac tellus. Duis tristique. Nulla vitae neque sit amet mauris fringilla luctus. Aliquam nonummy consectetuer augue. Etiam dictum tristique diam. Fusce vel urna. Integer vitae tellus. Etiam semper dui sed nulla. Morbi pellentesque. Quisque pretium, nisi sit amet pretium dapibus, ante dolor sollicitudin tellus, a dapibus sapien orci eget lectus. Sed suscipit, ligula eget ultricies cursus, nisi libero consequat massa, vel tempus dui justo id diam. Proin suscipit sapien vel elit.\n\nQuisque pharetra, tellus eget ornare lobortis, mauris ligula aliquet neque, sit amet laoreet leo purus eu lacus. Cras rutrum sapien ac augue. Curabitur tempor. In vestibulum, pede nec rutrum viverra, augue purus ultricies mauris, eget euismod nulla odio ornare felis. Morbi sodales mollis eros. Vivamus arcu lacus, interdum nec, sollicitudin at, tempor nec, urna. In sit amet magna. Vivamus nisi. Nulla diam leo, vehicula nec, adipiscing id, interdum non, quam. Curabitur suscipit libero non dolor. Vestibulum id lacus. Maecenas porta. Nunc semper varius pede. Duis luctus risus a nisi. Nunc non justo ultrices elit gravida mollis. Suspendisse tincidunt condimentum ante. Aenean eget sapien.\n\nSuspendisse ut est at magna adipiscing commodo. Vestibulum fermentum commodo mauris. Vivamus pulvinar ligula quis nulla. Quisque adipiscing elit at sem auctor aliquet. Donec tellus erat, aliquam vel, gravida vel, dapibus vitae, quam. Mauris ornare. Praesent scelerisque dolor non diam. Vestibulum vel ante non metus ullamcorper dictum. In nisl neque, ultrices sodales, aliquam tempor, commodo at, massa. Aenean rutrum orci non est. Mauris hendrerit. In mollis, pede vel euismod interdum, velit metus sodales urna, sit amet elementum risus pede vel eros. In condimentum adipiscing pede. Sed odio. Curabitur adipiscing odio in dolor. Maecenas fermentum mi sed enim. Aenean ipsum. Cras semper molestie odio.\n\nNulla in felis. Duis malesuada libero vestibulum justo. Maecenas egestas lorem quis tellus. Phasellus turpis pede, posuere sit amet, sagittis sollicitudin, faucibus vitae, velit. Curabitur at libero ut diam placerat feugiat. Fusce sed lacus. Maecenas metus enim, luctus non, placerat ut, ornare nec, tellus. Pellentesque ac magna eget elit suscipit auctor. Duis mattis. Ut sapien velit, ullamcorper congue, molestie et, accumsan ut, quam. Curabitur hendrerit.\n\nAenean pellentesque, metus a condimentum feugiat, nibh arcu tristique risus, sed bibendum magna augue at justo. Proin laoreet vestibulum sapien. Phasellus congue, libero nec fringilla consequat, ligula lorem bibendum purus, a consectetuer ligula diam et justo. Morbi id urna eget ipsum mattis dignissim. In elit. Donec gravida quam et purus. Praesent rhoncus sem quis tortor varius ornare. Fusce quis felis non leo porta sollicitudin. Donec suscipit urna. Vestibulum ornare ante nec nisl. In consectetuer libero nec sem. Aenean felis justo, pretium ut, facilisis a, consectetuer in, eros. Nulla semper semper eros. Maecenas sed nunc dignissim enim consequat rutrum."));
  echo(toString(v_big));
  echo(toString(LINE(30,x_strpos(toString(v_big), "Duis"))));
  echo(toString(LINE(31,x_strpos(toString(v_big), "metus"))));
  echo(toString(LINE(32,x_strpos(toString(v_big), "fermentum"))));
  {
    LOOP_COUNTER(2);
    for ((v_i = 0LL); less(v_i, LINE(34,x_strlen(toString(v_big)))); v_i += 5LL) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(toString(LINE(35,x_strpos(toString(v_big), "sit", toInt32(v_i)))));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
