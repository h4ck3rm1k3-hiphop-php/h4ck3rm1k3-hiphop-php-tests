
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "complex", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  "complex_abs", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  "complex_add", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  "complex_multiply", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  "imag_part", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  "julia_ppm", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  "ppm_write", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  "real_part", "roadsend/pcc/benchmarks/tests/julia-ppm.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
