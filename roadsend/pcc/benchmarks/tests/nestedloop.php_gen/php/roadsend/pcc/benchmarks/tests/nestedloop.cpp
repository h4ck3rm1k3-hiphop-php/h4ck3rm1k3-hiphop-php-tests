
#include <php/roadsend/pcc/benchmarks/tests/nestedloop.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/nestedloop.php line 8 */
void f_nestedloop(CVarRef v_n) {
  FUNCTION_INJECTION(nestedloop);
  int64 v_x = 0;
  int64 v_a = 0;
  int64 v_b = 0;
  int64 v_f = 0;

  (v_x = 0LL);
  {
    LOOP_COUNTER(1);
    for ((v_a = 0LL); less(v_a, v_n); v_a++) {
      LOOP_COUNTER_CHECK(1);
      {
        LOOP_COUNTER(2);
        for ((v_b = 0LL); less(v_b, v_n); v_b++) {
          LOOP_COUNTER_CHECK(2);
          {
            LOOP_COUNTER(3);
            for ((v_f = 0LL); less(v_f, v_n); v_f++) {
              LOOP_COUNTER_CHECK(3);
              v_x++;
            }
          }
        }
      }
    }
  }
  print(toString(v_x) + toString("\n"));
} /* function */
Variant pm_php$roadsend$pcc$benchmarks$tests$nestedloop_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/nestedloop.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$nestedloop_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(100LL)));
  LINE(7,f_nestedloop(v_n));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
