
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_nestedloop_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_nestedloop_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/nestedloop.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$nestedloop_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_nestedloop(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_nestedloop_h__
