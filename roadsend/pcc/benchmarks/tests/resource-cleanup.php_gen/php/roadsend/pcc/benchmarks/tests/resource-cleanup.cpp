
#include <php/roadsend/pcc/benchmarks/tests/resource-cleanup.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$benchmarks$tests$resource_cleanup_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/resource-cleanup.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$resource_cleanup_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);

  LOOP_COUNTER(1);
  {
    while (less(v_i++, 65536LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_fp = LINE(3,x_fopen(get_source_filename("roadsend/pcc/benchmarks/tests/resource-cleanup.php"), "r")));
        if (!(toBoolean(v_fp))) {
          f_exit("oops");
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
