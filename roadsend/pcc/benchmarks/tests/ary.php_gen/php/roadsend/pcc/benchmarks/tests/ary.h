
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_ary_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_ary_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/ary.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_ary(CVarRef v_n);
Variant pm_php$roadsend$pcc$benchmarks$tests$ary_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_ary_h__
