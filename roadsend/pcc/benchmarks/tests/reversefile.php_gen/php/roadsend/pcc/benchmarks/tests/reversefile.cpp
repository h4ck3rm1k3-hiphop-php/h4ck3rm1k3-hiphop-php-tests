
#include <php/roadsend/pcc/benchmarks/tests/reversefile.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$benchmarks$tests$reversefile_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/reversefile.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$reversefile_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_datafile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("datafile") : g->GV(datafile);
  Variant &v_fd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fd") : g->GV(fd);
  Variant &v_lines __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lines") : g->GV(lines);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);

  (v_datafile = concat(toString(g->gv__ENV.rvalAt("PCC_HOME", 0x4F7B58EF1F3DF563LL)), "/benchmarks/data/spellcheck"));
  if (!(LINE(8,x_file_exists(toString(v_datafile))))) {
    LINE(9,(assignCallTemp(eo_0, concat3("Couldn't find datafile ", toString(v_datafile), ".")),x_trigger_error(eo_0, toInt32(256LL) /* E_USER_ERROR */)));
  }
  (v_fd = LINE(11,x_fopen(toString(v_datafile), "r")));
  (v_lines = ScalarArrays::sa_[0]);
  LOOP_COUNTER(1);
  {
    while (!(LINE(13,x_feof(toObject(v_fd))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (assignCallTemp(eo_0, ref(v_lines)),assignCallTemp(eo_1, x_fgets(toObject(v_fd), 4096LL)),x_array_push(2, eo_0, eo_1));
      }
    }
  }
  LINE(14,x_fclose(toObject(v_fd)));
  {
    LOOP_COUNTER(2);
    Variant map3 = LINE(15,x_array_reverse(v_lines));
    for (ArrayIterPtr iter4 = map3.begin(); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_line = iter4->second();
      print(toString(v_line));
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
