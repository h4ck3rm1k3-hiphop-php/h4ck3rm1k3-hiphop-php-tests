
#include <php/roadsend/pcc/benchmarks/tests/preg_replace.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$benchmarks$tests$preg_replace_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/preg_replace.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$preg_replace_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_target __attribute__((__unused__)) = (variables != gVariables) ? variables->get("target") : g->GV(target);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_target = "foobarfoobarfoo");
  concat_assign(v_target, toString(v_target));
  concat_assign(v_target, toString(v_target));
  concat_assign(v_target, toString(v_target));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 1000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(11,x_preg_replace("/foo/", "baz", v_target));
      }
    }
  }
  print(toString(LINE(14,x_preg_replace("/foo/", "baz", v_target))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
