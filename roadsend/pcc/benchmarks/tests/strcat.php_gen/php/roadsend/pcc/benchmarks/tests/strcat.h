
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_strcat_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_strcat_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/strcat.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$strcat_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_afunc();
void f_mystrcat(Variant v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_strcat_h__
