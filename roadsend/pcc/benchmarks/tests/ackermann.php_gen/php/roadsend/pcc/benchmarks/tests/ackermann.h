
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_ackermann_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_ackermann_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/ackermann.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$ackermann_php(bool incOnce = false, LVariableTable* variables = NULL);
Numeric f_ack(Numeric v_m, CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_ackermann_h__
