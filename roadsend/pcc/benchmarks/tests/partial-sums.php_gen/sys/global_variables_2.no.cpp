
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 7:
      HASH_INITIALIZED(0x68DF81F26D942FC7LL, g->GV(a1),
                       a1);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x117B8667E4662809LL, g->GV(n),
                       n);
      break;
    case 13:
      HASH_INITIALIZED(0x43EDA7BEE714570DLL, g->GV(a3),
                       a3);
      HASH_INITIALIZED(0x437D9F169312FE4DLL, g->GV(k2),
                       k2);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 15:
      HASH_INITIALIZED(0x0D6857FDDD7F21CFLL, g->GV(k),
                       k);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      HASH_INITIALIZED(0x6E47C085C7F241D3LL, g->GV(sk),
                       sk);
      break;
    case 24:
      HASH_INITIALIZED(0x180CCF01FBC81F18LL, g->GV(a7),
                       a7);
      break;
    case 25:
      HASH_INITIALIZED(0x1DC175F7E4343259LL, g->GV(a4),
                       a4);
      break;
    case 26:
      HASH_INITIALIZED(0x7C60E6EC5B67C29ALL, g->GV(k3),
                       k3);
      break;
    case 27:
      HASH_INITIALIZED(0x2AB21AF4F5B8471BLL, g->GV(alt),
                       alt);
      break;
    case 31:
      HASH_INITIALIZED(0x0EFAF509EE3FE5DFLL, g->GV(twothirds),
                       twothirds);
      break;
    case 32:
      HASH_INITIALIZED(0x4555E94E338480E0LL, g->GV(a5),
                       a5);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 41:
      HASH_INITIALIZED(0x598AD62830EAF1E9LL, g->GV(a6),
                       a6);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 47:
      HASH_INITIALIZED(0x36551E10CAD29DEFLL, g->GV(ck),
                       ck);
      break;
    case 52:
      HASH_INITIALIZED(0x00FF5C07497953F4LL, g->GV(a8),
                       a8);
      break;
    case 54:
      HASH_INITIALIZED(0x6C05C2858C72A876LL, g->GV(a2),
                       a2);
      break;
    case 55:
      HASH_INITIALIZED(0x3A3C288F3DA61B77LL, g->GV(a9),
                       a9);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
