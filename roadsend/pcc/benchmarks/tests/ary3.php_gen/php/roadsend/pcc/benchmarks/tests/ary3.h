
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_ary3_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_ary3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/ary3.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$ary3_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_ary3(CVarRef v_n);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_ary3_h__
