
#include <php/roadsend/pcc/benchmarks/tests/ary3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/ary3.php line 10 */
void f_ary3(CVarRef v_n) {
  FUNCTION_INJECTION(ary3);
  Numeric v_i = 0;
  Sequence v_X;
  int64 v_k = 0;
  Sequence v_Y;
  Numeric v_last = 0;

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_X.set(v_i, (v_i + 1LL));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_k = 0LL); less(v_k, 1000LL); v_k++) {
      LOOP_COUNTER_CHECK(2);
      {
        {
          LOOP_COUNTER(3);
          for ((v_i = v_n - 1LL); not_less(v_i, 0LL); v_i--) {
            LOOP_COUNTER_CHECK(3);
            {
              lval(v_Y.lvalAt(v_i)) += v_X.rvalAt(v_i);
            }
          }
        }
      }
    }
  }
  (v_last = v_n - 1LL);
  print(LINE(20,concat4(toString(v_Y.rvalAt(0, 0x77CFA1EEF01BCA90LL)), " ", toString(v_Y.rvalAt(v_last)), "\n")));
} /* function */
Variant pm_php$roadsend$pcc$benchmarks$tests$ary3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/ary3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$ary3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(1000LL)));
  LINE(8,f_ary3(v_n));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
