
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_fannkuch_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_fannkuch_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/fannkuch.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_fannkuch(CVarRef v_n);
Variant pm_php$roadsend$pcc$benchmarks$tests$fannkuch_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_fannkuch_h__
