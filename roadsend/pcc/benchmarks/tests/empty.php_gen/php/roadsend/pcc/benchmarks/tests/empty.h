
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_empty_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_empty_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/empty.fw.h>

// Declarations
#include <cls/o.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$empty_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_o(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_empty_h__
