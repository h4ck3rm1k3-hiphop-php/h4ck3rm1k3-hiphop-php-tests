
#include <php/roadsend/pcc/benchmarks/tests/empty.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/empty.php line 3 */
Variant c_o::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_o::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_o::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("v1", m_v1));
  c_ObjectData::o_get(props);
}
bool c_o::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x079A01435AA36382LL, v1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_o::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x079A01435AA36382LL, m_v1,
                         v1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_o::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x079A01435AA36382LL, m_v1,
                      v1, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_o::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_o::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(o)
ObjectData *c_o::cloneImpl() {
  c_o *obj = NEW(c_o)();
  cloneSet(obj);
  return obj;
}
void c_o::cloneSet(c_o *clone) {
  clone->m_v1 = m_v1;
  ObjectData::cloneSet(clone);
}
Variant c_o::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_o::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_o::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_o$os_get(const char *s) {
  return c_o::os_get(s, -1);
}
Variant &cw_o$os_lval(const char *s) {
  return c_o::os_lval(s, -1);
}
Variant cw_o$os_constant(const char *s) {
  return c_o::os_constant(s);
}
Variant cw_o$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_o::os_invoke(c, s, params, -1, fatal);
}
void c_o::init() {
  m_v1 = null;
}
Object co_o(CArrRef params, bool init /* = true */) {
  return Object(p_o(NEW(c_o)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$benchmarks$tests$empty_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/empty.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$empty_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_o = ((Object)(LINE(7,p_o(p_o(NEWOBJ(c_o)())->create())))));
  (v_a = Array(ArrayInit(13).set(0, "s1", "some string", 0x0B875164E69FC819LL).set(1, "s2", "", 0x1F1A885E46B787D2LL).set(2, "s3", "0", 0x510176132E65358ELL).set(3, "s3", "555", 0x510176132E65358ELL).set(4, "b1", false, 0x29C390281340176BLL).set(5, "b2", true, 0x2D5185583EF85E98LL).set(6, "i1", 0LL, 0x608D572C3F34DB68LL).set(7, "i2", 555LL, 0x6014C6271D255F8BLL).set(8, "n1", null, 0x4FB33C0307D52C46LL).set(9, "a1", ScalarArrays::sa_[0], 0x68DF81F26D942FC7LL).set(10, "a2", ScalarArrays::sa_[1], 0x6C05C2858C72A876LL).set(11, "o1", v_o, 0x4877B8D09F78456BLL).set(12, "o2", v_o.o_get("v1", 0x079A01435AA36382LL), 0x55D1F13047B68DAALL).create()));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 100000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          LOOP_COUNTER(2);
          for (ArrayIterPtr iter4 = v_a.begin(); !iter4->end(); iter4->next()) {
            LOOP_COUNTER_CHECK(2);
            v_v = iter4->second();
            {
              (v_foo = empty(v_v));
            }
          }
        }
      }
    }
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
