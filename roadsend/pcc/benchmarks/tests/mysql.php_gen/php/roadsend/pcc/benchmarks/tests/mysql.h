
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_tests_mysql_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_tests_mysql_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/tests/mysql.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$tests$mysql_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_domysql();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_tests_mysql_h__
