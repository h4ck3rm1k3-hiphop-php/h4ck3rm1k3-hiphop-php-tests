
#include <php/roadsend/pcc/benchmarks/tests/multi-dimensional.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/multi-dimensional.php line 15 */
void f_test_global_x() {
  FUNCTION_INJECTION(test_global_x);
  DECLARE_GLOBAL_VARIABLES(g);
  echo(concat(toString(g->GV(globules).rvalAt("x", 0x04BFC205E59FA416LL).rvalAt("k", 0x0D6857FDDD7F21CFLL).rvalAt("y", 0x4F56B733A4DFC78ALL).rvalAt("alpha", 0x193DEC4B3E25D217LL)), "\n\n"));
} /* function */
Variant pm_php$roadsend$pcc$benchmarks$tests$multi_dimensional_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/multi-dimensional.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$multi_dimensional_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_globules __attribute__((__unused__)) = (variables != gVariables) ? variables->get("globules") : g->GV(globules);

  echo("This benchmark was contributed by \"BDKR\"\n");
  lval(lval(lval(v_globules.lvalAt("x", 0x04BFC205E59FA416LL)).lvalAt("k", 0x0D6857FDDD7F21CFLL)).lvalAt("y", 0x4F56B733A4DFC78ALL)).set("alpha", (0LL), 0x193DEC4B3E25D217LL);
  LOOP_COUNTER(1);
  {
    while (less(v_globules.rvalAt("x", 0x04BFC205E59FA416LL).rvalAt("k", 0x0D6857FDDD7F21CFLL).rvalAt("y", 0x4F56B733A4DFC78ALL).rvalAt("alpha", 0x193DEC4B3E25D217LL), 1000000LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        ++lval(lval(lval(lval(v_globules.lvalAt("x", 0x04BFC205E59FA416LL)).lvalAt("k", 0x0D6857FDDD7F21CFLL)).lvalAt("y", 0x4F56B733A4DFC78ALL)).lvalAt("alpha", 0x193DEC4B3E25D217LL));
      }
    }
  }
  LINE(10,f_test_global_x());
  lval(lval(lval(v_globules.lvalAt("x", 0x04BFC205E59FA416LL)).lvalAt("k", 0x0D6857FDDD7F21CFLL)).lvalAt("y", 0x4F56B733A4DFC78ALL)).set("alpha", ("For a later function"), 0x193DEC4B3E25D217LL);
  LINE(12,f_test_global_x());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
