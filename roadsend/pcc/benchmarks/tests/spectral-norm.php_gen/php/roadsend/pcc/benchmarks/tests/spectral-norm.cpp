
#include <php/roadsend/pcc/benchmarks/tests/spectral-norm.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/benchmarks/tests/spectral-norm.php line 14 */
Variant f_av(CVarRef v_n, CVarRef v_v) {
  FUNCTION_INJECTION(Av);
  Variant v_Av;
  int64 v_i = 0;
  int64 v_j = 0;

  (v_Av = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        v_Av.set(v_i, (0LL));
        {
          LOOP_COUNTER(2);
          for ((v_j = 0LL); less(v_j, v_n); v_j++) {
            LOOP_COUNTER_CHECK(2);
            lval(v_Av.lvalAt(v_i)) += LINE(18,f_a(v_i, v_j)) * v_v.rvalAt(v_j);
          }
        }
      }
    }
  }
  return v_Av;
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/spectral-norm.php line 23 */
Variant f_atv(CVarRef v_n, CVarRef v_v) {
  FUNCTION_INJECTION(Atv);
  Variant v_Atv;
  int64 v_i = 0;
  int64 v_j = 0;

  (v_Atv = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(3);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(3);
      {
        v_Atv.set(v_i, (0LL));
        {
          LOOP_COUNTER(4);
          for ((v_j = 0LL); less(v_j, v_n); v_j++) {
            LOOP_COUNTER_CHECK(4);
            lval(v_Atv.lvalAt(v_i)) += LINE(27,f_a(v_j, v_i)) * v_v.rvalAt(v_j);
          }
        }
      }
    }
  }
  return v_Atv;
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/spectral-norm.php line 10 */
Numeric f_a(int64 v_i, int64 v_j) {
  FUNCTION_INJECTION(A);
  return divide(1.0, (divide((v_i + v_j) * (v_i + v_j + 1LL), 2LL) + v_i + 1LL));
} /* function */
/* SRC: roadsend/pcc/benchmarks/tests/spectral-norm.php line 32 */
Variant f_atav(CVarRef v_n, CVarRef v_v) {
  FUNCTION_INJECTION(AtAv);
  Variant eo_0;
  Variant eo_1;
  return LINE(33,(assignCallTemp(eo_0, v_n),assignCallTemp(eo_1, f_av(v_n, v_v)),f_atv(eo_0, eo_1)));
} /* function */
Variant pm_php$roadsend$pcc$benchmarks$tests$spectral_norm_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/benchmarks/tests/spectral-norm.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$benchmarks$tests$spectral_norm_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_u __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u") : g->GV(u);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_vBv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("vBv") : g->GV(vBv);
  Variant &v_vv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("vv") : g->GV(vv);

  (v_n = (equal(v_argc, 2LL)) ? ((Variant)(v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)(50LL)));
  (v_u = LINE(38,x_array_pad(ScalarArrays::sa_[0], toInt32(v_n), 1LL)));
  {
    LOOP_COUNTER(5);
    for ((v_i = 0LL); less(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(5);
      {
        (v_v = LINE(41,f_atav(v_n, v_u)));
        (v_u = LINE(42,f_atav(v_n, v_v)));
      }
    }
  }
  (v_vBv = 0LL);
  (v_vv = 0LL);
  {
    LOOP_COUNTER(6);
    for ((v_i = 0LL); less(v_i, v_n); v_i++) {
      LOOP_COUNTER_CHECK(6);
      {
        v_vBv += v_u.rvalAt(v_i) * v_v.rvalAt(v_i);
        v_vv += v_v.rvalAt(v_i) * v_v.rvalAt(v_i);
      }
    }
  }
  LINE(51,(assignCallTemp(eo_1, x_sqrt(toDouble(divide(v_vBv, v_vv)))),x_printf(2, "%0.9f\n", Array(ArrayInit(1).set(0, eo_1).create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
