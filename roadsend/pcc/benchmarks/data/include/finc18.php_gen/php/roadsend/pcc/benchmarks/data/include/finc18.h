
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc18_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc18_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/data/include/finc18.fw.h>

// Declarations
#include <cls/sm_smtag_arear.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$data$include$finc18_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_arear(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc18_h__
