
#ifndef __GENERATED_cls_tfunctionmenu_h__
#define __GENERATED_cls_tfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc7.php line 22 */
class c_tfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(tfunctionmenu)
  END_CLASS_MAP(tfunctionmenu)
  DECLARE_CLASS(tfunctionmenu, tFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_tfunctionmenu_h__
