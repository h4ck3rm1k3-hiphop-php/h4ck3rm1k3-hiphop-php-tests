
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc7_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc7_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/data/include/finc7.fw.h>

// Declarations
#include <cls/sm_smtag_areag.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$data$include$finc7_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_areag(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc7_h__
