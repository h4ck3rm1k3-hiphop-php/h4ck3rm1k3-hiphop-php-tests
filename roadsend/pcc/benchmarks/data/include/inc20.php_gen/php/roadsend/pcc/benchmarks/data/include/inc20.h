
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc20_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc20_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/data/include/inc20.fw.h>

// Declarations
#include <cls/gfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$data$include$inc20_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_gfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc20_h__
