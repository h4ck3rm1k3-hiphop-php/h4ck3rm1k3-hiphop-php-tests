
#ifndef __GENERATED_cls_cfunctionmenu_h__
#define __GENERATED_cls_cfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc16.php line 22 */
class c_cfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(cfunctionmenu)
  END_CLASS_MAP(cfunctionmenu)
  DECLARE_CLASS(cfunctionmenu, cFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cfunctionmenu_h__
