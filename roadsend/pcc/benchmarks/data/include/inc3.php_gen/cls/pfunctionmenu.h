
#ifndef __GENERATED_cls_pfunctionmenu_h__
#define __GENERATED_cls_pfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc3.php line 22 */
class c_pfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(pfunctionmenu)
  END_CLASS_MAP(pfunctionmenu)
  DECLARE_CLASS(pfunctionmenu, pFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pfunctionmenu_h__
