
#ifndef __GENERATED_cls_sm_smtag_arean_h__
#define __GENERATED_cls_sm_smtag_arean_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/finc14.php line 23 */
class c_sm_smtag_arean : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_arean)
  END_CLASS_MAP(sm_smtag_arean)
  DECLARE_CLASS(sm_smtag_arean, SM_smTag_AREAn, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_arean_h__
