
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc19_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc19_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/data/include/finc19.fw.h>

// Declarations
#include <cls/sm_smtag_areas.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$data$include$finc19_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_areas(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc19_h__
