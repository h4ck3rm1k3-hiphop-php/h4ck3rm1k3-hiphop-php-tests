
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc3_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/data/include/finc3.fw.h>

// Declarations
#include <cls/sm_smtag_areac.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$data$include$finc3_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_sm_smtag_areac(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_data_include_finc3_h__
