
#ifndef __GENERATED_cls_sm_smtag_aream_h__
#define __GENERATED_cls_sm_smtag_aream_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/finc13.php line 23 */
class c_sm_smtag_aream : virtual public ObjectData {
  BEGIN_CLASS_MAP(sm_smtag_aream)
  END_CLASS_MAP(sm_smtag_aream)
  DECLARE_CLASS(sm_smtag_aream, SM_smTag_AREAm, sm_smtag)
  void init();
  public: Variant m_areaName;
  public: Variant m_itemList;
  public: void t_additem(Variant v_data);
  public: void t_tagreset();
  public: void t_tagconfig();
  public: void t_tagthink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sm_smtag_aream_h__
