
#ifndef __GENERATED_cls_bfunctionmenu_h__
#define __GENERATED_cls_bfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc15.php line 22 */
class c_bfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(bfunctionmenu)
  END_CLASS_MAP(bfunctionmenu)
  DECLARE_CLASS(bfunctionmenu, bFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bfunctionmenu_h__
