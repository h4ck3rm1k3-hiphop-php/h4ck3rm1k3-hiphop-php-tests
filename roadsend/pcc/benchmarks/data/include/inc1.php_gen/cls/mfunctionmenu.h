
#ifndef __GENERATED_cls_mfunctionmenu_h__
#define __GENERATED_cls_mfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc1.php line 22 */
class c_mfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(mfunctionmenu)
  END_CLASS_MAP(mfunctionmenu)
  DECLARE_CLASS(mfunctionmenu, mFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mfunctionmenu_h__
