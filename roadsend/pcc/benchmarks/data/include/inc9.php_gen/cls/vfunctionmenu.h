
#ifndef __GENERATED_cls_vfunctionmenu_h__
#define __GENERATED_cls_vfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc9.php line 22 */
class c_vfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(vfunctionmenu)
  END_CLASS_MAP(vfunctionmenu)
  DECLARE_CLASS(vfunctionmenu, vFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_vfunctionmenu_h__
