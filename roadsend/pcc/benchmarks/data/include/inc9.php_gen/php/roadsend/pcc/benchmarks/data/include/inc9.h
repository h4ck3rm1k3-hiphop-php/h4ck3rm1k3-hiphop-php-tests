
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc9_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc9_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/data/include/inc9.fw.h>

// Declarations
#include <cls/vfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$data$include$inc9_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_vfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc9_h__
