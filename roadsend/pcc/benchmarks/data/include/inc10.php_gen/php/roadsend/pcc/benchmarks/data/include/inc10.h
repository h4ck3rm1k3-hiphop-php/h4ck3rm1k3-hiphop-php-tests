
#ifndef __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc10_h__
#define __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc10_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/benchmarks/data/include/inc10.fw.h>

// Declarations
#include <cls/wfunctionmenu.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$benchmarks$data$include$inc10_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_wfunctionmenu(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_benchmarks_data_include_inc10_h__
