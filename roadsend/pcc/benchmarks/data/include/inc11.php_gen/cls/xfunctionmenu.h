
#ifndef __GENERATED_cls_xfunctionmenu_h__
#define __GENERATED_cls_xfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc11.php line 22 */
class c_xfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(xfunctionmenu)
  END_CLASS_MAP(xfunctionmenu)
  DECLARE_CLASS(xfunctionmenu, xFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_xfunctionmenu_h__
