
#ifndef __GENERATED_cls_qfunctionmenu_h__
#define __GENERATED_cls_qfunctionmenu_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/benchmarks/data/include/inc4.php line 22 */
class c_qfunctionmenu : virtual public ObjectData {
  BEGIN_CLASS_MAP(qfunctionmenu)
  END_CLASS_MAP(qfunctionmenu)
  DECLARE_CLASS(qfunctionmenu, qFunctionMenu, sm_module)
  void init();
  public: void t_moduleconfig();
  public: void t_modulethink();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_qfunctionmenu_h__
