
#include <php/roadsend/pcc/sa-tests/run-tests.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 405 */
Variant f_run_test(Variant v_php, CVarRef v_file) {
  FUNCTION_INJECTION(run_test);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_log_format __attribute__((__unused__)) = g->GV(log_format);
  Variant &gv_info_params __attribute__((__unused__)) = g->GV(info_params);
  Variant &gv_ini_overwrites __attribute__((__unused__)) = g->GV(ini_overwrites);
  Variant &gv_toDelete __attribute__((__unused__)) = g->GV(toDelete);
  Variant v_section_text;
  Variant v_fp;
  Variant v_section;
  Variant v_line;
  Variant v_r;
  Variant v_old_php;
  Variant v_shortname;
  String v_tested;
  Variant v_tmp;
  String v_tmp_skipif;
  String v_tmp_file;
  String v_tmp_post;
  String v_info;
  bool v_warn = false;
  Variant v_extra;
  Variant v_output;
  Variant v_reason;
  Variant v_ini_settings;
  String v_query_string;
  Variant v_args;
  Variant v_pccargs;
  String v_post;
  int v_content_length = 0;
  String v_cmd;
  String v_preCompile;
  String v_out;
  String v_postCompile;
  Variant &gv_cwd __attribute__((__unused__)) = g->GV(cwd);
  Variant v_rtexpect;
  String v_rtcmd;
  String v_preRun;
  String v_rtout;
  String v_type;
  Variant v_pos;
  Variant v_wanted;
  Variant v_wanted_re;
  bool v_ok = false;
  String v_logname;
  Variant v_log;

  {
  }
  if (toBoolean(get_global_variables()->k_DETAILED)) echo(LINE(413,concat3("\n=================\nTEST ", toString(v_file), "\n")));
  (v_section_text = ScalarArrays::sa_[5]);
  (toBoolean((v_fp = (silenceInc(), silenceDec(LINE(425,x_fopen(toString(v_file), "r"))))))) || ((f_error(toString("Cannot open test file: ") + toString(v_file)), null));
  (v_section = "");
  LOOP_COUNTER(1);
  {
    while (!(LINE(428,x_feof(toObject(v_fp))))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_line = LINE(429,x_fgets(toObject(v_fp))));
        if (toBoolean(LINE(432,x_ereg("^--([A-Z]+)--", toString(v_line), ref(v_r))))) {
          (v_section = v_r.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          v_section_text.set(v_section, (""));
          continue;
        }
        concat_assign(lval(v_section_text.lvalAt(v_section)), toString(v_line));
      }
    }
  }
  LINE(441,x_fclose(toObject(v_fp)));
  if (equal(LINE(444,x_getenv("ZEND_COMPAT")), 1LL)) {
    v_section_text.set("RTEXPECT", (v_section_text.rvalAt("EXPECT", 0x36CC00F3D4F35017LL)), 0x33B8AEC669BD3922LL);
    v_section_text.set("EXPECT", (""), 0x36CC00F3D4F35017LL);
  }
  if ((!(empty(v_section_text, "GET", 0x25DCCC35D69AD828LL)) || !(empty(v_section_text, "POST", 0x73E3781F017E3A02LL)))) {
    if (LINE(451,x_file_exists("./sapi/cgi/php"))) {
      (v_old_php = v_php);
      (v_php = concat(toString(LINE(453,x_realpath("./sapi/cgi/php"))), " -C "));
    }
  }
  (v_shortname = LINE(457,x_str_replace(concat(toString(g->GV(cwd)), "/"), "", v_file)));
  (v_tested = concat_rev(LINE(458,concat3(" [", toString(v_shortname), "]")), x_trim(toString(v_section_text.rvalAt("TEST", 0x37349B25A0ED29E7LL)))));
  (v_tmp = LINE(460,x_realpath(x_dirname(toString(v_file)))));
  (v_tmp_skipif = concat(toString(v_tmp), LINE(461,x_uniqid("/phpt."))));
  (v_tmp_file = LINE(462,x_ereg_replace("\\.phpt$", ".php", toString(v_file))));
  (v_tmp_post = concat(toString(v_tmp), LINE(463,x_uniqid("/phpt."))));
  (silenceInc(), silenceDec(LINE(465,x_unlink(v_tmp_skipif))));
  (silenceInc(), silenceDec(LINE(466,x_unlink(v_tmp_file))));
  (silenceInc(), silenceDec(LINE(467,x_unlink(v_tmp_post))));
  (silenceInc(), silenceDec(LINE(470,x_unlink(x_ereg_replace("\\.phpt$", ".exp", toString(v_file))))));
  (silenceInc(), silenceDec(LINE(471,x_unlink(x_ereg_replace("\\.phpt$", ".out", toString(v_file))))));
  (silenceInc(), silenceDec(LINE(472,x_unlink(x_ereg_replace("\\.phpt$", ".rtout", toString(v_file))))));
  (silenceInc(), silenceDec(LINE(473,x_unlink(x_ereg_replace("\\.phpt$", ".rtexp", toString(v_file))))));
  LINE(476,x_putenv("REDIRECT_STATUS="));
  LINE(477,x_putenv("QUERY_STRING="));
  LINE(478,x_putenv("PATH_TRANSLATED="));
  LINE(479,x_putenv("SCRIPT_FILENAME="));
  LINE(480,x_putenv("REQUEST_METHOD="));
  LINE(481,x_putenv("CONTENT_TYPE="));
  LINE(482,x_putenv("CONTENT_LENGTH="));
  (v_info = "");
  (v_warn = false);
  if (LINE(487,x_array_key_exists("SKIPIF", v_section_text))) {
    if (toBoolean(LINE(488,x_trim(toString(v_section_text.rvalAt("SKIPIF", 0x027C39A55E059F80LL)))))) {
      LINE(489,f_save_text(v_tmp_skipif, v_section_text.rvalAt("SKIPIF", 0x027C39A55E059F80LL)));
      (v_extra = !same(LINE(490,x_substr("Linux" /* PHP_OS */, toInt32(0LL), toInt32(3LL))), "WIN") ? (("unset REQUEST_METHOD;")) : (("")));
      (v_output = f_shell_exec(toString(v_extra) + toString(" ") + toString(v_php) + toString(" ") + toString(gv_info_params) + toString(" -f ") + v_tmp_skipif));
      (silenceInc(), silenceDec(LINE(494,x_unlink(v_tmp_skipif))));
      if (toBoolean(LINE(495,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_eregi("^skip", eo_1))))) {
        echo(toString("SKIP ") + v_tested);
        (v_reason = (toBoolean(LINE(497,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_eregi("^skip[[:space:]]*(.+)$", eo_1))))) ? ((Variant)((assignCallTemp(eo_2, x_trim(toString(v_output))),x_eregi_replace("^skip[[:space:]]*(.+)$", "\\1", eo_2)))) : ((Variant)(false)));
        if (toBoolean(v_reason)) {
          echo(LINE(499,concat3(" (reason: ", toString(v_reason), ")\n")));
        }
        else {
          echo("\n");
        }
        if (isset(v_old_php)) {
          (v_php = v_old_php);
        }
        return "SKIPPED";
      }
      if (toBoolean(LINE(508,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_eregi("^info", eo_1))))) {
        (v_reason = (toBoolean(LINE(509,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_ereg("^info[[:space:]]*(.+)$", eo_1))))) ? ((Variant)((assignCallTemp(eo_2, x_trim(toString(v_output))),x_ereg_replace("^info[[:space:]]*(.+)$", "\\1", eo_2)))) : ((Variant)(false)));
        if (toBoolean(v_reason)) {
          (v_info = LINE(511,concat3(" (info: ", toString(v_reason), ")")));
        }
      }
      if (toBoolean(LINE(514,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_eregi("^warn", eo_1))))) {
        (v_reason = (toBoolean(LINE(515,(assignCallTemp(eo_1, x_trim(toString(v_output))),x_ereg("^warn[[:space:]]*(.+)$", eo_1))))) ? ((Variant)((assignCallTemp(eo_2, x_trim(toString(v_output))),x_ereg_replace("^warn[[:space:]]*(.+)$", "\\1", eo_2)))) : ((Variant)(false)));
        if (toBoolean(v_reason)) {
          (v_warn = true);
          (v_info = LINE(518,concat3(" (warn: ", toString(v_reason), ")")));
        }
      }
    }
  }
  (v_ini_settings = ScalarArrays::sa_[0]);
  LINE(528,f_settings2array(gv_ini_overwrites, ref(v_ini_settings)));
  if (LINE(532,x_array_key_exists("INI", v_section_text))) {
    LINE(533,(assignCallTemp(eo_0, x_preg_split("/[\n\r]+/", v_section_text.rvalAt("INI", 0x1B3ED6203A6B8527LL))),assignCallTemp(eo_1, ref(v_ini_settings)),f_settings2array(eo_0, eo_1)));
  }
  LINE(535,f_settings2params(ref(v_ini_settings)));
  LINE(538,f_save_text(v_tmp_file, v_section_text.rvalAt("FILE", 0x7863294A8F33D14FLL)));
  if (LINE(539,x_array_key_exists("GET", v_section_text))) {
    (v_query_string = LINE(540,x_trim(toString(v_section_text.rvalAt("GET", 0x25DCCC35D69AD828LL)))));
  }
  else {
    (v_query_string = "");
  }
  LINE(545,x_putenv("REDIRECT_STATUS=1"));
  LINE(546,x_putenv(toString("QUERY_STRING=") + v_query_string));
  LINE(547,x_putenv(toString("PATH_TRANSLATED=") + v_tmp_file));
  LINE(548,x_putenv(toString("SCRIPT_FILENAME=") + v_tmp_file));
  (v_args = toBoolean(v_section_text.rvalAt("ARGS", 0x7A24AFD8ADE43B8ELL)) ? ((Variant)(concat(" -- ", toString(v_section_text.rvalAt("ARGS", 0x7A24AFD8ADE43B8ELL))))) : ((Variant)("")));
  (v_pccargs = LINE(552,f_parsetestfilevars(x_trim(toString(v_section_text.rvalAt("PCCARGS", 0x26368B5C3A6EADCFLL))))));
  if (LINE(554,x_array_key_exists("POST", v_section_text)) && !(empty(v_section_text, "POST", 0x73E3781F017E3A02LL))) {
    (v_post = LINE(556,x_trim(toString(v_section_text.rvalAt("POST", 0x73E3781F017E3A02LL)))));
    LINE(557,f_save_text(v_tmp_post, v_post));
    (v_content_length = LINE(558,x_strlen(v_post)));
    LINE(560,x_putenv("REQUEST_METHOD=POST"));
    LINE(561,x_putenv("CONTENT_TYPE=application/x-www-form-urlencoded"));
    LINE(562,x_putenv(toString("CONTENT_LENGTH=") + toString(v_content_length)));
    (v_cmd = concat(toString(v_php), LINE(565,concat6(" ", toString(v_pccargs), " \"", v_tmp_file, "\" 2>&1 < ", v_tmp_post))));
  }
  else {
    LINE(569,x_putenv("REQUEST_METHOD=GET"));
    LINE(570,x_putenv("CONTENT_TYPE="));
    LINE(571,x_putenv("CONTENT_LENGTH="));
    (v_cmd = LINE(574,(assignCallTemp(eo_0, toString(v_php)),assignCallTemp(eo_2, concat6(toString(v_pccargs), " \"", v_tmp_file, "\" ", toString(v_args), " 2>&1")),concat3(eo_0, " -d 0 ", eo_2))));
  }
  if (toBoolean(get_global_variables()->k_DETAILED)) echo(concat_rev(LINE(587,concat3("\nCOMMAND ", v_cmd, "\n")), concat_rev(toString(LINE(584,x_getenv("SCRIPT_FILENAME"))), concat(concat_rev(toString(LINE(583,x_getenv("REQUEST_METHOD"))), concat(concat_rev(toString(LINE(582,x_getenv("REDIRECT_STATUS"))), concat(concat_rev(toString(LINE(581,x_getenv("QUERY_STRING"))), concat(concat_rev(toString(LINE(580,x_getenv("PATH_TRANSLATED"))), concat(concat_rev(toString(LINE(579,x_getenv("CONTENT_TYPE"))), (assignCallTemp(eo_1, toString(LINE(578,x_getenv("CONTENT_LENGTH")))),concat3("\nCONTENT_LENGTH  = ", eo_1, "\nCONTENT_TYPE    = "))), "\nPATH_TRANSLATED = ")), "\nQUERY_STRING    = ")), "\nREDIRECT_STATUS = ")), "\nREQUEST_METHOD  = ")), "\nSCRIPT_FILENAME = "))));
  if (LINE(590,x_array_key_exists("PRECOMPILE", v_section_text))) {
    (v_preCompile = LINE(591,x_trim(toString(v_section_text.rvalAt("PRECOMPILE", 0x2FBF3D95BEE2B92ELL)))));
    if (!(empty(v_preCompile))) {
      if (toBoolean(get_global_variables()->k_DETAILED)) echo(LINE(594,concat3("PRECOMPILE: [", v_preCompile, "]\n")));
      LINE(595,x_system(v_preCompile));
    }
  }
  (v_out = f_shell_exec(v_cmd));
  if (LINE(603,x_array_key_exists("POSTCOMPILE", v_section_text))) {
    (v_postCompile = LINE(604,x_trim(toString(v_section_text.rvalAt("POSTCOMPILE", 0x68DA86A0EBE66208LL)))));
    if (!(empty(v_postCompile))) {
      if (toBoolean(get_global_variables()->k_DETAILED)) echo(LINE(607,concat3("POSTCOMPILE: [", v_postCompile, "]\n")));
      concat_assign(v_out, f_shell_exec(v_postCompile));
    }
  }
  (v_rtexpect = LINE(615,x_trim(toString(v_section_text.rvalAt("RTEXPECT", 0x33B8AEC669BD3922LL)))));
  if ((equal(0LL, LINE(616,(assignCallTemp(eo_0, x_trim(v_out)),assignCallTemp(eo_1, x_trim(toString(v_section_text.rvalAt("EXPECT", 0x36CC00F3D4F35017LL)))),x_strcmp(eo_0, eo_1))))) && !(empty(v_rtexpect))) {
    (v_rtcmd = LINE(618,x_ereg_replace("\\.phpt$", "", toString(v_file))));
    (v_rtexpect = LINE(619,f_parsetestfilevars(v_rtexpect)));
    if (LINE(622,x_array_key_exists("PRERUN", v_section_text))) {
      (v_preRun = LINE(623,x_trim(toString(v_section_text.rvalAt("PRERUN", 0x0A8B1FD48C7421B3LL)))));
      if (!(empty(v_preRun))) {
        if (toBoolean(get_global_variables()->k_DETAILED)) echo(LINE(626,concat3("PRERUN: [", v_preRun, "]\n")));
        LINE(627,x_system(v_preRun));
      }
    }
    (v_rtout = f_shell_exec(v_rtcmd + toString(" 2>&1")));
  }
  (silenceInc(), silenceDec(LINE(635,x_unlink(x_ereg_replace("\\.phpt$", ".o", toString(v_file))))));
  if (!same(LINE(637,x_strpos(toString(v_pccargs), "-l")), false)) {
    (v_type = "_u");
    (silenceInc(), silenceDec(LINE(642,x_unlink(x_ereg_replace("\\.phpt$", concat(v_type, ".o"), toString(v_file))))));
  }
  (silenceInc(), silenceDec(LINE(644,x_unlink(v_tmp_post))));
  (v_output = LINE(647,x_trim(v_out)));
  (v_output = LINE(648,x_preg_replace("/\\r\\n/", "\n", v_output)));
  if (isset(v_old_php) && !same(((v_pos = LINE(651,x_strpos(toString(v_output), "\n\n")))), false)) {
    (v_output = LINE(652,x_substr(toString(v_output), (toInt32(v_pos + 2LL)))));
  }
  if (isset(v_section_text, "EXPECTF", 0x78CEAF60AAE95596LL) || isset(v_section_text, "EXPECTREGEX", 0x304B9E394FBFDA24LL)) {
    if (isset(v_section_text, "EXPECTF", 0x78CEAF60AAE95596LL)) {
      (v_wanted = LINE(657,x_trim(toString(v_section_text.rvalAt("EXPECTF", 0x78CEAF60AAE95596LL)))));
    }
    else {
      (v_wanted = LINE(659,x_trim(toString(v_section_text.rvalAt("EXPECTREGEX", 0x304B9E394FBFDA24LL)))));
    }
    (v_wanted_re = LINE(661,x_preg_replace("/\\r\\n/", "\n", v_wanted)));
    if (isset(v_section_text, "EXPECTF", 0x78CEAF60AAE95596LL)) {
      (v_wanted_re = LINE(663,x_preg_quote(toString(v_wanted_re), "/")));
      (v_wanted_re = LINE(665,x_str_replace("%s", ".+\?", v_wanted_re)));
      (v_wanted_re = LINE(666,x_str_replace("%i", "[+\\-]\?[0-9]+", v_wanted_re)));
      (v_wanted_re = LINE(667,x_str_replace("%d", "[0-9]+", v_wanted_re)));
      (v_wanted_re = LINE(668,x_str_replace("%x", "[0-9a-fA-F]+", v_wanted_re)));
      (v_wanted_re = LINE(669,x_str_replace("%f", "[+\\-]\?\\.\?[0-9]+\\.\?[0-9]*(E-\?[0-9]+)\?", v_wanted_re)));
      (v_wanted_re = LINE(670,x_str_replace("%c", ".", v_wanted_re)));
    }
    if (toBoolean(LINE(678,(assignCallTemp(eo_0, concat3("/^", toString(v_wanted_re), "$/s")),assignCallTemp(eo_1, toString(v_output)),x_preg_match(eo_0, eo_1))))) {
      (silenceInc(), silenceDec(LINE(679,x_unlink(v_tmp_file))));
      echo(LINE(680,concat4("PASS ", v_tested, v_info, "\n")));
      if (isset(v_old_php)) {
        (v_php = v_old_php);
      }
      return "PASSED";
    }
  }
  else {
    (v_wanted = LINE(688,x_trim(toString(v_section_text.rvalAt("EXPECT", 0x36CC00F3D4F35017LL)))));
    (v_wanted = LINE(689,f_parsetestfilevars(v_wanted)));
    (v_wanted = LINE(690,x_preg_replace("/\\r\\n/", "\n", v_wanted)));
    (v_ok = (equal(0LL, LINE(693,x_strcmp(toString(v_output), toString(v_wanted))))));
    if (!(empty(v_rtexpect))) {
      (v_ok = (equal(0LL, LINE(695,x_strcmp(v_rtout, toString(v_rtexpect))))));
    }
    if (v_ok) {
      (silenceInc(), silenceDec(LINE(698,x_unlink(v_tmp_file))));
      echo(LINE(714,concat4("PASS ", v_tested, v_info, "\n")));
      if (isset(v_old_php)) {
        (v_php = v_old_php);
      }
      return "PASSED";
    }
  }
  if (v_warn) {
    echo(LINE(724,concat4("WARN ", v_tested, v_info, "\n")));
  }
  else {
    echo(LINE(726,concat4("FAIL ", v_tested, v_info, "\n")));
  }
  g->GV(__PHP_FAILED_TESTS__).append((Array(ArrayInit(3).set(0, "name", v_file, 0x0BCDB293DC3CBDDCLL).set(1, "test_name", v_tested, 0x6D72CEB44AC01301LL).set(2, "info", v_info, 0x59E9384E33988B3ELL).create())));
  if (!same(LINE(736,x_strpos(toString(gv_log_format), "E")), false)) {
    (v_logname = LINE(737,x_ereg_replace("\\.phpt$", ".exp", toString(v_file))));
    (toBoolean((v_log = LINE(738,x_fopen(v_logname, "w"))))) || ((f_error(toString("Cannot create test log - ") + v_logname), null));
    LINE(739,x_fwrite(toObject(v_log), toString(v_wanted)));
    LINE(740,x_fclose(toObject(v_log)));
    if (!(empty(v_rtexpect))) {
      (v_logname = LINE(742,x_ereg_replace("\\.phpt$", ".rtexp", toString(v_file))));
      (toBoolean((v_log = LINE(743,x_fopen(v_logname, "w"))))) || ((f_error(toString("Cannot create test log - ") + v_logname), null));
      LINE(744,x_fwrite(toObject(v_log), toString(v_rtexpect)));
      LINE(745,x_fclose(toObject(v_log)));
    }
  }
  if (!same(LINE(750,x_strpos(toString(gv_log_format), "O")), false)) {
    (v_logname = LINE(751,x_ereg_replace("\\.phpt$", ".out", toString(v_file))));
    (toBoolean((v_log = LINE(752,x_fopen(v_logname, "w"))))) || ((f_error(toString("Cannot create test log - ") + v_logname), null));
    LINE(753,x_fwrite(toObject(v_log), toString(v_output)));
    LINE(754,x_fclose(toObject(v_log)));
    if (!(empty(v_rtout))) {
      (v_logname = LINE(756,x_ereg_replace("\\.phpt$", ".rtout", toString(v_file))));
      (toBoolean((v_log = LINE(757,x_fopen(v_logname, "w"))))) || ((f_error(toString("Cannot create test log - ") + v_logname), null));
      LINE(758,x_fwrite(toObject(v_log), v_rtout));
      LINE(759,x_fclose(toObject(v_log)));
    }
  }
  if (isset(v_old_php)) {
    (v_php = v_old_php);
  }
  return v_warn ? (("WARNED")) : (("FAILED"));
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 789 */
void f_settings2params(Variant v_ini_settings) {
  FUNCTION_INJECTION(settings2params);
  String v_settings;
  Primitive v_name = 0;
  Variant v_value;

  if (toBoolean(LINE(791,x_count(v_ini_settings)))) {
    (v_settings = "");
    {
      LOOP_COUNTER(2);
      for (ArrayIterPtr iter4 = v_ini_settings.begin(); !iter4->end(); iter4->next()) {
        LOOP_COUNTER_CHECK(2);
        v_value = iter4->second();
        v_name = iter4->first();
        {
          (v_value = LINE(794,x_addslashes(toString(v_value))));
          concat_assign(v_settings, LINE(795,concat5(" -d \"", toString(v_name), "=", toString(v_value), "\"")));
        }
      }
    }
    (v_ini_settings = v_settings);
  }
  else {
    (v_ini_settings = "");
  }
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 771 */
void f_error(CStrRef v_message) {
  FUNCTION_INJECTION(error);
  echo(LINE(773,concat3("ERROR: ", v_message, "\n")));
  f_exit(1LL);
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 385 */
void f_error_report(Variant v_testname, Variant v_logname, CVarRef v_tested) {
  FUNCTION_INJECTION(error_report);
  (v_testname = LINE(387,x_realpath(toString(v_testname))));
  (v_logname = LINE(388,x_realpath(toString(v_logname))));
  {
    String tmp6 = (LINE(389,x_strtoupper(toString(x_getenv("TEST_PHP_ERROR_STYLE")))));
    int tmp7 = -1;
    if (equal(tmp6, ("MSVC"))) {
      tmp7 = 0;
    } else if (equal(tmp6, ("EMACS"))) {
      tmp7 = 1;
    }
    switch (tmp7) {
    case 0:
      {
        echo(concat(toString(v_testname), LINE(391,concat3("(1) : ", toString(v_tested), "\n"))));
        echo(concat(toString(v_logname), LINE(392,concat3("(1) :  ", toString(v_tested), "\n"))));
        goto break5;
      }
    case 1:
      {
        echo(concat(toString(v_testname), LINE(395,concat3(":1: ", toString(v_tested), "\n"))));
        echo(concat(toString(v_logname), LINE(396,concat3(":1:  ", toString(v_tested), "\n"))));
        goto break5;
      }
    }
    break5:;
  }
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 268 */
Variant f_test_sort(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(test_sort);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_cwd __attribute__((__unused__)) = g->GV(cwd);
  int64 v_ta = 0;
  int64 v_tb = 0;

  (v_ta = same(LINE(271,x_strpos(toString(v_a), toString(gv_cwd) + toString("/tests"))), 0LL) ? ((1LL + (same(x_strpos(toString(v_a), toString(gv_cwd) + toString("/tests/run-test")), 0LL) ? ((1LL)) : ((0LL))))) : ((0LL)));
  (v_tb = same(LINE(272,x_strpos(toString(v_b), toString(gv_cwd) + toString("/tests"))), 0LL) ? ((1LL + (same(x_strpos(toString(v_b), toString(gv_cwd) + toString("/tests/run-test")), 0LL) ? ((1LL)) : ((0LL))))) : ((0LL)));
  if (equal(v_ta, v_tb)) {
    return LINE(274,x_strcmp(toString(v_a), toString(v_b)));
  }
  else {
    return v_tb - v_ta;
  }
  return null;
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 235 */
void f_find_files(CVarRef v_dir, bool v_is_ext_dir //  = false
, bool v_ignore //  = false
) {
  FUNCTION_INJECTION(find_files);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_test_files __attribute__((__unused__)) = g->GV(test_files);
  Variant &gv_exts_to_test __attribute__((__unused__)) = g->GV(exts_to_test);
  Variant &gv_ignored_by_ext __attribute__((__unused__)) = g->GV(ignored_by_ext);
  Variant &gv_exts_skipped __attribute__((__unused__)) = g->GV(exts_skipped);
  Variant &gv_exts_tested __attribute__((__unused__)) = g->GV(exts_tested);
  Variant v_o;
  Variant v_name;
  bool v_skip_ext = false;
  Variant v_testfile;

  {
  }
  (toBoolean((v_o = LINE(239,x_opendir(toString(v_dir)))))) || ((f_error(toString("cannot open directory: ") + toString(v_dir)), null));
  LOOP_COUNTER(8);
  {
    while (!same(((v_name = LINE(240,x_readdir(toObject(v_o))))), false)) {
      LOOP_COUNTER_CHECK(8);
      {
        if (LINE(241,x_is_dir(concat3(toString(v_dir), "/", toString(v_name)))) && !(x_in_array(v_name, ScalarArrays::sa_[3]))) {
          (v_skip_ext = (v_is_ext_dir && !(LINE(242,x_in_array(v_name, gv_exts_to_test)))));
          if (v_skip_ext) {
            gv_exts_skipped++;
          }
          LINE(246,(assignCallTemp(eo_0, concat3(toString(v_dir), "/", toString(v_name))),assignCallTemp(eo_2, v_ignore || v_skip_ext),f_find_files(eo_0, false, eo_2)));
        }
        if (equal(LINE(250,x_substr(toString(v_name), toInt32(-4LL))), ".tmp")) {
          (silenceInc(), silenceDec(LINE(251,x_unlink(concat3(toString(v_dir), "/", toString(v_name))))));
          continue;
        }
        if (equal(LINE(256,x_substr(toString(v_name), toInt32(-5LL))), ".phpt")) {
          if (v_ignore) {
            gv_ignored_by_ext++;
          }
          else {
            (v_testfile = LINE(260,x_realpath(concat3(toString(v_dir), "/", toString(v_name)))));
            gv_test_files.append((v_testfile));
          }
        }
      }
    }
  }
  LINE(265,x_closedir(toObject(v_o)));
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 810 */
Variant f_parsetestfilevars(Variant v_string) {
  FUNCTION_INJECTION(parseTestFileVars);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_testFileVars __attribute__((__unused__)) = g->GV(testFileVars);
  Primitive v_key = 0;
  Variant v_val;

  {
    LOOP_COUNTER(9);
    for (ArrayIterPtr iter11 = gv_testFileVars.begin(); !iter11->end(); iter11->next()) {
      LOOP_COUNTER_CHECK(9);
      v_val = iter11->second();
      v_key = iter11->first();
      {
        (v_string = LINE(813,(assignCallTemp(eo_0, concat3("/", toString(v_key), "/")),assignCallTemp(eo_1, v_val),assignCallTemp(eo_2, v_string),x_preg_replace(eo_0, eo_1, eo_2))));
      }
    }
  }
  return v_string;
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 367 */
void f_save_text(CVarRef v_filename, CVarRef v_text) {
  FUNCTION_INJECTION(save_text);
  Variant v_fp;

  (toBoolean((v_fp = (silenceInc(), silenceDec(LINE(369,x_fopen(toString(v_filename), "w"))))))) || ((f_error(concat3("Cannot open file '", toString(v_filename), "' (save_text)")), null));
  LINE(370,x_fwrite(toObject(v_fp), toString(v_text)));
  LINE(371,x_fclose(toObject(v_fp)));
  if (less(1LL, get_global_variables()->k_DETAILED)) echo(LINE(378,concat5("\nFILE ", toString(v_filename), " {{{\n", toString(v_text), "\n}}} \n")));
} /* function */
/* SRC: roadsend/pcc/sa-tests/run-tests.php line 777 */
void f_settings2array(CVarRef v_settings, Variant v_ini_settings) {
  FUNCTION_INJECTION(settings2array);
  Variant v_setting;
  String v_name;
  String v_value;

  {
    LOOP_COUNTER(12);
    for (ArrayIterPtr iter14 = v_settings.begin(); !iter14->end(); iter14->next()) {
      LOOP_COUNTER_CHECK(12);
      v_setting = iter14->second();
      {
        if (!same(LINE(780,x_strpos(toString(v_setting), "=")), false)) {
          (v_setting = LINE(781,x_explode("=", toString(v_setting), toInt32(2LL))));
          (v_name = LINE(782,x_trim(x_strtolower(toString(v_setting.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))))));
          (v_value = LINE(783,x_trim(toString(v_setting.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
          v_ini_settings.set(v_name, (v_value));
        }
      }
    }
  }
} /* function */
Variant i_test_sort(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x11FACE507E317D45LL, test_sort) {
    return (f_test_sort(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$sa_tests$run_tests_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/sa-tests/run-tests.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$sa_tests$run_tests_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cwd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cwd") : g->GV(cwd);
  Variant &v_php __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php") : g->GV(php);
  Variant &v_os __attribute__((__unused__)) = (variables != gVariables) ? variables->get("os") : g->GV(os);
  Variant &v_argc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argc") : g->gv_argc;
  Variant &v_argv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("argv") : g->gv_argv;
  Variant &v_log_format __attribute__((__unused__)) = (variables != gVariables) ? variables->get("log_format") : g->GV(log_format);
  Variant &v_user_tests __attribute__((__unused__)) = (variables != gVariables) ? variables->get("user_tests") : g->GV(user_tests);
  Variant &v_toDelete __attribute__((__unused__)) = (variables != gVariables) ? variables->get("toDelete") : g->GV(toDelete);
  Variant &v_info_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("info_file") : g->GV(info_file);
  Variant &v_php_info __attribute__((__unused__)) = (variables != gVariables) ? variables->get("php_info") : g->GV(php_info);
  Variant &v_ini_overwrites __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ini_overwrites") : g->GV(ini_overwrites);
  Variant &v_info_params __attribute__((__unused__)) = (variables != gVariables) ? variables->get("info_params") : g->GV(info_params);
  Variant &v_testFileVars __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testFileVars") : g->GV(testFileVars);
  Variant &v_test_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_dir") : g->GV(test_dir);
  Variant &v_test_files __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_files") : g->GV(test_files);
  Variant &v_test_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_results") : g->GV(test_results);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_testfile __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testfile") : g->GV(testfile);
  Variant &v_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("name") : g->GV(name);
  Variant &v_exts_to_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exts_to_test") : g->GV(exts_to_test);
  Variant &v_exts_tested __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exts_tested") : g->GV(exts_tested);
  Variant &v_exts_skipped __attribute__((__unused__)) = (variables != gVariables) ? variables->get("exts_skipped") : g->GV(exts_skipped);
  Variant &v_ignored_by_ext __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ignored_by_ext") : g->GV(ignored_by_ext);
  Variant &v_test_dirs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_dirs") : g->GV(test_dirs);
  Variant &v_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dir") : g->GV(dir);
  Variant &v_start_time __attribute__((__unused__)) = (variables != gVariables) ? variables->get("start_time") : g->GV(start_time);
  Variant &v_end_time __attribute__((__unused__)) = (variables != gVariables) ? variables->get("end_time") : g->GV(end_time);
  Variant &v_n_total __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n_total") : g->GV(n_total);
  Variant &v_sum_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sum_results") : g->GV(sum_results);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_percent_results __attribute__((__unused__)) = (variables != gVariables) ? variables->get("percent_results") : g->GV(percent_results);
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_fileToDelete __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fileToDelete") : g->GV(fileToDelete);
  Variant &v_summary __attribute__((__unused__)) = (variables != gVariables) ? variables->get("summary") : g->GV(summary);
  Variant &v_failed_test_summary __attribute__((__unused__)) = (variables != gVariables) ? variables->get("failed_test_summary") : g->GV(failed_test_summary);
  Variant &v_failed_test_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("failed_test_data") : g->GV(failed_test_data);

  if (toBoolean(LINE(42,x_getenv("TEST_PHP_SRCDIR")))) {
    (silenceInc(), silenceDec(LINE(43,x_chdir(toString(x_getenv("TEST_PHP_SRCDIR"))))));
  }
  (v_cwd = LINE(46,x_getcwd()));
  LOOP_COUNTER(15);
  {
    while (toBoolean((silenceInc(), silenceDec(LINE(49,x_ob_end_clean()))))) {
      LOOP_COUNTER_CHECK(15);
    }
  }
  if (toBoolean(LINE(50,x_ob_get_level()))) echo("Not all buffers were deleted.\n");
  LINE(52,x_error_reporting(toInt32(6143LL) /* E_ALL */));
  LINE(53,x_ini_set("magic_quotes_runtime", toString(0LL)));
  if (toBoolean(LINE(56,x_getenv("TEST_PHP_EXECUTABLE")))) {
    (v_php = LINE(57,x_getenv("TEST_PHP_EXECUTABLE")));
  }
  if (empty(v_php) || !(LINE(62,x_file_exists(toString(v_php))))) {
    (v_php = LINE(64,x_trim(f_shell_exec(toString("which pcc")))));
  }
  if (empty(v_php) || !(LINE(68,x_file_exists(toString(v_php))))) {
    echo("No pcc in your path. Trying default location.\n");
    (v_os = "Linux" /* PHP_OS */);
    if (!equal(LINE(71,(assignCallTemp(eo_0, x_strtoupper(toString(v_os))),x_substr(eo_0, toInt32(0LL), toInt32(3LL)))), "LIN")) (v_php = "c:/roadsend/local/bin/pcc.exe");
    else (v_php = "/usr/bin/pcc");
  }
  if (empty(v_php) || !(LINE(77,x_file_exists(toString(v_php))))) {
    if (more(v_argc, 1LL)) (v_php = v_argv.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  }
  if (empty(v_php) || !(LINE(85,x_file_exists(toString(v_php))))) {
    LINE(86,f_error("Can't find pcc. Please set the environment variable TEST_PHP_EXECUTABLE with the location."));
  }
  if (toBoolean(LINE(89,x_getenv("TEST_PHP_LOG_FORMAT")))) {
    (v_log_format = LINE(90,x_strtoupper(toString(x_getenv("TEST_PHP_LOG_FORMAT")))));
  }
  else {
    (v_log_format = "LEOD");
  }
  if (toBoolean(LINE(100,x_getenv("TEST_PHP_DETAILED")))) {
    LINE(101,g->declareConstant("DETAILED", g->k_DETAILED, x_getenv("TEST_PHP_DETAILED")));
  }
  else {
    LINE(103,g->declareConstant("DETAILED", g->k_DETAILED, 0LL));
  }
  if (equal(LINE(106,x_getenv("ZEND_COMPAT")), 1LL)) {
    echo("running in ZEND_COMPAT mode: all EXPECT sections moved to RTEXPECT\n");
  }
  if (toBoolean(LINE(111,x_getenv("TEST_PHP_USER")))) {
    (v_user_tests = LINE(112,(assignCallTemp(eo_1, toString(x_getenv("TEST_PHP_USER"))),x_explode(",", eo_1))));
  }
  else {
    (v_user_tests = ScalarArrays::sa_[0]);
  }
  (v_toDelete = ScalarArrays::sa_[0]);
  (v_info_file = concat(toString(LINE(121,x_realpath(x_dirname(get_source_filename("roadsend/pcc/sa-tests/run-tests.php"))))), "/run-test-info.php"));
  (silenceInc(), silenceDec(LINE(122,x_unlink(toString(v_info_file)))));
  (v_php_info = "<\?php echo \"\nPHP_SAPI    : \" . PHP_SAPI . \"\nPHP_VERSION : \" . phpversion() . \"\nZEND_VERSION: \" . zend_version() . \"\nPHP_OS      : \" . PHP_OS . \" - \" . php_uname() .\"\nPCC_VERSION : \" . PCC_VERSION;\n\?>");
  LINE(132,f_save_text(v_info_file, v_php_info));
  (v_ini_overwrites = ScalarArrays::sa_[1]);
  (v_info_params = ScalarArrays::sa_[0]);
  LINE(157,f_settings2array(v_ini_overwrites, ref(v_info_params)));
  LINE(158,f_settings2params(ref(v_info_params)));
  (v_php_info = f_shell_exec(toString(v_php) + toString(" -f ") + toString(v_info_file)));
  (silenceInc(), silenceDec(LINE(160,x_unlink(toString(v_info_file)))));
  LINE(161,g->declareConstant("TESTED_PHP_VERSION", g->k_TESTED_PHP_VERSION, f_shell_exec(toString(v_php) + toString(" --version"))));
  (v_testFileVars = Array(ArrayInit(1).set(0, "%CWD%", v_cwd, 0x28C5510BB4A9A6BELL).create()));
  echo(concat("\n=====================================================================\nCWD         : ", LINE(175,concat6(toString(v_cwd), "\nPHP         : ", toString(v_php), " ", toString(v_php_info), "\nExtra dirs  : "))));
  {
    LOOP_COUNTER(16);
    for (ArrayIterPtr iter18 = v_user_tests.begin(); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_test_dir = iter18->second();
      {
        echo(toString(v_test_dir) + toString("\n              "));
      }
    }
  }
  echo("\n=====================================================================\n");
  (v_test_files = ScalarArrays::sa_[0]);
  (v_test_results = ScalarArrays::sa_[0]);
  (g->GV(__PHP_FAILED_TESTS__) = ScalarArrays::sa_[0]);
  if (isset(v_argc) && more(v_argc, 1LL)) {
    {
      LOOP_COUNTER(19);
      for ((v_i = 1LL); less(v_i, v_argc); v_i++) {
        LOOP_COUNTER_CHECK(19);
        {
          (v_testfile = LINE(192,x_realpath(toString(v_argv.rvalAt(v_i)))));
          if (LINE(193,x_is_dir(toString(v_testfile)))) {
            LINE(194,f_find_files(v_testfile));
          }
          else if (toBoolean(LINE(195,x_preg_match("/\\.phpt$/", toString(v_testfile))))) {
            v_test_files.append((v_testfile));
          }
        }
      }
    }
    (v_test_files = LINE(199,x_array_unique(v_test_files)));
    if (toBoolean(LINE(202,x_count(v_test_files)))) {
      LINE(203,x_usort(ref(v_test_files), "test_sort"));
      echo("Running selected tests.\n");
      {
        LOOP_COUNTER(20);
        for (ArrayIterPtr iter22 = v_test_files.begin(); !iter22->end(); iter22->next()) {
          LOOP_COUNTER_CHECK(20);
          v_name = iter22->second();
          {
            v_test_results.set(v_name, (LINE(206,f_run_test(v_php, v_name))));
          }
        }
      }
      if ((equal(LINE(208,x_getenv("REPORT_EXIT_STATUS")), 1LL)) && (toBoolean((assignCallTemp(eo_1, x_implode(" ", v_test_results)),x_ereg("FAILED( |$)", eo_1))))) {
        f_exit(1LL);
      }
      f_exit(0LL);
    }
  }
  (v_test_files = ScalarArrays::sa_[0]);
  (v_exts_to_test = LINE(217,x_get_loaded_extensions()));
  (v_exts_tested = LINE(218,x_count(v_exts_to_test)));
  (v_exts_skipped = 0LL);
  (v_ignored_by_ext = 0LL);
  LINE(221,x_sort(ref(v_exts_to_test)));
  (v_test_dirs = ScalarArrays::sa_[2]);
  if (empty(v_user_tests)) {
    {
      LOOP_COUNTER(23);
      for (ArrayIterPtr iter25 = v_test_dirs.begin(); !iter25->end(); iter25->next()) {
        LOOP_COUNTER_CHECK(23);
        v_dir = iter25->second();
        {
          LINE(226,(assignCallTemp(eo_0, concat3(toString(v_cwd), "/", toString(v_dir))),assignCallTemp(eo_1, (equal(v_dir, "ext"))),f_find_files(eo_0, eo_1)));
        }
      }
    }
  }
  else {
    {
      LOOP_COUNTER(26);
      for (ArrayIterPtr iter28 = v_user_tests.begin(); !iter28->end(); iter28->next()) {
        LOOP_COUNTER_CHECK(26);
        v_dir = iter28->second();
        {
          LINE(231,f_find_files(v_dir, (equal(v_dir, "ext"))));
        }
      }
    }
  }
  (v_test_files = LINE(280,x_array_unique(v_test_files)));
  LINE(281,x_usort(ref(v_test_files), "test_sort"));
  (v_start_time = LINE(283,x_time()));
  echo(LINE(287,(assignCallTemp(eo_1, LINE(285,x_date("Y-m-d H:i:s", toInt64(v_start_time)))),concat3("TIME START ", eo_1, "\n=====================================================================\n"))));
  {
    LOOP_COUNTER(29);
    for (ArrayIterPtr iter31 = v_test_files.begin(); !iter31->end(); iter31->next()) {
      LOOP_COUNTER_CHECK(29);
      v_name = iter31->second();
      {
        v_test_results.set(v_name, (LINE(290,f_run_test(v_php, v_name))));
      }
    }
  }
  (v_end_time = LINE(293,x_time()));
  if (equal(0LL, LINE(297,x_count(v_test_results)))) {
    echo("No tests were run.\n");
    return null;
  }
  (v_n_total = LINE(302,x_count(v_test_results)));
  v_n_total += v_ignored_by_ext;
  (v_sum_results = ScalarArrays::sa_[4]);
  {
    LOOP_COUNTER(32);
    for (ArrayIterPtr iter34 = v_test_results.begin(); !iter34->end(); iter34->next()) {
      LOOP_COUNTER_CHECK(32);
      v_v = iter34->second();
      {
        lval(v_sum_results.lvalAt(v_v))++;
      }
    }
  }
  lval(v_sum_results.lvalAt("SKIPPED", 0x663C301D44C750E5LL)) += v_ignored_by_ext;
  (v_percent_results = ScalarArrays::sa_[0]);
  LOOP_COUNTER(35);
  {
    while (toBoolean(df_lambda_1(LINE(311,x_each(ref(v_sum_results))), v_v, v_n))) {
      LOOP_COUNTER_CHECK(35);
      {
        v_percent_results.set(v_v, (divide((100.0 * toDouble(v_n)), v_n_total)));
      }
    }
  }
  {
    LOOP_COUNTER(36);
    for (ArrayIterPtr iter38 = v_toDelete.begin(); !iter38->end(); iter38->next()) {
      LOOP_COUNTER_CHECK(36);
      v_fileToDelete = iter38->second();
      {
        (silenceInc(), silenceDec(LINE(317,x_unlink(concat3(toString(v_cwd), "/", toString(v_fileToDelete))))));
      }
    }
  }
  echo(concat("\n=====================================================================\nTIME END ", LINE(322,x_date("Y-m-d H:i:s", toInt64(v_end_time)))));
  (v_summary = concat(concat_rev(LINE(337,x_sprintf(2, "%4d seconds", Array(ArrayInit(1).set(0, v_end_time - v_start_time).create()))), concat(concat_rev(LINE(335,x_sprintf(3, "%4d (%2.1f%%)", Array(ArrayInit(2).set(0, v_sum_results.rvalAt("PASSED", 0x2F0347678F7BF36ELL)).set(1, v_percent_results.rvalAt("PASSED", 0x2F0347678F7BF36ELL)).create()))), concat(concat_rev(LINE(334,x_sprintf(3, "%4d (%2.1f%%)", Array(ArrayInit(2).set(0, v_sum_results.rvalAt("FAILED", 0x52B3F0742123E85BLL)).set(1, v_percent_results.rvalAt("FAILED", 0x52B3F0742123E85BLL)).create()))), concat(concat_rev(LINE(333,x_sprintf(3, "%4d (%2.1f%%)", Array(ArrayInit(2).set(0, v_sum_results.rvalAt("WARNED", 0x277E5E0800E36957LL)).set(1, v_percent_results.rvalAt("WARNED", 0x277E5E0800E36957LL)).create()))), concat(concat_rev(LINE(332,x_sprintf(3, "%4d (%2.1f%%)", Array(ArrayInit(2).set(0, v_sum_results.rvalAt("SKIPPED", 0x663C301D44C750E5LL)).set(1, v_percent_results.rvalAt("SKIPPED", 0x663C301D44C750E5LL)).create()))), concat(concat_rev(LINE(331,x_sprintf(2, "%4d", Array(ArrayInit(1).set(0, v_n_total).create()))), concat(concat_rev(LINE(329,x_sprintf(2, "%4d", Array(ArrayInit(1).set(0, v_exts_tested).create()))), (assignCallTemp(eo_1, LINE(328,x_sprintf(2, "%4d", Array(ArrayInit(1).set(0, v_exts_skipped).create())))),concat3("\n=====================================================================\nTEST RESULT SUMMARY\n---------------------------------------------------------------------\nExts skipped    : ", eo_1, "\nExts tested     : "))), "\n---------------------------------------------------------------------\nNumber of tests : ")), "\nTests skipped   : ")), "\nTests warned    : ")), "\nTests failed    : ")), "\nTests passed    : ")), "\n---------------------------------------------------------------------\nTime taken      : ")), "\n=====================================================================\n"));
  echo(toString(v_summary));
  (v_failed_test_summary = "");
  if (toBoolean(LINE(343,x_count(g->GV(__PHP_FAILED_TESTS__))))) {
    concat_assign(v_failed_test_summary, "\n=====================================================================\nFAILED TEST SUMMARY\n---------------------------------------------------------------------\n");
    {
      LOOP_COUNTER(39);
      Variant map40 = g->GV(__PHP_FAILED_TESTS__);
      for (ArrayIterPtr iter41 = map40.begin(); !iter41->end(); iter41->next()) {
        LOOP_COUNTER_CHECK(39);
        v_failed_test_data = iter41->second();
        {
          concat_assign(v_failed_test_summary, LINE(350,concat3(toString(v_failed_test_data.rvalAt("test_name", 0x6D72CEB44AC01301LL)), toString(v_failed_test_data.rvalAt("info", 0x59E9384E33988B3ELL)), "\n")));
        }
      }
    }
    concat_assign(v_failed_test_summary, "=====================================================================\n");
  }
  if (toBoolean(v_failed_test_summary) && !(toBoolean(LINE(355,x_getenv("NO_PHPTEST_SUMMARY"))))) {
    echo(toString(v_failed_test_summary));
  }
  if ((equal(LINE(359,x_getenv("REPORT_EXIT_STATUS")), 1LL)) && (toBoolean(v_sum_results.rvalAt("FAILED", 0x52B3F0742123E85BLL)))) {
    f_exit(1LL);
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
