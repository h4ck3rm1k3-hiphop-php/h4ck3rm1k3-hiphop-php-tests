
#ifndef __GENERATED_php_roadsend_pcc_sa_tests_run_tests_h__
#define __GENERATED_php_roadsend_pcc_sa_tests_run_tests_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/sa-tests/run-tests.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_run_test(Variant v_php, CVarRef v_file);
void f_settings2params(Variant v_ini_settings);
void f_error(CStrRef v_message);
void f_error_report(Variant v_testname, Variant v_logname, CVarRef v_tested);
Variant f_test_sort(CVarRef v_a, CVarRef v_b);
void f_find_files(CVarRef v_dir, bool v_is_ext_dir = false, bool v_ignore = false);
Variant f_parsetestfilevars(Variant v_string);
void f_save_text(CVarRef v_filename, CVarRef v_text);
Variant pm_php$roadsend$pcc$sa_tests$run_tests_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_settings2array(CVarRef v_settings, Variant v_ini_settings);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_sa_tests_run_tests_h__
