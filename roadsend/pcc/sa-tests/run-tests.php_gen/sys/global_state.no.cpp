
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

static void print(FILE *fp, String s) {
  if (fp) {
    fwrite(s.c_str(), 1, s.size(), fp);
    return;
  }
  echo(s);
}
static void output_dynamic_constants(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_constants = json_decode('");
  Array dynamic_constants;
  dynamic_constants.set("k_TESTED_PHP_VERSION", g->k_TESTED_PHP_VERSION);
  dynamic_constants.set("k_DETAILED", g->k_DETAILED);
  String s = f_json_encode(dynamic_constants);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_static_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$static_global_variables = json_decode('");
  Array static_global_variables;
  static_global_variables.set("gv_argc", g->get("argc"));
  static_global_variables.set("gv_argv", g->get("argv"));
  static_global_variables.set("gv__SERVER", g->get("_SERVER"));
  static_global_variables.set("gv__GET", g->get("_GET"));
  static_global_variables.set("gv__POST", g->get("_POST"));
  static_global_variables.set("gv__COOKIE", g->get("_COOKIE"));
  static_global_variables.set("gv__FILES", g->get("_FILES"));
  static_global_variables.set("gv__ENV", g->get("_ENV"));
  static_global_variables.set("gv__REQUEST", g->get("_REQUEST"));
  static_global_variables.set("gv__SESSION", g->get("_SESSION"));
  static_global_variables.set("gv_HTTP_RAW_POST_DATA", g->get("HTTP_RAW_POST_DATA"));
  static_global_variables.set("gv_http_response_header", g->get("http_response_header"));
  static_global_variables.set("gv_cwd", g->get("cwd"));
  static_global_variables.set("gv_php", g->get("php"));
  static_global_variables.set("gv_os", g->get("os"));
  static_global_variables.set("gv_log_format", g->get("log_format"));
  static_global_variables.set("gv_user_tests", g->get("user_tests"));
  static_global_variables.set("gv_toDelete", g->get("toDelete"));
  static_global_variables.set("gv_info_file", g->get("info_file"));
  static_global_variables.set("gv_php_info", g->get("php_info"));
  static_global_variables.set("gv_ini_overwrites", g->get("ini_overwrites"));
  static_global_variables.set("gv_info_params", g->get("info_params"));
  static_global_variables.set("gv_testFileVars", g->get("testFileVars"));
  static_global_variables.set("gv_test_dir", g->get("test_dir"));
  static_global_variables.set("gv_test_files", g->get("test_files"));
  static_global_variables.set("gv_test_results", g->get("test_results"));
  static_global_variables.set("gv___PHP_FAILED_TESTS__", g->get("__PHP_FAILED_TESTS__"));
  static_global_variables.set("gv_i", g->get("i"));
  static_global_variables.set("gv_testfile", g->get("testfile"));
  static_global_variables.set("gv_name", g->get("name"));
  static_global_variables.set("gv_exts_to_test", g->get("exts_to_test"));
  static_global_variables.set("gv_exts_tested", g->get("exts_tested"));
  static_global_variables.set("gv_exts_skipped", g->get("exts_skipped"));
  static_global_variables.set("gv_ignored_by_ext", g->get("ignored_by_ext"));
  static_global_variables.set("gv_test_dirs", g->get("test_dirs"));
  static_global_variables.set("gv_dir", g->get("dir"));
  static_global_variables.set("gv_start_time", g->get("start_time"));
  static_global_variables.set("gv_end_time", g->get("end_time"));
  static_global_variables.set("gv_n_total", g->get("n_total"));
  static_global_variables.set("gv_sum_results", g->get("sum_results"));
  static_global_variables.set("gv_v", g->get("v"));
  static_global_variables.set("gv_percent_results", g->get("percent_results"));
  static_global_variables.set("gv_n", g->get("n"));
  static_global_variables.set("gv_fileToDelete", g->get("fileToDelete"));
  static_global_variables.set("gv_summary", g->get("summary"));
  static_global_variables.set("gv_failed_test_summary", g->get("failed_test_summary"));
  static_global_variables.set("gv_failed_test_data", g->get("failed_test_data"));
  String s = f_json_encode(static_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_dynamic_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_global_variables = json_decode('");
  Array dynamic_global_variables;
  dynamic_global_variables = *get_variable_table();
  String s = f_json_encode(dynamic_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_variables = json_decode('");
  Array method_static_variables;
  String s = f_json_encode(method_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_inited(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_inited = json_decode('");
  Array method_static_inited;
  String s = f_json_encode(method_static_inited);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_class_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$class_static_variables = json_decode('");
  Array class_static_variables;
  String s = f_json_encode(class_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_pseudomain_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$pseudomain_variables = json_decode('");
  Array pseudomain_variables;
  pseudomain_variables.set("run_pm_php$roadsend$pcc$sa_tests$run_tests_php", g->run_pm_php$roadsend$pcc$sa_tests$run_tests_php);
  String s = f_json_encode(pseudomain_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_functions(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_functions = json_decode('");
  Array redeclared_functions;
  String s = f_json_encode(redeclared_functions);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_classes(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_classes = json_decode('");
  Array redeclared_classes;
  String s = f_json_encode(redeclared_classes);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

void output_global_state(FILE *fp) {
  output_static_global_variables(fp);
  output_dynamic_global_variables(fp);
  output_dynamic_constants(fp);
  output_method_static_variables(fp);
  output_method_static_inited(fp);
  output_class_static_variables(fp);
  output_pseudomain_variables(fp);
  output_redeclared_functions(fp);
  output_redeclared_classes(fp);
}


///////////////////////////////////////////////////////////////////////////////
}
