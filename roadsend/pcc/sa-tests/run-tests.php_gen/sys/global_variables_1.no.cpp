
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      HASH_RETURN(0x4B9AE5064B0A5283LL, g->GV(exts_to_test),
                  exts_to_test);
      break;
    case 5:
      HASH_RETURN(0x178B5DB860BB1F05LL, g->GV(n_total),
                  n_total);
      break;
    case 9:
      HASH_RETURN(0x117B8667E4662809LL, g->GV(n),
                  n);
      break;
    case 13:
      HASH_RETURN(0x262F35FB33EC540DLL, g->GV(ini_overwrites),
                  ini_overwrites);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 15:
      HASH_RETURN(0x7F3CBABB98929C0FLL, g->GV(failed_test_data),
                  failed_test_data);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      HASH_RETURN(0x67FECFEAB01DEA90LL, g->GV(percent_results),
                  percent_results);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 22:
      HASH_RETURN(0x6B3093D1F2F27D16LL, g->GV(cwd),
                  cwd);
      break;
    case 23:
      HASH_RETURN(0x72CCC88F90317797LL, g->GV(exts_tested),
                  exts_tested);
      break;
    case 24:
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 28:
      HASH_RETURN(0x1F09B43053EBEB1CLL, g->GV(log_format),
                  log_format);
      break;
    case 29:
      HASH_RETURN(0x72C2CA3ECF7C191DLL, g->GV(test_dirs),
                  test_dirs);
      break;
    case 32:
      HASH_RETURN(0x72A6FF1221C87FA0LL, g->GV(info_file),
                  info_file);
      break;
    case 34:
      HASH_RETURN(0x24CC58C2E4D68622LL, g->GV(summary),
                  summary);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 45:
      HASH_RETURN(0x1B723EAC6938822DLL, g->GV(end_time),
                  end_time);
      break;
    case 52:
      HASH_RETURN(0x3AD6EDBE00AC3BB4LL, g->GV(php_info),
                  php_info);
      HASH_RETURN(0x50C061D9A30E3034LL, g->GV(testFileVars),
                  testFileVars);
      break;
    case 54:
      HASH_RETURN(0x31CF71EAC03B86B6LL, g->GV(testfile),
                  testfile);
      break;
    case 55:
      HASH_RETURN(0x310559E2D6E6EC37LL, g->GV(user_tests),
                  user_tests);
      break;
    case 57:
      HASH_RETURN(0x25E6CA5109175A39LL, g->GV(__PHP_FAILED_TESTS__),
                  __PHP_FAILED_TESTS__);
      break;
    case 61:
      HASH_RETURN(0x6DE3CFB71905593DLL, g->GV(test_files),
                  test_files);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 65:
      HASH_RETURN(0x52B182B0A94E9C41LL, g->GV(php),
                  php);
      break;
    case 68:
      HASH_RETURN(0x7E7EB0DE39E966C4LL, g->GV(sum_results),
                  sum_results);
      break;
    case 70:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x77EE85A013BFFCC6LL, g->GV(ignored_by_ext),
                  ignored_by_ext);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 75:
      HASH_RETURN(0x47B4FBF2606A2E4BLL, g->GV(fileToDelete),
                  fileToDelete);
      break;
    case 79:
      HASH_RETURN(0x522821F5063592CFLL, g->GV(dir),
                  dir);
      break;
    case 81:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 92:
      HASH_RETURN(0x0BCDB293DC3CBDDCLL, g->GV(name),
                  name);
      break;
    case 94:
      HASH_RETURN(0x7932B72E7B0466DELL, g->GV(exts_skipped),
                  exts_skipped);
      break;
    case 101:
      HASH_RETURN(0x687BED2B0019D265LL, g->GV(start_time),
                  start_time);
      break;
    case 105:
      HASH_RETURN(0x51DA1403EA629E69LL, g->GV(info_params),
                  info_params);
      break;
    case 107:
      HASH_RETURN(0x68A4B705B0B086EBLL, g->GV(toDelete),
                  toDelete);
      HASH_RETURN(0x3C2F961831E4EF6BLL, g->GV(v),
                  v);
      break;
    case 110:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 112:
      HASH_RETURN(0x599AF95F802F1DF0LL, g->GV(test_dir),
                  test_dir);
      break;
    case 117:
      HASH_RETURN(0x3AA4D0F56E8340F5LL, g->GV(os),
                  os);
      break;
    case 119:
      HASH_RETURN(0x400AC3BA82543C77LL, g->GV(failed_test_summary),
                  failed_test_summary);
      break;
    case 126:
      HASH_RETURN(0x5154C50FC7F55FFELL, g->GV(test_results),
                  test_results);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
