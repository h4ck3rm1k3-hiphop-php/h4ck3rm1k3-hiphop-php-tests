
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "cwd",
    "php",
    "os",
    "log_format",
    "user_tests",
    "toDelete",
    "info_file",
    "php_info",
    "ini_overwrites",
    "info_params",
    "testFileVars",
    "test_dir",
    "test_files",
    "test_results",
    "__PHP_FAILED_TESTS__",
    "i",
    "testfile",
    "name",
    "exts_to_test",
    "exts_tested",
    "exts_skipped",
    "ignored_by_ext",
    "test_dirs",
    "dir",
    "start_time",
    "end_time",
    "n_total",
    "sum_results",
    "v",
    "percent_results",
    "n",
    "fileToDelete",
    "summary",
    "failed_test_summary",
    "failed_test_data",
  };
  if (idx >= 0 && idx < 47) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(cwd);
    case 13: return GV(php);
    case 14: return GV(os);
    case 15: return GV(log_format);
    case 16: return GV(user_tests);
    case 17: return GV(toDelete);
    case 18: return GV(info_file);
    case 19: return GV(php_info);
    case 20: return GV(ini_overwrites);
    case 21: return GV(info_params);
    case 22: return GV(testFileVars);
    case 23: return GV(test_dir);
    case 24: return GV(test_files);
    case 25: return GV(test_results);
    case 26: return GV(__PHP_FAILED_TESTS__);
    case 27: return GV(i);
    case 28: return GV(testfile);
    case 29: return GV(name);
    case 30: return GV(exts_to_test);
    case 31: return GV(exts_tested);
    case 32: return GV(exts_skipped);
    case 33: return GV(ignored_by_ext);
    case 34: return GV(test_dirs);
    case 35: return GV(dir);
    case 36: return GV(start_time);
    case 37: return GV(end_time);
    case 38: return GV(n_total);
    case 39: return GV(sum_results);
    case 40: return GV(v);
    case 41: return GV(percent_results);
    case 42: return GV(n);
    case 43: return GV(fileToDelete);
    case 44: return GV(summary);
    case 45: return GV(failed_test_summary);
    case 46: return GV(failed_test_data);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
