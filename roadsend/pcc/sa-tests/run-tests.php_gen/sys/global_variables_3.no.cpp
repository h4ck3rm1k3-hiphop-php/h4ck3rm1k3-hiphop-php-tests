
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      HASH_INDEX(0x4B9AE5064B0A5283LL, exts_to_test, 30);
      break;
    case 5:
      HASH_INDEX(0x178B5DB860BB1F05LL, n_total, 38);
      break;
    case 9:
      HASH_INDEX(0x117B8667E4662809LL, n, 42);
      break;
    case 13:
      HASH_INDEX(0x262F35FB33EC540DLL, ini_overwrites, 20);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x7F3CBABB98929C0FLL, failed_test_data, 46);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x67FECFEAB01DEA90LL, percent_results, 41);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 22:
      HASH_INDEX(0x6B3093D1F2F27D16LL, cwd, 12);
      break;
    case 23:
      HASH_INDEX(0x72CCC88F90317797LL, exts_tested, 31);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 27);
      break;
    case 28:
      HASH_INDEX(0x1F09B43053EBEB1CLL, log_format, 15);
      break;
    case 29:
      HASH_INDEX(0x72C2CA3ECF7C191DLL, test_dirs, 34);
      break;
    case 32:
      HASH_INDEX(0x72A6FF1221C87FA0LL, info_file, 18);
      break;
    case 34:
      HASH_INDEX(0x24CC58C2E4D68622LL, summary, 44);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 45:
      HASH_INDEX(0x1B723EAC6938822DLL, end_time, 37);
      break;
    case 52:
      HASH_INDEX(0x3AD6EDBE00AC3BB4LL, php_info, 19);
      HASH_INDEX(0x50C061D9A30E3034LL, testFileVars, 22);
      break;
    case 54:
      HASH_INDEX(0x31CF71EAC03B86B6LL, testfile, 28);
      break;
    case 55:
      HASH_INDEX(0x310559E2D6E6EC37LL, user_tests, 16);
      break;
    case 57:
      HASH_INDEX(0x25E6CA5109175A39LL, __PHP_FAILED_TESTS__, 26);
      break;
    case 61:
      HASH_INDEX(0x6DE3CFB71905593DLL, test_files, 24);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 65:
      HASH_INDEX(0x52B182B0A94E9C41LL, php, 13);
      break;
    case 68:
      HASH_INDEX(0x7E7EB0DE39E966C4LL, sum_results, 39);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x77EE85A013BFFCC6LL, ignored_by_ext, 33);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 75:
      HASH_INDEX(0x47B4FBF2606A2E4BLL, fileToDelete, 43);
      break;
    case 79:
      HASH_INDEX(0x522821F5063592CFLL, dir, 35);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 92:
      HASH_INDEX(0x0BCDB293DC3CBDDCLL, name, 29);
      break;
    case 94:
      HASH_INDEX(0x7932B72E7B0466DELL, exts_skipped, 32);
      break;
    case 101:
      HASH_INDEX(0x687BED2B0019D265LL, start_time, 36);
      break;
    case 105:
      HASH_INDEX(0x51DA1403EA629E69LL, info_params, 21);
      break;
    case 107:
      HASH_INDEX(0x68A4B705B0B086EBLL, toDelete, 17);
      HASH_INDEX(0x3C2F961831E4EF6BLL, v, 40);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 112:
      HASH_INDEX(0x599AF95F802F1DF0LL, test_dir, 23);
      break;
    case 117:
      HASH_INDEX(0x3AA4D0F56E8340F5LL, os, 14);
      break;
    case 119:
      HASH_INDEX(0x400AC3BA82543C77LL, failed_test_summary, 45);
      break;
    case 126:
      HASH_INDEX(0x5154C50FC7F55FFELL, test_results, 25);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 47) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
