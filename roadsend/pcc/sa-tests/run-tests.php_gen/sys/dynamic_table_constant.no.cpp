
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 3:
      HASH_RETURN(0x6511131491A84B6FLL, g->k_DETAILED, DETAILED);
      HASH_RETURN(0x5A0BBFB64B8EC317LL, g->k_TESTED_PHP_VERSION, TESTED_PHP_VERSION);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
