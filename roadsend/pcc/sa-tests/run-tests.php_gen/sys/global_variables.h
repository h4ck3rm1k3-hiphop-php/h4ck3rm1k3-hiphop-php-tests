
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(cwd)
    GVS(php)
    GVS(os)
    GVS(log_format)
    GVS(user_tests)
    GVS(toDelete)
    GVS(info_file)
    GVS(php_info)
    GVS(ini_overwrites)
    GVS(info_params)
    GVS(testFileVars)
    GVS(test_dir)
    GVS(test_files)
    GVS(test_results)
    GVS(__PHP_FAILED_TESTS__)
    GVS(i)
    GVS(testfile)
    GVS(name)
    GVS(exts_to_test)
    GVS(exts_tested)
    GVS(exts_skipped)
    GVS(ignored_by_ext)
    GVS(test_dirs)
    GVS(dir)
    GVS(start_time)
    GVS(end_time)
    GVS(n_total)
    GVS(sum_results)
    GVS(v)
    GVS(percent_results)
    GVS(n)
    GVS(fileToDelete)
    GVS(summary)
    GVS(failed_test_summary)
    GVS(failed_test_data)
  END_GVS(35)

  // Dynamic Constants
  Variant k_TESTED_PHP_VERSION;
  Variant k_DETAILED;

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$roadsend$pcc$sa_tests$run_tests_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 47;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[6];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
