
#ifndef __GENERATED_php_roadsend_pcc_sa_tests_nophp_h__
#define __GENERATED_php_roadsend_pcc_sa_tests_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/sa-tests.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$sa_tests(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_sa_tests_nophp_h__
