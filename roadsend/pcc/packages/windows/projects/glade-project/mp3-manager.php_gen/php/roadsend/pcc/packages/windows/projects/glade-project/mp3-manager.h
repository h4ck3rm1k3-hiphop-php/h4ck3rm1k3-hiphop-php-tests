
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_glade_project_mp3_manager_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_glade_project_mp3_manager_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/glade-project/mp3-manager.fw.h>

// Declarations
#include <cls/songlist.h>
#include <cls/filedialog.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_addsong(CVarRef v_widget, Variant v_form, Variant v_window);
void f_handlenewsong(CVarRef v_widget);
Variant pm_php$roadsend$pcc$packages$windows$projects$glade_project$mp3_manager_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_alert(CStrRef v_message);
Object co_songlist(CArrRef params, bool init = true);
Object co_filedialog(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_glade_project_mp3_manager_h__
