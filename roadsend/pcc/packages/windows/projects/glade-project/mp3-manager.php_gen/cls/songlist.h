
#ifndef __GENERATED_cls_songlist_h__
#define __GENERATED_cls_songlist_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/packages/windows/projects/glade-project/mp3-manager.php line 28 */
class c_songlist : virtual public ObjectData {
  BEGIN_CLASS_MAP(songlist)
  END_CLASS_MAP(songlist)
  DECLARE_CLASS(songlist, SongList, ObjectData)
  void init();
  public: Variant m_view;
  public: Variant m_model;
  public: void t_songlist(Variant v_clist);
  public: ObjectData *create(Variant v_clist);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addsong(CVarRef v_title, CVarRef v_artist, CVarRef v_filename);
  public: void t_redisplay();
  public: void t_deleteselected();
  public: void t_save();
  public: void t_load();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_songlist_h__
