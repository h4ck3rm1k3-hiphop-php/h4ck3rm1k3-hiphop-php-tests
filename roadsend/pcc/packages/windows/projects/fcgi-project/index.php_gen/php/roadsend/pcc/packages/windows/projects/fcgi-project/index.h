
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_fcgi_project_index_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_fcgi_project_index_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/fcgi-project/index.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$packages$windows$projects$fcgi_project$index_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_fcgi_project_index_h__
