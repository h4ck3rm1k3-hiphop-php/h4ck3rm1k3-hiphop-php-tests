
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_lib_project_lib2_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_lib_project_lib2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/lib-project/lib2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_lib2func1(CVarRef v_a, CVarRef v_b);
void f_lib2func2(CVarRef v_a, CVarRef v_b);
Variant pm_php$roadsend$pcc$packages$windows$projects$lib_project$lib2_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_lib_project_lib2_h__
