
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$pcc$packages$windows$projects$lib_project$lib1_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$pcc$packages$windows$projects$lib_project$lib2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$pcc$packages$windows$projects$lib_project$lib3_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_INCLUDE(0x0BA56760B6DB6CBBLL, "roadsend/pcc/packages/windows/projects/lib-project/lib2.php", php$roadsend$pcc$packages$windows$projects$lib_project$lib2_php);
      break;
    case 6:
      HASH_INCLUDE(0x4262E27D4D740656LL, "roadsend/pcc/packages/windows/projects/lib-project/lib1.php", php$roadsend$pcc$packages$windows$projects$lib_project$lib1_php);
      break;
    case 7:
      HASH_INCLUDE(0x218750B6FAB0A83FLL, "roadsend/pcc/packages/windows/projects/lib-project/lib3.php", php$roadsend$pcc$packages$windows$projects$lib_project$lib3_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
