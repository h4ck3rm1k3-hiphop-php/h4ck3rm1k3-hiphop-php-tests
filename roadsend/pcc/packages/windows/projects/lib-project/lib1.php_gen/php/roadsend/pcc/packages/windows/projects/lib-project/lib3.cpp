
#include <php/roadsend/pcc/packages/windows/projects/lib-project/lib3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/packages/windows/projects/lib-project/lib3.php line 8 */
void f_lib3func1(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(lib3func1);
  echo("this is a sample function\n");
} /* function */
/* SRC: roadsend/pcc/packages/windows/projects/lib-project/lib3.php line 14 */
void f_lib3func2(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(lib3func2);
  echo("this is another sample function\n");
} /* function */
Variant pm_php$roadsend$pcc$packages$windows$projects$lib_project$lib3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/packages/windows/projects/lib-project/lib3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$packages$windows$projects$lib_project$lib3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
