
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_lib_project_lib3_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_lib_project_lib3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/lib-project/lib3.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_lib3func1(CVarRef v_a, CVarRef v_b);
void f_lib3func2(CVarRef v_a, CVarRef v_b);
Variant pm_php$roadsend$pcc$packages$windows$projects$lib_project$lib3_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_lib_project_lib3_h__
