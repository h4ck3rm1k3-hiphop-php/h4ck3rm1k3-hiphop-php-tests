
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_destroy(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_delete_event(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_hello_world(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[8])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 8; i++) funcTable[i] = &invoke_builtin;
    funcTable[2] = &i_destroy;
    funcTable[3] = &i_delete_event;
    funcTable[4] = &i_hello_world;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 7](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
