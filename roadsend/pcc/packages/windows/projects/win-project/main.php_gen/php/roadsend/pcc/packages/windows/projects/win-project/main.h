
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_win_project_main_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_win_project_main_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/win-project/main.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$packages$windows$projects$win_project$main_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_win_project_main_h__
