
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_win_project_main_fw_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_win_project_main_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_MB_ICONASTERISK;
extern const StaticString k_IDYES;
extern const StaticString k_MB_YESNOCANCEL;
extern const StaticString k_IDNO;
extern const StaticString k_PCC_VERSION_MINOR;
extern const StaticString k_HKEY_LOCAL_MACHINE;
extern const StaticString k_PCC_VERSION_TAG;
extern const StaticString k_IDCANCEL;
extern const StaticString k_PCC_VERSION_MAJOR;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_win_project_main_fw_h__
