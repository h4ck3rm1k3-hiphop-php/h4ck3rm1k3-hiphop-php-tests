
#include <php/roadsend/pcc/packages/windows/projects/win-project/main.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_MB_ICONASTERISK = "MB_ICONASTERISK";
const StaticString k_IDYES = "IDYES";
const StaticString k_MB_YESNOCANCEL = "MB_YESNOCANCEL";
const StaticString k_IDNO = "IDNO";
const StaticString k_PCC_VERSION_MINOR = "PCC_VERSION_MINOR";
const StaticString k_HKEY_LOCAL_MACHINE = "HKEY_LOCAL_MACHINE";
const StaticString k_PCC_VERSION_TAG = "PCC_VERSION_TAG";
const StaticString k_IDCANCEL = "IDCANCEL";
const StaticString k_PCC_VERSION_MAJOR = "PCC_VERSION_MAJOR";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$packages$windows$projects$win_project$main_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/packages/windows/projects/win-project/main.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$packages$windows$projects$win_project$main_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ret __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ret") : g->GV(ret);
  Variant &v_install_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("install_dir") : g->GV(install_dir);

  (v_ret = LINE(17,invoke_failed("win_messagebox", Array(ArrayInit(3).set(0, "Welcome to the Roadsend Compiler").set(1, k_PCC_VERSION_TAG).set(2, bitwise_or(k_MB_YESNOCANCEL, k_MB_ICONASTERISK)).create()), 0x00000000DE8E3E96LL)));
  {
    Variant tmp2 = (v_ret);
    int tmp3 = -1;
    if (equal(tmp2, (k_IDYES))) {
      tmp3 = 0;
    } else if (equal(tmp2, (k_IDNO))) {
      tmp3 = 1;
    } else if (equal(tmp2, (k_IDCANCEL))) {
      tmp3 = 2;
    }
    switch (tmp3) {
    case 0:
      {
        LINE(20,invoke_failed("win_messagebox", Array(ArrayInit(2).set(0, "You answered Yes").set(1, "Go").create()), 0x00000000DE8E3E96LL));
        goto break1;
      }
    case 1:
      {
        LINE(23,invoke_failed("win_messagebox", Array(ArrayInit(2).set(0, "You answered No").set(1, "Stop").create()), 0x00000000DE8E3E96LL));
        goto break1;
      }
    case 2:
      {
        LINE(26,invoke_failed("win_messagebox", Array(ArrayInit(2).set(0, "You answered Cancel").set(1, "Cancel").create()), 0x00000000DE8E3E96LL));
        goto break1;
      }
    }
    break1:;
  }
  (v_install_dir = LINE(34,(assignCallTemp(eo_0, k_HKEY_LOCAL_MACHINE),assignCallTemp(eo_1, LINE(33,concat4("SOFTWARE\\Roadsend\\Compiler\\", k_PCC_VERSION_MAJOR, ".", k_PCC_VERSION_MINOR))),invoke_failed("win_get_registry_key", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, "root").create()), 0x0000000017C5C5C3LL))));
  LINE(36,(assignCallTemp(eo_0, concat3("The Roadsend install dir is [", toString(v_install_dir), "]\n")),invoke_failed("win_messagebox", Array(ArrayInit(2).set(0, eo_0).set(1, "Version").create()), 0x00000000DE8E3E96LL)));
  echo(concat("last error: ", toString(LINE(38,invoke_failed("win_getlasterror", Array(), 0x00000000899D122ELL)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
