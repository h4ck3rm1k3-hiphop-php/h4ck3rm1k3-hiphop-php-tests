
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_MB_ICONASTERISK;
extern const StaticString k_IDYES;
extern const StaticString k_MB_YESNOCANCEL;
extern const StaticString k_IDNO;
extern const StaticString k_PCC_VERSION_MINOR;
extern const StaticString k_HKEY_LOCAL_MACHINE;
extern const StaticString k_PCC_VERSION_TAG;
extern const StaticString k_IDCANCEL;
extern const StaticString k_PCC_VERSION_MAJOR;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 31) {
    case 2:
      HASH_RETURN(0x240D166FF8D9D9E2LL, k_IDNO, IDNO);
      break;
    case 3:
      HASH_RETURN(0x487654FABD9BDC03LL, k_MB_YESNOCANCEL, MB_YESNOCANCEL);
      HASH_RETURN(0x32C542381EF7B9E3LL, k_IDCANCEL, IDCANCEL);
      break;
    case 6:
      HASH_RETURN(0x63003F76E77774E6LL, k_PCC_VERSION_MINOR, PCC_VERSION_MINOR);
      break;
    case 12:
      HASH_RETURN(0x17D97BE293021A4CLL, k_PCC_VERSION_MAJOR, PCC_VERSION_MAJOR);
      break;
    case 18:
      HASH_RETURN(0x1A109F4242331732LL, k_MB_ICONASTERISK, MB_ICONASTERISK);
      break;
    case 25:
      HASH_RETURN(0x14EC515EAAA03A19LL, k_IDYES, IDYES);
      break;
    case 30:
      HASH_RETURN(0x23AF10083B1B7CFELL, k_PCC_VERSION_TAG, PCC_VERSION_TAG);
      break;
    case 31:
      HASH_RETURN(0x75D309876FCF399FLL, k_HKEY_LOCAL_MACHINE, HKEY_LOCAL_MACHINE);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
