
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_micro_project_info_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_micro_project_info_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/micro-project/info.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$packages$windows$projects$micro_project$info_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_micro_project_info_h__
