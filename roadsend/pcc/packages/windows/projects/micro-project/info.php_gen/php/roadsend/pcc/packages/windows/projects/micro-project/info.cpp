
#include <php/roadsend/pcc/packages/windows/projects/micro-project/info.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$packages$windows$projects$micro_project$info_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/packages/windows/projects/micro-project/info.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$packages$windows$projects$micro_project$info_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(3,x_phpinfo());
  echo("<pre>\r\n");
  echo("_SERVER:\n");
  LINE(10,x_var_dump(1, g->gv__SERVER));
  echo("\n\n");
  echo("_GET:\n");
  LINE(14,x_var_dump(1, g->gv__GET));
  echo("\n\n");
  echo("_POST:\n");
  LINE(18,x_var_dump(1, g->gv__POST));
  echo("\n\n");
  echo("_COOKIE:\n");
  LINE(22,x_var_dump(1, g->gv__COOKIE));
  echo("\n\n");
  echo("</pre>\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
