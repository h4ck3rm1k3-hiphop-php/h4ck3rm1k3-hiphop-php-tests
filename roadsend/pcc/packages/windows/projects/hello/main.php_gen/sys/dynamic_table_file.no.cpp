
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$pcc$packages$windows$projects$hello$main_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$roadsend$pcc$packages$windows$projects$hello$inc_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_INCLUDE(0x0EBAA5EE14953A7DLL, "roadsend/pcc/packages/windows/projects/hello/main.php", php$roadsend$pcc$packages$windows$projects$hello$main_php);
      break;
    case 2:
      HASH_INCLUDE(0x70F2591BAD26CE72LL, "roadsend/pcc/packages/windows/projects/hello/inc.php", php$roadsend$pcc$packages$windows$projects$hello$inc_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
