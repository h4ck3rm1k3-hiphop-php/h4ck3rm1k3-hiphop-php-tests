
#ifndef __GENERATED_cls_myclass_h__
#define __GENERATED_cls_myclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/packages/windows/projects/hello/main.php line 61 */
class c_myclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass)
  END_CLASS_MAP(myclass)
  DECLARE_CLASS(myclass, myClass, ObjectData)
  void init();
  public: int64 m_size;
  public: int64 m_height;
  public: void t_myclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_classfunc();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass_h__
