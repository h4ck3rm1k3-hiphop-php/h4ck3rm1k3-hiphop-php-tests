
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_hello_main_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_hello_main_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/hello/main.fw.h>

// Declarations
#include <cls/myclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$packages$windows$projects$hello$main_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_my_function(CVarRef v_adjective);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_hello_main_h__
