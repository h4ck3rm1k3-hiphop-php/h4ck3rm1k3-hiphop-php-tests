
#include <php/roadsend/pcc/packages/windows/projects/hello/inc.h>
#include <php/roadsend/pcc/packages/windows/projects/hello/main.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/packages/windows/projects/hello/main.php line 61 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("size", m_size));
  props.push_back(NEW(ArrayElement)("height", m_height));
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x674F0FBB080B0779LL, height, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x3FE8023BDF261C0FLL, size, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x674F0FBB080B0779LL, m_height,
                         height, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x3FE8023BDF261C0FLL, m_size,
                         size, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x674F0FBB080B0779LL, m_height,
                      height, 6);
      break;
    case 3:
      HASH_SET_STRING(0x3FE8023BDF261C0FLL, m_size,
                      size, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::create() {
  init();
  t_myclass();
  return this;
}
ObjectData *c_myclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  clone->m_size = m_size;
  clone->m_height = m_height;
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x4470D343CFF0942FLL, classfunc) {
        return (t_classfunc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x4470D343CFF0942FLL, classfunc) {
        return (t_classfunc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
  m_size = 50LL;
  m_height = 2LL;
}
/* SRC: roadsend/pcc/packages/windows/projects/hello/main.php line 66 */
void c_myclass::t_myclass() {
  INSTANCE_METHOD_INJECTION(myClass, myClass::myClass);
  bool oldInCtor = gasInCtor(true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/packages/windows/projects/hello/main.php line 80 */
void c_myclass::t_classfunc() {
  INSTANCE_METHOD_INJECTION(myClass, myClass::classFunc);
  echo("Call to classFunc\n");
} /* function */
/* SRC: roadsend/pcc/packages/windows/projects/hello/main.php line 43 */
String f_my_function(CVarRef v_adjective) {
  FUNCTION_INJECTION(my_function);
  String v_s;

  (v_s = LINE(45,concat3("have a ", toString(v_adjective), " day!")));
  return v_s;
} /* function */
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$packages$windows$projects$hello$main_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/packages/windows/projects/hello/main.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$packages$windows$projects$hello$main_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  LINE(26,pm_php$roadsend$pcc$packages$windows$projects$hello$inc_php(false, variables));
  LINE(29,f_includefunc("foo"));
  (v_a = "Welcome to the Roadsend Compiler!\n\n");
  print(LINE(35,concat3(":: ", toString(v_a), "\n")));
  (v_b = ScalarArrays::sa_[0]);
  LINE(41,x_var_dump(1, v_b));
  (v_c = ((Object)(LINE(86,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  LINE(87,v_c.o_invoke_few_args("classFunc", 0x4470D343CFF0942FLL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
