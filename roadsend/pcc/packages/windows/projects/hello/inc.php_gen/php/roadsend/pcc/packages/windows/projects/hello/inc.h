
#ifndef __GENERATED_php_roadsend_pcc_packages_windows_projects_hello_inc_h__
#define __GENERATED_php_roadsend_pcc_packages_windows_projects_hello_inc_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/packages/windows/projects/hello/inc.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$packages$windows$projects$hello$inc_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_includefunc(CVarRef v_a);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_packages_windows_projects_hello_inc_h__
