
#include <php/roadsend/pcc/packages/windows/projects/hello/inc.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/packages/windows/projects/hello/inc.php line 18 */
void f_includefunc(CVarRef v_a) {
  FUNCTION_INJECTION(includeFunc);
  echo(LINE(19,concat3("includeFunc was called: ", toString(v_a), "\n")));
} /* function */
Variant pm_php$roadsend$pcc$packages$windows$projects$hello$inc_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/packages/windows/projects/hello/inc.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$packages$windows$projects$hello$inc_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("inc.php was included!\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
