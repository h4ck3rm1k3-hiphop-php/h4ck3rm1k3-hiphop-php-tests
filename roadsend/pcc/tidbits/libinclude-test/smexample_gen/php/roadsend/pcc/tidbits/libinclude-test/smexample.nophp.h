
#ifndef __GENERATED_php_roadsend_pcc_tidbits_libinclude_test_smexample_nophp_h__
#define __GENERATED_php_roadsend_pcc_tidbits_libinclude_test_smexample_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tidbits/libinclude-test/smexample.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tidbits$libinclude_test$smexample(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tidbits_libinclude_test_smexample_nophp_h__
