
#ifndef __GENERATED_php_roadsend_pcc_tidbits_libinclude_test_useit_useit_h__
#define __GENERATED_php_roadsend_pcc_tidbits_libinclude_test_useit_useit_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tidbits/libinclude-test/useit/useit.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tidbits$libinclude_test$useit$useit_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tidbits_libinclude_test_useit_useit_h__
