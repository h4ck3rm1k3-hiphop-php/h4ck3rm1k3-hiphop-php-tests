
#include <php/roadsend/pcc/tidbits/libinclude-test/smexample/redist/pear/apear.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tidbits$libinclude_test$smexample$redist$pear$apear_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tidbits/libinclude-test/smexample/redist/pear/apear.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tidbits$libinclude_test$smexample$redist$pear$apear_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("path stripping appears to be working.\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
