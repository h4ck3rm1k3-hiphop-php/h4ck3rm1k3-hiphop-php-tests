
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$pcc$tidbits$libinclude_test$smexample$home$index_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_INCLUDE(0x396D5F783451C94CLL, "roadsend/pcc/tidbits/libinclude-test/smexample/home/index.php", php$roadsend$pcc$tidbits$libinclude_test$smexample$home$index_php);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$roadsend$pcc$tidbits$libinclude_test$smexample$home$index_php(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
