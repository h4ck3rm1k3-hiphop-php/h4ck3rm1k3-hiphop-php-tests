
#ifndef __GENERATED_cls_foo_h__
#define __GENERATED_cls_foo_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/class-parent.php line 3 */
class c_foo : virtual public ObjectData {
  BEGIN_CLASS_MAP(foo)
  END_CLASS_MAP(foo)
  DECLARE_CLASS(foo, foo, ObjectData)
  void init();
  public: int64 m_zot;
  public: void t_foo();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_anothermethod(CVarRef v_a, CVarRef v_b = 32LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_foo_h__
