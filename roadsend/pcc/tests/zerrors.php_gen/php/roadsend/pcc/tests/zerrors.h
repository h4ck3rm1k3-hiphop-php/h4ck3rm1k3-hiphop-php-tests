
#ifndef __GENERATED_php_roadsend_pcc_tests_zerrors_h__
#define __GENERATED_php_roadsend_pcc_tests_zerrors_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zerrors.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_scale_by_log(CVarRef v_vect, double v_scale);
Variant pm_php$roadsend$pcc$tests$zerrors_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_myerrorhandler(CVarRef v_errno, CVarRef v_errstr, CVarRef v_errfile, CVarRef v_errline, CVarRef v_vars);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zerrors_h__
