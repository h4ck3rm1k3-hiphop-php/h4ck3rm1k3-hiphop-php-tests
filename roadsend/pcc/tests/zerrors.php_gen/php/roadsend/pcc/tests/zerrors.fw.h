
#ifndef __GENERATED_php_roadsend_pcc_tests_zerrors_fw_h__
#define __GENERATED_php_roadsend_pcc_tests_zerrors_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_ERROR;
extern const int64 k_FATAL;
extern const int64 k_WARNING;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zerrors_fw_h__
