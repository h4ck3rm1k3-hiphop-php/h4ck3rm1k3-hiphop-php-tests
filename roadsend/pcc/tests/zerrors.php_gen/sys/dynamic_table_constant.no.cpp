
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_ERROR;
extern const int64 k_FATAL;
extern const int64 k_WARNING;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 1:
      HASH_RETURN(0x009D9FB08AE89B71LL, k_FATAL, FATAL);
      break;
    case 6:
      HASH_RETURN(0x0FBE0FB6B386A8E6LL, k_WARNING, WARNING);
      break;
    case 7:
      HASH_RETURN(0x016509DCA13DB6DFLL, k_ERROR, ERROR);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
