
#include <php/roadsend/pcc/tests/shutdown_function.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/shutdown_function.php line 8 */
void f_shut2() {
  FUNCTION_INJECTION(shut2);
  echo("just kidding! this is really it though\n");
} /* function */
/* SRC: roadsend/pcc/tests/shutdown_function.php line 4 */
void f_shut() {
  FUNCTION_INJECTION(shut);
  echo("shutdown complete\n");
} /* function */
Variant i_shut2(CArrRef params) {
  return (f_shut2(), null);
}
Variant i_shut(CArrRef params) {
  return (f_shut(), null);
}
Variant pm_php$roadsend$pcc$tests$shutdown_function_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/shutdown_function.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$shutdown_function_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(12,x_register_shutdown_function(1, "shut"));
  LINE(13,x_register_shutdown_function(1, "shut2"));
  echo("still running, last command\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
