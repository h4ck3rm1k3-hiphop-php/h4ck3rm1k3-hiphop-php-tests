
#include <php/roadsend/pcc/tests/class-static-members.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/class-static-members.php line 48 */
Variant c_bar2::os_get(const char *s, int64 hash) {
  return c_foo2::os_get(s, hash);
}
Variant &c_bar2::os_lval(const char *s, int64 hash) {
  return c_foo2::os_lval(s, hash);
}
void c_bar2::o_get(ArrayElementVec &props) const {
  c_foo2::o_get(props);
}
bool c_bar2::o_exists(CStrRef s, int64 hash) const {
  return c_foo2::o_exists(s, hash);
}
Variant c_bar2::o_get(CStrRef s, int64 hash) {
  return c_foo2::o_get(s, hash);
}
Variant c_bar2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_foo2::o_set(s, hash, v, forInit);
}
Variant &c_bar2::o_lval(CStrRef s, int64 hash) {
  return c_foo2::o_lval(s, hash);
}
Variant c_bar2::os_constant(const char *s) {
  return c_foo2::os_constant(s);
}
IMPLEMENT_CLASS(bar2)
ObjectData *c_bar2::cloneImpl() {
  c_bar2 *obj = NEW(c_bar2)();
  cloneSet(obj);
  return obj;
}
void c_bar2::cloneSet(c_bar2 *clone) {
  c_foo2::cloneSet(clone);
}
Variant c_bar2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x13F311710ED1BC02LL, staticvalue) {
        return (t_staticvalue());
      }
      HASH_GUARD(0x0FA51212D6881C7ELL, foostatic) {
        return (t_foostatic());
      }
      break;
    default:
      break;
  }
  return c_foo2::o_invoke(s, params, hash, fatal);
}
Variant c_bar2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x13F311710ED1BC02LL, staticvalue) {
        return (t_staticvalue());
      }
      HASH_GUARD(0x0FA51212D6881C7ELL, foostatic) {
        return (t_foostatic());
      }
      break;
    default:
      break;
  }
  return c_foo2::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_foo2::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar2$os_get(const char *s) {
  return c_bar2::os_get(s, -1);
}
Variant &cw_bar2$os_lval(const char *s) {
  return c_bar2::os_lval(s, -1);
}
Variant cw_bar2$os_constant(const char *s) {
  return c_bar2::os_constant(s);
}
Variant cw_bar2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar2::os_invoke(c, s, params, -1, fatal);
}
void c_bar2::init() {
  c_foo2::init();
}
/* SRC: roadsend/pcc/tests/class-static-members.php line 50 */
String c_bar2::t_foostatic() {
  INSTANCE_METHOD_INJECTION(Bar2, Bar2::fooStatic);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->s_foo2_DupIdmy_static;
} /* function */
/* SRC: roadsend/pcc/tests/class-static-members.php line 39 */
Variant c_foo2::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x556B4BEB9B40C47FLL, g->s_foo2_DupIdmy_static,
                  my_static);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo2::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo2::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo2::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo2::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo2)
ObjectData *c_foo2::cloneImpl() {
  c_foo2 *obj = NEW(c_foo2)();
  cloneSet(obj);
  return obj;
}
void c_foo2::cloneSet(c_foo2 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x13F311710ED1BC02LL, staticvalue) {
        return (t_staticvalue());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x13F311710ED1BC02LL, staticvalue) {
        return (t_staticvalue());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo2$os_get(const char *s) {
  return c_foo2::os_get(s, -1);
}
Variant &cw_foo2$os_lval(const char *s) {
  return c_foo2::os_lval(s, -1);
}
Variant cw_foo2$os_constant(const char *s) {
  return c_foo2::os_constant(s);
}
Variant cw_foo2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo2::os_invoke(c, s, params, -1, fatal);
}
void c_foo2::init() {
}
void c_foo2::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_foo2_DupIdmy_static = "foo";
}
void csi_foo2() {
  c_foo2::os_static_initializer();
}
/* SRC: roadsend/pcc/tests/class-static-members.php line 43 */
String c_foo2::t_staticvalue() {
  INSTANCE_METHOD_INJECTION(Foo2, Foo2::staticValue);
  DECLARE_GLOBAL_VARIABLES(g);
  return g->s_foo2_DupIdmy_static;
} /* function */
/* SRC: roadsend/pcc/tests/class-static-members.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 15) {
    case 7:
      HASH_RETURN(0x5C657D380DA684F7LL, g->s_foo_DupIdp1,
                  p1);
      HASH_RETURN(0x0FEFA20DB1B08AC7LL, g->s_foo_DupIdp2,
                  p2);
      HASH_RETURN(0x5AEC3B20C03ED687LL, g->s_foo_DupIdp7,
                  p7);
      break;
    case 13:
      HASH_RETURN(0x7712320985A0560DLL, g->s_foo_DupIdp5,
                  p5);
      break;
    case 14:
      HASH_RETURN(0x7ADD4C161B321ABELL, g->s_foo_DupIdp6,
                  p6);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x22E9535DED5669B0LL, inc) {
        return (t_inc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x22E9535DED5669B0LL, inc) {
        return (t_inc(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
void c_foo::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_foo_DupIdp1 = ScalarArrays::sa_[0];
  g->s_foo_DupIdp2 = "foo";
  g->s_foo_DupIdp5 = "bar";
  g->s_foo_DupIdp6 = "baz";
  g->s_foo_DupIdp7 = 0LL;
}
void csi_foo() {
  c_foo::os_static_initializer();
}
/* SRC: roadsend/pcc/tests/class-static-members.php line 9 */
void c_foo::t_inc() {
  INSTANCE_METHOD_INJECTION(foo, foo::inc);
  DECLARE_GLOBAL_VARIABLES(g);
  (g->s_foo_DupIdp7 = 20LL);
} /* function */
/* SRC: roadsend/pcc/tests/class-static-members.php line 14 */
Variant c_bar::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 3) {
    case 2:
      HASH_RETURN(0x18B6B61EF19E261ELL, g->s_bar_DupIdp3,
                  p3);
      break;
    case 3:
      HASH_RETURN(0x0FEFA20DB1B08AC7LL, g->s_bar_DupIdp2,
                  p2);
      break;
    default:
      break;
  }
  return c_foo::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_foo::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  c_foo::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_foo::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_foo::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_foo::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_foo::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_foo::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  c_foo::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x22E9535DED5669B0LL, inc) {
        return (t_inc(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x22E9535DED5669B0LL, inc) {
        return (t_inc(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_foo::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
  c_foo::init();
}
void c_bar::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_bar_DupIdp2 = "bar";
  g->s_bar_DupIdp3 = 2LL;
}
void csi_bar() {
  c_bar::os_static_initializer();
}
Object co_bar2(CArrRef params, bool init /* = true */) {
  return Object(p_bar2(NEW(c_bar2)())->dynCreate(params, init));
}
Object co_foo2(CArrRef params, bool init /* = true */) {
  return Object(p_foo2(NEW(c_foo2)())->dynCreate(params, init));
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$class_static_members_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/class-static-members.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$class_static_members_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o1") : g->GV(o1);
  Variant &v_o2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o2") : g->GV(o2);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_o1 = ((Object)(LINE(19,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(20,x_print_r(v_o1));
  LINE(21,x_var_dump(1, v_o1));
  LINE(23,x_var_dump(1, v_o1.o_get("p1", 0x5C657D380DA684F7LL)));
  (v_o2 = ((Object)(LINE(24,p_bar(p_bar(NEWOBJ(c_bar)())->create())))));
  LINE(26,x_print_r(v_o2));
  LINE(27,x_var_dump(1, v_o2));
  LINE(30,x_var_dump(1, g->s_foo_DupIdp1));
  LINE(31,x_var_dump(1, g->s_foo_DupIdp2));
  LINE(32,x_var_dump(1, g->s_foo_DupIdp1));
  LINE(33,x_var_dump(1, g->s_bar_DupIdp2));
  LINE(34,x_var_dump(1, g->s_bar_DupIdp3));
  LINE(36,v_o1.o_invoke_few_args("inc", 0x22E9535DED5669B0LL, 0));
  LINE(37,x_var_dump(1, g->s_foo_DupIdp7));
  print(concat(g->s_foo2_DupIdmy_static, "\n"));
  (v_foo = ((Object)(LINE(58,p_foo2(p_foo2(NEWOBJ(c_foo2)())->create())))));
  print(concat(toString(LINE(59,v_foo.o_invoke_few_args("staticValue", 0x13F311710ED1BC02LL, 0))), "\n"));
  print(concat(toString(v_foo.o_get("my_static", 0x556B4BEB9B40C47FLL)), "\n"));
  print(concat(g->s_foo2_DupIdmy_static, "\n"));
  (v_bar = ((Object)(LINE(65,p_bar2(p_bar2(NEWOBJ(c_bar2)())->create())))));
  print(concat(toString(LINE(66,v_bar.o_invoke_few_args("fooStatic", 0x0FA51212D6881C7ELL, 0))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
