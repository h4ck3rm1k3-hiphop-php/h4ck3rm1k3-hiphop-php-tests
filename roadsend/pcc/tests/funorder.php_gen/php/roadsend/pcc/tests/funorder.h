
#ifndef __GENERATED_php_roadsend_pcc_tests_funorder_h__
#define __GENERATED_php_roadsend_pcc_tests_funorder_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/funorder.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$funorder_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_foo(int64 v_a, int64 v_b);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_funorder_h__
