
#ifndef __GENERATED_php_roadsend_pcc_tests_variable_arity_h__
#define __GENERATED_php_roadsend_pcc_tests_variable_arity_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/variable-arity.fw.h>

// Declarations
#include <cls/aclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foo_with_args(int num_args, int64 v_a, int64 v_b, Array args = Array());
void f_foo(int num_args, Array args = Array());
Variant pm_php$roadsend$pcc$tests$variable_arity_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_aclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_variable_arity_h__
