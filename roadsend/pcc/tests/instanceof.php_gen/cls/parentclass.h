
#ifndef __GENERATED_cls_parentclass_h__
#define __GENERATED_cls_parentclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/instanceof.php line 14 */
class c_parentclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(parentclass)
  END_CLASS_MAP(parentclass)
  DECLARE_CLASS(parentclass, ParentClass, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parentclass_h__
