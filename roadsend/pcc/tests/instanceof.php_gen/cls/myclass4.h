
#ifndef __GENERATED_cls_myclass4_h__
#define __GENERATED_cls_myclass4_h__

#include <cls/myinterface2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/instanceof.php line 40 */
class c_myclass4 : virtual public c_myinterface2 {
  BEGIN_CLASS_MAP(myclass4)
    PARENT_CLASS(myinterface2)
  END_CLASS_MAP(myclass4)
  DECLARE_CLASS(myclass4, MyClass4, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass4_h__
