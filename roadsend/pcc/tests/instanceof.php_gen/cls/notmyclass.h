
#ifndef __GENERATED_cls_notmyclass_h__
#define __GENERATED_cls_notmyclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/instanceof.php line 6 */
class c_notmyclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(notmyclass)
  END_CLASS_MAP(notmyclass)
  DECLARE_CLASS(notmyclass, NotMyClass, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_notmyclass_h__
