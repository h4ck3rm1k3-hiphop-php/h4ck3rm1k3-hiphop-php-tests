
#ifndef __GENERATED_cls_myclass3_h__
#define __GENERATED_cls_myclass3_h__

#include <cls/myinterface.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/instanceof.php line 29 */
class c_myclass3 : virtual public c_myinterface {
  BEGIN_CLASS_MAP(myclass3)
    PARENT_CLASS(myinterface)
  END_CLASS_MAP(myclass3)
  DECLARE_CLASS(myclass3, MyClass3, ObjectData)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass3_h__
