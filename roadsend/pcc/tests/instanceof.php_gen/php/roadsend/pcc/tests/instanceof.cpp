
#include <php/roadsend/pcc/tests/instanceof.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/instanceof.php line 17 */
Variant c_myclass2::os_get(const char *s, int64 hash) {
  return c_parentclass::os_get(s, hash);
}
Variant &c_myclass2::os_lval(const char *s, int64 hash) {
  return c_parentclass::os_lval(s, hash);
}
void c_myclass2::o_get(ArrayElementVec &props) const {
  c_parentclass::o_get(props);
}
bool c_myclass2::o_exists(CStrRef s, int64 hash) const {
  return c_parentclass::o_exists(s, hash);
}
Variant c_myclass2::o_get(CStrRef s, int64 hash) {
  return c_parentclass::o_get(s, hash);
}
Variant c_myclass2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_parentclass::o_set(s, hash, v, forInit);
}
Variant &c_myclass2::o_lval(CStrRef s, int64 hash) {
  return c_parentclass::o_lval(s, hash);
}
Variant c_myclass2::os_constant(const char *s) {
  return c_parentclass::os_constant(s);
}
IMPLEMENT_CLASS(myclass2)
ObjectData *c_myclass2::cloneImpl() {
  c_myclass2 *obj = NEW(c_myclass2)();
  cloneSet(obj);
  return obj;
}
void c_myclass2::cloneSet(c_myclass2 *clone) {
  c_parentclass::cloneSet(clone);
}
Variant c_myclass2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parentclass::o_invoke(s, params, hash, fatal);
}
Variant c_myclass2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_parentclass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_parentclass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass2$os_get(const char *s) {
  return c_myclass2::os_get(s, -1);
}
Variant &cw_myclass2$os_lval(const char *s) {
  return c_myclass2::os_lval(s, -1);
}
Variant cw_myclass2$os_constant(const char *s) {
  return c_myclass2::os_constant(s);
}
Variant cw_myclass2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass2::os_invoke(c, s, params, -1, fatal);
}
void c_myclass2::init() {
  c_parentclass::init();
}
/* SRC: roadsend/pcc/tests/instanceof.php line 29 */
Variant c_myclass3::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass3::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass3::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myclass3::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass3::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass3::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass3::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass3::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass3)
ObjectData *c_myclass3::cloneImpl() {
  c_myclass3 *obj = NEW(c_myclass3)();
  cloneSet(obj);
  return obj;
}
void c_myclass3::cloneSet(c_myclass3 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myclass3::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass3::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass3::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass3$os_get(const char *s) {
  return c_myclass3::os_get(s, -1);
}
Variant &cw_myclass3$os_lval(const char *s) {
  return c_myclass3::os_lval(s, -1);
}
Variant cw_myclass3$os_constant(const char *s) {
  return c_myclass3::os_constant(s);
}
Variant cw_myclass3$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass3::os_invoke(c, s, params, -1, fatal);
}
void c_myclass3::init() {
}
/* SRC: roadsend/pcc/tests/instanceof.php line 40 */
Variant c_myclass4::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass4::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass4::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myclass4::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass4::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass4::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass4::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass4::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass4)
ObjectData *c_myclass4::cloneImpl() {
  c_myclass4 *obj = NEW(c_myclass4)();
  cloneSet(obj);
  return obj;
}
void c_myclass4::cloneSet(c_myclass4 *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myclass4::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass4::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass4::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass4$os_get(const char *s) {
  return c_myclass4::os_get(s, -1);
}
Variant &cw_myclass4$os_lval(const char *s) {
  return c_myclass4::os_lval(s, -1);
}
Variant cw_myclass4$os_constant(const char *s) {
  return c_myclass4::os_constant(s);
}
Variant cw_myclass4$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass4::os_invoke(c, s, params, -1, fatal);
}
void c_myclass4::init() {
}
/* SRC: roadsend/pcc/tests/instanceof.php line 14 */
Variant c_parentclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parentclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parentclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_parentclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parentclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_parentclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parentclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parentclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parentclass)
ObjectData *c_parentclass::cloneImpl() {
  c_parentclass *obj = NEW(c_parentclass)();
  cloneSet(obj);
  return obj;
}
void c_parentclass::cloneSet(c_parentclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_parentclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parentclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parentclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parentclass$os_get(const char *s) {
  return c_parentclass::os_get(s, -1);
}
Variant &cw_parentclass$os_lval(const char *s) {
  return c_parentclass::os_lval(s, -1);
}
Variant cw_parentclass$os_constant(const char *s) {
  return c_parentclass::os_constant(s);
}
Variant cw_parentclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parentclass::os_invoke(c, s, params, -1, fatal);
}
void c_parentclass::init() {
}
/* SRC: roadsend/pcc/tests/instanceof.php line 6 */
Variant c_notmyclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_notmyclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_notmyclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_notmyclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_notmyclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_notmyclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_notmyclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_notmyclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(notmyclass)
ObjectData *c_notmyclass::cloneImpl() {
  c_notmyclass *obj = NEW(c_notmyclass)();
  cloneSet(obj);
  return obj;
}
void c_notmyclass::cloneSet(c_notmyclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_notmyclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_notmyclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_notmyclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_notmyclass$os_get(const char *s) {
  return c_notmyclass::os_get(s, -1);
}
Variant &cw_notmyclass$os_lval(const char *s) {
  return c_notmyclass::os_lval(s, -1);
}
Variant cw_notmyclass$os_constant(const char *s) {
  return c_notmyclass::os_constant(s);
}
Variant cw_notmyclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_notmyclass::os_invoke(c, s, params, -1, fatal);
}
void c_notmyclass::init() {
}
/* SRC: roadsend/pcc/tests/instanceof.php line 3 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
}
Object co_myclass2(CArrRef params, bool init /* = true */) {
  return Object(p_myclass2(NEW(c_myclass2)())->dynCreate(params, init));
}
Object co_myclass3(CArrRef params, bool init /* = true */) {
  return Object(p_myclass3(NEW(c_myclass3)())->dynCreate(params, init));
}
Object co_myclass4(CArrRef params, bool init /* = true */) {
  return Object(p_myclass4(NEW(c_myclass4)())->dynCreate(params, init));
}
Object co_parentclass(CArrRef params, bool init /* = true */) {
  return Object(p_parentclass(NEW(c_parentclass)())->dynCreate(params, init));
}
Object co_notmyclass(CArrRef params, bool init /* = true */) {
  return Object(p_notmyclass(NEW(c_notmyclass)())->dynCreate(params, init));
}
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$instanceof_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/instanceof.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$instanceof_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  (v_a = ((Object)(LINE(9,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  LINE(11,x_var_dump(1, instanceOf(v_a, "MyClass")));
  LINE(12,x_var_dump(1, instanceOf(v_a, "NotMyClass")));
  (v_a = ((Object)(LINE(20,p_myclass2(p_myclass2(NEWOBJ(c_myclass2)())->create())))));
  LINE(22,x_var_dump(1, instanceOf(v_a, "MyClass2")));
  LINE(23,x_var_dump(1, instanceOf(v_a, "ParentClass")));
  (v_a = ((Object)(LINE(32,p_myclass3(p_myclass3(NEWOBJ(c_myclass3)())->create())))));
  LINE(34,x_var_dump(1, instanceOf(v_a, "MyClass3")));
  LINE(35,x_var_dump(1, instanceOf(v_a, "MyInterface")));
  (v_a = ((Object)(LINE(43,p_myclass4(p_myclass4(NEWOBJ(c_myclass4)())->create())))));
  (v_b = ((Object)(LINE(44,p_myclass4(p_myclass4(NEWOBJ(c_myclass4)())->create())))));
  (v_c = "MyClass4");
  (v_d = "NotMyClass");
  LINE(47,x_var_dump(1, instanceOf(v_a, toString(v_b))));
  LINE(48,x_var_dump(1, instanceOf(v_a, toString(v_c))));
  LINE(49,x_var_dump(1, instanceOf(v_a, toString(v_d))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
