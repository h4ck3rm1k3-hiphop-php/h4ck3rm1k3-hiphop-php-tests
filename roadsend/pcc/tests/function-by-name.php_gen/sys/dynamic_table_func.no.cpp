
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_fun_with_opt_arg(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_fun_with_ref_arg(CArrRef params);
Variant i_mofun(CArrRef params);
static Variant invoke_case_7(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x36A9CFBF09F23D67LL, fun_with_ref_arg);
  HASH_INVOKE(0x789EEF08EDE847F7LL, mofun);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[8])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 8; i++) funcTable[i] = &invoke_builtin;
    funcTable[3] = &i_fun_with_opt_arg;
    funcTable[7] = &invoke_case_7;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 7](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
