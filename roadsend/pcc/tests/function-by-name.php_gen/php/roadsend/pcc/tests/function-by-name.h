
#ifndef __GENERATED_php_roadsend_pcc_tests_function_by_name_h__
#define __GENERATED_php_roadsend_pcc_tests_function_by_name_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/function-by-name.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_fun_with_ref_arg(CVarRef v_foo, Variant v_bar);
Variant pm_php$roadsend$pcc$tests$function_by_name_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_mofun(CVarRef v_moarg);
void f_fun_with_opt_arg(CVarRef v_foo, CVarRef v_bar, CVarRef v_baz = 2LL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_function_by_name_h__
