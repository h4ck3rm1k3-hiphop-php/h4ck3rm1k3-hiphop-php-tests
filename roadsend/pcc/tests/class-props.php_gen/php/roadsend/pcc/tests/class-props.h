
#ifndef __GENERATED_php_roadsend_pcc_tests_class_props_h__
#define __GENERATED_php_roadsend_pcc_tests_class_props_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/class-props.fw.h>

// Declarations
#include <cls/foo.h>
#include <cls/bar.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$class_props_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);
Object co_bar(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_class_props_h__
