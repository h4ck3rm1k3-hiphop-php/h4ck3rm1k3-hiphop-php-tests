
#ifndef __GENERATED_php_roadsend_pcc_tests_class_props_fw_h__
#define __GENERATED_php_roadsend_pcc_tests_class_props_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(foo)
FORWARD_DECLARE_CLASS(bar)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_class_props_fw_h__
