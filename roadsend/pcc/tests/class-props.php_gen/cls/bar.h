
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__

#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/class-props.php line 21 */
class c_bar : virtual public c_foo {
  BEGIN_CLASS_MAP(bar)
    PARENT_CLASS(foo)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, BaR, foo)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
