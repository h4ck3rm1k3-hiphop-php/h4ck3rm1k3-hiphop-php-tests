
#ifndef __GENERATED_php_roadsend_pcc_tests_constructor_h__
#define __GENERATED_php_roadsend_pcc_tests_constructor_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/constructor.fw.h>

// Declarations
#include <cls/aclass.h>
#include <cls/bclass.h>
#include <cls/cclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$constructor_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_aclass(CArrRef params, bool init = true);
Object co_bclass(CArrRef params, bool init = true);
Object co_cclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_constructor_h__
