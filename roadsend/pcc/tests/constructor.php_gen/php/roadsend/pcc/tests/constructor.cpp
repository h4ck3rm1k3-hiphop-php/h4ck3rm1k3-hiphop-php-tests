
#include <php/roadsend/pcc/tests/constructor.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/constructor.php line 3 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_aclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_aclass::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
}
/* SRC: roadsend/pcc/tests/constructor.php line 4 */
void c_aclass::t___construct() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::__construct);
  bool oldInCtor = gasInCtor(true);
  print("Constructor of aclass called\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/constructor.php line 8 */
void c_aclass::t_aclass() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::aclass);
  print("Old constructor of aclass called\n");
} /* function */
/* SRC: roadsend/pcc/tests/constructor.php line 14 */
Variant c_bclass::os_get(const char *s, int64 hash) {
  return c_aclass::os_get(s, hash);
}
Variant &c_bclass::os_lval(const char *s, int64 hash) {
  return c_aclass::os_lval(s, hash);
}
void c_bclass::o_get(ArrayElementVec &props) const {
  c_aclass::o_get(props);
}
bool c_bclass::o_exists(CStrRef s, int64 hash) const {
  return c_aclass::o_exists(s, hash);
}
Variant c_bclass::o_get(CStrRef s, int64 hash) {
  return c_aclass::o_get(s, hash);
}
Variant c_bclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_aclass::o_set(s, hash, v, forInit);
}
Variant &c_bclass::o_lval(CStrRef s, int64 hash) {
  return c_aclass::o_lval(s, hash);
}
Variant c_bclass::os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
IMPLEMENT_CLASS(bclass)
ObjectData *c_bclass::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_bclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_bclass::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_bclass::cloneImpl() {
  c_bclass *obj = NEW(c_bclass)();
  cloneSet(obj);
  return obj;
}
void c_bclass::cloneSet(c_bclass *clone) {
  c_aclass::cloneSet(clone);
}
Variant c_bclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_aclass::o_invoke(s, params, hash, fatal);
}
Variant c_bclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_aclass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_aclass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bclass$os_get(const char *s) {
  return c_bclass::os_get(s, -1);
}
Variant &cw_bclass$os_lval(const char *s) {
  return c_bclass::os_lval(s, -1);
}
Variant cw_bclass$os_constant(const char *s) {
  return c_bclass::os_constant(s);
}
Variant cw_bclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bclass::os_invoke(c, s, params, -1, fatal);
}
void c_bclass::init() {
  c_aclass::init();
}
/* SRC: roadsend/pcc/tests/constructor.php line 15 */
void c_bclass::t___construct() {
  INSTANCE_METHOD_INJECTION(bclass, bclass::__construct);
  bool oldInCtor = gasInCtor(true);
  print("Constructor of bclass called\n");
  LINE(17,c_aclass::t_aclass());
  LINE(18,c_aclass::t___construct());
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/constructor.php line 22 */
Variant c_cclass::os_get(const char *s, int64 hash) {
  return c_bclass::os_get(s, hash);
}
Variant &c_cclass::os_lval(const char *s, int64 hash) {
  return c_bclass::os_lval(s, hash);
}
void c_cclass::o_get(ArrayElementVec &props) const {
  c_bclass::o_get(props);
}
bool c_cclass::o_exists(CStrRef s, int64 hash) const {
  return c_bclass::o_exists(s, hash);
}
Variant c_cclass::o_get(CStrRef s, int64 hash) {
  return c_bclass::o_get(s, hash);
}
Variant c_cclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_bclass::o_set(s, hash, v, forInit);
}
Variant &c_cclass::o_lval(CStrRef s, int64 hash) {
  return c_bclass::o_lval(s, hash);
}
Variant c_cclass::os_constant(const char *s) {
  return c_bclass::os_constant(s);
}
IMPLEMENT_CLASS(cclass)
ObjectData *c_cclass::create() {
  init();
  t_cclass();
  return this;
}
ObjectData *c_cclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_cclass::cloneImpl() {
  c_cclass *obj = NEW(c_cclass)();
  cloneSet(obj);
  return obj;
}
void c_cclass::cloneSet(c_cclass *clone) {
  c_bclass::cloneSet(clone);
}
Variant c_cclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_bclass::o_invoke(s, params, hash, fatal);
}
Variant c_cclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_bclass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_cclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_bclass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_cclass$os_get(const char *s) {
  return c_cclass::os_get(s, -1);
}
Variant &cw_cclass$os_lval(const char *s) {
  return c_cclass::os_lval(s, -1);
}
Variant cw_cclass$os_constant(const char *s) {
  return c_cclass::os_constant(s);
}
Variant cw_cclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_cclass::os_invoke(c, s, params, -1, fatal);
}
void c_cclass::init() {
  c_bclass::init();
}
/* SRC: roadsend/pcc/tests/constructor.php line 23 */
void c_cclass::t_cclass() {
  INSTANCE_METHOD_INJECTION(cclass, cclass::cclass);
  bool oldInCtor = gasInCtor(true);
  print("Old constructor of cclass called\n");
  gasInCtor(oldInCtor);
} /* function */
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Object co_bclass(CArrRef params, bool init /* = true */) {
  return Object(p_bclass(NEW(c_bclass)())->dynCreate(params, init));
}
Object co_cclass(CArrRef params, bool init /* = true */) {
  return Object(p_cclass(NEW(c_cclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$constructor_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/constructor.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$constructor_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_baz __attribute__((__unused__)) = (variables != gVariables) ? variables->get("baz") : g->GV(baz);

  echo("1\n");
  (v_foo = ((Object)(LINE(29,p_aclass(p_aclass(NEWOBJ(c_aclass)())->create())))));
  echo("2\n");
  (v_bar = ((Object)(LINE(31,p_bclass(p_bclass(NEWOBJ(c_bclass)())->create())))));
  echo("3\n");
  (v_baz = ((Object)(LINE(33,p_cclass(p_cclass(NEWOBJ(c_cclass)())->create())))));
  echo("4\n");
  LINE(35,v_baz.o_invoke_few_args("__construct", 0x0D31D0AC229C615FLL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
