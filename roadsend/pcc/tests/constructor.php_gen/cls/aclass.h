
#ifndef __GENERATED_cls_aclass_h__
#define __GENERATED_cls_aclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/constructor.php line 3 */
class c_aclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(aclass)
  END_CLASS_MAP(aclass)
  DECLARE_CLASS(aclass, aclass, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_aclass();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_aclass_h__
