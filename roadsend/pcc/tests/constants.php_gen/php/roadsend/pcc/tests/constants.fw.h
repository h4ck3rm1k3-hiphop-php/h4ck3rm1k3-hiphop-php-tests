
#ifndef __GENERATED_php_roadsend_pcc_tests_constants_fw_h__
#define __GENERATED_php_roadsend_pcc_tests_constants_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_upper;
extern const StaticString k_MID;
extern const StaticString k_DYNAMIC_CONSTANT;
extern const StaticString k_ACONSTANT;
extern const StaticString k_LOWER;
extern const StaticString k_SENSIBLE;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_constants_fw_h__
