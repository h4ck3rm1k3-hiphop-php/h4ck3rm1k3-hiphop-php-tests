
#include <php/roadsend/pcc/tests/constants.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_upper = "upper";
const StaticString k_MID = "MID";
const StaticString k_DYNAMIC_CONSTANT = "DYNAMIC_CONSTANT";
const StaticString k_ACONSTANT = "ACONSTANT";
const StaticString k_LOWER = "LOWER";
const StaticString k_SENSIBLE = "SENSIBLE";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$constants_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/constants.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$constants_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  LINE(5,g->declareConstant("lower", g->k_lower, 42LL));
  LINE(6,g->declareConstant("lower", g->k_lower, 12LL));
  print((LINE(9,concat3("Lower is ", toString(get_global_variables()->k_lower), "\n"))));
  print((LINE(10,concat3("Lower is ", k_LOWER, "\n"))));
  LINE(14,g->declareConstant("UPPER", g->k_UPPER, 42LL));
  LINE(15,g->declareConstant("UPPER", g->k_UPPER, 12LL));
  print((LINE(17,concat3("Upper is ", k_upper, "\n"))));
  print((LINE(18,concat3("Upper is ", toString(get_global_variables()->k_UPPER), "\n"))));
  LINE(22,g->declareConstant("MId", g->k_MId, 42LL));
  LINE(23,g->declareConstant("MId", g->k_MId, 12LL));
  print((LINE(25,concat3("Mid is ", toString(get_global_variables()->k_MId), "\n"))));
  print((LINE(26,concat3("Mid is ", k_MID, "\n"))));
  LINE(30,g->declareConstant("sensible", g->k_sensible, 12LL));
  LINE(31,g->declareConstant("sensible", g->k_sensible, 42LL));
  print((LINE(33,concat3("Sensible is ", toString(get_global_variables()->k_sensible), "\n"))));
  print((LINE(34,concat3("Sensible is ", k_SENSIBLE, "\n"))));
  LINE(37,throw_fatal("bad define"));
  print("ACONSTANT is :");
  LINE(38,x_var_dump(1, k_ACONSTANT));
  (v_foo = "DYNAMIC_CONSTANT");
  (v_bar = "second-val");
  LINE(42,throw_fatal("bad define"));
  echo(LINE(43,concat3("constant: ", k_DYNAMIC_CONSTANT, "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
