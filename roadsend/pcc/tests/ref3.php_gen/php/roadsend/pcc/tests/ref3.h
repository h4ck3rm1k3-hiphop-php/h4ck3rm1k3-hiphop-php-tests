
#ifndef __GENERATED_php_roadsend_pcc_tests_ref3_h__
#define __GENERATED_php_roadsend_pcc_tests_ref3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/ref3.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$ref3_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_ref3_h__
