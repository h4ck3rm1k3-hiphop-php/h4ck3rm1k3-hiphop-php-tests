
#include <php/roadsend/pcc/tests/extends.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/extends.php line 4 */
Variant c_base::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_base::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_base::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("base_var", m_base_var));
  c_ObjectData::o_get(props);
}
bool c_base::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x6E138EFBB3A8AA6ALL, base_var, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_base::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x6E138EFBB3A8AA6ALL, m_base_var,
                         base_var, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_base::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x6E138EFBB3A8AA6ALL, m_base_var,
                      base_var, 8);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_base::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_base::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(base)
ObjectData *c_base::create() {
  init();
  t_base();
  return this;
}
ObjectData *c_base::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_base::dynConstruct(CArrRef params) {
  (t_base());
}
ObjectData *c_base::cloneImpl() {
  c_base *obj = NEW(c_base)();
  cloneSet(obj);
  return obj;
}
void c_base::cloneSet(c_base *clone) {
  clone->m_base_var = m_base_var;
  ObjectData::cloneSet(clone);
}
Variant c_base::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x62D2775E17EE82D1LL, base) {
        return (t_base(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x22FA8D0CB1B110F3LL, base_method) {
        return (t_base_method(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x67AE1798ED476E06LL, arith_op) {
        return (t_arith_op(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_base::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x62D2775E17EE82D1LL, base) {
        return (t_base(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x22FA8D0CB1B110F3LL, base_method) {
        return (t_base_method(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x67AE1798ED476E06LL, arith_op) {
        return (t_arith_op(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_base::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_base$os_get(const char *s) {
  return c_base::os_get(s, -1);
}
Variant &cw_base$os_lval(const char *s) {
  return c_base::os_lval(s, -1);
}
Variant cw_base$os_constant(const char *s) {
  return c_base::os_constant(s);
}
Variant cw_base$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_base::os_invoke(c, s, params, -1, fatal);
}
void c_base::init() {
  m_base_var = "base";
}
/* SRC: roadsend/pcc/tests/extends.php line 8 */
void c_base::t_base() {
  INSTANCE_METHOD_INJECTION(base, base::base);
  bool oldInCtor = gasInCtor(true);
  print(m_base_var + toString(" boogey woogey\n"));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/extends.php line 12 */
void c_base::t_base_method() {
  INSTANCE_METHOD_INJECTION(base, base::base_method);
  print(LINE(13,concat3("a method in ", m_base_var, "\n")));
} /* function */
/* SRC: roadsend/pcc/tests/extends.php line 16 */
void c_base::t_arith_op(CVarRef v_a, CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(base, base::arith_op);
  print(concat("base adds ", LINE(17,concat6(toString(v_a), " and ", toString(v_b), ", to get: ", (toString(v_a + v_b)), "\n"))));
} /* function */
/* SRC: roadsend/pcc/tests/extends.php line 43 */
Variant c_grandchild::os_get(const char *s, int64 hash) {
  return c_child::os_get(s, hash);
}
Variant &c_grandchild::os_lval(const char *s, int64 hash) {
  return c_child::os_lval(s, hash);
}
void c_grandchild::o_get(ArrayElementVec &props) const {
  c_child::o_get(props);
}
bool c_grandchild::o_exists(CStrRef s, int64 hash) const {
  return c_child::o_exists(s, hash);
}
Variant c_grandchild::o_get(CStrRef s, int64 hash) {
  return c_child::o_get(s, hash);
}
Variant c_grandchild::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_child::o_set(s, hash, v, forInit);
}
Variant &c_grandchild::o_lval(CStrRef s, int64 hash) {
  return c_child::o_lval(s, hash);
}
Variant c_grandchild::os_constant(const char *s) {
  return c_child::os_constant(s);
}
IMPLEMENT_CLASS(grandchild)
ObjectData *c_grandchild::create(CVarRef v_foo) {
  init();
  t_grandchild(v_foo);
  return this;
}
ObjectData *c_grandchild::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_grandchild::dynConstruct(CArrRef params) {
  (t_grandchild(params.rvalAt(0)));
}
ObjectData *c_grandchild::cloneImpl() {
  c_grandchild *obj = NEW(c_grandchild)();
  cloneSet(obj);
  return obj;
}
void c_grandchild::cloneSet(c_grandchild *clone) {
  c_child::cloneSet(clone);
}
Variant c_grandchild::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x62D2775E17EE82D1LL, base) {
        return (t_base(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x22FA8D0CB1B110F3LL, base_method) {
        return (t_base_method(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x67AE1798ED476E06LL, arith_op) {
        return (t_arith_op(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x21FC1EB8E42F870CLL, grandchild) {
        return (t_grandchild(params.rvalAt(0)), null);
      }
      break;
    case 13:
      HASH_GUARD(0x50480B77712AE18DLL, child_method) {
        return (t_child_method(), null);
      }
      break;
    default:
      break;
  }
  return c_child::o_invoke(s, params, hash, fatal);
}
Variant c_grandchild::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x62D2775E17EE82D1LL, base) {
        return (t_base(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x22FA8D0CB1B110F3LL, base_method) {
        return (t_base_method(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x67AE1798ED476E06LL, arith_op) {
        return (t_arith_op(a0, a1), null);
      }
      break;
    case 12:
      HASH_GUARD(0x21FC1EB8E42F870CLL, grandchild) {
        return (t_grandchild(a0), null);
      }
      break;
    case 13:
      HASH_GUARD(0x50480B77712AE18DLL, child_method) {
        return (t_child_method(), null);
      }
      break;
    default:
      break;
  }
  return c_child::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_grandchild::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_child::os_invoke(c, s, params, hash, fatal);
}
Variant cw_grandchild$os_get(const char *s) {
  return c_grandchild::os_get(s, -1);
}
Variant &cw_grandchild$os_lval(const char *s) {
  return c_grandchild::os_lval(s, -1);
}
Variant cw_grandchild$os_constant(const char *s) {
  return c_grandchild::os_constant(s);
}
Variant cw_grandchild$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_grandchild::os_invoke(c, s, params, -1, fatal);
}
void c_grandchild::init() {
  c_child::init();
}
/* SRC: roadsend/pcc/tests/extends.php line 45 */
void c_grandchild::t_grandchild(CVarRef v_foo) {
  INSTANCE_METHOD_INJECTION(grandchild, grandchild::grandchild);
  bool oldInCtor = gasInCtor(true);
  (m_base_var = "grandchild");
  print(LINE(47,concat3("you just became a grandfather! ", toString(v_foo), ".\n")));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/extends.php line 25 */
Variant c_child::os_get(const char *s, int64 hash) {
  return c_base::os_get(s, hash);
}
Variant &c_child::os_lval(const char *s, int64 hash) {
  return c_base::os_lval(s, hash);
}
void c_child::o_get(ArrayElementVec &props) const {
  c_base::o_get(props);
}
bool c_child::o_exists(CStrRef s, int64 hash) const {
  return c_base::o_exists(s, hash);
}
Variant c_child::o_get(CStrRef s, int64 hash) {
  return c_base::o_get(s, hash);
}
Variant c_child::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_base::o_set(s, hash, v, forInit);
}
Variant &c_child::o_lval(CStrRef s, int64 hash) {
  return c_base::o_lval(s, hash);
}
Variant c_child::os_constant(const char *s) {
  return c_base::os_constant(s);
}
IMPLEMENT_CLASS(child)
ObjectData *c_child::cloneImpl() {
  c_child *obj = NEW(c_child)();
  cloneSet(obj);
  return obj;
}
void c_child::cloneSet(c_child *clone) {
  c_base::cloneSet(clone);
}
Variant c_child::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x62D2775E17EE82D1LL, base) {
        return (t_base(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x22FA8D0CB1B110F3LL, base_method) {
        return (t_base_method(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x50480B77712AE18DLL, child_method) {
        return (t_child_method(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x67AE1798ED476E06LL, arith_op) {
        return (t_arith_op(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke(s, params, hash, fatal);
}
Variant c_child::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x62D2775E17EE82D1LL, base) {
        return (t_base(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x22FA8D0CB1B110F3LL, base_method) {
        return (t_base_method(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x50480B77712AE18DLL, child_method) {
        return (t_child_method(), null);
      }
      break;
    case 6:
      HASH_GUARD(0x67AE1798ED476E06LL, arith_op) {
        return (t_arith_op(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_base::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_child::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_base::os_invoke(c, s, params, hash, fatal);
}
Variant cw_child$os_get(const char *s) {
  return c_child::os_get(s, -1);
}
Variant &cw_child$os_lval(const char *s) {
  return c_child::os_lval(s, -1);
}
Variant cw_child$os_constant(const char *s) {
  return c_child::os_constant(s);
}
Variant cw_child$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_child::os_invoke(c, s, params, -1, fatal);
}
void c_child::init() {
  c_base::init();
}
/* SRC: roadsend/pcc/tests/extends.php line 27 */
void c_child::t_arith_op(CVarRef v_a, CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(child, child::arith_op);
  print(concat("child multiplies ", LINE(28,concat6(toString(v_a), " and ", toString(v_b), ", to get: ", (toString(v_a * v_b)), "\n"))));
} /* function */
/* SRC: roadsend/pcc/tests/extends.php line 31 */
void c_child::t_child_method() {
  INSTANCE_METHOD_INJECTION(child, child::child_method);
  print(LINE(32,concat3("not a method in ", m_base_var, "\n")));
} /* function */
Object co_base(CArrRef params, bool init /* = true */) {
  return Object(p_base(NEW(c_base)())->dynCreate(params, init));
}
Object co_grandchild(CArrRef params, bool init /* = true */) {
  return Object(p_grandchild(NEW(c_grandchild)())->dynCreate(params, init));
}
Object co_child(CArrRef params, bool init /* = true */) {
  return Object(p_child(NEW(c_child)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$extends_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/extends.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$extends_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_abase __attribute__((__unused__)) = (variables != gVariables) ? variables->get("abase") : g->GV(abase);
  Variant &v_achild __attribute__((__unused__)) = (variables != gVariables) ? variables->get("achild") : g->GV(achild);
  Variant &v_agrandchild __attribute__((__unused__)) = (variables != gVariables) ? variables->get("agrandchild") : g->GV(agrandchild);

  (v_abase = ((Object)(LINE(21,p_base(p_base(NEWOBJ(c_base)())->create())))));
  LINE(22,v_abase.o_invoke_few_args("base_method", 0x22FA8D0CB1B110F3LL, 0));
  LINE(23,v_abase.o_invoke_few_args("arith_op", 0x67AE1798ED476E06LL, 2, 1LL, 2LL));
  (v_achild = ((Object)(LINE(37,p_child(p_child(NEWOBJ(c_child)())->create())))));
  LINE(38,v_achild.o_invoke_few_args("base_method", 0x22FA8D0CB1B110F3LL, 0));
  LINE(39,v_achild.o_invoke_few_args("child_method", 0x50480B77712AE18DLL, 0));
  LINE(40,v_achild.o_invoke_few_args("arith_op", 0x67AE1798ED476E06LL, 2, 1LL, 2LL));
  (v_agrandchild = ((Object)(LINE(52,p_grandchild(p_grandchild(NEWOBJ(c_grandchild)())->create("bar"))))));
  LINE(53,v_agrandchild.o_invoke_few_args("base_method", 0x22FA8D0CB1B110F3LL, 0));
  LINE(54,v_agrandchild.o_invoke_few_args("child_method", 0x50480B77712AE18DLL, 0));
  LINE(55,v_agrandchild.o_invoke_few_args("arith_op", 0x67AE1798ED476E06LL, 2, 1LL, 2LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
