
#include <php/roadsend/pcc/tests/superglobal_bindings.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/superglobal_bindings.php line 5 */
void f_yay() {
  FUNCTION_INJECTION(yay);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_a;
  Variant v_b;

  echo(toString(g->GV(foo)));
  (g->GV(foo) = 6LL);
  echo(toString(g->GV(foo)));
  (v_a = ref(get_global_array_wrapper()));
  v_a.set("foo", (7LL), 0x4154FA2EF733DA8FLL);
  (v_b = 5LL);
  (g->GV(bar) = ref(v_b));
} /* function */
Variant pm_php$roadsend$pcc$tests$superglobal_bindings_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/superglobal_bindings.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$superglobal_bindings_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_foo = 5LL);
  LINE(19,f_yay());
  echo(toString(v_foo));
  echo(toString(v_bar));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
