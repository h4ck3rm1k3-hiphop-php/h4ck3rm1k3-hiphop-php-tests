
#include <php/roadsend/pcc/tests/implicit_copy.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/implicit_copy.php line 33 */
void f_incarray(Variant v_arr) {
  FUNCTION_INJECTION(incarray);
  lval(v_arr.lvalAt(1LL, 0x5BCA7C69B794F8CELL))++;
} /* function */
/* SRC: roadsend/pcc/tests/implicit_copy.php line 25 */
void f_incvar(Variant v_var) {
  FUNCTION_INJECTION(incvar);
  v_var++;
} /* function */
Variant pm_php$roadsend$pcc$tests$implicit_copy_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/implicit_copy.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$implicit_copy_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);
  Variant &v_wib __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wib") : g->GV(wib);

  v_foo.set(1LL, (2LL), 0x5BCA7C69B794F8CELL);
  (v_bar = v_foo);
  lval(v_bar.lvalAt(1LL, 0x5BCA7C69B794F8CELL))++;
  print("foo\n");
  LINE(14,x_print_r(v_foo));
  print("bar\n");
  LINE(16,x_print_r(v_bar));
  (v_zot = 2LL);
  (v_wib = v_zot);
  v_wib++;
  print(LINE(23,concat5("wib ", toString(v_wib), " zot ", toString(v_zot), "\n")));
  LINE(29,f_incvar(v_zot));
  print(LINE(31,concat3("zot ", toString(v_zot), "\n")));
  LINE(37,f_incarray(v_foo));
  print("foo\n");
  LINE(40,x_print_r(v_foo));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
