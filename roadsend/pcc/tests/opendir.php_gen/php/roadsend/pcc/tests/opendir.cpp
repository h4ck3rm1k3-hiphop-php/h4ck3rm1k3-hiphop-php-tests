
#include <php/roadsend/pcc/tests/opendir.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$opendir_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/opendir.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$opendir_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dir") : g->GV(dir);
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);

  if (toBoolean((v_dir = LINE(3,x_opendir("/etc"))))) {
    LOOP_COUNTER(1);
    {
      while (!same(((v_file = LINE(4,x_readdir(toObject(v_dir))))), false)) {
        LOOP_COUNTER_CHECK(1);
        {
          echo(toString(v_file) + toString("\n"));
        }
      }
    }
    LINE(7,x_closedir(toObject(v_dir)));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
