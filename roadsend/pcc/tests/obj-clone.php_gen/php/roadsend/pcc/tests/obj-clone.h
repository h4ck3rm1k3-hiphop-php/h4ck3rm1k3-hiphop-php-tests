
#ifndef __GENERATED_php_roadsend_pcc_tests_obj_clone_h__
#define __GENERATED_php_roadsend_pcc_tests_obj_clone_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/obj-clone.fw.h>

// Declarations
#include <cls/subobject.h>
#include <cls/mycloneable.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$obj_clone_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_subobject(CArrRef params, bool init = true);
Object co_mycloneable(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_obj_clone_h__
