
#include <php/roadsend/pcc/tests/zcomplex_switch.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$zcomplex_switch_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/zcomplex_switch.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$zcomplex_switch_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_blah __attribute__((__unused__)) = (variables != gVariables) ? variables->get("blah") : g->GV(blah);

  (v_i = "abc");
  {
    LOOP_COUNTER(1);
    for ((v_j = 0LL); less(v_j, 10LL); v_j++) {
      LOOP_COUNTER_CHECK(1);
      {
        {
          int64 tmp3 = (1LL);
          int tmp4 = -1;
          if (equal(tmp3, (1LL))) {
            tmp4 = 0;
          } else if (equal(tmp3, (2LL))) {
            tmp4 = 1;
          } else if (equal(tmp3, (toInt64(v_i)))) {
            tmp4 = 2;
          } else if (equal(tmp3, (4LL))) {
            tmp4 = 3;
          } else if (true) {
            tmp4 = 4;
          }
          switch (tmp4) {
          case 0:
            {
              echo("In branch 1\n");
              {
                Variant tmp6 = (v_i);
                int tmp7 = -1;
                if (equal(tmp6, ("ab"))) {
                  tmp7 = 0;
                } else if (equal(tmp6, ("abcd"))) {
                  tmp7 = 1;
                } else if (equal(tmp6, ("blah"))) {
                  tmp7 = 2;
                } else if (true) {
                  tmp7 = 3;
                }
                switch (tmp7) {
                case 0:
                  {
                    echo("This doesn't work... :(\n");
                    goto break5;
                  }
                case 1:
                  {
                    echo("This works!\n");
                    goto break5;
                  }
                case 2:
                  {
                    echo("Hmmm, no worki\n");
                    goto break5;
                  }
                case 3:
                  {
                    echo("Inner default...\n");
                  }
                }
                break5:;
              }
              {
                LOOP_COUNTER(8);
                for ((v_blah = 0LL); less(v_blah, 200LL); v_blah++) {
                  LOOP_COUNTER_CHECK(8);
                  {
                    if (equal(v_blah, 100LL)) {
                      echo(LINE(24,concat3("blah=", toString(v_blah), "\n")));
                    }
                  }
                }
              }
              goto break2;
            }
          case 1:
            {
              echo("In branch 2\n");
              goto break2;
            }
          case 2:
            {
              echo("In branch $i\n");
              goto break2;
            }
          case 3:
            {
              echo("In branch 4\n");
              goto break2;
            }
          case 4:
            {
              echo("Hi, I'm default\n");
              goto break2;
            }
          }
          break2:;
        }
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
