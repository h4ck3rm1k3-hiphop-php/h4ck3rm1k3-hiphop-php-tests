
#ifndef __GENERATED_php_roadsend_pcc_tests_simple_assignment_h__
#define __GENERATED_php_roadsend_pcc_tests_simple_assignment_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/simple-assignment.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$simple_assignment_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_simple_assignment_h__
