
#ifndef __GENERATED_cls_argconstructor_h__
#define __GENERATED_cls_argconstructor_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/object.php line 28 */
class c_argconstructor : virtual public ObjectData {
  BEGIN_CLASS_MAP(argconstructor)
  END_CLASS_MAP(argconstructor)
  DECLARE_CLASS(argconstructor, argconstructor, ObjectData)
  void init();
  public: int64 m_bing;
  public: int64 m_otherplacement;
  public: String t___tostring();
  public: void t_argconstructor(CVarRef v_a, CVarRef v_b = "foo");
  public: ObjectData *create(CVarRef v_a, CVarRef v_b = "foo");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_afun(CVarRef v_anarg);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_argconstructor_h__
