
#include <php/roadsend/pcc/tests/object.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/object.php line 3 */
Variant c_zot::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_zot::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_zot::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("bing", m_bing));
  props.push_back(NEW(ArrayElement)("Bing", m_Bing));
  props.push_back(NEW(ArrayElement)("otherplacement", m_otherplacement));
  c_ObjectData::o_get(props);
}
bool c_zot::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_EXISTS_STRING(0x7DC551456639E572LL, otherplacement, 14);
      break;
    case 4:
      HASH_EXISTS_STRING(0x30C09D28777EB43CLL, Bing, 4);
      break;
    case 5:
      HASH_EXISTS_STRING(0x7BEB392C92C926C5LL, bing, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_zot::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_RETURN_STRING(0x7DC551456639E572LL, m_otherplacement,
                         otherplacement, 14);
      break;
    case 4:
      HASH_RETURN_STRING(0x30C09D28777EB43CLL, m_Bing,
                         Bing, 4);
      break;
    case 5:
      HASH_RETURN_STRING(0x7BEB392C92C926C5LL, m_bing,
                         bing, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_zot::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 2:
      HASH_SET_STRING(0x7DC551456639E572LL, m_otherplacement,
                      otherplacement, 14);
      break;
    case 4:
      HASH_SET_STRING(0x30C09D28777EB43CLL, m_Bing,
                      Bing, 4);
      break;
    case 5:
      HASH_SET_STRING(0x7BEB392C92C926C5LL, m_bing,
                      bing, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_zot::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_zot::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(zot)
ObjectData *c_zot::create() {
  init();
  t_zot();
  return this;
}
ObjectData *c_zot::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_zot::cloneImpl() {
  c_zot *obj = NEW(c_zot)();
  cloneSet(obj);
  return obj;
}
void c_zot::cloneSet(c_zot *clone) {
  clone->m_bing = m_bing;
  clone->m_Bing = m_Bing;
  clone->m_otherplacement = m_otherplacement;
  ObjectData::cloneSet(clone);
}
Variant c_zot::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        return (t_afun(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_zot::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        return (t_afun(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_zot::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_zot$os_get(const char *s) {
  return c_zot::os_get(s, -1);
}
Variant &cw_zot$os_lval(const char *s) {
  return c_zot::os_lval(s, -1);
}
Variant cw_zot$os_constant(const char *s) {
  return c_zot::os_constant(s);
}
Variant cw_zot$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_zot::os_invoke(c, s, params, -1, fatal);
}
void c_zot::init() {
  m_bing = 12LL;
  m_Bing = "this is the capitalized bing";
  m_otherplacement = 9LL;
}
/* SRC: roadsend/pcc/tests/object.php line 9 */
void c_zot::t_zot() {
  INSTANCE_METHOD_INJECTION(zot, zot::zot);
  bool oldInCtor = gasInCtor(true);
  echo("constructor\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 15 */
String c_zot::t___tostring() {
  INSTANCE_METHOD_INJECTION(zot, zot::__toString);
  return "[this is a zot class]";
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 19 */
void c_zot::t_afun(CVarRef v_anarg) {
  INSTANCE_METHOD_INJECTION(zot, zot::afun);
  echo(toString(((Object)(this))));
  echo(LINE(22,concat5("you called zot->afun with ", toString(v_anarg), ".  bing was ", toString(m_bing), ".\n")));
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 86 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_a = m_a;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x53B2FEAD43626754LL, assign) {
        return (t_assign(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x53B2FEAD43626754LL, assign) {
        return (t_assign(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_a = "bar";
}
/* SRC: roadsend/pcc/tests/object.php line 90 */
void c_foo::t_assign() {
  INSTANCE_METHOD_INJECTION(foo, foo::assign);
  Variant v_zap;
  Variant v_zap2;

  (v_zap = LINE(91,create_object((m_a), Array(ArrayInit(1).set(0, "setbaz").create()))));
  LINE(92,x_print_r(v_zap));
  (v_zap2 = LINE(93,create_object((m_a), Array())));
  LINE(94,x_print_r(v_zap));
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 81 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("baz", m_baz.isReferenced() ? ref(m_baz) : m_baz));
  c_ObjectData::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x5BE8348359F05B37LL, baz, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x5BE8348359F05B37LL, m_baz,
                         baz, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x5BE8348359F05B37LL, m_baz,
                      baz, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x5BE8348359F05B37LL, m_baz,
                         baz, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::create(CVarRef v_a //  = "noarg"
) {
  init();
  t_bar(v_a);
  return this;
}
ObjectData *c_bar::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_bar::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t_bar());
  (t_bar(params.rvalAt(0)));
}
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  clone->m_baz = m_baz.isReferenced() ? ref(m_baz) : m_baz;
  ObjectData::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        int count = params.size();
        if (count <= 0) return (t_bar(), null);
        return (t_bar(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x5E008C5BA4C3B3FDLL, bar) {
        if (count <= 0) return (t_bar(), null);
        return (t_bar(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
  m_baz = null;
}
/* SRC: roadsend/pcc/tests/object.php line 83 */
void c_bar::t_bar(CVarRef v_a //  = "noarg"
) {
  INSTANCE_METHOD_INJECTION(bar, bar::bar);
  bool oldInCtor = gasInCtor(true);
  (m_baz = v_a);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 28 */
Variant c_argconstructor::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_argconstructor::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_argconstructor::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("bing", m_bing));
  props.push_back(NEW(ArrayElement)("otherplacement", m_otherplacement));
  c_ObjectData::o_get(props);
}
bool c_argconstructor::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x7BEB392C92C926C5LL, bing, 4);
      break;
    case 2:
      HASH_EXISTS_STRING(0x7DC551456639E572LL, otherplacement, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_argconstructor::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x7BEB392C92C926C5LL, m_bing,
                         bing, 4);
      break;
    case 2:
      HASH_RETURN_STRING(0x7DC551456639E572LL, m_otherplacement,
                         otherplacement, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_argconstructor::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x7BEB392C92C926C5LL, m_bing,
                      bing, 4);
      break;
    case 2:
      HASH_SET_STRING(0x7DC551456639E572LL, m_otherplacement,
                      otherplacement, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_argconstructor::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_argconstructor::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(argconstructor)
ObjectData *c_argconstructor::create(CVarRef v_a, CVarRef v_b //  = "foo"
) {
  init();
  t_argconstructor(v_a, v_b);
  return this;
}
ObjectData *c_argconstructor::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_argconstructor::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t_argconstructor(params.rvalAt(0)));
  (t_argconstructor(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_argconstructor::cloneImpl() {
  c_argconstructor *obj = NEW(c_argconstructor)();
  cloneSet(obj);
  return obj;
}
void c_argconstructor::cloneSet(c_argconstructor *clone) {
  clone->m_bing = m_bing;
  clone->m_otherplacement = m_otherplacement;
  ObjectData::cloneSet(clone);
}
Variant c_argconstructor::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x24C88774C163BC8CLL, argconstructor) {
        int count = params.size();
        if (count <= 1) return (t_argconstructor(params.rvalAt(0)), null);
        return (t_argconstructor(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        return (t_afun(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_argconstructor::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x24C88774C163BC8CLL, argconstructor) {
        if (count <= 1) return (t_argconstructor(a0), null);
        return (t_argconstructor(a0, a1), null);
      }
      break;
    case 1:
      HASH_GUARD(0x64AA3995684BE7FDLL, afun) {
        return (t_afun(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_argconstructor::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_argconstructor$os_get(const char *s) {
  return c_argconstructor::os_get(s, -1);
}
Variant &cw_argconstructor$os_lval(const char *s) {
  return c_argconstructor::os_lval(s, -1);
}
Variant cw_argconstructor$os_constant(const char *s) {
  return c_argconstructor::os_constant(s);
}
Variant cw_argconstructor$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_argconstructor::os_invoke(c, s, params, -1, fatal);
}
void c_argconstructor::init() {
  m_bing = 12LL;
  m_otherplacement = 9LL;
}
/* SRC: roadsend/pcc/tests/object.php line 33 */
String c_argconstructor::t___tostring() {
  INSTANCE_METHOD_INJECTION(argconstructor, argconstructor::__toString);
  return "[this is an argconstructor class]";
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 37 */
void c_argconstructor::t_argconstructor(CVarRef v_a, CVarRef v_b //  = "foo"
) {
  INSTANCE_METHOD_INJECTION(argconstructor, argconstructor::argconstructor);
  bool oldInCtor = gasInCtor(true);
  echo(LINE(38,concat5("constructor ", toString(v_a), ", ", toString(v_b), "\n")));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 43 */
void c_argconstructor::t_afun(CVarRef v_anarg) {
  INSTANCE_METHOD_INJECTION(argconstructor, argconstructor::afun);
  echo(toString(((Object)(this))));
  echo(LINE(46,concat5("you called argconstructor->afun with ", toString(v_anarg), ".  bing was ", toString(m_bing), ".\n")));
} /* function */
/* SRC: roadsend/pcc/tests/object.php line 52 */
void f_zot() {
  FUNCTION_INJECTION(zot);
  echo("I am the walrus\n");
} /* function */
Object co_zot(CArrRef params, bool init /* = true */) {
  return Object(p_zot(NEW(c_zot)())->dynCreate(params, init));
}
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Object co_argconstructor(CArrRef params, bool init /* = true */) {
  return Object(p_argconstructor(NEW(c_argconstructor)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$object_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/object.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$object_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bing __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bing") : g->GV(bing);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_bap __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bap") : g->GV(bap);
  Variant &v_bpa __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bpa") : g->GV(bpa);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_bing = ((Object)(LINE(56,p_zot(p_zot(NEWOBJ(c_zot)())->create())))));
  print(LINE(58,concat5("lower: ", toString(v_bing.o_get("bing", 0x7BEB392C92C926C5LL)), ", capitalized: ", toString(v_bing.o_get("Bing", 0x30C09D28777EB43CLL)), "\n")));
  echo("foo");
  LINE(62,v_bing.o_invoke_few_args("afun", 0x64AA3995684BE7FDLL, 1, 19LL));
  LINE(63,v_bing.o_invoke_few_args("afun", 0x64AA3995684BE7FDLL, 1, 20LL));
  (v_bing = ((Object)(LINE(65,p_zot(p_zot(NEWOBJ(c_zot)())->create())))));
  (v_bing = ((Object)(LINE(66,p_zot(p_zot(NEWOBJ(c_zot)())->create())))));
  LINE(69,v_bing.o_invoke_few_args("afun", 0x64AA3995684BE7FDLL, 1, 34LL));
  LINE(72,f_zot());
  (v_bing = ((Object)(LINE(74,p_argconstructor(p_argconstructor(NEWOBJ(c_argconstructor)())->create(12LL))))));
  LINE(75,v_bing.o_invoke_few_args("afun", 0x64AA3995684BE7FDLL, 1, 12LL));
  (v_c = "argconstructor");
  (v_bap = LINE(78,create_object(toString(v_c), Array())));
  (v_bpa = LINE(79,create_object(toString(v_c), Array(ArrayInit(1).set(0, 12LL).create()))));
  (v_a = ((Object)(LINE(99,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(100,v_a.o_invoke_few_args("assign", 0x53B2FEAD43626754LL, 0));
  LINE(101,x_print_r(v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
