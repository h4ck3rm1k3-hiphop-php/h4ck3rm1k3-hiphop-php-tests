
#include <php/roadsend/pcc/tests/escapingtest.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$escapingtest_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/escapingtest.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$escapingtest_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_expression __attribute__((__unused__)) = (variables != gVariables) ? variables->get("expression") : g->GV(expression);

  if (toBoolean(v_expression)) {
    echo("    ");
    echo("<s");
    echo("trong>This is true.</strong>\n    ");
  }
  else {
    echo("    ");
    echo("<s");
    echo("trong>This is \\false.</strong>\n    ");
  }
  (v_expression = 1LL);
  if (toBoolean(v_expression)) {
    echo("    ");
    echo("<s");
    echo("trong>This is 'true'.</strong>\n    ");
  }
  else {
    echo("    ");
    echo("<s");
    echo("trong>This is false.</strong>\n    ");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
