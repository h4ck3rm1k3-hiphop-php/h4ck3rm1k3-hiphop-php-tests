
#include <php/roadsend/pcc/tests/varvararg.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/varvararg.php line 16 */
void f_zot(String v_arg3) {
  FUNCTION_INJECTION(zot);
  DECLARE_GLOBAL_VARIABLES(g);

  class VariableTable : public RVariableTable {
  public:
    String &v_arg3;
    VariableTable(String &r_arg3) : v_arg3(r_arg3) {}
    virtual Variant getImpl(const char *s) {
      int64 hash = hash_string(s);
      switch (hash & 1) {
        case 0:
          HASH_RETURN(0x2A34206F3B5C280ALL, v_arg3,
                      arg3);
          break;
        default:
          break;
      }
      return rvalAt(s);
    }
  } variableTable(v_arg3);
  RVariableTable* __attribute__((__unused__)) variables = &variableTable;
  throw_fatal("dynamic global");
  print(toString(variables->get(v_arg3)));
} /* function */
/* SRC: roadsend/pcc/tests/varvararg.php line 4 */
void f_foo(Variant v_arg1) {
  FUNCTION_INJECTION(foo);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_bazoom;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_arg1; Variant &v_bazoom;
    VariableTable(Variant &r_arg1, Variant &r_bazoom) : v_arg1(r_arg1), v_bazoom(r_bazoom) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 3) {
        case 1:
          HASH_RETURN(0x4EA6B9CB9F1718B5LL, v_bazoom,
                      bazoom);
          break;
        case 2:
          HASH_RETURN(0x5779C2A9622744AALL, v_arg1,
                      arg1);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_arg1, v_bazoom);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (variables->get(toString(v_bazoom)) = 2LL);
  print(toString(v_arg1));
} /* function */
/* SRC: roadsend/pcc/tests/varvararg.php line 10 */
void f_bar(Variant v_arg2) {
  FUNCTION_INJECTION(bar);
  DECLARE_GLOBAL_VARIABLES(g);

  class VariableTable : public LVariableTable {
  public:
    Variant &v_arg2;
    VariableTable(Variant &r_arg2) : v_arg2(r_arg2) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 1:
          HASH_RETURN(0x30BB46664DFFCCE7LL, v_arg2,
                      arg2);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_arg2);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  (variables->get(toString(v_arg2)) = 2LL);
  print(toString(variables->get(toString(v_arg2))));
} /* function */
Variant pm_php$roadsend$pcc$tests$varvararg_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/varvararg.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$varvararg_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bing __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bing") : g->GV(bing);

  LINE(23,f_foo("flop"));
  LINE(25,f_bar("zork"));
  (v_bing = "printme\n");
  LINE(28,f_zot("bing"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
