
#ifndef __GENERATED_php_roadsend_pcc_tests_varvararg_h__
#define __GENERATED_php_roadsend_pcc_tests_varvararg_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/varvararg.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_zot(String v_arg3);
void f_foo(Variant v_arg1);
void f_bar(Variant v_arg2);
Variant pm_php$roadsend$pcc$tests$varvararg_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_varvararg_h__
