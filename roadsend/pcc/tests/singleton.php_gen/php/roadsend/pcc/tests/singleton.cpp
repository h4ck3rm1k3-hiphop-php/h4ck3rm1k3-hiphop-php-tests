
#include <php/roadsend/pcc/tests/singleton.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/singleton.php line 3 */
Variant c_example::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x24965F78CA389518LL, g->s_example_DupIdinstance,
                  instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_example::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x24965F78CA389518LL, g->s_example_DupIdinstance,
                  instance);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_example::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_example::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_example::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_example::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_example::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_example::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(example)
ObjectData *c_example::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_example::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_example::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_example::cloneImpl() {
  c_example *obj = NEW(c_example)();
  cloneSet(obj);
  return obj;
}
void c_example::cloneSet(c_example *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_example::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x077F9DE77F983732LL, bark) {
        return (t_bark(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_example::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x077F9DE77F983732LL, bark) {
        return (t_bark(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_example::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_example$os_get(const char *s) {
  return c_example::os_get(s, -1);
}
Variant &cw_example$os_lval(const char *s) {
  return c_example::os_lval(s, -1);
}
Variant cw_example$os_constant(const char *s) {
  return c_example::os_constant(s);
}
Variant cw_example$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_example::os_invoke(c, s, params, -1, fatal);
}
void c_example::init() {
}
void c_example::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_example_DupIdinstance = null;
}
void csi_example() {
  c_example::os_static_initializer();
}
/* SRC: roadsend/pcc/tests/singleton.php line 9 */
void c_example::t___construct() {
  INSTANCE_METHOD_INJECTION(Example, Example::__construct);
  bool oldInCtor = gasInCtor(true);
  echo("I am constructed");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/singleton.php line 15 */
Variant c_example::ti_singleton(const char* cls) {
  STATIC_METHOD_INJECTION(Example, Example::singleton);
  DECLARE_GLOBAL_VARIABLES(g);
  String v_c;

  if (!(isset(g->s_example_DupIdinstance))) {
    (v_c = "Example");
    (g->s_example_DupIdinstance = LINE(19,create_object(v_c, Array())));
  }
  return g->s_example_DupIdinstance;
} /* function */
/* SRC: roadsend/pcc/tests/singleton.php line 26 */
void c_example::t_bark() {
  INSTANCE_METHOD_INJECTION(Example, Example::bark);
  echo("Woof!");
} /* function */
/* SRC: roadsend/pcc/tests/singleton.php line 32 */
Variant c_example::t___clone() {
  INSTANCE_METHOD_INJECTION(Example, Example::__clone);
  echo("throw clone error\n");
  return null;
} /* function */
Object co_example(CArrRef params, bool init /* = true */) {
  return Object(p_example(NEW(c_example)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$singleton_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/singleton.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$singleton_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_test __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test") : g->GV(test);
  Variant &v_test_clone __attribute__((__unused__)) = (variables != gVariables) ? variables->get("test_clone") : g->GV(test_clone);

  (v_test = LINE(45,c_example::t_singleton()));
  LINE(46,v_test.o_invoke_few_args("bark", 0x077F9DE77F983732LL, 0));
  (v_test_clone = f_clone(toObject(v_test)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
