
#include <php/roadsend/pcc/tests/zswitch2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$zswitch2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/zswitch2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$zswitch2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); not_more(v_i, 5LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(5,concat3("i=", toString(v_i), "\n")));
        {
          switch (toInt64(v_i)) {
          case 0LL:
            {
              echo("In branch 0\n");
              break;
            }
          case 1LL:
            {
              echo("In branch 1\n");
              break;
            }
          case 2LL:
            {
              echo("In branch 2\n");
              break;
            }
          case 3LL:
            {
              echo("In branch 3\n");
              goto break1;
            }
          case 4LL:
            {
              echo("In branch 4\n");
              break;
            }
          default:
            {
              echo("In default\n");
              break;
            }
          }
        }
      }
    }
    break1:;
  }
  echo("hi\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
