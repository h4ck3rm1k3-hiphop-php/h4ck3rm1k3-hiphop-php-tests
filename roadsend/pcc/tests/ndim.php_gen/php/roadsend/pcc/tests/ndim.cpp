
#include <php/roadsend/pcc/tests/ndim.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$ndim_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/ndim.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$ndim_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_fruits __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fruits") : g->GV(fruits);
  Variant &v_firstquarter __attribute__((__unused__)) = (variables != gVariables) ? variables->get("firstquarter") : g->GV(firstquarter);
  Variant &v_handle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("handle") : g->GV(handle);
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);
  Variant &v_files __attribute__((__unused__)) = (variables != gVariables) ? variables->get("files") : g->GV(files);

  v_foo.set("asdf", ("foo"), 0x06528546A3758F5CLL);
  lval(v_foo.lvalAt("fdsa", 0x6E8C227039DBA18CLL)).append(("bar"));
  echo(LINE(5,concat4(toString(v_foo.rvalAt("asdf", 0x06528546A3758F5CLL)), " ", toString(v_foo.rvalAt("fdsa", 0x6E8C227039DBA18CLL)), "[0]\n")));
  lval(lval(v_bar.lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).lvalAt(0LL, 0x77CFA1EEF01BCA90LL)).set(0LL, ("foo"), 0x77CFA1EEF01BCA90LL);
  echo(concat(toString(v_bar.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "\n"));
  echo(toString(v_bar.rvalAt(0, 0x77CFA1EEF01BCA90LL)) + toString("[0][0]\n"));
  (v_fruits = ScalarArrays::sa_[0]);
  echo(toString(v_fruits.rvalAt("holes", 0x05F5827B87B68A31LL)) + toString("[5]\n"));
  (v_firstquarter = ScalarArrays::sa_[1]);
  LINE(33,x_print_r(v_firstquarter));
  (v_handle = LINE(46,x_opendir("/etc")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_file = LINE(47,x_readdir(toObject(v_handle)))))) {
      LOOP_COUNTER_CHECK(1);
      {
        v_files.append((v_file));
      }
    }
  }
  LINE(51,x_closedir(toObject(v_handle)));
  LINE(54,x_print_r(v_files));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
