
#ifndef __GENERATED_cls_c_h__
#define __GENERATED_cls_c_h__

#include <cls/b.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/ext-prop-order.php line 22 */
class c_c : virtual public c_b {
  BEGIN_CLASS_MAP(c)
    PARENT_CLASS(a)
    PARENT_CLASS(b)
  END_CLASS_MAP(c)
  DECLARE_CLASS(c, c, b)
  void init();
  public: int64 m_ppc1;
  public: int64 m_pc1;
  public: int64 m_c1;
  public: int64 m_c2;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_c_h__
