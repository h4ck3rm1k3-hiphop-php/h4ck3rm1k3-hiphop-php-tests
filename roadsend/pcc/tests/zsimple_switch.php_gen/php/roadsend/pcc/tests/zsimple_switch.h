
#ifndef __GENERATED_php_roadsend_pcc_tests_zsimple_switch_h__
#define __GENERATED_php_roadsend_pcc_tests_zsimple_switch_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zsimple_switch.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$zsimple_switch_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zsimple_switch_h__
