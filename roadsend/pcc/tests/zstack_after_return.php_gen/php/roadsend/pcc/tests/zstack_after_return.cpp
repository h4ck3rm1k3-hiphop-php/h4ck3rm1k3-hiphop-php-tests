
#include <php/roadsend/pcc/tests/zstack_after_return.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/zstack_after_return.php line 2 */
Variant f_f() {
  FUNCTION_INJECTION(F);
  if (toBoolean(1LL)) {
    return "Hello";
  }
  return null;
} /* function */
Variant pm_php$roadsend$pcc$tests$zstack_after_return_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/zstack_after_return.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$zstack_after_return_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_i = 0LL);
  LOOP_COUNTER(1);
  {
    while (less(v_i, 2LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(LINE(10,f_f())));
        v_i++;
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
