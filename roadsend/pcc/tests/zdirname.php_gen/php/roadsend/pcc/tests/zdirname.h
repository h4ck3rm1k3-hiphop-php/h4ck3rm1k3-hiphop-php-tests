
#ifndef __GENERATED_php_roadsend_pcc_tests_zdirname_h__
#define __GENERATED_php_roadsend_pcc_tests_zdirname_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zdirname.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$zdirname_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_check_dirname(CStrRef v_path);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zdirname_h__
