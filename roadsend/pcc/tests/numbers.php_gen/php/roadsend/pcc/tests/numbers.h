
#ifndef __GENERATED_php_roadsend_pcc_tests_numbers_h__
#define __GENERATED_php_roadsend_pcc_tests_numbers_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/numbers.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$numbers_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_test_num(CStrRef v_title, double v_a);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_numbers_h__
