
#include <php/roadsend/pcc/tests/logical.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$logical_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/logical.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$logical_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);

  (v_a = false);
  (v_b = !(toBoolean(v_a)));
  echo(toString(!(toBoolean(v_a))));
  echo(toString(v_b));
  echo((toString(equal(!(toBoolean(v_a)), v_b))));
  (v_a = true);
  (v_b = false);
  (v_c = true);
  (v_d = (toBoolean(v_a) && (toBoolean(v_b) && toBoolean(v_c))));
  echo(toString(v_d));
  (v_a = true);
  (v_b = false);
  (v_c = true);
  (v_d = (toBoolean(v_a) || (toBoolean(v_b) || toBoolean(v_c))));
  echo(toString(v_d));
  echo((toString(toBoolean(v_a) && (!(toBoolean(v_b)) || (toBoolean(v_a) && toBoolean(v_d))) || toBoolean(v_c))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
