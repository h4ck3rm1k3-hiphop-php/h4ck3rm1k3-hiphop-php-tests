
#include <php/roadsend/pcc/tests/forlooptest.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$forlooptest_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/forlooptest.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$forlooptest_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); not_more(v_i, 10LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        print(toString(v_i));
      }
    }
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = 1LL); ; v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        if (more(v_i, 10LL)) {
          break;
        }
        print(toString(v_i));
      }
    }
  }
  ;
  (v_i = 1LL);
  {
    LOOP_COUNTER(3);
    for (; ; ) {
      LOOP_COUNTER_CHECK(3);
      {
        if (more(v_i, 10LL)) {
          break;
        }
        print(toString(v_i));
        v_i++;
      }
    }
  }
  {
    LOOP_COUNTER(4);
    for ((v_i = 1LL); not_more(v_i, 10LL); print(toString(v_i)), v_i++) {
      LOOP_COUNTER_CHECK(4);
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
