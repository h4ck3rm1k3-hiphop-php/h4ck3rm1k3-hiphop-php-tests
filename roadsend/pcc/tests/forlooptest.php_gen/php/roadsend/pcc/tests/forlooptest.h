
#ifndef __GENERATED_php_roadsend_pcc_tests_forlooptest_h__
#define __GENERATED_php_roadsend_pcc_tests_forlooptest_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/forlooptest.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$forlooptest_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_forlooptest_h__
