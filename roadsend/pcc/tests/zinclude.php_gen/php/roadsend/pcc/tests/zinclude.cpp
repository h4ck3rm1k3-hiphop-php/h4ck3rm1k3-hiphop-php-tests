
#include <php/roadsend/pcc/tests/zinclude.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$zinclude_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/zinclude.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$zinclude_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_includesuffix __attribute__((__unused__)) = (variables != gVariables) ? variables->get("includesuffix") : g->GV(includesuffix);

  LINE(2,include("includetest.inc", false, variables, "roadsend/pcc/tests/"));
  LINE(3,include("includetest.inc", false, variables, "roadsend/pcc/tests/"));
  LINE(4,require("includetest.inc", false, variables, "roadsend/pcc/tests/"));
  LINE(5,require("includetest.inc", false, variables, "roadsend/pcc/tests/"));
  LINE(7,include("includetest-once.inc", true, variables, "roadsend/pcc/tests/"));
  LINE(8,require("includetest-once.inc", true, variables, "roadsend/pcc/tests/"));
  LINE(9,include("includetest-once.inc", true, variables, "roadsend/pcc/tests/"));
  LINE(10,require("includetest-once.inc", true, variables, "roadsend/pcc/tests/"));
  (v_includesuffix = ".inc");
  LINE(14,include(concat("includetest", toString(v_includesuffix)), false, variables, "roadsend/pcc/tests/"));
  LINE(15,include((concat("includetest", toString(v_includesuffix))), false, variables, "roadsend/pcc/tests/"));
  LINE(16,require(concat("includetest", toString(v_includesuffix)), false, variables, "roadsend/pcc/tests/"));
  LINE(17,require((concat("includetest", toString(v_includesuffix))), false, variables, "roadsend/pcc/tests/"));
  LINE(19,include(concat("includetest-once", toString(v_includesuffix)), true, variables, "roadsend/pcc/tests/"));
  LINE(20,require(concat("includetest-once", toString(v_includesuffix)), true, variables, "roadsend/pcc/tests/"));
  LINE(21,include((concat("includetest-once", toString(v_includesuffix))), true, variables, "roadsend/pcc/tests/"));
  LINE(22,require((concat("includetest-once", toString(v_includesuffix))), true, variables, "roadsend/pcc/tests/"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
