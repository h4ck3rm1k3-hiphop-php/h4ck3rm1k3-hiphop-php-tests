
#ifndef __GENERATED_php_roadsend_pcc_tests_class_constants_h__
#define __GENERATED_php_roadsend_pcc_tests_class_constants_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/class-constants.fw.h>

// Declarations
#include <cls/myclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$class_constants_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_class_constants_h__
