
#include <php/roadsend/pcc/tests/zeval1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/zeval1.php line 2 */
void f_f(Variant v_a) {
  FUNCTION_INJECTION(F);
  DECLARE_GLOBAL_VARIABLES(g);

  class VariableTable : public LVariableTable {
  public:
    Variant &v_a;
    VariableTable(Variant &r_a) : v_a(r_a) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 0:
          HASH_RETURN(0x4292CEE227B9150ALL, v_a,
                      a);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_a);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  f_eval(toString(v_a));
} /* function */
Variant pm_php$roadsend$pcc$tests$zeval1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/zeval1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$zeval1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(6,x_error_reporting(toInt32(0LL)));
  LINE(7,f_f("echo \"Hello\";"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
