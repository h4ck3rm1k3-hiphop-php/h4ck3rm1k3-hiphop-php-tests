
#ifndef __GENERATED_php_roadsend_pcc_tests_zfunction_falling_h__
#define __GENERATED_php_roadsend_pcc_tests_zfunction_falling_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zfunction_falling.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_test(CVarRef v_a);
Variant pm_php$roadsend$pcc$tests$zfunction_falling_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zfunction_falling_h__
