
#include <php/roadsend/pcc/tests/shared-environment.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/shared-environment.php line 2 */
void f_foo() {
  FUNCTION_INJECTION(foo);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_root;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_root;
    VariableTable(Variant &r_root) : v_root(r_root) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 1) {
        case 1:
          HASH_RETURN(0x05E3DE381E45A0DBLL, v_root,
                      root);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
  } variableTable(v_root);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  LINE(3,include("shared-environment.inc", false, variables, "roadsend/pcc/tests/"));
  print((LINE(4,concat3("my root dir is ", toString(v_root), "\n"))));
} /* function */
Variant pm_php$roadsend$pcc$tests$shared_environment_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/shared-environment.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$shared_environment_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(6,f_foo());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
