
#include <php/roadsend/pcc/tests/ref2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$ref2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/ref2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$ref2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_wibb __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wibb") : g->GV(wibb);

  (v_foo = 5LL);
  (v_wibb = Array(ArrayInit(2).set(0, v_foo).setRef(1, ref(v_foo)).create()));
  (v_foo = 6LL);
  echo(LINE(6,concat4(toString(v_wibb.rvalAt(0, 0x77CFA1EEF01BCA90LL)), ", ", toString(v_wibb.rvalAt(1, 0x5BCA7C69B794F8CELL)), "\n")));
  v_wibb.set(0LL, (9LL), 0x77CFA1EEF01BCA90LL);
  v_wibb.set(1LL, (10LL), 0x5BCA7C69B794F8CELL);
  echo(LINE(12,concat6(toString(v_foo), ", ", toString(v_wibb.rvalAt(0, 0x77CFA1EEF01BCA90LL)), ", ", toString(v_wibb.rvalAt(1, 0x5BCA7C69B794F8CELL)), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
