
#ifndef __GENERATED_php_roadsend_pcc_tests_exceptions_h__
#define __GENERATED_php_roadsend_pcc_tests_exceptions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/exceptions.fw.h>

// Declarations
#include <cls/other2.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_funcfunc();
Variant f_inverse(int64 v_x);
void f_func();
Variant pm_php$roadsend$pcc$tests$exceptions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_inverse2(int64 v_x);
Object co_other2(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_exceptions_h__
