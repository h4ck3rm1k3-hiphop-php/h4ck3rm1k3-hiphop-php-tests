
#include <php/roadsend/pcc/tests/exceptions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/exceptions.php line 17 */
Variant c_other2::os_get(const char *s, int64 hash) {
  return c_exception::os_get(s, hash);
}
Variant &c_other2::os_lval(const char *s, int64 hash) {
  return c_exception::os_lval(s, hash);
}
void c_other2::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("foo", m_foo));
  c_exception::o_get(props);
}
bool c_other2::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x4154FA2EF733DA8FLL, foo, 3);
      break;
    default:
      break;
  }
  return c_exception::o_exists(s, hash);
}
Variant c_other2::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4154FA2EF733DA8FLL, m_foo,
                         foo, 3);
      break;
    default:
      break;
  }
  return c_exception::o_get(s, hash);
}
Variant c_other2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x4154FA2EF733DA8FLL, m_foo,
                      foo, 3);
      break;
    default:
      break;
  }
  return c_exception::o_set(s, hash, v, forInit);
}
Variant &c_other2::o_lval(CStrRef s, int64 hash) {
  return c_exception::o_lval(s, hash);
}
Variant c_other2::os_constant(const char *s) {
  return c_exception::os_constant(s);
}
IMPLEMENT_CLASS(other2)
ObjectData *c_other2::cloneImpl() {
  c_other2 *obj = NEW(c_other2)();
  cloneSet(obj);
  return obj;
}
void c_other2::cloneSet(c_other2 *clone) {
  clone->m_foo = m_foo;
  c_exception::cloneSet(clone);
}
Variant c_other2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x71859D7313E682D2LL, getmessage) {
        return (t_getmessage());
      }
      break;
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke(s, params, hash, fatal);
}
Variant c_other2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x71859D7313E682D2LL, getmessage) {
        return (t_getmessage());
      }
      break;
    case 3:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        if (count == 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_exception::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_other2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_exception::os_invoke(c, s, params, hash, fatal);
}
Variant cw_other2$os_get(const char *s) {
  return c_other2::os_get(s, -1);
}
Variant &cw_other2$os_lval(const char *s) {
  return c_other2::os_lval(s, -1);
}
Variant cw_other2$os_constant(const char *s) {
  return c_other2::os_constant(s);
}
Variant cw_other2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_other2::os_invoke(c, s, params, -1, fatal);
}
void c_other2::init() {
  c_exception::init();
  m_foo = "excellent";
}
/* SRC: roadsend/pcc/tests/exceptions.php line 51 */
void f_funcfunc() {
  FUNCTION_INJECTION(funcfunc);
  p_exception v_ex;

  try {
    LINE(53,f_func());
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_ex = e;
      throw_exception(((Object)(v_ex)));
    } else {
      throw;
    }
  }
} /* function */
/* SRC: roadsend/pcc/tests/exceptions.php line 3 */
Variant f_inverse(int64 v_x) {
  FUNCTION_INJECTION(inverse);
  if (!(toBoolean(v_x))) {
    throw_exception(((Object)(LINE(5,p_exception(p_exception(NEWOBJ(c_exception)())->create("Division by zero."))))));
  }
  else return divide(1LL, v_x);
  return null;
} /* function */
/* SRC: roadsend/pcc/tests/exceptions.php line 58 */
void f_func() {
  FUNCTION_INJECTION(func);
  throw_exception(((Object)(LINE(59,p_exception(p_exception(NEWOBJ(c_exception)())->create("blah", 101LL))))));
} /* function */
/* SRC: roadsend/pcc/tests/exceptions.php line 10 */
Variant f_inverse2(int64 v_x) {
  FUNCTION_INJECTION(inverse2);
  if (!(toBoolean(v_x))) {
    throw_exception(((Object)(LINE(12,p_other2(p_other2(NEWOBJ(c_other2)())->create("Division by zero."))))));
  }
  else return divide(1LL, v_x);
  return null;
} /* function */
Object co_other2(CArrRef params, bool init /* = true */) {
  return Object(p_other2(NEW(c_other2)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$exceptions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/exceptions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$exceptions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_ex __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ex") : g->GV(ex);

  try {
    echo(concat(toString(LINE(22,f_inverse(5LL))), "\n"));
    echo(concat(toString(LINE(23,f_inverse(0LL))), "\n"));
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_e = e;
      {
        echo("Caught exception: ");
        echo(toString(LINE(25,v_e.o_invoke_few_args("getMessage", 0x71859D7313E682D2LL, 0))));
        echo("\n");
      }
    } else if (true) {
      throw_fatal("unknown class other");
    } else if (e.instanceof("other2")) {
      v_e = e;
      echo("Caught other exception 2");
    } else {
      throw;
    }
  }
  echo("Hello World");
  try {
    echo(concat(toString(LINE(36,f_inverse2(0LL))), "\n"));
  } catch (Object e) {
    if (true) {
      throw_fatal("unknown class other");
    } else if (e.instanceof("other2")) {
      v_e = e;
      echo("Caught other exception 2");
    } else {
      throw;
    }
  }
  try {
    LINE(46,f_funcfunc());
  } catch (Object e) {
    if (e.instanceof("exception")) {
      v_ex = e;
      print("caught an exception\n");
    } else {
      throw;
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
