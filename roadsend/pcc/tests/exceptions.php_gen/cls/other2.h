
#ifndef __GENERATED_cls_other2_h__
#define __GENERATED_cls_other2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/exceptions.php line 17 */
class c_other2 : virtual public c_exception {
  BEGIN_CLASS_MAP(other2)
    PARENT_CLASS(exception)
  END_CLASS_MAP(other2)
  DECLARE_CLASS(other2, Other2, exception)
  void init();
  public: String m_foo;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_other2_h__
