
#ifndef __GENERATED_cls_bar_h__
#define __GENERATED_cls_bar_h__

#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/class-functions.php line 7 */
class c_bar : virtual public c_foo {
  BEGIN_CLASS_MAP(bar)
    PARENT_CLASS(foo)
  END_CLASS_MAP(bar)
  DECLARE_CLASS(bar, bar, foo)
  void init();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bar_h__
