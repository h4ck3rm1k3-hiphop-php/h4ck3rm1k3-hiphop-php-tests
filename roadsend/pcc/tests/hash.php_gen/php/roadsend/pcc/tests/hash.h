
#ifndef __GENERATED_php_roadsend_pcc_tests_hash_h__
#define __GENERATED_php_roadsend_pcc_tests_hash_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/hash.fw.h>

// Declarations
#include <cls/aclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$hash_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_aclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_hash_h__
