
#ifndef __GENERATED_php_roadsend_pcc_tests_retval_methodcall_h__
#define __GENERATED_php_roadsend_pcc_tests_retval_methodcall_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/retval-methodcall.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$retval_methodcall_php(bool incOnce = false, LVariableTable* variables = NULL);
p_foo f_afun();
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_retval_methodcall_h__
