
#include <php/roadsend/pcc/tests/retval-methodcall.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/retval-methodcall.php line 4 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("prop", m_prop.isReferenced() ? ref(m_prop) : m_prop));
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x4EA429E0DCC990B7LL, prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4EA429E0DCC990B7LL, m_prop,
                         prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x4EA429E0DCC990B7LL, m_prop,
                      prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4EA429E0DCC990B7LL, m_prop,
                         prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::create(CVarRef v_arg) {
  init();
  t_foo(v_arg);
  return this;
}
ObjectData *c_foo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  clone->m_prop = m_prop.isReferenced() ? ref(m_prop) : m_prop;
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x797B42C72E405E22LL, method2) {
        return (t_method2());
      }
      HASH_GUARD(0x4C211FA78927D132LL, method) {
        return (t_method());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 2:
      HASH_GUARD(0x797B42C72E405E22LL, method2) {
        return (t_method2());
      }
      HASH_GUARD(0x4C211FA78927D132LL, method) {
        return (t_method());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
  m_prop = "a value";
}
/* SRC: roadsend/pcc/tests/retval-methodcall.php line 7 */
void c_foo::t_foo(CVarRef v_arg) {
  INSTANCE_METHOD_INJECTION(foo, foo::foo);
  bool oldInCtor = gasInCtor(true);
  print(LINE(8,concat3("Constructor called on ", toString(v_arg), "\n")));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/retval-methodcall.php line 11 */
p_foo c_foo::t_method() {
  INSTANCE_METHOD_INJECTION(foo, foo::method);
  print("Method called\n");
  return ((Object)(this));
} /* function */
/* SRC: roadsend/pcc/tests/retval-methodcall.php line 15 */
p_foo c_foo::t_method2() {
  INSTANCE_METHOD_INJECTION(foo, foo::method2);
  print("Method 2 called\n");
  return ((Object)(this));
} /* function */
/* SRC: roadsend/pcc/tests/retval-methodcall.php line 31 */
p_foo f_afun() {
  FUNCTION_INJECTION(afun);
  return ((Object)(LINE(32,p_foo(p_foo(NEWOBJ(c_foo)())->create(22LL)))));
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$retval_methodcall_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/retval-methodcall.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$retval_methodcall_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  echo("Test the new $foo->bar()->baz() syntax.\n");
  (v_foo = ((Object)(LINE(21,p_foo(p_foo(NEWOBJ(c_foo)())->create("an argument"))))));
  (assignCallTemp(eo_0, toObject(LINE(22,v_foo.o_invoke_few_args("method", 0x4C211FA78927D132LL, 0)))),eo_0.o_invoke_few_args("method", 0x4C211FA78927D132LL, 0));
  print(LINE(24,(assignCallTemp(eo_2, toString(v_foo.o_invoke_few_args("method", 0x4C211FA78927D132LL, 0).o_get("prop", 0x4EA429E0DCC990B7LL))),concat3("property is :", eo_2, "\n"))));
  ((assignCallTemp(eo_1, toObject(LINE(25,v_foo.o_invoke_few_args("method", 0x4C211FA78927D132LL, 0)))),eo_1.o_invoke_few_args("method2", 0x797B42C72E405E22LL, 0)).o_lval("prop", 0x4EA429E0DCC990B7LL) = "newval");
  print(LINE(26,(assignCallTemp(eo_3, toString((assignCallTemp(eo_5, toObject(v_foo.o_invoke_few_args("method", 0x4C211FA78927D132LL, 0))),eo_5.o_invoke_few_args("method2", 0x797B42C72E405E22LL, 0)).o_get("prop", 0x4EA429E0DCC990B7LL))),concat3("property is :", eo_3, "\n"))));
  print(LINE(29,(assignCallTemp(eo_3, toString(v_foo.o_invoke_few_args("method", 0x4C211FA78927D132LL, 0).o_get("prop", 0x4EA429E0DCC990B7LL))),concat3("property is : ", eo_3, "\n"))));
  print(toString(LINE(35,f_afun())->m_prop));
  LINE(36,x_print_r(((Object)((assignCallTemp(eo_2, f_afun()),eo_2.toObject().getTyped<c_foo>()->t_method())))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
