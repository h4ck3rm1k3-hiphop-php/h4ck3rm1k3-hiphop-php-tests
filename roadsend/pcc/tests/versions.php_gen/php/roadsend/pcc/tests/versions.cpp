
#include <php/roadsend/pcc/tests/versions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$versions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/versions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$versions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(concat(toString(LINE(3,x_substr("5.2.5.hiphop" /* PHP_VERSION */, toInt32(0LL), toInt32(5LL)))), "\n"));
  echo(concat(toString(LINE(4,(assignCallTemp(eo_0, x_zend_version()),x_substr(eo_0, toInt32(0LL), toInt32(5LL))))), "\n"));
  echo(concat(toString(LINE(5,(assignCallTemp(eo_0, x_phpversion()),x_substr(eo_0, toInt32(0LL), toInt32(5LL))))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
