
#ifndef __GENERATED_cls_flag_h__
#define __GENERATED_cls_flag_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/equality.php line 20 */
class c_flag : virtual public ObjectData {
  BEGIN_CLASS_MAP(flag)
  END_CLASS_MAP(flag)
  DECLARE_CLASS(flag, Flag, ObjectData)
  void init();
  public: Variant m_flag;
  public: void t_flag(bool v_flag = true);
  public: ObjectData *create(bool v_flag = true);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_flag_h__
