
#include <php/roadsend/pcc/tests/equality.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/equality.php line 20 */
Variant c_flag::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_flag::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_flag::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("flag", m_flag.isReferenced() ? ref(m_flag) : m_flag));
  c_ObjectData::o_get(props);
}
bool c_flag::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x0756933295A05303LL, flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_flag::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x0756933295A05303LL, m_flag,
                         flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_flag::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x0756933295A05303LL, m_flag,
                      flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_flag::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x0756933295A05303LL, m_flag,
                         flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_flag::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(flag)
ObjectData *c_flag::create(bool v_flag //  = true
) {
  init();
  t_flag(v_flag);
  return this;
}
ObjectData *c_flag::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_flag::cloneImpl() {
  c_flag *obj = NEW(c_flag)();
  cloneSet(obj);
  return obj;
}
void c_flag::cloneSet(c_flag *clone) {
  clone->m_flag = m_flag.isReferenced() ? ref(m_flag) : m_flag;
  ObjectData::cloneSet(clone);
}
Variant c_flag::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_flag::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_flag::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_flag$os_get(const char *s) {
  return c_flag::os_get(s, -1);
}
Variant &cw_flag$os_lval(const char *s) {
  return c_flag::os_lval(s, -1);
}
Variant cw_flag$os_constant(const char *s) {
  return c_flag::os_constant(s);
}
Variant cw_flag$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_flag::os_invoke(c, s, params, -1, fatal);
}
void c_flag::init() {
  m_flag = null;
}
/* SRC: roadsend/pcc/tests/equality.php line 24 */
void c_flag::t_flag(bool v_flag //  = true
) {
  INSTANCE_METHOD_INJECTION(Flag, Flag::Flag);
  bool oldInCtor = gasInCtor(true);
  (m_flag = v_flag);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/equality.php line 29 */
Variant c_otherflag::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_otherflag::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_otherflag::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("flag", m_flag.isReferenced() ? ref(m_flag) : m_flag));
  c_ObjectData::o_get(props);
}
bool c_otherflag::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x0756933295A05303LL, flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_otherflag::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x0756933295A05303LL, m_flag,
                         flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_otherflag::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x0756933295A05303LL, m_flag,
                      flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_otherflag::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x0756933295A05303LL, m_flag,
                         flag, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_otherflag::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(otherflag)
ObjectData *c_otherflag::create(bool v_flag //  = true
) {
  init();
  t_otherflag(v_flag);
  return this;
}
ObjectData *c_otherflag::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
ObjectData *c_otherflag::cloneImpl() {
  c_otherflag *obj = NEW(c_otherflag)();
  cloneSet(obj);
  return obj;
}
void c_otherflag::cloneSet(c_otherflag *clone) {
  clone->m_flag = m_flag.isReferenced() ? ref(m_flag) : m_flag;
  ObjectData::cloneSet(clone);
}
Variant c_otherflag::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_otherflag::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_otherflag::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_otherflag$os_get(const char *s) {
  return c_otherflag::os_get(s, -1);
}
Variant &cw_otherflag$os_lval(const char *s) {
  return c_otherflag::os_lval(s, -1);
}
Variant cw_otherflag$os_constant(const char *s) {
  return c_otherflag::os_constant(s);
}
Variant cw_otherflag$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_otherflag::os_invoke(c, s, params, -1, fatal);
}
void c_otherflag::init() {
  m_flag = null;
}
/* SRC: roadsend/pcc/tests/equality.php line 33 */
void c_otherflag::t_otherflag(bool v_flag //  = true
) {
  INSTANCE_METHOD_INJECTION(OtherFlag, OtherFlag::OtherFlag);
  bool oldInCtor = gasInCtor(true);
  (m_flag = v_flag);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/equality.php line 12 */
void f_compareobjects(Variant v_o1, Variant v_o2) {
  FUNCTION_INJECTION(compareObjects);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  echo(LINE(14,(assignCallTemp(eo_1, toString(f_bool2str(equal(v_o1, v_o2)))),concat3("o1 == o2 : ", eo_1, "\n"))));
  echo(LINE(15,(assignCallTemp(eo_1, toString(f_bool2str(!equal(v_o1, v_o2)))),concat3("o1 != o2 : ", eo_1, "\n"))));
  echo(LINE(16,(assignCallTemp(eo_1, toString(f_bool2str(same(v_o1, v_o2)))),concat3("o1 === o2 : ", eo_1, "\n"))));
  echo(LINE(17,(assignCallTemp(eo_1, toString(f_bool2str(!same(v_o1, v_o2)))),concat3("o1 !== o2 : ", eo_1, "\n"))));
} /* function */
/* SRC: roadsend/pcc/tests/equality.php line 3 */
Variant f_bool2str(bool v_bool) {
  FUNCTION_INJECTION(bool2str);
  if (same(v_bool, false)) {
    return "FALSE";
  }
  else {
    return "TRUE";
  }
  return null;
} /* function */
Object co_flag(CArrRef params, bool init /* = true */) {
  return Object(p_flag(NEW(c_flag)())->dynCreate(params, init));
}
Object co_otherflag(CArrRef params, bool init /* = true */) {
  return Object(p_otherflag(NEW(c_otherflag)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$equality_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/equality.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$equality_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_o __attribute__((__unused__)) = (variables != gVariables) ? variables->get("o") : g->GV(o);
  Variant &v_p __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p") : g->GV(p);
  Variant &v_q __attribute__((__unused__)) = (variables != gVariables) ? variables->get("q") : g->GV(q);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  (v_o = ((Object)(LINE(38,p_flag(p_flag(NEWOBJ(c_flag)())->create())))));
  (v_p = ((Object)(LINE(39,p_flag(p_flag(NEWOBJ(c_flag)())->create())))));
  (v_q = v_o);
  (v_r = ((Object)(LINE(41,p_otherflag(p_otherflag(NEWOBJ(c_otherflag)())->create())))));
  echo("Two instances of the same class\n");
  LINE(44,f_compareobjects(ref(v_o), ref(v_p)));
  echo("\nTwo references to the same instance\n");
  LINE(47,f_compareobjects(ref(v_o), ref(v_q)));
  echo("\nInstances of two different classes\n");
  LINE(50,f_compareobjects(ref(v_o), ref(v_r)));
  echo("--type cast compare--\n");
  echo(LINE(54,concat3("o1: ", (toString(equal(toObject(v_a), null))), "\n")));
  echo(LINE(55,concat3("o2: ", (toString(equal(toObject(v_a), true))), "\n")));
  echo(LINE(56,concat3("o3: ", (toString(equal(toObject(v_a), false))), "\n")));
  echo(LINE(57,concat3("o4: ", (toString(equal(toObject(v_a), 1LL))), "\n")));
  echo(LINE(58,concat3("o5: ", (toString(equal(toObject(v_a), toObject(v_b)))), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
