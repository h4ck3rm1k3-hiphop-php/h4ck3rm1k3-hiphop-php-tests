
#ifndef __GENERATED_php_roadsend_pcc_tests_zswitch3_h__
#define __GENERATED_php_roadsend_pcc_tests_zswitch3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zswitch3.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$zswitch3_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_switchtest(int64 v_i, CVarRef v_j);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zswitch3_h__
