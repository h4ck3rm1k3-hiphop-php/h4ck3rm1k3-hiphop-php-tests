
#ifndef __GENERATED_php_roadsend_pcc_tests_ackermann_h__
#define __GENERATED_php_roadsend_pcc_tests_ackermann_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/ackermann.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Numeric f_ack(int64 v_m, Variant v_n);
Variant pm_php$roadsend$pcc$tests$ackermann_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_ackermann_h__
