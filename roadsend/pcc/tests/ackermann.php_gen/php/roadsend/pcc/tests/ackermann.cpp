
#include <php/roadsend/pcc/tests/ackermann.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/ackermann.php line 3 */
Numeric f_ack(int64 v_m, Variant v_n) {
  FUNCTION_INJECTION(Ack);
  Variant eo_0;
  Variant eo_1;
  (v_m = toInt64(v_m));
  (v_n = toInt64(v_n));
  if (equal(v_m, 0LL)) return v_n + 1LL;
  if (equal(v_n, 0LL)) return LINE(7,f_ack(v_m - 1LL, 1LL));
  return LINE(8,(assignCallTemp(eo_0, v_m - 1LL),assignCallTemp(eo_1, f_ack(v_m, (v_n - 1LL))),f_ack(eo_0, eo_1)));
} /* function */
Variant pm_php$roadsend$pcc$tests$ackermann_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/ackermann.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$ackermann_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_n __attribute__((__unused__)) = (variables != gVariables) ? variables->get("n") : g->GV(n);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  (v_n = 5LL);
  (v_r = LINE(13,f_ack(3LL, v_n)));
  print(LINE(15,concat5("Ack(3,", toString(v_n), "): ", toString(v_r), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
