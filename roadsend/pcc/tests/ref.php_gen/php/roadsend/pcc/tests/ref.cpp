
#include <php/roadsend/pcc/tests/ref.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/ref.php line 7 */
void f_foocrement(Variant v_foo) {
  FUNCTION_INJECTION(foocrement);
  echo("foocrement\n");
  (v_foo = v_foo + 1LL);
  echo(toString(v_foo) + toString("\n"));
  echo("endcrement\n");
} /* function */
Variant pm_php$roadsend$pcc$tests$ref_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/ref.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$ref_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = 5LL);
  echo(toString(v_foo));
  LINE(14,f_foocrement(ref(v_foo)));
  echo(toString(v_foo) + toString("\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
