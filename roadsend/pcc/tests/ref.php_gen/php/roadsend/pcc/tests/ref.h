
#ifndef __GENERATED_php_roadsend_pcc_tests_ref_h__
#define __GENERATED_php_roadsend_pcc_tests_ref_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/ref.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_foocrement(Variant v_foo);
Variant pm_php$roadsend$pcc$tests$ref_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_ref_h__
