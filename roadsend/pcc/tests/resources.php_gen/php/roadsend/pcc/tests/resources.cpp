
#include <php/roadsend/pcc/tests/resources.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$resources_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/resources.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$resources_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_foo = LINE(3,x_opendir("./")));
  echo(concat(toString(v_foo), "\n"));
  LINE(5,x_var_dump(1, v_foo));
  echo("\n\nas an array key:\n");
  v_bar.set(v_foo, (true));
  LINE(8,x_var_dump(1, v_bar));
  echo(LINE(9,(assignCallTemp(eo_1, x_get_resource_type(toObject(v_foo))),concat3("\n", eo_1, "\n"))));
  echo(LINE(11,concat3("and reading: ", toString(v_bar.rvalAt(v_foo)), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
