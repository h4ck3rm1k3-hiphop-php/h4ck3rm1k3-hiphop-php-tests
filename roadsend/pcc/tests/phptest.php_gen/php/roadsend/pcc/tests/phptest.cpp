
#include <php/roadsend/pcc/tests/phptest.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/phptest.php line 5 */
void f_zammo(CStrRef v_first, CStrRef v_second) {
  FUNCTION_INJECTION(zammo);
  Variant v_zop;

  (v_zop = LINE(6,f_zapit()));
  echo((LINE(7,concat4(v_first, " ", v_second, " 5\n"))));
  echo(LINE(8,concat3("zop was ", toString(v_zop), "!\n")));
} /* function */
/* SRC: roadsend/pcc/tests/phptest.php line 11 */
Variant f_zapit() {
  FUNCTION_INJECTION(zapit);
  return 12LL;
  echo("thou art a knave!\n");
  return null;
} /* function */
Variant pm_php$roadsend$pcc$tests$phptest_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/phptest.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$phptest_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_cnt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cnt") : g->GV(cnt);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_zonk __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zonk") : g->GV(zonk);
  Variant &v_avar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("avar") : g->GV(avar);
  Variant &v_somenewvarfirst __attribute__((__unused__)) = (variables != gVariables) ? variables->get("somenewvarfirst") : g->GV(somenewvarfirst);
  Variant &v_somenewvarsecond __attribute__((__unused__)) = (variables != gVariables) ? variables->get("somenewvarsecond") : g->GV(somenewvarsecond);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_zippy __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zippy") : g->GV(zippy);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_bork __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bork") : g->GV(bork);
  Variant &v_zing __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zing") : g->GV(zing);
  Variant &v_zops __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zops") : g->GV(zops);

  echo("<HtML>\n<CRUD>\n");
  {
    LOOP_COUNTER(1);
    for ((v_cnt = 0LL); less(v_cnt, 1000LL); v_cnt++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo("Hello, world\n");
        (v_foo = "baz\n");
        echo(toString((toString(v_foo))));
        echo(" ");
        (v_zonk = false);
        (v_avar = 91.299999999999997);
        echo((concat(toString(v_zonk), "\n")));
        echo((concat(toString(v_avar), "\n")));
        if (toBoolean(1LL)) {
          (v_somenewvarfirst = 12LL);
          (v_somenewvarsecond = 13LL);
        }
        LINE(44,f_zammo("first", "second"));
        echo("-1\n");
        echo("47\n");
        echo("-1\n");
        (v_a = ((v_b = 4LL)) + 5LL);
        echo((LINE(57,concat5("1: a is ", toString(v_a), " and b is ", toString(v_b), "\n"))));
        (v_a = 23LL);
        (v_b = (v_a *= 7LL) + 1LL);
        echo((LINE(62,concat5("2: a is ", toString(v_a), " and b is ", toString(v_b), "\n"))));
        (v_a = 1LL);
        if (toBoolean(v_a)) if (toBoolean(0LL)) {
          (v_zippy = 4LL);
          (v_a = 9LL);
          echo((concat(toString(v_zippy), " was true\n")));
        }
        else echo("should print this\n");
        echo("just want to see bar, not zap or anything\n");
        {
          echo("bar\n");
        }
        echo("whadja see\?\n");
        (v_i = 10LL);
        LOOP_COUNTER(2);
        {
          while (more(v_i, 3LL)) {
            LOOP_COUNTER_CHECK(2);
            {
              (v_i = v_i - 1LL);
            }
          }
        }
        echo((LINE(98,concat6(toString(v_bork), " is not ", toString(v_a), ", but $a his ", toString(v_a), "\n"))));
        echo("</CRUD>\n");
        echo("Hello, other world\n");
        v_zing.set("foo", ("a value"), 0x4154FA2EF733DA8FLL);
        echo(concat(toString(v_zing.rvalAt("foo", 0x4154FA2EF733DA8FLL)), "\n"));
        echo(LINE(108,concat4(toString(v_zops.rvalAt("zirt", 0x00D8BD00F7D1CA09LL)), ", ", toString(v_zing.rvalAt("foo", 0x4154FA2EF733DA8FLL)), "\n")));
        echo("$zops[php, \"foo\", $zing[foo]\\n");
      }
    }
  }
  echo("</HTML>\n\n\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
