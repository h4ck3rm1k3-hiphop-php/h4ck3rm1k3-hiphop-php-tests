
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x41140526F1E1D5C1LL, somenewvarfirst, 16);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 10:
      HASH_INDEX(0x2A6558C007B3B00ALL, avar, 15);
      HASH_INDEX(0x4292CEE227B9150ALL, a, 19);
      HASH_INDEX(0x1E46B4C4E7D774CALL, bork, 22);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 15:
      HASH_INDEX(0x4154FA2EF733DA8FLL, foo, 13);
      HASH_INDEX(0x13DEB4809EAAC8CFLL, somenewvarsecond, 17);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x04A52CA825E8EDD0LL, zops, 24);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 18);
      break;
    case 24:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 21);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 41:
      HASH_INDEX(0x44512AF0D29AB3A9LL, zippy, 20);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 50:
      HASH_INDEX(0x045A631FC0FF2A32LL, zing, 23);
      break;
    case 61:
      HASH_INDEX(0x2072FF3918A89F3DLL, cnt, 12);
      HASH_INDEX(0x26E70030364D72FDLL, zonk, 14);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 25) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
