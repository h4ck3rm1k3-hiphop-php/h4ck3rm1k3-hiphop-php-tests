
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "cnt",
    "foo",
    "zonk",
    "avar",
    "somenewvarfirst",
    "somenewvarsecond",
    "b",
    "a",
    "zippy",
    "i",
    "bork",
    "zing",
    "zops",
  };
  if (idx >= 0 && idx < 25) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(cnt);
    case 13: return GV(foo);
    case 14: return GV(zonk);
    case 15: return GV(avar);
    case 16: return GV(somenewvarfirst);
    case 17: return GV(somenewvarsecond);
    case 18: return GV(b);
    case 19: return GV(a);
    case 20: return GV(zippy);
    case 21: return GV(i);
    case 22: return GV(bork);
    case 23: return GV(zing);
    case 24: return GV(zops);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
