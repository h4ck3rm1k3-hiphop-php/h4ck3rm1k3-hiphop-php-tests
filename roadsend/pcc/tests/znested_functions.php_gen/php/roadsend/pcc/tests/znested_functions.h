
#ifndef __GENERATED_php_roadsend_pcc_tests_znested_functions_h__
#define __GENERATED_php_roadsend_pcc_tests_znested_functions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/znested_functions.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$znested_functions_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_f();
void f_g();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_znested_functions_h__
