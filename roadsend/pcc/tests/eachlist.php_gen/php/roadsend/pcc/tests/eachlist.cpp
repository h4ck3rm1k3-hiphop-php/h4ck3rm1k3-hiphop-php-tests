
#include <php/roadsend/pcc/tests/eachlist.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1, Variant &p2) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$eachlist_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/eachlist.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$eachlist_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);
  Variant &v_zing __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zing") : g->GV(zing);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);

  (v_foo = ScalarArrays::sa_[0]);
  LINE(8,x_print_r(v_foo));
  (v_bar = LINE(10,x_each(ref(v_foo))));
  LINE(11,x_print_r(v_bar));
  (v_bar = LINE(13,x_each(ref(v_foo))));
  LINE(14,x_print_r(v_bar));
  (v_bar = LINE(16,x_each(ref(v_foo))));
  LINE(17,x_print_r(v_bar));
  df_lambda_1(LINE(20,x_each(ref(v_foo))), v_zot, v_zing);
  echo(LINE(21,concat5("zot ", toString(v_zot), " zing ", toString(v_zing), "\n")));
  LINE(23,x_print_r(ScalarArrays::sa_[1]));
  df_lambda_2(ScalarArrays::sa_[1], v_a, v_b, v_c);
  echo(concat("a :", LINE(26,concat6(toString(v_a), ": b :", toString(v_b), ": c :", toString(v_c), ":\n"))));
  df_lambda_3(ScalarArrays::sa_[2], v_a, v_b, v_c);
  echo(concat("a :", LINE(29,concat6(toString(v_a), ": b :", toString(v_b), ": c :", toString(v_c), ":\n"))));
  (v_foo = df_lambda_4(ScalarArrays::sa_[3], v_a, v_b, v_c));
  echo(concat("a :", LINE(32,concat6(toString(v_a), ": b :", toString(v_b), ": c :", toString(v_c), ":\n"))));
  echo(LINE(34,concat3("foo ", toString(v_foo), "\n")));
  (v_foo = ScalarArrays::sa_[0]);
  (v_key = 1LL);
  LOOP_COUNTER(1);
  {
    while (!same(v_key, null)) {
      LOOP_COUNTER_CHECK(1);
      {
        df_lambda_5(LINE(41,x_each(ref(v_foo))), v_key, v_val);
        echo(LINE(42,concat4(toString(v_key), " => ", toString(v_val), "\n")));
        LINE(43,x_next(ref(v_foo)));
        echo("current is:\n");
        LINE(45,x_var_dump(1, x_current(ref(v_foo))));
      }
    }
  }
  echo("\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
