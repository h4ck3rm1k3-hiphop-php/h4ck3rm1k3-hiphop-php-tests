
#ifndef __GENERATED_php_roadsend_pcc_tests_globals_h__
#define __GENERATED_php_roadsend_pcc_tests_globals_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/globals.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_funoftwoglobals();
Variant pm_php$roadsend$pcc$tests$globals_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_funofoneglobal();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_globals_h__
