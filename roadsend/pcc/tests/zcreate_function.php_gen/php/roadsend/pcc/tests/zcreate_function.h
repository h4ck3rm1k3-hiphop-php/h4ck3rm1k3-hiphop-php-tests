
#ifndef __GENERATED_php_roadsend_pcc_tests_zcreate_function_h__
#define __GENERATED_php_roadsend_pcc_tests_zcreate_function_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zcreate_function.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$zcreate_function_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_process(Variant v_var1, Variant v_var2, CVarRef v_farr);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zcreate_function_h__
