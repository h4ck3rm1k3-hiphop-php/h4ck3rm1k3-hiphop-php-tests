
#include <php/roadsend/pcc/tests/zcreate_function.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/zcreate_function.php line 18 */
void f_process(Variant v_var1, Variant v_var2, CVarRef v_farr) {
  FUNCTION_INJECTION(process);
  int64 v_f = 0;

  {
    LOOP_COUNTER(1);
    for ((v_f = 0LL); less(v_f, LINE(20,x_count(v_farr))); v_f++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(concat(toString(LINE(21,invoke((toString(v_farr.rvalAt(v_f))), Array(ArrayInit(2).set(0, ref(v_var1)).set(1, ref(v_var2)).create()), -1))), "\n"));
      }
    }
  }
} /* function */
Variant pm_php$roadsend$pcc$tests$zcreate_function_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/zcreate_function.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$zcreate_function_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_newfunc __attribute__((__unused__)) = (variables != gVariables) ? variables->get("newfunc") : g->GV(newfunc);
  Variant &v_f1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f1") : g->GV(f1);
  Variant &v_f2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f2") : g->GV(f2);
  Variant &v_f3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f3") : g->GV(f3);
  Variant &v_farr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("farr") : g->GV(farr);
  Variant &v_garr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("garr") : g->GV(garr);
  Variant &v_av __attribute__((__unused__)) = (variables != gVariables) ? variables->get("av") : g->GV(av);
  Variant &v_sv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sv") : g->GV(sv);

  (v_newfunc = LINE(3,"df_lambda_1"));
  echo(concat(toString(LINE(6,invoke(toString(v_newfunc), Array(ArrayInit(2).set(0, 2LL).set(1, 2.7182818284589998 /* M_E */).create()), -1))), "\n"));
  echo("\nOr, perhaps to have general handler function that can apply a set of operations to a list of parameters:\n\nExample 2. Making a general processing function with create_function()\n\n");
  (v_f1 = "if ($a >=0) {return \"b*a^2 = \".$b*sqrt($a);} else {return false;}");
  (v_f2 = "return \"min(b^2+a, a^2,b) = \".min($a*$a+$b,$b*$b+$a);");
  (v_f3 = "if ($a > 0 && $b != 0) {return \"ln(a)/b = \".log($a)/$b; } else { return false; }");
  (v_farr = (assignCallTemp(eo_0, LINE(30,"df_lambda_2")),assignCallTemp(eo_1, LINE(31,"df_lambda_3")),assignCallTemp(eo_2, LINE(32,x_create_function("$a,$b", toString(v_f1)))),assignCallTemp(eo_3, LINE(33,x_create_function("$a,$b", toString(v_f2)))),assignCallTemp(eo_4, LINE(34,x_create_function("$a,$b", toString(v_f3)))),Array(ArrayInit(5).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).set(4, eo_4).create())));
  echo("\nUsing the first array of anonymous functions\n");
  echo("parameters: 2.3445, M_PI\n");
  LINE(39,f_process(2.3445, 3.1415926535898002 /* M_PI */, v_farr));
  (v_garr = (assignCallTemp(eo_0, LINE(44,"df_lambda_4")),assignCallTemp(eo_1, LINE(45,"df_lambda_5")),assignCallTemp(eo_2, LINE(46,"df_lambda_6")),Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, eo_2).create())));
  echo("\nUsing the second array of anonymous functions\n");
  LINE(49,f_process("Twas brilling and the slithy toves", "Twas the night", v_garr));
  echo("\n\t\t\t    and when you run the code above, the output will be:\n\nUsing the first array of anonymous functions\n\t\t\t      parameters: 2.3445, M_PI\n\t\t\t      some trig: -1.6291725057799\n\t\t\t      a hypotenuse: 3.9199852871011\n\t\t\t      b*a^2 = 4.8103313314525\n\t\t\t      min(b^2+a, a^2,b) = 8.6382729035898\n\t\t\t      ln(a/b) = 0.27122299212594\n\n\t\t\t      Using the second array of anonymous functions\n\t\t\t      ** \"");
  echo("Twas the night\" and \"Twas brilling and the slithy toves\"\n\t\t\t      ** Look the same to me! (looking at the first 3 chars)\n     CRCs: -725381282 , 1908338681\n\t\t\t      similar(a,b) = 11(45.833333333333%)\n\n     But perhaps the most common use for of lambda-style (anonymous) functions is to create callback functions, for example when using array_walk() or usort()\n\n     Example 3. Using anonymous functi");
  echo("ons as callback functions\n       ");
  (v_av = ScalarArrays::sa_[0]);
  LINE(73,(assignCallTemp(eo_0, ref(v_av)),assignCallTemp(eo_1, "df_lambda_7"),x_array_walk(eo_0, eo_1)));
  LINE(74,x_print_r(v_av));
  echo("\noutputs:\n\nArray\n(\n [0] => the mango\n [1] => a mango\n [2] => that mango\n [3] => this mango\n )\n\n     an array of strings ordered from shorter to longer\n");
  (v_sv = ScalarArrays::sa_[1]);
  LINE(91,x_print_r(v_sv));
  echo("\noutputs:\n\nArray\n(\n [0] => small\n [1] => larger\n [2] => a big string\n [3] => it is a string thing\n )\n\n     sort it from longer to shorter\n");
  LINE(108,(assignCallTemp(eo_0, ref(v_sv)),assignCallTemp(eo_1, "df_lambda_8"),x_usort(eo_0, eo_1)));
  LINE(109,x_print_r(v_sv));
  echo("\noutputs:\n\nArray\n(\n [0] => it is a string thing\n [1] => a big string\n [2] => larger\n [3] => small\n )\n\n     \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
