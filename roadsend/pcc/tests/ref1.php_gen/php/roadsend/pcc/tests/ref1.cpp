
#include <php/roadsend/pcc/tests/ref1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$ref1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/ref1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$ref1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_zot __attribute__((__unused__)) = (variables != gVariables) ? variables->get("zot") : g->GV(zot);
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_foo = 5LL);
  (v_zot = 7LL);
  (v_bar = ref(v_foo));
  (v_bar = 8LL);
  (v_bar = ref(v_zot));
  (v_bar = 9LL);
  echo(LINE(9,concat6(toString(v_foo), ", ", toString(v_zot), ", ", toString(v_bar), "\n")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
