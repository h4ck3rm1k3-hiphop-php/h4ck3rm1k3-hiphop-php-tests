
#ifndef __GENERATED_php_roadsend_pcc_tests_obj_arrayaccess_h__
#define __GENERATED_php_roadsend_pcc_tests_obj_arrayaccess_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/obj-arrayaccess.fw.h>

// Declarations
#include <cls/object.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$obj_arrayaccess_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_object(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_obj_arrayaccess_h__
