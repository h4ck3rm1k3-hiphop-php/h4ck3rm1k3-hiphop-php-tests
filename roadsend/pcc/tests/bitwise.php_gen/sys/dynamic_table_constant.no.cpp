
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_CONSTFOO;
extern const int64 k_CONSTBAR;
extern const int64 k_CONSTBAZ;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x6514C2CE0D25F9F8LL, k_CONSTBAR, CONSTBAR);
      break;
    case 6:
      HASH_RETURN(0x492A8A7F3DBC0326LL, k_CONSTFOO, CONSTFOO);
      HASH_RETURN(0x0D0293C3E44AEB96LL, k_CONSTBAZ, CONSTBAZ);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
