
#include <php/roadsend/pcc/tests/bitwise.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const int64 k_CONSTFOO = 1LL;
const int64 k_CONSTBAR = 2LL;
const int64 k_CONSTBAZ = 4LL;

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$bitwise_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/bitwise.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$bitwise_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  ;
  ;
  ;
  print("the angle of the 7.\n");
  (v_a = 1LL);
  (v_b = 3LL);
  (v_c = 39LL);
  echo((LINE(14,concat3("foo:", (toString(bitwise_and(bitwise_and(v_a, v_b), v_c))), ":foo\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
