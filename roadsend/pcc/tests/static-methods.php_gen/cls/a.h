
#ifndef __GENERATED_cls_a_h__
#define __GENERATED_cls_a_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/static-methods.php line 56 */
class c_a : virtual public ObjectData {
  BEGIN_CLASS_MAP(a)
  END_CLASS_MAP(a)
  DECLARE_CLASS(a, A, ObjectData)
  void init();
  public: static void ti_foo(const char* cls);
  public: static void ti_callfoo(const char* cls);
  public: static void t_foo() { ti_foo("a"); }
  public: static void t_callfoo() { ti_callfoo("a"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_a_h__
