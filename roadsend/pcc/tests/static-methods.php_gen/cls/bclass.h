
#ifndef __GENERATED_cls_bclass_h__
#define __GENERATED_cls_bclass_h__

#include <cls/aclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/static-methods.php line 29 */
class c_bclass : virtual public c_aclass {
  BEGIN_CLASS_MAP(bclass)
    PARENT_CLASS(aclass)
  END_CLASS_MAP(bclass)
  DECLARE_CLASS(bclass, bclass, aclass)
  void init();
  public: String m_bprop;
  public: void t_bclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void t_bmethod();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_bclass_h__
