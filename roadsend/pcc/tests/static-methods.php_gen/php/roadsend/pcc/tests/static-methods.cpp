
#include <php/roadsend/pcc/tests/static-methods.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/static-methods.php line 3 */
Variant c_notparent::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_notparent::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_notparent::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("nprop", m_nprop));
  c_ObjectData::o_get(props);
}
bool c_notparent::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x362DFBB73A521BEALL, nprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_notparent::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x362DFBB73A521BEALL, m_nprop,
                         nprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_notparent::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x362DFBB73A521BEALL, m_nprop,
                      nprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_notparent::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_notparent::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(notparent)
ObjectData *c_notparent::cloneImpl() {
  c_notparent *obj = NEW(c_notparent)();
  cloneSet(obj);
  return obj;
}
void c_notparent::cloneSet(c_notparent *clone) {
  clone->m_nprop = m_nprop;
  ObjectData::cloneSet(clone);
}
Variant c_notparent::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_notparent::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_notparent::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_notparent$os_get(const char *s) {
  return c_notparent::os_get(s, -1);
}
Variant &cw_notparent$os_lval(const char *s) {
  return c_notparent::os_lval(s, -1);
}
Variant cw_notparent$os_constant(const char *s) {
  return c_notparent::os_constant(s);
}
Variant cw_notparent$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_notparent::os_invoke(c, s, params, -1, fatal);
}
void c_notparent::init() {
  m_nprop = "foo";
}
/* SRC: roadsend/pcc/tests/static-methods.php line 6 */
void c_notparent::t_nmethod() {
  INSTANCE_METHOD_INJECTION(notparent, notparent::nmethod);
  echo("in nmethod()\n");
  LINE(8,x_var_dump(1, ((Object)(this))));
  echo("/in nmethod()\n");
} /* function */
/* SRC: roadsend/pcc/tests/static-methods.php line 13 */
Variant c_aclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_aclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_aclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("aprop", m_aprop));
  c_ObjectData::o_get(props);
}
bool c_aclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x572D6D4C827191C9LL, aprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_aclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x572D6D4C827191C9LL, m_aprop,
                         aprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_aclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x572D6D4C827191C9LL, m_aprop,
                      aprop, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_aclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_aclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(aclass)
ObjectData *c_aclass::create() {
  init();
  t_aclass();
  return this;
}
ObjectData *c_aclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_aclass::cloneImpl() {
  c_aclass *obj = NEW(c_aclass)();
  cloneSet(obj);
  return obj;
}
void c_aclass::cloneSet(c_aclass *clone) {
  clone->m_aprop = m_aprop;
  ObjectData::cloneSet(clone);
}
Variant c_aclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_aclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_aclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_aclass$os_get(const char *s) {
  return c_aclass::os_get(s, -1);
}
Variant &cw_aclass$os_lval(const char *s) {
  return c_aclass::os_lval(s, -1);
}
Variant cw_aclass$os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
Variant cw_aclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_aclass::os_invoke(c, s, params, -1, fatal);
}
void c_aclass::init() {
  m_aprop = "wrong";
}
/* SRC: roadsend/pcc/tests/static-methods.php line 16 */
void c_aclass::t_aclass() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::aclass);
  bool oldInCtor = gasInCtor(true);
  echo("in aclass()\n");
  LINE(18,x_var_dump(1, ((Object)(this))));
  echo("/in aclass()\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/static-methods.php line 22 */
void c_aclass::t_amethod() {
  INSTANCE_METHOD_INJECTION(aclass, aclass::amethod);
  echo("in amethod()\n");
  LINE(24,x_var_dump(1, ((Object)(this))));
  echo("/in amethod()\n");
} /* function */
/* SRC: roadsend/pcc/tests/static-methods.php line 29 */
Variant c_bclass::os_get(const char *s, int64 hash) {
  return c_aclass::os_get(s, hash);
}
Variant &c_bclass::os_lval(const char *s, int64 hash) {
  return c_aclass::os_lval(s, hash);
}
void c_bclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("bprop", m_bprop));
  c_aclass::o_get(props);
}
bool c_bclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x7A17297E520DDBABLL, bprop, 5);
      break;
    default:
      break;
  }
  return c_aclass::o_exists(s, hash);
}
Variant c_bclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x7A17297E520DDBABLL, m_bprop,
                         bprop, 5);
      break;
    default:
      break;
  }
  return c_aclass::o_get(s, hash);
}
Variant c_bclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x7A17297E520DDBABLL, m_bprop,
                      bprop, 5);
      break;
    default:
      break;
  }
  return c_aclass::o_set(s, hash, v, forInit);
}
Variant &c_bclass::o_lval(CStrRef s, int64 hash) {
  return c_aclass::o_lval(s, hash);
}
Variant c_bclass::os_constant(const char *s) {
  return c_aclass::os_constant(s);
}
IMPLEMENT_CLASS(bclass)
ObjectData *c_bclass::create() {
  init();
  t_bclass();
  return this;
}
ObjectData *c_bclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_bclass::cloneImpl() {
  c_bclass *obj = NEW(c_bclass)();
  cloneSet(obj);
  return obj;
}
void c_bclass::cloneSet(c_bclass *clone) {
  clone->m_bprop = m_bprop;
  c_aclass::cloneSet(clone);
}
Variant c_bclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x32D1D146CABFDC16LL, bmethod) {
        return (t_bmethod(), null);
      }
      break;
    default:
      break;
  }
  return c_aclass::o_invoke(s, params, hash, fatal);
}
Variant c_bclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x32D1D146CABFDC16LL, bmethod) {
        return (t_bmethod(), null);
      }
      break;
    default:
      break;
  }
  return c_aclass::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_aclass::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bclass$os_get(const char *s) {
  return c_bclass::os_get(s, -1);
}
Variant &cw_bclass$os_lval(const char *s) {
  return c_bclass::os_lval(s, -1);
}
Variant cw_bclass$os_constant(const char *s) {
  return c_bclass::os_constant(s);
}
Variant cw_bclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bclass::os_invoke(c, s, params, -1, fatal);
}
void c_bclass::init() {
  c_aclass::init();
  m_bprop = "okay";
}
/* SRC: roadsend/pcc/tests/static-methods.php line 32 */
void c_bclass::t_bclass() {
  INSTANCE_METHOD_INJECTION(bclass, bclass::bclass);
  bool oldInCtor = gasInCtor(true);
  (m_aprop = "right");
  echo("in bclass()\n");
  LINE(35,c_aclass::t_aclass());
  echo("/in bclass()\n");
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/static-methods.php line 39 */
void c_bclass::t_bmethod() {
  INSTANCE_METHOD_INJECTION(bclass, bclass::bmethod);
  (m_aprop = "the current instance");
  LINE(41,c_aclass::t_aclass());
  LINE(42,c_aclass::t_amethod());
} /* function */
/* SRC: roadsend/pcc/tests/static-methods.php line 56 */
Variant c_a::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_a::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_a::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_a::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_a::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_a::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_a::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_a::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(a)
ObjectData *c_a::cloneImpl() {
  c_a *obj = NEW(c_a)();
  cloneSet(obj);
  return obj;
}
void c_a::cloneSet(c_a *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_a::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (ti_foo(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_a::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (ti_foo(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_a::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (ti_foo(c), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_a$os_get(const char *s) {
  return c_a::os_get(s, -1);
}
Variant &cw_a$os_lval(const char *s) {
  return c_a::os_lval(s, -1);
}
Variant cw_a$os_constant(const char *s) {
  return c_a::os_constant(s);
}
Variant cw_a$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_a::os_invoke(c, s, params, -1, fatal);
}
void c_a::init() {
}
/* SRC: roadsend/pcc/tests/static-methods.php line 57 */
void c_a::ti_foo(const char* cls) {
  STATIC_METHOD_INJECTION(A, A::foo);
  echo("A::foo()");
} /* function */
/* SRC: roadsend/pcc/tests/static-methods.php line 61 */
void c_a::ti_callfoo(const char* cls) {
  STATIC_METHOD_INJECTION(A, A::callFoo);
  LINE(62,c_a::t_foo());
} /* function */
/* SRC: roadsend/pcc/tests/static-methods.php line 66 */
Variant c_b::os_get(const char *s, int64 hash) {
  return c_a::os_get(s, hash);
}
Variant &c_b::os_lval(const char *s, int64 hash) {
  return c_a::os_lval(s, hash);
}
void c_b::o_get(ArrayElementVec &props) const {
  c_a::o_get(props);
}
bool c_b::o_exists(CStrRef s, int64 hash) const {
  return c_a::o_exists(s, hash);
}
Variant c_b::o_get(CStrRef s, int64 hash) {
  return c_a::o_get(s, hash);
}
Variant c_b::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_a::o_set(s, hash, v, forInit);
}
Variant &c_b::o_lval(CStrRef s, int64 hash) {
  return c_a::o_lval(s, hash);
}
Variant c_b::os_constant(const char *s) {
  return c_a::os_constant(s);
}
IMPLEMENT_CLASS(b)
ObjectData *c_b::cloneImpl() {
  c_b *obj = NEW(c_b)();
  cloneSet(obj);
  return obj;
}
void c_b::cloneSet(c_b *clone) {
  c_a::cloneSet(clone);
}
Variant c_b::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (ti_foo(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke(s, params, hash, fatal);
}
Variant c_b::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (ti_foo(o_getClassName()), null);
      }
      break;
    default:
      break;
  }
  return c_a::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_b::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x6176C0B993BF5914LL, foo) {
        return (ti_foo(c), null);
      }
      break;
    default:
      break;
  }
  return c_a::os_invoke(c, s, params, hash, fatal);
}
Variant cw_b$os_get(const char *s) {
  return c_b::os_get(s, -1);
}
Variant &cw_b$os_lval(const char *s) {
  return c_b::os_lval(s, -1);
}
Variant cw_b$os_constant(const char *s) {
  return c_b::os_constant(s);
}
Variant cw_b$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_b::os_invoke(c, s, params, -1, fatal);
}
void c_b::init() {
  c_a::init();
}
/* SRC: roadsend/pcc/tests/static-methods.php line 67 */
void c_b::ti_foo(const char* cls) {
  STATIC_METHOD_INJECTION(B, B::foo);
  echo("B::foo()");
} /* function */
Object co_notparent(CArrRef params, bool init /* = true */) {
  return Object(p_notparent(NEW(c_notparent)())->dynCreate(params, init));
}
Object co_aclass(CArrRef params, bool init /* = true */) {
  return Object(p_aclass(NEW(c_aclass)())->dynCreate(params, init));
}
Object co_bclass(CArrRef params, bool init /* = true */) {
  return Object(p_bclass(NEW(c_bclass)())->dynCreate(params, init));
}
Object co_a(CArrRef params, bool init /* = true */) {
  return Object(p_a(NEW(c_a)())->dynCreate(params, init));
}
Object co_b(CArrRef params, bool init /* = true */) {
  return Object(p_b(NEW(c_b)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$static_methods_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/static-methods.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$static_methods_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_foo = ((Object)(LINE(49,p_bclass(p_bclass(NEWOBJ(c_bclass)())->create())))));
  LINE(53,v_foo.o_invoke_few_args("bmethod", 0x32D1D146CABFDC16LL, 0));
  LINE(72,c_b::t_callfoo());
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
