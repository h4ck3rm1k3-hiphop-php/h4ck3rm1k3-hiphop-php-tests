
#ifndef __GENERATED_php_roadsend_pcc_tests_static_methods_fw_h__
#define __GENERATED_php_roadsend_pcc_tests_static_methods_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(notparent)
FORWARD_DECLARE_CLASS(aclass)
FORWARD_DECLARE_CLASS(bclass)
FORWARD_DECLARE_CLASS(a)
FORWARD_DECLARE_CLASS(b)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_static_methods_fw_h__
