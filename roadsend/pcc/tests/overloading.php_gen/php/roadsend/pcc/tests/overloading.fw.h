
#ifndef __GENERATED_php_roadsend_pcc_tests_overloading_fw_h__
#define __GENERATED_php_roadsend_pcc_tests_overloading_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(tester)
FORWARD_DECLARE_CLASS(setter)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_overloading_fw_h__
