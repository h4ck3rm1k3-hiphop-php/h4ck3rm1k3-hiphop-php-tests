
#ifndef __GENERATED_cls_setter_h__
#define __GENERATED_cls_setter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/overloading.php line 38 */
class c_setter : virtual public ObjectData {
  BEGIN_CLASS_MAP(setter)
  END_CLASS_MAP(setter)
  DECLARE_CLASS(setter, Setter, ObjectData)
  void init();
  public: Variant m_n;
  public: Array m_x;
  public: Variant t___get(Variant v_nm);
  public: Variant &___lval(Variant v_nm);
  public: Variant t___set(Variant v_nm, Variant v_val);
  public: bool t___isset(Variant v_nm);
  public: Variant t___unset(Variant v_nm);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_setter_h__
