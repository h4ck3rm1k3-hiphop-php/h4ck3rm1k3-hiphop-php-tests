
#include <php/roadsend/pcc/tests/string-ref.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/string-ref.php line 3 */
String f_foo() {
  FUNCTION_INJECTION(foo);
  return "f";
} /* function */
Variant pm_php$roadsend$pcc$tests$string_ref_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/string-ref.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$string_ref_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_bar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("bar") : g->GV(bar);

  (v_bar = "ooo");
  v_bar.set(0LL, (LINE(8,f_foo())), 0x77CFA1EEF01BCA90LL);
  echo(LINE(10,concat3(toString(v_bar.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_bar.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toString(v_bar.rvalAt(2LL, 0x486AFCC090D5F98CLL)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
