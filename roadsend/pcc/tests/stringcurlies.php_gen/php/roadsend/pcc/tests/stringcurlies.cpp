
#include <php/roadsend/pcc/tests/stringcurlies.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_a = "a";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$tests$stringcurlies_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/stringcurlies.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$stringcurlies_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_beer __attribute__((__unused__)) = (variables != gVariables) ? variables->get("beer") : g->GV(beer);
  Variant &v_keg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("keg") : g->GV(keg);
  Variant &v_beers __attribute__((__unused__)) = (variables != gVariables) ? variables->get("beers") : g->GV(beers);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  (v_beer = "mmm, mmm, good");
  v_keg.set("a", (17LL), 0x4292CEE227B9150ALL);
  v_keg.set(3LL, (18LL), 0x135FDDF6A6BFBBDDLL);
  echo(LINE(9,concat3("a few ", toString(v_beers), "\n")));
  echo(LINE(10,concat3("a few ", toString(v_beer), "s\n")));
  echo(LINE(11,concat3("a few ", toString(v_beer), "s\n")));
  echo(LINE(13,concat3("from a large", toString(v_keg.rvalAt("a", 0x4292CEE227B9150ALL)), "\n")));
  echo(LINE(14,concat3("from a large", toString(v_keg.rvalAt("a", 0x4292CEE227B9150ALL)), "\n")));
  echo(LINE(15,concat3("from a large", toString(v_keg.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), "\n")));
  echo("foo[asdf]");
  echo(LINE(19,concat3("from a large", toString(variables->get(toString(v_keg.rvalAt("a", 0x4292CEE227B9150ALL)))), "\n")));
  echo(LINE(20,concat3("from a large", toString(variables->get(toString(v_keg.rvalAt(k_a)))), "\n")));
  echo(LINE(21,concat3("from a large{", toString(v_keg.rvalAt("a", 0x4292CEE227B9150ALL)), "\n")));
  echo(LINE(23,concat3("from a large\\{", toString(variables->get(toString(v_keg.rvalAt("a", 0x4292CEE227B9150ALL)))), "\n")));
  echo(LINE(25,concat3("from a large", toString(v_keg.rvalAt("a", 0x4292CEE227B9150ALL)), "}\n")));
  echo("\nPHP breaks the strings up into constant strings and non-constant strings in \ntheir lexer.  The constant strings don't eat the \\ in \"\\{\", but the non-\nconstant strings do.  (The key difference between a constant and non-constant\nstring is the presense of a non-escaped $).\n\nTest the \\{ stuff:\n");
  echo("\\{");
  echo("\\{\n");
  echo("\\{$\n");
  echo("${\n");
  echo("{\n");
  echo("\\.3");
  (v_foo = "bar");
  echo("\n$foo\n");
  echo(LINE(43,concat3("\n\\{", toString(v_foo), "}\n")));
  echo("\n${foo}\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
