
#ifndef __GENERATED_php_roadsend_pcc_tests_autoload_member_h__
#define __GENERATED_php_roadsend_pcc_tests_autoload_member_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/autoload-member.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$autoload_member_php(bool incOnce = false, LVariableTable* variables = NULL);
void f___autoload(CVarRef v_class);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_autoload_member_h__
