
#ifndef __GENERATED_php_roadsend_pcc_tests_zthis_h__
#define __GENERATED_php_roadsend_pcc_tests_zthis_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zthis.fw.h>

// Declarations
#include <cls/foo.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_foo2(Variant v_foo);
Variant pm_php$roadsend$pcc$tests$zthis_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_foo(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zthis_h__
