
#include <php/roadsend/pcc/tests/magic-constants.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/magic-constants.php line 3 */
Variant c_foo::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_foo::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_foo::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_foo::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_foo::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_foo::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_foo::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_foo::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(foo)
ObjectData *c_foo::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_foo::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_foo::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_foo::cloneImpl() {
  c_foo *obj = NEW(c_foo)();
  cloneSet(obj);
  return obj;
}
void c_foo::cloneSet(c_foo *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_foo::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x34E5A547E60630AALL, testm) {
        return (t_testm(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x35AAD1C9B8C0D4BBLL, testm2) {
        return (t_testm2(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_foo::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x34E5A547E60630AALL, testm) {
        return (t_testm(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x35AAD1C9B8C0D4BBLL, testm2) {
        return (t_testm2(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_foo::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_foo$os_get(const char *s) {
  return c_foo::os_get(s, -1);
}
Variant &cw_foo$os_lval(const char *s) {
  return c_foo::os_lval(s, -1);
}
Variant cw_foo$os_constant(const char *s) {
  return c_foo::os_constant(s);
}
Variant cw_foo$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_foo::os_invoke(c, s, params, -1, fatal);
}
void c_foo::init() {
}
/* SRC: roadsend/pcc/tests/magic-constants.php line 5 */
void c_foo::t___construct() {
  INSTANCE_METHOD_INJECTION(Foo, Foo::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  echo("Foo -- Foo::__construct\n");
  echo(LINE(7,(assignCallTemp(eo_1, x_basename(get_source_filename("roadsend/pcc/tests/magic-constants.php"))),concat3("location is ", eo_1, " on line 7\n"))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/magic-constants.php line 10 */
void c_foo::t_testm() {
  INSTANCE_METHOD_INJECTION(Foo, Foo::testm);
  echo("Foo -- Foo::testm\n");
  echo("function: testm\n");
} /* function */
/* SRC: roadsend/pcc/tests/magic-constants.php line 15 */
void c_foo::t_testm2() {
  INSTANCE_METHOD_INJECTION(Foo, Foo::testm2);
  echo("Foo -- Foo::testm2\n");
} /* function */
/* SRC: roadsend/pcc/tests/magic-constants.php line 22 */
Variant c_bar::os_get(const char *s, int64 hash) {
  return c_foo::os_get(s, hash);
}
Variant &c_bar::os_lval(const char *s, int64 hash) {
  return c_foo::os_lval(s, hash);
}
void c_bar::o_get(ArrayElementVec &props) const {
  c_foo::o_get(props);
}
bool c_bar::o_exists(CStrRef s, int64 hash) const {
  return c_foo::o_exists(s, hash);
}
Variant c_bar::o_get(CStrRef s, int64 hash) {
  return c_foo::o_get(s, hash);
}
Variant c_bar::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_foo::o_set(s, hash, v, forInit);
}
Variant &c_bar::o_lval(CStrRef s, int64 hash) {
  return c_foo::o_lval(s, hash);
}
Variant c_bar::os_constant(const char *s) {
  return c_foo::os_constant(s);
}
IMPLEMENT_CLASS(bar)
ObjectData *c_bar::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_bar::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_bar::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_bar::cloneImpl() {
  c_bar *obj = NEW(c_bar)();
  cloneSet(obj);
  return obj;
}
void c_bar::cloneSet(c_bar *clone) {
  c_foo::cloneSet(clone);
}
Variant c_bar::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x34E5A547E60630AALL, testm) {
        return (t_testm(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x35AAD1C9B8C0D4BBLL, testm2) {
        return (t_testm2(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke(s, params, hash, fatal);
}
Variant c_bar::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 2:
      HASH_GUARD(0x34E5A547E60630AALL, testm) {
        return (t_testm(), null);
      }
      break;
    case 3:
      HASH_GUARD(0x35AAD1C9B8C0D4BBLL, testm2) {
        return (t_testm2(), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_foo::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_bar::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_foo::os_invoke(c, s, params, hash, fatal);
}
Variant cw_bar$os_get(const char *s) {
  return c_bar::os_get(s, -1);
}
Variant &cw_bar$os_lval(const char *s) {
  return c_bar::os_lval(s, -1);
}
Variant cw_bar$os_constant(const char *s) {
  return c_bar::os_constant(s);
}
Variant cw_bar$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_bar::os_invoke(c, s, params, -1, fatal);
}
void c_bar::init() {
  c_foo::init();
}
/* SRC: roadsend/pcc/tests/magic-constants.php line 24 */
void c_bar::t___construct() {
  INSTANCE_METHOD_INJECTION(BaR, BaR::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  echo("BaR -- BaR::__construct\n");
  echo("function: __construct\n");
  echo(LINE(27,(assignCallTemp(eo_1, x_basename(get_source_filename("roadsend/pcc/tests/magic-constants.php"))),concat3("location is ", eo_1, " on line 27\n"))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/magic-constants.php line 30 */
void c_bar::t_testm2() {
  INSTANCE_METHOD_INJECTION(BaR, BaR::testm2);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  echo("BaR -- BaR::testm2\n");
  echo(LINE(32,(assignCallTemp(eo_1, x_basename(get_source_filename("roadsend/pcc/tests/magic-constants.php"))),concat3("location is ", eo_1, " on line 32\n"))));
} /* function */
/* SRC: roadsend/pcc/tests/magic-constants.php line 36 */
void f_testfunc() {
  FUNCTION_INJECTION(testfunc);
  echo("function: testfunc\n");
} /* function */
Object co_foo(CArrRef params, bool init /* = true */) {
  return Object(p_foo(NEW(c_foo)())->dynCreate(params, init));
}
Object co_bar(CArrRef params, bool init /* = true */) {
  return Object(p_bar(NEW(c_bar)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$magic_constants_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/magic-constants.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$magic_constants_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_f __attribute__((__unused__)) = (variables != gVariables) ? variables->get("f") : g->GV(f);

  echo(" -- , function: \n");
  echo(LINE(42,(assignCallTemp(eo_1, x_basename(get_source_filename("roadsend/pcc/tests/magic-constants.php"))),concat3("location is ", eo_1, " on line 42\n"))));
  (v_f = ((Object)(LINE(44,p_foo(p_foo(NEWOBJ(c_foo)())->create())))));
  LINE(45,v_f.o_invoke_few_args("testm", 0x34E5A547E60630AALL, 0));
  LINE(46,v_f.o_invoke_few_args("testm2", 0x35AAD1C9B8C0D4BBLL, 0));
  (v_f = ((Object)(LINE(48,p_bar(p_bar(NEWOBJ(c_bar)())->create())))));
  LINE(49,v_f.o_invoke_few_args("testm", 0x34E5A547E60630AALL, 0));
  LINE(50,v_f.o_invoke_few_args("testm2", 0x35AAD1C9B8C0D4BBLL, 0));
  LINE(52,f_testfunc());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
