
#ifndef __GENERATED_php_roadsend_pcc_tests_zfunction_call_with_global_and_static_vars_h__
#define __GENERATED_php_roadsend_pcc_tests_zfunction_call_with_global_and_static_vars_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/zfunction_call_with_global_and_static_vars.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$zfunction_call_with_global_and_static_vars_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_test();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_zfunction_call_with_global_and_static_vars_h__
