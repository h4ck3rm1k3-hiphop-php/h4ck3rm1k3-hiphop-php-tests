
#include <php/roadsend/pcc/tests/zfunction_call_with_global_and_static_vars.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/zfunction_call_with_global_and_static_vars.php line 3 */
void f_test() {
  FUNCTION_INJECTION(Test);
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_a __attribute__((__unused__)) = g->sv_test_DupIda;
  bool &inited_sv_a __attribute__((__unused__)) = g->inited_sv_test_DupIda;
  Variant &gv_b __attribute__((__unused__)) = g->GV(b);
  int64 v_c = 0;

  if (!inited_sv_a) {
    (sv_a = 1LL);
    inited_sv_a = true;
  }
  (v_c = 1LL);
  (gv_b = 5LL);
  echo(LINE(9,concat5("one ", toString(sv_a), " ", toString(gv_b), " one")));
  sv_a++;
  v_c++;
  echo(LINE(12,concat5("two ", toString(sv_a), " ", toString(v_c), " two")));
} /* function */
Variant pm_php$roadsend$pcc$tests$zfunction_call_with_global_and_static_vars_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/zfunction_call_with_global_and_static_vars.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$zfunction_call_with_global_and_static_vars_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);

  LINE(1,x_error_reporting(toInt32(0LL)));
  (v_a = 10LL);
  LINE(14,f_test());
  echo(concat("buckle ", LINE(15,concat6(toString(v_a), " ", toString(v_b), " ", toString(v_c), " my"))));
  LINE(16,f_test());
  echo(concat("shoe ", LINE(17,concat6(toString(v_a), " ", toString(v_b), " ", toString(v_c), " shoe"))));
  LINE(18,f_test());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
