
#ifndef __GENERATED_php_roadsend_pcc_tests_varargs_h__
#define __GENERATED_php_roadsend_pcc_tests_varargs_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/tests/varargs.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$tests$varargs_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_crud(int64 v_a, CVarRef v_b = "asdf", CVarRef v_c = "hkjl");

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_tests_varargs_h__
