
#include <php/roadsend/pcc/tests/varargs.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/varargs.php line 3 */
void f_crud(int64 v_a, CVarRef v_b //  = "asdf"
, CVarRef v_c //  = "hkjl"
) {
  FUNCTION_INJECTION(crud);
  {
    echo(toString(v_a));
    echo(toString(v_b));
    echo(concat(toString(v_c), "\n"));
  }
} /* function */
Variant pm_php$roadsend$pcc$tests$varargs_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/varargs.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$varargs_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(7,f_crud(9LL));
  LINE(8,f_crud(9LL, 10LL));
  LINE(9,f_crud(9LL, 10LL, 11LL));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
