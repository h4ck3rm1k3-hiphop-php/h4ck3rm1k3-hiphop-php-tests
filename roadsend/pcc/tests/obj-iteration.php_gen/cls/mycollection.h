
#ifndef __GENERATED_cls_mycollection_h__
#define __GENERATED_cls_mycollection_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/tests/obj-iteration.php line 51 */
class c_mycollection : virtual public c_iteratoraggregate {
  BEGIN_CLASS_MAP(mycollection)
    PARENT_CLASS(traversable)
    PARENT_CLASS(iteratoraggregate)
  END_CLASS_MAP(mycollection)
  DECLARE_CLASS(mycollection, MyCollection, ObjectData)
  void init();
  public: Array m_items;
  public: int64 m_count;
  public: p_myiterator t_getiterator();
  public: void t_add(CVarRef v_value);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mycollection_h__
