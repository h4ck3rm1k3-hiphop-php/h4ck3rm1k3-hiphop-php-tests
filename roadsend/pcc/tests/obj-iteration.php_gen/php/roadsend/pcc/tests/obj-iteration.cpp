
#include <php/roadsend/pcc/tests/obj-iteration.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 3 */
Variant c_myiterator::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myiterator::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myiterator::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("var", m_var.isReferenced() ? ref(m_var) : m_var));
  c_ObjectData::o_get(props);
}
bool c_myiterator::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x0953EE1CC9042120LL, var, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myiterator::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0953EE1CC9042120LL, m_var,
                         var, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_myiterator::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x0953EE1CC9042120LL, m_var,
                      var, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myiterator::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x0953EE1CC9042120LL, m_var,
                         var, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myiterator::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myiterator)
ObjectData *c_myiterator::create(Variant v_array) {
  init();
  t___construct(v_array);
  return this;
}
ObjectData *c_myiterator::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_myiterator::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_myiterator::cloneImpl() {
  c_myiterator *obj = NEW(c_myiterator)();
  cloneSet(obj);
  return obj;
}
void c_myiterator::cloneSet(c_myiterator *clone) {
  clone->m_var = m_var.isReferenced() ? ref(m_var) : m_var;
  ObjectData::cloneSet(clone);
}
Variant c_myiterator::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myiterator::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x56EDB60C824E8C51LL, key) {
        return (t_key());
      }
      break;
    case 4:
      HASH_GUARD(0x6413CB5154808C44LL, valid) {
        return (t_valid());
      }
      break;
    case 8:
      HASH_GUARD(0x3C6D50F3BB8102B8LL, next) {
        return (t_next());
      }
      break;
    case 10:
      HASH_GUARD(0x1670096FDE27AF6ALL, rewind) {
        return (t_rewind(), null);
      }
      break;
    case 12:
      HASH_GUARD(0x5B3A4A72846B21DCLL, current) {
        return (t_current());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myiterator::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myiterator$os_get(const char *s) {
  return c_myiterator::os_get(s, -1);
}
Variant &cw_myiterator$os_lval(const char *s) {
  return c_myiterator::os_lval(s, -1);
}
Variant cw_myiterator$os_constant(const char *s) {
  return c_myiterator::os_constant(s);
}
Variant cw_myiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myiterator::os_invoke(c, s, params, -1, fatal);
}
void c_myiterator::init() {
  m_var = ScalarArrays::sa_[0];
}
/* SRC: roadsend/pcc/tests/obj-iteration.php line 7 */
void c_myiterator::t___construct(Variant v_array) {
  INSTANCE_METHOD_INJECTION(MyIterator, MyIterator::__construct);
  bool oldInCtor = gasInCtor(true);
  if (LINE(9,x_is_array(v_array))) {
    (m_var = v_array);
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 14 */
void c_myiterator::t_rewind() {
  INSTANCE_METHOD_INJECTION(MyIterator, MyIterator::rewind);
  echo("rewinding\n");
  LINE(16,x_reset(ref(lval(m_var))));
} /* function */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 19 */
Variant c_myiterator::t_current() {
  INSTANCE_METHOD_INJECTION(MyIterator, MyIterator::current);
  Variant v_var;

  (v_var = LINE(20,x_current(ref(lval(m_var)))));
  echo(LINE(21,concat3("current: ", toString(v_var), "\n")));
  return v_var;
} /* function */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 25 */
Variant c_myiterator::t_key() {
  INSTANCE_METHOD_INJECTION(MyIterator, MyIterator::key);
  Variant v_var;

  (v_var = LINE(26,x_key(ref(lval(m_var)))));
  echo(LINE(27,concat3("key: ", toString(v_var), "\n")));
  return v_var;
} /* function */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 31 */
Variant c_myiterator::t_next() {
  INSTANCE_METHOD_INJECTION(MyIterator, MyIterator::next);
  Variant v_var;

  (v_var = LINE(32,x_next(ref(lval(m_var)))));
  echo(LINE(33,concat3("next: ", toString(v_var), "\n")));
  return v_var;
} /* function */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 37 */
bool c_myiterator::t_valid() {
  INSTANCE_METHOD_INJECTION(MyIterator, MyIterator::valid);
  bool v_var = false;

  (v_var = !same(LINE(38,o_root_invoke_few_args("current", 0x5B3A4A72846B21DCLL, 0)), false));
  echo(LINE(39,concat3("valid: ", toString(v_var), "\n")));
  return v_var;
} /* function */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 51 */
Variant c_mycollection::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_mycollection::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_mycollection::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("items", m_items));
  props.push_back(NEW(ArrayElement)("count", m_count));
  c_ObjectData::o_get(props);
}
bool c_mycollection::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x252EF5F42BAAFA42LL, items, 5);
      break;
    case 3:
      HASH_EXISTS_STRING(0x3D66B5980D54BABBLL, count, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_mycollection::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x252EF5F42BAAFA42LL, m_items,
                         items, 5);
      break;
    case 3:
      HASH_RETURN_STRING(0x3D66B5980D54BABBLL, m_count,
                         count, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_mycollection::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x252EF5F42BAAFA42LL, m_items,
                      items, 5);
      break;
    case 3:
      HASH_SET_STRING(0x3D66B5980D54BABBLL, m_count,
                      count, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_mycollection::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_mycollection::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(mycollection)
ObjectData *c_mycollection::cloneImpl() {
  c_mycollection *obj = NEW(c_mycollection)();
  cloneSet(obj);
  return obj;
}
void c_mycollection::cloneSet(c_mycollection *clone) {
  clone->m_items = m_items;
  clone->m_count = m_count;
  ObjectData::cloneSet(clone);
}
Variant c_mycollection::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_mycollection::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 3:
      HASH_GUARD(0x570B2E1232A12503LL, getiterator) {
        return (t_getiterator());
      }
      HASH_GUARD(0x15D34462FC79458BLL, add) {
        return (t_add(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_mycollection::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_mycollection$os_get(const char *s) {
  return c_mycollection::os_get(s, -1);
}
Variant &cw_mycollection$os_lval(const char *s) {
  return c_mycollection::os_lval(s, -1);
}
Variant cw_mycollection$os_constant(const char *s) {
  return c_mycollection::os_constant(s);
}
Variant cw_mycollection$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_mycollection::os_invoke(c, s, params, -1, fatal);
}
void c_mycollection::init() {
  m_items = ScalarArrays::sa_[0];
  m_count = 0LL;
}
/* SRC: roadsend/pcc/tests/obj-iteration.php line 57 */
p_myiterator c_mycollection::t_getiterator() {
  INSTANCE_METHOD_INJECTION(MyCollection, MyCollection::getIterator);
  return ((Object)(LINE(58,p_myiterator(p_myiterator(NEWOBJ(c_myiterator)())->create(m_items)))));
} /* function */
/* SRC: roadsend/pcc/tests/obj-iteration.php line 61 */
void c_mycollection::t_add(CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(MyCollection, MyCollection::add);
  m_items.set(m_count++, (v_value));
} /* function */
Object co_myiterator(CArrRef params, bool init /* = true */) {
  return Object(p_myiterator(NEW(c_myiterator)())->dynCreate(params, init));
}
Object co_mycollection(CArrRef params, bool init /* = true */) {
  return Object(p_mycollection(NEW(c_mycollection)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$tests$obj_iteration_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/tests/obj-iteration.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$tests$obj_iteration_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_values __attribute__((__unused__)) = (variables != gVariables) ? variables->get("values") : g->GV(values);
  Variant &v_it __attribute__((__unused__)) = (variables != gVariables) ? variables->get("it") : g->GV(it);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_coll __attribute__((__unused__)) = (variables != gVariables) ? variables->get("coll") : g->GV(coll);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);

  (v_values = ScalarArrays::sa_[1]);
  (v_it = ((Object)(LINE(45,p_myiterator(p_myiterator(NEWOBJ(c_myiterator)())->create(v_values))))));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_it.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_b = iter3->second();
      v_a = iter3->first();
      {
        print(LINE(48,concat4(toString(v_a), ": ", toString(v_b), "\n")));
      }
    }
  }
  (v_coll = ((Object)(LINE(66,p_mycollection(p_mycollection(NEWOBJ(c_mycollection)())->create())))));
  LINE(67,v_coll.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, "value 1"));
  LINE(68,v_coll.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, "value 2"));
  LINE(69,v_coll.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, "value 3"));
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_coll.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_val = iter6->second();
      v_key = iter6->first();
      {
        echo(LINE(72,concat5("key/value: [", toString(v_key), " -> ", toString(v_val), "]\n\n")));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
