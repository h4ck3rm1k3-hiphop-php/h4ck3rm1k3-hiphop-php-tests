
#include <php/roadsend/pcc/runtime/ext/gtk/tests/clist.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$gtk$tests$clist_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/gtk/tests/clist.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$gtk$tests$clist_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_titles __attribute__((__unused__)) = (variables != gVariables) ? variables->get("titles") : g->GV(titles);
  Variant &v_clist __attribute__((__unused__)) = (variables != gVariables) ? variables->get("clist") : g->GV(clist);
  Variant &v_text __attribute__((__unused__)) = (variables != gVariables) ? variables->get("text") : g->GV(text);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);

  echo("test the custom properties of a gtkclist\n");
  if (!(LINE(4,x_extension_loaded("gtk")))) {
    LINE(5,x_dl("php_gtk.so"));
  }
  (v_titles = ScalarArrays::sa_[0]);
  (v_clist = LINE(11,create_object("gtkclist", Array(ArrayInit(2).set(0, 12LL).set(1, v_titles).create()))));
  echo("the clist: ");
  LINE(14,x_var_dump(1, v_clist));
  (v_text = ScalarArrays::sa_[1]);
  (v_row = LINE(22,v_clist.o_invoke_few_args("prepend", 0x65C5EF328C77C917LL, 1, v_text)));
  (v_row = LINE(23,v_clist.o_invoke_few_args("prepend", 0x65C5EF328C77C917LL, 1, v_text)));
  LINE(25,x_var_dump(1, v_row));
  echo("\n\n-- in gtkwidget: \n");
  echo("\nstyle\n");
  LINE(30,x_var_dump(1, v_clist.o_get("style", 0x040EF9119982696ALL)));
  echo("\nwindow\n");
  LINE(33,x_var_dump(1, v_clist.o_get("window", 0x6B178C4E4A2617E2LL)));
  echo("\nallocation\n");
  LINE(36,x_var_dump(1, v_clist.o_get("allocation", 0x08A76C0C7C76DE59LL)));
  echo("\nstate\n");
  LINE(39,x_var_dump(1, v_clist.o_get("state", 0x5C84C80672BA5E22LL)));
  echo("\nparent\n");
  LINE(42,x_var_dump(1, v_clist.o_get("parent", 0x16E2F26FFB10FD8CLL)));
  echo("\n\n-- in gtkclist itself: \n");
  echo("\nfocus_row\n");
  LINE(49,x_var_dump(1, v_clist.o_get("focus_row", 0x06A25AAAF397E6B3LL)));
  echo("\nrows\n");
  LINE(52,x_var_dump(1, v_clist.o_get("rows", 0x3107565F03C446C8LL)));
  echo("\nsort_column\n");
  LINE(55,x_var_dump(1, v_clist.o_get("sort_column", 0x31E555E7D71EBDD2LL)));
  echo("\nsort_type\n");
  LINE(58,x_var_dump(1, v_clist.o_get("sort_type", 0x4EEE890B8327DB1CLL)));
  echo("\nselection\n");
  LINE(61,x_var_dump(1, v_clist.o_get("selection", 0x2781B225767237F1LL)));
  echo("\nselection_mode\n");
  LINE(64,x_var_dump(1, v_clist.o_get("selection_mode", 0x0FE22FFCE6AB82F1LL)));
  echo("\nrow_list\n");
  LINE(67,x_var_dump(1, v_clist.o_get("row_list", 0x5355B9243688E9CELL)));
  LINE(69,x_var_dump(1, v_clist.o_get("row_list", 0x5355B9243688E9CELL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  LINE(74,x_var_dump(1, v_clist.o_get("row_list", 0x5355B9243688E9CELL)));
  (v_foo = v_clist.o_get("row_list", 0x5355B9243688E9CELL));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_foo.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_b = iter3->second();
      v_a = iter3->first();
      {
        print(LINE(85,concat4(toString(v_a), " ", toString(v_b), "\n")));
      }
    }
  }
  print((LINE(93,(assignCallTemp(eo_1, toString(v_clist.o_invoke_few_args("get_text", 0x2B657E6110B24C09LL, 2, 1LL, 2LL))),concat3("test at 1,2: ", eo_1, "\n")))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
