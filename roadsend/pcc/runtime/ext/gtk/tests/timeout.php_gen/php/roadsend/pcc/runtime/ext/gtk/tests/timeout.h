
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_gtk_tests_timeout_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_gtk_tests_timeout_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/gtk/tests/timeout.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_callback(CVarRef v_data);
Variant pm_php$roadsend$pcc$runtime$ext$gtk$tests$timeout_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_gtk_tests_timeout_h__
