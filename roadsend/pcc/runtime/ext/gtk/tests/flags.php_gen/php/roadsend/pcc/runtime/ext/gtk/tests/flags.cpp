
#include <php/roadsend/pcc/runtime/ext/gtk/tests/flags.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const Variant k_GTK_CAN_DEFAULT = "GTK_CAN_DEFAULT";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$gtk$tests$flags_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/gtk/tests/flags.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$gtk$tests$flags_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_button __attribute__((__unused__)) = (variables != gVariables) ? variables->get("button") : g->GV(button);

  echo("make sure that flags are working about the same\n");
  if (!(LINE(4,x_extension_loaded("gtk")))) {
    LINE(5,x_dl("php_gtk.so"));
  }
  (v_button = LINE(8,create_object("gtkbutton", Array(ArrayInit(1).set(0, "foo").create()))));
  echo("button flags: \n");
  LINE(10,x_var_dump(1, v_button.o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)));
  echo("flag: \n");
  LINE(13,x_var_dump(1, k_GTK_CAN_DEFAULT));
  LINE(15,v_button.o_invoke_few_args("set_flags", 0x388CFA29241FE1A4LL, 1, k_GTK_CAN_DEFAULT));
  echo("new button flags: \n");
  LINE(17,x_var_dump(1, v_button.o_invoke_few_args("flags", 0x7401482B86AFCBFBLL, 0)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
