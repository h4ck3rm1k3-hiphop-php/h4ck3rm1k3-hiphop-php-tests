
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_gtk_tests_idle_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_gtk_tests_idle_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/gtk/tests/idle.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_callback(CVarRef v_data);
Variant pm_php$roadsend$pcc$runtime$ext$gtk$tests$idle_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_gtk_tests_idle_h__
