
#include <php/roadsend/pcc/runtime/ext/gtk/other-tests/hello-world.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_GTK_WINDOW_POPUP = "GTK_WINDOW_POPUP";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/hello-world.php line 6 */
bool f_delete_event(CVarRef v_widget) {
  FUNCTION_INJECTION(delete_event);
  return false;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/hello-world.php line 17 */
void f_hello(CVarRef v_widget) {
  FUNCTION_INJECTION(hello);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_window __attribute__((__unused__)) = g->GV(window);
  print("Hello World!\n");
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/hello-world.php line 11 */
void f_shutdown(CVarRef v_widget) {
  FUNCTION_INJECTION(shutdown);
  print("Shutting down...\n");
  LINE(14,throw_fatal("unknown class gtk", ((void*)NULL)));
} /* function */
Variant i_delete_event(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0549FAE24D08964BLL, delete_event) {
    return (f_delete_event(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_hello(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x35B3B21789F02516LL, hello) {
    return (f_hello(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_shutdown(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2FDA955CE1D84E71LL, shutdown) {
    return (f_shutdown(params.rvalAt(0)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$gtk$other_tests$hello_world_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/gtk/other-tests/hello-world.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$gtk$other_tests$hello_world_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_window __attribute__((__unused__)) = (variables != gVariables) ? variables->get("window") : g->GV(window);
  Variant &v_button __attribute__((__unused__)) = (variables != gVariables) ? variables->get("button") : g->GV(button);

  if (!(LINE(2,x_extension_loaded("gtk")))) {
    LINE(3,x_dl("php_gtk.so"));
  }
  (v_window = LINE(23,create_object("gtkwindow", Array(ArrayInit(1).set(0, k_GTK_WINDOW_POPUP).create()))));
  LINE(24,v_window.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "destroy", "shutdown"));
  LINE(25,v_window.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
  LINE(26,v_window.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
  (v_button = LINE(28,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Hello World!").create()))));
  LINE(29,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "hello"));
  LINE(30,v_window.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_button));
  LINE(32,v_window.o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
  LINE(34,x_ob_implicit_flush(true));
  LINE(36,throw_fatal("unknown class gtk", ((void*)NULL)));
  echo("\r\n\r\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
