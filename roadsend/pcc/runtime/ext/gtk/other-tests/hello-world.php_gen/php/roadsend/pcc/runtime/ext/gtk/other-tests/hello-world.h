
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_hello_world_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_hello_world_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/gtk/other-tests/hello-world.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool f_delete_event(CVarRef v_widget);
Variant pm_php$roadsend$pcc$runtime$ext$gtk$other_tests$hello_world_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_hello(CVarRef v_widget);
void f_shutdown(CVarRef v_widget);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_hello_world_h__
