
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_gtk_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_gtk_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/gtk/other-tests/gtk.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_ctree_toggle_justify(CVarRef v_menu_item, CVarRef v_ctree, Variant v_justification);
void f_clist_click_column(CVarRef v_clist, Variant v_column);
void f_create_ctree();
void f_create_radio_buttons();
void f_close_window(CVarRef v_widget);
void f_after_press(CVarRef v_ctree);
void f_dnd_drag_data_received(CVarRef v_widget, CVarRef v_context, CVarRef v_x, CVarRef v_y, CVarRef v_data, CVarRef v_info, CVarRef v_time);
void f_create_clist();
void f_create_dnd();
void f_count_items(CVarRef v_ctree, CVarRef v_node);
void f_create_cursor_test();
void f_add1000_clist(CVarRef v_button, CVarRef v_clist);
void f_event_watcher_down(Variant v_event_watcher_enter_id, Variant v_event_watcher_leave_id);
void f_label_toggle(CVarRef v_dialog, Variant v_label, CVarRef v_dialog0);
void f_ctree_toggle_sel_mode(CVarRef v_menu_item, CVarRef v_ctree, Variant v_sel_mode);
void f_create_check_buttons();
void f_create_dialog();
void f_unselect_all(CVarRef v_button, CVarRef v_ctree);
void f_create_pixmap();
bool f_tips_query_widget_selected(CVarRef v_tips_query, CVarRef v_widget, CVarRef v_tip_text, CVarRef v_tip_private, CVarRef v_event);
bool f_delete_event(CVarRef v_window, CVarRef v_event);
void f_entry_toggle_editable(CVarRef v_check_button, CVarRef v_entry);
void f_collapse_all(CVarRef v_button, CVarRef v_ctree);
bool f_event_watcher(CVarRef v_object, CVarRef v_signal_id);
void f_create_file_selection();
void f_create_entry();
void f_show_all_pages(CVarRef v_notebook);
void f_notebook_homogeneous(CVarRef v_button, CVarRef v_notebook);
void f_create_panes();
Object f_build_option_menu(CArrRef v_items, Variant v_history = null);
void f_tab_fill(CVarRef v_button, Variant v_child, CVarRef v_notebook);
void f_create_tooltips();
void f_file_selection_ok(CVarRef v_button, CVarRef v_fs);
void f_change_spacing(CVarRef v_adj, CVarRef v_ctree);
void f_clist_remove_selection(CVarRef v_button, CVarRef v_clist);
void f_create_pages(Variant v_notebook, int64 v_start, int64 v_end);
void f_expand_all(CVarRef v_button, CVarRef v_ctree);
void f_ctree_toggle_expander_style(CVarRef v_menu_item, CVarRef v_ctree, Variant v_expander_style);
void f_create_buttons();
void f_clist_toggle_sel_mode(CVarRef v_menu_item, CVarRef v_clist, Variant v_sel_mode);
void f_tips_query_widget_entered(CVarRef v_tips_query, CVarRef v_widget, CVarRef v_tip_text, CVarRef v_tip_private, CVarRef v_toggle);
void f_change_indent(CVarRef v_adj, CVarRef v_ctree);
void f_notebook_popup(CVarRef v_button, CVarRef v_notebook);
void f_toggle_shrink(Variant v_child);
void f_set_cursor(CVarRef v_spinner, CVarRef v_darea, CVarRef v_cur_name);
void f_notabs_notebook(CVarRef v_menuitem, CVarRef v_notebook);
void f_toggle_show(CVarRef v_b);
void f_toggle_resize(Variant v_child);
void f_tab_pack(CVarRef v_button, Variant v_child, CVarRef v_notebook);
void f_create_labels();
void f_standard_notebook(CVarRef v_menuitem, CVarRef v_notebook);
Object f_create_pane_options(CVarRef v_paned, CStrRef v_frame_label, CStrRef v_label1, CStrRef v_label2);
void f_expose_event(CVarRef v_darea, CVarRef v_event);
void f_rebuild_tree(CVarRef v_button, CVarRef v_ctree);
void f_tab_expand(CVarRef v_button, Variant v_child, CVarRef v_notebook);
void f_build_recursive(CVarRef v_ctree, int64 v_cur_depth, CVarRef v_depth, CVarRef v_num_books, CVarRef v_num_pages, Variant v_parent);
void f_button_press(CVarRef v_widget, CVarRef v_event, CVarRef v_spinner);
void f_entry_toggle_visibility(CVarRef v_check_button, CVarRef v_entry);
void f_create_event_watcher();
Object f_create_bbox(bool v_horizontal, CStrRef v_title, Variant v_spacing, Variant v_child_w, Variant v_child_h, Variant v_layout);
void f_toggle_reorderable(CVarRef v_button, CVarRef v_clist);
void f_create_color_selection();
void f_scrollable_notebook(CVarRef v_menuitem, Variant v_notebook);
void f_dnd_drag_data_get(CVarRef v_widget, CVarRef v_context, CVarRef v_selection_data, CVarRef v_info, CVarRef v_time);
void f_add10000_clist(CVarRef v_button, CVarRef v_clist);
void f_page_switch(CVarRef v_notebook, CVarRef v_page, Variant v_page_num);
void f_notebook_rotate(CVarRef v_notebook);
void f_create_toggle_buttons();
void f_set_background(CVarRef v_ctree, Variant v_node);
void f_ctree_toggle_line_style(CVarRef v_menu_item, CVarRef v_ctree, Variant v_line_style);
void f_event_watcher_toggle(Variant v_event_watcher_enter_id, Variant v_event_watcher_leave_id);
void f_ctree_click_column(CVarRef v_ctree, Variant v_column);
void f_entry_toggle_sensitive(CVarRef v_check_button, CVarRef v_entry);
void f_remove_selection(CVarRef v_button, CVarRef v_ctree);
void f_insert_row_clist(CVarRef v_button, CVarRef v_clist);
Variant pm_php$roadsend$pcc$runtime$ext$gtk$other_tests$gtk_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_change_row_height(CVarRef v_adj, CVarRef v_ctree);
void f_change_style(CVarRef v_button, CVarRef v_ctree);
void f_create_button_box();
void f_create_main_window();
void f_toggle_title_buttons(CVarRef v_button, CVarRef v_clist);
void f_create_notebook();
void f_select_all(CVarRef v_button, CVarRef v_ctree);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_gtk_h__
