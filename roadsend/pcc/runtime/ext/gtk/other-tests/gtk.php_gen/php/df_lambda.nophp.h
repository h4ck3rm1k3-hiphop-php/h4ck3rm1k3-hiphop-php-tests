
#ifndef __GENERATED_php_df_lambda_nophp_h__
#define __GENERATED_php_df_lambda_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/df_lambda.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$df_lambda(bool incOnce = false, LVariableTable* variables = NULL);
void f_df_lambda_1(Variant v_x);
void f_df_lambda_2(CVarRef v_b, CVarRef v_b2);
void f_df_lambda_3(Object v_w);
void f_df_lambda_4(Object v_w);
void f_df_lambda_5(Variant v_w);
void f_df_lambda_6(Variant v_w);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_df_lambda_nophp_h__
