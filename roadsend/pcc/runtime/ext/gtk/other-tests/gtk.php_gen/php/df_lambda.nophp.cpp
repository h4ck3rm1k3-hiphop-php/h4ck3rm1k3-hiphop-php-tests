
#include <php/df_lambda.nophp.h>
#include <php/roadsend/pcc/runtime/ext/gtk/other-tests/gtk.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: php$df_lambda line 2 */
void f_df_lambda_1(Variant v_x) {
  FUNCTION_INJECTION(df_lambda_1);
  (v_x = LINE(2,create_object("gtkbutton", Array(ArrayInit(1).set(0, concat("button", (toString(v_x + 1LL)))).create()))));
} /* function */
/* SRC: php$df_lambda line 3 */
void f_df_lambda_2(CVarRef v_b, CVarRef v_b2) {
  FUNCTION_INJECTION(df_lambda_2);
  LINE(3,f_toggle_show(v_b2));
} /* function */
/* SRC: php$df_lambda line 4 */
void f_df_lambda_3(Object v_w) {
  FUNCTION_INJECTION(df_lambda_3);
  LINE(4,v_w->o_invoke_few_args("hide_fileop_buttons", 0x71D33FCF10BC1557LL, 0));
} /* function */
/* SRC: php$df_lambda line 5 */
void f_df_lambda_4(Object v_w) {
  FUNCTION_INJECTION(df_lambda_4);
  LINE(5,v_w->o_invoke_few_args("show_fileop_buttons", 0x0D98023BFFAB5C15LL, 0));
} /* function */
/* SRC: php$df_lambda line 6 */
void f_df_lambda_5(Variant v_w) {
  FUNCTION_INJECTION(df_lambda_5);
  setNull(v_w);
} /* function */
/* SRC: php$df_lambda line 7 */
void f_df_lambda_6(Variant v_w) {
  FUNCTION_INJECTION(df_lambda_6);
  setNull(v_w);
} /* function */
Variant pm_php$df_lambda(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::df_lambda);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$df_lambda;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
