
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_gtk_fw_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_gtk_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_GTK_BUTTONBOX_START;
extern const StaticString k_GTK_WIN_POS_CENTER;
extern const StaticString k_GTK_SPIN_STEP_FORWARD;
extern const StaticString k_GTK_DEST_DEFAULT_ALL;
extern const StaticString k_GTK_JUSTIFY_FILL;
extern const StaticString k_GTK_BUTTONBOX_SPREAD;
extern const StaticString k_GTK_CTREE_EXPANDER_SQUARE;
extern const StaticString k_GTK_SHADOW_OUT;
extern const StaticString k_GDK_BUTTON_PRESS_MASK;
extern const StaticString k_GDK_BUTTON3_MASK;
extern const StaticString k_GTK_CTREE_LINES_TABBED;
extern const StaticString k_GTK_UPDATE_CONTINUOUS;
extern const StaticString k_GDK_EXPOSURE_MASK;
extern const Variant k_GTK_VISIBLE;
extern const StaticString k_GTK_STATE_NORMAL;
extern const Variant k_GTK_JUSTIFY_LEFT;
extern const StaticString k_GTK_PACK_START;
extern const Variant k_GTK_JUSTIFY_RIGHT;
extern const StaticString k_GTK_POS_TOP;
extern const StaticString k_GTK_BUTTONBOX_EDGE;
extern const StaticString k_GTK_CTREE_EXPANDER_TRIANGLE;
extern const Variant k_GTK_SELECTION_EXTENDED;
extern const StaticString k_GTK_SELECTION_MULTIPLE;
extern const Variant k_GTK_WIN_POS_MOUSE;
extern const StaticString k_GTK_POLICY_ALWAYS;
extern const StaticString k_GTK_CTREE_EXPANDER_CIRCULAR;
extern const StaticString k_GTK_STATE_SELECTED;
extern const Variant k_GTK_SORT_ASCENDING;
extern const StaticString k_GTK_CTREE_EXPANDER_NONE;
extern const StaticString k_GTK_CTREE_LINES_DOTTED;
extern const Variant k_GTK_SORT_DESCENDING;
extern const StaticString k_GTK_SELECTION_BROWSE;
extern const StaticString k_GTK_SPIN_STEP_BACKWARD;
extern const StaticString k_GTK_JUSTIFY_CENTER;
extern const Variant k_GDK_ACTION_COPY;
extern const StaticString k_GTK_SELECTION_SINGLE;
extern const StaticString k_GTK_CTREE_LINES_NONE;
extern const Variant k_GTK_CAN_DEFAULT;
extern const StaticString k_GDK_BUTTON1_MASK;
extern const Variant k_GTK_SHADOW_IN;
extern const StaticString k_GDK_BUTTON_PRESS;
extern const StaticString k_GTK_BUTTONBOX_END;
extern const StaticString k_GTK_CTREE_LINES_SOLID;
extern const Variant k_GTK_POLICY_AUTOMATIC;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_gtk_other_tests_gtk_fw_h__
