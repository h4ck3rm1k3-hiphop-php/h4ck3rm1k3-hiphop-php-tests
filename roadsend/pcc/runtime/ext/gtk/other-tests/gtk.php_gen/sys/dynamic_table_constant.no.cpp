
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_GTK_BUTTONBOX_START;
extern const StaticString k_GTK_WIN_POS_CENTER;
extern const StaticString k_GTK_SPIN_STEP_FORWARD;
extern const StaticString k_GTK_DEST_DEFAULT_ALL;
extern const StaticString k_GTK_JUSTIFY_FILL;
extern const StaticString k_GTK_BUTTONBOX_SPREAD;
extern const StaticString k_GTK_CTREE_EXPANDER_SQUARE;
extern const StaticString k_GTK_SHADOW_OUT;
extern const StaticString k_GDK_BUTTON_PRESS_MASK;
extern const StaticString k_GDK_BUTTON3_MASK;
extern const StaticString k_GTK_CTREE_LINES_TABBED;
extern const StaticString k_GTK_UPDATE_CONTINUOUS;
extern const StaticString k_GDK_EXPOSURE_MASK;
extern const Variant k_GTK_VISIBLE;
extern const StaticString k_GTK_STATE_NORMAL;
extern const Variant k_GTK_JUSTIFY_LEFT;
extern const StaticString k_GTK_PACK_START;
extern const Variant k_GTK_JUSTIFY_RIGHT;
extern const StaticString k_GTK_POS_TOP;
extern const StaticString k_GTK_BUTTONBOX_EDGE;
extern const StaticString k_GTK_CTREE_EXPANDER_TRIANGLE;
extern const Variant k_GTK_SELECTION_EXTENDED;
extern const StaticString k_GTK_SELECTION_MULTIPLE;
extern const Variant k_GTK_WIN_POS_MOUSE;
extern const StaticString k_GTK_POLICY_ALWAYS;
extern const StaticString k_GTK_CTREE_EXPANDER_CIRCULAR;
extern const StaticString k_GTK_STATE_SELECTED;
extern const Variant k_GTK_SORT_ASCENDING;
extern const StaticString k_GTK_CTREE_EXPANDER_NONE;
extern const StaticString k_GTK_CTREE_LINES_DOTTED;
extern const Variant k_GTK_SORT_DESCENDING;
extern const StaticString k_GTK_SELECTION_BROWSE;
extern const StaticString k_GTK_SPIN_STEP_BACKWARD;
extern const StaticString k_GTK_JUSTIFY_CENTER;
extern const Variant k_GDK_ACTION_COPY;
extern const StaticString k_GTK_SELECTION_SINGLE;
extern const StaticString k_GTK_CTREE_LINES_NONE;
extern const Variant k_GTK_CAN_DEFAULT;
extern const StaticString k_GDK_BUTTON1_MASK;
extern const Variant k_GTK_SHADOW_IN;
extern const StaticString k_GDK_BUTTON_PRESS;
extern const StaticString k_GTK_BUTTONBOX_END;
extern const StaticString k_GTK_CTREE_LINES_SOLID;
extern const Variant k_GTK_POLICY_AUTOMATIC;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 1:
      HASH_RETURN(0x521EC72F69C47081LL, k_GTK_WIN_POS_CENTER, GTK_WIN_POS_CENTER);
      break;
    case 7:
      HASH_RETURN(0x5C111BD1588AF587LL, k_GTK_BUTTONBOX_EDGE, GTK_BUTTONBOX_EDGE);
      break;
    case 12:
      HASH_RETURN(0x2C181654F9C5F68CLL, k_GTK_SHADOW_IN, GTK_SHADOW_IN);
      break;
    case 13:
      HASH_RETURN(0x29C4F207D110BD8DLL, k_GTK_SELECTION_BROWSE, GTK_SELECTION_BROWSE);
      break;
    case 16:
      HASH_RETURN(0x6986AE1B0027A810LL, k_GTK_JUSTIFY_FILL, GTK_JUSTIFY_FILL);
      break;
    case 20:
      HASH_RETURN(0x13305AC8BD2EEC94LL, k_GTK_JUSTIFY_LEFT, GTK_JUSTIFY_LEFT);
      break;
    case 22:
      HASH_RETURN(0x3A9262C8A684A696LL, k_GDK_BUTTON_PRESS_MASK, GDK_BUTTON_PRESS_MASK);
      break;
    case 28:
      HASH_RETURN(0x2D04DA8A9B119F9CLL, k_GDK_BUTTON_PRESS, GDK_BUTTON_PRESS);
      break;
    case 30:
      HASH_RETURN(0x083DB0D90F178D1ELL, k_GTK_SORT_DESCENDING, GTK_SORT_DESCENDING);
      break;
    case 31:
      HASH_RETURN(0x7901E59DB88A419FLL, k_GTK_POLICY_ALWAYS, GTK_POLICY_ALWAYS);
      break;
    case 35:
      HASH_RETURN(0x083FF5581301FCA3LL, k_GTK_CTREE_EXPANDER_TRIANGLE, GTK_CTREE_EXPANDER_TRIANGLE);
      break;
    case 42:
      HASH_RETURN(0x0355059F598F2F2ALL, k_GTK_DEST_DEFAULT_ALL, GTK_DEST_DEFAULT_ALL);
      break;
    case 45:
      HASH_RETURN(0x0D5F1C3EC26434ADLL, k_GTK_SPIN_STEP_BACKWARD, GTK_SPIN_STEP_BACKWARD);
      break;
    case 47:
      HASH_RETURN(0x59C2F7BFBA51B42FLL, k_GTK_STATE_SELECTED, GTK_STATE_SELECTED);
      HASH_RETURN(0x2C1F4722592C7B2FLL, k_GTK_JUSTIFY_RIGHT, GTK_JUSTIFY_RIGHT);
      break;
    case 49:
      HASH_RETURN(0x74294244ABAD0F31LL, k_GTK_CTREE_EXPANDER_SQUARE, GTK_CTREE_EXPANDER_SQUARE);
      break;
    case 51:
      HASH_RETURN(0x3530EE76DD735AB3LL, k_GTK_BUTTONBOX_START, GTK_BUTTONBOX_START);
      break;
    case 56:
      HASH_RETURN(0x628D5D88061118B8LL, k_GTK_STATE_NORMAL, GTK_STATE_NORMAL);
      HASH_RETURN(0x2B05E36ECD7C8A38LL, k_GTK_CTREE_LINES_TABBED, GTK_CTREE_LINES_TABBED);
      break;
    case 58:
      HASH_RETURN(0x60A893CD3C66113ALL, k_GTK_JUSTIFY_CENTER, GTK_JUSTIFY_CENTER);
      break;
    case 59:
      HASH_RETURN(0x46563F89A8D713BBLL, k_GTK_BUTTONBOX_SPREAD, GTK_BUTTONBOX_SPREAD);
      break;
    case 64:
      HASH_RETURN(0x1B73CB6E7DC48440LL, k_GTK_CTREE_LINES_SOLID, GTK_CTREE_LINES_SOLID);
      break;
    case 71:
      HASH_RETURN(0x7E6A70DCCBDAE8C7LL, k_GTK_POS_TOP, GTK_POS_TOP);
      break;
    case 76:
      HASH_RETURN(0x4B70782A88E6704CLL, k_GTK_CTREE_EXPANDER_CIRCULAR, GTK_CTREE_EXPANDER_CIRCULAR);
      break;
    case 77:
      HASH_RETURN(0x7D942CBA5E6C44CDLL, k_GDK_EXPOSURE_MASK, GDK_EXPOSURE_MASK);
      break;
    case 79:
      HASH_RETURN(0x41E858CEB08995CFLL, k_GTK_CTREE_EXPANDER_NONE, GTK_CTREE_EXPANDER_NONE);
      break;
    case 87:
      HASH_RETURN(0x0EF447E2D6E830D7LL, k_GTK_SELECTION_SINGLE, GTK_SELECTION_SINGLE);
      HASH_RETURN(0x7DE4E447EE7F32D7LL, k_GTK_SELECTION_MULTIPLE, GTK_SELECTION_MULTIPLE);
      break;
    case 89:
      HASH_RETURN(0x3A0FA7809969A859LL, k_GTK_PACK_START, GTK_PACK_START);
      break;
    case 90:
      HASH_RETURN(0x1D0FB70975E0EA5ALL, k_GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
      break;
    case 91:
      HASH_RETURN(0x4F3D5F3991B3405BLL, k_GDK_BUTTON1_MASK, GDK_BUTTON1_MASK);
      break;
    case 96:
      HASH_RETURN(0x1131D73BC1DA4C60LL, k_GTK_SORT_ASCENDING, GTK_SORT_ASCENDING);
      break;
    case 98:
      HASH_RETURN(0x27196DED51840A62LL, k_GDK_ACTION_COPY, GDK_ACTION_COPY);
      break;
    case 101:
      HASH_RETURN(0x45562AC3AF49DDE5LL, k_GTK_CTREE_LINES_NONE, GTK_CTREE_LINES_NONE);
      break;
    case 102:
      HASH_RETURN(0x71BE1E30F4AC6966LL, k_GTK_CTREE_LINES_DOTTED, GTK_CTREE_LINES_DOTTED);
      break;
    case 105:
      HASH_RETURN(0x4C11B693CC35CAE9LL, k_GTK_BUTTONBOX_END, GTK_BUTTONBOX_END);
      break;
    case 108:
      HASH_RETURN(0x0CADC24C26A5FA6CLL, k_GTK_VISIBLE, GTK_VISIBLE);
      break;
    case 112:
      HASH_RETURN(0x0B4197686B5518F0LL, k_GTK_SHADOW_OUT, GTK_SHADOW_OUT);
      break;
    case 114:
      HASH_RETURN(0x4D3F5214F59E3172LL, k_GTK_UPDATE_CONTINUOUS, GTK_UPDATE_CONTINUOUS);
      break;
    case 117:
      HASH_RETURN(0x70E72505451ADA75LL, k_GTK_WIN_POS_MOUSE, GTK_WIN_POS_MOUSE);
      break;
    case 120:
      HASH_RETURN(0x66AD5468B56E8C78LL, k_GTK_SELECTION_EXTENDED, GTK_SELECTION_EXTENDED);
      break;
    case 121:
      HASH_RETURN(0x016461407B91F1F9LL, k_GTK_CAN_DEFAULT, GTK_CAN_DEFAULT);
      break;
    case 122:
      HASH_RETURN(0x5BCEDE36BCE85DFALL, k_GTK_SPIN_STEP_FORWARD, GTK_SPIN_STEP_FORWARD);
      break;
    case 126:
      HASH_RETURN(0x1A5E123E84F8177ELL, k_GDK_BUTTON3_MASK, GDK_BUTTON3_MASK);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
