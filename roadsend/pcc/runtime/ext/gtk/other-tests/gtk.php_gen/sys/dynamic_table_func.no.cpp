
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_notebook_homogeneous(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_notebook_rotate(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_clist_remove_selection(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_notabs_notebook(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_button_press(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_dnd_drag_data_received(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_show_all_pages(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_entry(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_insert_row_clist(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_buttons(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_labels(CArrRef params);
Variant i_scrollable_notebook(CArrRef params);
Variant i_page_switch(CArrRef params);
static Variant invoke_case_45(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x502DAE4F4EFFC42DLL, create_labels);
  HASH_INVOKE(0x0EFD958D9A50CD2DLL, scrollable_notebook);
  HASH_INVOKE(0x5A5F63DCE2DCD02DLL, page_switch);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_notebook(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_event_watcher_toggle(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_ctree_toggle_line_style(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_tab_pack(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_event_watcher(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_ctree_click_column(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_clist(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_toggle_reorderable(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_delete_event(CArrRef params);
Variant i_dnd_drag_data_get(CArrRef params);
static Variant invoke_case_75(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x0549FAE24D08964BLL, delete_event);
  HASH_INVOKE(0x7BCF24C67D65D94BLL, dnd_drag_data_get);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_create_color_selection(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_event_watcher_down(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_entry_toggle_sensitive(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_rebuild_tree(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_change_spacing(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_ctree(CArrRef params);
Variant i_expose_event(CArrRef params);
Variant i_create_button_box(CArrRef params);
static Variant invoke_case_100(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x5DEDF04EA69A8164LL, create_ctree);
  HASH_INVOKE(0x1E0760FD9AB0E064LL, expose_event);
  HASH_INVOKE(0x321700227FCF1D64LL, create_button_box);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_tab_expand(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_radio_buttons(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_tooltips(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_ctree_toggle_justify(CArrRef params);
Variant i_entry_toggle_editable(CArrRef params);
static Variant invoke_case_112(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x46D3B73C113E8170LL, ctree_toggle_justify);
  HASH_INVOKE(0x6D016D614B2AD570LL, entry_toggle_editable);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_change_style(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_check_buttons(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_change_row_height(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_dialog(CArrRef params);
Variant i_file_selection_ok(CArrRef params);
static Variant invoke_case_147(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x7BB93E2DDE9F2093LL, create_dialog);
  HASH_INVOKE(0x39A300E0E2AAD093LL, file_selection_ok);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_label_toggle(CArrRef params);
Variant i_create_pixmap(CArrRef params);
static Variant invoke_case_151(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x5EFE684B7B96E397LL, label_toggle);
  HASH_INVOKE(0x1978D60A7EE5C897LL, create_pixmap);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_add10000_clist(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_file_selection(CArrRef params);
Variant i_toggle_title_buttons(CArrRef params);
static Variant invoke_case_156(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x23E706DBB907E19CLL, create_file_selection);
  HASH_INVOKE(0x3559E1A0EEA2939CLL, toggle_title_buttons);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_entry_toggle_visibility(CArrRef params);
Variant i_create_toggle_buttons(CArrRef params);
static Variant invoke_case_157(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x2B4FDA434603879DLL, entry_toggle_visibility);
  HASH_INVOKE(0x4445949460B2BF9DLL, create_toggle_buttons);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_set_background(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_toggle_shrink(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_notebook_popup(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_event_watcher(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_collapse_all(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_close_window(CArrRef params);
Variant i_tips_query_widget_selected(CArrRef params);
Variant i_tab_fill(CArrRef params);
static Variant invoke_case_182(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x2BE49BB7DC8FE7B6LL, close_window);
  HASH_INVOKE(0x6E5E90661F6F5FB6LL, tips_query_widget_selected);
  HASH_INVOKE(0x1E7F385392EBFEB6LL, tab_fill);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_tips_query_widget_entered(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_standard_notebook(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_remove_selection(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_dnd(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_after_press(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_unselect_all(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_panes(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_change_indent(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_add1000_clist(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_set_cursor(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_toggle_resize(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_select_all(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_create_cursor_test(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_clist_click_column(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_ctree_toggle_sel_mode(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_clist_toggle_sel_mode(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_expand_all(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_count_items(CArrRef params);
Variant i_ctree_toggle_expander_style(CArrRef params);
static Variant invoke_case_241(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x6E12503C58A105F1LL, count_items);
  HASH_INVOKE(0x2BB94591958811F1LL, ctree_toggle_expander_style);
  return invoke_builtin(s, params, hash, fatal);
}

// Function Invoke Table
static Variant (*funcTable[256])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 256; i++) funcTable[i] = &invoke_builtin;
    funcTable[0] = &i_notebook_homogeneous;
    funcTable[4] = &i_notebook_rotate;
    funcTable[8] = &i_clist_remove_selection;
    funcTable[9] = &i_notabs_notebook;
    funcTable[13] = &i_button_press;
    funcTable[14] = &i_dnd_drag_data_received;
    funcTable[21] = &i_show_all_pages;
    funcTable[23] = &i_create_entry;
    funcTable[31] = &i_insert_row_clist;
    funcTable[39] = &i_create_buttons;
    funcTable[45] = &invoke_case_45;
    funcTable[50] = &i_create_notebook;
    funcTable[51] = &i_event_watcher_toggle;
    funcTable[55] = &i_ctree_toggle_line_style;
    funcTable[60] = &i_tab_pack;
    funcTable[67] = &i_event_watcher;
    funcTable[69] = &i_ctree_click_column;
    funcTable[70] = &i_create_clist;
    funcTable[71] = &i_toggle_reorderable;
    funcTable[75] = &invoke_case_75;
    funcTable[76] = &i_create_color_selection;
    funcTable[79] = &i_event_watcher_down;
    funcTable[84] = &i_entry_toggle_sensitive;
    funcTable[87] = &i_rebuild_tree;
    funcTable[99] = &i_change_spacing;
    funcTable[100] = &invoke_case_100;
    funcTable[104] = &i_tab_expand;
    funcTable[105] = &i_create_radio_buttons;
    funcTable[107] = &i_create_tooltips;
    funcTable[112] = &invoke_case_112;
    funcTable[114] = &i_change_style;
    funcTable[125] = &i_create_check_buttons;
    funcTable[134] = &i_change_row_height;
    funcTable[147] = &invoke_case_147;
    funcTable[151] = &invoke_case_151;
    funcTable[153] = &i_add10000_clist;
    funcTable[156] = &invoke_case_156;
    funcTable[157] = &invoke_case_157;
    funcTable[158] = &i_set_background;
    funcTable[161] = &i_toggle_shrink;
    funcTable[164] = &i_notebook_popup;
    funcTable[172] = &i_create_event_watcher;
    funcTable[180] = &i_collapse_all;
    funcTable[182] = &invoke_case_182;
    funcTable[183] = &i_tips_query_widget_entered;
    funcTable[185] = &i_standard_notebook;
    funcTable[186] = &i_remove_selection;
    funcTable[192] = &i_create_dnd;
    funcTable[194] = &i_after_press;
    funcTable[202] = &i_unselect_all;
    funcTable[204] = &i_create_panes;
    funcTable[205] = &i_change_indent;
    funcTable[214] = &i_add1000_clist;
    funcTable[215] = &i_set_cursor;
    funcTable[219] = &i_toggle_resize;
    funcTable[220] = &i_select_all;
    funcTable[225] = &i_create_cursor_test;
    funcTable[230] = &i_clist_click_column;
    funcTable[231] = &i_ctree_toggle_sel_mode;
    funcTable[237] = &i_clist_toggle_sel_mode;
    funcTable[238] = &i_expand_all;
    funcTable[241] = &invoke_case_241;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 255](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
