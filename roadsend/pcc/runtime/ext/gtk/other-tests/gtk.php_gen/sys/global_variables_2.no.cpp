
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 5:
      HASH_INITIALIZED(0x335FD6562A3D1F05LL, g->GV(book_open),
                       book_open);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 13:
      HASH_INITIALIZED(0x5B6A9A4AAA85D88DLL, g->GV(book_open_xpm),
                       book_open_xpm);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 36:
      HASH_INITIALIZED(0x37B89A99FCFE7264LL, g->GV(book_closed_xpm),
                       book_closed_xpm);
      break;
    case 39:
      HASH_INITIALIZED(0x0D9816AEF052A2E7LL, g->GV(windows),
                       windows);
      break;
    case 45:
      HASH_INITIALIZED(0x76922572589C17ADLL, g->GV(book_closed_mask),
                       book_closed_mask);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 48:
      HASH_INITIALIZED(0x0F62E6BFB82F8670LL, g->GV(book_closed),
                       book_closed);
      break;
    case 51:
      HASH_INITIALIZED(0x49565354E7F3F273LL, g->GV(book_open_mask),
                       book_open_mask);
      break;
    case 53:
      HASH_INITIALIZED(0x5AA2ECBEAD1E8975LL, g->GV(ctree_data),
                       ctree_data);
      HASH_INITIALIZED(0x74C5961A79B903F5LL, g->GV(sample_notebook),
                       sample_notebook);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
