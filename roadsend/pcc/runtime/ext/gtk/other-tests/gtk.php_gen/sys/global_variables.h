
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(windows)
    GVS(book_closed_xpm)
    GVS(book_open_xpm)
    GVS(ctree_data)
    GVS(book_open)
    GVS(book_open_mask)
    GVS(book_closed)
    GVS(book_closed_mask)
    GVS(sample_notebook)
  END_GVS(9)

  // Dynamic Constants

  // Function/Method Static Variables
  Variant sv_insert_row_clist_DupIdstyle2;
  Variant sv_insert_row_clist_DupIdstyle3;
  Variant sv_change_style_DupIdstyle1;
  Variant sv_change_style_DupIdstyle2;
  Variant sv_create_event_watcher_DupIdevent_watcher_leave_id;
  Variant sv_create_event_watcher_DupIdevent_watcher_enter_id;
  Variant sv_create_dialog_DupIdlabel;
  Variant sv_create_event_watcher_DupIddialog;
  Variant sv_insert_row_clist_DupIdstyle1;

  // Function/Method Static Variable Initialization Booleans
  bool inited_sv_insert_row_clist_DupIdstyle2;
  bool inited_sv_insert_row_clist_DupIdstyle3;
  bool inited_sv_change_style_DupIdstyle1;
  bool inited_sv_change_style_DupIdstyle2;
  bool inited_sv_create_event_watcher_DupIdevent_watcher_leave_id;
  bool inited_sv_create_event_watcher_DupIdevent_watcher_enter_id;
  bool inited_sv_create_dialog_DupIdlabel;
  bool inited_sv_create_event_watcher_DupIddialog;
  bool inited_sv_insert_row_clist_DupIdstyle1;

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$roadsend$pcc$runtime$ext$gtk$other_tests$gtk_php;
  bool run_pm_php$df_lambda;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 21;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[11];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
