
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 5:
      HASH_INDEX(0x335FD6562A3D1F05LL, book_open, 16);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 13:
      HASH_INDEX(0x5B6A9A4AAA85D88DLL, book_open_xpm, 14);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 36:
      HASH_INDEX(0x37B89A99FCFE7264LL, book_closed_xpm, 13);
      break;
    case 39:
      HASH_INDEX(0x0D9816AEF052A2E7LL, windows, 12);
      break;
    case 45:
      HASH_INDEX(0x76922572589C17ADLL, book_closed_mask, 19);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 48:
      HASH_INDEX(0x0F62E6BFB82F8670LL, book_closed, 18);
      break;
    case 51:
      HASH_INDEX(0x49565354E7F3F273LL, book_open_mask, 17);
      break;
    case 53:
      HASH_INDEX(0x5AA2ECBEAD1E8975LL, ctree_data, 15);
      HASH_INDEX(0x74C5961A79B903F5LL, sample_notebook, 20);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 21) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
