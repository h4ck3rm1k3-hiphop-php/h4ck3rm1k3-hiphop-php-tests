
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_GTK_WIN_POS_CENTER;
extern const StaticString k_GDK_LEAVE_NOTIFY_MASK;
extern const StaticString k_GDK_BUTTON_PRESS_MASK;
extern const StaticString k_GDK_EXPOSURE_MASK;
extern const StaticString k_GTK_STATE_NORMAL;
extern const StaticString k_GDK_POINTER_MOTION_MASK;
extern const StaticString k_GDK_BUTTON1_MASK;
extern const StaticString k_GDK_POINTER_MOTION_HINT_MASK;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 15) {
    case 1:
      HASH_RETURN(0x521EC72F69C47081LL, k_GTK_WIN_POS_CENTER, GTK_WIN_POS_CENTER);
      break;
    case 4:
      HASH_RETURN(0x2EA2E230E31AC7F4LL, k_GDK_POINTER_MOTION_HINT_MASK, GDK_POINTER_MOTION_HINT_MASK);
      break;
    case 6:
      HASH_RETURN(0x3A9262C8A684A696LL, k_GDK_BUTTON_PRESS_MASK, GDK_BUTTON_PRESS_MASK);
      break;
    case 8:
      HASH_RETURN(0x628D5D88061118B8LL, k_GTK_STATE_NORMAL, GTK_STATE_NORMAL);
      break;
    case 11:
      HASH_RETURN(0x4F3D5F3991B3405BLL, k_GDK_BUTTON1_MASK, GDK_BUTTON1_MASK);
      HASH_RETURN(0x68B2BE0B4CEAAEBBLL, k_GDK_POINTER_MOTION_MASK, GDK_POINTER_MOTION_MASK);
      break;
    case 13:
      HASH_RETURN(0x7D942CBA5E6C44CDLL, k_GDK_EXPOSURE_MASK, GDK_EXPOSURE_MASK);
      HASH_RETURN(0x79FE7A75168979ADLL, k_GDK_LEAVE_NOTIFY_MASK, GDK_LEAVE_NOTIFY_MASK);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
