
#include <php/roadsend/pcc/runtime/ext/gtk/other-tests/scribble.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_GTK_WIN_POS_CENTER = "GTK_WIN_POS_CENTER";
const StaticString k_GDK_LEAVE_NOTIFY_MASK = "GDK_LEAVE_NOTIFY_MASK";
const StaticString k_GDK_BUTTON_PRESS_MASK = "GDK_BUTTON_PRESS_MASK";
const StaticString k_GDK_EXPOSURE_MASK = "GDK_EXPOSURE_MASK";
const StaticString k_GTK_STATE_NORMAL = "GTK_STATE_NORMAL";
const StaticString k_GDK_POINTER_MOTION_MASK = "GDK_POINTER_MOTION_MASK";
const StaticString k_GDK_BUTTON1_MASK = "GDK_BUTTON1_MASK";
const StaticString k_GDK_POINTER_MOTION_HINT_MASK = "GDK_POINTER_MOTION_HINT_MASK";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/scribble.php line 65 */
bool f_motion_notify_event(CVarRef v_widget, CVarRef v_event) {
  FUNCTION_INJECTION(motion_notify_event);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_pixmap __attribute__((__unused__)) = g->GV(pixmap);
  Variant v_window;
  Variant v_pointer;
  int64 v_x = 0;
  int64 v_y = 0;
  Variant v_state;

  if (toBoolean(toObject(v_event).o_get("is_hint", 0x25E1AA100D6A674CLL))) {
    (v_window = toObject(v_event).o_get("window", 0x6B178C4E4A2617E2LL));
    (v_pointer = LINE(71,v_window.o_invoke_few_args("get_pointer", 0x6581902F9171507BLL, 0)));
    (v_x = toInt64(v_pointer.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
    (v_y = toInt64(v_pointer.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
    (v_state = v_pointer.rvalAt(2LL, 0x486AFCC090D5F98CLL));
  }
  else {
    (v_x = toInt64(toObject(v_event).o_get("x", 0x04BFC205E59FA416LL)));
    (v_y = toInt64(toObject(v_event).o_get("y", 0x4F56B733A4DFC78ALL)));
    (v_state = toObject(v_event).o_get("state", 0x5C84C80672BA5E22LL));
  }
  if ((toBoolean(bitwise_and(v_state, k_GDK_BUTTON1_MASK))) && toBoolean(gv_pixmap)) LINE(82,f_draw_brush(v_widget, v_x, v_y));
  return true;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/scribble.php line 39 */
bool f_expose_event(CVarRef v_widget, CVarRef v_event) {
  FUNCTION_INJECTION(expose_event);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_pixmap __attribute__((__unused__)) = g->GV(pixmap);
  LINE(48,throw_fatal("unknown class gdk", ((void*)NULL)));
  return false;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/scribble.php line 88 */
void f_draw_brush(CVarRef v_widget, int64 v_x, int64 v_y) {
  FUNCTION_INJECTION(draw_brush);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_pixmap __attribute__((__unused__)) = g->GV(pixmap);
  LINE(93,throw_fatal("unknown class gdk", ((void*)NULL)));
  LINE(94,toObject(v_widget)->o_invoke_few_args("draw", 0x0576A414FD788F39LL, 1, create_object("gdkrectangle", Array(ArrayInit(4).set(0, v_x - 4LL).set(1, v_y - 4LL).set(2, 8LL).set(3, 8LL).create()))));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/scribble.php line 54 */
bool f_button_press_event(CVarRef v_widget, CVarRef v_event) {
  FUNCTION_INJECTION(button_press_event);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_pixmap __attribute__((__unused__)) = g->GV(pixmap);
  if (equal(toObject(v_event).o_get("button", 0x614CB098CEBB2764LL), 1LL) && toBoolean(gv_pixmap)) LINE(59,f_draw_brush(v_widget, toInt64(toObject(v_event).o_get("x", 0x04BFC205E59FA416LL)), toInt64(toObject(v_event).o_get("y", 0x4F56B733A4DFC78ALL))));
  return true;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk/other-tests/scribble.php line 10 */
bool f_configure_event(CVarRef v_widget, CVarRef v_event) {
  FUNCTION_INJECTION(configure_event);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_pixmap __attribute__((__unused__)) = g->GV(pixmap);
  Object v_clTransparentTest;
  Object v_style;

  (gv_pixmap = LINE(17,create_object("gdkpixmap", Array(ArrayInit(4).set(0, toObject(v_widget).o_get("window", 0x6B178C4E4A2617E2LL)).set(1, toObject(v_widget).o_get("allocation", 0x08A76C0C7C76DE59LL).o_get("width", 0x79CE5ED9259796ADLL)).set(2, toObject(v_widget).o_get("allocation", 0x08A76C0C7C76DE59LL).o_get("height", 0x674F0FBB080B0779LL)).set(3, -1LL).create()))));
  (v_clTransparentTest = LINE(19,create_object("gdkcolor", Array(ArrayInit(3).set(0, 0LL).set(1, 0LL).set(2, 0LL).create()))));
  LINE(20,throw_fatal("unknown class gdk", ((void*)NULL)));
  (v_style = LINE(21,create_object("gtkstyle", Array())));
  lval(v_style.o_lval("base", 0x56FD8ECCBDFDD7A7LL)).set(k_GTK_STATE_NORMAL, (v_clTransparentTest));
  LINE(33,throw_fatal("unknown class gdk", ((void*)NULL)));
  return true;
} /* function */
Variant i_motion_notify_event(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7BF3FFC487D5DE15LL, motion_notify_event) {
    return (f_motion_notify_event(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_expose_event(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x1E0760FD9AB0E064LL, expose_event) {
    return (f_expose_event(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_button_press_event(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2AE0AE4F4444516ELL, button_press_event) {
    return (f_button_press_event(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_configure_event(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x42AFEE2A2D0EF462LL, configure_event) {
    return (f_configure_event(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$gtk$other_tests$scribble_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/gtk/other-tests/scribble.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$gtk$other_tests$scribble_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_pixmap __attribute__((__unused__)) = (variables != gVariables) ? variables->get("pixmap") : g->GV(pixmap);
  Variant &v_window __attribute__((__unused__)) = (variables != gVariables) ? variables->get("window") : g->GV(window);
  Variant &v_vbox __attribute__((__unused__)) = (variables != gVariables) ? variables->get("vbox") : g->GV(vbox);
  Variant &v_drawing_area __attribute__((__unused__)) = (variables != gVariables) ? variables->get("drawing_area") : g->GV(drawing_area);
  Variant &v_button __attribute__((__unused__)) = (variables != gVariables) ? variables->get("button") : g->GV(button);

  if (!(LINE(4,x_extension_loaded("gtk")))) {
    LINE(5,x_dl("php_gtk.so"));
  }
  setNull(v_pixmap);
  (v_window = LINE(98,create_object("gtkwindow", Array())));
  LINE(99,v_window.o_invoke_few_args("set_name", 0x20D03A6494326391LL, 1, "Test Input"));
  LINE(100,v_window.o_invoke_few_args("set_position", 0x17D027C2B74A20CBLL, 1, k_GTK_WIN_POS_CENTER));
  LINE(102,v_window.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "destroy", Array(ArrayInit(2).set(0, "gtk").set(1, "main_quit").create())));
  (v_vbox = LINE(104,create_object("gtkvbox", Array())));
  LINE(105,v_window.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_vbox));
  LINE(106,v_vbox.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  (v_drawing_area = LINE(108,create_object("gtkdrawingarea", Array())));
  LINE(109,v_drawing_area.o_invoke_few_args("size", 0x3A88D49C06AF8890LL, 2, 300LL, 300LL));
  LINE(110,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_drawing_area));
  LINE(111,v_drawing_area.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(113,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "expose_event", "expose_event"));
  LINE(114,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "configure_event", "configure_event"));
  LINE(116,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "motion_notify_event", "motion_notify_event"));
  LINE(117,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "button_press_event", "button_press_event"));
  (assignCallTemp(eo_0, toObject(v_drawing_area)),LINE(123,eo_0.o_invoke_few_args("set_events", 0x7FC3726CF670E586LL, 1, bitwise_or(bitwise_or(bitwise_or(bitwise_or(k_GDK_EXPOSURE_MASK, k_GDK_LEAVE_NOTIFY_MASK), k_GDK_BUTTON_PRESS_MASK), k_GDK_POINTER_MOTION_MASK), k_GDK_POINTER_MOTION_HINT_MASK))));
  (v_button = LINE(125,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Quit").create()))));
  LINE(126,v_vbox.o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 3, v_button, false, false));
  LINE(127,v_button.o_invoke_few_args("connect_object", 0x3E0DE7B55A894443LL, 2, "clicked", Array(ArrayInit(2).set(0, v_window).set(1, "destroy").create())));
  LINE(128,v_button.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(130,v_window.o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  LINE(132,throw_fatal("unknown class gtk", ((void*)NULL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
