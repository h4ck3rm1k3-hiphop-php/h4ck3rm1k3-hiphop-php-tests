
#include <php/roadsend/pcc/runtime/ext/standard/tests/soundex.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$soundex_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/soundex.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$soundex_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(toString(LINE(3,x_soundex("Euler"))));
  echo(toString(LINE(4,x_soundex("Ellery"))));
  echo("\n");
  echo(toString(LINE(6,x_soundex("Gauss"))));
  echo(toString(LINE(7,x_soundex("Ghosh"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
