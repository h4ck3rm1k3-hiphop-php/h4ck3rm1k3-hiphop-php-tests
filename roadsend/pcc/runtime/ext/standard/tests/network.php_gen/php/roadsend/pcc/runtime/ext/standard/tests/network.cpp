
#include <php/roadsend/pcc/runtime/ext/standard/tests/network.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/network.php line 3 */
Variant f_necho(int64 v_line_number, CVarRef v_string) {
  FUNCTION_INJECTION(necho);
  echo(LINE(4,concat4(toString(v_line_number), ": ", toString(v_string), "\n")));
  return v_string;
} /* function */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$network_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/network.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$network_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_id __attribute__((__unused__)) = (variables != gVariables) ? variables->get("id") : g->GV(id);
  Variant &v_mxhosts __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mxhosts") : g->GV(mxhosts);
  Variant &v_weights __attribute__((__unused__)) = (variables != gVariables) ? variables->get("weights") : g->GV(weights);

  {
    LINE(10,(assignCallTemp(eo_1, x_checkdnsrr("roadsend.com")),f_necho(10LL, eo_1)));
    LINE(11,(assignCallTemp(eo_1, x_checkdnsrr("roadsend.com", "A")),f_necho(20LL, eo_1)));
    LINE(12,(assignCallTemp(eo_1, x_checkdnsrr("blah.roadsend.com", "A")),f_necho(30LL, eo_1)));
    LINE(13,(assignCallTemp(eo_1, x_checkdnsrr("roadsend.com", "MX")),f_necho(40LL, eo_1)));
    LINE(14,(assignCallTemp(eo_1, x_checkdnsrr("roadsend.com", "SOA")),f_necho(50LL, eo_1)));
    LINE(15,(assignCallTemp(eo_1, x_checkdnsrr("roadsend.com", "PTR")),f_necho(60LL, eo_1)));
    LINE(16,(assignCallTemp(eo_1, x_checkdnsrr("smtp.roadsend.com", "CNAME")),f_necho(70LL, eo_1)));
    LINE(17,(assignCallTemp(eo_1, x_checkdnsrr("joplin.roadsend.com", "CNAME")),f_necho(80LL, eo_1)));
    LINE(18,(assignCallTemp(eo_1, x_checkdnsrr("roadsend.com", "ANY")),f_necho(90LL, eo_1)));
    LINE(19,(assignCallTemp(eo_1, x_checkdnsrr("-no-way-inthe-w0rld.com", "MX")),f_necho(100LL, eo_1)));
  }
  LINE(23,(assignCallTemp(eo_1, x_gethostbyname("www.roadsend.com")),f_necho(110LL, eo_1)));
  LINE(24,(assignCallTemp(eo_1, x_gethostbyname("tenkan.org")),f_necho(120LL, eo_1)));
  LINE(25,(assignCallTemp(eo_1, x_gethostbyname("localhost")),f_necho(130LL, eo_1)));
  LINE(28,(assignCallTemp(eo_1, x_gethostbyaddr("127.0.0.1")),f_necho(140LL, eo_1)));
  LINE(29,(assignCallTemp(eo_1, x_gethostbyaddr("216.244.107.14")),f_necho(150LL, eo_1)));
  LINE(30,(assignCallTemp(eo_1, x_gethostbyaddr("216.239.59.99")),f_necho(160LL, eo_1)));
  LINE(37,(assignCallTemp(eo_1, x_getprotobyname("ip")),f_necho(200LL, eo_1)));
  LINE(38,(assignCallTemp(eo_1, x_getprotobyname("tcp")),f_necho(210LL, eo_1)));
  LINE(39,(assignCallTemp(eo_1, x_getprotobyname("icmp")),f_necho(220LL, eo_1)));
  LINE(40,(assignCallTemp(eo_1, x_getprotobyname("udp")),f_necho(230LL, eo_1)));
  LINE(43,(assignCallTemp(eo_1, x_getprotobynumber(toInt32(0LL))),f_necho(240LL, eo_1)));
  LINE(44,(assignCallTemp(eo_1, x_getprotobynumber(toInt32(1LL))),f_necho(250LL, eo_1)));
  LINE(45,(assignCallTemp(eo_1, x_getprotobynumber(toInt32(17LL))),f_necho(260LL, eo_1)));
  LINE(46,(assignCallTemp(eo_1, x_getprotobynumber(toInt32(6LL))),f_necho(270LL, eo_1)));
  LINE(49,(assignCallTemp(eo_1, x_getservbyname("smtp", "tcp")),f_necho(280LL, eo_1)));
  LINE(50,(assignCallTemp(eo_1, x_getservbyname("daytime", "udp")),f_necho(290LL, eo_1)));
  LINE(51,(assignCallTemp(eo_1, x_getservbyname("echo", "tcp")),f_necho(300LL, eo_1)));
  LINE(52,(assignCallTemp(eo_1, x_getservbyname("ssh", "tcp")),f_necho(310LL, eo_1)));
  LINE(53,(assignCallTemp(eo_1, x_getservbyname("foobar", "udp")),f_necho(320LL, eo_1)));
  LINE(56,(assignCallTemp(eo_1, x_getservbyport(toInt32(19LL), "udp")),f_necho(330LL, eo_1)));
  LINE(57,(assignCallTemp(eo_1, x_getservbyport(toInt32(23LL), "tcp")),f_necho(340LL, eo_1)));
  LINE(58,(assignCallTemp(eo_1, x_getservbyport(toInt32(7LL), "udp")),f_necho(350LL, eo_1)));
  LINE(59,(assignCallTemp(eo_1, x_getservbyport(toInt32(119LL), "tcp")),f_necho(360LL, eo_1)));
  LINE(60,(assignCallTemp(eo_1, x_getservbyport(toInt32(123LL), "udp")),f_necho(370LL, eo_1)));
  LINE(88,f_necho(400LL, "log output: "));
  if (equal(LINE(89,x_posix_geteuid()), 0LL)) {
    LINE(90,x_system(concat5("tail -5000 `find /var/log -type f` | grep -a ", toString(v_id), " | sed s/..:..:..//g | sed 's/PCC_TEST\\[.*\\]/PCC_TEST/' | sed 's/", toString(v_id), "/PCC_TEST/' | sort")));
  }
  LINE(94,(assignCallTemp(eo_1, (x_define_syslog_variables(), null)),f_necho(410LL, eo_1)));
  LINE(99,(assignCallTemp(eo_1, x_gethostbynamel("www.roadsend.com")),f_necho(520LL, eo_1)));
  LINE(100,(assignCallTemp(eo_1, x_gethostbynamel("tenkan.org")),f_necho(530LL, eo_1)));
  LINE(101,(assignCallTemp(eo_1, x_gethostbynamel("localhost")),f_necho(540LL, eo_1)));
  {
    (v_mxhosts = ScalarArrays::sa_[0]);
    (v_weights = ScalarArrays::sa_[0]);
    LINE(107,(assignCallTemp(eo_1, x_getmxrr("roadsend.com", ref(v_mxhosts), ref(v_weights))),f_necho(550LL, eo_1)));
    LINE(108,x_sort(ref(v_mxhosts)));
    LINE(109,x_sort(ref(v_weights)));
    LINE(110,f_necho(560LL, "mxhosts: "));
    LINE(111,x_print_r(v_mxhosts));
    LINE(112,f_necho(570LL, "weights: "));
    LINE(113,x_print_r(v_weights));
    LINE(114,(assignCallTemp(eo_1, x_getmxrr("google.com", ref(v_mxhosts), ref(v_weights))),f_necho(580LL, eo_1)));
    LINE(115,x_sort(ref(v_mxhosts)));
    LINE(116,x_sort(ref(v_weights)));
    LINE(117,f_necho(590LL, "mxhosts: "));
    LINE(118,x_print_r(v_mxhosts));
    LINE(119,f_necho(600LL, "weights: "));
    LINE(120,x_print_r(v_weights));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
