
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_network_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_network_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/network.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$network_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_necho(int64 v_line_number, CVarRef v_string);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_network_h__
