
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "var_array",
    "color",
    "size",
    "shape",
    "pref_color",
    "pref_size",
    "pref_shape",
    "newpref_color",
    "newpref_size",
    "blahshape",
    "inpref_22",
    "noshape",
    "ifpref_color",
    "ifpref_size",
    "meepshape",
    "fnord",
  };
  if (idx >= 0 && idx < 28) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(var_array);
    case 13: return GV(color);
    case 14: return GV(size);
    case 15: return GV(shape);
    case 16: return GV(pref_color);
    case 17: return GV(pref_size);
    case 18: return GV(pref_shape);
    case 19: return GV(newpref_color);
    case 20: return GV(newpref_size);
    case 21: return GV(blahshape);
    case 22: return GV(inpref_22);
    case 23: return GV(noshape);
    case 24: return GV(ifpref_color);
    case 25: return GV(ifpref_size);
    case 26: return GV(meepshape);
    case 27: return GV(fnord);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
