
#include <php/roadsend/pcc/runtime/ext/standard/tests/setlocale.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$setlocale_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/setlocale.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$setlocale_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_loc_de __attribute__((__unused__)) = (variables != gVariables) ? variables->get("loc_de") : g->GV(loc_de);

  LINE(3,x_setlocale(2, toInt32(6LL) /* LC_ALL */, "nl_NL"));
  echo(LINE(6,(assignCallTemp(eo_1, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(12LL), toInt32(22LL), toInt32(1978LL)))),x_strftime("%A %e %B %Y", eo_1))));
  echo("\n");
  echo(LINE(8,(assignCallTemp(eo_1, toInt64(x_mktime(toInt32(0LL), toInt32(0LL), toInt32(0LL), toInt32(12LL), toInt32(22LL), toInt32(1978LL)))),x_gmstrftime("%A %e %B %Y", eo_1))));
  echo("\n");
  (v_loc_de = LINE(12,x_setlocale(5, toInt32(6LL) /* LC_ALL */, "de_DE@euro", ScalarArrays::sa_[0])));
  echo(LINE(13,concat3("Preferred locale for german on this system is '", toString(v_loc_de), "'\n")));
  (v_loc_de = LINE(17,x_setlocale(2, toInt32(6LL) /* LC_ALL */, ScalarArrays::sa_[1])));
  echo(LINE(18,concat3("Preferred locale for german on this system is '", toString(v_loc_de), "'\n")));
  echo(" \n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
