
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_files_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_files_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/files.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_first_digit(CVarRef v_number);
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$files_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_files_h__
