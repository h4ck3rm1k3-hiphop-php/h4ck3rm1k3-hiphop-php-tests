
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x2027864469AD4382LL, string, 14);
      HASH_INDEX(0x640D12BF35788502LL, newc, 21);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 23);
      break;
    case 13:
      HASH_INDEX(0x41C3897C4E915A4DLL, testfile2, 13);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x60A103FBE1446E10LL, buffer, 18);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x3A1DC49AA6CFEA13LL, test, 16);
      break;
    case 28:
      HASH_INDEX(0x48E8F48146EEEF5CLL, handle, 17);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 40:
      HASH_INDEX(0x30164401A9853128LL, data, 19);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      HASH_INDEX(0x6BB4A0689FBAD42ELL, f, 22);
      break;
    case 48:
      HASH_INDEX(0x5B0C0C547374D530LL, fp, 15);
      HASH_INDEX(0x32C769EE5C5509B0LL, c, 25);
      break;
    case 54:
      HASH_INDEX(0x31CF71EAC03B86B6LL, testfile, 12);
      HASH_INDEX(0x66D0F03A76191AF6LL, here, 24);
      break;
    case 62:
      HASH_INDEX(0x5FAC83E143BEACFELL, contents, 20);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 26) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
