
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_core_fw_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_core_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_FOO;
extern const StaticString k_ZoT;


// 2. Classes
FORWARD_DECLARE_CLASS(someclass)
FORWARD_DECLARE_CLASS(myclass)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_core_fw_h__
