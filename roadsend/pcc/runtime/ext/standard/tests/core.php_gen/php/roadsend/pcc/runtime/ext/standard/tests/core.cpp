
#include <php/roadsend/pcc/runtime/ext/standard/tests/core.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_FOO = "bar";
const StaticString k_ZoT = "baz";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 83 */
Variant c_someclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_someclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_someclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_someclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_someclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_someclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_someclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_someclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(someclass)
ObjectData *c_someclass::cloneImpl() {
  c_someclass *obj = NEW(c_someclass)();
  cloneSet(obj);
  return obj;
}
void c_someclass::cloneSet(c_someclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_someclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x7801B24A1BB8370FLL, somemethod) {
        return (t_somemethod(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_someclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x7801B24A1BB8370FLL, somemethod) {
        return (t_somemethod(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_someclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_someclass$os_get(const char *s) {
  return c_someclass::os_get(s, -1);
}
Variant &cw_someclass$os_lval(const char *s) {
  return c_someclass::os_lval(s, -1);
}
Variant cw_someclass$os_constant(const char *s) {
  return c_someclass::os_constant(s);
}
Variant cw_someclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_someclass::os_invoke(c, s, params, -1, fatal);
}
void c_someclass::init() {
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 85 */
void c_someclass::t_somemethod() {
  INSTANCE_METHOD_INJECTION(someClass, someClass::someMethod);
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 8 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x7A77480740086EA1LL, somefunction) {
        return (t_somefunction(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x7A77480740086EA1LL, somefunction) {
        return (t_somefunction(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 10 */
void c_myclass::t_somefunction(CVarRef v_a, CVarRef v_b) {
  INSTANCE_METHOD_INJECTION(myClass, myClass::someFunction);
  echo(LINE(11,concat5("in class with someFunction ", toString(v_a), " and ", toString(v_b), "\n")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 16 */
void f_afunction(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(aFunction);
  echo(LINE(17,concat5("in aFunction ", toString(v_a), " and ", toString(v_b), "\n")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 68 */
void f_somefunction() {
  FUNCTION_INJECTION(someFunction);
} /* function */
Variant i_afunction(CArrRef params) {
  return (f_afunction(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_somefunction(CArrRef params) {
  return (f_somefunction(), null);
}
Object co_someclass(CArrRef params, bool init /* = true */) {
  return Object(p_someclass(NEW(c_someclass)())->dynCreate(params, init));
}
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$core_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/core.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$core_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_cl __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cl") : g->GV(cl);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_functionVariable __attribute__((__unused__)) = (variables != gVariables) ? variables->get("functionVariable") : g->GV(functionVariable);
  Variant &v_callable_name __attribute__((__unused__)) = (variables != gVariables) ? variables->get("callable_name") : g->GV(callable_name);
  Variant &v_anObject __attribute__((__unused__)) = (variables != gVariables) ? variables->get("anObject") : g->GV(anObject);
  Variant &v_methodVariable __attribute__((__unused__)) = (variables != gVariables) ? variables->get("methodVariable") : g->GV(methodVariable);

  g->declareClass("myclass");
  LINE(3,x_putenv("TEST_ENV=testval"));
  (v_val = LINE(5,x_getenv("TEST_ENV")));
  echo(LINE(6,concat3("the environment value is: [", toString(v_val), "]\n")));
  LINE(20,x_call_user_func(3, "aFunction", ScalarArrays::sa_[0]));
  LINE(21,x_call_user_func(3, ScalarArrays::sa_[1], ScalarArrays::sa_[0]));
  LINE(23,x_call_user_func_array("aFunction", ScalarArrays::sa_[2]));
  (v_c = ((Object)(LINE(25,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  LINE(26,x_call_user_method(4, "someFunction", ref(v_c), ScalarArrays::sa_[3]));
  LINE(27,x_call_user_method_array("someFunction", ref(v_c), ScalarArrays::sa_[4]));
  LINE(30,x_call_user_func(3, Array(ArrayInit(2).setRef(0, ref(v_c)).set(1, "someFunction").create()), ScalarArrays::sa_[5]));
  echo("1:");
  echo("\n");
  echo(concat("2:", toString(LINE(33,f_class_exists("myclass")))));
  echo("\n");
  echo(concat("2.5:", toString(LINE(34,f_class_exists("myclass")))));
  echo("\n");
  echo(concat("3:", toString(LINE(36,x_method_exists("myClass", "someFunction")))));
  echo("\n");
  echo(concat("3.5:", toString(LINE(37,x_method_exists("myclass", "somefunction")))));
  echo("\n");
  echo(concat("4:", toString(LINE(38,x_method_exists("myClass", "aFunction")))));
  echo("\n");
  echo(concat("5:", toString(LINE(39,x_method_exists("noClass", "someFunction")))));
  echo("\n");
  (v_cl = LINE(43,x_get_declared_classes()));
  LINE(47,x_set_include_path("./:/home/test"));
  echo(concat(LINE(48,x_get_include_path()), "\n"));
  ;
  ;
  (v_v = LINE(53,x_get_defined_constants()));
  echo(concat("Last modified: ", LINE(57,(assignCallTemp(eo_1, toInt64(x_getlastmod())),x_date("F d Y H:i:s.", eo_1)))));
  {
    echo(concat(LINE(60,x_get_current_user()), "\n"));
    echo(concat(toString(LINE(63,x_getmyuid())), "\n"));
    echo(concat(toString(LINE(64,x_getmygid())), "\n"));
  }
  (v_functionVariable = "someFunction");
  LINE(74,x_var_dump(1, x_is_callable(v_functionVariable, false, ref(v_callable_name))));
  LINE(75,x_var_dump(1, x_is_callable(v_functionVariable, true, ref(v_callable_name))));
  {
    echo(toString(v_callable_name));
    echo("\n");
  }
  (v_anObject = ((Object)(LINE(91,p_someclass(p_someclass(NEWOBJ(c_someclass)())->create())))));
  (v_methodVariable = Array(ArrayInit(2).set(0, v_anObject).set(1, "someMethod").create()));
  LINE(95,x_var_dump(1, x_is_callable(v_methodVariable, true, ref(v_callable_name))));
  LINE(96,x_var_dump(1, x_is_callable(v_methodVariable, false, ref(v_callable_name))));
  {
    echo(toString(v_callable_name));
    echo("\n");
  }
  LINE(100,x_var_dump(1, x_is_callable(ScalarArrays::sa_[6], true, ref(v_callable_name))));
  LINE(101,x_var_dump(1, x_is_callable(ScalarArrays::sa_[7], true, ref(v_callable_name))));
  LINE(102,x_var_dump(1, x_is_callable(32131LL, true, ref(v_callable_name))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
