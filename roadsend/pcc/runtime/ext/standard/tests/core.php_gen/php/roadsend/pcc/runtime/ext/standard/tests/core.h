
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_core_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_core_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/core.fw.h>

// Declarations
#include <cls/someclass.h>
#include <cls/myclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_afunction(CVarRef v_a, CVarRef v_b);
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$core_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_somefunction();
Object co_someclass(CArrRef params, bool init = true);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_core_h__
