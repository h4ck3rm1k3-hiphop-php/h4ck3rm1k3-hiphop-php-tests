
#ifndef __GENERATED_cls_someclass_h__
#define __GENERATED_cls_someclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 83 */
class c_someclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(someclass)
  END_CLASS_MAP(someclass)
  DECLARE_CLASS(someclass, someClass, ObjectData)
  void init();
  public: void t_somemethod();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_someclass_h__
