
#ifndef __GENERATED_cls_myclass_h__
#define __GENERATED_cls_myclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/standard/tests/core.php line 8 */
class c_myclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass)
  END_CLASS_MAP(myclass)
  DECLARE_CLASS(myclass, myClass, ObjectData)
  void init();
  public: void t_somefunction(CVarRef v_a, CVarRef v_b);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass_h__
