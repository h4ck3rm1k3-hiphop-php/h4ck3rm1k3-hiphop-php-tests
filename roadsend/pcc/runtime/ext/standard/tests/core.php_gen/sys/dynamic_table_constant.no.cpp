
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_FOO;
extern const StaticString k_ZoT;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x6176C0B993BF5914LL, k_FOO, FOO);
      break;
    case 3:
      HASH_RETURN(0x5D671D2B81C42D93LL, k_ZoT, ZoT);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
