
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_strip_tags_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_strip_tags_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/strip_tags.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$strip_tags_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_strip_tags_h__
