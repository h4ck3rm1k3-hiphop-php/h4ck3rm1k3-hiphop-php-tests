
#include <php/roadsend/pcc/runtime/ext/standard/tests/strip_tags.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$strip_tags_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/strip_tags.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$strip_tags_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_html __attribute__((__unused__)) = (variables != gVariables) ? variables->get("html") : g->GV(html);

  (v_html = "\n\n<h1\n><a\nNAME=\"function.strip-tags\"\n></a\n>strip_tags</h1\n><div\nCLASS=\"refnamediv\"\n><a\nNAME=\"AEN101923\"\n></a\n><p\n>    (PHP 3&#62;= 3.0.8, PHP 4 )</p\n>strip_tags&nbsp;--&nbsp;Strip HTML and PHP tags from a string</div\n><div\nCLASS=\"refsect1\"\n><a\nNAME=\"AEN101926\"\n></a\n><h2\n>Description</h2\n>string <b\nCLASS=\"methodname\"\n>strip_tags</b\n> ( string str [, string allowable_tags])<br\n></br\n><p\n>&#13;     This function tries to return a string with all HTML and PHP tags\n     stripped from a given <tt\nCLASS=\"parameter\"\n><i\n>str</i\n></tt\n>.  It errors on\n     the side of caution in case of incomplete or bogus tags.  It uses\n     the same tag stripping state machine as the\n     <a\nHREF=\"function.fgetss.php\"\n><b\nCLASS=\"function\"\n>fgetss()</b\n></a\n> function.\n    </p\n><p\n>&#13;     You can use the optional second parameter to specify tags which\n     should not be stripped.\n     <div\nCLASS=\"note\"\n><blockquote\nCLASS=\"note\"\n><p\n><b\n>Note: </b\n>\n\n       <tt\nCLASS=\"parameter\"\n><i\n>allowable_tags</i\n></tt\n> was added in PHP 3.0.13\n       and PHP 4.0b3.\n      </p\n></blockquote\n></div\n>\n    </p\n><p\n>&#13;     <table\nWIDTH=\"100%\"\nBORDER=\"0\"\nCELLPADDING=\"0\"\nCELLSPACING=\"0\"\nCLASS=\"EXAMPLE\"\n><tr\n><td\n><div\nCLASS=\"example\"\n><a\nNAME=\"AEN101945\"\n></a\n><p\n><b\n>Example 1. <b\nCLASS=\"function\"\n>strip_tags()</b\n> example</b\n></p\n><table\nBORDER=\"0\"\nBGCOLOR=\"#E0E0E0\"\nCELLPADDING=\"5\"\n><tr\n><td\n><pre\nCLASS=\"php\"\n>&#60;\?php\n$string = strip_tags($string, '&#60;a&#62;&#60;b&#62;&#60;i&#62;&#60;u&#62;');\n\?&#62;</pre\n></td\n></tr\n></table\n></div\n></td\n></tr\n></table\n>\n\n    </p\n><div\nCLASS=\"warning\"\n><p\n></p\n><table\nCLASS=\"warning\"\nBORDER=\"1\"\nWIDTH=\"100%\"\n><tr\n><td\nALIGN=\"CENTER\"\n><b\n>Warning</b\n></td\n></tr\n><tr\n><td\nALIGN=\"LEFT\"\n><p\n>&#13;      This function does not modify any attributes on the tags that you allow\n      using <tt\nCLASS=\"parameter\"\n><i\n>allowable_tags</i\n></tt\n>, including the\n      <tt\nCLASS=\"literal\"\n>style</tt\n> and <tt\nCLASS=\"literal\"\n>onmouseover</tt\n> attributes\n      that a mischievous user may abuse when posting text that will be shown\n      to other users.\n     </p\n></td\n></tr\n></table\n></div\n></div\n><br /><br />\n\n");
  echo(LINE(161,x_strip_tags("NEAT <img src=\"test\"> STUFF")));
  echo("\n");
  echo(LINE(164,x_strip_tags("NEAT <img > src=\"test\"> STUFF")));
  echo("\n");
  echo(LINE(167,x_strip_tags("NEAT <img < src=\"test\"> STUFF")));
  echo("\n");
  echo(LINE(170,x_strip_tags("NEAT < img < src=\"test\"> STUFF")));
  echo("\n");
  echo(LINE(175,x_strip_tags("<b\n>STUFF</b\n>")));
  echo("\n");
  echo(LINE(178,x_strip_tags("<td>NEAT < here <img < src=\"test\"> STUFF</td>")));
  echo("\n");
  echo(LINE(181,x_strip_tags("NEAT <\? cool < blah \?> STUFF")));
  echo("\n");
  echo(LINE(184,x_strip_tags("NEAT <\? cool > blah \?> STUFF")));
  echo("\n");
  echo(LINE(187,x_strip_tags("NEAT <!-- cool < blah --> STUFF")));
  echo("\n");
  echo(LINE(191,x_strip_tags("NEAT <!-- cool > blah --> STUFF")));
  echo("\n");
  echo(LINE(194,x_strip_tags("NEAT <\? echo \\\"\\\\\"\\\"\?> STUFF")));
  echo("\n");
  echo(LINE(197,x_strip_tags("NEAT <\? echo '\\''\?> STUFF")));
  echo("\n");
  echo(LINE(199,x_strip_tags("TESTS \?!!\?!\?!!!\?!!")));
  echo("\n");
  echo(LINE(202,x_strip_tags(toString(v_html))));
  echo(LINE(203,x_strip_tags(toString(v_html), "<b>")));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
