
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_files2_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_files2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/files2.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$files2_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_first_digit(CVarRef v_number);
Variant f_necho(int64 v_line_number, CVarRef v_string);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_files2_h__
