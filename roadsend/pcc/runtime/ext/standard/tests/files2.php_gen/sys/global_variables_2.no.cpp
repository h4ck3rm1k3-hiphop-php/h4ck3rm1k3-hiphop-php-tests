
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      break;
    case 4:
      HASH_INITIALIZED(0x42DD5992F362B3C4LL, g->GV(path),
                       path);
      HASH_INITIALIZED(0x336176791BC03F04LL, g->GV(num),
                       num);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 7:
      HASH_INITIALIZED(0x322D3D8EFCB96007LL, g->GV(tmpfname),
                       tmpfname);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      HASH_INITIALIZED(0x4E60097CEE8FD6CELL, g->GV(tmpdir),
                       tmpdir);
      break;
    case 15:
      HASH_INITIALIZED(0x0D6857FDDD7F21CFLL, g->GV(k),
                       k);
      HASH_INITIALIZED(0x3B32AB004231C30FLL, g->GV(stat),
                       stat);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 18:
      HASH_INITIALIZED(0x4307151CEB3C6312LL, g->GV(row),
                       row);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 28:
      HASH_INITIALIZED(0x48E8F48146EEEF5CLL, g->GV(handle),
                       handle);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 40:
      HASH_INITIALIZED(0x30164401A9853128LL, g->GV(data),
                       data);
      break;
    case 43:
      HASH_INITIALIZED(0x3C2F961831E4EF6BLL, g->GV(v),
                       v);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 48:
      HASH_INITIALIZED(0x32C769EE5C5509B0LL, g->GV(c),
                       c);
      break;
    case 50:
      HASH_INITIALIZED(0x4DCB2F04C3F8C672LL, g->GV(symlink_name),
                       symlink_name);
      break;
    case 51:
      HASH_INITIALIZED(0x5497579F8DA0CBF3LL, g->GV(newname),
                       newname);
      break;
    case 53:
      HASH_INITIALIZED(0x483374BF62404B35LL, g->GV(link_name),
                       link_name);
      break;
    case 54:
      HASH_INITIALIZED(0x31CF71EAC03B86B6LL, g->GV(testfile),
                       testfile);
      break;
    case 57:
      HASH_INITIALIZED(0x2ED65F776BB66679LL, g->GV(fstat),
                       fstat);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
