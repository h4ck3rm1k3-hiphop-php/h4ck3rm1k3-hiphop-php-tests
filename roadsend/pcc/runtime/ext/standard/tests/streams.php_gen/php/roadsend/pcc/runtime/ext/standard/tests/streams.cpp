
#include <php/roadsend/pcc/runtime/ext/standard/tests/streams.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/streams.php line 10 */
Variant f_necho(int64 v_line_number, CVarRef v_string) {
  FUNCTION_INJECTION(necho);
  echo(LINE(11,concat4(toString(v_line_number), ": ", toString(v_string), "\n")));
  return v_string;
} /* function */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$streams_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/streams.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$streams_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_handle __attribute__((__unused__)) = (variables != gVariables) ? variables->get("handle") : g->GV(handle);
  Variant &v_buffer __attribute__((__unused__)) = (variables != gVariables) ? variables->get("buffer") : g->GV(buffer);

  LINE(8,x_error_reporting(toInt32(6143LL) /* E_ALL */));
  LINE(22,f_necho(70LL, 1LL /* STREAM_USE_PATH */));
  LINE(23,f_necho(80LL, 8LL /* STREAM_REPORT_ERRORS */));
  LINE(29,f_necho(140LL, 2LL /* STREAM_NOTIFY_CONNECT */));
  LINE(30,f_necho(150LL, 3LL /* STREAM_NOTIFY_AUTH_REQUIRED */));
  LINE(31,f_necho(160LL, 4LL /* STREAM_NOTIFY_MIME_TYPE_IS */));
  LINE(32,f_necho(170LL, 5LL /* STREAM_NOTIFY_FILE_SIZE_IS */));
  LINE(33,f_necho(180LL, 6LL /* STREAM_NOTIFY_REDIRECTED */));
  LINE(34,f_necho(190LL, 7LL /* STREAM_NOTIFY_PROGRESS */));
  LINE(36,f_necho(210LL, 9LL /* STREAM_NOTIFY_FAILURE */));
  LINE(37,f_necho(220LL, 10LL /* STREAM_NOTIFY_AUTH_RESULT */));
  LINE(38,f_necho(230LL, 0LL /* STREAM_NOTIFY_SEVERITY_INFO */));
  LINE(39,f_necho(240LL, 1LL /* STREAM_NOTIFY_SEVERITY_WARN */));
  LINE(40,f_necho(250LL, 2LL /* STREAM_NOTIFY_SEVERITY_ERR */));
  (v_fp = LINE(44,x_fopen("/tmp/foobar-context-test-file", "w+")));
  LINE(45,f_necho(260LL, "stream_context_get_options:"));
  LINE(46,x_var_dump(1, x_stream_context_get_options(toObject(v_fp))));
  LINE(47,(assignCallTemp(eo_1, x_stream_context_set_option(toObject(v_fp), "the-wrapper", "the-option", "the-value")),f_necho(270LL, eo_1)));
  LINE(48,f_necho(280LL, "stream_context_get_options:"));
  LINE(49,x_var_dump(1, x_stream_context_get_options(toObject(v_fp))));
  LINE(61,f_necho(310LL, "stream_context_get_options:"));
  LINE(62,x_var_dump(1, x_stream_context_get_options(toObject(v_fp))));
  LINE(63,(assignCallTemp(eo_1, x_fclose(toObject(v_fp))),f_necho(320LL, eo_1)));
  (v_handle = LINE(72,x_fopen("http://www.roadsend.com/license/rpl1.txt", "r")));
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_buffer = LINE(80,x_fgets(toObject(v_handle)))))) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(81,concat3("--<", toString(v_buffer), ">--")));
      }
    }
  }
  LINE(83,x_fclose(toObject(v_handle)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
