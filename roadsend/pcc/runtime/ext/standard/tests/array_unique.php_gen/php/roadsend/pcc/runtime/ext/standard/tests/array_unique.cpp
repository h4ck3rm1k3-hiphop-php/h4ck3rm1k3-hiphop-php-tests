
#include <php/roadsend/pcc/runtime/ext/standard/tests/array_unique.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$array_unique_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/array_unique.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$array_unique_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_input2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input2") : g->GV(input2);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);

  (v_input = ScalarArrays::sa_[0]);
  (v_input2 = ScalarArrays::sa_[0]);
  LINE(6,x_var_dump(1, v_input));
  LINE(8,x_asort(ref(v_input2), toInt32(2LL) /* SORT_STRING */));
  LINE(9,x_var_dump(1, v_input2));
  (v_result = LINE(11,x_array_unique(v_input)));
  LINE(12,x_var_dump(1, v_result));
  (v_input = ScalarArrays::sa_[1]);
  (v_result = LINE(16,x_array_unique(v_input)));
  LINE(17,x_var_dump(1, v_result));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
