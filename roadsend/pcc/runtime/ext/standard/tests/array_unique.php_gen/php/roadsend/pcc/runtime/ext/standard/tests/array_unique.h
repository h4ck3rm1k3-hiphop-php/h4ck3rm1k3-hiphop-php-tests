
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_array_unique_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_array_unique_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/array_unique.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$array_unique_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_array_unique_h__
