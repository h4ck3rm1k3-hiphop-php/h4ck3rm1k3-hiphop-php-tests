
#include <php/roadsend/pcc/runtime/ext/standard/tests/eregexp.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$eregexp_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/eregexp.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$eregexp_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_regs __attribute__((__unused__)) = (variables != gVariables) ? variables->get("regs") : g->GV(regs);
  Variant &v_date __attribute__((__unused__)) = (variables != gVariables) ? variables->get("date") : g->GV(date);

  (v_string = "adsf asdfljasldkfj als alskj abc asdfl jaskdjf lsdjg ");
  if (toBoolean(LINE(6,x_ereg("abc", toString(v_string))))) {
    echo("works\n");
  }
  if (toBoolean(LINE(12,x_ereg("^abc", toString(v_string))))) {
    echo("broken\n");
  }
  if (toBoolean(LINE(16,x_ereg("abc$", toString(v_string))))) {
    echo("broken\n");
  }
  (v_string = "adsf asdfljasldkfj als\n alskj abc asdfl\n jaskdjf lsdjg abc");
  if (toBoolean(LINE(24,x_ereg("abc$", toString(v_string))))) {
    echo("works\n");
  }
  LINE(32,x_ereg("([[:alnum:]]+) ([[:alnum:]]+) ([[:alnum:]]+)", toString(v_string), ref(v_regs)));
  LINE(34,x_print_r(v_regs));
  print(toString((toString(v_string))));
  print(toString((toString(v_string))));
  (v_string = "adsf asdfljasldkfj als\n alskj abc asdfl\n jaskdjf lsdjg abc");
  (v_string = LINE(53,x_ereg_replace("a", "zoot", toString(v_string))));
  print(toString((toString(v_string))));
  LINE(57,x_print_r(x_split(" ", toString(v_string), toInt32(3LL))));
  (v_date = "2003-07-03");
  if (toBoolean(LINE(64,x_ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", toString(v_date), ref(v_regs))))) {
    echo(LINE(65,concat5(toString(v_regs.rvalAt(3, 0x135FDDF6A6BFBBDDLL)), ".", toString(v_regs.rvalAt(2, 0x486AFCC090D5F98CLL)), ".", toString(v_regs.rvalAt(1, 0x5BCA7C69B794F8CELL)))));
  }
  else {
    echo(toString("Invalid date format: ") + toString(v_date));
  }
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
