
#include <php/roadsend/pcc/runtime/ext/standard/tests/html.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$html_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/html.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$html_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_new __attribute__((__unused__)) = (variables != gVariables) ? variables->get("new") : g->GV(new);
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_t = LINE(3,x_get_html_translation_table(toInt32(1LL) /* HTML_ENTITIES */)));
  LINE(4,x_var_dump(1, v_t));
  (v_t = LINE(6,x_get_html_translation_table(toInt32(0LL) /* HTML_SPECIALCHARS */)));
  LINE(7,x_var_dump(1, v_t));
  (v_t = LINE(9,x_get_html_translation_table(toInt32(0LL) /* HTML_SPECIALCHARS */, toInt32(0LL) /* ENT_NOQUOTES */)));
  LINE(10,x_var_dump(1, v_t));
  (v_t = LINE(12,x_get_html_translation_table(toInt32(0LL) /* HTML_SPECIALCHARS */, toInt32(3LL) /* ENT_QUOTES */)));
  LINE(13,x_var_dump(1, v_t));
  (v_new = LINE(16,x_htmlspecialchars("<a href='test'>Test \"test\"</a>\n")));
  echo(toString(v_new));
  (v_new = LINE(19,x_htmlspecialchars("<a href='test'>Test \"test\"</a>\n", toInt32(0LL) /* ENT_NOQUOTES */)));
  echo(toString(v_new));
  (v_new = LINE(22,x_htmlspecialchars("<a href='test'>Test \"test\"</a>\n", toInt32(3LL) /* ENT_QUOTES */)));
  echo(toString(v_new));
  (v_str = "");
  {
    LOOP_COUNTER(1);
    for ((v_i = 30LL); less(v_i, 256LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_str, concat(LINE(27,x_chr(toInt64(v_i))), "\n"));
      }
    }
  }
  echo(toString(v_str) + toString("\n"));
  echo(LINE(31,(assignCallTemp(eo_1, x_htmlentities(toString(v_str))),concat3("compat: ", eo_1, "\n"))));
  echo(LINE(32,(assignCallTemp(eo_1, x_htmlentities(toString(v_str), toInt32(0LL) /* ENT_NOQUOTES */)),concat3("noquotes: ", eo_1, "\n"))));
  echo(LINE(33,(assignCallTemp(eo_1, x_htmlentities(toString(v_str), toInt32(3LL) /* ENT_QUOTES */)),concat3("quotes: ", eo_1, "\n"))));
  echo(LINE(35,(assignCallTemp(eo_1, x_html_entity_decode(x_htmlentities(toString(v_str), toInt32(3LL) /* ENT_QUOTES */))),concat3("compat: ", eo_1, "\n"))));
  echo(LINE(36,(assignCallTemp(eo_1, (assignCallTemp(eo_3, x_htmlentities(toString(v_str), toInt32(3LL) /* ENT_QUOTES */)),x_html_entity_decode(eo_3, toInt32(0LL) /* ENT_NOQUOTES */))),concat3("noquotes: ", eo_1, "\n"))));
  echo(LINE(37,(assignCallTemp(eo_1, (assignCallTemp(eo_3, x_htmlentities(toString(v_str), toInt32(3LL) /* ENT_QUOTES */)),x_html_entity_decode(eo_3, toInt32(3LL) /* ENT_QUOTES */))),concat3("quotes: ", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
