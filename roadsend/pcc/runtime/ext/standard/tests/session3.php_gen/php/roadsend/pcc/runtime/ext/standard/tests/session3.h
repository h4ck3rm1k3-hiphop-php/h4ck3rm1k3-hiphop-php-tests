
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_session3_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_session3_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/session3.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant f_destroy(CVarRef v_id);
bool f_gc(CVarRef v_maxlifetime);
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$session3_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_write(CVarRef v_id, CVarRef v_sess_data);
bool f_close();
bool f_open(CVarRef v_save_path, CVarRef v_session_name);
Variant f_read(CVarRef v_id);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_session3_h__
