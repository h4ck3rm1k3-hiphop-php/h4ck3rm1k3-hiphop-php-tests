
#include <php/roadsend/pcc/runtime/ext/standard/tests/blocking.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$blocking_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/blocking.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$blocking_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_sLine __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sLine") : g->GV(sLine);

  echo("This test thanks to groove :)\n\n");
  echo("without blocking: \n");
  (v_fp = LINE(5,x_fsockopen("www.ebay.com", toInt32(80LL))));
  LINE(6,x_fputs(toObject(v_fp), "GET / HTTP/1.0\r\nHost: www.ebay.com\r\n\r\n"));
  LINE(7,x_stream_set_blocking(toObject(v_fp), toInt32(false)));
  (v_sLine = LINE(8,x_fgets(toObject(v_fp), 1024LL)));
  echo(toString(v_sLine));
  echo("and with blocking: \n");
  LINE(11,x_stream_set_blocking(toObject(v_fp), toInt32(true)));
  (v_sLine = LINE(12,x_fgets(toObject(v_fp), 1024LL)));
  echo(toString(v_sLine));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
