
#include <php/roadsend/pcc/runtime/ext/standard/tests/utf.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$utf_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/utf.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$utf_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(concat(LINE(3,x_utf8_encode(concat("test", x_chr(205LL)))), "\n"));
  echo(concat(LINE(4,x_utf8_decode("test")), "\n"));
  echo(LINE(6,(assignCallTemp(eo_1, x_utf8_encode("\346")),concat3("utf_encode: ", eo_1, "\n"))));
  LINE(8,(assignCallTemp(eo_1, x_urlencode("\346")),assignCallTemp(eo_2, x_urlencode(x_utf8_encode("\346"))),x_printf(3, "%s -> %s\n", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create()))));
  LINE(9,(assignCallTemp(eo_1, x_urlencode(x_utf8_decode(x_urldecode("%C3%A6")))),x_printf(3, "%s <- %s\n", Array(ArrayInit(2).set(0, eo_1).set(1, "%C3%A6").create()))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
