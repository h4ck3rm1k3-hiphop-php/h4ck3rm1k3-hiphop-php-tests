
#include <php/roadsend/pcc/runtime/ext/standard/tests/array_sorting.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_6(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_7(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_10(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_11(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/array_sorting.php line 83 */
int64 f_cmp(CVarRef v_a, CVarRef v_b) {
  FUNCTION_INJECTION(cmp);
  if (equal(v_a, v_b)) {
    return 0LL;
  }
  return (less(v_a, v_b)) ? ((-1LL)) : ((1LL));
} /* function */
Variant i_cmp(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x30E2D55E70B155CDLL, cmp) {
    return (f_cmp(params.rvalAt(0), params.rvalAt(1)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$array_sorting_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/array_sorting.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$array_sorting_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_input2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input2") : g->GV(input2);
  Variant &v_input3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input3") : g->GV(input3);
  Variant &v_input4 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input4") : g->GV(input4);
  Variant &v_fruits __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fruits") : g->GV(fruits);
  Variant &v_key __attribute__((__unused__)) = (variables != gVariables) ? variables->get("key") : g->GV(key);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_value __attribute__((__unused__)) = (variables != gVariables) ? variables->get("value") : g->GV(value);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_array1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array1") : g->GV(array1);
  Variant &v_array2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array2") : g->GV(array2);
  Variant &v_array3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array3") : g->GV(array3);
  Variant &v_img __attribute__((__unused__)) = (variables != gVariables) ? variables->get("img") : g->GV(img);
  Variant &v_wtfarray __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wtfarray") : g->GV(wtfarray);
  Variant &v_b2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b2") : g->GV(b2);

  print("input:\n");
  (v_input = ScalarArrays::sa_[0]);
  (v_input2 = ScalarArrays::sa_[0]);
  LINE(7,x_var_dump(1, v_input));
  print("asort string:\n");
  LINE(9,x_asort(ref(v_input2), toInt32(2LL) /* SORT_STRING */));
  LINE(10,x_var_dump(1, v_input2));
  (v_input = ScalarArrays::sa_[1]);
  (v_input2 = ScalarArrays::sa_[1]);
  (v_input3 = ScalarArrays::sa_[1]);
  (v_input4 = ScalarArrays::sa_[1]);
  print("input:\n");
  LINE(18,x_var_dump(1, v_input));
  LINE(20,x_asort(ref(v_input2), toInt32(2LL) /* SORT_STRING */));
  print("asort string:\n");
  LINE(22,x_var_dump(1, v_input2));
  LINE(24,x_asort(ref(v_input3), toInt32(0LL) /* SORT_REGULAR */));
  print("asort regular:\n");
  LINE(26,x_var_dump(1, v_input3));
  LINE(27,x_asort(ref(v_input4), toInt32(1LL) /* SORT_NUMERIC */));
  print("asort numeric:\n");
  LINE(29,x_var_dump(1, v_input4));
  (v_input = ScalarArrays::sa_[2]);
  (v_input2 = ScalarArrays::sa_[2]);
  (v_input3 = ScalarArrays::sa_[2]);
  (v_input4 = ScalarArrays::sa_[2]);
  print("input:\n");
  LINE(36,x_var_dump(1, v_input));
  LINE(38,x_asort(ref(v_input2), toInt32(2LL) /* SORT_STRING */));
  print("asort string:\n");
  LINE(40,x_var_dump(1, v_input2));
  LINE(41,x_asort(ref(v_input3), toInt32(0LL) /* SORT_REGULAR */));
  print("asort regular:\n");
  LINE(43,x_var_dump(1, v_input3));
  LINE(44,x_asort(ref(v_input4), toInt32(1LL) /* SORT_NUMERIC */));
  print("asort numeric:\n");
  LINE(46,x_var_dump(1, v_input4));
  (v_input = ScalarArrays::sa_[3]);
  (v_input2 = ScalarArrays::sa_[3]);
  (v_input3 = ScalarArrays::sa_[3]);
  (v_input4 = ScalarArrays::sa_[3]);
  print("input:\n");
  LINE(53,x_var_dump(1, v_input));
  LINE(55,x_asort(ref(v_input2), toInt32(2LL) /* SORT_STRING */));
  print("asort string:\n");
  LINE(57,x_var_dump(1, v_input2));
  LINE(58,x_asort(ref(v_input3), toInt32(0LL) /* SORT_REGULAR */));
  print("asort regular:\n");
  LINE(60,x_var_dump(1, v_input3));
  LINE(61,x_asort(ref(v_input4), toInt32(1LL) /* SORT_NUMERIC */));
  print("asort numeric:\n");
  LINE(63,x_var_dump(1, v_input4));
  print("ksort:\n");
  (v_fruits = ScalarArrays::sa_[4]);
  LINE(69,x_ksort(ref(v_fruits)));
  LINE(70,x_reset(ref(v_fruits)));
  LOOP_COUNTER(1);
  {
    while (toBoolean(df_lambda_1(LINE(71,x_each(ref(v_fruits))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(72,concat4(toString(v_key), " = ", toString(v_val), "\n")));
      }
    }
  }
  (v_fruits = ScalarArrays::sa_[4]);
  LINE(75,x_krsort(ref(v_fruits)));
  LINE(76,x_reset(ref(v_fruits)));
  LOOP_COUNTER(2);
  {
    while (toBoolean(df_lambda_2(LINE(77,x_each(ref(v_fruits))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(2);
      {
        echo(LINE(78,concat4(toString(v_key), " = ", toString(v_val), "\n")));
      }
    }
  }
  (v_a = ScalarArrays::sa_[5]);
  LINE(92,x_usort(ref(v_a), "cmp"));
  LOOP_COUNTER(3);
  {
    while (toBoolean(df_lambda_3(LINE(93,x_each(ref(v_a))), v_key, v_value))) {
      LOOP_COUNTER_CHECK(3);
      {
        echo(LINE(94,concat4(toString(v_key), ": ", toString(v_value), "\n")));
      }
    }
  }
  (v_a2 = ScalarArrays::sa_[6]);
  LINE(99,x_usort(ref(v_a2), "cmp"));
  LOOP_COUNTER(4);
  {
    while (toBoolean(df_lambda_4(LINE(100,x_each(ref(v_a2))), v_key, v_value))) {
      LOOP_COUNTER_CHECK(4);
      {
        echo(LINE(101,concat4(toString(v_key), ": ", toString(v_value), "\n")));
      }
    }
  }
  (v_b = ScalarArrays::sa_[7]);
  LINE(106,x_uksort(ref(v_b), "cmp"));
  LOOP_COUNTER(5);
  {
    while (toBoolean(df_lambda_5(LINE(107,x_each(ref(v_b))), v_key, v_value))) {
      LOOP_COUNTER_CHECK(5);
      {
        echo(LINE(108,concat4(toString(v_key), ": ", toString(v_value), "\n")));
      }
    }
  }
  (v_fruits = ScalarArrays::sa_[4]);
  LINE(113,x_asort(ref(v_fruits)));
  LINE(114,x_reset(ref(v_fruits)));
  LOOP_COUNTER(6);
  {
    while (toBoolean(df_lambda_6(LINE(115,x_each(ref(v_fruits))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(6);
      {
        echo(LINE(116,concat4(toString(v_key), " = ", toString(v_val), "\n")));
      }
    }
  }
  (v_fruits = ScalarArrays::sa_[4]);
  LINE(120,x_arsort(ref(v_fruits)));
  LINE(121,x_reset(ref(v_fruits)));
  LOOP_COUNTER(7);
  {
    while (toBoolean(df_lambda_7(LINE(122,x_each(ref(v_fruits))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(7);
      {
        echo(LINE(123,concat4(toString(v_key), " = ", toString(v_val), "\n")));
      }
    }
  }
  (v_fruits = ScalarArrays::sa_[4]);
  LINE(128,x_sort(ref(v_fruits)));
  LINE(129,x_reset(ref(v_fruits)));
  LOOP_COUNTER(8);
  {
    while (toBoolean(df_lambda_8(LINE(130,x_each(ref(v_fruits))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(8);
      {
        echo(LINE(131,concat4(toString(v_key), " = ", toString(v_val), "\n")));
      }
    }
  }
  (v_fruits = ScalarArrays::sa_[4]);
  LINE(134,x_rsort(ref(v_fruits)));
  LINE(135,x_reset(ref(v_fruits)));
  LOOP_COUNTER(9);
  {
    while (toBoolean(df_lambda_9(LINE(136,x_each(ref(v_fruits))), v_key, v_val))) {
      LOOP_COUNTER_CHECK(9);
      {
        echo(LINE(137,concat4(toString(v_key), " = ", toString(v_val), "\n")));
      }
    }
  }
  (v_input = ScalarArrays::sa_[2]);
  (v_input2 = ScalarArrays::sa_[2]);
  (v_input3 = ScalarArrays::sa_[2]);
  (v_input4 = ScalarArrays::sa_[2]);
  print("input:\n");
  LINE(145,x_var_dump(1, v_input));
  LINE(147,x_sort(ref(v_input2), toInt32(2LL) /* SORT_STRING */));
  print("sort string:\n");
  LINE(149,x_var_dump(1, v_input2));
  LINE(150,x_sort(ref(v_input3), toInt32(0LL) /* SORT_REGULAR */));
  print("sort regular:\n");
  LINE(152,x_var_dump(1, v_input3));
  LINE(153,x_sort(ref(v_input4), toInt32(1LL) /* SORT_NUMERIC */));
  print("sort numeric:\n");
  LINE(155,x_var_dump(1, v_input4));
  (v_array1 = ScalarArrays::sa_[8]);
  (v_array2 = ScalarArrays::sa_[8]);
  (v_array3 = ScalarArrays::sa_[8]);
  LINE(162,x_sort(ref(v_array1)));
  echo("Standard sorting\n");
  {
    LOOP_COUNTER(10);
    for (ArrayIterPtr iter12 = v_array1.begin(); !iter12->end(); iter12->next()) {
      LOOP_COUNTER_CHECK(10);
      v_img = iter12->second();
      {
        echo(toString(v_img) + toString("\n"));
      }
    }
  }
  LINE(168,x_natsort(ref(v_array2)));
  echo("\nNatural order sorting\n");
  {
    LOOP_COUNTER(13);
    for (ArrayIterPtr iter15 = v_array2.begin(); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_img = iter15->second();
      {
        echo(toString(v_img) + toString("\n"));
      }
    }
  }
  LINE(174,x_natcasesort(ref(v_array3)));
  echo("\nNatural case order sorting\n");
  {
    LOOP_COUNTER(16);
    for (ArrayIterPtr iter18 = v_array3.begin(); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_img = iter18->second();
      {
        echo(toString(v_img) + toString("\n"));
      }
    }
  }
  (v_array3 = ScalarArrays::sa_[9]);
  LINE(181,x_natsort(ref(v_array3)));
  echo("\nNatural order sorting\n");
  LINE(183,x_print_r(v_array3));
  (v_wtfarray = ScalarArrays::sa_[10]);
  LINE(186,x_natcasesort(ref(v_wtfarray)));
  echo("\nNatural case order sorting\n");
  {
    LOOP_COUNTER(19);
    for (ArrayIterPtr iter21 = v_wtfarray.begin(); !iter21->end(); iter21->next()) {
      LOOP_COUNTER_CHECK(19);
      v_img = iter21->second();
      {
        echo(toString(v_img) + toString("\n"));
      }
    }
  }
  (v_b = ScalarArrays::sa_[11]);
  LINE(194,x_uasort(ref(v_b), "cmp"));
  LOOP_COUNTER(22);
  {
    while (toBoolean(df_lambda_10(LINE(195,x_each(ref(v_b))), v_key, v_value))) {
      LOOP_COUNTER_CHECK(22);
      {
        echo(LINE(196,concat4(toString(v_key), ": ", toString(v_value), "\n")));
      }
    }
  }
  (v_b2 = ScalarArrays::sa_[12]);
  LINE(206,x_uasort(ref(v_b2), "cmp"));
  LOOP_COUNTER(23);
  {
    while (toBoolean(df_lambda_11(LINE(207,x_each(ref(v_b2))), v_key, v_value))) {
      LOOP_COUNTER_CHECK(23);
      {
        echo(LINE(208,concat4(toString(v_key), ": ", toString(v_value), "\n")));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
