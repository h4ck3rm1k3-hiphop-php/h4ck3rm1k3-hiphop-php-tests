
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_RETURN(0x35D32B9EF30B4340LL, g->GV(fruits),
                  fruits);
      break;
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 2:
      HASH_RETURN(0x4056C2E766E0B782LL, g->GV(val),
                  val);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 7:
      HASH_RETURN(0x612DD31212E90587LL, g->GV(key),
                  key);
      break;
    case 8:
      HASH_RETURN(0x22012958B0EDE8C8LL, g->GV(input3),
                  input3);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x301931AB31B0F289LL, g->GV(input4),
                  input4);
      break;
    case 10:
      HASH_RETURN(0x4292CEE227B9150ALL, g->GV(a),
                  a);
      break;
    case 11:
      HASH_RETURN(0x1532499416EFFACBLL, g->GV(input),
                  input);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 21:
      HASH_RETURN(0x08FBB133F8576BD5LL, g->GV(b),
                  b);
      break;
    case 24:
      HASH_RETURN(0x2D5185583EF85E98LL, g->GV(b2),
                  b2);
      break;
    case 29:
      HASH_RETURN(0x01EC08C71128761DLL, g->GV(input2),
                  input2);
      break;
    case 34:
      HASH_RETURN(0x1D05EC09E2BD4C22LL, g->GV(img),
                  img);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 49:
      HASH_RETURN(0x69E7413AE0C88471LL, g->GV(value),
                  value);
      break;
    case 53:
      HASH_RETURN(0x331353645577AFF5LL, g->GV(array1),
                  array1);
      break;
    case 54:
      HASH_RETURN(0x6C05C2858C72A876LL, g->GV(a2),
                  a2);
      HASH_RETURN(0x1AF5392A9D424176LL, g->GV(array3),
                  array3);
      break;
    case 57:
      HASH_RETURN(0x3D4B408A972F19F9LL, g->GV(array2),
                  array2);
      break;
    case 60:
      HASH_RETURN(0x2F9A5EEE8A58063CLL, g->GV(wtfarray),
                  wtfarray);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
