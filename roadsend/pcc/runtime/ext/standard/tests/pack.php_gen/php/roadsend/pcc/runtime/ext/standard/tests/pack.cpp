
#include <php/roadsend/pcc/runtime/ext/standard/tests/pack.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/pack.php line 12 */
void f_tp(CStrRef v_fmt, double v_val) {
  FUNCTION_INJECTION(tp);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  int64 &sv_index __attribute__((__unused__)) = g->sv_tp_DupIdindex;
  bool &inited_sv_index __attribute__((__unused__)) = g->inited_sv_tp_DupIdindex;
  Variant v_v;

  if (!inited_sv_index) {
    (sv_index = 1LL);
    inited_sv_index = true;
  }
  echo(LINE(14,(assignCallTemp(eo_1, toString(sv_index)),assignCallTemp(eo_3, x_sprintf(3, "%d | 0x%02x\n", Array(ArrayInit(2).set(0, v_val).set(1, v_val).create()))),concat4("[", eo_1, "]: orig  : ", eo_3))));
  (v_v = LINE(15,x_pack(2, v_fmt, Array(ArrayInit(1).set(0, v_val).create()))));
  echo(LINE(16,(assignCallTemp(eo_1, toString(sv_index)),assignCallTemp(eo_3, f_hexdump(v_v)),concat4("[", eo_1, "]: pack  : ", eo_3))));
  (v_v = LINE(17,x_unpack(v_fmt, toString(v_v))));
  echo(LINE(18,(assignCallTemp(eo_1, toString(sv_index)),assignCallTemp(eo_3, x_sprintf(3, "%d | 0x%02x\n", Array(ArrayInit(2).set(0, v_v.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).set(1, v_v.rvalAt(1LL, 0x5BCA7C69B794F8CELL)).create()))),concat4("[", eo_1, "]: unpack: ", eo_3))));
  echo("-----------------------\n");
  sv_index++;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/pack.php line 3 */
String f_hexdump(CVarRef v_a) {
  FUNCTION_INJECTION(hexdump);
  Variant eo_0;
  Variant eo_1;
  String v_s;
  int64 v_i = 0;

  (v_s = "");
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, LINE(5,x_strlen(toString(v_a)))); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_s, LINE(6,(assignCallTemp(eo_1, x_ord(toString(v_a.rvalAt(v_i)))),x_sprintf(2, "[%02x] ", Array(ArrayInit(1).set(0, eo_1).create())))));
      }
    }
  }
  return concat(v_s, "\n");
} /* function */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$pack_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/pack.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$pack_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_header __attribute__((__unused__)) = (variables != gVariables) ? variables->get("header") : g->GV(header);
  Variant &v_u __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u") : g->GV(u);

  LINE(24,f_tp("N", toDouble(-1LL)));
  LINE(25,f_tp("N", toDouble(65534LL)));
  LINE(26,f_tp("N", toDouble(0LL)));
  LINE(27,f_tp("N", 2147483650.0));
  LINE(28,f_tp("N", toDouble(4294967296LL)));
  LINE(29,f_tp("N", -2147483648.0));
  LINE(30,f_tp("N", toDouble(-30000LL)));
  LINE(33,f_tp("V", toDouble(65534LL)));
  LINE(34,f_tp("V", toDouble(0LL)));
  LINE(35,f_tp("V", 2147483650.0));
  LINE(36,f_tp("V", toDouble(4294967296LL)));
  LINE(37,f_tp("V", -2147483648.0));
  LINE(40,f_tp("v", toDouble(65534LL)));
  LINE(41,f_tp("v", toDouble(65537LL)));
  LINE(42,f_tp("v", toDouble(0LL)));
  LINE(43,f_tp("v", toDouble(-1000LL)));
  LINE(44,f_tp("v", toDouble(-64434LL)));
  LINE(45,f_tp("v", toDouble(-65535LL)));
  LINE(48,f_tp("C", toDouble(-127LL)));
  LINE(49,f_tp("C", toDouble(127LL)));
  LINE(50,f_tp("C", toDouble(255LL)));
  LINE(51,f_tp("C", toDouble(-129LL)));
  LINE(54,f_tp("c", toDouble(-127LL)));
  LINE(55,f_tp("c", toDouble(127LL)));
  LINE(56,f_tp("c", toDouble(255LL)));
  LINE(57,f_tp("c", toDouble(-129LL)));
  LINE(60,f_tp("L", toDouble(65534LL)));
  LINE(61,f_tp("L", toDouble(0LL)));
  LINE(62,f_tp("L", 2147483650.0));
  LINE(63,f_tp("L", 4294967295.0));
  LINE(64,f_tp("L", -2147483648.0));
  LINE(67,f_tp("l", toDouble(65534LL)));
  LINE(68,f_tp("l", toDouble(0LL)));
  LINE(69,f_tp("l", 2147483650.0));
  LINE(70,f_tp("l", 4294967295.0));
  LINE(71,f_tp("l", -2147483648.0));
  LINE(74,f_tp("I", toDouble(65534LL)));
  LINE(75,f_tp("I", toDouble(0LL)));
  LINE(76,f_tp("I", toDouble(-1000LL)));
  LINE(77,f_tp("I", toDouble(-64434LL)));
  LINE(78,f_tp("I", toDouble(4294967296LL)));
  LINE(79,f_tp("I", toDouble(-4294967296LL)));
  LINE(82,f_tp("i", toDouble(65534LL)));
  LINE(83,f_tp("i", toDouble(0LL)));
  LINE(84,f_tp("i", toDouble(-1000LL)));
  LINE(85,f_tp("i", toDouble(-64434LL)));
  LINE(86,f_tp("i", toDouble(4294967296LL)));
  LINE(87,f_tp("i", toDouble(-4294967296LL)));
  LINE(90,f_tp("S", toDouble(65534LL)));
  LINE(91,f_tp("S", toDouble(65537LL)));
  LINE(92,f_tp("S", toDouble(0LL)));
  LINE(93,f_tp("S", toDouble(-1000LL)));
  LINE(94,f_tp("S", toDouble(-64434LL)));
  LINE(95,f_tp("S", toDouble(-65535LL)));
  LINE(98,f_tp("s", toDouble(65534LL)));
  LINE(99,f_tp("s", toDouble(65537LL)));
  LINE(100,f_tp("s", toDouble(0LL)));
  LINE(101,f_tp("s", toDouble(-1000LL)));
  LINE(102,f_tp("s", toDouble(-64434LL)));
  LINE(103,f_tp("s", toDouble(-65535LL)));
  LINE(106,f_tp("n", toDouble(65534LL)));
  LINE(107,f_tp("n", toDouble(65537LL)));
  LINE(108,f_tp("n", toDouble(0LL)));
  LINE(109,f_tp("n", toDouble(-1000LL)));
  LINE(110,f_tp("n", toDouble(-64434LL)));
  LINE(111,f_tp("n", toDouble(-65535LL)));
  {
    (v_fp = LINE(135,x_fopen("/bin/sh", "r")));
    if (toBoolean(v_fp)) {
      (v_header = LINE(137,x_fread(toObject(v_fp), 9LL)));
      (v_u = LINE(138,x_unpack("C*", toString(v_header))));
      LINE(139,x_var_dump(1, v_u));
      {
        Variant tmp3 = (v_u.rvalAt(5LL, 0x350AEB726A15D700LL));
        int tmp4 = -1;
        if (equal(tmp3, ("1"))) {
          tmp4 = 0;
        } else if (equal(tmp3, ("2"))) {
          tmp4 = 1;
        } else if (true) {
          tmp4 = 2;
        }
        switch (tmp4) {
        case 0:
          {
            echo("32 bit\n");
            goto break2;
          }
        case 1:
          {
            echo("64 bit\n");
            goto break2;
          }
        case 2:
          {
            echo("invalid ELF header\?\n");
            goto break2;
          }
        }
        break2:;
      }
      LINE(151,x_fclose(toObject(v_fp)));
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
