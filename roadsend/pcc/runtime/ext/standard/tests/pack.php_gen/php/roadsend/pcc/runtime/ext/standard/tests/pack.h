
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_pack_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_pack_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/pack.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_tp(CStrRef v_fmt, double v_val);
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$pack_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_hexdump(CVarRef v_a);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_pack_h__
