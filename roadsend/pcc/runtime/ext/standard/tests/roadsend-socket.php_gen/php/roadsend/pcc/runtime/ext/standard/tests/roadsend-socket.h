
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_roadsend_socket_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_roadsend_socket_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/roadsend-socket.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$roadsend_socket_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_roadsend_socket_h__
