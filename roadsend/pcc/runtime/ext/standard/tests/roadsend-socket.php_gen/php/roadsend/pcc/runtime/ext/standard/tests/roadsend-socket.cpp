
#include <php/roadsend/pcc/runtime/ext/standard/tests/roadsend-socket.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$roadsend_socket_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/roadsend-socket.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$roadsend_socket_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_errno __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errno") : g->GV(errno);
  Variant &v_errstr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errstr") : g->GV(errstr);
  Variant &v_fsock __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fsock") : g->GV(fsock);
  Variant &v_google __attribute__((__unused__)) = (variables != gVariables) ? variables->get("google") : g->GV(google);

  (v_fsock = LINE(3,x_fsockopen("www.roadsend.com", toInt32(80LL), ref(v_errno), ref(v_errstr), toDouble(10LL))));
  LINE(6,x_fputs(toObject(v_fsock), "GET /updatecheck/20x.txt HTTP/1.1\r\n"));
  LINE(7,x_fputs(toObject(v_fsock), "HOST: www.phpbb.com\r\n"));
  LINE(8,x_fputs(toObject(v_fsock), "Connection: close\r\n\r\n"));
  LOOP_COUNTER(1);
  {
    while (!(LINE(10,x_feof(toObject(v_fsock))))) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_google, toString(LINE(11,x_fread(toObject(v_fsock), 1024LL))));
        print(LINE(13,(assignCallTemp(eo_1, toString(x_strlen(toString(v_google)))),concat3("loop ", eo_1, "\n"))));
      }
    }
  }
  print(toString(LINE(15,x_strlen(toString(v_google)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
