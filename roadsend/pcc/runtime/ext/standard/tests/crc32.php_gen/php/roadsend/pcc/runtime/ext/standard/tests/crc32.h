
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_crc32_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_crc32_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/crc32.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$crc32_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_crc32_h__
