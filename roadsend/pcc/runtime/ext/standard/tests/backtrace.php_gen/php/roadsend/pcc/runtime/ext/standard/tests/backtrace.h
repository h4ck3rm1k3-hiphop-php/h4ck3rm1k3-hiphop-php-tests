
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_backtrace_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_backtrace_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/backtrace.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_dumpbt(CArrRef v_bt);
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$backtrace_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_a(CStrRef v_b);
void f_b(CStrRef v_b, CStrRef v_c);
void f_c();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_backtrace_h__
