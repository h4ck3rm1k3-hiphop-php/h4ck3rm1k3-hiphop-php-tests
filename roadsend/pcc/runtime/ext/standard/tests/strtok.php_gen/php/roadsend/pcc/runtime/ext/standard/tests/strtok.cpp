
#include <php/roadsend/pcc/runtime/ext/standard/tests/strtok.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$strtok_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/strtok.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$strtok_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_tok __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tok") : g->GV(tok);
  Variant &v_first_token __attribute__((__unused__)) = (variables != gVariables) ? variables->get("first_token") : g->GV(first_token);
  Variant &v_second_token __attribute__((__unused__)) = (variables != gVariables) ? variables->get("second_token") : g->GV(second_token);

  (v_string = "This is\tan example\nstring");
  (v_tok = LINE(5,x_strtok(toString(v_string), " \n\t")));
  LOOP_COUNTER(1);
  {
    while (toBoolean(v_tok)) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(LINE(7,concat3("Word=", toString(v_tok), "<br>")));
        (v_tok = LINE(8,x_strtok(" \n\t")));
      }
    }
  }
  (v_first_token = LINE(12,x_strtok("/something", "/")));
  (v_second_token = LINE(13,x_strtok("/")));
  LINE(14,x_var_dump(2, v_first_token, Array(ArrayInit(1).set(0, v_second_token).create())));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
