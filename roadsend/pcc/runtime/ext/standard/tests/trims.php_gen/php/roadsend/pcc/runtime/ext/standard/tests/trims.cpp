
#include <php/roadsend/pcc/runtime/ext/standard/tests/trims.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/trims.php line 3 */
void f_exercise_trims(CVarRef v_string, CVarRef v_chars) {
  FUNCTION_INJECTION(exercise_trims);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_strings;

  echo(LINE(5,x_trim(toString(v_string))));
  echo(x_ltrim(toString(v_string)));
  echo(x_rtrim(toString(v_string)));
  echo(x_chop(toString(v_string)));
  echo("\n\n");
  echo(LINE(6,x_trim(toString(v_string), toString(v_chars))));
  echo(x_ltrim(toString(v_string), toString(v_chars)));
  echo(x_rtrim(toString(v_string), toString(v_chars)));
  echo(x_chop(toString(v_string), toString(v_chars)));
  echo("\n\n");
  echo(LINE(7,x_trim(toString(v_string), "")));
  echo(x_ltrim(toString(v_string), ""));
  echo(x_rtrim(toString(v_string), ""));
  echo(x_chop(toString(v_string), ""));
  echo("\n\n");
  echo("\n\n");
  echo(concat(toString(v_string), toString(v_strings)));
  echo(concat(toString(v_string), toString(v_strings)));
  echo(concat(toString(v_string), toString(v_strings)));
  echo(concat(toString(v_string), toString(v_strings)));
  echo("\n\n");
  echo(LINE(12,(assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_trim(toString(v_chars))),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2))));
  echo((assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_ltrim(toString(v_chars))),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2)));
  echo((assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_rtrim(toString(v_chars))),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2)));
  echo((assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_chop(toString(v_chars))),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2)));
  echo("\n\n");
  echo(LINE(13,(assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_trim("")),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2))));
  echo((assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_ltrim("")),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2)));
  echo((assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_rtrim("")),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2)));
  echo((assignCallTemp(eo_0, toString(v_string)),assignCallTemp(eo_1, x_chop("")),assignCallTemp(eo_2, toString(v_strings)),concat3(eo_0, eo_1, eo_2)));
  echo("\n\n");
  echo("\n\n");
} /* function */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$trims_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/trims.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$trims_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_strings __attribute__((__unused__)) = (variables != gVariables) ? variables->get("strings") : g->GV(strings);
  Variant &v_charses __attribute__((__unused__)) = (variables != gVariables) ? variables->get("charses") : g->GV(charses);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);
  Variant &v_string __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string") : g->GV(string);
  Variant &v_j __attribute__((__unused__)) = (variables != gVariables) ? variables->get("j") : g->GV(j);
  Variant &v_chars __attribute__((__unused__)) = (variables != gVariables) ? variables->get("chars") : g->GV(chars);

  (v_strings = ScalarArrays::sa_[0]);
  (v_charses = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_strings.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_string = iter3->second();
      v_i = iter3->first();
      {
        {
          LOOP_COUNTER(4);
          for (ArrayIterPtr iter6 = v_charses.begin(); !iter6->end(); iter6->next()) {
            LOOP_COUNTER_CHECK(4);
            v_chars = iter6->second();
            v_j = iter6->first();
            {
              LINE(53,f_exercise_trims(v_string, v_chars));
            }
          }
        }
      }
    }
  }
  echo("\n\n");
  LINE(55,f_exercise_trims("ABC\\x50\\xC1\\x60\\x90", "asvs \\0e..s..d..r..e..a....;elkj"));
  echo("\n\n");
  echo("\n\n");
  LINE(57,f_exercise_trims("\\xC1", "asvs \\0e..s..d..r..e..a....;elkj"));
  echo("\n\n");
  echo("\n\n");
  LINE(59,f_exercise_trims("\\xC1", "....;"));
  echo("\n\n");
  echo("\n\n");
  LINE(61,f_exercise_trims("\\xC1", "....;"));
  echo("\n\n");
  echo("\n\n");
  echo(LINE(63,x_rtrim("\\xC1", "....;")));
  echo("\n\n");
  echo("\n\n");
  echo(LINE(64,x_rtrim("\\xC1", ".;")));
  echo("\n\n");
  echo("\n\n");
  echo(LINE(65,x_rtrim("\301", "....;")));
  echo("\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
