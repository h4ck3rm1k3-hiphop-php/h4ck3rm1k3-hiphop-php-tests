
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_trims_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_trims_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/trims.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_exercise_trims(CVarRef v_string, CVarRef v_chars);
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$trims_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_trims_h__
