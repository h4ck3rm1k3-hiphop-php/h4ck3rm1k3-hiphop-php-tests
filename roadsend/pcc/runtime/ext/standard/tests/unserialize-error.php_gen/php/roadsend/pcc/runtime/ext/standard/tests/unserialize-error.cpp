
#include <php/roadsend/pcc/runtime/ext/standard/tests/unserialize-error.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/unserialize-error.php line 4 */
Variant f_maybe_unserialize(CStrRef v_original) {
  FUNCTION_INJECTION(maybe_unserialize);
  Variant v_gm;

  if (!same(false, (v_gm = (silenceInc(), silenceDec(LINE(5,x_unserialize(v_original))))))) return v_gm;
  else return v_original;
  return null;
} /* function */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$unserialize_error_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/unserialize-error.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$unserialize_error_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("check that unserialize doesn't produce a fatal error when encountering bad data\n");
  LINE(11,x_var_dump(1, concat("value ", toString(f_maybe_unserialize("http://hummer/blog")))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
