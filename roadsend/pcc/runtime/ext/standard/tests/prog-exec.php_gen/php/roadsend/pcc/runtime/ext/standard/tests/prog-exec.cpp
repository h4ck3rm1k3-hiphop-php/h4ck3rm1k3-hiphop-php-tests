
#include <php/roadsend/pcc/runtime/ext/standard/tests/prog-exec.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$prog_exec_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/prog-exec.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$prog_exec_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ret __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ret") : g->GV(ret);
  Variant &v_output __attribute__((__unused__)) = (variables != gVariables) ? variables->get("output") : g->GV(output);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_cmd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("cmd") : g->GV(cmd);

  echo(concat("system 1: ", LINE(4,x_system("ls /etc/"))));
  echo(concat("system 2: ", LINE(5,x_system("ls /etc/", ref(v_ret)))));
  echo(LINE(6,concat3("return: ", toString(v_ret), "\n")));
  echo(concat("system 3: ", LINE(7,x_system("ls /this-does-not-exist/", ref(v_ret)))));
  echo(LINE(8,concat3("return: ", toString(v_ret), "\n")));
  echo(concat("passthru 1: ", (LINE(10,x_passthru("ls /etc/", ref(v_ret))), null)));
  echo(LINE(11,concat3("return: ", toString(v_ret), "\n")));
  echo(LINE(13,(assignCallTemp(eo_1, x_exec("ls /etc/")),concat3("exec ---> ", eo_1, "<-----\n"))));
  echo(LINE(14,x_exec("ls /etc/", ref(v_output))));
  LINE(15,x_var_dump(1, v_output));
  (v_output = ScalarArrays::sa_[0]);
  echo(LINE(17,x_exec("ls /etc/", ref(v_output))));
  LINE(18,x_var_dump(1, v_output));
  echo(LINE(19,x_exec("ls /etc/", ref(v_output), ref(v_ret))));
  LINE(20,x_var_dump(1, v_output));
  echo(LINE(21,concat3("return: ", toString(v_ret), "\n")));
  unset(v_output);
  echo(LINE(24,x_exec("fnord", ref(v_output), ref(v_ret))));
  LINE(25,x_var_dump(1, v_output));
  echo(LINE(26,concat3("exec return: ", toString(v_ret), "\n")));
  echo(LINE(28,x_system("fnord", ref(v_r))));
  LINE(29,x_var_dump(1, v_r));
  echo(concat(LINE(31,x_escapeshellarg("%\"''/\\b'la''h\\''")), "\n"));
  (v_cmd = "$this\" {}'!@#$%^&*(){}[]<>/\\n\\n\\t;;`\\ffabcdefghijklmnopqrstuvwxyz");
  echo(concat(LINE(34,x_escapeshellcmd(toString(v_cmd))), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
