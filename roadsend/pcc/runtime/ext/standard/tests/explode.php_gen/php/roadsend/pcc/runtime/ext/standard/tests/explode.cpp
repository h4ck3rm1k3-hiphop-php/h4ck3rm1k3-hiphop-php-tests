
#include <php/roadsend/pcc/runtime/ext/standard/tests/explode.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$explode_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/explode.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$explode_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo("1\n");
  LINE(3,x_var_dump(1, x_explode("foo", "barfoobarfoobar")));
  echo("2\n");
  LINE(5,x_var_dump(1, x_explode("barfoobarfoobar", "foo")));
  echo("3\n");
  LINE(7,x_var_dump(1, x_explode("foo", "foo")));
  echo("4\n");
  LINE(9,x_var_dump(1, x_explode("foo", "foo", toInt32(0LL))));
  echo("5\n");
  LINE(11,x_var_dump(1, x_explode("foo", "barFOOBARFOOBAR")));
  echo("6\n");
  LINE(13,x_var_dump(1, x_explode("foo", "barfoobarfoobar", toInt32(1LL))));
  echo("7\n");
  LINE(15,x_var_dump(1, x_explode("foo", "barfoobarfoobar", toInt32(2LL))));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
