
#include <php/roadsend/pcc/runtime/ext/standard/tests/variables.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/variables.php line 110 */
Variant c_uclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_uclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_uclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  props.push_back(NEW(ArrayElement)("b", m_b.isReferenced() ? ref(m_b) : m_b));
  c_ObjectData::o_get(props);
}
bool c_uclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x08FBB133F8576BD5LL, b, 1);
      break;
    case 2:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_uclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_uclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x08FBB133F8576BD5LL, m_b,
                      b, 1);
      break;
    case 2:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_uclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x08FBB133F8576BD5LL, m_b,
                         b, 1);
      break;
    case 2:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_uclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(uclass)
ObjectData *c_uclass::create() {
  init();
  t_uclass();
  return this;
}
ObjectData *c_uclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_uclass::cloneImpl() {
  c_uclass *obj = NEW(c_uclass)();
  cloneSet(obj);
  return obj;
}
void c_uclass::cloneSet(c_uclass *clone) {
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  clone->m_b = m_b.isReferenced() ? ref(m_b) : m_b;
  ObjectData::cloneSet(clone);
}
Variant c_uclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_uclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_uclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_uclass$os_get(const char *s) {
  return c_uclass::os_get(s, -1);
}
Variant &cw_uclass$os_lval(const char *s) {
  return c_uclass::os_lval(s, -1);
}
Variant cw_uclass$os_constant(const char *s) {
  return c_uclass::os_constant(s);
}
Variant cw_uclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_uclass::os_invoke(c, s, params, -1, fatal);
}
void c_uclass::init() {
  m_a = null;
  m_b = null;
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/variables.php line 113 */
void c_uclass::t_uclass() {
  INSTANCE_METHOD_INJECTION(uclass, uclass::uclass);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_a __attribute__((__unused__)) = g->GV(a);
  (m_b = ref(gv_a));
  (m_a = ScalarArrays::sa_[2]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/variables.php line 29 */
Variant c_tclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_tclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_tclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("a", m_a.isReferenced() ? ref(m_a) : m_a));
  c_ObjectData::o_get(props);
}
bool c_tclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4292CEE227B9150ALL, a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_tclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_tclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4292CEE227B9150ALL, m_a,
                      a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_tclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4292CEE227B9150ALL, m_a,
                         a, 1);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_tclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(tclass)
ObjectData *c_tclass::create() {
  init();
  t_tclass();
  return this;
}
ObjectData *c_tclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_tclass::cloneImpl() {
  c_tclass *obj = NEW(c_tclass)();
  cloneSet(obj);
  return obj;
}
void c_tclass::cloneSet(c_tclass *clone) {
  clone->m_a = m_a.isReferenced() ? ref(m_a) : m_a;
  ObjectData::cloneSet(clone);
}
Variant c_tclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_tclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_tclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_tclass$os_get(const char *s) {
  return c_tclass::os_get(s, -1);
}
Variant &cw_tclass$os_lval(const char *s) {
  return c_tclass::os_lval(s, -1);
}
Variant cw_tclass$os_constant(const char *s) {
  return c_tclass::os_constant(s);
}
Variant cw_tclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_tclass::os_invoke(c, s, params, -1, fatal);
}
void c_tclass::init() {
  m_a = null;
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/variables.php line 31 */
String c_tclass::t___tostring() {
  INSTANCE_METHOD_INJECTION(tclass, tclass::__toString);
  return "[class tclass]";
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/variables.php line 34 */
void c_tclass::t_tclass() {
  INSTANCE_METHOD_INJECTION(tclass, tclass::tclass);
  bool oldInCtor = gasInCtor(true);
  (m_a = ScalarArrays::sa_[2]);
  gasInCtor(oldInCtor);
} /* function */
Object co_uclass(CArrRef params, bool init /* = true */) {
  return Object(p_uclass(NEW(c_uclass)())->dynCreate(params, init));
}
Object co_tclass(CArrRef params, bool init /* = true */) {
  return Object(p_tclass(NEW(c_tclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$variables_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/variables.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$variables_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);
  Variant &v_testarr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("testarr") : g->GV(testarr);
  Variant &v_multi __attribute__((__unused__)) = (variables != gVariables) ? variables->get("multi") : g->GV(multi);
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_a2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a2") : g->GV(a2);
  Variant &v_lastkey __attribute__((__unused__)) = (variables != gVariables) ? variables->get("lastkey") : g->GV(lastkey);
  Variant &v_t __attribute__((__unused__)) = (variables != gVariables) ? variables->get("t") : g->GV(t);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);
  Variant &v_z __attribute__((__unused__)) = (variables != gVariables) ? variables->get("z") : g->GV(z);
  Variant &v_x __attribute__((__unused__)) = (variables != gVariables) ? variables->get("x") : g->GV(x);
  Variant &v_y __attribute__((__unused__)) = (variables != gVariables) ? variables->get("y") : g->GV(y);
  Variant &v_w __attribute__((__unused__)) = (variables != gVariables) ? variables->get("w") : g->GV(w);
  Variant &v_u __attribute__((__unused__)) = (variables != gVariables) ? variables->get("u") : g->GV(u);
  Variant &v_b __attribute__((__unused__)) = (variables != gVariables) ? variables->get("b") : g->GV(b);
  Variant &v_obja __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obja") : g->GV(obja);
  Variant &v_objb __attribute__((__unused__)) = (variables != gVariables) ? variables->get("objb") : g->GV(objb);

  (v_a = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_a.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      {
        echo(LINE(5,concat3("empty: [", toString(empty(v_v)), "]\n")));
      }
    }
  }
  print("trying print_r on a recursive array\n");
  (v_testarr = ScalarArrays::sa_[1]);
  v_testarr.set("baz", (v_testarr), 0x5BE8348359F05B37LL);
  LINE(12,x_print_r(v_testarr));
  print("trying var_dump on a recursive array\n");
  LINE(17,x_var_dump(1, v_testarr));
  (v_multi = Array(ArrayInit(2).set(0, "key1", v_testarr, 0x655F4C945FA57A70LL).set(1, "key2", v_testarr, 0x227687E59C1F7C8BLL).create()));
  print("trying print_r on a multiply recursive array\n");
  LINE(23,x_print_r(v_multi));
  print("trying var_dump on a multiply recursive array\n");
  LINE(27,x_var_dump(1, v_multi));
  (v_obj = ((Object)(LINE(39,p_tclass(p_tclass(NEWOBJ(c_tclass)())->create())))));
  LINE(42,x_print_r(v_obj));
  (v_s = "some string");
  (v_a2 = Array(ArrayInit(10).set(0, "string", "test", 0x2027864469AD4382LL).set(1, "int", 23LL, 0x37AD4EBE928A1EF0LL).set(2, "float", 1.23, 0x19AB2CA7919BFFE4LL).set(3, "null", null, 0x2FF6F3DFF927FF2CLL).set(4, "double", 3.21, 0x104F6C1CAEA2D57ALL).set(5, "array", ScalarArrays::sa_[3], 0x063D35168C04B960LL).set(6, "object", v_obj, 0x7F30BC4E222B1861LL).setRef(7, "object ref", ref(v_obj), 0x2212244E85CE6806LL).setRef(8, "string ref", ref(v_s), 0x47669AE731C23A14LL).set(9, "boolean", false, 0x2A3C5D9AB36538F4LL).create()));
  LINE(57,x_var_dump(1, v_a2));
  (v_a = ScalarArrays::sa_[4]);
  setNull(v_lastkey);
  {
    LOOP_COUNTER(4);
    for (ArrayIterPtr iter6 = v_a.begin(); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_v = iter6->second();
      v_t = iter6->first();
      {
        echo(LINE(70,(assignCallTemp(eo_0, toString(v_t) + toString(" ===> ")),assignCallTemp(eo_1, x_gettype(v_v)),concat3(eo_0, eo_1, "\n"))));
        echo(toString(LINE(71,x_is_string(v_v))));
        echo(toString(LINE(72,x_is_int(v_v))));
        echo(toString(LINE(73,x_is_integer(v_v))));
        echo(toString(LINE(74,x_is_float(v_v))));
        echo(toString(LINE(75,x_is_null(v_v))));
        echo(toString(LINE(76,x_is_double(v_v))));
        echo(toString(LINE(77,x_is_array(v_v))));
        echo(toString(LINE(78,x_is_object(v_v))));
        echo(toString(LINE(79,x_is_bool(v_v))));
        (v_e = v_v);
        echo(LINE(83,(assignCallTemp(eo_1, toString(v_e)),assignCallTemp(eo_3, x_gettype(v_e)),concat5("e is now ", eo_1, " ===> ", eo_3, "\n"))));
        if (!(empty(v_lastkey))) {
          echo(LINE(85,concat3("changing to ", toString(v_lastkey), "\n")));
          LINE(86,x_settype(ref(v_e), toString(v_lastkey)));
          echo(LINE(87,(assignCallTemp(eo_1, toString(v_e)),assignCallTemp(eo_3, x_gettype(v_e)),concat5("after change, e is now ", eo_1, " ===> ", eo_3, "\n"))));
        }
        (v_lastkey = v_t);
      }
    }
  }
  (v_z = 1LL);
  (v_x = "hi");
  (v_y = true);
  setNull(v_v);
  (v_w = 1.23);
  (v_u = ScalarArrays::sa_[5]);
  (v_a = Array(ArrayInit(6).set(0, v_z).set(1, v_x).set(2, v_y).set(3, v_v).set(4, v_w).set(5, v_u).create()));
  (v_b = Array(ArrayInit(6).setRef(0, ref(v_z)).setRef(1, ref(v_x)).setRef(2, ref(v_y)).setRef(3, ref(v_v)).setRef(4, ref(v_w)).setRef(5, ref(v_u)).create()));
  LINE(103,x_var_dump(1, v_a));
  LINE(104,x_var_dump(1, v_b));
  (v_a = 1LL);
  (v_b = ref(v_a));
  LINE(108,x_var_dump(1, v_b));
  (v_obja = ((Object)(LINE(119,p_uclass(p_uclass(NEWOBJ(c_uclass)())->create())))));
  (v_objb = LINE(120,p_uclass(p_uclass(NEWOBJ(c_uclass)())->create())));
  LINE(121,x_var_dump(1, v_obja));
  LINE(122,x_var_dump(1, v_objb));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
