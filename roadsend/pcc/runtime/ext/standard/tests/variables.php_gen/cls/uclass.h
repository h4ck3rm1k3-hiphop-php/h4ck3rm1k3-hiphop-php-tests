
#ifndef __GENERATED_cls_uclass_h__
#define __GENERATED_cls_uclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/standard/tests/variables.php line 110 */
class c_uclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(uclass)
  END_CLASS_MAP(uclass)
  DECLARE_CLASS(uclass, uclass, ObjectData)
  void init();
  public: Variant m_a;
  public: Variant m_b;
  public: void t_uclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_uclass_h__
