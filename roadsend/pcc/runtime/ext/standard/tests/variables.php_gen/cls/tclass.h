
#ifndef __GENERATED_cls_tclass_h__
#define __GENERATED_cls_tclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/standard/tests/variables.php line 29 */
class c_tclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(tclass)
  END_CLASS_MAP(tclass)
  DECLARE_CLASS(tclass, tclass, ObjectData)
  void init();
  public: Variant m_a;
  public: String t___tostring();
  public: void t_tclass();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_tclass_h__
