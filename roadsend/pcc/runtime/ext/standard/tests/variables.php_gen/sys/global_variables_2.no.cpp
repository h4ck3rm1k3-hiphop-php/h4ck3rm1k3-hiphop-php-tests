
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

bool GlobalVariables::exists(const char *s, int64 hash /* = -1 */) const {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INITIALIZED(0x50645ABB5EE07801LL, g->gv__POST,
                       _POST);
      break;
    case 3:
      HASH_INITIALIZED(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                       HTTP_RAW_POST_DATA);
      HASH_INITIALIZED(0x11A5375142A75D03LL, g->gv_http_response_header,
                       http_response_header);
      HASH_INITIALIZED(0x449338826D29CF03LL, g->GV(multi),
                       multi);
      HASH_INITIALIZED(0x7FB577570F61BD03LL, g->GV(obj),
                       obj);
      break;
    case 6:
      HASH_INITIALIZED(0x10EA7DC57768F8C6LL, g->gv_argv,
                       argv);
      break;
    case 9:
      HASH_INITIALIZED(0x0759FB4517508949LL, g->gv__GET,
                       _GET);
      HASH_INITIALIZED(0x2A98F317E9E2AA49LL, g->GV(u),
                       u);
      break;
    case 10:
      HASH_INITIALIZED(0x4292CEE227B9150ALL, g->GV(a),
                       a);
      HASH_INITIALIZED(0x4F56B733A4DFC78ALL, g->GV(y),
                       y);
      break;
    case 12:
      HASH_INITIALIZED(0x294493CC17F275CCLL, g->GV(objb),
                       objb);
      break;
    case 14:
      HASH_INITIALIZED(0x516FBD36FC674A0ELL, g->gv__FILES,
                       _FILES);
      break;
    case 16:
      HASH_INITIALIZED(0x7320B4E3FF243290LL, g->gv__ENV,
                       _ENV);
      HASH_INITIALIZED(0x0BC2CDE4BBFC9C10LL, g->GV(s),
                       s);
      break;
    case 17:
      HASH_INITIALIZED(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                       _SESSION);
      break;
    case 19:
      HASH_INITIALIZED(0x596A642EB89EED13LL, g->gv_argc,
                       argc);
      break;
    case 21:
      HASH_INITIALIZED(0x08FBB133F8576BD5LL, g->GV(b),
                       b);
      break;
    case 22:
      HASH_INITIALIZED(0x04BFC205E59FA416LL, g->GV(x),
                       x);
      break;
    case 23:
      HASH_INITIALIZED(0x402FA614D9F5B817LL, g->GV(obja),
                       obja);
      break;
    case 35:
      HASH_INITIALIZED(0x3760929554A51DA3LL, g->gv__COOKIE,
                       _COOKIE);
      break;
    case 41:
      HASH_INITIALIZED(0x009CBDB5D52B2AA9LL, g->GV(w),
                       w);
      break;
    case 43:
      HASH_INITIALIZED(0x3C2F961831E4EF6BLL, g->GV(v),
                       v);
      HASH_INITIALIZED(0x61B161496B7EA7EBLL, g->GV(e),
                       e);
      break;
    case 45:
      HASH_INITIALIZED(0x4DCAAC426B2BACADLL, g->GV(testarr),
                       testarr);
      break;
    case 46:
      HASH_INITIALIZED(0x14297F74B68B58EELL, g->gv__SERVER,
                       _SERVER);
      break;
    case 51:
      HASH_INITIALIZED(0x62A103F6518DE2B3LL, g->GV(z),
                       z);
      break;
    case 54:
      HASH_INITIALIZED(0x6C05C2858C72A876LL, g->GV(a2),
                       a2);
      break;
    case 59:
      HASH_INITIALIZED(0x6CFF85DD1D641EFBLL, g->GV(lastkey),
                       lastkey);
      break;
    case 60:
      HASH_INITIALIZED(0x11C9D2391242AEBCLL, g->GV(t),
                       t);
      break;
    case 63:
      HASH_INITIALIZED(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                       _REQUEST);
      break;
    default:
      break;
  }
  return LVariableTable::exists(s, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
