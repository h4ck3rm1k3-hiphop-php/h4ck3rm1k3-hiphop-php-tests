
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      HASH_INDEX(0x449338826D29CF03LL, multi, 15);
      HASH_INDEX(0x7FB577570F61BD03LL, obj, 16);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x2A98F317E9E2AA49LL, u, 26);
      break;
    case 10:
      HASH_INDEX(0x4292CEE227B9150ALL, a, 12);
      HASH_INDEX(0x4F56B733A4DFC78ALL, y, 24);
      break;
    case 12:
      HASH_INDEX(0x294493CC17F275CCLL, objb, 29);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x0BC2CDE4BBFC9C10LL, s, 17);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 21:
      HASH_INDEX(0x08FBB133F8576BD5LL, b, 27);
      break;
    case 22:
      HASH_INDEX(0x04BFC205E59FA416LL, x, 23);
      break;
    case 23:
      HASH_INDEX(0x402FA614D9F5B817LL, obja, 28);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 41:
      HASH_INDEX(0x009CBDB5D52B2AA9LL, w, 25);
      break;
    case 43:
      HASH_INDEX(0x3C2F961831E4EF6BLL, v, 13);
      HASH_INDEX(0x61B161496B7EA7EBLL, e, 21);
      break;
    case 45:
      HASH_INDEX(0x4DCAAC426B2BACADLL, testarr, 14);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 51:
      HASH_INDEX(0x62A103F6518DE2B3LL, z, 22);
      break;
    case 54:
      HASH_INDEX(0x6C05C2858C72A876LL, a2, 18);
      break;
    case 59:
      HASH_INDEX(0x6CFF85DD1D641EFBLL, lastkey, 19);
      break;
    case 60:
      HASH_INDEX(0x11C9D2391242AEBCLL, t, 20);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 30) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
