
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "a",
    "v",
    "testarr",
    "multi",
    "obj",
    "s",
    "a2",
    "lastkey",
    "t",
    "e",
    "z",
    "x",
    "y",
    "w",
    "u",
    "b",
    "obja",
    "objb",
  };
  if (idx >= 0 && idx < 30) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(a);
    case 13: return GV(v);
    case 14: return GV(testarr);
    case 15: return GV(multi);
    case 16: return GV(obj);
    case 17: return GV(s);
    case 18: return GV(a2);
    case 19: return GV(lastkey);
    case 20: return GV(t);
    case 21: return GV(e);
    case 22: return GV(z);
    case 23: return GV(x);
    case 24: return GV(y);
    case 25: return GV(w);
    case 26: return GV(u);
    case 27: return GV(b);
    case 28: return GV(obja);
    case 29: return GV(objb);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
