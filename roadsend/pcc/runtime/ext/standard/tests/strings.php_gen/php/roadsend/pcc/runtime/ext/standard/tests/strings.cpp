
#include <php/roadsend/pcc/runtime/ext/standard/tests/strings.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/strings.php line 172 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("foo", m_foo));
  props.push_back(NEW(ArrayElement)("baz", m_baz));
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_EXISTS_STRING(0x4154FA2EF733DA8FLL, foo, 3);
      HASH_EXISTS_STRING(0x5BE8348359F05B37LL, baz, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_RETURN_STRING(0x4154FA2EF733DA8FLL, m_foo,
                         foo, 3);
      HASH_RETURN_STRING(0x5BE8348359F05B37LL, m_baz,
                         baz, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 3:
      HASH_SET_STRING(0x4154FA2EF733DA8FLL, m_foo,
                      foo, 3);
      HASH_SET_STRING(0x5BE8348359F05B37LL, m_baz,
                      baz, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::create() {
  init();
  t_myclass();
  return this;
}
ObjectData *c_myclass::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  clone->m_foo = m_foo;
  clone->m_baz = m_baz;
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
  m_foo = null;
  m_baz = null;
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/strings.php line 176 */
void c_myclass::t_myclass() {
  INSTANCE_METHOD_INJECTION(myClass, myClass::myClass);
  bool oldInCtor = gasInCtor(true);
  (m_foo = "bar");
  (m_baz = "boom");
  gasInCtor(oldInCtor);
} /* function */
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$strings_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/strings.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$strings_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_string1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string1") : g->GV(string1);
  Variant &v_mix __attribute__((__unused__)) = (variables != gVariables) ? variables->get("mix") : g->GV(mix);
  Variant &v_caps __attribute__((__unused__)) = (variables != gVariables) ? variables->get("caps") : g->GV(caps);
  Variant &v_var1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("var1") : g->GV(var1);
  Variant &v_var2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("var2") : g->GV(var2);
  Variant &v_text __attribute__((__unused__)) = (variables != gVariables) ? variables->get("text") : g->GV(text);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_var __attribute__((__unused__)) = (variables != gVariables) ? variables->get("var") : g->GV(var);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);

  (v_string1 = "dolphin-flogger");
  echo(concat(toString(LINE(5,x_strlen(toString(v_string1)))), "\n"));
  (v_mix = "xAnDu-LaMbAsTIc\n");
  echo(LINE(8,x_strtoupper(toString(v_mix))));
  echo(LINE(9,x_strtolower(toString(v_mix))));
  (v_caps = "this is a test of the caps function\n");
  echo(LINE(12,x_ucwords(toString(v_caps))));
  echo(LINE(13,x_ucfirst(toString(v_caps))));
  echo(LINE(15,x_chr(40LL)));
  echo(toString(LINE(16,x_ord("c"))));
  echo(LINE(18,x_strrev("Hello World!")));
  echo(LINE(20,x_str_repeat("-=-", toInt32(20LL))));
  (v_var1 = "Hello");
  (v_var2 = "hello");
  if (equal(LINE(24,x_strcasecmp(toString(v_var1), toString(v_var2))), 0LL)) {
    echo(LINE(25,concat4(toString(v_var1), " is equal to ", toString(v_var2), " in a case-insensitive string comparison")));
  }
  echo(toString(LINE(27,x_strcasecmp("Z", "AAA"))));
  echo(toString(LINE(28,x_strcasecmp("Z", "A"))));
  echo(toString(LINE(30,x_strncasecmp("thiS BE a TeST", "THis be a TEST", toInt32(5LL)))));
  echo(toString(LINE(38,x_strnatcmp("img1.png", "img10.png"))));
  echo(toString(LINE(39,x_strnatcmp("img10.png", "img1.png"))));
  echo(toString(LINE(40,x_strnatcmp("img202.png", "img20.png"))));
  echo(toString(LINE(41,x_strnatcasecmp("iMG1.png", "img10.png"))));
  echo(toString(LINE(42,x_strnatcasecmp("imG10.png", "IMg1.png"))));
  echo(toString(LINE(43,x_strnatcasecmp("ImG202.png", "Img20.png"))));
  echo(LINE(46,(assignCallTemp(eo_1, x_base64_encode("this is an encoding string. zot!")),concat3("\nbase64_encode: [", eo_1, "]\n"))));
  echo(LINE(47,(assignCallTemp(eo_1, x_base64_encode("this is an encoding string. zot! and a little bit more this time")),concat3("\nbase64_encode: [", eo_1, "]\n"))));
  echo(LINE(48,(assignCallTemp(eo_1, x_base64_encode("this is an encoding string. zot! cha cha now yall!!")),concat3("\nbase64_encode: [", eo_1, "]\n"))));
  echo(LINE(50,(assignCallTemp(eo_1, toString(x_base64_decode(x_base64_encode("this is an encoding string. zot!")))),concat3("\nbase64_decode: [", eo_1, "]\n"))));
  echo(LINE(51,(assignCallTemp(eo_1, toString(x_base64_decode(x_base64_encode("this is an encoding string. zot! and a little bit more this time")))),concat3("\nbase64_decode: [", eo_1, "]\n"))));
  echo(LINE(52,(assignCallTemp(eo_1, toString(x_base64_decode(x_base64_encode("this is an encoding string. zot! cha cha now yall!!")))),concat3("\nbase64_decode: [", eo_1, "]\n"))));
  echo(concat(LINE(54,x_quoted_printable_decode("hi=20this=20is=20a=20test")), "\n"));
  echo(concat(LINE(56,x_quotemeta("hey.\\\\\\'!@#$%()[]{}/.,<>;:\"\\|")), "\n"));
  echo(LINE(58,x_str_rot13("01234567890abcdefghijklmnopqrstuvwxyz,./;[]\\-=")));
  print(concat(toString(LINE(60,x_substr_count("This is a test", "is"))), "\n"));
  print(concat(toString(LINE(61,x_substr_count("cgcgcgcgcgcgcgcgcgcgcgc", "cgc"))), "\n"));
  print(concat(toString(LINE(62,x_substr_count("is IS iS Is", "is"))), "\n"));
  echo(LINE(64,(assignCallTemp(eo_1, toString(x_strspn("42 is the answer, what is the question ...", "1234567890"))),concat3("1: ", eo_1, "\n"))));
  echo(LINE(66,(assignCallTemp(eo_1, toString(x_strcmp("tEsT", "tEsT"))),concat3("2: ", eo_1, "\n"))));
  echo(LINE(67,(assignCallTemp(eo_1, toString(x_strcmp("tEsT", "test"))),concat3("3: ", eo_1, "\n"))));
  echo(LINE(68,(assignCallTemp(eo_1, toString(x_strcmp("blah", "tEsT"))),concat3("4: ", eo_1, "\n"))));
  echo(LINE(69,(assignCallTemp(eo_1, toString(x_strncmp("meepMopeBlah", "me", toInt32(2LL)))),concat3("5: ", eo_1, "\n"))));
  echo(LINE(71,(assignCallTemp(eo_1, toString(x_strcoll("tEsT", "test"))),concat3("6: ", eo_1, "\n"))));
  echo(LINE(72,(assignCallTemp(eo_1, toString(x_strcoll("blah", "tEsT"))),concat3("7: ", eo_1, "\n"))));
  echo(concat(toString(LINE(75,x_strcspn("dzg", "abcdefg"))), "\n"));
  echo(concat(toString(LINE(78,(assignCallTemp(eo_0, toString((assignCallTemp(eo_2, toString(x_getenv("PATH"))),x_strrchr(eo_2, ":")))),x_substr(eo_0, toInt32(1LL))))), "\n"));
  echo("strrchr stuff\n");
  (v_text = "Line 1\nLine 2\nLine 3");
  echo(concat(toString(LINE(83,(assignCallTemp(eo_0, toString(x_strrchr(toString(v_text), 10LL))),x_substr(eo_0, toInt32(1LL))))), "\n"));
  echo(toString(LINE(86,x_strrpos("test.php", "."))));
  echo(toString(LINE(87,x_strrpos("test.php.test", "."))));
  (v_a = LINE(92,x_parse_url("http://username:password@hostname/path\?arg=value#anchor")));
  LINE(93,x_var_dump(1, v_a));
  (v_a = LINE(95,x_parse_url("http://username@hostname/path#anchor")));
  LINE(96,x_var_dump(1, v_a));
  (v_a = LINE(98,x_parse_url("http://hostname/path\?arg=value")));
  LINE(99,x_var_dump(1, v_a));
  (v_a = LINE(101,x_parse_url("http://hostname/path")));
  LINE(102,x_var_dump(1, v_a));
  (v_str = "abcdefghijklmnopqrstuvwxyz");
  (v_var = "ABCDEFGH:/MNRPQR/");
  echo(LINE(110,concat3("Original: ", toString(v_var), "<hr />\n")));
  echo(concat(toString(LINE(113,x_substr_replace(v_var, "bob", 0LL))), "<br />\n"));
  echo(concat(toString(LINE(114,(assignCallTemp(eo_0, v_var),assignCallTemp(eo_3, x_strlen(toString(v_var))),x_substr_replace(eo_0, "bob", 0LL, eo_3)))), "<br />\n"));
  echo(concat(toString(LINE(117,x_substr_replace(v_var, "bob", 0LL, 0LL))), "<br />\n"));
  echo(concat(toString(LINE(120,x_substr_replace(v_var, "bob", 10LL, -1LL))), "<br />\n"));
  echo(concat(toString(LINE(121,x_substr_replace(v_var, "bob", -7LL, -1LL))), "<br />\n"));
  echo(concat(toString(LINE(124,x_substr_replace(v_var, "", 10LL, -1LL))), "<br />\n"));
  LINE(127,x_var_dump(1, x_substr_replace("bork", "", 10LL, -1LL)));
  (v_data = "Two Ts and one F.");
  (v_result = LINE(131,x_count_chars(toString(v_data), 0LL)));
  LINE(132,x_var_dump(1, v_result));
  (v_result = LINE(134,x_count_chars(toString(v_data), 1LL)));
  LINE(135,x_var_dump(1, v_result));
  (v_result = LINE(137,x_count_chars(toString(v_data), 2LL)));
  LINE(138,x_var_dump(1, v_result));
  (v_result = LINE(140,x_count_chars(toString(v_data), 3LL)));
  LINE(141,x_var_dump(1, v_result));
  (v_result = LINE(143,x_count_chars(toString(v_data), 4LL)));
  LINE(144,x_var_dump(1, v_result));
  (v_data = ScalarArrays::sa_[0]);
  echo(concat(LINE(151,x_http_build_query(v_data)), "\n"));
  echo(concat(LINE(152,x_http_build_query(v_data, "", "&amp;")), "\n"));
  (v_data = ScalarArrays::sa_[1]);
  echo(concat(LINE(154,x_http_build_query(v_data)), "\n"));
  echo(concat(LINE(155,x_http_build_query(v_data, "myvar_")), "\n"));
  (v_data = ScalarArrays::sa_[2]);
  echo(LINE(169,x_http_build_query(v_data, "flags_")));
  (v_data = ((Object)(LINE(182,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  echo(LINE(184,x_http_build_query(v_data)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
