
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      HASH_RETURN(0x2A28A0084DD3A743LL, g->GV(text),
                  text);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 8:
      HASH_RETURN(0x599E3F67E2599248LL, g->GV(result),
                  result);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 10:
      HASH_RETURN(0x4292CEE227B9150ALL, g->GV(a),
                  a);
      break;
    case 12:
      HASH_RETURN(0x004E9D9EC5A1334CLL, g->GV(var2),
                  var2);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 20:
      HASH_RETURN(0x2E6C98C2893B8E14LL, g->GV(caps),
                  caps);
      break;
    case 22:
      HASH_RETURN(0x424E3EFB6BEA3ED6LL, g->GV(mix),
                  mix);
      break;
    case 28:
      HASH_RETURN(0x362158638F83BD9CLL, g->GV(str),
                  str);
      break;
    case 32:
      HASH_RETURN(0x0953EE1CC9042120LL, g->GV(var),
                  var);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      HASH_RETURN(0x72A4916AD3C888E3LL, g->GV(var1),
                  var1);
      break;
    case 40:
      HASH_RETURN(0x4799F3E302DB8168LL, g->GV(string1),
                  string1);
      HASH_RETURN(0x30164401A9853128LL, g->GV(data),
                  data);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
