
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 127) {
    case 0:
      HASH_INDEX(0x2AC6042EF2A11F00LL, ttyname, 30);
      break;
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 7:
      HASH_INDEX(0x79F0EE2AD5236D07LL, getgrgid, 36);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x51FE5505C0CE6491LL, getpgid, 22);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 22:
      HASH_INDEX(0x6B3093D1F2F27D16LL, cwd, 32);
      break;
    case 24:
      HASH_INDEX(0x07D7BDC495A07A18LL, uname, 25);
      HASH_INDEX(0x3928BFE39F221018LL, errstr, 42);
      break;
    case 25:
      HASH_INDEX(0x66FE4C97C9502A99LL, isatty, 31);
      break;
    case 27:
      HASH_INDEX(0x5B22A7472448739BLL, gid, 16);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 44:
      HASH_INDEX(0x5E660E204990332CLL, ppid, 13);
      break;
    case 54:
      HASH_INDEX(0x31CF71EAC03B86B6LL, testfile, 33);
      break;
    case 61:
      HASH_INDEX(0x69DBBF74301F96BDLL, retval, 40);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 67:
      HASH_INDEX(0x225D2F8E6409A9C3LL, login, 19);
      break;
    case 70:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x60B57838E4303D46LL, getpwuid, 38);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 74:
      HASH_INDEX(0x2B7634BE3D12BDCALL, uid, 14);
      HASH_INDEX(0x23767E1D6F8CFCCALL, groups, 18);
      HASH_INDEX(0x57A84E31E9BBAFCALL, times, 26);
      break;
    case 78:
      HASH_INDEX(0x3580763DDA0EFDCELL, setpgid, 24);
      break;
    case 79:
      HASH_INDEX(0x0D6857FDDD7F21CFLL, k, 27);
      break;
    case 80:
      HASH_INDEX(0x6EAB859F44A4DED0LL, egid, 17);
      HASH_INDEX(0x409A9095CEE37350LL, mkfifo, 34);
      break;
    case 81:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 85:
      HASH_INDEX(0x1DED64CF31236F55LL, errno, 41);
      break;
    case 98:
      HASH_INDEX(0x702CB6B3DEDF0FE2LL, setsid, 21);
      break;
    case 102:
      HASH_INDEX(0x13C446138383FA66LL, getgrnam, 35);
      break;
    case 103:
      HASH_INDEX(0x502920B03EBA16E7LL, getrlimit, 39);
      break;
    case 107:
      HASH_INDEX(0x3C2F961831E4EF6BLL, v, 28);
      break;
    case 108:
      HASH_INDEX(0x528E3AD621755D6CLL, pgrp, 20);
      break;
    case 110:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 115:
      HASH_INDEX(0x133F26DF225E6273LL, pid, 12);
      break;
    case 118:
      HASH_INDEX(0x5E5C63DE043CB3F6LL, ctermid, 29);
      break;
    case 120:
      HASH_INDEX(0x4DE3E5A930A830F8LL, getsid, 23);
      break;
    case 124:
      HASH_INDEX(0x16F9AB1EABFCA77CLL, getpwnam, 37);
      break;
    case 125:
      HASH_INDEX(0x2D4B2239845CC37DLL, euid, 15);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 43) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
