
#include <php/roadsend/pcc/runtime/ext/standard/tests/array_splice.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$array_splice_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/array_splice.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$array_splice_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_out __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out") : g->GV(out);

  echo("splice 1\n");
  (v_input = ScalarArrays::sa_[0]);
  (v_out = LINE(5,x_array_splice(ref(v_input), toInt32(2LL))));
  LINE(7,x_var_dump(1, v_input));
  LINE(8,x_var_dump(1, v_out));
  echo("splice 2\n");
  (v_input = ScalarArrays::sa_[0]);
  (v_out = LINE(12,x_array_splice(ref(v_input), toInt32(1LL), -1LL)));
  LINE(14,x_var_dump(1, v_input));
  LINE(15,x_var_dump(1, v_out));
  echo("splice 3\n");
  (v_input = ScalarArrays::sa_[0]);
  (v_out = LINE(19,(assignCallTemp(eo_0, ref(v_input)),assignCallTemp(eo_2, x_count(v_input)),x_array_splice(eo_0, toInt32(1LL), eo_2, "orange"))));
  LINE(21,x_var_dump(1, v_input));
  LINE(22,x_var_dump(1, v_out));
  echo("splice 4\n");
  (v_input = ScalarArrays::sa_[0]);
  (v_out = LINE(26,x_array_splice(ref(v_input), toInt32(-1LL), 1LL, ScalarArrays::sa_[1])));
  LINE(29,x_var_dump(1, v_input));
  LINE(30,x_var_dump(1, v_out));
  echo("splice 5\n");
  (v_input = ScalarArrays::sa_[0]);
  (v_out = LINE(34,x_array_splice(ref(v_input), toInt32(3LL), 0LL, "purple")));
  LINE(37,x_var_dump(1, v_input));
  LINE(38,x_var_dump(1, v_out));
  echo("splice 6, keys\n");
  (v_input = ScalarArrays::sa_[2]);
  (v_out = LINE(42,x_array_splice(ref(v_input), toInt32(3LL), 0LL, "green")));
  LINE(43,x_var_dump(1, v_input));
  LINE(44,x_var_dump(1, v_out));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
