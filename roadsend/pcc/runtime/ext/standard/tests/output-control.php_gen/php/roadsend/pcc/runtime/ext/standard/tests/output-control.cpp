
#include <php/roadsend/pcc/runtime/ext/standard/tests/output-control.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/output-control.php line 55 */
String f_callback(CVarRef v_buffer) {
  FUNCTION_INJECTION(callback);
  return (LINE(58,x_ereg_replace("apples", "oranges", toString(v_buffer))));
} /* function */
Variant i_callback(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2DE38EBA0ED55F48LL, callback) {
    return (f_callback(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$output_control_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/output-control.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$output_control_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_worp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("worp") : g->GV(worp);
  Variant &v_out __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out") : g->GV(out);

  (v_s = LINE(3,x_ob_get_status()));
  LINE(4,x_var_dump(1, v_s));
  LINE(6,x_ob_start());
  echo("This is a large foo");
  (v_worp = LINE(10,x_ob_get_contents()));
  (v_s = LINE(12,x_ob_get_status()));
  LINE(14,x_ob_end_clean());
  echo(LINE(16,concat3("You tried to say: ", toString(v_worp), "\n")));
  v_s.weakRemove("size", 0x3FE8023BDF261C0FLL);
  LINE(19,x_var_dump(1, v_s));
  LINE(23,x_ob_start());
  LINE(25,x_ob_start());
  (v_s = LINE(27,x_ob_get_status(true)));
  echo("This is a large foo");
  LINE(31,x_ob_end_flush());
  echo("wibble");
  LINE(35,x_ob_end_flush());
  lval(unsetLval(v_s, 0LL, 0x77CFA1EEF01BCA90LL)).weakRemove("size", 0x3FE8023BDF261C0FLL);
  lval(unsetLval(v_s, 1LL, 0x5BCA7C69B794F8CELL)).weakRemove("size", 0x3FE8023BDF261C0FLL);
  LINE(39,x_var_dump(1, v_s));
  LINE(41,x_ob_start());
  print("Hello World");
  (v_out = LINE(45,x_ob_get_clean()));
  (v_out = LINE(46,x_strtolower(toString(v_out))));
  LINE(48,x_var_dump(1, v_out));
  LINE(62,x_ob_start("callback"));
  echo("\n<html>\n<body>\n<p>It's like comparing apples to oranges.\n</body>\n</html>\n\n");
  LINE(74,x_ob_end_flush());
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
