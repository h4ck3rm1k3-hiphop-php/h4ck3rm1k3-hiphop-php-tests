
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_error_handler_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_error_handler_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/error-handler.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$error_handler_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_soretoes(CVarRef v_a, CVarRef v_b, CVarRef v_c, CVarRef v_d);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_error_handler_h__
