
#include <php/roadsend/pcc/runtime/ext/standard/tests/substr.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$substr_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/substr.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$substr_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_string1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("string1") : g->GV(string1);

  (v_string1 = "dolphin-flogger");
  echo(LINE(5,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(0LL), toInt32(5LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(6,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(4LL), toInt32(9LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(9LL), toInt32(23LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(8,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(25LL), toInt32(40LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(9,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(0LL), toInt32(-4LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(10,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(-3LL), toInt32(-2LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(11,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(-20LL), toInt32(24LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(12,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(-20LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(13,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(negate(x_strlen(toString(v_string1))))))),concat3("<", eo_1, ">\n"))));
  echo(LINE(14,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(negate(x_strlen(toString(v_string1))) - 1LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(15,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(-5LL), toInt32(4LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(16,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(13LL), toInt32(-8LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(17,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(40LL), toInt32(0LL)))),concat3("<", eo_1, ">\n"))));
  echo(LINE(18,(assignCallTemp(eo_1, toString(x_substr(toString(v_string1), toInt32(4LL)))),concat3("<", eo_1, ">\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
