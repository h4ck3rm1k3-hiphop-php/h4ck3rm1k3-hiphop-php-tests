
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 4:
      HASH_INDEX(0x336176791BC03F04LL, num, 12);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 8:
      HASH_INDEX(0x4F451C70DF501D48LL, formatted, 22);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x3F444870EE149B09LL, money2, 20);
      break;
    case 12:
      HASH_INDEX(0x3E9D67504EE78ACCLL, month, 16);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 18:
      HASH_INDEX(0x065327C8F5142892LL, isodate, 18);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x4CB7156046B03E53LL, money1, 19);
      break;
    case 23:
      HASH_INDEX(0x2CDDD058B4D49597LL, money, 21);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 40:
      HASH_INDEX(0x0E12BE46FD64D268LL, day, 17);
      break;
    case 43:
      HASH_INDEX(0x2B6706CF4608DE2BLL, location, 13);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 54:
      HASH_INDEX(0x4575D5E219281E76LL, format, 14);
      HASH_INDEX(0x28D044C5EA490CB6LL, year, 15);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 23) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
