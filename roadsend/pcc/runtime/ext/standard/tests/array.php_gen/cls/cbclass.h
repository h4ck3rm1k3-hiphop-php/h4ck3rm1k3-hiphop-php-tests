
#ifndef __GENERATED_cls_cbclass_h__
#define __GENERATED_cls_cbclass_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/standard/tests/array.php line 85 */
class c_cbclass : virtual public ObjectData {
  BEGIN_CLASS_MAP(cbclass)
  END_CLASS_MAP(cbclass)
  DECLARE_CLASS(cbclass, cbclass, ObjectData)
  void init();
  public: Variant t_cbmethod(CVarRef v_o);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_cbclass_h__
