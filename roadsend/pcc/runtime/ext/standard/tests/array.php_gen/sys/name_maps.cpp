
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "cbclass", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "cube", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "even", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "map_spanish", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "odd", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "rmul", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "rsum", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "show_spanish", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "test_alter", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  "test_print", "roadsend/pcc/runtime/ext/standard/tests/array.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
