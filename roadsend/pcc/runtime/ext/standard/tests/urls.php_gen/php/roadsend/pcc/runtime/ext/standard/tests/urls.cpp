
#include <php/roadsend/pcc/runtime/ext/standard/tests/urls.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$urls_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/urls.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$urls_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_url __attribute__((__unused__)) = (variables != gVariables) ? variables->get("url") : g->GV(url);
  Variant &v_res __attribute__((__unused__)) = (variables != gVariables) ? variables->get("res") : g->GV(res);

  (v_url = "http://username:password@hostname/path\?arg=value#anchor");
  LINE(4,x_print_r(x_parse_url(toString(v_url))));
  (v_url = LINE(6,x_urlencode("this/is\\a\\+t est&o f!@#$% ^&*()url 3nc 0d3 _-=+;:,.><[]{}")));
  echo(toString(v_url) + toString("\n"));
  echo(concat(LINE(9,x_urldecode(toString(v_url))), "\n"));
  (v_url = LINE(11,x_rawurlencode("this/is\\a\\+t est&o f!@#$% ^&*()url 3nc 0d3 _-=+;:,.><[]{}")));
  echo(toString(v_url) + toString("\n"));
  echo(concat(LINE(14,x_rawurldecode(toString(v_url))), "\n"));
  (v_res = LINE(17,x_parse_url("http://username:password@hostname:999/path\?arg=value#anchor")));
  LINE(18,x_var_dump(1, v_res));
  (v_res = LINE(20,x_parse_url("http://invalid_host..name/")));
  LINE(21,x_var_dump(1, v_res));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
