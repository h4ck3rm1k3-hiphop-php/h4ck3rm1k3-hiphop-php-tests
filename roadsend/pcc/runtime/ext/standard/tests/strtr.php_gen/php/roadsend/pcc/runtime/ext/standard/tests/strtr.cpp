
#include <php/roadsend/pcc/runtime/ext/standard/tests/strtr.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$strtr_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/strtr.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$strtr_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_trans __attribute__((__unused__)) = (variables != gVariables) ? variables->get("trans") : g->GV(trans);
  Variant &v_trans1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("trans1") : g->GV(trans1);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  echo(concat(LINE(3,x_strtr(toString(1LL), 2LL, 3LL)), "\n"));
  echo(concat(LINE(4,x_strtr("hello", "ello", "3770")), "\n"));
  echo(concat(LINE(5,x_strtr("hello", "ello", "377")), "\n"));
  echo(concat(LINE(6,x_strtr("hello", "ell", "37770")), "\n"));
  (v_trans = ScalarArrays::sa_[0]);
  echo(concat(LINE(9,x_strtr("hi all, I said hello", v_trans)), "\n"));
  (v_trans = ScalarArrays::sa_[1]);
  echo(concat(LINE(12,x_strtr("hi all, I said hello", v_trans)), "\n"));
  (v_trans = ScalarArrays::sa_[0]);
  (v_trans1 = ScalarArrays::sa_[1]);
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, 10000LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        concat(LINE(19,x_strtr(toString(1LL), 2LL, 3LL)), "\n");
        concat(LINE(20,x_strtr("hello", "ello", "3770")), "\n");
        concat(LINE(21,x_strtr("hello", "ello", "377")), "\n");
        concat(LINE(22,x_strtr("hello", "ell", "37770")), "\n");
        concat(LINE(25,x_strtr("hi all, I said hello", v_trans)), "\n");
        concat(LINE(28,x_strtr("hi all, I said hello", v_trans1)), "\n");
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
