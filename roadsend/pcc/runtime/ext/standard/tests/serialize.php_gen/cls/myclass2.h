
#ifndef __GENERATED_cls_myclass2_h__
#define __GENERATED_cls_myclass2_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 48 */
class c_myclass2 : virtual public ObjectData {
  BEGIN_CLASS_MAP(myclass2)
  END_CLASS_MAP(myclass2)
  DECLARE_CLASS(myclass2, myClass2, ObjectData)
  void init();
  public: Variant m_p1;
  public: Variant m_p2;
  public: Variant m_p3;
  public: Variant m_p4;
  public: Variant t___sleep();
  public: void t_hello();
  public: Variant t___wakeup();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_myclass2_h__
