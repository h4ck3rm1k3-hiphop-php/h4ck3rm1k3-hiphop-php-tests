
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_serialize_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_serialize_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/standard/tests/serialize.fw.h>

// Declarations
#include <cls/myclass2.h>
#include <cls/zot.h>
#include <cls/myclass.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_test_ser(CVarRef v_val);
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$serialize_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_myclass2(CArrRef params, bool init = true);
Object co_zot(CArrRef params, bool init = true);
Object co_myclass(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_standard_tests_serialize_h__
