
#include <php/roadsend/pcc/runtime/ext/standard/tests/serialize.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 48 */
Variant c_myclass2::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass2::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass2::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("p1", m_p1.isReferenced() ? ref(m_p1) : m_p1));
  props.push_back(NEW(ArrayElement)("p2", m_p2.isReferenced() ? ref(m_p2) : m_p2));
  props.push_back(NEW(ArrayElement)("p3", m_p3.isReferenced() ? ref(m_p3) : m_p3));
  props.push_back(NEW(ArrayElement)("p4", m_p4.isReferenced() ? ref(m_p4) : m_p4));
  c_ObjectData::o_get(props);
}
bool c_myclass2::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_EXISTS_STRING(0x20511842520A276CLL, p4, 2);
      break;
    case 6:
      HASH_EXISTS_STRING(0x18B6B61EF19E261ELL, p3, 2);
      break;
    case 7:
      HASH_EXISTS_STRING(0x5C657D380DA684F7LL, p1, 2);
      HASH_EXISTS_STRING(0x0FEFA20DB1B08AC7LL, p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass2::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_RETURN_STRING(0x20511842520A276CLL, m_p4,
                         p4, 2);
      break;
    case 6:
      HASH_RETURN_STRING(0x18B6B61EF19E261ELL, m_p3,
                         p3, 2);
      break;
    case 7:
      HASH_RETURN_STRING(0x5C657D380DA684F7LL, m_p1,
                         p1, 2);
      HASH_RETURN_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                         p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass2::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_SET_STRING(0x20511842520A276CLL, m_p4,
                      p4, 2);
      break;
    case 6:
      HASH_SET_STRING(0x18B6B61EF19E261ELL, m_p3,
                      p3, 2);
      break;
    case 7:
      HASH_SET_STRING(0x5C657D380DA684F7LL, m_p1,
                      p1, 2);
      HASH_SET_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                      p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass2::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 4:
      HASH_RETURN_STRING(0x20511842520A276CLL, m_p4,
                         p4, 2);
      break;
    case 6:
      HASH_RETURN_STRING(0x18B6B61EF19E261ELL, m_p3,
                         p3, 2);
      break;
    case 7:
      HASH_RETURN_STRING(0x5C657D380DA684F7LL, m_p1,
                         p1, 2);
      HASH_RETURN_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                         p2, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass2::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass2)
ObjectData *c_myclass2::cloneImpl() {
  c_myclass2 *obj = NEW(c_myclass2)();
  cloneSet(obj);
  return obj;
}
void c_myclass2::cloneSet(c_myclass2 *clone) {
  clone->m_p1 = m_p1.isReferenced() ? ref(m_p1) : m_p1;
  clone->m_p2 = m_p2.isReferenced() ? ref(m_p2) : m_p2;
  clone->m_p3 = m_p3.isReferenced() ? ref(m_p3) : m_p3;
  clone->m_p4 = m_p4.isReferenced() ? ref(m_p4) : m_p4;
  ObjectData::cloneSet(clone);
}
Variant c_myclass2::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x35B3B21789F02516LL, hello) {
        return (t_hello(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass2::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x35B3B21789F02516LL, hello) {
        return (t_hello(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass2::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass2$os_get(const char *s) {
  return c_myclass2::os_get(s, -1);
}
Variant &cw_myclass2$os_lval(const char *s) {
  return c_myclass2::os_lval(s, -1);
}
Variant cw_myclass2$os_constant(const char *s) {
  return c_myclass2::os_constant(s);
}
Variant cw_myclass2$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass2::os_invoke(c, s, params, -1, fatal);
}
void c_myclass2::init() {
  m_p1 = null;
  m_p2 = "hi there";
  m_p3 = ScalarArrays::sa_[0];
  m_p4 = true;
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 55 */
Variant c_myclass2::t___sleep() {
  INSTANCE_METHOD_INJECTION(myClass2, myClass2::__sleep);
  Array v_a;

  echo("zzzzzzzz......\n");
  v_a.append(("p3"));
  return v_a;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 61 */
void c_myclass2::t_hello() {
  INSTANCE_METHOD_INJECTION(myClass2, myClass2::hello);
  echo("hello");
  echo(LINE(63,concat3("this should NOT say 'hi there': ", toString(m_p2), "\n")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 66 */
Variant c_myclass2::t___wakeup() {
  INSTANCE_METHOD_INJECTION(myClass2, myClass2::__wakeup);
  echo("yay i woke up!!!\n");
  return null;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 44 */
Variant c_zot::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_zot::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_zot::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("prop", m_prop.isReferenced() ? ref(m_prop) : m_prop));
  c_ObjectData::o_get(props);
}
bool c_zot::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x4EA429E0DCC990B7LL, prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_zot::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4EA429E0DCC990B7LL, m_prop,
                         prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_zot::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x4EA429E0DCC990B7LL, m_prop,
                      prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_zot::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x4EA429E0DCC990B7LL, m_prop,
                         prop, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_zot::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(zot)
ObjectData *c_zot::cloneImpl() {
  c_zot *obj = NEW(c_zot)();
  cloneSet(obj);
  return obj;
}
void c_zot::cloneSet(c_zot *clone) {
  clone->m_prop = m_prop.isReferenced() ? ref(m_prop) : m_prop;
  ObjectData::cloneSet(clone);
}
Variant c_zot::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_zot::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_zot::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_zot$os_get(const char *s) {
  return c_zot::os_get(s, -1);
}
Variant &cw_zot$os_lval(const char *s) {
  return c_zot::os_lval(s, -1);
}
Variant cw_zot$os_constant(const char *s) {
  return c_zot::os_constant(s);
}
Variant cw_zot$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_zot::os_invoke(c, s, params, -1, fatal);
}
void c_zot::init() {
  m_prop = ScalarArrays::sa_[1];
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 24 */
Variant c_myclass::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_myclass::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_myclass::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("p1", m_p1.isReferenced() ? ref(m_p1) : m_p1));
  props.push_back(NEW(ArrayElement)("p2", m_p2.isReferenced() ? ref(m_p2) : m_p2));
  props.push_back(NEW(ArrayElement)("p3", m_p3.isReferenced() ? ref(m_p3) : m_p3));
  props.push_back(NEW(ArrayElement)("p4", m_p4.isReferenced() ? ref(m_p4) : m_p4));
  props.push_back(NEW(ArrayElement)("p5", m_p5.isReferenced() ? ref(m_p5) : m_p5));
  props.push_back(NEW(ArrayElement)("p6", m_p6.isReferenced() ? ref(m_p6) : m_p6));
  c_ObjectData::o_get(props);
}
bool c_myclass::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 7:
      HASH_EXISTS_STRING(0x5C657D380DA684F7LL, p1, 2);
      HASH_EXISTS_STRING(0x0FEFA20DB1B08AC7LL, p2, 2);
      break;
    case 12:
      HASH_EXISTS_STRING(0x20511842520A276CLL, p4, 2);
      break;
    case 13:
      HASH_EXISTS_STRING(0x7712320985A0560DLL, p5, 2);
      break;
    case 14:
      HASH_EXISTS_STRING(0x18B6B61EF19E261ELL, p3, 2);
      HASH_EXISTS_STRING(0x7ADD4C161B321ABELL, p6, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_myclass::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 7:
      HASH_RETURN_STRING(0x5C657D380DA684F7LL, m_p1,
                         p1, 2);
      HASH_RETURN_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                         p2, 2);
      break;
    case 12:
      HASH_RETURN_STRING(0x20511842520A276CLL, m_p4,
                         p4, 2);
      break;
    case 13:
      HASH_RETURN_STRING(0x7712320985A0560DLL, m_p5,
                         p5, 2);
      break;
    case 14:
      HASH_RETURN_STRING(0x18B6B61EF19E261ELL, m_p3,
                         p3, 2);
      HASH_RETURN_STRING(0x7ADD4C161B321ABELL, m_p6,
                         p6, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_myclass::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 7:
      HASH_SET_STRING(0x5C657D380DA684F7LL, m_p1,
                      p1, 2);
      HASH_SET_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                      p2, 2);
      break;
    case 12:
      HASH_SET_STRING(0x20511842520A276CLL, m_p4,
                      p4, 2);
      break;
    case 13:
      HASH_SET_STRING(0x7712320985A0560DLL, m_p5,
                      p5, 2);
      break;
    case 14:
      HASH_SET_STRING(0x18B6B61EF19E261ELL, m_p3,
                      p3, 2);
      HASH_SET_STRING(0x7ADD4C161B321ABELL, m_p6,
                      p6, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_myclass::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 7:
      HASH_RETURN_STRING(0x5C657D380DA684F7LL, m_p1,
                         p1, 2);
      HASH_RETURN_STRING(0x0FEFA20DB1B08AC7LL, m_p2,
                         p2, 2);
      break;
    case 12:
      HASH_RETURN_STRING(0x20511842520A276CLL, m_p4,
                         p4, 2);
      break;
    case 13:
      HASH_RETURN_STRING(0x7712320985A0560DLL, m_p5,
                         p5, 2);
      break;
    case 14:
      HASH_RETURN_STRING(0x18B6B61EF19E261ELL, m_p3,
                         p3, 2);
      HASH_RETURN_STRING(0x7ADD4C161B321ABELL, m_p6,
                         p6, 2);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_myclass::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(myclass)
ObjectData *c_myclass::cloneImpl() {
  c_myclass *obj = NEW(c_myclass)();
  cloneSet(obj);
  return obj;
}
void c_myclass::cloneSet(c_myclass *clone) {
  clone->m_p1 = m_p1.isReferenced() ? ref(m_p1) : m_p1;
  clone->m_p2 = m_p2.isReferenced() ? ref(m_p2) : m_p2;
  clone->m_p3 = m_p3.isReferenced() ? ref(m_p3) : m_p3;
  clone->m_p4 = m_p4.isReferenced() ? ref(m_p4) : m_p4;
  clone->m_p5 = m_p5.isReferenced() ? ref(m_p5) : m_p5;
  clone->m_p6 = m_p6.isReferenced() ? ref(m_p6) : m_p6;
  ObjectData::cloneSet(clone);
}
Variant c_myclass::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x35B3B21789F02516LL, hello) {
        return (t_hello(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_myclass::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x35B3B21789F02516LL, hello) {
        return (t_hello(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_myclass::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_myclass$os_get(const char *s) {
  return c_myclass::os_get(s, -1);
}
Variant &cw_myclass$os_lval(const char *s) {
  return c_myclass::os_lval(s, -1);
}
Variant cw_myclass$os_constant(const char *s) {
  return c_myclass::os_constant(s);
}
Variant cw_myclass$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_myclass::os_invoke(c, s, params, -1, fatal);
}
void c_myclass::init() {
  m_p1 = null;
  m_p2 = "hi th\";ere";
  m_p3 = ScalarArrays::sa_[0];
  m_p4 = true;
  m_p5 = null;
  m_p6 = false;
}
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 32 */
String c_myclass::t___tostring() {
  INSTANCE_METHOD_INJECTION(myClass, myClass::__toString);
  return "[myClass instance]";
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 35 */
void c_myclass::t_hello() {
  INSTANCE_METHOD_INJECTION(myClass, myClass::hello);
  echo("hello");
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 39 */
Variant c_myclass::t___wakeup() {
  INSTANCE_METHOD_INJECTION(myClass, myClass::__wakeup);
  echo("yay i woke up!!!\n");
  return null;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/standard/tests/serialize.php line 3 */
void f_test_ser(CVarRef v_val) {
  FUNCTION_INJECTION(test_ser);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  String v_sv;
  Variant v_usv;

  (v_sv = LINE(15,x_serialize(v_val)));
  echo(LINE(16,(assignCallTemp(eo_1, toString(v_val)),assignCallTemp(eo_3, x_gettype(v_val)),assignCallTemp(eo_4, concat3(" serialize is \n", v_sv, "\n")),concat5("val is ", eo_1, ", type: ", eo_3, eo_4))));
  (v_usv = LINE(17,x_unserialize(v_sv)));
  LINE(18,x_var_dump(1, v_usv));
} /* function */
Object co_myclass2(CArrRef params, bool init /* = true */) {
  return Object(p_myclass2(NEW(c_myclass2)())->dynCreate(params, init));
}
Object co_zot(CArrRef params, bool init /* = true */) {
  return Object(p_zot(NEW(c_zot)())->dynCreate(params, init));
}
Object co_myclass(CArrRef params, bool init /* = true */) {
  return Object(p_myclass(NEW(c_myclass)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$serialize_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/serialize.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$serialize_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_globa __attribute__((__unused__)) = (variables != gVariables) ? variables->get("globa") : g->GV(globa);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);
  Variant &v_c __attribute__((__unused__)) = (variables != gVariables) ? variables->get("c") : g->GV(c);
  Variant &v_d __attribute__((__unused__)) = (variables != gVariables) ? variables->get("d") : g->GV(d);
  Variant &v_sd __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sd") : g->GV(sd);
  Variant &v_e __attribute__((__unused__)) = (variables != gVariables) ? variables->get("e") : g->GV(e);

  (v_globa = "my global hi there");
  (v_a = ScalarArrays::sa_[2]);
  LINE(72,f_test_ser(v_a));
  (v_a = ScalarArrays::sa_[3]);
  LINE(75,f_test_ser(v_a));
  (v_a = ScalarArrays::sa_[4]);
  LINE(78,f_test_ser(v_a));
  (v_a = ScalarArrays::sa_[5]);
  LINE(81,f_test_ser(v_a));
  LINE(83,f_test_ser(true));
  LINE(84,f_test_ser(false));
  LINE(85,f_test_ser("this is a test of a string with \"; some a:0 N; shit in it i:23123; to screw tim up b:0; on the unserializer"));
  LINE(86,f_test_ser(2371238LL));
  LINE(87,f_test_ser(23.231200000000001));
  LINE(88,f_test_ser(23.123456789012344));
  LINE(89,f_test_ser(null));
  (v_c = ((Object)(LINE(91,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  LINE(92,f_test_ser(v_c));
  (v_d = ((Object)(LINE(94,p_myclass(p_myclass(NEWOBJ(c_myclass)())->create())))));
  (v_sd = LINE(95,x_serialize(v_d)));
  echo(toString(v_sd));
  (v_e = LINE(97,x_unserialize(toString(v_sd))));
  LINE(98,v_e.o_invoke_few_args("hello", 0x35B3B21789F02516LL, 0));
  (v_d = ((Object)(LINE(100,p_myclass2(p_myclass2(NEWOBJ(c_myclass2)())->create())))));
  (v_sd = LINE(101,x_serialize(v_d)));
  echo(toString(v_sd));
  (v_e = LINE(103,x_unserialize(toString(v_sd))));
  LINE(104,x_print_r(v_e));
  LINE(105,v_e.o_invoke_few_args("hello", 0x35B3B21789F02516LL, 0));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
