
#include <php/roadsend/pcc/runtime/ext/standard/tests/number_format.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$number_format_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/number_format.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$number_format_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(LINE(3,(assignCallTemp(eo_1, x_number_format(toDouble(-2354987LL))),concat3("1:", eo_1, "\n"))));
  echo(LINE(4,(assignCallTemp(eo_1, x_number_format(toDouble("foo"))),concat3("2:", eo_1, "\n"))));
  echo(LINE(5,(assignCallTemp(eo_1, x_number_format(toDouble(0LL))),concat3("3:", eo_1, "\n"))));
  echo(LINE(6,(assignCallTemp(eo_1, x_number_format(98095.100000000006)),concat3("4:", eo_1, "\n"))));
  echo(LINE(7,(assignCallTemp(eo_1, x_number_format(1.2344999999999999)),concat3("5:", eo_1, "\n"))));
  echo(LINE(9,(assignCallTemp(eo_1, x_number_format(toDouble(-2354987LL), toInt32(2LL))),concat3("6:", eo_1, "\n"))));
  echo(LINE(10,(assignCallTemp(eo_1, x_number_format(toDouble("foo"), toInt32(3LL))),concat3("7:", eo_1, "\n"))));
  echo(LINE(11,(assignCallTemp(eo_1, x_number_format(toDouble(0LL), toInt32(1LL))),concat3("8:", eo_1, "\n"))));
  echo(LINE(12,(assignCallTemp(eo_1, x_number_format(98095.100000000006, toInt32(1LL))),concat3("9:", eo_1, "\n"))));
  echo(LINE(13,(assignCallTemp(eo_1, x_number_format(1.2344999999999999, toInt32(3LL))),concat3("10:", eo_1, "\n"))));
  echo(LINE(17,(assignCallTemp(eo_1, x_number_format(toDouble(0LL), toInt32(0LL), "foo", "bar")),concat3("11:", eo_1, "\n"))));
  echo(LINE(18,(assignCallTemp(eo_1, x_number_format(98095.100000000006, toInt32(2LL), ",", " ")),concat3("12:", eo_1, "\n"))));
  echo(LINE(19,(assignCallTemp(eo_1, x_number_format(1.2344999999999999, toInt32(5LL), ",", " ")),concat3("13:", eo_1, "\n"))));
  echo(LINE(21,(assignCallTemp(eo_1, x_number_format(toDouble("9foo8"), toInt32(2LL), ".", ",")),concat3("14:", eo_1, "\n"))));
  echo(LINE(22,(assignCallTemp(eo_1, x_number_format(toDouble("chars10chars.987"), toInt32(3LL), ".", ",")),concat3("15:", eo_1, "\n"))));
  echo(LINE(23,(assignCallTemp(eo_1, x_number_format(toDouble(".9chars9"), toInt32(2LL))),concat3("16:", eo_1, "\n"))));
  echo(LINE(24,(assignCallTemp(eo_1, x_number_format(toDouble("+93.240"), toInt32(4LL))),concat3("17:", eo_1, "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
