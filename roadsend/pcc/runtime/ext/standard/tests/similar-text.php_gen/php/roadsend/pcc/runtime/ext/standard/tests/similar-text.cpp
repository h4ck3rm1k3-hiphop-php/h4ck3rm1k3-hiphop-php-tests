
#include <php/roadsend/pcc/runtime/ext/standard/tests/similar-text.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$similar_text_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/similar-text.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$similar_text_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_percent __attribute__((__unused__)) = (variables != gVariables) ? variables->get("percent") : g->GV(percent);

  print("kkkk and kkkk\n");
  print((toString(LINE(4,x_similar_text("kkkk", "kkkk")))));
  print("\n");
  print("kkkk and kkkk, take 2 (with percent)\n");
  print((toString(LINE(8,x_similar_text("kkkk", "kkkk", ref(v_percent))))));
  print("\n");
  print((LINE(10,concat3("percentage similar: ", toString(v_percent), "\n"))));
  print("asld;fjk asl;dfjkl and jasdklflaskdjf\n");
  print((toString(LINE(14,x_similar_text("asld;fjk asl;dfjkl", "jasdklflaskdjf", ref(v_percent))))));
  print("\n");
  print((LINE(16,concat3("percentage similar: ", toString(v_percent), "\n"))));
  print("asdf22asdf22fdsa and fdsa22jasdf22asdf\n");
  print((toString(LINE(19,x_similar_text("asdf22asdf22fdsa", "fdsa22jasdf22asdf", ref(v_percent))))));
  print("\n");
  print((LINE(21,concat3("percentage similar: ", toString(v_percent), "\n"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
