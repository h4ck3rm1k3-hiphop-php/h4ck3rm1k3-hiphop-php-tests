
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$exts_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_INCLUDE(0x0E9DED4A1CC5F444LL, "roadsend/pcc/runtime/ext/standard/tests/exts.php", php$roadsend$pcc$runtime$ext$standard$tests$exts_php);
      break;
    default:
      break;
  }
  if (s.empty()) return pm_php$roadsend$pcc$runtime$ext$standard$tests$exts_php(once, variables);
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
