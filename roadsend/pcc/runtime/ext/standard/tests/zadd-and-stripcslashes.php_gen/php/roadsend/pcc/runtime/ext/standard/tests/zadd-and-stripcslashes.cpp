
#include <php/roadsend/pcc/runtime/ext/standard/tests/zadd-and-stripcslashes.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$standard$tests$zadd_and_stripcslashes_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/standard/tests/zadd-and-stripcslashes.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$standard$tests$zadd_and_stripcslashes_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(concat(LINE(3,x_addcslashes("", "")), "\n"));
  echo(concat(LINE(4,x_addcslashes("", "burp")), "\n"));
  echo(concat(LINE(5,x_addcslashes("kaboemkara!", "")), "\n"));
  echo(concat(LINE(6,x_addcslashes("foobarbaz", "bar")), "\n"));
  echo(concat(LINE(7,x_addcslashes("foo[ ]", "A..z")), "\n"));
  echo(concat(toString((silenceInc(), silenceDec(LINE(9,x_addcslashes("zoo['.']", "z..A"))))), "\n"));
  echo(concat(toString((silenceInc(), silenceDec(LINE(11,x_addcslashes("zoo['.']", "z..z"))))), "\n"));
  echo(LINE(14,(assignCallTemp(eo_1, x_decoct(x_ord("e"))),concat3("octal value of \\145 is ", eo_1, "\n"))));
  echo(LINE(15,(assignCallTemp(eo_1, x_dechex(x_ord("\345"))),concat3("hex value of \\xe5 is ", eo_1, "\n"))));
  echo(concat(LINE(17,x_addcslashes("abcdefghijklmnopqrstuvwxyz", "ae..pz")), "\n"));
  echo(concat(LINE(18,x_stripcslashes(x_addcslashes("abcdefghijklmnopqrstuvwxyz", "ae..pz"))), "\n"));
  echo(LINE(19,(assignCallTemp(eo_1, x_decoct(x_ord(x_stripcslashes("\\f")))),concat3("octal for \\f is: ", eo_1, "\n"))));
  echo(LINE(21,(assignCallTemp(eo_1, x_stripcslashes("\\f")),concat3("[", eo_1, "]\n"))));
  {
    echo(toString(equal("\n\r", LINE(23,x_stripcslashes("\\n\\r")))));
    echo("\n");
  }
  echo(concat(LINE(24,x_stripcslashes("\\065\\x64")), "\n"));
  echo(concat(LINE(26,x_stripcslashes("")), "\n"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
