
#include <php/roadsend/pcc/runtime/ext/pcc-win/tests/messagebox.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_IDYES = "IDYES";
const StaticString k_MB_YESNOCANCEL = "MB_YESNOCANCEL";
const StaticString k_IDNO = "IDNO";
const StaticString k_IDCANCEL = "IDCANCEL";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$pcc_win$tests$messagebox_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/pcc-win/tests/messagebox.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$pcc_win$tests$messagebox_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ret __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ret") : g->GV(ret);

  (v_ret = LINE(3,invoke_failed("win_messagebox", Array(ArrayInit(3).set(0, "this is text yaya").set(1, "roadsend caption").set(2, k_MB_YESNOCANCEL).create()), 0x00000000DE8E3E96LL)));
  echo(LINE(4,concat3("ret was ", toString(v_ret), "\n")));
  {
    Variant tmp2 = (v_ret);
    int tmp3 = -1;
    if (equal(tmp2, (k_IDYES))) {
      tmp3 = 0;
    } else if (equal(tmp2, (k_IDNO))) {
      tmp3 = 1;
    } else if (equal(tmp2, (k_IDCANCEL))) {
      tmp3 = 2;
    }
    switch (tmp3) {
    case 0:
      {
        LINE(7,invoke_failed("win_messagebox", Array(ArrayInit(2).set(0, "you said yes").set(1, "yay!!").create()), 0x00000000DE8E3E96LL));
        goto break1;
      }
    case 1:
      {
        LINE(10,invoke_failed("win_messagebox", Array(ArrayInit(2).set(0, "you said no :(").set(1, "dang").create()), 0x00000000DE8E3E96LL));
        goto break1;
      }
    case 2:
      {
        LINE(13,invoke_failed("win_messagebox", Array(ArrayInit(2).set(0, "or, not").set(1, "doh").create()), 0x00000000DE8E3E96LL));
        goto break1;
      }
    }
    break1:;
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
