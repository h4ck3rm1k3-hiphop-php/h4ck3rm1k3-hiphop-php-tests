
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_pcc_win_tests_messagebox_fw_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_pcc_win_tests_messagebox_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_IDYES;
extern const StaticString k_MB_YESNOCANCEL;
extern const StaticString k_IDNO;
extern const StaticString k_IDCANCEL;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_pcc_win_tests_messagebox_fw_h__
