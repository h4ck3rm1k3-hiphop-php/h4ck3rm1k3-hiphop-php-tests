
#include <php/roadsend/pcc/runtime/ext/pcc-win/tests/registry.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_PCC_VERSION_MINOR = "PCC_VERSION_MINOR";
const StaticString k_HKEY_LOCAL_MACHINE = "HKEY_LOCAL_MACHINE";
const StaticString k_HKEY_CURRENT_USER = "HKEY_CURRENT_USER";
const StaticString k_PCC_VERSION_MAJOR = "PCC_VERSION_MAJOR";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$pcc_win$tests$registry_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/pcc-win/tests/registry.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$pcc_win$tests$registry_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_wallpaper __attribute__((__unused__)) = (variables != gVariables) ? variables->get("wallpaper") : g->GV(wallpaper);
  Variant &v_install_dir __attribute__((__unused__)) = (variables != gVariables) ? variables->get("install_dir") : g->GV(install_dir);

  echo("Linux\n");
  (v_wallpaper = LINE(5,invoke_failed("win_get_registry_key", Array(ArrayInit(3).set(0, k_HKEY_CURRENT_USER).set(1, "Control Panel\\Desktop").set(2, "Wallpaper").create()), 0x0000000017C5C5C3LL)));
  echo(LINE(6,concat3("The current wallpaper file is [", toString(v_wallpaper), "]\n")));
  (v_install_dir = LINE(12,(assignCallTemp(eo_0, k_HKEY_LOCAL_MACHINE),assignCallTemp(eo_1, LINE(11,concat4("SOFTWARE\\Roadsend\\Compiler\\", k_PCC_VERSION_MAJOR, ".", k_PCC_VERSION_MINOR))),invoke_failed("win_get_registry_key", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, "root").create()), 0x0000000017C5C5C3LL))));
  echo(LINE(14,concat3("The Roadsend install dir is [", toString(v_install_dir), "]\n")));
  LINE(21,(assignCallTemp(eo_0, k_HKEY_LOCAL_MACHINE),assignCallTemp(eo_1, LINE(19,concat4("SOFTWARE\\Roadsend\\Compiler\\", k_PCC_VERSION_MAJOR, ".", k_PCC_VERSION_MINOR))),invoke_failed("win_set_registry_key", Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, "stringtest").set(3, "mystringval").create()), 0x00000000F2C31C7ELL)));
  echo(toString(LINE(27,(assignCallTemp(eo_0, k_HKEY_LOCAL_MACHINE),assignCallTemp(eo_1, LINE(26,concat4("SOFTWARE\\Roadsend\\Compiler\\", k_PCC_VERSION_MAJOR, ".", k_PCC_VERSION_MINOR))),invoke_failed("win_get_registry_key", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, "stringtest").create()), 0x0000000017C5C5C3LL)))));
  LINE(34,(assignCallTemp(eo_0, k_HKEY_LOCAL_MACHINE),assignCallTemp(eo_1, LINE(32,concat4("SOFTWARE\\Roadsend\\Compiler\\", k_PCC_VERSION_MAJOR, ".", k_PCC_VERSION_MINOR))),invoke_failed("win_set_registry_key", Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, "inttest").set(3, 25LL).create()), 0x00000000F2C31C7ELL)));
  echo(toString(LINE(40,(assignCallTemp(eo_0, k_HKEY_LOCAL_MACHINE),assignCallTemp(eo_1, LINE(39,concat4("SOFTWARE\\Roadsend\\Compiler\\", k_PCC_VERSION_MAJOR, ".", k_PCC_VERSION_MINOR))),invoke_failed("win_get_registry_key", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, "inttest").create()), 0x0000000017C5C5C3LL)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
