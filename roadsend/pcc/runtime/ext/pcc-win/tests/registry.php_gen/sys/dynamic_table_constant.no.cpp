
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_PCC_VERSION_MINOR;
extern const StaticString k_HKEY_LOCAL_MACHINE;
extern const StaticString k_HKEY_CURRENT_USER;
extern const StaticString k_PCC_VERSION_MAJOR;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 4:
      HASH_RETURN(0x17D97BE293021A4CLL, k_PCC_VERSION_MAJOR, PCC_VERSION_MAJOR);
      break;
    case 6:
      HASH_RETURN(0x63003F76E77774E6LL, k_PCC_VERSION_MINOR, PCC_VERSION_MINOR);
      break;
    case 7:
      HASH_RETURN(0x50130D256346DD37LL, k_HKEY_CURRENT_USER, HKEY_CURRENT_USER);
      HASH_RETURN(0x75D309876FCF399FLL, k_HKEY_LOCAL_MACHINE, HKEY_LOCAL_MACHINE);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
