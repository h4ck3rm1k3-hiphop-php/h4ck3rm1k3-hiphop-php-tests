
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_pcc_win_tests_nophp_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_pcc_win_tests_nophp_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/pcc-win/tests.nophp.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$pcc_win$tests(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_pcc_win_tests_nophp_h__
