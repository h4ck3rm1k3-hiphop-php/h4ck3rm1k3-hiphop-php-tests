
#include <php/roadsend/pcc/runtime/ext/curl/tests/basic.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$curl$tests$basic_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/curl/tests/basic.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$curl$tests$basic_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ch") : g->GV(ch);
  Variant &v_foo __attribute__((__unused__)) = (variables != gVariables) ? variables->get("foo") : g->GV(foo);

  LINE(3,x_var_dump(1, x_curl_version()));
  echo("initing\n");
  (v_ch = LINE(6,x_curl_init()));
  echo("\nsetting options:");
  echo(toString(LINE(9,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "http://www.google.com"))));
  echo("\nexecing:");
  echo(toString(LINE(13,x_curl_exec(toObject(v_ch)))));
  echo("\nsetting RETURNTRANSFER:");
  LINE(16,x_curl_setopt(toObject(v_ch), toInt32(19913LL) /* CURLOPT_RETURNTRANSFER */, 1LL));
  echo("\nexecing:");
  (v_foo = LINE(19,x_curl_exec(toObject(v_ch))));
  LINE(20,x_var_dump(1, v_foo));
  echo("\nexecing:");
  (v_foo = LINE(23,x_curl_exec(toObject(v_ch))));
  LINE(24,x_var_dump(1, v_foo));
  echo("\nclosing:");
  echo((LINE(28,x_curl_close(toObject(v_ch))), null));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
