
#include <php/roadsend/pcc/runtime/ext/curl/tests/curlerrors.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$curl$tests$curlerrors_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/curl/tests/curlerrors.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$curl$tests$curlerrors_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ch") : g->GV(ch);

  (v_ch = LINE(3,x_curl_init()));
  echo("\n1 setopt: ");
  echo(toString(LINE(5,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "fnord://this is not a url"))));
  echo("\n2 exec: ");
  echo(toString(LINE(7,x_curl_exec(toObject(v_ch)))));
  echo("\n3 error: ");
  echo(LINE(9,x_curl_error(toObject(v_ch))));
  echo("\n4 errno: ");
  echo(toString(LINE(11,x_curl_errno(toObject(v_ch)))));
  echo(toString(LINE(14,x_curl_setopt(toObject(v_ch), toInt32("fake option"), 0LL))));
  echo("\n5 close: ");
  echo((LINE(17,x_curl_close(toObject(v_ch))), null));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
