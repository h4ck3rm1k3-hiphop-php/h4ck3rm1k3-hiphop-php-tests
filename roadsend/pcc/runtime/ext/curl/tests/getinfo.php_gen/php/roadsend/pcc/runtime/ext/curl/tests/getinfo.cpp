
#include <php/roadsend/pcc/runtime/ext/curl/tests/getinfo.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$curl$tests$getinfo_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/curl/tests/getinfo.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$curl$tests$getinfo_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ch") : g->GV(ch);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  (v_ch = LINE(3,x_curl_init()));
  LINE(5,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "http://www.example.com"));
  LINE(6,x_curl_exec(toObject(v_ch)));
  (v_a = LINE(7,x_curl_getinfo(toObject(v_ch))));
  v_a.set("total_time", ("xxx"), 0x4FDC7130C55A7B2ELL);
  v_a.set("namelookup_time", ("xxx"), 0x643F2D07DFDFE699LL);
  v_a.set("connect_time", ("xxx"), 0x1406BD0A1A1915F2LL);
  v_a.set("pretransfer_time", ("xxx"), 0x484C122103982369LL);
  v_a.set("starttransfer_time", ("xxx"), 0x3BF02E500937697DLL);
  v_a.set("speed_upload", ("xxx"), 0x779092A5D8405787LL);
  v_a.set("speed_download", ("xxx"), 0x1133F17384F90991LL);
  LINE(18,x_print_r(v_a));
  echo(toString(LINE(20,x_curl_getinfo(toObject(v_ch), toInt32(2097154LL) /* CURLINFO_HTTP_CODE */))));
  LINE(22,x_curl_close(toObject(v_ch)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
