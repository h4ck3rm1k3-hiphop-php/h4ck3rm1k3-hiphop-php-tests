
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_curl_tests_callbacks_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_curl_tests_callbacks_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/curl/tests/callbacks.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$curl$tests$callbacks_php(bool incOnce = false, LVariableTable* variables = NULL);
int f_read_header(CVarRef v_ch, CVarRef v_string);
int f_read_body(CVarRef v_ch, CVarRef v_string);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_curl_tests_callbacks_h__
