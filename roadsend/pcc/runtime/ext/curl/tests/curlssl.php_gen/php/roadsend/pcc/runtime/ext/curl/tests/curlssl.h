
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_curl_tests_curlssl_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_curl_tests_curlssl_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/curl/tests/curlssl.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$curl$tests$curlssl_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_curl_tests_curlssl_h__
