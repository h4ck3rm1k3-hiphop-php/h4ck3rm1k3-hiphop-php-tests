
#include <php/roadsend/pcc/runtime/ext/curl/other-tests/ftpupload.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_CURLOPT_FTPASCII = "CURLOPT_FTPASCII";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/curl/other-tests/ftpupload.php line 3 */
int f_curl_upload(CStrRef v_src) {
  FUNCTION_INJECTION(curl_upload);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_fn;
  Variant v_fp;
  String v_dest;
  Variant v_ch;
  Variant v_result;
  String v_errorMsg;
  int v_errorNumber = 0;

  (v_fn = LINE(4,x_basename(v_src)));
  (v_fp = LINE(5,x_fopen(v_src, "r")));
  (v_dest = toString("ftp://devel:foobar@localhost/") + v_fn);
  (v_ch = LINE(7,x_curl_init()));
  LINE(8,x_curl_setopt(toObject(v_ch), toInt32(46LL) /* CURLOPT_UPLOAD */, 1LL));
  LINE(9,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, v_dest));
  LINE(10,x_curl_setopt(toObject(v_ch), toInt32(k_CURLOPT_FTPASCII), 0LL));
  LINE(11,x_curl_setopt(toObject(v_ch), toInt32(19913LL) /* CURLOPT_RETURNTRANSFER */, 1LL));
  LINE(12,x_curl_setopt(toObject(v_ch), toInt32(10009LL) /* CURLOPT_INFILE */, v_fp));
  LINE(13,(assignCallTemp(eo_0, toObject(v_ch)),assignCallTemp(eo_2, x_filesize(v_src)),x_curl_setopt(eo_0, toInt32(14LL) /* CURLOPT_INFILESIZE */, eo_2)));
  (v_result = LINE(14,x_curl_exec(toObject(v_ch))));
  LINE(15,x_fclose(toObject(v_fp)));
  LINE(16,x_print_r(x_curl_getinfo(toObject(v_ch))));
  (v_errorMsg = LINE(17,x_curl_error(toObject(v_ch))));
  (v_errorNumber = LINE(18,x_curl_errno(toObject(v_ch))));
  echo(LINE(19,concat5("\nErrMsg: ", v_errorMsg, "\nErrNo: ", toString(v_errorNumber), "\n")));
  LINE(20,x_curl_close(toObject(v_ch)));
  return v_errorNumber;
} /* function */
Variant pm_php$roadsend$pcc$runtime$ext$curl$other_tests$ftpupload_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/curl/other-tests/ftpupload.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$curl$other_tests$ftpupload_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  LINE(25,f_curl_upload("foo.bin"));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
