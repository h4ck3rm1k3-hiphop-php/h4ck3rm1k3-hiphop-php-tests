
#include <php/roadsend/pcc/runtime/ext/curl/other-tests/dumppost.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$curl$other_tests$dumppost_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/curl/other-tests/dumppost.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$curl$other_tests$dumppost_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  if (toBoolean(LINE(4,x_count(g->gv__POST)))) {
    echo("these fields were posted:<br>\n");
    LINE(6,x_var_dump(1, g->gv__POST));
  }
  else {
    echo("nothing was posted\n");
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
