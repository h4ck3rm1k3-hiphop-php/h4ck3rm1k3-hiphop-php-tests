
#include <php/roadsend/pcc/runtime/ext/curl/other-tests/formpost.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/curl/other-tests/formpost.php line 3 */
Variant f_encodepostarr(CVarRef v_arr) {
  FUNCTION_INJECTION(encodePostArr);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_str;
  Primitive v_k = 0;
  Variant v_v;

  (v_str = "");
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_arr.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_v = iter3->second();
      v_k = iter3->first();
      {
        concat_assign(v_str, LINE(6,(assignCallTemp(eo_0, toString(v_k) + toString("=")),assignCallTemp(eo_1, x_urlencode(toString(v_v))),concat3(eo_0, eo_1, "&"))));
      }
    }
  }
  return LINE(8,x_substr(v_str, toInt32(0LL), toInt32(-1LL)));
} /* function */
Variant pm_php$roadsend$pcc$runtime$ext$curl$other_tests$formpost_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/curl/other-tests/formpost.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$curl$other_tests$formpost_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_ch __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ch") : g->GV(ch);
  Variant &v_fields __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fields") : g->GV(fields);
  Variant &v_error_code __attribute__((__unused__)) = (variables != gVariables) ? variables->get("error_code") : g->GV(error_code);

  (v_ch = LINE(11,x_curl_init()));
  LINE(12,x_curl_setopt(toObject(v_ch), toInt32(47LL) /* CURLOPT_POST */, 1LL));
  LINE(13,x_curl_setopt(toObject(v_ch), toInt32(10002LL) /* CURLOPT_URL */, "http://localhost/curl/dumppost.php"));
  LINE(14,x_curl_setopt(toObject(v_ch), toInt32(10018LL) /* CURLOPT_USERAGENT */, "agent"));
  LINE(15,x_curl_setopt(toObject(v_ch), toInt32(42LL) /* CURLOPT_HEADER */, 0LL));
  LINE(16,x_curl_setopt(toObject(v_ch), toInt32(19913LL) /* CURLOPT_RETURNTRANSFER */, 0LL));
  LINE(17,x_curl_setopt(toObject(v_ch), toInt32(13LL) /* CURLOPT_TIMEOUT */, 180LL));
  LINE(19,x_curl_setopt(toObject(v_ch), toInt32(41LL) /* CURLOPT_VERBOSE */, 1LL));
  LINE(20,x_curl_setopt(toObject(v_ch), toInt32(43LL) /* CURLOPT_NOPROGRESS */, 1LL));
  LINE(21,x_curl_setopt(toObject(v_ch), toInt32(45LL) /* CURLOPT_FAILONERROR */, 0LL));
  (v_fields = ScalarArrays::sa_[0]);
  LINE(28,(assignCallTemp(eo_0, toObject(v_ch)),assignCallTemp(eo_2, f_encodepostarr(v_fields)),x_curl_setopt(eo_0, toInt32(10015LL) /* CURLOPT_POSTFIELDS */, eo_2)));
  print(toString(LINE(31,x_curl_exec(toObject(v_ch)))));
  if (toBoolean(LINE(33,x_curl_errno(toObject(v_ch))))) {
    (v_error_code = "MSG_CURL_ERROR");
    f_exit(LINE(35,x_curl_error(toObject(v_ch))));
  }
  LINE(38,x_curl_close(toObject(v_ch)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
