
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_curl_other_tests_formpost_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_curl_other_tests_formpost_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/curl/other-tests/formpost.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$curl$other_tests$formpost_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_encodepostarr(CVarRef v_arr);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_curl_other_tests_formpost_h__
