
#include <php/roadsend/pcc/runtime/ext/gtk2/other-tests/hello.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/hello.php line 19 */
void f_destroy() {
  FUNCTION_INJECTION(destroy);
  LINE(21,throw_fatal("unknown class gtk", ((void*)NULL)));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/hello.php line 11 */
bool f_delete_event() {
  FUNCTION_INJECTION(delete_event);
  return false;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/hello.php line 27 */
void f_hello() {
  FUNCTION_INJECTION(hello);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_window __attribute__((__unused__)) = g->GV(window);
  print("Hello World!\n");
  LINE(31,gv_window.o_invoke_few_args("destroy", 0x45CCFF10463AB662LL, 0));
} /* function */
Variant i_destroy(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x45CCFF10463AB662LL, destroy) {
    return (f_destroy(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_delete_event(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0549FAE24D08964BLL, delete_event) {
    return (f_delete_event());
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_hello(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x35B3B21789F02516LL, hello) {
    return (f_hello(), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$gtk2$other_tests$hello_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/gtk2/other-tests/hello.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$gtk2$other_tests$hello_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_window __attribute__((__unused__)) = (variables != gVariables) ? variables->get("window") : g->GV(window);
  Variant &v_button __attribute__((__unused__)) = (variables != gVariables) ? variables->get("button") : g->GV(button);
  Variant &v_tt __attribute__((__unused__)) = (variables != gVariables) ? variables->get("tt") : g->GV(tt);

  if (!(LINE(4,x_extension_loaded("php-gtk")))) {
    LINE(5,x_dl("php_gtk.so"));
  }
  (v_window = LINE(38,create_object("gtkwindow", Array())));
  LINE(39,v_window.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "destroy", "destroy"));
  LINE(40,v_window.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "delete-event", "delete_event"));
  LINE(41,v_window.o_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 10LL));
  (v_button = LINE(47,create_object("gtkbutton", Array(ArrayInit(1).set(0, "Hello World!").create()))));
  LINE(48,v_button.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "clicked", "hello"));
  (v_tt = LINE(53,create_object("gtktooltips", Array())));
  LINE(54,v_tt.o_invoke_few_args("set_delay", 0x78C815B577A3085ELL, 1, 200LL));
  LINE(55,v_tt.o_invoke_few_args("set_tip", 0x79F69867444F1012LL, 3, v_button, "Prints \"Hello World!\"", ""));
  LINE(56,v_tt.o_invoke_few_args("enable", 0x36861E5E851AEE23LL, 0));
  LINE(58,v_window.o_invoke_few_args("add", 0x15D34462FC79458BLL, 1, v_button));
  LINE(63,v_window.o_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
  LINE(66,throw_fatal("unknown class gtk", ((void*)NULL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
